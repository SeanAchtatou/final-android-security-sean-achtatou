package com.crittermap.backcountrynavigator.map.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.location.GpsStatus;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.view.MultiTouchController;
import com.crittermap.backcountrynavigator.nav.Navigator;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.places.PlaceAddress;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.settings.CompassRotation;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.Ruler;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import com.crittermap.backcountrynavigator.view.VersionedScaleGestureDetector;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class BCNMapView extends View implements GestureDetector.OnGestureListener, Observer, SeekBar.OnSeekBarChangeListener, MultiTouchController.MultiTouchObjectCanvas<Object>, VersionedScaleGestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    static final int FINGERSIZE = 48;
    private static final double ZOOM_LOG_BASE_INV = (1.0d / Math.log(1.5384615384615383d));
    private static final double ZOOM_SENSITIVITY = 1.3d;
    private int BH;
    private int BW;
    final int MINMOVEMENT = 8;
    private Paint acquirePaint;
    private boolean acquiring;
    boolean bPanning = false;
    boolean bScrolling = false;
    ArrayList<CoordinateBoundingBox> boxList = new ArrayList<>();
    Paint boxPaint;
    Paint boxesPaint;
    Position center = this.renderedAtPosition;
    Drawable dCompassRose;
    Drawable dGpsArrow;
    float decl = 0.0f;
    private float densityScale;
    GestureDetector detector;
    private Paint errorCirclePaint;
    boolean following = false;
    Position gotoPos = null;
    private float lastAccuracy;
    private Position lastGpsPosition;
    long lastNumRecordings = 0;
    private int level = 9;
    private boolean mBAngleFromCompass;
    private int mCompassAccuracy = 0;
    BViewContextMenuInfo mContextMenuInfo = null;
    int mGPSStatCounter = 0;
    LabelListener mLabelListener = null;
    VersionedScaleGestureDetector mScaleDetector;
    Scroller mScroller;
    boolean mShowRuler = true;
    float mapAngle = 0.0f;
    ArrayList<CoordinateBoundingBox> markedRectangles = new ArrayList<>();
    private int maxlevel = 15;
    private int minlevel = 1;
    private MultiTouchController<Object> multiTouchController;
    Navigator nav = null;
    private Paint paintCross;
    private Paint paintGotoLine;
    Paint paintGpsMarker;
    private Paint paintRuler;
    Paint paintRulerShadow;
    private Paint paintTrack;
    private Paint paintTrackPending;
    private GpsStatus recentGpsStatus;
    int renderToken;
    boolean rendered = false;
    private Position renderedAtPosition = new Position(-120.0d, 45.0d);
    private Bitmap renderedBitmap = null;
    private int renderedLevel = 10;
    MapRenderer renderer;
    boolean renderingRequested = false;
    Position renderingRequestedPos = null;
    float sculptEndX;
    float sculptEndY;
    boolean sculptInProgress = false;
    float sculptStartX;
    float sculptStartY;
    Drawable sculpticon;
    boolean sculpting;
    boolean shouldTiltMap = true;
    TextView tC;
    TileResolver tileResolver = new GMTileResolver();
    boolean tiltMap = false;
    boolean trackingAngle = false;
    public TextView tvLevel;

    public interface LabelListener {
        void labelEvent(BViewContextMenuInfo bViewContextMenuInfo);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public BCNMapView(Context context) {
        super(context);
        init(context);
    }

    public BCNMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public BCNMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /* access modifiers changed from: package-private */
    public void init(Context context) {
        this.detector = new GestureDetector(context, this);
        this.detector.setIsLongpressEnabled(true);
        this.detector.setOnDoubleTapListener(this);
        this.mScaleDetector = VersionedScaleGestureDetector.newInstance(context, this);
        this.mScroller = new Scroller(context);
        this.renderer = new MapRenderer(new Handler() {
            public void handleMessage(Message msg) {
                if (msg.obj != null) {
                    BCNMapView.this.onRenderUpdate((RenderParams) msg.obj);
                }
            }
        }, getContext());
        this.densityScale = getContext().getResources().getDisplayMetrics().density;
        this.boxPaint = new Paint();
        Resources res = getResources();
        this.boxPaint.setColor(res.getColor(R.color.sculpt_box));
        this.boxPaint.setStyle(Paint.Style.FILL);
        this.boxesPaint = new Paint();
        this.boxesPaint.setColor(res.getColor(R.color.sculpted_box));
        this.sculpticon = res.getDrawable(17301562);
        this.paintGpsMarker = new Paint();
        this.paintGpsMarker.setARGB(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintGpsMarker.setTextAlign(Paint.Align.CENTER);
        this.paintGpsMarker.setTextSize(this.densityScale * 14.0f);
        this.paintGotoLine = new Paint();
        this.paintGotoLine.setARGB(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, 89, 42, 170);
        this.paintGotoLine.setAntiAlias(true);
        this.paintGotoLine.setStrokeWidth(5.0f);
        this.paintGotoLine.setTextAlign(Paint.Align.LEFT);
        this.paintGotoLine.setTextSize(this.densityScale * 14.0f);
        this.paintRuler = new Paint();
        this.paintRuler.setARGB(176, 255, 0, 0);
        this.paintRuler.setAntiAlias(true);
        this.paintRuler.setStrokeWidth(this.densityScale * 2.0f);
        this.paintRuler.setTextAlign(Paint.Align.LEFT);
        this.paintRuler.setTextSize(16.0f * this.densityScale);
        this.paintRulerShadow = new Paint();
        this.paintRulerShadow.setARGB(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, 255, 255, 255);
        this.paintRulerShadow.setAntiAlias(true);
        this.paintRulerShadow.setStrokeWidth(4.0f * this.densityScale);
        this.acquirePaint = new Paint();
        this.acquirePaint.setColor(context.getResources().getColor(R.color.error_circle));
        this.acquirePaint.setAntiAlias(true);
        this.acquirePaint.setStrokeWidth(this.densityScale * 2.0f);
        this.acquirePaint.setTextSize(this.densityScale * 14.0f);
        this.acquirePaint.setTextAlign(Paint.Align.LEFT);
        this.dGpsArrow = getResources().getDrawable(R.drawable.gps_pointer);
        this.dCompassRose = getResources().getDrawable(R.drawable.north);
        this.multiTouchController = new MultiTouchController<>(this, getResources(), false);
        this.errorCirclePaint = new Paint();
        this.errorCirclePaint.setColor(context.getResources().getColor(R.color.error_circle));
        this.errorCirclePaint.setStyle(Paint.Style.STROKE);
        this.errorCirclePaint.setStrokeWidth(this.densityScale * 2.0f);
        this.errorCirclePaint.setAlpha(127);
        this.errorCirclePaint.setAntiAlias(true);
        this.paintCross = new Paint();
        this.paintCross.setARGB(112, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS, MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintCross.setStyle(Paint.Style.STROKE);
        this.paintCross.setStrokeWidth(this.densityScale * 2.0f);
        this.paintCross.setAntiAlias(true);
        this.paintCross.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        this.paintTrack = new Paint();
        this.paintTrack.setColor((int) MyTracksConstants.MENU_SEND_TO_GOOGLE);
        this.paintTrack.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintTrack.setStrokeWidth(5.0f);
        this.paintTrack.setAntiAlias(true);
        this.paintTrackPending = new Paint();
        this.paintTrackPending.setColor((int) MyTracksConstants.MENU_SEND_TO_GOOGLE);
        this.paintTrack.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        this.paintTrackPending.setStrokeWidth(3.0f);
        this.paintTrackPending.setAntiAlias(true);
    }

    public MapRenderer getRenderer() {
        return this.renderer;
    }

    public TextView getTvLevel() {
        return this.tvLevel;
    }

    public void setTvLevel(TextView tvLevel2) {
        this.tvLevel = tvLevel2;
    }

    /* access modifiers changed from: private */
    public void onRenderUpdate(RenderParams parm) {
        if (this.renderToken == parm.token && this.renderedBitmap != null) {
            this.renderer.draw(new Canvas(this.renderedBitmap), parm);
            this.rendered = true;
            this.renderedAtPosition = parm.center;
            this.renderedLevel = parm.level;
            invalidate();
            this.mScaleDetector.setRenderDone();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measure(widthMeasureSpec), measure(heightMeasureSpec));
    }

    private int measure(int measurespec) {
        int result = 0;
        int specMode = View.MeasureSpec.getMode(measurespec);
        int specSize = View.MeasureSpec.getSize(measurespec);
        if (specMode == 0) {
            return 200;
        }
        if (specMode == 1073741824) {
            result = specSize;
        } else if (specMode == Integer.MIN_VALUE) {
            result = specSize;
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public float getTiltAngle() {
        if (this.mBAngleFromCompass) {
            return ((float) CompassRotation.getrotation(this)) + this.mapAngle;
        }
        return this.mapAngle;
    }

    /* access modifiers changed from: package-private */
    public void drawRecentTrackPoints(Canvas canvas, float cx, float cy) {
        FloatBuffer fbuf;
        if (this.nav != null && (fbuf = this.nav.getRecentTrackPoints()) != null) {
            float[] path = this.tileResolver.transformToScreen(fbuf, this.level, getWidth(), getHeight(), this.tileResolver.screenBoundingBox(this.center, this.level, getWidth(), getHeight()));
            canvas.drawLines(path, this.paintTrack);
            if (this.lastGpsPosition != null) {
                float tx = path[path.length - 2];
                float ty = path[path.length - 1];
                if (isFollowing()) {
                    canvas.drawLine(tx, ty, cx, cy, this.paintTrackPending);
                } else {
                    canvas.drawLine(tx, ty, cx + ((float) this.tileResolver.pixelDistanceX(this.center, this.lastGpsPosition, this.level)), cy + ((float) this.tileResolver.pixelDistanceY(this.center, this.lastGpsPosition, this.level)), this.paintTrackPending);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.rendered || this.renderingRequested) {
            canvas.drawColor(17170446);
            float offCenterX = -((float) this.tileResolver.pixelDistanceX(this.renderedAtPosition, this.center, this.level));
            float offCenterY = -((float) this.tileResolver.pixelDistanceY(this.renderedAtPosition, this.center, this.level));
            float offSetY = offCenterY - (((float) (this.renderedBitmap.getHeight() - getHeight())) / 2.0f);
            float offSetX = offCenterX - (((float) (this.renderedBitmap.getWidth() - getWidth())) / 2.0f);
            float cx = ((float) getWidth()) / 2.0f;
            float cy = ((float) getHeight()) / 2.0f;
            canvas.save();
            if (this.mScaleDetector.shouldInflate()) {
                float scale = this.mScaleDetector.getInflationScale();
                canvas.scale(scale, scale, this.mScaleDetector.getFocusX(), this.mScaleDetector.getFocusY());
            } else if (this.renderedLevel != this.level) {
                float scale2 = (float) Math.pow(2.0d, (double) (this.level - this.renderedLevel));
                canvas.scale(scale2, scale2, cx + offCenterX, cy + offCenterY);
            }
            if (isTiltMap()) {
                canvas.rotate(-getTiltAngle(), cx, cy);
            }
            canvas.drawBitmap(this.renderedBitmap, offSetX, offSetY, (Paint) null);
            drawRecentTrackPoints(canvas, cx, cy);
            if (isTiltMap()) {
                drawNorthArrow(canvas, cx, cy);
            }
            if (this.mShowRuler) {
                drawRuler(canvas, cx, cy);
            }
            if (!(this.lastGpsPosition == null || this.gotoPos == null)) {
                canvas.drawLine(cx + ((float) this.tileResolver.pixelDistanceX(this.center, this.lastGpsPosition, this.level)), cy + ((float) this.tileResolver.pixelDistanceY(this.center, this.lastGpsPosition, this.level)), cx + ((float) this.tileResolver.pixelDistanceX(this.center, this.gotoPos, this.level)), cy + ((float) this.tileResolver.pixelDistanceY(this.center, this.gotoPos, this.level)), this.paintGotoLine);
            }
            if (this.sculptInProgress) {
                canvas.drawRect(Math.min(this.sculptStartX, this.sculptEndX), Math.min(this.sculptStartY, this.sculptEndY), Math.max(this.sculptStartX, this.sculptEndX), Math.max(this.sculptStartY, this.sculptEndY), this.boxPaint);
                this.sculpticon.setBounds(((int) this.sculptEndX) - 12, ((int) this.sculptEndY) - 12, ((int) this.sculptEndX) + 12, ((int) this.sculptEndY) + 12);
            }
            Iterator<CoordinateBoundingBox> it = this.markedRectangles.iterator();
            while (it.hasNext()) {
                canvas.drawRect(this.tileResolver.findPixelRange(it.next(), this.center, (int) cx, (int) cy, this.level), this.boxesPaint);
            }
            if (this.lastGpsPosition != null) {
                if (isFollowing()) {
                    drawGpsMarker(canvas, cx, cy, this.lastGpsPosition);
                } else {
                    drawGpsMarker(canvas, cx + ((float) this.tileResolver.pixelDistanceX(this.center, this.lastGpsPosition, this.level)), cy + ((float) this.tileResolver.pixelDistanceY(this.center, this.lastGpsPosition, this.level)), this.lastGpsPosition);
                }
            }
            canvas.restore();
            if (this.nav != null) {
                drawGpsStatus(canvas, cx);
            }
            if (!isFollowing()) {
                float t = 6.0f * this.densityScale;
                canvas.drawLine(cx - t, cy, cx + t, cy, this.paintCross);
                canvas.drawLine(cx, cy - t, cx, cy + t, this.paintCross);
                return;
            }
            return;
        }
        requestRendering();
    }

    private void drawGpsStatus(Canvas canvas, float cx) {
        if (this.nav.isAcquiring()) {
            int i = this.mGPSStatCounter;
            this.mGPSStatCounter = i + 1;
            if ((i & 1) > 0) {
            }
            canvas.drawText(String.valueOf(getContext().getString(R.string.notice_acquiring)) + ". . . .", cx, 16.0f * this.densityScale, this.acquirePaint);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawNorthArrow(Canvas canvas, float cx, float cy) {
        Drawable d;
        int px = (int) cx;
        int py = ((int) cy) - (Math.min(getWidth(), getHeight()) / 2);
        switch (this.mCompassAccuracy) {
            case 0:
                d = getResources().getDrawable(R.drawable.northred);
                this.paintGpsMarker.setColor(-2130771968);
                break;
            case 1:
                d = getResources().getDrawable(R.drawable.northyellow);
                this.paintGpsMarker.setColor(-2130706688);
                break;
            case 2:
                d = getResources().getDrawable(R.drawable.northgreenyellow);
                this.paintGpsMarker.setColor(-2136277248);
                break;
            case 3:
                d = getResources().getDrawable(R.drawable.northgreen);
                this.paintGpsMarker.setColor(-2147418368);
                break;
            default:
                d = getResources().getDrawable(R.drawable.north);
                this.paintGpsMarker.setColor(-2130771968);
                break;
        }
        d.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
        int h = d.getIntrinsicHeight();
        int w = d.getIntrinsicWidth();
        d.setBounds(px - (w / 2), py, (w / 2) + px, py + h);
        canvas.drawText(getResources().getString(R.string.north_letter), (float) px, (float) (((h * 2) / 3) + py), this.paintGpsMarker);
    }

    /* access modifiers changed from: package-private */
    public void drawGpsMarker(Canvas canvas, float cx, float cy, Position center2) {
        canvas.save();
        canvas.rotate(getTiltAngle(), cx, cy);
        Drawable d = this.dGpsArrow;
        int h = d.getIntrinsicHeight() / 2;
        int w = d.getIntrinsicWidth() / 2;
        d.setBounds(((int) cx) - w, ((int) cy) - h, ((int) cx) + w, ((int) cy) + h);
        d.draw(canvas);
        if (this.lastAccuracy != 0.0f) {
            canvas.drawCircle(cx, cy, this.tileResolver.getPixelWidthForDistance(center2, this.level, this.lastAccuracy), this.errorCirclePaint);
        }
        canvas.restore();
    }

    /* access modifiers changed from: package-private */
    public void drawRuler(Canvas canvas, float cx, float cy) {
        String rulerText;
        float textWidth;
        int min = Math.min(getWidth(), getHeight()) / 2;
        int i = (int) cx;
        Ruler ruler = Ruler.getRuler(this.center, (getWidth() * 2) / 3, getHeight() / 2, this.level, this.tileResolver);
        canvas.drawLine(cx - ((float) (ruler.pixels / 2)), (float) (0 + 5), cx + ((float) (ruler.pixels / 2)), (float) (0 + 5), this.paintRulerShadow);
        canvas.drawLine(cx - ((float) (ruler.pixels / 2)), (float) (0 + 5), cx - ((float) (ruler.pixels / 2)), (float) (0 + 28), this.paintRulerShadow);
        canvas.drawLine(cx + ((float) (ruler.pixels / 2)), (float) (0 + 5), cx + ((float) (ruler.pixels / 2)), (float) (0 + 28), this.paintRulerShadow);
        if (BCNSettings.MetricDisplay.get()) {
            if (ruler.distance > 1000.0d) {
                rulerText = " " + (ruler.distance / 1000.0d) + " km";
            } else {
                rulerText = " " + ruler.distance + " m";
            }
            textWidth = this.paintRuler.measureText(rulerText);
        } else {
            rulerText = " " + ruler.distance + " mi";
            textWidth = this.paintRuler.measureText(rulerText);
        }
        canvas.drawRect(cx - ((float) (ruler.pixels / 2)), (float) (0 + 5), (textWidth + cx) - ((float) (ruler.pixels / 2)), (float) (0 + 31), this.paintRulerShadow);
        canvas.drawLine(cx - ((float) (ruler.pixels / 2)), (float) (0 + 5), cx + ((float) (ruler.pixels / 2)), (float) (0 + 5), this.paintRuler);
        canvas.drawLine(cx - ((float) (ruler.pixels / 2)), (float) (0 + 5), cx - ((float) (ruler.pixels / 2)), (float) (0 + 28), this.paintRuler);
        canvas.drawLine(cx + ((float) (ruler.pixels / 2)), (float) (0 + 5), cx + ((float) (ruler.pixels / 2)), (float) (0 + 28), this.paintRuler);
        canvas.drawText(rulerText, cx - ((float) (ruler.pixels / 2)), (float) (0 + 28), this.paintRuler);
    }

    public boolean isTiltMap() {
        return this.tiltMap;
    }

    public void setTiltMap(boolean tiltMap2) {
        this.tiltMap = tiltMap2;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        createRenderedBitmap(w, h, true);
    }

    private void createRenderedBitmap(int w, int h, boolean requestrender) {
        if (w > 0 && h > 0) {
            int W = w;
            int H = h;
            int B = (int) Math.ceil(Math.sqrt((double) ((W * W) + (H * H))));
            if (this.renderedBitmap == null || B > this.BH || B > this.BW) {
                this.BH = (B * 27) / 25;
                this.BW = (B * 27) / 25;
                if (this.renderedBitmap != null) {
                    this.renderedBitmap.recycle();
                    Log.i("MapView", "Recycled old bitmap");
                }
                Log.i("MapView", "Screen Buffer about to allocate: " + this.BW + "x" + this.BH);
                System.gc();
                try {
                    this.renderedBitmap = Bitmap.createBitmap(this.BW, this.BH, Bitmap.Config.ARGB_8888);
                } catch (OutOfMemoryError e) {
                    Log.e("MapView", "Screen Buffer failure: " + this.BW + "x" + this.BH, e);
                    System.gc();
                    this.renderedBitmap = Bitmap.createBitmap(this.BW, this.BH, Bitmap.Config.ARGB_8888);
                }
                if (requestrender) {
                    requestRendering();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    public ArrayList<CoordinateBoundingBox> getMarkedRectangles() {
        return this.markedRectangles;
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean handled = this.detector.onTouchEvent(event);
        this.mScaleDetector.onTouchEvent(event);
        if (event.getAction() == 1) {
            this.bScrolling = false;
            if (this.sculptInProgress) {
                float cx = ((float) getWidth()) / 2.0f;
                float cy = ((float) getHeight()) / 2.0f;
                this.markedRectangles.add(new CoordinateBoundingBox(this.tileResolver.shift(this.center, (int) (this.sculptEndX - cx), (int) (this.sculptEndY - cy), this.level), this.tileResolver.shift(this.center, (int) (this.sculptStartX - cx), (int) (this.sculptStartY - cy), this.level)));
                this.sculptInProgress = false;
                invalidate();
            }
        }
        if (!handled) {
            return super.onTouchEvent(event);
        }
        return true;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
        if (this.renderedBitmap != null && this.level != this.renderedLevel) {
            requestRendering();
            invalidate();
        }
    }

    public Position getCenter() {
        return this.center;
    }

    public void setCenter(Position center2) {
        this.center = center2;
        Position comparePos = this.renderedAtPosition;
        if (this.renderingRequestedPos != null) {
            comparePos = this.renderingRequestedPos;
        }
        int pixX = Math.abs(this.tileResolver.pixelDistanceX(center2, comparePos, this.level));
        int pixY = Math.abs(this.tileResolver.pixelDistanceY(center2, comparePos, this.level));
        if (this.renderedBitmap != null && (pixX > (this.renderedBitmap.getWidth() - getWidth()) / 2 || pixY > (this.renderedBitmap.getHeight() - getHeight()) / 2)) {
            requestRendering();
        }
        invalidate();
    }

    public void setCenterAndZoom(Position newCenter, int newLevel) {
        this.center = newCenter;
        if (newLevel > 20) {
            newLevel = 20;
        }
        if (newLevel < this.minlevel) {
            newLevel = this.minlevel;
        }
        this.level = newLevel;
        if (this.tvLevel != null) {
            this.tvLevel.setText(String.valueOf(this.level));
        }
        if (this.renderedBitmap != null) {
            requestRendering();
        }
        invalidate();
    }

    private void requestRendering() {
        if (this.renderedBitmap != null) {
            this.renderToken = this.renderer.requestRendering(this.center, this.renderedBitmap.getHeight(), this.renderedBitmap.getHeight(), this.level);
            this.renderingRequested = true;
            this.renderingRequestedPos = this.center;
        }
    }

    public void refresh(MapServer server) {
        this.minlevel = server.getMinZoom();
        this.maxlevel = server.getMaxZoom();
        if (this.level < this.minlevel) {
            this.level = this.minlevel;
        }
        this.tileResolver = server.getTileResolver();
        requestRendering();
    }

    public void refresh() {
        requestRendering();
    }

    /* access modifiers changed from: package-private */
    public void adjustCenter(float dx, float dy) {
        setCenter(this.tileResolver.shift(this.center, (int) dx, (int) dy, this.level));
    }

    public boolean onDown(MotionEvent arg0) {
        return true;
    }

    public boolean onFling(MotionEvent arg0, MotionEvent arg1, float velocityX, float velocityY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
        BViewContextMenuInfo info = new BViewContextMenuInfo();
        info.pixx = e.getX();
        info.pixy = e.getY();
        float cx = ((float) getWidth()) / 2.0f;
        float cy = ((float) getHeight()) / 2.0f;
        Position pos = this.tileResolver.shift(this.center, (int) (info.pixx - cx), (int) (info.pixy - cy), this.level);
        info.position = pos;
        this.mContextMenuInfo = info;
        info.box = this.tileResolver.screenBoundingBox(pos, this.level, FINGERSIZE, FINGERSIZE);
        if (this.mLabelListener != null) {
            this.mLabelListener.labelEvent(info);
        }
    }

    public CoordinateBoundingBox getBounds() {
        return this.tileResolver.screenBoundingBox(this.center, this.level, FINGERSIZE, FINGERSIZE);
    }

    public void setLabelListener(LabelListener listener) {
        this.mLabelListener = listener;
    }

    public class BViewContextMenuInfo implements ContextMenu.ContextMenuInfo {
        public CoordinateBoundingBox box;
        public float pixx;
        public float pixy;
        public Position position;

        public BViewContextMenuInfo() {
        }
    }

    /* access modifiers changed from: protected */
    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return this.mContextMenuInfo;
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (!this.mScaleDetector.shouldDrag()) {
            return true;
        }
        if (!this.sculpting) {
            adjustCenter(distanceX, distanceY);
            setFollowing(false);
            setTiltMap(false);
            return true;
        }
        this.sculptEndX = e2.getX();
        this.sculptEndY = e2.getY();
        invalidate();
        return true;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public void setFollowing(boolean follow) {
        if (!follow && this.following) {
            Toast.makeText(getContext(), (int) R.string.t_following_off, 0).show();
        }
        this.following = follow;
    }

    public boolean isTrackingAngle() {
        return this.trackingAngle;
    }

    public void setTrackingAngle(boolean trackingAngle2) {
        this.trackingAngle = trackingAngle2;
    }

    public void onShowPress(MotionEvent event) {
        if (isSculpting()) {
            this.sculptStartX = event.getX();
            this.sculptStartY = event.getY();
            this.sculptEndX = event.getX();
            this.sculptEndY = event.getY();
            this.sculptInProgress = true;
            invalidate();
        }
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public boolean isShouldTiltMap() {
        return this.shouldTiltMap;
    }

    public void setShouldTiltMap(boolean shouldTiltMap2) {
        this.shouldTiltMap = shouldTiltMap2;
        if (!shouldTiltMap2) {
            setTiltMap(false);
        } else {
            setTiltMap(this.following);
        }
    }

    public void update(Observable observable, Object arg1) {
        this.nav = (Navigator) observable;
        if (arg1.equals(Navigator.LOCATION)) {
            this.lastGpsPosition = new Position(this.nav.getCurrentLocation().getLongitude(), this.nav.getCurrentLocation().getLatitude());
            if (this.following) {
                setCenter(new Position(this.nav.getCurrentLocation().getLongitude(), this.nav.getCurrentLocation().getLatitude()));
                if (!this.trackingAngle) {
                    setTrackingAngle(true);
                }
                if (this.trackingAngle) {
                    this.mapAngle = this.nav.getMapBearing();
                    this.mBAngleFromCompass = this.nav.isMapBearingFromCompass();
                    this.decl = this.nav.getDeclination();
                    invalidate();
                }
                if (this.shouldTiltMap) {
                    setTiltMap(true);
                }
            }
            this.gotoPos = this.nav.getGotoPos();
            if (this.nav.getCurrentLocation().hasAccuracy()) {
                this.lastAccuracy = this.nav.getCurrentLocation().getAccuracy();
            } else {
                this.lastAccuracy = 0.0f;
            }
        }
        if (arg1.equals(Navigator.ANGLE)) {
            this.mCompassAccuracy = this.nav.getAccuracy();
            if (this.trackingAngle) {
                this.mapAngle = this.nav.getMapBearing();
                this.mBAngleFromCompass = this.nav.isMapBearingFromCompass();
                this.decl = this.nav.getDeclination();
                invalidate();
            }
            this.gotoPos = this.nav.getGotoPos();
        }
        if (arg1.equals(Navigator.GPSSTATUS)) {
            this.recentGpsStatus = this.nav.getGpsStatus();
            if (this.recentGpsStatus.getTimeToFirstFix() <= 0) {
                invalidate();
            }
        }
    }

    public void gotoAddress(PlaceAddress address) {
        Position loc = new Position(address.getAddress().getLongitude(), address.getAddress().getLatitude());
        this.tiltMap = false;
        this.following = false;
        Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.t_center_place, address), 1);
        toast.setGravity(17, 0, 20);
        toast.show();
        setCenter(loc);
    }

    public void gotoCoordinate(Position pos, String label) {
        this.tiltMap = false;
        this.following = false;
        Toast toast = Toast.makeText(getContext(), getResources().getString(R.string.t_center_place, label), 1);
        toast.setGravity(17, 0, 20);
        toast.show();
        setCenter(pos);
    }

    public boolean isSculpting() {
        return this.sculpting;
    }

    public void setSculpting(boolean sculpting2) {
        this.sculpting = sculpting2;
        if (sculpting2) {
            setFollowing(false);
            setTiltMap(false);
            this.detector.setIsLongpressEnabled(false);
            return;
        }
        this.detector.setIsLongpressEnabled(true);
    }

    public void clearSculpting() {
        this.markedRectangles.clear();
        invalidate();
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress > this.maxlevel) {
            progress = this.maxlevel;
        }
        if (progress < this.minlevel) {
            progress = this.minlevel;
        }
        if (progress != seekBar.getProgress()) {
            seekBar.setProgress(progress);
        }
        setLevel(progress);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public int zoomIn(boolean multistep) {
        int newlevel;
        if (multistep) {
            newlevel = Math.min(20, this.level + 4);
        } else {
            newlevel = Math.min(this.level + 1, 20);
        }
        setLevel(newlevel);
        return this.level;
    }

    public int zoomOut(boolean multistep) {
        int newlevel;
        if (multistep) {
            newlevel = Math.max(this.minlevel, this.level - 4);
        } else {
            newlevel = Math.max(this.level - 1, this.minlevel);
        }
        setLevel(newlevel);
        return this.level;
    }

    public Object getDraggableObjectAtPoint(MultiTouchController.PointInfo pointInfo) {
        return this;
    }

    public void getPositionAndScale(Object obj, MultiTouchController.PositionAndScale objPosAndScaleOut) {
        objPosAndScaleOut.set(0.0f, 0.0f, 1.0f);
    }

    public void selectObject(Object obj, MultiTouchController.PointInfo pointInfo) {
    }

    public boolean setPositionAndScale(Object obj, MultiTouchController.PositionAndScale update, MultiTouchController.PointInfo pointInfo) {
        float newXOff = update.getXOff();
        float newYOff = update.getYOff();
        float newRelativeScale = update.getScale();
        Position newCenter = this.tileResolver.shift(this.center, (int) newXOff, (int) newYOff, this.level);
        int targetlevel = Math.max(this.level + Math.round((float) (Math.log((double) newRelativeScale) * ZOOM_LOG_BASE_INV)), this.minlevel);
        if (this.level == targetlevel) {
            return true;
        }
        Log.i("ScaleAndZoom", "New level =" + targetlevel);
        setCenterAndZoom(newCenter, targetlevel);
        return true;
    }

    public void onDestroy() {
        if (this.renderedBitmap != null) {
            this.renderedBitmap.recycle();
        }
        this.renderedBitmap = null;
        this.tvLevel = null;
    }

    public void onReScale(float scaleFactor, float fx, float fy) {
        int nlevel = this.level + ((int) Math.round(Math.log((double) scaleFactor) / Math.log(2.0d)));
        if (nlevel > 20) {
            nlevel = 20;
        }
        if (nlevel < this.minlevel) {
            nlevel = this.minlevel;
        }
        float cx = ((float) getWidth()) / 2.0f;
        float cy = ((float) getHeight()) / 2.0f;
        float mult = (float) Math.pow(2.0d, (double) (nlevel - this.level));
        setCenterAndZoom(this.tileResolver.shift(this.center, (int) ((mult - 1.0f) * (fx - cx)), (int) ((mult - 1.0f) * (fy - cy)), nlevel), nlevel);
    }

    public void onScale(float scaleFactor, float fx, float fy) {
        invalidate();
    }

    public void setShouldShowRuler(boolean boolean1) {
        this.mShowRuler = boolean1;
    }

    public boolean onDoubleTap(MotionEvent paramMotionEvent) {
        adjustCenter(paramMotionEvent.getX() - (((float) getWidth()) / 2.0f), paramMotionEvent.getY() - (((float) getHeight()) / 2.0f));
        setFollowing(false);
        setTiltMap(false);
        zoomIn(false);
        if (this.tvLevel == null) {
            return true;
        }
        this.tvLevel.setText(String.valueOf(this.level));
        return true;
    }

    public boolean onDoubleTapEvent(MotionEvent paramMotionEvent) {
        return false;
    }

    public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent) {
        return false;
    }
}
