package com.crittermap.backcountrynavigator.map;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapServerListAdapter extends BaseAdapter {
    HashMap<MapServer, Integer> chosenServers = new HashMap<>();
    ArrayList<Integer> intList = new ArrayList<>(20);
    private Context mContext;
    private LayoutInflater mInflater;
    List<MapServer> serverList;

    public MapServerListAdapter(List<MapServer> servers, Context context) {
        for (int i = 20; i >= 1; i--) {
            this.intList.add(Integer.valueOf(i));
        }
        this.serverList = servers;
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public HashMap<MapServer, Integer> getChosenServers() {
        return this.chosenServers;
    }

    public int getCount() {
        return this.serverList.size();
    }

    public Object getItem(int pos) {
        return this.serverList.get(pos);
    }

    public long getItemId(int pos) {
        return (long) pos;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int pos, View convertView, ViewGroup parent) {
        View sv;
        if (convertView == null) {
            sv = this.mInflater.inflate((int) R.layout.download_layer_list_item, parent, false);
        } else {
            sv = convertView;
        }
        final MapServer server = this.serverList.get(pos);
        CompoundButton toggle = (CompoundButton) sv.findViewById(R.id.download_layer_toggle);
        toggle.setChecked(false);
        ((TextView) sv.findViewById(R.id.download_layer_text)).setText(server.displayName);
        final EditText level = (EditText) sv.findViewById(R.id.download_level);
        final int maxZoom = server.getMaxZoom();
        final int minZoom = server.getMinZoom();
        final int currentZoom = Math.min(15, maxZoom);
        level.setText(String.valueOf(currentZoom));
        level.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                try {
                    int l = Integer.valueOf(editable.toString()).intValue();
                    if (MapServerListAdapter.this.chosenServers.containsKey(server)) {
                        if (l > maxZoom || l < minZoom) {
                            level.setText(MapServerListAdapter.this.chosenServers.get(server).toString());
                        } else {
                            MapServerListAdapter.this.chosenServers.put(server, Integer.valueOf(l));
                        }
                    } else if (l > maxZoom || l < minZoom) {
                        level.setText(String.valueOf(currentZoom));
                    }
                } catch (NumberFormatException e) {
                    level.setText(String.valueOf(currentZoom));
                }
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton cb, boolean checked) {
                if (checked) {
                    MapServerListAdapter.this.chosenServers.put(server, Integer.valueOf(level.getText().toString()));
                } else {
                    MapServerListAdapter.this.chosenServers.remove(server);
                }
            }
        });
        return sv;
    }
}
