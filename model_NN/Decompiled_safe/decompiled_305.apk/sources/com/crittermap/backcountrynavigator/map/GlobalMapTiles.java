package com.crittermap.backcountrynavigator.map;

import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileID;

public class GlobalMapTiles {
    static final double INITIALRESOLUTION = 156543.03392804062d;
    static final double ORIGINSHIFT = 2.0037508342789244E7d;
    static final int TILESIZE = 256;

    static double resolution(int zoom) {
        return INITIALRESOLUTION / Math.pow(2.0d, (double) zoom);
    }

    public static CoordinateBoundingBox tileBounds(TileID tid) {
        return new CoordinateBoundingBox(pixelToMeters(tid.x * TILESIZE, tid.level), pixelToMeters(tid.y * TILESIZE, tid.level), pixelToMeters((tid.x + 1) * TILESIZE, tid.level), pixelToMeters((tid.y + 1) * TILESIZE, tid.level));
    }

    static double pixelToMeters(int pix, int zoom) {
        return (((double) pix) * resolution(zoom)) - ORIGINSHIFT;
    }
}
