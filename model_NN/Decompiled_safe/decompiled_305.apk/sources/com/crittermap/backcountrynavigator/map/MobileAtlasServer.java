package com.crittermap.backcountrynavigator.map;

import android.net.Uri;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.tile.GMTileResolver;
import com.crittermap.backcountrynavigator.tile.TileID;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MobileAtlasServer extends TTemplateServer {
    public static final String MOBILEATLASSCHEME = "atlas";
    String atlas;
    Position center = null;
    String layer;
    String root;
    String suffix;

    public Position getCenter() {
        return this.center;
    }

    public MobileAtlasServer(String rt, String at, String lyr, String sfx) {
        this.root = rt;
        this.layer = lyr;
        this.suffix = sfx;
        this.atlas = at;
        this.minZoom = 1;
        this.maxZoom = 20;
        File froot = new File(String.valueOf(this.root) + File.separatorChar + this.atlas + File.separatorChar + this.layer);
        int i = 1;
        while (true) {
            if (i > 20) {
                break;
            } else if (new File(froot, new StringBuilder().append(i).toString()).exists()) {
                this.minZoom = i;
                break;
            } else {
                i++;
            }
        }
        int i2 = 20;
        while (true) {
            if (i2 < 1) {
                break;
            }
            File ldir = new File(froot, new StringBuilder().append(i2).toString());
            if (ldir.exists()) {
                this.maxZoom = i2;
                this.center = centerOf(ldir, i2);
                break;
            }
            i2--;
        }
        this.shortName = this.layer;
        this.copyright = this.layer;
        this.displayName = String.valueOf(this.atlas) + ":" + this.layer;
        this.tileResolver = new GMTileResolver();
    }

    /* access modifiers changed from: package-private */
    public Position centerOf(File ldir, int level) {
        File[] subdirs = ldir.listFiles(new NumberFileFilter(0, Integer.MAX_VALUE));
        if (subdirs.length <= 0) {
            return null;
        }
        int total = 0;
        int length = subdirs.length;
        for (int i = 0; i < length; i++) {
            total += Integer.valueOf(subdirs[i].getName()).intValue();
        }
        int ave = total / subdirs.length;
        File file = new File(ldir, new StringBuilder().append(ave).toString());
        if (!file.exists()) {
            return null;
        }
        File[] ydirs = file.listFiles();
        if (ydirs.length <= 0) {
            return null;
        }
        String[] tokens = ydirs[0].getName().split("\\.");
        if (tokens.length <= 0) {
            return null;
        }
        try {
            return new GMTileResolver().boundingBox(new TileID(level, ave, Integer.parseInt(tokens[0]))).getCenter();
        } catch (Exception e) {
            return null;
        }
    }

    public Uri getUri() {
        return Uri.fromParts(MOBILEATLASSCHEME, String.valueOf(this.root) + "/" + this.atlas + "/" + this.layer, null);
    }

    public static MobileAtlasServer createFromUri(Uri uri) {
        String part = uri.getSchemeSpecificPart();
        int bi = part.lastIndexOf(47);
        if (bi < 1) {
            return null;
        }
        int ai = part.lastIndexOf(47, bi - 1);
        if (ai < 1) {
            return null;
        }
        return create(part.substring(0, ai), part.substring(ai + 1, bi), part.substring(bi + 1));
    }

    /* JADX INFO: Multiple debug info for r0v1 java.io.File[]: [D('froot' java.io.File), D('levels' java.io.File[])] */
    /* JADX INFO: Multiple debug info for r0v2 java.io.File: [D('levels' java.io.File[]), D('levelDir' java.io.File)] */
    /* JADX INFO: Multiple debug info for r0v3 java.io.File[]: [D('xdirs' java.io.File[]), D('levelDir' java.io.File)] */
    /* JADX INFO: Multiple debug info for r0v5 java.io.File[]: [D('xdirs' java.io.File[]), D('yfiles' java.io.File[])] */
    /* JADX INFO: Multiple debug info for r0v7 java.lang.String: [D('candidate' java.lang.String), D('yfiles' java.io.File[])] */
    /* JADX INFO: Multiple debug info for r1v17 java.lang.String: [D('firstdot' int), D('suffix' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v8 com.crittermap.backcountrynavigator.map.MobileAtlasServer: [D('candidate' java.lang.String), D('mServer' com.crittermap.backcountrynavigator.map.MobileAtlasServer)] */
    public static MobileAtlasServer create(String root2, String atlas2, String layer2) {
        File[] xdirs;
        File[] yfiles;
        int firstdot;
        File[] levels = new File(String.valueOf(root2) + File.separatorChar + atlas2 + File.separatorChar + layer2).listFiles(new NumberFileFilter(0, 20));
        if (levels == null || levels.length < 1 || (xdirs = levels[0].listFiles(new NumberFileFilter(0, -1))) == null || xdirs.length < 1 || (yfiles = xdirs[0].listFiles()) == null || yfiles.length < 1) {
            return null;
        }
        String candidate = yfiles[0].getName();
        if (candidate.length() >= 1 && (firstdot = candidate.indexOf(".")) != -1) {
            return new MobileAtlasServer(root2, atlas2, layer2, candidate.substring(firstdot));
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r11v1 java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>: [D('root' java.lang.String), D('list' java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>)] */
    /* JADX INFO: Multiple debug info for r3v6 com.crittermap.backcountrynavigator.map.MobileAtlasServer: [D('s' java.io.File), D('mServer' com.crittermap.backcountrynavigator.map.MobileAtlasServer)] */
    /* JADX INFO: Multiple debug info for r11v2 java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>: [D('root' java.lang.String), D('list' java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>)] */
    /* JADX INFO: Multiple debug info for r11v3 java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>: [D('root' java.lang.String), D('list' java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>)] */
    /* JADX INFO: Multiple debug info for r11v4 java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>: [D('root' java.lang.String), D('list' java.util.HashMap<java.lang.String, java.util.List<com.crittermap.backcountrynavigator.map.MobileAtlasServer>>)] */
    public static HashMap<String, List<MobileAtlasServer>> find(String root2) {
        File[] subdirs;
        HashMap<String, List<MobileAtlasServer>> list = new HashMap<>();
        File froot = new File(root2);
        if (!froot.exists()) {
            return list;
        }
        File[] files = froot.listFiles();
        if (files == null) {
            return list;
        }
        if (files.length < 1) {
            return list;
        }
        for (File f : files) {
            if (f != null && f.isDirectory() && (subdirs = f.listFiles()) != null && subdirs.length >= 1) {
                List<MobileAtlasServer> subList = new ArrayList<>();
                for (File s : subdirs) {
                    MobileAtlasServer mServer = create(root2, f.getName(), s.getName());
                    if (mServer != null) {
                        subList.add(mServer);
                    }
                }
                list.put(f.getName(), subList);
            }
        }
        return list;
    }

    public String getRoot() {
        return this.root;
    }

    public void setRoot(String root2) {
        this.root = root2;
    }

    public String getLayer() {
        return this.layer;
    }

    public void setLayer(String layer2) {
        this.layer = layer2;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setSuffix(String suffix2) {
        this.suffix = suffix2;
    }

    static class NumberFileFilter implements FilenameFilter {
        private int mMax;
        private int mMin;

        NumberFileFilter(int min, int max) {
            this.mMin = min;
            this.mMax = max;
        }

        public boolean accept(File arg0, String fname) {
            try {
                int number = Integer.parseInt(fname);
                if (this.mMax != -1 && number > this.mMax) {
                    return false;
                }
                if (number < this.mMin) {
                    return false;
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        }
    }

    public String getAtlasPath() {
        return String.valueOf(this.root) + File.separatorChar + this.atlas;
    }
}
