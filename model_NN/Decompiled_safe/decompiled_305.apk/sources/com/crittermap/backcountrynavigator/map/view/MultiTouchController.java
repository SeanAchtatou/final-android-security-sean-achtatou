package com.crittermap.backcountrynavigator.map.view;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import java.lang.reflect.Method;

public class MultiTouchController<T> {
    private static final long EVENT_SETTLE_TIME_INTERVAL = 100;
    private static final float MAX_MULTITOUCH_DIM_JUMP_SIZE = 40.0f;
    private static final float MAX_MULTITOUCH_POS_JUMP_SIZE = 30.0f;
    private static final float MIN_MULTITOUCH_SEPARATION = 30.0f;
    private static final int MODE_DRAG = 1;
    private static final int MODE_NOTHING = 0;
    private static final int MODE_STRETCH = 2;
    private static Method m_findPointerIndex;
    private static Method m_getHistoricalPressure;
    private static Method m_getHistoricalX;
    private static Method m_getHistoricalY;
    private static Method m_getPointerCount;
    private static Method m_getPressure;
    private static Method m_getX;
    private static Method m_getY;
    public static final boolean multiTouchSupported;
    private PointInfo currPt;
    private int dragMode;
    private long dragSettleTime;
    private long dragStartTime;
    private T draggedObject;
    private boolean handleSingleTouchEvents;
    private float objDraggedPointX;
    private float objDraggedPointY;
    private PositionAndScale objPosAndScale;
    private float objStartScale;
    MultiTouchObjectCanvas<T> objectCanvas;
    private PointInfo prevPt;

    public interface MultiTouchObjectCanvas<T> {
        T getDraggableObjectAtPoint(PointInfo pointInfo);

        void getPositionAndScale(T t, PositionAndScale positionAndScale);

        void selectObject(T t, PointInfo pointInfo);

        boolean setPositionAndScale(T t, PositionAndScale positionAndScale, PointInfo pointInfo);
    }

    public MultiTouchController(MultiTouchObjectCanvas<T> objectCanvas2, Resources res) {
        this(objectCanvas2, res, true);
    }

    public MultiTouchController(MultiTouchObjectCanvas<T> objectCanvas2, Resources res, boolean handleSingleTouchEvents2) {
        this.draggedObject = null;
        this.objPosAndScale = new PositionAndScale();
        this.dragMode = 0;
        this.currPt = new PointInfo(res);
        this.prevPt = new PointInfo(res);
        this.handleSingleTouchEvents = handleSingleTouchEvents2;
        this.objectCanvas = objectCanvas2;
    }

    /* access modifiers changed from: protected */
    public void setHandleSingleTouchEvents(boolean handleSingleTouchEvents2) {
        this.handleSingleTouchEvents = handleSingleTouchEvents2;
    }

    /* access modifiers changed from: protected */
    public boolean getHandleSingleTouchEvents() {
        return this.handleSingleTouchEvents;
    }

    static {
        boolean succeed = false;
        try {
            m_getPointerCount = MotionEvent.class.getMethod("getPointerCount", new Class[0]);
            m_findPointerIndex = MotionEvent.class.getMethod("findPointerIndex", Integer.TYPE);
            m_getPressure = MotionEvent.class.getMethod("getPressure", Integer.TYPE);
            m_getHistoricalX = MotionEvent.class.getMethod("getHistoricalX", Integer.TYPE, Integer.TYPE);
            m_getHistoricalY = MotionEvent.class.getMethod("getHistoricalY", Integer.TYPE, Integer.TYPE);
            m_getHistoricalPressure = MotionEvent.class.getMethod("getHistoricalPressure", Integer.TYPE, Integer.TYPE);
            m_getX = MotionEvent.class.getMethod("getX", Integer.TYPE);
            m_getY = MotionEvent.class.getMethod("getY", Integer.TYPE);
            succeed = true;
        } catch (Exception e) {
        }
        multiTouchSupported = succeed;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!multiTouchSupported) {
            return false;
        }
        try {
            int pointerCount = ((Integer) m_getPointerCount.invoke(event, new Object[0])).intValue();
            if (this.dragMode == 0 && !this.handleSingleTouchEvents && pointerCount == 1) {
                return false;
            }
            int histLen = event.getHistorySize() / pointerCount;
            int secondPointerIndex = ((Integer) m_findPointerIndex.invoke(event, 1)).intValue();
            for (int i = 0; i < histLen; i++) {
                float pressureI = ((Float) m_getPressure.invoke(event, Integer.valueOf(i))).floatValue();
                if (secondPointerIndex >= 0) {
                    decodeTouchEvent(event.getHistoricalX(i), event.getHistoricalY(i), pressureI, pointerCount, ((Float) m_getHistoricalX.invoke(event, Integer.valueOf(secondPointerIndex), Integer.valueOf(i))).floatValue(), ((Float) m_getHistoricalY.invoke(event, Integer.valueOf(secondPointerIndex), Integer.valueOf(i))).floatValue(), ((Float) m_getHistoricalPressure.invoke(event, Integer.valueOf(secondPointerIndex), Integer.valueOf(i))).floatValue(), 2, true, event.getHistoricalEventTime(i));
                } else {
                    decodeTouchEvent(event.getHistoricalX(i), event.getHistoricalY(i), pressureI, pointerCount, 0.0f, 0.0f, 0.0f, 2, true, event.getHistoricalEventTime(i));
                }
            }
            if (secondPointerIndex >= 0) {
                decodeTouchEvent(event.getX(), event.getY(), event.getPressure(), pointerCount, ((Float) m_getX.invoke(event, Integer.valueOf(secondPointerIndex))).floatValue(), ((Float) m_getY.invoke(event, Integer.valueOf(secondPointerIndex))).floatValue(), ((Float) m_getPressure.invoke(event, Integer.valueOf(secondPointerIndex))).floatValue(), event.getAction(), (event.getAction() == 1 || event.getAction() == 3) ? false : true, event.getEventTime());
            } else {
                decodeTouchEvent(event.getX(), event.getY(), event.getPressure(), pointerCount, 0.0f, 0.0f, 0.0f, event.getAction(), (event.getAction() == 1 || event.getAction() == 3) ? false : true, event.getEventTime());
            }
            return true;
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }

    private void decodeTouchEvent(float x, float y, float pressure, int pointerCount, float x2, float y2, float pressure2, int action, boolean down, long eventTime) {
        this.prevPt.set(this.currPt);
        this.currPt.set(x, y, pressure, pointerCount, x2, y2, pressure2, action, down, eventTime);
        multiTouchController();
    }

    private void resetDrag() {
        if (this.draggedObject != null) {
            this.objectCanvas.getPositionAndScale(this.draggedObject, this.objPosAndScale);
            float scaleInv = this.objPosAndScale.scale == 0.0f ? 1.0f : 1.0f / this.objPosAndScale.scale;
            this.objDraggedPointX = (this.currPt.getX() - this.objPosAndScale.xOff) * scaleInv;
            this.objDraggedPointY = (this.currPt.getY() - this.objPosAndScale.yOff) * scaleInv;
            float diam = this.currPt.getMultiTouchDiameter();
            this.objStartScale = this.objPosAndScale.scale / (diam == 0.0f ? 1.0f : diam);
        }
    }

    private void performDrag() {
        float diam;
        if (this.draggedObject != null) {
            float scale = this.objPosAndScale.scale == 0.0f ? 1.0f : this.objPosAndScale.scale;
            float newObjPosX = this.currPt.getX() - (this.objDraggedPointX * scale);
            float newObjPosY = this.currPt.getY() - (this.objDraggedPointY * scale);
            if (!this.currPt.isMultiTouch) {
                diam = 1.0f;
            } else {
                diam = this.currPt.getMultiTouchDiameter();
                if (diam < 30.0f) {
                    diam = 30.0f;
                }
            }
            this.objPosAndScale.set(newObjPosX, newObjPosY, diam * this.objStartScale);
            if (!this.objectCanvas.setPositionAndScale(this.draggedObject, this.objPosAndScale, this.currPt)) {
            }
        }
    }

    private void multiTouchController() {
        switch (this.dragMode) {
            case 0:
                if (this.currPt.isDown()) {
                    this.draggedObject = this.objectCanvas.getDraggableObjectAtPoint(this.currPt);
                    if (this.draggedObject != null) {
                        this.dragMode = 1;
                        this.objectCanvas.selectObject(this.draggedObject, this.currPt);
                        resetDrag();
                        long eventTime = this.currPt.getEventTime();
                        this.dragSettleTime = eventTime;
                        this.dragStartTime = eventTime;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                if (!this.currPt.isDown()) {
                    this.dragMode = 0;
                    MultiTouchObjectCanvas<T> multiTouchObjectCanvas = this.objectCanvas;
                    this.draggedObject = null;
                    multiTouchObjectCanvas.selectObject(null, this.currPt);
                    return;
                } else if (this.currPt.isMultiTouch()) {
                    this.dragMode = 2;
                    resetDrag();
                    this.dragStartTime = this.currPt.getEventTime();
                    this.dragSettleTime = this.dragStartTime + EVENT_SETTLE_TIME_INTERVAL;
                    return;
                } else if (this.currPt.getEventTime() < this.dragSettleTime) {
                    resetDrag();
                    return;
                } else {
                    performDrag();
                    return;
                }
            case 2:
                if (!this.currPt.isMultiTouch() || !this.currPt.isDown()) {
                    if (!this.currPt.isDown()) {
                        this.dragMode = 0;
                        MultiTouchObjectCanvas<T> multiTouchObjectCanvas2 = this.objectCanvas;
                        this.draggedObject = null;
                        multiTouchObjectCanvas2.selectObject(null, this.currPt);
                        return;
                    }
                    this.dragMode = 1;
                    resetDrag();
                    this.dragStartTime = this.currPt.getEventTime();
                    this.dragSettleTime = this.dragStartTime + EVENT_SETTLE_TIME_INTERVAL;
                    return;
                } else if (Math.abs(this.currPt.getX() - this.prevPt.getX()) > 30.0f || Math.abs(this.currPt.getY() - this.prevPt.getY()) > 30.0f || Math.abs(this.currPt.getMultiTouchWidth() - this.prevPt.getMultiTouchWidth()) * 0.5f > MAX_MULTITOUCH_DIM_JUMP_SIZE || Math.abs(this.currPt.getMultiTouchHeight() - this.prevPt.getMultiTouchHeight()) * 0.5f > MAX_MULTITOUCH_DIM_JUMP_SIZE) {
                    resetDrag();
                    this.dragStartTime = this.currPt.getEventTime();
                    this.dragSettleTime = this.dragStartTime + EVENT_SETTLE_TIME_INTERVAL;
                    return;
                } else if (this.currPt.eventTime < this.dragSettleTime) {
                    resetDrag();
                    return;
                } else {
                    performDrag();
                    return;
                }
            default:
                return;
        }
    }

    public static class PointInfo {
        private int action;
        private float angle;
        private boolean angleIsCalculated;
        private float diameter;
        private boolean diameterIsCalculated;
        private float diameterSq;
        private boolean diameterSqIsCalculated;
        private int displayHeight;
        private int displayWidth;
        private boolean down;
        private float dx;
        private float dy;
        /* access modifiers changed from: private */
        public long eventTime;
        /* access modifiers changed from: private */
        public boolean isMultiTouch;
        private float pressure;
        private float pressure2;
        private float size;
        private float x;
        private float y;

        public PointInfo(Resources res) {
            DisplayMetrics metrics = res.getDisplayMetrics();
            this.displayWidth = metrics.widthPixels;
            this.displayHeight = metrics.heightPixels;
        }

        public PointInfo(PointInfo other) {
            set(other);
        }

        public void set(PointInfo other) {
            this.displayWidth = other.displayWidth;
            this.displayHeight = other.displayHeight;
            this.x = other.x;
            this.y = other.y;
            this.dx = other.dx;
            this.dy = other.dy;
            this.size = other.size;
            this.diameter = other.diameter;
            this.diameterSq = other.diameterSq;
            this.angle = other.angle;
            this.pressure = other.pressure;
            this.pressure2 = other.pressure2;
            this.down = other.down;
            this.action = other.action;
            this.isMultiTouch = other.isMultiTouch;
            this.diameterIsCalculated = other.diameterIsCalculated;
            this.diameterSqIsCalculated = other.diameterSqIsCalculated;
            this.angleIsCalculated = other.angleIsCalculated;
            this.eventTime = other.eventTime;
        }

        /* access modifiers changed from: private */
        public void set(float x2, float y2, float pressure3, int pointerCount, float x22, float y22, float pressure22, int action2, boolean down2, long eventTime2) {
            this.eventTime = eventTime2;
            this.action = action2;
            this.x = x2;
            this.y = y2;
            this.pressure = pressure3;
            this.pressure2 = pressure22;
            this.down = down2;
            this.isMultiTouch = pointerCount == 2;
            if (this.isMultiTouch) {
                this.dx = Math.abs(x22 - x2);
                this.dy = Math.abs(y22 - y2);
                this.x = (x22 + x2) * 0.5f;
                this.y = (y22 + y2) * 0.5f;
            } else {
                this.dy = 0.0f;
                this.dx = 0.0f;
            }
            this.angleIsCalculated = false;
            this.diameterIsCalculated = false;
            this.diameterSqIsCalculated = false;
        }

        private int julery_isqrt(int val) {
            int g = 0;
            int b = 32768;
            int bshft = 15;
            while (true) {
                int bshft2 = bshft;
                bshft = bshft2 - 1;
                int temp = ((g << 1) + b) << bshft2;
                if (val >= temp) {
                    g += b;
                    val -= temp;
                }
                b >>= 1;
                if (b <= 0) {
                    return g;
                }
            }
        }

        public float getMultiTouchDiameterSq() {
            if (!this.diameterSqIsCalculated) {
                this.diameterSq = this.isMultiTouch ? (this.dx * this.dx) + (this.dy * this.dy) : 0.0f;
                this.diameterSqIsCalculated = true;
            }
            return this.diameterSq;
        }

        public float getMultiTouchDiameter() {
            if (!this.diameterIsCalculated) {
                float diamSq = getMultiTouchDiameterSq();
                this.diameter = diamSq == 0.0f ? 0.0f : ((float) julery_isqrt((int) (256.0f * diamSq))) / 16.0f;
                if (this.diameter < this.dx) {
                    this.diameter = this.dx;
                }
                if (this.diameter < this.dy) {
                    this.diameter = this.dy;
                }
                this.diameterIsCalculated = true;
            }
            return this.diameter;
        }

        public float getMultiTouchAngle() {
            if (!this.angleIsCalculated) {
                this.angle = (float) Math.atan2((double) this.dy, (double) this.dx);
                this.angleIsCalculated = true;
            }
            return this.angle;
        }

        public float getX() {
            return this.x;
        }

        public float getY() {
            return this.y;
        }

        public float getMultiTouchWidth() {
            return this.dx;
        }

        public float getMultiTouchHeight() {
            return this.dy;
        }

        public float getPressure() {
            return this.pressure;
        }

        public float getPressure2() {
            return this.pressure2;
        }

        public boolean isDown() {
            return this.down;
        }

        public int getAction() {
            return this.action;
        }

        public boolean isMultiTouch() {
            return this.isMultiTouch;
        }

        public long getEventTime() {
            return this.eventTime;
        }
    }

    public static class PositionAndScale {
        /* access modifiers changed from: private */
        public float scale;
        /* access modifiers changed from: private */
        public float xOff;
        /* access modifiers changed from: private */
        public float yOff;

        public void set(float xOff2, float yOff2, float scale2) {
            this.xOff = xOff2;
            this.yOff = yOff2;
            this.scale = scale2;
        }

        public float getXOff() {
            return this.xOff;
        }

        public float getYOff() {
            return this.yOff;
        }

        public float getScale() {
            return this.scale;
        }
    }
}
