package com.crittermap.backcountrynavigator.map.view;

import android.database.Cursor;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tile.TileResolver;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class TrackIntDrawer {
    private static final double M = 4194304.0d;
    BCNMapDatabase bdb;
    String sql;
    TileResolver tileResolver;

    public TrackIntDrawer(BCNMapDatabase b, TileResolver t) {
        this.bdb = b;
        this.tileResolver = t;
        StringBuilder builder = new StringBuilder();
        builder.setLength(0);
        builder.append("SELECT BUFFER FROM PSG WHERE ");
        builder.append("MinLevel <  ?");
        builder.append(" AND MaxLevel >= ?");
        builder.append(" AND ");
        builder.append("MaxLon > ?");
        builder.append(" AND MinLon < ?");
        builder.append(" AND MaxLat > ?");
        builder.append(" AND MinLat < ?");
        this.sql = builder.toString();
    }

    public List<TrackSegment> plotAllTracks(CoordinateBoundingBox box, int W, int H, int level) {
        ArrayList arrayList = new ArrayList();
        String[] selectionArgs = {String.valueOf(level), String.valueOf(level), String.valueOf((int) (box.minlon * M)), String.valueOf((int) (box.maxlon * M)), String.valueOf((int) (box.minlat * M)), String.valueOf((int) (box.maxlat * M))};
        Cursor cursor = this.bdb.getDb().rawQuery(this.sql, selectionArgs);
        int bi = cursor.getColumnIndex("Buffer");
        while (cursor.moveToNext()) {
            float[] pixels = this.tileResolver.transformToScreen(ByteBuffer.wrap(cursor.getBlob(bi)).asFloatBuffer(), level, W, H, box);
            TrackSegment ts = new TrackSegment();
            ts.color = -16776961;
            ts.path = pixels;
            arrayList.add(ts);
        }
        cursor.close();
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public float[] boundingBoxQuery(BCNMapDatabase bdb2, long PathID, CoordinateBoundingBox box) {
        Cursor cursor = bdb2.getDb().rawQuery("SELECT T1.lon as X1,T1.lat as Y1,T2.lat as Y2,T2.lon as X2 FROM TrackPoints as T1, TrackPoints as T2 WHERE T1.PathID = " + PathID + " AND " + "T1.TrackPointID = T2.PredecessorID AND " + "MAX(T1.Lat,T2.Lat) > " + box.minlat + " AND MIN(T1.Lat,T2.Lat) < " + box.maxlat + " AND " + "MAX(T1.lon,T2.lon) > " + box.minlon + " AND MIN(T1.lon,T2.lon) < " + box.maxlon, null);
        int xi = cursor.getColumnIndex("X1");
        int yi = cursor.getColumnIndex("Y1");
        int x2i = cursor.getColumnIndex("X2");
        int y2i = cursor.getColumnIndex("Y2");
        float[] answer = new float[(cursor.getCount() * 4)];
        int i = 0;
        while (cursor.moveToNext()) {
            int i2 = i + 1;
            answer[i] = (float) cursor.getDouble(xi);
            int i3 = i2 + 1;
            answer[i2] = (float) cursor.getDouble(yi);
            int i4 = i3 + 1;
            answer[i3] = (float) cursor.getDouble(x2i);
            i = i4 + 1;
            answer[i4] = (float) cursor.getDouble(y2i);
        }
        cursor.close();
        return answer;
    }
}
