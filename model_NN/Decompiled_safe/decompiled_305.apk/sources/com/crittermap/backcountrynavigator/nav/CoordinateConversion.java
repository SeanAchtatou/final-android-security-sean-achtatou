package com.crittermap.backcountrynavigator.nav;

import java.util.Hashtable;
import java.util.Map;

public class CoordinateConversion {
    public double[] utm2LatLon(String UTM) {
        return new UTM2LatLon(this, null, null).convertUTMToLatLong(UTM);
    }

    public String latLon2UTM(double latitude, double longitude) {
        return new LatLon2UTM(this, null, null).convertLatLonToUTM(latitude, longitude);
    }

    /* access modifiers changed from: private */
    public void validate(double latitude, double longitude) {
        if (latitude < -90.0d || latitude > 90.0d || longitude < -180.0d || longitude >= 180.0d) {
            throw new IllegalArgumentException("Legal ranges: latitude [-90,90], longitude [-180,180).");
        }
    }

    public String latLon2MGRUTM(double latitude, double longitude) {
        return new LatLon2MGRUTM(this, null).convertLatLonToMGRUTM(latitude, longitude);
    }

    public double[] mgrutm2LatLon(String MGRUTM) {
        return new MGRUTM2LatLon(this, null).convertMGRUTMToLatLong(MGRUTM);
    }

    public double degreeToRadian(double degree) {
        return (3.141592653589793d * degree) / 180.0d;
    }

    public double radianToDegree(double radian) {
        return (180.0d * radian) / 3.141592653589793d;
    }

    /* access modifiers changed from: private */
    public double POW(double a, double b) {
        return Math.pow(a, b);
    }

    /* access modifiers changed from: private */
    public double SIN(double value) {
        return Math.sin(value);
    }

    /* access modifiers changed from: private */
    public double COS(double value) {
        return Math.cos(value);
    }

    /* access modifiers changed from: private */
    public double TAN(double value) {
        return Math.tan(value);
    }

    private class LatLon2UTM {
        double A0;
        double A6;
        double B0;
        double C0;
        double D0;
        double E0;
        double K1;
        double K2;
        double K3;
        double K4;
        double K5;
        double S;
        double e;
        double e1sq;
        double equatorialRadius;
        double flattening;
        double inverseFlattening;
        double k0;
        double n;
        double nu;
        double p;
        double polarRadius;
        double rho;
        double rm;
        double sin1;

        private LatLon2UTM() {
            this.equatorialRadius = 6378137.0d;
            this.polarRadius = 6356752.314d;
            this.flattening = 0.00335281066474748d;
            this.inverseFlattening = 298.257223563d;
            this.rm = CoordinateConversion.this.POW(this.equatorialRadius * this.polarRadius, 0.5d);
            this.k0 = 0.9996d;
            this.e = Math.sqrt(1.0d - CoordinateConversion.this.POW(this.polarRadius / this.equatorialRadius, 2.0d));
            this.e1sq = (this.e * this.e) / (1.0d - (this.e * this.e));
            this.n = (this.equatorialRadius - this.polarRadius) / (this.equatorialRadius + this.polarRadius);
            this.rho = 6368573.744d;
            this.nu = 6389236.914d;
            this.S = 5103266.421d;
            this.A0 = 6367449.146d;
            this.B0 = 16038.42955d;
            this.C0 = 16.83261333d;
            this.D0 = 0.021984404d;
            this.E0 = 3.12705E-4d;
            this.p = -0.483084d;
            this.sin1 = 4.84814E-6d;
            this.K1 = 5101225.115d;
            this.K2 = 3750.291596d;
            this.K3 = 1.397608151d;
            this.K4 = 214839.3105d;
            this.K5 = -2.995382942d;
            this.A6 = -1.00541E-7d;
        }

        /* synthetic */ LatLon2UTM(CoordinateConversion coordinateConversion, LatLon2UTM latLon2UTM) {
            this();
        }

        /* synthetic */ LatLon2UTM(CoordinateConversion coordinateConversion, LatLon2UTM latLon2UTM, LatLon2UTM latLon2UTM2) {
            this();
        }

        public String convertLatLonToUTM(double latitude, double longitude) {
            CoordinateConversion.this.validate(latitude, longitude);
            setVariables(latitude, longitude);
            String longZone = getLongZone(longitude);
            return String.valueOf(longZone) + " " + new LatZones().getLatZone(latitude) + " " + ((int) getEasting()) + " " + ((int) getNorthing(latitude));
        }

        /* access modifiers changed from: protected */
        public void setVariables(double latitude, double longitude) {
            double var1;
            double latitude2 = CoordinateConversion.this.degreeToRadian(latitude);
            this.rho = (this.equatorialRadius * (1.0d - (this.e * this.e))) / CoordinateConversion.this.POW(1.0d - CoordinateConversion.this.POW(this.e * CoordinateConversion.this.SIN(latitude2), 2.0d), 1.5d);
            this.nu = this.equatorialRadius / CoordinateConversion.this.POW(1.0d - CoordinateConversion.this.POW(this.e * CoordinateConversion.this.SIN(latitude2), 2.0d), 0.5d);
            if (longitude < 0.0d) {
                var1 = (double) (((int) ((180.0d + longitude) / 6.0d)) + 1);
            } else {
                var1 = (double) (((int) (longitude / 6.0d)) + 31);
            }
            this.p = (3600.0d * (longitude - ((6.0d * var1) - 183.0d))) / 10000.0d;
            this.S = ((((this.A0 * latitude2) - (this.B0 * CoordinateConversion.this.SIN(2.0d * latitude2))) + (this.C0 * CoordinateConversion.this.SIN(4.0d * latitude2))) - (this.D0 * CoordinateConversion.this.SIN(6.0d * latitude2))) + (this.E0 * CoordinateConversion.this.SIN(8.0d * latitude2));
            this.K1 = this.S * this.k0;
            this.K2 = (((((this.nu * CoordinateConversion.this.SIN(latitude2)) * CoordinateConversion.this.COS(latitude2)) * CoordinateConversion.this.POW(this.sin1, 2.0d)) * this.k0) * 1.0E8d) / 2.0d;
            this.K3 = ((((CoordinateConversion.this.POW(this.sin1, 4.0d) * this.nu) * CoordinateConversion.this.SIN(latitude2)) * Math.pow(CoordinateConversion.this.COS(latitude2), 3.0d)) / 24.0d) * ((5.0d - CoordinateConversion.this.POW(CoordinateConversion.this.TAN(latitude2), 2.0d)) + (9.0d * this.e1sq * CoordinateConversion.this.POW(CoordinateConversion.this.COS(latitude2), 2.0d)) + (4.0d * CoordinateConversion.this.POW(this.e1sq, 2.0d) * CoordinateConversion.this.POW(CoordinateConversion.this.COS(latitude2), 4.0d))) * this.k0 * 1.0E16d;
            this.K4 = this.nu * CoordinateConversion.this.COS(latitude2) * this.sin1 * this.k0 * 10000.0d;
            this.K5 = CoordinateConversion.this.POW(this.sin1 * CoordinateConversion.this.COS(latitude2), 3.0d) * (this.nu / 6.0d) * ((1.0d - CoordinateConversion.this.POW(CoordinateConversion.this.TAN(latitude2), 2.0d)) + (this.e1sq * CoordinateConversion.this.POW(CoordinateConversion.this.COS(latitude2), 2.0d))) * this.k0 * 1.0E12d;
            this.A6 = ((((CoordinateConversion.this.POW(this.p * this.sin1, 6.0d) * this.nu) * CoordinateConversion.this.SIN(latitude2)) * CoordinateConversion.this.POW(CoordinateConversion.this.COS(latitude2), 5.0d)) / 720.0d) * ((((61.0d - (58.0d * CoordinateConversion.this.POW(CoordinateConversion.this.TAN(latitude2), 2.0d))) + CoordinateConversion.this.POW(CoordinateConversion.this.TAN(latitude2), 4.0d)) + ((270.0d * this.e1sq) * CoordinateConversion.this.POW(CoordinateConversion.this.COS(latitude2), 2.0d))) - ((330.0d * this.e1sq) * CoordinateConversion.this.POW(CoordinateConversion.this.SIN(latitude2), 2.0d))) * this.k0 * 1.0E24d;
        }

        /* access modifiers changed from: protected */
        public String getLongZone(double longitude) {
            double longZone;
            if (longitude < 0.0d) {
                longZone = ((180.0d + longitude) / 6.0d) + 1.0d;
            } else {
                longZone = (longitude / 6.0d) + 31.0d;
            }
            String val = String.valueOf((int) longZone);
            if (val.length() == 1) {
                return "0" + val;
            }
            return val;
        }

        /* access modifiers changed from: protected */
        public double getNorthing(double latitude) {
            double northing = this.K1 + (this.K2 * this.p * this.p) + (this.K3 * CoordinateConversion.this.POW(this.p, 4.0d));
            if (latitude < 0.0d) {
                return northing + 1.0E7d;
            }
            return northing;
        }

        /* access modifiers changed from: protected */
        public double getEasting() {
            return 500000.0d + (this.K4 * this.p) + (this.K5 * CoordinateConversion.this.POW(this.p, 3.0d));
        }
    }

    private class LatLon2MGRUTM extends LatLon2UTM {
        private LatLon2MGRUTM() {
            super(CoordinateConversion.this, null);
        }

        /* synthetic */ LatLon2MGRUTM(CoordinateConversion coordinateConversion, LatLon2MGRUTM latLon2MGRUTM) {
            this();
        }

        public String convertLatLonToMGRUTM(double latitude, double longitude) {
            CoordinateConversion.this.validate(latitude, longitude);
            setVariables(latitude, longitude);
            String longZone = getLongZone(longitude);
            String latZone = new LatZones().getLatZone(latitude);
            double _easting = getEasting();
            double _northing = getNorthing(latitude);
            Digraphs digraphs = new Digraphs();
            String digraph1 = digraphs.getDigraph1(Integer.parseInt(longZone), _easting);
            String digraph2 = digraphs.getDigraph2(Integer.parseInt(longZone), _northing);
            String easting = String.valueOf((int) _easting);
            if (easting.length() < 5) {
                easting = "00000" + easting;
            }
            String easting2 = easting.substring(easting.length() - 5);
            String northing = String.valueOf((int) _northing);
            if (northing.length() < 5) {
                northing = "0000" + northing;
            }
            return String.valueOf(longZone) + latZone + digraph1 + digraph2 + easting2 + northing.substring(northing.length() - 5);
        }
    }

    private class MGRUTM2LatLon extends UTM2LatLon {
        private MGRUTM2LatLon() {
            super(CoordinateConversion.this, null);
        }

        /* synthetic */ MGRUTM2LatLon(CoordinateConversion coordinateConversion, MGRUTM2LatLon mGRUTM2LatLon) {
            this();
        }

        public double[] convertMGRUTMToLatLong(String mgrutm) {
            double[] latlon = {0.0d, 0.0d};
            int zone = Integer.parseInt(mgrutm.substring(0, 2));
            String latZone = mgrutm.substring(2, 3);
            String digraph1 = mgrutm.substring(3, 4);
            String digraph2 = mgrutm.substring(4, 5);
            this.easting = Double.parseDouble(mgrutm.substring(5, 10));
            this.northing = Double.parseDouble(mgrutm.substring(10, 15));
            double latZoneDegree = (double) new LatZones().getLatZoneDegree(latZone);
            double a2 = 2000000.0d * Math.floor(((4.0E7d * latZoneDegree) / 360.0d) / 2000000.0d);
            Digraphs digraphs = new Digraphs();
            double digraph2Index = (double) digraphs.getDigraph2Index(digraph2);
            double startindexEquator = 1.0d;
            if ((zone % 2) + 1 == 1) {
                startindexEquator = 6.0d;
            }
            double a3 = a2 + ((digraph2Index - startindexEquator) * 100000.0d);
            if (a3 <= 0.0d) {
                a3 += 1.0E7d;
            }
            this.northing = this.northing + a3;
            this.zoneCM = (double) ((zone * 6) - 183);
            this.easting = this.easting + (100000.0d * (((double) digraphs.getDigraph1Index(digraph1)) - new double[]{16.0d, 0.0d, 8.0d}[((zone % 3) + 1) - 1]));
            setVariables();
            double latitude = (180.0d * (this.phi1 - (this.fact1 * ((this.fact2 + this.fact3) + this.fact4)))) / 3.141592653589793d;
            if (latZoneDegree < 0.0d) {
                latitude = 90.0d - latitude;
            }
            double longitude = this.zoneCM - ((this._a2 * 180.0d) / 3.141592653589793d);
            if (getHemisphere(latZone).equals("S")) {
                latitude = -latitude;
            }
            latlon[0] = latitude;
            latlon[1] = longitude;
            return latlon;
        }
    }

    private class UTM2LatLon {
        double Q0;
        double _a1;
        double _a2;
        double _a3;
        double a;
        double arc;
        double b;
        double ca;
        double cb;
        double cc;
        double cd;
        double dd0;
        double e;
        double e1sq;
        double easting;
        double ei;
        double fact1;
        double fact2;
        double fact3;
        double fact4;
        double k0;
        double lof1;
        double lof2;
        double lof3;
        double mu;
        double n0;
        double northing;
        double phi1;
        double r0;
        String southernHemisphere;
        double t0;
        int zone;
        double zoneCM;

        private UTM2LatLon() {
            this.southernHemisphere = "ACDEFGHJKLM";
            this.b = 6356752.314d;
            this.a = 6378137.0d;
            this.e = 0.081819191d;
            this.e1sq = 0.006739497d;
            this.k0 = 0.9996d;
        }

        /* synthetic */ UTM2LatLon(CoordinateConversion coordinateConversion, UTM2LatLon uTM2LatLon) {
            this();
        }

        /* synthetic */ UTM2LatLon(CoordinateConversion coordinateConversion, UTM2LatLon uTM2LatLon, UTM2LatLon uTM2LatLon2) {
            this();
        }

        /* access modifiers changed from: protected */
        public String getHemisphere(String latZone) {
            if (this.southernHemisphere.indexOf(latZone) > -1) {
                return "S";
            }
            return "N";
        }

        public double[] convertUTMToLatLong(String UTM) {
            double[] latlon = {0.0d, 0.0d};
            String[] utm = UTM.split(" ");
            this.zone = Integer.parseInt(utm[0]);
            String latZone = utm[1];
            this.easting = Double.parseDouble(utm[2]);
            this.northing = Double.parseDouble(utm[3]);
            String hemisphere = getHemisphere(latZone);
            if (hemisphere.equals("S")) {
                this.northing = 1.0E7d - this.northing;
            }
            setVariables();
            double latitude = (180.0d * (this.phi1 - (this.fact1 * ((this.fact2 + this.fact3) + this.fact4)))) / 3.141592653589793d;
            if (this.zone > 0) {
                this.zoneCM = ((double) (this.zone * 6)) - 183.0d;
            } else {
                this.zoneCM = 3.0d;
            }
            double longitude = this.zoneCM - this._a3;
            if (hemisphere.equals("S")) {
                latitude = -latitude;
            }
            latlon[0] = latitude;
            latlon[1] = longitude;
            return latlon;
        }

        /* access modifiers changed from: protected */
        public void setVariables() {
            this.arc = this.northing / this.k0;
            this.mu = this.arc / (this.a * (((1.0d - (CoordinateConversion.this.POW(this.e, 2.0d) / 4.0d)) - ((3.0d * CoordinateConversion.this.POW(this.e, 4.0d)) / 64.0d)) - ((5.0d * CoordinateConversion.this.POW(this.e, 6.0d)) / 256.0d)));
            this.ei = (1.0d - CoordinateConversion.this.POW(1.0d - (this.e * this.e), 0.5d)) / (1.0d + CoordinateConversion.this.POW(1.0d - (this.e * this.e), 0.5d));
            this.ca = ((3.0d * this.ei) / 2.0d) - ((27.0d * CoordinateConversion.this.POW(this.ei, 3.0d)) / 32.0d);
            this.cb = ((21.0d * CoordinateConversion.this.POW(this.ei, 2.0d)) / 16.0d) - ((55.0d * CoordinateConversion.this.POW(this.ei, 4.0d)) / 32.0d);
            this.cc = (151.0d * CoordinateConversion.this.POW(this.ei, 3.0d)) / 96.0d;
            this.cd = (1097.0d * CoordinateConversion.this.POW(this.ei, 4.0d)) / 512.0d;
            this.phi1 = this.mu + (this.ca * CoordinateConversion.this.SIN(2.0d * this.mu)) + (this.cb * CoordinateConversion.this.SIN(4.0d * this.mu)) + (this.cc * CoordinateConversion.this.SIN(6.0d * this.mu)) + (this.cd * CoordinateConversion.this.SIN(8.0d * this.mu));
            this.n0 = this.a / CoordinateConversion.this.POW(1.0d - CoordinateConversion.this.POW(this.e * CoordinateConversion.this.SIN(this.phi1), 2.0d), 0.5d);
            this.r0 = (this.a * (1.0d - (this.e * this.e))) / CoordinateConversion.this.POW(1.0d - CoordinateConversion.this.POW(this.e * CoordinateConversion.this.SIN(this.phi1), 2.0d), 1.5d);
            this.fact1 = (this.n0 * CoordinateConversion.this.TAN(this.phi1)) / this.r0;
            this._a1 = 500000.0d - this.easting;
            this.dd0 = this._a1 / (this.n0 * this.k0);
            this.fact2 = (this.dd0 * this.dd0) / 2.0d;
            this.t0 = CoordinateConversion.this.POW(CoordinateConversion.this.TAN(this.phi1), 2.0d);
            this.Q0 = this.e1sq * CoordinateConversion.this.POW(CoordinateConversion.this.COS(this.phi1), 2.0d);
            this.fact3 = (((((5.0d + (3.0d * this.t0)) + (10.0d * this.Q0)) - ((4.0d * this.Q0) * this.Q0)) - (9.0d * this.e1sq)) * CoordinateConversion.this.POW(this.dd0, 4.0d)) / 24.0d;
            this.fact4 = ((((((61.0d + (90.0d * this.t0)) + (298.0d * this.Q0)) + ((45.0d * this.t0) * this.t0)) - (252.0d * this.e1sq)) - ((3.0d * this.Q0) * this.Q0)) * CoordinateConversion.this.POW(this.dd0, 6.0d)) / 720.0d;
            this.lof1 = this._a1 / (this.n0 * this.k0);
            this.lof2 = (((1.0d + (2.0d * this.t0)) + this.Q0) * CoordinateConversion.this.POW(this.dd0, 3.0d)) / 6.0d;
            this.lof3 = ((((((5.0d - (2.0d * this.Q0)) + (28.0d * this.t0)) - (3.0d * CoordinateConversion.this.POW(this.Q0, 2.0d))) + (8.0d * this.e1sq)) + (24.0d * CoordinateConversion.this.POW(this.t0, 2.0d))) * CoordinateConversion.this.POW(this.dd0, 5.0d)) / 120.0d;
            this._a2 = ((this.lof1 - this.lof2) + this.lof3) / CoordinateConversion.this.COS(this.phi1);
            this._a3 = (this._a2 * 180.0d) / 3.141592653589793d;
        }
    }

    private class Digraphs {
        private Map digraph1 = new Hashtable();
        private String[] digraph1Array = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        private Map digraph2 = new Hashtable();
        private String[] digraph2Array = {"V", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V"};

        public Digraphs() {
            this.digraph1.put(new Integer(1), "A");
            this.digraph1.put(new Integer(2), "B");
            this.digraph1.put(new Integer(3), "C");
            this.digraph1.put(new Integer(4), "D");
            this.digraph1.put(new Integer(5), "E");
            this.digraph1.put(new Integer(6), "F");
            this.digraph1.put(new Integer(7), "G");
            this.digraph1.put(new Integer(8), "H");
            this.digraph1.put(new Integer(9), "J");
            this.digraph1.put(new Integer(10), "K");
            this.digraph1.put(new Integer(11), "L");
            this.digraph1.put(new Integer(12), "M");
            this.digraph1.put(new Integer(13), "N");
            this.digraph1.put(new Integer(14), "P");
            this.digraph1.put(new Integer(15), "Q");
            this.digraph1.put(new Integer(16), "R");
            this.digraph1.put(new Integer(17), "S");
            this.digraph1.put(new Integer(18), "T");
            this.digraph1.put(new Integer(19), "U");
            this.digraph1.put(new Integer(20), "V");
            this.digraph1.put(new Integer(21), "W");
            this.digraph1.put(new Integer(22), "X");
            this.digraph1.put(new Integer(23), "Y");
            this.digraph1.put(new Integer(24), "Z");
            this.digraph2.put(new Integer(0), "V");
            this.digraph2.put(new Integer(1), "A");
            this.digraph2.put(new Integer(2), "B");
            this.digraph2.put(new Integer(3), "C");
            this.digraph2.put(new Integer(4), "D");
            this.digraph2.put(new Integer(5), "E");
            this.digraph2.put(new Integer(6), "F");
            this.digraph2.put(new Integer(7), "G");
            this.digraph2.put(new Integer(8), "H");
            this.digraph2.put(new Integer(9), "J");
            this.digraph2.put(new Integer(10), "K");
            this.digraph2.put(new Integer(11), "L");
            this.digraph2.put(new Integer(12), "M");
            this.digraph2.put(new Integer(13), "N");
            this.digraph2.put(new Integer(14), "P");
            this.digraph2.put(new Integer(15), "Q");
            this.digraph2.put(new Integer(16), "R");
            this.digraph2.put(new Integer(17), "S");
            this.digraph2.put(new Integer(18), "T");
            this.digraph2.put(new Integer(19), "U");
            this.digraph2.put(new Integer(20), "V");
        }

        public int getDigraph1Index(String letter) {
            for (int i = 0; i < this.digraph1Array.length; i++) {
                if (this.digraph1Array[i].equals(letter)) {
                    return i + 1;
                }
            }
            return -1;
        }

        public int getDigraph2Index(String letter) {
            for (int i = 0; i < this.digraph2Array.length; i++) {
                if (this.digraph2Array[i].equals(letter)) {
                    return i;
                }
            }
            return -1;
        }

        public String getDigraph1(int longZone, double easting) {
            return (String) this.digraph1.get(new Integer((int) Math.floor((((double) ((int) (easting / 100000.0d))) + ((double) ((((longZone - 1) % 3) * 8) + 1))) - 1.0d)));
        }

        public String getDigraph2(int longZone, double northing) {
            double a2 = (double) ((((longZone - 1) % 2) * 5) + 1);
            double a3 = northing;
            double d = a2 + ((double) ((int) (a3 / 100000.0d)));
            double a4 = Math.floor((((double) ((int) (a3 / 100000.0d))) + a2) % 20.0d);
            if (a4 < 0.0d) {
                a4 += 19.0d;
            }
            return (String) this.digraph2.get(new Integer((int) Math.floor(a4)));
        }
    }

    private class LatZones {
        private int arrayLength;
        private int[] degrees;
        private char[] letters = {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z'};
        private int[] negDegrees;
        private char[] negLetters;
        private int[] posDegrees;
        private char[] posLetters;

        public LatZones() {
            int[] iArr = new int[22];
            iArr[0] = -90;
            iArr[1] = -84;
            iArr[2] = -72;
            iArr[3] = -64;
            iArr[4] = -56;
            iArr[5] = -48;
            iArr[6] = -40;
            iArr[7] = -32;
            iArr[8] = -24;
            iArr[9] = -16;
            iArr[10] = -8;
            iArr[12] = 8;
            iArr[13] = 16;
            iArr[14] = 24;
            iArr[15] = 32;
            iArr[16] = 40;
            iArr[17] = 48;
            iArr[18] = 56;
            iArr[19] = 64;
            iArr[20] = 72;
            iArr[21] = 84;
            this.degrees = iArr;
            this.negLetters = new char[]{'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M'};
            this.negDegrees = new int[]{-90, -84, -72, -64, -56, -48, -40, -32, -24, -16, -8};
            this.posLetters = new char[]{'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z'};
            int[] iArr2 = new int[11];
            iArr2[1] = 8;
            iArr2[2] = 16;
            iArr2[3] = 24;
            iArr2[4] = 32;
            iArr2[5] = 40;
            iArr2[6] = 48;
            iArr2[7] = 56;
            iArr2[8] = 64;
            iArr2[9] = 72;
            iArr2[10] = 84;
            this.posDegrees = iArr2;
            this.arrayLength = 22;
        }

        public int getLatZoneDegree(String letter) {
            char ltr = letter.charAt(0);
            for (int i = 0; i < this.arrayLength; i++) {
                if (this.letters[i] == ltr) {
                    return this.degrees[i];
                }
            }
            return -100;
        }

        public String getLatZone(double latitude) {
            int latIndex = -2;
            int lat = (int) latitude;
            if (lat >= 0) {
                int len = this.posLetters.length;
                int i = 0;
                while (true) {
                    if (i < len) {
                        if (lat != this.posDegrees[i]) {
                            if (lat <= this.posDegrees[i]) {
                                latIndex = i - 1;
                                break;
                            }
                            i++;
                        } else {
                            latIndex = i;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            } else {
                int len2 = this.negLetters.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= len2) {
                        break;
                    } else if (lat == this.negDegrees[i2]) {
                        latIndex = i2;
                        break;
                    } else if (lat < this.negDegrees[i2]) {
                        latIndex = i2 - 1;
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            if (latIndex == -1) {
                latIndex = 0;
            }
            if (lat >= 0) {
                if (latIndex == -2) {
                    latIndex = this.posLetters.length - 1;
                }
                return String.valueOf(this.posLetters[latIndex]);
            }
            if (latIndex == -2) {
                latIndex = this.negLetters.length - 1;
            }
            return String.valueOf(this.negLetters[latIndex]);
        }
    }
}
