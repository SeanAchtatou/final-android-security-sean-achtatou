package com.crittermap.backcountrynavigator.nav;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.google.android.apps.mytracks.stats.TripStatistics;
import com.google.android.apps.mytracks.stats.TripStatisticsBuilder;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.TimeZone;
import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.OSRef;
import uk.me.jstott.jcoord.datum.WGS84Datum;
import uk.me.jstott.jcoord.datum.nad27.NAD27ContiguousUSDatum;

public class NavStatUpdater implements Observer {
    static final double FEETFROMMETERS = 3.2808399d;
    public static final float FEETFROMMETERSFLOAT = 3.28084f;
    public static final float KMSFROMMETERS = 0.001f;
    static final float KPHFROMMETERSPERSECOND = 3.6f;
    public static final float MILESFROMMETERS = 6.213712E-4f;
    static final float MPHFROMMETERSPERSECOND = 2.2369363f;
    static final int STRINGBUFFERSIZE = 32;
    private static final int TIMEOFFSET = TimeZone.getDefault().getRawOffset();
    private static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    String[] contentString;
    Context context;
    CoordinateConversion converter = new CoordinateConversion();
    NumberFormat degreeFormat;
    NumberFormat fourFractionFormat;
    NumberFormat fractionFormat;
    String[] labels;
    private LayoutInflater mInflater;
    private TextView mtvAccuracy;
    private TextView mtvAltitude;
    private TextView mtvAltitudeGain;
    private TextView mtvAvgMovingSpeed;
    private TextView mtvElevationGain;
    private TextView mtvElevationLoss;
    private TextView mtvGotoName;
    private TextView mtvHeading;
    private TextView mtvLocation;
    private TextView mtvMaxAltitude;
    private TextView mtvMaxGrade;
    private TextView mtvMaxSpeed;
    private TextView mtvMinAltitude;
    private TextView mtvMinGrade;
    private TextView mtvMovingTime;
    private TextView mtvPosition;
    private TextView mtvSpeed;
    private TextView mtvTargetBearing;
    private TextView mtvTargetRange;
    private TextView mtvTotalDistance;
    private TextView mtvTotalStart;
    private TextView mtvTotalTime;
    Navigator navigator;
    NumberFormat oneFractionFormat;
    Date time;
    private TripStatistics tripStats;
    /* access modifiers changed from: private */
    public TripStatisticsBuilder tripStatsBuilder;

    public NavStatUpdater(Context ctx, Navigator nav, ViewGroup container) {
        this.context = ctx;
        this.navigator = nav;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
        this.navigator.addObserver(this);
        this.labels = ctx.getResources().getStringArray(R.array.navstat_labels);
        this.contentString = new String[this.labels.length];
        for (int i = 0; i < this.contentString.length; i++) {
            this.contentString[i] = "";
        }
        this.degreeFormat = NumberFormat.getInstance();
        this.degreeFormat.setMaximumFractionDigits(0);
        this.fractionFormat = NumberFormat.getInstance();
        this.fractionFormat.setMaximumFractionDigits(0);
        this.fractionFormat.setGroupingUsed(false);
        this.oneFractionFormat = NumberFormat.getInstance();
        this.oneFractionFormat.setMaximumFractionDigits(1);
        this.fourFractionFormat = NumberFormat.getInstance();
        this.fourFractionFormat.setMaximumFractionDigits(4);
        this.mtvLocation = (TextView) container.findViewById(R.id.stat_position);
        this.mtvHeading = (TextView) container.findViewById(R.id.stat_heading);
        this.mtvSpeed = (TextView) container.findViewById(R.id.stat_speed);
        this.mtvMovingTime = (TextView) container.findViewById(R.id.stat_moving_time);
        this.mtvAvgMovingSpeed = (TextView) container.findViewById(R.id.stat_avg_moving_speed);
        this.mtvMaxSpeed = (TextView) container.findViewById(R.id.stat_max_speed);
        this.mtvAltitudeGain = (TextView) container.findViewById(R.id.stat_altitude_gain);
        this.mtvMinAltitude = (TextView) container.findViewById(R.id.stat_altitude_min);
        this.mtvMaxAltitude = (TextView) container.findViewById(R.id.stat_altitude_max);
        this.mtvMinGrade = (TextView) container.findViewById(R.id.stat_grade_min);
        this.mtvMaxGrade = (TextView) container.findViewById(R.id.stat_grade_max);
        this.mtvAltitude = (TextView) container.findViewById(R.id.stat_altitude);
        this.mtvAccuracy = (TextView) container.findViewById(R.id.stat_accuracy);
        this.mtvGotoName = (TextView) container.findViewById(R.id.t_goto_destination);
        this.mtvTargetBearing = (TextView) container.findViewById(R.id.stat_target_bearing);
        this.mtvTargetRange = (TextView) container.findViewById(R.id.stat_target_range);
        this.mtvTotalStart = (TextView) container.findViewById(R.id.stat_total_beginning);
        this.mtvTotalTime = (TextView) container.findViewById(R.id.stat_total_time);
        this.mtvTotalDistance = (TextView) container.findViewById(R.id.stat_total_distance);
        this.mtvElevationGain = (TextView) container.findViewById(R.id.stat_total_elevation_gain);
        this.mtvElevationLoss = (TextView) container.findViewById(R.id.stat_total_elevation_loss);
        ((Button) container.findViewById(R.id.button_stat_reset)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                NavStatUpdater.this.navigator.clear();
                NavStatUpdater.this.tripStatsBuilder = new TripStatisticsBuilder();
            }
        });
        this.tripStatsBuilder = new TripStatisticsBuilder();
        this.navigator.hasGotoPos();
    }

    public void update(Observable observable, Object data) {
        float target;
        if (data.equals(Navigator.LOCATION)) {
            boolean metric = BCNSettings.MetricDisplay.get();
            Location loc = this.navigator.currentLocation;
            this.tripStatsBuilder.resume();
            gotLocation(loc, metric);
            int format = BCNSettings.CoordinateFormat.get();
            if (format < BCNSettings.UTMCOORDINATES) {
                LatLng latlon = determineDatum(new LatLng(loc.getLatitude(), loc.getLongitude()));
                this.mtvLocation.setText(String.valueOf(Location.convert(latlon.getLatitude(), format)) + "," + Location.convert(latlon.getLongitude(), format));
            } else if (format == BCNSettings.UTMCOORDINATES) {
                LatLng latlon2 = determineDatum(new LatLng(loc.getLatitude(), loc.getLongitude()));
                this.mtvLocation.setText(this.converter.latLon2UTM(latlon2.getLatitude(), latlon2.getLongitude()));
            } else if (format == BCNSettings.MGRSCOORDINATES) {
                LatLng latlon3 = determineDatum(new LatLng(loc.getLatitude(), loc.getLongitude()));
                this.mtvLocation.setText(this.converter.latLon2MGRUTM(latlon3.getLatitude(), latlon3.getLongitude()));
            } else if (format == BCNSettings.OSREFCOORDINATESEN) {
                try {
                    OSRef oSRef = new OSRef(new LatLng(loc.getLatitude(), loc.getLongitude()));
                    this.mtvLocation.setText(String.valueOf(this.fractionFormat.format(oSRef.getEasting())) + " " + this.fractionFormat.format(oSRef.getNorthing()));
                } catch (Exception e) {
                    this.mtvLocation.setText("OSRef:Out of Range");
                }
            } else if (format == BCNSettings.OSREFCOORDINATES6FIG) {
                try {
                    this.mtvLocation.setText(new OSRef(new LatLng(loc.getLatitude(), loc.getLongitude())).toSixFigureString());
                } catch (Exception e2) {
                    this.mtvLocation.setText("OSRef:Out of Range");
                }
            }
            if (loc.hasBearing()) {
                float bearing = loc.getBearing();
                if (!BCNSettings.MagneticDegrees.get()) {
                    float bearing2 = bearing + this.navigator.declination;
                }
                this.mtvHeading.setText(String.valueOf(this.degreeFormat.format((double) loc.getBearing())) + "°");
            }
            if (loc.hasSpeed()) {
                if (!metric) {
                    this.mtvSpeed.setText(String.valueOf(this.oneFractionFormat.format((double) (loc.getSpeed() * MPHFROMMETERSPERSECOND))) + " mph");
                } else {
                    this.mtvSpeed.setText(String.valueOf(this.oneFractionFormat.format((double) (loc.getSpeed() * KPHFROMMETERSPERSECOND))) + " kph");
                }
            }
            if (loc.hasAltitude()) {
                if (!metric) {
                    this.mtvAltitude.setText(String.valueOf(this.degreeFormat.format(loc.getAltitude() * 3.2808399d)) + " ft");
                } else {
                    this.mtvAltitude.setText(String.valueOf(this.degreeFormat.format(loc.getAltitude())) + " m");
                }
            }
            if (loc.hasAccuracy()) {
                if (!metric) {
                    this.mtvAccuracy.setText(String.valueOf(this.degreeFormat.format(((double) loc.getAccuracy()) * 3.2808399d)) + " ft");
                } else {
                    this.mtvAccuracy.setText(String.valueOf(this.degreeFormat.format((double) loc.getAccuracy())) + " m");
                }
            }
            if (this.navigator.hasGotoPos()) {
                float range = this.navigator.getGotoDistance();
                if (!metric) {
                    if (range > 400.0f) {
                        this.mtvTargetRange.setText(String.valueOf(this.oneFractionFormat.format((double) (range * 6.213712E-4f))) + " mi");
                    } else {
                        this.mtvTargetRange.setText(String.valueOf(this.oneFractionFormat.format((double) (range * 3.28084f))) + " ft");
                    }
                } else if (range > 1000.0f) {
                    this.mtvTargetRange.setText(String.valueOf(this.oneFractionFormat.format((double) (range * 0.001f))) + " k");
                } else {
                    this.mtvTargetRange.setText(String.valueOf(this.oneFractionFormat.format((double) range)) + " m");
                }
                if (BCNSettings.MagneticDegrees.get()) {
                    target = this.navigator.getGotoMagneticBearing();
                } else {
                    target = this.navigator.getGotoBearing();
                }
                this.mtvTargetBearing.setText(String.valueOf(this.degreeFormat.format((double) target)) + "°");
            } else {
                this.mtvTargetBearing.setText("");
                this.mtvTargetRange.setText("");
            }
            if (this.navigator.startTime != null) {
                this.mtvTotalStart.setText(this.navigator.startTime.toString());
                double range2 = this.navigator.distanceTravelled;
                new Date();
                if (!metric) {
                    if (range2 > 400.0d) {
                        this.mtvTotalDistance.setText(String.valueOf(this.oneFractionFormat.format(range2 * 6.21371204033494E-4d)) + " mi");
                    } else {
                        this.mtvTotalDistance.setText(String.valueOf(this.oneFractionFormat.format(range2 * 3.2808399d)) + " ft");
                    }
                } else if (range2 > 1000.0d) {
                    this.mtvTotalDistance.setText(String.valueOf(this.oneFractionFormat.format(range2 * 0.0010000000474974513d)) + " k");
                } else {
                    this.mtvTotalDistance.setText(String.valueOf(this.oneFractionFormat.format(range2)) + " m");
                }
                if (!metric) {
                    this.mtvElevationGain.setText(String.valueOf(this.degreeFormat.format(this.navigator.altitudeClimbed * 3.2808399d)) + " ft");
                    this.mtvElevationLoss.setText(String.valueOf(this.degreeFormat.format(this.navigator.altitudeDescended * 3.2808399d)) + " ft");
                    return;
                }
                this.mtvElevationGain.setText(String.valueOf(this.degreeFormat.format(this.navigator.altitudeClimbed)) + " m");
                this.mtvElevationLoss.setText(String.valueOf(this.degreeFormat.format(this.navigator.altitudeDescended)) + " m");
            }
        } else if (data.equals(Navigator.GPSSTATUS)) {
            this.navigator.getGpsStatus();
        }
    }

    private LatLng determineDatum(LatLng latlngToConvert) {
        LatLng latlng = latlngToConvert;
        int type = BCNSettings.DatumType.get();
        if (type == 0 && latlngToConvert.getDatum().getName() != WGS84Datum.getInstance().getName()) {
            latlngToConvert.toDatum(WGS84Datum.getInstance());
            return latlngToConvert;
        } else if (type != 1 || latlngToConvert.getDatum().getName() == NAD27ContiguousUSDatum.getInstance().getName()) {
            return latlng;
        } else {
            latlngToConvert.toDatum(NAD27ContiguousUSDatum.getInstance());
            return latlngToConvert;
        }
    }

    private void gotLocation(Location loc, boolean metric) {
        if (this.tripStatsBuilder.addLocation(loc, System.currentTimeMillis())) {
            this.tripStats = this.tripStatsBuilder.getStatistics();
            if (this.tripStats.getMovingTime() != 0) {
                this.time = new Date(this.tripStats.getMovingTime());
                if (TimeZone.getDefault().inDaylightTime(this.time)) {
                    this.time = new Date(this.tripStats.getMovingTime() - ((long) TimeZone.getDefault().getOffset(this.tripStats.getMovingTime())));
                } else if (!TimeZone.getDefault().inDaylightTime(this.time)) {
                    this.time = new Date(this.tripStats.getMovingTime() - ((long) TIMEOFFSET));
                }
                this.mtvMovingTime.setText(formatter.format(this.time));
            }
            if (!Double.isNaN(this.tripStats.getAverageMovingSpeed())) {
                if (!metric) {
                    this.mtvAvgMovingSpeed.setText(String.valueOf(this.oneFractionFormat.format(this.tripStats.getAverageMovingSpeed() * 2.236936330795288d)) + " mph");
                } else {
                    this.mtvAvgMovingSpeed.setText(String.valueOf(this.oneFractionFormat.format(this.tripStats.getAverageMovingSpeed() * 3.5999999046325684d)) + " kph");
                }
            }
            if (!Double.isNaN(this.tripStats.getAverageMovingSpeed())) {
                if (!metric) {
                    this.mtvMaxSpeed.setText(String.valueOf(this.oneFractionFormat.format(this.tripStats.getMaxSpeed() * 2.236936330795288d)) + " mph");
                } else {
                    this.mtvMaxSpeed.setText(String.valueOf(this.oneFractionFormat.format(this.tripStats.getMaxSpeed() * 3.5999999046325684d)) + " kph");
                }
            }
            if (loc.hasAltitude()) {
                if (!metric) {
                    this.mtvAltitudeGain.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getTotalElevationGain() * 3.2808399d)) + " ft");
                    this.mtvMinAltitude.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getMinElevation() * 3.2808399d)) + " ft");
                    this.mtvMaxAltitude.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getMaxElevation() * 3.2808399d)) + " ft");
                } else {
                    this.mtvAltitudeGain.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getTotalElevationGain())) + " m");
                    this.mtvMinAltitude.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getMinElevation())) + " m");
                    this.mtvMaxAltitude.setText(String.valueOf(this.degreeFormat.format(this.tripStats.getMaxElevation())) + " m");
                }
            }
            if (!Double.isInfinite(this.tripStats.getMinGrade())) {
                this.mtvMinGrade.setText(String.valueOf(this.fourFractionFormat.format(this.tripStats.getMinGrade())) + " %");
            }
            if (!Double.isInfinite(this.tripStats.getMaxGrade())) {
                this.mtvMaxGrade.setText(String.valueOf(this.fourFractionFormat.format(this.tripStats.getMaxGrade())) + " %");
            }
        }
    }
}
