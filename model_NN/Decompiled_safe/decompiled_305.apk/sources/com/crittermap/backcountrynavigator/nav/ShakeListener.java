package com.crittermap.backcountrynavigator.nav;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class ShakeListener implements SensorEventListener {
    private static final float FORCE_THRESHOLD = 20.0f;
    private static final int SHAKE_COUNT = 1;
    private static final int SHAKE_DURATION = 1000000000;
    private static final int SHAKE_TIMEOUT = 500000000;
    private static final int TIME_THRESHOLD = 100000000;
    private Context mContext;
    private long mLastForce;
    private long mLastShake;
    private long mLastTime = 0;
    private float mLastX = -1.0f;
    private float mLastY = -1.0f;
    private float mLastZ = -1.0f;
    private SensorManager mSensorMgr;
    private int mShakeCount = 0;
    private OnShakeListener mShakeListener;

    public interface OnShakeListener {
        void onShake();
    }

    public ShakeListener(Context context) {
        this.mContext = context;
    }

    public void setOnShakeListener(OnShakeListener listener) {
        this.mShakeListener = listener;
    }

    public void resume() {
        this.mSensorMgr = (SensorManager) this.mContext.getSystemService("sensor");
        if (this.mSensorMgr != null && !this.mSensorMgr.registerListener(this, this.mSensorMgr.getDefaultSensor(1), 2)) {
            this.mSensorMgr.unregisterListener(this);
            throw new UnsupportedOperationException("Accelerometer not supported");
        }
    }

    public void pause() {
        if (this.mSensorMgr != null) {
            this.mSensorMgr.unregisterListener(this);
            this.mSensorMgr = null;
        }
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    public void onSensorChanged(SensorEvent event) {
        if ((event.sensor.getType() & 1) != 0) {
            long now = event.timestamp;
            if (now - this.mLastForce > 500000000) {
                this.mShakeCount = 0;
            }
            float[] values = event.values;
            float x = values[0];
            float y = values[1];
            float z = values[2];
            if (now - this.mLastTime > 100000000) {
                long diff = now - this.mLastTime;
                if (diff > 0 && (Math.abs(((((x + y) + z) - this.mLastX) - this.mLastY) - this.mLastZ) / ((float) diff)) * 1.0E9f > FORCE_THRESHOLD) {
                    int i = this.mShakeCount + 1;
                    this.mShakeCount = i;
                    if (i >= 1 && now - this.mLastShake > 1000000000) {
                        this.mLastShake = now;
                        this.mShakeCount = 0;
                        if (this.mShakeListener != null) {
                            this.mShakeListener.onShake();
                        }
                    }
                    this.mLastForce = now;
                }
                this.mLastTime = now;
                this.mLastX = x;
                this.mLastY = y;
                this.mLastZ = z;
            }
        }
    }
}
