package com.crittermap.backcountrynavigator.nav;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import java.text.NumberFormat;
import java.util.Observable;
import java.util.Observer;

public class NavStatAdapter extends BaseAdapter implements Observer {
    static final double FEETFROMMETERS = 3.2808399d;
    static final float FEETFROMMETERSFLOAT = 3.28084f;
    static final float KMSFROMMETERS = 0.001f;
    static final float KPHFROMMETERSPERSECOND = 3.6f;
    static final float MILESFROMMETERS = 6.213712E-4f;
    static final float MPHFROMMETERSPERSECOND = 2.2369363f;
    static final int STRINGBUFFERSIZE = 32;
    String[] contentString;
    Context context;
    CoordinateConversion converter = new CoordinateConversion();
    NumberFormat degreeFormat;
    String[] labels;
    private LayoutInflater mInflater;
    Navigator navigator;
    NumberFormat oneFractionFormat;

    public NavStatAdapter(Context ctx, Navigator nav) {
        this.context = ctx;
        this.navigator = nav;
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
        this.navigator.addObserver(this);
        this.labels = ctx.getResources().getStringArray(R.array.navstat_labels);
        this.contentString = new String[this.labels.length];
        for (int i = 0; i < this.contentString.length; i++) {
            this.contentString[i] = "";
        }
        this.degreeFormat = NumberFormat.getInstance();
        this.degreeFormat.setMaximumFractionDigits(0);
        this.oneFractionFormat = NumberFormat.getInstance();
        this.oneFractionFormat.setMaximumFractionDigits(1);
    }

    public int getCount() {
        return this.labels.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.nav_stat, (ViewGroup) null);
            holder = new ViewHolder();
            holder.label = (TextView) convertView.findViewById(R.id.stat_label);
            holder.content = (TextView) convertView.findViewById(R.id.stat_content);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.label.setText(this.labels[position]);
        holder.content.setText(this.contentString[position]);
        return convertView;
    }

    public void update(Observable observable, Object data) {
        String UTMString;
        int idx;
        if (data.equals(Navigator.LOCATION)) {
            boolean metric = BCNSettings.MetricDisplay.get();
            Location loc = this.navigator.currentLocation;
            int format = BCNSettings.CoordinateFormat.get();
            if (format < BCNSettings.UTMCOORDINATES) {
                this.contentString[1] = Location.convert(loc.getLatitude(), BCNSettings.CoordinateFormat.get());
                this.contentString[0] = Location.convert(loc.getLongitude(), BCNSettings.CoordinateFormat.get());
            } else if (format == BCNSettings.UTMCOORDINATES) {
                String UTMString2 = this.converter.latLon2UTM(loc.getLatitude(), loc.getLongitude());
                int idx2 = UTMString2.lastIndexOf(32);
                if (idx2 != -1) {
                    this.contentString[0] = UTMString2.substring(0, idx2).replaceAll(" ", "");
                    this.contentString[1] = UTMString2.substring(idx2 + 1, UTMString2.length());
                }
            } else if (format == BCNSettings.MGRSCOORDINATES && (idx = (UTMString = this.converter.latLon2MGRUTM(loc.getLatitude(), loc.getLongitude())).lastIndexOf(32)) != -1) {
                this.contentString[0] = UTMString.substring(0, idx).replaceAll(" ", "");
                this.contentString[1] = UTMString.substring(idx + 1, UTMString.length());
            }
            if (loc.hasBearing()) {
                this.contentString[2] = String.valueOf(this.degreeFormat.format((double) loc.getBearing())) + "°";
            }
            if (loc.hasSpeed()) {
                if (!metric) {
                    this.contentString[3] = String.valueOf(this.oneFractionFormat.format((double) (loc.getSpeed() * MPHFROMMETERSPERSECOND))) + " mph";
                } else {
                    this.contentString[3] = String.valueOf(this.oneFractionFormat.format((double) (loc.getSpeed() * KPHFROMMETERSPERSECOND))) + " kph";
                }
            }
            if (loc.hasAltitude()) {
                if (!metric) {
                    this.contentString[4] = String.valueOf(this.degreeFormat.format(loc.getAltitude() * 3.2808399d)) + " ft";
                } else {
                    this.contentString[4] = String.valueOf(this.degreeFormat.format(loc.getAltitude())) + " m";
                }
            }
            if (this.navigator.hasGotoPos()) {
                float range = this.navigator.getGotoDistance();
                if (!metric) {
                    if (range > 400.0f) {
                        this.contentString[6] = String.valueOf(this.oneFractionFormat.format((double) (range * 6.213712E-4f))) + " mi";
                    } else {
                        this.contentString[6] = String.valueOf(this.oneFractionFormat.format((double) (range * 3.28084f))) + " ft";
                    }
                } else if (range > 1000.0f) {
                    this.contentString[6] = String.valueOf(this.oneFractionFormat.format((double) (range * 0.001f))) + " k";
                } else {
                    this.contentString[6] = String.valueOf(this.oneFractionFormat.format((double) range)) + " m";
                }
                this.contentString[5] = String.valueOf(this.degreeFormat.format((double) this.navigator.getGotoBearing())) + "°";
            } else {
                this.contentString[5] = "";
                this.contentString[6] = "";
            }
        } else if (data.equals(Navigator.GPSSTATUS)) {
            this.contentString[7] = this.degreeFormat.format((long) this.navigator.getGpsStatus().getTimeToFirstFix());
        }
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView content;
        TextView label;

        ViewHolder() {
        }
    }
}
