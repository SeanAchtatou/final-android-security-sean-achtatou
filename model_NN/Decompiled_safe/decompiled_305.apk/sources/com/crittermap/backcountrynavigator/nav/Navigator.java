package com.crittermap.backcountrynavigator.nav;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.tracks.TrackRecordingService;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

public class Navigator extends Observable implements SensorEventListener, GpsStatus.Listener, LocationListener {
    public static final Integer ANGLE = new Integer(1);
    public static final int DEFAULT_ANNOUNCEMENT_FREQUENCY = -1;
    public static final int DEFAULT_MAX_RECORDING_DISTANCE = 200;
    public static final int DEFAULT_MIN_RECORDING_DISTANCE = 5;
    public static final int DEFAULT_MIN_RECORDING_INTERVAL = 0;
    public static final int DEFAULT_MIN_REQUIRED_ACCURACY = 200;
    public static final int DEFAULT_SPLIT_FREQUENCY = 0;
    public static final Integer GPSSTATUS = new Integer(2);
    public static final Integer LOCATION = new Integer(0);
    static final int MATRIX_SIZE = 9;
    static final int ORBUFFERSIZE = 7;
    static final float THRESHSPEED = 0.44444445f;
    int accuracy;
    private boolean acquiring = false;
    double altitudeClimbed;
    double altitudeDescended;
    double averageSpeedWhenMoving;
    boolean bRegistered = false;
    float compassBearing;
    Location currentLocation = null;
    float declination = 0.0f;
    double distanceTravelled;
    float gotoBearing = 0.0f;
    float gotoDistance = 0.0f;
    GpsStatus gpsStatus = null;
    Location lastLocation = null;
    private Position lastPos = null;
    private float[] mAccelerometerValues;
    boolean mAcceptNetworkLocation = true;
    private float mAzimuth;
    Context mContext;
    String mGotoDestination = "";
    Position mGotoPos = null;
    private float[] mGravityValues;
    LocationManager mLocationManager;
    private float[] mMagneticValues;
    private float mPitch;
    private float mRoll;
    SensorManager mSensorManager;
    private float[] mValues;
    private int maxRecordingDistance = 200;
    private int minRecordingDistance = 5;
    private int minRecordingInterval = 0;
    private int minRequiredAccuracy = 200;
    long numRecordings = 0;
    LinkedList<Float> orQueue = new LinkedList<>();
    private long recordingTrackId = -1;
    int sensorfrequency = 3;
    private List<Sensor> sensorslist;
    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("Navigator", "Track Recording Service now connected.");
            Navigator.this.trackRecordingService = ((TrackRecordingService.TrackRecordingServiceBinder) service).getService();
            if (Navigator.this.startNewTrackRequested) {
                Navigator.this.startNewTrackRequested = false;
                Navigator.this.trackRecordingService.startTrack(Navigator.this.startNewTrackIn);
                Toast.makeText(Navigator.this.mContext, (int) R.string.status_now_recording, 0).show();
            }
            if (Navigator.this.trackRecordingService.isRecording() && !Navigator.this.isStarted()) {
                Navigator.this.start();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.d("Navigator", "MyTracks: Service now disconnected.");
            Navigator.this.trackRecordingService = null;
        }
    };
    String startNewTrackIn = null;
    protected boolean startNewTrackRequested = false;
    Date startTime = null;
    boolean started;
    ArrayList<NavStat> statList = new ArrayList<>();
    private Navigator theInstance = null;
    long timeMoving;
    long timeStopped;
    TrackRecordingService trackRecordingService = null;
    boolean useOrientation = true;

    public Navigator(Context context) {
        this.mContext = context;
        this.mLocationManager = (LocationManager) this.mContext.getSystemService("location");
        List<String> allProviders = this.mLocationManager.getAllProviders();
    }

    public void start() {
        if (!this.started) {
            this.mLocationManager.addGpsStatusListener(this);
            LocationProvider provider = this.mLocationManager.getProvider(MyTracksConstants.GPS_PROVIDER);
            this.mLocationManager.requestLocationUpdates(MyTracksConstants.GPS_PROVIDER, 1000, 0.1f, this);
            this.started = true;
            this.mAcceptNetworkLocation = true;
        }
        if (this.currentLocation != null) {
            setChanged();
            notifyObservers(LOCATION);
        }
    }

    private Location findRecentLocation() {
        Location gpsL = null;
        Location netL = null;
        try {
            gpsL = this.mLocationManager.getLastKnownLocation(MyTracksConstants.GPS_PROVIDER);
            netL = this.mLocationManager.getLastKnownLocation("network");
        } catch (Exception e) {
            Log.e("Navigator", "findrecentLocation", e);
        }
        if (gpsL == null || netL == null) {
            if (gpsL != null) {
                return gpsL;
            }
            if (netL != null) {
                return netL;
            }
            return null;
        } else if (gpsL.getTime() > netL.getTime()) {
            return gpsL;
        } else {
            return netL;
        }
    }

    public void resume() {
        if (this.mSensorManager == null) {
            this.mSensorManager = (SensorManager) this.mContext.getSystemService("sensor");
        }
        if (this.mSensorManager != null) {
            this.sensorslist = this.mSensorManager.getSensorList(-1);
            if (this.useOrientation) {
                boolean registerListener = this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(3), this.sensorfrequency);
            } else {
                boolean registerListener2 = this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(2), this.sensorfrequency);
                boolean supported = this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(9), this.sensorfrequency);
                boolean supported2 = this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), this.sensorfrequency);
                this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(11), this.sensorfrequency);
            }
            if (this.trackRecordingService == null) {
                tryBindTrackRecordingService();
            }
        }
    }

    public void pause() {
        if (this.mSensorManager != null) {
            this.mSensorManager.unregisterListener(this);
            this.mSensorManager = null;
        }
        if (this.trackRecordingService != null) {
            tryUnbindTrackRecordingService();
        }
    }

    public boolean isStarted() {
        return this.started;
    }

    public void stop() {
        this.mLocationManager.removeGpsStatusListener(this);
        LocationProvider provider = this.mLocationManager.getProvider(MyTracksConstants.GPS_PROVIDER);
        this.mLocationManager.removeUpdates(this);
        this.started = false;
        this.acquiring = false;
    }

    public int getAccuracy() {
        return this.accuracy;
    }

    public Location getCurrentLocation() {
        return this.currentLocation;
    }

    /* access modifiers changed from: protected */
    public void setCurrentLocation(Location newLocation) {
        if (!(this.currentLocation == null || this.startTime == null)) {
            this.distanceTravelled += (double) newLocation.distanceTo(this.currentLocation);
            if (this.currentLocation.hasAltitude() && newLocation.hasAltitude()) {
                double diff = newLocation.getAltitude() - this.currentLocation.getAltitude();
                if (diff > 0.0d) {
                    this.altitudeClimbed += diff;
                } else {
                    this.altitudeDescended += -diff;
                }
            }
        }
        this.currentLocation = newLocation;
        this.declination = (float) Declination.declination(this.currentLocation.getLongitude(), this.currentLocation.getLatitude());
        if (hasGotoPos()) {
            float[] results = new float[3];
            Location.distanceBetween(this.currentLocation.getLatitude(), this.currentLocation.getLongitude(), this.mGotoPos.lat, this.mGotoPos.lon, results);
            this.gotoDistance = results[0];
            this.gotoBearing = results[1];
        }
    }

    public Position getLastPos() {
        return this.lastPos;
    }

    public void setLastPos(Position lastPos2) {
        this.lastPos = lastPos2;
    }

    public void onAccuracyChanged(Sensor arg0, int arg1) {
        this.accuracy = arg1;
    }

    private float averageAngleIn(SensorEvent orevent) {
        float f = orevent.values[0];
        if (this.orQueue.size() >= 6) {
            this.orQueue.removeFirst();
        }
        this.orQueue.add(Float.valueOf(f));
        float answer = 0.0f;
        int N = this.orQueue.size();
        float denom = ((float) ((N + 1) * N)) / 2.0f;
        for (int i = 0; i < this.orQueue.size(); i++) {
            answer += (this.orQueue.get(i).floatValue() * ((float) (i + 1))) / denom;
        }
        return (float) Math.rint((double) answer);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSensorChanged(android.hardware.SensorEvent r11) {
        /*
            r10 = this;
            float[] r6 = r11.values
            r10.mValues = r6
            long r3 = java.lang.System.nanoTime()
            long r6 = r11.timestamp
            long r6 = r3 - r6
            r8 = 1000000000(0x3b9aca00, double:4.94065646E-315)
            int r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r6 <= 0) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            r2 = 0
            boolean r6 = r10.useOrientation
            if (r6 != 0) goto L_0x00df
            monitor-enter(r10)
            android.hardware.Sensor r6 = r11.sensor     // Catch:{ all -> 0x008b }
            int r6 = r6.getType()     // Catch:{ all -> 0x008b }
            switch(r6) {
                case 1: goto L_0x0099;
                case 2: goto L_0x0080;
                case 9: goto L_0x008e;
                default: goto L_0x0023;
            }     // Catch:{ all -> 0x008b }
        L_0x0023:
            float[] r6 = r10.mMagneticValues     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x00a4
            float[] r6 = r10.mAccelerometerValues     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x00a4
            r6 = 9
            float[] r0 = new float[r6]     // Catch:{ all -> 0x008b }
            r6 = 0
            float[] r7 = r10.mAccelerometerValues     // Catch:{ all -> 0x008b }
            float[] r8 = r10.mMagneticValues     // Catch:{ all -> 0x008b }
            android.hardware.SensorManager.getRotationMatrix(r0, r6, r7, r8)     // Catch:{ all -> 0x008b }
            r6 = 3
            float[] r5 = new float[r6]     // Catch:{ all -> 0x008b }
            android.hardware.SensorManager.getOrientation(r0, r5)     // Catch:{ all -> 0x008b }
            r6 = 0
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mAzimuth = r6     // Catch:{ all -> 0x008b }
            r6 = 1
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mPitch = r6     // Catch:{ all -> 0x008b }
            r6 = 2
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mRoll = r6     // Catch:{ all -> 0x008b }
            float r6 = r10.mAzimuth     // Catch:{ all -> 0x008b }
            r7 = 1127481344(0x43340000, float:180.0)
            float r6 = r6 * r7
            double r6 = (double) r6     // Catch:{ all -> 0x008b }
            r8 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
            double r6 = r6 / r8
            float r2 = (float) r6     // Catch:{ all -> 0x008b }
        L_0x0059:
            monitor-exit(r10)     // Catch:{ all -> 0x008b }
        L_0x005a:
            float r6 = r10.compassBearing
            float r6 = r2 - r6
            float r1 = java.lang.Math.abs(r6)
            int r6 = r11.accuracy
            r10.accuracy = r6
            r6 = 1065353216(0x3f800000, float:1.0)
            int r6 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r6 < 0) goto L_0x0013
            r10.compassBearing = r2
            r10.setChanged()
            java.lang.Integer r6 = new java.lang.Integer
            java.lang.Integer r7 = com.crittermap.backcountrynavigator.nav.Navigator.ANGLE
            int r7 = r7.intValue()
            r6.<init>(r7)
            r10.notifyObservers(r6)
            goto L_0x0013
        L_0x0080:
            float[] r6 = r11.values     // Catch:{ all -> 0x008b }
            java.lang.Object r0 = r6.clone()     // Catch:{ all -> 0x008b }
            float[] r0 = (float[]) r0     // Catch:{ all -> 0x008b }
            r10.mMagneticValues = r0     // Catch:{ all -> 0x008b }
            goto L_0x0023
        L_0x008b:
            r6 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x008b }
            throw r6
        L_0x008e:
            float[] r6 = r11.values     // Catch:{ all -> 0x008b }
            java.lang.Object r0 = r6.clone()     // Catch:{ all -> 0x008b }
            float[] r0 = (float[]) r0     // Catch:{ all -> 0x008b }
            r10.mGravityValues = r0     // Catch:{ all -> 0x008b }
            goto L_0x0023
        L_0x0099:
            float[] r6 = r11.values     // Catch:{ all -> 0x008b }
            java.lang.Object r0 = r6.clone()     // Catch:{ all -> 0x008b }
            float[] r0 = (float[]) r0     // Catch:{ all -> 0x008b }
            r10.mAccelerometerValues = r0     // Catch:{ all -> 0x008b }
            goto L_0x0023
        L_0x00a4:
            float[] r6 = r10.mMagneticValues     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x00dc
            float[] r6 = r10.mGravityValues     // Catch:{ all -> 0x008b }
            if (r6 == 0) goto L_0x00dc
            r6 = 9
            float[] r0 = new float[r6]     // Catch:{ all -> 0x008b }
            r6 = 0
            float[] r7 = r10.mGravityValues     // Catch:{ all -> 0x008b }
            float[] r8 = r10.mMagneticValues     // Catch:{ all -> 0x008b }
            android.hardware.SensorManager.getRotationMatrix(r0, r6, r7, r8)     // Catch:{ all -> 0x008b }
            r6 = 3
            float[] r5 = new float[r6]     // Catch:{ all -> 0x008b }
            android.hardware.SensorManager.getOrientation(r0, r5)     // Catch:{ all -> 0x008b }
            r6 = 0
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mAzimuth = r6     // Catch:{ all -> 0x008b }
            r6 = 1
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mPitch = r6     // Catch:{ all -> 0x008b }
            r6 = 2
            r6 = r5[r6]     // Catch:{ all -> 0x008b }
            r10.mRoll = r6     // Catch:{ all -> 0x008b }
            float r6 = r10.mAzimuth     // Catch:{ all -> 0x008b }
            r7 = 1127481344(0x43340000, float:180.0)
            float r6 = r6 * r7
            double r6 = (double) r6     // Catch:{ all -> 0x008b }
            r8 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
            double r6 = r6 / r8
            float r2 = (float) r6     // Catch:{ all -> 0x008b }
            goto L_0x0059
        L_0x00dc:
            monitor-exit(r10)     // Catch:{ all -> 0x008b }
            goto L_0x0013
        L_0x00df:
            android.hardware.Sensor r6 = r11.sensor
            int r6 = r6.getType()
            r7 = 3
            if (r6 != r7) goto L_0x005a
            float[] r6 = r10.mValues
            r7 = 0
            r2 = r6[r7]
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.crittermap.backcountrynavigator.nav.Navigator.onSensorChanged(android.hardware.SensorEvent):void");
    }

    public float getDeclination() {
        return this.declination;
    }

    public float getMapBearing() {
        Location loc = getCurrentLocation();
        if (loc == null || !loc.hasSpeed() || loc.getSpeed() <= THRESHSPEED || !loc.hasBearing()) {
            return getCompassBearing() + getDeclination();
        }
        return loc.getBearing();
    }

    public float getCompassBearing() {
        return this.compassBearing;
    }

    public void setCompassBearing(float compassBearing2) {
        this.compassBearing = compassBearing2;
    }

    public float getTrueBearing() {
        return this.compassBearing + this.declination;
    }

    /* access modifiers changed from: package-private */
    public float normalize(float bearingIn) {
        float bearingIn2 = (float) Math.IEEEremainder((double) bearingIn, 360.0d);
        if (bearingIn2 < 0.0f) {
            return (float) (((double) bearingIn2) + 360.0d);
        }
        return bearingIn2;
    }

    public float getGotoBearing() {
        return normalize(this.gotoBearing);
    }

    public float getGotoMagneticBearing() {
        return normalize(this.gotoBearing - this.declination);
    }

    public float getGotoDistance() {
        return this.gotoDistance;
    }

    public void onGpsStatusChanged(int event) {
        this.gpsStatus = this.mLocationManager.getGpsStatus(this.gpsStatus);
        switch (event) {
            case 1:
                setAcquiring(true);
                break;
            case 2:
                setAcquiring(false);
                break;
            case 3:
                this.mAcceptNetworkLocation = false;
                setAcquiring(false);
                break;
        }
        setChanged();
        notifyObservers(GPSSTATUS);
    }

    public void setAcquiring(boolean acquiring2) {
        this.acquiring = acquiring2;
    }

    public boolean isAcquiring() {
        return this.acquiring;
    }

    public GpsStatus getGpsStatus() {
        return this.gpsStatus;
    }

    public void onLocationChanged(Location loc) {
        if (loc.getProvider() == null || !loc.getProvider().equals("network") || this.mAcceptNetworkLocation) {
            setCurrentLocation(loc);
            if (this.startTime == null) {
                clear();
            }
            setChanged();
            notifyObservers(LOCATION);
        }
    }

    public void onProviderDisabled(String arg0) {
    }

    public void onProviderEnabled(String arg0) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public class NavStat {
        public String fact;
        public String label;

        public NavStat(String labelstring, String factstring) {
        }
    }

    public void clear() {
        this.distanceTravelled = 0.0d;
        this.altitudeClimbed = 0.0d;
        this.altitudeDescended = 0.0d;
        this.startTime = new Date();
        if (isStarted() && this.currentLocation != null) {
            setChanged();
            notifyObservers(LOCATION);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateStats() {
        if (this.currentLocation != null) {
        }
    }

    public void setGotoPos(Position pos, String destination) {
        this.mGotoPos = pos;
        this.mGotoDestination = destination;
        if (isStarted() && this.currentLocation != null) {
            setChanged();
            notifyObservers(LOCATION);
        }
    }

    public Position getGotoPos() {
        return this.mGotoPos;
    }

    public String getGotoDestination() {
        return this.mGotoDestination;
    }

    public void clearGotoPos() {
        this.mGotoPos = null;
        if (isStarted() && this.currentLocation != null) {
            setChanged();
            notifyObservers(LOCATION);
        }
    }

    public boolean hasGotoPos() {
        return this.mGotoPos != null;
    }

    public Position getRecentPosition() {
        Location loc = findRecentLocation();
        if (loc != null) {
            return new Position(loc.getLongitude(), loc.getLatitude());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void startTrackRecording() {
    }

    /* access modifiers changed from: package-private */
    public void stopTrackRecording() {
    }

    public void onRecordingMade() {
        this.numRecordings++;
    }

    public long getNumRecordings() {
        return this.numRecordings;
    }

    public boolean shouldRefresh() {
        if (this.trackRecordingService != null) {
            return this.trackRecordingService.shouldRefresh();
        }
        return false;
    }

    public boolean isMapBearingFromCompass() {
        Location loc = getCurrentLocation();
        if (loc == null || !loc.hasSpeed() || loc.getSpeed() <= THRESHSPEED || !loc.hasBearing()) {
            return true;
        }
        return false;
    }

    private void tryBindTrackRecordingService() {
        Log.d("Navigator", "Trying to bind to track recording service...");
        this.mContext.bindService(new Intent(this.mContext, TrackRecordingService.class), this.serviceConnection, 0);
    }

    private void tryUnbindTrackRecordingService() {
        Log.d("Navigator", "Trying to unbind from track recording service...");
        try {
            this.mContext.unbindService(this.serviceConnection);
        } catch (IllegalArgumentException e) {
            Log.d("Navigator", "MyTracks: Tried unbinding, but service was not registered.", e);
        }
    }

    public boolean isRecording() {
        if (this.trackRecordingService == null) {
            return false;
        }
        return this.trackRecordingService.isRecording();
    }

    public void startRecording(String path) {
        if (this.trackRecordingService == null) {
            this.startNewTrackRequested = true;
            this.startNewTrackIn = path;
            this.mContext.startService(new Intent(this.mContext, TrackRecordingService.class));
            tryBindTrackRecordingService();
            return;
        }
        this.trackRecordingService.startTrack(path);
        Toast.makeText(this.mContext, this.mContext.getString(R.string.status_now_recording), 0).show();
    }

    public void stopRecording() {
        if (this.trackRecordingService != null) {
            this.trackRecordingService.endCurrentTrack();
            Toast.makeText(this.mContext, (int) R.string.status_stopped_recording, 0).show();
        }
        tryUnbindTrackRecordingService();
        try {
            this.mContext.stopService(new Intent(this.mContext, TrackRecordingService.class));
        } catch (SecurityException e) {
            Log.e(MyTracksConstants.TAG, "Encountered a security exception when trying to stop service.", e);
        }
        this.trackRecordingService = null;
    }

    public FloatBuffer getRecentTrackPoints() {
        if (this.trackRecordingService == null) {
            return null;
        }
        if (this.trackRecordingService.locationList.size() < 2) {
            return null;
        }
        FloatBuffer fbuf = FloatBuffer.allocate(this.trackRecordingService.locationList.size() * 2);
        Iterator<Location> it = this.trackRecordingService.locationList.iterator();
        while (it.hasNext()) {
            Location loc = it.next();
            fbuf.put((float) loc.getLongitude());
            fbuf.put((float) loc.getLatitude());
        }
        return fbuf;
    }
}
