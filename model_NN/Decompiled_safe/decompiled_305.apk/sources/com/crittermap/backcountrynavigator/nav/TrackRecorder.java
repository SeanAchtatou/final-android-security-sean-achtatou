package com.crittermap.backcountrynavigator.nav;

import android.content.ContentValues;
import android.location.Location;
import android.util.Log;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.tracks.TrackOptimizer;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import org.xmlrpc.android.IXMLRPCSerializer;

public class TrackRecorder implements Observer {
    public static final int DEFAULT_ANNOUNCEMENT_FREQUENCY = -1;
    public static final int DEFAULT_MAX_RECORDING_DISTANCE = 200;
    public static final int DEFAULT_MIN_RECORDING_DISTANCE = 5;
    public static final int DEFAULT_MIN_RECORDING_INTERVAL = 6900;
    public static final int DEFAULT_MIN_REQUIRED_ACCURACY = 200;
    public static final int DEFAULT_SPLIT_FREQUENCY = 0;
    private static final String TAG = "TrackRecorder";
    private BCNMapDatabase bdb;
    private boolean isMoving;
    Location lastLocation = null;
    Location lastRecordedLocation = null;
    private long lastRowID;
    boolean mRecording = false;
    private int mTrackNumber;
    private int maxRecordingDistance = 200;
    private int minRecordingDistance = 5;
    private int minRecordingInterval = DEFAULT_MIN_RECORDING_INTERVAL;
    private int minRequiredAccuracy = 200;
    Navigator navigator;
    private TrackOptimizer optimizer;
    private long recordingTrackId = -1;
    private long rowid;

    private void startTrack() {
        ContentValues values = new ContentValues();
        values.put(IXMLRPCSerializer.TAG_NAME, new Date().toString());
        values.put("color", Integer.valueOf((int) MyTracksConstants.MENU_SEND_TO_GOOGLE));
        values.put("PathType", "track");
        this.rowid = this.bdb.getDb().insert(BCNMapDatabase.PATHS, IXMLRPCSerializer.TAG_NAME, values);
        this.optimizer = new TrackOptimizer(this.bdb, this.rowid);
        this.optimizer.setRecordingMode(5.0f);
        this.lastRowID = 0;
    }

    public void update(Observable observable, Object data) {
        if (this.mRecording && data.equals(Navigator.LOCATION)) {
            try {
                recordLocation(this.navigator.currentLocation);
            } catch (Exception e) {
                Log.w(TAG, e);
            }
        }
    }

    public boolean recordLocation(Location location) {
        if (location == null) {
            Log.w(TAG, "Location changed, but location is null.");
            return false;
        } else if (location.getAccuracy() > ((float) this.minRequiredAccuracy)) {
            return false;
        } else {
            long j = this.lastRowID;
            double distanceToLastRecorded = Double.POSITIVE_INFINITY;
            if (this.lastRecordedLocation != null) {
                distanceToLastRecorded = (double) location.distanceTo(this.lastRecordedLocation);
            }
            if (this.lastLocation != null) {
                double distanceToLast = (double) location.distanceTo(this.lastLocation);
            }
            long timetoLastRecorded = Long.MAX_VALUE;
            if (this.lastRecordedLocation != null) {
                timetoLastRecorded = location.getTime() - this.lastRecordedLocation.getTime();
            }
            boolean recorded = false;
            if (distanceToLastRecorded > ((double) this.minRecordingDistance) && timetoLastRecorded > ((long) this.minRecordingInterval)) {
                insertLocation(location);
                this.lastRecordedLocation = location;
                recorded = true;
            }
            this.lastLocation = location;
            return recorded;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean insertLocation(Location location) {
        ContentValues values = new ContentValues();
        float x = (float) location.getLongitude();
        float y = (float) location.getLatitude();
        this.optimizer.add(x, y);
        values.put("lon", Float.valueOf(x));
        values.put("lat", Float.valueOf(y));
        values.put("PathID", Long.valueOf(this.rowid));
        if (this.lastRowID != 0) {
            values.put("PredecessorID", Long.valueOf(this.lastRowID));
        }
        if (location.hasAltitude()) {
            values.put("ele", Double.valueOf(location.getAltitude()));
        }
        values.put("ttime", Long.valueOf(location.getTime()));
        this.lastRowID = this.bdb.getDb().insert(BCNMapDatabase.TRACKPOINTS, "lon", values);
        return true;
    }

    public void startRecording(Navigator nav, BCNMapDatabase db) {
        this.bdb = db;
        this.navigator = nav;
        startTrack();
        this.navigator.addObserver(this);
        this.mRecording = true;
    }

    public void startRecording(BCNMapDatabase db) {
        this.bdb = db;
        startTrack();
        this.mRecording = true;
    }

    public void stopRecording() {
        this.mRecording = false;
        this.optimizer.done();
        this.bdb = null;
        this.navigator = null;
    }

    public static boolean isValidLocation(Location location) {
        return location != null && Math.abs(location.getLatitude()) <= 90.0d && Math.abs(location.getLongitude()) <= 180.0d;
    }
}
