package com.crittermap.backcountrynavigator.nav;

public abstract class UnitConversions {
    public static final double KMH_TO_MPH = 0.6213711931818182d;
    public static final double KM_TO_MI = 0.621371192d;
    public static final double MI_TO_FEET = 5280.0d;
    public static final double MI_TO_M = 1609.344d;
    public static final double M_TO_FT = 3.2808399d;
    public static final double TO_RADIANS = 0.017453292519943295d;

    protected UnitConversions() {
    }
}
