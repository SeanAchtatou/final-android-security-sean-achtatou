package com.crittermap.backcountrynavigator.nav;

public class Position {
    public double lat;
    public double lon;

    public Position(double lon2, double lat2) {
        this.lon = lon2;
        this.lat = lat2;
    }
}
