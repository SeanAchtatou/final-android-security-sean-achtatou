package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.crittermap.analytics.TrackerFactory;
import com.crittermap.backcountrynavigator.LocationFindTask;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.data.CleanupService;
import com.crittermap.backcountrynavigator.data.ExportTask;
import com.crittermap.backcountrynavigator.data.TileRepository;
import com.crittermap.backcountrynavigator.dialog.MapControlPanelDialog;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.map.MapServerResourceFactory;
import com.crittermap.backcountrynavigator.map.MobileAtlasServer;
import com.crittermap.backcountrynavigator.map.view.BCNMapView;
import com.crittermap.backcountrynavigator.map.view.MapRenderer;
import com.crittermap.backcountrynavigator.nav.NavStatUpdater;
import com.crittermap.backcountrynavigator.nav.Navigator;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.nav.ShakeListener;
import com.crittermap.backcountrynavigator.places.PlaceAddress;
import com.crittermap.backcountrynavigator.settings.BCNSettings;
import com.crittermap.backcountrynavigator.settings.Versioning;
import com.crittermap.backcountrynavigator.tile.CoordinateBoundingBox;
import com.crittermap.backcountrynavigator.tracks.TracksList;
import com.crittermap.backcountrynavigator.view.CompassView;
import com.crittermap.backcountrynavigator.view.PointPopup;
import com.crittermap.backcountrynavigator.view.ToggleImageButton;
import com.crittermap.backcountrynavigator.waypoint.WaypointActivity;
import com.crittermap.backcountrynavigator.waypoint.WaypointFilterQueryProvider;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class BackCountryActivity extends Activity implements BCNMapView.LabelListener, SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener, Observer, AdWhirlLayout.AdWhirlInterface {
    static final String ACTION_DOWNLOAD_BEGIN = "com.crittermap.backcountrynavigator.startdownloads";
    public static final String CHECK_LICENSE = (String.valueOf(BackCountryActivity.class.getPackage().getName()) + ".Check_License");
    static final String DEFAULTDBNAME = "default";
    public static final int DOWNLOAD_ACTIVITY_RESULT = 32;
    static final String LICENSE_PACKAGE = "com.crittermap.backcountrynavigator.license";
    public static final String OPEN_DATABASE = (String.valueOf(BackCountryActivity.class.getPackage().getName()) + ".Open_Database");
    public static final String OPEN_LOCATION = (String.valueOf(BackCountryActivity.class.getPackage().getName()) + ".Open_Location");
    public static final String PREFS_NAME = "com.critermap.backcountrynavigator.MapPreferences";
    private static final int REQUEST_CODE_PREFERENCES = 57;
    private static final String TRANSFER_PREFERENCES = "com.crittermap.backcountrynavigator.transfer";
    private static boolean mAlreadyShowedHelp = false;
    private static final Class<?>[] mInvalidateMenuSignature = new Class[0];
    View bPage;
    ImageButton bZoomIn;
    ImageButton bZoomOut;
    private Button b_goto;
    Button b_stop_goto;
    private Button b_waypoint_delete;
    private ImageButton b_waypoint_symbol;
    /* access modifiers changed from: private */
    public BCNMapDatabase bdb;
    BCNMapView bview;
    private int contextPosition = -1;
    CompassView cview;
    ErrorReporter errorReporter;
    MapServerResourceFactory factory;
    ViewFlipper flipper;
    ViewGroup goto_panel;
    /* access modifiers changed from: private */
    public ViewGroup lStorageWarning;
    ImageButton mButtonUnlock;
    boolean mExternalStorageAvailable = false;
    BroadcastReceiver mExternalStorageReceiver;
    boolean mExternalStorageWriteable = false;
    private boolean mFullScreen;
    LayersChoices mLayersChoices = new LayersChoices();
    View mListPageView;
    private Menu mMenu;
    private NavStatUpdater mNavStatUpdater;
    PointPopup mPointPopup = null;
    private ShakeListener mShaker;
    private boolean mShowAtlas;
    private WaypointSymbolFactory mSymbolFactory;
    private ListView mWaypointList;
    WaypointFilterQueryProvider mWaypointQueryprovider;
    private ViewGroup mapPanel;
    Navigator navigator;
    TileRepository repository = new TileRepository();
    private boolean showingExpiredMessage = false;
    /* access modifiers changed from: private */
    public boolean showingFirstChoose;
    private TextView tCopyright;
    private ImageView tIcon;
    /* access modifiers changed from: private */
    public TextView tLevel;
    ImageButton tb_;
    ImageButton tb_burn;
    ImageButton tb_help;
    ImageButton tb_mark;
    ImageButton tb_recenter;
    ImageButton tb_record;
    ToggleImageButton tb_sculpt;
    View tb_sculpt_clear;
    private View tb_sculpt_recyle;
    private TextView textGotoDestination;
    GoogleAnalyticsTracker tracker;
    View vToolBarOpen;
    private long waypointId = -1;

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return new AlertDialog.Builder(this).setIcon((int) R.drawable.mn_layers).setTitle((int) R.string.d_first_choose_map_source).setMessage((int) R.string.d_start_preview).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                BackCountryActivity.this.showMapChoices();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface paramDialogInterface) {
                BackCountryActivity.this.showingFirstChoose = false;
                Log.w("NoMapMessage", "Cancel");
            }
        }).create();
    }

    public Object onRetainNonConfigurationInstance() {
        return super.onRetainNonConfigurationInstance();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.mMenu = menu;
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    public boolean onContextItemSelected(MenuItem item) {
        long id = this.waypointId;
        if (this.bdb == null) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.itemListCtxEdit:
                setWayPointView(id);
                break;
            case R.id.itemListCtxCenter:
                Cursor c = this.bdb.getWayPoint(id);
                if (c.getCount() > 0) {
                    c.moveToFirst();
                    this.bview.setCenter(new Position(c.getDouble(c.getColumnIndex("Longitude")), c.getDouble(c.getColumnIndex("Latitude"))));
                    this.flipper.setDisplayedChild(0);
                }
                c.close();
                break;
            case R.id.itemListCtxGoto:
                Cursor c2 = this.bdb.getWayPoint(id);
                if (c2.getCount() > 0) {
                    c2.moveToFirst();
                    double lon2 = c2.getDouble(c2.getColumnIndex("Longitude"));
                    double lat2 = c2.getDouble(c2.getColumnIndex("Latitude"));
                    startGoto(c2.getString(c2.getColumnIndex("Name")), new Position(lon2, lat2));
                }
                c2.close();
                break;
            case R.id.itemListCtxDelete:
                if (!(id == -1 || this.bdb == null)) {
                    this.bdb.deleteWayPoint(id);
                    this.bview.refresh();
                    this.mWaypointQueryprovider.refresh();
                    break;
                }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void setWayPointView(long id) {
        if (this.bdb != null) {
            Intent wi = new Intent(this, WaypointActivity.class);
            wi.putExtra("DBFileName", this.bdb.getFileName());
            wi.putExtra("WaypointId", id);
            startActivityForResult(wi, WaypointActivity.REQUEST_WAYPOINT);
        }
    }

    private void createWaypointView() {
    }

    private void deleteWaypoint(long wid) {
        if (wid != -1 && this.bdb != null) {
            this.bdb.deleteWayPoint(wid);
            this.bview.refresh();
            this.flipper.setDisplayedChild(0);
        }
    }

    /* access modifiers changed from: protected */
    public void startGoto(String destination, Position pos) {
        this.navigator.setGotoPos(pos, destination);
        this.textGotoDestination.setText(destination);
        this.goto_panel.setVisibility(0);
        this.flipper.setDisplayedChild(2);
        if (!this.navigator.isStarted()) {
            startGps();
        }
    }

    /* access modifiers changed from: protected */
    public void stopGoto() {
        this.navigator.clearGotoPos();
        this.goto_panel.setVisibility(8);
        this.bview.refresh();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        this.contextPosition = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        this.waypointId = this.mWaypointList.getAdapter().getItemId(this.contextPosition);
        getMenuInflater().inflate(R.menu.listcontextmenu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.find:
                this.tracker.trackPageView("/options-menu/find_place");
                onSearchRequested();
                return true;
            case R.id.show_submenu:
                Intent intent = new Intent(this, MapControlPanelDialog.class);
                intent.putExtra("offline", this.bview.getRenderer().isOffline());
                startActivityForResult(intent, R.id.activity_request_map_control_panel);
                return true;
            case R.id.file:
            case R.id.menu_help:
            default:
                return true;
            case R.id.file_create_new:
                this.tracker.trackPageView("/options-menu/file/trip_create");
                showTripCreate();
                dirtyOptionsMenu();
                return true;
            case R.id.file_load_existing:
                this.tracker.trackPageView("/options-menu/file/trip_load");
                String[] bcnavfiles = BCNMapDatabase.findExisting();
                final String[] strArr = bcnavfiles;
                new AlertDialog.Builder(this).setIcon(17301582).setTitle(BCNMapDatabase.SD_CARD_PATH()).setSingleChoiceItems(bcnavfiles, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (BackCountryActivity.this.isRecording()) {
                            BackCountryActivity.this.startStopRecording();
                        }
                        BackCountryActivity.this.bdb = BCNMapDatabase.openTrip(strArr[whichButton]);
                        BackCountryActivity.this.setAsDatabase(BackCountryActivity.this.bdb);
                        dialog.dismiss();
                        BackCountryActivity.this.bview.refresh();
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create().show();
                dirtyOptionsMenu();
                return true;
            case R.id.file_show_tracks:
                this.tracker.trackPageView("/options-menu/file/show_tracks");
                Intent intent2 = new Intent(this, TracksList.class);
                intent2.putExtra("DBFileName", this.bdb.getFileName());
                startActivity(intent2);
                return true;
            case R.id.file_close:
                this.tracker.trackPageView("/options-menu/file/trip_close");
                if (this.bdb == null) {
                    return true;
                }
                if (isRecording()) {
                    startStopRecording();
                }
                this.bdb = null;
                this.mWaypointQueryprovider.setDatabase(this.bdb);
                this.bview.getRenderer().setBdb(null);
                this.bview.refresh();
                dirtyOptionsMenu();
                return true;
            case R.id.file_import:
                this.tracker.trackPageView("/options-menu/file/import");
                startActivity(new Intent(this, AndroidExplorer.class));
                return true;
            case R.id.file_export:
                this.tracker.trackPageView("/options-menu/file/export");
                showGpxExport();
                return true;
            case R.id.settings:
                this.tracker.trackPageView("/options-menu/settings");
                startActivityForResult(new Intent().setClass(this, SettingsPreferenceActivity.class), REQUEST_CODE_PREFERENCES);
                return true;
            case R.id.menu_help_online:
                this.tracker.trackPageView("/options-menu/help/screen");
                startActivity(new Intent(this, WelcomeActivity.class));
                return true;
            case R.id.menu_help_desk:
                this.tracker.trackPageView("/options-menu/help/desk");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://crittermap.zendesk.com/?utm_source=Android&utm_medium=App&utm_campaign=Help")));
                return true;
            case R.id.menu_help_videos:
                this.tracker.trackPageView("/options-menu/help/videos");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.youtube.com/user/crittermap?utm_source=Android&utm_medium=App&utm_campaign=Help")));
                return true;
            case R.id.menu_share:
                this.tracker.trackPageView("/options-menu/help");
                Intent intent3 = new Intent("android.intent.action.SEND");
                intent3.putExtra("android.intent.extra.SUBJECT", "BackCountry Navigator");
                intent3.setType("text/plain");
                intent3.putExtra("android.intent.extra.TEXT", "I'm trying this Android app with topo maps and gps -- http://bit.ly/95A94x");
                startActivity(intent3);
                return true;
            case R.id.menu_feedback:
                this.tracker.trackPageView("/options-menu/feedback");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.surveymonkey.com/s/BackCountryNavigatorAndroid")));
                return true;
            case R.id.menu_choose_trailmaps:
                if (Versioning.getSDKNumber() <= 3) {
                    return true;
                }
                try {
                    Class<?> cls = Class.forName("com.urbanairship.storefront.InventoryListActivity");
                    Intent trint = new Intent();
                    trint.setClass(getBaseContext(), cls);
                    startActivity(trint);
                    return true;
                } catch (ClassNotFoundException e) {
                    Log.i("BackCountryActivity", "TRail Maps Inventory List Activity not present");
                    return true;
                } catch (Exception e2) {
                    Log.e("BackCountryActivity", "Starting AddOn List", e2);
                    return true;
                }
        }
    }

    private void dirtyOptionsMenu() {
        if (Versioning.getSDKNumber() >= 11) {
            try {
                getClass().getMethod("invalidateOptionsMenu", mInvalidateMenuSignature).invoke(this, new Object[0]);
            } catch (IllegalArgumentException e) {
                Log.e("invalidateOptionsMenu", "Argument problem", e);
            } catch (IllegalAccessException | NoSuchMethodException | SecurityException e2) {
            } catch (InvocationTargetException e3) {
                Log.e("invalidateOptionsMenu", "Invocation problem", e3);
            }
        }
    }

    private void setFullScreen(boolean bFullScreen) {
        if (Versioning.getSDKNumber() >= 11) {
            return;
        }
        if (bFullScreen) {
            if (this.tracker != null) {
                this.tracker.trackPageView("/options-menu/fullscreen");
            }
            WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags |= 1024;
            getWindow().setAttributes(attrs);
            return;
        }
        if (this.tracker != null) {
            this.tracker.trackPageView("/options-menu/notfullscreen");
        }
        WindowManager.LayoutParams attribs = getWindow().getAttributes();
        attribs.flags &= -1025;
        getWindow().setAttributes(attribs);
    }

    private void setMapUrn(String displayed, String chosen) {
        if (chosen.contains("atlas:")) {
            setMobileAtlasLayer(Uri.parse(chosen));
        } else if (this.bview.getRenderer().isOffline()) {
            setMapOfflineLayer(displayed);
        } else {
            setMapPreviewID(displayed);
        }
    }

    private void showOfflineChoices() {
        String[] strArr = {"MyTopo.com", "USGS Color Aerials", "Open Street Map - MAPNIK", "NR Canada Topo Maps", "Italy PCN Topo Maps", "Spain IGN Topo Maps", "Australia MapConnect", "NASA Landsat", "USGS Topo Maps", "USGS BW Aerial Photo"};
        List<MapServer> possibleservers = this.factory.getServerList();
        ArrayList<String> layers = new ArrayList<>();
        for (MapServer ser : possibleservers) {
            if (this.repository.layerExists(ser.getShortName())) {
                layers.add(ser.getDisplayName());
            }
        }
        final String[] oservers = (String[]) layers.toArray(new String[layers.size()]);
        new AlertDialog.Builder(this).setIcon(17301597).setTitle((int) R.string.m_show_offline).setSingleChoiceItems(oservers, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                BackCountryActivity.this.setMapOfflineLayer(oservers[whichButton]);
                dialog.dismiss();
            }
        }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void showTripCreate() {
        final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.new_file_entry, (ViewGroup) null);
        new AlertDialog.Builder(this).setIcon(17301582).setTitle((int) R.string.d_choose_existing).setView(textEntryView).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                BackCountryActivity.this.bdb = BCNMapDatabase.newTrip(((EditText) textEntryView.findViewById(R.id.filename_edit)).getText().toString());
                BackCountryActivity.this.setAsDatabase(BackCountryActivity.this.bdb);
                BackCountryActivity.this.bview.refresh();
            }
        }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void showGpxExport() {
        View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.export_file_entry, (ViewGroup) null);
        final EditText eview = (EditText) textEntryView.findViewById(R.id.filename_edit);
        new AlertDialog.Builder(this).setIcon(17301582).setTitle((int) R.string.d_choose_existing).setView(textEntryView).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String filename = eview.getText().toString();
                new ExportTask(new ExportTask.ExportListener() {
                    public void exportFinished(Boolean success) {
                        if (success.booleanValue()) {
                            Toast toast = Toast.makeText(BackCountryActivity.this, (int) R.string.t_export_sucess, 1);
                            toast.setGravity(17, 0, 20);
                            toast.show();
                            return;
                        }
                        Toast toast2 = Toast.makeText(BackCountryActivity.this, (int) R.string.t_export_failure, 1);
                        toast2.setGravity(17, 0, 20);
                        toast2.show();
                    }
                }, BackCountryActivity.this.bdb).execute(filename);
                dialog.dismiss();
            }
        }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void showMapChoices() {
        startActivity(new Intent(this, MapSourceChooserActivity.class));
    }

    /* access modifiers changed from: package-private */
    public void setAsDatabase(BCNMapDatabase db) {
        CoordinateBoundingBox box;
        this.bview.getRenderer().setBdb(db);
        if (!(db == null || (box = this.bdb.findBounds()) == null)) {
            this.bview.setCenterAndZoom(box.getCenter(), box.getZoomLevel());
        }
        this.mWaypointQueryprovider.setDatabase(db);
    }

    private void startDownload() {
        if (this.bview.getMarkedRectangles() == null || this.bview.getMarkedRectangles().size() <= 0) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.sculpt).setTitle((int) R.string.d_title_offline_download).setMessage((int) R.string.d_offline_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        }
        DownloadActivity.center = this.bview.getCenter();
        DownloadActivity.rects = this.bview.getMarkedRectangles();
        DownloadActivity.zoomLevel = this.bview.getLevel();
        DownloadActivity.layerName = this.bview.getRenderer().getLayerName();
        this.bview.setSculpting(false);
        this.tb_sculpt.setChecked(false);
        Intent intent = new Intent(this, DownloadActivity.class);
        intent.setAction(ACTION_DOWNLOAD_BEGIN);
        startActivityForResult(intent, 32);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean currentPreview;
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 32 && resultCode == -1 && data != null && data.hasExtra("layer")) {
            String layerString = data.getStringExtra("layer");
            this.tracker.trackPageView("/download/" + layerString);
            this.tracker.dispatch();
            Toast toast = Toast.makeText(this, getResources().getString(R.string.t_download_started, layerString), 1);
            toast.setGravity(17, 0, 20);
            toast.show();
        }
        if (requestCode == 2000) {
            switch (resultCode) {
                case R.id.itemListCtxCenter:
                    this.bview.setCenter(new Position(data.getDoubleExtra("Longitude", this.bview.getCenter().lon), data.getDoubleExtra("Latitude", this.bview.getCenter().lat)));
                    this.bview.refresh();
                    break;
                case R.id.itemListCtxGoto:
                    startGoto(data.getStringExtra("Destination"), new Position(data.getDoubleExtra("Longitude", this.bview.getCenter().lon), data.getDoubleExtra("Latitude", this.bview.getCenter().lat)));
                    this.bview.refresh();
                    break;
                case R.id.itemListCtxDelete:
                    this.bview.refresh();
                    break;
                case R.id.itemListCtxSave:
                    this.bview.refresh();
                    break;
            }
            this.mWaypointQueryprovider.refresh();
        }
        if (requestCode == R.layout.screen_control) {
            advanceView(resultCode);
        }
        if (requestCode == R.id.activity_request_toolbar) {
            switch (resultCode) {
                case R.id.tb_select_for_download:
                    this.mapPanel.setVisibility(0);
                    this.bview.setSculpting(true);
                    this.tb_sculpt.setChecked(true);
                    Toast.makeText(this, R.string.t_select_rectangles, 1).show();
                    break;
                case R.id.tbn_recenter:
                    if (this.navigator.isStarted()) {
                        this.bview.setFollowing(true);
                        this.navigator.start();
                        break;
                    } else {
                        startGps();
                        break;
                    }
                case R.id.tbn_record_track:
                    startStopRecording();
                    break;
                case R.id.tbn_gps_off:
                    startStopGps();
                    break;
                case R.id.tb_map_layers:
                    Intent intent = new Intent(this, MapControlPanelDialog.class);
                    intent.putExtra("offline", this.bview.getRenderer().isOffline());
                    startActivityForResult(intent, R.id.activity_request_map_control_panel);
                    break;
            }
        }
        if (requestCode == R.id.activity_request_mark) {
            switch (resultCode) {
                case R.id.activity_result_mark_new:
                    if (!this.bview.isFollowing()) {
                        Position pos = this.bview.getCenter();
                        setWayPointView(this.bdb.newWayPoint(pos.lon, pos.lat));
                        break;
                    } else {
                        Location loc = this.navigator.getCurrentLocation();
                        if (loc != null) {
                            if (!loc.hasAltitude()) {
                                setWayPointView(this.bdb.newWayPoint(loc.getLongitude(), loc.getLatitude()));
                                break;
                            } else {
                                setWayPointView(this.bdb.newWayPoint(loc.getLongitude(), loc.getLatitude(), loc.getAltitude()));
                                break;
                            }
                        }
                    }
                    break;
                case R.id.activity_result_view_existing:
                    startActivityForResult(data, WaypointActivity.REQUEST_WAYPOINT);
                    break;
            }
        }
        if (requestCode == R.id.activity_request_map_control_panel && resultCode == R.id.map_cp_result) {
            if (this.bview.getRenderer().isOffline()) {
                currentPreview = false;
            } else {
                currentPreview = true;
            }
            if (data.getBooleanExtra("Preview", currentPreview) != currentPreview) {
                this.tracker.trackPageView("/options-menu/show_offline_only");
                String layerName = this.bview.getRenderer().getLayerName();
                if (layerName != null) {
                    if (this.bview.getRenderer().isOffline()) {
                        setMapPreviewID(layerName);
                    } else {
                        setMapOfflineLayer(layerName);
                    }
                }
            }
            int mi = data.getIntExtra("MapIndex", 0);
            if (mi != 0) {
                setMapUrn(this.mLayersChoices.getChoiceLabels().get(mi), this.mLayersChoices.getChosen(mi));
            }
            switch (data.getIntExtra("NextActivity", 0)) {
                case R.id.cp_button_choose_map:
                    if (!this.bview.getRenderer().isOffline()) {
                        this.tracker.trackPageView("/options-menu/show_maps_preview_choices");
                        showMapChoices();
                        break;
                    } else {
                        this.tracker.trackPageView("/options-menu/show_maps_offline_choices");
                        showMapChoices();
                        break;
                    }
                case R.id.cp_button_choose_atlas:
                    startActivity(new Intent(this, SelectMobileAtlasActivity.class));
                    break;
                case R.id.tb_select_for_download:
                    this.mapPanel.setVisibility(0);
                    this.bview.setSculpting(true);
                    this.tb_sculpt.setChecked(true);
                    Toast.makeText(this, R.string.t_select_rectangles, 1).show();
                    break;
            }
        }
        if (requestCode == R.id.activity_request_dashboard && resultCode == R.id.dashboard_result) {
            switch (data.getIntExtra("NextActivity", 0)) {
                case R.id.currently_opened_button:
                    Toast.makeText(this, "You pressed the dasbhoard currently opened button!", 1).show();
                    this.tracker.trackPageView("/options-menu/file/trip_load");
                    String[] bcnavfiles = BCNMapDatabase.findExisting();
                    final String[] strArr = bcnavfiles;
                    new AlertDialog.Builder(this).setIcon(17301582).setTitle(BCNMapDatabase.SD_CARD_PATH()).setSingleChoiceItems(bcnavfiles, 0, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (BackCountryActivity.this.isRecording()) {
                                BackCountryActivity.this.startStopRecording();
                            }
                            BackCountryActivity.this.bdb = BCNMapDatabase.openTrip(strArr[whichButton]);
                            BackCountryActivity.this.setAsDatabase(BackCountryActivity.this.bdb);
                            dialog.dismiss();
                            BackCountryActivity.this.bview.refresh();
                        }
                    }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).create().show();
                    dirtyOptionsMenu();
                    return;
                case R.id.closed_button:
                    Toast.makeText(this, "You preseed the dashboard closed button", 1).show();
                    this.tracker.trackPageView("/options-menu/file/trip_close");
                    if (this.bdb != null) {
                        if (isRecording()) {
                            startStopRecording();
                        }
                        this.bdb = null;
                        this.mWaypointQueryprovider.setDatabase(this.bdb);
                        this.bview.getRenderer().setBdb(null);
                        this.bview.refresh();
                        dirtyOptionsMenu();
                        return;
                    }
                    return;
                case R.id.show_tracks_button:
                    Toast.makeText(this, "You pressed the show tracks button!", 1).show();
                    this.tracker.trackPageView("/options-menu/file/show_tracks");
                    Intent intent2 = new Intent(this, TracksList.class);
                    intent2.putExtra("DBFileName", this.bdb.getFileName());
                    startActivity(intent2);
                    return;
                case R.id.show_waypoint_button:
                    Toast.makeText(this, "You pressed the dashboard show waypoint button!", 1).show();
                    return;
                case R.id.export_gpx_button:
                    Toast.makeText(this, "You pressed the dashboard export gpx button!", 1).show();
                    this.tracker.trackPageView("/options-menu/file/export");
                    showGpxExport();
                    return;
                case R.id.import_gpx_button:
                    Toast.makeText(this, "You pressed the dashboard import gpx button!", 1).show();
                    this.tracker.trackPageView("/options-menu/file/import");
                    startActivity(new Intent(this, AndroidExplorer.class));
                    return;
                case R.id.diff_trip_button:
                    Toast.makeText(this, "You pressed the dashboard diff trip button!", 1).show();
                    return;
                case R.id.start_new_button:
                    Toast.makeText(this, "You pressed the dashboard start new button!", 1).show();
                    this.tracker.trackPageView("/options-menu/file/trip_create");
                    showTripCreate();
                    dirtyOptionsMenu();
                    return;
                default:
                    return;
            }
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem mClose = menu.findItem(R.id.file_close);
        MenuItem mExport = menu.findItem(R.id.file_export);
        MenuItem mShowTracks = menu.findItem(R.id.file_show_tracks);
        if (this.bdb == null) {
            mClose.setVisible(false);
            mExport.setVisible(false);
            mShowTracks.setVisible(false);
        } else {
            mClose.setVisible(true);
            mExport.setVisible(true);
            mShowTracks.setVisible(true);
            mClose.setTitle(getString(R.string.m_file_close_name, new Object[]{this.bdb.getName()}));
            mShowTracks.setTitle(getString(R.string.m_file_tracks, new Object[]{this.bdb.getName()}));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar now = Calendar.getInstance();
        String format = String.format("%d-%d-%d-create", Integer.valueOf(now.get(5)), Integer.valueOf(now.get(11)), Integer.valueOf(now.get(12)));
        if (Versioning.getSDKNumber() < 11) {
            requestWindowFeature(7);
        }
        setContentView((int) R.layout.main);
        if (Versioning.getSDKNumber() < 11) {
            getWindow().setFeatureInt(7, R.layout.custom_title_1);
            getWindow().setFormat(1);
        }
        this.tracker = TrackerFactory.getTracker(this);
        this.errorReporter = new ErrorReporter();
        this.errorReporter.Init(this);
        this.errorReporter.CheckErrorAndSendMail(this);
        this.flipper = (ViewFlipper) findViewById(R.id.tabcontent);
        this.flipper.setDisplayedChild(0);
        this.bview = (BCNMapView) findViewById(R.id.view_map);
        this.bview.setLabelListener(this);
        this.cview = (CompassView) findViewById(R.id.view_compass);
        this.factory = new MapServerResourceFactory(this);
        this.navigator = new Navigator(this);
        this.navigator.addObserver(this.bview);
        this.navigator.addObserver(this.cview);
        if (Versioning.getSDKNumber() < 11) {
            this.tCopyright = (TextView) findViewById(R.id.title_bar_layer);
            this.tCopyright.setClickable(true);
            this.tCopyright.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    BackCountryActivity.this.showCopyright();
                }
            });
        }
        this.lStorageWarning = (ViewGroup) findViewById(R.id.warning_storage);
        this.mapPanel = (ViewGroup) findViewById(R.id.toolbar_map);
        this.textGotoDestination = (TextView) findViewById(R.id.t_goto_destination);
        this.goto_panel = (ViewGroup) findViewById(R.id.goto_panel);
        if (Versioning.getSDKNumber() < 11) {
            this.tLevel = (TextView) findViewById(R.id.title_bar_level);
        } else {
            this.tLevel = (TextView) findViewById(R.id.zoom_level_indicator);
            this.tLevel.setVisibility(0);
        }
        this.bview.setTvLevel(this.tLevel);
        this.tLevel.setText(String.valueOf(this.bview.getLevel()));
        this.tIcon = (ImageView) findViewById(R.id.title_bar_icon);
        this.tb_sculpt = (ToggleImageButton) findViewById(R.id.tb_sculpt);
        this.tb_sculpt.setOnClickListener(this);
        this.tb_sculpt_clear = findViewById(R.id.tb_sculpt_clear);
        this.tb_sculpt_clear.setOnClickListener(this);
        this.tb_burn = (ImageButton) findViewById(R.id.tb_burn);
        this.tb_burn.setOnClickListener(this);
        this.tb_sculpt_recyle = findViewById(R.id.tb_clean);
        this.tb_sculpt_recyle.setOnClickListener(this);
        this.mButtonUnlock = (ImageButton) findViewById(R.id.button_unlock);
        this.mButtonUnlock.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                BackCountryActivity.this.showUnlockMessage(false);
            }
        });
        this.tb_mark = (ImageButton) findViewById(R.id.tb_mark);
        this.tb_mark.setOnClickListener(this);
        this.b_stop_goto = (Button) findViewById(R.id.button_stop_goto);
        this.b_stop_goto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                BackCountryActivity.this.stopGoto();
            }
        });
        this.bZoomIn = (ImageButton) findViewById(R.id.zoom_in);
        this.bZoomOut = (ImageButton) findViewById(R.id.zoom_out);
        this.bZoomIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                BackCountryActivity.this.tLevel.setText(String.valueOf(BackCountryActivity.this.bview.zoomIn(false)));
            }
        });
        this.bZoomOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                BackCountryActivity.this.tLevel.setText(String.valueOf(BackCountryActivity.this.bview.zoomOut(false)));
            }
        });
        this.bZoomIn.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                BackCountryActivity.this.tLevel.setText(String.valueOf(BackCountryActivity.this.bview.zoomIn(true)));
                return true;
            }
        });
        this.bZoomOut.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                BackCountryActivity.this.tLevel.setText(String.valueOf(BackCountryActivity.this.bview.zoomOut(true)));
                return true;
            }
        });
        this.bPage = findViewById(R.id.button_page_switch);
        this.bPage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                BackCountryActivity.this.startActivityForResult(new Intent(BackCountryActivity.this, ScreenChooserActivity.class), R.layout.screen_control);
                Class<Activity> cls = Activity.class;
                try {
                    cls.getMethod("overridePendingTransition", Integer.TYPE, Integer.TYPE).invoke(BackCountryActivity.this, Integer.valueOf((int) R.anim.zoom_enter), 0);
                } catch (Exception e) {
                }
            }
        });
        this.vToolBarOpen = findViewById(R.id.button_toolbar_pull);
        this.vToolBarOpen.setOnClickListener(this);
        findViewById(R.id.warning_storage).setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                BackCountryActivity.this.updateExternalStorageState();
            }
        });
        this.mNavStatUpdater = new NavStatUpdater(this, this.navigator, (ViewGroup) findViewById(R.id.view_stat));
        createWaypointView();
        this.mSymbolFactory = new WaypointSymbolFactory(this);
        this.bview.getRenderer().setSymFactory(this.mSymbolFactory);
        this.mListPageView = findViewById(R.id.waypointslist_root);
        this.mWaypointQueryprovider = new WaypointFilterQueryProvider(this, this.mListPageView, this.navigator, this.mSymbolFactory);
        this.mWaypointList = (ListView) findViewById(R.id.waypoint_list);
        registerForContextMenu(this.mWaypointList);
        setPreferences();
        Intent intent = getIntent();
        String action = intent.getAction();
        if ("android.intent.action.SEARCH".equals(action)) {
            doSearchWithIntent(intent);
        } else if (OPEN_LOCATION.equals(action)) {
            setViewWithIntent(intent);
        } else if (OPEN_DATABASE.equals(action)) {
            setViewWithIntent(intent);
        } else if ("android.intent.action.VIEW".equals(action)) {
            centerWithIntent(intent);
        } else {
            setViewWithPreferences();
        }
        verifyLicense();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        new Date();
        if (sharedPref.getBoolean("show_help_startup_preference", true) && !mAlreadyShowedHelp && "android.intent.action.MAIN".equals(getIntent().getAction())) {
            startActivityForResult(new Intent(this, WelcomeActivity.class), R.id.welcome_request);
            mAlreadyShowedHelp = true;
        }
    }

    /* access modifiers changed from: protected */
    public void startHelp() {
        new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?q=12989%20NW%20SKYLINE.@45.6168,-122.8727"));
        startActivity(new Intent(this, WelcomeActivity.class));
    }

    /* access modifiers changed from: package-private */
    public boolean isRecording() {
        return this.navigator.isRecording();
    }

    /* access modifiers changed from: protected */
    public void startStopRecording() {
        if (!isRecording() && this.bdb != null) {
            if (!this.navigator.isStarted()) {
                startGps();
            }
            if (this.navigator.isStarted()) {
                this.navigator.startRecording(this.bdb.getFileName());
            }
        } else if (isRecording() && this.bdb != null) {
            this.navigator.stopRecording();
            this.bview.refresh();
        } else if (!isRecording() && this.bdb == null) {
            if (!this.navigator.isStarted()) {
                startGps();
            }
            if (this.navigator.isStarted()) {
                this.bdb = BCNMapDatabase.newOrExistingTrip(DEFAULTDBNAME);
                if (this.bdb != null) {
                    this.bview.getRenderer().setBdb(this.bdb);
                    this.mWaypointQueryprovider.setDatabase(this.bdb);
                    this.navigator.startRecording(this.bdb.getFileName());
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showUnlockMessage(final boolean closeAfter) {
        this.tracker.trackPageView("/button/unlock");
        AlertDialog dialog = new AlertDialog.Builder(this).setTitle((int) R.string.d_unlock_title).setMessage((int) R.string.d_unlock_message).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BackCountryActivity.this.tracker.trackPageView("/button/unlock/ok");
                dialog.dismiss();
                try {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("market://details?id=com.crittermap.backcountrynavigator.license&referrer=utm_source%3Dapp%26utm_medium%3Dinapplink%26utm_campaign%3Dfromdemo"));
                    BackCountryActivity.this.startActivity(intent);
                } catch (Exception e) {
                    BackCountryActivity.this.tracker.trackPageView("/unlock/nomarket");
                }
            }
        }).setNeutralButton((int) R.string.alert_dialog_alternate, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BackCountryActivity.this.tracker.trackPageView("/button/unlock/alternate");
                dialog.dismiss();
                try {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse("http://andappstore.com/AndroidApplications/apps/BackCountry_Navigator_LICENSE"));
                    BackCountryActivity.this.startActivity(intent);
                } catch (Exception e) {
                    BackCountryActivity.this.tracker.trackPageView("/unlock/noalternate");
                }
            }
        }).create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface arg0) {
                if (closeAfter) {
                    BackCountryActivity.this.finish();
                }
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void verifyLicense() {
        try {
            SharedPreferences refpref = getSharedPreferences(ReferringReceiver.PREFERENCES, 1);
            if (!refpref.getBoolean("referrer_tracked", true)) {
                String referer = refpref.getString("referrer_string", "");
                String intenturl = refpref.getString("install_intent_data", "");
                if (referer.length() > 0) {
                    this.tracker.trackEvent("Install", "refer", URLEncoder.encode(referer), 0);
                }
                if (intenturl.length() > 0) {
                    this.tracker.trackEvent("Install", "url", URLEncoder.encode(intenturl), 0);
                }
                SharedPreferences.Editor editor = refpref.edit();
                editor.putBoolean("referrer_tracked", true);
                editor.commit();
            }
        } catch (Exception e) {
            Log.e("refer", "Exception in refpref", e);
        }
        if (TrialCheck.isLicensed(this)) {
            findViewById(R.id.status_demo).setVisibility(8);
            findViewById(R.id.adbar).setVisibility(4);
            if (getPackageName().equals(LICENSE_PACKAGE)) {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
                boolean alreadyTransferred = sharedPref.getBoolean("ArePreferencesTransfered", false);
                if (sharedPref.getBoolean("AlreadyRegistered", false) || !alreadyTransferred) {
                    this.tracker.trackPageView("/start/registered");
                    return;
                }
                SharedPreferences.Editor prefsEditor = sharedPref.edit();
                prefsEditor.putBoolean("AlreadyRegistered", true);
                if (prefsEditor.commit()) {
                    this.tracker.trackPageView("/start/firstpaid");
                    return;
                }
                return;
            }
            return;
        }
        startHouseAds();
        Date now = new Date();
        Date end = TrialCheck.expiration(this);
        if (end != null) {
            ((TextView) findViewById(R.id.status_demo_expiration)).setText(getString(R.string.demo_expires, new Object[]{end}));
        }
        if (end != null && now.after(end)) {
            this.showingExpiredMessage = true;
            new AlertDialog.Builder(this).setTitle((int) R.string.d_expired_title).setMessage((int) R.string.d_software_expired).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    BackCountryActivity.this.showUnlockMessage(true);
                }
            }).setCancelable(false).create().show();
        }
        this.tracker.trackPageView("/fingerprint/" + URLEncoder.encode(String.valueOf(Build.FINGERPRINT) + ";" + Build.MODEL));
        this.tracker.trackPageView("/start/demo");
    }

    public void startHouseAds() {
        AdWhirlManager.setConfigExpireTimeout(18000000);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "267f4debfab347faaeead3115f1aa76b");
        float density = getResources().getDisplayMetrics().density;
        adWhirlLayout.setMaxWidth((int) (320.0f * density));
        adWhirlLayout.setMaxHeight((int) (52.0f * density));
        adWhirlLayout.setAdWhirlInterface(this);
        ((LinearLayout) findViewById(R.id.adbar)).addView(adWhirlLayout, layoutParams);
    }

    private void setPreferences() {
        boolean metricByLocale;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.registerOnSharedPreferenceChangeListener(this);
        this.bview.setShouldTiltMap(sharedPref.getBoolean("tilt_map_preference", true));
        this.bview.setShouldShowRuler(sharedPref.getBoolean("show_ruler_preference", true));
        if (!sharedPref.contains("show_metric_preference")) {
            SharedPreferences.Editor editor = sharedPref.edit();
            if (Locale.getDefault().getCountry().equals("US")) {
                metricByLocale = false;
            } else {
                metricByLocale = true;
            }
            editor.putBoolean("show_metric_preference", metricByLocale);
            editor.commit();
        }
        setFullScreen(sharedPref.getBoolean("full_screen_preference", false));
        BCNSettings.MetricDisplay.set(sharedPref.getBoolean("show_metric_preference", true));
        BCNSettings.ShowWaypointLabels.set(sharedPref.getBoolean("show_waypoint_labels", true));
        BCNSettings.CoordinateFormat.set(Integer.valueOf(sharedPref.getString("coord_preference", "0")).intValue());
        BCNSettings.DatumType.set(Integer.valueOf(sharedPref.getString("datum_preference", "0")).intValue());
        BCNSettings.IconScaling.set(Integer.valueOf(sharedPref.getString("icon_scale_preference", "0")).intValue());
        BCNSettings.CompassSnap.set(sharedPref.getBoolean("bearing_compass_preference", false));
        BCNSettings.MagneticDegrees.set(sharedPref.getBoolean("magnetic_degrees_preference", false));
        BCNSettings.DefaultSymbol.set(sharedPref.getString("LastIcon", ""));
        BCNSettings.FileBase.set(String.valueOf(sharedPref.getString("storage_path_preference", Environment.getExternalStorageDirectory().toString())) + "/bcnav");
        this.mLayersChoices.read();
        this.mShowAtlas = sharedPref.getBoolean("enable_mobile_atlas", false);
    }

    /* access modifiers changed from: protected */
    public void startPrintOrder() {
        this.tracker.trackPageView("/print");
        new AlertDialog.Builder(this).setIcon((int) R.drawable.printer).setTitle((int) R.string.d_title_paper_map).setMessage((int) R.string.d_paper_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BackCountryActivity.this.tracker.trackPageView("/print/ok");
                dialog.dismiss();
                BackCountryActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mytopo.com/searchgeo.cfm?lat=" + BackCountryActivity.this.bview.getCenter().lat + "&lon=" + BackCountryActivity.this.bview.getCenter().lon + "&pid=crittermap")));
            }
        }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void advanceView() {
        int child = this.flipper.getDisplayedChild();
        int position = child + 1;
        if (position > this.flipper.getChildCount() - 1) {
            position = 0;
        }
        if (child == this.flipper.getChildCount() - 1) {
            this.mWaypointQueryprovider.refresh();
            this.bview.refresh();
        }
        this.flipper.setDisplayedChild(position);
        if (this.flipper.getDisplayedChild() == 3) {
            Toast toast = Toast.makeText(this, getString(R.string.t_waypoint_list_instructions), 0);
            toast.setGravity(17, 0, 20);
            toast.show();
        }
    }

    /* access modifiers changed from: protected */
    public void advanceView(int resultCode) {
        int child = this.flipper.getDisplayedChild();
        int position = child;
        switch (resultCode) {
            case R.id.btn_tab_map:
                position = 0;
                break;
            case R.id.btn_tab_compass:
                position = 1;
                break;
            case R.id.btn_tab_waypointlist:
                position = 3;
                break;
            case R.id.btn_tab_computer:
                position = 2;
                break;
        }
        if (position != child) {
            this.flipper.setDisplayedChild(position);
            if (this.flipper.getDisplayedChild() == 3) {
                Toast toast = Toast.makeText(this, getString(R.string.t_waypoint_list_instructions), 0);
                toast.setGravity(17, 0, 20);
                toast.show();
            }
        }
    }

    private void showLicenseAgreementIfNecessary() {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences.Editor prefsEditor = prefs.edit();
        if (!prefs.getBoolean("LicenseAccepted", false)) {
            new AlertDialog.Builder(this).setMessage((int) R.string.d_software_license).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    prefsEditor.putBoolean("LicenseAccepted", true);
                    prefsEditor.commit();
                }
            }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BackCountryActivity.this.finish();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    BackCountryActivity.this.finish();
                }
            }).create().show();
        }
    }

    /* access modifiers changed from: protected */
    public void showCopyright() {
        MapServer server = this.factory.getServer(this.bview.getRenderer().getLayerName());
        if (server != null) {
            new AlertDialog.Builder(this).setTitle((int) R.string.d_copyright_title).setMessage(String.valueOf(server.getCopyrightExplanation()) + "\n" + server.getCopyrightUrl()).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        isRecording();
        if (this.navigator.isStarted()) {
            this.navigator.stop();
        }
        TrackerFactory.releaseTracker();
        this.tracker = null;
        this.bview.getRenderer().onDestroy();
        this.bview.onDestroy();
        this.errorReporter.DeInit();
        this.errorReporter = null;
        if (this.bdb != null) {
        }
        this.factory = null;
        this.navigator.deleteObservers();
        this.navigator = null;
        this.mPointPopup = null;
        this.mSymbolFactory = null;
        this.flipper.removeAllViews();
        this.bview = null;
        this.cview = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putFloat("Longitude", (float) this.bview.getCenter().lon);
        editor.putFloat("Latitude", (float) this.bview.getCenter().lat);
        editor.putInt("ZoomLevel", this.bview.getLevel());
        MapRenderer rend = this.bview.getRenderer();
        String layerName = rend.getLayerName();
        if (layerName != null && layerName.length() > 0) {
            if (rend.isOffline()) {
                editor.putBoolean("Offline", true);
                editor.putString("OfflineLayer", layerName);
            } else {
                editor.putBoolean("Offline", false);
                editor.putString("PreviewLayer", layerName);
            }
        }
        if (this.bdb != null) {
            editor.putString("TripDb", this.bdb.getName());
        }
        editor.commit();
        this.navigator.pause();
        stopWatchingExternalStorage();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Calendar now = Calendar.getInstance();
        String format = String.format("%d-%d-%d", Integer.valueOf(now.get(5)), Integer.valueOf(now.get(11)), Integer.valueOf(now.get(12)));
        if (this.navigator == null) {
            this.navigator = new Navigator(this);
            this.navigator.addObserver(this.bview);
            this.navigator.addObserver(this.cview);
        }
        this.navigator.resume();
        Log.w("OnResume", "CheckingForFirstChoose");
        if (this.showingFirstChoose) {
            try {
                dismissDialog(0);
            } catch (Exception e) {
                Log.e("OnResume", "Dismissing previous dialog");
            }
            this.showingFirstChoose = false;
        }
        if (!this.showingExpiredMessage && !this.showingFirstChoose && this.bview.getRenderer().getLayerName().length() <= 0) {
            this.showingFirstChoose = true;
            Log.w("OnResume", "NoMapMessage");
            showDialog(0);
            Log.w("OnResume", "AfterNoMapMessage");
        }
        updateExternalStorageState();
        startWatchingExternalStorage();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setMapPreviewID(String string) {
        final MapServer server = this.factory.getServer(string);
        if (server != null) {
            this.bview.getRenderer().setPreview(server);
            this.mLayersChoices.addChoice(server.getDisplayName(), "server:" + server.getShortName());
            this.bview.post(new Runnable() {
                public void run() {
                    BackCountryActivity.this.bview.refresh(server);
                }
            });
            server.getShortName().equals("mytopo");
            setCopyRight(server);
            setIcon(false);
            this.tracker.trackPageView("/preview/" + server.getShortName());
            this.tracker.dispatch();
        }
    }

    /* access modifiers changed from: private */
    public void setMapOfflineLayer(String string) {
        final MapServer server = this.factory.getServer(string);
        if (server != null) {
            this.bview.getRenderer().setOfflineLayer(server);
            this.mLayersChoices.addChoice(server.getDisplayName(), "server:" + server.getShortName());
            server.getShortName().equals("mytopo");
            this.bview.post(new Runnable() {
                public void run() {
                    BackCountryActivity.this.bview.refresh(server);
                }
            });
            setCopyRight(server);
            setIcon(true);
            this.tracker.trackPageView("/offline/" + server.getShortName());
        }
    }

    private void setCopyRight(MapServer server) {
        if (Versioning.getSDKNumber() < 11) {
            this.tCopyright.setText(Html.fromHtml("<a href=\"" + server.getCopyrightUrl() + "\">&copy;" + server.getCopyright() + "</a>"));
        }
    }

    private void setIcon(boolean offline) {
        if (Versioning.getSDKNumber() >= 11) {
            return;
        }
        if (offline) {
            this.tIcon.setImageResource(R.drawable.tt_offline);
        } else {
            this.tIcon.setImageResource(R.drawable.tt_preview);
        }
    }

    public boolean onSearchRequested() {
        startSearch(null, false, null, false);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String action = intent.getAction();
        if ("android.intent.action.SEARCH".equals(action)) {
            doSearchWithIntent(intent);
        } else if (OPEN_LOCATION.equals(action)) {
            setViewWithIntentNew(intent);
        } else if (OPEN_DATABASE.equals(action)) {
            setViewWithIntentNew(intent);
        } else if (CHECK_LICENSE.equals(action)) {
            verifyLicense();
        } else if (TRANSFER_PREFERENCES.equals(action)) {
            receivePreferences(intent);
            setViewWithPreferences();
        } else if ("android.intent.action.VIEW".equals(action)) {
            centerWithIntent(intent);
        }
    }

    private void centerWithIntent(Intent intent) {
        String query;
        Position pos;
        Uri data = intent.getData();
        if (data != null && (query = data.getQueryParameter("q")) != null && (pos = PlaceAddress.positionFromQueryString(query)) != null) {
            this.bview.gotoCoordinate(pos, query);
        }
    }

    private void doSearchWithIntent(Intent queryIntent) {
        final String queryString = queryIntent.getStringExtra("query");
        this.tracker.trackEvent("Place", "Search", URLEncoder.encode(queryString), 1);
        Position pos = PlaceAddress.positionFromQueryString(queryString);
        if (pos != null) {
            this.bview.gotoCoordinate(pos, queryString);
        } else {
            new LocationFindTask(this, new LocationFindTask.Listener() {
                /* Debug info: failed to restart local var, previous not found, register: 13 */
                public void onResult(Map<String, Position> result) {
                    final SearchRecentSuggestions suggestions = new SearchRecentSuggestions(BackCountryActivity.this, SearchRecentPlacesProvider.getAuthority(BackCountryActivity.this), 1);
                    if (result == null || result.size() <= 0) {
                        Toast toast = Toast.makeText(BackCountryActivity.this, BackCountryActivity.this.getResources().getString(R.string.t_not_found, queryString), 1);
                        toast.setGravity(17, 0, 20);
                        toast.show();
                        return;
                    }
                    Set<String> keys = result.keySet();
                    final String[] keylist = new String[keys.size()];
                    int i = 0;
                    for (String key : keys) {
                        keylist[i] = key;
                        i++;
                    }
                    if (result.size() == 1) {
                        suggestions.saveRecentQuery(keylist[0], null);
                        BackCountryActivity.this.bview.gotoCoordinate(result.get(keylist[0]), keylist[0]);
                    } else if (result.size() > 1) {
                        AlertDialog.Builder title = new AlertDialog.Builder(BackCountryActivity.this).setIcon(17301546).setTitle((int) R.string.d_choose_place);
                        final String str = queryString;
                        final Map<String, Position> map = result;
                        title.setSingleChoiceItems(keylist, 0, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                int i = whichButton;
                                suggestions.saveRecentQuery(str, null);
                                suggestions.saveRecentQuery(keylist[i], null);
                                BackCountryActivity.this.bview.gotoCoordinate((Position) map.get(keylist[i]), keylist[i]);
                            }
                        }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }).create().show();
                    }
                }
            }).execute(queryString);
        }
        setLevelLabel(this.bview.getLevel());
    }

    /* access modifiers changed from: package-private */
    public void setLevelLabel(int level) {
        this.tLevel.setText(String.valueOf(level));
    }

    private void setViewWithIntentNew(Intent viewIntent) {
        CoordinateBoundingBox box;
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.Longitude") && viewIntent.hasExtra("com.crittermap.backcountrynavigator.Latitude")) {
            this.bview.setCenter(new Position(viewIntent.getDoubleExtra("com.crittermap.backcountrynavigator.Longitude", this.bview.getCenter().lon), viewIntent.getDoubleExtra("com.crittermap.backcountrynavigator.Latitude", this.bview.getCenter().lat)));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.ZoomLevel")) {
            this.bview.setLevel(viewIntent.getIntExtra("com.crittermap.backcountrynavigator.ZoomLevel", this.bview.getLevel()));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.RemoveNotification")) {
            ((NotificationManager) getSystemService("notification")).cancel(viewIntent.getIntExtra("com.crittermap.backcountrynavigator.RemoveNotification", 2));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.DbFile")) {
            String dbstring = viewIntent.getStringExtra("com.crittermap.backcountrynavigator.DbFile");
            if (dbstring != null) {
                this.bdb = BCNMapDatabase.openTrip(dbstring);
                this.bview.getRenderer().setBdb(this.bdb);
                this.mWaypointQueryprovider.setDatabase(this.bdb);
            }
            if (!(this.bdb == null || (box = this.bdb.findBounds()) == null)) {
                this.bview.setCenterAndZoom(box.getCenter(), box.getZoomLevel());
            }
        }
        if (viewIntent.hasExtra("Chosen.Server")) {
            String serverString = viewIntent.getStringExtra("Chosen.Server");
            if (this.bview.getRenderer().isOffline()) {
                setMapOfflineLayer(serverString);
            } else {
                setMapPreviewID(serverString);
            }
        }
        if (viewIntent.getData() != null) {
            if (Log.isLoggable("ViewIntent", 4)) {
                Log.i("ViewIntent", viewIntent.getDataString());
            }
            String layerString = viewIntent.getData().getSchemeSpecificPart();
            String scheme = viewIntent.getData().getScheme();
            if (scheme.equals("offline")) {
                setMapOfflineLayer(layerString);
            } else if (scheme.equals(MobileAtlasServer.MOBILEATLASSCHEME)) {
                setMobileAtlasLayer(viewIntent.getData());
            } else if (scheme.equals("show")) {
                if (this.bview.getRenderer().isOffline()) {
                    setMapOfflineLayer(layerString);
                } else {
                    setMapPreviewID(layerString);
                }
            }
        }
        this.tLevel.setText(String.valueOf(this.bview.getLevel()));
    }

    private void setMobileAtlasLayer(Uri data) {
        final MobileAtlasServer server = MobileAtlasServer.createFromUri(data);
        if (server != null) {
            this.bview.getRenderer().setMobileAtlasLayer(server);
            this.mLayersChoices.addChoice(server.getDisplayName(), data.toString());
            this.bview.post(new Runnable() {
                public void run() {
                    BackCountryActivity.this.bview.refresh(server);
                }
            });
            setCopyRight(server);
            setIcon(true);
            this.tracker.trackPageView("/atlas/" + URLEncoder.encode(server.getDisplayName()));
        }
    }

    private void setViewWithIntent(Intent viewIntent) {
        String dbstring;
        CoordinateBoundingBox box;
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.Longitude") && viewIntent.hasExtra("com.crittermap.backcountrynavigator.Latitude")) {
            this.bview.setCenter(new Position(viewIntent.getDoubleExtra("com.crittermap.backcountrynavigator.Longitude", this.bview.getCenter().lon), viewIntent.getDoubleExtra("com.crittermap.backcountrynavigator.Latitude", this.bview.getCenter().lat)));
        } else if (preferences.contains("Longitude") && preferences.contains("Latitude")) {
            this.bview.setCenter(new Position((double) preferences.getFloat("Longitude", (float) this.bview.getCenter().lon), (double) preferences.getFloat("Latitude", (float) this.bview.getCenter().lat)));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.ZoomLevel")) {
            this.bview.setLevel(viewIntent.getIntExtra("com.crittermap.backcountrynavigator.ZoomLevel", this.bview.getLevel()));
        } else {
            this.bview.setLevel(preferences.getInt("ZoomLevel", this.bview.getLevel()));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.OfflineLayer")) {
            String layerString = viewIntent.getStringExtra("com.crittermap.backcountrynavigator.OfflineLayer");
            if (this.bview.getRenderer().isOffline()) {
                setMapOfflineLayer(layerString);
            } else {
                setMapPreviewID(layerString);
            }
        } else if (preferences.contains("Offline")) {
            if (preferences.getBoolean("Offline", false)) {
                if (preferences.contains("OfflineLayer")) {
                    setMapOfflineLayer(preferences.getString("OfflineLayer", "MyTopo.com"));
                }
            } else if (preferences.contains("PreviewLayer")) {
                setMapPreviewID(preferences.getString("PreviewLayer", "MyTopo.com"));
            }
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.RemoveNotification")) {
            ((NotificationManager) getSystemService("notification")).cancel(viewIntent.getIntExtra("com.crittermap.backcountrynavigator.RemoveNotification", 2));
        }
        if (viewIntent.hasExtra("com.crittermap.backcountrynavigator.DbFile")) {
            String dbstring2 = viewIntent.getStringExtra("com.crittermap.backcountrynavigator.DbFile");
            if (dbstring2 != null) {
                this.bdb = BCNMapDatabase.openTrip(dbstring2);
                this.bview.getRenderer().setBdb(this.bdb);
                this.mWaypointQueryprovider.setDatabase(this.bdb);
            }
            if (!(this.bdb == null || (box = this.bdb.findBounds()) == null)) {
                this.bview.setCenterAndZoom(box.getCenter(), box.getZoomLevel());
            }
        } else if (preferences.contains("TripDb")) {
            String dbstring3 = preferences.getString("TripDb", null);
            if (dbstring3 != null) {
                this.bdb = BCNMapDatabase.openTrip(dbstring3);
                this.bview.getRenderer().setBdb(this.bdb);
                this.mWaypointQueryprovider.setDatabase(this.bdb);
            }
        } else if (preferences.contains("DbFile") && (dbstring = preferences.getString("DbFile", null)) != null) {
            this.bdb = BCNMapDatabase.openExisting(dbstring);
            this.bview.getRenderer().setBdb(this.bdb);
            this.mWaypointQueryprovider.setDatabase(this.bdb);
        }
        if (viewIntent.getData() != null) {
            if (Log.isLoggable("ViewIntent", 4)) {
                Log.i("ViewIntent", viewIntent.getDataString());
            }
            String layerString2 = viewIntent.getData().getSchemeSpecificPart();
            String scheme = viewIntent.getData().getScheme();
            if (scheme.equals("offline")) {
                setMapOfflineLayer(layerString2);
            } else if (scheme.equals(MobileAtlasServer.MOBILEATLASSCHEME)) {
                setMobileAtlasLayer(viewIntent.getData());
            } else if (scheme.equals("show")) {
                if (this.bview.getRenderer().isOffline()) {
                    setMapOfflineLayer(layerString2);
                } else {
                    setMapPreviewID(layerString2);
                }
            }
        }
        this.tLevel.setText(String.valueOf(this.bview.getLevel()));
    }

    class LayersChoices {
        static final int MAXCHOICES = 4;
        ArrayList<String> choiceList = new ArrayList<>();
        ArrayList<String> labelList = new ArrayList<>();

        LayersChoices() {
        }

        public void addChoice(String label, String choice) {
            for (int i = 0; i < this.choiceList.size(); i++) {
                if (this.choiceList.get(i).equals(choice)) {
                    this.choiceList.remove(i);
                    this.labelList.remove(i);
                    this.choiceList.add(0, choice);
                    this.labelList.add(0, label);
                    write();
                    return;
                }
            }
            if (this.choiceList.size() >= 4) {
                this.choiceList.remove(this.choiceList.size() - 1);
                this.labelList.remove(this.labelList.size() - 1);
            }
            this.choiceList.add(0, choice);
            this.labelList.add(0, label);
            write();
        }

        public void shuffle(int mi) {
            addChoice(this.labelList.get(mi), this.choiceList.get(mi));
        }

        public int size() {
            return this.choiceList.size();
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        public String getTopChoice() {
            if (this.choiceList.size() <= 0) {
                return null;
            }
            return this.choiceList.get(0);
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public String getChosen(int index) {
            if (this.choiceList.size() <= index) {
                return null;
            }
            return this.choiceList.get(index);
        }

        public List<String> getChoiceLabels() {
            return this.labelList;
        }

        public void read() {
            SharedPreferences preferences = BackCountryActivity.this.getSharedPreferences(BackCountryActivity.PREFS_NAME, 0);
            String choiceGroup = preferences.getString("LayerChoiceGroup", "");
            String labelGroup = preferences.getString("LayerLabelGroup", "");
            String[] choices = choiceGroup.split("\\|");
            String[] labels = labelGroup.split("\\|");
            this.choiceList.clear();
            this.labelList.clear();
            int j = 0;
            while (j < choices.length && j < labels.length) {
                String ch = choices[j];
                String lb = labels[j];
                if (ch.length() > 0 && lb.length() > 0) {
                    this.choiceList.add(ch);
                    this.labelList.add(lb);
                }
                j++;
            }
        }

        /* access modifiers changed from: protected */
        public void write() {
            SharedPreferences.Editor editor = BackCountryActivity.this.getSharedPreferences(BackCountryActivity.PREFS_NAME, 0).edit();
            String choiceGroup = "";
            String labelGroup = "";
            for (int i = 0; i < this.choiceList.size(); i++) {
                choiceGroup = String.valueOf(choiceGroup) + this.choiceList.get(i);
                labelGroup = String.valueOf(labelGroup) + this.labelList.get(i);
                if (i != this.choiceList.size() - 1) {
                    choiceGroup = String.valueOf(choiceGroup) + "|";
                    labelGroup = String.valueOf(labelGroup) + "|";
                }
            }
            editor.putString("LayerChoiceGroup", choiceGroup);
            editor.putString("LayerLabelGroup", labelGroup);
            editor.commit();
        }
    }

    private void setViewWithPreferences() {
        String dbstring;
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
        if (!preferences.contains("Longitude") || !preferences.contains("Latitude")) {
            Position p = this.navigator.getRecentPosition();
            if (p != null) {
                this.bview.setCenter(p);
            }
        } else {
            this.bview.setCenter(new Position((double) preferences.getFloat("Longitude", (float) this.bview.getCenter().lon), (double) preferences.getFloat("Latitude", (float) this.bview.getCenter().lat)));
        }
        if (preferences.contains("ZoomLevel")) {
            this.bview.setLevel(preferences.getInt("ZoomLevel", this.bview.getLevel()));
        } else {
            this.bview.setLevel(13);
        }
        String lastChosen = this.mLayersChoices.getTopChoice();
        if (lastChosen != null && lastChosen.contains(MobileAtlasServer.MOBILEATLASSCHEME)) {
            setMapUrn("", lastChosen);
        } else if (preferences.contains("Offline")) {
            if (preferences.getBoolean("Offline", false)) {
                if (preferences.contains("OfflineLayer")) {
                    setMapOfflineLayer(preferences.getString("OfflineLayer", "MyTopo.com"));
                }
            } else if (preferences.contains("PreviewLayer")) {
                setMapPreviewID(preferences.getString("PreviewLayer", "MyTopo.com"));
            }
        }
        if (preferences.contains("TripDb")) {
            String dbstring2 = preferences.getString("TripDb", null);
            if (dbstring2 != null) {
                this.bdb = BCNMapDatabase.openTrip(dbstring2);
                this.bview.getRenderer().setBdb(this.bdb);
                this.mWaypointQueryprovider.setDatabase(this.bdb);
            }
        } else if (preferences.contains("DbFile") && (dbstring = preferences.getString("DbFile", null)) != null) {
            this.bdb = BCNMapDatabase.openExisting(dbstring);
            this.bview.getRenderer().setBdb(this.bdb);
            this.mWaypointQueryprovider.setDatabase(this.bdb);
        }
        if (this.bdb == null) {
            this.bdb = BCNMapDatabase.newOrExistingTrip(DEFAULTDBNAME);
            this.bview.getRenderer().setBdb(this.bdb);
            this.mWaypointQueryprovider.setDatabase(this.bdb);
        }
        this.tLevel.setText(String.valueOf(this.bview.getLevel()));
    }

    public void labelEvent(BCNMapView.BViewContextMenuInfo info) {
        if (this.mPointPopup == null) {
            this.mPointPopup = new PointPopup(this);
            this.mPointPopup.setPointPopupListener(new PointPopup.PointPopupListener() {
                public void pressEvent(long id, Position pos) {
                    if (BackCountryActivity.this.bdb == null) {
                        return;
                    }
                    if (id == -1) {
                        BackCountryActivity.this.setWayPointView(BackCountryActivity.this.bdb.newWayPoint(pos.lon, pos.lat));
                        return;
                    }
                    BackCountryActivity.this.setWayPointView(id);
                }
            });
        }
        if (this.bdb != null) {
            Cursor c = null;
            try {
                c = this.bdb.getNearbyWayPoints(info.position, info.box);
                if (c.getCount() > 0) {
                    c.moveToFirst();
                    this.mPointPopup.showEdit(this.bview, c.getString(c.getColumnIndex("Name")), c.getString(c.getColumnIndex("Description")), (int) info.pixx, (int) info.pixy, c.getLong(c.getColumnIndex("PointID")));
                } else {
                    this.mPointPopup.showNew(this.bview, (int) info.pixx, (int) info.pixy, info.position);
                }
                if (c != null) {
                    c.close();
                }
            } catch (Exception e) {
                Log.e("BackCountryActivity", "labelEvent", e);
                if (c != null) {
                    c.close();
                }
            } catch (Throwable th) {
                if (c != null) {
                    c.close();
                }
                throw th;
            }
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key != null && sharedPreferences != null) {
            Log.i("Preference Changed:", key);
            if (key.equals("full_screen_preference")) {
                setFullScreen(sharedPreferences.getBoolean(key, false));
            }
            if (key.equals("show_metric_preference")) {
                BCNSettings.MetricDisplay.set(sharedPreferences.getBoolean(key, false));
            }
            if (key.equals("show_waypoint_labels")) {
                BCNSettings.ShowWaypointLabels.set(sharedPreferences.getBoolean(key, false));
                if (this.bview != null) {
                    this.bview.refresh();
                }
            }
            if (key.equals("coord_preference")) {
                BCNSettings.CoordinateFormat.set(Integer.valueOf(sharedPreferences.getString(key, "0")).intValue());
            }
            if (key.equals("datum_preference")) {
                BCNSettings.DatumType.set(Integer.valueOf(sharedPreferences.getString(key, "0")).intValue());
            }
            if (key.equals("icon_scale_preference")) {
                BCNSettings.IconScaling.set(Integer.valueOf(sharedPreferences.getString(key, "0")).intValue());
                if (this.bview != null) {
                    this.bview.refresh();
                }
            }
            if (key.equals("tilt_map_preference") && this.bview != null) {
                this.bview.setShouldTiltMap(sharedPreferences.getBoolean(key, true));
                this.bview.invalidate();
            }
            if (key.equals("show_ruler_preference") && this.bview != null) {
                this.bview.setShouldShowRuler(sharedPreferences.getBoolean(key, true));
                this.bview.invalidate();
            }
            if (key.equals("enable_mobile_atlas")) {
                this.mShowAtlas = sharedPreferences.getBoolean(key, true);
            }
            if (key.equals("bearing_compass_preference")) {
                BCNSettings.CompassSnap.set(sharedPreferences.getBoolean(key, false));
            }
            if (key.equals("magnetic_degrees_preference")) {
                BCNSettings.MagneticDegrees.set(sharedPreferences.getBoolean(key, false));
            }
            if (key.equals("LastIcon")) {
                BCNSettings.DefaultSymbol.set(sharedPreferences.getString(key, ""));
            }
            if (key.equals("storage_path_preference")) {
                AlertDialog dialog = new AlertDialog.Builder(this).setTitle((int) R.string.d_storage_path_changed).setMessage(getString(R.string.d_storage_path_message, new Object[]{sharedPreferences.getString(key, "")})).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        BackCountryActivity.this.finish();
                    }
                });
                try {
                    dialog.show();
                } catch (Exception ex) {
                    Log.e("PreferenceChanged", "Unable to show storage message", ex);
                }
            }
            if (key.equals(TrialCheck.EXPIRATIONGUID)) {
                Date end = new Date(sharedPreferences.getLong(key, -1));
                TextView expire = (TextView) findViewById(R.id.status_demo_expiration);
                if (expire != null) {
                    expire.setText(getString(R.string.demo_expires, new Object[]{end}));
                }
                Date now = new Date();
                if (end != null && now.after(end)) {
                    new AlertDialog.Builder(this).setTitle((int) R.string.d_expired_title).setMessage((int) R.string.d_software_expired).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            BackCountryActivity.this.showUnlockMessage(true);
                        }
                    }).create().show();
                }
            }
            if (key.equals("ArePreferencesTransfered")) {
                this.mLayersChoices.read();
                setViewWithPreferences();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            if (this.flipper.getDisplayedChild() != 0) {
                this.flipper.setDisplayedChild(0);
                return true;
            } else if (isRecording()) {
                new AlertDialog.Builder(this).setIcon((int) R.drawable.track).setTitle((int) R.string.d_title_exiting).setItems((int) R.array.d_leaving_options, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                dialog.dismiss();
                                BackCountryActivity.this.finish();
                                return;
                            case 1:
                                BackCountryActivity.this.startStopRecording();
                                dialog.dismiss();
                                BackCountryActivity.this.finish();
                                return;
                            case 2:
                                dialog.dismiss();
                                return;
                            default:
                                return;
                        }
                    }
                }).create().show();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: package-private */
    public void updateExternalStorageState() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            this.mExternalStorageWriteable = true;
            this.mExternalStorageAvailable = true;
        } else if ("mounted_ro".equals(state)) {
            this.mExternalStorageAvailable = true;
            this.mExternalStorageWriteable = false;
        } else {
            this.mExternalStorageWriteable = false;
            this.mExternalStorageAvailable = false;
        }
        handleExternalStorageState(this.mExternalStorageAvailable, this.mExternalStorageWriteable);
        Log.w("UpdateExternalStorageState", "Available:" + this.mExternalStorageAvailable + ". Writeable:" + this.mExternalStorageWriteable);
    }

    private void handleExternalStorageState(boolean externalStorageAvailable, boolean externalStorageWriteable) {
        View view = findViewById(R.id.warning_storage);
        int oldvisibility = view.getVisibility();
        if (!externalStorageAvailable || !externalStorageWriteable) {
            if (oldvisibility != 0) {
                this.tracker.trackPageView("/warning/externalstorage");
                view.setVisibility(0);
            }
        } else if (oldvisibility != 8) {
            view.setVisibility(8);
            this.bview.refresh();
        }
    }

    /* access modifiers changed from: package-private */
    public void startWatchingExternalStorage() {
        this.mExternalStorageReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.w("BackCountryActivity", "Storage: " + intent.getAction());
                BackCountryActivity.this.updateExternalStorageState();
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.MEDIA_MOUNTED");
        filter.addAction("android.intent.action.MEDIA_REMOVED");
        filter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        filter.addAction("android.intent.action.MEDIA_UNMOUNTABLE");
        filter.addAction("android.intent.action.MEDIA_SHARED");
        filter.addAction("android.intent.action.MEDIA_CHECKING");
        filter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
        Intent registerReceiver = registerReceiver(this.mExternalStorageReceiver, filter);
        updateExternalStorageState();
    }

    /* access modifiers changed from: package-private */
    public void stopWatchingExternalStorage() {
        unregisterReceiver(this.mExternalStorageReceiver);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tb_mark:
                if (this.bdb != null) {
                    Position center = this.bview.getCenter();
                    Intent mi = new Intent(this, MarkActivity.class);
                    mi.putExtra("TripPath", this.bdb.getFileName());
                    mi.putExtra("lon", center.lon);
                    mi.putExtra("lat", center.lat);
                    CoordinateBoundingBox box = this.bview.getBounds();
                    mi.putExtra("minlon", box.minlon);
                    mi.putExtra("minlat", box.minlat);
                    mi.putExtra("maxlon", box.maxlon);
                    mi.putExtra("maxlat", box.maxlat);
                    startActivityForResult(mi, R.id.activity_request_mark);
                    return;
                }
                return;
            case R.id.button_toolbar_pull:
                Intent intent = new Intent(this, ToolBarActivity.class);
                intent.putExtra("recording", this.navigator.isRecording());
                intent.putExtra("gpson", this.navigator.isStarted());
                startActivityForResult(intent, R.id.activity_request_toolbar);
                Class<Activity> cls = Activity.class;
                try {
                    Method method = cls.getMethod("overridePendingTransition", Integer.TYPE, Integer.TYPE);
                    return;
                } catch (Exception e) {
                    return;
                }
            case R.id.tb_sculpt:
                this.bview.setSculpting(this.tb_sculpt.isChecked());
                return;
            case R.id.tb_burn:
                startDownload();
                return;
            case R.id.tb_clean:
                startCleaning();
                return;
            case R.id.tb_sculpt_clear:
                this.bview.clearSculpting();
                this.bview.setSculpting(false);
                this.mapPanel.setVisibility(8);
                return;
            case R.id.tbn_recenter:
                this.bview.setFollowing(true);
                this.navigator.start();
                return;
            case R.id.tbn_record_track:
                startStopRecording();
                return;
            case R.id.tbn_gps_off:
                startStopGps();
                return;
            default:
                return;
        }
    }

    private void startStopGps() {
        this.tracker.trackPageView("/tb_button/mylocation");
        if (!this.navigator.isStarted()) {
            startGps();
            return;
        }
        this.navigator.stop();
        this.bview.setFollowing(false);
        this.bview.setTiltMap(false);
        Toast.makeText(this, (int) R.string.t_gps_deactivated, 1).show();
    }

    private void startGps() {
        if (!((LocationManager) getSystemService("location")).isProviderEnabled(MyTracksConstants.GPS_PROVIDER)) {
            Toast.makeText(this, "Please allow usage of GPS satellites in settings", 1).show();
            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            return;
        }
        this.bview.setFollowing(true);
        this.navigator.start();
        Toast.makeText(this, (int) R.string.t_gps_activated, 1).show();
    }

    private void startCleaning() {
        if (this.bview.getMarkedRectangles() == null || this.bview.getMarkedRectangles().size() <= 0) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.sculpt).setTitle((int) R.string.d_title_offline_download).setMessage((int) R.string.d_offline_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        }
        final MapServer server = this.bview.getRenderer().getMServer();
        if (server != null) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.sculpt).setTitle((int) R.string.d_title_cleanup).setMessage((int) R.string.d_cleanup_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent cleanIntent = new Intent(BackCountryActivity.this, CleanupService.class);
                    cleanIntent.putExtra("boxes", CoordinateBoundingBox.toArray(BackCountryActivity.this.bview.getMarkedRectangles()));
                    cleanIntent.putExtra("layer", server.getShortName());
                    cleanIntent.putExtra("resolverType", server.getTileResolverType());
                    BackCountryActivity.this.startService(cleanIntent);
                }
            }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }
    }

    /* access modifiers changed from: package-private */
    public void cleanupFinished(Integer tcount) {
        Toast toast = Toast.makeText(this, getString(R.string.t_cleanup_tiles, new Object[]{tcount}), 1);
        toast.setGravity(17, 0, 20);
        toast.show();
    }

    public void update(Observable arg0, Object arg1) {
        final Boolean ok = (Boolean) arg1;
        runOnUiThread(new Runnable() {
            public void run() {
                if (ok.booleanValue()) {
                    BackCountryActivity.this.lStorageWarning.setVisibility(8);
                } else {
                    BackCountryActivity.this.lStorageWarning.setVisibility(0);
                }
            }
        });
    }

    public void sendPreferences() {
        Intent intent = new Intent();
        intent.setClassName(LICENSE_PACKAGE, "com.crittermap.backcountrynavigator.BackCountryActivity");
        intent.setAction(TRANSFER_PREFERENCES);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        intent.putExtra("tilt_map_preference", sharedPref.getBoolean("tilt_map_preference", true));
        intent.putExtra("show_ruler_preference", sharedPref.getBoolean("show_ruler_preference", true));
        intent.putExtra("show_metric_preference", sharedPref.getBoolean("show_metric_preference", true));
        intent.putExtra("utm_coord_preference", sharedPref.getBoolean("utm_coord_preference", false));
        intent.putExtra("coord_preference", sharedPref.getString("coord_preference", "0"));
        intent.putExtra("bearing_compass_preference", sharedPref.getBoolean("bearing_compass_preference", false));
        intent.putExtra("LastIcon", sharedPref.getString("LastIcon", ""));
        intent.putExtra("LayerChoiceGroup", sharedPref.getString("LayerChoiceGroup", ""));
        intent.putExtra("LayerLabelGroup", sharedPref.getString("LayerLabelGroup", ""));
        intent.putExtra("mobile_atlas_path", sharedPref.getString("mobile_atlas_path", String.valueOf(BCNSettings.FileBase.get()) + "/atlases"));
        intent.putExtra("AlreadyRegistered", sharedPref.getBoolean("AlreadyRegistered", false));
        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
        intent.putExtra("Longitude", preferences.getFloat("Longitude", (float) this.bview.getCenter().lon));
        intent.putExtra("Latitude", preferences.getFloat("Latitude", (float) this.bview.getCenter().lat));
        intent.putExtra("ZoomLevel", preferences.getInt("ZoomLevel", this.bview.getLevel()));
    }

    public void receivePreferences(Intent intent) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean("tilt_map_preference", intent.getBooleanExtra("tilt_map_preference", true));
        editor.putBoolean("show_ruler_preference", intent.getBooleanExtra("show_ruler_preference", true));
        editor.putBoolean("show_metric_preference", intent.getBooleanExtra("show_metric_preference", false));
        editor.putBoolean("utm_coord_preference", intent.getBooleanExtra("utm_coord_preference", false));
        editor.putBoolean("bearing_compass_preference", intent.getBooleanExtra("bearing_compass_preference", false));
        editor.putBoolean("AlreadyRegistered", intent.getBooleanExtra("AlreadyRegistered", false));
        editor.putString("coord_preference", intent.getStringExtra("coord_preference"));
        editor.putString("LayerChoiceGroup", intent.getStringExtra("LayerChoiceGroup"));
        editor.putString("LayerLabelGroup", intent.getStringExtra("LayerLabelGroup"));
        editor.putString("mobile_atlas_path", intent.getStringExtra("mobile_atlas_path"));
        editor.putString("LastIcon", intent.getStringExtra("LastIcon"));
        editor.commit();
        SharedPreferences.Editor editor2 = getSharedPreferences(PREFS_NAME, 0).edit();
        editor2.putFloat("Longitude", intent.getFloatExtra("Longitude", -120.0f));
        editor2.putFloat("Latitude", intent.getFloatExtra("Latitude", 45.0f));
        editor2.putInt("ZoomLevel", intent.getIntExtra("ZoomLevel", 13));
        editor2.commit();
    }

    public void adWhirlGeneric() {
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
