package com.crittermap.backcountrynavigator;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import java.util.ArrayList;
import java.util.List;

public class ProximityActivity extends ListActivity {
    private String path;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.proximity_list_activity);
        initList();
    }

    private void initList() {
        this.path = getIntent().getStringExtra("Path");
        String[] cols = {"Name", "Comment", "PointID", "SymbolName"};
        SQLiteDatabase db = null;
        double lon = getIntent().getDoubleExtra("lon", 0.0d);
        double lat = getIntent().getDoubleExtra("lat", 0.0d);
        StringBuilder orderByClause = new StringBuilder().append(Math.pow(Math.cos(Math.toRadians(lat)), 2.0d));
        orderByClause.append("*(");
        orderByClause.append(lon);
        orderByClause.append("-Longitude)*(");
        orderByClause.append(lon);
        orderByClause.append("-Longitude) + (");
        orderByClause.append(lat);
        orderByClause.append("-Latitude)*(");
        orderByClause.append(lat);
        orderByClause.append("-Latitude) LIMIT 40");
        Cursor c = null;
        try {
            SQLiteDatabase db2 = SQLiteDatabase.openDatabase(this.path, null, 0);
            Cursor c2 = db2.query(BCNMapDatabase.WAY_POINTS, cols, null, null, null, null, orderByClause.toString());
            List<ProximityListBean> beans = new ArrayList<>();
            if (c2.moveToFirst()) {
                do {
                    ProximityListBean bean = new ProximityListBean();
                    bean.setName(c2.getString(0));
                    bean.setComment(c2.getString(1));
                    bean.setId((long) c2.getInt(2));
                    bean.setSymbol(c2.getString(3));
                    beans.add(bean);
                } while (c2.moveToNext());
            }
            setListAdapter(new ProximityListAdapter(this, R.layout.proximity_list_item, R.id.proximity_item_name, beans));
            if (c2 != null) {
                c2.close();
            }
            if (db2 != null) {
                db2.close();
            }
        } catch (Exception e) {
            Log.e("ProximityActivity", "Querying", e);
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this, AttributesDisplayActivity.class);
        intent.putExtra("Path", this.path);
        intent.putExtra("poi", (Integer) v.getTag());
        startActivity(intent);
    }
}
