package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.data.FileImporter;
import com.crittermap.backcountrynavigator.data.ImportListener;
import com.crittermap.backcountrynavigator.data.ImportService;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;
import uk.me.jstott.jcoord.MGRSRef;

public class ImportActivity extends Activity {
    public static final String DBFILENAME_EXTRA = "com.crittermap.backcountrynavigator.DbFile";
    BCNMapDatabase bdb = null;
    String mAction;
    ProgressBar mBar;
    Uri mDataToImport;
    String mDbFileName;
    String mDbFileUsed = null;
    String mImportFileName;
    EditText mNewDbFile;
    private ProgressDialog mProgressDialog;
    Spinner mSpinnerDbChoices;
    private String mType;
    RadioGroup radioGroup;
    Button startButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.import_activity);
        this.radioGroup = (RadioGroup) findViewById(R.id.radio_import_choice);
        this.startButton = (Button) findViewById(R.id.btn_import_start);
        EditText textCurrentDb = (EditText) findViewById(R.id.import_current_db);
        Intent intent = getIntent();
        this.mAction = intent.getAction();
        this.mImportFileName = intent.getDataString();
        this.mType = intent.getType();
        this.mDataToImport = intent.getData();
        ((EditText) findViewById(R.id.import_from_url)).setText(this.mImportFileName);
        if (this.mDbFileName != null) {
            textCurrentDb.setText(this.mDbFileName);
        } else {
            textCurrentDb.setVisibility(8);
            findViewById(R.id.import_into_current_choice).setVisibility(8);
        }
        this.mDbFileName = getSharedPreferences(BackCountryActivity.PREFS_NAME, 0).getString("DbFile", null);
        String tripName = null;
        if (this.mDbFileName != null) {
            tripName = new File(this.mDbFileName).getName();
            if (tripName.endsWith(".bcn")) {
                tripName = tripName.replace(".bcn", "");
            }
        }
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setIcon((int) R.drawable.tt_offline);
        this.mProgressDialog.setTitle((int) R.string.import_progress);
        this.mProgressDialog.setProgressStyle(1);
        this.mNewDbFile = (EditText) findViewById(R.id.import_new_db);
        this.mSpinnerDbChoices = (Spinner) findViewById(R.id.import_spinner_existing);
        String[] bcnavfiles = BCNMapDatabase.findExisting();
        ArrayList<String> fileList = new ArrayList<>();
        if (tripName != null) {
            fileList.add(tripName);
        }
        for (String f : bcnavfiles) {
            fileList.add(f);
        }
        this.mSpinnerDbChoices.setAdapter((SpinnerAdapter) new ArrayAdapter(this, 17367049, fileList));
        this.mSpinnerDbChoices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View paramView, int paramInt, long paramLong) {
                ImportActivity.this.radioGroup.check(R.id.import_into_existing_choice);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mNewDbFile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View paramView, boolean paramBoolean) {
                if (paramBoolean) {
                    ImportActivity.this.radioGroup.check(R.id.import_into_new_choice);
                }
            }
        });
        this.mBar = (ProgressBar) findViewById(R.id.pb_import_progress);
        this.startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImportActivity.this.startImport();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void startImport() {
        switch (this.radioGroup.getCheckedRadioButtonId()) {
            case R.id.import_into_current_choice /*2131230794*/:
                if (this.mDbFileName != null) {
                    this.mDbFileUsed = this.mDbFileName;
                    this.bdb = BCNMapDatabase.openTrip(this.mDbFileName);
                    break;
                }
                break;
            case R.id.import_into_new_choice /*2131230796*/:
                String newfilename = this.mNewDbFile.getText().toString();
                if (newfilename != null && newfilename.length() > 0) {
                    this.mDbFileUsed = newfilename;
                    this.bdb = BCNMapDatabase.newTrip(newfilename);
                    break;
                }
            case R.id.import_into_existing_choice /*2131230797*/:
                String filename = (String) this.mSpinnerDbChoices.getSelectedItem();
                if (filename != null) {
                    this.mDbFileUsed = filename;
                    this.bdb = BCNMapDatabase.openTrip(filename);
                    break;
                }
                break;
        }
        if (this.bdb != null && this.mImportFileName != null) {
            if (this.mType == null || this.mType.equals("*/*")) {
                if (this.mImportFileName.toLowerCase().endsWith(".gpx")) {
                    this.mType = "application/gpx";
                }
                if (this.mImportFileName.toLowerCase().endsWith(".loc")) {
                    this.mType = "application/xml-loc";
                }
                if (this.mImportFileName.toLowerCase().endsWith(".kml")) {
                    this.mType = "application/kml";
                }
                if (this.mImportFileName.toLowerCase().endsWith(".kmz")) {
                    this.mType = "application/kmz";
                }
            }
            if (this.mType != null) {
                if (this.mType.equals("application/gpx")) {
                    Intent intent = new Intent(this, ImportService.class);
                    intent.putExtra("Data", this.mImportFileName);
                    intent.putExtra("DBName", this.mDbFileUsed);
                    intent.putExtra("Format", "gpx");
                    startService(intent);
                    finish();
                }
                if (this.mType.equals("application/xml-loc")) {
                    Intent intent2 = new Intent(this, ImportService.class);
                    intent2.putExtra("Data", this.mImportFileName);
                    intent2.putExtra("DBName", this.mDbFileUsed);
                    intent2.putExtra("Format", "loc");
                    startService(intent2);
                    finish();
                }
                if (this.mType.equals("application/kml")) {
                    Intent intent3 = new Intent(this, ImportService.class);
                    intent3.putExtra("Data", this.mImportFileName);
                    intent3.putExtra("DBName", this.mDbFileUsed);
                    intent3.putExtra("Format", "kml");
                    startService(intent3);
                    finish();
                }
                if (this.mType.equals("application/kmz")) {
                    Intent intent4 = new Intent(this, ImportService.class);
                    intent4.putExtra("Data", this.mImportFileName);
                    intent4.putExtra("DBName", this.mDbFileUsed);
                    intent4.putExtra("Format", "kmz");
                    startService(intent4);
                    finish();
                    return;
                }
                return;
            }
            this.bdb = null;
        }
    }

    private InputStream OpenHttpConnection(String urlString) throws IOException {
        URLConnection conn = new URL(urlString).openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            if (httpConn.getResponseCode() == 200) {
                return httpConn.getInputStream();
            }
            return null;
        } catch (Exception e) {
            throw new IOException("Error connecting");
        }
    }

    /* access modifiers changed from: package-private */
    public void finishImport(FileImporter result) {
        if (result != null) {
            Intent intent = new Intent();
            intent.setClass(this, BackCountryActivity.class);
            intent.setAction(BackCountryActivity.OPEN_DATABASE);
            intent.putExtra("com.crittermap.backcountrynavigator.DbFile", this.mDbFileUsed);
            if (result.bounded) {
                intent.putExtra("com.crittermap.backcountrynavigator.Longitude", (result.minlon.doubleValue() + result.maxlon.doubleValue()) / 2.0d);
                intent.putExtra("com.crittermap.backcountrynavigator.Latitude", (result.minlat.doubleValue() + result.maxlat.doubleValue()) / 2.0d);
            }
            startActivity(intent);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    class ImportTask extends AsyncTask<FileImporter, Integer, FileImporter> implements ImportListener {
        ImportTask() {
        }

        /* access modifiers changed from: protected */
        public FileImporter doInBackground(FileImporter... params) {
            FileImporter importer = params[0];
            importer.setIListener(this);
            try {
                importer.importFile();
                return importer;
            } catch (XmlPullParserException e) {
                Log.e("ImportTask", "Importing File", e);
                return null;
            } catch (IOException e2) {
                Log.e("ImportTask", "Importing the File", e2);
                return null;
            } catch (OutOfMemoryError err) {
                Log.e("ImportTask", "OutOfMemory Importing the File", err);
                return null;
            } catch (Exception ex) {
                Log.e("ImportTask", "Exception in import", ex);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(FileImporter result) {
            super.onPostExecute((Object) result);
            ImportActivity.this.finishImport(result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ImportActivity.this.mBar.setVisibility(0);
            ImportActivity.this.mBar.setMax(MGRSRef.PRECISION_1000M);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            super.onProgressUpdate((Object[]) values);
            ImportActivity.this.mBar.setProgress(values[0].intValue());
        }

        public boolean reportProgress(int numerator) {
            publishProgress(Integer.valueOf(numerator));
            return isCancelled();
        }
    }
}
