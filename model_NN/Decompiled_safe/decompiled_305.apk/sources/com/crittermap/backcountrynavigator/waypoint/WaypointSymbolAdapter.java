package com.crittermap.backcountrynavigator.waypoint;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.crittermap.backcountrynavigator.waypoint.WaypointSymbolFactory;

public class WaypointSymbolAdapter extends BaseAdapter {
    private WaypointSymbolFactory.SymbolEntry[] idlist = this.symfactory.getSymbolList();
    Context mContext;
    WaypointSymbolFactory symfactory;

    public WaypointSymbolAdapter(WaypointSymbolFactory factory, Context ctx) {
        this.symfactory = factory;
        this.mContext = ctx;
    }

    public int getCount() {
        return this.idlist.length;
    }

    public Object getItem(int pos) {
        return this.idlist[pos].key;
    }

    public long getItemId(int pos) {
        return (long) this.idlist[pos].value.intValue();
    }

    public View getView(int pos, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(this.mContext);
            imageView.setLayoutParams(new AbsListView.LayoutParams(58, 58));
            imageView.setAdjustViewBounds(false);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setPadding(1, 1, 1, 1);
        } else {
            imageView = (ImageView) convertView;
        }
        imageView.setImageResource(this.idlist[pos].value.intValue());
        imageView.setTag(this.idlist[pos].key);
        return imageView;
    }
}
