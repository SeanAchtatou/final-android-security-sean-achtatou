package com.crittermap.backcountrynavigator.waypoint;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Navigator;
import com.crittermap.backcountrynavigator.nav.Position;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicReference;

public class WaypointFilterQueryProvider implements FilterQueryProvider, Observer, TextWatcher {
    static final String WAYPOINTSQUERY = "SELECT PointID as _id,Longitude,Latitude,Elevation,Name,Description,SymbolName,ttime FROM WayPoints";
    WaypointListAdapter adapter = null;
    AtomicReference<BCNMapDatabase> database = new AtomicReference<>(null);
    Context mContext;
    EditText mFilterEdit = null;
    TextView mListLabel = null;
    ListView mListView = null;
    private Location mLocation;
    private Navigator mNavigator;
    Position mPosition;
    private WaypointSymbolFactory mSymbolFactory;

    public WaypointFilterQueryProvider(Context context, View parentView, Navigator nav, WaypointSymbolFactory factory) {
        this.mNavigator = nav;
        if (this.mNavigator != null) {
            this.mNavigator.addObserver(this);
            this.mPosition = this.mNavigator.getRecentPosition();
        }
        this.mSymbolFactory = factory;
        this.mContext = context;
        if (parentView != null) {
            setupView(parentView);
        }
    }

    private void setupView(View parentView) {
        this.mFilterEdit = (EditText) parentView.findViewById(R.id.list_filter);
        this.mFilterEdit.addTextChangedListener(this);
        this.mListView = (ListView) parentView.findViewById(R.id.waypoint_list);
        this.mListLabel = (TextView) parentView.findViewById(R.id.message_list);
    }

    /* Debug info: failed to restart local var, previous not found, register: 17 */
    public Cursor runQuery(CharSequence paramCharSequence) {
        if (this.database.get() == null) {
            Log.w("WayPointFilter", "Unexpected: bdb==null");
            return null;
        }
        try {
            StringBuilder buffer = new StringBuilder(WAYPOINTSQUERY);
            String[] strArr = null;
            if (paramCharSequence != null && paramCharSequence.length() > 0) {
                buffer.append(" WHERE Name LIKE '%");
                buffer.append(paramCharSequence.toString());
                buffer.append("%' OR Description LIKE '%");
                buffer.append(paramCharSequence.toString());
                buffer.append("%'");
            }
            if (this.mPosition != null) {
                double ratio = Math.pow(Math.cos(Math.toRadians(this.mPosition.lat)), 2.0d);
                double lon = this.mPosition.lon;
                double lat = this.mPosition.lat;
                buffer.append(" ORDER BY ");
                buffer.append(ratio);
                buffer.append("*(");
                buffer.append(lon);
                buffer.append("-Longitude)*(");
                buffer.append(lon);
                buffer.append("-Longitude) + (");
                buffer.append(lat);
                buffer.append("-Latitude)*(");
                buffer.append(lat);
                buffer.append("-Latitude) LIMIT 40");
            } else {
                buffer.append(" LIMIT 40");
            }
            return this.database.get().getDb().rawQuery(buffer.toString(), null);
        } catch (Exception e) {
            return null;
        }
    }

    public void setDatabase(BCNMapDatabase bdb) {
        if (bdb == null) {
            this.mListLabel.setText(this.mContext.getString(R.string.list_header, ""));
            this.database.set(null);
            if (this.adapter != null) {
                this.mFilterEdit.setText("");
                this.adapter.getFilter().filter(null);
                this.adapter = null;
                if (this.mListView != null) {
                    this.mListView.setAdapter((ListAdapter) null);
                    return;
                }
                return;
            }
            return;
        }
        this.database.set(bdb);
        this.mListLabel.setText(this.mContext.getString(R.string.list_header, bdb.getName()));
        if (this.adapter != null) {
            this.mFilterEdit.setText("");
            this.adapter.getFilter().filter(null);
        } else if (runQuery(null) != null) {
            this.adapter = new WaypointListAdapter(this.mContext, runQuery(null), true, this.mSymbolFactory);
            this.adapter.setFilterQueryProvider(this);
            this.adapter.centerPosition = this.mPosition;
            if (this.mListView != null) {
                this.mListView.setAdapter((ListAdapter) this.adapter);
            }
        }
    }

    public void update(Observable paramObservable, Object data) {
        if (data.equals(Navigator.LOCATION)) {
            this.mLocation = this.mNavigator.getCurrentLocation();
            this.mPosition = new Position(this.mLocation.getLongitude(), this.mLocation.getLatitude());
            if (this.adapter != null) {
                this.adapter.centerPosition = this.mPosition;
                this.adapter.getFilter().filter(this.mFilterEdit.getText());
            }
        }
    }

    public void refresh() {
        if (this.adapter != null) {
            this.adapter.getFilter().filter(this.mFilterEdit.getText());
        }
    }

    public void afterTextChanged(Editable paramEditable) {
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        if (this.adapter != null) {
            this.adapter.getFilter().filter(paramCharSequence);
        }
    }

    public void onDestroy() {
        if (this.adapter != null) {
            this.adapter.changeCursor(null);
            this.adapter = null;
        }
    }
}
