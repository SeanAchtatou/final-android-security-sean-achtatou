package com.crittermap.backcountrynavigator.waypoint;

import android.content.Context;
import android.util.Log;
import com.crittermap.backcountrynavigator.R;
import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

public class WaypointSymbolFactory {
    boolean loaded = false;
    ConcurrentHashMap<String, Integer> mapSymbols = new ConcurrentHashMap<>();
    String[] msymbolsAvailable;
    final String[][] waypointmapping = {new String[]{"Airport", "airport"}, new String[]{"Amusement Park", "amusement"}, new String[]{"Anchor", "port"}, new String[]{"Anchor Prohibited", "ancprohib"}, new String[]{"Animal Tracks", "animal tracks"}, new String[]{"ATV", "atv"}, new String[]{"Bait and Tackle", "bait and tackle"}, new String[]{"Ball Park", "sports"}, new String[]{"Bank", "bank"}, new String[]{"Bar", "bar"}, new String[]{"Beach", "beach"}, new String[]{"Beacon", "beacon"}, new String[]{"Bell", "bell"}, new String[]{"Big Game", "deer"}, new String[]{"Bike Trail", "cycle"}, new String[]{"Blind", "blind"}, new String[]{"Block, Blue", "blockblue"}, new String[]{"Block, Green", "blockgreen"}, new String[]{"Block, Red", "blockRed"}, new String[]{"Blood Trail", "blood"}, new String[]{"Boat Ramp", "boatramp"}, new String[]{"Bowling", "bowling"}, new String[]{"Bridge", "bridge"}, new String[]{"Building", "building"}, new String[]{"Buoy, White", "whitebuoy"}, new String[]{"Campground", "campground"}, new String[]{"Car", "car"}, new String[]{"Car Rental", "car rental"}, new String[]{"Car Repair", "car repair"}, new String[]{"Cemetery", "cemetery"}, new String[]{"Church", "church"}, new String[]{"Circle with X", "circlex"}, new String[]{"Circle, Blue", "circleblue"}, new String[]{"Circle, Green", "circlegreen"}, new String[]{"Circle, Red", "circlered"}, new String[]{"City (Capitol)", "capitol"}, new String[]{"City (Large)", "citylarge"}, new String[]{"City (Medium)", "citymedium"}, new String[]{"City (Small)", "citysmall"}, new String[]{"City Hall", "civil"}, new String[]{"Civil", "civil"}, new String[]{"Coast Guard", "coastguard"}, new String[]{"Contact, Afro", "afro"}, new String[]{"Contact, Alien", "alien"}, new String[]{"Contact, Ball Cap", "ballcap"}, new String[]{"Contact, Big Ears", "bigears"}, new String[]{"Contact, Biker", "biker"}, new String[]{"Contact, Blonde", "blonde"}, new String[]{"Contact, Bug", "bug"}, new String[]{"Contact, Cat", "cat"}, new String[]{"Contact, Clown", "clown"}, new String[]{"Contact, Dog", "dog"}, new String[]{"Contact, Dreadlocks", "dreadlocks"}, new String[]{"Contact, Female1", "female1"}, new String[]{"Contact, Female2", "female2"}, new String[]{"Contact, Female3", "female3"}, new String[]{"Contact, Glasses", "glasses"}, new String[]{"Contact, Goatee", "goatee"}, new String[]{"Contact, Kung-Fu", "kungfu"}, new String[]{"Contact, Panda", "panda"}, new String[]{"Contact, Pig", "pig"}, new String[]{"Contact, Pirate", "pirate"}, new String[]{"Contact, Ranger", "ranger"}, new String[]{"Contact, Smiley", "smiley"}, new String[]{"Contact, Spike", "spike"}, new String[]{"Contact, Sumo", "sumo"}, new String[]{"Controlled Area", "conarea"}, new String[]{"Convenience Store", "cons"}, new String[]{"Cover", "cover"}, new String[]{"Covey", "covey"}, new String[]{"Crossing", "crossing"}, new String[]{"Custom 0", ""}, new String[]{"Custom 1", ""}, new String[]{"Custom 2", ""}, new String[]{"Custom 3", ""}, new String[]{"Custom 4", ""}, new String[]{"Custom 5", ""}, new String[]{"Custom 6", ""}, new String[]{"Custom 7", ""}, new String[]{"Dam", "dam"}, new String[]{"Danger Area", "caution"}, new String[]{"Department Store", "deptstore"}, new String[]{"Diamond, Blue", "diamondBlue"}, new String[]{"Diamond, Green", "diamondGreen"}, new String[]{"Diamond, Red", "diamondRed"}, new String[]{"Diver Down Flag 1", "dflag1"}, new String[]{"Diver Down Flag 2", "dflag2"}, new String[]{"Dock", "dock"}, new String[]{"Dot, White", "dotWhite"}, new String[]{"Drinking Water", "water"}, new String[]{"Dropoff", "cliff"}, new String[]{"Exit", "exit"}, new String[]{"Fast Food", "fast_food"}, new String[]{"Fishing Area", "fish"}, new String[]{"Fishing Hot Spot Facility", "fishing"}, new String[]{"Fitness Center", "fitness"}, new String[]{"Flag", "flag"}, new String[]{"Flag, Blue", "flagblue"}, new String[]{"Flag, Green", "flaggreen"}, new String[]{"Flag, Red", "flagred"}, new String[]{"Food Source", "food"}, new String[]{"Forest", "forest"}, new String[]{"Furbearer", "fur"}, new String[]{"Gas Station", "fuel"}, new String[]{"Geocache Found", "geocache_found"}, new String[]{"Geocache", "geocache"}, new String[]{"Ghost Town", "ghost"}, new String[]{"Glider Area", "glider"}, new String[]{"Golf Course", "golf"}, new String[]{"Ground Transportation", "grtrans"}, new String[]{"Heliport", "heliport"}, new String[]{"Horn", "cape"}, new String[]{"Hunting Area", "hunting"}, new String[]{"Ice Skating", "ice_skate"}, new String[]{"Information", "info"}, new String[]{"Letter A, Blue", "ablue"}, new String[]{"Letter A, Green", "agreen"}, new String[]{"Letter A, Red", "ared"}, new String[]{"Letter B, Blue", "bblue"}, new String[]{"Letter B, Green", "bgreen"}, new String[]{"Letter B, Red", "bred"}, new String[]{"Letter C, Blue", "cblue"}, new String[]{"Letter C, Green", "cgreen"}, new String[]{"Letter C, Red", "cred"}, new String[]{"Letter D, Blue", "dblue"}, new String[]{"Letter D, Green", "dgreen"}, new String[]{"Letter D, Red", "dred"}, new String[]{"Letterbox Cache", "ltrcache"}, new String[]{"Levee", "levee"}, new String[]{"Library", "library"}, new String[]{"Light", "light"}, new String[]{"Live Theater", "theatre"}, new String[]{"Lodge", "lodge"}, new String[]{"Lodging", "lodge"}, new String[]{"Man Overboard", "overbrd"}, new String[]{"Marina", "marina"}, new String[]{"Medical Facility", "hospital"}, new String[]{"Mile Marker", "milemarker"}, new String[]{"Military", "military"}, new String[]{"Mine", "mine"}, new String[]{"Movie Theater", "theatre"}, new String[]{"Multi-Cache", "multicache"}, new String[]{"Museum", "museum"}, new String[]{"Navaid, Amber", "navamber"}, new String[]{"Navaid, Black", "navblack"}, new String[]{"Navaid, Blue", "navblue"}, new String[]{"Navaid, Green", "navgreen"}, new String[]{"Navaid, Green/Red", "navgr"}, new String[]{"Navaid, Green/White", "navgw"}, new String[]{"Navaid, Orange", "navorange"}, new String[]{"Navaid, Red", "navred"}, new String[]{"Navaid, Red/Green", "navrg"}, new String[]{"Navaid, Violet", "navviolet"}, new String[]{"Navaid, White", "navwhite"}, new String[]{"Navaid, White/Green", "navwg"}, new String[]{"Navaid, White/Red", "navwr"}, new String[]{"Navaid, Red/White", "navrw"}, new String[]{"Number 0, Blue", "nzeroblue"}, new String[]{"Number 0, Green", "nzerogreen"}, new String[]{"Number 0, Red", "nzerored"}, new String[]{"Number 1, Blue", "oneblue"}, new String[]{"Number 1, Green", "onegreen"}, new String[]{"Number 1, Red", "onered"}, new String[]{"Number 2, Blue", "twoblue"}, new String[]{"Number 2, Green", "twogreen"}, new String[]{"Number 2, Red", "twored"}, new String[]{"Number 3, Blue", "threeblue"}, new String[]{"Number 3, Green", "threegreen"}, new String[]{"Number 3, Red", "threered"}, new String[]{"Number 4, Blue", "fourblue"}, new String[]{"Number 4, Green", "fourgreen"}, new String[]{"Number 4, Red", "fourred"}, new String[]{"Number 5, Blue", "fiveblue"}, new String[]{"Number 5, Green", "fivegreen"}, new String[]{"Number 5, Red", "fivered"}, new String[]{"Number 6, Blue", "sixblue"}, new String[]{"Number 6, Green", "sixgreen"}, new String[]{"Number 6, Red", "sixred"}, new String[]{"Number 7, Blue", "sevenblue"}, new String[]{"Number 7, Green", "sevengreen"}, new String[]{"Number 7, Red", "sevenred"}, new String[]{"Number 8, Blue", "eightblue"}, new String[]{"Number 8, Green", "eightgreen"}, new String[]{"Number 8, Red", "eightred"}, new String[]{"Number 9, Blue", "nineblue"}, new String[]{"Number 9, Green", "ninegreen"}, new String[]{"Number 9, Red", "ninered"}, new String[]{"Oil Field", "oil_field"}, new String[]{"Oval, Blue", "ovalblue"}, new String[]{"Oval, Green", "ovalgreen"}, new String[]{"Oval, Red", "ovalred"}, new String[]{"Parachute Area", "parachute area"}, new String[]{"Park", "park"}, new String[]{"Parking Area", "parking"}, new String[]{"Pharmacy", "pharmacy"}, new String[]{"Picnic Area", "picnic"}, new String[]{"Pin, Blue", "pinBlue"}, new String[]{"Pin, Green", "pinGreen"}, new String[]{"Pin, Red", "pinRed"}, new String[]{"Pizza", "pizza"}, new String[]{"Police Station", "police station"}, new String[]{"Post Office", "po"}, new String[]{"Private Field", "private"}, new String[]{"Puzzle Cache", "puzzle"}, new String[]{"Radio Beacon", "radio beacon"}, new String[]{"Rectangle, Blue", "rectangleBlue"}, new String[]{"Rectangle, Green", "rectangleGreen"}, new String[]{"Rectangle, Red", "rectangleRed"}, new String[]{"Reef", "reef"}, new String[]{"Residence", "house"}, new String[]{"Restaurant", "restaurant"}, new String[]{"Restricted Area", "restricted"}, new String[]{"Restroom", "rest_room"}, new String[]{"RV Park", "rv"}, new String[]{"Scales", "scales"}, new String[]{"School", "school"}, new String[]{"Seaplane Base", "seaplane base"}, new String[]{"Scenic Area", "scenic area"}, new String[]{"Shipwreck", "wreck"}, new String[]{"Shopping Center", "shop"}, new String[]{"Short Tower", "tower"}, new String[]{"Shower", "shower"}, new String[]{"Ski Resort", "ski"}, new String[]{"Skiing Area", "ski"}, new String[]{"Skull and Crossbones", "skulls"}, new String[]{"Small Game", "sgame"}, new String[]{"Soft Field", "softfld"}, new String[]{"Square, Blue", "squareBlue"}, new String[]{"Square, Green", "squareGreen"}, new String[]{"Square, Red", "squareRed"}, new String[]{"Stadium", "stadium"}, new String[]{"Stump", "stump"}, new String[]{"Summit", "summit"}, new String[]{"Swimming Area", "pool"}, new String[]{"Tall Tower", "tower"}, new String[]{"Telephone", "phone"}, new String[]{"Toll Booth", "toll booth"}, new String[]{"TracBack Point", "trackback"}, new String[]{"Trail Head", "trailhead"}, new String[]{"Tree Stand", "tstand"}, new String[]{"Treed Quarry", "treed"}, new String[]{"Triangle, Blue", "triangleblue"}, new String[]{"Triangle, Green", "trianglegreen"}, new String[]{"Triangle, Red", "trianglered"}, new String[]{"Truck", "truck"}, new String[]{"Truck Stop", "truck stop"}, new String[]{"Tunnel", "tunnel"}, new String[]{"Ultralight Area", "ultra"}, new String[]{"Upland Game", "upland"}, new String[]{"Water Hydrant", "hydrant"}, new String[]{"Water Source", "water"}, new String[]{"Waterfowl", "duck"}, new String[]{"Waypoint", "waypoint"}, new String[]{"Weed Bed", "weed"}, new String[]{"Winery", "winery"}, new String[]{"Wrecker", "wrecker"}, new String[]{"Zoo", "zoo"}, new String[]{"Mushroom", "mushroom"}, new String[]{"Plant", "plant"}};

    public WaypointSymbolFactory(Context ctx) {
    }

    public String[] getSymbolsAvailable() {
        if (!this.loaded) {
            loadSymbols();
        }
        return this.msymbolsAvailable;
    }

    public class SymbolEntry {
        String key;
        Integer value;

        public SymbolEntry() {
        }
    }

    class SymbolEntryComparator implements Comparator<SymbolEntry> {
        SymbolEntryComparator() {
        }

        public int compare(SymbolEntry paramT1, SymbolEntry paramT2) {
            return paramT1.value.compareTo(paramT2.value);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public SymbolEntry[] getSymbolList() {
        if (!this.loaded) {
            loadSymbols();
        }
        TreeSet<SymbolEntry> list = new TreeSet<>(new SymbolEntryComparator());
        for (Map.Entry<String, Integer> entry : this.mapSymbols.entrySet()) {
            SymbolEntry se = new SymbolEntry();
            se.key = (String) entry.getKey();
            se.value = (Integer) entry.getValue();
            list.add(se);
        }
        return (SymbolEntry[]) list.toArray(new SymbolEntry[0]);
    }

    private void loadSymbols() {
        for (Field field : R.drawable.class.getDeclaredFields()) {
            String xmlName = field.getName();
            if (xmlName.startsWith("wpt_")) {
                try {
                    this.mapSymbols.put(xmlName.split("_", 2)[1], Integer.valueOf(field.getInt(R.xml.class)));
                } catch (Exception e) {
                    Log.e("WayPointSymbolFactory", "Exceptions: " + e.getMessage());
                }
            }
        }
        for (String[] pair : this.waypointmapping) {
            if (this.mapSymbols.containsKey(pair[1])) {
                this.mapSymbols.put(pair[0].toLowerCase(), this.mapSymbols.get(pair[1]));
            }
        }
        this.msymbolsAvailable = (String[]) this.mapSymbols.keySet().toArray(new String[0]);
        this.loaded = true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Integer getSymbol(String key) {
        if (key == null) {
            return null;
        }
        if (!this.loaded) {
            loadSymbols();
        }
        String kel = key.toLowerCase();
        if (this.mapSymbols.containsKey(kel)) {
            return this.mapSymbols.get(kel);
        }
        return null;
    }
}
