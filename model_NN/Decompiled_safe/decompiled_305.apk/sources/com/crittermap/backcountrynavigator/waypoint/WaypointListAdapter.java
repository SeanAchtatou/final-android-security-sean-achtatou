package com.crittermap.backcountrynavigator.waypoint;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;

public class WaypointListAdapter extends CursorAdapter {
    Position centerPosition = null;
    final int idxDesc;
    final int idxElev;
    final int idxName;
    final int idxSymbol;
    final int idxTime;
    final int idxlat;
    final int idxlon;
    Location lastLocation;
    private LayoutInflater mInflater;
    Location mLocation;
    WaypointSymbolFactory mSymbolFactory;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void}
     arg types: [android.content.Context, android.database.Cursor, int]
     candidates:
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, int):void}
      ClspMth{android.widget.CursorAdapter.<init>(android.content.Context, android.database.Cursor, boolean):void} */
    public WaypointListAdapter(Context context, Cursor c, boolean autoRequery, WaypointSymbolFactory factory) {
        super(context, c, false);
        this.mSymbolFactory = factory;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.idxTime = c.getColumnIndexOrThrow("ttime");
        this.idxlon = c.getColumnIndexOrThrow("Longitude");
        this.idxlat = c.getColumnIndexOrThrow("Latitude");
        this.idxElev = c.getColumnIndexOrThrow("Elevation");
        this.idxName = c.getColumnIndexOrThrow("Name");
        this.idxDesc = c.getColumnIndexOrThrow("Description");
        this.idxSymbol = c.getColumnIndexOrThrow("SymbolName");
    }

    public void bindView(View paramView, Context paramContext, Cursor paramCursor) {
        fillData(paramView, paramContext, paramCursor);
    }

    public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup) {
        View view = this.mInflater.inflate((int) R.layout.waypoint_item, (ViewGroup) null);
        fillData(view, paramContext, paramCursor);
        return view;
    }

    /* access modifiers changed from: package-private */
    public double normalize(double bearingIn) {
        double bearingIn2 = Math.IEEEremainder(bearingIn, 360.0d);
        if (bearingIn2 < 0.0d) {
            return bearingIn2 + 360.0d;
        }
        return bearingIn2;
    }

    /* access modifiers changed from: package-private */
    public void fillData(View v, Context ctx, Cursor c) {
        String lengthUnit;
        TextView tDistance = (TextView) v.findViewById(R.id.waypointslist_item_distance);
        ImageView iSymbol = (ImageView) v.findViewById(R.id.waypointslist_item_icon);
        ((TextView) v.findViewById(R.id.waypointslist_item_name)).setText(c.getString(this.idxName));
        ((TextView) v.findViewById(R.id.waypointslist_item_description)).setText(c.getString(this.idxDesc));
        Integer symbolid = this.mSymbolFactory.getSymbol(c.getString(this.idxSymbol));
        if (symbolid != null) {
            iSymbol.setImageResource(symbolid.intValue());
        } else {
            iSymbol.setImageResource(R.drawable.wpt_triangle_red);
        }
        double lon = c.getDouble(this.idxlon);
        double lat = c.getDouble(this.idxlat);
        if (this.centerPosition != null) {
            float[] results = new float[3];
            Location.distanceBetween(this.centerPosition.lat, this.centerPosition.lon, lat, lon, results);
            double length = (double) results[0];
            double degree = normalize((double) results[1]);
            if (BCNSettings.MetricDisplay.get()) {
                if (length > 1000.0d) {
                    length /= 1000.0d;
                    lengthUnit = ctx.getString(R.string.kilometer);
                } else {
                    lengthUnit = ctx.getString(R.string.meter);
                }
            } else if (length > 1609.344d) {
                length /= 1609.344d;
                lengthUnit = ctx.getString(R.string.mile);
            } else {
                length *= 3.2808399d;
                lengthUnit = ctx.getString(R.string.feet);
            }
            tDistance.setText(String.format("%.2f %s @%.0fº", Double.valueOf(length), lengthUnit, Double.valueOf(degree)));
            return;
        }
        tDistance.setText("");
    }
}
