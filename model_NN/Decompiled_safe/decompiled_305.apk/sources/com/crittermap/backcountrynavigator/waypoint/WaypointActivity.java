package com.crittermap.backcountrynavigator.waypoint;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import com.crittermap.backcountrynavigator.R;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.view.WayPointView;

public class WaypointActivity extends Activity {
    private static final int CAMERA_REQUEST = 2020;
    public static final int REQUEST_WAYPOINT = 2000;
    public static final int RESULT_WAYPOINT_CANCELLED = 2001;
    public static final int RESULT_WAYPOINT_CENTER = 2004;
    public static final int RESULT_WAYPOINT_GOTO = 2003;
    public static final int RESULT_WAYPOINT_SAVED = 2002;
    private Button b_goto;
    private Button b_waypoint_delete;
    private ImageButton b_waypoint_symbol;
    /* access modifiers changed from: private */
    public BCNMapDatabase bdb;
    private final View.OnCreateContextMenuListener contextMenuListener = new View.OnCreateContextMenuListener() {
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            WaypointActivity.this.getMenuInflater().inflate(R.menu.waypointactioncontextmenu, menu);
        }
    };
    private String dbPath;
    private WayPointView mWView;
    /* access modifiers changed from: private */
    public WayPointView mWayPointView;
    private long mWaypointId;
    /* access modifiers changed from: private */
    public ImageButton optionsBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mWView = new WayPointView(this);
        setContentView(this.mWView);
        this.dbPath = getIntent().getStringExtra("DBFileName");
        this.bdb = BCNMapDatabase.openExisting(this.dbPath);
        this.mWaypointId = getIntent().getLongExtra("WaypointId", -1);
        if (this.mWaypointId < 0) {
            Log.w("WayPointActivity", "Waypoint intent was launched w/o id.");
            finish();
            return;
        }
        Cursor cW = this.bdb.getWayPoint(this.mWaypointId);
        this.mWView.setWayPoint(cW);
        cW.close();
        this.mWayPointView = this.mWView;
        this.optionsBtn = (ImageButton) findViewById(R.id.waypoint_show_options);
        this.optionsBtn.setOnCreateContextMenuListener(this.contextMenuListener);
        this.optionsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WaypointActivity.this.optionsBtn.performLongClick();
            }
        });
        this.b_waypoint_symbol = (ImageButton) this.mWayPointView.findViewById(R.id.waypoint_symbol_button);
        this.b_waypoint_symbol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long wid = WaypointActivity.this.mWayPointView.getWayPointID();
                if (wid != -1 && WaypointActivity.this.bdb != null) {
                    WaypointActivity.this.mWayPointView.showSymbolChooser(wid, WaypointActivity.this.bdb);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
            data.getData();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.optionsBtn.performLongClick();
        return true;
    }

    public boolean onMenuItemSelected(int featureId, final MenuItem item) {
        if (super.onMenuItemSelected(featureId, item)) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.itemListCtxCenter:
                this.mWayPointView.saveWayPoint(this.bdb);
                String label = this.mWayPointView.getLabel();
                Position pos = this.mWayPointView.getPosition();
                Intent result = new Intent();
                result.putExtra("Longitude", pos.lon);
                result.putExtra("Latitude", pos.lat);
                setResult(item.getItemId(), result);
                finish();
                return true;
            case R.id.itemListCtxGoto:
                this.mWayPointView.saveWayPoint(this.bdb);
                String destination = this.mWayPointView.getLabel();
                Position pos2 = this.mWayPointView.getPosition();
                Intent result2 = new Intent();
                result2.putExtra("Longitude", pos2.lon);
                result2.putExtra("Latitude", pos2.lat);
                result2.putExtra("Destination", destination);
                setResult(item.getItemId(), result2);
                finish();
                return true;
            case R.id.itemListCtxDelete:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.sculpt).setMessage((int) R.string.d_deletewaypoint_explanation).setPositiveButton((int) R.string.alert_dialog_okay, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        WaypointActivity.this.bdb.deleteWayPoint(WaypointActivity.this.mWayPointView.getWayPointID());
                        WaypointActivity.this.setResult(item.getItemId(), new Intent());
                        WaypointActivity.this.finish();
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
                return true;
            case R.id.itemListCtxSave:
                this.mWayPointView.saveWayPoint(this.bdb);
                setResult(item.getItemId(), new Intent());
                finish();
                return true;
            case R.id.itemListCtxCancel:
                setResult(item.getItemId(), new Intent());
                finish();
                return true;
            default:
                return true;
        }
    }
}
