package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.content.SearchRecentSuggestionsProvider;

public class SearchRecentPlacesProvider extends SearchRecentSuggestionsProvider {
    static final String AUTHORITY = "com.crittermap.backcountrynavigator.SuggestionProvider";
    static final int MODE = 1;

    public SearchRecentPlacesProvider() {
        setupSuggestions(AUTHORITY, 1);
    }

    public static String getAuthority(Context context) {
        return String.valueOf(context.getPackageName()) + ".SuggestionProvider";
    }
}
