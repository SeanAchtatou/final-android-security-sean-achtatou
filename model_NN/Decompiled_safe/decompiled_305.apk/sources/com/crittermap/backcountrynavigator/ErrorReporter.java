package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class ErrorReporter implements Thread.UncaughtExceptionHandler {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    private static ErrorReporter S_mInstance;
    String AndroidVersion;
    String Board;
    String Brand;
    private Context CurContext;
    HashMap<String, String> CustomParameters = new HashMap<>();
    String Device;
    String Display;
    String FilePath;
    String FingerPrint;
    String Host;
    String ID;
    String Manufacturer;
    String Model;
    String PackageName;
    String PhoneModel;
    private Thread.UncaughtExceptionHandler PreviousHandler;
    String Product;
    String Tags;
    long Time;
    String Type;
    String User;
    String VersionName;

    public void AddCustomData(String Key, String Value) {
        this.CustomParameters.put(Key, Value);
    }

    private String CreateCustomInfoString() {
        String CustomInfo = "";
        for (String CurrentKey : this.CustomParameters.keySet()) {
            CustomInfo = String.valueOf(CustomInfo) + CurrentKey + " = " + this.CustomParameters.get(CurrentKey) + "\n";
        }
        return CustomInfo;
    }

    static ErrorReporter getInstance() {
        if (S_mInstance == null) {
            S_mInstance = new ErrorReporter();
        }
        return S_mInstance;
    }

    public void Init(Context context) {
        this.PreviousHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.CurContext = context;
    }

    public void DeInit() {
        Thread.setDefaultUncaughtExceptionHandler(this.PreviousHandler);
        this.CurContext = null;
    }

    public long getAvailableInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
    }

    /* access modifiers changed from: package-private */
    public void RecoltInformations(Context context) {
        try {
            this.PhoneModel = Build.MODEL;
            this.AndroidVersion = Build.VERSION.RELEASE;
            this.Board = Build.BOARD;
            this.Brand = Build.BRAND;
            this.Device = Build.DEVICE;
            this.Display = Build.DISPLAY;
            this.FingerPrint = Build.FINGERPRINT;
            this.Host = Build.HOST;
            this.ID = Build.ID;
            this.Model = Build.MODEL;
            this.Product = Build.PRODUCT;
            this.Tags = Build.TAGS;
            this.Time = Build.TIME;
            this.Type = Build.TYPE;
            this.User = Build.USER;
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            this.VersionName = pi.versionName;
            this.PackageName = pi.packageName;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String CreateInformationString() {
        RecoltInformations(this.CurContext);
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "Version : " + this.VersionName) + "\n") + "Package : " + this.PackageName) + "\n") + "FilePath : " + this.FilePath) + "\n") + "Phone Model" + this.PhoneModel) + "\n") + "Android Version : " + this.AndroidVersion) + "\n") + "Board : " + this.Board) + "\n") + "Brand : " + this.Brand) + "\n") + "Device : " + this.Device) + "\n") + "Display : " + this.Display) + "\n") + "Finger Print : " + this.FingerPrint) + "\n") + "Host : " + this.Host) + "\n") + "ID : " + this.ID) + "\n") + "Model : " + this.Model) + "\n") + "Product : " + this.Product) + "\n") + "Tags : " + this.Tags) + "\n") + "Time : " + this.Time) + "\n") + "Type : " + this.Type) + "\n") + "User : " + this.User) + "\n") + "Total Internal memory : " + getTotalInternalMemorySize()) + "\n") + "Available Internal memory : " + getAvailableInternalMemorySize()) + "\n";
    }

    public void uncaughtException(Thread t, Throwable e) {
        PrintWriter pw = new PrintWriter(getErrorFile());
        pw.println("Error Report collected on : " + new Date().toString());
        pw.println("");
        pw.println("Informations :");
        pw.println("==============");
        pw.println("");
        pw.print(CreateInformationString());
        pw.println("Custom Informations :");
        pw.println("=====================");
        pw.println(CreateCustomInfoString());
        pw.println();
        pw.println("Stack : ");
        pw.println("=======");
        e.printStackTrace(pw);
        pw.println();
        pw.println("Cause : ");
        pw.println("======= ");
        for (Throwable cause = e.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(pw);
        }
        try {
            ArrayList<String> commandLine = new ArrayList<>();
            commandLine.add("logcat");
            commandLine.add("-d");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) commandLine.toArray(new String[0])).getInputStream()));
            pw.println("=======Logcat Log:======= ");
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                pw.println(line);
            }
        } catch (Exception e2) {
            Log.e("AndroidLogCollector", "CollectLog failed", e2);
        }
        pw.println("****  End of current Report ***");
        pw.close();
        this.PreviousHandler.uncaughtException(t, e);
    }

    /* access modifiers changed from: private */
    public void SendErrorMail(Context _context, String ErrorContent) {
        Intent sendIntent = new Intent("android.intent.action.SEND");
        String subject = _context.getResources().getString(R.string.CrashReport_MailSubject);
        sendIntent.putExtra("android.intent.extra.EMAIL", new String[]{"support@backcountrynavigator.com"});
        sendIntent.putExtra("android.intent.extra.TEXT", String.valueOf(_context.getResources().getString(R.string.CrashReport_MailBody)) + "\n\n" + ErrorContent + "\n\n");
        sendIntent.putExtra("android.intent.extra.SUBJECT", subject);
        sendIntent.setType("message/rfc822");
        _context.startActivity(Intent.createChooser(sendIntent, "Title:"));
    }

    private void SaveAsFile(String ErrorContent) {
        try {
            FileOutputStream trace = this.CurContext.openFileOutput("stack-" + new Random().nextInt(99999) + ".stacktrace", 0);
            trace.write(ErrorContent.getBytes());
            trace.close();
        } catch (Exception e) {
        }
    }

    private FileOutputStream getErrorFile() {
        try {
            return this.CurContext.openFileOutput("stack-" + new Random().nextInt(99999) + ".stacktrace", 0);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public String[] GetErrorFileList() {
        File dir = new File(String.valueOf(this.FilePath) + "/");
        dir.mkdir();
        return dir.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".stacktrace");
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean bIsThereAnyErrorFile() {
        return GetErrorFileList() != null && GetErrorFileList().length > 0;
    }

    public void CheckErrorAndSendMail(Context _context) {
        try {
            this.FilePath = _context.getFilesDir().getAbsolutePath();
            if (bIsThereAnyErrorFile()) {
                new ReportTask(_context);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ReportTask extends AsyncTask<Context, Integer, String> {
        Context _context;

        ReportTask(Context ctx) {
            this._context = ctx;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Context... params) {
            try {
                String absolutePath = this._context.getFilesDir().getAbsolutePath();
                if (ErrorReporter.this.bIsThereAnyErrorFile()) {
                    String WholeErrorText = "";
                    String FilePath = this._context.getFilesDir().getAbsolutePath();
                    String[] ErrorFileList = ErrorReporter.this.GetErrorFileList();
                    int length = ErrorFileList.length;
                    int i = 0;
                    int curIndex = 0;
                    while (i < length) {
                        String curString = ErrorFileList[i];
                        int curIndex2 = curIndex + 1;
                        if (curIndex <= 5) {
                            WholeErrorText = String.valueOf(String.valueOf(WholeErrorText) + "New Trace collected :\n") + "=====================\n ";
                            BufferedReader input = new BufferedReader(new FileReader(String.valueOf(FilePath) + "/" + curString));
                            while (true) {
                                String line = input.readLine();
                                if (line == null) {
                                    break;
                                }
                                WholeErrorText = String.valueOf(WholeErrorText) + line + "\n";
                            }
                            input.close();
                        }
                        new File(String.valueOf(FilePath) + "/" + curString).delete();
                        i++;
                        curIndex = curIndex2;
                    }
                    return WholeErrorText;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            if (result != null) {
                ErrorReporter.this.SendErrorMail(this._context, result);
            }
        }
    }
}
