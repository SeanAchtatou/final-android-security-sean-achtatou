package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.crittermap.backcountrynavigator.tiledb.TileDataBase;

public class MapServerDBTileRetriever implements TileRetriever {
    MapServer server;
    TileDataBase tiledb;

    public MapServerDBTileRetriever(MapServer mserver, String dbasePath) {
        this.server = mserver;
        this.tiledb = new TileDataBase(dbasePath, null);
    }

    public RenderableTile getTile(TileID tid) {
        byte[] bytes = this.tiledb.retrieveTile(tid);
        if (bytes != null) {
            return new RenderableTile(bytes);
        }
        RenderableTile rt = new RenderableTile((Bitmap) null);
        rt.setStatus(-1);
        return rt;
    }

    public int getTileSize() {
        return 256;
    }
}
