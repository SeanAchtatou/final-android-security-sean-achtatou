package com.crittermap.backcountrynavigator.tile;

import android.graphics.Rect;
import android.location.Location;
import com.crittermap.backcountrynavigator.nav.Position;
import java.nio.FloatBuffer;

public class TileResolverGeo implements TileResolver {
    static final int PIXPERTILE = 512;

    /* access modifiers changed from: package-private */
    public double segmentsx(int level) {
        return Math.pow(2.0d, (double) level);
    }

    /* access modifiers changed from: package-private */
    public double segmentsy(int level) {
        return Math.pow(2.0d, (double) (level - 1));
    }

    /* access modifiers changed from: package-private */
    public double latPerPixel(int level) {
        return 256.0d / (512.0d * segmentsy(level));
    }

    public double lonPerPixel(int level) {
        return 512.0d / (segmentsx(level) * 512.0d);
    }

    public CoordinateBoundingBox boundingBox(TileID tid) {
        double lonpertile = lonPerPixel(tid.level) * 512.0d;
        double latpertile = latPerPixel(tid.level) * 512.0d;
        return new CoordinateBoundingBox((((double) (tid.x - 1)) * lonpertile) - 256.0d, 128.0d - (((double) tid.y) * latpertile), (((double) tid.x) * lonpertile) - 256.0d, 128.0d - (((double) (tid.y - 1)) * latpertile));
    }

    public TileIDWithOffset findTileID(int level, double lon, double lat) {
        double multix = segmentsx(level);
        double multiy = segmentsy(level);
        double lonper = (256.0d + lon) / 512.0d;
        double latper = (128.0d - lat) / 256.0d;
        int y = ((int) Math.floor(latper * multiy)) + 1;
        int x = ((int) Math.floor(lonper * multix)) + 1;
        double remy = (latper * multiy) - Math.floor(latper * multiy);
        double remx = (lonper * multix) - Math.floor(lonper * multix);
        return new TileIDWithOffset(level, x, y, (int) Math.rint(512.0d * remx), (int) Math.rint(512.0d * remy));
    }

    public TileSet findTileSet(double clon, double clat, int zoomlevel, int width, int height, int srcZoom) {
        int Z;
        TileSet tileSet = new TileSet(width, height);
        int i = srcZoom;
        TileIDWithOffset tidwo = findTileID(i, clon - ((((double) width) / 2.0d) * lonPerPixel(zoomlevel)), clat + ((((double) height) / 2.0d) * latPerPixel(zoomlevel)));
        if (srcZoom < zoomlevel) {
            Z = (int) Math.pow(2.0d, (double) (zoomlevel - srcZoom));
        } else {
            Z = 1;
        }
        int M = (((tidwo.xpix * Z) + width) / (Z * 512)) + 1;
        int N = (((tidwo.ypix * Z) + height) / (Z * 512)) + 1;
        for (int i2 = 0; i2 < M; i2++) {
            for (int j = 0; j < N; j++) {
                int left = Z * ((-tidwo.xpix) + (i2 * 512));
                int top = Z * ((-tidwo.ypix) + (j * 512));
                tileSet.put(new TileID(srcZoom, tidwo.x + i2, tidwo.y + j), new Rect(left, top, left + (Z * 512), top + (Z * 512)));
            }
        }
        return tileSet;
    }

    public int pixelDistanceX(Position start, Position next, int level) {
        return (int) ((next.lon - start.lon) / lonPerPixel(level));
    }

    public int pixelDistanceY(Position start, Position next, int level) {
        return (int) ((start.lat - next.lat) / latPerPixel(level));
    }

    public Position shift(Position start, int xoff, int yoff, int level) {
        return new Position(start.lon + (lonPerPixel(level) * ((double) xoff)), start.lat - (latPerPixel(level) * ((double) yoff)));
    }

    public Rect findPixelRange(CoordinateBoundingBox box, Position center, int cx, int cy, int level) {
        int minx = pixFromLon(level, box.minlon);
        int maxy = pixFromLat(level, box.minlat);
        int miny = pixFromLat(level, box.maxlat);
        int maxx = pixFromLon(level, box.maxlon);
        int xoff = pixFromLon(level, center.lon) - cx;
        int yoff = pixFromLat(level, center.lat) - cy;
        return new Rect(minx - xoff, miny - yoff, maxx - xoff, maxy - yoff);
    }

    private int pixFromLat(int level, double lat) {
        return (int) Math.rint((128.0d - lat) / latPerPixel(level));
    }

    private int pixFromLon(int level, double lon) {
        return (int) Math.rint((256.0d + lon) / lonPerPixel(level));
    }

    public TileID[] findTileRange(CoordinateBoundingBox box, int level) {
        return new TileID[]{findTileID(level, box.minlon, box.maxlat), findTileID(level, box.maxlon, box.minlat)};
    }

    public float[] transformToScreen(float[] coord, int level, int W, int H, CoordinateBoundingBox box) {
        float[] answer = new float[coord.length];
        float minx = (float) pixFromLon(level, box.minlon);
        float miny = (float) pixFromLat(level, box.maxlat);
        for (int i = 0; i < coord.length; i += 2) {
            float x = (float) pixFromLon(level, (double) coord[i]);
            float y = (float) pixFromLat(level, (double) coord[i + 1]);
            answer[i] = x - minx;
            answer[i + 1] = y - miny;
        }
        return answer;
    }

    public CoordinateBoundingBox screenBoundingBox(Position center, int level, int width, int height) {
        return new CoordinateBoundingBox(shift(center, (-width) / 2, (-height) / 2, level), shift(center, width / 2, height / 2, level));
    }

    public float[] transformToScreen(FloatBuffer fb, int level, int W, int H, CoordinateBoundingBox box) {
        int outsize;
        int length = fb.capacity();
        int pts = length / 2;
        int rep = pts - 2;
        if (pts == 1) {
            outsize = 2;
        } else {
            outsize = (pts + rep) * 2;
        }
        float[] answer = new float[outsize];
        float minx = (float) pixFromLon(level, box.minlon);
        float miny = (float) pixFromLat(level, box.maxlat);
        int i = 0;
        int c = 0;
        while (i < length) {
            float x = (float) pixFromLon(level, (double) fb.get(i));
            float y = (float) pixFromLat(level, (double) fb.get(i + 1));
            int c2 = c + 1;
            answer[c] = x - minx;
            int c3 = c2 + 1;
            answer[c2] = y - miny;
            if (!(i == 0 || i == length - 2)) {
                int c4 = c3 + 1;
                answer[c3] = x - minx;
                c3 = c4 + 1;
                answer[c4] = y - miny;
            }
            i += 2;
            c = c3;
        }
        return answer;
    }

    public float getPixelWidthForDistance(Position center, int level, float distance) {
        double lonperpix = lonPerPixel(level);
        float[] results = new float[2];
        Location.distanceBetween(center.lat, center.lon, center.lat, center.lon + 1.0d, results);
        return (float) (((double) distance) / (((double) results[0]) * lonperpix));
    }

    public int getPixPerTile() {
        return 512;
    }
}
