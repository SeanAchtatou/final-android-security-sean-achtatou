package com.crittermap.backcountrynavigator.tile;

public class TileIDWithOffset extends TileID {
    final int xpix;
    final int ypix;

    public TileIDWithOffset(int level, int x, int y, int xpix2, int ypix2) {
        super(level, x, y);
        this.xpix = xpix2;
        this.ypix = ypix2;
    }
}
