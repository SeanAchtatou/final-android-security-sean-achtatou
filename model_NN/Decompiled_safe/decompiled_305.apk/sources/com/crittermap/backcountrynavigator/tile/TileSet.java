package com.crittermap.backcountrynavigator.tile;

import android.graphics.Rect;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TileSet extends HashMap<TileID, Rect> {
    private static final long serialVersionUID = 7465430044940224802L;
    Rect clipRect;
    int cx;
    int cy;

    public TileSet(int w, int h) {
        this.cx = w / 2;
        this.cy = h / 2;
        this.clipRect = new Rect(0, 0, w, h);
    }

    public List<TileID> getPrioritizedList() {
        Set<Map.Entry<TileID, Rect>> entrySet = entrySet();
        LinkedList<TileID> list = new LinkedList<>();
        for (Map.Entry<TileID, Rect> entry : entrySet) {
            list.add((TileID) entry.getKey());
        }
        Collections.sort(list, new TilePrioritizer());
        return list;
    }

    class TilePrioritizer implements Comparator<TileID> {
        TilePrioritizer() {
        }

        public int compare(TileID t1, TileID t2) {
            Rect ra = (Rect) TileSet.this.get(t1);
            Rect rb = (Rect) TileSet.this.get(t2);
            int ax = (ra.left + ra.right) / 2;
            int ay = (ra.top + ra.bottom) / 2;
            int bx = (rb.left + rb.right) / 2;
            int by = (rb.top + rb.bottom) / 2;
            return (((ax - TileSet.this.cx) * (ax - TileSet.this.cx)) + ((ay - TileSet.this.cy) * (ay - TileSet.this.cy))) - (((bx - TileSet.this.cx) * (bx - TileSet.this.cx)) + ((by - TileSet.this.cy) * (by - TileSet.this.cy)));
        }
    }
}
