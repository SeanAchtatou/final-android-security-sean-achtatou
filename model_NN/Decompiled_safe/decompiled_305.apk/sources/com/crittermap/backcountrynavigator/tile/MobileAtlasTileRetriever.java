package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import com.crittermap.backcountrynavigator.data.TileRepository;
import com.crittermap.backcountrynavigator.map.MobileAtlasServer;

public class MobileAtlasTileRetriever implements TileRetriever {
    String mLayerName;
    private TileResolver mTileResolver;
    TileRepository tileRepository;

    MobileAtlasTileRetriever(String atlasPath, String extension, String layername) {
        this.mTileResolver = new GMTileResolver();
        this.mLayerName = layername;
        this.tileRepository = new TileRepository(atlasPath, extension);
    }

    public MobileAtlasTileRetriever(MobileAtlasServer server) {
        this(server.getAtlasPath(), server.getSuffix(), server.getLayer());
    }

    public TileResolver getTileResolver() {
        return this.mTileResolver;
    }

    public RenderableTile getTile(TileID tid) {
        byte[] bytes = this.tileRepository.retrieveTile(this.mLayerName, tid);
        if (bytes != null) {
            return new RenderableTile(bytes);
        }
        RenderableTile rt = new RenderableTile((Bitmap) null);
        rt.setStatus(-1);
        return rt;
    }

    public int getTileSize() {
        return this.mTileResolver.getPixPerTile();
    }
}
