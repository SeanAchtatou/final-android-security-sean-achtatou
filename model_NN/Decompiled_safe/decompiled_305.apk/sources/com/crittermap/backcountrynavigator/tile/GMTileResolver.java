package com.crittermap.backcountrynavigator.tile;

import android.graphics.Rect;
import android.location.Location;
import com.crittermap.backcountrynavigator.nav.Position;
import java.nio.FloatBuffer;

public class GMTileResolver implements TileResolver {
    static int PIXPERTILE = 256;

    public TileIDWithOffset findTileID(int level, double lon, double lat) {
        int xpix = pixFromLon(level, lon);
        int ypix = pixFromLat(level, lat);
        int xoff = xpix % PIXPERTILE;
        int yoff = ypix % PIXPERTILE;
        return new TileIDWithOffset(level, xpix / PIXPERTILE, ypix / PIXPERTILE, xoff, yoff);
    }

    private int pixFromLat(int level, double lat) {
        return GMercator.LatToY(lat, level);
    }

    private int pixFromLon(int level, double lon) {
        return GMercator.LonToX(lon, level);
    }

    public Position shift(Position start, int xoff, int yoff, int level) {
        return new Position(GMercator.XToLon(GMercator.LonToX(start.lon, level) + xoff, level), GMercator.YToLat(GMercator.LatToY(start.lat, level) + yoff, level));
    }

    public CoordinateBoundingBox boundingBox(TileID tid) {
        double maxlat = GMercator.YToLat(tid.y * PIXPERTILE, tid.level);
        return new CoordinateBoundingBox(GMercator.XToLon(tid.x * PIXPERTILE, tid.level), GMercator.YToLat((tid.y + 1) * PIXPERTILE, tid.level), GMercator.XToLon((tid.x + 1) * PIXPERTILE, tid.level), maxlat);
    }

    /* access modifiers changed from: package-private */
    public double getNormalizedLon(double lon) {
        double lng = lon;
        if (lng > 180.0d) {
            lng -= 360.0d;
        }
        return (lng / 360.0d) + 0.5d;
    }

    /* access modifiers changed from: package-private */
    public double getNormalizedLat(double lat) {
        return 0.5d - ((Math.log(Math.tan(0.7853981633974483d + Math.toRadians(0.5d * lat))) / 3.141592653589793d) / 2.0d);
    }

    public TileSet findTileSet(double clon, double clat, int zoomlevel, int width, int height, int srcZoom) {
        int Z;
        TileSet tileSet = new TileSet(width, height);
        Position corner = shift(new Position(clon, clat), (-width) / 2, (-height) / 2, zoomlevel);
        TileIDWithOffset tidwo = findTileID(srcZoom, corner.lon, corner.lat);
        if (srcZoom < zoomlevel) {
            Z = (int) Math.pow(2.0d, (double) (zoomlevel - srcZoom));
        } else {
            Z = 1;
        }
        int M = (((tidwo.xpix * Z) + width) / (PIXPERTILE * Z)) + 1;
        int N = (((tidwo.ypix * Z) + height) / (PIXPERTILE * Z)) + 1;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                int left = Z * ((-tidwo.xpix) + (PIXPERTILE * i));
                int right = left + (PIXPERTILE * Z);
                int top = Z * ((-tidwo.ypix) + (PIXPERTILE * j));
                tileSet.put(new TileID(srcZoom, tidwo.x + i, tidwo.y + j), new Rect(left, top, right, top + (PIXPERTILE * Z)));
            }
        }
        return tileSet;
    }

    public int pixelDistanceX(Position start, Position next, int level) {
        return pixFromLon(level, next.lon) - pixFromLon(level, start.lon);
    }

    public int pixelDistanceY(Position start, Position next, int level) {
        return pixFromLat(level, next.lat) - pixFromLat(level, start.lat);
    }

    public Rect findPixelRange(CoordinateBoundingBox box, Position center, int cx, int cy, int level) {
        int minx = pixFromLon(level, box.minlon);
        int maxy = pixFromLat(level, box.minlat);
        int miny = pixFromLat(level, box.maxlat);
        int maxx = pixFromLon(level, box.maxlon);
        int xoff = pixFromLon(level, center.lon) - cx;
        int yoff = pixFromLat(level, center.lat) - cy;
        return new Rect(minx - xoff, miny - yoff, maxx - xoff, maxy - yoff);
    }

    public TileID[] findTileRange(CoordinateBoundingBox box, int level) {
        return new TileID[]{findTileID(level, box.minlon, box.maxlat), findTileID(level, box.maxlon, box.minlat)};
    }

    static class GMercator {
        public static final double MAX_LAT = 85.05112877980659d;
        public static final double MIN_LAT = -85.05112877980659d;
        private static int TILE_SIZE = 256;

        GMercator() {
        }

        public static double radius(int aZoomlevel) {
            return ((double) (TILE_SIZE * (1 << aZoomlevel))) / 6.283185307179586d;
        }

        public static int getMaxPixels(int aZoomlevel) {
            return TILE_SIZE * (1 << aZoomlevel);
        }

        public static int falseEasting(int aZoomlevel) {
            return getMaxPixels(aZoomlevel) / 2;
        }

        public static int falseNorthing(int aZoomlevel) {
            return (getMaxPixels(aZoomlevel) * -1) / 2;
        }

        public static int LonToX(double aLongitude, int aZoomlevel) {
            return Math.min((int) ((radius(aZoomlevel) * Math.toRadians(aLongitude)) + ((double) falseEasting(aZoomlevel))), getMaxPixels(aZoomlevel) - 1);
        }

        public static int LatToY(double aLat, int aZoomlevel) {
            if (aLat < -85.05112877980659d) {
                aLat = -85.05112877980659d;
            } else if (aLat > 85.05112877980659d) {
                aLat = 85.05112877980659d;
            }
            double latitude = Math.toRadians(aLat);
            return Math.min((int) ((-1.0d * ((radius(aZoomlevel) / 2.0d) * Math.log((Math.sin(latitude) + 1.0d) / (1.0d - Math.sin(latitude))))) - ((double) falseNorthing(aZoomlevel))), getMaxPixels(aZoomlevel) - 1);
        }

        public static double XToLon(int aX, int aZoomlevel) {
            return Math.toDegrees(((double) (aX - falseEasting(aZoomlevel))) / radius(aZoomlevel));
        }

        public static double YToLat(int aY, int aZoomlevel) {
            return Math.toDegrees(1.5707963267948966d - (2.0d * Math.atan(Math.exp((((double) (aY + falseNorthing(aZoomlevel))) * -1.0d) / radius(aZoomlevel))))) * -1.0d;
        }
    }

    public float[] transformToScreen(float[] coord, int level, int W, int H, CoordinateBoundingBox box) {
        float[] answer = new float[coord.length];
        float minx = (float) pixFromLon(level, box.minlon);
        float miny = (float) pixFromLat(level, box.maxlat);
        for (int i = 0; i < coord.length; i += 2) {
            float x = (float) pixFromLon(level, (double) coord[i]);
            float y = (float) pixFromLat(level, (double) coord[i + 1]);
            answer[i] = x - minx;
            answer[i + 1] = y - miny;
        }
        return answer;
    }

    public CoordinateBoundingBox screenBoundingBox(Position center, int level, int width, int height) {
        return new CoordinateBoundingBox(shift(center, (-width) / 2, (-height) / 2, level), shift(center, width / 2, height / 2, level));
    }

    public float[] transformToScreen(FloatBuffer fb, int level, int W, int H, CoordinateBoundingBox box) {
        int outsize;
        int length = fb.capacity();
        int pts = length / 2;
        int rep = pts - 2;
        if (pts == 1) {
            outsize = 2;
        } else {
            outsize = (pts + rep) * 2;
        }
        float[] answer = new float[outsize];
        float minx = (float) pixFromLon(level, box.minlon);
        float miny = (float) pixFromLat(level, box.maxlat);
        int i = 0;
        int c = 0;
        while (i < length) {
            float x = (float) pixFromLon(level, (double) fb.get(i));
            float y = (float) pixFromLat(level, (double) fb.get(i + 1));
            int c2 = c + 1;
            answer[c] = x - minx;
            int c3 = c2 + 1;
            answer[c2] = y - miny;
            if (!(i == 0 || i == length - 2)) {
                int c4 = c3 + 1;
                answer[c3] = x - minx;
                c3 = c4 + 1;
                answer[c4] = y - miny;
            }
            i += 2;
            c = c3;
        }
        return answer;
    }

    public float getPixelWidthForDistance(Position center, int level, float distance) {
        double pixperlon = (double) (pixFromLon(level, center.lon + 1.0d) - pixFromLon(level, center.lon));
        float[] results = new float[2];
        Location.distanceBetween(center.lat, center.lon, center.lat, center.lon + 1.0d, results);
        return (float) (((double) distance) / (((double) results[0]) / pixperlon));
    }

    public int getPixPerTile() {
        return PIXPERTILE;
    }
}
