package com.crittermap.backcountrynavigator.tile;

import com.crittermap.backcountrynavigator.nav.Position;
import java.util.List;

public class CoordinateBoundingBox {
    public final double maxlat;
    public final double maxlon;
    public final double minlat;
    public final double minlon;

    public CoordinateBoundingBox(double minlon2, double minlat2, double maxlon2, double maxlat2) {
        this.minlon = minlon2;
        this.minlat = minlat2;
        this.maxlon = maxlon2;
        this.maxlat = maxlat2;
    }

    public CoordinateBoundingBox(Position p1, Position p2) {
        this.minlon = Math.min(p1.lon, p2.lon);
        this.minlat = Math.min(p1.lat, p2.lat);
        this.maxlon = Math.max(p1.lon, p2.lon);
        this.maxlat = Math.max(p1.lat, p2.lat);
    }

    public Position getCenter() {
        return new Position((this.minlon + this.maxlon) / 2.0d, (this.minlat + this.maxlat) / 2.0d);
    }

    public int getZoomLevel() {
        double d = this.maxlat - this.minlat;
        double lon = this.maxlon - this.minlon;
        return Math.min((int) Math.floor(9.0d - (Math.log(lon) / Math.log(2.0d))), (int) Math.floor(9.0d - (Math.log(lon) / Math.log(2.0d))));
    }

    public static double[] toArray(List<CoordinateBoundingBox> boxes) {
        double[] encoded = new double[(boxes.size() * 4)];
        int i = 0;
        for (CoordinateBoundingBox box : boxes) {
            int i2 = i + 1;
            encoded[i] = box.minlon;
            int i3 = i2 + 1;
            encoded[i2] = box.minlat;
            int i4 = i3 + 1;
            encoded[i3] = box.maxlon;
            i = i4 + 1;
            encoded[i4] = box.maxlat;
        }
        return encoded;
    }

    public static CoordinateBoundingBox[] fromArray(double[] d) {
        int size = d.length / 4;
        CoordinateBoundingBox[] result = new CoordinateBoundingBox[size];
        for (int i = 0; i < size; i++) {
            result[i] = new CoordinateBoundingBox(d[i * 4], d[(i * 4) + 1], d[(i * 4) + 2], d[(i * 4) + 3]);
        }
        return result;
    }
}
