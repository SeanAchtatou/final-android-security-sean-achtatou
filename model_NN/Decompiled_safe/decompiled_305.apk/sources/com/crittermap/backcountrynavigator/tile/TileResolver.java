package com.crittermap.backcountrynavigator.tile;

import android.graphics.Rect;
import com.crittermap.backcountrynavigator.nav.Position;
import java.nio.FloatBuffer;

public interface TileResolver {
    CoordinateBoundingBox boundingBox(TileID tileID);

    Rect findPixelRange(CoordinateBoundingBox coordinateBoundingBox, Position position, int i, int i2, int i3);

    TileIDWithOffset findTileID(int i, double d, double d2);

    TileID[] findTileRange(CoordinateBoundingBox coordinateBoundingBox, int i);

    TileSet findTileSet(double d, double d2, int i, int i2, int i3, int i4);

    int getPixPerTile();

    float getPixelWidthForDistance(Position position, int i, float f);

    int pixelDistanceX(Position position, Position position2, int i);

    int pixelDistanceY(Position position, Position position2, int i);

    CoordinateBoundingBox screenBoundingBox(Position position, int i, int i2, int i3);

    Position shift(Position position, int i, int i2, int i3);

    float[] transformToScreen(FloatBuffer floatBuffer, int i, int i2, int i3, CoordinateBoundingBox coordinateBoundingBox);

    float[] transformToScreen(float[] fArr, int i, int i2, int i3, CoordinateBoundingBox coordinateBoundingBox);
}
