package com.crittermap.backcountrynavigator.tile;

public interface TileRetriever {
    RenderableTile getTile(TileID tileID);

    int getTileSize();
}
