package com.crittermap.backcountrynavigator.tile;

import android.location.Location;
import com.crittermap.backcountrynavigator.nav.Position;
import com.crittermap.backcountrynavigator.settings.BCNSettings;

public class Ruler {
    private static final float MILESPERMETER = 6.213712E-4f;
    static final double[] RULERDISTANCESMETRIC = {1.0d, 2.0d, 5.0d, 10.0d, 100.0d, 500.0d, 1000.0d, 2000.0d, 4000.0d, 8000.0d, 16000.0d, 32000.0d, 64000.0d, 128000.0d, 256000.0d, 512000.0d, 1024000.0d, 2048000.0d, 4096000.0d, 8192000.0d, 1.6384E7d, 3.2768E7d};
    static final double[] RULERDISTANCESMILES = {0.01d, 0.02d, 0.05d, 0.1d, 0.25d, 0.5d, 1.0d, 2.0d, 4.0d, 8.0d, 16.0d, 32.0d, 64.0d, 128.0d, 256.0d, 512.0d, 1024.0d, 2048.0d, 4096.0d, 8192.0d, 16384.0d, 32768.0d};
    public double distance;
    public int pixels;

    /* JADX INFO: Multiple debug info for r9v1 com.crittermap.backcountrynavigator.nav.Position: [D('right' com.crittermap.backcountrynavigator.nav.Position), D('pos' com.crittermap.backcountrynavigator.nav.Position)] */
    /* JADX INFO: Multiple debug info for r9v3 float: [D('right' com.crittermap.backcountrynavigator.nav.Position), D('dist' float)] */
    /* JADX INFO: Multiple debug info for r12v1 boolean: [D('level' int), D('metric' boolean)] */
    public static Ruler getRuler(Position pos, int maxPixelWidth, int verticaloffset, int level, TileResolver resolver) {
        int i;
        Position left = resolver.shift(pos, (-maxPixelWidth) / 2, -verticaloffset, level);
        Position pos2 = resolver.shift(pos, maxPixelWidth / 2, -verticaloffset, level);
        float[] results = new float[1];
        Location.distanceBetween(left.lat, left.lon, pos2.lat, pos2.lon, results);
        float dist = results[0];
        boolean metric = BCNSettings.MetricDisplay.get();
        if (!metric) {
            dist *= 6.213712E-4f;
        }
        if (metric) {
            i = RULERDISTANCESMETRIC.length - 1;
            while (i > 0 && RULERDISTANCESMETRIC[i] >= ((double) dist)) {
                i--;
            }
        } else {
            int i2 = RULERDISTANCESMILES.length - 1;
            while (i > 0 && RULERDISTANCESMILES[i] >= ((double) dist)) {
                i2 = i - 1;
            }
        }
        Ruler ruler = new Ruler();
        if (!metric) {
            ruler.distance = RULERDISTANCESMILES[i];
        } else {
            ruler.distance = RULERDISTANCESMETRIC[i];
        }
        ruler.pixels = (int) ((((double) maxPixelWidth) * ruler.distance) / ((double) dist));
        return ruler;
    }
}
