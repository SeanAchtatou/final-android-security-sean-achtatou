package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class LabelTileRetriever implements TileRetriever {
    public RenderableTile getTile(TileID tid) {
        Bitmap labelledTile = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888);
        labelledTile.eraseColor(Color.rgb((tid.x % 8) * 32, (tid.y % 8) * 32, tid.x * tid.y * 32));
        String text = String.format("%d:%d,%d", Integer.valueOf(tid.level), Integer.valueOf(tid.x), Integer.valueOf(tid.y));
        Canvas canv = new Canvas(labelledTile);
        Paint textPaint = new Paint();
        textPaint.setARGB(127, 0, 0, 0);
        textPaint.setTextSize(30.0f);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.SERIF);
        canv.drawText(text, 128.0f, 128.0f, textPaint);
        try {
            Thread.sleep((long) (Math.random() * 3000.0d));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new RenderableTile(labelledTile);
    }

    public int getTileSize() {
        return 256;
    }
}
