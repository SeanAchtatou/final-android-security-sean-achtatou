package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class RenderableTile {
    public static final int ONFILE = 1;
    public static final int OUTOFRANGE = -1;
    static boolean PURGEABLE = false;
    public static final int READY = 0;
    public static final int UNABLETORETRIEVE = -2;
    static final BitmapFactory.Options options = new BitmapFactory.Options();
    Bitmap bitmap;
    byte[] data;
    String pathToBitmap;
    int status;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public Bitmap getBitmap() {
        if (this.bitmap != null) {
            return this.bitmap;
        }
        if (this.data != null) {
            try {
                Bitmap b = BitmapFactory.decodeByteArray(this.data, 0, this.data.length, options);
                if (PURGEABLE) {
                    this.bitmap = b;
                }
                return b;
            } catch (Exception e) {
                return null;
            } catch (OutOfMemoryError e2) {
                return null;
            }
        } else if (this.pathToBitmap == null) {
            return null;
        } else {
            try {
                Bitmap b2 = BitmapFactory.decodeFile(this.pathToBitmap, options);
                if (PURGEABLE) {
                    this.bitmap = b2;
                }
                return b2;
            } catch (Exception e3) {
                return null;
            } catch (OutOfMemoryError e4) {
                return null;
            }
        }
    }

    public byte[] getData() {
        return this.data;
    }

    static {
        try {
            BitmapFactory.Options.class.getField("inPurgeable").setBoolean(options, true);
            BitmapFactory.Options.class.getField("inInputShareable").setBoolean(options, true);
            PURGEABLE = true;
        } catch (NoSuchFieldException e) {
            Log.w("Renderabletile", "Unable to set BitMapFactory.Options", e);
            PURGEABLE = false;
        } catch (SecurityException e2) {
            Log.e("Renderabletile", "Unable to set BitMapFactory.Options", e2);
            PURGEABLE = false;
        } catch (IllegalArgumentException e3) {
            Log.e("Renderabletile", "Unable to set BitMapFactory.Options", e3);
            PURGEABLE = false;
        } catch (IllegalAccessException e4) {
            Log.e("Renderabletile", "Unable to set BitMapFactory.Options", e4);
            PURGEABLE = false;
        }
    }

    public RenderableTile(byte[] buffer) {
        this.bitmap = null;
        this.pathToBitmap = null;
        this.data = buffer;
    }

    public RenderableTile(String path) {
        this.bitmap = null;
        this.pathToBitmap = null;
        this.status = 0;
        this.pathToBitmap = path;
    }

    public RenderableTile(Bitmap b) {
        this.bitmap = null;
        this.pathToBitmap = null;
        this.bitmap = b;
    }

    public RenderableTile(int astatus) {
        this.bitmap = null;
        this.pathToBitmap = null;
        this.status = astatus;
        this.data = null;
        this.bitmap = null;
    }

    public void doneWithBitmap() {
        if (this.bitmap != null) {
            this.bitmap.recycle();
            this.bitmap = null;
        }
    }

    public void recycle() {
        if (this.bitmap != null) {
            this.bitmap.recycle();
            this.bitmap = null;
        }
    }
}
