package com.crittermap.backcountrynavigator.tile;

import com.crittermap.backcountrynavigator.settings.Versioning;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TileCache {
    public static final int NOT_AVAILABLE = 1;
    public static final int SUCCESSFUL_REQUEST = 0;
    final int CACHESIZE;
    List<TileID> lastAccess = Collections.synchronizedList(new LinkedList());
    TileResolver tileResolver;
    TileRetriever tileRetriever;
    ConcurrentHashMap<TileID, RenderableTile> tilesFound = new ConcurrentHashMap<>();

    interface TileListener {
        void onWantedTileReceived();
    }

    public TileCache(TileRetriever retriever, int csize) {
        this.tileRetriever = retriever;
        this.CACHESIZE = csize;
    }

    public TileCache(TileRetriever retriever) {
        this.tileRetriever = retriever;
        boolean bigTiles = retriever.getTileSize() == 512;
        int SDKNumber = Versioning.getSDKNumber();
        if (SDKNumber <= 3) {
            if (bigTiles) {
                this.CACHESIZE = 9;
            } else {
                this.CACHESIZE = 16;
            }
        } else if (SDKNumber >= 11) {
            if (bigTiles) {
                this.CACHESIZE = 25;
            } else {
                this.CACHESIZE = 64;
            }
        } else if (bigTiles) {
            this.CACHESIZE = 16;
        } else {
            this.CACHESIZE = 49;
        }
    }

    public TileResolver getTileResolver() {
        return this.tileResolver;
    }

    public void setTileResolver(TileResolver tileResolver2) {
        this.tileResolver = tileResolver2;
    }

    private void markUse(TileID tid) {
        this.lastAccess.remove(tid);
        this.lastAccess.add(tid);
    }

    public boolean hasTile(TileID tile) {
        if (!this.tilesFound.containsKey(tile) || this.tilesFound.get(tile).getStatus() != 0) {
            return false;
        }
        markUse(tile);
        return true;
    }

    public boolean purgeTile(TileID tid) {
        this.lastAccess.remove(tid);
        this.tilesFound.remove(tid);
        return true;
    }

    public void requestTiles(List<TileID> list) {
    }

    public int requestTile(TileID tid) {
        TileID key;
        RenderableTile rt;
        if (this.tilesFound.containsKey(tid) && this.tilesFound.get(tid).getStatus() == 0) {
            return 0;
        }
        if (!(this.tilesFound.size() < this.CACHESIZE || (key = this.lastAccess.remove(0)) == null || (rt = this.tilesFound.remove(key)) == null)) {
            rt.recycle();
        }
        this.tilesFound.put(tid, this.tileRetriever.getTile(tid));
        markUse(tid);
        return 0;
    }

    public RenderableTile getTile(TileID tileID) {
        RenderableTile t = this.tilesFound.get(tileID);
        if (t == null) {
            return null;
        }
        markUse(tileID);
        return t;
    }
}
