package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.util.Log;
import com.crittermap.backcountrynavigator.data.ErrorCollector;
import com.crittermap.backcountrynavigator.data.TileRepository;
import com.crittermap.backcountrynavigator.map.MapServer;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

public class MapServerTileRetriever implements TileRetriever {
    MapServer mapServer;
    TileRetryHandler retryHandler = new TileRetryHandler();
    TileRepository tileRepository = new TileRepository();

    class TileRetryHandler implements HttpRequestRetryHandler {
        TileRetryHandler() {
        }

        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (exception.getClass().isInstance(InterruptedIOException.class)) {
                return false;
            }
            return executionCount < 3;
        }
    }

    public MapServerTileRetriever(MapServer server) {
        this.mapServer = server;
    }

    public RenderableTile getTileOld(TileID tid) {
        ByteArrayOutputStream outstream;
        if (this.tileRepository.hasTile(this.mapServer.getShortName(), tid)) {
            return getTileFromRepository(tid);
        }
        try {
            String url = this.mapServer.getURLforTileID(tid);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            client.setHttpRequestRetryHandler(this.retryHandler);
            HttpConnectionParams.setSoTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
            HttpConnectionParams.setConnectionTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                new RenderableTile((Bitmap) null).setStatus(-2);
                Log.e("GetTile", String.valueOf(response.getStatusLine().getReasonPhrase()) + ":" + this.mapServer.getShortName() + " : " + tid.toString());
            }
            HttpEntity entity = response.getEntity();
            Header contentType = entity.getContentType();
            long length = entity.getContentLength();
            if (length > 0) {
                outstream = new ByteArrayOutputStream((int) length);
            } else {
                outstream = new ByteArrayOutputStream();
            }
            entity.writeTo(outstream);
            entity.consumeContent();
            outstream.flush();
            return new RenderableTile(outstream.toByteArray());
        } catch (Exception e) {
            RenderableTile rt = new RenderableTile(-2);
            Log.e("MapServerTileRetriever", Thread.currentThread().getName(), e);
            return rt;
        }
    }

    public RenderableTile getTile(TileID tid) {
        if (this.tileRepository.hasTile(this.mapServer.getShortName(), tid)) {
            return getTileFromRepository(tid);
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        try {
            if (retrieveIntoFile(this.mapServer.getShortName(), this.tileRepository, tid)) {
                return getTileFromRepository(tid);
            }
            return new RenderableTile(-2);
        } catch (Exception e) {
            Log.e("MapServerTileRetriever", String.valueOf(Thread.currentThread().getName()) + "- Exception downloading tile " + tid, e);
            return new RenderableTile(-2);
        }
    }

    public boolean retrieveToFile(String layerName, TileRepository repository, TileID tid) {
        try {
            return retrieveIntoFile(layerName, repository, tid);
        } catch (Exception ex) {
            Log.w("MapServerTileRetriever", "Exception downloading tile", ex);
            return false;
        } catch (OutOfMemoryError err) {
            Log.e("MapServerTileRetreiver", "OutOfMemoryError in tile download", err);
            return false;
        }
    }

    private boolean retrieveIntoFile(String layerName, TileRepository repository, TileID tid) throws IOException, ClientProtocolException {
        String url = this.mapServer.getURLforTileID(tid);
        DefaultHttpClient client = new DefaultHttpClient();
        int maxTotalConnections = ConnManagerParams.getMaxTotalConnections(client.getParams());
        new ConnPerRouteBean(9);
        HttpConnectionParams.setLinger(client.getParams(), 0);
        HttpGet request = new HttpGet(url);
        client.setHttpRequestRetryHandler(this.retryHandler);
        HttpConnectionParams.setSoTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
        HttpConnectionParams.setConnectionTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
        HttpConnectionParams.setSocketBufferSize(client.getParams(), 8192);
        HttpResponse response = client.execute(request);
        if (response.getStatusLine().getStatusCode() != 200) {
            Log.w("GetTile", String.valueOf(response.getStatusLine().getReasonPhrase()) + ":" + this.mapServer.getShortName() + " : " + tid.toString());
            return false;
        }
        HttpEntity entity = response.getEntity();
        long contentLength = entity.getContentLength();
        OutputStream outstream = repository.getOutputStream(layerName, tid);
        if (outstream != null) {
            entity.writeTo(outstream);
            entity.consumeContent();
            outstream.flush();
            outstream.close();
            ErrorCollector.fileSuccess();
            return true;
        }
        entity.consumeContent();
        return false;
    }

    /* access modifiers changed from: protected */
    public RenderableTile getTileFromRepository(TileID tid) {
        byte[] bytes = this.tileRepository.retrieveTile(this.mapServer.getShortName(), tid);
        if (bytes != null) {
            return new RenderableTile(bytes);
        }
        RenderableTile rt = new RenderableTile((Bitmap) null);
        rt.setStatus(-1);
        return rt;
    }

    public int getTileSize() {
        return this.mapServer.getTileResolver().getPixPerTile();
    }
}
