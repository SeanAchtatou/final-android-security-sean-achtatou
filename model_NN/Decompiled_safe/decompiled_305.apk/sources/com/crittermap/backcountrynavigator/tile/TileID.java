package com.crittermap.backcountrynavigator.tile;

public class TileID {
    public final int level;
    public final int x;
    public final int y;

    public String toString() {
        return "[" + this.level + ":" + this.x + "," + this.y + "]";
    }

    public int hashCode() {
        int i = 1 * 31;
        return ((((new Integer(this.level).hashCode() + 31) * 31) + new Integer(this.x).hashCode()) * 31) + new Integer(this.y).hashCode();
    }

    public boolean equals(Object o) {
        TileID t2 = (TileID) o;
        return this.level == t2.level && this.x == t2.x && this.y == t2.y;
    }

    public TileID(int level2, int x2, int y2) {
        this.level = level2;
        this.x = x2;
        this.y = y2;
    }
}
