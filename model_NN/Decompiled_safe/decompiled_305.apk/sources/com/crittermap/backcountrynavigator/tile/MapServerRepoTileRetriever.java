package com.crittermap.backcountrynavigator.tile;

import android.graphics.Bitmap;
import com.crittermap.backcountrynavigator.data.TileRepository;
import com.crittermap.backcountrynavigator.map.MapServer;

public class MapServerRepoTileRetriever implements TileRetriever {
    MapServer server;
    TileRepository tileRepository = new TileRepository();

    public MapServerRepoTileRetriever(MapServer mserver) {
        this.server = mserver;
    }

    public RenderableTile getTile(TileID tid) {
        byte[] bytes = this.tileRepository.retrieveTile(this.server.getShortName(), tid);
        if (bytes != null) {
            return new RenderableTile(bytes);
        }
        RenderableTile rt = new RenderableTile((Bitmap) null);
        rt.setStatus(-1);
        return rt;
    }

    public int getTileSize() {
        return this.server.getTileResolver().getPixPerTile();
    }
}
