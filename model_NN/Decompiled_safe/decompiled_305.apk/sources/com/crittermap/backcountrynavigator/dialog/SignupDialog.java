package com.crittermap.backcountrynavigator.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.crittermap.backcountrynavigator.R;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import rsg.mailchimp.api.MailChimpApiException;
import rsg.mailchimp.api.lists.ListMethods;

public class SignupDialog extends Dialog implements View.OnClickListener {
    public GoogleAnalyticsTracker tracker;

    public SignupDialog(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog);
        setTitle(getContext().getText(R.string.mc_dialog_title));
        ((Button) findViewById(R.id.SubscribeButton)).setOnClickListener(this);
        ((Button) findViewById(R.id.CancelButton)).setOnClickListener(this);
    }

    public void onClick(View clicked) {
        Log.d(getClass().getName(), "Clicked: " + clicked.toString());
        switch (clicked.getId()) {
            case R.id.SubscribeButton:
                final ProgressDialog progressDialog = ProgressDialog.show(getContext(), getContext().getResources().getText(R.string.mc_dialog_uploading_title), getContext().getResources().getText(R.string.mc_dialog_uploading_desc), true, false);
                new Thread(new Runnable() {
                    public void run() {
                        EditText text = (EditText) SignupDialog.this.findViewById(R.id.EMailField);
                        if (text.getText() != null && text.getText().toString().trim().length() > 0) {
                            SignupDialog.this.addToList(text.getText().toString(), progressDialog);
                        }
                    }
                }).start();
                return;
            case R.id.CancelButton:
                ((EditText) findViewById(R.id.EMailField)).setText("");
                cancel();
                return;
            default:
                Log.e("MailChimp", "Unable to handle onClick for view " + clicked.toString());
                return;
        }
    }

    /* access modifiers changed from: private */
    public void addToList(String emailAddy, ProgressDialog progressDialog) {
        String message;
        message = "Signup successful!";
        try {
            new ListMethods(getContext().getResources().getText(R.string.mc_api_key)).listSubscribe(getContext().getText(R.string.mc_list_id).toString(), emailAddy, null, null, true, null, null, true);
            if (this.tracker != null) {
                this.tracker.trackPageView("/newsletter/signup_successful");
            }
        } catch (MailChimpApiException e) {
            MailChimpApiException e2 = e;
            Log.e("MailChimp", "Exception subscribing person: " + e2.getMessage());
            message = "Signup failed: " + e2.getMessage();
        } finally {
            progressDialog.dismiss();
            dismiss();
            showResult(message);
        }
    }

    private void showResult(final String message) {
        getOwnerActivity().runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(SignupDialog.this.getContext());
                builder.setMessage(message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
        });
    }

    public static void showSignupDialog(Activity context, GoogleAnalyticsTracker tracker2) {
        SignupDialog dialog = new SignupDialog(context);
        dialog.tracker = tracker2;
        dialog.setOwnerActivity(context);
        dialog.show();
    }
}
