package com.crittermap.backcountrynavigator.dialog;

import android.content.Context;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.widget.Toast;
import com.crittermap.backcountrynavigator.R;
import java.io.File;

public class StorageDialogPreference extends EditTextPreference implements Preference.OnPreferenceChangeListener {
    public StorageDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        setOnPreferenceChangeListener(this);
        String text = getText();
        if (text == null || text.length() <= 0) {
            setText(Environment.getExternalStorageDirectory().getAbsolutePath());
        }
    }

    public StorageDialogPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        try {
            File f = new File((String) newValue);
            if (!f.exists()) {
                Toast.makeText(getContext(), (int) R.string.t_storage_invalid, 1).show();
                return false;
            } else if (!f.isDirectory()) {
                Toast.makeText(getContext(), (int) R.string.t_storage_invalid, 1).show();
                return false;
            } else if (f.canWrite()) {
                return true;
            } else {
                Toast.makeText(getContext(), (int) R.string.t_storage_invalid, 1).show();
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
