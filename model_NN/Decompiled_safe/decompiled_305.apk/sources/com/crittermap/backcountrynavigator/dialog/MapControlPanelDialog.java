package com.crittermap.backcountrynavigator.dialog;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.BackCountryActivity;
import com.crittermap.backcountrynavigator.R;

public class MapControlPanelDialog extends Activity implements CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnClickListener {
    private View mButtonChangeTrip;
    private View mButtonChooseMapSource;
    private View mButtonChooseMobileAtlas;
    private View mButtonNewTrip;
    private View mButtonSelectMapDownload;
    private CheckBox mCheckBox;
    private ArrayAdapter<String> mLayerAdapter;
    private TextView mOfflineSummary;
    private TextView mOfflineTitle;
    Intent mResultIntent = new Intent();
    private Spinner mSpinner;
    private boolean offline;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        setResult(R.id.map_cp_result, this.mResultIntent);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        boolean z2;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.control_panel);
        SharedPreferences preferences = getSharedPreferences(BackCountryActivity.PREFS_NAME, 0);
        this.mCheckBox = (CheckBox) findViewById(R.id.offlinecheckbox);
        this.mOfflineSummary = (TextView) findViewById(R.id.offlinesummary);
        this.mOfflineTitle = (TextView) findViewById(R.id.offlinetitle);
        this.mSpinner = (Spinner) findViewById(R.id.MapLayerSpinner);
        this.mSpinner.setOnItemSelectedListener(this);
        this.mLayerAdapter = new ArrayAdapter<>(this, 17367048);
        this.mLayerAdapter.setDropDownViewResource(17367049);
        this.mSpinner.setAdapter((SpinnerAdapter) this.mLayerAdapter);
        this.offline = getIntent().getBooleanExtra("offline", false);
        if (this.offline) {
            z = false;
        } else {
            z = true;
        }
        setPreviewSummary(z);
        CheckBox checkBox = this.mCheckBox;
        if (this.offline) {
            z2 = false;
        } else {
            z2 = true;
        }
        checkBox.setChecked(z2);
        this.mCheckBox.setOnCheckedChangeListener(this);
        String[] labels = preferences.getString("LayerLabelGroup", "").split("\\|");
        for (String lb : labels) {
            if (lb.length() > 0) {
                this.mLayerAdapter.add(lb);
            }
        }
        this.mButtonChooseMapSource = findViewById(R.id.cp_button_choose_map);
        this.mButtonChooseMapSource.setOnClickListener(this);
        this.mButtonChooseMobileAtlas = findViewById(R.id.cp_button_choose_atlas);
        this.mButtonChooseMobileAtlas.setOnClickListener(this);
        this.mButtonSelectMapDownload = findViewById(R.id.tb_select_for_download);
        this.mButtonSelectMapDownload.setOnClickListener(this);
    }

    /* access modifiers changed from: package-private */
    public void setPreviewSummary(boolean preview) {
        if (!preview) {
            this.mOfflineSummary.setText((int) R.string.cp_offline_summary);
            this.mOfflineTitle.setText((int) R.string.cp_offline_title);
            return;
        }
        this.mOfflineSummary.setText((int) R.string.cp_offline_off_summary);
        this.mOfflineTitle.setText((int) R.string.cp_preview_title);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked != (!this.offline)) {
            this.mResultIntent.putExtra("Preview", isChecked);
            setResult(R.id.map_cp_result, this.mResultIntent);
            setPreviewSummary(isChecked);
            finish();
        }
    }

    public void onItemSelected(AdapterView<?> adapterView, View arg1, int index, long arg3) {
        if (index != 0) {
            this.mResultIntent.putExtra("MapIndex", index);
            setResult(R.id.map_cp_result, this.mResultIntent);
            finish();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onClick(View paramView) {
        this.mResultIntent.putExtra("NextActivity", paramView.getId());
        setResult(R.id.map_cp_result, this.mResultIntent);
        finish();
    }
}
