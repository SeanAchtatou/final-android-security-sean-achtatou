package com.crittermap.backcountrynavigator.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.view.View;
import com.google.android.apps.mytracks.stats.MyTracksConstants;

public class ColorPickerDialog extends Dialog {
    private int mInitialColor;
    /* access modifiers changed from: private */
    public OnColorChangedListener mListener;

    public interface OnColorChangedListener {
        void colorChanged(int i);
    }

    private static class ColorPickerView extends View {
        private static final float PI = 3.1415925f;
        int CENTER_RADIUS = 32;
        int CENTER_X = 100;
        int CENTER_Y = 100;
        private Paint mCenterPaint;
        private Paint mCenterText;
        private final int[] mColors;
        private boolean mHighlightCenter;
        private OnColorChangedListener mListener;
        private Paint mPaint;
        private boolean mTrackingCenter;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
         arg types: [int, int, int[], ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
        ColorPickerView(Context c, OnColorChangedListener l, int color) {
            super(c);
            this.mListener = l;
            this.mColors = new int[]{-65536, -65281, -16776961, -16711681, -16711936, -256, -65536};
            Shader s = new SweepGradient(0.0f, 0.0f, this.mColors, (float[]) null);
            this.mPaint = new Paint(1);
            this.mPaint.setShader(s);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mPaint.setStrokeWidth(32.0f);
            this.mCenterPaint = new Paint(1);
            this.mCenterPaint.setColor(color);
            this.mCenterPaint.setStrokeWidth(5.0f);
            this.mCenterText = new Paint(1);
            this.mCenterText.setColor(-1);
            this.mCenterText.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
            this.mCenterText.setTextSize(35.0f);
            this.mCenterText.setTextAlign(Paint.Align.CENTER);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            float r = ((float) this.CENTER_X) - (this.mPaint.getStrokeWidth() * 0.5f);
            canvas.translate((float) this.CENTER_X, (float) this.CENTER_X);
            canvas.drawOval(new RectF(-r, -r, r, r), this.mPaint);
            canvas.drawCircle(0.0f, 0.0f, (float) this.CENTER_RADIUS, this.mCenterPaint);
            if (this.mTrackingCenter) {
                int c = this.mCenterPaint.getColor();
                this.mCenterPaint.setStyle(Paint.Style.STROKE);
                if (this.mHighlightCenter) {
                    this.mCenterPaint.setAlpha(255);
                } else {
                    this.mCenterPaint.setAlpha(MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS);
                }
                canvas.drawCircle(0.0f, 0.0f, ((float) this.CENTER_RADIUS) + this.mCenterPaint.getStrokeWidth(), this.mCenterPaint);
                this.mCenterPaint.setStyle(Paint.Style.FILL);
                this.mCenterPaint.setColor(c);
            }
            canvas.drawText("OK", 0.0f, -15.0f, this.mCenterText);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int d = Math.min(measure(widthMeasureSpec), measure(heightMeasureSpec));
            this.CENTER_X = d / 2;
            this.CENTER_Y = d / 2;
            this.CENTER_RADIUS = (d / 6) - 1;
            setMeasuredDimension(d, d);
        }

        private int measure(int measurespec) {
            int result = 0;
            int specMode = View.MeasureSpec.getMode(measurespec);
            int specSize = View.MeasureSpec.getSize(measurespec);
            if (specMode == 0) {
                return 200;
            }
            if (specMode == 1073741824) {
                result = specSize;
            } else if (specMode == Integer.MIN_VALUE) {
                result = specSize;
            }
            return result;
        }

        private int floatToByte(float x) {
            return Math.round(x);
        }

        private int pinToByte(int n) {
            if (n < 0) {
                return 0;
            }
            if (n > 255) {
                return 255;
            }
            return n;
        }

        private int ave(int s, int d, float p) {
            return Math.round(((float) (d - s)) * p) + s;
        }

        private int interpColor(int[] colors, float unit) {
            if (unit <= 0.0f) {
                return colors[0];
            }
            if (unit >= 1.0f) {
                return colors[colors.length - 1];
            }
            float p = unit * ((float) (colors.length - 1));
            int i = (int) p;
            float p2 = p - ((float) i);
            int c0 = colors[i];
            int c1 = colors[i + 1];
            return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
        }

        private int rotateColor(int color, float rad) {
            int r = Color.red(color);
            int g = Color.green(color);
            int b = Color.blue(color);
            ColorMatrix cm = new ColorMatrix();
            ColorMatrix tmp = new ColorMatrix();
            cm.setRGB2YUV();
            tmp.setRotate(0, (180.0f * rad) / 3.1415927f);
            cm.postConcat(tmp);
            tmp.setYUV2RGB();
            cm.postConcat(tmp);
            float[] a = cm.getArray();
            return Color.argb(Color.alpha(color), pinToByte(floatToByte((a[0] * ((float) r)) + (a[1] * ((float) g)) + (a[2] * ((float) b)))), pinToByte(floatToByte((a[5] * ((float) r)) + (a[6] * ((float) g)) + (a[7] * ((float) b)))), pinToByte(floatToByte((a[10] * ((float) r)) + (a[11] * ((float) g)) + (a[12] * ((float) b)))));
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0048  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r12) {
            /*
                r11 = this;
                r10 = 0
                r9 = 1
                float r5 = r12.getX()
                int r6 = r11.CENTER_X
                float r6 = (float) r6
                float r3 = r5 - r6
                float r5 = r12.getY()
                int r6 = r11.CENTER_Y
                float r6 = (float) r6
                float r4 = r5 - r6
                float r5 = r3 * r3
                float r6 = r4 * r4
                float r5 = r5 + r6
                double r5 = (double) r5
                double r5 = java.lang.Math.sqrt(r5)
                int r7 = r11.CENTER_RADIUS
                double r7 = (double) r7
                int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x002e
                r1 = r9
            L_0x0026:
                int r5 = r12.getAction()
                switch(r5) {
                    case 0: goto L_0x0030;
                    case 1: goto L_0x006b;
                    case 2: goto L_0x003a;
                    default: goto L_0x002d;
                }
            L_0x002d:
                return r9
            L_0x002e:
                r1 = r10
                goto L_0x0026
            L_0x0030:
                r11.mTrackingCenter = r1
                if (r1 == 0) goto L_0x003a
                r11.mHighlightCenter = r9
                r11.invalidate()
                goto L_0x002d
            L_0x003a:
                boolean r5 = r11.mTrackingCenter
                if (r5 == 0) goto L_0x0048
                boolean r5 = r11.mHighlightCenter
                if (r5 == r1) goto L_0x002d
                r11.mHighlightCenter = r1
                r11.invalidate()
                goto L_0x002d
            L_0x0048:
                double r5 = (double) r4
                double r7 = (double) r3
                double r5 = java.lang.Math.atan2(r5, r7)
                float r0 = (float) r5
                r5 = 1086918618(0x40c90fda, float:6.283185)
                float r2 = r0 / r5
                r5 = 0
                int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
                if (r5 >= 0) goto L_0x005c
                r5 = 1065353216(0x3f800000, float:1.0)
                float r2 = r2 + r5
            L_0x005c:
                android.graphics.Paint r5 = r11.mCenterPaint
                int[] r6 = r11.mColors
                int r6 = r11.interpColor(r6, r2)
                r5.setColor(r6)
                r11.invalidate()
                goto L_0x002d
            L_0x006b:
                boolean r5 = r11.mTrackingCenter
                if (r5 == 0) goto L_0x002d
                if (r1 == 0) goto L_0x007c
                com.crittermap.backcountrynavigator.dialog.ColorPickerDialog$OnColorChangedListener r5 = r11.mListener
                android.graphics.Paint r6 = r11.mCenterPaint
                int r6 = r6.getColor()
                r5.colorChanged(r6)
            L_0x007c:
                r11.mTrackingCenter = r10
                r11.invalidate()
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.crittermap.backcountrynavigator.dialog.ColorPickerDialog.ColorPickerView.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public ColorPickerDialog(Context context, OnColorChangedListener listener, int initialColor) {
        super(context);
        this.mListener = listener;
        this.mInitialColor = initialColor;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ColorPickerView(getContext(), new OnColorChangedListener() {
            public void colorChanged(int color) {
                ColorPickerDialog.this.mListener.colorChanged(color);
                ColorPickerDialog.this.dismiss();
            }
        }, this.mInitialColor));
        setTitle("Pick a Color");
    }
}
