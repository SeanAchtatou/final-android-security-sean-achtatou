package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ToolBarActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.toolbar);
        Intent intent = getIntent();
        View mylocation = findViewById(R.id.tbn_recenter);
        mylocation.setOnClickListener(this);
        if (intent.getBooleanExtra("centered", false)) {
            mylocation.setVisibility(8);
        } else {
            mylocation.setVisibility(0);
        }
        findViewById(R.id.tbn_record_track).setOnClickListener(this);
        ImageView rview = (ImageView) findViewById(R.id.tbn_record_image);
        TextView rtview = (TextView) findViewById(R.id.tbn_record_text);
        View gpsoff = findViewById(R.id.tbn_gps_off);
        gpsoff.setOnClickListener(this);
        if (intent.getBooleanExtra("recording", false)) {
            rview.setImageResource(R.drawable.track_stop);
            rtview.setText((int) R.string.tb_trackrecord_stop);
            gpsoff.setVisibility(8);
        } else {
            rview.setImageResource(R.drawable.track_record);
            rtview.setText((int) R.string.tb_trackrecord);
            if (intent.getBooleanExtra("gpson", false)) {
                gpsoff.setVisibility(0);
            } else {
                gpsoff.setVisibility(8);
            }
        }
        findViewById(R.id.tb_map_layers).setOnClickListener(this);
    }

    public void onClick(View v) {
        setResult(v.getId());
        finish();
    }
}
