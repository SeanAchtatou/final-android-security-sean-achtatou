package com.crittermap.backcountrynavigator;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

public class DateCheckTask extends AsyncTask<String, Integer, Date> {
    Context context;
    DateRetryHandler retryHandler = new DateRetryHandler();

    public DateCheckTask(Context ctx) {
        this.context = ctx;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Date result) {
        if (result != null) {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.context);
            long dateExpired = result.getTime();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putLong(TrialCheck.EXPIRATIONGUID, dateExpired);
            editor.commit();
        }
        super.onPostExecute((Object) result);
    }

    class DateRetryHandler implements HttpRequestRetryHandler {
        DateRetryHandler() {
        }

        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            return executionCount < 3;
        }
    }

    /* access modifiers changed from: protected */
    public Date doInBackground(String... arg) {
        try {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost("https://www.crittermap.com/software/registration/trial?device_id=" + URLEncoder.encode(arg[0], "UTF-8"));
            client.setHttpRequestRetryHandler(this.retryHandler);
            HttpConnectionParams.setSoTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
            HttpConnectionParams.setConnectionTimeout(client.getParams(), MyTracksConstants.MAX_LOADED_TRACK_POINTS);
            String answer = (String) client.execute(request, new BasicResponseHandler<>());
            if (answer != null) {
                return new Date(1000 * Long.parseLong(answer.trim()));
            }
            return null;
        } catch (Exception e) {
            Log.w("Connect", "error", e);
            return null;
        }
    }
}
