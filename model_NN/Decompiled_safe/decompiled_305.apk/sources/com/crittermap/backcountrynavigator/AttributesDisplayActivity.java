package com.crittermap.backcountrynavigator;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.crittermap.backcountrynavigator.data.BCNMapDatabase;
import org.xmlrpc.android.IXMLRPCSerializer;

public class AttributesDisplayActivity extends Activity {
    private LayoutInflater inflate;
    private LinearLayout layoutList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.attrib_display_activity);
        String path = getIntent().getStringExtra("Path");
        this.layoutList = (LinearLayout) findViewById(R.id.attriblist);
        this.inflate = (LayoutInflater) getSystemService("layout_inflater");
        initList(path);
    }

    public void initList(String path) {
        String attrib;
        String attrib2;
        String attrib3;
        Cursor c = null;
        try {
            SQLiteDatabase db = SQLiteDatabase.openDatabase(path, null, 0);
            if (getIntent().hasExtra("trail")) {
                String[] strArr = {IXMLRPCSerializer.TAG_NAME, "cmt", "county", "contact_num", "web_ad", "email", "classif", "club_name", "length", "ohv"};
                c = db.query(BCNMapDatabase.PATHS, null, "PathID=" + getIntent().getLongExtra("trail", -1), null, null, null, null, null);
                c.moveToFirst();
            } else if (getIntent().hasExtra("poi")) {
                String[] strArr2 = {IXMLRPCSerializer.TAG_NAME, "Comment", "county", "contact_num", "web_ad", "email", "classif", "club_name", "ohv"};
                c = db.query(BCNMapDatabase.WAY_POINTS, null, "PointID=" + getIntent().getLongExtra("poi", -1), null, null, null, null);
                c.moveToFirst();
            }
            for (int i = 0; i < c.getColumnCount(); i++) {
                View v = this.inflate.inflate((int) R.layout.attrib_display_item, (ViewGroup) null);
                TextView value = (TextView) v.findViewById(R.id.attrib_value);
                TextView name = (TextView) v.findViewById(R.id.attrib_name);
                try {
                    value.setText(c.getString(i));
                    if (!value.getText().equals("") && (attrib3 = determineAttribute(c.getColumnName(i))) != null) {
                        name.setText(attrib3);
                        this.layoutList.addView(v);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!value.getText().equals("") && (attrib2 = determineAttribute(c.getColumnName(i))) != null) {
                        name.setText(attrib2);
                        this.layoutList.addView(v);
                    }
                } catch (Throwable th) {
                    if (!value.getText().equals("") && (attrib = determineAttribute(c.getColumnName(i))) != null) {
                        name.setText(attrib);
                        this.layoutList.addView(v);
                    }
                    throw th;
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            c.close();
        }
    }

    private String determineAttribute(String attrib) {
        String[] attribList = {IXMLRPCSerializer.TAG_NAME, "cmt", "comment", "contact_num", "web_ad", "classif", "email", "club_name", "state", "county", "city", "river_name", "run_name", "min_class", "max_class", "length", "ohv", "description", "in_out"};
        boolean inlist = false;
        for (String equalsIgnoreCase : attribList) {
            if (equalsIgnoreCase.equalsIgnoreCase(attrib)) {
                inlist = true;
            }
        }
        if (!inlist) {
            return null;
        }
        if (attrib.length() < 1) {
            return attrib;
        }
        String attrib2 = String.valueOf(attrib.substring(0, 1).toUpperCase()) + attrib.substring(1);
        if (attrib2.equalsIgnoreCase("cmt")) {
            attrib2 = "Comment";
        } else if (attrib2.equalsIgnoreCase("contact_num")) {
            attrib2 = "Contact Number";
        } else if (attrib2.equalsIgnoreCase("web_ad")) {
            attrib2 = "Web Address";
        } else if (attrib2.equalsIgnoreCase("classif")) {
            attrib2 = "Classification";
        } else if (attrib2.equalsIgnoreCase("club_name")) {
            attrib2 = "Club Name";
        } else if (attrib2.equalsIgnoreCase("ohv")) {
            attrib2 = "OHV";
        } else if (attrib2.equalsIgnoreCase("in_out")) {
            attrib2 = "Type";
        } else if (attrib2.equalsIgnoreCase("river_name")) {
            attrib2 = "River Name";
        } else if (attrib2.equalsIgnoreCase("run_name")) {
            attrib2 = "Run Name";
        }
        return attrib2;
    }
}
