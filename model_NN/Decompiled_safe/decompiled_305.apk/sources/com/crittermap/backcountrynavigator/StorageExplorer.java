package com.crittermap.backcountrynavigator;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StorageExplorer extends ListActivity {
    private List<String> item = null;
    private TextView myPath;
    private List<String> path = null;
    private String root = "/";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_explorer_main);
        this.myPath = (TextView) findViewById(R.id.path);
        getDir(Environment.getExternalStorageDirectory().getPath());
    }

    private void getDir(String dirPath) {
        this.myPath.setText("Location: " + dirPath);
        this.item = new ArrayList();
        this.path = new ArrayList();
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (!dirPath.equals(this.root)) {
            this.item.add(this.root);
            this.path.add(this.root);
            this.item.add("../");
            this.path.add(f.getParent());
        }
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    this.item.add(String.valueOf(file.getName()) + "/");
                    this.path.add(file.getPath());
                } else if (file.getName().toLowerCase().endsWith(".gpx")) {
                    this.item.add(file.getName());
                    this.path.add(file.getPath());
                } else if (file.getName().toLowerCase().endsWith(".loc")) {
                    this.item.add(file.getName());
                    this.path.add(file.getPath());
                } else if (file.getName().toLowerCase().endsWith(".kml")) {
                    this.item.add(file.getName());
                    this.path.add(file.getPath());
                } else if (file.getName().toLowerCase().endsWith(".kmz")) {
                    this.item.add(file.getName());
                    this.path.add(file.getPath());
                }
            }
        }
        setListAdapter(new ArrayAdapter<>(this, (int) R.layout.row, this.item));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        File file = new File(this.path.get(position));
        if (!file.isDirectory()) {
            Intent intent = new Intent();
            intent.setClass(this, ImportActivity.class);
            Uri urifile = Uri.fromFile(file);
            if (urifile.toString().endsWith(".gpx")) {
                intent.setDataAndType(urifile, "application/gpx");
            } else if (urifile.toString().endsWith(".loc")) {
                intent.setDataAndType(urifile, "application/xml-loc");
            } else if (urifile.toString().endsWith(".kml")) {
                intent.setDataAndType(urifile, "application/kml");
            } else if (urifile.toString().endsWith(".kmz")) {
                intent.setDataAndType(urifile, "application/kmz");
            }
            startActivity(intent);
            finish();
        } else if (file.canRead()) {
            getDir(this.path.get(position));
        } else {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("[" + file.getName() + "] folder can't be read!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
        }
    }
}
