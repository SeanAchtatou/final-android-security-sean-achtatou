package com.crittermap.analytics;

import android.content.Context;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class TrackerFactory {
    static int mReferenceCount = 0;
    static GoogleAnalyticsTracker trackerInstance = null;

    public static GoogleAnalyticsTracker getTracker(Context ctx) {
        if (trackerInstance == null) {
            trackerInstance = GoogleAnalyticsTracker.getInstance();
            trackerInstance.start("UA-11476428-4", ctx.getApplicationContext());
        }
        mReferenceCount++;
        return trackerInstance;
    }

    public static void releaseTracker() {
        mReferenceCount--;
        if (mReferenceCount <= 0) {
            trackerInstance.stop();
            trackerInstance = null;
        }
    }
}
