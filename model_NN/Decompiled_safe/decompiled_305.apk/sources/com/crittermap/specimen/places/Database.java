package com.crittermap.specimen.places;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.xmlrpc.android.IXMLRPCSerializer;

public class Database extends SQLiteOpenHelper {
    static final String DATABASE_ALTERNATENAMES_TABLE = "alternatenames";
    static final String DATABASE_GEONAME_TABLE = "geoname";
    static final String DATABASE_ICONSPEC_TABLE = "iconspec";
    static String DATABASE_NAME = "critterSoftDb.sqlite";
    static String ICONSPEC_FILE = "/sdcard/iconspec.xls";
    final String ALTERNATENAME_I_GEONAMEID = "geonameid";
    final String ALTERNATENAME_I_ID = "_id";
    final String ALTERNATENAME_S_ALTERNATENAME = "altname";
    final String ALTERNATENAME_S_ISOLANGUAGE = "isolanguage";
    final String ALTERNATENAME_S_ISPREFERREDNAME = "ispreferredname";
    final String ALTERNATENAME_S_ISSHORTNAME = "isshortname";
    final String GEONAME_C_FEATURECLASS = "featureclass";
    final String GEONAME_D_LATITUDE = "latitude";
    final String GEONAME_D_LONGITUDE = "longitude";
    final String GEONAME_I_ELEVATION = "elevation";
    final String GEONAME_I_GEONAMEID = "geonameid";
    final String GEONAME_I_GTOP030 = "gtopo30";
    final String GEONAME_I_ID = "_id";
    final String GEONAME_I_POPULATION = "population";
    final String GEONAME_S_ADMIN1CODE = "admin1code";
    final String GEONAME_S_ADMIN2CODE = "admin2code";
    final String GEONAME_S_ADMIN3CODE = "admin3code";
    final String GEONAME_S_ADMIN4CODE = "admin4code";
    final String GEONAME_S_ALTERNATECOUNTRYCODE = "cc2";
    final String GEONAME_S_ALTERNATENAMES = DATABASE_ALTERNATENAMES_TABLE;
    final String GEONAME_S_COUNTRYCODE = "countrycode";
    final String GEONAME_S_FEATURECODE = "featurecode";
    final String GEONAME_S_GTOP030 = "modificationdate";
    final String GEONAME_S_NAME = IXMLRPCSerializer.TAG_NAME;
    final String ICONSPEC_I_ID = "_id";
    final String ICONSPEC_I_MINLEVEL = "minlevel";
    final String ICONSPEC_S_DESCRIPTION = "description";
    final String ICONSPEC_S_FEATURECODE = "featurecode";
    final String ICONSPEC_S_ICONNAME = "iconname";
    Context mContext = null;
    SQLiteDatabase mDb = null;
    String[] mFilenames = {"AG.txt"};

    public Database(String dbName, Context context) {
        super(context, dbName, (SQLiteDatabase.CursorFactory) null, 1);
        this.mContext = context;
        DATABASE_NAME = dbName;
    }

    public Database(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        this.mContext = context;
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public synchronized void openDataBase() throws SQLException {
        this.mDb = SQLiteDatabase.openDatabase("/sdcard/" + DATABASE_NAME, null, 268435456);
        this.mDb.execSQL("CREATE TABLE IF NOT EXISTS geoname ( _id INTEGER ASC, geonameid INTEGER PRIMARY KEY UNIQUE ,name STRING,alternatenames STRING,latitude DOUBLE,longitude DOUBLE,featureclass STRING,featurecode STRING, countrycode STRING, cc2 STRING, admin1code STRING, admin2code STRING, admin3code STRING, admin4code STRING, population INTEGER, elevation INTEGER, gtopo30 INTEGER, modificationdate STRING );");
        this.mDb.execSQL("CREATE INDEX IF NOT EXISTS geoname_idx ON geoname(geonameid,latitude,longitude);");
        this.mDb.execSQL("CREATE TABLE IF NOT EXISTS alternatenames ( _id INTEGER ASC, geonameid INTEGER PRIMARY KEY ,isolanguage STRING,altname STRING,ispreferredname STRING,isshortname STRING);");
        this.mDb.execSQL("CREATE INDEX IF NOT EXISTS alternatename_idx ON alternatenames(geonameid);");
        this.mDb.execSQL("CREATE TABLE IF NOT EXISTS iconspec ( _id INTEGER, iconname STRING,featurecode STRING PRIMARY KEY ASC,description STRING,minlevel INTEGER UNIQUE);");
        this.mDb.execSQL("CREATE INDEX IF NOT EXISTS iconspec_idx ON iconspec(featurecode);");
    }

    public synchronized void close() {
        if (this.mDb != null) {
            this.mDb.close();
        }
        super.close();
    }

    public Cursor getPlacesForArea(float minLat, float minLon, float maxLat, float maxLon) {
        return this.mDb.query(DATABASE_GEONAME_TABLE, null, "latitude < " + maxLat + " AND " + "latitude" + " > " + minLat + " AND " + "longitude" + " < " + maxLon + " AND " + "longitude" + " > " + minLon, null, null, null, "_id  COLLATE NOCASE ASC");
    }

    public int getGeonameTableSize() {
        return this.mDb.query(DATABASE_GEONAME_TABLE, null, null, null, null, null, "_id  COLLATE NOCASE ASC").getCount();
    }

    public void insertGeoname(GeoName geo) {
        ContentValues stateValues = new ContentValues();
        stateValues.put("geonameid", Integer.valueOf(geo.getId()));
        stateValues.put(IXMLRPCSerializer.TAG_NAME, geo.getName());
        stateValues.put(DATABASE_ALTERNATENAMES_TABLE, geo.getAlternateNames());
        stateValues.put("latitude", Double.valueOf(geo.getLatitude()));
        stateValues.put("longitude", Double.valueOf(geo.getLongitude()));
        stateValues.put("featureclass", geo.getFeatureClass());
        stateValues.put("featurecode", geo.getFeatureCode());
        stateValues.put("countrycode", geo.getCountryCode());
        stateValues.put("cc2", geo.getAlternateCountryCode());
        stateValues.put("admin1code", geo.getAdminCode1());
        stateValues.put("admin2code", geo.getAdminCode2());
        stateValues.put("admin3code", geo.getAdminCode3());
        stateValues.put("admin4code", geo.getAdminCode4());
        stateValues.put("population", Integer.valueOf(geo.getPopulation()));
        stateValues.put("elevation", Integer.valueOf(geo.getElevation()));
        stateValues.put("gtopo30", Integer.valueOf(geo.getGtopo30()));
        stateValues.put("modificationdate", geo.getModificationDate());
        this.mDb.insert(DATABASE_GEONAME_TABLE, null, stateValues);
    }

    public void insertIconSpec(String iconName, String featureCode, String description, int minLevel) {
        ContentValues iconSpecValues = new ContentValues();
        iconSpecValues.put("iconname", iconName);
        iconSpecValues.put("featurecode", featureCode);
        iconSpecValues.put("description", description);
        iconSpecValues.put("minlevel", Integer.valueOf(minLevel));
        this.mDb.insert(DATABASE_ICONSPEC_TABLE, null, iconSpecValues);
    }

    public int getIconSpecTableSize() {
        return this.mDb.query(DATABASE_ICONSPEC_TABLE, null, null, null, null, null, "_id  COLLATE NOCASE ASC").getCount();
    }

    public void populateGeonameTable() {
        int files = 0;
        while (files < this.mFilenames.length) {
            try {
                InputStream in = new FileInputStream("/sdcard/" + this.mFilenames[files]);
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                int count = 0;
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    String[] data = line.split("\t");
                    if (data.length > 0) {
                        try {
                            GeoName geo = new GeoName();
                            String datSingle = data[0];
                            if (!datSingle.trim().equalsIgnoreCase("")) {
                                geo.setId(Integer.parseInt(datSingle));
                            } else {
                                geo.setId(0);
                            }
                            String datSingle2 = data[1];
                            if (!datSingle2.trim().equalsIgnoreCase("")) {
                                geo.setName(datSingle2);
                            } else {
                                geo.setName("");
                            }
                            String datSingle3 = data[2];
                            if (!datSingle3.trim().equalsIgnoreCase("")) {
                                geo.setAsciiName(datSingle3);
                            } else {
                                geo.setAsciiName("");
                            }
                            String datSingle4 = data[3];
                            if (!datSingle4.trim().equalsIgnoreCase("")) {
                                geo.setAlternateNames(datSingle4);
                            } else {
                                geo.setAlternateNames("");
                            }
                            String datSingle5 = data[4];
                            if (!datSingle5.trim().equalsIgnoreCase("")) {
                                geo.setLatitude(Double.parseDouble(datSingle5));
                            } else {
                                geo.setLatitude(Double.parseDouble(""));
                            }
                            String datSingle6 = data[5];
                            if (!datSingle6.trim().equalsIgnoreCase("")) {
                                geo.setLongitude(Double.parseDouble(datSingle6));
                            } else {
                                geo.setLongitude(Double.parseDouble(""));
                            }
                            String datSingle7 = data[6];
                            if (!datSingle7.trim().equalsIgnoreCase("")) {
                                geo.setFeatureClass(datSingle7);
                            } else {
                                geo.setFeatureClass("");
                            }
                            String datSingle8 = data[7];
                            if (!datSingle8.trim().equalsIgnoreCase("")) {
                                geo.setFeatureCode(datSingle8);
                            } else {
                                geo.setFeatureCode("");
                            }
                            String datSingle9 = data[8];
                            if (!datSingle9.trim().equalsIgnoreCase("")) {
                                geo.setCountryCode(datSingle9);
                            } else {
                                geo.setCountryCode("");
                            }
                            String datSingle10 = data[9];
                            if (!datSingle10.trim().equalsIgnoreCase("")) {
                                geo.setAlternateCountryCode(datSingle10);
                            } else {
                                geo.setAlternateCountryCode("");
                            }
                            String datSingle11 = data[10];
                            if (!datSingle11.trim().equalsIgnoreCase("")) {
                                geo.setAdminCode1(datSingle11);
                            } else {
                                geo.setAdminCode1("");
                            }
                            String datSingle12 = data[11];
                            if (!datSingle12.trim().equalsIgnoreCase("")) {
                                geo.setAdminCode2(datSingle12);
                            } else {
                                geo.setAdminCode2("");
                            }
                            String datSingle13 = data[12];
                            if (!datSingle13.trim().equalsIgnoreCase("")) {
                                geo.setAdminCode3(datSingle13);
                            } else {
                                geo.setAdminCode3("");
                            }
                            String datSingle14 = data[13];
                            if (!datSingle14.trim().equalsIgnoreCase("")) {
                                geo.setAdminCode4(datSingle14);
                            } else {
                                geo.setAdminCode4("");
                            }
                            String popuStr = data[14];
                            if (!popuStr.trim().equalsIgnoreCase("")) {
                                geo.setPopulation(Integer.parseInt(popuStr));
                            } else {
                                geo.setPopulation(0);
                            }
                            String eleStr = data[15];
                            if (!eleStr.trim().equalsIgnoreCase("")) {
                                geo.setElevation(Integer.parseInt(eleStr));
                            } else {
                                geo.setElevation(0);
                            }
                            String gtopoStr = data[16];
                            if (!gtopoStr.trim().equalsIgnoreCase("")) {
                                geo.setGtopo30(Integer.parseInt(gtopoStr));
                            } else {
                                geo.setGtopo30(0);
                            }
                            String datSingle15 = data[17];
                            if (!datSingle15.trim().equalsIgnoreCase("")) {
                                geo.setTimezone(datSingle15);
                            } else {
                                geo.setTimezone("");
                            }
                            String datSingle16 = data[18];
                            if (!datSingle16.trim().equalsIgnoreCase("")) {
                                geo.setModificationDate(datSingle16);
                            } else {
                                geo.setModificationDate("");
                            }
                            insertGeoname(geo);
                            count++;
                        } catch (Exception e) {
                            Log.e("ERR", "ERR " + e.getLocalizedMessage());
                        }
                    }
                }
                in.close();
                Log.e("count", "inserted " + files + " = " + count);
                files++;
            } catch (IOException e2) {
                IOException e3 = e2;
                Log.e("ERR", " populateGeonameTable " + e3.getLocalizedMessage());
                e3.printStackTrace();
                return;
            }
        }
    }

    public void populateIconSpecTable(String iconSpecFilePath) {
        String inputFilePath;
        if (iconSpecFilePath == null) {
            inputFilePath = ICONSPEC_FILE;
        } else if (!iconSpecFilePath.equalsIgnoreCase("")) {
            inputFilePath = iconSpecFilePath;
        } else {
            inputFilePath = ICONSPEC_FILE;
        }
        try {
            InputStream in = new FileInputStream(inputFilePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            int count = 0;
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    in.close();
                    Log.e("count", "icon spec inserted  = " + count);
                    return;
                } else if (!line.trim().equalsIgnoreCase("")) {
                    String[] data = line.split("\t");
                    if (data.length > 0) {
                        insertIconSpec(data[0], data[1], data[2], Integer.parseInt(data[3]));
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("ERR", " populateIconSpecTable " + e2.getLocalizedMessage());
            e2.printStackTrace();
        }
    }

    public void deleteAll() {
        this.mDb.delete(DATABASE_GEONAME_TABLE, null, null);
        this.mDb.delete(DATABASE_ICONSPEC_TABLE, null, null);
        this.mDb.delete(DATABASE_ALTERNATENAMES_TABLE, null, null);
    }

    public void backupDB() {
        try {
            FileInputStream fi = new FileInputStream(new File("/data/data/" + this.mContext.getPackageName() + "/databases/" + DATABASE_NAME));
            FileOutputStream fo = new FileOutputStream(new File("/sdcard/" + DATABASE_NAME));
            byte[] buffer = new byte[1024];
            while (true) {
                int length = fi.read(buffer);
                if (length <= 0) {
                    fo.flush();
                    fo.close();
                    fi.close();
                    return;
                }
                fo.write(buffer, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
