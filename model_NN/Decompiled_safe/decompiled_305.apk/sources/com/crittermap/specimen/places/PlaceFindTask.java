package com.crittermap.specimen.places;

import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import java.util.List;
import org.json.JSONException;

public class PlaceFindTask extends AsyncTask<String, Integer, List<GeoName>> {
    static final int MAXRESULTS = 10;
    Geocoder coder;
    public String exceptionText = null;
    Listener listener;

    interface Listener {
        void onResult(List<GeoName> list);
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((List<GeoName>) ((List) obj));
    }

    public PlaceFindTask(Context ctx, Listener receiver) {
        this.coder = new Geocoder(ctx);
        this.listener = receiver;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(List<GeoName> result) {
        super.onPostExecute((Object) result);
        this.listener.onResult(result);
    }

    /* access modifiers changed from: protected */
    public List<GeoName> doInBackground(String... arg0) {
        try {
            List<GeoName> places = PlacesProvider.placesFind(arg0[0], 10);
            Log.d("LocationFindTask", "FOUND PLACES COUNT = " + places.size());
            return places;
        } catch (JSONException e) {
            JSONException e2 = e;
            Log.e("LocationFindTask", "Message: " + e2.getMessage(), e2);
            this.exceptionText = e2.getMessage();
            return null;
        }
    }
}
