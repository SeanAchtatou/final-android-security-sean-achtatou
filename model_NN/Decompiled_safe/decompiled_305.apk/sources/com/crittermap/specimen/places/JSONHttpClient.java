package com.crittermap.specimen.places;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONHttpClient {
    private static final String TAG = "HttpClient";

    public static JSONObject SendHttpPost(String URL, JSONObject jsonObjSend) {
        return SendHttpPostWithCookies(URL, jsonObjSend, null);
    }

    /* JADX INFO: Multiple debug info for r6v1 org.apache.http.client.methods.HttpPost: [D('httpPostRequest' org.apache.http.client.methods.HttpPost), D('cookieStore' org.apache.http.client.CookieStore)] */
    /* JADX INFO: Multiple debug info for r6v2 org.apache.http.HttpResponse: [D('httpPostRequest' org.apache.http.client.methods.HttpPost), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r4v16 org.apache.http.Header: [D('entity' org.apache.http.HttpEntity), D('contentEncoding' org.apache.http.Header)] */
    /* JADX INFO: Multiple debug info for r5v9 java.lang.String: [D('instream' java.io.InputStream), D('resultString' java.lang.String)] */
    public static JSONObject SendHttpPostWithCookies(String URL, JSONObject jsonObjSend, CookieStore cookieStore) {
        InputStream instream;
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            if (cookieStore != null) {
                httpclient.setCookieStore(cookieStore);
            }
            CookieStore cookieStore2 = new HttpPost(URL);
            cookieStore2.setEntity(new StringEntity(URLEncoder.encode(jsonObjSend.toString(), "UTF-8")));
            cookieStore2.setHeader("Accept", "application/json");
            cookieStore2.setHeader("Content-type", "application/json");
            cookieStore2.setHeader("User-Agent", "Specimen");
            long t = System.currentTimeMillis();
            HttpResponse response = httpclient.execute(cookieStore2);
            Log.e(TAG, "HTTPResponse received in [" + (System.currentTimeMillis() - t) + "ms]");
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream2 = entity.getContent();
                Header contentEncoding = response.getFirstHeader("Content-Encoding");
                if (contentEncoding == null || !contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    instream = instream2;
                } else {
                    instream = new GZIPInputStream(instream2);
                }
                String resultString = convertStreamToString(instream);
                instream.close();
                return new JSONObject(resultString);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                return new JSONObject("{\"success\":false}");
            } catch (JSONException e2) {
            }
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static JSONObject SendHttpGetWithCookies(String URL, CookieStore cookieStore) {
        try {
            Log.e(TAG, "Starting request ");
            DefaultHttpClient mClient = new DefaultHttpClient();
            Log.e(TAG, "Request: " + URL.toString());
            HttpGet httpGetRequest = new HttpGet(URL);
            httpGetRequest.setHeader("User-Agent", "Specimen");
            String resultString = (String) mClient.execute(httpGetRequest, new BasicResponseHandler<>());
            Log.e(TAG, "Response: " + resultString);
            JSONObject jsonObjRecv = new JSONObject(resultString);
            Log.e(TAG, jsonObjRecv.toString());
            Log.e(TAG, "<jsonobject>\n" + jsonObjRecv.toString() + "\n</jsonobject>");
            return jsonObjRecv;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Log.e(TAG, "Couldn't parse response");
                return new JSONObject("{\"success\":false}");
            } catch (JSONException e2) {
                return null;
            }
        }
    }

    public static JSONObject SendHttpGet(String URL) {
        return SendHttpGetWithCookies(URL, null);
    }
}
