package com.crittermap.specimen.places;

import android.database.Cursor;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlrpc.android.IXMLRPCSerializer;

public class PlacesProvider implements IPlacesProvider {
    Database mDb = null;

    public PlacesProvider(Database db) {
        this.mDb = db;
    }

    public void requestArea(float centerLat, float centerLon, float radiusInkm) {
        ArrayList<GeoName> geos = fetchJSON(centerLat, centerLon, radiusInkm);
        Log.e("Specimen", "Fetched requestArea = " + geos.size());
        Iterator<GeoName> it = geos.iterator();
        while (it.hasNext()) {
            try {
                this.mDb.insertGeoname(it.next());
            } catch (Exception e) {
                Exception e2 = e;
                Log.e("Specimen", "requestArea ERR DUPLICATE " + e2.getLocalizedMessage());
                e2.printStackTrace();
            }
        }
    }

    public Cursor getPlacesForArea(float minLat, float minLon, float maxLat, float maxLon) {
        return this.mDb.getPlacesForArea(minLat, minLon, maxLat, maxLon);
    }

    public ArrayList<GeoName> fetchJSON(float lat, float lng, float radius) {
        ArrayList<GeoName> geonameList = new ArrayList<>();
        JSONObject jsonObj = JSONHttpClient.SendHttpGet("http://ws.geonames.org/findNearbyJSON?lat=" + lat + "&lng=" + lng + "&radius=" + radius);
        if (jsonObj != null) {
            try {
                JSONArray jsonApps = jsonObj.getJSONArray("geonames");
                for (int i = 0; i < jsonApps.length(); i++) {
                    GeoName geo = new GeoName();
                    JSONObject jsonObj2 = jsonApps.getJSONObject(i);
                    geo.setAdminCode1(jsonObj2.getString("adminCode1"));
                    geo.setFeatureClass(jsonObj2.getString("fcl"));
                    geo.setCountryCode(jsonObj2.getString("countryCode"));
                    geo.setLongitude(jsonObj2.getDouble("lng"));
                    geo.setFeatureCode(jsonObj2.getString("fcode"));
                    geo.setName(jsonObj2.getString(IXMLRPCSerializer.TAG_NAME));
                    geo.setId(jsonObj2.getInt("geonameId"));
                    geo.setLatitude(jsonObj2.getDouble("lat"));
                    geo.setPopulation(jsonObj2.getInt("population"));
                    geonameList.add(geo);
                }
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("Specimen", "fetchJSON ERR " + e2.getLocalizedMessage());
                e2.printStackTrace();
            }
        }
        return geonameList;
    }

    public static ArrayList<GeoName> placesFind(String keyword, int maxResults) throws JSONException {
        ArrayList<GeoName> geonameList = new ArrayList<>();
        JSONObject jsonObj = JSONHttpClient.SendHttpGet("http://ws.geonames.org/searchJSON?q=" + keyword + "&maxRows=" + maxResults + "&style=full");
        if (jsonObj != null) {
            try {
                JSONArray jsonApps = jsonObj.getJSONArray("geonames");
                for (int i = 0; i < jsonApps.length(); i++) {
                    GeoName geo = new GeoName();
                    JSONObject jsonObj2 = jsonApps.getJSONObject(i);
                    geo.setAdminCode1(jsonObj2.getString("adminCode1"));
                    geo.setFeatureClass(jsonObj2.getString("fcl"));
                    geo.setCountryCode(jsonObj2.getString("countryCode"));
                    geo.setLongitude(jsonObj2.getDouble("lng"));
                    geo.setFeatureCode(jsonObj2.getString("fcode"));
                    geo.setName(jsonObj2.getString(IXMLRPCSerializer.TAG_NAME));
                    geo.setId(jsonObj2.getInt("geonameId"));
                    geo.setLatitude(jsonObj2.getDouble("lat"));
                    geo.setPopulation(jsonObj2.getInt("population"));
                    geonameList.add(geo);
                }
            } catch (JSONException e) {
                JSONException e2 = e;
                Log.e("Specimen", "placesFind ERR " + e2.getLocalizedMessage());
                e2.printStackTrace();
                throw e2;
            }
        }
        return geonameList;
    }
}
