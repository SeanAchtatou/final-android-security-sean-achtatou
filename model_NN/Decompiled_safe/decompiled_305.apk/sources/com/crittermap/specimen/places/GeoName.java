package com.crittermap.specimen.places;

public class GeoName {
    String adminCode1 = "";
    String adminCode2 = "";
    String adminCode3 = "";
    String adminCode4 = "";
    String alternateCountryCode = "";
    String alternateNames = "";
    String asciiName = "";
    String countryCode = "";
    int elevation = 0;
    String featureClass = "";
    String featureCode = "";
    int gtopo30 = 0;
    int id = 0;
    double latitude = 0.0d;
    double longitude = 0.0d;
    String modificationDate = "";
    String name = "";
    int population = 0;
    String timezone = "";

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getAsciiName() {
        return this.asciiName;
    }

    public void setAsciiName(String asciiName2) {
        this.asciiName = asciiName2;
    }

    public String getAlternateNames() {
        return this.alternateNames;
    }

    public void setAlternateNames(String alternateNames2) {
        this.alternateNames = alternateNames2;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public String getFeatureClass() {
        return this.featureClass;
    }

    public void setFeatureClass(String featureClass2) {
        this.featureClass = featureClass2;
    }

    public String getFeatureCode() {
        return this.featureCode;
    }

    public void setFeatureCode(String featureCode2) {
        this.featureCode = featureCode2;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode2) {
        this.countryCode = countryCode2;
    }

    public String getAlternateCountryCode() {
        return this.alternateCountryCode;
    }

    public void setAlternateCountryCode(String alternateCountryCode2) {
        this.alternateCountryCode = alternateCountryCode2;
    }

    public String getAdminCode1() {
        return this.adminCode1;
    }

    public void setAdminCode1(String adminCode12) {
        this.adminCode1 = adminCode12;
    }

    public String getAdminCode2() {
        return this.adminCode2;
    }

    public void setAdminCode2(String adminCode22) {
        this.adminCode2 = adminCode22;
    }

    public String getAdminCode3() {
        return this.adminCode3;
    }

    public void setAdminCode3(String adminCode32) {
        this.adminCode3 = adminCode32;
    }

    public String getAdminCode4() {
        return this.adminCode4;
    }

    public void setAdminCode4(String adminCode42) {
        this.adminCode4 = adminCode42;
    }

    public int getPopulation() {
        return this.population;
    }

    public void setPopulation(int population2) {
        this.population = population2;
    }

    public int getElevation() {
        return this.elevation;
    }

    public void setElevation(int elevation2) {
        this.elevation = elevation2;
    }

    public int getGtopo30() {
        return this.gtopo30;
    }

    public void setGtopo30(int gtopo302) {
        this.gtopo30 = gtopo302;
    }

    public String getTimezone() {
        return this.timezone;
    }

    public void setTimezone(String timezone2) {
        this.timezone = timezone2;
    }

    public String getModificationDate() {
        return this.modificationDate;
    }

    public void setModificationDate(String modificationDate2) {
        this.modificationDate = modificationDate2;
    }

    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        sbuilder.append(getName());
        if (this.adminCode1 != null) {
            sbuilder.append(",");
            sbuilder.append(this.adminCode1);
        }
        if (this.countryCode != null) {
            sbuilder.append(",");
            sbuilder.append(this.countryCode);
        }
        sbuilder.append("@");
        sbuilder.append(getLatitude());
        sbuilder.append(",");
        sbuilder.append(getLongitude());
        return sbuilder.toString();
    }
}
