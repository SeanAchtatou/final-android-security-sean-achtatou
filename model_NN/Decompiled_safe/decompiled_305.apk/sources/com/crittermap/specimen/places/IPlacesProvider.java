package com.crittermap.specimen.places;

import android.database.Cursor;

public interface IPlacesProvider {
    Cursor getPlacesForArea(float f, float f2, float f3, float f4);

    void requestArea(float f, float f2, float f3);
}
