package com.google.android.apps.mytracks.stats;

public abstract class MyTracksConstants {
    public static final String ACCOUNT_TYPE = "com.google";
    public static final int ADD_LIST = 4;
    public static final int AUTHENTICATE_TO_DOCS = 8;
    public static final int AUTHENTICATE_TO_TRIX = 9;
    public static final int CLEAR_MAP = 22;
    public static final int CREATE_MAP = 2;
    public static final int DELETE_TRACK = 11;
    public static final int DISTANCE_SMOOTHING_FACTOR = 25;
    public static final int EDIT_DETAILS = 18;
    public static final int EDIT_WAYPOINT = 24;
    public static final int ELEVATION_SMOOTHING_FACTOR = 25;
    public static final int FEATURE_DETAILS = 5;
    public static final int GET_LOGIN = 0;
    public static final int GET_MAP = 1;
    public static final String GPS_PROVIDER = "gps";
    public static final int GRADE_SMOOTHING_FACTOR = 5;
    public static final double MAX_ACCELERATION = 0.02d;
    public static final int MAX_DISPLAYED_TRACK_POINTS = 10000;
    public static final int MAX_DISPLAYED_WAYPOINTS_POINTS = 128;
    public static final int MAX_LOADED_TRACK_POINTS = 20000;
    public static final int MAX_LOADED_WAYPOINTS_POINTS = 10000;
    public static final double MAX_NO_MOVEMENT_DISTANCE = 2.0d;
    public static final double MAX_NO_MOVEMENT_SPEED = 0.224d;
    public static final int MENU_CHART_SETTINGS = 8;
    public static final int MENU_CLEAR_MAP = 208;
    public static final int MENU_CURRENT_SEGMENT = 10;
    public static final int MENU_DELETE = 101;
    public static final int MENU_EDIT = 100;
    public static final int MENU_HELP = 9;
    public static final int MENU_LIST_MARKERS = 4;
    public static final int MENU_LIST_TRACKS = 3;
    public static final int MENU_MY_LOCATION = 6;
    public static final int MENU_SAVE_CSV_FILE = 207;
    public static final int MENU_SAVE_GPX_FILE = 205;
    public static final int MENU_SAVE_KML_FILE = 206;
    public static final int MENU_SEND_TO_GOOGLE = 102;
    public static final int MENU_SETTINGS = 5;
    public static final int MENU_SHARE = 103;
    public static final int MENU_SHARE_CSV_FILE = 203;
    public static final int MENU_SHARE_GPX_FILE = 201;
    public static final int MENU_SHARE_KML_FILE = 202;
    public static final int MENU_SHARE_LINK = 200;
    public static final int MENU_SHOW = 104;
    public static final int MENU_START_RECORDING = 1;
    public static final int MENU_STOP_RECORDING = 2;
    public static final int MENU_TOGGLE_LAYERS = 7;
    public static final int MENU_WRITE_TO_SD_CARD = 204;
    public static final int SAVE_CSV_FILE = 21;
    public static final int SAVE_GPX_FILE = 19;
    public static final int SAVE_KML_FILE = 20;
    public static final int SEND_TO_DOCS = 10;
    public static final int SEND_TO_GOOGLE = 12;
    public static final int SEND_TO_GOOGLE_DIALOG = 13;
    public static final int SHARE_CSV_FILE = 17;
    public static final int SHARE_GPX_FILE = 15;
    public static final int SHARE_KML_FILE = 16;
    public static final int SHARE_LINK = 14;
    public static final int SHOW_TRACK = 3;
    public static final int SHOW_WAYPOINT = 23;
    public static final int SPEED_SMOOTHING_FACTOR = 25;
    public static final int START_RECORDING = 6;
    public static final int STOP_RECORDING = 7;
    public static final String TAG = "MyTracks";
    public static final int WELCOME = 25;

    public static int getActionFromMenuId(int menuId) {
        switch (menuId) {
            case 100:
                return 18;
            case MENU_DELETE /*101*/:
                return 11;
            case MENU_SEND_TO_GOOGLE /*102*/:
                return 13;
            case 200:
                return 14;
            case MENU_SHARE_GPX_FILE /*201*/:
                return 15;
            case MENU_SHARE_KML_FILE /*202*/:
                return 16;
            case MENU_SHARE_CSV_FILE /*203*/:
                return 17;
            case MENU_SAVE_GPX_FILE /*205*/:
                return 19;
            case MENU_SAVE_KML_FILE /*206*/:
                return 20;
            case MENU_SAVE_CSV_FILE /*207*/:
                return 21;
            case MENU_CLEAR_MAP /*208*/:
                return 22;
            default:
                return -1;
        }
    }

    protected MyTracksConstants() {
    }
}
