package com.google.android.apps.mytracks.stats;

public class ExtremityMonitor {
    private double max;
    private double min;

    public ExtremityMonitor() {
        reset();
    }

    public boolean update(double value) {
        boolean changed = false;
        if (value < this.min) {
            this.min = value;
            changed = true;
        }
        if (value <= this.max) {
            return changed;
        }
        this.max = value;
        return true;
    }

    public double getMin() {
        return this.min;
    }

    public double getMax() {
        return this.max;
    }

    public void reset() {
        this.min = Double.POSITIVE_INFINITY;
        this.max = Double.NEGATIVE_INFINITY;
    }

    public void set(double min2, double max2) {
        this.min = min2;
        this.max = max2;
    }

    public void setMin(double min2) {
        this.min = min2;
    }

    public void setMax(double max2) {
        this.max = max2;
    }
}
