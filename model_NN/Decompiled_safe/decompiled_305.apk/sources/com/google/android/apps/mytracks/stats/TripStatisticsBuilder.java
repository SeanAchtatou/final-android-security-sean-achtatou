package com.google.android.apps.mytracks.stats;

import android.location.Location;
import android.util.Log;

public class TripStatisticsBuilder {
    private double currentGrade;
    private double currentSpeed;
    private final TripStatistics data;
    private final DoubleBuffer distanceBuffer;
    private final DoubleBuffer elevationBuffer;
    private final DoubleBuffer gradeBuffer;
    private Location lastLocation;
    private Location lastMovingLocation;
    private boolean paused;
    private final DoubleBuffer speedBuffer;
    private long totalLocations;

    public TripStatisticsBuilder() {
        this.lastLocation = null;
        this.lastMovingLocation = null;
        this.paused = true;
        this.speedBuffer = new DoubleBuffer(25);
        this.elevationBuffer = new DoubleBuffer(25);
        this.distanceBuffer = new DoubleBuffer(25);
        this.gradeBuffer = new DoubleBuffer(5);
        this.totalLocations = 0;
        this.data = new TripStatistics();
    }

    public TripStatisticsBuilder(TripStatistics statsData) {
        this.lastLocation = null;
        this.lastMovingLocation = null;
        this.paused = true;
        this.speedBuffer = new DoubleBuffer(25);
        this.elevationBuffer = new DoubleBuffer(25);
        this.distanceBuffer = new DoubleBuffer(25);
        this.gradeBuffer = new DoubleBuffer(5);
        this.totalLocations = 0;
        this.data = new TripStatistics(statsData);
    }

    public boolean addLocation(Location currentLocation, long systemTime) {
        if (this.paused) {
            Log.w(MyTracksConstants.TAG, "Tried to account for location while track is paused");
            return false;
        }
        this.totalLocations++;
        double elevationDifference = updateElevation(currentLocation.getAltitude());
        this.data.setTotalTime(systemTime - this.data.getStartTime());
        this.currentSpeed = (double) currentLocation.getSpeed();
        if (this.lastLocation == null) {
            this.lastLocation = currentLocation;
            this.lastMovingLocation = currentLocation;
            return false;
        }
        updateBounds(currentLocation);
        double distance = (double) this.lastLocation.distanceTo(currentLocation);
        if (distance >= 2.0d || this.currentSpeed >= 0.224d) {
            this.data.addTotalDistance((double) this.lastMovingLocation.distanceTo(currentLocation));
            updateSpeed(currentLocation.getTime(), this.currentSpeed, this.lastLocation.getTime(), (double) this.lastLocation.getSpeed());
            updateGrade(distance, elevationDifference);
            this.lastLocation = currentLocation;
            this.lastMovingLocation = currentLocation;
            return true;
        }
        this.lastLocation = currentLocation;
        return false;
    }

    private void updateBounds(Location location) {
        this.data.updateLatitudeExtremities(location.getLatitude());
        this.data.updateLongitudeExtremities(location.getLongitude());
    }

    /* access modifiers changed from: package-private */
    public double updateElevation(double elevation) {
        double elevationDifference;
        double oldSmoothedElevation = getSmoothedElevation();
        this.elevationBuffer.setNext(elevation);
        double smoothedElevation = getSmoothedElevation();
        this.data.updateElevationExtremities(smoothedElevation);
        if (this.elevationBuffer.isFull()) {
            elevationDifference = smoothedElevation - oldSmoothedElevation;
        } else {
            elevationDifference = 0.0d;
        }
        if (elevationDifference > 0.0d) {
            this.data.addTotalElevationGain(elevationDifference);
        }
        return elevationDifference;
    }

    /* access modifiers changed from: package-private */
    public void updateSpeed(long updateTime, double speed, long lastLocationTime, double lastLocationSpeed) {
        long timeDifference = updateTime - lastLocationTime;
        if (timeDifference < 0) {
            Log.e(MyTracksConstants.TAG, "Found negative time change: " + timeDifference);
        }
        this.data.addMovingTime(timeDifference);
        if (isValidSpeed(updateTime, speed, lastLocationTime, lastLocationSpeed, this.speedBuffer)) {
            this.speedBuffer.setNext(speed);
            if (speed > this.data.getMaxSpeed()) {
                this.data.setMaxSpeed(speed);
            }
            double movingSpeed = this.data.getAverageMovingSpeed();
            if (this.speedBuffer.isFull() && movingSpeed > this.data.getMaxSpeed()) {
                this.data.setMaxSpeed(movingSpeed);
                return;
            }
            return;
        }
        Log.d(MyTracksConstants.TAG, "TripStatistics ignoring big change: Raw Speed: " + speed + " old: " + lastLocationSpeed + " [" + toString() + "]");
    }

    public static boolean isValidSpeed(long updateTime, double speed, long lastLocationTime, double lastLocationSpeed, DoubleBuffer speedBuffer2) {
        if (speed == 0.0d) {
            return false;
        }
        long timeDifference = updateTime - lastLocationTime;
        if (Math.abs(speed - 128.0d) < 1.0d || Math.abs(lastLocationSpeed - speed) > 0.02d * ((double) timeDifference)) {
            return false;
        }
        double smoothedSpeed = speedBuffer2.getAverage();
        return !speedBuffer2.isFull() || (speed < smoothedSpeed * 10.0d && Math.abs(smoothedSpeed - speed) < 0.02d * ((double) timeDifference));
    }

    /* access modifiers changed from: package-private */
    public void updateGrade(double distance, double elevationDifference) {
        this.distanceBuffer.setNext(distance);
        double smoothedDistance = this.distanceBuffer.getAverage();
        if (this.elevationBuffer.isFull() && this.distanceBuffer.isFull() && smoothedDistance >= 5.0d) {
            this.currentGrade = elevationDifference / smoothedDistance;
            this.gradeBuffer.setNext(this.currentGrade);
            this.data.updateGradeExtremities(this.gradeBuffer.getAverage());
        }
    }

    public void pause() {
        pauseAt(System.currentTimeMillis());
    }

    public void pauseAt(long time) {
        if (!this.paused) {
            this.data.setStopTime(time);
            this.data.setTotalTime(time - this.data.getStartTime());
            this.lastLocation = null;
            this.paused = true;
        }
    }

    public void resume() {
        resumeAt(System.currentTimeMillis());
    }

    public void resumeAt(long time) {
        if (this.paused) {
            this.data.setStartTime(time);
            this.data.setStopTime(-1);
            this.paused = false;
        }
    }

    public String toString() {
        return "TripStatistics { Data: " + this.data.toString() + "; Total Locations: " + this.totalLocations + "; Paused: " + this.paused + "; Current speed: " + this.currentSpeed + "; Current grade: " + this.currentGrade + "}";
    }

    public long getIdleTime() {
        return this.lastLocation.getTime() - this.lastMovingLocation.getTime();
    }

    public double getSmoothedElevation() {
        return this.elevationBuffer.getAverage();
    }

    public TripStatistics getStatistics() {
        return new TripStatistics(this.data);
    }
}
