package com.google.android.apps.mytracks.stats;

public class DoubleBuffer {
    private final double[] buffer;
    private int index;
    private boolean isFull;

    public DoubleBuffer(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("The buffer size must be positive.");
        }
        this.buffer = new double[size];
        reset();
    }

    public void setNext(double d) {
        if (this.index == this.buffer.length) {
            this.index = 0;
        }
        this.buffer[this.index] = d;
        this.index++;
        if (this.index == this.buffer.length) {
            this.isFull = true;
        }
    }

    public boolean isFull() {
        return this.isFull;
    }

    public void reset() {
        this.index = 0;
        this.isFull = false;
    }

    public double getAverage() {
        int numberOfEntries = this.isFull ? this.buffer.length : this.index;
        if (numberOfEntries == 0) {
            return 0.0d;
        }
        double sum = 0.0d;
        for (int i = 0; i < numberOfEntries; i++) {
            sum += this.buffer[i];
        }
        return sum / ((double) numberOfEntries);
    }

    public double[] getAverageAndVariance() {
        int numberOfEntries = this.isFull ? this.buffer.length : this.index;
        if (numberOfEntries == 0) {
            return new double[]{0.0d, 0.0d};
        }
        double sum = 0.0d;
        double sumSquares = 0.0d;
        for (int i = 0; i < numberOfEntries; i++) {
            sum += this.buffer[i];
            sumSquares += Math.pow(this.buffer[i], 2.0d);
        }
        double average = sum / ((double) numberOfEntries);
        return new double[]{average, (sumSquares / ((double) numberOfEntries)) - Math.pow(average, 2.0d)};
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("Full: ");
        stringBuffer.append(this.isFull);
        stringBuffer.append("\n");
        int i = 0;
        while (i < this.buffer.length) {
            stringBuffer.append(i == this.index ? "<<" : "[");
            stringBuffer.append(this.buffer[i]);
            stringBuffer.append(i == this.index ? ">> " : "] ");
            i++;
        }
        return stringBuffer.toString();
    }
}
