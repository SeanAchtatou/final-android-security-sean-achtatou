package com.google.android.apps.mytracks.stats;

import android.os.Parcel;
import android.os.Parcelable;

public class TripStatistics implements Parcelable {
    public static final Creator CREATOR = new Creator();
    /* access modifiers changed from: private */
    public final ExtremityMonitor elevationExtremities = new ExtremityMonitor();
    /* access modifiers changed from: private */
    public final ExtremityMonitor gradeExtremities = new ExtremityMonitor();
    /* access modifiers changed from: private */
    public final ExtremityMonitor latitudeExtremities = new ExtremityMonitor();
    /* access modifiers changed from: private */
    public final ExtremityMonitor longitudeExtremities = new ExtremityMonitor();
    /* access modifiers changed from: private */
    public double maxSpeed;
    /* access modifiers changed from: private */
    public long movingTime;
    /* access modifiers changed from: private */
    public long startTime = -1;
    private long stopTime = -1;
    /* access modifiers changed from: private */
    public double totalDistance;
    /* access modifiers changed from: private */
    public double totalElevationGain;
    /* access modifiers changed from: private */
    public long totalTime;

    public TripStatistics() {
    }

    public TripStatistics(TripStatistics other) {
        this.maxSpeed = other.maxSpeed;
        this.movingTime = other.movingTime;
        this.startTime = other.startTime;
        this.stopTime = other.stopTime;
        this.totalDistance = other.totalDistance;
        this.totalElevationGain = other.totalElevationGain;
        this.totalTime = other.totalTime;
        this.latitudeExtremities.set(other.latitudeExtremities.getMin(), other.latitudeExtremities.getMax());
        this.longitudeExtremities.set(other.longitudeExtremities.getMin(), other.longitudeExtremities.getMax());
        this.elevationExtremities.set(other.elevationExtremities.getMin(), other.elevationExtremities.getMax());
        this.gradeExtremities.set(other.gradeExtremities.getMin(), other.gradeExtremities.getMax());
    }

    public void merge(TripStatistics other) {
        this.startTime = Math.min(this.startTime, other.startTime);
        this.stopTime = Math.max(this.stopTime, other.stopTime);
        this.totalTime += other.totalTime;
        this.movingTime += other.movingTime;
        this.totalDistance += other.totalDistance;
        this.totalElevationGain += other.totalElevationGain;
        this.maxSpeed = Math.max(this.maxSpeed, other.maxSpeed);
        this.latitudeExtremities.update(other.latitudeExtremities.getMax());
        this.latitudeExtremities.update(other.latitudeExtremities.getMin());
        this.longitudeExtremities.update(other.longitudeExtremities.getMax());
        this.longitudeExtremities.update(other.longitudeExtremities.getMin());
        this.elevationExtremities.update(other.elevationExtremities.getMax());
        this.elevationExtremities.update(other.elevationExtremities.getMin());
        this.gradeExtremities.update(other.gradeExtremities.getMax());
        this.gradeExtremities.update(other.gradeExtremities.getMin());
    }

    public long getStartTime() {
        return this.startTime;
    }

    public long getStopTime() {
        return this.startTime;
    }

    public long getTotalTime() {
        return this.totalTime;
    }

    public double getTotalDistance() {
        return this.totalDistance;
    }

    public double getAverageSpeed() {
        return this.totalDistance / (((double) this.totalTime) / 1000.0d);
    }

    public double getAverageMovingSpeed() {
        return this.totalDistance / (((double) this.movingTime) / 1000.0d);
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public long getMovingTime() {
        return this.movingTime;
    }

    public double getTotalElevationGain() {
        return this.totalElevationGain;
    }

    public int getLeft() {
        return (int) (this.longitudeExtremities.getMin() * 1000000.0d);
    }

    public int getRight() {
        return (int) (this.longitudeExtremities.getMax() * 1000000.0d);
    }

    public int getBottom() {
        return (int) (this.latitudeExtremities.getMin() * 1000000.0d);
    }

    public int getTop() {
        return (int) (this.latitudeExtremities.getMax() * 1000000.0d);
    }

    public double getMinElevation() {
        return this.elevationExtremities.getMin();
    }

    public double getMaxElevation() {
        return this.elevationExtremities.getMax();
    }

    public double getMaxGrade() {
        return this.gradeExtremities.getMax();
    }

    public double getMinGrade() {
        return this.gradeExtremities.getMin();
    }

    public void setStartTime(long startTime2) {
        this.startTime = startTime2;
    }

    public void setStopTime(long stopTime2) {
        this.stopTime = stopTime2;
    }

    public void setMovingTime(long movingTime2) {
        this.movingTime = movingTime2;
    }

    public void setTotalTime(long totalTime2) {
        this.totalTime = totalTime2;
    }

    public void setTotalDistance(double totalDistance2) {
        this.totalDistance = totalDistance2;
    }

    public void setTotalElevationGain(double totalElevationGain2) {
        this.totalElevationGain = totalElevationGain2;
    }

    public void setMaxSpeed(double maxSpeed2) {
        this.maxSpeed = maxSpeed2;
    }

    public void setMinElevation(double elevation) {
        this.elevationExtremities.setMin(elevation);
    }

    public void setMaxElevation(double elevation) {
        this.elevationExtremities.setMax(elevation);
    }

    public void setMinGrade(double grade) {
        this.gradeExtremities.setMin(grade);
    }

    public void setMaxGrade(double grade) {
        this.gradeExtremities.setMax(grade);
    }

    public void setBounds(int left, int top, int right, int bottom) {
        this.latitudeExtremities.set((double) bottom, (double) top);
        this.longitudeExtremities.set((double) left, (double) right);
    }

    /* access modifiers changed from: package-private */
    public void addTotalDistance(double distance) {
        this.totalDistance += distance;
    }

    /* access modifiers changed from: package-private */
    public void addTotalElevationGain(double gain) {
        this.totalElevationGain += gain;
    }

    /* access modifiers changed from: package-private */
    public void addMovingTime(long time) {
        this.movingTime += time;
    }

    /* access modifiers changed from: package-private */
    public void updateLatitudeExtremities(double latitude) {
        this.latitudeExtremities.update(latitude);
    }

    /* access modifiers changed from: package-private */
    public void updateLongitudeExtremities(double longitude) {
        this.longitudeExtremities.update(longitude);
    }

    /* access modifiers changed from: package-private */
    public void updateElevationExtremities(double elevation) {
        this.elevationExtremities.update(elevation);
    }

    /* access modifiers changed from: package-private */
    public void updateGradeExtremities(double grade) {
        this.gradeExtremities.update(grade);
    }

    public String toString() {
        return "TripStatistics { Start Time: " + getStartTime() + "; Total Time: " + getTotalTime() + "; Moving Time: " + getMovingTime() + "; Total Distance: " + getTotalDistance() + "; Elevation Gain: " + getTotalElevationGain() + "; Min Elevation: " + getMinElevation() + "; Max Elevation: " + getMaxElevation() + "; Average Speed: " + getAverageMovingSpeed() + "; Min Grade: " + getMinGrade() + "; Max Grade: " + getMaxGrade() + "}";
    }

    public static class Creator implements Parcelable.Creator<TripStatistics> {
        public TripStatistics createFromParcel(Parcel source) {
            TripStatistics data = new TripStatistics();
            data.startTime = source.readLong();
            data.movingTime = source.readLong();
            data.totalTime = source.readLong();
            data.totalDistance = source.readDouble();
            data.totalElevationGain = source.readDouble();
            data.maxSpeed = source.readDouble();
            data.latitudeExtremities.set(source.readDouble(), source.readDouble());
            data.longitudeExtremities.set(source.readDouble(), source.readDouble());
            data.elevationExtremities.set(source.readDouble(), source.readDouble());
            data.gradeExtremities.set(source.readDouble(), source.readDouble());
            return data;
        }

        public TripStatistics[] newArray(int size) {
            return new TripStatistics[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.startTime);
        dest.writeLong(this.movingTime);
        dest.writeLong(this.totalTime);
        dest.writeDouble(this.totalDistance);
        dest.writeDouble(this.totalElevationGain);
        dest.writeDouble(this.maxSpeed);
        dest.writeDouble(this.latitudeExtremities.getMin());
        dest.writeDouble(this.latitudeExtremities.getMax());
        dest.writeDouble(this.longitudeExtremities.getMin());
        dest.writeDouble(this.longitudeExtremities.getMax());
        dest.writeDouble(this.elevationExtremities.getMin());
        dest.writeDouble(this.elevationExtremities.getMax());
        dest.writeDouble(this.gradeExtremities.getMin());
        dest.writeDouble(this.gradeExtremities.getMax());
    }
}
