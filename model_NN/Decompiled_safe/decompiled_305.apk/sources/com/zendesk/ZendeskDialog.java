package com.zendesk;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.apps.mytracks.stats.MyTracksConstants;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ZendeskDialog {
    private static final String DESCRIPTION_DEFAULT = "How may we help you? Please fill in details below, and we'll get back to you as soon as possible.";
    public static final String TAG = "Zendesk";
    private static final String TAG_DEFAULT = "dropbox";
    private static final String TITLE_DEFAULT = "Title";
    /* access modifiers changed from: private */
    public static AlertDialog aDialog;
    private static View.OnClickListener buttonListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case -2:
                    ZendeskDialog.aDialog.dismiss();
                    ZendeskDialog.resetDialogView();
                    return;
                case -1:
                    if (ZendeskDialog.descriptionET.length() == 0 || ZendeskDialog.subjectET.length() == 0 || ZendeskDialog.emailET.length() == 0) {
                        if (ZendeskDialog.descriptionET.length() == 0) {
                            ZendeskDialog.descriptionTV.setTextColor(-65536);
                        } else {
                            ZendeskDialog.descriptionTV.setTextColor(-1);
                        }
                        if (ZendeskDialog.subjectET.length() == 0) {
                            ZendeskDialog.subjectTV.setTextColor(-65536);
                        } else {
                            ZendeskDialog.subjectTV.setTextColor(-1);
                        }
                        if (ZendeskDialog.emailET.length() == 0) {
                            ZendeskDialog.emailTV.setTextColor(-65536);
                        } else {
                            ZendeskDialog.emailTV.setTextColor(-1);
                        }
                        Toast.makeText(ZendeskDialog.context, "Please fill out all Fields", 0).show();
                        return;
                    }
                    ZendeskDialog.toastHandler = new Handler() {
                        public void handleMessage(Message msg) {
                            String message;
                            super.handleMessage(msg);
                            if (msg.getData().getString("submit").equals("successfully")) {
                                message = "Your request has successfully been submitted";
                            } else {
                                message = "Your request couldn't be submitted, please try again";
                            }
                            Toast.makeText(ZendeskDialog.context, message, 0).show();
                        }
                    };
                    new Thread(ZendeskDialog.runnable).start();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public static Context context;
    private static String description;
    /* access modifiers changed from: private */
    public static EditText descriptionET;
    /* access modifiers changed from: private */
    public static TextView descriptionTV;
    private static View dialogView;
    /* access modifiers changed from: private */
    public static EditText emailET;
    /* access modifiers changed from: private */
    public static TextView emailTV;
    static Runnable runnable = new Runnable() {
        public void run() {
            Message message = new Message();
            String description = ZendeskDialog.descriptionET.getText().toString();
            String subject = ZendeskDialog.subjectET.getText().toString();
            String email = ZendeskDialog.emailET.getText().toString();
            try {
                String reqDesc = "description=" + URLEncoder.encode(description, "UTF-8");
                String reqContent = String.valueOf(reqDesc) + "&" + ("email=" + URLEncoder.encode(email, "UTF-8")) + "&" + ("subject=" + URLEncoder.encode(subject, "UTF-8")) + "&" + ("set_tags=" + URLEncoder.encode(ZendeskDialog.tag, "UTF-8"));
                URL url = new URL("http://" + ZendeskDialog.url + "/requests/mobile_api/create.json");
                Log.d(ZendeskDialog.TAG, "Sending Request " + url.toExternalForm());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", Integer.toString(reqContent.getBytes().length));
                connection.addRequestProperty("X-Zendesk-Mobile-API", "1.0");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                dataOutputStream.writeBytes(reqContent);
                dataOutputStream.flush();
                dataOutputStream.close();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()), 8192);
                while (true) {
                    String line = bufferedReader.readLine();
                    if (line == null) {
                        bufferedReader.close();
                        ZendeskDialog.aDialog.dismiss();
                        ZendeskDialog.resetDialogView();
                        message.getData().putString("submit", "successfully");
                        ZendeskDialog.toastHandler.sendMessage(message);
                        return;
                    }
                    Log.d(ZendeskDialog.TAG, line);
                }
            } catch (Exception e) {
                message.getData().putString("submit", "failed");
                ZendeskDialog.toastHandler.sendMessage(message);
                Log.e(ZendeskDialog.TAG, "Error while, submit request", e);
            }
        }
    };
    /* access modifiers changed from: private */
    public static EditText subjectET;
    /* access modifiers changed from: private */
    public static TextView subjectTV;
    /* access modifiers changed from: private */
    public static String tag;
    private static String title;
    /* access modifiers changed from: private */
    public static Handler toastHandler;
    /* access modifiers changed from: private */
    public static String url;

    private ZendeskDialog(Context context2) {
        context = context2;
        dialogView = createDialogView(context2);
        aDialog = new AlertDialog.Builder(context2).setTitle(TITLE_DEFAULT).setView(dialogView).create();
    }

    public static ZendeskDialog Builder(Context context2) {
        return new ZendeskDialog(context2);
    }

    public ZendeskDialog setTitle(String title2) {
        title = title2;
        return new ZendeskDialog(context);
    }

    public ZendeskDialog setDescription(String description2) {
        description = description2;
        return new ZendeskDialog(context);
    }

    public ZendeskDialog setUrl(String url2) {
        url = url2;
        return new ZendeskDialog(context);
    }

    public ZendeskDialog setTag(String tag2) {
        tag = tag2;
        return new ZendeskDialog(context);
    }

    public AlertDialog create() {
        if (title != null) {
            aDialog.setTitle(title);
        } else if (getMetaDataByKey("zendesk_title") != null) {
            aDialog.setTitle(getMetaDataByKey("zendesk_title"));
        }
        descriptionTV.setText(DESCRIPTION_DEFAULT);
        if (description != null) {
            descriptionTV.setText(description);
        } else if (getMetaDataByKey("zendesk_description") != null) {
            descriptionTV.setText(getMetaDataByKey("zendesk_description"));
        }
        if (tag == null) {
            if (getMetaDataByKey("zendesk_tag") != null) {
                tag = getMetaDataByKey("zendesk_tag");
            } else {
                tag = TAG_DEFAULT;
            }
        }
        if (url == null) {
            url = getMetaDataByKey("zendesk_url");
        }
        if (url != null) {
            return aDialog;
        }
        Log.e(TAG, "Meta Data with key \"zendesk_url\" couldn't be found in AndroidManifext.xml");
        return null;
    }

    private static String getMetaDataByKey(String key) {
        try {
            String valueByKey = context.getPackageManager().getApplicationInfo(context.getPackageName(), MyTracksConstants.MAX_DISPLAYED_WAYPOINTS_POINTS).metaData.getString(key);
            Log.d(TAG, "Key: " + key + " - Value: " + valueByKey);
            return valueByKey;
        } catch (Exception e) {
            Log.e(TAG, "Error reading meta data from AndroidManifest.xml", e);
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r0v34 android.graphics.Bitmap: [D('in' java.io.InputStream), D('poweredBy' android.graphics.Bitmap)] */
    private static View createDialogView(Context context2) {
        LinearLayout llRoot = new LinearLayout(context2);
        llRoot.setOrientation(1);
        llRoot.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        ScrollView sv = new ScrollView(context2);
        sv.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        LinearLayout llContent = new LinearLayout(context2);
        llContent.setOrientation(1);
        llContent.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        llContent.setPadding(10, 0, 10, 0);
        LinearLayout llTop = new LinearLayout(context2);
        llTop.setOrientation(1);
        descriptionTV = new TextView(context2);
        descriptionTV.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        descriptionTV.setTextColor(-1);
        descriptionET = new EditText(context2);
        descriptionET.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        descriptionET.setMinLines(2);
        descriptionET.setMaxLines(2);
        descriptionET.setInputType(49153);
        subjectTV = new TextView(context2);
        subjectTV.setText("Subject:");
        subjectTV.setTextColor(-1);
        subjectET = new EditText(context2);
        subjectET.setSingleLine(true);
        subjectET.setInputType(40961);
        emailTV = new TextView(context2);
        emailTV.setText("E-Mail:");
        emailTV.setTextColor(-1);
        emailET = new EditText(context2);
        emailET.setSingleLine(true);
        emailET.setInputType(33);
        LinearLayout llBottom = new LinearLayout(context2);
        llBottom.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        TextView poweredByTV = new TextView(context2);
        poweredByTV.setText("Powered By");
        poweredByTV.setPadding(0, 0, 10, 0);
        poweredByTV.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        poweredByTV.setGravity(16);
        ImageView poweredByIV = new ImageView(context2);
        poweredByIV.setImageBitmap(BitmapFactory.decodeStream(ZendeskDialog.class.getResourceAsStream("/com/zendesk/zendesk.png")));
        LinearLayout llButton = new LinearLayout(context2);
        llButton.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        llButton.setOrientation(0);
        llButton.setBackgroundColor(-4342339);
        llButton.setPadding(0, 4, 0, 0);
        Button submit = new Button(context2);
        submit.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        submit.setText("Submit Query");
        submit.setId(-1);
        submit.setOnClickListener(buttonListener);
        Button cancel = new Button(context2);
        cancel.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        cancel.setText("Cancel");
        cancel.setId(-2);
        cancel.setOnClickListener(buttonListener);
        llRoot.addView(sv);
        llRoot.addView(llButton);
        sv.addView(llContent);
        llContent.addView(llTop);
        llContent.addView(llBottom);
        llTop.addView(descriptionTV);
        llTop.addView(descriptionET);
        llTop.addView(subjectTV);
        llTop.addView(subjectET);
        llTop.addView(emailTV);
        llTop.addView(emailET);
        llBottom.addView(poweredByTV);
        llBottom.addView(poweredByIV);
        llButton.addView(submit);
        llButton.addView(cancel);
        return llRoot;
    }

    /* access modifiers changed from: private */
    public static void resetDialogView() {
        descriptionET.setText("");
        subjectET.setText("");
        emailET.setText("");
        descriptionTV.setTextColor(-1);
        subjectTV.setTextColor(-1);
        emailTV.setTextColor(-1);
    }
}
