package rsg.mailchimp.api.data;

public class AccountContact extends GenericStructConverter {
    public String address1;
    public String address2;
    public String city;
    public String company;
    public String country;
    public String email;
    public String fname;
    public String lname;
    public String state;
    public String url;
    public String zip;
}
