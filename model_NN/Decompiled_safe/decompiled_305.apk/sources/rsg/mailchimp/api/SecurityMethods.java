package rsg.mailchimp.api;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.xmlrpc.android.XMLRPCException;
import rsg.mailchimp.api.data.ApiKeyInfo;

public class SecurityMethods extends MailChimpApi {
    public SecurityMethods(CharSequence apiKey) {
        super(apiKey);
    }

    public SecurityMethods(Context ctx) {
        super(ctx);
    }

    public String apikeyAdd(String username, String password) throws MailChimpApiException {
        return (String) callMethod("apikeyAdd", username, password, this.apiKey);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public boolean apiKeyExpire(String username, String password, String key) throws MailChimpApiException {
        Object[] objArr = new Object[3];
        objArr[0] = username;
        objArr[1] = password;
        objArr[2] = key == null ? this.apiKey : key;
        return ((Boolean) callMethod("apikeyExpire", objArr)).booleanValue();
    }

    public List<ApiKeyInfo> getApiKeys(String username, String password, Boolean expiredOnly) throws MailChimpApiException {
        Object[] objArr = new Object[4];
        objArr[0] = username;
        objArr[1] = password;
        objArr[2] = this.apiKey;
        objArr[3] = Boolean.valueOf(expiredOnly == null ? false : expiredOnly.booleanValue());
        Object callResult = callMethod("apikeys", objArr);
        if (!(callResult instanceof Object[])) {
            return new ArrayList(0);
        }
        Object[] results = (Object[]) callResult;
        ArrayList<ApiKeyInfo> list = new ArrayList<>(results.length);
        for (Object result : results) {
            ApiKeyInfo info = new ApiKeyInfo();
            info.populateFromRPCStruct(null, (Map) result);
            list.add(info);
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public Object callMethod(String methodName, Object... params) throws MailChimpApiException {
        try {
            return getClient().callEx(methodName, params);
        } catch (XMLRPCException e) {
            throw buildMailChimpException(e);
        }
    }
}
