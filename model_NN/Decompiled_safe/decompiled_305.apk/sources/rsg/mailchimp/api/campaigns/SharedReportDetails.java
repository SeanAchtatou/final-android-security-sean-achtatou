package rsg.mailchimp.api.campaigns;

public class SharedReportDetails {
    public String password;
    public String secureUrl;
    public String title;
    public String url;
}
