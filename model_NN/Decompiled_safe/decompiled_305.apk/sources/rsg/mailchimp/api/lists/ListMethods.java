package rsg.mailchimp.api.lists;

import android.content.Context;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import rsg.mailchimp.api.Constants;
import rsg.mailchimp.api.MailChimpApi;
import rsg.mailchimp.api.MailChimpApiException;
import rsg.mailchimp.api.Utils;
import rsg.mailchimp.api.campaigns.AbuseReport;

public class ListMethods extends MailChimpApi {
    public ListMethods(CharSequence apiKey) {
        super(apiKey);
    }

    public ListMethods(Context ctx) {
        super(ctx);
    }

    public boolean listSubscribe(String listId, String emailAddress) throws MailChimpApiException {
        return listSubscribe(listId, emailAddress, null, null, null, null, null, null);
    }

    public boolean listSubscribe(String listId, String emailAddress, MergeFieldListUtil mergeFields) throws MailChimpApiException {
        return listSubscribe(listId, emailAddress, mergeFields, null, null, null, null, null);
    }

    public boolean listSubscribe(String listId, String emailAddress, MergeFieldListUtil mergeFields, Constants.EmailType emailType) throws MailChimpApiException {
        return listSubscribe(listId, emailAddress, mergeFields, emailType, null, null, null, null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v18, types: [java.lang.String[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean listSubscribe(java.lang.String r7, java.lang.String r8, rsg.mailchimp.api.lists.MergeFieldListUtil r9, rsg.mailchimp.api.Constants.EmailType r10, java.lang.Boolean r11, java.lang.Boolean r12, java.lang.Boolean r13, java.lang.Boolean r14) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            r6 = this;
            r3 = 1
            r5 = 0
            java.lang.String r0 = "listSubscribe"
            r1 = 8
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r5] = r7
            r1[r3] = r8
            r2 = 2
            if (r9 != 0) goto L_0x0045
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = ""
            r3[r5] = r4
        L_0x0015:
            r1[r2] = r3
            r2 = 3
            if (r10 != 0) goto L_0x0047
            java.lang.String r3 = ""
        L_0x001c:
            r1[r2] = r3
            r2 = 4
            if (r11 != 0) goto L_0x004c
            java.lang.String r3 = ""
        L_0x0023:
            r1[r2] = r3
            r2 = 5
            if (r12 != 0) goto L_0x004e
            java.lang.String r3 = ""
        L_0x002a:
            r1[r2] = r3
            r2 = 6
            if (r13 != 0) goto L_0x0050
            java.lang.String r3 = ""
        L_0x0031:
            r1[r2] = r3
            r2 = 7
            if (r14 != 0) goto L_0x0052
            java.lang.String r3 = ""
        L_0x0038:
            r1[r2] = r3
            java.lang.Object r6 = r6.callMethod(r0, r1)
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r0 = r6.booleanValue()
            return r0
        L_0x0045:
            r3 = r9
            goto L_0x0015
        L_0x0047:
            java.lang.String r3 = r10.toString()
            goto L_0x001c
        L_0x004c:
            r3 = r11
            goto L_0x0023
        L_0x004e:
            r3 = r12
            goto L_0x002a
        L_0x0050:
            r3 = r13
            goto L_0x0031
        L_0x0052:
            r3 = r14
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.lists.ListMethods.listSubscribe(java.lang.String, java.lang.String, rsg.mailchimp.api.lists.MergeFieldListUtil, rsg.mailchimp.api.Constants$EmailType, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean):boolean");
    }

    public boolean listUnsubscribe(String listId, String emailAddress) throws MailChimpApiException {
        return listUnsubscribe(listId, emailAddress, null, null, null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public boolean listUnsubscribe(String listId, String emailAddress, Boolean deleteMember, Boolean sendGoodbye, Boolean sendNotify) throws MailChimpApiException {
        Object[] objArr = new Object[5];
        objArr[0] = listId;
        objArr[1] = emailAddress;
        objArr[2] = deleteMember == null ? "" : deleteMember;
        objArr[3] = sendGoodbye == null ? "" : sendGoodbye;
        objArr[4] = sendNotify == null ? "" : sendNotify;
        return ((Boolean) callMethod("listUnsubscribe", objArr)).booleanValue();
    }

    public List<String> listsForEmail(String email) throws MailChimpApiException {
        Object obj = callMethod("listsForEmail", email);
        if (obj instanceof Object[]) {
            return Utils.convertObjectArrayToString((Object[]) obj);
        }
        return new ArrayList(0);
    }

    public MemberInfo listMemberInfo(String listId, String emailAddress) throws MailChimpApiException {
        Object obj = callMethod("listMemberInfo", listId, emailAddress);
        if (obj == null || !(obj instanceof Map)) {
            return null;
        }
        MemberInfo info = new MemberInfo();
        info.populateFromRPCStruct(null, (Map) obj);
        return info;
    }

    public List<MemberInfo> listMembers(String listId) throws MailChimpApiException {
        return listMembers(listId, null);
    }

    public List<MemberInfo> listMembers(String listId, Constants.SubscribeStatus status) throws MailChimpApiException {
        return listMembers(listId, status, null, null, null);
    }

    public List<MemberInfo> listMembers(String listId, Constants.SubscribeStatus status, Integer start, Integer limit, Date since) throws MailChimpApiException {
        ArrayList<Object> params = new ArrayList<>();
        params.add(listId);
        params.add(status == null ? Constants.SubscribeStatus.subscribed.toString() : status.toString());
        if (since != null) {
            params.add(Constants.TIME_FMT.format(since));
        }
        if (start != null) {
            params.add(start);
        }
        if (limit != null) {
            params.add(limit);
        }
        return callListMethod(MemberInfo.class, "listMembers", params.toArray());
    }

    public List<AbuseReport> listAbuseReports(String listId) throws MailChimpApiException {
        return listAbuseReports(listId, null, null, null);
    }

    public List<AbuseReport> listAbuseReports(String listId, Integer start, Integer limit, Date since) throws MailChimpApiException {
        Object limitVal;
        int startVal = start == null ? 0 : start.intValue();
        if (limit == null) {
            limitVal = "";
        } else {
            limitVal = limit;
        }
        return callListMethod(AbuseReport.class, "listAbuseReports", listId, Integer.valueOf(startVal), limitVal, since == null ? "" : Constants.TIME_FMT.format(since));
    }

    public BatchResults listBatchSubscribe(String listId, List<MergeFieldListUtil> toSubscribe) throws MailChimpApiException {
        return listBatchSubscribe(listId, toSubscribe, null, null, null);
    }

    public BatchResults listBatchSubscribe(String listId, List<MergeFieldListUtil> toSubscribe, Boolean doubleOptIn, Boolean updateExisting, Boolean replaceInterests) throws MailChimpApiException {
        Object[] objArr = new Object[5];
        objArr[0] = listId;
        objArr[1] = toSubscribe;
        objArr[2] = doubleOptIn == null ? "" : doubleOptIn;
        objArr[3] = updateExisting == null ? "" : updateExisting;
        objArr[4] = replaceInterests == null ? "" : replaceInterests;
        Object obj = callMethod("listBatchSubscribe", objArr);
        if (obj == null || !(obj instanceof Map)) {
            return null;
        }
        BatchResults results = new BatchResults();
        results.populateFromRPCStruct(null, (Map) obj);
        return results;
    }

    public BatchResults listBatchUnsubscribe(String listId, List<String> toUnsubscribe) throws MailChimpApiException {
        return listBatchUnsubscribe(listId, toUnsubscribe, null, null, null);
    }

    public BatchResults listBatchUnsubscribe(String listId, List<String> toUnsubscribe, Boolean deleteMember, Boolean sendGoodbye, Boolean sendNotify) throws MailChimpApiException {
        Object[] objArr = new Object[5];
        objArr[0] = listId;
        objArr[1] = toUnsubscribe;
        objArr[2] = deleteMember == null ? "" : deleteMember;
        objArr[3] = sendGoodbye == null ? "" : sendGoodbye;
        objArr[4] = sendNotify == null ? "" : sendNotify;
        Object obj = callMethod("listBatchUnsubscribe", objArr);
        if (obj == null || !(obj instanceof Map)) {
            return null;
        }
        BatchResults results = new BatchResults();
        results.populateFromRPCStruct(null, (Map) obj);
        return results;
    }

    public List<GrowthHistory> listGrowthHistory(String listId) throws MailChimpApiException {
        return callListMethod(GrowthHistory.class, "listGrowthHistory", listId);
    }

    public boolean listInterestGroupAdd(String listId, String groupName) throws MailChimpApiException {
        return ((Boolean) callMethod("listInterestGroupAdd", listId, groupName)).booleanValue();
    }

    public boolean listInterestGroupDel(String listId, String groupName) throws MailChimpApiException {
        return ((Boolean) callMethod("listInterestGroupDel", listId, groupName)).booleanValue();
    }

    public boolean listInterestGroupUpdate(String listId, String oldGroupName, String newGroupName) throws MailChimpApiException {
        return ((Boolean) callMethod("listInterestGroupUpdate", listId, oldGroupName, newGroupName)).booleanValue();
    }

    public InterestGroupInfo listInterestGroups(String listId) throws MailChimpApiException {
        Object obj = callMethod("listInterestGroups", listId);
        if (obj == null || !(obj instanceof Map)) {
            return null;
        }
        InterestGroupInfo info = new InterestGroupInfo();
        info.populateFromRPCStruct(null, (Map) obj);
        return info;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.String[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean listMergeVarAdd(java.lang.String r7, java.lang.String r8, java.lang.String r9, rsg.mailchimp.api.lists.MergeFieldOptions r10) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            r6 = this;
            r3 = 1
            r5 = 0
            java.lang.String r0 = "listMergeVarAdd"
            r1 = 4
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r5] = r7
            r1[r3] = r8
            r2 = 2
            r1[r2] = r9
            r2 = 3
            if (r10 != 0) goto L_0x0024
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = ""
            r3[r5] = r4
        L_0x0017:
            r1[r2] = r3
            java.lang.Object r6 = r6.callMethod(r0, r1)
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r0 = r6.booleanValue()
            return r0
        L_0x0024:
            r3 = r10
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.lists.ListMethods.listMergeVarAdd(java.lang.String, java.lang.String, java.lang.String, rsg.mailchimp.api.lists.MergeFieldOptions):boolean");
    }

    public List<MergeFieldInfo> listMergeVars(String listId) throws MailChimpApiException {
        return callListMethod(MergeFieldInfo.class, "listMergeVars", listId);
    }

    public boolean listMergeVarDel(String listId, String tag) throws MailChimpApiException {
        return ((Boolean) callMethod("listMergeVarDel", listId, tag)).booleanValue();
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.lang.String[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean listMergeVarUpdate(java.lang.String r7, java.lang.String r8, rsg.mailchimp.api.lists.MergeFieldOptions r9) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            r6 = this;
            r3 = 1
            r5 = 0
            java.lang.String r0 = "listMergeVarUpdate"
            r1 = 3
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r5] = r7
            r1[r3] = r8
            r2 = 2
            if (r9 != 0) goto L_0x0021
            java.lang.String[] r3 = new java.lang.String[r3]
            java.lang.String r4 = ""
            r3[r5] = r4
        L_0x0014:
            r1[r2] = r3
            java.lang.Object r6 = r6.callMethod(r0, r1)
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r0 = r6.booleanValue()
            return r0
        L_0x0021:
            r3 = r9
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.lists.ListMethods.listMergeVarUpdate(java.lang.String, java.lang.String, rsg.mailchimp.api.lists.MergeFieldOptions):boolean");
    }

    public boolean listWebhookAdd(String listId, String url, Integer actionsFlag, Integer sourcesFlag) throws MailChimpApiException {
        ArrayList<Object> params = new ArrayList<>(4);
        params.add(listId);
        params.add(url);
        if (actionsFlag != null) {
            params.add(Utils.getWebHookActions(actionsFlag.intValue()));
        }
        if (sourcesFlag != null) {
            params.add(Utils.getWebHookSources(sourcesFlag.intValue()));
        }
        return ((Boolean) callMethod("listWebhookAdd", params.toArray())).booleanValue();
    }

    public boolean listWebhookDel(String listId, String url) throws MailChimpApiException {
        return ((Boolean) callMethod("listWebhookDel", listId, url)).booleanValue();
    }

    public List<WebHookInfo> listWebhooks(String listId) throws MailChimpApiException {
        return callListMethod(WebHookInfo.class, "listWebhooks", listId);
    }

    public List<ListDetails> lists() throws MailChimpApiException {
        return callListMethod(ListDetails.class, "lists", new Object[0]);
    }
}
