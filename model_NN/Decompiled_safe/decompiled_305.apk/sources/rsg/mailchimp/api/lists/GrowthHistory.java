package rsg.mailchimp.api.lists;

import rsg.mailchimp.api.data.GenericStructConverter;

public class GrowthHistory extends GenericStructConverter {
    public Integer existing;
    public Integer imports;
    public String month;
    public Integer optins;
}
