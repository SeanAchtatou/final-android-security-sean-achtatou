package rsg.mailchimp.api.lists;

import rsg.mailchimp.api.Constants;

public class MailChimpListStatus {
    public String listId;
    public Constants.SubscribeStatus status;
}
