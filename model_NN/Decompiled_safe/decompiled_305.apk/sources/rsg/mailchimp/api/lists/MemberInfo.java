package rsg.mailchimp.api.lists;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import rsg.mailchimp.api.Constants;
import rsg.mailchimp.api.MailChimpApiException;
import rsg.mailchimp.api.RPCStructConverter;
import rsg.mailchimp.api.Utils;

public class MemberInfo implements RPCStructConverter {
    public String campaignId;
    public String email;
    public Constants.EmailType emailType;
    public String id;
    public String ipOpt;
    public String ipSignup;
    public MailChimpListStatus[] lists;
    public Integer memberRating;
    public MergeFieldListUtil mergeFields;
    public Constants.SubscribeStatus status;
    public Date timestamp;

    public void populateFromRPCStruct(String key, Map struct) throws MailChimpApiException {
        Utils.populateObjectFromRPCStruct(this, struct, true, "emailType", "status", "lists", "merges");
        Object rpcObj = struct.get("email_type");
        if (rpcObj != null) {
            this.emailType = Constants.EmailType.valueOf((String) rpcObj);
        }
        Object rpcObj2 = struct.get("status");
        if (rpcObj2 != null) {
            this.status = Constants.SubscribeStatus.valueOf((String) rpcObj2);
        }
        Object rpcObj3 = struct.get("merges");
        this.mergeFields = new MergeFieldListUtil();
        if (rpcObj3 instanceof Map) {
            this.mergeFields.putAll((Map) struct.get("merges"));
        }
        Object rpcObj4 = struct.get("lists");
        if (rpcObj4 instanceof Map) {
            Set<Map.Entry<String, Object>> entries = ((Map) rpcObj4).entrySet();
            this.lists = new MailChimpListStatus[entries.size()];
            int i = 0;
            for (Map.Entry<String, Object> entry : entries) {
                this.lists[i] = new MailChimpListStatus();
                this.lists[i].listId = (String) entry.getKey();
                this.lists[i].status = Constants.SubscribeStatus.valueOf(entry.getValue().toString());
                i++;
            }
        }
    }
}
