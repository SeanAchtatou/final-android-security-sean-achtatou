package rsg.mailchimp.api;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Utils {
    public static final Hashtable<String, Boolean> getWebHookActions(int flags) {
        Hashtable<String, Boolean> retVal = new Hashtable<>();
        if ((flags & 2) != 0) {
            retVal.put("subscribe", true);
        } else {
            retVal.put("subscribe", false);
        }
        if ((flags & 4) != 0) {
            retVal.put("unsubscribe", true);
        } else {
            retVal.put("unsubscribe", false);
        }
        if ((flags & 8) != 0) {
            retVal.put("profile", true);
        } else {
            retVal.put("profile", false);
        }
        if ((flags & 16) != 0) {
            retVal.put("cleaned", true);
        } else {
            retVal.put("cleaned", false);
        }
        if ((flags & 32) != 0) {
            retVal.put("upemail", true);
        } else {
            retVal.put("upemail", false);
        }
        return retVal;
    }

    public static final Hashtable<String, Boolean> getWebHookSources(int flags) {
        Hashtable<String, Boolean> retVal = new Hashtable<>();
        if ((flags & 2) != 0) {
            retVal.put("user", true);
        } else {
            retVal.put("user", false);
        }
        if ((flags & 4) != 0) {
            retVal.put("admin", true);
        } else {
            retVal.put("admin", false);
        }
        if ((flags & 8) != 0) {
            retVal.put("api", true);
        } else {
            retVal.put("api", false);
        }
        return retVal;
    }

    /* JADX INFO: Multiple debug info for r10v1 java.util.Set: [D('vals' java.util.Map), D('entries' java.util.Set<java.util.Map$Entry>)] */
    /* JADX INFO: Multiple debug info for r0v3 java.util.Map$Entry: [D('key' java.lang.String), D('entry' java.util.Map$Entry)] */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00ef, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0123, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0124, code lost:
        r1 = r5;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x017d, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01b1, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01e5, code lost:
        r10 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x021a, code lost:
        r11 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ef A[ExcHandler: SecurityException (e java.lang.SecurityException), PHI: r4 
      PHI: (r4v14 'type' java.lang.Class) = (r4v1 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class) binds: [B:6:0x002f, B:20:0x0085, B:24:0x009b, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0123 A[ExcHandler: NoSuchFieldException (e java.lang.NoSuchFieldException), PHI: r4 
      PHI: (r4v13 'type' java.lang.Class) = (r4v1 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class) binds: [B:6:0x002f, B:20:0x0085, B:24:0x009b, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x017d A[ExcHandler: IllegalArgumentException (e java.lang.IllegalArgumentException), PHI: r4 
      PHI: (r4v6 'type' java.lang.Class<?>) = (r4v1 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class) binds: [B:6:0x002f, B:20:0x0085, B:24:0x009b, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01b1 A[ExcHandler: IllegalAccessException (e java.lang.IllegalAccessException), PHI: r4 
      PHI: (r4v5 'type' java.lang.Class<?>) = (r4v1 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class) binds: [B:6:0x002f, B:20:0x0085, B:24:0x009b, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01e5 A[ExcHandler: ParseException (e java.text.ParseException), PHI: r4 
      PHI: (r4v4 'type' java.lang.Class<?>) = (r4v1 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class), (r4v15 'type' java.lang.Class) binds: [B:6:0x002f, B:20:0x0085, B:24:0x009b, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:6:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0150 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void populateObjectFromRPCStruct(java.lang.Object r9, java.util.Map r10, boolean r11, java.lang.String... r12) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            java.util.Set r10 = r10.entrySet()
            r2 = 0
            r1 = 0
            r0 = 0
            r3 = 0
            java.util.Iterator r6 = r10.iterator()
            r5 = r3
            r10 = r0
            r4 = r2
            r0 = r1
        L_0x0010:
            boolean r10 = r6.hasNext()
            if (r10 != 0) goto L_0x0017
            return
        L_0x0017:
            java.lang.Object r0 = r6.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r10 = r0.getKey()
            java.lang.String r3 = r10.toString()
            java.lang.String r2 = translateFieldName(r3)
            boolean r10 = ignoreFieldName(r12, r2)
            if (r10 != 0) goto L_0x0282
            java.lang.Class r10 = r9.getClass()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r1 = translateFieldName(r3)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.reflect.Field r1 = r10.getField(r1)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Class r4 = r1.getType()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Class<java.lang.Boolean> r10 = java.lang.Boolean.class
            if (r4 != r10) goto L_0x0056
            java.lang.Boolean r10 = new java.lang.Boolean     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Object r7 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r7 = r7.toString()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10.<init>(r7)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r1.set(r9, r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x0056:
            java.lang.Class<java.util.Date> r10 = java.util.Date.class
            if (r4 != r10) goto L_0x0081
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r10 = r10.toString()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r5 = r10.trim()     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            int r5 = r5.length()     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            if (r5 <= 0) goto L_0x027d
            java.text.SimpleDateFormat r5 = rsg.mailchimp.api.Constants.TIME_FMT     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            java.lang.Object r7 = r0.getValue()     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            java.lang.String r7 = r7.toString()     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            java.util.Date r5 = r5.parse(r7)     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            r1.set(r9, r5)     // Catch:{ SecurityException -> 0x0277, NoSuchFieldException -> 0x0271, IllegalArgumentException -> 0x026b, IllegalAccessException -> 0x0265, ParseException -> 0x0260, ClassCastException -> 0x025b }
            r5 = r10
            r0 = r3
            r10 = r2
            goto L_0x0010
        L_0x0081:
            java.lang.Class<java.lang.Integer> r10 = java.lang.Integer.class
            if (r4 != r10) goto L_0x00b0
            java.lang.Object r10 = r0.getValue()     // Catch:{ ClassCastException -> 0x009a, SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5 }
            java.lang.Number r10 = (java.lang.Number) r10     // Catch:{ ClassCastException -> 0x009a, SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5 }
            int r10 = r10.intValue()     // Catch:{ ClassCastException -> 0x009a, SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ ClassCastException -> 0x009a, SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5 }
            r1.set(r9, r10)     // Catch:{ ClassCastException -> 0x009a, SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x009a:
            r10 = move-exception
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            int r10 = java.lang.Integer.parseInt(r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r1.set(r9, r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x00b0:
            java.lang.Class<java.lang.Double> r10 = java.lang.Double.class
            if (r4 != r10) goto L_0x00e4
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Class r10 = r10.getClass()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            boolean r10 = r10.equals(r7)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            if (r10 == 0) goto L_0x00d9
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            double r7 = java.lang.Double.parseDouble(r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            java.lang.Double r10 = java.lang.Double.valueOf(r7)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r1.set(r9, r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x00d9:
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r1.set(r9, r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x00e4:
            java.lang.Object r10 = r0.getValue()     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r1.set(r9, r10)     // Catch:{ SecurityException -> 0x00ef, NoSuchFieldException -> 0x0123, IllegalArgumentException -> 0x017d, IllegalAccessException -> 0x01b1, ParseException -> 0x01e5, ClassCastException -> 0x0219 }
            r10 = r2
            r0 = r3
            goto L_0x0010
        L_0x00ef:
            r10 = move-exception
            r12 = r5
            r11 = r4
        L_0x00f2:
            rsg.mailchimp.api.MailChimpApiException r11 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r1 = "Couldn't translate values to "
            r12.<init>(r1)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " causing SecurityException, value: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.Object r12 = r0.getValue()
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r9 = r9.toString()
            r11.<init>(r9, r10)
            throw r11
        L_0x0123:
            r10 = move-exception
            r1 = r5
            r0 = r4
        L_0x0126:
            if (r11 == 0) goto L_0x0150
            java.lang.String r10 = "MailChimp"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Unable to find field named: "
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r5 = " in class "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.Class r5 = r9.getClass()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            android.util.Log.e(r10, r4)
            r5 = r1
            r10 = r2
            r4 = r0
            r0 = r3
            goto L_0x0010
        L_0x0150:
            rsg.mailchimp.api.MailChimpApiException r11 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r0 = "Couldn't translate values to "
            r12.<init>(r0)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " field doens't exist: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r2)
            java.lang.String r9 = r9.toString()
            r11.<init>(r9, r10)
            throw r11
        L_0x017d:
            r10 = move-exception
            r12 = r5
            r11 = r4
        L_0x0180:
            rsg.mailchimp.api.MailChimpApiException r11 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r1 = "Couldn't translate values to "
            r12.<init>(r1)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " causing IllegalArgumentException, value: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.Object r12 = r0.getValue()
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r9 = r9.toString()
            r11.<init>(r9, r10)
            throw r11
        L_0x01b1:
            r10 = move-exception
            r12 = r5
            r11 = r4
        L_0x01b4:
            rsg.mailchimp.api.MailChimpApiException r11 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r1 = "Couldn't translate values to "
            r12.<init>(r1)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " causing IllegalAccessException, value: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.Object r12 = r0.getValue()
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r9 = r9.toString()
            r11.<init>(r9, r10)
            throw r11
        L_0x01e5:
            r10 = move-exception
            r12 = r5
            r11 = r4
        L_0x01e8:
            rsg.mailchimp.api.MailChimpApiException r11 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r1 = "Couldn't translate values to "
            r12.<init>(r1)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " causing parse failure, value that couldn't be parsed: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.Object r12 = r0.getValue()
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r9 = r9.toString()
            r11.<init>(r9, r10)
            throw r11
        L_0x0219:
            r10 = move-exception
            r12 = r5
            r11 = r4
        L_0x021c:
            rsg.mailchimp.api.MailChimpApiException r10 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r1 = "Couldn't transalte values to "
            r12.<init>(r1)
            java.lang.Class r9 = r9.getClass()
            java.lang.StringBuilder r9 = r12.append(r9)
            java.lang.String r12 = ", key: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r12 = " is of type: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.Object r12 = r0.getValue()
            java.lang.Class r12 = r12.getClass()
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r12 = " and field is of type: "
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.StringBuilder r9 = r9.append(r11)
            java.lang.String r9 = r9.toString()
            r10.<init>(r9)
            throw r10
        L_0x025b:
            r11 = move-exception
            r12 = r10
            r10 = r11
            r11 = r4
            goto L_0x021c
        L_0x0260:
            r11 = move-exception
            r12 = r10
            r10 = r11
            r11 = r4
            goto L_0x01e8
        L_0x0265:
            r11 = move-exception
            r12 = r10
            r10 = r11
            r11 = r4
            goto L_0x01b4
        L_0x026b:
            r11 = move-exception
            r12 = r10
            r10 = r11
            r11 = r4
            goto L_0x0180
        L_0x0271:
            r0 = move-exception
            r1 = r10
            r10 = r0
            r0 = r4
            goto L_0x0126
        L_0x0277:
            r11 = move-exception
            r12 = r10
            r10 = r11
            r11 = r4
            goto L_0x00f2
        L_0x027d:
            r5 = r10
            r0 = r3
            r10 = r2
            goto L_0x0010
        L_0x0282:
            r10 = r2
            r0 = r3
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.Utils.populateObjectFromRPCStruct(java.lang.Object, java.util.Map, boolean, java.lang.String[]):void");
    }

    static boolean ignoreFieldName(String[] ignoreFieldNames, String fieldName) {
        for (String ignore : ignoreFieldNames) {
            if (ignore.equals(fieldName)) {
                return true;
            }
        }
        return false;
    }

    public static String translateFieldName(String key) {
        StringBuffer buf = new StringBuffer();
        int i = 0;
        while (i < key.length()) {
            char c = key.charAt(i);
            switch (c) {
                case '_':
                    buf.append(Character.toUpperCase(key.charAt(i + 1)));
                    i++;
                    break;
                default:
                    buf.append(c);
                    break;
            }
            i++;
        }
        return buf.toString();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends rsg.mailchimp.api.RPCStructConverter> java.util.ArrayList<T> extractObjectsFromList(java.lang.Class<T> r10, java.lang.Object[] r11) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            r1 = r11
            java.util.ArrayList r5 = new java.util.ArrayList
            int r7 = r1.length
            r5.<init>(r7)
            r6 = 0
            int r7 = r1.length
            r8 = 0
        L_0x000a:
            if (r8 < r7) goto L_0x000d
            return r5
        L_0x000d:
            r4 = r1[r8]
            r0 = r4
            java.util.Map r0 = (java.util.Map) r0
            r6 = r0
            java.lang.Object r3 = r10.newInstance()     // Catch:{ IllegalAccessException -> 0x0023, InstantiationException -> 0x0044 }
            rsg.mailchimp.api.RPCStructConverter r3 = (rsg.mailchimp.api.RPCStructConverter) r3     // Catch:{ IllegalAccessException -> 0x0023, InstantiationException -> 0x0044 }
            r9 = 0
            r3.populateFromRPCStruct(r9, r6)
            r5.add(r3)
            int r8 = r8 + 1
            goto L_0x000a
        L_0x0023:
            r7 = move-exception
            r2 = r7
            rsg.mailchimp.api.MailChimpApiException r7 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Couldn't create instance of class ("
            r8.<init>(r9)
            java.lang.String r9 = r10.getName()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ") make sure it is publically accessible"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.<init>(r8)
            throw r7
        L_0x0044:
            r7 = move-exception
            r2 = r7
            rsg.mailchimp.api.MailChimpApiException r7 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Couldn't create instance of class ("
            r8.<init>(r9)
            java.lang.String r9 = r10.getName()
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ") make sure it has a zero-args constructor"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.<init>(r8)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.Utils.extractObjectsFromList(java.lang.Class, java.lang.Object[]):java.util.ArrayList");
    }

    public static List<String> convertObjectArrayToString(Object[] array) {
        ArrayList<String> vals = new ArrayList<>(array.length);
        for (Object o : array) {
            vals.add(o.toString());
        }
        return vals;
    }
}
