package rsg.mailchimp.api;

import android.content.Context;
import com.crittermap.backcountrynavigator.R;
import java.text.MessageFormat;
import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;

public class MailChimpApi {
    private static final MessageFormat API_URL = new MessageFormat("https://{0}.api.mailchimp.com/1.2/");
    private static final String DEFAULT_DATA_CENTER = "us1";
    protected String apiKey;
    private String endpointUrl;

    public MailChimpApi(Context ctx) {
        this(ctx.getResources().getText(R.anim.fade));
    }

    public MailChimpApi(CharSequence apiKey2) {
        this.apiKey = apiKey2.toString();
        this.endpointUrl = API_URL.format(new Object[]{parseDataCenter(this.apiKey)});
    }

    /* access modifiers changed from: protected */
    public MailChimpApiException buildMailChimpException(XMLRPCException e) {
        return new MailChimpApiException("API Call failed, due to '" + e.getMessage() + "'", e);
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected <T extends rsg.mailchimp.api.RPCStructConverter> java.util.List<T> callListMethod(java.lang.Class<T> r12, java.lang.String r13, java.lang.Object... r14) throws rsg.mailchimp.api.MailChimpApiException {
        /*
            r11 = this;
            java.lang.Object r1 = r11.callMethod(r13, r14)
            boolean r8 = r1 instanceof java.lang.Object[]
            if (r8 == 0) goto L_0x0010
            java.lang.Object[] r1 = (java.lang.Object[]) r1
            java.util.ArrayList r6 = rsg.mailchimp.api.Utils.extractObjectsFromList(r12, r1)
            r8 = r6
        L_0x000f:
            return r8
        L_0x0010:
            boolean r8 = r1 instanceof java.util.Map
            if (r8 == 0) goto L_0x0092
            r0 = r1
            java.util.Map r0 = (java.util.Map) r0
            r7 = r0
            java.util.Set r3 = r7.entrySet()
            java.util.ArrayList r6 = new java.util.ArrayList
            int r8 = r3.size()
            r6.<init>(r8)
            java.util.Iterator r9 = r3.iterator()
        L_0x0029:
            boolean r8 = r9.hasNext()
            if (r8 != 0) goto L_0x0031
            r8 = r6
            goto L_0x000f
        L_0x0031:
            java.lang.Object r4 = r9.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            java.lang.Object r5 = r12.newInstance()     // Catch:{ IllegalAccessException -> 0x0050, InstantiationException -> 0x0071 }
            rsg.mailchimp.api.RPCStructConverter r5 = (rsg.mailchimp.api.RPCStructConverter) r5     // Catch:{ IllegalAccessException -> 0x0050, InstantiationException -> 0x0071 }
            java.lang.Object r11 = r4.getKey()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r8 = r4.getValue()
            java.util.Map r8 = (java.util.Map) r8
            r5.populateFromRPCStruct(r11, r8)
            r6.add(r5)
            goto L_0x0029
        L_0x0050:
            r8 = move-exception
            r2 = r8
            rsg.mailchimp.api.MailChimpApiException r8 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Couldn't create instance of class ("
            r9.<init>(r10)
            java.lang.String r10 = r12.getName()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ") make sure it is publically accessible"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            r8.<init>(r9)
            throw r8
        L_0x0071:
            r8 = move-exception
            r2 = r8
            rsg.mailchimp.api.MailChimpApiException r8 = new rsg.mailchimp.api.MailChimpApiException
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Couldn't create instance of class ("
            r9.<init>(r10)
            java.lang.String r10 = r12.getName()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ") make sure it has a zero-args constructor"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            r8.<init>(r9)
            throw r8
        L_0x0092:
            java.util.ArrayList r8 = new java.util.ArrayList
            r9 = 0
            r8.<init>(r9)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: rsg.mailchimp.api.MailChimpApi.callListMethod(java.lang.Class, java.lang.String, java.lang.Object[]):java.util.List");
    }

    /* access modifiers changed from: protected */
    public Object callMethod(String methodName, Object... params) throws MailChimpApiException {
        int i = 1;
        if (params != null) {
            i = params.length + 1;
        }
        try {
            Object[] parameters = new Object[i];
            if (params != null && params.length > 0) {
                System.arraycopy(params, 0, parameters, 1, params.length);
            }
            parameters[0] = this.apiKey;
            return getClient().callEx(methodName, parameters);
        } catch (XMLRPCException e) {
            throw buildMailChimpException(e);
        }
    }

    /* access modifiers changed from: protected */
    public XMLRPCClient getClient() {
        return new XMLRPCClient(this.endpointUrl);
    }

    private static final String parseDataCenter(String apiKey2) {
        int index = apiKey2.lastIndexOf(45);
        if (index > 0) {
            return apiKey2.substring(index + 1);
        }
        return DEFAULT_DATA_CENTER;
    }
}
