package rsg.mailchimp.gui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.crittermap.backcountrynavigator.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import rsg.mailchimp.api.MailChimpApiException;
import rsg.mailchimp.api.lists.ListMethods;
import rsg.mailchimp.api.lists.MergeFieldListUtil;

public class Subscribe extends Activity implements View.OnClickListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.attrib_display_activity);
        ((Button) findViewById(R.xml.map_germanydls)).setOnClickListener(this);
        ((Button) findViewById(R.xml.map_italyigm)).setOnClickListener(this);
    }

    public void onClick(View clicked) {
        Log.d(getClass().getName(), "Clicked: " + clicked.toString());
        switch (clicked.getId()) {
            case R.xml.map_germanydls:
                final ProgressDialog dialog = ProgressDialog.show(this, getResources().getText(R.anim.hold), getResources().getText(R.anim.push_left_in), true, false);
                new Thread(new Runnable() {
                    public void run() {
                        EditText text = (EditText) Subscribe.this.findViewById(R.xml.map_cantopo);
                        if (text.getText() != null && text.getText().toString().trim().length() > 0) {
                            Subscribe.this.addToList(text.getText().toString(), dialog);
                        }
                    }
                }).start();
                return;
            case R.xml.map_italyigm:
                ((EditText) findViewById(R.xml.map_cantopo)).setText("");
                return;
            default:
                Log.e("MailChimp", "Unable to handle onClick for view " + clicked.toString());
                return;
        }
    }

    /* access modifiers changed from: private */
    public void addToList(String emailAddy, ProgressDialog dialog) {
        MergeFieldListUtil mergeFields = new MergeFieldListUtil();
        mergeFields.addEmail(emailAddy);
        try {
            mergeFields.addDateField("BIRFDAY", new SimpleDateFormat("MM/dd/yyyy").parse("07/30/2007"));
        } catch (ParseException e) {
            Log.e("MailChimp", "Couldn't parse date, boo: " + e.getMessage());
        }
        mergeFields.addField("FNAME", "Ona");
        mergeFields.addField("LNAME", "StoutMuntz");
        try {
            new ListMethods(getResources().getText(R.anim.fade)).listSubscribe(getText(R.anim.fadeout).toString(), emailAddy, mergeFields);
        } catch (MailChimpApiException e2) {
            Log.e("MailChimp", "Exception subscribing person: " + e2.getMessage());
        }
        dialog.cancel();
    }
}
