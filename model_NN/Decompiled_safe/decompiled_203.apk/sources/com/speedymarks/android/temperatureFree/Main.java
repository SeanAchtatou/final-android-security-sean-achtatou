package com.speedymarks.android.temperatureFree;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class Main extends Activity implements LocationListener {
    private static final String LOG_TAG = "Temperature Free";
    /* access modifiers changed from: private */
    public Location lastLocation = null;
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public LocationManager myManager;
    private boolean pauseUpdates = false;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.mWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.addJavascriptInterface(new LocationJavaScriptInterface(), "android");
        this.mWebView.setVerticalScrollbarOverlay(true);
        this.mWebView.clearCache(true);
        this.mWebView.setBackgroundColor(-12566464);
        CookieSyncManager.createInstance(this.mWebView.getContext());
        this.mWebView.loadUrl("http://temperature.worldclockr.com/android.html");
        this.myManager = (LocationManager) getSystemService("location");
    }

    public void onResume() {
        CookieSyncManager.getInstance().startSync();
        CookieSyncManager.getInstance().sync();
        startListening();
        super.onResume();
    }

    public void onPause() {
        CookieSyncManager.getInstance().sync();
        stopListening();
        super.onPause();
    }

    public void onStop() {
        CookieSyncManager.getInstance().sync();
        CookieSyncManager.getInstance().stopSync();
        stopListening();
        super.onStop();
    }

    private void startListening() {
        if (this.myManager.isProviderEnabled("gps")) {
            this.myManager.requestLocationUpdates("gps", 1800000, 5000.0f, this);
        } else if (this.myManager.isProviderEnabled("network")) {
            this.myManager.requestLocationUpdates("network", 1800000, 5000.0f, this);
        }
    }

    private void stopListening() {
        if (this.myManager != null) {
            this.myManager.removeUpdates(this);
        }
    }

    /* access modifiers changed from: private */
    public void sendLocationUpdate(String msg) {
        if (!this.pauseUpdates) {
            if (this.lastLocation != null) {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        String alt = null;
                        if (Main.this.lastLocation.hasAltitude()) {
                            alt = new StringBuilder().append(Main.this.lastLocation.getAltitude()).toString();
                        }
                        String sats = null;
                        if (Main.this.lastLocation.getExtras() != null) {
                            sats = Main.this.lastLocation.getExtras().getString("satellites");
                        }
                        Main.this.mWebView.loadUrl("javascript:setLocation(" + Main.this.lastLocation.getLatitude() + "," + Main.this.lastLocation.getLongitude() + "," + alt + "," + Main.this.lastLocation.getAccuracy() + "," + Main.this.lastLocation.getSpeed() + "," + sats + ")");
                    }
                });
            } else {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        Main.this.mWebView.loadUrl("javascript:debug('update failed')");
                        Main.this.mWebView.loadUrl("javascript:setLocation('error')");
                    }
                });
            }
        }
    }

    public void onLocationChanged(Location location) {
        this.lastLocation = location;
        sendLocationUpdate("set location");
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void setPauseUpdates(boolean pauseUpdates2) {
        this.pauseUpdates = pauseUpdates2;
        if (!pauseUpdates2) {
            sendLocationUpdate("After pause");
        }
    }

    final class LocationJavaScriptInterface {
        LocationJavaScriptInterface() {
        }

        public void updateLocation() {
            if (Main.this.myManager.isProviderEnabled("gps")) {
                Main.this.lastLocation = Main.this.myManager.getLastKnownLocation("gps");
                Log.d(Main.LOG_TAG, "GPS:" + Main.this.lastLocation);
            }
            if (Main.this.lastLocation == null && Main.this.myManager.isProviderEnabled("network")) {
                Main.this.lastLocation = Main.this.myManager.getLastKnownLocation("network");
                Log.d(Main.LOG_TAG, "Network:" + Main.this.lastLocation);
            }
            if (Main.this.lastLocation == null) {
                Log.d(Main.LOG_TAG, "No Provider:" + Main.this.myManager);
            }
            Log.d(Main.LOG_TAG, "updateLocation:" + Main.this.lastLocation);
            Main.this.sendLocationUpdate("updated");
        }

        public void setPause(boolean pauseUpdates) {
            Main.this.setPauseUpdates(pauseUpdates);
        }

        public void playSound(String sound, boolean loop) {
        }

        public void stopSound() {
        }

        public void vibrate() {
            ((Vibrator) Main.this.getSystemService("vibrator")).vibrate(500);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Log.d(Main.LOG_TAG, message);
            result.confirm();
            return true;
        }
    }
}
