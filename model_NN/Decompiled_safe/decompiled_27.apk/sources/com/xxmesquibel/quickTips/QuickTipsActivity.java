package com.xxmesquibel.quickTips;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.text.DecimalFormat;

public class QuickTipsActivity extends Activity {
    private TextView bill;
    private double billData;
    private EditText billET;
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.calcButton) {
                QuickTipsActivity.this.calculate();
                ((InputMethodManager) QuickTipsActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(QuickTipsActivity.this.calcButton.getApplicationWindowToken(), 0);
            }
            if (v.getId() == R.id.resetButton) {
                QuickTipsActivity.this.reset();
            }
        }
    };
    /* access modifiers changed from: private */
    public Button calcButton;
    /* access modifiers changed from: private */
    public SeekBar customPercent;
    private RadioButton customRB;
    int percent;
    /* access modifiers changed from: private */
    public EditText percentET;
    private RadioGroup percentRG;
    private RadioButton rB15;
    private Button resetButton;
    private TextView tip;
    private double tipData;
    private TextView total;
    private double totalData;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.billET = (EditText) findViewById(R.id.billAmount);
        this.percentRG = (RadioGroup) findViewById(R.id.percentRG);
        this.rB15 = (RadioButton) findViewById(R.id.rB15);
        this.customRB = (RadioButton) findViewById(R.id.customRB);
        this.percentET = (EditText) findViewById(R.id.percentET);
        this.customPercent = (SeekBar) findViewById(R.id.customPercent);
        this.bill = (TextView) findViewById(R.id.bill);
        this.tip = (TextView) findViewById(R.id.tip);
        this.total = (TextView) findViewById(R.id.total);
        this.calcButton = (Button) findViewById(R.id.calcButton);
        this.resetButton = (Button) findViewById(R.id.resetButton);
        getWindow().setSoftInputMode(3);
        ((AdView) findViewById(R.id.myAdd)).loadAd(new AdRequest());
        this.customPercent.setEnabled(false);
        this.percentET.setEnabled(false);
        this.percentRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rB15) {
                    QuickTipsActivity.this.customPercent.setEnabled(false);
                    QuickTipsActivity.this.percentET.setEnabled(false);
                    QuickTipsActivity.this.percentET.setText("15");
                }
                if (checkedId == R.id.customRB) {
                    QuickTipsActivity.this.customPercent.setEnabled(true);
                    QuickTipsActivity.this.percentET.setEnabled(true);
                    QuickTipsActivity.this.percentET.requestFocus();
                    QuickTipsActivity.this.percentET.selectAll();
                }
            }
        });
        this.customPercent.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int percent, boolean fromUser) {
                QuickTipsActivity.this.percentET.setText(Integer.toString(percent));
            }
        });
        this.calcButton.setOnClickListener(this.buttonListener);
        this.resetButton.setOnClickListener(this.buttonListener);
    }

    /* access modifiers changed from: private */
    public void calculate() {
        if (this.billET.getText().toString().equals("") && this.percentET.getText().toString().equals("")) {
            this.billData = 0.0d;
            this.percent = 0;
            this.tipData = (this.billData * ((double) this.percent)) / 100.0d;
            this.totalData = this.tipData + this.billData;
            DecimalFormat myFormatter = new DecimalFormat("0.00");
            String billS = myFormatter.format(this.billData);
            String tipS = myFormatter.format(this.tipData);
            String totalS = myFormatter.format(this.totalData);
            this.bill.setText(billS);
            this.tip.setText(tipS);
            this.total.setText(totalS);
        } else if (this.billET.getText().toString().equals("") && this.percentET.getText().toString().length() > 0) {
            this.billData = 0.0d;
            this.percent = Integer.parseInt(this.percentET.getText().toString());
            this.tipData = (this.billData * ((double) this.percent)) / 100.0d;
            this.totalData = this.tipData + this.billData;
            DecimalFormat myFormatter2 = new DecimalFormat("0.00");
            String billS2 = myFormatter2.format(this.billData);
            String tipS2 = myFormatter2.format(this.tipData);
            String totalS2 = myFormatter2.format(this.totalData);
            this.bill.setText(billS2);
            this.tip.setText(tipS2);
            this.total.setText(totalS2);
        } else if (!this.percentET.getText().toString().equals("") || this.billET.getText().toString().length() <= 0) {
            this.billData = Double.parseDouble(this.billET.getText().toString());
            this.percent = Integer.parseInt(this.percentET.getText().toString());
            this.tipData = (this.billData * ((double) this.percent)) / 100.0d;
            this.totalData = this.tipData + this.billData;
            DecimalFormat myFormatter3 = new DecimalFormat("0.00");
            String billS3 = myFormatter3.format(this.billData);
            String tipS3 = myFormatter3.format(this.tipData);
            String totalS3 = myFormatter3.format(this.totalData);
            this.bill.setText(billS3);
            this.tip.setText(tipS3);
            this.total.setText(totalS3);
        } else {
            this.percent = 0;
            this.billData = Double.parseDouble(this.billET.getText().toString());
            this.tipData = (this.billData * ((double) this.percent)) / 100.0d;
            this.totalData = this.tipData + this.billData;
            DecimalFormat myFormatter4 = new DecimalFormat("0.00");
            String billS4 = myFormatter4.format(this.billData);
            String tipS4 = myFormatter4.format(this.tipData);
            String totalS4 = myFormatter4.format(this.totalData);
            this.bill.setText(billS4);
            this.tip.setText(tipS4);
            this.total.setText(totalS4);
        }
    }

    /* access modifiers changed from: private */
    public void reset() {
        this.billET.setText("");
        this.rB15.setChecked(true);
        this.percentET.setText("15");
        this.bill.setText("0");
        this.tip.setText("0");
        this.total.setText("0");
    }
}
