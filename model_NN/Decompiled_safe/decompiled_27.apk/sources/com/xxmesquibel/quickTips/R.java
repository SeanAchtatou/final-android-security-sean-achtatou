package com.xxmesquibel.quickTips;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int green = 2130968577;
        public static final int lightGreen = 2130968578;
        public static final int orange = 2130968579;
        public static final int white = 2130968576;
    }

    public static final class drawable {
        public static final int calc = 2130837504;
        public static final int seek_background = 2130837505;
    }

    public static final class id {
        public static final int bill = 2131099665;
        public static final int billAmount = 2131099651;
        public static final int billSum = 2131099663;
        public static final int billSumTV = 2131099664;
        public static final int billTV = 2131099650;
        public static final int calcButton = 2131099660;
        public static final int customPercent = 2131099658;
        public static final int customRB = 2131099656;
        public static final int mainLayout = 2131099648;
        public static final int myAdd = 2131099670;
        public static final int percentET = 2131099657;
        public static final int percentRG = 2131099654;
        public static final int percentTV = 2131099653;
        public static final int rB15 = 2131099655;
        public static final int relative1 = 2131099649;
        public static final int resetButton = 2131099661;
        public static final int seperator1 = 2131099652;
        public static final int seperator2 = 2131099659;
        public static final int seperator3 = 2131099662;
        public static final int tip = 2131099667;
        public static final int tipSumTV = 2131099666;
        public static final int total = 2131099669;
        public static final int totalSumTV = 2131099668;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int billSummary = 2131034118;
        public static final int billTitle = 2131034113;
        public static final int calculate = 2131034122;
        public static final int custom = 2131034115;
        public static final int fifteen = 2131034116;
        public static final int percentTitle = 2131034114;
        public static final int reset = 2131034123;
        public static final int summary = 2131034117;
        public static final int tipSummary = 2131034119;
        public static final int totalBill = 2131034120;
        public static final int zero = 2131034121;
    }
}
