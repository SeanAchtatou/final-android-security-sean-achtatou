package net.monkey;

public class BalooneYellow {
    public int life = 6;
    public int speed = -6;
    public int state = 0;
    public int x;
    public int y;

    public BalooneYellow(int x2, int y2, int speed2) {
        this.x = x2;
        this.y = y2;
        this.speed = speed2;
    }

    public int getX() {
        return this.x;
    }

    public void setY() {
        this.y -= 2;
    }

    public int getY() {
        return this.y;
    }

    public void setX() {
        this.x += this.speed;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }
}
