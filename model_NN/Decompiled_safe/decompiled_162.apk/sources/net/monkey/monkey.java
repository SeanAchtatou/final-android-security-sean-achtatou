package net.monkey;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class monkey extends Activity {
    private static final String AD_UNIT_ID = "a14e3806f3407ce";
    public static final int CREATE_AD = 1;
    public static final int REMOVE_AD = 2;
    private static final String TEST_DEVICE_ID = "********************************";
    public static final String version = "1.00";
    public AdView mAdView = null;
    private Handler mHandler;
    private FrameLayout mLayout;
    private monkeyView mMainView = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.mLayout = new FrameLayout(this);
        this.mMainView = new monkeyView(this);
        TextView tv = new TextView(this);
        try {
            HttpURLConnection http = (HttpURLConnection) new URL("http://www24.atpages.jp/spaceflash2/saru/saru_version.html").openConnection();
            http.setRequestMethod("GET");
            http.connect();
            InputStream in = http.getInputStream();
            byte[] b = new byte[1024];
            in.read(b);
            in.close();
            http.disconnect();
            String[] c = new String(b).split("version", 0);
            if (version.equals(c[1])) {
                this.mLayout.addView(this.mMainView);
            } else {
                tv.setText(c[1]);
                this.mLayout.addView(tv);
            }
        } catch (Exception e) {
            tv.setText("NOT WEB SERVICE!!");
            this.mLayout.addView(tv);
            this.mLayout.addView(this.mMainView);
        }
        requestWindowFeature(1);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (((Integer) msg.obj).intValue()) {
                    case monkey.CREATE_AD /*1*/:
                        monkey.this.createAd();
                        return;
                    case monkey.REMOVE_AD /*2*/:
                        monkey.this.removeAd();
                        return;
                    default:
                        return;
                }
            }
        };
        setRequestedOrientation(0);
        setContentView(this.mLayout);
        createAd();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mMainView.Ad_flg == 1) {
            removeAd();
            this.mMainView.Ad_flg = 0;
        } else if (this.mMainView.Ad_flg == 1) {
            createAd();
            this.mMainView.Ad_flg = 0;
        }
        return true;
    }

    public void createAd() {
        if (this.mAdView == null) {
            this.mAdView = new AdView(this, AdSize.BANNER, AD_UNIT_ID);
            AdRequest req = new AdRequest();
            req.addTestDevice(AdRequest.TEST_EMULATOR);
            this.mAdView.loadAd(req);
            this.mLayout.addView(this.mAdView, new FrameLayout.LayoutParams(-1, -2, 48));
        }
    }

    public void removeAd() {
        if (this.mAdView != null) {
            this.mLayout.removeView(this.mAdView);
            this.mAdView = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mAdView.destroy();
        super.onDestroy();
    }
}
