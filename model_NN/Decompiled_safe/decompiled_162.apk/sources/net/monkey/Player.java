package net.monkey;

public class Player {
    public int damCnt = 0;
    public int damFlg = 0;
    public int lefe = 3;
    public int life = 6;
    public int speed = -6;
    public int state = 0;
    public int x;
    public int y;

    public Player(int x2, int y2, int speed2) {
        this.x = x2;
        this.y = y2;
        this.speed = speed2;
        this.life = 3;
    }

    public int getLife() {
        return this.life;
    }

    public void setLife(int i) {
        this.life += i;
    }

    public int getState() {
        return this.state;
    }

    public int getDamFlg() {
        return this.damFlg;
    }

    public void setDamFlg(int i) {
        this.damFlg = i;
        this.damCnt = 0;
    }

    public void goDamFlg() {
        if (this.damFlg != 0) {
            this.damCnt++;
            this.y += 2;
            if (this.damCnt >= 20) {
                this.damFlg = 0;
                if (this.state == 2) {
                    this.state = 0;
                } else {
                    this.state = 1;
                }
                this.damCnt = 0;
            }
        }
    }

    public void setState(int i) {
        this.state = i;
    }

    public int getX() {
        return this.x;
    }

    public void setY() {
        this.y += this.speed;
    }

    public void goY(int y2) {
        this.y += y2;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int i) {
        this.x += i;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }
}
