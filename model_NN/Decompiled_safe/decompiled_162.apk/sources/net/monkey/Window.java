package net.monkey;

public class Window {
    public int life = 6;
    public int speed = -6;
    public int state = 0;
    public int x;
    public int y;

    public Window(int x2, int y2, int speed2) {
        this.x = x2;
        this.y = y2;
        this.speed = speed2;
    }

    public int getX() {
        return this.x;
    }

    public void setY() {
        this.y += this.speed;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int i) {
        if (i == 0) {
            this.x--;
        } else {
            this.x++;
        }
    }

    public void setSpeed(int s) {
        this.speed = s;
    }
}
