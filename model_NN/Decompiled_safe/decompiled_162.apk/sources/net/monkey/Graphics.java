package net.monkey;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;

public class Graphics {
    private Canvas canvas;
    private SurfaceHolder holder;
    private Paint paint = new Paint();
    private int perH;
    private int perW;

    public Graphics(SurfaceHolder holder2, int w, int h) {
        this.holder = holder2;
        if (w < h) {
            int temp = w;
            w = h;
            h = temp;
        }
        this.perW = w;
        this.perH = h;
        this.paint.setAntiAlias(true);
    }

    public void lock() {
        this.canvas = this.holder.lockCanvas();
    }

    public void unlock() {
        this.holder.unlockCanvasAndPost(this.canvas);
    }

    public void setColor(int color) {
        this.paint.setColor(color);
    }

    public void setFontSize(int fontSize) {
        this.paint.setTextSize((float) fontSize);
    }

    public int stringWidth(String string) {
        return (int) this.paint.measureText(string);
    }

    public void fillRect(int x, int y, int w, int h) {
        this.paint.setStyle(Paint.Style.FILL);
        this.canvas.drawRect(new Rect(x, y, x + w, y + h), this.paint);
    }

    public void drawBitmap(Bitmap bitmap, int x, int y) {
        this.canvas.drawBitmap(bitmap, (float) x, (float) y, (Paint) null);
    }

    public void drawBigBitmap(Bitmap bitmap, int x, int y) {
        this.canvas.drawBitmap(bitmap, new Rect(x, y, bitmap.getWidth(), bitmap.getHeight()), new Rect(x, y, this.perW, this.perH), (Paint) null);
    }

    public void drawString(String string, int x, int y) {
        this.canvas.drawText(string, (float) x, (float) y, this.paint);
    }
}
