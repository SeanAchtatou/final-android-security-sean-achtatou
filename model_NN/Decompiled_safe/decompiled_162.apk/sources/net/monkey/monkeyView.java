package net.monkey;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import java.util.ArrayList;
import java.util.Random;

public class monkeyView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private static final int B_HEIGHT = 50;
    private static final int B_WIDTH = 50;
    private static final int M_EX = 670;
    private static final int M_EY = 320;
    private static final int M_FX = 550;
    private static final int M_FY = 200;
    private static final int S_DUM = 6;
    private static final int S_END = 5;
    private static final int S_FIRST = 7;
    private static final int S_GAMEOVER = 3;
    private static final int S_LODING = 4;
    private static final int S_LOGO = 1;
    private static final int S_PLAY = 2;
    private static final int S_TITLE = 0;
    private static final int T_Time = 1;
    private static final int T_maxTime = 600;
    private static final int T_syutsuTime = 150;
    private static final int T_tekiTime = 3;
    public static Bitmap backGd = null;
    public static Bitmap bana2_img = null;
    public static Bitmap bana_img = null;
    public static Bitmap black_img = null;
    public static Bitmap blue_img = null;
    public static Bitmap green_img = null;
    public static Bitmap load_img = null;
    public static Bitmap logo1_img = null;
    public static Bitmap logo2_img = null;
    public static Bitmap logo3_img = null;
    public static Bitmap logo4_img = null;
    public static Bitmap logo5_img = null;
    public static Bitmap logo6_img = null;
    public static Bitmap monkey_left1_img = null;
    public static Bitmap monkey_left2_img = null;
    public static Bitmap monkey_right1_img = null;
    public static Bitmap monkey_right2_img = null;
    public static Bitmap music_off = null;
    public static Bitmap music_on = null;
    public static Bitmap over_img = null;
    private static final int player_height = 100;
    private static final int player_width = 80;
    private static final int player_x = 380;
    private static final int player_y = 50;
    private static final int rank_height = 100;
    private static final int rank_start_x = 330;
    private static final int rank_start_y = 230;
    private static final int rank_width = 150;
    public static Bitmap red_img = null;
    public static Bitmap title_img = null;
    private static final int win_Height = 100;
    private static final int win_Width = 150;
    private static final int win_pos1_X = 150;
    private static final int win_pos1_Y = 600;
    private static final int win_pos2_X = 350;
    private static final int win_pos2_Y = 600;
    private static final int win_pos3_X = 550;
    private static final int win_pos3_Y = 600;
    public static Bitmap window_img;
    public static Bitmap yellow_img;
    public int Ad_flg = S_TITLE;
    Activity activity;
    private ArrayList<Banana> bana = new ArrayList<>();
    private MediaPlayer battle1;
    private ArrayList<BalooneBlack> black = new ArrayList<>();
    private ArrayList<BalooneBlue> blue = new ArrayList<>();
    int change_logo = S_TITLE;
    private int cnt = 10;
    private String debug = "";
    private int firstFlg = 1;
    int frist;
    private Graphics g;
    int gameover_flg = 10;
    private ArrayList<BalooneGreen> green = new ArrayList<>();
    private SurfaceHolder holder;
    private int init = S_TITLE;
    private double lastRot;
    public int life = S_END;
    private int loadFlg = S_TITLE;
    private int logo_flg = S_TITLE;
    private GestureDetector m_clGestureDetector;
    public int maxBomb = 3;
    public int maxWidth;
    public int moveCnt = S_TITLE;
    public int moveFlg = S_TITLE;
    public int music_flg = 1;
    private Context myCon;
    private Player play;
    public int point = S_TITLE;
    private int rank1;
    private int rank2;
    private int rank3;
    private int rank4;
    private int rank5;
    private ArrayList<BalooneRed> red = new ArrayList<>();
    private int score;
    public int speed = -3;
    private int stage = 1;
    private int state = 1;
    private Thread thread;
    private int timer;
    private ArrayList<Window> win = new ArrayList<>();
    private ArrayList<BalooneYellow> yellow = new ArrayList<>();

    public monkeyView(Context context) {
        super(context);
        this.myCon = context;
        this.play = new Player(player_x, 50, S_LODING);
        Resources r = getResources();
        backGd = BitmapFactory.decodeResource(r, R.drawable.bg2);
        monkey_right1_img = BitmapFactory.decodeResource(r, R.drawable.monkey_right_1);
        monkey_right2_img = BitmapFactory.decodeResource(r, R.drawable.monkey_right_2);
        monkey_left1_img = BitmapFactory.decodeResource(r, R.drawable.monkey_left_1);
        monkey_left2_img = BitmapFactory.decodeResource(r, R.drawable.monkey_left_2);
        window_img = BitmapFactory.decodeResource(r, R.drawable.window);
        red_img = BitmapFactory.decodeResource(r, R.drawable.baloon_red);
        blue_img = BitmapFactory.decodeResource(r, R.drawable.baloon_blue);
        green_img = BitmapFactory.decodeResource(r, R.drawable.baloon_green);
        black_img = BitmapFactory.decodeResource(r, R.drawable.baloon_black);
        yellow_img = BitmapFactory.decodeResource(r, R.drawable.baloon_yellow);
        bana_img = BitmapFactory.decodeResource(r, R.drawable.bana);
        bana2_img = BitmapFactory.decodeResource(r, R.drawable.bana2);
        title_img = BitmapFactory.decodeResource(r, R.drawable.title);
        logo1_img = BitmapFactory.decodeResource(r, R.drawable.logo1);
        logo2_img = BitmapFactory.decodeResource(r, R.drawable.logo2);
        logo3_img = BitmapFactory.decodeResource(r, R.drawable.logo3);
        logo4_img = BitmapFactory.decodeResource(r, R.drawable.logo4);
        logo5_img = BitmapFactory.decodeResource(r, R.drawable.logo5);
        logo6_img = BitmapFactory.decodeResource(r, R.drawable.logo6);
        load_img = BitmapFactory.decodeResource(r, R.drawable.loader);
        over_img = BitmapFactory.decodeResource(r, R.drawable.gameover2);
        this.activity = (Activity) getContext();
        this.holder = getHolder();
        this.holder.addCallback(this);
        Display disp = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        int width = disp.getWidth();
        int height = disp.getHeight();
        if (width < height) {
            this.maxWidth = height;
        } else {
            this.maxWidth = width;
        }
        this.g = new Graphics(this.holder, width, height);
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        stop_all_music();
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int w, int h) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void stop_all_music() {
    }

    public void run() {
        while (this.thread != null) {
            if (this.state == 1) {
                this.g.lock();
                this.Ad_flg = 2;
                if ((this.logo_flg == S_LODING && this.change_logo != S_DUM) || (this.logo_flg == 10 && this.change_logo == S_DUM)) {
                    if (this.change_logo == 0) {
                        this.change_logo = 1;
                    } else if (this.change_logo == 1) {
                        this.change_logo = 2;
                    } else if (this.change_logo == 2) {
                        this.change_logo = 3;
                    } else if (this.change_logo == 3) {
                        this.change_logo = S_LODING;
                    } else if (this.change_logo == S_LODING) {
                        this.change_logo = S_END;
                    } else if (this.change_logo == S_END) {
                        this.change_logo = S_DUM;
                    } else if (this.change_logo == S_DUM) {
                        this.state = S_TITLE;
                    }
                    this.logo_flg = S_TITLE;
                }
                if (this.change_logo == 0) {
                    this.g.drawBigBitmap(logo1_img, S_TITLE, S_TITLE);
                } else if (this.change_logo == 1) {
                    this.g.drawBigBitmap(logo2_img, S_TITLE, S_TITLE);
                } else if (this.change_logo == 2) {
                    this.g.drawBigBitmap(logo3_img, S_TITLE, S_TITLE);
                } else if (this.change_logo == 3) {
                    this.g.drawBigBitmap(logo5_img, S_TITLE, S_TITLE);
                } else if (this.change_logo == S_LODING) {
                    this.g.drawBigBitmap(logo4_img, S_TITLE, S_TITLE);
                } else if (this.change_logo == S_END || this.change_logo == S_DUM) {
                    this.g.drawBigBitmap(logo6_img, S_TITLE, S_TITLE);
                }
                this.logo_flg++;
                this.g.unlock();
            }
            if (this.state == 0) {
                this.g.lock();
                this.g.drawBigBitmap(title_img, S_TITLE, S_TITLE);
                this.g.unlock();
            }
            if (this.state == S_FIRST) {
                this.frist = S_TITLE;
                this.cnt = S_TITLE;
                this.play = new Player(player_x, 50, S_LODING);
                this.win = new ArrayList<>();
                this.red = new ArrayList<>();
                this.blue = new ArrayList<>();
                this.green = new ArrayList<>();
                this.yellow = new ArrayList<>();
                this.black = new ArrayList<>();
                this.bana = new ArrayList<>();
                this.point = S_TITLE;
                this.speed = -5;
                this.maxBomb = 3;
                this.cnt = 10;
                this.life = S_END;
                this.state = S_DUM;
            }
            if (this.state == S_DUM) {
                if (this.cnt == 40) {
                    createWindow();
                    this.cnt = S_TITLE;
                    this.frist++;
                }
                if (this.frist == S_LODING) {
                    this.state = 2;
                    this.Ad_flg = 1;
                }
                this.g.lock();
                this.g.drawBigBitmap(load_img, S_TITLE, S_TITLE);
                this.g.unlock();
                moveWindow();
                this.cnt++;
            }
            if (this.state == 3) {
                this.g.lock();
                this.g.drawBigBitmap(over_img, S_TITLE, S_TITLE);
                this.g.drawString("Rank1:  " + String.valueOf(this.rank1), win_pos2_X, rank_start_y);
                this.g.drawString("Rank2:  " + String.valueOf(this.rank2), win_pos2_X, 265);
                this.g.drawString("Rank3:  " + String.valueOf(this.rank3), win_pos2_X, 300);
                this.g.drawString("Rank4:  " + String.valueOf(this.rank4), win_pos2_X, 335);
                this.g.drawString("Rank5:  " + String.valueOf(this.rank5), win_pos2_X, 370);
                this.gameover_flg--;
                this.g.unlock();
            }
            if (this.state == 2) {
                this.g.lock();
                this.g.drawBigBitmap(backGd, S_TITLE, S_TITLE);
                if (this.cnt >= 40) {
                    createWindow();
                }
                moveWindow();
                drawWindow();
                if (this.cnt == 20 || this.cnt == 40) {
                    createBaloone();
                    this.point += this.speed * -1;
                }
                movePlay();
                moveBaloone();
                drawPlay();
                drawBaloone();
                if (this.cnt >= 40) {
                    this.cnt = S_TITLE;
                }
                this.g.setColor(-16776961);
                this.g.setFontSize(26);
                this.g.drawString("Life:" + String.valueOf(this.play.getLife()), 680, 27);
                this.g.drawString("point" + String.valueOf(this.point), 680, 55);
                this.cnt++;
                this.g.unlock();
            }
            try {
                Thread.sleep(60);
                this.timer++;
                if (this.timer > 600) {
                    this.timer = S_TITLE;
                }
            } catch (Exception e) {
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int touchX = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            if (this.state == 2) {
                if (touchX <= this.maxWidth / 2) {
                    this.moveFlg = 1;
                    this.moveCnt = S_TITLE;
                    this.play.setState(1);
                } else {
                    this.moveFlg = 2;
                    this.moveCnt = S_TITLE;
                    this.play.setState(S_TITLE);
                }
            }
            if (this.state == 0 || (this.state == 3 && this.gameover_flg <= 0)) {
                this.Ad_flg = 1;
                this.gameover_flg = 20;
                this.state = S_FIRST;
            }
        }
        return true;
    }

    public void movePlay() {
        if (this.moveFlg == 0) {
            this.play.setY();
        } else if (this.moveFlg == 1) {
            this.play.setX(-10);
            this.play.setY();
            if (this.moveCnt == S_END) {
                this.play.goY(-2);
            } else if (this.moveCnt == S_LODING) {
                this.play.goY(-3);
            } else {
                this.play.goY(-12);
            }
            this.moveCnt++;
        } else {
            this.play.setX(10);
            this.play.setY();
            if (this.moveCnt == S_END) {
                this.play.goY(-2);
            } else if (this.moveCnt == S_LODING) {
                this.play.goY(-3);
            } else {
                this.play.goY(-12);
            }
            this.moveCnt++;
        }
        if (this.moveCnt == S_END) {
            this.moveCnt = S_TITLE;
            this.moveFlg = S_TITLE;
        }
        this.play.goDamFlg();
        if (this.play.getX() <= 0) {
            this.play.setX(this.play.getX() * -1);
        } else if (this.play.getX() >= this.maxWidth - player_width) {
            this.play.setX((this.play.getX() - (this.maxWidth - player_width)) * -1);
        }
        if (this.play.getY() <= 20) {
            this.play.goY(this.play.getY());
        } else if (this.play.getY() > this.maxWidth - 100) {
            this.play.setDamFlg(1);
            this.play.setLife(-1);
            if (this.play.getState() == 0) {
                this.play.setState(2);
            } else {
                this.play.setState(3);
            }
            this.play.goY(-30);
            if (this.play.getLife() <= 0) {
                endGame();
            }
        }
    }

    public void drawPlay() {
        if (this.play.getState() == 0) {
            this.g.drawBitmap(monkey_right1_img, this.play.getX(), this.play.getY());
        } else if (this.play.getState() == 1) {
            this.g.drawBitmap(monkey_left1_img, this.play.getX(), this.play.getY());
        } else if (this.play.getState() == 2) {
            this.g.drawBitmap(monkey_right2_img, this.play.getX(), this.play.getY());
        } else if (this.play.getState() == 3) {
            this.g.drawBitmap(monkey_left2_img, this.play.getX(), this.play.getY());
        }
    }

    public void createWindow() {
        windowcre(150, 600, this.speed);
        windowcre(win_pos2_X, 600, this.speed);
        windowcre(550, 600, this.speed);
    }

    public void moveWindow() {
        for (int i = S_TITLE; i < this.win.size(); i++) {
            this.win.get(i).setY();
            if (this.win.get(i).getY() < -150) {
                this.win.remove(i);
            }
        }
    }

    public void drawWindow() {
        for (int i = S_TITLE; i < this.win.size(); i++) {
            this.g.drawBitmap(window_img, this.win.get(i).getX(), this.win.get(i).getY());
        }
    }

    public void createBaloone() {
        Random random = new Random();
        int posX = random.nextInt(this.maxWidth);
        if (this.point < 100) {
            redcre(posX, 600, this.speed - 2);
            this.maxBomb = 1;
        } else if (this.point >= 100 && this.point < 300) {
            this.maxBomb = 3;
            for (int i = S_TITLE; i < this.maxBomb; i++) {
                int color = random.nextInt(3);
                if (color == 1) {
                    redcre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color == 2) {
                    int posX2 = random.nextInt(this.maxWidth);
                    if (posX2 < 300) {
                        bluecre(posX2, 600, this.speed - 3, S_TITLE);
                    } else {
                        bluecre(posX2, 600, this.speed - 3, 1);
                    }
                } else if (random.nextInt(S_LODING) == 1) {
                    banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, S_TITLE);
                }
            }
        } else if (this.point >= 300 && this.point < 600) {
            this.maxBomb = S_LODING;
            for (int i2 = S_TITLE; i2 < this.maxBomb; i2++) {
                int color2 = random.nextInt(S_END);
                if (color2 == 1) {
                    redcre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color2 == 2) {
                    int posX3 = random.nextInt(this.maxWidth);
                    if (posX3 < 300) {
                        bluecre(posX3, 600, this.speed - 3, S_TITLE);
                    } else {
                        bluecre(posX3, 600, this.speed - 3, 1);
                    }
                } else if (color2 == 3) {
                    greencre(random.nextInt(this.maxWidth), 600, this.speed);
                } else if (color2 == S_LODING) {
                    if (random.nextInt(S_END) == 1) {
                        banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, 1);
                    }
                } else if (random.nextInt(S_LODING) == 1) {
                    banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, S_TITLE);
                }
            }
        } else if (this.point >= 600 && this.point < 1000) {
            this.maxBomb = S_DUM;
            for (int i3 = S_TITLE; i3 < this.maxBomb; i3++) {
                int color3 = random.nextInt(S_DUM);
                if (color3 == 1) {
                    redcre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color3 == 2) {
                    int posX4 = random.nextInt(this.maxWidth);
                    if (posX4 < 300) {
                        bluecre(posX4, 600, this.speed - 3, S_TITLE);
                    } else {
                        bluecre(posX4, 600, this.speed - 3, 1);
                    }
                } else if (color3 == 3) {
                    greencre(random.nextInt(this.maxWidth), 600, this.speed);
                } else if (color3 == S_LODING) {
                    blackcre(random.nextInt(this.maxWidth), -100, this.speed * -1);
                } else if (color3 == S_END) {
                    if (random.nextInt(S_END) == 1) {
                        banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, 1);
                    }
                } else if (random.nextInt(S_LODING) == 1) {
                    banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, S_TITLE);
                }
            }
        } else if (this.point < 1000 || this.point >= 2000) {
            this.maxBomb = 10;
            for (int i4 = S_TITLE; i4 < this.maxBomb; i4++) {
                int color4 = random.nextInt(S_FIRST);
                if (color4 == 1) {
                    redcre(random.nextInt(this.maxWidth), 600, this.speed);
                } else if (color4 == 2) {
                    int posX5 = random.nextInt(this.maxWidth);
                    if (posX5 < 300) {
                        bluecre(posX5, 600, this.speed - 3, S_TITLE);
                    } else {
                        bluecre(posX5, 600, this.speed - 3, 1);
                    }
                } else if (color4 == 3) {
                    greencre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color4 == S_LODING) {
                    blackcre(random.nextInt(this.maxWidth), -100, (this.speed * -1) + 2);
                } else if (color4 == S_END) {
                    int change = random.nextInt(2);
                    int posY = random.nextInt(this.maxWidth);
                    if (change == 1) {
                        yellowcre(-100, posY, this.speed * -1);
                    } else {
                        yellowcre(860, posY, this.speed);
                    }
                } else if (color4 == S_DUM) {
                    if (random.nextInt(S_LODING) == 1) {
                        banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, 1);
                    }
                } else if (random.nextInt(S_LODING) == 1) {
                    banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, S_TITLE);
                }
            }
        } else {
            this.maxBomb = 8;
            for (int i5 = S_TITLE; i5 < this.maxBomb; i5++) {
                int color5 = random.nextInt(S_FIRST);
                if (color5 == 1) {
                    redcre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color5 == 2) {
                    int posX6 = random.nextInt(this.maxWidth);
                    if (posX6 < 300) {
                        bluecre(posX6, 600, this.speed - 3, S_TITLE);
                    } else {
                        bluecre(posX6, 600, this.speed - 3, 1);
                    }
                } else if (color5 == 3) {
                    greencre(random.nextInt(this.maxWidth), 600, this.speed - 2);
                } else if (color5 == S_LODING) {
                    blackcre(random.nextInt(this.maxWidth), -100, (this.speed * -1) + 2);
                } else if (color5 == S_END) {
                    int change2 = random.nextInt(2);
                    int posY2 = random.nextInt(this.maxWidth);
                    if (change2 == 1) {
                        yellowcre(-100, posY2, this.speed * -1);
                    } else {
                        yellowcre(860, posY2, this.speed);
                    }
                } else if (color5 == S_DUM) {
                    if (random.nextInt(S_LODING) == 1) {
                        banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, 1);
                    }
                } else if (random.nextInt(S_LODING) == 1) {
                    banacre(random.nextInt(this.maxWidth), 600, this.speed - 2, S_TITLE);
                }
            }
        }
    }

    public void moveBaloone() {
        int flg = S_TITLE;
        for (int i = S_TITLE; i < this.red.size(); i++) {
            this.red.get(i).setY();
            if (((this.play.getX() + S_END > this.red.get(i).getX() && this.play.getX() + S_END < this.red.get(i).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.red.get(i).getX() && (this.play.getX() + player_width) - S_END < this.red.get(i).getX() + 50) || (this.play.getX() + 40 > this.red.get(i).getX() && this.play.getX() + 40 < this.red.get(i).getX() + 50))) && (((this.play.getY() > this.red.get(i).getY() && this.play.getY() < this.red.get(i).getY() + 50) || ((this.play.getY() + 100 > this.red.get(i).getY() && this.play.getY() + 100 < this.red.get(i).getY() + 50) || (this.play.getY() + 50 > this.red.get(i).getY() && this.play.getY() + 50 < this.red.get(i).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setDamFlg(1);
                this.play.setLife(-1);
                if (this.play.getState() == 0) {
                    this.play.setState(2);
                } else {
                    this.play.setState(3);
                }
                if (this.play.getLife() <= 0) {
                    endGame();
                }
                flg = 1;
            }
            if (this.red.get(i).getY() < -150 || flg == 1) {
                this.red.remove(i);
                flg = S_TITLE;
            }
        }
        for (int i2 = S_TITLE; i2 < this.blue.size(); i2++) {
            this.blue.get(i2).setY();
            this.blue.get(i2).goX();
            if (((this.play.getX() + S_END > this.blue.get(i2).getX() && this.play.getX() + S_END < this.blue.get(i2).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.blue.get(i2).getX() && (this.play.getX() + player_width) - S_END < this.blue.get(i2).getX() + 50) || (this.play.getX() + 40 > this.blue.get(i2).getX() && this.play.getX() + 40 < this.blue.get(i2).getX() + 50))) && (((this.play.getY() > this.blue.get(i2).getY() && this.play.getY() < this.blue.get(i2).getY() + 50) || ((this.play.getY() + 100 > this.blue.get(i2).getY() && this.play.getY() + 100 < this.blue.get(i2).getY() + 50) || (this.play.getY() + 50 > this.blue.get(i2).getY() && this.play.getY() + 50 < this.blue.get(i2).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setDamFlg(1);
                this.play.setLife(-1);
                if (this.play.getState() == 0) {
                    this.play.setState(2);
                } else {
                    this.play.setState(3);
                }
                if (this.play.getLife() <= 0) {
                    endGame();
                }
                flg = 1;
            }
            if (this.blue.get(i2).getY() < -150 || flg == 1) {
                this.blue.remove(i2);
                flg = S_TITLE;
            }
        }
        for (int i3 = S_TITLE; i3 < this.green.size(); i3++) {
            this.green.get(i3).setY();
            if (this.green.get(i3).getX() > this.play.getX()) {
                this.green.get(i3).setX(S_TITLE);
            } else {
                this.green.get(i3).setX(1);
            }
            if (((this.play.getX() + S_END > this.green.get(i3).getX() && this.play.getX() + S_END < this.green.get(i3).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.green.get(i3).getX() && (this.play.getX() + player_width) - S_END < this.green.get(i3).getX() + 50) || (this.play.getX() + 40 > this.green.get(i3).getX() && this.play.getX() + 40 < this.green.get(i3).getX() + 50))) && (((this.play.getY() > this.green.get(i3).getY() && this.play.getY() < this.green.get(i3).getY() + 50) || ((this.play.getY() + 100 > this.green.get(i3).getY() && this.play.getY() + 100 < this.green.get(i3).getY() + 50) || (this.play.getY() + 50 > this.green.get(i3).getY() && this.play.getY() + 50 < this.green.get(i3).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setDamFlg(1);
                this.play.setLife(-1);
                if (this.play.getState() == 0) {
                    this.play.setState(2);
                } else {
                    this.play.setState(3);
                }
                if (this.play.getLife() <= 0) {
                    endGame();
                }
                flg = 1;
            }
            if (this.green.get(i3).getY() < -150 || flg == 1) {
                this.green.remove(i3);
                flg = S_TITLE;
            }
        }
        for (int i4 = S_TITLE; i4 < this.black.size(); i4++) {
            this.black.get(i4).setY();
            if (((this.play.getX() + S_END > this.black.get(i4).getX() && this.play.getX() + S_END < this.black.get(i4).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.black.get(i4).getX() && (this.play.getX() + player_width) - S_END < this.black.get(i4).getX() + 50) || (this.play.getX() + 40 > this.black.get(i4).getX() && this.play.getX() + 40 < this.black.get(i4).getX() + 50))) && (((this.play.getY() > this.black.get(i4).getY() && this.play.getY() < this.black.get(i4).getY() + 50) || ((this.play.getY() + 100 > this.black.get(i4).getY() && this.play.getY() + 100 < this.black.get(i4).getY() + 50) || (this.play.getY() + 50 > this.black.get(i4).getY() && this.play.getY() + 50 < this.black.get(i4).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setDamFlg(1);
                this.play.setLife(-1);
                if (this.play.getState() == 0) {
                    this.play.setState(2);
                } else {
                    this.play.setState(3);
                }
                if (this.play.getLife() <= 0) {
                    endGame();
                }
                flg = 1;
            }
            if (this.black.get(i4).getY() > 650 || flg == 1) {
                this.black.remove(i4);
                flg = S_TITLE;
            }
        }
        for (int i5 = S_TITLE; i5 < this.yellow.size(); i5++) {
            this.yellow.get(i5).setX();
            this.yellow.get(i5).setY();
            if (((this.play.getX() + S_END > this.yellow.get(i5).getX() && this.play.getX() + S_END < this.yellow.get(i5).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.yellow.get(i5).getX() && (this.play.getX() + player_width) - S_END < this.yellow.get(i5).getX() + 50) || (this.play.getX() + 40 > this.yellow.get(i5).getX() && this.play.getX() + 40 < this.yellow.get(i5).getX() + 50))) && (((this.play.getY() > this.yellow.get(i5).getY() && this.play.getY() < this.yellow.get(i5).getY() + 50) || ((this.play.getY() + 100 > this.yellow.get(i5).getY() && this.play.getY() + 100 < this.yellow.get(i5).getY() + 50) || (this.play.getY() + 50 > this.yellow.get(i5).getY() && this.play.getY() + 50 < this.yellow.get(i5).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setDamFlg(1);
                this.play.setLife(-1);
                if (this.play.getState() == 0) {
                    this.play.setState(2);
                } else {
                    this.play.setState(3);
                }
                if (this.play.getLife() <= 0) {
                    endGame();
                }
                flg = 1;
            }
            if (this.yellow.get(i5).getX() > 900 || this.yellow.get(i5).getX() < -100 || flg == 1) {
                this.yellow.remove(i5);
                flg = S_TITLE;
            }
        }
        for (int i6 = S_TITLE; i6 < this.bana.size(); i6++) {
            this.bana.get(i6).setY();
            if (((this.play.getX() + S_END > this.bana.get(i6).getX() && this.play.getX() + S_END < this.bana.get(i6).getX() + 50) || (((this.play.getX() + player_width) - S_END > this.bana.get(i6).getX() && (this.play.getX() + player_width) - S_END < this.bana.get(i6).getX() + 50) || (this.play.getX() + 40 > this.bana.get(i6).getX() && this.play.getX() + 40 < this.bana.get(i6).getX() + 50))) && (((this.play.getY() > this.bana.get(i6).getY() && this.play.getY() < this.bana.get(i6).getY() + 50) || ((this.play.getY() + 100 > this.bana.get(i6).getY() && this.play.getY() + 100 < this.bana.get(i6).getY() + 50) || (this.play.getY() + 50 > this.bana.get(i6).getY() && this.play.getY() + 50 < this.bana.get(i6).getY() + 50))) && this.play.getDamFlg() == 0)) {
                this.play.setLife(this.bana.get(i6).setLife());
                flg = 1;
                if (this.bana.get(i6).getCk() == 1) {
                    this.red = new ArrayList<>();
                    this.blue = new ArrayList<>();
                    this.green = new ArrayList<>();
                    this.yellow = new ArrayList<>();
                    this.black = new ArrayList<>();
                }
            }
            if (this.bana.get(i6).getY() < -150 || flg == 1) {
                this.bana.remove(i6);
                flg = S_TITLE;
            }
        }
    }

    public void drawBaloone() {
        for (int i = S_TITLE; i < this.red.size(); i++) {
            this.g.drawBitmap(red_img, this.red.get(i).getX(), this.red.get(i).getY());
        }
        for (int i2 = S_TITLE; i2 < this.blue.size(); i2++) {
            this.g.drawBitmap(blue_img, this.blue.get(i2).getX(), this.blue.get(i2).getY());
        }
        for (int i3 = S_TITLE; i3 < this.green.size(); i3++) {
            this.g.drawBitmap(green_img, this.green.get(i3).getX(), this.green.get(i3).getY());
        }
        for (int i4 = S_TITLE; i4 < this.black.size(); i4++) {
            this.g.drawBitmap(black_img, this.black.get(i4).getX(), this.black.get(i4).getY());
        }
        for (int i5 = S_TITLE; i5 < this.yellow.size(); i5++) {
            this.g.drawBitmap(yellow_img, this.yellow.get(i5).getX(), this.yellow.get(i5).getY());
        }
        for (int i6 = S_TITLE; i6 < this.bana.size(); i6++) {
            if (this.bana.get(i6).getCk() == 0) {
                this.g.drawBitmap(bana_img, this.bana.get(i6).getX(), this.bana.get(i6).getY());
            } else {
                this.g.drawBitmap(bana2_img, this.bana.get(i6).getX(), this.bana.get(i6).getY());
            }
        }
    }

    public void windowcre(int px, int py, int speed2) {
        this.win.add(new Window(px, py, speed2));
    }

    public void redcre(int px, int py, int speed2) {
        this.red.add(new BalooneRed(px, py, speed2));
    }

    public void bluecre(int px, int py, int speed2, int ck) {
        this.blue.add(new BalooneBlue(px, py, speed2, ck));
    }

    public void greencre(int px, int py, int speed2) {
        this.green.add(new BalooneGreen(px, py, speed2));
    }

    public void blackcre(int px, int py, int speed2) {
        this.black.add(new BalooneBlack(px, py, speed2));
    }

    public void yellowcre(int px, int py, int speed2) {
        this.yellow.add(new BalooneYellow(px, py, speed2));
    }

    public void banacre(int px, int py, int speed2, int ck) {
        this.bana.add(new Banana(px, py, speed2, ck));
    }

    public void endGame() {
        this.Ad_flg = 2;
        this.state = 3;
        loadFile();
        saveFile();
    }

    public void saveFile() {
        if (this.rank5 < this.point) {
            if (this.rank4 >= this.point) {
                this.rank5 = this.point;
            } else if (this.rank3 >= this.point) {
                this.rank5 = this.rank4;
                this.rank4 = this.point;
            } else if (this.rank2 >= this.point) {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.point;
            } else if (this.rank1 < this.point) {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.rank2;
                this.rank2 = this.rank1;
                this.rank1 = this.point;
            } else {
                this.rank5 = this.rank4;
                this.rank4 = this.rank3;
                this.rank3 = this.rank2;
                this.rank2 = this.point;
            }
        }
        SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(this.myCon).edit();
        e.putInt("rank1", this.rank1);
        e.putInt("rank2", this.rank2);
        e.putInt("rank3", this.rank3);
        e.putInt("rank4", this.rank4);
        e.putInt("rank5", this.rank5);
        e.commit();
    }

    public void loadFile() {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.myCon);
        this.rank1 = pref.getInt("rank1", S_TITLE);
        this.rank2 = pref.getInt("rank2", S_TITLE);
        this.rank3 = pref.getInt("rank3", S_TITLE);
        this.rank4 = pref.getInt("rank4", S_TITLE);
        this.rank5 = pref.getInt("rank5", S_TITLE);
    }
}
