package net.monkey;

public class Banana {
    public int ck = 1;
    public int damCnt = 0;
    public int damFlg = 0;
    public int lefe = 3;
    public int life = 6;
    public int speed = -6;
    public int state = 0;
    public int x;
    public int y;

    public Banana(int x2, int y2, int speed2, int ck2) {
        this.x = x2;
        this.y = y2;
        this.speed = speed2;
        this.life = 3;
        this.ck = ck2;
    }

    public int setLife() {
        if (this.ck == 0) {
            this.life = 1;
        } else {
            this.life = 3;
        }
        return this.life;
    }

    public int getCk() {
        return this.ck;
    }

    public int getX() {
        return this.x;
    }

    public void setY() {
        this.y += this.speed;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int i) {
        if (i == 0) {
            this.x--;
        } else {
            this.x++;
        }
    }

    public void setSpeed(int s) {
        this.speed = s;
    }
}
