package net.monkey;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int baloon_black = 2130837505;
        public static final int baloon_blue = 2130837506;
        public static final int baloon_green = 2130837507;
        public static final int baloon_red = 2130837508;
        public static final int baloon_yellow = 2130837509;
        public static final int bana = 2130837510;
        public static final int bana2 = 2130837511;
        public static final int banana = 2130837512;
        public static final int bg = 2130837513;
        public static final int bg2 = 2130837514;
        public static final int gameover = 2130837515;
        public static final int gameover2 = 2130837516;
        public static final int icon = 2130837517;
        public static final int loader = 2130837518;
        public static final int logo1 = 2130837519;
        public static final int logo2 = 2130837520;
        public static final int logo3 = 2130837521;
        public static final int logo4 = 2130837522;
        public static final int logo5 = 2130837523;
        public static final int logo6 = 2130837524;
        public static final int monkey_left_1 = 2130837525;
        public static final int monkey_left_2 = 2130837526;
        public static final int monkey_right_1 = 2130837527;
        public static final int monkey_right_2 = 2130837528;
        public static final int title = 2130837529;
        public static final int window = 2130837530;
    }

    public static final class id {
        public static final int adView = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
