package com.dis.helper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import com.dis.tools.ExitApp;

public class main extends TabActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(5);
        TabHost host = getTabHost();
        host.addTab(host.newTabSpec(getString(R.string.tabsimple)).setIndicator("", getResources().getDrawable(R.drawable.simpleinfo)).setContent(new Intent(this, SimpleInfo.class).addFlags(67108864)));
        host.addTab(host.newTabSpec(getString(R.string.tabsystem)).setIndicator("", getResources().getDrawable(R.drawable.systeminfo)).setContent(new Intent(this, SystemInfo.class)));
        ExitApp.getInstance().addActivity(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                showDialog(1);
                return true;
            case R.id.quit:
                quit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        quit();
        return true;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                View view = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.about, (ViewGroup) findViewById(R.id.abt));
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.setTitle(getString(R.string.Tabout));
                builder.setView(view);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    private void quit() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title)).setMessage(getString(R.string.msg)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ExitApp.getInstance().exit();
            }
        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }
}
