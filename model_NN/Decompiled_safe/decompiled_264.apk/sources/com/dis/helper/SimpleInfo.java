package com.dis.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.dis.tools.ExitApp;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.IntBuffer;

public class SimpleInfo extends Activity {
    private LinearLayout layout;
    private GLSurfaceView mGLSurfaceView;
    private String mGlExtensions = null;
    private int mGlMaxTextureSize;
    private int mGlMaxTextureUnits;
    private String mGlRenderer = null;
    private String mGlVendor = null;
    private String mGlVersion = null;
    private Handler mHandler;
    private IntBuffer mIntBuffer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sysinfo);
        this.layout = (LinearLayout) findViewById(R.id.linearLayout1);
        ((LinearLayout) findViewById(R.id.ads)).addView(new AdMogoLayout(this, "141d7aa480694effa198c875a0ffd08f"));
        addTextTitle("基本信息");
        addText("型号：" + getPhoneModel());
        addText("品牌：" + getBrand());
        String[] s = getCpuInfo();
        addText("CPU型号：" + s[0]);
        addText("CPU频率：" + s[1] + "MHz");
        addText("屏幕分辨率：" + getDisplayinfo()[0]);
        addText("总内存大小：" + getTotalMemory());
        addText("手机存储大小：" + getTotalInternalMemorySize() + "MB");
        addText("SD卡总大小：" + getSDCardMemory()[0] + "MB");
        getMark();
        ExitApp.getInstance().addActivity(this);
    }

    private void getMark() {
        String[] s = getCpuInfo();
        int markc = 0;
        int markr = 0;
        int ram = Integer.valueOf(getTotalMemory().replace("MB", "")).intValue();
        if (Float.valueOf(s[1]).floatValue() >= 1300.0f) {
            markc = 5;
        } else if (Float.valueOf(s[1]).floatValue() >= 900.0f && Float.valueOf(s[1]).floatValue() < 1300.0f) {
            markc = 4;
        } else if (Float.valueOf(s[1]).floatValue() >= 700.0f && Float.valueOf(s[1]).floatValue() < 900.0f) {
            markc = 3;
        } else if (Float.valueOf(s[1]).floatValue() >= 500.0f && Float.valueOf(s[1]).floatValue() < 700.0f) {
            markc = 2;
        } else if (Float.valueOf(s[1]).floatValue() < 500.0f) {
            markc = 1;
        }
        if (ram >= 800) {
            markr = 5;
        } else if (ram >= 600 && ram < 800) {
            markr = 4;
        } else if (ram >= 400 && ram < 600) {
            markr = 3;
        } else if (ram < 400) {
            markr = 2;
        }
        if (markr + markc >= 9) {
            addResultText("您的机器硬件超一流");
        } else if (markr + markc >= 8 && markr + markc < 9) {
            addResultText("您的机器硬件高端");
        } else if (markr + markc < 6 || markr + markc >= 8) {
            addResultText("您的机器硬件普通");
        } else {
            addResultText("您的机器硬件中端");
        }
    }

    private void addTextTitle(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(22.0f);
        textView.setBackgroundColor(-12303292);
        this.layout.addView(textView);
    }

    private void addText(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(16.0f);
        this.layout.addView(textView);
    }

    private void addResultText(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(25.0f);
        this.layout.addView(textView);
    }

    private void addSmallText(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(14.0f);
        this.layout.addView(textView);
    }

    public long[] getSDCardMemory() {
        long[] sdCardInfo = new long[2];
        if ("mounted".equals(Environment.getExternalStorageState())) {
            StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bSize = (long) sf.getBlockSize();
            sdCardInfo[0] = ((bSize * ((long) sf.getBlockCount())) / 1024) / 1024;
            sdCardInfo[1] = ((bSize * ((long) sf.getAvailableBlocks())) / 1024) / 1024;
        }
        return sdCardInfo;
    }

    public long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((((long) stat.getBlockCount()) * ((long) stat.getBlockSize())) / 1024) / 1024;
    }

    private String getTotalMemory() {
        long initial_memory = 0;
        try {
            BufferedReader localBufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            String str2 = localBufferedReader.readLine();
            String[] arrayOfString = str2.split("\\s+");
            int length = arrayOfString.length;
            for (int i = 0; i < length; i++) {
                Log.i(str2, String.valueOf(arrayOfString[i]) + "\t");
            }
            initial_memory = (long) (Integer.valueOf(arrayOfString[1]).intValue() * 1024);
            localBufferedReader.close();
        } catch (IOException e) {
        }
        return Formatter.formatFileSize(getBaseContext(), initial_memory);
    }

    public String[] getCpuInfo() {
        String[] cpuInfo = {"", "", "", "", "", ""};
        try {
            BufferedReader localBufferedReader = new BufferedReader(new FileReader("/proc/cpuinfo"), 8192);
            String[] arrayOfString = localBufferedReader.readLine().split("\\s+");
            for (int i = 2; i < arrayOfString.length; i++) {
                cpuInfo[0] = String.valueOf(cpuInfo[0]) + arrayOfString[i] + " ";
            }
            cpuInfo[1] = String.valueOf(cpuInfo[1]) + localBufferedReader.readLine().split("\\s+")[2];
            String str2 = localBufferedReader.readLine();
            String str22 = localBufferedReader.readLine();
            cpuInfo[2] = localBufferedReader.readLine().split("\\s+")[2];
            String str23 = localBufferedReader.readLine();
            String str24 = localBufferedReader.readLine();
            cpuInfo[3] = localBufferedReader.readLine().split("\\s+")[3];
            String str25 = localBufferedReader.readLine();
            cpuInfo[4] = localBufferedReader.readLine().split("\\s+")[2];
            String str26 = localBufferedReader.readLine();
            cpuInfo[5] = localBufferedReader.readLine().split("\\s+")[2];
            localBufferedReader.close();
        } catch (IOException e) {
        }
        return cpuInfo;
    }

    private static String initProperty(String description, String propertyStr) {
        StringBuffer buffer = new StringBuffer();
        if (buffer == null) {
            buffer = new StringBuffer();
        }
        buffer.append(description).append(":");
        buffer.append(System.getProperty(propertyStr)).append("");
        return buffer.toString();
    }

    public String getPhoneModel() {
        return Build.MODEL;
    }

    public String getBrand() {
        return Build.BRAND;
    }

    private String[] getDisplayinfo() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int h = 0;
        if (((float) dm.heightPixels) * dm.density > 850.0f && ((float) dm.heightPixels) * dm.density < 860.0f) {
            h = 854;
        }
        String[] s = {"", "", "", "", ""};
        s[0] = String.valueOf((int) (((float) dm.widthPixels) * dm.density)) + "*" + h;
        s[1] = String.valueOf(dm.density);
        s[2] = String.valueOf(dm.scaledDensity);
        s[3] = String.valueOf(dm.xdpi);
        s[4] = String.valueOf(dm.ydpi);
        return s;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        quit();
        return true;
    }

    private void quit() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title)).setMessage(getString(R.string.msg)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ExitApp.getInstance().exit();
            }
        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }
}
