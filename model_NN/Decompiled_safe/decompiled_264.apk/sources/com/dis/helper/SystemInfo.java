package com.dis.helper;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.dis.tools.ExitApp;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.IntBuffer;
import java.util.Enumeration;
import java.util.List;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class SystemInfo extends Activity implements GLSurfaceView.Renderer, Runnable {
    private LinearLayout layout;
    private GLSurfaceView mGLSurfaceView;
    private String mGlExtensions = null;
    private int mGlMaxTextureSize;
    private int mGlMaxTextureUnits;
    private String mGlRenderer = null;
    private String mGlVendor = null;
    private String mGlVersion = null;
    private Handler mHandler = new Handler();
    private IntBuffer mIntBuffer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mGLSurfaceView = new GLSurfaceView(this);
        this.mGLSurfaceView.setRenderer(this);
        this.mIntBuffer = IntBuffer.allocate(1);
        setContentView(this.mGLSurfaceView);
        showDialog(1);
        ExitApp.getInstance().addActivity(this);
    }

    private void addTextTitle(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(22.0f);
        textView.setBackgroundColor(-12303292);
        this.layout.addView(textView);
    }

    private void addText(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(16.0f);
        this.layout.addView(textView);
    }

    private void addSmallText(String adds) {
        TextView textView = new TextView(this);
        textView.setText(adds);
        textView.setTextSize(14.0f);
        this.layout.addView(textView);
    }

    public long[] getSDCardMemory() {
        long[] sdCardInfo = new long[2];
        if ("mounted".equals(Environment.getExternalStorageState())) {
            StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bSize = (long) sf.getBlockSize();
            sdCardInfo[0] = ((bSize * ((long) sf.getBlockCount())) / 1024) / 1024;
            sdCardInfo[1] = ((bSize * ((long) sf.getAvailableBlocks())) / 1024) / 1024;
        }
        return sdCardInfo;
    }

    public long[] getRomMemroy() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return new long[]{getTotalInternalMemorySize(), ((((long) stat.getBlockSize()) * ((long) stat.getAvailableBlocks())) / 1024) / 1024};
    }

    public long getTotalInternalMemorySize() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return ((((long) stat.getBlockCount()) * ((long) stat.getBlockSize())) / 1024) / 1024;
    }

    private String getTotalMemory() {
        long initial_memory = 0;
        try {
            BufferedReader localBufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
            String str2 = localBufferedReader.readLine();
            String[] arrayOfString = str2.split("\\s+");
            int length = arrayOfString.length;
            for (int i = 0; i < length; i++) {
                Log.i(str2, String.valueOf(arrayOfString[i]) + "\t");
            }
            initial_memory = (long) (Integer.valueOf(arrayOfString[1]).intValue() * 1024);
            localBufferedReader.close();
        } catch (IOException e) {
        }
        return Formatter.formatFileSize(getBaseContext(), initial_memory);
    }

    private String getAvailMemory() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ((ActivityManager) getSystemService("activity")).getMemoryInfo(mi);
        return Formatter.formatFileSize(getBaseContext(), mi.availMem);
    }

    public String[] getCpuInfo() {
        String[] cpuInfo = {"", "", "", "", "", ""};
        try {
            BufferedReader localBufferedReader = new BufferedReader(new FileReader("/proc/cpuinfo"), 8192);
            String[] arrayOfString = localBufferedReader.readLine().split("\\s+");
            for (int i = 2; i < arrayOfString.length; i++) {
                cpuInfo[0] = String.valueOf(cpuInfo[0]) + arrayOfString[i] + " ";
            }
            cpuInfo[1] = String.valueOf(cpuInfo[1]) + localBufferedReader.readLine().split("\\s+")[2];
            String str2 = localBufferedReader.readLine();
            String str22 = localBufferedReader.readLine();
            cpuInfo[2] = localBufferedReader.readLine().split("\\s+")[2];
            String str23 = localBufferedReader.readLine();
            String str24 = localBufferedReader.readLine();
            cpuInfo[3] = localBufferedReader.readLine().split("\\s+")[3];
            String str25 = localBufferedReader.readLine();
            cpuInfo[4] = localBufferedReader.readLine().split("\\s+")[2];
            String str26 = localBufferedReader.readLine();
            cpuInfo[5] = localBufferedReader.readLine().split("\\s+")[2];
            localBufferedReader.close();
        } catch (IOException e) {
        }
        return cpuInfo;
    }

    private String getCpuUse() {
        String Result = "";
        new StringBuffer();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("top -n 1").getInputStream()));
            do {
                Result = br.readLine();
                if (Result == null) {
                    break;
                }
            } while (Result.trim().length() < 1);
        } catch (IOException e) {
        }
        return Result;
    }

    private static String initProperty(String description, String propertyStr) {
        StringBuffer buffer = new StringBuffer();
        if (buffer == null) {
            buffer = new StringBuffer();
        }
        buffer.append(description).append(":");
        buffer.append(System.getProperty(propertyStr)).append("");
        return buffer.toString();
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getPhoneModel() {
        return Build.MODEL;
    }

    public String getBoard() {
        return Build.BOARD;
    }

    public String getBrand() {
        return Build.BRAND;
    }

    public String getFingerPrint() {
        return Build.FINGERPRINT;
    }

    public String getID() {
        return Build.ID;
    }

    public String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public String getProduct() {
        return Build.PRODUCT;
    }

    public String getUser() {
        return Build.USER;
    }

    public String getMyNum() {
        return ((TelephonyManager) getSystemService("phone")).getLine1Number();
    }

    public void onDrawFrame(GL10 gl) {
        try {
            Thread.sleep(1000, 0);
        } catch (Exception e) {
        }
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        int i = Log.i("GLRenderer", "onSurfaceChanged");
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        this.mGlRenderer = gl.glGetString(7937);
        this.mGlVersion = gl.glGetString(7938);
        this.mGlVendor = gl.glGetString(7936);
        this.mGlExtensions = String.valueOf(String.valueOf(gl.glGetString(7939))) + "\n";
        gl.glGetIntegerv(3379, this.mIntBuffer);
        this.mGlMaxTextureSize = this.mIntBuffer.get(0);
        gl.glGetIntegerv(34018, this.mIntBuffer);
        this.mGlMaxTextureUnits = this.mIntBuffer.get(0);
        new StringBuilder("Surface Created: ");
        int i = Log.i("GLRenderer", this.mGlRenderer);
        boolean post = this.mHandler.post(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setProgressStyle(0);
                progressDialog.setMessage("Loading...");
                return progressDialog;
            default:
                return null;
        }
    }

    public void run() {
        setContentView((int) R.layout.sysinfo);
        this.layout = (LinearLayout) findViewById(R.id.linearLayout1);
        ((LinearLayout) findViewById(R.id.ads)).addView(new AdMogoLayout(this, "141d7aa480694effa198c875a0ffd08f"));
        addTextTitle("系统信息");
        addText("Android版本 ：" + getOSVersion());
        addText("操作系统：" + initProperty("", "os.name"));
        addText("系统版本：" + initProperty("", "os.version"));
        addText("ID：" + getID() + "\n");
        addTextTitle("产品信息");
        addText("型号：" + getPhoneModel());
        addText("产品名称：" + getProduct());
        addText("品牌：" + getBrand());
        addText("主板：" + getBoard());
        addText("制造商：" + getManufacturer());
        addText("用户：" + getUser());
        addText("指纹信息：" + getFingerPrint() + "\n");
        addTextTitle("CPU信息");
        String[] s = getCpuInfo();
        addText("CPU型号：" + s[0]);
        addText("CPU频率：" + s[1] + "MHz");
        addText("CPU使用率：" + getCpuUse());
        addText("CPU架构：" + s[2]);
        addText("修正版本：" + s[3]);
        addText("硬件：" + s[4]);
        addText("序列号：" + s[5] + "\n");
        addTextTitle("屏幕信息");
        String[] s2 = getDisplayinfo();
        addText("屏幕分辨率：" + s2[0]);
        addText("逻辑密度：" + s2[1]);
        addText("缩放率：" + s2[2]);
        addText("XDPI：" + s2[3]);
        addText("YDPI：" + s2[4] + "\n");
        addTextTitle("网络信息");
        addText("IP地址：" + getLocalIpAddress());
        addText("MAC地址：" + getLocalMacAddress() + "\n");
        addTextTitle("SIM信息");
        addText("本机号码：" + getMyNum());
        addText("运营商：" + getServCompny());
        addText("IMEI：" + getIMEI() + "\n");
        addTextTitle("存储信息");
        addText("总内存大小：" + getTotalMemory());
        addText("可用内存大小：" + getAvailMemory());
        addText("手机存储大小：" + getTotalInternalMemorySize() + "MB");
        addText("可用存储大小：" + getRomMemroy()[1] + "MB");
        addText("SD卡总大小：" + getSDCardMemory()[0] + "MB");
        addText("SD卡剩余大小：" + getSDCardMemory()[1] + "MB\n");
        addTextTitle("GPU信息");
        addText("渲染器:" + this.mGlRenderer);
        addText("版本:" + this.mGlVersion);
        addText("供应商:" + this.mGlVendor);
        addText("最大纹理单位:" + this.mGlMaxTextureUnits);
        addText("最大纹理尺寸:" + this.mGlMaxTextureSize);
        addText("扩展:");
        addSmallText(String.valueOf(this.mGlExtensions) + "\n");
        addTextTitle("传感器信息");
        List list = getSensorInfo();
        for (int i = 0; i < list.size(); i++) {
            Sensor sensor = (Sensor) list.get(i);
            addText(sensor.getName());
            addSmallText("供应商：" + sensor.getVendor());
            addSmallText("最大范围：" + sensor.getMaximumRange());
            addSmallText("解析度：" + sensor.getResolution());
            addSmallText("耗能：" + sensor.getPower() + "mA");
        }
        addSmallText("");
        addTextTitle("相机参数");
        addText(getCameraInfo());
        dismissDialog(1);
    }

    private String getIMEI() {
        return ((TelephonyManager) getSystemService("phone")).getSimSerialNumber();
    }

    private String getServCompny() {
        String operator = ((TelephonyManager) getSystemService("phone")).getSimOperator();
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002") || operator.equals("46007")) {
                return "中国移动";
            }
            if (operator.equals("46001")) {
                return "中国联通";
            }
            if (operator.equals("46003")) {
                return "中国电信";
            }
        }
        return null;
    }

    public String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("WifiPreference IpAddress", e.toString());
        }
        return "";
    }

    public String getLocalMacAddress() {
        return ((WifiManager) getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    private String[] getDisplayinfo() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int h = 0;
        if (((float) dm.heightPixels) * dm.density > 850.0f && ((float) dm.heightPixels) * dm.density < 860.0f) {
            h = 854;
        }
        String[] s = {"", "", "", "", ""};
        s[0] = String.valueOf((int) (((float) dm.widthPixels) * dm.density)) + "*" + h;
        s[1] = String.valueOf(dm.density);
        s[2] = String.valueOf(dm.scaledDensity);
        s[3] = String.valueOf(dm.xdpi);
        s[4] = String.valueOf(dm.ydpi);
        return s;
    }

    private String getCameraInfo() {
        return Camera.open().getParameters().flatten().replace(";", "\n").replace("=", "：").replace("-", " ");
    }

    private List getSensorInfo() {
        return ((SensorManager) getSystemService("sensor")).getSensorList(-1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mGLSurfaceView.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mGLSurfaceView.onResume();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        quit();
        return true;
    }

    private void quit() {
        new AlertDialog.Builder(this).setTitle(getString(R.string.title)).setMessage(getString(R.string.msg)).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ExitApp.getInstance().exit();
            }
        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }
}
