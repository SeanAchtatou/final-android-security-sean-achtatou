package com.dis.helper;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int simpleinfo = 2130837505;
        public static final int systeminfo = 2130837506;
    }

    public static final class id {
        public static final int about = 2131099669;
        public static final int abt = 2131099648;
        public static final int ads = 2131099666;
        public static final int button1 = 2131099665;
        public static final int linearLayout1 = 2131099668;
        public static final int main = 2131099664;
        public static final int quit = 2131099670;
        public static final int scrollView1 = 2131099667;
        public static final int tableRow1 = 2131099649;
        public static final int tableRow2 = 2131099652;
        public static final int tableRow3 = 2131099655;
        public static final int tableRow5 = 2131099658;
        public static final int tableRow6 = 2131099661;
        public static final int textView1 = 2131099651;
        public static final int textView10 = 2131099656;
        public static final int textView12 = 2131099659;
        public static final int textView13 = 2131099662;
        public static final int textView2 = 2131099654;
        public static final int textView3 = 2131099657;
        public static final int textView5 = 2131099660;
        public static final int textView8 = 2131099650;
        public static final int textView9 = 2131099653;
        public static final int webs = 2131099663;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int main = 2130903041;
        public static final int sysinfo = 2130903042;
    }

    public static final class menu {
        public static final int mainmenu = 2131034112;
    }

    public static final class string {
        public static final int Tabout = 2130968580;
        public static final int Tquit = 2130968581;
        public static final int app_name = 2130968576;
        public static final int close = 2130968586;
        public static final int findinfo = 2130968577;
        public static final int msg = 2130968583;
        public static final int no = 2130968585;
        public static final int ok = 2130968584;
        public static final int platf = 2130968589;
        public static final int qq = 2130968590;
        public static final int tabsimple = 2130968578;
        public static final int tabsystem = 2130968579;
        public static final int title = 2130968582;
        public static final int utime = 2130968588;
        public static final int ver = 2130968587;
        public static final int webs = 2130968591;
    }
}
