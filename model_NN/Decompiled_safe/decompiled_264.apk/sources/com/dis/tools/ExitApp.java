package com.dis.tools;

import android.app.Activity;
import android.app.Application;
import java.util.LinkedList;
import java.util.List;

public class ExitApp extends Application {
    private static ExitApp instance;
    private List<Activity> activityList = new LinkedList();

    private ExitApp() {
    }

    public static ExitApp getInstance() {
        if (instance == null) {
            instance = new ExitApp();
        }
        return instance;
    }

    public void addActivity(Activity activity) {
        this.activityList.add(activity);
    }

    public void exit() {
        for (Activity activity : this.activityList) {
            activity.finish();
        }
        System.exit(0);
    }
}
