package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.greystripe.android.sdk.BannerListener;
import com.greystripe.android.sdk.BannerView;
import com.greystripe.android.sdk.GSSDK;

public class GreystripeAdapter extends AdMogoAdapter implements BannerListener {
    private Activity activity;
    private BannerView adView;
    private GSSDK sdk;

    public GreystripeAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            adMogoLayout.removeAllViews();
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    this.sdk = GSSDK.initialize(this.activity, this.ration.key);
                    if (adMogoLayout.getAdType() == 1) {
                        this.adView = new BannerView(this.activity);
                        this.adView.addListener(this);
                        this.adView.refresh();
                    } else if (adMogoLayout.getAdType() != 6) {
                    } else {
                        if (this.sdk.displayAd(this.activity)) {
                            Log.d(AdMogoUtil.ADMOGO, "Greystripe Full Screen Success");
                            adMogoLayout.CountImpAd();
                            return;
                        }
                        Log.d(AdMogoUtil.ADMOGO, "Greystripe Full Screen failure");
                        adMogoLayout.rollover();
                    }
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(BannerView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Greystripe failure");
        adView2.addListener((BannerListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceivedAd(BannerView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Greystripe success");
        adView2.addListener((BannerListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 7));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Greystripe Finished");
    }
}
