package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import java.util.Timer;

public class AdView extends RelativeLayout {
    public static final int AD_MEASURE_176 = 176;
    public static final int AD_MEASURE_240 = 240;
    public static final int AD_MEASURE_320 = 320;
    public static final int AD_MEASURE_360 = 360;
    public static final int AD_MEASURE_480 = 480;
    public static final int AD_MEASURE_640 = 640;
    public static final int BANNER_ANIMATION_TYPE_CURLDOWN = 6;
    public static final int BANNER_ANIMATION_TYPE_CURLUP = 5;
    public static final int BANNER_ANIMATION_TYPE_FADEINOUT = 2;
    public static final int BANNER_ANIMATION_TYPE_FLIPFROMLEFT = 3;
    public static final int BANNER_ANIMATION_TYPE_FLIPFROMRIGHT = 4;
    public static final int BANNER_ANIMATION_TYPE_NONE = 0;
    public static final int BANNER_ANIMATION_TYPE_RANDOM = 1;
    public static final int BANNER_ANIMATION_TYPE_SLIDEFROMLEFT = 7;
    public static final int BANNER_ANIMATION_TYPE_SLIDEFROMRIGHT = 8;
    public static final int EVENT_EXISTAD = 3;
    public static final int EVENT_FINISHAD = 2;
    public static final int EVENT_INCOMPLETE_PERMISSION = 5;
    public static final int EVENT_INVALIDAD = 4;
    public static final int EVENT_NEWAD = 1;
    public static final int RETRUNCODE_INVALIDADPOSITION = 401;
    public static final int RETRUNCODE_INVALIDIP = 404;
    public static final int RETRUNCODE_INVALIDUSER = 402;
    public static final int RETRUNCODE_INVALIDUSERAGENT = 403;
    public static final int RETRUNCODE_NOADS = 400;
    public static final int RETRUNCODE_OK = 200;
    public static final int RETRUNCODE_SERVERBUSY = 500;
    public static final int RETRUNCODE_SERVERERROR = 501;
    public static final int RETRUNCODE_TRYAGAIN = 405;
    /* access modifiers changed from: private */
    public static final float[][] _ = {new float[]{0.0f, 90.0f, -90.0f, 0.0f}, new float[]{0.0f, -90.0f, 90.0f, 0.0f}};
    private static final float[][] __ = {new float[]{0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f}, new float[]{0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f}, new float[]{0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f}, new float[]{0.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f}};
    /* access modifiers changed from: private */
    public static Handler bbbb;
    /* access modifiers changed from: private */
    public String $;
    /* access modifiers changed from: private */
    public int $$;
    /* access modifiers changed from: private */
    public int $$$;
    /* access modifiers changed from: private */
    public int $$$$;
    private boolean $$$$$;
    /* access modifiers changed from: private */
    public String ___;
    /* access modifiers changed from: private */
    public String ____;
    /* access modifiers changed from: private */
    public String _____;
    /* access modifiers changed from: private */
    public boolean a;
    /* access modifiers changed from: private */
    public boolean aa;
    /* access modifiers changed from: private */
    public AdView aaa;
    /* access modifiers changed from: private */
    public ____ aaaa;
    private eeee aaaaa;
    /* access modifiers changed from: private */
    public AdListener b;
    /* access modifiers changed from: private */
    public Context bb;
    private Timer bbb;
    private boolean bbbbb;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.bbbbb = false;
        _(context, attributeSet, null, 0, 0, false);
    }

    public AdView(Context context, AttributeSet attributeSet, int i, String str, int i2, int i3, boolean z) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.bbbbb = false;
        _(context, attributeSet, str, i2, i3, z);
    }

    public AdView(Context context, AttributeSet attributeSet, int i, String str, int i2, boolean z) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.bbbbb = false;
        _(context, attributeSet, str, i2, 0, z);
    }

    private void _(Context context) {
        Window window = ((Activity) context).getWindow();
        int i = window.getAttributes().flags;
        window.setFlags(1024, 1024);
        if (i == window.getAttributes().flags) {
            this.bbbbb = true;
        }
    }

    private void _(Context context, AttributeSet attributeSet, String str, int i, int i2, boolean z) {
        this.aaa = this;
        this.$$$$$ = false;
        setFocusable(true);
        setClickable(true);
        setDescendantFocusability(262144);
        this.bb = context;
        eee._ = z;
        if (attributeSet != null) {
            String str2 = I.I(1876) + context.getPackageName();
            eee._ = attributeSet.getAttributeBooleanValue(str2, I.I(1600), false);
            String attributeValue = attributeSet.getAttributeValue(str2, I.I(1912));
            if (attributeValue != null) {
                this._____ = attributeValue;
                int attributeIntValue = attributeSet.getAttributeIntValue(str2, I.I(1951), 60);
                if (attributeIntValue == 0) {
                    eee.__(I.I(1962));
                    _(context);
                }
                _(attributeIntValue);
                __(attributeSet.getAttributeIntValue(str2, I.I(2014), 0));
                setAnimationType(attributeSet.getAttributeIntValue(str2, I.I(2024), 1));
            } else {
                eee.____(I.I(1923));
                return;
            }
        }
        if (this._____ == null) {
            if (str != null) {
                this._____ = str;
            } else {
                eee.____(I.I(1923));
                return;
            }
        }
        if (this.$$ == -1) {
            if (i == 0) {
                eee.__(I.I(1962));
                _(context);
            }
            _(i);
        }
        if (this.$$$ == -1) {
            __(i2);
        }
        byte[] _2 = n._(this.bb, I.I(1244));
        if (_2 != null) {
            String str3 = new String(_2);
            int indexOf = str3.indexOf(I.I(1242));
            if (indexOf == 10) {
                this.____ = str3.substring(0, indexOf);
            } else {
                this.____ = null;
            }
        } else {
            this.____ = null;
        }
        if (this.___ == null) {
            String _3 = f._(this.bb, I.I(2298));
            if (_3 != null) {
                this.___ = _3;
            } else {
                String _4 = AdManager._();
                if (_4 != null) {
                    this.___ = _4;
                } else {
                    eee.____(I.I(2305));
                }
            }
        }
        try {
            context.enforceCallingOrSelfPermission(I.I(2042), I.I(2078));
            context.enforceCallingOrSelfPermission(I.I(2130), I.I(2170));
            context.enforceCallingOrSelfPermission(I.I(2226), I.I(2254));
            this.aa = true;
        } catch (SecurityException e) {
            eee.____(e.toString());
            this.aa = false;
        }
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof l)) {
            Thread.setDefaultUncaughtExceptionHandler(new l(this.bb));
        }
        if (super.getVisibility() == 0) {
            _();
        }
    }

    static final /* synthetic */ void _(AdView adView, Context context) {
        if (!adView.bbbbb) {
            ((Activity) context).getWindow().clearFlags(1024);
        }
    }

    static final /* synthetic */ void _(AdView adView, ____ ____2, int i, int i2) {
        ____2.setVisibility(8);
        int i3 = adView.$$$$ - 3;
        float f = _[i3][0];
        float f2 = _[i3][1];
        ___ ___2 = new ___(f, f2, ((float) i) / 2.0f, ((float) i2) / 2.0f, -0.4f * ((float) i), true, 1);
        ___2.setDuration(700);
        ___2.setFillAfter(true);
        ___2.setInterpolator(new AccelerateInterpolator());
        ___2.setAnimationListener(new gggg(adView, ____2, i, i2));
        adView.startAnimation(___2);
    }

    static final /* synthetic */ void _(AdView adView, eeee eeee) {
        ___ ___2 = new ___(0.0f, -1080.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, 10000.0f, true, 3);
        ___2.setDuration(700);
        ___2.setFillAfter(true);
        ___2.setInterpolator(new AccelerateInterpolator());
        ___2.setAnimationListener(new i(adView));
        adView.startAnimation(___2);
    }

    private void _(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.$$ > 0) {
                    if (this.bbb == null) {
                        this.bbb = new Timer();
                        this.bbb.schedule(new ff(this), (long) this.$$, (long) this.$$);
                    }
                }
            }
            if ((!z || this.$$ == 0) && this.bbb != null) {
                this.bbb.cancel();
                this.bbb = null;
            }
        }
    }

    private void __(int i) {
        switch (i) {
            case AD_MEASURE_176 /*176*/:
                this.$$$ = AD_MEASURE_176;
                return;
            case AD_MEASURE_240 /*240*/:
                this.$$$ = AD_MEASURE_240;
                return;
            case AD_MEASURE_320 /*320*/:
                this.$$$ = AD_MEASURE_320;
                return;
            case AD_MEASURE_360 /*360*/:
                this.$$$ = AD_MEASURE_360;
                return;
            case AD_MEASURE_480 /*480*/:
                this.$$$ = AD_MEASURE_480;
                return;
            case AD_MEASURE_640 /*640*/:
                this.$$$ = AD_MEASURE_640;
                return;
            default:
                this.$$$ = 0;
                return;
        }
    }

    static final /* synthetic */ void __(AdView adView, ____ ____2) {
        adView.aaaa = ____2;
        if (adView.$$$$$) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(350);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            adView.startAnimation(alphaAnimation);
        }
    }

    static final /* synthetic */ void __(AdView adView, eeee eeee) {
        float f;
        float f2;
        adView.aaaaa = eeee;
        int width = adView.getWidth();
        int height = adView.getHeight();
        if (width <= 0 || height <= 0) {
            f = ((float) eeee._) / 2.0f;
            f2 = ((float) eeee.__) / 2.0f;
        } else {
            f = ((float) width) / 2.0f;
            f2 = ((float) height) / 2.0f;
        }
        ___ ___2 = new ___(0.0f, -1080.0f, f, f2, 10000.0f, true, 2);
        ___2.setDuration(700);
        ___2.setFillAfter(true);
        ___2.setInterpolator(new DecelerateInterpolator());
        ___2.setAnimationListener(new h(adView));
        adView.startAnimation(___2);
    }

    static final /* synthetic */ void ___(AdView adView, ____ ____2) {
        ____2.setVisibility(8);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        alphaAnimation.setAnimationListener(new gg(adView, ____2));
        adView.aaaa.startAnimation(alphaAnimation);
    }

    static final /* synthetic */ void ____(AdView adView, ____ ____2) {
        int i = adView.$$$$ - 5;
        if (adView.aaaa != null) {
            adView.aaaa.setClickable(false);
            TranslateAnimation translateAnimation = new TranslateAnimation(1, __[i][0], 1, __[i][1], 1, __[i][2], 1, __[i][3]);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new AccelerateInterpolator());
            translateAnimation.setDuration(700);
            adView.aaaa.startAnimation(translateAnimation);
        }
        if (____2 != null) {
            ____2.setClickable(false);
            TranslateAnimation translateAnimation2 = new TranslateAnimation(1, __[i][4], 1, __[i][5], 1, __[i][6], 1, __[i][7]);
            translateAnimation2.setFillAfter(true);
            translateAnimation2.setInterpolator(new AccelerateInterpolator());
            translateAnimation2.setDuration(700);
            translateAnimation2.setAnimationListener(new ggggg(adView, ____2));
            ____2.startAnimation(translateAnimation2);
        }
    }

    /* access modifiers changed from: protected */
    public final void _() {
        if (super.getVisibility() != 0) {
            eee.___(I.I(1803));
        } else if (this.a) {
            eee._(I.I(1831));
        } else {
            this.a = true;
            if (bbbb == null) {
                bbbb = new Handler();
            }
            new fff(this).start();
        }
    }

    /* access modifiers changed from: protected */
    public final void _(int i) {
        int i2 = 20;
        if (i <= 0) {
            i2 = 0;
        } else if (i >= 20) {
            i2 = i > 600 ? 600 : i;
        }
        this.$$ = i2 * 1000;
        if (this.$$ == 0) {
            _(false);
        } else {
            _(true);
        }
    }

    public final int getVisibility() {
        return super.getVisibility();
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        this.$$$$$ = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.$$$$$ = false;
        _(false);
        if (this.aaaa != null) {
            this.aaaa._();
        }
        if (this.aaaaa != null) {
            this.aaaaa._();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (!eee._) {
            if (this.aaaa != null && z) {
                if (i >= 0 && i2 >= 0) {
                    int width = getWidth();
                    int height = getHeight();
                    if (width > 0 && height > 0) {
                        int i5 = this.aaaa._;
                        int i6 = this.aaaa.__;
                        if (width >= i5 && height >= i6) {
                            return;
                        }
                    }
                }
                setVisibility(8);
                _(false);
            }
        } else if (this.aaaa != null && z) {
            if (i < 0 || i2 < 0) {
                eee.___(I.I(216));
                return;
            }
            int width2 = getWidth();
            int height2 = getHeight();
            if (width2 <= 0 || height2 <= 0) {
                eee.___(I.I(162));
                return;
            }
            int i7 = this.aaaa._;
            int i8 = this.aaaa.__;
            if (width2 < i7 || height2 < i8) {
                eee.___(I.I(109));
            }
        }
    }

    public final void onWindowFocusChanged(boolean z) {
        _(z);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        switch (i) {
            case BANNER_ANIMATION_TYPE_NONE /*0*/:
                _(true);
                return;
            case 4:
            case 8:
                _(false);
                return;
            default:
                return;
        }
    }

    public final void setAnimationType(int i) {
        if (i < 0 || i > 8) {
            this.$$$$ = 4;
        } else {
            this.$$$$ = i;
        }
    }

    public final void setListener(AdListener adListener) {
        synchronized (this) {
            this.b = adListener;
        }
    }

    public final void setVisibility(int i) {
        if (super.getVisibility() != i) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(i);
                }
                super.setVisibility(i);
                if (i == 0) {
                    _();
                } else {
                    if (this.aaaa != null) {
                        this.aaaa._();
                        removeView(this.aaaa);
                        this.aaaa = null;
                    }
                    if (this.aaaaa != null) {
                        this.aaaaa._();
                        removeView(this.aaaaa);
                        this.aaaaa = null;
                    }
                    invalidate();
                }
            }
        }
    }
}
