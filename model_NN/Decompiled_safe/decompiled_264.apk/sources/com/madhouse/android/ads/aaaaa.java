package com.madhouse.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

final class aaaaa extends ImageView {
    private Bitmap _;
    private boolean __;
    private int ___ = 128;
    private View.OnClickListener ____;

    protected aaaaa(C$$$$$ $$$$$, Context context, int i, int i2, float f, int i3, boolean z) {
        super(context);
        this._ = f._(context, i, i2, f);
        _(z);
        __();
        setImageBitmap(this._);
    }

    private void _() {
        if (isPressed()) {
            this.____.onClick(this);
        }
    }

    private void __() {
        if (!this.__) {
            setBackgroundColor(-16777216);
        } else if (isPressed()) {
            setBackgroundColor(-256);
        } else if (hasFocus()) {
            setBackgroundColor(-16776961);
        } else {
            setBackgroundColor(-16777216);
        }
    }

    /* access modifiers changed from: protected */
    public final void _(boolean z) {
        this.__ = z;
        if (this.__) {
            setClickable(true);
            setFocusable(true);
            setAlpha(255);
            return;
        }
        setClickable(false);
        setFocusable(false);
        setAlpha(this.___);
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.__) {
            switch (motionEvent.getAction()) {
                case AdView.BANNER_ANIMATION_TYPE_NONE:
                    setPressed(true);
                    requestFocus();
                    __();
                    break;
                case 1:
                    _();
                    setPressed(false);
                    __();
                    break;
                case 2:
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    int left = getLeft();
                    int top = getTop();
                    int right = getRight();
                    int bottom = getBottom();
                    if (x < 0.0f || x > ((float) (right - left)) || y < 0.0f || y > ((float) (bottom - top))) {
                        setPressed(false);
                        __();
                        break;
                    }
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (this.__) {
            if (motionEvent.getAction() == 0) {
                setPressed(true);
                __();
            } else if (motionEvent.getAction() == 1) {
                _();
                setPressed(false);
                __();
            }
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i, Rect rect) {
        __();
        super.onFocusChanged(z, i, rect);
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.__ && (i == 66 || i == 23)) {
            setPressed(true);
            __();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.__ && (i == 66 || i == 23)) {
            _();
            setPressed(false);
            __();
        }
        return super.onKeyUp(i, keyEvent);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.____ = onClickListener;
    }
}
