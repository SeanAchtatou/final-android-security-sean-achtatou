package com.madhouse.android.ads;

import android.content.Context;
import android.widget.LinearLayout;

/* renamed from: com.madhouse.android.ads.$$$$$  reason: invalid class name */
final class C$$$$$ extends LinearLayout {
    aaaaa _;
    aaaaa __;
    final /* synthetic */ _____ ___;
    private aaaaa ____;
    private aaaaa _____;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected C$$$$$(_____ _____2, Context context, int i, int i2) {
        super(context);
        this.___ = _____2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i, i2);
        setOrientation(0);
        setBackgroundColor(-16777216);
        setLayoutParams(layoutParams);
        this._ = new aaaaa(this, context, 17301540, 180, 1.0f, 128, false);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams2.gravity = 16;
        addView(this._, layoutParams2);
        this._.setOnClickListener(new a(this));
        this.__ = new aaaaa(this, context, 17301540, 0, 1.0f, 128, false);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams3.gravity = 16;
        addView(this.__, layoutParams3);
        this.__.setOnClickListener(new aa(this));
        this.____ = new aaaaa(this, context, 17301593, 0, 0.75f, 128, true);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams4.gravity = 16;
        addView(this.____, layoutParams4);
        this.____.setOnClickListener(new aaa(this));
        this._____ = new aaaaa(this, context, 17301533, 0, 1.1f, 128, true);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams5.gravity = 16;
        addView(this._____, layoutParams5);
        this._____.setOnClickListener(new aaaa(this));
    }

    /* access modifiers changed from: package-private */
    public final void _() {
        if (this.___.___.canGoBack()) {
            this._._(true);
        } else {
            this._._(false);
        }
        if (this.___.___.canGoForward()) {
            this.__._(true);
        } else {
            this.__._(false);
        }
    }
}
