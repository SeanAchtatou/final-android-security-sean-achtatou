package com.madhouse.android.ads;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

final class _____ extends LinearLayout {
    private int $;
    private int $$;
    boolean _ = true;
    b __;
    bbbb ___;
    C$$$$$ ____;
    dd _____;

    protected _____(Context context) {
        super(context);
        new DisplayMetrics();
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        if (displayMetrics.heightPixels > displayMetrics.widthPixels) {
            this.$ = (int) (((double) displayMetrics.heightPixels) * 0.0475d);
            this.$$ = (int) (((double) displayMetrics.heightPixels) * 0.08125d);
        } else {
            this.$ = (int) (((double) displayMetrics.widthPixels) * 0.0475d);
            this.$$ = (int) (((double) displayMetrics.widthPixels) * 0.08125d);
        }
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.__ = new b(this, context, this.$);
        addView(this.__, new LinearLayout.LayoutParams(-1, this.$, 1.0f));
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.___ = new bbbb(this, context);
        relativeLayout.addView(this.___, new RelativeLayout.LayoutParams(-1, -1));
        this.___.setDownloadListener(new C$(this, context));
        this._____ = new dd(this, context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        dd ddVar = this._____;
        ddVar._.setOnClickListener(new C$$(this));
        dd ddVar2 = this._____;
        ddVar2.__.setOnClickListener(new C$$$(this));
        this._____.setVisibility(4);
        relativeLayout.addView(this._____, layoutParams);
        addView(relativeLayout, new LinearLayout.LayoutParams(-1, -1, 100.0f));
        this.____ = new C$$$$$(this, context, displayMetrics.widthPixels, this.$$);
        addView(this.____, new LinearLayout.LayoutParams(-1, this.$$, 1.0f));
    }

    /* access modifiers changed from: protected */
    public final void _() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        translateAnimation.setDuration(700);
        translateAnimation.setAnimationListener(new C$$$$(this));
        startAnimation(translateAnimation);
    }

    /* access modifiers changed from: protected */
    public final void _(String str) {
        f._(true);
        this._ = false;
        this.___.loadUrl(str);
        this.___.requestFocus();
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(700);
        startAnimation(translateAnimation);
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (this.___.canGoBack()) {
            this.___.goBack();
        } else if (!this._) {
            _();
            this._ = true;
        }
        return true;
    }
}
