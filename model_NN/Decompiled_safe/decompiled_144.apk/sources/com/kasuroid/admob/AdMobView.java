package com.kasuroid.admob;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.kasuroid.core.Debug;

public class AdMobView extends AdView {
    private static final String TAG = AdMobView.class.getSimpleName();

    public AdMobView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdMobView(Activity arg0, AdSize arg1, String arg2) {
        super(arg0, arg1, arg2);
    }

    public AdMobView(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (isShown()) {
            return super.onTouchEvent(event);
        }
        Debug.inf("CoreAdView", "touchEvent, but ad not visible, passing event to core!");
        return false;
    }
}
