package com.kasuroid.admob;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.kasuroid.core.AdViewCtrl;

public class AdMobViewCtrl extends AdViewCtrl {
    private static AdView mAd;

    public void setAd(AdMobView ad) {
        mAd = ad;
    }

    public void showAd() {
        if (mAd != null) {
            mAd.setVisibility(0);
        }
    }

    public void hideAd() {
        if (mAd != null) {
            mAd.setVisibility(4);
        }
    }

    public void enableTestAds() {
    }

    public void load() {
        if (mAd != null) {
            mAd.loadAd(new AdRequest());
        }
    }

    public int getAdHeight() {
        if (mAd != null) {
            return mAd.getHeight();
        }
        return 0;
    }
}
