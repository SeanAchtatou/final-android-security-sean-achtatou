package com.kasuroid.eastereggs;

import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;

public class BoardSkin {
    private static final String TAG = BoardSkin.class.getSimpleName();
    private static BoardSkin mInstance = null;
    private float mFieldIconFactor = (((float) this.mTex1.getHeight()) / ((float) this.mTex1.getWidth()));
    private Texture mTex0 = TextureManager.load(R.drawable.skin_field_0);
    private Texture mTex1 = TextureManager.load(R.drawable.skin_field_1);
    private Texture mTex10 = TextureManager.load(R.drawable.skin_field_1_0);
    private Texture mTex11 = TextureManager.load(R.drawable.skin_field_1_1);
    private Texture mTex2 = TextureManager.load(R.drawable.skin_field_2);
    private Texture mTex20 = TextureManager.load(R.drawable.skin_field_2_0);
    private Texture mTex21 = TextureManager.load(R.drawable.skin_field_2_1);
    private Texture mTex3 = TextureManager.load(R.drawable.skin_field_3);
    private Texture mTex30 = TextureManager.load(R.drawable.skin_field_3_0);
    private Texture mTex31 = TextureManager.load(R.drawable.skin_field_3_1);
    private Texture mTex4 = TextureManager.load(R.drawable.skin_field_4);
    private Texture mTex40 = TextureManager.load(R.drawable.skin_field_4_0);
    private Texture mTex41 = TextureManager.load(R.drawable.skin_field_4_1);
    private Texture mTex5 = TextureManager.load(R.drawable.skin_field_5);
    private Texture mTex50 = TextureManager.load(R.drawable.skin_field_5_0);
    private Texture mTex51 = TextureManager.load(R.drawable.skin_field_5_1);
    private Texture mTex6 = TextureManager.load(R.drawable.skin_field_6);
    private Texture mTex60 = TextureManager.load(R.drawable.skin_field_6_0);
    private Texture mTex61 = TextureManager.load(R.drawable.skin_field_6_1);
    private Texture mTexChicken = TextureManager.load(R.drawable.skin_chicken);

    public static synchronized BoardSkin getInstance() {
        BoardSkin boardSkin;
        synchronized (BoardSkin.class) {
            if (mInstance == null) {
                mInstance = new BoardSkin();
            }
            boardSkin = mInstance;
        }
        return boardSkin;
    }

    private BoardSkin() {
    }

    public void scale(int width, int height) {
        this.mTex0 = TextureManager.load(R.drawable.skin_field_0).scaleToTexture(width, height);
        this.mTex1 = TextureManager.load(R.drawable.skin_field_1).scaleToTexture(width, height);
        this.mTex2 = TextureManager.load(R.drawable.skin_field_2).scaleToTexture(width, height);
        this.mTex3 = TextureManager.load(R.drawable.skin_field_3).scaleToTexture(width, height);
        this.mTex4 = TextureManager.load(R.drawable.skin_field_4).scaleToTexture(width, height);
        this.mTex5 = TextureManager.load(R.drawable.skin_field_5).scaleToTexture(width, height);
        this.mTex6 = TextureManager.load(R.drawable.skin_field_6).scaleToTexture(width, height);
        this.mTex10 = TextureManager.load(R.drawable.skin_field_1_0).scaleToTexture(width, height);
        this.mTex11 = TextureManager.load(R.drawable.skin_field_1_1).scaleToTexture(width, height);
        this.mTex20 = TextureManager.load(R.drawable.skin_field_2_0).scaleToTexture(width, height);
        this.mTex21 = TextureManager.load(R.drawable.skin_field_2_1).scaleToTexture(width, height);
        this.mTex30 = TextureManager.load(R.drawable.skin_field_3_0).scaleToTexture(width, height);
        this.mTex31 = TextureManager.load(R.drawable.skin_field_3_1).scaleToTexture(width, height);
        this.mTex40 = TextureManager.load(R.drawable.skin_field_4_0).scaleToTexture(width, height);
        this.mTex41 = TextureManager.load(R.drawable.skin_field_4_1).scaleToTexture(width, height);
        this.mTex50 = TextureManager.load(R.drawable.skin_field_5_0).scaleToTexture(width, height);
        this.mTex51 = TextureManager.load(R.drawable.skin_field_5_1).scaleToTexture(width, height);
        this.mTex60 = TextureManager.load(R.drawable.skin_field_6_0).scaleToTexture(width, height);
        this.mTex61 = TextureManager.load(R.drawable.skin_field_6_1).scaleToTexture(width, height);
        this.mTexChicken = TextureManager.load(R.drawable.skin_chicken).scaleToTexture(this.mTex0.getWidth(), this.mTex0.getWidth());
    }

    /* access modifiers changed from: package-private */
    public Texture getFieldIcon(int type) {
        switch (type) {
            case 0:
                return this.mTex0;
            case 1:
                return this.mTex1;
            case 2:
                return this.mTex2;
            case 3:
                return this.mTex3;
            case 4:
                return this.mTex4;
            case 5:
                return this.mTex5;
            case 6:
                return this.mTex6;
            default:
                return this.mTex0;
        }
    }

    /* access modifiers changed from: package-private */
    public Texture getFieldIconSmall(int type) {
        switch (type) {
            case 0:
                return this.mTex11;
            case Field.TYPE_CHICKEN /*11*/:
                return this.mTexChicken;
            case Field.TYPE_1_0 /*100*/:
                return this.mTex10;
            case Field.TYPE_1_1 /*101*/:
                return this.mTex11;
            case Field.TYPE_2_0 /*200*/:
                return this.mTex20;
            case Field.TYPE_2_1 /*201*/:
                return this.mTex21;
            case Field.TYPE_3_0 /*300*/:
                return this.mTex30;
            case Field.TYPE_3_1 /*301*/:
                return this.mTex31;
            case Field.TYPE_4_0 /*400*/:
                return this.mTex40;
            case Field.TYPE_4_1 /*401*/:
                return this.mTex41;
            case Field.TYPE_5_0 /*500*/:
                return this.mTex50;
            case Field.TYPE_5_1 /*501*/:
                return this.mTex51;
            case Field.TYPE_6_0 /*600*/:
                return this.mTex60;
            case Field.TYPE_6_1 /*601*/:
                return this.mTex61;
            default:
                return this.mTex11;
        }
    }

    public int getFieldIconSmallWidth() {
        return this.mTex11.getWidth();
    }

    public int getFieldIconSmallHeight() {
        return this.mTex11.getHeight();
    }

    public int getFieldIconWidth() {
        return this.mTex1.getWidth();
    }

    public int getFieldIconHeight() {
        return this.mTex1.getHeight();
    }

    public float getFieldIconFactor() {
        return this.mFieldIconFactor;
    }
}
