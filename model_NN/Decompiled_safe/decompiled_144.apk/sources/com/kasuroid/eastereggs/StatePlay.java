package com.kasuroid.eastereggs;

import android.view.KeyEvent;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.GameState;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;

public class StatePlay extends GameState {
    private static final String TAG = "StatePlay";
    private GameListener mGameListener;
    /* access modifiers changed from: private */
    public GameManager mGameManager;
    private int mLevelToStart;
    private Texture mTexBkg;

    public StatePlay(int levelToStart) {
        this.mLevelToStart = levelToStart;
    }

    public int onInit() {
        super.onInit();
        Debug.inf(getClass().getName(), "onInit()");
        Core.hideFps();
        Core.showAd();
        this.mGameManager = GameManager.getInstance();
        this.mGameManager.enableOptionsMenu();
        this.mTexBkg = TextureManager.load(GameConfig.getInstance().getBkgId());
        this.mTexBkg.scale(Renderer.getWidth(), Renderer.getHeight());
        initGameListener();
        this.mGameManager.restartGame(this.mLevelToStart);
        this.mGameManager.nextLevel();
        return 0;
    }

    public int onTerm() {
        super.onTerm();
        Debug.inf(getClass().getName(), "onTerm()");
        this.mGameManager.finishGame();
        this.mGameManager.disableOptionsMenu();
        return 0;
    }

    public int onPause() {
        Debug.inf(getClass().getName(), "onPause()");
        super.onPause();
        return 0;
    }

    public int onResume() {
        Debug.inf(getClass().getName(), "onResume()");
        Core.showAd();
        super.onResume();
        return 0;
    }

    public int onUpdate() {
        this.mGameManager.update();
        return 0;
    }

    public int onRender() {
        drawBackground();
        this.mGameManager.render();
        return 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouchEvent(InputEvent event) {
        switch (this.mGameManager.getState()) {
            case 0:
                return this.mGameManager.onTouchEvent(event);
            case 1:
            case 2:
            case 3:
            case 4:
                break;
            default:
                Debug.err(TAG, "Unsupported game state: " + this.mGameManager.getState() + "!");
                break;
        }
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            Debug.inf(TAG, "Going to main menu!");
            if (changeState(new StateMainMenu()) == 0) {
                return true;
            }
            Debug.err(getClass().getName(), "Problem with poping the state!");
            return false;
        }
        Debug.inf(getClass().getName(), "onKeyDown");
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return false;
        }
        Debug.inf(getClass().getName(), "Blocking the MENU/BACK key!");
        return true;
    }

    /* access modifiers changed from: private */
    public void onLevelStarted() {
        Debug.inf(TAG, "onLevelStarted()");
    }

    /* access modifiers changed from: private */
    public void onLevelFinished() {
        Debug.inf(TAG, "onLevelFinished, going to next level!");
        if (!this.mGameManager.areMoreLevels()) {
            Debug.inf(TAG, "No more levels, game finished!");
            onGameFinished();
        } else if (Core.pushState(new StateNextLevel()) != 0) {
            Debug.err(TAG, "Problem with pushing the state!");
        }
    }

    /* access modifiers changed from: private */
    public void onLevelFailed() {
        Debug.inf(TAG, "onLevelFailed, restarting level");
        if (Core.pushState(new StateTryAgain()) != 0) {
            Debug.err(TAG, "Problem with pushing the state!");
        }
    }

    /* access modifiers changed from: private */
    public void onGameFailed() {
        Debug.inf(TAG, "onGameFailed");
    }

    /* access modifiers changed from: private */
    public void onGameFinished() {
        Debug.inf(TAG, "onGameFinished");
        if (pushState(new StateLevelsDone()) != 0) {
            Debug.err(getClass().getName(), "Problem with changing the state!");
        }
    }

    private void initGameListener() {
        this.mGameListener = new GameListener() {
            public void notify(GameManager gamManager, int state) {
                switch (state) {
                    case 0:
                        StatePlay.this.onLevelStarted();
                        return;
                    case 1:
                        StatePlay.this.onLevelFinished();
                        return;
                    case 2:
                        StatePlay.this.onLevelFailed();
                        return;
                    case 3:
                        StatePlay.this.onGameFailed();
                        return;
                    case 4:
                        StatePlay.this.onGameFinished();
                        return;
                    default:
                        Debug.err(StatePlay.TAG, "Unsupported game state: " + StatePlay.this.mGameManager.getState() + "!");
                        return;
                }
            }
        };
        this.mGameManager.setListener(this.mGameListener);
    }

    private void drawBackground() {
        Renderer.setAlpha(255);
        Renderer.drawTexture(this.mTexBkg, 0.0f, 0.0f);
    }
}
