package com.kasuroid.eastereggs;

import com.kasuroid.core.Core;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.scene.SceneNode;
import com.kasuroid.core.scene.Sprite;
import com.kasuroid.core.scene.Vector3d;

public class Particle extends SceneNode {
    private static final int DEF_GRAVITY = 1;
    private static final int DEF_MAX_TOP = 30;
    private static final String TAG = Particle.class.getSimpleName();
    private static BoardSkin mSkin = BoardSkin.getInstance();
    private int mAlpha = 255;
    private float mGravity;
    private Sprite mPic;
    private float mPosY;
    private float mSpeedX;
    private float mSpeedY;
    private int mType;

    public Particle(float x, float y, float speedX, float speedY, int type) {
        super(new Vector3d(x, y, 0.0f));
        this.mSpeedX = speedX;
        this.mSpeedY = speedY;
        this.mPosY = y;
        this.mGravity = 1.0f;
        this.mType = type;
        this.mPic = new Sprite(x, y);
        this.mPic.setTexture(mSkin.getFieldIconSmall(type));
        this.mPic.setAlpha(this.mAlpha);
    }

    public int onUpdate() {
        if (this.mPic.getCoordsX() >= ((float) Renderer.getWidth()) || this.mPic.getCoordsY() >= ((float) Renderer.getHeight())) {
            setFinish(true);
        }
        if (this.mPic.getCoordsY() <= this.mPosY - (30.0f * Core.getScale())) {
            this.mSpeedY = -this.mSpeedY;
        }
        this.mSpeedY += this.mGravity;
        this.mPic.updateCoordsXY(this.mSpeedX, this.mSpeedY);
        return 0;
    }

    public int onRender() {
        this.mPic.render();
        return 0;
    }
}
