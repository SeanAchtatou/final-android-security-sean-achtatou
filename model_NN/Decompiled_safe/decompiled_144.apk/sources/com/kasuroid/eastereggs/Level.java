package com.kasuroid.eastereggs;

public class Level {
    private static int mNextId = 0;
    private int mCols = 5;
    private int mId = mNextId;
    private int mRows = 5;
    private int mTypesCount = 3;

    public Level() {
        mNextId++;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public int getId() {
        return this.mId;
    }

    public void setRows(int rows) {
        this.mRows = rows;
    }

    public int getRows() {
        return this.mRows;
    }

    public void setCols(int cols) {
        this.mCols = cols;
    }

    public int getCols() {
        return this.mCols;
    }

    public void setTypesCount(int count) {
        this.mTypesCount = count;
    }

    public int getTypesCount() {
        return this.mTypesCount;
    }
}
