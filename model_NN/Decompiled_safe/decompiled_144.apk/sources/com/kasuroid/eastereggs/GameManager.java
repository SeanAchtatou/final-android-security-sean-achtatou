package com.kasuroid.eastereggs;

import android.graphics.Paint;
import android.graphics.Typeface;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.scene.Sprite;

public class GameManager {
    private static final int DEF_MAX_LEVELS = 22;
    public static final int STATE_GAME_FAILED = 3;
    public static final int STATE_GAME_FINISHED = 4;
    public static final int STATE_LEVEL_FAILED = 2;
    public static final int STATE_LEVEL_FINISHED = 1;
    public static final int STATE_LEVEL_STARTED = 0;
    private static final String TAG = GameManager.class.getSimpleName();
    private static GameManager mInstance = null;
    private BoardSprite mBoard;
    private Level mCurrentLevel = new Level();
    private int mCurrentLevelId;
    private int mGameState;
    private boolean mIsOptionsMenu;
    private GameListener mListener;
    private int mMovesRecord = -1;
    private Player mPlayer = new Player();
    private float mSpaceX = (Core.getScale() * 4.0f);
    private float mSpaceY = (Core.getScale() * 4.0f);
    private int mStatusLevelSize = ((int) (22.0f * Core.getScale()));
    private int mStatusTitleSize = ((int) (14.0f * Core.getScale()));
    private Sprite mTopBkg = new Sprite(R.drawable.top_bkg, 0.0f, 0.0f);

    public static synchronized GameManager getInstance() {
        GameManager gameManager;
        synchronized (GameManager.class) {
            if (mInstance == null) {
                mInstance = new GameManager();
            }
            gameManager = mInstance;
        }
        return gameManager;
    }

    private GameManager() {
        Debug.inf(getClass().getName(), "GameManager contructor");
    }

    public void restartGame(int levelToStart) {
        GameConfig.getInstance().load();
        this.mCurrentLevelId = levelToStart;
        initBoard();
        initPlayer();
    }

    public void update() {
        if (this.mGameState == 0) {
            this.mBoard.update();
            checkGameStatus();
        }
    }

    public void render() {
        this.mBoard.render();
        drawTopBkg();
        drawTopStatus();
    }

    public void finishGame() {
        this.mListener = null;
    }

    public boolean areMoreLevels() {
        return this.mCurrentLevelId < DEF_MAX_LEVELS;
    }

    public void setListener(GameListener listener) {
        this.mListener = listener;
    }

    public void notifyListener() {
        if (this.mListener != null) {
            this.mListener.notify(this, this.mGameState);
        }
    }

    public boolean isOptionsMenuEnabled() {
        return this.mIsOptionsMenu;
    }

    public void enableOptionsMenu() {
        this.mIsOptionsMenu = true;
    }

    public void disableOptionsMenu() {
        this.mIsOptionsMenu = false;
    }

    public boolean isNextOptionsMenu() {
        if (this.mCurrentLevelId >= GameConfig.getInstance().getMaxLevel() || this.mCurrentLevelId >= DEF_MAX_LEVELS) {
            return false;
        }
        return true;
    }

    public boolean isPrevOptionsMenu() {
        if (this.mCurrentLevelId <= 1) {
            return false;
        }
        return true;
    }

    public boolean onTouchEvent(InputEvent input) {
        boolean ret = false;
        if (this.mGameState == 0 && input.getAction() == 3 && (ret = this.mBoard.touch(input.getX(), input.getY()))) {
            Resources.playSound(6, 0);
            this.mPlayer.updateMoves(1);
            this.mPlayer.setRemainedBlocks(this.mBoard.getRemainedBlocks());
        }
        return ret;
    }

    public void nextLevel() {
        this.mCurrentLevelId++;
        if (this.mCurrentLevelId > DEF_MAX_LEVELS) {
            Debug.inf(TAG, "No more levels!");
            this.mCurrentLevelId--;
            gameFinished();
            return;
        }
        Debug.inf(TAG, "Going to play: " + this.mCurrentLevelId + " level");
        generateLevel(this.mCurrentLevelId);
        initBoard();
        initLevel();
        initPlayer();
    }

    public void previousLevel() {
        if (this.mCurrentLevelId > 1) {
            Debug.inf(TAG, "Going to previous level");
            this.mCurrentLevelId--;
            generateLevel(this.mCurrentLevelId);
            initBoard();
            initLevel();
            initPlayer();
        }
    }

    public Player getPlayer() {
        return this.mPlayer;
    }

    public int getState() {
        return this.mGameState;
    }

    public boolean isTopScore() {
        return true;
    }

    private void initPlayer() {
        this.mPlayer.setCurrentLevel(this.mCurrentLevelId);
        this.mPlayer.setMoves(0);
        this.mPlayer.setRemainedBlocks(this.mBoard.getFieldsCount());
    }

    private void initLevel() {
        Debug.inf(TAG, "initLevel");
        this.mGameState = 0;
        this.mPlayer.setCurrentLevel(this.mCurrentLevelId);
        this.mMovesRecord = GameConfig.getInstance().getMovesRecord(this.mCurrentLevelId);
        GameConfig.getInstance().setLastLevel(this.mCurrentLevelId);
        if (this.mCurrentLevelId > GameConfig.getInstance().getMaxLevel()) {
            GameConfig.getInstance().setMaxLevel(this.mCurrentLevelId);
        }
        notifyListener();
    }

    public void restartLevel() {
        initLevel();
        initPlayer();
        this.mBoard.reset();
    }

    private void checkGameStatus() {
        if (this.mPlayer.getRemainedFields() == 0 && !this.mBoard.areParticlesActive()) {
            if (this.mPlayer.getMoves() < this.mMovesRecord || this.mMovesRecord == -1) {
                GameConfig.getInstance().setMovesRecord(this.mCurrentLevelId, this.mPlayer.getMoves());
            }
            levelFinished();
        } else if (!this.mBoard.areParticlesActive() && !this.mBoard.isMovePossible()) {
            Debug.inf(TAG, "Move is not possible!");
            levelFailed();
        }
    }

    private void levelFinished() {
        Resources.playSound(4, 0);
        this.mGameState = 1;
        notifyListener();
    }

    private void levelFailed() {
        Resources.playSound(5, 0);
        this.mGameState = 2;
        notifyListener();
    }

    private void gameFinished() {
        Debug.inf(TAG, "Game finished!");
        Resources.playSound(4, 0);
        this.mGameState = 4;
        notifyListener();
    }

    private void gameFailed() {
        Debug.inf(TAG, "Game over!");
        this.mGameState = 3;
        notifyListener();
    }

    private void initBoard() {
        this.mBoard = new BoardSprite(this.mCurrentLevel.getRows(), this.mCurrentLevel.getCols(), Renderer.getWidth(), Renderer.getHeight() - Core.getAdHeight(), this.mCurrentLevel.getTypesCount(), 0, 0);
    }

    private void drawTopBkg() {
        this.mTopBkg.setAlpha(Field.TYPE_2_0);
        this.mTopBkg.render();
    }

    private void drawTopStatus() {
        String title = "Moves: " + this.mPlayer.getMoves();
        if (this.mMovesRecord != -1) {
            title = String.valueOf(title) + "/" + Integer.toString(this.mMovesRecord);
        }
        Renderer.setStrokeWidth(0);
        Renderer.setStyle(Paint.Style.FILL);
        float x = this.mSpaceX;
        float y = (float) (this.mStatusTitleSize + ((this.mTopBkg.getHeight() - this.mStatusTitleSize) / 2));
        Renderer.setTextSize(this.mStatusTitleSize);
        if (this.mPlayer.getMoves() < this.mMovesRecord || this.mMovesRecord == -1) {
            Renderer.setColor(-16777216);
        } else {
            Renderer.setColor(-16777216);
        }
        Renderer.setTypeface(Typeface.DEFAULT);
        Renderer.drawText(title, x, y);
        int fieldsCount = this.mBoard.getFieldsCount();
        String title2 = "Eggs: " + (fieldsCount - this.mPlayer.getRemainedFields()) + "/" + fieldsCount;
        Renderer.setTextSize(this.mStatusTitleSize);
        Renderer.setColor(-16777216);
        Renderer.setTypeface(Typeface.DEFAULT);
        Renderer.drawText(title2, (((float) Renderer.getWidth()) - this.mSpaceX) - ((float) Renderer.getTextBounds(title2).width()), (float) (this.mStatusTitleSize + ((this.mTopBkg.getHeight() - this.mStatusTitleSize) / 2)));
        String title3 = Integer.toString(this.mCurrentLevelId);
        Renderer.setColor(-16777216);
        Renderer.setTypeface(Typeface.DEFAULT_BOLD);
        Renderer.setTextSize(this.mStatusLevelSize);
        float x2 = (float) ((Renderer.getWidth() - Renderer.getTextBounds(title3).width()) / 2);
        int tmp = Renderer.getTextBounds(title3).height();
        Renderer.drawText(title3, x2, (float) (((this.mTopBkg.getHeight() - tmp) / 2) + tmp));
    }

    private void generateLevel(int id) {
        int cols = 5;
        int numOfTypes = 1;
        switch (id) {
            case 1:
                cols = 5;
                numOfTypes = 2;
                break;
            case 2:
                cols = 5;
                numOfTypes = 3;
                break;
            case 3:
                cols = 7;
                numOfTypes = 2;
                break;
            case 4:
                cols = 7;
                numOfTypes = 3;
                break;
            case 5:
                cols = 8;
                numOfTypes = 2;
                break;
            case 6:
                cols = 8;
                numOfTypes = 3;
                break;
            case 7:
                cols = 8;
                numOfTypes = 4;
                break;
            case 8:
                cols = 8;
                numOfTypes = 2;
                break;
            case 9:
                cols = 8;
                numOfTypes = 3;
                break;
            case Field.TYPE_10:
                cols = 8;
                numOfTypes = 4;
                break;
            case Field.TYPE_CHICKEN:
                cols = 9;
                numOfTypes = 3;
                break;
            case 12:
                cols = 9;
                numOfTypes = 4;
                break;
            case 13:
                cols = 10;
                numOfTypes = 3;
                break;
            case 14:
                cols = 10;
                numOfTypes = 4;
                break;
            case 15:
                cols = 10;
                numOfTypes = 5;
                break;
            case 16:
                cols = 11;
                numOfTypes = 3;
                break;
            case 17:
                cols = 11;
                numOfTypes = 4;
                break;
            case 18:
                cols = 11;
                numOfTypes = 5;
                break;
            case 19:
                cols = 12;
                numOfTypes = 3;
                break;
            case 20:
                cols = 12;
                numOfTypes = 4;
                break;
            case 21:
                cols = 12;
                numOfTypes = 5;
                break;
            case DEF_MAX_LEVELS /*22*/:
                cols = 12;
                numOfTypes = 6;
                break;
            default:
                Debug.warn(TAG, "No level for id: " + id);
                break;
        }
        int fieldWidth = (int) (BoardSkin.getInstance().getFieldIconFactor() * ((float) (Renderer.getWidth() / cols)));
        int height = Renderer.getHeight() - Core.getAdHeight();
        int rowsCounter = 0;
        do {
            rowsCounter++;
        } while (rowsCounter * fieldWidth <= height);
        int rows = rowsCounter - 1;
        Debug.inf(TAG, "rows: " + rows + ", fieldWidth: " + fieldWidth + "height: " + height);
        this.mCurrentLevel.setRows(rows);
        this.mCurrentLevel.setCols(cols);
        this.mCurrentLevel.setTypesCount(numOfTypes);
    }
}
