package com.kasuroid.eastereggs;

import com.kasuroid.core.scene.Sprite;
import com.kasuroid.core.scene.Vector3d;

public class SpriteCircle extends Sprite {
    protected Vector3d mCenter;
    protected int mRadius;

    public SpriteCircle(int resId, float x, float y, int radius) {
        super(resId, x, y);
        this.mCoords.x -= (float) (getWidth() / 2);
        this.mCoords.y -= (float) (getHeight() / 2);
        this.mRadius = radius;
        this.mCenter = new Vector3d(x, y, 0.0f);
    }

    public boolean isCollisionPoint(float x, float y) {
        return ((float) (this.mRadius * this.mRadius)) >= ((this.mCenter.x - x) * (this.mCenter.x - x)) + ((this.mCenter.y - y) * (this.mCenter.y - y));
    }
}
