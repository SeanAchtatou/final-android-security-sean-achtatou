package com.kasuroid.eastereggs;

import android.content.SharedPreferences;
import android.graphics.Color;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;

public class GameConfig {
    public static final String APPID = "4@>]<2DFC@:5]62DE6C688D";
    public static final int MENU_BORDER_COLOR = Color.argb(255, 0, 0, 0);
    public static final int MENU_COLOR = Color.argb(255, 250, 250, 250);
    public static final int MENU_FONT_SIZE = 40;
    private static final String PREFS_NAME = "EASTEREGGS_PREFS";
    private static final String TAG = GameConfig.class.getSimpleName();
    private static GameConfig mInstance;
    private int[] mBkgThemes = {R.drawable.bkg_0, R.drawable.bkg_1, R.drawable.bkg_2, R.drawable.bkg_3};
    private int mBkgType;

    public static GameConfig getInstance() {
        if (mInstance == null) {
            mInstance = new GameConfig();
        }
        return mInstance;
    }

    private GameConfig() {
    }

    public void load() {
        this.mBkgType = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).getInt("bkgType", 0);
    }

    public void write() {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putInt("bkgType", this.mBkgType);
        editor.commit();
    }

    public int getMovesRecord(int level) {
        return ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).getInt("moves_" + Integer.toString(level), -1);
    }

    public void setMovesRecord(int level, int moves) {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putInt("moves_" + Integer.toString(level), moves);
        editor.commit();
    }

    public void setLastLevel(int level) {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putInt("lastLevel", level);
        editor.commit();
    }

    public int getLastLevel() {
        return ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).getInt("lastLevel", 1);
    }

    public void setMaxLevel(int level) {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        Debug.inf(TAG, "maxLevel: " + level);
        editor.putInt("maxLevel", level);
        editor.commit();
    }

    public int getMaxLevel() {
        return ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).getInt("maxLevel", 0);
    }

    public void reset() {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putInt("maxLevel", 0);
        editor.commit();
    }

    public int getBkgType() {
        return this.mBkgType;
    }

    public void setBkgType(int type) {
        this.mBkgType = type;
        Debug.inf(TAG, "bkgType: " + type);
    }

    public boolean isSoundEnabled() {
        return ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).getBoolean("soundEnabled", true);
    }

    public void setSoundEnabled(boolean enable) {
        SharedPreferences.Editor editor = ((MainActivity) Core.getContext()).getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putBoolean("soundEnabled", enable);
        editor.commit();
    }

    public int getBkgId() {
        return this.mBkgThemes[Core.getRandom().nextInt(this.mBkgThemes.length)];
    }
}
