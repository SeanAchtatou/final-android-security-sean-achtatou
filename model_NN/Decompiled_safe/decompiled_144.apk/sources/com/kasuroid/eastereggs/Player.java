package com.kasuroid.eastereggs;

public class Player {
    private static final String TAG = Player.class.getSimpleName();
    private int mLevel = 1;
    private int mMoves = 0;
    private int mRemainedBlocks = 0;

    public void setCurrentLevel(int level) {
        this.mLevel = level;
    }

    public int getLevel() {
        return this.mLevel;
    }

    public void setMoves(int moves) {
        this.mMoves = moves;
    }

    public int getMoves() {
        return this.mMoves;
    }

    public void updateMoves(int moves) {
        this.mMoves += moves;
    }

    public void setRemainedBlocks(int blocks) {
        this.mRemainedBlocks = blocks;
    }

    public int getRemainedFields() {
        return this.mRemainedBlocks;
    }
}
