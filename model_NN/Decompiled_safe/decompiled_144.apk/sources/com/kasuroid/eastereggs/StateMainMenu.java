package com.kasuroid.eastereggs;

import android.graphics.Typeface;
import android.view.KeyEvent;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.GameState;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Menu;
import com.kasuroid.core.MenuHandler;
import com.kasuroid.core.MenuItem;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;
import com.kasuroid.core.scene.Sprite;
import com.kasuroid.core.scene.Text;
import java.util.Random;

public class StateMainMenu extends GameState {
    private static final int DEF_PARTICLE_HEIGHT = 60;
    private static final int DEF_PARTICLE_WIDTH = 45;
    private static final int MENU_BTN_SPACE = 20;
    private static final String TAG = "StateMainMenu";
    private boolean mIsHelp;
    private Menu mMenu;
    private int mMenuTextSize;
    private Particles mParticles;
    private boolean mPaused;
    private Random mRandom;
    private Sprite mSprTitle;
    private Texture mTexBkg;

    public int onInit() {
        super.onInit();
        GameManager.getInstance();
        this.mSprTitle = new Sprite(R.drawable.main_title, 0.0f, 0.0f);
        this.mTexBkg = TextureManager.load(GameConfig.getInstance().getBkgId());
        this.mTexBkg.scale(Renderer.getWidth(), Renderer.getHeight());
        this.mMenuTextSize = (int) (40.0f * Core.getScale());
        BoardSkin.getInstance().scale((int) (45.0f * Core.getScale()), (int) (60.0f * Core.getScale()));
        prepareMenu();
        this.mPaused = false;
        this.mIsHelp = false;
        this.mParticles = new Particles();
        this.mRandom = new Random();
        GameManager.getInstance().disableOptionsMenu();
        Core.showAd();
        return 0;
    }

    public int onTerm() {
        super.onTerm();
        Debug.inf(TAG, "onTerm()");
        return 0;
    }

    public int onPause() {
        super.onPause();
        Debug.inf(TAG, "onPause()");
        this.mMenu.hide();
        if (this.mIsHelp) {
            this.mPaused = false;
        } else {
            this.mPaused = true;
        }
        return 0;
    }

    public int onResume() {
        super.onResume();
        Debug.inf(TAG, "onResume()");
        Core.showAd();
        this.mPaused = false;
        this.mIsHelp = false;
        this.mMenu.show();
        return 0;
    }

    public int onUpdate() {
        if (this.mPaused) {
            return 0;
        }
        this.mParticles.update();
        return 0;
    }

    public int onRender() {
        if (this.mPaused) {
            return 0;
        }
        drawBackground();
        this.mMenu.render();
        if (!this.mIsHelp) {
            this.mSprTitle.render();
        }
        this.mParticles.render();
        return 0;
    }

    public boolean onTouchEvent(InputEvent event) {
        if (this.mMenu.onTouchEvent(event) || event.getAction() != 3) {
            return true;
        }
        this.mParticles.addParticles(event.getX(), event.getY(), this.mRandom.nextInt(6) + 1);
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return false;
        }
        Core.quit();
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            Debug.inf(TAG, "Blocking the BACK key!");
            return true;
        }
        Debug.inf(TAG, "onKeyUp");
        return false;
    }

    private void prepareMenu() {
        Typeface typeface = Renderer.loadTtfFont("menu.ttf");
        this.mMenu = new Menu();
        MenuHandler handler = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateMainMenu.this.onMenuNewGame();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text = new Text("NEW GAME", 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover = new Text("NEW GAME", 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text.setTypeface(typeface);
        text.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text.setStrokeEnable(true);
        text.setStrokeWidth(2);
        text.setAlpha(150);
        textHover.setTypeface(typeface);
        textHover.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover.setStrokeEnable(true);
        textHover.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text, textHover, textHover, handler));
        MenuHandler handler2 = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateMainMenu.this.onMenuHomepage();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text2 = new Text("HOMEPAGE", 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover2 = new Text("HOMEPAGE", 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text2.setTypeface(typeface);
        text2.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text2.setStrokeEnable(true);
        text2.setStrokeWidth(2);
        text2.setAlpha(150);
        textHover2.setTypeface(typeface);
        textHover2.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover2.setStrokeEnable(true);
        textHover2.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text2, textHover2, textHover2, handler2));
        MenuHandler handler3 = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateMainMenu.this.onMenuSettings();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text3 = new Text("SETTINGS", 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover3 = new Text("SETTINGS", 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text3.setTypeface(typeface);
        text3.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text3.setStrokeEnable(true);
        text3.setStrokeWidth(2);
        text3.setAlpha(150);
        textHover3.setTypeface(typeface);
        textHover3.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover3.setStrokeEnable(true);
        textHover3.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text3, textHover3, textHover3, handler3));
        MenuHandler handler4 = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateMainMenu.this.onMenuHelp();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text4 = new Text("HELP", 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover4 = new Text("HELP", 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text4.setTypeface(typeface);
        text4.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text4.setStrokeEnable(true);
        text4.setStrokeWidth(2);
        text4.setAlpha(150);
        textHover4.setTypeface(typeface);
        textHover4.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover4.setStrokeEnable(true);
        textHover4.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text4, textHover4, textHover4, handler4));
        MenuHandler handler5 = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateMainMenu.this.onMenuExit();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text5 = new Text("EXIT", 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover5 = new Text("EXIT", 0.0f, 0.0f, this.mMenuTextSize, -65536);
        text5.setTypeface(typeface);
        text5.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text5.setStrokeEnable(true);
        text5.setStrokeWidth(2);
        text5.setAlpha(150);
        textHover5.setTypeface(typeface);
        textHover5.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover5.setStrokeEnable(true);
        textHover5.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text5, textHover5, textHover5, handler5));
        this.mMenu.setPositionType(4);
        this.mMenu.setVerticalItemsOffset((int) (20.0f * Core.getScale()));
        this.mMenu.setOffset(0.0f, -20.0f * Core.getScale());
        this.mMenu.setTitle(this.mSprTitle);
        this.mMenu.setTitleOffset(0.0f, 50.0f * Core.getScale());
    }

    /* access modifiers changed from: private */
    public void onMenuNewGame() {
        Debug.inf(TAG, "Going to play!");
        if (changeState(new StatePlay(GameConfig.getInstance().getLastLevel() - 1)) != 0) {
            Debug.err(TAG, "Problem with changing the state!");
        }
    }

    /* access modifiers changed from: private */
    public void onMenuHelp() {
        this.mIsHelp = true;
        if (pushState(new StateHelp()) != 0) {
            Debug.err(TAG, "Problem with pushing the state!");
        }
    }

    /* access modifiers changed from: private */
    public void onMenuExit() {
        Debug.inf(TAG, "Exiting!");
        Core.quit();
    }

    /* access modifiers changed from: private */
    public void onMenuSettings() {
        Debug.inf(TAG, "Going to settings");
        if (pushState(new StateSettings(this.mTexBkg)) != 0) {
            Debug.err(TAG, "Problem with pushing the state!");
        }
    }

    /* access modifiers changed from: private */
    public void onMenuHomepage() {
        Debug.inf(TAG, "Going to view the homepage.");
        Core.openHomepage();
    }

    private void drawBackground() {
        Renderer.setAlpha(255);
        Renderer.drawTexture(this.mTexBkg, 0.0f, 0.0f);
    }

    private void genereteNewParticles() {
        if (this.mParticles.getSize() == 0) {
            this.mParticles.addParticles(this.mRandom.nextFloat() * ((float) Renderer.getWidth()), this.mRandom.nextFloat() * ((float) Renderer.getHeight()), this.mRandom.nextInt(6) + 1);
        }
    }
}
