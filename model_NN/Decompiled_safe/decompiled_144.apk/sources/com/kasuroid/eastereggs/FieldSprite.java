package com.kasuroid.eastereggs;

import com.kasuroid.core.scene.Sprite;

public class FieldSprite extends Sprite {
    private int mFieldType;
    private int mStatus = 0;

    public FieldSprite(float x, float y) {
        super(x, y);
        setFieldType(0);
    }

    public int getFieldType() {
        return this.mFieldType;
    }

    public void setFieldType(int type) {
        this.mFieldType = type;
        setTexture(BoardSkin.getInstance().getFieldIcon(this.mFieldType));
    }

    public int getStatus() {
        return this.mStatus;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }
}
