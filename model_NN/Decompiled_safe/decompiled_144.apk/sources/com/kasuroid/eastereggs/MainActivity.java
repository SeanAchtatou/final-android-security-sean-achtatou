package com.kasuroid.eastereggs;

import android.view.Menu;
import android.view.MenuItem;
import com.kasuroid.admob.AdMobView;
import com.kasuroid.admob.AdMobViewCtrl;
import com.kasuroid.core.Core;
import com.kasuroid.core.CoreActivity;
import com.kasuroid.core.CoreView;
import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.SoundManager;

public class MainActivity extends CoreActivity {
    private static final int MENU_GROUP_1 = 1;
    private static final int MENU_NEXT_ID = 3;
    private static final int MENU_PREV_ID = 1;
    private static final int MENU_RESET_ID = 2;
    private static final String TAG = MainActivity.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void onMainCreate() {
        setContentView((int) R.layout.kasuroid_layout);
        setCoreView((CoreView) findViewById(R.id.id_kasuroid_view));
        Core.enableAds();
        if (Core.areAdsEnabled()) {
            Debug.inf(TAG, "Ads enabled");
            AdMobViewCtrl ctrl = new AdMobViewCtrl();
            ctrl.setAd((AdMobView) findViewById(R.id.id_ad));
            ctrl.load();
            setAdCtrl(ctrl);
        }
    }

    /* access modifiers changed from: protected */
    public void onInit() {
        super.onInit();
    }

    /* access modifiers changed from: protected */
    public void onTerm() {
        Resources.stopMusic();
    }

    /* access modifiers changed from: protected */
    public void onReady() {
        super.onReady();
        Core.checkPackage(GameConfig.APPID);
        Renderer.setAntiAlias(true);
        Resources.loadSounds();
        GameConfig.getInstance().load();
        SoundManager.setEnabled(GameConfig.getInstance().isSoundEnabled());
        Resources.playMusic(1, true);
        Core.changeState(new StateMainMenu());
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!GameManager.getInstance().isOptionsMenuEnabled()) {
            return false;
        }
        synchronized (Core.getLock()) {
            menu.removeItem(1);
            menu.removeItem(2);
            menu.removeItem(3);
            if (GameManager.getInstance().isPrevOptionsMenu()) {
                menu.add(1, 1, 1, "Previous");
            }
            menu.add(1, 2, 2, "Reset");
            if (GameManager.getInstance().isNextOptionsMenu()) {
                menu.add(1, 3, 3, "Next");
            }
        }
        menu.setGroupVisible(1, true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                synchronized (Core.getLock()) {
                    Debug.inf(TAG, "Menu PREV selected");
                    GameManager.getInstance().previousLevel();
                }
                return true;
            case 2:
                synchronized (Core.getLock()) {
                    Debug.inf(TAG, "Menu RESET selected");
                    GameManager.getInstance().restartLevel();
                }
                return true;
            case 3:
                synchronized (Core.getLock()) {
                    Debug.inf(TAG, "Menu NEXT selected");
                    GameManager.getInstance().nextLevel();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
