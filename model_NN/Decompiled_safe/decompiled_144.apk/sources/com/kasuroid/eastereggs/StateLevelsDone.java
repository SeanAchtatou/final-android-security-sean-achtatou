package com.kasuroid.eastereggs;

import android.graphics.Typeface;
import android.view.KeyEvent;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.GameState;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Menu;
import com.kasuroid.core.MenuHandler;
import com.kasuroid.core.MenuItem;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.scene.Rectangle;
import com.kasuroid.core.scene.Sprite;
import com.kasuroid.core.scene.Text;

public class StateLevelsDone extends GameState {
    private static final int MENU_BTN_SPACE = 20;
    private static final String TAG = StateLevelsDone.class.getSimpleName();
    private int mHelpTextSize;
    private int mHelpTextSpace;
    private Menu mMenu;
    private int mMenuTextSize;
    private Rectangle mRect;
    private Sprite mSprTitle;
    private Text mText1;
    private Text mText2;

    public int onInit() {
        super.onInit();
        this.mSprTitle = new Sprite(R.drawable.main_title, 0.0f, 0.0f);
        this.mHelpTextSize = (int) (20.0f * Core.getScale());
        this.mHelpTextSpace = (int) (5.0f * Core.getScale());
        this.mMenuTextSize = (int) (40.0f * Core.getScale());
        this.mRect = new Rectangle(0, 0, Renderer.getWidth(), Renderer.getHeight());
        this.mRect.setColor(-16777216);
        this.mRect.setAlpha(150);
        Core.showAd();
        prepareMenu();
        prepareText();
        GameManager.getInstance().disableOptionsMenu();
        return 0;
    }

    public int onTerm() {
        super.onTerm();
        Debug.inf(getClass().getName(), "onTerm()");
        GameManager.getInstance().enableOptionsMenu();
        return 0;
    }

    public int onPause() {
        super.onPause();
        Debug.inf(getClass().getName(), "onPause()");
        return 0;
    }

    public int onResume() {
        super.onResume();
        return 0;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        this.mRect.render();
        drawHelp();
        this.mMenu.render();
        return 0;
    }

    public boolean onTouchEvent(InputEvent event) {
        return this.mMenu.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return false;
        }
        GameManager.getInstance().nextLevel();
        if (popState() == 0) {
            return true;
        }
        Debug.err(getClass().getName(), "Problem with poping the state!");
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            Debug.inf(getClass().getName(), "Blocking the BACK key!");
            return true;
        }
        Debug.inf(getClass().getName(), "onKeyUp");
        return false;
    }

    private void prepareMenu() {
        Typeface typeface = Renderer.loadTtfFont("menu.ttf");
        this.mMenu = new Menu();
        MenuHandler handler = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateLevelsDone.this.onMenuMainMenu();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text = new Text(Core.getString(R.string.menu_main_menu), 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover = new Text(Core.getString(R.string.menu_main_menu), 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text.setTypeface(typeface);
        text.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text.setStrokeEnable(true);
        text.setStrokeWidth(2);
        text.setAlpha(150);
        textHover.setTypeface(typeface);
        textHover.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover.setStrokeEnable(true);
        textHover.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text, textHover, textHover, handler));
        this.mMenu.setPositionType(4);
        this.mMenu.setVerticalItemsOffset(MENU_BTN_SPACE);
        this.mMenu.setOffset(0.0f, -15.0f * Core.getScale());
        this.mMenu.setTitle(this.mSprTitle);
        this.mMenu.setTitleOffset(0.0f, 120.0f * Core.getScale());
    }

    /* access modifiers changed from: private */
    public void onMenuMainMenu() {
        if (Core.changeState(new StateMainMenu()) != 0) {
            Debug.err(getClass().getName(), "Problem with changing the state!");
        }
    }

    private void prepareText() {
        int y = ((int) this.mSprTitle.getCoordsY()) + this.mSprTitle.getHeight() + ((int) (25.0f * Core.getScale()));
        this.mText1 = new Text("Great work!", (float) 0, (float) y, this.mHelpTextSize + ((int) (10.0f * Core.getScale())), -1);
        int x = (Renderer.getWidth() - this.mText1.getWidth()) / 2;
        this.mText1.setCoordsXY((float) x, (float) y);
        int y2 = y + this.mHelpTextSize;
        this.mText2 = new Text("All levels completed!", (float) x, (float) y2, this.mHelpTextSize, -1);
        this.mText2.setCoordsXY((float) ((Renderer.getWidth() - this.mText2.getWidth()) / 2), (float) (y2 + this.mHelpTextSize + this.mHelpTextSpace));
    }

    private void drawHelp() {
        this.mText1.render();
        this.mText2.render();
    }
}
