package com.kasuroid.eastereggs;

import android.graphics.Paint;
import android.graphics.Rect;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.scene.Vector3d;
import java.util.Random;

public class AnimatedBoard {
    private static final String TAG = AnimatedBoard.class.getSimpleName();
    private Rect mBounds;
    private int mCols;
    private Vector3d mCoords;
    private float mCurrHeight;
    private float mCurrWidth;
    private int mHeight;
    private Vector3d mOffs;
    private int mRows;
    private float mSpeedX = 1.0f;
    private float mSpeedY = 1.0f;
    private Rect mTmpRect;
    private int mWidth;

    public AnimatedBoard(int rows, int cols, int width, int height) {
        this.mRows = rows;
        this.mCols = cols;
        this.mWidth = width;
        this.mHeight = height;
        this.mBounds = new Rect(0, 0, (width * cols) / 4, (height * rows) / 4);
        Random random = new Random();
        this.mCoords = new Vector3d((float) random.nextInt(this.mBounds.width()), (float) random.nextInt(this.mBounds.height()), 0.0f);
        this.mOffs = new Vector3d((float) (((-width) * cols) / 2), (float) (((-height) * rows) / 2), 0.0f);
        this.mTmpRect = new Rect(0, 0, 1, 1);
        this.mCurrWidth = (float) this.mWidth;
        this.mCurrHeight = (float) this.mHeight;
    }

    public void update() {
        this.mCoords.x += this.mSpeedX;
        if (this.mCoords.x > ((float) this.mBounds.right)) {
            this.mCoords.x = (float) this.mBounds.right;
            this.mSpeedX = -this.mSpeedX;
        } else if (this.mCoords.x <= ((float) this.mBounds.left)) {
            this.mCoords.x = (float) this.mBounds.left;
            this.mSpeedX = -this.mSpeedX;
        }
        this.mCoords.y += this.mSpeedY;
        if (this.mCoords.y > ((float) this.mBounds.bottom)) {
            this.mCoords.y = (float) this.mBounds.bottom;
            this.mSpeedY = -this.mSpeedY;
        } else if (this.mCoords.y <= ((float) this.mBounds.top)) {
            this.mCoords.y = (float) this.mBounds.top;
            this.mSpeedY = -this.mSpeedY;
        }
    }

    public void render() {
        boolean odd = false;
        Renderer.setStyle(Paint.Style.FILL);
        Renderer.setColor(-1);
        Renderer.setAlpha(50);
        int i = (int) (this.mCoords.x + this.mOffs.x);
        int curY = (int) (this.mCoords.y + this.mOffs.y);
        for (int row = 0; row < this.mRows; row++) {
            if (this.mCols % 2 == 0) {
                if (odd) {
                    odd = false;
                } else {
                    odd = true;
                }
            }
            int curX = (int) (this.mCoords.x + this.mOffs.x);
            for (int col = 0; col < this.mCols; col++) {
                this.mTmpRect.left = curX;
                this.mTmpRect.top = curY;
                this.mTmpRect.right = ((int) this.mCurrWidth) + curX;
                this.mTmpRect.bottom = ((int) this.mCurrHeight) + curY;
                if (odd) {
                    odd = false;
                } else {
                    odd = true;
                }
                if (odd) {
                    Renderer.drawRect(this.mTmpRect);
                }
                curX += (int) this.mCurrWidth;
            }
            curY += (int) this.mCurrHeight;
        }
    }
}
