package com.kasuroid.eastereggs;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bkg_0 = 2130837504;
        public static final int bkg_1 = 2130837505;
        public static final int bkg_2 = 2130837506;
        public static final int bkg_3 = 2130837507;
        public static final int icon = 2130837508;
        public static final int main_title = 2130837509;
        public static final int skin_chicken = 2130837510;
        public static final int skin_field_0 = 2130837511;
        public static final int skin_field_1 = 2130837512;
        public static final int skin_field_11 = 2130837513;
        public static final int skin_field_1_0 = 2130837514;
        public static final int skin_field_1_1 = 2130837515;
        public static final int skin_field_2 = 2130837516;
        public static final int skin_field_22 = 2130837517;
        public static final int skin_field_2_0 = 2130837518;
        public static final int skin_field_2_1 = 2130837519;
        public static final int skin_field_3 = 2130837520;
        public static final int skin_field_33 = 2130837521;
        public static final int skin_field_3_0 = 2130837522;
        public static final int skin_field_3_1 = 2130837523;
        public static final int skin_field_4 = 2130837524;
        public static final int skin_field_44 = 2130837525;
        public static final int skin_field_4_0 = 2130837526;
        public static final int skin_field_4_1 = 2130837527;
        public static final int skin_field_5 = 2130837528;
        public static final int skin_field_55 = 2130837529;
        public static final int skin_field_5_0 = 2130837530;
        public static final int skin_field_5_1 = 2130837531;
        public static final int skin_field_6 = 2130837532;
        public static final int skin_field_66 = 2130837533;
        public static final int skin_field_6_0 = 2130837534;
        public static final int skin_field_6_1 = 2130837535;
        public static final int top_bkg = 2130837536;
    }

    public static final class id {
        public static final int id_ad = 2131099649;
        public static final int id_kasuroid_view = 2131099648;
    }

    public static final class layout {
        public static final int kasuroid_layout = 2130903040;
    }

    public static final class raw {
        public static final int balls_break = 2130968576;
        public static final int level_failed = 2130968577;
        public static final int level_finished = 2130968578;
        public static final int menu_up = 2130968579;
        public static final int music = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int kasuroidactivity_base = 2131034113;
        public static final int menu_back = 2131034118;
        public static final int menu_exit = 2131034121;
        public static final int menu_help = 2131034117;
        public static final int menu_homepage = 2131034116;
        public static final int menu_main_menu = 2131034119;
        public static final int menu_next_level = 2131034120;
        public static final int menu_settings = 2131034115;
        public static final int menu_sound_off = 2131034123;
        public static final int menu_sound_on = 2131034124;
        public static final int menu_start = 2131034114;
        public static final int menu_try_again = 2131034122;
    }
}
