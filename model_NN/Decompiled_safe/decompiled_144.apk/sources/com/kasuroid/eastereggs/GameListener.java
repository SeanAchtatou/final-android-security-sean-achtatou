package com.kasuroid.eastereggs;

public interface GameListener {
    void notify(GameManager gameManager, int i);
}
