package com.kasuroid.eastereggs;

import android.graphics.Paint;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;
import java.lang.reflect.Array;
import java.util.Random;

public class BoardSprite {
    private static final int DEF_RANDOMIZE_DEPTH = 5;
    private static final String TAG = BoardSprite.class.getSimpleName();
    private int mCols;
    private int mFieldHeight;
    private int mFieldSpace;
    private int mFieldWidth;
    private FieldSprite[][] mFields;
    private int mHeight;
    private int mNumOfTypes;
    private int mOffsetX;
    private int mOffsetY;
    private Particles mParticles;
    private Random mRandom;
    private int mRows;
    private int mWidth;

    public BoardSprite(int rows, int cols, int width, int height, int numOfTypes, int offsetX, int offsetY) {
        this.mRows = rows;
        this.mCols = cols;
        this.mWidth = width;
        this.mHeight = height;
        this.mFields = (FieldSprite[][]) Array.newInstance(FieldSprite.class, this.mRows, this.mCols);
        this.mOffsetX = offsetX;
        this.mOffsetY = offsetY;
        this.mFieldWidth = 0;
        this.mFieldHeight = 0;
        this.mFieldSpace = (int) (0.0f * Core.getScale());
        this.mNumOfTypes = numOfTypes;
        this.mRandom = new Random();
        BoardSkin.getInstance();
        calculateFieldSize();
        this.mOffsetX += (width - (this.mCols * this.mFieldWidth)) / 2;
        this.mOffsetY += (height - (this.mRows * this.mFieldHeight)) / 2;
        generateSkin();
        calculateFields();
        randomizeFields(5);
        this.mParticles = new Particles();
    }

    public BoardSprite(int rows, int cols, int width, int height) {
        this.mRows = rows;
        this.mCols = cols;
        this.mWidth = width;
        this.mHeight = height;
        this.mFields = (FieldSprite[][]) Array.newInstance(FieldSprite.class, this.mRows, this.mCols);
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mFieldWidth = 0;
        this.mFieldHeight = 0;
        this.mFieldSpace = (int) (0.0f * Core.getScale());
        this.mNumOfTypes = 1;
        this.mRandom = new Random();
        BoardSkin.getInstance();
        calculateFieldSize();
        generateSkin();
        calculateFields();
        this.mParticles = new Particles();
    }

    public void randomize(int count, int startType, int endType) {
        for (int i = 0; i < count; i++) {
            this.mFields[this.mRandom.nextInt(this.mRows)][this.mRandom.nextInt(this.mCols)].setFieldType(this.mRandom.nextInt(endType) + startType);
        }
    }

    public void reset() {
        randomizeFields(5);
    }

    public void update() {
        updateFields();
        this.mParticles.update();
    }

    public void render() {
        renderFields();
        this.mParticles.render();
    }

    public void renderBkgOnly() {
        renderBkg();
    }

    public boolean touch(float x, float y) {
        return proceedFields(x, y);
    }

    public int getRows() {
        return this.mRows;
    }

    public int getCols() {
        return this.mCols;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getRemainedBlocks() {
        int count = 0;
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                if (this.mFields[row][col].getFieldType() != 0) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getFieldsCount() {
        return this.mRows * this.mCols;
    }

    public boolean isMovePossible() {
        for (int row = this.mRows / 2; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                int type = this.mFields[row][col].getFieldType();
                if (type != 0 && checkMove(row, col, type)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkMove(int row, int col, int type) {
        if (row > 0 && this.mFields[row - 1][col].getFieldType() == type) {
            return true;
        }
        if (col + 1 < this.mCols && this.mFields[row][col + 1].getFieldType() == type) {
            return true;
        }
        if (row + 1 < this.mRows && this.mFields[row + 1][col].getFieldType() == type) {
            return true;
        }
        if (col <= 0 || this.mFields[row][col - 1].getFieldType() != type) {
            return false;
        }
        return true;
    }

    public boolean areParticlesActive() {
        return this.mParticles.getSize() != 0;
    }

    private void updateFields() {
    }

    private void renderFields() {
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                renderField(this.mFields[row][col]);
            }
        }
    }

    private void renderField(FieldSprite field) {
        field.render();
    }

    private void renderBkg() {
        boolean odd = false;
        for (int row = 0; row < this.mRows; row++) {
            if (this.mCols % 2 == 0) {
                if (odd) {
                    odd = false;
                } else {
                    odd = true;
                }
            }
            for (int col = 0; col < this.mCols; col++) {
                FieldSprite field = this.mFields[row][col];
                if (odd) {
                    odd = false;
                } else {
                    odd = true;
                }
                if (odd) {
                    Renderer.setStyle(Paint.Style.FILL);
                    Renderer.setColor(-1);
                    Renderer.setAlpha(20);
                    Renderer.drawRect(field.getCoordsX(), field.getCoordsY(), field.getCoordsX() + ((float) this.mFieldWidth), field.getCoordsY() + ((float) this.mFieldHeight));
                }
            }
        }
    }

    private void calculateFieldSize() {
        this.mFieldWidth = (this.mWidth - ((this.mCols - 1) * this.mFieldSpace)) / this.mCols;
        this.mFieldHeight = (this.mHeight - ((this.mRows - 1) * this.mFieldSpace)) / this.mRows;
        this.mFieldHeight = (int) (BoardSkin.getInstance().getFieldIconFactor() * ((float) this.mFieldWidth));
        Debug.inf(TAG, "mFieldWidth: " + this.mFieldWidth);
        Debug.inf(TAG, "mFieldHeight: " + this.mFieldHeight);
    }

    private void generateSkin() {
        Debug.inf(TAG, "Generating skin");
        BoardSkin.getInstance().scale(this.mFieldWidth, this.mFieldHeight);
    }

    private void calculateFields() {
        Debug.inf(TAG, "fieldWidth: " + this.mFieldWidth + ", fieldHeight: " + this.mFieldHeight);
        int x = this.mOffsetX;
        int y = this.mOffsetY;
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                FieldSprite field = new FieldSprite((float) x, (float) y);
                field.setCoordsXY((float) x, (float) y);
                this.mFields[row][col] = field;
                x = x + this.mFieldWidth + this.mFieldSpace;
            }
            y = y + this.mFieldHeight + this.mFieldSpace;
            x = this.mOffsetX;
        }
    }

    private void randomizeFields(int depth) {
        initFieldsTypes();
        for (int loop = 0; loop < depth; loop++) {
            for (int row = 0; row < this.mRows; row++) {
                for (int col = 0; col < this.mCols; col++) {
                    int tmp = this.mFields[row][col].getFieldType();
                    int nextRow = this.mRandom.nextInt(this.mRows);
                    int nextCol = this.mRandom.nextInt(this.mCols);
                    this.mFields[row][col].setFieldType(this.mFields[nextRow][nextCol].getFieldType());
                    this.mFields[nextRow][nextCol].setFieldType(tmp);
                }
            }
        }
    }

    private void initFieldsTypes() {
        int numOfBalls = this.mRows * this.mCols;
        int ballsPerType = numOfBalls / this.mNumOfTypes;
        int extraBalls = numOfBalls - (this.mNumOfTypes * ballsPerType);
        if (extraBalls != 0) {
            Debug.inf(TAG, "Not equal number of types: " + extraBalls);
        }
        int[] fieldTypes = new int[this.mNumOfTypes];
        for (int i = 0; i < this.mNumOfTypes; i++) {
            boolean found = false;
            while (!found) {
                fieldTypes[i] = this.mRandom.nextInt(6) + 1;
                int tmp = fieldTypes[i];
                Debug.inf(TAG, "tmp: " + tmp);
                found = true;
                int j = 0;
                while (true) {
                    if (j >= i) {
                        break;
                    } else if (tmp == fieldTypes[j]) {
                        found = false;
                        break;
                    } else {
                        j++;
                    }
                }
            }
        }
        int count = 0;
        int type = 0;
        int extra = 0;
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                this.mFields[row][col].setFieldType(fieldTypes[type]);
                count++;
                if (count >= ballsPerType + extra) {
                    count = 0;
                    type++;
                    if (type == this.mNumOfTypes) {
                        extra = extraBalls;
                        type--;
                    }
                }
            }
        }
    }

    private FieldSprite getTouchedField(float x, float y) {
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                FieldSprite field = this.mFields[row][col];
                if (field.isCollisionPoint(x, y)) {
                    return field;
                }
            }
        }
        return null;
    }

    private boolean proceedFields(float x, float y) {
        FieldSprite field = null;
        boolean fieldFound = false;
        boolean ret = false;
        int col = 0;
        int row = 0;
        while (row < this.mRows) {
            col = 0;
            while (true) {
                if (col >= this.mCols) {
                    break;
                }
                field = this.mFields[row][col];
                if (field.isCollisionPoint(x, y)) {
                    fieldFound = true;
                    break;
                }
                col++;
            }
            if (fieldFound) {
                break;
            }
            row++;
        }
        if (fieldFound && field.getFieldType() != 0) {
            field.setStatus(1);
            proceedFoundField(row, col, field.getFieldType());
            ret = proceedProcessingFields();
            if (!ret) {
                field.setStatus(0);
            } else {
                moveFieldsDown();
                moveFieldsToSide();
            }
        }
        return ret;
    }

    private void proceedFoundField(int row, int col, int type) {
        if (row > 0) {
            FieldSprite field = this.mFields[row - 1][col];
            if (field.getStatus() != 1 && field.getFieldType() == type) {
                field.setStatus(1);
                proceedFoundField(row - 1, col, type);
            }
        }
        if (col + 1 < this.mCols) {
            FieldSprite field2 = this.mFields[row][col + 1];
            if (field2.getStatus() != 1 && field2.getFieldType() == type) {
                field2.setStatus(1);
                proceedFoundField(row, col + 1, type);
            }
        }
        if (row + 1 < this.mRows) {
            FieldSprite field3 = this.mFields[row + 1][col];
            if (field3.getStatus() != 1 && field3.getFieldType() == type) {
                field3.setStatus(1);
                proceedFoundField(row + 1, col, type);
            }
        }
        if (col > 0) {
            FieldSprite field4 = this.mFields[row][col - 1];
            if (field4.getStatus() != 1 && field4.getFieldType() == type) {
                field4.setStatus(1);
                proceedFoundField(row, col - 1, type);
            }
        }
    }

    private boolean proceedProcessingFields() {
        int count = 0;
        for (int row = 0; row < this.mRows; row++) {
            for (int col = 0; col < this.mCols; col++) {
                if (this.mFields[row][col].getStatus() == 1) {
                    count++;
                }
            }
        }
        if (count > 1) {
            for (int row2 = 0; row2 < this.mRows; row2++) {
                for (int col2 = 0; col2 < this.mCols; col2++) {
                    FieldSprite field = this.mFields[row2][col2];
                    if (field.getStatus() == 1) {
                        this.mParticles.addParticles(field.getCoords().x + ((float) ((field.getWidth() - BoardSkin.getInstance().getFieldIconSmallWidth()) / 2)), field.getCoords().y + ((float) ((field.getHeight() - BoardSkin.getInstance().getFieldIconSmallHeight()) / 2)), field.getFieldType());
                        field.setStatus(0);
                        field.setFieldType(0);
                    }
                }
            }
            return true;
        }
        Debug.inf(TAG, "Not enough the same fields: " + count);
        return false;
    }

    private void moveFieldsDown() {
        for (int col = 0; col < this.mCols; col++) {
            for (int row = this.mRows - 1; row > 0; row--) {
                FieldSprite fieldDest = this.mFields[row][col];
                if (fieldDest.getFieldType() == 0) {
                    int r = row - 1;
                    while (true) {
                        if (r < 0) {
                            break;
                        }
                        FieldSprite fieldSrc = this.mFields[r][col];
                        if (fieldSrc.getFieldType() != 0) {
                            Debug.inf(TAG, "col: " + col + ", row: " + row + ", r: " + r);
                            fieldDest.setFieldType(fieldSrc.getFieldType());
                            fieldSrc.setFieldType(0);
                            break;
                        }
                        r--;
                    }
                }
            }
        }
    }

    private void moveFieldsToSide() {
        boolean moved = true;
        while (moved) {
            moved = false;
            for (int col = 0; col < this.mCols - 1; col++) {
                boolean empty = true;
                int row = 0;
                while (true) {
                    if (row >= this.mRows) {
                        break;
                    } else if (this.mFields[row][col].getFieldType() != 0) {
                        empty = false;
                        break;
                    } else {
                        row++;
                    }
                }
                if (empty && moveFieldsToLeft(col)) {
                    moved = true;
                }
            }
        }
    }

    private boolean moveFieldsToLeft(int col) {
        boolean moved = false;
        for (int row = 0; row < this.mRows; row++) {
            FieldSprite field = this.mFields[row][col + 1];
            if (field.getFieldType() != 0) {
                this.mFields[row][col].setFieldType(field.getFieldType());
                field.setFieldType(0);
                moved = true;
            }
        }
        return moved;
    }
}
