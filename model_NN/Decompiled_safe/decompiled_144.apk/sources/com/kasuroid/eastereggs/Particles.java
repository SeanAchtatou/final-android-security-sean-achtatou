package com.kasuroid.eastereggs;

import com.kasuroid.core.scene.Scene;
import java.util.Random;

public class Particles extends Scene {
    private static final int DEF_MAX_SPEED_X = 2;
    private static final int DEF_MAX_SPEED_Y = 5;
    private static final String TAG = Particles.class.getSimpleName();
    private static Random mRandom = new Random();

    public void addParticles(float x, float y, int type) {
        switch (type) {
            case 1:
                addBrokenEgg(x, y, 100);
                addBrokenEgg(x, y, Field.TYPE_1_1);
                break;
            case 2:
                addBrokenEgg(x, y, Field.TYPE_2_0);
                addBrokenEgg(x, y, Field.TYPE_2_1);
                break;
            case 3:
                addBrokenEgg(x, y, Field.TYPE_3_0);
                addBrokenEgg(x, y, Field.TYPE_3_1);
                break;
            case 4:
                addBrokenEgg(x, y, Field.TYPE_4_0);
                addBrokenEgg(x, y, Field.TYPE_4_1);
                break;
            case 5:
                addBrokenEgg(x, y, Field.TYPE_5_0);
                addBrokenEgg(x, y, Field.TYPE_5_1);
                break;
            case 6:
                addBrokenEgg(x, y, Field.TYPE_6_0);
                addBrokenEgg(x, y, Field.TYPE_6_1);
                break;
        }
        addBrokenEgg(x, y, 11);
    }

    private int getRandom(int max) {
        if (mRandom.nextBoolean()) {
            return mRandom.nextInt(max) + 1;
        }
        return -(mRandom.nextInt(max) + 1);
    }

    private void addBrokenEgg(float x, float y, int type) {
        addNode(new Particle(x, y, (float) getRandom(2), (float) (-(mRandom.nextInt(5) + 1)), type));
    }
}
