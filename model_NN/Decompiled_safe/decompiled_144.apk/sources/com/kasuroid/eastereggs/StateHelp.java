package com.kasuroid.eastereggs;

import android.graphics.Typeface;
import android.view.KeyEvent;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.GameState;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Menu;
import com.kasuroid.core.MenuHandler;
import com.kasuroid.core.MenuItem;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.scene.Rectangle;
import com.kasuroid.core.scene.Text;

public class StateHelp extends GameState {
    private static final int MENU_BTN_SPACE = 20;
    private int mHelpTextSize;
    private int mHelpTextSpace;
    private Menu mMenu;
    private int mMenuTextSize;
    private Rectangle mRect;
    private Text mText1;
    private Text mText2;
    private Text mText3;
    private Text mText4;
    private Text mText5;
    private Text mText6;
    private Text mText7;
    private Text mText8;
    private Text mText9;

    public int onInit() {
        super.onInit();
        prepareMenu();
        this.mHelpTextSize = (int) (17.0f * Core.getScale());
        this.mHelpTextSpace = (int) (5.0f * Core.getScale());
        this.mRect = new Rectangle(0, 0, Renderer.getWidth(), Renderer.getHeight());
        this.mRect.setColor(-16777216);
        this.mRect.setAlpha(150);
        prepareHelp();
        Core.showAd();
        return 0;
    }

    public int onTerm() {
        super.onTerm();
        Debug.inf(getClass().getName(), "onTerm()");
        return 0;
    }

    public int onPause() {
        super.onPause();
        Debug.inf(getClass().getName(), "onPause()");
        return 0;
    }

    public int onResume() {
        super.onResume();
        return 0;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        this.mRect.render();
        drawHelp();
        this.mMenu.render();
        return 0;
    }

    public boolean onTouchEvent(InputEvent event) {
        return this.mMenu.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return false;
        }
        if (popState() == 0) {
            return true;
        }
        Debug.err(getClass().getName(), "Problem with poping the state!");
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            Debug.inf(getClass().getName(), "Blocking the BACK key!");
            return true;
        }
        Debug.inf(getClass().getName(), "onKeyUp");
        return false;
    }

    /* access modifiers changed from: private */
    public void onMenuBack() {
        if (Core.popState() != 0) {
            Debug.err(getClass().getName(), "Problem with poping the state!");
        }
    }

    private void prepareMenu() {
        Typeface typeface = Renderer.loadTtfFont("menu.ttf");
        this.mMenu = new Menu();
        MenuHandler handler = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateHelp.this.onMenuBack();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        this.mMenuTextSize = (int) (40.0f * Core.getScale());
        Text text = new Text(Core.getString(R.string.menu_back), 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover = new Text(Core.getString(R.string.menu_back), 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text.setTypeface(typeface);
        text.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text.setStrokeEnable(true);
        text.setStrokeWidth(2);
        text.setAlpha(150);
        textHover.setTypeface(typeface);
        textHover.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover.setStrokeEnable(true);
        textHover.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text, textHover, textHover, handler));
        this.mMenu.setPositionType(4);
        this.mMenu.setVerticalItemsOffset(MENU_BTN_SPACE);
        this.mMenu.setOffset(0.0f, 95.0f * Core.getScale());
    }

    private void prepareHelp() {
        int y = (((Renderer.getHeight() - this.mMenu.getMenuHeight()) / 2) + ((int) this.mMenu.getOffsetY())) - (((this.mHelpTextSize + this.mHelpTextSpace) * 10) + this.mHelpTextSpace);
        this.mText1 = new Text("Tap at least two same eggs", (float) 0, (float) y, this.mHelpTextSize, -1);
        int x = (Renderer.getWidth() - this.mText1.getWidth()) / 2;
        this.mText1.setCoordsXY((float) x, (float) y);
        this.mText1.setTypeface(Typeface.DEFAULT);
        this.mText2 = new Text("to break them out. To complete", (float) x, (float) y, this.mHelpTextSize, -1);
        int x2 = (Renderer.getWidth() - this.mText2.getWidth()) / 2;
        int y2 = y + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText2.setCoordsXY((float) x2, (float) y2);
        this.mText2.setTypeface(Typeface.DEFAULT);
        this.mText3 = new Text("the level you have to remove all", (float) x2, (float) y2, this.mHelpTextSize, -1);
        int x3 = (Renderer.getWidth() - this.mText3.getWidth()) / 2;
        int y3 = y2 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText3.setCoordsXY((float) x3, (float) y3);
        this.mText3.setTypeface(Typeface.DEFAULT);
        this.mText4 = new Text("eggs. After that next level will be", (float) x3, (float) y3, this.mHelpTextSize, -1);
        int x4 = (Renderer.getWidth() - this.mText4.getWidth()) / 2;
        int y4 = y3 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText4.setCoordsXY((float) x4, (float) y4);
        this.mText4.setTypeface(Typeface.DEFAULT);
        this.mText5 = new Text("unlocked. Press MENU in game to", (float) x4, (float) y4, this.mHelpTextSize, -1);
        int x5 = (Renderer.getWidth() - this.mText5.getWidth()) / 2;
        int y5 = y4 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText5.setCoordsXY((float) x5, (float) y5);
        this.mText5.setTypeface(Typeface.DEFAULT);
        this.mText6 = new Text("see the options menu and reset", (float) x5, (float) y5, this.mHelpTextSize, -1);
        int x6 = (Renderer.getWidth() - this.mText6.getWidth()) / 2;
        int y6 = y5 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText6.setCoordsXY((float) x6, (float) y6);
        this.mText6.setTypeface(Typeface.DEFAULT);
        this.mText7 = new Text("current board or traverse", (float) x6, (float) y6, this.mHelpTextSize, -1);
        int x7 = (Renderer.getWidth() - this.mText7.getWidth()) / 2;
        int y7 = y6 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText7.setCoordsXY((float) x7, (float) y7);
        this.mText7.setTypeface(Typeface.DEFAULT);
        this.mText8 = new Text("the completed levels. Enjoy!", (float) x7, (float) y7, this.mHelpTextSize, -1);
        int x8 = (Renderer.getWidth() - this.mText8.getWidth()) / 2;
        int y8 = y7 + this.mHelpTextSize + this.mHelpTextSpace;
        this.mText8.setCoordsXY((float) x8, (float) y8);
        this.mText8.setTypeface(Typeface.DEFAULT);
        this.mText9 = new Text("kasurdev@gmail.com", (float) x8, (float) y8, this.mHelpTextSize, -1);
        this.mText9.setCoordsXY((float) ((Renderer.getWidth() - this.mText9.getWidth()) / 2), (float) (y8 + this.mHelpTextSize + (this.mHelpTextSpace * 2)));
        this.mText9.setTypeface(Typeface.DEFAULT_BOLD);
    }

    private void drawHelp() {
        this.mText1.render();
        this.mText2.render();
        this.mText3.render();
        this.mText4.render();
        this.mText5.render();
        this.mText6.render();
        this.mText7.render();
        this.mText8.render();
        this.mText9.render();
    }
}
