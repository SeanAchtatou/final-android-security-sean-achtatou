package com.kasuroid.eastereggs;

import android.graphics.Typeface;
import android.view.KeyEvent;
import com.kasuroid.core.Core;
import com.kasuroid.core.Debug;
import com.kasuroid.core.GameState;
import com.kasuroid.core.InputEvent;
import com.kasuroid.core.Menu;
import com.kasuroid.core.MenuHandler;
import com.kasuroid.core.MenuItem;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.SoundManager;
import com.kasuroid.core.Texture;
import com.kasuroid.core.scene.Text;

public class StateSettings extends GameState {
    private static final int MENU_BTN_SPACE = 20;
    private static final String TAG = StateNextLevel.class.getSimpleName();
    private Menu mMenu;
    private MenuItem mMenuItemSoundOff;
    private MenuItem mMenuItemSoundOn;
    private int mMenuTextSize;
    private Text mMenuTitle;
    private int mMenuTitleSize;
    private Texture mTexBkg;

    public StateSettings(Texture tex) {
        this.mTexBkg = tex;
    }

    public int onInit() {
        super.onInit();
        this.mMenuTitleSize = (int) (Core.getScale() * 40.0f);
        this.mMenuTextSize = (int) (Core.getScale() * 40.0f);
        GameConfig.getInstance().load();
        Core.showAd();
        prepareMenu();
        return 0;
    }

    public int onTerm() {
        super.onTerm();
        Debug.inf(getClass().getName(), "onTerm()");
        return 0;
    }

    public int onPause() {
        super.onPause();
        Debug.inf(getClass().getName(), "onPause()");
        return 0;
    }

    public int onResume() {
        super.onResume();
        return 0;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        drawBackground();
        this.mMenu.render();
        return 0;
    }

    public boolean onTouchEvent(InputEvent event) {
        return this.mMenu.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (keyCode != 4) {
            return false;
        }
        if (popState() == 0) {
            return true;
        }
        Debug.err(getClass().getName(), "Problem with poping the state!");
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        if (keyCode == 4) {
            Debug.inf(getClass().getName(), "Blocking the BACK key!");
            return true;
        }
        Debug.inf(getClass().getName(), "onKeyUp");
        return false;
    }

    private void prepareMenu() {
        MenuItem item;
        Typeface typeface = Renderer.loadTtfFont("menu.ttf");
        this.mMenu = new Menu();
        MenuHandler handler = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateSettings.this.onMenuSoundChange();
            }
        };
        Text text = new Text(Core.getString(R.string.menu_sound_on), 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover = new Text(Core.getString(R.string.menu_sound_on), 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text.setTypeface(typeface);
        text.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text.setStrokeEnable(true);
        text.setStrokeWidth(2);
        text.setAlpha(150);
        textHover.setTypeface(typeface);
        textHover.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover.setStrokeEnable(true);
        textHover.setStrokeWidth(2);
        this.mMenuItemSoundOn = new MenuItem(text, textHover, textHover, handler);
        Text text2 = new Text(Core.getString(R.string.menu_sound_off), 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover2 = new Text(Core.getString(R.string.menu_sound_off), 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text2.setTypeface(typeface);
        text2.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text2.setStrokeEnable(true);
        text2.setStrokeWidth(2);
        text2.setAlpha(150);
        textHover2.setTypeface(typeface);
        textHover2.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover2.setStrokeEnable(true);
        textHover2.setStrokeWidth(2);
        this.mMenuItemSoundOff = new MenuItem(text2, textHover2, textHover2, handler);
        if (GameConfig.getInstance().isSoundEnabled()) {
            item = this.mMenuItemSoundOn;
        } else {
            item = this.mMenuItemSoundOff;
        }
        this.mMenu.addItem(item);
        MenuHandler handler2 = new MenuHandlerFx() {
            public void onUp() {
                super.onUp();
                StateSettings.this.onMenuBack();
            }

            public void onMove() {
            }

            public void onDown() {
            }
        };
        Text text3 = new Text(Core.getString(R.string.menu_back), 0.0f, 0.0f, this.mMenuTextSize, GameConfig.MENU_COLOR);
        Text textHover3 = new Text(Core.getString(R.string.menu_back), 0.0f, 0.0f, this.mMenuTextSize, -16711936);
        text3.setTypeface(typeface);
        text3.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        text3.setStrokeEnable(true);
        text3.setStrokeWidth(2);
        text3.setAlpha(150);
        textHover3.setTypeface(typeface);
        textHover3.setStrokeColor(GameConfig.MENU_BORDER_COLOR);
        textHover3.setStrokeEnable(true);
        textHover3.setStrokeWidth(2);
        this.mMenu.addItem(new MenuItem(text3, textHover3, textHover3, handler2));
        this.mMenuTitle = new Text("Settings", 0.0f, 0.0f, this.mMenuTitleSize, GameConfig.MENU_COLOR);
        this.mMenuTitle.setTypeface(Typeface.DEFAULT_BOLD);
        this.mMenu.setTitle(this.mMenuTitle);
        this.mMenu.setPositionType(4);
        this.mMenu.setVerticalItemsOffset((int) (20.0f * Core.getScale()));
        this.mMenu.setOffset(0.0f, 20.0f * Core.getScale());
        this.mMenu.setTitleOffset(0.0f, 10.0f * Core.getScale());
    }

    /* access modifiers changed from: private */
    public void onMenuSoundChange() {
        MenuItem item;
        boolean enabled;
        if (GameConfig.getInstance().isSoundEnabled()) {
            item = this.mMenuItemSoundOff;
            enabled = false;
            Resources.stopMusic();
            SoundManager.setEnabled(false);
        } else {
            item = this.mMenuItemSoundOn;
            enabled = true;
            SoundManager.setEnabled(true);
            Resources.playMusic(1, true);
        }
        GameConfig.getInstance().setSoundEnabled(enabled);
        this.mMenu.replaceItem(0, item);
    }

    /* access modifiers changed from: private */
    public void onMenuBack() {
        Debug.inf(TAG, "Going to next level");
        if (Core.popState() != 0) {
            Debug.err(getClass().getName(), "Problem with poping the state!");
        }
    }

    private void drawBackground() {
        Renderer.setAlpha(255);
        Renderer.drawTexture(this.mTexBkg, 0.0f, 0.0f);
    }
}
