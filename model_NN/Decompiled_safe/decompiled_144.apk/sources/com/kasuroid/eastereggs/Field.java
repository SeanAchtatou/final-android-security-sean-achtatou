package com.kasuroid.eastereggs;

public class Field {
    public static final int STATUS_PROCESSING = 1;
    public static final int STATUS_READY = 0;
    private static final String TAG = Field.class.getSimpleName();
    public static final int TYPE_1 = 1;
    public static final int TYPE_10 = 10;
    public static final int TYPE_1_0 = 100;
    public static final int TYPE_1_1 = 101;
    public static final int TYPE_2 = 2;
    public static final int TYPE_2_0 = 200;
    public static final int TYPE_2_1 = 201;
    public static final int TYPE_3 = 3;
    public static final int TYPE_3_0 = 300;
    public static final int TYPE_3_1 = 301;
    public static final int TYPE_4 = 4;
    public static final int TYPE_4_0 = 400;
    public static final int TYPE_4_1 = 401;
    public static final int TYPE_5 = 5;
    public static final int TYPE_5_0 = 500;
    public static final int TYPE_5_1 = 501;
    public static final int TYPE_6 = 6;
    public static final int TYPE_6_0 = 600;
    public static final int TYPE_6_1 = 601;
    public static final int TYPE_7 = 7;
    public static final int TYPE_8 = 8;
    public static final int TYPE_9 = 9;
    public static final int TYPE_CHICKEN = 11;
    public static final int TYPE_EMPTY = 0;
}
