package com.kasuroid.eastereggs;

import com.kasuroid.core.Debug;
import com.kasuroid.core.SoundManager;

public class Resources {
    public static final int DEF_MUSIC_GAME = 2;
    public static final int DEF_MUSIC_MENU = 1;
    public static final int DEF_SOUND_BALLS_BREAK = 6;
    public static final int DEF_SOUND_LEVEL_FAILED = 5;
    public static final int DEF_SOUND_LEVEL_FINISHED = 4;
    public static final int DEF_SOUND_MENU_UP = 3;
    private static final String TAG = Resources.class.getSimpleName();
    private static int mSoundBallsBreak;
    private static int mSoundLevelFailed;
    private static int mSoundLevelFinished;
    private static int mSoundMenuUp;

    public static void loadSounds() {
        mSoundBallsBreak = SoundManager.loadSound(R.raw.balls_break);
        mSoundLevelFinished = SoundManager.loadSound(R.raw.level_finished);
        mSoundLevelFailed = SoundManager.loadSound(R.raw.level_failed);
        mSoundMenuUp = SoundManager.loadSound(R.raw.menu_up);
    }

    public static int playMusic(int id, boolean loop) {
        return SoundManager.playMusic(getSoundId(id), loop);
    }

    public static int stopMusic() {
        return SoundManager.stopMusic();
    }

    public static int playSound(int id, int loop) {
        int ret = SoundManager.playSound(getSoundId(id), loop);
        if (ret != 0) {
            Debug.err(TAG, "Couldn't play the sound");
        }
        return ret;
    }

    public static int stopSound(int id) {
        return SoundManager.stopSound(getSoundId(id));
    }

    public static int pauseSound(int id) {
        return SoundManager.pauseSound(getSoundId(id));
    }

    public static int resumeSound(int id) {
        return SoundManager.resumeSound(getSoundId(id));
    }

    public static int getSoundId(int id) {
        switch (id) {
            case 1:
                return R.raw.music;
            case 2:
            default:
                return -1;
            case 3:
                return mSoundMenuUp;
            case 4:
                return mSoundLevelFinished;
            case 5:
                return mSoundLevelFailed;
            case 6:
                return mSoundBallsBreak;
        }
    }
}
