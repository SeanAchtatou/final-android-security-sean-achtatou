package com.kasuroid.eastereggs;

import com.kasuroid.core.MenuHandler;

public class MenuHandlerFx implements MenuHandler {
    private int mSoundUp = Resources.getSoundId(3);

    public void onDown() {
    }

    public void onMove() {
    }

    public void onUp() {
        Resources.playSound(3, 0);
    }
}
