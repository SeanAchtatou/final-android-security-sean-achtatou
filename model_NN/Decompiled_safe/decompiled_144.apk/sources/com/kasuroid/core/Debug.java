package com.kasuroid.core;

public class Debug {
    private static final boolean mIsDebug = false;
    private static String mTag = "kasuroid";

    public static void setTag(String tag) {
        mTag = tag;
    }

    public static boolean getDebugStatus() {
        return false;
    }

    public static void err(String scope, String msg) {
    }

    public static void warn(String scope, String msg) {
    }

    public static void inf(String scope, String msg) {
    }

    public static void verb(String scope, String msg) {
    }
}
