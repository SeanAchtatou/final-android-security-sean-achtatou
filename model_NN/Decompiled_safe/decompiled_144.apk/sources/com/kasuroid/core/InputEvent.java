package com.kasuroid.core;

import android.view.KeyEvent;
import android.view.MotionEvent;
import java.util.concurrent.ArrayBlockingQueue;

public class InputEvent {
    public static final int ACTION_KEY_DOWN = 1;
    public static final int ACTION_KEY_UP = 2;
    public static final int ACTION_TOUCH_DOWN = 3;
    public static final int ACTION_TOUCH_MOVE = 4;
    public static final int ACTION_TOUCH_UP = 5;
    public static final byte EVENT_TYPE_KEY = 1;
    public static final byte EVENT_TYPE_TOUCH = 2;
    public int mAction;
    public byte mEventType;
    public int mKeyCode;
    public ArrayBlockingQueue<InputEvent> mPool;
    public long mTime;
    public float mX;
    public float mY;

    public byte getType() {
        return this.mEventType;
    }

    public long getTime() {
        return this.mTime;
    }

    public int getAction() {
        return this.mAction;
    }

    public int getKeyCode() {
        return this.mKeyCode;
    }

    public float getX() {
        return this.mX;
    }

    public float getY() {
        return this.mY;
    }

    public InputEvent(ArrayBlockingQueue<InputEvent> pool) {
        this.mPool = pool;
    }

    public void useEvent(KeyEvent event) {
        this.mEventType = 1;
        switch (event.getAction()) {
            case 0:
                this.mAction = 1;
                break;
            case 1:
                this.mAction = 2;
                break;
            default:
                this.mAction = 0;
                break;
        }
        this.mTime = event.getEventTime();
        this.mKeyCode = event.getKeyCode();
    }

    public void useEvent(MotionEvent event) {
        this.mEventType = 2;
        switch (event.getAction()) {
            case 0:
                this.mAction = 3;
                break;
            case 1:
                this.mAction = 5;
                break;
            case 2:
                this.mAction = 4;
                break;
            default:
                this.mAction = 0;
                break;
        }
        this.mTime = event.getEventTime();
        this.mX = (float) ((int) event.getX());
        this.mY = (float) ((int) event.getY());
    }

    public void useEventHistory(MotionEvent event, int historyItem) {
        this.mEventType = 2;
        this.mAction = 4;
        this.mTime = event.getHistoricalEventTime(historyItem);
        this.mX = (float) ((int) event.getHistoricalX(historyItem));
        this.mY = (float) ((int) event.getHistoricalY(historyItem));
    }

    public void returnToPool() {
        this.mPool.add(this);
    }
}
