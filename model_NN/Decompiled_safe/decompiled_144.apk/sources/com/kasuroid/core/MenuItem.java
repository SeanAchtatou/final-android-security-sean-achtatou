package com.kasuroid.core;

import android.graphics.Rect;
import com.kasuroid.core.scene.SceneNode;

public class MenuItem extends SceneNode {
    public static final int STATE_DOWN = 2;
    public static final int STATE_MOVE = 1;
    public static final int STATE_UP = 0;
    private static final String TAG = "MenuItem";
    private SceneNode mCurrentNode;
    private boolean mEnabled = true;
    private MenuHandler mHandler;
    private SceneNode mNodeDisabled;
    private SceneNode mNodeDown;
    private SceneNode mNodeMove;
    private SceneNode mNodeUp;
    private int mState = 0;
    private boolean mWasDown;

    public MenuItem(SceneNode nodeUp, SceneNode nodeMove, SceneNode nodeDown, MenuHandler handler) {
        this.mNodeDown = nodeDown;
        this.mNodeMove = nodeMove;
        if (nodeUp == null) {
            Debug.err(TAG, "At least node up should be defined!");
        }
        this.mNodeUp = nodeUp;
        this.mCurrentNode = nodeUp;
        this.mHandler = handler;
        this.mWasDown = false;
        calcBounds();
    }

    /* access modifiers changed from: protected */
    public void calcBounds() {
        Rect bounds = new Rect();
        if (this.mNodeUp != null) {
            bounds = this.mNodeUp.getBounds();
        }
        if (this.mNodeMove != null) {
            Rect tmp = this.mNodeMove.getBounds();
            if (tmp.left < bounds.left) {
                bounds.left = tmp.left;
            }
            if (tmp.top < bounds.top) {
                bounds.top = tmp.top;
            }
            if (tmp.right > bounds.right) {
                bounds.right = tmp.right;
            }
            if (tmp.bottom > bounds.bottom) {
                bounds.bottom = tmp.bottom;
            }
        }
        if (this.mNodeDown != null) {
            Rect tmp2 = this.mNodeDown.getBounds();
            if (tmp2.left < bounds.left) {
                bounds.left = tmp2.left;
            }
            if (tmp2.top < bounds.top) {
                bounds.top = tmp2.top;
            }
            if (tmp2.right > bounds.right) {
                bounds.right = tmp2.right;
            }
            if (tmp2.bottom > bounds.bottom) {
                bounds.bottom = tmp2.bottom;
            }
        }
        setBounds(bounds);
    }

    public boolean isEnabled() {
        return this.mEnabled;
    }

    public void disable() {
        this.mEnabled = false;
    }

    public void enable() {
        this.mEnabled = true;
    }

    public void setEnabled(boolean enabled) {
        this.mEnabled = enabled;
    }

    public void setNodeDisabled(SceneNode node) {
        this.mNodeDisabled = node;
    }

    public SceneNode getNodeDisabled() {
        return this.mNodeDisabled;
    }

    public SceneNode getNodeUp() {
        return this.mNodeUp;
    }

    public SceneNode getNodeDown() {
        return this.mNodeDown;
    }

    public SceneNode getNodeMove() {
        return this.mNodeMove;
    }

    public int onRender() {
        if (isVisible()) {
            if (this.mEnabled) {
                return this.mCurrentNode.render();
            }
            if (this.mNodeDisabled != null) {
                return this.mNodeDisabled.render();
            }
        }
        return 0;
    }

    public void down() {
        Debug.inf(TAG, "down");
        this.mState = 2;
        this.mWasDown = true;
        if (this.mNodeDown != null) {
            this.mCurrentNode = this.mNodeDown;
        }
        if (this.mHandler != null) {
            this.mHandler.onDown();
        }
    }

    public void move() {
        this.mState = 1;
        if (this.mNodeMove != null) {
            this.mCurrentNode = this.mNodeMove;
        }
        if (this.mHandler != null) {
            this.mHandler.onMove();
        }
    }

    public void up() {
        Debug.inf(TAG, "up");
        int prevState = this.mState;
        this.mState = 0;
        if (this.mNodeUp != null) {
            this.mCurrentNode = this.mNodeUp;
        }
        if (this.mHandler == null) {
            return;
        }
        if ((prevState == 2 || prevState == 1) && this.mWasDown) {
            this.mWasDown = false;
            this.mHandler.onUp();
        }
    }

    public void moveOff() {
        this.mCurrentNode = this.mNodeUp;
        this.mState = 0;
    }

    public int getState() {
        return this.mState;
    }

    public int getWidth() {
        return this.mBounds.width();
    }

    public int getHeight() {
        return this.mNodeUp.getHeight();
    }

    public void setCoordsXY(float x, float y) {
        if (this.mNodeUp != null) {
            this.mNodeUp.setCoordsXY(x, y);
        }
        if (this.mNodeMove != null) {
            this.mNodeMove.setCoordsXY(x, y);
        }
        if (this.mNodeDown != null) {
            this.mNodeDown.setCoordsXY(x, y);
        }
        super.setCoordsXY(x, y);
    }

    public void setCoordsZ(float z) {
        if (this.mNodeUp != null) {
            this.mNodeUp.setCoordsZ(z);
        }
        if (this.mNodeMove != null) {
            this.mNodeMove.setCoordsZ(z);
        }
        if (this.mNodeDown != null) {
            this.mNodeDown.setCoordsZ(z);
        }
        super.setCoordsZ(z);
    }

    public void setCurrentNode(SceneNode node) {
        this.mCurrentNode = node;
    }

    public boolean isCollisionPoint(float x, float y) {
        return this.mCurrentNode.isCollisionPoint(x, y);
    }
}
