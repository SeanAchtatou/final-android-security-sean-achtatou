package com.kasuroid.core;

import java.util.Enumeration;
import java.util.Vector;

public class TimerManager {
    private static final String TAG;
    private static final String mName;
    private static Vector<Timer> mTimers = null;

    static {
        Class<TimerManager> cls = TimerManager.class;
        Class<TimerManager> cls2 = TimerManager.class;
        mName = cls.getSimpleName();
        Class<TimerManager> cls3 = TimerManager.class;
        TAG = cls.getSimpleName();
    }

    public static int init() {
        mTimers = new Vector<>();
        Debug.inf(TAG, "TimerManager initialized");
        return 0;
    }

    public static int term() {
        int ret = stopAll();
        if (ret != 0) {
            Debug.err(TAG, "Problem with stopping timers!");
        }
        mTimers.clear();
        Debug.inf(TAG, "TimerManager terminated");
        return ret;
    }

    public static Timer create() {
        Timer timer = new Timer();
        mTimers.add(timer);
        return timer;
    }

    public static int remove(int id) {
        Enumeration<Timer> e = mTimers.elements();
        int i = 0;
        while (e.hasMoreElements()) {
            if (e.nextElement().getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Removing.");
                mTimers.remove(i);
                return 0;
            }
            i++;
        }
        return 2;
    }

    public static int startAll() {
        Enumeration<Timer> e = mTimers.elements();
        int ret = 0;
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            ret = timer.start();
            if (ret != 0) {
                Debug.err(TAG, "Problem with starting timer (id: " + timer.getId());
                return ret;
            }
        }
        return ret;
    }

    public static int stopAll() {
        Enumeration<Timer> e = mTimers.elements();
        int ret = 0;
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            ret = timer.stop();
            if (ret != 0) {
                Debug.err(TAG, "Problem with stopping timer (id: " + timer.getId());
                return ret;
            }
        }
        return ret;
    }

    public static int pauseAll() {
        Enumeration<Timer> e = mTimers.elements();
        int ret = 0;
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            ret = timer.pause();
            if (ret != 0) {
                Debug.err(TAG, "Problem with pausing timer (id: " + timer.getId());
                return ret;
            }
        }
        return ret;
    }

    public static int resumeAll() {
        Enumeration<Timer> e = mTimers.elements();
        int ret = 0;
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            ret = timer.resume();
            if (ret != 0) {
                Debug.err(TAG, "Problem with resuming timer (id: " + timer.getId());
                return ret;
            }
        }
        return ret;
    }

    public static int updateAll() {
        Enumeration<Timer> e = mTimers.elements();
        int ret = 0;
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            ret = timer.update();
            if (ret != 0) {
                Debug.err(TAG, "Problem with updating timer (id: " + timer.getId());
                return ret;
            }
        }
        return ret;
    }

    public static int start(int id) {
        Enumeration<Timer> e = mTimers.elements();
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            if (timer.getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Starting.");
                int ret = timer.start();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with starting the timer!");
                }
                return ret;
            }
        }
        return 2;
    }

    public static int stop(int id) {
        Enumeration<Timer> e = mTimers.elements();
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            if (timer.getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Stopping.");
                int ret = timer.stop();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with stopping the timer!");
                }
                return ret;
            }
        }
        return 2;
    }

    public static int pause(int id) {
        Enumeration<Timer> e = mTimers.elements();
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            if (timer.getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Pausing.");
                int ret = timer.pause();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with pausing the timer!");
                }
                return ret;
            }
        }
        return 2;
    }

    public static int resume(int id) {
        Enumeration<Timer> e = mTimers.elements();
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            if (timer.getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Resuming.");
                int ret = timer.resume();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with resuming the timer!");
                }
                return ret;
            }
        }
        return 2;
    }

    public static Timer get(int id) {
        Enumeration<Timer> e = mTimers.elements();
        while (e.hasMoreElements()) {
            Timer timer = e.nextElement();
            if (timer.getId() == id) {
                Debug.inf(TAG, "Timer with id " + id + " found. Pausing.");
                return timer;
            }
        }
        return null;
    }

    public static String getName() {
        return mName;
    }
}
