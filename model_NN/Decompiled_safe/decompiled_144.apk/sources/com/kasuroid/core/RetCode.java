package com.kasuroid.core;

public class RetCode {
    public static final int BAD_PARAM = 2;
    public static final int FAILURE = 1;
    public static final int SUCCESS = 0;

    public static String toString(int code) {
        switch (code) {
            case 0:
                return "SUCCESS";
            case 1:
                return "FAILURE";
            case 2:
                return "BAD_PARAM";
            default:
                return "UKNOWN";
        }
    }
}
