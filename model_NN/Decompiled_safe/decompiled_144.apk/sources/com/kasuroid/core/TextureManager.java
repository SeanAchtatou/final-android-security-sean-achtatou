package com.kasuroid.core;

import android.graphics.Bitmap;
import java.util.HashMap;

public class TextureManager {
    private static final String TAG = TextureManager.class.getSimpleName();
    private static final String mName = "TextureManager";
    private static HashMap<Integer, Texture> mTexturesPool;

    public static int init() {
        mTexturesPool = new HashMap<>();
        Debug.inf(TAG, "TextureManager  initialized");
        return 0;
    }

    public static int term() {
        mTexturesPool.clear();
        Debug.inf(TAG, "TextureManager terminated");
        return 0;
    }

    public static Texture load(int pathId) {
        Bitmap bitmap;
        Texture texture = mTexturesPool.get(Integer.valueOf(pathId));
        if (texture == null && (bitmap = UnscaledBitmapLoader.loadFromResource(Core.getContext().getResources(), pathId, null)) != null) {
            texture = new Texture(bitmap);
            if (((double) Core.getScale()) != 1.0d) {
                int newWidth = (int) (((float) texture.getWidth()) * Core.getScale());
                int newHeight = (int) (((float) texture.getHeight()) * Core.getScale());
                Debug.inf(TAG, "Going to scale (" + Core.getScale() + ") loaded texture from " + texture.getWidth() + "x" + texture.getHeight() + " to " + newWidth + "x" + newHeight);
                texture.scale(newWidth, newHeight);
            }
            mTexturesPool.put(Integer.valueOf(pathId), texture);
        }
        return texture;
    }

    public static String getName() {
        return mName;
    }
}
