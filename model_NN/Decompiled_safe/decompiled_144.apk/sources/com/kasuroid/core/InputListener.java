package com.kasuroid.core;

import android.view.KeyEvent;

public interface InputListener {
    boolean onKeyDown(int i, KeyEvent keyEvent);

    boolean onKeyUp(int i, KeyEvent keyEvent);

    boolean onTouchEvent(InputEvent inputEvent);
}
