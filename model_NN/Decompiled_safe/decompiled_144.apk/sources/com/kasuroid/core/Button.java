package com.kasuroid.core;

import android.graphics.Paint;
import android.graphics.Rect;
import com.kasuroid.eastereggs.Field;

public class Button {
    private int mAlpha;
    private int mBkgColor;
    private int mFrtColor;
    private Rect mRect;
    private Texture mTexture;
    private String mTitle;

    public Button(Rect rect, int bkgColor, String title, int frtColor) {
        this.mRect = rect;
        this.mTitle = title;
        this.mBkgColor = bkgColor;
        this.mFrtColor = frtColor;
        this.mTexture = null;
        this.mAlpha = Field.TYPE_2_0;
    }

    public Button(Texture texture, int x, int y) {
        this.mAlpha = Field.TYPE_2_0;
        this.mTexture = texture;
        this.mRect = new Rect(x, y, this.mTexture.getWidth() + x, this.mTexture.getHeight() + y);
    }

    public boolean isClicked(int x, int y) {
        if (x < this.mRect.left || x > this.mRect.right || y < this.mRect.top || y > this.mRect.bottom) {
            return false;
        }
        Debug.inf(getClass().getName(), "Clicked");
        return true;
    }

    public void render() {
        Renderer.setAlpha(this.mAlpha);
        if (this.mTexture == null) {
            Renderer.setColor(this.mBkgColor);
            Renderer.drawRect(this.mRect);
            Renderer.setColor(this.mFrtColor);
            drawCenterText();
            return;
        }
        Renderer.drawBitmap(this.mTexture.getBitmap(), (float) this.mRect.left, (float) this.mRect.top);
    }

    public void setAlpha(int alpha) {
        this.mAlpha = alpha;
    }

    public int getAlpha() {
        return this.mAlpha;
    }

    public Rect getRect() {
        return this.mRect;
    }

    /* access modifiers changed from: protected */
    public void drawCenterText() {
        Rect bounds = new Rect();
        new Paint().getTextBounds(this.mTitle, 0, this.mTitle.length(), bounds);
        Renderer.drawText(this.mTitle, ((float) this.mRect.left) + ((((float) (this.mRect.right - this.mRect.left)) - ((float) bounds.width())) / 2.0f), ((float) this.mRect.top) + ((((float) (this.mRect.bottom - this.mRect.top)) - ((float) bounds.height())) / 2.0f) + ((float) bounds.height()));
    }
}
