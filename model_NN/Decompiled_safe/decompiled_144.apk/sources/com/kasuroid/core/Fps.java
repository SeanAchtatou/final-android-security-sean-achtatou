package com.kasuroid.core;

import java.text.DecimalFormat;

public class Fps {
    private static final int FPS_HISTORY_NR = 10;
    private static final int STAT_INTERVAL = 1000;
    private static final String TAG = Fps.class.getSimpleName();
    private static DecimalFormat df = new DecimalFormat("0.##");
    private static float mAverageFps = 0.0f;
    private static float[] mFpsStore;
    private static int mFrameCountPerStatCycle = 0;
    public static long mFramesSkippedPerStatCycle = 0;
    private static long mLastStatusStore = 0;
    private static long mStatsCount = 0;
    private static long mStatusIntervalTimer = 0;
    private static long mTotalFrameCount = 0;
    private static long mTotalFramesSkipped = 0;

    public static void updateStats() {
        mFrameCountPerStatCycle++;
        mTotalFrameCount++;
        mStatusIntervalTimer += System.currentTimeMillis() - mStatusIntervalTimer;
        if (mStatusIntervalTimer >= mLastStatusStore + 1000) {
            mFpsStore[((int) mStatsCount) % 10] = (float) (mFrameCountPerStatCycle / 1);
            mStatsCount++;
            float totalFps = 0.0f;
            for (int i = 0; i < 10; i++) {
                totalFps += mFpsStore[i];
            }
            if (mStatsCount < 10) {
                mAverageFps = totalFps / ((float) mStatsCount);
            } else {
                mAverageFps = totalFps / 10.0f;
            }
            mTotalFramesSkipped += mFramesSkippedPerStatCycle;
            mFramesSkippedPerStatCycle = 0;
            mStatusIntervalTimer = 0;
            mFrameCountPerStatCycle = 0;
            mStatusIntervalTimer = System.currentTimeMillis();
            mLastStatusStore = mStatusIntervalTimer;
        }
    }

    public static void init() {
        mFpsStore = new float[10];
        for (int i = 0; i < 10; i++) {
            mFpsStore[i] = 0.0f;
        }
    }

    public static float get() {
        return mAverageFps;
    }

    public static String getString() {
        return df.format((double) mAverageFps);
    }
}
