package com.kasuroid.core;

import android.graphics.Bitmap;

public class Texture {
    private static final String TAG = Texture.class.getSimpleName();
    Bitmap mBitmap;

    Texture() {
        this.mBitmap = null;
    }

    Texture(Bitmap bitmap) {
        setBitmap(bitmap);
    }

    public void setBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return this.mBitmap;
    }

    public int getWidth() {
        return this.mBitmap.getWidth();
    }

    public int getHeight() {
        return this.mBitmap.getHeight();
    }

    public void scale(int newWidth, int newHeight) {
        this.mBitmap = Bitmap.createScaledBitmap(this.mBitmap, newWidth, newHeight, true);
    }

    public Texture scaleToTexture(int newWidth, int newHeight) {
        return new Texture(Bitmap.createScaledBitmap(this.mBitmap, newWidth, newHeight, true));
    }
}
