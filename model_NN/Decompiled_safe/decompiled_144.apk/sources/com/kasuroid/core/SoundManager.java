package com.kasuroid.core;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import java.util.HashMap;

public class SoundManager {
    private static final int MAX_STREAMS = 5;
    private static final String TAG = SoundManager.class.getSimpleName();
    private static AudioManager mAudioManager;
    private static Context mContext;
    private static boolean mEnabled;
    private static boolean mEnabledMusic;
    private static boolean mEnabledSound;
    private static int mNextSoundId;
    private static MediaPlayer mPlayer;
    private static SoundPool mSoundPool;
    private static HashMap<Integer, Integer> mSoundPoolMap;

    private SoundManager() {
    }

    public static int init(Context context) {
        Debug.inf(TAG, "init");
        mPlayer = null;
        mContext = context;
        mSoundPool = new SoundPool(5, 3, 0);
        mSoundPoolMap = new HashMap<>();
        mAudioManager = (AudioManager) mContext.getSystemService("audio");
        mNextSoundId = 0;
        enable();
        return 0;
    }

    public static int term() {
        Debug.inf(TAG, "term");
        mSoundPool.release();
        mSoundPool = null;
        mSoundPoolMap.clear();
        mAudioManager.unloadSoundEffects();
        stopMusic();
        return 0;
    }

    public static int playMusic(int id, boolean loop) {
        if (!isEnabled() || !isEnabledMusic()) {
            return 0;
        }
        if (mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
        }
        mPlayer = MediaPlayer.create(Core.getContext(), id);
        if (mPlayer == null) {
            Debug.err(TAG, "Problem with creating player!");
        } else {
            mPlayer.setLooping(loop);
            mPlayer.start();
        }
        return 0;
    }

    public static int stopMusic() {
        if (!isEnabled() || !isEnabledMusic()) {
            return 0;
        }
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
        return 0;
    }

    public static int loadSound(int resId) {
        mNextSoundId++;
        Debug.inf(TAG, "loadSound, id: " + mNextSoundId + ", resId: " + resId);
        mSoundPoolMap.put(Integer.valueOf(mNextSoundId), Integer.valueOf(mSoundPool.load(mContext, resId, 1)));
        return mNextSoundId;
    }

    public static int playSound(int id, int loop) {
        if (!isEnabled() || !isEnabledSound()) {
            return 0;
        }
        Integer stream = mSoundPoolMap.get(Integer.valueOf(id));
        if (stream == null) {
            Debug.err(TAG, "Wrong sound id: " + id);
            return 2;
        }
        float streamVolume = ((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3));
        if (mSoundPool.play(stream.intValue(), streamVolume, streamVolume, 1, loop, 1.0f) != 0) {
            return 0;
        }
        Debug.err(TAG, "Problem with playing stream: " + stream);
        return 1;
    }

    public static int stopSound(int id) {
        if (!isEnabled() || !isEnabledSound()) {
            return 0;
        }
        Integer stream = mSoundPoolMap.get(Integer.valueOf(id));
        if (stream == null) {
            Debug.err(TAG, "Wrong sound id: " + id);
            return 2;
        }
        mSoundPool.stop(stream.intValue());
        return 0;
    }

    public static int pauseSound(int id) {
        if (!isEnabled() || !isEnabledSound()) {
            return 0;
        }
        Integer stream = mSoundPoolMap.get(Integer.valueOf(id));
        if (stream == null) {
            Debug.err(TAG, "Wrong sound id: " + id);
            return 2;
        }
        mSoundPool.pause(stream.intValue());
        return 0;
    }

    public static int resumeSound(int id) {
        if (!isEnabled() || !isEnabledSound()) {
            return 0;
        }
        Integer stream = mSoundPoolMap.get(Integer.valueOf(id));
        if (stream == null) {
            Debug.err(TAG, "Wrong sound id: " + id);
            return 2;
        }
        mSoundPool.resume(stream.intValue());
        return 0;
    }

    public static int setLoop(int id, int loop) {
        if (!isEnabled() || !isEnabledSound()) {
            return 0;
        }
        Integer stream = mSoundPoolMap.get(Integer.valueOf(id));
        if (stream == null) {
            Debug.err(TAG, "Wrong sound id: " + id);
            return 2;
        }
        mSoundPool.setLoop(stream.intValue(), loop);
        return 0;
    }

    public static boolean isEnabled() {
        return mEnabled;
    }

    public static void enable() {
        enableSound();
        enableMusic();
        mEnabled = true;
    }

    public static void disable() {
        disableSound();
        disableMusic();
        mEnabled = false;
    }

    public static void setEnabled(boolean enable) {
        mEnabled = enable;
        setEnabledSound(enable);
        setEnabledMusic(enable);
    }

    public static boolean isEnabledSound() {
        return mEnabledSound;
    }

    public static void enableSound() {
        mEnabledSound = true;
    }

    public static void disableSound() {
        mEnabledSound = false;
    }

    public static void setEnabledSound(boolean enable) {
        mEnabledSound = enable;
    }

    public static boolean isEnabledMusic() {
        return mEnabledMusic;
    }

    public static void enableMusic() {
        mEnabledMusic = true;
    }

    public static void disableMusic() {
        mEnabledMusic = false;
    }

    public static void setEnabledMusic(boolean enable) {
        mEnabledMusic = enable;
    }
}
