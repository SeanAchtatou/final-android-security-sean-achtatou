package com.kasuroid.core.scene;

public class Vector3d {
    public float x;
    public float y;
    public float z;

    public Vector3d() {
        this.z = 0.0f;
        this.y = 0.0f;
        this.x = 0.0f;
    }

    public Vector3d(float x2, float y2, float z2) {
        this.x = x2;
        this.y = y2;
        this.z = z2;
    }
}
