package com.kasuroid.core.scene;

import android.graphics.Paint;
import android.graphics.Typeface;
import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;

public class Text extends SceneNode {
    private static final String TAG = "Text";
    private int mColor;
    private int mSize;
    private int mStrokeColor;
    private boolean mStrokeEnable;
    private int mStrokeWidth;
    private String mText;
    private Typeface mTypeface;

    public Text(String text) {
        super(new Vector3d(0.0f, 0.0f, 0.0f));
        setType(7);
        this.mText = text;
        this.mSize = 12;
        this.mColor = -1;
        Renderer.setTextSize(this.mSize);
        Renderer.getTextBounds(this.mText, this.mBounds);
        this.mStrokeWidth = 0;
        this.mStrokeColor = -16777216;
        this.mStrokeEnable = false;
        this.mTypeface = Typeface.DEFAULT;
    }

    public Text(String text, float x, float y, int size, int color) {
        super(new Vector3d(x, y, 0.0f));
        setType(7);
        setText(text);
        setSize(size);
        this.mColor = color;
        this.mStrokeWidth = 0;
        this.mStrokeColor = -16777216;
        this.mStrokeEnable = false;
        this.mTypeface = Typeface.DEFAULT;
    }

    public void setText(String text) {
        this.mText = text;
        Renderer.setTextSize(this.mSize);
        this.mBounds = Renderer.getTextBounds(this.mText);
    }

    public String getText() {
        return this.mText;
    }

    public void setStrokeEnable(boolean enable) {
        this.mStrokeEnable = enable;
    }

    public void setStrokeWidth(int width) {
        this.mStrokeWidth = width;
    }

    public void setStrokeColor(int color) {
        this.mStrokeColor = color;
    }

    public int getWidth() {
        if (this.mText == null || this.mText.length() <= 0) {
            Debug.inf(TAG, "textWidth: 0");
            return 0;
        }
        Renderer.setTextSize(this.mSize);
        Renderer.setTypeface(this.mTypeface);
        return Renderer.measureText(this.mText);
    }

    public int getHeight() {
        if (this.mText == null || this.mText.length() <= 0) {
            Debug.inf(TAG, "textHeight: 0");
            return 0;
        }
        Renderer.setTextSize(this.mSize);
        Renderer.setTypeface(this.mTypeface);
        Renderer.getTextBounds(this.mText, this.mBounds);
        updateBounds(this.mCoords.x, this.mCoords.y);
        return this.mBounds.height();
    }

    public void setFace(String face) {
    }

    public void setSize(int size) {
        this.mSize = size;
        Renderer.setTextSize(this.mSize);
        this.mBounds = Renderer.getTextBounds(this.mText);
    }

    public int getSize() {
        return this.mSize;
    }

    public void setColor(int color) {
        this.mColor = color;
    }

    public void setTypeface(Typeface typeface) {
        this.mTypeface = typeface;
    }

    public Typeface getTypeface() {
        return this.mTypeface;
    }

    public int getColor() {
        return this.mColor;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        if (!isVisible()) {
            return 0;
        }
        Renderer.setTypeface(this.mTypeface);
        Renderer.setStyle(Paint.Style.FILL);
        Renderer.setTextSize(this.mSize);
        Renderer.setColor(this.mColor);
        Renderer.setAlpha(this.mAlpha);
        Renderer.setStyle(Paint.Style.FILL);
        Renderer.drawText(this.mText, this.mCoords.x, this.mCoords.y);
        if (!this.mStrokeEnable) {
            return 0;
        }
        Renderer.setStyle(Paint.Style.STROKE);
        Renderer.setStrokeWidth(this.mStrokeWidth);
        Renderer.setColor(this.mStrokeColor);
        Renderer.drawText(this.mText, this.mCoords.x, this.mCoords.y);
        return 0;
    }

    public boolean isCollisionPoint(float x, float y) {
        return super.isCollisionPoint(x, y);
    }
}
