package com.kasuroid.core.scene;

import android.graphics.Rect;
import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;
import com.kasuroid.core.Timer;

public class SpriteAnimated extends SceneNode {
    private static final String TAG = SpriteAnimated.class.getSimpleName();
    private int mCurrentFrame;
    private Rect mDstRect;
    private int mFps;
    private int mFrameHeight;
    private int mFramePeriod;
    private long mFrameTime;
    private int mFrameWidth;
    private int mFramesCount;
    private Rect mSrcRect;
    private Texture mTexture;
    private Timer mTimer;

    public SpriteAnimated(int resId, float x, float y, int fps, int framesCount, Timer timer) {
        super(new Vector3d(x, y, 0.0f));
        this.mFramesCount = framesCount - 1;
        if (load(resId) != 0) {
            Debug.err(TAG, "Problem with loading texture!");
        }
        this.mFps = fps;
        this.mTimer = timer;
        Debug.inf(TAG, "width: " + getWidth());
        this.mFrameWidth = getWidth() / (this.mFramesCount + 1);
        Debug.inf(TAG, "frameWidth: " + this.mFrameWidth);
        this.mFrameHeight = getHeight();
        this.mFramePeriod = 1000 / this.mFps;
        Debug.inf(TAG, "frameWidth: " + this.mFrameWidth + ", frameHeight: " + this.mFrameHeight);
        this.mSrcRect = new Rect(0, 0, this.mFrameWidth, this.mFrameHeight);
        this.mDstRect = new Rect(0, 0, this.mFrameWidth, this.mFrameHeight);
        this.mCurrentFrame = 0;
        this.mFrameTime = this.mTimer.getTimeDeltaMillis();
        calcBounds();
    }

    public int load(int resId) {
        if (this.mTexture != null) {
            Debug.warn(TAG, "Going to override existing texture!");
        }
        this.mTexture = TextureManager.load(resId);
        if (this.mTexture == null) {
            Debug.err(TAG, "Problem with loading texture, res: " + resId + "!");
            return 1;
        }
        calcBounds();
        return 0;
    }

    public int onUpdate() {
        if (this.mTimer.getTimeElapsedMillis() > this.mFrameTime + ((long) this.mFramePeriod)) {
            this.mFrameTime = this.mTimer.getTimeElapsedMillis();
            this.mCurrentFrame++;
            if (this.mCurrentFrame > this.mFramesCount) {
                this.mCurrentFrame = 0;
            }
        }
        this.mSrcRect.left = this.mCurrentFrame * this.mFrameWidth;
        this.mSrcRect.right = this.mSrcRect.left + this.mFrameWidth;
        return 0;
    }

    public int onRender() {
        if (!isVisible()) {
            return 0;
        }
        Renderer.setAlpha(this.mAlpha);
        this.mDstRect.left = (int) this.mCoords.x;
        this.mDstRect.top = (int) this.mCoords.y;
        this.mDstRect.right = this.mDstRect.left + this.mFrameWidth;
        this.mDstRect.bottom = this.mDstRect.top + this.mFrameHeight;
        Renderer.drawTexturePart(this.mTexture, this.mSrcRect, this.mDstRect);
        return 0;
    }

    public int getWidth() {
        return this.mTexture.getWidth();
    }

    public int getHeight() {
        return this.mTexture.getHeight();
    }

    public int getFrameWidth() {
        return this.mFrameWidth;
    }

    public int getFrameHeight() {
        return this.mFrameHeight;
    }

    public void reset() {
        this.mCurrentFrame = 0;
        this.mFrameTime = this.mTimer.getTimeDeltaMillis();
    }

    /* access modifiers changed from: protected */
    public void calcBounds() {
        this.mBounds.left = (int) this.mCoords.x;
        this.mBounds.top = (int) this.mCoords.y;
        this.mBounds.right = this.mBounds.left + (getWidth() / this.mFramesCount);
        this.mBounds.bottom = this.mBounds.top + getHeight();
    }
}
