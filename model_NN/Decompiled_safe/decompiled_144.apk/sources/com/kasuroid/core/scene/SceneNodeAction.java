package com.kasuroid.core.scene;

public interface SceneNodeAction {
    public static final int ACTION_FINISHED = 0;

    void notify(SceneNode sceneNode, int i);
}
