package com.kasuroid.core.scene;

import com.kasuroid.core.Debug;
import java.util.Enumeration;
import java.util.Vector;

public class SceneManager {
    private static final String TAG = SceneManager.class.getSimpleName();
    private static final String mName = "SceneManager";
    private static Vector<Scene> mScenes;

    public static int init() {
        mScenes = new Vector<>();
        Debug.inf(TAG, "SceneManager initialized");
        return 0;
    }

    public static int term() {
        Debug.inf(TAG, "SceneManager terminated");
        removeAll();
        return 0;
    }

    public static String getName() {
        return mName;
    }

    public static int update() {
        Enumeration<Scene> e = mScenes.elements();
        while (e.hasMoreElements()) {
            if (e.nextElement().update() != 0) {
                Debug.err(TAG, "Problem with updating the scene!");
                return 1;
            }
        }
        return 0;
    }

    public static int render() {
        Enumeration<Scene> e = mScenes.elements();
        while (e.hasMoreElements()) {
            if (e.nextElement().render() != 0) {
                Debug.err(TAG, "Problem with rendering the scene!");
                return 1;
            }
        }
        return 0;
    }

    public static int addScene(Scene scene) {
        if (scene == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        mScenes.add(scene);
        return 0;
    }

    public static int insertScene(int position, Scene scene) {
        if (scene == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        try {
            mScenes.add(position, scene);
            return 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            Debug.err(TAG, "Bad parameter, e: " + e.toString());
            return 2;
        }
    }

    public static int replaceScene(int position, Scene scene) {
        if (scene == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        try {
            mScenes.set(position, scene);
            return 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            Debug.err(TAG, "Bad parameter, e: " + e.toString());
            return 2;
        }
    }

    public static int removeScene(Scene scene) {
        if (mScenes.remove(scene)) {
            return 0;
        }
        Debug.err(TAG, "Couldn't find scene!");
        return 2;
    }

    public static int removeAll() {
        mScenes.clear();
        return 0;
    }
}
