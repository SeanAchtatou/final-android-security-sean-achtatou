package com.kasuroid.core.scene;

import com.kasuroid.core.Debug;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

public class Scene {
    private static final String TAG = Scene.class.getSimpleName();
    private boolean mEnabled = true;
    protected Vector<SceneNode> mNodes = new Vector<>();
    private boolean mVisible = true;

    public int addNode(SceneNode node) {
        if (node == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        this.mNodes.add(node);
        return 0;
    }

    public int insertNode(int position, SceneNode node) {
        if (node == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        try {
            this.mNodes.add(position, node);
            return 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            Debug.err(TAG, "Bad parameter, e: " + e.toString());
            return 2;
        }
    }

    public int replaceNode(int position, SceneNode node) {
        if (node == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        try {
            this.mNodes.set(position, node);
            return 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            Debug.err(TAG, "Bad parameter, e: " + e.toString());
            return 2;
        }
    }

    public int removeNode(SceneNode node) {
        if (this.mNodes.remove(node)) {
            return 0;
        }
        Debug.err(TAG, "Node not found!");
        return 1;
    }

    public int removeAll() {
        this.mNodes.clear();
        return 0;
    }

    public int getSize() {
        return this.mNodes.size();
    }

    public int update() {
        if (this.mEnabled) {
            Enumeration<SceneNode> e = this.mNodes.elements();
            while (e.hasMoreElements()) {
                SceneNode node = e.nextElement();
                if (node.update() != 0) {
                    Debug.err(TAG, "Problem with updating the nodes!");
                    return 1;
                } else if (node.isFinished()) {
                    removeNode(node);
                }
            }
        }
        return 0;
    }

    public int render() {
        if (this.mVisible) {
            Enumeration<SceneNode> e = this.mNodes.elements();
            while (e.hasMoreElements()) {
                if (e.nextElement().render() != 0) {
                    Debug.err(TAG, "Problem with rendering the nodes!");
                    return 1;
                }
            }
        }
        return 0;
    }

    public boolean areNodesDone() {
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            if (!e.nextElement().isDone()) {
                return false;
            }
        }
        return true;
    }

    public Vector<SceneNode> getNodes() {
        return this.mNodes;
    }

    public void show() {
        this.mVisible = true;
    }

    public void hide() {
        this.mVisible = false;
    }

    public void setVisible(boolean visible) {
        this.mVisible = visible;
    }

    public boolean isVisible() {
        return this.mVisible;
    }

    public void enable() {
        this.mEnabled = true;
    }

    public void disable() {
        this.mEnabled = false;
    }

    public void setEnable(boolean enable) {
        this.mEnabled = enable;
    }

    class ComparerZ implements Comparator<SceneNode> {
        ComparerZ() {
        }

        public int compare(SceneNode node1, SceneNode node2) {
            if (node1.mCoords.z < node2.mCoords.z) {
                return -1;
            }
            if (node1.mCoords.z == node2.mCoords.z) {
                return 0;
            }
            return 1;
        }
    }

    public void sortByZ() {
        Collections.sort(this.mNodes, new ComparerZ());
    }
}
