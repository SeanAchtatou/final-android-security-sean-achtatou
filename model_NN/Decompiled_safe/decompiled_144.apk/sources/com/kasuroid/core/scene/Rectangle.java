package com.kasuroid.core.scene;

import android.graphics.Paint;
import android.graphics.Rect;
import com.kasuroid.core.Renderer;

public class Rectangle extends SceneNode {
    protected int mColor;
    protected Rect mRect;
    protected int mStrokeWidth;
    protected Paint.Style mStyle;

    public Rectangle(Rect rect, int color) {
        this.mCoords.x = (float) rect.left;
        this.mCoords.y = (float) rect.top;
        this.mRect = rect;
        this.mBounds = rect;
        setType(4);
        this.mColor = color;
        this.mStyle = Paint.Style.FILL;
        this.mStrokeWidth = 1;
    }

    public Rectangle(Rect rect) {
        this.mCoords.x = (float) rect.left;
        this.mCoords.y = (float) rect.top;
        this.mRect = rect;
        this.mBounds = rect;
        setType(4);
        this.mColor = -16777216;
        this.mStyle = Paint.Style.FILL;
        this.mStrokeWidth = 1;
    }

    public Rectangle(int left, int top, int width, int height) {
        this(new Rect(left, top, left + width, top + height));
    }

    public int getColor() {
        return this.mColor;
    }

    public void setColor(int color) {
        this.mColor = color;
    }

    public int getLeft() {
        return this.mRect.left;
    }

    public int getRight() {
        return this.mRect.right;
    }

    public int getTop() {
        return this.mRect.top;
    }

    public int getBottom() {
        return this.mRect.bottom;
    }

    public int getWidth() {
        return this.mRect.width();
    }

    public int getHeight() {
        return this.mRect.height();
    }

    public int onRender() {
        Renderer.setStyle(this.mStyle);
        Renderer.setStrokeWidth(this.mStrokeWidth);
        Renderer.setColor(this.mColor);
        Renderer.setAlpha(this.mAlpha);
        Renderer.drawRect(this.mRect);
        return 0;
    }

    public void setRect(Rect rect) {
        this.mRect = rect;
        this.mBounds = rect;
    }

    public Rect getRect() {
        return this.mRect;
    }

    public void setStyle(Paint.Style style) {
        this.mStyle = style;
    }

    public Paint.Style getStyle() {
        return this.mStyle;
    }
}
