package com.kasuroid.core.scene;

import android.graphics.Rect;
import com.kasuroid.core.Debug;

public class SceneNode {
    protected static final int MAX_MODIFIERS = 3;
    public static final int NODE_TYPE_CIRCLE = 3;
    public static final int NODE_TYPE_LINE = 1;
    public static final int NODE_TYPE_POINT = 2;
    public static final int NODE_TYPE_RECT = 4;
    public static final int NODE_TYPE_SPRITE = 5;
    public static final int NODE_TYPE_SPRITE_ANIMATED = 6;
    public static final int NODE_TYPE_TEXT = 7;
    public static final int NODE_TYPE_UKNOWN = 0;
    private static final String TAG = "SceneNode";
    private SceneNodeAction mActionCallback;
    protected int mAlpha;
    protected Rect mBounds;
    protected Vector3d mCoords;
    protected boolean mDone;
    protected boolean mFinished;
    protected Modifier[] mModifiers;
    protected int mNumOfModifiers;
    private int mType;
    protected boolean mVisible;

    public SceneNode() {
        this.mModifiers = new Modifier[3];
        this.mCoords = new Vector3d();
        this.mType = 0;
        this.mVisible = true;
        this.mAlpha = 255;
        this.mBounds = new Rect();
        this.mNumOfModifiers = 0;
        this.mFinished = false;
        this.mDone = false;
    }

    public SceneNode(Vector3d coords) {
        this.mModifiers = new Modifier[3];
        this.mCoords = coords;
        this.mType = 0;
        this.mVisible = true;
        this.mAlpha = 255;
        this.mBounds = new Rect();
        this.mFinished = false;
        this.mDone = false;
    }

    public final int getType() {
        return this.mType;
    }

    public final void done() {
        this.mDone = true;
    }

    public final void notDone() {
        this.mDone = false;
    }

    public final boolean isDone() {
        return this.mDone;
    }

    public int attach(SceneNode node) {
        Debug.inf(TAG, "attach, currently disabled.");
        return 2;
    }

    public final int clear() {
        Debug.inf(TAG, "clear, currently disabled.");
        return 0;
    }

    public final int update() {
        if (this.mNumOfModifiers > 0) {
            for (int i = 0; i < 3; i++) {
                if (this.mModifiers[i] != null) {
                    this.mModifiers[i].update(this);
                }
            }
        }
        return onUpdate();
    }

    public final int render() {
        return onRender();
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        return 0;
    }

    public int addModifier(Modifier modifier) {
        if (modifier == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        } else if (this.mNumOfModifiers >= 3) {
            Debug.err(TAG, "Modifiers pool full!");
            return 1;
        } else {
            int i = 0;
            while (true) {
                if (i >= 3) {
                    break;
                } else if (this.mModifiers[i] == null) {
                    this.mModifiers[i] = modifier;
                    this.mNumOfModifiers++;
                    break;
                } else {
                    i++;
                }
            }
            return 0;
        }
    }

    public int removeModifier(Modifier modifier) {
        if (modifier == null) {
            Debug.err(TAG, "Bad parameter!");
            return 2;
        }
        int i = 0;
        while (true) {
            if (i >= 3) {
                break;
            } else if (this.mModifiers[i] == modifier) {
                this.mModifiers[i] = null;
                this.mNumOfModifiers--;
                break;
            } else {
                i++;
            }
        }
        return 0;
    }

    public final Vector3d getCoords() {
        return this.mCoords;
    }

    public void setCoords(Vector3d coords) {
        this.mCoords = coords;
        calcBounds();
    }

    public void setCoordsX(float x) {
        this.mCoords.x = x;
        calcBounds();
    }

    public final float getCoordsX() {
        return this.mCoords.x;
    }

    public void setCoordsY(float y) {
        this.mCoords.y = y;
        calcBounds();
    }

    public final float getCoordsY() {
        return this.mCoords.y;
    }

    public void setCoordsZ(float z) {
        this.mCoords.z = z;
    }

    public final float getCoordsZ() {
        return this.mCoords.z;
    }

    public void setCoordsXY(float x, float y) {
        this.mCoords.x = x;
        this.mCoords.y = y;
        calcBounds();
    }

    public void updateCoords(Vector3d vector) {
        this.mCoords.x += vector.x;
        this.mCoords.y += vector.y;
        this.mCoords.z += vector.z;
        updateBounds(vector.x, vector.y);
    }

    public void updateCoordsXY(float x, float y) {
        this.mCoords.x += x;
        this.mCoords.y += y;
        updateBounds(x, y);
    }

    public boolean isCollisionPoint(float x, float y) {
        if (x < ((float) this.mBounds.left) || x > ((float) this.mBounds.right) || y < ((float) this.mBounds.top) || y > ((float) this.mBounds.bottom)) {
            return false;
        }
        return true;
    }

    public boolean isCollisionRect(Rect rect) {
        if (isCollisionPoint((float) rect.left, (float) rect.top)) {
            return true;
        }
        if (isCollisionPoint((float) rect.right, (float) rect.top)) {
            return true;
        }
        if (isCollisionPoint((float) rect.left, (float) rect.bottom)) {
            return true;
        }
        if (isCollisionPoint((float) rect.right, (float) rect.bottom)) {
            return true;
        }
        return false;
    }

    public final boolean isVisible() {
        return this.mVisible;
    }

    public final void hide() {
        this.mVisible = false;
    }

    public final void show() {
        this.mVisible = true;
    }

    public final void setVisible(boolean visible) {
        this.mVisible = visible;
    }

    public final void setAlpha(int alpha) {
        this.mAlpha = alpha;
    }

    public final int getAlpha() {
        return this.mAlpha;
    }

    public Rect getBounds() {
        return this.mBounds;
    }

    public void setBounds(Rect bounds) {
        this.mBounds = bounds;
    }

    public void updateBounds(float x, float y) {
        this.mBounds.left += (int) x;
        this.mBounds.top += (int) y;
        this.mBounds.right += (int) x;
        this.mBounds.bottom += (int) y;
    }

    public int getWidth() {
        return 0;
    }

    public int getHeight() {
        return 0;
    }

    public void setFinish(boolean finish) {
        this.mFinished = finish;
        if (this.mActionCallback != null) {
            this.mActionCallback.notify(this, 0);
        }
    }

    public boolean isFinished() {
        return this.mFinished;
    }

    public void setActionCallback(SceneNodeAction callback) {
        this.mActionCallback = callback;
    }

    /* access modifiers changed from: protected */
    public final void setType(int type) {
        this.mType = type;
    }

    /* access modifiers changed from: protected */
    public void calcBounds() {
        this.mBounds.left = (int) this.mCoords.x;
        this.mBounds.top = (int) this.mCoords.y;
        this.mBounds.right = this.mBounds.left + getWidth();
        this.mBounds.bottom = this.mBounds.top + getHeight();
    }
}
