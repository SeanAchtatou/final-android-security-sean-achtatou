package com.kasuroid.core.scene;

import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;
import com.kasuroid.core.Timer;

public class SpriteFramed extends SceneNode {
    private static final String TAG = SpriteFramed.class.getSimpleName();
    private int mCurrentFrame;
    private Texture mCurrentTexture;
    private int mFps;
    private int mFrameHeight;
    private int mFramePeriod;
    private long mFrameTime;
    private int mFrameWidth;
    private int mFramesCount;
    private Texture[] mTextureFrames;
    private Timer mTimer;

    public SpriteFramed(int[] framesIds, float x, float y, int fps, int framesCount, Timer timer) {
        super(new Vector3d(x, y, 0.0f));
        if (loadFrames(framesIds) != 0) {
            Debug.err(TAG, "Problem with loading frames!");
            return;
        }
        this.mFramesCount = framesCount - 1;
        if (this.mFramesCount > framesIds.length) {
            Debug.inf(TAG, "Wrong frames count, setting it to: " + framesIds.length);
            this.mFramesCount = framesIds.length;
        }
        this.mFps = fps;
        this.mTimer = timer;
        this.mFramePeriod = 1000 / this.mFps;
        this.mCurrentFrame = 0;
        this.mFrameTime = this.mTimer.getTimeDeltaMillis();
        calcBounds();
    }

    /* access modifiers changed from: protected */
    public int loadFrames(int[] ids) {
        if (ids == null || ids.length == 0) {
            Debug.err(TAG, "Wrong parameter!");
            return 2;
        }
        this.mTextureFrames = new Texture[ids.length];
        for (int i = 0; i < ids.length; i++) {
            this.mTextureFrames[i] = TextureManager.load(ids[i]);
            if (this.mTextureFrames[i] == null) {
                Debug.err(TAG, "Problem with loading texture, res: " + ids[i] + "!");
                return 1;
            }
        }
        this.mCurrentTexture = this.mTextureFrames[0];
        calcBounds();
        return 0;
    }

    public int onUpdate() {
        if (this.mTimer.getTimeElapsedMillis() > this.mFrameTime + ((long) this.mFramePeriod)) {
            this.mFrameTime = this.mTimer.getTimeElapsedMillis();
            this.mCurrentFrame++;
            if (this.mCurrentFrame > this.mFramesCount) {
                this.mCurrentFrame = 0;
            }
            this.mCurrentTexture = this.mTextureFrames[this.mCurrentFrame];
        }
        return 0;
    }

    public int onRender() {
        if (!isVisible()) {
            return 0;
        }
        Renderer.setAlpha(this.mAlpha);
        Renderer.drawTexture(this.mCurrentTexture, this.mCoords.x, this.mCoords.y);
        return 0;
    }

    public int getWidth() {
        return this.mCurrentTexture.getWidth();
    }

    public int getHeight() {
        return this.mCurrentTexture.getHeight();
    }

    public int getFrameWidth() {
        return this.mFrameWidth;
    }

    public int getFrameHeight() {
        return this.mFrameHeight;
    }

    public void reset() {
        this.mCurrentFrame = 0;
        this.mFrameTime = this.mTimer.getTimeDeltaMillis();
    }

    /* access modifiers changed from: protected */
    public void calcBounds() {
        this.mBounds.left = (int) this.mCoords.x;
        this.mBounds.top = (int) this.mCoords.y;
        this.mBounds.right = this.mBounds.left + getWidth();
        this.mBounds.bottom = this.mBounds.top + getHeight();
    }
}
