package com.kasuroid.core.scene;

public interface Modifier {
    int start();

    int stop();

    int update(SceneNode sceneNode);
}
