package com.kasuroid.core.scene;

import com.kasuroid.core.Debug;
import com.kasuroid.core.Renderer;
import com.kasuroid.core.Texture;
import com.kasuroid.core.TextureManager;

public class Sprite extends SceneNode {
    private static final String TAG = "Sprite";
    protected Texture mTexture;

    public Sprite(float x, float y) {
        super(new Vector3d(x, y, 0.0f));
    }

    public Sprite(int resId, float x, float y) {
        super(new Vector3d(x, y, 0.0f));
        if (load(resId) != 0) {
            Debug.err(TAG, "Problem with loading texture!");
        } else {
            calcBounds();
        }
    }

    public int load(int resId) {
        if (this.mTexture != null) {
            Debug.warn(TAG, "Going to override existing texture!");
        }
        this.mTexture = TextureManager.load(resId);
        if (this.mTexture == null) {
            Debug.err(TAG, "Problem with loading texture, res: " + resId + "!");
            return 1;
        }
        calcBounds();
        return 0;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        if (!isVisible()) {
            return 0;
        }
        Renderer.setAlpha(this.mAlpha);
        Renderer.drawTexture(this.mTexture, this.mCoords.x, this.mCoords.y);
        return 0;
    }

    public void scale(int width, int height) {
        if (this.mTexture != null) {
            this.mTexture.scale(width, height);
            calcBounds();
        }
    }

    public void scaleNew(int width, int height) {
        if (this.mTexture != null) {
            this.mTexture = this.mTexture.scaleToTexture(width, height);
            calcBounds();
        }
    }

    public void setTexture(Texture texture) {
        this.mTexture = texture;
    }

    public Texture getTexture() {
        return this.mTexture;
    }

    public int getWidth() {
        return this.mTexture.getWidth();
    }

    public int getHeight() {
        return this.mTexture.getHeight();
    }
}
