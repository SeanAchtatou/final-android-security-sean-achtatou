package com.kasuroid.core.scene;

public class Vector2d {
    public float x;
    public float y;

    public Vector2d() {
        this.y = 0.0f;
        this.x = 0.0f;
    }

    public Vector2d(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }
}
