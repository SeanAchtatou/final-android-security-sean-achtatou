package com.kasuroid.core.scene;

import android.graphics.Rect;
import com.kasuroid.core.Renderer;

public class Circle extends SceneNode {
    protected int mColor;
    protected float mRadius;

    public Circle(Vector3d center, float radius, int color) {
        super(center);
        setType(3);
        this.mColor = color;
        this.mRadius = radius;
        this.mBounds = new Rect((int) (center.x - radius), (int) (center.y - radius), (int) (center.x + radius), (int) (center.y + radius));
    }

    public int getColor() {
        return this.mColor;
    }

    public void setColor(int color) {
        this.mColor = color;
    }

    public float getRadius() {
        return this.mRadius;
    }

    public void setRadius(float radius) {
        this.mRadius = radius;
    }

    public int onRender() {
        Renderer.setColor(this.mColor);
        Renderer.setAlpha(this.mAlpha);
        Renderer.drawCircle(this.mCoords.x, this.mCoords.y, this.mRadius);
        return 0;
    }

    public boolean isCollisionPoint(float x, float y) {
        return this.mRadius * this.mRadius >= ((this.mCoords.x - x) * (this.mCoords.x - x)) + ((this.mCoords.y - y) * (this.mCoords.y - y));
    }

    public boolean isCollisionRect(Rect rect) {
        if (isCollisionPoint((float) rect.left, (float) rect.top)) {
            return true;
        }
        if (isCollisionPoint((float) rect.right, (float) rect.top)) {
            return true;
        }
        if (isCollisionPoint((float) rect.left, (float) rect.bottom)) {
            return true;
        }
        if (isCollisionPoint((float) rect.right, (float) rect.bottom)) {
            return true;
        }
        return false;
    }

    public int getWidth() {
        return (int) (2.0f * this.mRadius);
    }

    public int getHeight() {
        return (int) (2.0f * this.mRadius);
    }
}
