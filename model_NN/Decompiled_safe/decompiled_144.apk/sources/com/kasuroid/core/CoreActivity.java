package com.kasuroid.core;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class CoreActivity extends Activity {
    private static final String TAG = CoreActivity.class.getSimpleName();
    protected AdViewCtrl mAdCtrl;
    protected CoreView mCoreView;
    protected boolean mIsQuiting = false;
    protected Handler mMainHandler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        onMainCreate();
        this.mIsQuiting = false;
        this.mMainHandler = new Handler() {
            public void handleMessage(Message msg) {
                String strMsg = (String) msg.obj;
                Debug.inf(getClass().getName(), "Message: " + strMsg);
                if (strMsg.equalsIgnoreCase("showAd")) {
                    CoreActivity.this.showAd();
                } else if (strMsg.equalsIgnoreCase("hideAd")) {
                    CoreActivity.this.hideAd();
                } else if (strMsg.equalsIgnoreCase("showHelp")) {
                    CoreActivity.this.showModalDialog();
                } else {
                    CoreActivity.this.onMessage(strMsg);
                }
            }
        };
        if (Core.getState() == 0) {
            Debug.inf(TAG, "onCreate - first time, creating activity!");
            if (Core.init(this.mCoreView.getHolder(), this) != 0) {
                Debug.err(TAG, "Problem with initializing the core!");
            }
            onInit();
            return;
        }
        Debug.inf(TAG, "Core already initialized, initializing surface and context!");
        Core.setSurfaceHolder(this.mCoreView.getHolder());
        Core.setContext(this);
    }

    public void showModalDialog() {
    }

    public void sendMessage(Message msg) {
        this.mMainHandler.sendMessage(msg);
    }

    /* access modifiers changed from: protected */
    public void onMessage(String msg) {
        Debug.inf(TAG, "Received message: " + msg);
    }

    public void onDestroy() {
        super.onDestroy();
        Debug.inf(TAG, "onDestroy");
        if (this.mIsQuiting) {
            Debug.inf(TAG, "onDestroy - activity finished!");
            if (Core.stop() != 0) {
                Debug.err(TAG, "Couldn't stop the core!");
            }
            if (Core.term() != 0) {
                Debug.err(TAG, "Couldn't terminate the core!");
            }
            onTerm();
            return;
        }
        Debug.inf(TAG, "onDestroy - activity is just rotating?");
    }

    public void ready() {
        onReady();
    }

    public void showAd() {
        if (this.mAdCtrl != null) {
            Debug.inf(getClass().getName(), "Showing ad!");
            this.mAdCtrl.showAd();
        }
    }

    public void hideAd() {
        if (this.mAdCtrl != null) {
            Debug.inf(getClass().getName(), "Hiding ad!");
            this.mAdCtrl.hideAd();
        }
    }

    /* access modifiers changed from: protected */
    public void setAdCtrl(AdViewCtrl ctrl) {
        this.mAdCtrl = ctrl;
    }

    /* access modifiers changed from: protected */
    public void enableTestAd() {
        if (this.mAdCtrl != null) {
            this.mAdCtrl.enableTestAds();
        }
    }

    /* access modifiers changed from: protected */
    public void setCoreView(CoreView view) {
        this.mCoreView = view;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Debug.inf(TAG, "onSaveInstanceState()");
    }

    /* access modifiers changed from: protected */
    public void onMainCreate() {
        Debug.inf(TAG, "onMainCreate");
    }

    /* access modifiers changed from: protected */
    public void onInit() {
        Debug.inf(TAG, "onInit");
    }

    /* access modifiers changed from: protected */
    public void onTerm() {
        Debug.inf(TAG, "onTerm");
    }

    /* access modifiers changed from: protected */
    public void onReady() {
        Debug.inf(TAG, "onReady");
    }

    public void onPause() {
        super.onPause();
        Core.pause();
        Debug.inf(TAG, "onPause");
    }

    public void onResume() {
        super.onResume();
        Core.resume();
        Debug.inf(TAG, "onResume");
    }

    public void onStart() {
        super.onStart();
        Debug.inf(TAG, "onStart");
    }

    public void onStop() {
        super.onStop();
        Debug.inf(TAG, "onStop");
    }

    public void quit() {
        this.mIsQuiting = true;
        super.finish();
    }

    public int getAdHeight() {
        if (this.mAdCtrl != null) {
            return this.mAdCtrl.getAdHeight();
        }
        return 0;
    }
}
