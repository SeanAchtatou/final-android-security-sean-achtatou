package com.kasuroid.core;

public interface MenuHandler {
    void onDown();

    void onMove();

    void onUp();
}
