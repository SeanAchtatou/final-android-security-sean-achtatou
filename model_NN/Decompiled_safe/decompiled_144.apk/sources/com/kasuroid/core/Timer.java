package com.kasuroid.core;

public class Timer {
    private static int mNextId = 0;
    private static final float mTimerFrameLength = 1000.0f;
    private long mCurrentFrame = 0;
    private long mDelta;
    private float mDeltaSecs = 0.0f;
    private int mId = mNextId;
    private long mLastFrame = 0;
    private boolean mPaused = false;
    private boolean mStarted = false;
    private long mTimeElapsed = 0;

    public Timer() {
        mNextId++;
        Debug.inf(getClass().getName(), "Timer with id: " + this.mId + " created!");
    }

    public int start() {
        if (!isStarted()) {
            long currentTimeMillis = System.currentTimeMillis();
            this.mLastFrame = currentTimeMillis;
            this.mCurrentFrame = currentTimeMillis;
            this.mTimeElapsed = 0;
            this.mDelta = 0;
            this.mDeltaSecs = 0.0f;
            this.mStarted = true;
            this.mPaused = false;
            Debug.inf(getClass().getName(), "Timer with id: " + this.mId + " started.");
            return 0;
        }
        Debug.warn(getClass().getName(), "Timer already started!");
        return 0;
    }

    public int stop() {
        this.mCurrentFrame = 0;
        this.mLastFrame = 0;
        this.mDelta = 0;
        this.mDeltaSecs = 0.0f;
        this.mTimeElapsed = 0;
        this.mPaused = false;
        this.mStarted = false;
        Debug.inf(getClass().getName(), "Timer stopped.");
        return 0;
    }

    public int pause() {
        if (!isPaused() && isStarted()) {
            this.mPaused = true;
            this.mDelta = 0;
            this.mDeltaSecs = 0.0f;
            Debug.inf(getClass().getName(), "Timer paused.");
            return 0;
        } else if (isPaused()) {
            Debug.warn(getClass().getName(), "Timer already paused");
            return 0;
        } else {
            Debug.err(getClass().getName(), "Timer must be started!");
            return 1;
        }
    }

    public int resume() {
        if (isPaused() && isStarted()) {
            this.mCurrentFrame = System.currentTimeMillis();
            this.mPaused = false;
            Debug.inf(getClass().getName(), "Timer resumed.");
            return 0;
        } else if (isStarted()) {
            Debug.warn(getClass().getName(), "Timer alredy running");
            return 0;
        } else {
            Debug.err(getClass().getName(), "Can't resume not paused timer!");
            return 1;
        }
    }

    public int update() {
        if (!isStarted() || isPaused()) {
            return 0;
        }
        this.mLastFrame = this.mCurrentFrame;
        this.mCurrentFrame = System.currentTimeMillis();
        this.mDelta = this.mCurrentFrame - this.mLastFrame;
        if (this.mDelta < 0) {
            this.mDelta = 0;
        }
        this.mTimeElapsed += this.mDelta;
        this.mDeltaSecs = ((float) this.mDelta) / mTimerFrameLength;
        return 0;
    }

    public boolean isStarted() {
        return this.mStarted;
    }

    public boolean isPaused() {
        return this.mPaused;
    }

    public int getId() {
        return this.mId;
    }

    public long getTimeElapsedMillis() {
        return this.mTimeElapsed;
    }

    public float getTimeElapsedSecs() {
        return ((float) this.mTimeElapsed) / mTimerFrameLength;
    }

    public float getTimeDeltaSecs() {
        return this.mDeltaSecs;
    }

    public long getTimeDeltaMillis() {
        return this.mDelta;
    }
}
