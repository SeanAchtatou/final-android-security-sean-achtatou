package com.kasuroid.core;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

public abstract class UnscaledBitmapLoader {
    public static final UnscaledBitmapLoader instance;

    /* access modifiers changed from: package-private */
    public abstract Bitmap load(Resources resources, int i, BitmapFactory.Options options);

    static {
        UnscaledBitmapLoader unscaledBitmapLoader;
        if (Integer.parseInt(Build.VERSION.SDK) < 4) {
            unscaledBitmapLoader = new Old(null);
        } else {
            unscaledBitmapLoader = new New(null);
        }
        instance = unscaledBitmapLoader;
    }

    public static Bitmap loadFromResource(Resources resources, int resId, BitmapFactory.Options options) {
        return instance.load(resources, resId, options);
    }

    private static class Old extends UnscaledBitmapLoader {
        private Old() {
        }

        /* synthetic */ Old(Old old) {
            this();
        }

        /* access modifiers changed from: package-private */
        public Bitmap load(Resources resources, int resId, BitmapFactory.Options options) {
            return BitmapFactory.decodeResource(resources, resId, options);
        }
    }

    private static class New extends UnscaledBitmapLoader {
        private New() {
        }

        /* synthetic */ New(New newR) {
            this();
        }

        /* access modifiers changed from: package-private */
        public Bitmap load(Resources resources, int resId, BitmapFactory.Options options) {
            if (options == null) {
                options = new BitmapFactory.Options();
            }
            options.inScaled = false;
            return BitmapFactory.decodeResource(resources, resId, options);
        }
    }
}
