package com.kasuroid.core;

import android.view.KeyEvent;

public class GameState implements InputListener {
    private static final String TAG = GameState.class.getSimpleName();
    protected int mState = 0;

    public final int init() {
        Debug.inf(getClass().getName(), "init()");
        if (this.mState != 0) {
            Debug.err(TAG, "Wrong state: " + StateCode.toString(this.mState));
            return 1;
        }
        int ret = onInit();
        if (ret != 0) {
            Debug.err(TAG, "Problem with initialization!");
            return ret;
        }
        this.mState = 1;
        this.mState = 2;
        return 0;
    }

    public final int term() {
        if (this.mState == 0) {
            Debug.warn(TAG, "Game state already terminated!");
            return 0;
        }
        int ret = onTerm();
        if (ret != 0) {
            Debug.err(TAG, "Problem with termination!");
            return ret;
        }
        this.mState = 0;
        return 0;
    }

    public final int pause() {
        if (this.mState == 4) {
            Debug.warn(TAG, "Already paused");
            return 0;
        } else if (this.mState != 2) {
            Debug.err(TAG, "Can't pause not running state: " + StateCode.toString(this.mState));
            return 1;
        } else {
            int ret = onPause();
            if (ret != 0) {
                Debug.err(TAG, "Problem with pausing the state!");
                return ret;
            }
            this.mState = 4;
            return 0;
        }
    }

    public final int resume() {
        if (this.mState == 2) {
            Debug.warn(TAG, "Already running");
            return 0;
        } else if (this.mState != 4) {
            Debug.err(TAG, "Can't resume not paused state: " + StateCode.toString(this.mState));
            return 1;
        } else {
            int ret = onResume();
            if (ret != 0) {
                Debug.err(TAG, "Problem with resuming the state!");
                return ret;
            }
            this.mState = 2;
            return 0;
        }
    }

    public final int update() {
        if (this.mState == 4) {
            return 0;
        }
        if (this.mState == 2) {
            return onUpdate();
        }
        Debug.err(TAG, "Wrong state: " + StateCode.toString(this.mState));
        return 1;
    }

    public final int render() {
        if (this.mState == 2 || this.mState == 4) {
            return onRender();
        }
        Debug.err(TAG, "Wrong state: " + StateCode.toString(this.mState));
        return 1;
    }

    public int onInit() {
        return 0;
    }

    public int onTerm() {
        return 0;
    }

    public int onUpdate() {
        return 0;
    }

    public int onRender() {
        return 0;
    }

    public int onPause() {
        return 0;
    }

    public int onResume() {
        return 0;
    }

    public int changeState(GameState gameState) {
        int ret = Core.changeState(gameState);
        if (ret != 0) {
            Debug.err(TAG, "Problem with changing the state!");
        }
        return ret;
    }

    public int pushState(GameState gameState) {
        int ret = Core.pushState(gameState);
        if (ret != 0) {
            Debug.err(TAG, "Problem with pushing the state to the stack!");
        }
        return ret;
    }

    public int popState() {
        int ret = Core.popState();
        if (ret != 0) {
            Debug.err(TAG, "Problem with popping the state from the stack!");
        }
        return ret;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return false;
    }

    public boolean onTouchEvent(InputEvent event) {
        return false;
    }

    public int onScreenCreated() {
        return 0;
    }

    public int onScreenDestroyed() {
        return 0;
    }

    public int onScreenChanged(int width, int height) {
        return 0;
    }

    public int getState() {
        return this.mState;
    }
}
