package com.kasuroid.core;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.SurfaceHolder;

public class Renderer {
    private static final String TAG = "Renderer";
    private static Canvas mCanvas = null;
    private static int mHeight = 0;
    private static final String mName = "Renderer";
    private static Paint mPaint = null;
    private static float mScaleFactor;
    private static SurfaceHolder mSurface = null;
    private static int mWidth;

    public static int init() {
        Debug.inf("Renderer", "Renderer initialized");
        mPaint = new Paint();
        return 0;
    }

    public static int term() {
        Debug.inf("Renderer", "Renderer terminated");
        return 0;
    }

    public static String getName() {
        return "Renderer";
    }

    public static void setSurfaceHolder(SurfaceHolder surface) {
        mSurface = surface;
    }

    public static void screenCreated(SurfaceHolder surface) {
        try {
            mSurface = surface;
            lock();
            mWidth = mCanvas.getWidth();
            mHeight = mCanvas.getHeight();
            mScaleFactor = ((float) mWidth) / 320.0f;
        } finally {
            unlock();
        }
    }

    public static void screenDestroyed() {
    }

    public static void screenChanged(SurfaceHolder surface, int width, int height) {
        try {
            mSurface = surface;
            lock();
            mWidth = mCanvas.getWidth();
            mHeight = mCanvas.getHeight();
            mScaleFactor = ((float) mWidth) / 320.0f;
        } finally {
            unlock();
        }
    }

    public static void lock() {
        mCanvas = mSurface.lockCanvas(null);
    }

    public static void unlock() {
        if (mCanvas != null) {
            mSurface.unlockCanvasAndPost(mCanvas);
        }
    }

    public static Canvas getCanvas() {
        return mCanvas;
    }

    public static Paint getPaint() {
        return mPaint;
    }

    public static int getWidth() {
        return mWidth;
    }

    public static int getHeight() {
        return mHeight;
    }

    public static float getScaleFactor() {
        return mScaleFactor;
    }

    public static void drawBitmapScale(Bitmap bitmap, Rect src, Rect dst) {
        mCanvas.drawBitmap(bitmap, src, dst, mPaint);
    }

    public static void drawBitmap(Bitmap bitmap, float x, float y) {
        mCanvas.drawBitmap(bitmap, x, y, mPaint);
    }

    public static void drawTexture(Texture texture, float x, float y) {
        mCanvas.drawBitmap(texture.getBitmap(), x, y, mPaint);
    }

    public static void drawTextureMtx(Texture texture, Matrix mtx) {
        mCanvas.drawBitmap(texture.getBitmap(), mtx, mPaint);
    }

    public static void drawTexturePart(Texture texture, Rect srcRect, Rect destRect) {
        mCanvas.drawBitmap(texture.getBitmap(), srcRect, destRect, mPaint);
    }

    public static void drawTextureSliceH(Texture slice, int repeats) {
        int x = 0;
        for (int i = 0; i < repeats; i++) {
            drawBitmap(slice.getBitmap(), (float) x, 0.0f);
            x += slice.getWidth();
        }
    }

    public static void drawRect(Rect rect) {
        mCanvas.drawRect(rect, mPaint);
    }

    public static void drawRect(float left, float top, float right, float bottom) {
        mCanvas.drawRect(left, top, right, bottom, mPaint);
    }

    public static void drawText(String text, float x, float y) {
        mCanvas.drawText(text, x, y, mPaint);
    }

    public static void drawCircle(float x, float y, float radius) {
        mCanvas.drawCircle(x, y, radius, mPaint);
    }

    public static void drawLine(float x1, float y1, float x2, float y2) {
        mCanvas.drawLine(x1, y1, x2, y2, mPaint);
    }

    public static void setColor(int color) {
        mPaint.setColor(color);
    }

    public static void setTextSize(int size) {
        mPaint.setTextSize((float) size);
    }

    public static void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    public static void clearScreen(int color) {
        mCanvas.drawColor(color);
    }

    public static void setAntiAlias(boolean flag) {
        mPaint.setAntiAlias(flag);
    }

    public static Rect getTextBounds(String text) {
        Rect bounds = new Rect();
        mPaint.getTextBounds(text, 0, text.length(), bounds);
        return bounds;
    }

    public static void getTextBounds(String text, Rect bounds) {
        mPaint.getTextBounds(text, 0, text.length(), bounds);
    }

    public static int measureText(String text) {
        return (int) mPaint.measureText(text);
    }

    public static void setStyle(Paint.Style style) {
        mPaint.setStyle(style);
    }

    public static void setStrokeWidth(int width) {
        mPaint.setStrokeWidth((float) width);
    }

    public static void setTypeface(Typeface typeface) {
        mPaint.setTypeface(typeface);
    }

    public static Typeface loadTtfFont(String fontName) {
        return Typeface.createFromAsset(Core.getContext().getAssets(), "fonts/" + fontName);
    }

    public static void drawPoint(float x, float y) {
        mCanvas.drawPoint(x, y, mPaint);
    }
}
