package com.kasuroid.core;

public class StateCode {
    public static final int INITIALIZED = 1;
    public static final int PAUSED = 4;
    public static final int RUNNING = 2;
    public static final int STOPPED = 3;
    public static final int TERMINATED = 0;

    public static String toString(int code) {
        switch (code) {
            case 0:
                return "TERMINATED";
            case 1:
                return "INITIALIZED";
            case 2:
                return "RUNNING";
            case 3:
                return "STOPPED";
            case 4:
                return "PAUSED";
            default:
                return "UKNOWN";
        }
    }
}
