package com.kasuroid.core;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.concurrent.ArrayBlockingQueue;

public class CoreView extends SurfaceView implements SurfaceHolder.Callback {
    private static final int INPUT_QUEUE_SIZE = 30;
    private static final String TAG = "CoreView";
    private ArrayBlockingQueue<InputEvent> mInputObjectPool;

    public CoreView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setFocusable(true);
        setFocusableInTouchMode(true);
        Debug.inf(TAG, "Creating input object pool");
        createInputObjectPool();
    }

    private void createInputObjectPool() {
        this.mInputObjectPool = new ArrayBlockingQueue<>(INPUT_QUEUE_SIZE);
        for (int i = 0; i < INPUT_QUEUE_SIZE; i++) {
            this.mInputObjectPool.add(new InputEvent(this.mInputObjectPool));
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Debug.inf(getClass().getName(), "surfaceChanged");
        Core.screenChanged(holder, width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Debug.inf(getClass().getName(), "surfaceCreated");
        Core.screenCreated(holder);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Debug.inf(getClass().getName(), "surfaceDestroyed");
        Core.screenDestroyed(holder);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return Core.keyDown(keyCode, msg);
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return Core.keyUp(keyCode, msg);
    }

    public boolean onTouchEvent(MotionEvent event) {
        try {
            InputEvent input = this.mInputObjectPool.take();
            input.useEvent(event);
            Core.feedInput(input);
        } catch (InterruptedException e) {
            Debug.err(TAG, "Something went wrong!");
        }
        try {
            Thread.sleep(40);
            return true;
        } catch (InterruptedException e2) {
            Debug.err(TAG, "Something went wrong with sleep!");
            return true;
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        Core.focusChanged(hasWindowFocus);
    }
}
