package com.kasuroid.core;

import com.kasuroid.core.scene.Scene;
import com.kasuroid.core.scene.SceneNode;
import java.util.Enumeration;

public class Menu extends Scene {
    private static final int DEF_ITEMS_HORIZ_OFFSET = 20;
    private static final int DEF_ITEMS_VERT_OFFSET = 20;
    public static final int POSITION_CENTER_BOTTOM = 7;
    public static final int POSITION_CENTER_CENTER = 4;
    public static final int POSITION_CENTER_TOP = 1;
    public static final int POSITION_CUSTOM = 9;
    public static final int POSITION_LEFT_BOTTOM = 6;
    public static final int POSITION_LEFT_CENTER = 3;
    public static final int POSITION_LEFT_TOP = 0;
    public static final int POSITION_RIGHT_BOTTOM = 8;
    public static final int POSITION_RIGHT_CENTER = 5;
    public static final int POSITION_RIGHT_TOP = 2;
    public static final int SHAPE_CUSTOM = 2;
    public static final int SHAPE_HORIZONTAL = 1;
    public static final int SHAPE_VERTICAL = 0;
    private static final String TAG = Menu.class.getSimpleName();
    private int mHorizontalItemsOffset = 20;
    private float mOffsetX = 0.0f;
    private float mOffsetY = 0.0f;
    private int mPosType = 0;
    private int mShapeType = 0;
    private SceneNode mTitle = null;
    private float mTitleOffsetX = 20.0f;
    private float mTitleOffsetY = 20.0f;
    private int mVerticalItemsOffset = 20;

    public int addItem(MenuItem item) {
        int ret = addNode(item);
        if (ret != 0) {
            Debug.err(TAG, "Problem with adding item!");
        } else {
            recalc();
        }
        return ret;
    }

    public int insertItem(int position, MenuItem item) {
        int ret = insertNode(position, item);
        if (ret != 0) {
            Debug.err(TAG, "Problem with inserting item!");
        } else {
            recalc();
        }
        return ret;
    }

    public int replaceItem(int position, MenuItem item) {
        int ret = replaceNode(position, item);
        if (ret != 0) {
            Debug.err(TAG, "Problem with inserting item!");
        } else {
            recalc();
        }
        return ret;
    }

    public int removeItem(MenuItem item) {
        int ret = removeNode(item);
        if (ret != 0) {
            Debug.err(TAG, "Problem with removing item!");
        } else {
            recalc();
        }
        return ret;
    }

    public void setTitle(SceneNode title) {
        this.mTitle = title;
        recalc();
    }

    public void setTitleOffset(float x, float y) {
        this.mTitleOffsetX = x;
        this.mTitleOffsetY = y;
        recalc();
    }

    public void setPositionType(int type) {
        this.mPosType = type;
        recalc();
    }

    public void setShapeType(int type) {
        this.mShapeType = type;
        recalc();
    }

    public void setVerticalItemsOffset(int offset) {
        this.mVerticalItemsOffset = offset;
    }

    public void setHorizontalItemsOffset(int offset) {
        this.mHorizontalItemsOffset = offset;
    }

    public boolean onTouchEvent(InputEvent input) {
        if (input.getAction() == 3) {
            return onDown(input.getX(), input.getY());
        }
        if (input.getAction() == 4) {
            return onMove(input.getX(), input.getY());
        }
        if (input.getAction() == 5) {
            return onUp(input.getX(), input.getY());
        }
        return false;
    }

    public void off() {
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            if (item.isVisible()) {
                item.moveOff();
            }
        }
    }

    public void setOffset(float x, float y) {
        this.mOffsetX = x;
        this.mOffsetY = y;
        recalc();
    }

    public float getOffsetX() {
        return this.mOffsetX;
    }

    public float getOffsetY() {
        return this.mOffsetY;
    }

    public int update() {
        int ret;
        if (this.mTitle == null || (ret = this.mTitle.update()) == 0) {
            return super.update();
        }
        Debug.err(TAG, "Problem with updating the title!");
        return ret;
    }

    public int render() {
        int ret;
        if (!isVisible()) {
            return 0;
        }
        if (this.mTitle == null || (ret = this.mTitle.render()) == 0) {
            return super.render();
        }
        Debug.err(TAG, "Problem with rendering the title!");
        return ret;
    }

    /* access modifiers changed from: protected */
    public boolean onDown(float x, float y) {
        int i = this.mNodes.size() - 1;
        while (i >= 0) {
            MenuItem item = (MenuItem) this.mNodes.get(i);
            if (!item.isVisible() || !item.isEnabled() || !item.isCollisionPoint(x, y)) {
                i--;
            } else {
                item.down();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onMove(float x, float y) {
        moveOffAll();
        int i = this.mNodes.size() - 1;
        while (i >= 0) {
            MenuItem item = (MenuItem) this.mNodes.get(i);
            if (!item.isVisible() || !item.isEnabled() || !item.isCollisionPoint(x, y)) {
                i--;
            } else {
                item.move();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean onUp(float x, float y) {
        int i = this.mNodes.size() - 1;
        while (i >= 0) {
            MenuItem item = (MenuItem) this.mNodes.get(i);
            if (!item.isVisible() || !item.isEnabled() || !item.isCollisionPoint(x, y)) {
                i--;
            } else {
                item.up();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void moveOffAll() {
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            ((MenuItem) e.nextElement()).moveOff();
        }
    }

    public void recalc() {
        switch (this.mPosType) {
            case 0:
                positionLeftTop();
                return;
            case 1:
                positionCenterTop();
                return;
            case 2:
                positionRightTop();
                return;
            case 3:
                positionLeftCenter();
                return;
            case 4:
                positionCenterCenter();
                return;
            case 5:
                positionRightCenter();
                return;
            case 6:
                positionLeftBottom();
                return;
            case 7:
                positionCenterBottom();
                return;
            case 8:
                positionRightBottom();
                return;
            case 9:
                Debug.inf(TAG, "Custom position!");
                return;
            default:
                Debug.err(TAG, "Unsupported position type!");
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void positionLeftTop() {
        int x = (int) this.mOffsetX;
        int y = (int) this.mOffsetY;
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionCenterTop() {
        int x = 0;
        int y = (int) this.mOffsetY;
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + ((Renderer.getWidth() - item.getWidth()) / 2);
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionRightTop() {
        int x = (int) this.mOffsetX;
        int y = (int) this.mOffsetY;
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + (Renderer.getWidth() - item.getWidth());
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionLeftCenter() {
        int x = (int) this.mOffsetX;
        int y = ((int) this.mOffsetY) + ((Renderer.getHeight() - getMenuHeight()) / 2);
        if (this.mTitle != null) {
            this.mTitle.setCoordsXY((float) ((int) this.mOffsetX), (float) y);
            y = (int) (((float) y) + this.mTitleOffsetY + ((float) this.mTitle.getHeight()));
        }
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionCenterCenter() {
        int x = (int) this.mOffsetX;
        int y = ((int) this.mOffsetY) + ((Renderer.getHeight() - getMenuHeight()) / 2);
        if (this.mTitle != null) {
            this.mTitle.setCoordsXY((float) (((int) this.mOffsetX) + ((Renderer.getWidth() - this.mTitle.getWidth()) / 2)), (float) y);
            y = (int) (((float) y) + this.mTitleOffsetY + ((float) this.mTitle.getHeight()));
        }
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + ((Renderer.getWidth() - item.getWidth()) / 2);
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionRightCenter() {
        int x = (int) this.mOffsetX;
        int y = ((int) this.mOffsetY) + ((Renderer.getHeight() - getMenuHeight()) / 2);
        if (this.mTitle != null) {
            this.mTitle.setCoordsXY((float) (((int) this.mOffsetX) + (Renderer.getWidth() - this.mTitle.getWidth())), (float) y);
            y = (int) (((float) y) + this.mTitleOffsetY + ((float) this.mTitle.getHeight()));
        }
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + (Renderer.getWidth() - item.getWidth());
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionLeftBottom() {
        int x = (int) this.mOffsetX;
        int y = (((int) this.mOffsetY) + Renderer.getHeight()) - getMenuHeight();
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionCenterBottom() {
        int x = (int) this.mOffsetX;
        int y = (((int) this.mOffsetY) + Renderer.getHeight()) - getMenuHeight();
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + ((Renderer.getWidth() - item.getWidth()) / 2);
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void positionRightBottom() {
        int x = (int) this.mOffsetX;
        int y = ((int) this.mOffsetY) + (Renderer.getHeight() - getMenuHeight());
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    x = ((int) this.mOffsetX) + (Renderer.getWidth() - item.getWidth());
                    item.setCoordsXY((float) x, (float) y);
                    y += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    item.setCoordsXY((float) x, (float) y);
                    x += item.getWidth() + this.mHorizontalItemsOffset;
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return;
            }
        }
    }

    public int getMenuHeight() {
        int height = -this.mVerticalItemsOffset;
        if (this.mTitle != null) {
            height = (int) (((float) height) + ((float) this.mTitle.getHeight()) + this.mTitleOffsetY);
        }
        Enumeration<SceneNode> e = this.mNodes.elements();
        while (e.hasMoreElements()) {
            MenuItem item = (MenuItem) e.nextElement();
            switch (this.mShapeType) {
                case 0:
                    height += item.getHeight() + this.mVerticalItemsOffset;
                    break;
                case 1:
                    height = item.getHeight();
                    break;
                case 2:
                    break;
                default:
                    Debug.err(TAG, "Unsupported shape type!");
                    return 0;
            }
        }
        return height;
    }
}
