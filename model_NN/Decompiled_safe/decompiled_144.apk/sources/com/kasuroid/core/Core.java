package com.kasuroid.core;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import com.kasuroid.core.scene.SceneManager;
import com.kasuroid.core.scene.SceneNode;
import com.kasuroid.eastereggs.Field;
import java.util.EmptyStackException;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;

public class Core {
    private static final int INPUT_QUEUE_SIZE = 30;
    /* access modifiers changed from: private */
    public static final String TAG = Core.class.getSimpleName();
    private static Object inputQueueMutex = new Object();
    private static boolean mAdsEnabled = true;
    private static SceneNode mBackground = null;
    private static Context mContext = null;
    private static CoreThread mCoreThread = null;
    private static boolean mFirstStart = true;
    private static Point mFpsPosition = null;
    protected static boolean mFpsVisible = false;
    private static Stack<GameState> mGameStatesStack = null;
    private static final String mHomepageUrl = "http://kasurdev.blogspot.com";
    private static ArrayBlockingQueue<InputEvent> mInputQueue = new ArrayBlockingQueue<>(INPUT_QUEUE_SIZE);
    protected static final Object mLock = new Object();
    private static Random mRandom = null;
    private static boolean mScreenExists = false;
    private static int mState = 0;
    private static SurfaceHolder mSurface = null;
    private static Timer mTimer;
    private static boolean mVibrationEnabled;

    private static class CoreThread extends Thread {
        private static final int FRAME_PERIOD = 40;
        private static final int MAX_FPS = 25;
        private static final int MAX_FRAME_SKIPS = 5;
        private boolean mRunning = false;

        public int runThread() {
            setRunning(true);
            start();
            return 0;
        }

        public int stopThread() {
            boolean retry = true;
            setRunning(false);
            while (retry) {
                try {
                    join();
                    retry = false;
                } catch (InterruptedException e) {
                    Debug.err(Core.TAG, e.toString());
                    return 1;
                }
            }
            return 0;
        }

        public void run() {
            Fps.init();
            while (this.mRunning) {
                long beginTime = System.currentTimeMillis();
                int framesSkipped = 0;
                Core.processTouchEvent();
                synchronized (Core.mLock) {
                    Core.update();
                    Core.render();
                }
                int sleepTime = (int) (40 - (System.currentTimeMillis() - beginTime));
                if (sleepTime > 0) {
                    try {
                        Thread.sleep((long) sleepTime);
                    } catch (InterruptedException e) {
                    }
                }
                while (sleepTime < 0 && framesSkipped < 5) {
                    synchronized (Core.mLock) {
                        Core.update();
                    }
                    sleepTime += 40;
                    framesSkipped++;
                }
                if (Core.mFpsVisible) {
                    Fps.mFramesSkippedPerStatCycle += (long) framesSkipped;
                    Fps.updateStats();
                }
            }
        }

        public void setRunning(boolean run) {
            this.mRunning = run;
        }

        public boolean isRunning() {
            return this.mRunning;
        }
    }

    public Core() {
        Debug.inf(TAG, "Core()");
    }

    public static int init(SurfaceHolder surfaceHolder, Context context) {
        Debug.inf(TAG, "init()");
        if (mState == 1 || mState == 2 || mState == 3) {
            Debug.warn(TAG, "Core already initialized, state: " + mState);
            return 0;
        }
        mRandom = new Random();
        mTimer = new Timer();
        mGameStatesStack = new Stack<>();
        mFpsVisible = false;
        mFpsPosition = new Point(Field.TYPE_2_0, Field.TYPE_2_0);
        mBackground = null;
        mVibrationEnabled = true;
        if (surfaceHolder == null || context == null) {
            Debug.err(TAG, "Bad parameters!");
            return 2;
        }
        mSurface = surfaceHolder;
        mContext = context;
        int ret = TimerManager.init();
        if (ret != 0) {
            Debug.err("Core", "Problem with initializing the TimerManager");
            return ret;
        }
        int ret2 = SceneManager.init();
        if (ret2 != 0) {
            Debug.err("Core", "Problem with initializing the SceneManager");
            return ret2;
        }
        int ret3 = TextureManager.init();
        if (ret3 != 0) {
            Debug.err("Core", "Problem with initializing the TextureManager");
            return ret3;
        }
        int ret4 = Renderer.init();
        if (ret4 != 0) {
            Debug.err("Core", "Problem with initializing the Renderer");
            return ret4;
        }
        int ret5 = SoundManager.init(mContext);
        if (ret5 != 0) {
            Debug.err("Core", "Problem with initializing the SoundManager");
            return ret5;
        }
        Renderer.setSurfaceHolder(mSurface);
        mState = 1;
        setFpsPosition(Field.TYPE_2_0, Field.TYPE_2_0);
        mFirstStart = true;
        return ret5;
    }

    public static int term() {
        int ret = 0;
        Debug.inf(TAG, "term()");
        if (mState == 0) {
            Debug.warn(TAG, "Core already terminated");
        } else {
            if (mState == 2) {
                Debug.warn(TAG, "Core is still running, stopping..");
                ret = stop();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with stopping the core!");
                }
            }
            if (removeGameStates() != 0) {
                Debug.err(TAG, "Problem with removing the game states!");
            }
            mGameStatesStack.clear();
            if (TimerManager.term() != 0) {
                Debug.err("Core", "Problem with terminating the TimerManager");
            }
            if (SoundManager.term() != 0) {
                Debug.err("Core", "Problem with terminating the SoundManager");
            }
            if (SceneManager.term() != 0) {
                Debug.err("Core", "Problem with terminating the SceneManager");
            }
            if (TextureManager.term() != 0) {
                Debug.err("Core", "Problem with terminating the TextureManager");
            }
            ret = Renderer.term();
            if (ret != 0) {
                Debug.err("Core", "Problem with terminating the Renderer");
            }
            mState = 0;
            mFirstStart = true;
        }
        return ret;
    }

    public static int start() {
        Debug.inf(TAG, "start()");
        if (mState == 0) {
            Debug.err(TAG, "Wrong core state: " + mState);
            return 1;
        } else if (mState == 2) {
            Debug.warn(TAG, "Core already running.");
            return 0;
        } else {
            mState = 2;
            if (mFirstStart) {
                Debug.inf(TAG, "Calling activity ready()");
                ((CoreActivity) mContext).ready();
            }
            return 0;
        }
    }

    public static int stop() {
        Debug.inf(TAG, "stop()");
        if (mState == 1 || mState == 0) {
            Debug.err(TAG, "Wrong core state: " + mState);
            return 1;
        } else if (mState == 3) {
            Debug.warn(TAG, "Core already stopped.");
            return 0;
        } else {
            mState = 3;
            return 0;
        }
    }

    public static int pause() {
        int ret;
        Debug.inf(TAG, "pause()");
        mTimer.stop();
        if (mGameStatesStack.isEmpty() || (ret = mGameStatesStack.peek().pause()) == 0) {
            int ret2 = mCoreThread.stopThread();
            if (ret2 != 0) {
                Debug.err(TAG, "Problem with stopping the core thread!");
                return ret2;
            }
            mCoreThread = null;
            return 0;
        }
        Debug.err(TAG, "Problem with pausing the game state!");
        return ret;
    }

    public static int resume() {
        int ret;
        Debug.inf(TAG, "resume()");
        mCoreThread = new CoreThread();
        int ret2 = mCoreThread.runThread();
        if (ret2 != 0) {
            Debug.err(TAG, "Problem with running the core thread!");
            return ret2;
        }
        mTimer.start();
        if (mGameStatesStack.isEmpty() || (ret = mGameStatesStack.peek().resume()) == 0) {
            return 0;
        }
        Debug.err(TAG, "Problem with pausing the game state!");
        return ret;
    }

    public static int getState() {
        return mState;
    }

    public static void setContext(Context context) {
        mContext = context;
    }

    public static Random getRandom() {
        return mRandom;
    }

    public static void setSurfaceHolder(SurfaceHolder holder) {
        mSurface = holder;
        Renderer.setSurfaceHolder(holder);
    }

    protected static int update() {
        int ret = 0;
        if (mScreenExists) {
            mTimer.update();
            if (!mGameStatesStack.isEmpty()) {
                GameState currentState = mGameStatesStack.peek();
                ret = TimerManager.updateAll();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with updating timers!");
                } else {
                    updateBackground();
                    ret = SceneManager.update();
                    if (ret != 0) {
                        Debug.err(TAG, "Problem with updating the scenes!");
                    } else {
                        ret = currentState.update();
                        if (ret != 0) {
                            Debug.err(TAG, "Problem with updating the state!");
                        } else {
                            GameState currentState2 = mGameStatesStack.peek();
                        }
                    }
                }
            }
        }
        return ret;
    }

    protected static int render() {
        int ret = 0;
        if (mScreenExists) {
            try {
                Renderer.lock();
                renderBackground();
                ret = SceneManager.render();
                if (ret != 0) {
                    Debug.err(TAG, "Problem with rendering the scenes!");
                    Renderer.unlock();
                } else {
                    int size = mGameStatesStack.size();
                    for (int i = 0; i < size; i++) {
                        mGameStatesStack.get(i).render();
                    }
                    if (mFpsVisible) {
                        renderFps();
                    }
                    Renderer.unlock();
                }
            } finally {
                Renderer.unlock();
            }
        }
        return ret;
    }

    public static int changeState(GameState gameState) {
        synchronized (mLock) {
            Debug.inf(TAG, "changeState()");
            if (gameState == null) {
                Debug.err(TAG, "Bad parameter!");
                return 2;
            }
            int ret = removeGameStates();
            if (ret != 0) {
                Debug.err(TAG, "Problem with removing the obsolete game states!");
                return ret;
            }
            Debug.inf(TAG, "Stack size: " + mGameStatesStack.size());
            mGameStatesStack.push(gameState);
            int ret2 = mGameStatesStack.peek().init();
            if (ret2 == 0) {
                return ret2;
            }
            Debug.err(TAG, "Problem with initializing the game state!");
            return ret2;
        }
    }

    public static int pushState(GameState gameState) {
        int ret;
        synchronized (mLock) {
            Debug.inf(TAG, "pushState()");
            if (gameState == null) {
                Debug.err(TAG, "Bad parameter!");
                return 2;
            } else if (mGameStatesStack.isEmpty() || (ret = mGameStatesStack.peek().pause()) == 0) {
                mGameStatesStack.push(gameState);
                int ret2 = mGameStatesStack.peek().init();
                if (ret2 == 0) {
                    return ret2;
                }
                Debug.err(TAG, "Problem with initializing the game state!");
                return ret2;
            } else {
                Debug.err(TAG, "Problem with pausing the game state!");
                return ret;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int popState() {
        /*
            java.lang.Object r2 = com.kasuroid.core.Core.mLock
            monitor-enter(r2)
            java.lang.String r3 = com.kasuroid.core.Core.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "popState()"
            com.kasuroid.core.Debug.inf(r3, r4)     // Catch:{ all -> 0x0053 }
            r0 = 0
            java.util.Stack<com.kasuroid.core.GameState> r3 = com.kasuroid.core.Core.mGameStatesStack     // Catch:{ all -> 0x0053 }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x0030
            java.util.Stack<com.kasuroid.core.GameState> r3 = com.kasuroid.core.Core.mGameStatesStack     // Catch:{ all -> 0x0053 }
            java.lang.Object r0 = r3.peek()     // Catch:{ all -> 0x0053 }
            com.kasuroid.core.GameState r0 = (com.kasuroid.core.GameState) r0     // Catch:{ all -> 0x0053 }
            int r0 = r0.term()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x002b
            java.lang.String r3 = com.kasuroid.core.Core.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "Problem with terminating the game stae!"
            com.kasuroid.core.Debug.err(r3, r4)     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r1 = r0
        L_0x002a:
            return r1
        L_0x002b:
            java.util.Stack<com.kasuroid.core.GameState> r3 = com.kasuroid.core.Core.mGameStatesStack     // Catch:{ all -> 0x0053 }
            r3.pop()     // Catch:{ all -> 0x0053 }
        L_0x0030:
            java.util.Stack<com.kasuroid.core.GameState> r3 = com.kasuroid.core.Core.mGameStatesStack     // Catch:{ all -> 0x0053 }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x0050
            java.util.Stack<com.kasuroid.core.GameState> r3 = com.kasuroid.core.Core.mGameStatesStack     // Catch:{ all -> 0x0053 }
            java.lang.Object r0 = r3.peek()     // Catch:{ all -> 0x0053 }
            com.kasuroid.core.GameState r0 = (com.kasuroid.core.GameState) r0     // Catch:{ all -> 0x0053 }
            int r0 = r0.resume()     // Catch:{ all -> 0x0053 }
            if (r0 == 0) goto L_0x0050
            java.lang.String r3 = com.kasuroid.core.Core.TAG     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "Problem with resuming the game state!"
            com.kasuroid.core.Debug.err(r3, r4)     // Catch:{ all -> 0x0053 }
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r1 = r0
            goto L_0x002a
        L_0x0050:
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            r1 = r0
            goto L_0x002a
        L_0x0053:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0053 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kasuroid.core.Core.popState():int");
    }

    public static boolean keyDown(int keyCode, KeyEvent msg) {
        synchronized (mLock) {
            if (mGameStatesStack.isEmpty()) {
                return false;
            }
            boolean onKeyDown = mGameStatesStack.peek().onKeyDown(keyCode, msg);
            return onKeyDown;
        }
    }

    public static boolean keyUp(int keyCode, KeyEvent msg) {
        synchronized (mLock) {
            if (mGameStatesStack.isEmpty()) {
                return false;
            }
            boolean onKeyUp = mGameStatesStack.peek().onKeyUp(keyCode, msg);
            return onKeyUp;
        }
    }

    public static void feedInput(InputEvent input) {
        synchronized (inputQueueMutex) {
            try {
                mInputQueue.put(input);
            } catch (InterruptedException e) {
                Debug.err(TAG, e.getMessage());
            }
        }
    }

    public static void processTouchEvent() {
        synchronized (inputQueueMutex) {
            ArrayBlockingQueue<InputEvent> inputQueue = mInputQueue;
            while (!inputQueue.isEmpty()) {
                try {
                    InputEvent input = inputQueue.take();
                    try {
                        mGameStatesStack.peek().onTouchEvent(input);
                    } catch (EmptyStackException e) {
                        Debug.warn(TAG, "No active states!");
                    }
                    input.returnToPool();
                } catch (InterruptedException e2) {
                    Debug.err(TAG, e2.getMessage());
                }
            }
        }
    }

    public static void quit() {
        Debug.inf(TAG, "QUIT!");
        ((CoreActivity) mContext).quit();
    }

    public static void focusChanged(boolean hasWindowFocus) {
    }

    public static Context getContext() {
        return mContext;
    }

    public static Timer getTimer() {
        return mTimer;
    }

    public static void showAd() {
        if (mAdsEnabled) {
            Message msg = new Message();
            msg.obj = "showAd";
            ((CoreActivity) mContext).sendMessage(msg);
        }
    }

    public static void hideAd() {
        if (mAdsEnabled) {
            Message msg = new Message();
            msg.obj = "hideAd";
            ((CoreActivity) mContext).sendMessage(msg);
        }
    }

    public static void showHelp() {
        Message msg = new Message();
        msg.obj = "showHelp";
        ((CoreActivity) mContext).sendMessage(msg);
    }

    public static void sendMessage(String strMsg) {
        Message msg = new Message();
        msg.obj = strMsg;
        ((CoreActivity) mContext).sendMessage(msg);
    }

    public static int getAdHeight() {
        if (mAdsEnabled) {
            return (int) (48.0f * getScale());
        }
        return 0;
    }

    public static void startBrowser(String url) {
        getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public static void openHomepage() {
        startBrowser(mHomepageUrl);
    }

    public static void setVibrationEnabled(boolean enabled) {
        mVibrationEnabled = enabled;
    }

    public static boolean isVibrationEnabled() {
        return mVibrationEnabled;
    }

    public static void screenCreated(SurfaceHolder surface) {
        synchronized (mLock) {
            mSurface = surface;
            Renderer.screenCreated(surface);
            Debug.inf(TAG, "Starting the thread");
            if (start() != 0) {
                Debug.err(TAG, "Couldn't start the core!");
            }
            mScreenExists = true;
            mFirstStart = false;
        }
        Debug.inf(TAG, "screenCreated");
    }

    public static void screenDestroyed(SurfaceHolder surface) {
        synchronized (mLock) {
            Renderer.screenDestroyed();
            if (!mGameStatesStack.isEmpty()) {
                mGameStatesStack.peek().onScreenDestroyed();
            }
            mScreenExists = false;
        }
        Debug.inf(TAG, "screenDestroyed");
    }

    public static void screenChanged(SurfaceHolder surface, int width, int height) {
        synchronized (mLock) {
            Renderer.screenChanged(surface, width, height);
            if (!mGameStatesStack.isEmpty()) {
                mGameStatesStack.peek().onScreenChanged(width, height);
            }
            mScreenExists = true;
        }
        Debug.inf(TAG, "screenChanged, width: " + width + ", height: " + height);
    }

    public static Object getLock() {
        return mLock;
    }

    public static void showFps() {
        mFpsVisible = true;
    }

    public static void hideFps() {
        mFpsVisible = false;
    }

    public static void setFpsPosition(int x, int y) {
        mFpsPosition.x = x;
        mFpsPosition.y = y;
    }

    public static String getString(int id) {
        if (mContext != null) {
            return mContext.getString(id);
        }
        return null;
    }

    public static void enableAds() {
        mAdsEnabled = true;
    }

    public static void disableAds() {
        mAdsEnabled = false;
    }

    public static boolean areAdsEnabled() {
        return mAdsEnabled;
    }

    private static void renderFps() {
        Renderer.setAlpha(255);
        Renderer.setColor(-1);
        Renderer.setTextSize(15);
        Renderer.setTypeface(Typeface.DEFAULT);
        Renderer.drawText("FPS: " + Fps.getString(), (float) mFpsPosition.x, (float) mFpsPosition.y);
    }

    private static int removeGameStates() {
        int ret = 0;
        Debug.inf(TAG, "Removing game states");
        while (true) {
            if (mGameStatesStack.isEmpty()) {
                break;
            }
            ret = mGameStatesStack.pop().term();
            if (ret != 0) {
                Debug.err(TAG, "Problem with terminating the game state!");
                break;
            }
        }
        return ret;
    }

    public static void setBackground(SceneNode node) {
        mBackground = node;
    }

    public static SceneNode getBackground() {
        return mBackground;
    }

    public static void vibrate(long time) {
        if (isVibrationEnabled()) {
            ((Vibrator) mContext.getSystemService("vibrator")).vibrate(time);
        }
    }

    public static float getScale() {
        return Renderer.getScaleFactor();
    }

    public static void checkPackage(String packageName) {
        try {
            if (!mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).packageName.equals(encode(packageName))) {
                Debug.inf(TAG, "App stolen!");
                quit();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String encode(String str) {
        String s = new String();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= '!' && c <= 'O') {
                c = (char) ((c + '/') % 127);
            } else if (c >= 'P' && c <= '~') {
                c = (char) ((c - '/') % 127);
            }
            s = String.valueOf(s) + c;
        }
        return s;
    }

    private static void renderBackground() {
        if (mBackground != null) {
            mBackground.render();
        }
    }

    private static void updateBackground() {
        if (mBackground != null) {
            mBackground.update();
        }
    }
}
