package com.wallpaperpuzzle.lionking;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wallpaperpuzzle.lionking.audio.AudioManager;
import com.wallpaperpuzzle.lionking.audio.Options;
import com.wallpaperpuzzle.lionking.audio.sound.Sound;
import com.wallpaperpuzzle.lionking.db.WallpaperDbManager;
import com.wallpaperpuzzle.lionking.game.GameStatus;
import com.wallpaperpuzzle.lionking.game.GameType;
import com.wallpaperpuzzle.lionking.game.Panel;
import com.wallpaperpuzzle.lionking.game.PanelListener;
import com.wallpaperpuzzle.lionking.game.ResourceLauncher;
import com.wallpaperpuzzle.lionking.game.SerializeUtility;
import com.wallpaperpuzzle.lionking.utils.GlobalUtils;
import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class WallpaperPuzzle extends Activity {
    private static final String GAME_STATUS = "gameStatus";
    private static final String GAME_TYPE = "gameType";
    private static final String ITEM_POSITION = "itemPosition";
    private static final String SHUFFLE_BITMAP = "shuffleBitmap";
    private static final String TAG = "WallpaperPuzzle";
    public static ProgressDialog mProgressDialog;
    private AlertDialog closeDialog;
    private GameStatus gameStatus;
    private GameType gameType;
    private Boolean hasIntent = false;
    /* access modifiers changed from: private */
    public Integer itemPosition;
    private Button mNext;
    private int mOrientation;
    private List<Integer> mShuffleBitmapList;
    private Panel panel;
    /* access modifiers changed from: private */
    public Dialog peekDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.puzzle);
        setVolumeControlStream(3);
        this.mOrientation = GlobalUtils.getOrientation(this);
        if (ResourceLauncher.getBackgroundMenu() == null) {
            ResourceLauncher.setBackgroundMenu(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.background)));
        }
        ((LinearLayout) findViewById(R.id.root)).setBackgroundDrawable(ResourceLauncher.getBackgroundMenu());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mShuffleBitmapList = null;
        if (ImageLevelAdapter.getImageLevelAdapter() == null) {
            GlobalUtils.init(this);
            WallpaperDbManager.initWallpaperDbManager(this);
            AudioManager.init(this);
            mProgressDialog = ProgressDialog.show(this, "", getString(R.string.f2apploading), true);
            new Thread() {
                public void run() {
                    try {
                        ImageLevelAdapter.initImageLevelAdapter(WallpaperPuzzle.this, WallpaperPuzzle.this.obtainStyledAttributes(R.styleable.MenuTheme), WallpaperDbManager.getCount());
                    } catch (Exception e) {
                        Log.e(WallpaperPuzzle.TAG, e.toString());
                    }
                    WallpaperPuzzle.mProgressDialog.dismiss();
                }
            }.start();
            mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    WallpaperPuzzle.this.defaultInitializing();
                }
            });
            return;
        }
        if (this.hasIntent.booleanValue()) {
            this.hasIntent = false;
        } else {
            loadPreferences();
        }
        initCreation();
    }

    private void loadPreferences() {
        SharedPreferences preferences = getPreferences(0);
        String outShuffleBitmapList = preferences.getString(SHUFFLE_BITMAP, null);
        String outGameType = preferences.getString(GAME_TYPE, null);
        String outItemPosition = preferences.getString("itemPosition", null);
        String outGameStatus = preferences.getString(GAME_STATUS, null);
        try {
            this.itemPosition = (Integer) SerializeUtility.deserializeString(outItemPosition);
            this.mShuffleBitmapList = (Vector) SerializeUtility.deserializeString(outShuffleBitmapList);
            this.gameType = (GameType) SerializeUtility.deserializeString(outGameType);
            this.gameStatus = (GameStatus) SerializeUtility.deserializeString(outGameStatus);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (ClassNotFoundException e2) {
            Log.e(TAG, e2.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void defaultInitializing() {
        ResourceLauncher.invalidateResources();
        this.itemPosition = Integer.valueOf(ImageLevelAdapter.getImageLevelAdapter().getDefaultPosition());
        this.gameType = ImageLevelAdapter.getGameTypeByPosition(this.itemPosition.intValue());
        initGameStatus();
        initCreation();
    }

    private void initCreation() {
        initCloseDialog();
        initSound(this);
        PanelListener panelListener = new PanelListener() {
            public void onComplete() {
                AudioManager.playSound(Sound.levelcompleted);
                WallpaperDbManager.saveItem(WallpaperPuzzle.this.itemPosition.intValue());
                ImageLevelAdapter.update(WallpaperPuzzle.this.itemPosition.intValue());
                WallpaperPuzzle.this.updateNextButton();
            }

            public void onNext() {
                WallpaperPuzzle.this.goNext();
            }
        };
        this.panel = new Panel(this, this.gameType, this.itemPosition.intValue());
        this.panel.setPanelListener(panelListener);
        this.panel.setGameStatus(this.gameStatus);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.puzzle);
        linearLayout.removeAllViewsInLayout();
        linearLayout.addView(this.panel);
        if (this.mShuffleBitmapList != null) {
            this.panel.setShufleBitmap(this.mShuffleBitmapList);
        }
        findViewById(R.id.bottom_bgr).setBackgroundDrawable(getBackgroundPuzzleBottom());
        LinearLayout menu = (LinearLayout) findViewById(R.id.menu);
        menu.setBackgroundDrawable(getBackgroundPuzzleMenu());
        menu.setVisibility(0);
        initButtons();
        initPeekDialog();
        updatePuzzleLevel(this, this.itemPosition.intValue());
        if (this.mShuffleBitmapList == null) {
            AudioManager.playSound(Sound.startpuzzle);
        }
    }

    private void updatePanel() {
        updatePuzzleLevel(this, this.itemPosition.intValue());
        initGameStatus();
        this.panel.setGameStatus(this.gameStatus);
        this.panel.update(this.gameType, this.itemPosition.intValue());
        initPeekDialog();
    }

    private void initButtons() {
        ((Button) findViewById(R.id.gallery)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("itemPosition", WallpaperPuzzle.this.itemPosition);
                AudioManager.playSound(Sound.pressbutton);
                GlobalUtils.startActivity(WallpaperPuzzle.this, WallpaperMenu.class, intent);
            }
        });
        ((Button) findViewById(R.id.peek)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AudioManager.playSound(Sound.pressbutton);
                WallpaperPuzzle.this.peekDialog.show();
            }
        });
        this.mNext = (Button) findViewById(R.id.next);
        this.mNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WallpaperPuzzle.this.goNext();
            }
        });
        updateNextButton();
    }

    /* access modifiers changed from: private */
    public void goNext() {
        AudioManager.playSound(Sound.pressbutton);
        if (WallpaperDbManager.getItemById(this.itemPosition.intValue()) == null || ImageLevelAdapter.getMaxItem() == this.itemPosition.intValue()) {
            Intent intent = new Intent();
            intent.putExtra("itemPosition", this.itemPosition);
            GlobalUtils.startActivity(this, WallpaperMenu.class, intent);
            return;
        }
        updateItemPosition();
        updateNextButton();
        this.gameType = ImageLevelAdapter.getGameTypeByPosition(this.itemPosition.intValue());
        initGameStatus();
        updatePanel();
    }

    /* access modifiers changed from: private */
    public void updateNextButton() {
        if (WallpaperDbManager.getItemById(this.itemPosition.intValue()) == null || ImageLevelAdapter.getMaxItem() == this.itemPosition.intValue()) {
            this.mNext.setVisibility(4);
        } else {
            this.mNext.setVisibility(0);
        }
    }

    private void updateItemPosition() {
        if (WallpaperDbManager.getItemById(this.itemPosition.intValue()) != null && ImageLevelAdapter.getMaxItem() != this.itemPosition.intValue()) {
            Integer num = this.itemPosition;
            this.itemPosition = Integer.valueOf(this.itemPosition.intValue() + 1);
        }
    }

    public static void updatePuzzleLevel(Activity pActivity, int itemPosition2) {
        ((TextView) pActivity.findViewById(R.id.puzzleLevel)).setText(pActivity.getString(R.string.f22puzzlelevel, new Object[]{Integer.valueOf(itemPosition2 + 1), Integer.valueOf(ImageLevelAdapter.getMaxItem() + 1)}));
    }

    private Drawable getBackgroundPuzzleBottom() {
        if (ResourceLauncher.getBackgroundPuzzleBottomPortrait() == null || ResourceLauncher.getBackgroundPuzzleBottomLandscape() == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background_bottom);
            if (1 == this.mOrientation) {
                ResourceLauncher.setBackgroundPuzzleBottomPortrait(new BitmapDrawable(getResources(), bitmap));
            } else {
                ResourceLauncher.setBackgroundPuzzleBottomLandscape(new BitmapDrawable(getResources(), bitmap));
            }
        }
        if (1 == this.mOrientation) {
            return ResourceLauncher.getBackgroundPuzzleBottomPortrait();
        }
        return ResourceLauncher.getBackgroundPuzzleBottomLandscape();
    }

    private Drawable getBackgroundPuzzleMenu() {
        if (ResourceLauncher.getBackgroundPuzzleMenuPortrait() == null || ResourceLauncher.getBackgroundPuzzleMenuLandscape() == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.background_puzzle_menu);
            if (1 == this.mOrientation) {
                ResourceLauncher.setBackgroundPuzzleMenuPortrait(new BitmapDrawable(getResources(), bitmap));
            } else {
                ResourceLauncher.setBackgroundPuzzleMenuLandscape(new BitmapDrawable(getResources(), bitmap));
            }
        }
        if (1 == this.mOrientation) {
            return ResourceLauncher.getBackgroundPuzzleMenuPortrait();
        }
        return ResourceLauncher.getBackgroundPuzzleMenuLandscape();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            this.hasIntent = true;
            this.itemPosition = Integer.valueOf(extras.getInt("itemPosition"));
            this.gameType = ImageLevelAdapter.getGameTypeByPosition(this.itemPosition.intValue());
            initGameStatus();
        }
    }

    private void initGameStatus() {
        this.gameStatus = GameStatus.started;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (WallpaperMenu.mProgressDialog != null) {
            WallpaperMenu.mProgressDialog.dismiss();
        }
        List<Integer> shuffleBitmapList = this.panel.getShuffleBitmap();
        GameStatus gameStatus2 = this.panel.getGameStatus();
        try {
            SharedPreferences.Editor editor = getPreferences(0).edit();
            editor.putString(SHUFFLE_BITMAP, SerializeUtility.serializeString(shuffleBitmapList));
            editor.putString(GAME_TYPE, SerializeUtility.serializeString(this.gameType));
            editor.putString("itemPosition", SerializeUtility.serializeString(this.itemPosition));
            editor.putString(GAME_STATUS, SerializeUtility.serializeString(gameStatus2));
            editor.commit();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void onBackPressed() {
        this.closeDialog.show();
    }

    public void initCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.f3appquit);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage((int) R.string.f4appquittext).setCancelable(true).setPositiveButton((int) R.string.f11dialogyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AudioManager.playSound(Sound.pressbutton);
                WallpaperPuzzle.this.finish();
            }
        }).setNeutralButton((int) R.string.f13dialogapps, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AudioManager.playSound(Sound.pressbutton);
                WallpaperPuzzle.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:" + WallpaperPuzzle.this.getString(R.string.f0apppublisher))));
            }
        }).setNegativeButton((int) R.string.f12dialogno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AudioManager.playSound(Sound.pressbutton);
                dialog.cancel();
            }
        });
        this.closeDialog = builder.create();
    }

    private void initPeekDialog() {
        this.peekDialog = new Dialog(this, R.style.FullHeightDialog);
        View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.peek, (ViewGroup) findViewById(R.id.layout_root));
        ImageView imageView = (ImageView) layout.findViewById(R.id.image);
        imageView.setImageBitmap(this.panel.getScratch());
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WallpaperPuzzle.this.peekDialog.cancel();
            }
        });
        this.peekDialog.setContentView(layout);
    }

    public static void initSound(final Activity pActivity) {
        boolean play = Options.getMusic(pActivity);
        ImageView sound = (ImageView) pActivity.findViewById(R.id.sound);
        setSoundView(sound, play);
        sound.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ImageView sound = (ImageView) view;
                boolean play = !Options.getMusic(pActivity);
                Options.putMusic(pActivity, play);
                if (!play) {
                    AudioManager.stopAudio();
                }
                WallpaperPuzzle.setSoundView(sound, play);
                AudioManager.playSound(Sound.pressbutton);
            }
        });
    }

    /* access modifiers changed from: private */
    public static void setSoundView(ImageView sound, boolean play) {
        if (play) {
            sound.setImageResource(R.drawable.sound_on);
        } else {
            sound.setImageResource(R.drawable.sound_off);
        }
    }
}
