package com.wallpaperpuzzle.lionking.audio.sound;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import com.wallpaperpuzzle.lionking.audio.Options;
import java.util.HashMap;

public class SoundManager {
    private static SoundManager instance;
    private AudioManager mAudioManager;
    private Context mContext = null;
    private SoundPool mSoundPool;
    private HashMap<Sound, Integer> mSoundPoolMap;

    private SoundManager() {
    }

    public static synchronized SoundManager getInstance() {
        SoundManager soundManager;
        synchronized (SoundManager.class) {
            if (instance == null) {
                instance = new SoundManager();
            }
            soundManager = instance;
        }
        return soundManager;
    }

    public void initSounds(Context theContext) {
        if (this.mContext == null) {
            this.mContext = theContext;
            this.mSoundPool = new SoundPool(4, 3, 0);
            this.mSoundPoolMap = new HashMap<>();
            this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
        }
    }

    public void loadSounds() {
        Sound[] sounds = Sound.values();
        for (int i = 0; i < sounds.length; i++) {
            this.mSoundPoolMap.put(Sound.values()[i], Integer.valueOf(this.mSoundPool.load(this.mContext, Sound.values()[i].getSound(), 1)));
        }
    }

    public void playSound(Sound sound) {
        if (Options.getMusic(this.mContext)) {
            float streamVolume = ((float) this.mAudioManager.getStreamVolume(3)) / ((float) this.mAudioManager.getStreamMaxVolume(3));
            this.mSoundPool.play(this.mSoundPoolMap.get(sound).intValue(), streamVolume, streamVolume, 1, 0, 1.0f);
        }
    }

    public void stopSound(Sound sound) {
        this.mSoundPool.stop(this.mSoundPoolMap.get(sound).intValue());
    }

    public void stopSounds() {
        Sound[] sounds = Sound.values();
        for (int i = 0; i < sounds.length; i++) {
            this.mSoundPool.stop(Sound.values()[i].getSound());
        }
    }

    public void cleanup() {
        this.mSoundPool.release();
        this.mSoundPool = null;
        this.mSoundPoolMap.clear();
        this.mAudioManager.unloadSoundEffects();
        instance = null;
    }
}
