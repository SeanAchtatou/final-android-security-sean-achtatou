package com.wallpaperpuzzle.lionking.game;

public interface PanelListener {
    void onComplete();

    void onNext();
}
