package com.wallpaperpuzzle.lionking.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
