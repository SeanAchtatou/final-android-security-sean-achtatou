package com.rogansoft.pokerdict;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.rogansoft.pokerdict.domain.Term;
import java.util.ArrayList;
import java.util.Random;

public class SearchActivity extends ListActivity {
    private static final String TAG = "SearchActivity";
    private DictDbAdapter mDb;
    private ListView mList;
    private ArrayList<Term> mTerms;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView((int) R.layout.search);
        this.mTerms = new ArrayList<>();
        this.mDb = new DictDbAdapter(this);
        this.mDb.open();
        this.mList = getListView();
        this.mList.setFastScrollEnabled(true);
        Intent intent = getIntent();
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            String query = intent.getStringExtra("query");
            if (query.length() > 0) {
                this.mTerms = this.mDb.fetchTermLike(query);
            }
        } else if ("android.intent.action.VIEW".equals(intent.getAction())) {
            Uri uri = intent.getData();
            if (uri != null) {
                Log.d(TAG, "uridata:" + uri.toString());
                launchViewTerm(new Long(uri.getLastPathSegment()).longValue());
                finish();
            } else {
                Log.d(TAG, "uridata:null");
                this.mTerms = this.mDb.fetchAllTerms();
            }
        } else {
            this.mTerms = this.mDb.fetchAllTerms();
        }
        setListAdapter(new ArrayAdapter(this, 17367043, this.mTerms));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mDb.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        launchViewTerm(((Term) getListView().getItemAtPosition(position)).getId());
        super.onListItemClick(l, v, position, id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_opt_random:
                launchViewTerm(getRandomRowId());
                return true;
            case R.id.menu_opt_search:
                onSearchRequested();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    private long getRandomRowId() {
        return this.mTerms.get(new Random().nextInt(this.mTerms.size())).getId();
    }

    /* access modifiers changed from: protected */
    public void launchViewTerm(long id) {
        Intent i = new Intent(this, ViewTerm.class);
        Bundle b = new Bundle();
        b.putLong("id", id);
        i.putExtras(b);
        startActivity(i);
    }
}
