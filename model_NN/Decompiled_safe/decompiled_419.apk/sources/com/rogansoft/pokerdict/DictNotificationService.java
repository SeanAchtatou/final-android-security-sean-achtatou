package com.rogansoft.pokerdict;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.util.Log;
import com.rogansoft.pokerdict.domain.Term;

public class DictNotificationService extends Service {
    private static final int NOTIFY_ID = 1;
    private static final String TAG = "DictNotificationService";
    private final IBinder mBinder = new CaffeineProcessorBinder();

    public class CaffeineProcessorBinder extends Binder {
        public CaffeineProcessorBinder() {
        }

        /* access modifiers changed from: package-private */
        public DictNotificationService getService() {
            return DictNotificationService.this;
        }
    }

    private void sendNotify(Intent intent) {
        Log.d(TAG, "sendNotify");
        long when = System.currentTimeMillis();
        DictDbAdapter dictDbAdapter = new DictDbAdapter(this);
        dictDbAdapter.open();
        Term term = dictDbAdapter.fetchRandomTerm();
        dictDbAdapter.close();
        Log.d(TAG, "id:" + term.getId() + "term:" + term.getTerm());
        Notification notification = new Notification(R.drawable.notification_icon, "Daily Poker Term", when);
        notification.flags |= 16;
        notification.defaults |= 1;
        CharSequence contentText = Html.fromHtml(term.getDefinition()).toString();
        Intent intent2 = new Intent(this, ViewTerm.class);
        Bundle bundle = new Bundle();
        bundle.putLong("id", term.getId());
        bundle.putBoolean("from_notify", true);
        intent2.putExtras(bundle);
        notification.setLatestEventInfo(getApplicationContext(), String.valueOf(term.getTerm()) + " (Daily Poker Term)", contentText, PendingIntent.getActivity(this, 0, intent2, 134217728));
        ((NotificationManager) getSystemService("notification")).notify(1, notification);
    }

    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }

    public void onRebind(Intent intent) {
        Log.d(TAG, "onRebind");
        super.onRebind(intent);
    }

    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
    }

    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "onStartCommand");
        sendNotify(intent);
    }

    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return this.mBinder;
    }
}
