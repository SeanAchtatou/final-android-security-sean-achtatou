package com.rogansoft.pokerdict;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static final String DATE_STAMP_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm";
    private static int DAY = (HOUR * 24);
    public static final String FUTURE_MSG_DATE_FORMAT = "E MM/dd";
    public static final String FUTURE_MSG_TIME_FORMAT = "h:mma";
    private static int HOUR = (MINUTE * 60);
    private static int MINUTE = (SECOND * 60);
    private static int MONTH = (DAY * 30);
    private static int SECOND = 1;

    public static String calToDateStr(Calendar adate) {
        return calToStr(adate, DATE_STAMP_FORMAT);
    }

    public static String secsToFutureDateStr(long secs) {
        return calToStr(secsToCal(secs), FUTURE_MSG_DATE_FORMAT);
    }

    public static String calToFutureDateStr(Calendar adate) {
        return calToStr(adate, FUTURE_MSG_DATE_FORMAT);
    }

    public static String secsToFutureTimeStr(long secs) {
        return calToStr(secsToCal(secs), FUTURE_MSG_TIME_FORMAT).toLowerCase();
    }

    public static String calToFutureTimeStr(Calendar adate) {
        return calToStr(adate, FUTURE_MSG_TIME_FORMAT);
    }

    private static String calToStr(Calendar adate, String format) {
        return new SimpleDateFormat(format).format(adate.getTime());
    }

    public static Calendar dateStrToCal(String datestampstr) throws ParseException {
        Date adate = new SimpleDateFormat(DATE_STAMP_FORMAT).parse(datestampstr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(adate);
        return cal;
    }

    public static String calToDateTimeStr(Calendar adatetime) {
        return calToStr(adatetime, DATE_TIME_STAMP_FORMAT);
    }

    public static Calendar dateTimeStrToCal(String datetimestampstr) throws ParseException {
        Date adatetime = new SimpleDateFormat(DATE_TIME_STAMP_FORMAT).parse(datetimestampstr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(adatetime);
        return cal;
    }

    public static Calendar secsToCal(long secs) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(1000 * secs);
        return cal;
    }

    public static String secsToDateTimeStr(long secs) {
        return calToDateTimeStr(secsToCal(secs));
    }

    public static String relativeTime(long fromSecs, long toSecs) {
        boolean future = fromSecs > toSecs;
        long fromSecs2 = future ? fromSecs - toSecs : toSecs - fromSecs;
        String qualifier = future ? "from now" : "ago";
        String singleDayQualifier = future ? "tomorrow" : "yesterday";
        if (fromSecs2 < ((long) (MINUTE * 1))) {
            if (fromSecs2 == 1) {
                return "1 second " + qualifier;
            }
            return String.valueOf(fromSecs2) + " seconds " + qualifier;
        } else if (fromSecs2 < ((long) (MINUTE * 2))) {
            return "a minute " + qualifier;
        } else {
            if (fromSecs2 < ((long) (MINUTE * 45))) {
                return String.valueOf(fromSecs2 / ((long) MINUTE)) + " minutes " + qualifier;
            }
            if (fromSecs2 < ((long) (MINUTE * 90))) {
                return "an hour " + qualifier;
            }
            if (fromSecs2 < ((long) (HOUR * 24))) {
                return String.valueOf(fromSecs2 / ((long) HOUR)) + " hours " + qualifier;
            }
            if (fromSecs2 < ((long) (HOUR * 48))) {
                return singleDayQualifier;
            }
            if (fromSecs2 < ((long) (DAY * 30))) {
                return String.valueOf(fromSecs2 / ((long) DAY)) + " days " + qualifier;
            }
            if (fromSecs2 < ((long) (MONTH * 12))) {
                long months = (fromSecs2 / ((long) DAY)) / 30;
                return months <= 1 ? "one month " + qualifier : String.valueOf(months) + " months " + qualifier;
            }
            long years = (fromSecs2 / ((long) DAY)) / 365;
            return years <= 1 ? "one year " + qualifier : String.valueOf(years) + " years " + qualifier;
        }
    }
}
