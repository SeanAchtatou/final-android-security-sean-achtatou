package com.rogansoft.pokerdict;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import com.flurry.android.FlurryAgent;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.text.ParseException;
import java.util.Calendar;

public class MainActivity extends Activity {
    public static final String FLURRY_KEY = "KTFPFTQFRZ2UW5HLWZ1N";
    private static final String TAG = "MainActivity";
    private PendingIntent mAlarmSender;
    private ImageButton mBtnBrowse;
    private ImageButton mBtnRandom;
    private ImageButton mBtnSearch;
    /* access modifiers changed from: private */
    public DictDbAdapter mDb;
    private boolean mPrefDailyTermEnabled = true;
    private String mPrefDailyTermTimeOfDay = "09:00";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        AdRequest request = new AdRequest();
        Utils.configureAdMobAdRequest(request);
        ((AdView) findViewById(R.id.ad_main)).loadAd(request);
        this.mDb = new DictDbAdapter(this);
        this.mDb.open();
        this.mBtnSearch = (ImageButton) findViewById(R.id.btn_search);
        this.mBtnRandom = (ImageButton) findViewById(R.id.btn_random);
        this.mBtnBrowse = (ImageButton) findViewById(R.id.btn_browse);
        this.mBtnSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.onSearchRequested();
            }
        });
        this.mBtnRandom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.launchViewTerm(MainActivity.this.mDb.fetchRandomTerm().getId());
            }
        });
        this.mBtnBrowse.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.this.launchListActivity();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mDb.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, FLURRY_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        loadPrefs();
        setupDailyTermNotification();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_opt_settings:
                launchPreferenceActivity();
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);
        }
    }

    private void setupDailyTermNotification() {
        AlarmManager am = (AlarmManager) getSystemService("alarm");
        if (this.mAlarmSender != null) {
            am.cancel(this.mAlarmSender);
        }
        if (this.mPrefDailyTermEnabled) {
            Calendar notifyTime = setupDailyTermNotificationDatetime();
            if (notifyTime != null) {
                this.mAlarmSender = PendingIntent.getService(this, 0, new Intent(this, DictNotificationService.class), 268435456);
                long firstTime = SystemClock.elapsedRealtime() + (notifyTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis());
                Log.d(TAG, "Setting alarm at:" + DateUtil.calToDateTimeStr(notifyTime));
                am.setRepeating(2, firstTime, 86400000, this.mAlarmSender);
                return;
            }
            Log.e(TAG, "Failed to setup notification datetime (preference " + this.mPrefDailyTermTimeOfDay + ")");
        }
    }

    private Calendar setupDailyTermNotificationDatetime() {
        try {
            Calendar now = Calendar.getInstance();
            String currentDateStr = DateUtil.calToDateStr(now);
            Calendar notificationTimeToday = DateUtil.dateTimeStrToCal(String.valueOf(currentDateStr) + " " + this.mPrefDailyTermTimeOfDay);
            if (notificationTimeToday.getTimeInMillis() < now.getTimeInMillis()) {
                notificationTimeToday.add(5, 1);
            }
            Log.d(TAG, "mPrefDailyTermTimeOfDay:" + this.mPrefDailyTermTimeOfDay);
            Log.d(TAG, "currentDate:" + currentDateStr);
            Log.d(TAG, "notificationTimeToday:" + DateUtil.calToDateTimeStr(notificationTimeToday));
            return notificationTimeToday;
        } catch (ParseException e) {
            Log.d(TAG, "setupDailyTermNotificationDatetime Exception:" + e.getMessage());
            return null;
        }
    }

    private void launchPreferenceActivity() {
        startActivity(new Intent(this, DictPreferenceActivity.class));
    }

    /* access modifiers changed from: protected */
    public void launchViewTerm(long id) {
        Intent i = new Intent(this, ViewTerm.class);
        Bundle b = new Bundle();
        b.putLong("id", id);
        i.putExtras(b);
        startActivity(i);
    }

    /* access modifiers changed from: protected */
    public void launchListActivity() {
        startActivity(new Intent(this, SearchActivity.class));
    }

    private void loadPrefs() {
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        this.mPrefDailyTermEnabled = Utils.getPrefBoolean(this, "daily_term_notify", Boolean.valueOf(this.mPrefDailyTermEnabled)).booleanValue();
        this.mPrefDailyTermTimeOfDay = Utils.getPref(this, "daily_term_time", this.mPrefDailyTermTimeOfDay);
        Log.d(TAG, "mPrefDailyTermEnabled:" + this.mPrefDailyTermEnabled);
        Log.d(TAG, "mPrefDailyTermTimeOfDay:" + this.mPrefDailyTermTimeOfDay);
    }
}
