package com.rogansoft.pokerdict;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.google.ads.AdRequest;

public class Utils {
    public static Boolean getPrefBoolean(Context ctx, String key, Boolean defValue) {
        return Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(ctx).getBoolean(key, defValue.booleanValue()));
    }

    public static String getPref(Context ctx, String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(ctx).getString(key, defValue);
    }

    public static void setPref(Context ctx, String key, String value) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ctx).edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static void toastShort(Context con, String msg) {
        Toast.makeText(con, msg, 0).show();
    }

    public static void configureAdMobAdRequest(AdRequest request) {
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        request.addKeyword("poker dictionary");
        request.addKeyword("poker");
        request.addKeyword("texas holdem");
        request.addKeyword("reference");
        request.addKeyword("dictionary");
        request.addKeyword("jargon");
    }
}
