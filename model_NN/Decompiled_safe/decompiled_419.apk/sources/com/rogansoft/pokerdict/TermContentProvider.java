package com.rogansoft.pokerdict;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class TermContentProvider extends ContentProvider {
    public static String AUTHORITY = "com.rogansoft.pokerdict.TermContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/terms");
    public static final String DEFINITION_MIME_TYPE = "vnd.android.cursor.item/vnd.rogansoft.android.pokerdict";
    private static final int GET_TERM = 1;
    private static final int REFRESH_SHORTCUT = 3;
    private static final int SEARCH_SUGGEST = 2;
    private static final int SEARCH_TERMS = 0;
    private static final String TAG = "TermContentProvider";
    public static final String WORDS_MIME_TYPE = "vnd.android.cursor.dir/vnd.rogansoft.android.pokerdict";
    private static final UriMatcher mURIMatcher = buildUriMatcher();
    private DictDbAdapter mDb;

    private static UriMatcher buildUriMatcher() {
        Log.d(TAG, "buildUriMatcher");
        UriMatcher matcher = new UriMatcher(-1);
        matcher.addURI(AUTHORITY, "terms", SEARCH_TERMS);
        matcher.addURI(AUTHORITY, "term/#", 1);
        matcher.addURI(AUTHORITY, "search_suggest_query", 2);
        matcher.addURI(AUTHORITY, "search_suggest_query/*", 2);
        matcher.addURI(AUTHORITY, "search_suggest_shortcut", REFRESH_SHORTCUT);
        matcher.addURI(AUTHORITY, "search_suggest_shortcut/*", REFRESH_SHORTCUT);
        return matcher;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    public String getType(Uri uri) {
        Log.d(TAG, "getType");
        switch (mURIMatcher.match(uri)) {
            case SEARCH_TERMS /*0*/:
                return WORDS_MIME_TYPE;
            case 1:
                return DEFINITION_MIME_TYPE;
            case 2:
                return "vnd.android.cursor.dir/vnd.android.search.suggest";
            case REFRESH_SHORTCUT /*3*/:
                return "vnd.android.cursor.item/vnd.android.search.suggest";
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    public boolean onCreate() {
        this.mDb = new DictDbAdapter(getContext());
        this.mDb.open();
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query uri:" + uri.toString());
        Log.d(TAG, "match result:" + mURIMatcher.match(uri));
        switch (mURIMatcher.match(uri)) {
            case 2:
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        if (selectionArgs != null) {
            return getSuggestions(selectionArgs[SEARCH_TERMS]);
        }
        throw new IllegalArgumentException("selectionArgs must be provided for the Uri: " + uri);
    }

    private Cursor getSuggestions(String query) {
        String query2 = query.toLowerCase();
        String[] columns = new String[REFRESH_SHORTCUT];
        columns[SEARCH_TERMS] = "_id";
        columns[1] = "suggest_text_1";
        columns[2] = "suggest_intent_data_id";
        return this.mDb.fetchSearchSuggestions(query2, columns);
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }
}
