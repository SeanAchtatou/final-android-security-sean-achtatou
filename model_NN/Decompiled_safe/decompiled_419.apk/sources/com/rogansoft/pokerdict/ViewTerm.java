package com.rogansoft.pokerdict;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import com.flurry.android.FlurryAgent;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.rogansoft.pokerdict.domain.Term;

public class ViewTerm extends Activity {
    private static final int ACTIVITY_FB = 1;
    private static final String TAG = "ViewTerm";
    private DictDbAdapter mDb;
    private Term mTerm;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.view_term);
        this.mDb = new DictDbAdapter(this);
        this.mDb.open();
        long id = getIntent().getExtras().getLong("id");
        this.mTerm = this.mDb.fetchTerm(id);
        Log.d(TAG, "id:" + id + " termid:" + this.mTerm.getId() + " term:" + this.mTerm.getTerm());
        ((TextView) findViewById(R.id.tvTerm)).setText(this.mTerm.getTerm());
        ((WebView) findViewById(R.id.wvDefinition)).loadData(this.mTerm.getDefinition(), "text/html", "utf-8");
        AdRequest request = new AdRequest();
        Utils.configureAdMobAdRequest(request);
        ((AdView) findViewById(R.id.ad_view)).loadAd(request);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mDb.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        FlurryAgent.onStartSession(this, MainActivity.FLURRY_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        FlurryAgent.onEndSession(this);
        super.onStop();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_opt_random:
                launchViewTerm(this.mDb.fetchRandomTerm().getId());
                return true;
            case R.id.menu_opt_search:
            case R.id.menu_opt_settings:
            default:
                return super.onMenuItemSelected(featureId, item);
            case R.id.menu_opt_fb:
                launchActivityFb(Html.fromHtml("Poker term: \"" + this.mTerm.getTerm() + "\" - \n\n" + this.mTerm.getDefinition() + "\n #pokerdictionary").toString());
                return true;
            case R.id.menu_opt_share:
                launchActivityShare();
                return true;
        }
    }

    private void launchActivityFb(String message) {
        Intent i = new Intent(this, DictToFacebook.class);
        Bundle b = new Bundle();
        b.putString("message", message);
        i.putExtras(b);
        startActivityForResult(i, 1);
    }

    /* access modifiers changed from: protected */
    public void launchViewTerm(long id) {
        Intent i = new Intent(this, ViewTerm.class);
        Bundle b = new Bundle();
        b.putLong("id", id);
        i.putExtras(b);
        i.setFlags(67108864);
        startActivity(i);
    }

    private void launchActivityShare() {
        Intent i = new Intent("android.intent.action.SEND");
        i.setType("text/plain");
        i.putExtra("android.intent.extra.SUBJECT", "Poker term: \"" + this.mTerm.getTerm() + "\"");
        i.putExtra("android.intent.extra.TEXT", Html.fromHtml("Poker term: \"" + this.mTerm.getTerm() + "\"\n " + this.mTerm.getDefinition() + "\n #pokerdictionary").toString());
        startActivity(Intent.createChooser(i, "Share Poker Term"));
    }
}
