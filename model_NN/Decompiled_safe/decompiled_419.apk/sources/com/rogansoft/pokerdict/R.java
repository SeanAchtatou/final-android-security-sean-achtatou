package com.rogansoft.pokerdict;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int facebook_icon = 2130837504;
        public static final int fb = 2130837505;
        public static final int ic_menu_info_details = 2130837506;
        public static final int ic_menu_list_all = 2130837507;
        public static final int ic_menu_prefs = 2130837508;
        public static final int ic_menu_random_word = 2130837509;
        public static final int ic_menu_search = 2130837510;
        public static final int ic_menu_settings = 2130837511;
        public static final int ic_menu_share = 2130837512;
        public static final int icon = 2130837513;
        public static final int login = 2130837514;
        public static final int login_button = 2130837515;
        public static final int login_down = 2130837516;
        public static final int logout = 2130837517;
        public static final int logout_button = 2130837518;
        public static final int logout_down = 2130837519;
        public static final int notification_icon = 2130837520;
        public static final int poker = 2130837521;
    }

    public static final class id {
        public static final int ad_main = 2131165190;
        public static final int ad_view = 2131165193;
        public static final int btn_browse = 2131165189;
        public static final int btn_random = 2131165188;
        public static final int btn_search = 2131165187;
        public static final int fb_login = 2131165184;
        public static final int fb_txt = 2131165185;
        public static final int menu_opt_fb = 2131165197;
        public static final int menu_opt_random = 2131165194;
        public static final int menu_opt_search = 2131165195;
        public static final int menu_opt_settings = 2131165196;
        public static final int menu_opt_share = 2131165198;
        public static final int postButton = 2131165186;
        public static final int tvTerm = 2131165191;
        public static final int wvDefinition = 2131165192;
    }

    public static final class layout {
        public static final int fb_main = 2130903040;
        public static final int main = 2130903041;
        public static final int search = 2130903042;
        public static final int view_term = 2130903043;
    }

    public static final class menu {
        public static final int list_menu = 2131099648;
        public static final int main_menu = 2131099649;
        public static final int view_menu = 2131099650;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int menu_info = 2131034116;
        public static final int menu_list_all = 2131034118;
        public static final int menu_random = 2131034114;
        public static final int menu_search = 2131034117;
        public static final int menu_settings = 2131034115;
        public static final int search_hint = 2131034113;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
        public static final int searchable = 2130968577;
    }
}
