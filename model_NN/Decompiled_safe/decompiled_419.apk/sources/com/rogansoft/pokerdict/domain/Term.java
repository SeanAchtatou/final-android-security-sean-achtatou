package com.rogansoft.pokerdict.domain;

public class Term {
    private String definition;
    private long id;
    private String term;

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getTerm() {
        return this.term;
    }

    public String getDefinition() {
        return this.definition;
    }

    public Term(long id2, String term2, String definition2) {
        this.id = id2;
        this.term = term2;
        this.definition = definition2;
    }

    private static String ellipsis(String text, int length) {
        if (text.length() > length) {
            return String.valueOf(text.substring(0, length - 3)) + "...";
        }
        return text;
    }

    public String toString() {
        return ellipsis(this.term, 23);
    }
}
