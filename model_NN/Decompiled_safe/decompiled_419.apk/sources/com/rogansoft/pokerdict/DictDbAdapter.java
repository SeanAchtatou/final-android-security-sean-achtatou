package com.rogansoft.pokerdict;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import com.rogansoft.pokerdict.domain.Term;
import java.util.ArrayList;
import java.util.HashMap;

public class DictDbAdapter {
    private static final String DATABASE_NAME = "poker_dict";
    private static final int DATABASE_VERSION = 9;
    private static final String TAG = "DictDbAdapter";
    private static final String TERM_CREATE_QUERY = "create table term (_id integer primary key autoincrement, term text not null, definition text not null );";
    private static final String TERM_TABLE_NAME = "term";
    private static final HashMap<String, String> mColumnMap = buildColumnMap();
    private final Context mCtx;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("_id", "_id");
        map.put(TERM_TABLE_NAME, TERM_TABLE_NAME);
        map.put("definition", "definition");
        map.put("suggest_text_1", "term AS suggest_text_1");
        map.put("suggest_text_2", "definition AS suggest_text_2");
        map.put("suggest_intent_data_id", "_id AS suggest_intent_data_id");
        map.put("suggest_shortcut_id", "_id AS suggest_shortcut_id");
        return map;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DictDbAdapter.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) DictDbAdapter.DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            initialize(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(DictDbAdapter.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS term");
            initialize(db);
        }

        private void initialize(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS term");
            db.execSQL(DictDbAdapter.TERM_CREATE_QUERY);
            initData(db);
        }

        private void initData(SQLiteDatabase db) {
            long unused = DictDbAdapter.createTerm(db, "act", "To make a play (bet, call, raise, or fold) at the required time. ''It is Ted's turn to act.'' Compare to \"in turn\".");
            long unused2 = DictDbAdapter.createTerm(db, "action button", "A marker similar to a kill button, on which a player places an extra forced bet.  In a seven-card stud high-low game, the action button is awarded to the winner of a scoop pot above a certain size, signifying that in the next pot, that player will be required to post an amount representing a completion of the bring-in to a full bet.  For example, in a stud game with $2 and $4 betting limits and a $1 bring-in, a player with the action button must post $2; after the cards are dealt, the player with the low card must still pay the $1 bring-in, then when the betting reaches the player who posted the $2, he is required to leave it in as a raise of the bring-in (and has the option to raise further).  Players in between the bring-in and the action button can just call the bring-in, but they know ahead of time that they will be raised by the action button.");
            long unused3 = DictDbAdapter.createTerm(db, "action card", "In Texas hold 'em or other community card games, a card appearing on the board that causes significant betting action because it helps two or more players.  For example, an ace on the flop when two players each hold an ace.");
            long unused4 = DictDbAdapter.createTerm(db, "action only", "In many cardrooms, with respect to an all-in bet, only a full (or half) bet can be reraised. Anything less than a full (or half) bet is considered to be ''action only'', that is, other players can call the bet but not raise it. For example, Alice bets $100. Bob calls. Carol goes all in for $119. When the action returns to Alice and Bob, they may only call the extra $19; they cannot raise it. Carol's raise is called ''action only''. Compare to \"full bet rule\", \"half bet rule\".");
            long unused5 = DictDbAdapter.createTerm(db, "action", "# A player's turn to act. ''The action is on you''.");
            long unused6 = DictDbAdapter.createTerm(db, "active player", "A player still involved in the pot.  If there are side pots, an all-in player may be active in some pots, but not in others.");
            long unused7 = DictDbAdapter.createTerm(db, "add-on", "In a live game, to buy more chips before you have busted. In tournament play, a single rebuy for which all players are eligible regardless of their stack size. This is usually allowed only once, at the end of the rebuy period.  The add-on often offers more chips per dollar invested than the buyin and rebuys.  Compare with \"rebuy\".");
            long unused8 = DictDbAdapter.createTerm(db, "advertising", "To make an obvious play or expose cards in such a way as to deliberately convey an impression to your opponents about your style of play. For example, to make a bad play or bluff to give the impression that you bluff frequently (hoping opponents will then call your legitimate bets) or to show only good hands to give the impression that you rarely bluff (hoping opponents will then fold when you bet).");
            long unused9 = DictDbAdapter.createTerm(db, "air", "# In lowball, \"giving air\" is letting an opponent who might otherwise fold know that you intend to draw one or more cards to induce him to call.");
            long unused10 = DictDbAdapter.createTerm(db, "angle", "A technically legal, but borderline unethical, play. For example, deliberately miscalling one's own hand to induce a fold, or placing odd amounts of chips in the pot to confuse opponents about whether you mean to call or raise. A player employing such tactics is called an \"angle shooter\".");
            long unused11 = DictDbAdapter.createTerm(db, "ante off", "In tournament play, to force an absent player to continue paying antes, blinds, bring-ins, or other forced bets so that the contest remains fair to the other players. ''Go ahead and take that phone call. We'll ante you off until you get back.''  Also \"blind off\".");
            long unused12 = DictDbAdapter.createTerm(db, "back in", "To enter a pot by checking and then calling someone else's open on the first betting round. Usually used in games like Jackpots, meaning to enter without openers.");
            long unused13 = DictDbAdapter.createTerm(db, "back into", "To win a pot with a hand that would have folded to any bet. For example, two players enter a pot of draw poker, both drawing to flushes. Both miss, and check after the draw. The player with the ace-high draw \"backs into\" winning the pot against the player with only a king-high draw. Also to make a backdoor draw, for example, a player who starts a hand with three of a kind, but makes a runner-runner flush, can be said to back into the flush.");
            long unused14 = DictDbAdapter.createTerm(db, "backdoor", "# A draw requiring two or more rounds to fill. For example, catching two consecutive cards in two rounds of seven-card stud or Texas hold 'em to fill a straight or flush.");
            long unused15 = DictDbAdapter.createTerm(db, "backraise", "A reraise from a player that previously limped in the same betting round. ''I decided to backraise with my pocket eights to isolate the all-in player.''  Also limp-reraise.");
            long unused16 = DictDbAdapter.createTerm(db, "balance", "Balance refers to playing very different hands in the same way, with the aim of making it more difficult for an opponent to gain useful information about the cards a player has, even though on a value basis one would play them differently.");
            long unused17 = DictDbAdapter.createTerm(db, "bank", "Also called the house, the person responsible for distributing chips, keeping track of the buy-ins, and paying winners at the end of the game.");
            long unused18 = DictDbAdapter.createTerm(db, "bankroll", "1. The amount of money that a player has to wager for the duration of his or her poker career.");
            long unused19 = DictDbAdapter.createTerm(db, "behind", "# Not (currently) having the best hand. ''I'm pretty sure my pair of jacks was behind Lou's kings, but I had other outs, so I kept playing.''");
            long unused20 = DictDbAdapter.createTerm(db, "bet", "# Any money wagered during the play of a hand.");
            long unused21 = DictDbAdapter.createTerm(db, "big bet game", "A game played with a no limit or pot limit betting structure.");
            long unused22 = DictDbAdapter.createTerm(db, "big full", "The best possible full house in community card games.  ''I had the big full when the flop came A-A-5 and my hole cards were A-5.''  A stronger hand than the \"underfull\".");
            long unused23 = DictDbAdapter.createTerm(db, "big stack", "A stack of chips that is relatively large for the stakes being played.  Compare with \"short stack\".");
            long unused24 = DictDbAdapter.createTerm(db, "blank", "A card, frequently a community card, of no apparent value. ''I suspected Margaret had a good draw, but the river card was a blank, so I bet again.''  Compare to \"rag\", \"brick\", \"bomb\".");
            long unused25 = DictDbAdapter.createTerm(db, "blaze", "A Non-standard poker hand of five face cards that outranks a flush.");
            long unused26 = DictDbAdapter.createTerm(db, "bleed", "Consistently losing chips through bad play, possibly resulting from tilt. When a player is consistently losing chips, it is referred to as \"bleeding chips\".");
            long unused27 = DictDbAdapter.createTerm(db, "blind off, blinded", "# To \"ante off\".");
            long unused28 = DictDbAdapter.createTerm(db, "blind stud", "A stud poker game in which all cards are dealt face down. Was popular in California before legal rulings made traditional stud legal there.");
            long unused29 = DictDbAdapter.createTerm(db, "blocker", "In community card poker, refers to holding one of the opponent's outs, typically when the board threatens a straight or straight draw. A blocker is also having a combination of cards that turn your opponents outs into your own, such as having four to a straight flush. The two cards to give you a straight flush are blockers against his high flush draw.''The board was A23 but with my pair of fives I held two blockers to the straight.'' Compare to \"dry ace\".");
            long unused30 = DictDbAdapter.createTerm(db, "blocking bet", "An abnormally small bet made by a player out of position intended to block a larger bet by an opponent.");
            long unused31 = DictDbAdapter.createTerm(db, "board", "# The set of community cards in a community card game. ''If another spade hits the board, I'll have to fold.''");
            long unused32 = DictDbAdapter.createTerm(db, "boat", "Another name for Full house");
            long unused33 = DictDbAdapter.createTerm(db, "both ways", "Both halves of a split pot, often declared by a player who thinks he or she will win both low and high.");
            long unused34 = DictDbAdapter.createTerm(db, "bottom end", "The lowest of several possible straights, especially in a community card game.  For example, in Texas hold 'em with the cards 5-6-7 on the board, a player holding 3-4 has the bottom end straight, while a player holding 4-8 or 8-9 has a higher straight.  Also \"idiot end\".");
            long unused35 = DictDbAdapter.createTerm(db, "bottom pair, bottom set", "In a community card game, a pair (or set) made by matching the lowest-ranking board card with one (or two) in one's private hand. Compare second pair, top pair.");
            long unused36 = DictDbAdapter.createTerm(db, "box", "The chip tray in front of a house dealer, and by extension, the house dealer's position at the table. ''You've been in the box for an hour now; don't you get a break?''");
            long unused37 = DictDbAdapter.createTerm(db, "boxed card", "A card encountered face-up in the assembled deck during the deal, as opposed to one overturned in the act of dealing. Most house rules treat a boxed card as if it didn't exist; that is, it is placed aside and not used. Different rules cover cards exposed during the deal.");
            long unused38 = DictDbAdapter.createTerm(db, "break", "# In a draw poker game, to discard cards that make a made hand in the hope of making a much better one. For example, a player with J-J-10-9-8 may wish to break his pair of jacks to draw for the straight, and a lowball player may break his 9-high 9-5-4-2-A to draw for the wheel. In a Jacks-or-better draw game, a player breaking a high pair must keep the discarded card aside, to prove he had openers.");
            long unused39 = DictDbAdapter.createTerm(db, "brick & mortar", "A ''brick & mortar'' or ''B&M'' casino is a term referring to a \"real\" casino based in a building, as opposed to an online casino.  This term is used to refer to many real world locations vs. their Internet counterparts. It is not just a poker term or even a gambling term; it is often used in e-commerce in similar situations.");
            long unused40 = DictDbAdapter.createTerm(db, "brick", "A \"blank\", though more often used in the derogatory sense of a card that is undesirable rather than merely inconsequential, such as a card of high rank or one that makes a pair in a low-hand game. Also known as a bomb. Compare to \"rags\".");
            long unused41 = DictDbAdapter.createTerm(db, "bridge order", "Poker is neutral about suits. A spade flush and a club flush with all ranks matching is a tie. But in determining the dealer at the start of a game, or in determining the bringin bettor in a stud game, bridge rank rules: Spades beat hearts beat diamonds beat clubs.  It's convenient but coincidental that this works out to reverse alphabetical order.");
            long unused42 = DictDbAdapter.createTerm(db, "bring in", "# To open a betting round. ''Alice brought it in for $4, and Bob raised to $10.''");
            long unused43 = DictDbAdapter.createTerm(db, "broadway", "A 10 through ace straight.");
            long unused44 = DictDbAdapter.createTerm(db, "brush", "# A casino employee whose job it is to greet players entering the poker room, maintain the list of persons waiting to play, announce open seats, and various other duties (including brushing off tables to prepare them for new games, hence the name).");
            long unused45 = DictDbAdapter.createTerm(db, "bubble", "The last finishing position in a poker tournament before entering the payout structure. ''He was very frustrated after getting eliminated on the bubble.''  Also can be applied to other situations like if six players will make a televised final table the player finishing seventh will go out on the \"TV bubble\". Also used to describe any situation close to the payout structure.");
            long unused46 = DictDbAdapter.createTerm(db, "bully", "A player who raises frequently to force out more cautious players, especially one with a large stack for the size of the game (a \"big stack\" bully).<ref>[http://www.pokerlistings.com/blog/pro-tips/being-a-bully Gus Hansen]</ref>");
            long unused47 = DictDbAdapter.createTerm(db, "busted", "# Not complete, such as four cards to a straight that never gets the fifth card to complete it.");
            long unused48 = DictDbAdapter.createTerm(db, "buy short", "To buy into a game for an amount smaller than the normal buy-in. Some casinos allow this under certain circumstances, such as after having lost a full buy-in, or if all players agree to allow it.");
            long unused49 = DictDbAdapter.createTerm(db, "buy the button", "# A rule originating in northern California casinos in games played with blinds, in which a new player sitting down with the button to his right (who would normally be required to sit out a hand as the button passed him, then post to come in) may choose to pay the amount of both blinds for this one hand (the amount of the large blind playing as a live blind, and the amount of the small blind as dead money), play this hand, and then receive the button on the next hand as if he had been playing all along. See public cardroom rules.");
            long unused50 = DictDbAdapter.createTerm(db, "buy the pot", "Making a bet when no one else is betting so as to force the other players to fold, thus winning the pot uncontested. A specialized version of this is \"buying the blinds\" by making a large raise in the first round forcing all other players out of the game.");
            long unused51 = DictDbAdapter.createTerm(db, "buy-in", "The minimum required amount of chips that must be \"bought\" to become involved in a game (or tournament). For example, a $4-$8 fixed limit game might require a player to buy at least $40 worth of chips. This is typically far less than an average player would expect to play with for any amount of time, but large enough that the player can play a number of hands without buying more, so the game isn't slowed down by constant chip-buying.");
            long unused52 = DictDbAdapter.createTerm(db, "call the clock", "A method of discouraging players from taking an excessively long time to act. When someone calls the clock, the player has a set amount of time in which to make up his mind; if he fails to do so, his hand is immediately declared dead. In tournament play, a common rule is that if a player takes too long and no one calls the clock, the dealer or floor personnel will automatically do so.");
            long unused53 = DictDbAdapter.createTerm(db, "cap game", "Similar to \"cap\" above, but used to describe a no-limit or pot limit game with a cap on the amount that a player can bet during the course of a hand. Once the cap is reached, all players remaining in the hand are considered all-in. For example, a no limit game could have a betting cap of 30 times the big blind.<ref></ref>");
            long unused54 = DictDbAdapter.createTerm(db, "cap", "A limit on the number of raises allowed in a betting round. Typically three or four (in addition the opening bet). In most casinos, the cap is removed if there are only two players remaining either (1) at the beginning of the betting round, or (2) at the time that what would have otherwise been the last raise is made.");
            long unused55 = DictDbAdapter.createTerm(db, "case card", "The last available card of a certain description (typically a rank). ''The only way I can win is to catch the case king.'', meaning the only king remaining in the deck.");
            long unused56 = DictDbAdapter.createTerm(db, "cash plays", "An announcement, usually by a dealer, that a player requested to buy chips and can bet the cash he has on the table in lieu of chips until he receives his chips. In many card rooms, it also refers to the policy that $100 bills may remain on the table and considered to be \"in play\" in cash form, rather than converted to chips.");
            long unused57 = DictDbAdapter.createTerm(db, "cashing out", "Exchanging chips for cash when leaving a game.");
            long unused58 = DictDbAdapter.createTerm(db, "cashing", "Winning a share of the prize money in a tournament.");
            long unused59 = DictDbAdapter.createTerm(db, "catch perfect", "To catch the only two possible cards that will complete a hand and win the pot, usually those leading to a straight flush.  Usually used in Texas hold 'em.  Compare with \"runner-runner\".");
            long unused60 = DictDbAdapter.createTerm(db, "catch up", "To successfully complete a draw, thus defeating a player who previously had a better hand. ''I was sure I had Alice beat, but she caught up when that spade fell.''");
            long unused61 = DictDbAdapter.createTerm(db, "catch", "To receive needed cards on a draw. ''I'm down 300--I can't catch anything today.'' or ''Joe caught his flush early, but I caught the boat on seventh street to beat him.'' Often used with an adjective to further specify, for example \"catch perfect\", \"catch inside\", \"catch smooth\".");
            long unused62 = DictDbAdapter.createTerm(db, "center pot", "The main pot in a table stakes game where one or more players are all in.");
            long unused63 = DictDbAdapter.createTerm(db, "chase", "# To call a bet to see the next card when holding a drawing hand when the pot odds do not merit it.");
            long unused64 = DictDbAdapter.createTerm(db, "check out", "To fold, in turn, even though there is no bet facing the player. In some games this is considered a breach of etiquette equivalent to folding out of turn.  In others it is permitted, but frowned upon.");
            long unused65 = DictDbAdapter.createTerm(db, "chip dumping", "A form of collusion that happens during tournaments, especially in the early rounds. Two or more players decide to go all-in early. The winner gets a large amount of chips, which increases the player's chance of cashing. The winnings are then split among the colluders.");
            long unused66 = DictDbAdapter.createTerm(db, "chip leader", "The player currently holding the most chips in a tournament (or occasionally a live no limit game).");
            long unused67 = DictDbAdapter.createTerm(db, "chip up", "# To exchange lower-denomination chips for higher-denomination chips. In tournament play, the term means to remove all the small chips from play by rounding up any odd small chips to the nearest large denomination, rather than using a chip race.");
            long unused68 = DictDbAdapter.createTerm(db, "chop", "# To split a pot because of a tie, split-pot game, or player agreement.");
            long unused69 = DictDbAdapter.createTerm(db, "click raise", "Making the minimum raise. Referring to online poker when you click the raise button without specifying the amount of raise.");
            long unused70 = DictDbAdapter.createTerm(db, "coffee housing", "Talking in an attempt to mislead other players about the strength of a hand. For example a player holding A-A as their first two cards might say \"lets gamble here\", implying a much weaker holding. Coffee housing is considered bad etiquette in the UK, but not in the USA. This is also called speech play.");
            long unused71 = DictDbAdapter.createTerm(db, "coin flip", "A situation where two players have, perhaps wisely, invested all their money in the pot and it's a roughly even chance which of them wins. A-K against a small pair is a common case; the A-K is only a modest dog.  Also \"race.");
            long unused72 = DictDbAdapter.createTerm(db, "cold call", "To call an amount that represents a sum of bets or raises by more than one player. ''Alice opened for $10, Bob raised another $20, and Carol cold called the $30''. Compare to \"flat call\", \"overcall\".");
            long unused73 = DictDbAdapter.createTerm(db, "cold deck", "A deck that has been intentionally rigged ('stacked') such that some player or players cannot win.");
            long unused74 = DictDbAdapter.createTerm(db, "color change, color up", "To exchange small-denomination chips for larger ones.");
            long unused75 = DictDbAdapter.createTerm(db, "combo, combination game", "A casino table at which multiple forms of poker are played in rotation.");
            long unused76 = DictDbAdapter.createTerm(db, "come bet, on the come", "A bet or raise made with a drawing hand, building the pot in anticipation of filling the draw. Usually a weak \"gambler's\" play, but occasionally correct with a very good draw and large pot or as a semi-bluff.");
            long unused77 = DictDbAdapter.createTerm(db, "completion", "To raise a small bet up to the amount of what would be a normal-sized bet. For example, in a $2/$4 stud game with $1 bring-in, a player after the bring-in may raise it to $2, completing what would otherwise be a sub-minimum bet up to the normal minimum. Also in limit games, if one player raises all in for less than the normally required minimum, a later player might complete the raise to the normal minimum (depending on house rules). See table stakes.");
            long unused78 = DictDbAdapter.createTerm(db, "connectors", "Two or more cards of consecutive or close to consecutive rank.");
            long unused79 = DictDbAdapter.createTerm(db, "continuation bet", "A bet made after the flop by the player who took the lead in betting before the flop (Texas hold 'em and Omaha hold 'em). Compare to \"probe bet\".");
            long unused80 = DictDbAdapter.createTerm(db, "cooler", "A case in which playing a strong hand (often the second best) that normally justifies the maximum bet is beaten by a still stronger hand.");
            long unused81 = DictDbAdapter.createTerm(db, "countdown", "# The act of counting the cards that remain in the stub after all cards have been dealt, done by a dealer to ensure that a complete deck is being used.");
            long unused82 = DictDbAdapter.createTerm(db, "cow", "A player with whom one is sharing a buy-in, with the intent to split the result after play.  To \"go cow\" is to make such an arrangement.");
            long unused83 = DictDbAdapter.createTerm(db, "cripple", "In some community card games, to ''cripple the deck'' means to have a hand that is virtually impossible for anyone else to catch up to.  For example, in Texas hold 'em, if a player's hole cards are '''A-T''' and the flop is '''A-A-T''' the player has \"crippled the deck\"; though that player's hand is high (probably unbeatable), other players are unlikely to see any possibility for improvement and will probably fold. Such a hand generally doesn't gain much money for the player holding such a hand, however it is possible to win a large amount through slow play.");
            long unused84 = DictDbAdapter.createTerm(db, "crying call", "Calling when a player thinks he does not have the best hand.");
            long unused85 = DictDbAdapter.createTerm(db, "cut card", "A distinctive card, usually stiff solid-colored plastic, held against the bottom of the deck during the deal to prevent observation of the bottom card.");
            long unused86 = DictDbAdapter.createTerm(db, "cutoff", "The seat immediately to the right of the dealer button. In home games where the player on the button actually shuffles and deals the cards, the player in the cutoff seat cuts the deck (hence the name).");
            long unused87 = DictDbAdapter.createTerm(db, "dark", "Describing an action taken before receiving information to which the player would normally be entitled. ''I'm drawing three, and I check in the dark.'' Compare to \"blind\".");
            long unused88 = DictDbAdapter.createTerm(db, "dead blind", "A blind that is not \"live\", in that the player posting it does not have the option to raise if other players just call. Usually refers to a small blind posted by a player entering, or returning to, a game (in a position other than the big blind) that is posted in addition to a live blind equal to the big blind.");
            long unused89 = DictDbAdapter.createTerm(db, "dead hand", "A player's hand that is not entitled to participate in the deal for some reason, such as having been fouled by touching another player's cards, being found to contain the wrong number of cards, being dealt to a player who did not make the appropriate forced bets, etc.");
            long unused90 = DictDbAdapter.createTerm(db, "deal twice", "In a cash game, when two players are involved in a large pot and one is all-in, they might agree to deal the remaining cards twice. If one player wins both times he wins the whole pot, but if both players win one hand they split the pot. Also, \"play twice\".");
            long unused91 = DictDbAdapter.createTerm(db, "deal", "# To distribute cards to players in accordance with the rules of the game being played.");
            long unused92 = DictDbAdapter.createTerm(db, "dealer", "# The person dealing the cards. ''Give Alice the cards, she's the dealer.''");
            long unused93 = DictDbAdapter.createTerm(db, "deuce", "# A 2-spot card. Also called a duck, quack, or swan.");
            long unused94 = DictDbAdapter.createTerm(db, "dirty stack", "A stack of chips apparently of a single denomination, but with one or more chips of another. Usually the result of inattention while stacking a pot, but may also be an intentional deception.");
            long unused95 = DictDbAdapter.createTerm(db, "discard", "To take a previously dealt card out of play. The set of all discards for a deal is called the \"muck\" or the \"deadwood\".");
            long unused96 = DictDbAdapter.createTerm(db, "dominated hand", "A hand that is extremely unlikely to win against another specific hand, even though it may not be a poor hand in its own right. Most commonly used in Texas hold 'em.  A hand like A-Q, for example, is a good hand in general but is dominated by A-K, because whenever the former makes a good hand, the latter is likely to make a better one. A hand like 7-8 is a poor hand in general, but is not dominated by A-K because it makes different kinds of hands. See also domination.");
            long unused97 = DictDbAdapter.createTerm(db, "door card", "# In a stud game, a player's first face-up card. ''Patty paired her door card on fifth street and raised, so I put her on trips.''");
            long unused98 = DictDbAdapter.createTerm(db, "double belly buster straight draw", "a combination of hole cards and exposed cards in holdem or stud games which does not include 4 connected cards, but where there are two different ranks of card that complete a straight. An example would be where the combination of hole cards and the flop is J9875.");
            long unused99 = DictDbAdapter.createTerm(db, "double raise", "The minimum raise in a no-limit or pot-limit game, raising by just the amount of the current bet.");
            long unused100 = DictDbAdapter.createTerm(db, "double suited", "Used to describe an Omaha hold 'em starting hand where two pairs of suited cards are held, e.g. two spades and two diamonds. May be abbreviated \"ds\" in written descriptions.  ''AAJT (ds) is widely considered a premium pot-limit Omaha hold 'em starting hand.''");
            long unused101 = DictDbAdapter.createTerm(db, "double up, double through", "In a big bet game, to bet all of one's chips on one hand against a single opponent (who has an equal or larger stack) and win, thereby doubling your stack. ''I was losing a bit, but then I doubled through Sarah to put me in good shape.''");
            long unused102 = DictDbAdapter.createTerm(db, "double-ace flush", "Under unconventional rules, a flush with one or more wild cards in which they play as aces, even if an ace is already present.");
            long unused103 = DictDbAdapter.createTerm(db, "double-board, double-flop", "Any of several community card game variants (usually Texas hold 'em) in which two separate boards of community cards are dealt simultaneously, with the pot split between the winning hands using each board.");
            long unused104 = DictDbAdapter.createTerm(db, "double-draw", "Any of several Draw poker games in which the draw phase and subsequent betting round are repeated twice.");
            long unused105 = DictDbAdapter.createTerm(db, "downcard", "A card that is dealt facedown.");
            long unused106 = DictDbAdapter.createTerm(db, "drag light", "To pull chips away from the pot to indicate that you don't have enough money to cover a bet. If you win, the amount is ignored. If you lose, you must cover the amount from your pocket. This is not allowed at any casino or any but the most casual home games; see table stakes.");
            long unused107 = DictDbAdapter.createTerm(db, "drawing dead", "# Playing a drawing hand that will lose even if successful (a state of affairs usually only discovered after the fact or in a tournament when two or more players are \"all in\" and they show their cards). ''I caught the jack to make my straight, but Rob had a full house all along, so I was drawing dead.''");
            long unused108 = DictDbAdapter.createTerm(db, "drawing live", "Not drawing dead; that is, drawing to a hand that will win if successful.");
            long unused109 = DictDbAdapter.createTerm(db, "drawing thin", "Not drawing completely dead, but chasing a draw in the face of poor odds. Example: a player who will only win by catching 1 or 2 specific cards is said to be drawing thin. Profitable drawing thin requires large pot odds.");
            long unused110 = DictDbAdapter.createTerm(db, "drop", "# To fold.");
            long unused111 = DictDbAdapter.createTerm(db, "dry ace", "In Omaha hold 'em or Texas hold 'em, refers to an ace in one's hand without another card of the same suit. Used especially to describe the situation where the board presents a flush possibility, when the player does not in fact have a flush, but holding the ace presents some bluffing or semi-bluffing opportunity and a redraw in case the flush draw comes on turn. Compare to \"blocker\".");
            long unused112 = DictDbAdapter.createTerm(db, "dry pot", "A side pot with no money created when a player goes all in and is called by more than one opponent, but not raised. If subsequent betting occurs, the money will go to the dry pot.");
            long unused113 = DictDbAdapter.createTerm(db, "duplicate", "To counterfeit, especially when the counterfeiting card matches one already present in the one's hand.");
            long unused114 = DictDbAdapter.createTerm(db, "eight or better", "A common qualifier in High-low split games that use Ace-5 ranking. Only hands where the highest card is an eight or smaller can win the low portion of the pot.");
            long unused115 = DictDbAdapter.createTerm(db, "equity", "One's mathematical expected value from the current deal, calculated by multiplying the amount of money in the pot by one's probability of winning. For example, if the pot currently contains $100, and you estimate that you have a one in four chance of winning it, then your equity in the pot is $25. If a split is possible, the equity also includes the probability of winning a split times the size of that split; for example, if the pot has $100, and you have a 1/4 chance of winning and a 1/5 chance of taking a $50 split, your equity is $25 + $10 = $35.");
            long unused116 = DictDbAdapter.createTerm(db, "exposed card", "A card whose face has been deliberately or accidentally revealed to players normally not entitled to that information during the play of the game. Various games have different rules about how to handle this irregularity. Compare to \"boxed card\".");
            long unused117 = DictDbAdapter.createTerm(db, "family pot", "A deal in which every (or almost every) seated player called the first opening bet.");
            long unused118 = DictDbAdapter.createTerm(db, "fast", "Aggressive play. ''I was afraid of too many chasers, so I played my trips fast.'' Compare to \"speeding\".");
            long unused119 = DictDbAdapter.createTerm(db, "favorite", "A hand which when matched against another in a showdown has an advantage odds-wise over the other. A hand can be called a small or a big favorite depending on how much it is dominating the other. Contrast \"underdog\" where the situations are reversed. Favorites are usually used but not exclusively comparing how 2 hole cards do against 2 other hole cards pre-flop.");
            long unused120 = DictDbAdapter.createTerm(db, "feeder", "In a casino setting, a second or third table playing the same game as a \"main\" table, and from which players move to the main game as players there leave. Also called a \"must-move table.");
            long unused121 = DictDbAdapter.createTerm(db, "felt", "The cloth covering of a poker table, whatever the actual material. Metaphorically, the table itself: ''Doyle and I have played across the felt''. Also used to refer to table felt made visible by being uncluttered with chips from a player having lost them all or taken all of an opponent's. ''I felted Carla when I filled up against her flush.''");
            long unused122 = DictDbAdapter.createTerm(db, "field", "# All players as a collective in a large tournament: ''There were many professionals amongst the field of the Main Event.''");
            long unused123 = DictDbAdapter.createTerm(db, "fifth street", "# The last card dealt to the board in community card games.  Also see river.");
            long unused124 = DictDbAdapter.createTerm(db, "fill, fill up", "To successfully draw to a hand that needs one card to complete it, by getting the last card of a straight, flush, or full house. ''Jerry made his flush when I was betting my kings up, but I filled on seventh street to catch up.''");
            long unused125 = DictDbAdapter.createTerm(db, "final table", "The last table in a multi-table poker tournament. The final table is set when a sufficient amount of people have been eliminated from the tournament leaving an exact amount of players to occupy one table (typically no more than ten players).");
            long unused126 = DictDbAdapter.createTerm(db, "first position", "The playing position to the direct left of the blinds in Texas hold 'em or Omaha hold 'em, also known as \"under the gun.\" The player in first position must act first on the first round of betting.");
            long unused127 = DictDbAdapter.createTerm(db, "fish", "A poor player.  See also Donkey.");
            long unused128 = DictDbAdapter.createTerm(db, "five of a kind", "A hand possible only in games with wild cards, or a game with more than one deck, defeating all other hands, comprising five cards of equal rank.");
            long unused129 = DictDbAdapter.createTerm(db, "flash", "# Any card which becomes briefly exposed by accident to at least 1 player must be shown to all the players by the dealer during dealing. The card is said to be \"flashed\" to all players before being discarded to the muck pile. See also exposed.");
            long unused130 = DictDbAdapter.createTerm(db, "flat call", "A call, in a situation where one might be expected to raise. ''Normally I raise with jacks, but with three limpers ahead of me I decided to flat call.'' Also \"smooth call\". Compare to \"cold call\", \"overcall\". See slow play.");
            long unused131 = DictDbAdapter.createTerm(db, "float", "Calling a bet with the intention of bluffing on a later betting round. A player might do this when he suspects an opponent of making a continuation bet on the flop in the hopes that the bettor will give up his unimproved hand and check on the turn, allowing the caller to bet with a weak hand and hopefully take the pot away from the preflop aggressor. ''We are floating over the other guys flop bet looking for an opportunity to take the pot.''");
            long unused132 = DictDbAdapter.createTerm(db, "floorman, floorperson", "A casino employee whose duties include adjudicating player disputes, keeping games filled and balanced, and managing dealers and other personnel. Players may shout \"floor!\" to call for a floorperson to resolve a dispute, to ask for a table or seat change, or to ask for some other casino service.");
            long unused133 = DictDbAdapter.createTerm(db, "flop game", "A community card game.");
            long unused134 = DictDbAdapter.createTerm(db, "flop", "The dealing of the first three face-up cards to the board, or to those three cards themselves.  Also see turn and river.");
            long unused135 = DictDbAdapter.createTerm(db, "fold equity", "The portion of the pot one expects to win, on average, by a bet that induces your opponents to fold, rather than seeing the showdown. For example, if your opponent folds 50%25 of the time to bets in situations like this, your fold equity = (current pot size) * (0.50).  See also equity.");
            long unused136 = DictDbAdapter.createTerm(db, "forced-move", "In a casino where more than one table is playing the same game with the same betting structure, one of the tables may be designated the \"main\" table, and will be kept full by requiring a player to move from one of the feeder tables to fill any vacancies. Players will generally be informed that their table is a \"forced-move\" table to be used in this way before they agree to play there. Also \"must-move\".");
            long unused137 = DictDbAdapter.createTerm(db, "forward motion", "A house rule of some casinos states that if a player in turn picks up chips from his stack and moves his hand toward the pot (\"forward motion with chips in hand\"), this constitutes a commitment to bet (or call), and the player may not withdraw his hand to check or fold. Such a player still has the choice of whether to call or raise. Compare to \"string bet\".");
            long unused138 = DictDbAdapter.createTerm(db, "fouled hand", "A hand that is ruled unplayable because of an irregularity, such as being found with too many or too few cards, having been mixed with cards of other players or the muck, having fallen off the table, etc. Compare to \"dead hand\".");
            long unused139 = DictDbAdapter.createTerm(db, "four-flush", "Four cards of the same suit. A non-standard poker hand in some games, an incomplete drawing hand in most. See Four flush.");
            long unused140 = DictDbAdapter.createTerm(db, "four-straight", "Four cards in rank sequence; either an open-ender or one-ender. A non-standard poker hand in some games, an incomplete drawing hand in most. Sometimes \"four to a straight\".");
            long unused141 = DictDbAdapter.createTerm(db, "fourth street", "# The fourth card dealt to the board in community card games. Also \"turn\".");
            long unused142 = DictDbAdapter.createTerm(db, "free card", "A card dealt to one's hand (or to the board of community cards) after a betting round in which no player opened. One is thereby being given a chance to improve one's hand without having to pay anything. ''I wasn't sure my hand was good, but I bet so I wouldn't give a free card to Bill's flush draw.''");
            long unused143 = DictDbAdapter.createTerm(db, "freezeout", "The most common form of tournament. There's no rebuy, play continues until one player has all the chips.");
            long unused144 = DictDbAdapter.createTerm(db, "full bet rule", "In some casinos, the rule that a player must wager the full amount required in order for his action to constitute a raise. For example, in a game with a $4 fixed limit, a player facing an opening bet of $4 who wagers $7 is deemed to have flat called, because $8 is required to raise. Compare to \"half bet rule\". See Public cardroom rules and \"All in\" betting.");
            long unused145 = DictDbAdapter.createTerm(db, "full ring", "A full ring game is a cash game with more than six players involved, typically nine to eleven. This term is normally used in the context of online poker. Compare to \"shorthanded\".");
            long unused146 = DictDbAdapter.createTerm(db, "gap hand", "In Texas hold 'em, a ''gap hand'' is a starting hand with at least one rank separating the two cards.  Usually referred to in context of ''one-gap'' and ''two-gap'' hands.");
            long unused147 = DictDbAdapter.createTerm(db, "get away", "To fold a good hand against a supposedly superior hand.  Compare with laydown.");
            long unused148 = DictDbAdapter.createTerm(db, "going south", "To sneak a portion of your chips from the table while the game is underway.  The intent is to reduce the stakes you have at risk.  Normally prohibited in public card rooms. Also \"ratholing\".");
            long unused149 = DictDbAdapter.createTerm(db, "grinder", "A player who earns a living by making small profits over a long period of consistent, conservative play. Compare to \"rock\".");
            long unused150 = DictDbAdapter.createTerm(db, "guts, guts to open", "# A game with no opening hand requirement; that is, where the only requirement to open the betting is \"guts\", or courage.");
            long unused151 = DictDbAdapter.createTerm(db, "gypsy", "To enter the pot cheaply by just calling the blind rather than raising. Also \"limp\".");
            long unused152 = DictDbAdapter.createTerm(db, "half bet rule", "In some casinos, the rule that placing chips equal to or greater than half the normal bet amount beyond the amount required to call constitutes a commitment to raise the normal amount. For example, in a game with a $4 fixed limit, a player facing a $4 opening bet who places $6 in the pot is deemed to have raised, and must complete his bet to $8. Compare to \"full bet rule\". See Public cardroom rules and \"all in\" betting.");
            long unused153 = DictDbAdapter.createTerm(db, "hand history", "The textual representation of a hand (or hands) played in an Internet cardroom. See Poker tools.");
            long unused154 = DictDbAdapter.createTerm(db, "hanger", "When the bottom card of the deck sticks out beyond the others, an unwanted tell that the dealer is dealing from the bottom of the deck.");
            long unused155 = DictDbAdapter.createTerm(db, "heads up", "Playing against a single opponent. ''After Lori folded, Frank and I were heads up for the rest of the hand.''");
            long unused156 = DictDbAdapter.createTerm(db, "high card", "# A no pair hand, ranked according to its highest-ranking cards.");
            long unused157 = DictDbAdapter.createTerm(db, "high hand, high", "The best hand using traditional poker hand values, as opposed to lowball. Used especially in high-low split games.");
            long unused158 = DictDbAdapter.createTerm(db, "hijack seat", "The seat to the right of the cutoff seat, or second to the right of the button.");
            long unused159 = DictDbAdapter.createTerm(db, "hole cam", "A camera that displays a player's face-down cards (\"hole cards\") to television viewers. Also \"pocket cam\" or \"lipstick cam\".");
            long unused160 = DictDbAdapter.createTerm(db, "hole cards, hole", "# Face-down cards. Also \"pocket cards\". ''I think Willy has two more queens in the hole.''");
            long unused161 = DictDbAdapter.createTerm(db, "home game", "A game played at a private venue (usually the home of one of the players), as opposed to a casino or public cardroom.");
            long unused162 = DictDbAdapter.createTerm(db, "horse", "A player financially backed by someone else. ''I lost today, but Larry was my horse in the stud game, and he won big.''  Compare with \"bankroll\" and \"staking\".");
            long unused163 = DictDbAdapter.createTerm(db, "ignorant end, idiot end", "In flop games, a player drawing to, or even flopping, a straight with undercards to the flop has the idiot end of it.  A player with 8-9 betting on a flop of A-T-J puts himself at great risk, because many of the cards that complete his straight give credible opponents higher ones.");
            long unused164 = DictDbAdapter.createTerm(db, "improve", "To achieve a better hand than one currently holds by adding or exchanging cards as provided in the rules of the game being played. ''I didn't think Paula was bluffing, so I decided not to call unless I improved on the draw.''");
            long unused165 = DictDbAdapter.createTerm(db, "in position, IP", "A player is said to be in position, if the player is last to act on the flop, turn and river betting rounds.  Compare to out of position.");
            long unused166 = DictDbAdapter.createTerm(db, "in the middle", "# In a game with multiple blinds, an incoming player may sometimes be allowed to post the blinds \"in the middle\" (that is, out of their normal order) rather than having to wait for them to pass.");
            long unused167 = DictDbAdapter.createTerm(db, "in the money", "To place high enough in a poker tournament to get prize money. Also \"ITM\".");
            long unused168 = DictDbAdapter.createTerm(db, "in turn", "A player, or an action, is said to be in turn if that player is expected to act next under the rules. ''Jerry said \"check\" while he was in turn, so he's not allowed to raise.''");
            long unused169 = DictDbAdapter.createTerm(db, "insurance", "A \"business\" deal in which players agree to split or reduce a pot (roughly in proportion to the chances of each of them winning) with more cards to come rather than playing out the hand, or else a deal where one player makes a side bet against himself with a third party to hedge against a large loss.");
            long unused170 = DictDbAdapter.createTerm(db, "irregular declaration", "An action taken by a player in turn that is not a straightforward declaration of intent, but that is reasonably interpreted as an action by other players, such as pointing a thumb up to signify \"raise\". House rules or dealer discretion may determine when such actions are meaningful and/or binding.");
            long unused171 = DictDbAdapter.createTerm(db, "jackpot", "# A game of \"jackpot poker\" or \"jackpots\", which is a variant of five-card draw with an ante from each player, no blinds, and an opening requirement of a pair of jacks or better.");
            long unused172 = DictDbAdapter.createTerm(db, "joker", "A 53rd card used mostly in draw games. The joker may usually be used as an Ace, or a card to complete a straight or flush, in high games, and as the lowest card not already present in a hand at low. See bug. A joker may give a player a great many outs.");
            long unused173 = DictDbAdapter.createTerm(db, "juice", "Money collected by the house. Also \"vig\", \"vigorish\". See rake.");
            long unused174 = DictDbAdapter.createTerm(db, "junk", "A hand with little expected value.");
            long unused175 = DictDbAdapter.createTerm(db, "kitty", "A pool of money built by collecting small amounts from certain pots, often used to buy refreshments, cards, and so on. The home-game equivalent of a rake.");
            long unused176 = DictDbAdapter.createTerm(db, "lag", "A \"loose aggressive\" style of play in which a player plays a lot of starting hands and makes many small raises in hopes of out-playing his opponents.");
            long unused177 = DictDbAdapter.createTerm(db, "last to act", "A player is last to act if all players between the player and the button have folded.");
            long unused178 = DictDbAdapter.createTerm(db, "laydown", "A tough choice to fold a good hand in anticipation of superior opposition.");
            long unused179 = DictDbAdapter.createTerm(db, "lead", "The player who makes the last bet or raise in a round of betting is said to have the lead at the start of the next round. Can also be used as a verb meaning to bet out into the pot, \"to lead into the pot.");
            long unused180 = DictDbAdapter.createTerm(db, "leg-up, leg-up button", "The button used to signify who has won the previous hand in a kill game.  Winning a pot in a \"2 consecutive pots\" kill game with the leg-up button in front of you, results in a kill.");
            long unused181 = DictDbAdapter.createTerm(db, "level", "Used in tournament play to refer to the size of the blinds which are periodically increased. For example, in the first level the small blind / big blind may be $50 / $100, in the second level the blinds may be $100 / $200.");
            long unused182 = DictDbAdapter.createTerm(db, "limit", "# The minimum or maximum amount of a bet.");
            long unused183 = DictDbAdapter.createTerm(db, "limp, limp in", "To enter a pot by simply calling the bet to them instead of raising, called so because a player with a marginal hand may be willing to pay the minimum to see more cards, but would likely fold if the bet increased further.");
            long unused184 = DictDbAdapter.createTerm(db, "limp-reraise", "A reraise from a player that previously limped in the same betting round. ''I decided to limp-reraise with my pocket eights to isolate the all-in player.''  Also backraise.");
            long unused185 = DictDbAdapter.createTerm(db, "live bet", "A bet posted by a player under conditions that give him the option to raise even if no other player raises first; typically because it was posted as a blind or straddle, or to enter a new game.");
            long unused186 = DictDbAdapter.createTerm(db, "live cards", "In stud poker games, cards that will improve your hand that have not been seen among anyone's upcards, and are therefore presumably still available. In games such as Texas hold 'em, a player's hand is said to contain \"live\" cards if matching either of them on the board would give that player the lead over his opponent. Typically used to describe a hand that is weak, but not dominated.");
            long unused187 = DictDbAdapter.createTerm(db, "live game", "A game with a lot of action, usually including many unskilled players, especially maniacs. See also ''live poker'', below.");
            long unused188 = DictDbAdapter.createTerm(db, "live hand", "A hand still eligible to win the pot; one with the correct number of cards that has not been mucked or otherwise invalidated.");
            long unused189 = DictDbAdapter.createTerm(db, "live poker", "A retronym for poker played with at a table with cards, as opposed to video poker or online poker.");
            long unused190 = DictDbAdapter.createTerm(db, "lock up", "To \"lock up\" a seat in a cash game means to place a poker chip, player's card, or other personal effect on the table in front of the seat, to signify that the seat is occupied even though the player may not be present.");
            long unused191 = DictDbAdapter.createTerm(db, "low", "# The lowest card by rank.");
            long unused192 = DictDbAdapter.createTerm(db, "maniac", "A very loose and aggressive player, who bets and raises frequently, and often in situations where it is not good strategy to do so.  Opposite of rock.");
            long unused193 = DictDbAdapter.createTerm(db, "match the pot", "To put in an amount equal to all the chips in the pot.");
            long unused194 = DictDbAdapter.createTerm(db, "micro-limit", "Internet poker games with stakes so small that real cardrooms couldn't possibly profit from them, are said to be at the \"micro-limit\" level (e.g. 25-50).");
            long unused195 = DictDbAdapter.createTerm(db, "middle pair", "In a community card game, making a pair with neither the highest nor lowest card of the community cards. See also second pair.");
            long unused196 = DictDbAdapter.createTerm(db, "misdeal", "A deal which is ruined for some reason and must be redealt.");
            long unused197 = DictDbAdapter.createTerm(db, "missed blind", "A required bet that is not posted when it is a player's turn to do so, perhaps occurring when a player absents himself from the table. Various rules require the missed bet to be made up upon the player's return.");
            long unused198 = DictDbAdapter.createTerm(db, "muck", "# To fold.");
            long unused199 = DictDbAdapter.createTerm(db, "multi-way pot", "A pot where several players compete for it. Also known as a ''family pot''.");
            long unused200 = DictDbAdapter.createTerm(db, "nit", "A player who is unwilling to take risks and plays only premium hands in the top range. Contrast weak player who plays like a nit but also folds extremely easily after taking risks even when holding an excellent hand. A weak player may be a nit but a nit is not necessarily a weak player.");
            long unused201 = DictDbAdapter.createTerm(db, "nothing", "When a player only has the possibility of a high card and no other hand that will win.");
            long unused202 = DictDbAdapter.createTerm(db, "nut hand (the nuts)", "The nut hand is the best possible hand in a given situation. Players sometimes evaluate hands by ranking them as being the \"Second nuts\" or being the \"Pure nuts\". The \"Pure nuts\" is usually the absolute best hand to have at that moment which is impossible to beat, the \"Second nuts\" is the second best hand only beat by the \"Pure nuts\", etc. The \"nut low\" is the absolute worst hand to have in a given round.");
            long unused203 = DictDbAdapter.createTerm(db, "nut low", "# The best possible low hand in high-low split games.");
            long unused204 = DictDbAdapter.createTerm(db, "offsuit", "Cards that are not of the same suit. ''The ace of clubs and the king of spades are called ace-king offsuit''");
            long unused205 = DictDbAdapter.createTerm(db, "one-chip rule", "A call of a previous bet using a chip of higher denomination than necessary is considered a call unless it is verbally announced as a raise.");
            long unused206 = DictDbAdapter.createTerm(db, "one-ended straight draw", "Four out of five cards needed for a straight that can only be completed with one specific rank of card, in cases where the needed card rank is either higher or lower than the cards already held as part of the sequence; as opposed to an inside straight draw or an open-ended straight draw");
            long unused207 = DictDbAdapter.createTerm(db, "open limp", "Being the first person in the pot preflop, but not raising.");
            long unused208 = DictDbAdapter.createTerm(db, "open-ended straight draw, open-ended", "An outside straight draw. Also \"two-way straight draw\" or \"double-ended straight draw\".");
            long unused209 = DictDbAdapter.createTerm(db, "openers", "The cards held by a player in a game of \"jackpots\" entitling him to open the pot. \"Splitting openers\" refers to holding onto one of your openers after discarding it to prove you had the necessary cards to open should you win the pot.");
            long unused210 = DictDbAdapter.createTerm(db, "option", "# An optional bet or draw, such as getting an extra card facedown for 50 cents or raising on the big blind when checked all the way around.");
            long unused211 = DictDbAdapter.createTerm(db, "orbit", "A full rotation of the blinds at a table.  Equal to the number of people at the table.");
            long unused212 = DictDbAdapter.createTerm(db, "out of position, OOP", "A player is said to be out of position, if he is either first to act, or is not last to act on a betting round.");
            long unused213 = DictDbAdapter.createTerm(db, "overbet", "To make a bet that is more than the size of the pot in a no limit game.");
            long unused214 = DictDbAdapter.createTerm(db, "overcall", "To call a bet after others have called, esp. big bets. ''Jim bet, Alice called, then Ted overcalled.'' Compare to \"cold call\", \"flat call\", \"smooth call\".");
            long unused215 = DictDbAdapter.createTerm(db, "overcard", "# A community card with a higher rank than a player's pocket pair.");
            long unused216 = DictDbAdapter.createTerm(db, "overpair", "In community card games such as Texas hold 'em and Omaha hold 'em, a pocket pair with a higher rank than any community card.");
            long unused217 = DictDbAdapter.createTerm(db, "overs", "An option to increase the stakes in limit games. Players may elect to play or not play overs; those who choose to play display some sort of token. If, at the beginning of a betting round after the first, only overs players remain in the hand, bets of twice the present limit are allowed. Most often used in home games as a compromise between aggressive and meek players.");
            long unused218 = DictDbAdapter.createTerm(db, "paint", "Any royal card. Used mostly in lowball games, where royal cards are rarely helpful.");
            long unused219 = DictDbAdapter.createTerm(db, "passive", "A style of play characterized by checking and calling. Compare to \"aggressive\", \"loose\", \"tight\".");
            long unused220 = DictDbAdapter.createTerm(db, "pat", "Already complete. A hand is a pat hand when, for example, a flush comes on the first five cards dealt in Draw poker.  Also see made hand.");
            long unused221 = DictDbAdapter.createTerm(db, "pay off", "To call a bet when you are most likely drawing dead because the pot odds justify the call.");
            long unused222 = DictDbAdapter.createTerm(db, "penny ante", "Frivolous, low stakes, or \"for fun\" only; A game where no significant stake is likely to change hands.");
            long unused223 = DictDbAdapter.createTerm(db, "perfect", "The best possible cards, in a lowball hand, after those already named. For example, 7-perfect would be 7-4-3-2-A, and 8-6-perfect would be 8-6-3-2-A.");
            long unused224 = DictDbAdapter.createTerm(db, "pick-up", "When the house picks up cash from the dealer after a player buys chips.");
            long unused225 = DictDbAdapter.createTerm(db, "play the board", "In games such as Texas hold 'em, where 5 community cards are dealt, if your best hand is on the board and you go to the showdown you are said to \"play the board\".");
            long unused226 = DictDbAdapter.createTerm(db, "PLO", "Pot-limit Omaha.");
            long unused227 = DictDbAdapter.createTerm(db, "pocket cards", "See \"hole cards\".");
            long unused228 = DictDbAdapter.createTerm(db, "pocket pair", "In community card poker or stud poker, when two of a player's private cards make a pair. Also \"wired pair\".");
            long unused229 = DictDbAdapter.createTerm(db, "poker face", "A blank expression that does not reveal anything about the cards being held. Often used outside the world of poker.");
            long unused230 = DictDbAdapter.createTerm(db, "position bet", "A bet that is made more due to the strength of the bettor's position than the strength of the bettor's cards.");
            long unused231 = DictDbAdapter.createTerm(db, "post dead", "To post a bet amount equal to the small and the big blind combined (the amount of the large blind playing as a live blind, and the amount of the small blind as dead money). In games played with blinds, a player who steps away from the table and misses his turn for the blinds must either post dead or wait for the big blind to re-enter the game. Compare to \"dead blind\".");
            long unused232 = DictDbAdapter.createTerm(db, "post", "To make the required small or big blind bet in Texas hold 'em or other games played with blinds rather than antes");
            long unused233 = DictDbAdapter.createTerm(db, "pot-committed", "More often in the context of a no limit game; the situation where you can no longer fold because the size of the pot is so large compared to the size of your stack.");
            long unused234 = DictDbAdapter.createTerm(db, "pre-flop", "On flop games refers to the time when players already have their pocket cards but no flop has been dealt yet. It's also the first round of bets.");
            long unused235 = DictDbAdapter.createTerm(db, "probe bet", "A bet after the flop by a player who did not take the lead in betting before the flop (and when the player that did take the lead in betting before the flop declined to act). Compare to \"continuation bet\".");
            long unused236 = DictDbAdapter.createTerm(db, "prop, proposition player", "A player who gets paid an hourly rate to start poker games or to help them stay active. Prop players play with their own money, which distinguishes them from shills, who play with the casino's money.");
            long unused237 = DictDbAdapter.createTerm(db, "protected pot", "A pot that seems impossible to bluff to win because too many players are active in it and the chances of another player either calling you to the end or raising beyond measure become an assurance.");
            long unused238 = DictDbAdapter.createTerm(db, "purse", "The total prize pool in a poker tournament");
            long unused239 = DictDbAdapter.createTerm(db, "push", "To bet all in.");
            long unused240 = DictDbAdapter.createTerm(db, "put on", "To ''put someone on'' a hand is to deduce what hand they have based on their actions and your knowledge of their gameplay. See also tells.");
            long unused241 = DictDbAdapter.createTerm(db, "quads", "Four of a kind.");
            long unused242 = DictDbAdapter.createTerm(db, "qualifier, qualifying low", "A qualifying low hand. High-low split games often require a minimum hand value, such as 8-high, in order to award the low half of the pot. In some home games, there are qualifiers for high hands as well: \"Seven stud, trips-eight\".");
            long unused243 = DictDbAdapter.createTerm(db, "quarter", "To win a quarter of a pot, usually by tying the low or high hand of a high-low split game. Generally, this is an unwanted outcome, as a player is often putting in a third of the pot in the hope of winning a quarter of the pot back.");
            long unused244 = DictDbAdapter.createTerm(db, "rabbit hunt", "After a hand is complete, to reveal cards that would have been dealt later in the hand had it continued. This is usually prohibited in casinos because it slows the game and may reveal information about concealed hands. Also \"fox hunt\".");
            long unused245 = DictDbAdapter.createTerm(db, "rack", "1. A collection of 100 chips of the same denomination, usually arranged in 5 stacks in a plastic tray.");
            long unused246 = DictDbAdapter.createTerm(db, "rag", "A low-valued (and presumably worthless) card. ''I don't like playing ace-rag from that position.'' Hence \"ragged\"/\"raggy\" - having a low value: ''The flop was pretty ragged, so I figured my queens were good''. Though note that if a flop consists of consecutive or same-suited low-value cards then it is not ragged/raggy, as it could be valuable as part of a straight or flush.");
            long unused247 = DictDbAdapter.createTerm(db, "rail", "The rail is the sideline at a poker tablethe (often imaginary) rail separating spectators from the field of play. Watching from the rail means watching a poker game as a spectator. \"Going to the rail\" usually means \"Losing all one's money\".");
            long unused248 = DictDbAdapter.createTerm(db, "railbird", "A non-participatory spectator of a poker game");
            long unused249 = DictDbAdapter.createTerm(db, "rainbow", "Three or four cards of different suits, especially said of a flop.");
            long unused250 = DictDbAdapter.createTerm(db, "Rakeback pro", "Rakeback pro is the definition given to a poker player who may not be a winning player, however, uses rakeback to supplement his losses and turn them in to winnings.");
            long unused251 = DictDbAdapter.createTerm(db, "rakeback", "Rebate/repayment to a player of a portion of the rake paid by that player, normally from a non-cardroom, third-party source such as an affiliate.  Rakeback is paid in many ways by online poker rooms, affiliates or brick and mortar rooms. Many use direct money payments for online poker play. Brick and Mortar rooms usually use rate cards to track and pay their rakeback. See Rake (poker)#Rakeback.");
            long unused252 = DictDbAdapter.createTerm(db, "range of hands", "Term used for the list of holdings that a player considers a opponent might have when trying to deduce their holding.  See also \"put on\".");
            long unused253 = DictDbAdapter.createTerm(db, "rathole", "To remove a portion of your chips from the table while the game is underway. Normally prohibited in public card rooms. Also \"going south\".");
            long unused254 = DictDbAdapter.createTerm(db, "rebuy", "An amount of chips purchased after the buy-in. In some tournaments, players are allowed to rebuy chips one or more times for a limited period after the start of the game, providing that their stack is at or under its initial level.  Compare with \"add-on\".");
            long unused255 = DictDbAdapter.createTerm(db, "redeal", "To deal a hand again, possibly after a misdeal.");
            long unused256 = DictDbAdapter.createTerm(db, "redraw", "# To make one hand and have a draw for a better hand. ''Ted made a straight on the turn with a redraw for a flush on the river.''.");
            long unused257 = DictDbAdapter.createTerm(db, "represent", "To ''represent'' a hand is to play as if you hold it (whether you actually hold it or are bluffing).");
            long unused258 = DictDbAdapter.createTerm(db, "reraise", "Raise after one has been raised. Also coming \"over the top\".");
            long unused259 = DictDbAdapter.createTerm(db, "river", "The river or \"river card\" is the final card dealt in a poker hand, to be followed by a final round of betting and, if necessary, a showdown. In Texas hold 'em and Omaha hold'em, the river, is the fifth and last card to be dealt to the community card board, after the flop and turn. A player losing the pot due only to the river card is said to have been \"rivered\".");
            long unused260 = DictDbAdapter.createTerm(db, "rock", "# A very tight player (plays very few hands and only continues with strong hands).");
            long unused261 = DictDbAdapter.createTerm(db, "rolled-up trips", "In seven-card stud, three of a kind dealt in the first three cards.");
            long unused262 = DictDbAdapter.createTerm(db, "rounder", "An expert player who travels a''round'' to seek out high-stakes games");
            long unused263 = DictDbAdapter.createTerm(db, "royal cards", "Royal card are also known as face cards or picture cards. These cards consist of the Jack, Queen, and King of any suit.");
            long unused264 = DictDbAdapter.createTerm(db, "royal flush", "A straight flush of the top five cards of any suit. This is generally the highest possible hand.");
            long unused265 = DictDbAdapter.createTerm(db, "run it twice, running it twice", "A gentleman's agreement (which isn't allowed in some casinos) where the players (usually two or three) agree to draw each remaining card to come in two different occasions instead of just once after all parties have gone all-in (two flops, turns and river for example for a total of 10 community cards in 2 sets of 5). You may run twice the flop, turn and river or just the turn and river or only the river. Cards are usually not run retroactively unless the players expressly request so (which is rare). The winner of one \"run\" gets half the pot while the winner of the second \"run\" gets the other half. Running it twice is done to minimize bad beats and remove variance (which works well if the hands are equally likely to win the hand but not so well if one is dominated over the other). Running it twice is a form of insurance.");
            long unused266 = DictDbAdapter.createTerm(db, "runner-runner", "A hand made by hitting two consecutive cards on the turn and river. Also \"backdoor\". Compare to \"bad beat\" and \"suck out\".");
            long unused267 = DictDbAdapter.createTerm(db, "rush", "A prolonged winning streak. A player who has won several big pots recently is said to be ''on a rush''. Also \"heater\".");
            long unused268 = DictDbAdapter.createTerm(db, "satellite", "A tournament in which the prize is a free entrance to another (larger) tournament.");
            long unused269 = DictDbAdapter.createTerm(db, "scare card", "A card dealt face up (either to a player in a game such as stud or to the board in a community card game) that could create a strong hand for someone. ''The Jack of spades on the turn was a scare card because it put both flush and straight possibilities on the board.''");
            long unused270 = DictDbAdapter.createTerm(db, "scoop", "In high-low split games, to win both the high and the low halves of the pot.");
            long unused271 = DictDbAdapter.createTerm(db, "second pair", "In community card poker games, a pair of cards of the second-top rank on the board. Second pair is a middle pair, but not necessarily vice-versa. Compare bottom pair, top pair.");
            long unused272 = DictDbAdapter.createTerm(db, "sell", "In spread limit poker, to ''sell'' a hand is to bet less than the maximum with a strong hand, in the hope that more of your opponents will call the bet.");
            long unused273 = DictDbAdapter.createTerm(db, "set", "Three of a kind, esp. the situation where two of the cards are concealed in the player's hole cards. Compare to \"trips\".");
            long unused274 = DictDbAdapter.createTerm(db, "set-up", "A deck that has been ordered, usually King to Ace by suit (spades, hearts, clubs and diamonds). In casinos, it is customary to use a set-up deck when introducing a new deck to the table. The set-up is spread face up for the players to demonstrate that all of the cards are present before the first shuffle. Also called to \"spade the deck\".");
            long unused275 = DictDbAdapter.createTerm(db, "sevens rule", "A rule in many A-5 lowball games that requires a player with a seven-low or better after the draw to bet, rather than check or check-raise. In some venues a violator loses any future interest in the pot; in others he forfeits his interest entirely.");
            long unused276 = DictDbAdapter.createTerm(db, "shark", "A professional player.  See also card sharp.");
            long unused277 = DictDbAdapter.createTerm(db, "shoe", "A slanted container used to hold the cards yet to be dealt, usually used by casinos or in professional poker tournaments.");
            long unused278 = DictDbAdapter.createTerm(db, "shootout", "A poker tournament format where the last remaining player of a table goes on to play the remaining players of other tables.  Each table plays independently of the others; that is, there is no balancing as players are eliminated. This format is particularly common in European televised poker programs, including Late Night Poker.");
            long unused279 = DictDbAdapter.createTerm(db, "short buy", "In no-limit poker, to buy in to a game for considerably less money than the stated maximum buyin, or less than other players at the table have in play.");
            long unused280 = DictDbAdapter.createTerm(db, "short stack", "A stack of chips that is relatively small for the stakes being played.");
            long unused281 = DictDbAdapter.createTerm(db, "shorthanded", "A poker game that is played with around six players or less, as opposed to a full ring game, which is usually nine or ten players. A tournament where all tables are shorthanded at all times is called a short table tournament.");
            long unused282 = DictDbAdapter.createTerm(db, "side game", "A ring game running concurrently with a tournament made up of players who have either been eliminated or opted not to play the tournament.");
            long unused283 = DictDbAdapter.createTerm(db, "sit and go", "A poker tournament with no scheduled starting time that starts whenever the necessary players have put up their money. Single-table sit-and-goes, with nine or ten players, are the norm, but multi-table games are common as well. Also called ''sit n' gos'' and a variety of other similar spellings.");
            long unused284 = DictDbAdapter.createTerm(db, "slow roll", "To delay or avoid showing one's hand at showdown, forcing other players to expose their hands first. When done while holding a good hand likely to be the winner, it is considered poor etiquette, because it often gives other players \"false hope\" that their hands might win before the slow-roller's is exposed.");
            long unused285 = DictDbAdapter.createTerm(db, "smooth call", "See \"flat call\".");
            long unused286 = DictDbAdapter.createTerm(db, "snow", "# To play a worthless hand misleadingly in draw poker in order to bluff.");
            long unused287 = DictDbAdapter.createTerm(db, "soft-play", "To intentionally go easy on a player (e.g. not betting or raising against him when you usually would).  Soft play is expressly prohibited in most card rooms, and may result in penalties ranging from forced sit-outs to forfeiture of stakes or winnings.");
            long unused288 = DictDbAdapter.createTerm(db, "splash the pot", "To throw one's chips in the pot in a disorderly fashion. Not typically allowed, because the dealer can't tell how much has been bet.");
            long unused289 = DictDbAdapter.createTerm(db, "split two pair", "In community card poker, a two pair hand, with each pair made of one of your hole cards, and one community card.");
            long unused290 = DictDbAdapter.createTerm(db, "spread", "The range between a table's minimum and maximum bets.");
            long unused291 = DictDbAdapter.createTerm(db, "spread-limit", "A form of limit poker where the bets and raises can be between a minimum and maximum value. The spread may change between rounds.");
            long unused292 = DictDbAdapter.createTerm(db, "squeeze play", "A bluff reraise in no limit hold'em with less-than-premium cards, after another player or players have already called the original raise. The goal is to bluff everyone out of the hand and steal the bets. This play is most effective when a loose aggressive player opens the pot and is called by one or more passive / weak players. Assuming a standard raise of 3BBs, and only one caller, then the minimum bluff squeezing stack is generally accepted as being at least 18 BBs (this increases the more cold callers there are in the pot).");
            long unused293 = DictDbAdapter.createTerm(db, "stack", "# The total chips and currency that a player has in play at a given moment.");
            long unused294 = DictDbAdapter.createTerm(db, "stakes", "The definition of the amount one buys in for and can bet. For example, a \"low stakes\" game might be a $10 buy-in with a $1 maximum raise.");
            long unused295 = DictDbAdapter.createTerm(db, "staking", "Staking is the act of one person putting up cash for a poker player to play with in hopes that the player wins. Any profits are split on a predetermined percentage between the backer and the player.  A backed player is often known as a \"horse\".  The player will then use the money to play in a tournament or ring game. Compare with \"bankroll\".");
            long unused296 = DictDbAdapter.createTerm(db, "stand pat", "In draw poker, playing the original hand using no draws, either as a bluff or in the belief it is the best hand.");
            long unused297 = DictDbAdapter.createTerm(db, "steam", "A state of anger, mental confusion, or frustration in which a player adopts a less than optimal strategy, usually resulting in poor play and poor performance. Compare to 'tilt'.");
            long unused298 = DictDbAdapter.createTerm(db, "stop and go", "''Stop and go'' or ''stop 'n' go'' is when a player bets into another player who has previously raised or otherwise shown aggression. Example:  On the flop, Bill bets into Tom, Tom raises, and Bill just calls. On the turn, Bill bets into Tom again. Bill has just pulled a ''stop 'n' go'' play.");
            long unused299 = DictDbAdapter.createTerm(db, "straight", "# Poker hand: see straight.");
            long unused300 = DictDbAdapter.createTerm(db, "strategy card", "A wallet sized card that is commonly used to help with poker strategies in online and casino games.");
            long unused301 = DictDbAdapter.createTerm(db, "street", "A street is another term for a dealt card or betting round, e.g. as in first street, second street, third street (flop), forth street (turn), fifth street (river)");
            long unused302 = DictDbAdapter.createTerm(db, "string bet", "A call with one motion and a later raise with another, or a reach for more chips without stating the intended amount. String bets are prohibited in public cardroom rules. Compare to \"forward motion\". A player can (and should) defend himself against string bet complaints by declaring his intention before moving any chips. Note that the \"I call, and raise...\" cliche is a string bet.");
            long unused303 = DictDbAdapter.createTerm(db, "structured", "A structured betting system is one where the spread of the bets may change from round to round.");
            long unused304 = DictDbAdapter.createTerm(db, "subscription poker", "Subscription poker is a form of online poker wherein users pay a monthly fee to become eligible to play in real-money tournaments.");
            long unused305 = DictDbAdapter.createTerm(db, "suck out", "A situation when a hand heavily favored to win loses to an inferior hand after all the cards are dealt. The winning hand is said to have \"sucked out\". Compare to \"bad beat\".");
            long unused306 = DictDbAdapter.createTerm(db, "super satellite", "A multi-table poker tournament in which the prize is a free entrance to a satellite tournament or a tournament in which all the top finishers gain entrance to a larger tournament.");
            long unused307 = DictDbAdapter.createTerm(db, "tag", "A \"tight aggressive\" style of play in which a player plays a small number of strong starting hands, but when in pots plays aggressively.");
            long unused308 = DictDbAdapter.createTerm(db, "third man walking", "A player who gets up from his seat in a cash game, after two other players are already away from the table, is referred to as the \"third man walking\". In a casino with a \"third man walking rule\", this player may be required to return to his seat within 10 minutes, or one rotation of the deal around the table, or else his seat in the game will be forfeited if there is a waiting list for the game.");
            long unused309 = DictDbAdapter.createTerm(db, "three bet, three betting, 3-bet, 3bet", "To be the first player to put in a 3rd unit of betting. For example, if Bob opens for $10, and Mary raises to make the bet $20, if Ted also raises to make the bet $30, this is to \"three bet\". (Before the flop, 3-betting means re-raising the ''first'' raiser.)");
            long unused310 = DictDbAdapter.createTerm(db, "three pair", "In a seven card game, such as seven-card stud or Texas hold 'em, it is possible for a player to have 3 pairs, although a player can only play two of them as part of a standard 5-card poker hand. This situation may jokingly be referred to as a player having a hand of three pair. Note that in Omaha hold 'em, it is possible to \"have\" 4 pair in the same manner.");
            long unused311 = DictDbAdapter.createTerm(db, "to go", "A term used to describe the amount that a player is required to call in order to stay in the hand, ''\"Alice was deciding whether to call now it was $50 to go.\"''");
            long unused312 = DictDbAdapter.createTerm(db, "toke", "In a brick and mortar casino, a ''toke'' is a \"tip\" given to the dealer by the winner of the pot. Tokes often represent a large percentage of a dealer's income.");
            long unused313 = DictDbAdapter.createTerm(db, "top kicker", "In community card poker games, ''top kicker'' is the best possible kicker to some given hand. Usually it would be an Ace, but with an Ace on the board it would be a King or lower.");
            long unused314 = DictDbAdapter.createTerm(db, "top pair", "In community card poker games, ''top pair'' is a pair comprising a pocket card and the highest ranking card on the board. Compare second pair, bottom pair.");
            long unused315 = DictDbAdapter.createTerm(db, "top two", "A split two pair, matching the highest-ranking two flop cards.");
            long unused316 = DictDbAdapter.createTerm(db, "trey", "A 3-spot card. Casino personnel refer to the 3 as the \"trey of clubs\".");
            long unused317 = DictDbAdapter.createTerm(db, "trips", "When one of a player's hole cards in Texas hold 'em connects with two cards on the board to make three of a kind. This differs from a \"set\" where three of a kind is made when a pocket pair connects with one card on the flop to make three of a kind.");
            long unused318 = DictDbAdapter.createTerm(db, "turbo", "A turbo is a type of tournament where the blind levels increase much faster than in standard play.");
            long unused319 = DictDbAdapter.createTerm(db, "turn", "The turn or \"turn card\" or \"fourth street\" is the fourth of five cards dealt to a community card board, constituting one face-up community card that each of the players in the game can use to make up their final hand.  See also flop and river.");
            long unused320 = DictDbAdapter.createTerm(db, "under the gun", "The playing position to the direct left of the blinds in Texas hold 'em or Omaha hold 'em. The player who is under the gun must act first on the first round of betting.");
            long unused321 = DictDbAdapter.createTerm(db, "underdog", "An ''underdog'' or ''dog'' is a player with a smaller chance to win than another specified player. Frequently used when the exact odds are expressed. ''Harry might have been bluffing, but if he really had the king, my hand was a 4-to-1 dog, so I folded.''");
            long unused322 = DictDbAdapter.createTerm(db, "underfull", "A full house made where the three of a kind has lower ranking cards than the pair. ''I had the underfull when the flop came A-A-5 and I had pocket 5's in the hole.'' Can be beaten by the \"big full\".");
            long unused323 = DictDbAdapter.createTerm(db, "up the ante", "Increase the stake. Also commonly used outside the context of poker.");
            long unused324 = DictDbAdapter.createTerm(db, "up", "When used with a card rank to describe a poker hand, refers to two pair with the named card being the higher pair. For example, a hand of QQ885 might be called \"queens up\".");
            long unused325 = DictDbAdapter.createTerm(db, "vigorish, vig", "The rake. See vigorish.");
            long unused326 = DictDbAdapter.createTerm(db, "VPIP", "A statistic that stands for Voluntary Put Money In Pot. It represents the percentage of hands with which a player puts money into the pot pre-flop, without counting any blind postings. Also called '''VP$IP'''. VPIP is an excellent measure of aggression.");
            long unused327 = DictDbAdapter.createTerm(db, "wake up", "To \"wake up with a hand\" means to discover a strong starting hand, often when there has already been action in front of the player.");
            long unused328 = DictDbAdapter.createTerm(db, "walk", "A walk is the situation where all players fold to the big blind.");
            long unused329 = DictDbAdapter.createTerm(db, "wash", "To mix the deck by spreading the cards face down on the table and mixing them up. A dealer may wash the deck before shuffling.");
            long unused330 = DictDbAdapter.createTerm(db, "weak ace", "An ace with a low kicker (e.g. four). Also \"small ace,\" \"soft ace,\" \"ace-rag.");
            long unused331 = DictDbAdapter.createTerm(db, "weak player", "A player who is easily bullied out of a hand post-flop by any sort of action (betting, raising), whether he has the best hand or not. Weak players are usually but not always nits. Weak players are poker player's favorite opponents second only to calling stations.");
            long unused332 = DictDbAdapter.createTerm(db, "webcam poker", "A form of online poker which allows players to watch each other during play via a webcam. Webcam poker gives competitors the chance to observe their rivals' reactions in virtual poker games and tournaments. Players can see the cards being dealt by live webcam poker dealers, rather than random number generators).");
            long unused333 = DictDbAdapter.createTerm(db, "window card", "An upcard in stud poker. The first window card in stud is called the \"door card\". In Texas hold'em and Omaha, the window card is the first card shown when the dealer puts out the three cards for the flop.");
            long unused334 = DictDbAdapter.createTerm(db, "wrap", "In Omaha hold 'em, an open ended straight draw comprising two board cards and three or four cards from a player's hand. A player holding 345A with the board 67K has a \"wrap\", as any  3, 4, or 5, or 8 will make a straight. A hand of 4589 would also be a wrap draw, but would often be referred to as a \"big wrap\" because it has twenty outs rather than thirteen, and is not at the idiot end.");
            long unused335 = DictDbAdapter.createTerm(db, "all in", "When a player is faced with a current bet amount that he has insufficient remaining stake to call and he wishes to call (he may of course fold without the need of special rules), he bets the remainder of his stake and declares himself all in. He may now hold onto his cards for the remainder of the deal as if he had called every bet, but he may not win any more money from any player above the amount of his bet. In no-limit games, a player may also go all in, that is, betting his entire stack at any point during a betting round.");
            long unused336 = DictDbAdapter.createTerm(db, "ante", "An ante is a forced bet in which all players put an equal amount of money or chips into the pot before the deal begins. Often this is either a single unit (a one-value or the smallest value in play) or some other small amount; a proportion such as a half or a quarter of the minimum bet is also common. An ante paid by every player ensures that a player who folds every round will lose money (though slowly), thus providing all players with an incentive, however small, to play the hand rather than toss it in when the opening bet reaches them.");
            long unused337 = DictDbAdapter.createTerm(db, "hand", "In poker, players construct hands of five cards according to predetermined rules, which vary according to which variant of poker is being played. These hands are compared using a hand ranking system that is standard across all variants of poker, the player with the highest-ranking hand winning that particular deal in most variants of poker. In some variants, the lowest-ranking hand can win or tie.");
            long unused338 = DictDbAdapter.createTerm(db, "blind", "The blinds are forced bets posted by players to the left of the dealer button in flop-style poker games. The number of blinds is usually two (2) but it can range from none (0) to three (3). The small blind is placed by the player to the left of the dealer button and the big blind is then posted by the player to the left of the person sitting in the small blind. The one exception to this rule is when there are only 2 players (a \"heads-up\" game). In this case the player on the button is the small blind, and the other player is the big blind.");
            long unused339 = DictDbAdapter.createTerm(db, "fold", "To discard one's hand and forfeit interest in the current pot.");
            long unused340 = DictDbAdapter.createTerm(db, "bluff", "In the card game of poker, a bluff is a bet or raise made with a hand which is not thought to be the best hand. To bluff is to make such a bet. The objective of a bluff is to induce a fold by at least one opponent who holds a better hand. The size and frequency of a bluff determines its profitability to the bluffer. By extension, the term is often used outside the context of poker to describe the act of making threats one cannot execute.");
            long unused341 = DictDbAdapter.createTerm(db, "check", "If no one has yet opened the betting round, a player may pass or check, which is equivalent to calling the current bet of zero. When checking, a player declines to make a bet; this indicates that he does not wish to open, but does wish to keep his cards and retain the right to call or raise later in the same round if an opponent opens. In games played with blinds, players may not check on the opening round because the blinds are live bets and must be called or raised to remain in the hand.");
            long unused342 = DictDbAdapter.createTerm(db, "big blind", "A designated amount that is placed by the player sitting in the second position, clockwise from the dealer, before any cards are dealt.");
            long unused343 = DictDbAdapter.createTerm(db, "flush", "A hand comprising five cards of the same suit.");
            long unused344 = DictDbAdapter.createTerm(db, "burn card, burn", "In card games, a burn card is a playing card dealt from the top of a deck, and discarded (\"burned\"), unused by the players. Burn cards are almost always placed face down next to the discard pile without being revealed to the players.");
            long unused345 = DictDbAdapter.createTerm(db, "dead money", "In poker, dead money is the amount of money in the pot other than the equal amounts bet by active remaining players in that pot. Examples of dead money include money contributed to the pot by players who have folded, a dead blind posted by a player returning to a game after missing blinds, or an odd chip left in the pot from a previous deal.");
            long unused346 = DictDbAdapter.createTerm(db, "button", "A marker used to indicate the player who is dealing or, in casino games with a house dealer, the player who acts last on that deal (who would be the dealer in a home game). The term button is also used for a variety of plastic discs, or lammers, used by casinos to mark the status of players.");
            long unused347 = DictDbAdapter.createTerm(db, "call", "To call is to match a bet or match a raise. A betting round ends when all active players have bet an equal amount or no opponents call a player's bet or raise. If no opponents call a player's bet or raise, the player wins the pot.");
            long unused348 = DictDbAdapter.createTerm(db, "chip", "Small discs used in lieu of currency in casinos (also known as checks, or cheques).");
            long unused349 = DictDbAdapter.createTerm(db, "ace-to-five, ace-to-six", "Methods of evaluating low hands. See \"lowball\".");
            long unused350 = DictDbAdapter.createTerm(db, "lowball", "Some forms of poker, often called lowball, sometimes called low poker, reward poor poker hands (in the traditional sense). There are four common variations on this idea, differing in whether aces are treated as high cards or low cards, and whether or not straights and flushes are used. The methods are: * Ace-to-five low: The lowest possible hand is 5-4-3-2-A, called a wheel. Aces are low and straights and flushes are ignored. This is the most common method. * Ace-to-six low: Also called 6-4 low, since the lowest possible hand is 6-4-3-2-A. Aces are low and straights and flushes count as high hands. * Deuce-to-seven low: Also called 7-5 low, since the lowest possible hand is 7-5-4-3-2. Almost the direct inverse of traditional \"high hand\" poker. Aces are high and straights and flushes count as high hands. Since aces are high, A-5-4-3-2 is not a straight, but just ace-high no pair. * Deuce-to-six low: The other, mostly unused, possibility would be 6-5 low. Aces are high, straights and flushes are ignored.");
            long unused351 = DictDbAdapter.createTerm(db, "bad beat", "In poker, bad beat is a subjective term for a hand in which a player with what appear to be strong cards nevertheless loses. It most often occurs where one player bets the clearly stronger hand and their opponent makes a poor call that eventually \"hits\" and wins. There is no consensus among poker players as to what exactly constitutes a bad beat and often players will disagree about whether a particular hand was a bad beat.");
            long unused352 = DictDbAdapter.createTerm(db, "bottom dealing", "Bottom dealing or \"base dealing\" is a sleight of hand technique in which the bottom card from a deck of playing cards is dealt instead of the top card. It is used in card illusions, and as a method of card sharp.");
        }
    }

    public DictDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public DictDbAdapter open() throws SQLException {
        this.mDbHelper = new DatabaseHelper(this.mCtx);
        this.mDb = this.mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.mDbHelper.close();
    }

    public static int fieldByNameInt(Cursor cur, String fldName) {
        return cur.getInt(cur.getColumnIndex(fldName));
    }

    public static String fieldByNameStr(Cursor cur, String fldName) {
        return cur.getString(cur.getColumnIndex(fldName));
    }

    public static long fieldByNameLong(Cursor cur, String fldName) {
        return cur.getLong(cur.getColumnIndex(fldName));
    }

    public static double fieldByNameDouble(Cursor cur, String fldName) {
        return cur.getDouble(cur.getColumnIndex(fldName));
    }

    /* access modifiers changed from: private */
    public static long createTerm(SQLiteDatabase db, String term, String definition) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(TERM_TABLE_NAME, term);
        initialValues.put("definition", definition);
        return db.insert(TERM_TABLE_NAME, null, initialValues);
    }

    public long createTerm(Term term) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(TERM_TABLE_NAME, term.getTerm());
        initialValues.put("definition", term.getDefinition());
        return this.mDb.insert(TERM_TABLE_NAME, null, initialValues);
    }

    public boolean deleteTerm(long rowId) {
        return this.mDb.delete(TERM_TABLE_NAME, new StringBuilder("_id=").append(rowId).toString(), null) > 0;
    }

    public boolean deleteAllTerms() {
        return this.mDb.delete(TERM_TABLE_NAME, null, null) > 0;
    }

    public Term fetchRandomTerm() throws SQLException {
        Term result = null;
        Cursor cur = this.mDb.query(true, TERM_TABLE_NAME, new String[]{"_id", TERM_TABLE_NAME, "definition"}, null, null, null, null, "RANDOM()", "1");
        if (cur.moveToFirst()) {
            result = new Term(fieldByNameLong(cur, "_id"), fieldByNameStr(cur, TERM_TABLE_NAME), fieldByNameStr(cur, "definition"));
        }
        cur.close();
        return result;
    }

    public Term fetchTerm(long rowId) throws SQLException {
        Term result = null;
        Cursor cur = this.mDb.query(true, TERM_TABLE_NAME, new String[]{"_id", TERM_TABLE_NAME, "definition"}, "_id=" + rowId, null, null, null, null, null);
        if (cur.moveToFirst()) {
            result = new Term(fieldByNameLong(cur, "_id"), fieldByNameStr(cur, TERM_TABLE_NAME), fieldByNameStr(cur, "definition"));
        }
        cur.close();
        return result;
    }

    public ArrayList<Term> fetchTermLike(String likeText) {
        ArrayList<Term> result = new ArrayList<>();
        Cursor cur = this.mDb.query(TERM_TABLE_NAME, new String[]{"_id", TERM_TABLE_NAME, "definition"}, "term LIKE ?", new String[]{String.valueOf(likeText) + "%"}, null, null, "term COLLATE NOCASE ASC");
        if (cur.moveToFirst()) {
            do {
                Term term = new Term(fieldByNameLong(cur, "_id"), fieldByNameStr(cur, TERM_TABLE_NAME), fieldByNameStr(cur, "definition"));
                result.add(term);
                Log.d(TAG, term.toString());
            } while (cur.moveToNext());
        }
        cur.close();
        return result;
    }

    public ArrayList<Term> fetchAllTerms() {
        ArrayList<Term> result = new ArrayList<>();
        Cursor cur = this.mDb.query(TERM_TABLE_NAME, new String[]{"_id", TERM_TABLE_NAME, "definition"}, null, null, null, null, "term COLLATE NOCASE ASC");
        if (cur.moveToFirst()) {
            do {
                result.add(new Term(fieldByNameLong(cur, "_id"), fieldByNameStr(cur, TERM_TABLE_NAME), fieldByNameStr(cur, "definition")));
            } while (cur.moveToNext());
        }
        cur.close();
        return result;
    }

    public Cursor fetchSearchSuggestions(String query, String[] columns) {
        Log.d(TAG, "fetchSearchSuggestions query:" + query);
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TERM_TABLE_NAME);
        builder.setProjectionMap(mColumnMap);
        Cursor cursor = builder.query(this.mDb, columns, "term LIKE ?", new String[]{String.valueOf(query) + "%"}, null, null, "term COLLATE NOCASE ASC");
        if (cursor == null) {
            return null;
        }
        if (cursor.moveToFirst()) {
            return cursor;
        }
        cursor.close();
        return null;
    }
}
