package com.rogansoft.pokerdict;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.rogansoft.pokerdict.domain.Term;

public class SearchActivity2 extends ListActivity {
    private DictDbAdapter mDb;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search);
        this.mDb = new DictDbAdapter(this);
        this.mDb.open();
        Intent intent = getIntent();
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            doMySearch(intent.getStringExtra("query"));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mDb.close();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        launchViewTerm(((Term) getListView().getItemAtPosition(position)).getId());
        super.onListItemClick(l, v, position, id);
    }

    private void doMySearch(String query) {
        setListAdapter(new ArrayAdapter(this, 17367043, this.mDb.fetchTermLike(query)));
    }

    /* access modifiers changed from: protected */
    public void launchViewTerm(long id) {
        Intent i = new Intent(this, ViewTerm.class);
        Bundle b = new Bundle();
        b.putLong("id", id);
        i.putExtras(b);
        i.setFlags(67108864);
        startActivity(i);
    }
}
