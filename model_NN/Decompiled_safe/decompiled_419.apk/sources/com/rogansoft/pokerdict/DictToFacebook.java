package com.rogansoft.pokerdict;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.rogansoft.pokerdict.fb.BaseRequestListener;
import com.rogansoft.pokerdict.fb.LoginButton;
import com.rogansoft.pokerdict.fb.SessionEvents;
import com.rogansoft.pokerdict.fb.SessionStore;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public class DictToFacebook extends Activity {
    public static final String FACEBOOK_APP_ID = "145347308878530";
    private static final String[] PERMISSIONS = {"publish_stream"};
    private static final int PROGRESS_DIALOG_KEY = 0;
    /* access modifiers changed from: private */
    public Facebook mFacebook;
    private LoginButton mLoginButton;
    /* access modifiers changed from: private */
    public String mMessage;
    /* access modifiers changed from: private */
    public Button mPostButton;
    /* access modifiers changed from: private */
    public TextView mText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mMessage = getIntent().getStringExtra("message");
        setContentView((int) R.layout.fb_main);
        setTitle("Post To Facebook");
        this.mLoginButton = (LoginButton) findViewById(R.id.fb_login);
        this.mText = (TextView) findViewById(R.id.fb_txt);
        this.mPostButton = (Button) findViewById(R.id.postButton);
        this.mFacebook = new Facebook(FACEBOOK_APP_ID);
        boolean isValid = SessionStore.restore(this.mFacebook, this);
        SessionEvents.addAuthListener(new SampleAuthListener());
        SessionEvents.addLogoutListener(new SampleLogoutListener());
        this.mText.setText(this.mMessage);
        if (isValid) {
            this.mPostButton.setVisibility(0);
        }
        this.mLoginButton.init(this.mFacebook, PERMISSIONS, this);
        this.mPostButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                DictToFacebook.this.addStringToBundle(bundle, "message", DictToFacebook.this.mText.getText().toString());
                DictToFacebook.this.addStringToBundle(bundle, "link", "http://www.rogansoftware.com/poker/");
                DictToFacebook.this.addStringToBundle(bundle, "name", "Poker Vocabulary for Android");
                DictToFacebook.this.addStringToBundle(bundle, "caption", "Poker vocabulary reference on your Android phone!");
                DictToFacebook.this.addStringToBundle(bundle, "picture", "http://www.rogansoftware.com/poker/graphics/icon.png");
                DictToFacebook.this.mFacebook.dialog(DictToFacebook.this, "stream.publish", bundle, new MyDialogListener(DictToFacebook.this, null));
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setMessage("Posting to Facebook wall...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    /* access modifiers changed from: private */
    public void addStringToBundle(Bundle b, String key, String value) {
        b.putString(key, value);
    }

    public class SampleAuthListener implements SessionEvents.AuthListener {
        public SampleAuthListener() {
        }

        public void onAuthSucceed() {
            DictToFacebook.this.mText.setText(DictToFacebook.this.mMessage);
            DictToFacebook.this.mPostButton.setVisibility(0);
        }

        public void onAuthFail(String error) {
            DictToFacebook.this.mText.setText("Login Failed: " + error);
        }
    }

    public class SampleLogoutListener implements SessionEvents.LogoutListener {
        public SampleLogoutListener() {
        }

        public void onLogoutBegin() {
            DictToFacebook.this.mText.setText("Logging out...");
        }

        public void onLogoutFinish() {
            DictToFacebook.this.mText.setText("You have logged out! ");
            DictToFacebook.this.mPostButton.setVisibility(4);
        }
    }

    private class MyDialogListener implements Facebook.DialogListener {
        private MyDialogListener() {
        }

        /* synthetic */ MyDialogListener(DictToFacebook dictToFacebook, MyDialogListener myDialogListener) {
            this();
        }

        public void onCancel() {
        }

        public void onComplete(Bundle values) {
            Toast.makeText(DictToFacebook.this, "Wall post successful!", 0).show();
        }

        public void onError(DialogError e) {
        }

        public void onFacebookError(FacebookError e) {
        }
    }

    public class SampleRequestListener extends BaseRequestListener {
        public SampleRequestListener() {
        }

        public void onComplete(String response, Object state) {
            Log.d("Facebook-Example", "Response: " + response.toString());
            DictToFacebook.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(DictToFacebook.this, "Wall post successful!", 0).show();
                    DictToFacebook.this.dismissDialog(0);
                }
            });
        }

        public void onFacebookError(FacebookError e, Object state) {
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
        }

        public void onIOException(IOException e, Object state) {
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
        }
    }
}
