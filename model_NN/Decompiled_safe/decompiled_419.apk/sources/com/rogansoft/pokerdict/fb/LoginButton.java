package com.rogansoft.pokerdict.fb;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.rogansoft.pokerdict.R;
import com.rogansoft.pokerdict.fb.SessionEvents;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public class LoginButton extends ImageButton {
    private static final String TAG = "LoginButton";
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public Facebook mFb;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String[] mPermissions;
    private SessionListener mSessionListener = new SessionListener(this, null);

    public LoginButton(Context context) {
        super(context);
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Facebook fb, String[] permissions, Activity activity) {
        int i;
        Log.d(TAG, "init:");
        this.mFb = fb;
        this.mPermissions = permissions;
        this.mHandler = new Handler();
        this.mActivity = activity;
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        if (fb.isSessionValid()) {
            i = R.drawable.logout_button;
        } else {
            i = R.drawable.login_button;
        }
        setImageResource(i);
        drawableStateChanged();
        SessionEvents.addAuthListener(this.mSessionListener);
        SessionEvents.addLogoutListener(this.mSessionListener);
        setOnClickListener(new ButtonOnClickListener(this, null));
    }

    private final class ButtonOnClickListener implements View.OnClickListener {
        private ButtonOnClickListener() {
        }

        /* synthetic */ ButtonOnClickListener(LoginButton loginButton, ButtonOnClickListener buttonOnClickListener) {
            this();
        }

        public void onClick(View arg0) {
            if (LoginButton.this.mFb.isSessionValid()) {
                Log.d(LoginButton.TAG, "onClick(valid session)");
                SessionEvents.onLogoutBegin();
                new AsyncFacebookRunner(LoginButton.this.mFb).logout(LoginButton.this.getContext(), new LogoutRequestListener(LoginButton.this, null));
                return;
            }
            Log.d(LoginButton.TAG, "onClick(invalid session, authorize...)");
            LoginButton.this.mFb.authorize(LoginButton.this.mActivity, LoginButton.this.mPermissions, new LoginDialogListener(LoginButton.this, null));
        }
    }

    private final class LoginDialogListener implements Facebook.DialogListener {
        private LoginDialogListener() {
        }

        /* synthetic */ LoginDialogListener(LoginButton loginButton, LoginDialogListener loginDialogListener) {
            this();
        }

        public void onComplete(Bundle values) {
            Log.d(LoginButton.TAG, "LoginDialogListener:onComplete");
            SessionEvents.onLoginSuccess();
        }

        public void onFacebookError(FacebookError error) {
            Log.d(LoginButton.TAG, "LoginDialogListener:onFacebookError");
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onError(DialogError error) {
            Log.d(LoginButton.TAG, "LoginDialogListener:onError");
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            Log.d(LoginButton.TAG, "LoginDialogListener:onCancel");
            SessionEvents.onLoginError("Action Canceled");
        }
    }

    private class LogoutRequestListener extends BaseRequestListener {
        private LogoutRequestListener() {
        }

        /* synthetic */ LogoutRequestListener(LoginButton loginButton, LogoutRequestListener logoutRequestListener) {
            this();
        }

        public void onComplete(String response, Object state) {
            Log.d(LoginButton.TAG, "LogoutRequestListener:onComplete");
            LoginButton.this.mHandler.post(new Runnable() {
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }

        public void onFacebookError(FacebookError e, Object state) {
            Log.d(LoginButton.TAG, "LogoutRequestListener:onFacebookError");
        }

        public void onFileNotFoundException(FileNotFoundException e, Object state) {
            Log.d(LoginButton.TAG, "LogoutRequestListener:onFileNotFoundException");
        }

        public void onIOException(IOException e, Object state) {
            Log.d(LoginButton.TAG, "LogoutRequestListener:onIOException");
        }

        public void onMalformedURLException(MalformedURLException e, Object state) {
            Log.d(LoginButton.TAG, "LogoutRequestListener:onMalformedURLException");
        }
    }

    private class SessionListener implements SessionEvents.AuthListener, SessionEvents.LogoutListener {
        private SessionListener() {
        }

        /* synthetic */ SessionListener(LoginButton loginButton, SessionListener sessionListener) {
            this();
        }

        public void onAuthSucceed() {
            Log.d(LoginButton.TAG, "SessionListener:onAuthSucceed");
            LoginButton.this.setImageResource(R.drawable.logout_button);
            SessionStore.save(LoginButton.this.mFb, LoginButton.this.getContext());
        }

        public void onAuthFail(String error) {
            Log.d(LoginButton.TAG, "SessionListener:onAuthFail");
        }

        public void onLogoutBegin() {
            Log.d(LoginButton.TAG, "SessionListener:onLogoutBegin");
        }

        public void onLogoutFinish() {
            Log.d(LoginButton.TAG, "SessionListener:onLogoutFinish");
            SessionStore.clear(LoginButton.this.getContext());
            LoginButton.this.setImageResource(R.drawable.login_button);
        }
    }
}
