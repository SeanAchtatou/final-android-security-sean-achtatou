package com.flurry.android;

public final class OfferInSdk {
    long a;
    ag b;
    String c;
    String d;
    int e;
    String f;
    AdImage g;

    OfferInSdk(long j, String str, AdImage adImage, String str2, String str3, int i) {
        this.a = j;
        this.c = str2;
        this.f = str;
        this.g = adImage;
        this.d = str3;
        this.e = i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[id=" + this.a).append(",name=" + this.c + "]");
        return sb.toString();
    }
}
