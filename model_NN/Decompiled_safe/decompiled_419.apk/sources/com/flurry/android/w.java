package com.flurry.android;

import android.content.Context;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class w {
    private Context a;
    private q b;
    private a c;
    private volatile long d;
    private ae e = new ae(30);
    private ae f = new ae(30);
    private Map g = new HashMap();
    private Map h = new HashMap();
    private Map i = new HashMap();
    private Map j = new HashMap();
    private volatile boolean k;

    public w(Context context, q qVar, a aVar) {
        this.a = context;
        this.b = qVar;
        this.c = aVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized s[] a(String str) {
        s[] sVarArr;
        sVarArr = (s[]) this.g.get(str);
        if (sVarArr == null) {
            sVarArr = (s[]) this.g.get("");
        }
        return sVarArr;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ak a(long j2) {
        return (ak) this.f.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set a() {
        return this.e.c();
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage b(long j2) {
        return (AdImage) this.e.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized AdImage a(short s) {
        Long l;
        l = (Long) this.j.get((short) 1);
        return l == null ? null : b(l.longValue());
    }

    /* access modifiers changed from: package-private */
    public final synchronized f b(String str) {
        f fVar;
        fVar = (f) this.h.get(str);
        if (fVar == null) {
            fVar = (f) this.h.get("");
        }
        return fVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.k;
    }

    private synchronized d a(byte b2) {
        return (d) this.i.get(Byte.valueOf(b2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.d = System.currentTimeMillis();
        for (Map.Entry entry : map4.entrySet()) {
            if (entry.getValue() != null) {
                this.e.a(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : map5.entrySet()) {
            if (entry2.getValue() != null) {
                this.f.a(entry2.getKey(), entry2.getValue());
            }
        }
        if (map2 != null && !map2.isEmpty()) {
            this.h = map2;
        }
        if (map3 != null && !map3.isEmpty()) {
            this.i = map3;
        }
        if (map6 != null && !map6.isEmpty()) {
            this.j = map6;
        }
        this.g = new HashMap();
        for (Map.Entry entry3 : map2.entrySet()) {
            f fVar = (f) entry3.getValue();
            s[] sVarArr = (s[]) map.get(Byte.valueOf(fVar.b));
            if (sVarArr != null) {
                this.g.put(entry3.getKey(), sVarArr);
            }
            d dVar = (d) map3.get(Byte.valueOf(fVar.c));
            if (dVar != null) {
                fVar.d = dVar;
            }
        }
        f();
        a((int) CallbackEvent.ADS_UPDATED);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:29:0x004c=Splitter:B:29:0x004c, B:12:0x002f=Splitter:B:12:0x002f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void d() {
        /*
            r6 = this;
            java.lang.String r0 = "FlurryAgent"
            monitor-enter(r6)
            android.content.Context r0 = r6.a     // Catch:{ all -> 0x0048 }
            java.lang.String r1 = r6.g()     // Catch:{ all -> 0x0048 }
            java.io.File r0 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x0048 }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x0050
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0073, all -> 0x004b }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0073, all -> 0x004b }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0073, all -> 0x004b }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x0073, all -> 0x004b }
            int r1 = r3.readUnsignedShort()     // Catch:{ Throwable -> 0x0038, all -> 0x006d }
            r2 = 46587(0xb5fb, float:6.5282E-41)
            if (r1 != r2) goto L_0x0034
            r6.a(r3)     // Catch:{ Throwable -> 0x0038, all -> 0x006d }
            r1 = 201(0xc9, float:2.82E-43)
            r6.a(r1)     // Catch:{ Throwable -> 0x0038, all -> 0x006d }
        L_0x002f:
            com.flurry.android.i.a(r3)     // Catch:{ all -> 0x0048 }
        L_0x0032:
            monitor-exit(r6)
            return
        L_0x0034:
            a(r0)     // Catch:{ Throwable -> 0x0038, all -> 0x006d }
            goto L_0x002f
        L_0x0038:
            r1 = move-exception
            r2 = r3
        L_0x003a:
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = "Discarding cache"
            com.flurry.android.ah.a(r3, r4, r1)     // Catch:{ all -> 0x0070 }
            a(r0)     // Catch:{ all -> 0x0070 }
            com.flurry.android.i.a(r2)     // Catch:{ all -> 0x0048 }
            goto L_0x0032
        L_0x0048:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            com.flurry.android.i.a(r1)     // Catch:{ all -> 0x0048 }
            throw r0     // Catch:{ all -> 0x0048 }
        L_0x0050:
            java.lang.String r1 = "FlurryAgent"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0048 }
            r2.<init>()     // Catch:{ all -> 0x0048 }
            java.lang.String r3 = "cache file does not exist, path="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0048 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ all -> 0x0048 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0048 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0048 }
            com.flurry.android.ah.c(r1, r0)     // Catch:{ all -> 0x0048 }
            goto L_0x0032
        L_0x006d:
            r0 = move-exception
            r1 = r3
            goto L_0x004c
        L_0x0070:
            r0 = move-exception
            r1 = r2
            goto L_0x004c
        L_0x0073:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.w.d():void");
    }

    private static void a(File file) {
        if (!file.delete()) {
            ah.b("FlurryAgent", "Cannot delete cached ads");
        }
    }

    private void f() {
        Iterator it = this.i.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
        for (s[] sVarArr : this.g.values()) {
            if (sVarArr != null) {
                for (s sVar : sVarArr) {
                    sVar.h = b(sVar.f.longValue());
                    a(sVar.a);
                }
            }
        }
        for (f fVar : this.h.values()) {
            if (fVar.d == null) {
                fVar.d = a(fVar.c);
            }
            if (fVar.d == null) {
                ah.d("FlurryAgent", "No ad theme found for " + ((int) fVar.c));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void e() {
        DataOutputStream dataOutputStream;
        try {
            File fileStreamPath = this.a.getFileStreamPath(g());
            File parentFile = fileStreamPath.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                try {
                    dataOutputStream2.writeShort(46587);
                    a(dataOutputStream2);
                    i.a(dataOutputStream2);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    dataOutputStream = dataOutputStream2;
                    th = th2;
                    i.a(dataOutputStream);
                    throw th;
                }
            } else {
                ah.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                i.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            i.a(dataOutputStream);
            throw th;
        }
    }

    private void a(DataInputStream dataInputStream) {
        ah.a("FlurryAgent", "Reading cache");
        if (dataInputStream.readUnsignedShort() == 2) {
            this.d = dataInputStream.readLong();
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            this.e = new ae(30);
            for (int i2 = 0; i2 < readUnsignedShort; i2++) {
                long readLong = dataInputStream.readLong();
                AdImage adImage = new AdImage();
                adImage.a(dataInputStream);
                this.e.a(Long.valueOf(readLong), adImage);
            }
            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
            this.f = new ae(30);
            for (int i3 = 0; i3 < readUnsignedShort2; i3++) {
                long readLong2 = dataInputStream.readLong();
                ak akVar = new ak();
                if (dataInputStream.readBoolean()) {
                    akVar.a = dataInputStream.readUTF();
                }
                if (dataInputStream.readBoolean()) {
                    akVar.b = dataInputStream.readUTF();
                }
                akVar.c = dataInputStream.readInt();
                this.f.a(Long.valueOf(readLong2), akVar);
            }
            int readUnsignedShort3 = dataInputStream.readUnsignedShort();
            this.h = new HashMap(readUnsignedShort3);
            for (int i4 = 0; i4 < readUnsignedShort3; i4++) {
                this.h.put(dataInputStream.readUTF(), new f(dataInputStream));
            }
            int readUnsignedShort4 = dataInputStream.readUnsignedShort();
            this.g = new HashMap(readUnsignedShort4);
            for (int i5 = 0; i5 < readUnsignedShort4; i5++) {
                String readUTF = dataInputStream.readUTF();
                int readUnsignedShort5 = dataInputStream.readUnsignedShort();
                s[] sVarArr = new s[readUnsignedShort5];
                for (int i6 = 0; i6 < readUnsignedShort5; i6++) {
                    s sVar = new s();
                    sVar.a(dataInputStream);
                    sVarArr[i6] = sVar;
                }
                this.g.put(readUTF, sVarArr);
            }
            int readUnsignedShort6 = dataInputStream.readUnsignedShort();
            this.i = new HashMap();
            for (int i7 = 0; i7 < readUnsignedShort6; i7++) {
                byte readByte = dataInputStream.readByte();
                d dVar = new d();
                dVar.b(dataInputStream);
                this.i.put(Byte.valueOf(readByte), dVar);
            }
            int readUnsignedShort7 = dataInputStream.readUnsignedShort();
            this.j = new HashMap(readUnsignedShort7);
            for (int i8 = 0; i8 < readUnsignedShort7; i8++) {
                this.j.put(Short.valueOf(dataInputStream.readShort()), Long.valueOf(dataInputStream.readLong()));
            }
            f();
            ah.a("FlurryAgent", "Cache read, num images: " + this.e.a());
        }
    }

    private void a(DataOutputStream dataOutputStream) {
        dataOutputStream.writeShort(2);
        dataOutputStream.writeLong(this.d);
        Collection<Map.Entry> b2 = this.e.b();
        dataOutputStream.writeShort(b2.size());
        for (Map.Entry entry : b2) {
            dataOutputStream.writeLong(((Long) entry.getKey()).longValue());
            AdImage adImage = (AdImage) entry.getValue();
            dataOutputStream.writeLong(adImage.a);
            dataOutputStream.writeInt(adImage.b);
            dataOutputStream.writeInt(adImage.c);
            dataOutputStream.writeUTF(adImage.d);
            dataOutputStream.writeInt(adImage.e.length);
            dataOutputStream.write(adImage.e);
        }
        Collection<Map.Entry> b3 = this.f.b();
        dataOutputStream.writeShort(b3.size());
        for (Map.Entry entry2 : b3) {
            dataOutputStream.writeLong(((Long) entry2.getKey()).longValue());
            ak akVar = (ak) entry2.getValue();
            boolean z = akVar.a != null;
            dataOutputStream.writeBoolean(z);
            if (z) {
                dataOutputStream.writeUTF(akVar.a);
            }
            boolean z2 = akVar.b != null;
            dataOutputStream.writeBoolean(z2);
            if (z2) {
                dataOutputStream.writeUTF(akVar.b);
            }
            dataOutputStream.writeInt(akVar.c);
        }
        dataOutputStream.writeShort(this.h.size());
        for (Map.Entry entry3 : this.h.entrySet()) {
            dataOutputStream.writeUTF((String) entry3.getKey());
            f fVar = (f) entry3.getValue();
            dataOutputStream.writeUTF(fVar.a);
            dataOutputStream.writeByte(fVar.b);
            dataOutputStream.writeByte(fVar.c);
        }
        dataOutputStream.writeShort(this.g.size());
        for (Map.Entry entry4 : this.g.entrySet()) {
            dataOutputStream.writeUTF((String) entry4.getKey());
            s[] sVarArr = (s[]) entry4.getValue();
            int length = sVarArr == null ? 0 : sVarArr.length;
            dataOutputStream.writeShort(length);
            for (int i2 = 0; i2 < length; i2++) {
                s sVar = sVarArr[i2];
                dataOutputStream.writeLong(sVar.a);
                dataOutputStream.writeLong(sVar.b);
                dataOutputStream.writeUTF(sVar.d);
                dataOutputStream.writeUTF(sVar.c);
                dataOutputStream.writeLong(sVar.e);
                dataOutputStream.writeLong(sVar.f.longValue());
                dataOutputStream.writeByte(sVar.g.length);
                dataOutputStream.write(sVar.g);
            }
        }
        dataOutputStream.writeShort(this.i.size());
        for (Map.Entry entry5 : this.i.entrySet()) {
            dataOutputStream.writeByte(((Byte) entry5.getKey()).byteValue());
            ((d) entry5.getValue()).a(dataOutputStream);
        }
        dataOutputStream.writeShort(this.j.size());
        for (Map.Entry entry6 : this.j.entrySet()) {
            dataOutputStream.writeShort(((Short) entry6.getKey()).shortValue());
            dataOutputStream.writeLong(((Long) entry6.getValue()).longValue());
        }
    }

    private String g() {
        return ".flurryappcircle." + Integer.toString(this.c.a.hashCode(), 16);
    }

    private void a(int i2) {
        this.k = !this.g.isEmpty();
        if (this.k) {
            this.b.a(i2);
        }
    }
}
