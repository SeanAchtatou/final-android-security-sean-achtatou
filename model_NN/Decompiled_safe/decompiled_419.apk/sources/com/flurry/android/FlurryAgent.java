package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.view.View;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public final class FlurryAgent implements LocationListener {
    private static String V;
    private static volatile String a = null;
    private static volatile String b = null;
    private static volatile String c = "http://ad.flurry.com/getCanvas.do";
    private static volatile String d = null;
    private static volatile String e = "http://ad.flurry.com/getAndroidApp.do";
    /* access modifiers changed from: private */
    public static final FlurryAgent f = new FlurryAgent();
    /* access modifiers changed from: private */
    public static long g = 10000;
    private static boolean h = true;
    private static boolean i = false;
    private static boolean j = true;
    private static Criteria k = null;
    private static volatile String kInsecureReportUrl = "http://data.flurry.com/aap.do";
    private static volatile String kSecureReportUrl = "https://data.flurry.com/aap.do";
    /* access modifiers changed from: private */
    public static boolean l = false;
    private static AppCircle m = new AppCircle();
    private boolean A;
    private long B;
    /* access modifiers changed from: private */
    public List C = new ArrayList();
    private long D;
    private long E;
    private long F;
    private String G = "";
    private String H = "";
    private byte I = -1;
    private String J;
    private byte K = -1;
    private Long L;
    private int M;
    private Location N;
    private Map O;
    private List P;
    private boolean Q;
    private int R;
    private List S;
    private int T;
    /* access modifiers changed from: private */
    public q U;
    /* access modifiers changed from: private */
    public final Handler n;
    private File o = null;
    private volatile boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    private long r;
    private Map s = new WeakHashMap();
    private String t;
    private String u;
    private String v;
    private boolean w = true;
    private List x;
    private LocationManager y;
    private String z;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static /* synthetic */ void a(com.flurry.android.FlurryAgent r7, android.content.Context r8) {
        /*
            java.lang.String r5 = ""
            java.lang.String r4 = "FlurryAgent"
            java.io.File r0 = r7.o
            boolean r0 = r0.exists()
            if (r0 == 0) goto L_0x003b
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x004a, all -> 0x0059 }
            java.io.File r2 = r7.o     // Catch:{ Throwable -> 0x004a, all -> 0x0059 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x004a, all -> 0x0059 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x004a, all -> 0x0059 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x004a, all -> 0x0059 }
            int r0 = r2.readUnsignedShort()     // Catch:{ Throwable -> 0x006f, all -> 0x006a }
            r1 = 46586(0xb5fa, float:6.5281E-41)
            if (r0 != r1) goto L_0x0025
            r7.b(r2)     // Catch:{ Throwable -> 0x006f, all -> 0x006a }
        L_0x0025:
            com.flurry.android.i.a(r2)
        L_0x0028:
            boolean r0 = r7.q     // Catch:{ Throwable -> 0x0061 }
            if (r0 != 0) goto L_0x003b
            java.io.File r0 = r7.o     // Catch:{ Throwable -> 0x0061 }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x0061 }
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Cannot delete persistence file"
            com.flurry.android.ah.b(r0, r1)     // Catch:{ Throwable -> 0x0061 }
        L_0x003b:
            boolean r0 = r7.q
            if (r0 != 0) goto L_0x0049
            r0 = 0
            r7.A = r0
            long r0 = r7.D
            r7.B = r0
            r0 = 1
            r7.q = r0
        L_0x0049:
            return
        L_0x004a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x004e:
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = ""
            com.flurry.android.ah.b(r2, r3, r0)     // Catch:{ all -> 0x006d }
            com.flurry.android.i.a(r1)
            goto L_0x0028
        L_0x0059:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x005d:
            com.flurry.android.i.a(r1)
            throw r0
        L_0x0061:
            r0 = move-exception
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r1 = ""
            com.flurry.android.ah.b(r4, r5, r0)
            goto L_0x003b
        L_0x006a:
            r0 = move-exception
            r1 = r2
            goto L_0x005d
        L_0x006d:
            r0 = move-exception
            goto L_0x005d
        L_0x006f:
            r0 = move-exception
            r1 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void");
    }

    static /* synthetic */ void a(FlurryAgent flurryAgent, Context context, boolean z2) {
        Location location = null;
        if (z2) {
            try {
                location = flurryAgent.b(context);
            } catch (Throwable th) {
                ah.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (flurryAgent) {
            flurryAgent.N = location;
        }
        if (l) {
            flurryAgent.U.a();
        }
        flurryAgent.c(true);
    }

    static /* synthetic */ void b(FlurryAgent flurryAgent, Context context) {
        boolean z2;
        try {
            synchronized (flurryAgent) {
                z2 = !flurryAgent.p && SystemClock.elapsedRealtime() - flurryAgent.r > g && flurryAgent.C.size() > 0;
            }
            if (z2) {
                flurryAgent.c(false);
            }
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    static /* synthetic */ void c(FlurryAgent flurryAgent) {
        DataOutputStream dataOutputStream;
        IOException e2;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(1);
                dataOutputStream2.writeUTF(flurryAgent.v);
                dataOutputStream2.writeLong(flurryAgent.D);
                dataOutputStream2.writeLong(flurryAgent.F);
                dataOutputStream2.writeLong(0);
                dataOutputStream2.writeUTF(flurryAgent.G);
                dataOutputStream2.writeUTF(flurryAgent.H);
                dataOutputStream2.writeByte(flurryAgent.I);
                dataOutputStream2.writeUTF(flurryAgent.J);
                if (flurryAgent.N == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeDouble(flurryAgent.N.getLatitude());
                    dataOutputStream2.writeDouble(flurryAgent.N.getLongitude());
                    dataOutputStream2.writeFloat(flurryAgent.N.getAccuracy());
                }
                dataOutputStream2.writeInt(flurryAgent.T);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(flurryAgent.K);
                if (flurryAgent.L == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeLong(flurryAgent.L.longValue());
                }
                dataOutputStream2.writeShort(flurryAgent.O.size());
                for (Map.Entry entry : flurryAgent.O.entrySet()) {
                    dataOutputStream2.writeUTF((String) entry.getKey());
                    dataOutputStream2.writeInt(((k) entry.getValue()).a);
                }
                dataOutputStream2.writeShort(flurryAgent.P.size());
                for (n b2 : flurryAgent.P) {
                    dataOutputStream2.write(b2.b());
                }
                dataOutputStream2.writeBoolean(flurryAgent.Q);
                dataOutputStream2.writeInt(flurryAgent.M);
                dataOutputStream2.writeShort(flurryAgent.S.size());
                for (y yVar : flurryAgent.S) {
                    dataOutputStream2.writeLong(yVar.a);
                    dataOutputStream2.writeUTF(yVar.b);
                    dataOutputStream2.writeUTF(yVar.c);
                    dataOutputStream2.writeUTF(yVar.d);
                }
                if (l) {
                    List<ag> e3 = flurryAgent.U.e();
                    dataOutputStream2.writeShort(e3.size());
                    for (ag a2 : e3) {
                        a2.a(dataOutputStream2);
                    }
                } else {
                    dataOutputStream2.writeShort(0);
                }
                flurryAgent.C.add(byteArrayOutputStream.toByteArray());
                i.a(dataOutputStream2);
            } catch (IOException e4) {
                e2 = e4;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = dataOutputStream2;
                i.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e5) {
            IOException iOException = e5;
            dataOutputStream = null;
            e2 = iOException;
            try {
                ah.b("FlurryAgent", "", e2);
                i.a(dataOutputStream);
            } catch (Throwable th3) {
                th = th3;
                i.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th = th5;
            i.a(dataOutputStream);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, long):void
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        th.printStackTrace();
        String str = "";
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            StackTraceElement stackTraceElement = stackTrace[0];
            StringBuilder sb = new StringBuilder();
            sb.append(stackTraceElement.getClassName()).append(".").append(stackTraceElement.getMethodName()).append(":").append(stackTraceElement.getLineNumber());
            if (th.getMessage() != null) {
                sb.append(" (" + th.getMessage() + ")");
            }
            str = sb.toString();
        } else if (th.getMessage() != null) {
            str = th.getMessage();
        }
        onError("uncaught", str, th.getClass().toString());
        this.s.clear();
        a((Context) null, true);
    }

    private FlurryAgent() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.n = new Handler(handlerThread.getLooper());
    }

    public static void setCatalogIntentName(String str) {
        V = str;
    }

    public static AppCircle getAppCircle() {
        return m;
    }

    static View a(Context context, String str, int i2) {
        if (!l) {
            return null;
        }
        try {
            return f.U.a(context, str, i2);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
            return null;
        }
    }

    static void a(Context context, String str) {
        if (l) {
            f.U.a(context, str);
        }
    }

    static Offer a(String str) {
        if (!l) {
            return null;
        }
        return f.U.b(str);
    }

    static List b(String str) {
        if (!l) {
            return null;
        }
        return f.U.c(str);
    }

    static void a(Context context, long j2) {
        if (!l) {
            ah.d("FlurryAgent", "Cannot accept Offer. AppCircle is not enabled");
        }
        f.U.a(context, j2);
    }

    static void a(List list) {
        if (l) {
            f.U.a(list);
        }
    }

    static void a(boolean z2) {
        if (l) {
            f.U.a(z2);
        }
    }

    static boolean a() {
        return f.U.g();
    }

    public static void enableAppCircle() {
        l = true;
    }

    public static void setDefaultNoAdsMessage(String str) {
        if (l) {
            q.b = str == null ? "" : str;
        }
    }

    static void a(AppCircleCallback appCircleCallback) {
        f.U.a(appCircleCallback);
    }

    public static void addUserCookie(String str, String str2) {
        if (l) {
            f.U.a(str, str2);
        }
    }

    public static void clearUserCookies() {
        if (l) {
            f.U.j();
        }
    }

    public static void onStartSession(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                f.b(context, str);
            } catch (Throwable th) {
                ah.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, long):void
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    public static void onEndSession(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            f.a(context, false);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    public static void onPageView() {
        try {
            f.h();
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str) {
        try {
            f.a(str, (Map) null, false);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str, Map map) {
        try {
            f.a(str, map, false);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, boolean z2) {
        try {
            f.a(str, (Map) null, z2);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, Map map, boolean z2) {
        try {
            f.a(str, map, z2);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void endTimedEvent(String str) {
        try {
            f.c(str);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "Failed to signify the end of event: " + str, th);
        }
    }

    public static void onError(String str, String str2, String str3) {
        try {
            f.a(str, str2, str3);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    public static void setReportUrl(String str) {
        a = str;
    }

    public static void setCanvasUrl(String str) {
        b = str;
    }

    public static void setGetAppUrl(String str) {
        d = str;
    }

    public static void setVersionName(String str) {
        synchronized (f) {
            f.v = str;
        }
    }

    public static void setReportLocation(boolean z2) {
        synchronized (f) {
            f.w = z2;
        }
    }

    public static void setLocationCriteria(Criteria criteria) {
        synchronized (f) {
            k = criteria;
        }
    }

    public static void setAge(int i2) {
        if (i2 > 0 && i2 < 110) {
            Date date = new Date(new Date(System.currentTimeMillis() - (((long) i2) * 31449600000L)).getYear(), 1, 1);
            f.L = Long.valueOf(date.getTime());
        }
    }

    public static void setGender(byte b2) {
        switch (b2) {
            case 0:
            case Constants.MODE_PORTRAIT:
                f.K = b2;
                return;
            default:
                f.K = -1;
                return;
        }
    }

    public static int getAgentVersion() {
        return 112;
    }

    public static boolean getForbidPlaintextFallback() {
        return false;
    }

    public static void setLogEnabled(boolean z2) {
        synchronized (f) {
            if (z2) {
                ah.b();
            } else {
                ah.a();
            }
        }
    }

    public static void setLogLevel(int i2) {
        synchronized (f) {
            ah.a(i2);
        }
    }

    public static void setContinueSessionMillis(long j2) {
        synchronized (f) {
            g = j2;
        }
    }

    public static void setLogEvents(boolean z2) {
        synchronized (f) {
            h = z2;
        }
    }

    public static void setUserId(String str) {
        synchronized (f) {
            f.J = i.a(str, 128);
        }
    }

    public static void setCaptureUncaughtExceptions(boolean z2) {
        synchronized (f) {
            if (f.p) {
                ah.b("FlurryAgent", "Cannot setCaptureUncaughtExceptions after onSessionStart");
            } else {
                j = z2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str) {
        try {
            f.a(str, (Map) null, false);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str, Map map) {
        try {
            f.a(str, map, false);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    static q b() {
        return f.U;
    }

    private synchronized void b(Context context, String str) {
        String str2;
        ah.a("FlurryAgent", "startSession called");
        if (this.t != null && !this.t.equals(str)) {
            ah.b("FlurryAgent", "onStartSession called with different api keys: " + this.t + " and " + str);
        }
        if (((Context) this.s.put(context, context)) != null) {
            ah.d("FlurryAgent", "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
        }
        if (!this.p) {
            ah.a("FlurryAgent", "Initializing Flurry session");
            this.t = str;
            this.o = context.getFileStreamPath(".flurryagent." + Integer.toString(this.t.hashCode(), 16));
            if (j) {
                Thread.setDefaultUncaughtExceptionHandler(new g());
            }
            Context applicationContext = context.getApplicationContext();
            if (this.v == null) {
                this.v = a(applicationContext);
            }
            String packageName = applicationContext.getPackageName();
            if (this.u != null && !this.u.equals(packageName)) {
                ah.b("FlurryAgent", "onStartSession called from different application packages: " + this.u + " and " + packageName);
            }
            this.u = packageName;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.r > g) {
                ah.a("FlurryAgent", "Starting new session");
                String string = Settings.System.getString(context.getContentResolver(), "android_id");
                if (string == null || string.length() <= 0 || string.equals("null") || string.equals("9774d56d682e549c")) {
                    str2 = "ID" + Long.toString(Double.doubleToLongBits(Math.random()) + (37 * (System.nanoTime() + ((long) (this.t.hashCode() * 37)))), 16);
                } else {
                    str2 = "AND" + string;
                }
                this.z = str2;
                this.D = System.currentTimeMillis();
                this.E = elapsedRealtime;
                this.F = -1;
                this.J = "";
                this.M = 0;
                this.N = null;
                this.H = TimeZone.getDefault().getID();
                this.G = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
                this.O = new HashMap();
                this.P = new ArrayList();
                this.Q = true;
                this.S = new ArrayList();
                this.R = 0;
                this.T = 0;
                if (l && this.U == null) {
                    ah.a("FlurryAgent", "Initializing AppCircle");
                    a aVar = new a();
                    aVar.a = this.t;
                    aVar.b = this.z;
                    aVar.c = this.B;
                    aVar.d = this.D;
                    aVar.e = this.E;
                    aVar.f = b != null ? b : c;
                    aVar.g = c();
                    aVar.h = this.n;
                    this.U = new q(context, aVar);
                    if (V != null) {
                        q.a(V);
                    }
                    ah.a("FlurryAgent", "AppCircle initialized");
                }
                a(new e(this, applicationContext, this.w));
            } else {
                ah.a("FlurryAgent", "Continuing previous session");
                a(new b(this));
            }
            this.p = true;
        }
    }

    private synchronized void a(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.s.remove(context)) == null) {
                ah.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.p && this.s.isEmpty()) {
            ah.a("FlurryAgent", "Ending session");
            j();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.u.equals(packageName)) {
                    ah.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.u + " actual: " + packageName);
                }
            }
            this.p = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.r = elapsedRealtime;
            this.F = elapsedRealtime - this.E;
            a(new c(this, z2, applicationContext));
        }
    }

    private void a(Runnable runnable) {
        this.n.post(runnable);
    }

    private synchronized void h() {
        this.T++;
    }

    private synchronized void a(String str, Map map, boolean z2) {
        Map map2;
        if (this.P == null) {
            ah.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.E;
            String a2 = i.a(str, 128);
            if (a2.length() != 0) {
                k kVar = (k) this.O.get(a2);
                if (kVar != null) {
                    kVar.a++;
                } else if (this.O.size() < 100) {
                    k kVar2 = new k();
                    kVar2.a = 1;
                    this.O.put(a2, kVar2);
                } else if (ah.a("FlurryAgent")) {
                    ah.a("FlurryAgent", "MaxEventIds exceeded: " + a2);
                }
                if (!h || this.P.size() >= 100 || this.R >= 8000) {
                    this.Q = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() <= 10) {
                        n nVar = new n(a2, map2, elapsedRealtime, z2);
                        if (nVar.b().length + this.R <= 8000) {
                            this.P.add(nVar);
                            this.R = nVar.b().length + this.R;
                        } else {
                            this.R = 8000;
                            this.Q = false;
                        }
                    } else if (ah.a("FlurryAgent")) {
                        ah.a("FlurryAgent", "MaxEventParams exceeded: " + map2.size());
                    }
                }
            }
        }
    }

    private synchronized void c(String str) {
        Iterator it = this.P.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            n nVar = (n) it.next();
            if (nVar.a(str)) {
                nVar.a();
                break;
            }
        }
    }

    private synchronized void a(String str, String str2, String str3) {
        if (this.S == null) {
            ah.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.M++;
            if (this.S.size() < 10) {
                y yVar = new y();
                yVar.a = System.currentTimeMillis();
                yVar.b = i.a(str, 128);
                yVar.c = i.a(str2, 512);
                yVar.d = i.a(str3, 128);
                this.S.add(yVar);
            }
        }
    }

    private synchronized byte[] b(boolean z2) {
        DataOutputStream dataOutputStream;
        byte[] bArr;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(15);
                if (!l || !z2) {
                    dataOutputStream2.writeShort(0);
                } else {
                    dataOutputStream2.writeShort(1);
                }
                if (l) {
                    dataOutputStream2.writeLong(this.U.c());
                    Set<Long> d2 = this.U.d();
                    dataOutputStream2.writeShort(d2.size());
                    for (Long longValue : d2) {
                        long longValue2 = longValue.longValue();
                        dataOutputStream2.writeByte(1);
                        dataOutputStream2.writeLong(longValue2);
                    }
                } else {
                    dataOutputStream2.writeLong(0);
                    dataOutputStream2.writeShort(0);
                }
                dataOutputStream2.writeShort(3);
                dataOutputStream2.writeShort(112);
                dataOutputStream2.writeLong(System.currentTimeMillis());
                dataOutputStream2.writeUTF(this.t);
                dataOutputStream2.writeUTF(this.v);
                dataOutputStream2.writeShort(0);
                dataOutputStream2.writeUTF(this.z);
                dataOutputStream2.writeLong(this.B);
                dataOutputStream2.writeLong(this.D);
                dataOutputStream2.writeShort(6);
                dataOutputStream2.writeUTF("device.model");
                dataOutputStream2.writeUTF(Build.MODEL);
                dataOutputStream2.writeUTF("build.brand");
                dataOutputStream2.writeUTF(Build.BRAND);
                dataOutputStream2.writeUTF("build.id");
                dataOutputStream2.writeUTF(Build.ID);
                dataOutputStream2.writeUTF("version.release");
                dataOutputStream2.writeUTF(Build.VERSION.RELEASE);
                dataOutputStream2.writeUTF("build.device");
                dataOutputStream2.writeUTF(Build.DEVICE);
                dataOutputStream2.writeUTF("build.product");
                dataOutputStream2.writeUTF(Build.PRODUCT);
                int size = this.C.size();
                dataOutputStream2.writeShort(size);
                for (int i2 = 0; i2 < size; i2++) {
                    dataOutputStream2.write((byte[]) this.C.get(i2));
                }
                this.x = new ArrayList(this.C);
                dataOutputStream2.close();
                bArr = byteArrayOutputStream.toByteArray();
                i.a(dataOutputStream2);
            } catch (IOException e2) {
                e = e2;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th) {
                th = th;
                dataOutputStream = dataOutputStream2;
                i.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            dataOutputStream = null;
            try {
                ah.b("FlurryAgent", "", e);
                i.a(dataOutputStream);
                bArr = null;
                return bArr;
            } catch (Throwable th2) {
                th = th2;
                i.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            i.a(dataOutputStream);
            throw th;
        }
        return bArr;
    }

    static String c() {
        return d != null ? d : e;
    }

    static boolean d() {
        if (!l) {
            return false;
        }
        return f.U.l();
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String str = a != null ? a : kInsecureReportUrl;
        if (str == null) {
            return false;
        }
        try {
            z2 = a(bArr, str);
        } catch (Exception e2) {
            ah.a("FlurryAgent", "Sending report exception: " + e2.getMessage());
            z2 = false;
        }
        if (z2 || a != null) {
        }
        return z2;
    }

    private boolean a(byte[] bArr, String str) {
        if ("local".equals(str)) {
            return true;
        }
        ah.a("FlurryAgent", "Sending report to: " + str);
        boolean z2 = false;
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType("application/octet-stream");
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        HttpResponse execute = new DefaultHttpClient().execute(httpPost);
        int statusCode = execute.getStatusLine().getStatusCode();
        synchronized (this) {
            if (statusCode == 200) {
                ah.a("FlurryAgent", "Report successful");
                this.A = true;
                this.C.removeAll(this.x);
                HttpEntity entity = execute.getEntity();
                ah.a("FlurryAgent", "Processing report response");
                if (entity == null || entity.getContentLength() == 0) {
                    z2 = true;
                } else {
                    try {
                        a(new DataInputStream(entity.getContent()));
                        entity.consumeContent();
                        z2 = true;
                    } catch (Throwable th) {
                        entity.consumeContent();
                        throw th;
                    }
                }
            } else {
                ah.a("FlurryAgent", "Report failed. HTTP response: " + statusCode);
            }
        }
        this.x = null;
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v12, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r15) {
        /*
            r14 = this;
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
        L_0x001e:
            int r7 = r15.readUnsignedShort()
            int r0 = r15.readInt()
            switch(r7) {
                case 258: goto L_0x005f;
                case 259: goto L_0x0063;
                case 260: goto L_0x0029;
                case 261: goto L_0x0029;
                case 262: goto L_0x0082;
                case 263: goto L_0x00b4;
                case 264: goto L_0x0044;
                case 265: goto L_0x0029;
                case 266: goto L_0x00cd;
                case 267: goto L_0x0029;
                case 268: goto L_0x0104;
                case 269: goto L_0x014b;
                case 270: goto L_0x00c8;
                case 271: goto L_0x00e5;
                case 272: goto L_0x0121;
                case 273: goto L_0x0150;
                default: goto L_0x0029;
            }
        L_0x0029:
            java.lang.String r8 = "FlurryAgent"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Unknown chunkType: "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            com.flurry.android.ah.a(r8, r9)
            r15.skipBytes(r0)
        L_0x0044:
            r0 = 264(0x108, float:3.7E-43)
            if (r7 != r0) goto L_0x001e
            boolean r0 = com.flurry.android.FlurryAgent.l
            if (r0 == 0) goto L_0x005e
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x0059
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r7 = "No ads from server"
            com.flurry.android.ah.a(r0, r7)
        L_0x0059:
            com.flurry.android.q r0 = r14.U
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x005e:
            return
        L_0x005f:
            r15.readInt()
            goto L_0x0044
        L_0x0063:
            byte r0 = r15.readByte()
            int r8 = r15.readUnsignedShort()
            com.flurry.android.s[] r9 = new com.flurry.android.s[r8]
            r10 = 0
        L_0x006e:
            if (r10 >= r8) goto L_0x007a
            com.flurry.android.s r11 = new com.flurry.android.s
            r11.<init>(r15)
            r9[r10] = r11
            int r10 = r10 + 1
            goto L_0x006e
        L_0x007a:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r1.put(r0, r9)
            goto L_0x0044
        L_0x0082:
            int r0 = r15.readUnsignedShort()
            r8 = 0
        L_0x0087:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.AdImage r9 = new com.flurry.android.AdImage
            r9.<init>(r15)
            long r10 = r9.a
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r4.put(r10, r9)
            java.lang.String r10 = "FlurryAgent"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Parsed image: "
            java.lang.StringBuilder r11 = r11.append(r12)
            long r12 = r9.a
            java.lang.StringBuilder r9 = r11.append(r12)
            java.lang.String r9 = r9.toString()
            com.flurry.android.ah.a(r10, r9)
            int r8 = r8 + 1
            goto L_0x0087
        L_0x00b4:
            int r0 = r15.readInt()
            r8 = 0
        L_0x00b9:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.f r9 = new com.flurry.android.f
            r9.<init>(r15)
            java.lang.String r10 = r9.a
            r2.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00b9
        L_0x00c8:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x00cd:
            byte r0 = r15.readByte()
            r8 = 0
        L_0x00d2:
            if (r8 >= r0) goto L_0x0044
            com.flurry.android.d r9 = new com.flurry.android.d
            r9.<init>(r15)
            byte r10 = r9.a
            java.lang.Byte r10 = java.lang.Byte.valueOf(r10)
            r3.put(r10, r9)
            int r8 = r8 + 1
            goto L_0x00d2
        L_0x00e5:
            byte r8 = r15.readByte()
            r0 = 0
            r9 = r0
        L_0x00eb:
            if (r9 >= r8) goto L_0x0044
            byte r0 = r15.readByte()
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object r0 = r3.get(r0)
            com.flurry.android.d r0 = (com.flurry.android.d) r0
            if (r0 == 0) goto L_0x0100
            r0.a(r15)
        L_0x0100:
            int r0 = r9 + 1
            r9 = r0
            goto L_0x00eb
        L_0x0104:
            int r0 = r15.readInt()
            r8 = 0
        L_0x0109:
            if (r8 >= r0) goto L_0x0044
            long r9 = r15.readLong()
            short r11 = r15.readShort()
            java.lang.Short r11 = java.lang.Short.valueOf(r11)
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r6.put(r11, r9)
            int r8 = r8 + 1
            goto L_0x0109
        L_0x0121:
            long r8 = r15.readLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r8)
            java.lang.Object r0 = r5.get(r0)
            com.flurry.android.ak r0 = (com.flurry.android.ak) r0
            if (r0 != 0) goto L_0x0136
            com.flurry.android.ak r0 = new com.flurry.android.ak
            r0.<init>()
        L_0x0136:
            java.lang.String r10 = r15.readUTF()
            r0.a = r10
            int r10 = r15.readInt()
            r0.c = r10
            java.lang.Long r8 = java.lang.Long.valueOf(r8)
            r5.put(r8, r0)
            goto L_0x0044
        L_0x014b:
            r15.skipBytes(r0)
            goto L_0x0044
        L_0x0150:
            r15.skipBytes(r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(java.io.DataInputStream):void");
    }

    private void c(boolean z2) {
        String str;
        try {
            byte[] b2 = b(z2);
            if (b2 != null && a(b2)) {
                StringBuilder append = new StringBuilder().append("Done sending ");
                if (this.p) {
                    str = "initial ";
                } else {
                    str = "";
                }
                ah.a("FlurryAgent", append.append(str).append("agent report").toString());
                i();
            }
        } catch (IOException e2) {
            ah.a("FlurryAgent", "", e2);
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    private synchronized void b(DataInputStream dataInputStream) {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        if (readUnsignedShort > 2) {
            throw new IOException("Unknown agent file version: " + readUnsignedShort);
        } else if (readUnsignedShort >= 2) {
            String readUTF = dataInputStream.readUTF();
            if (readUTF.equals(this.t)) {
                this.z = dataInputStream.readUTF();
                this.A = dataInputStream.readBoolean();
                this.B = dataInputStream.readLong();
                while (true) {
                    int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort2 == 0) {
                        break;
                    }
                    byte[] bArr = new byte[readUnsignedShort2];
                    dataInputStream.readFully(bArr);
                    this.C.add(0, bArr);
                }
                this.q = true;
            } else {
                ah.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.t);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void i() {
        DataOutputStream dataOutputStream;
        try {
            File parentFile = this.o.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.o));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.t);
                    dataOutputStream.writeUTF(this.z);
                    dataOutputStream.writeBoolean(this.A);
                    dataOutputStream.writeLong(this.B);
                    for (int size = this.C.size() - 1; size >= 0; size--) {
                        byte[] bArr = (byte[]) this.C.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                    }
                    dataOutputStream.writeShort(0);
                    i.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        ah.b("FlurryAgent", "", th);
                        i.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        i.a(dataOutputStream);
                        throw th;
                    }
                }
            } else {
                ah.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                i.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            i.a(dataOutputStream);
            throw th;
        }
    }

    private static String a(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
    }

    private Location b(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.y == null) {
                    this.y = locationManager;
                } else {
                    locationManager = this.y;
                }
            }
            Criteria criteria = k;
            if (criteria == null) {
                criteria = new Criteria();
            }
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    private synchronized void j() {
        if (this.y != null) {
            this.y.removeUpdates(this);
        }
    }

    public final synchronized void onLocationChanged(Location location) {
        try {
            this.N = location;
            j();
        } catch (Throwable th) {
            ah.b("FlurryAgent", "", th);
        }
        return;
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
