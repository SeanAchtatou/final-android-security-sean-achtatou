package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: v  reason: default package */
public final class v implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("action");
        if (webView instanceof g) {
            AdActivity b = ((g) webView).b();
            if (b == null) {
                d.a("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = (String) hashMap.get("url");
                Activity e = hVar.e();
                if (e == null) {
                    d.e("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(e);
                videoView.setVideoPath(str2);
                b.a(videoView);
            } else {
                VideoView a = b.a();
                if (a == null) {
                    d.e("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    a.setVisibility(0);
                    a.start();
                    d.d("Video is now playing.");
                    l.a(webView, "onVideoEvent", "{'event': 'playing'}");
                } else if (str.equals("pause")) {
                    a.pause();
                } else if (str.equals("stop")) {
                    a.stopPlayback();
                } else if (str.equals("remove")) {
                    a.setVisibility(8);
                } else if (str.equals("replay")) {
                    a.setVisibility(0);
                    a.seekTo(0);
                    a.start();
                } else if (str.equals("currentTime")) {
                    String str3 = (String) hashMap.get("time");
                    if (str3 == null) {
                        d.e("No \"time\" parameter!");
                    } else {
                        a.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    d.e("Unknown movie action: " + str);
                }
            }
        } else {
            d.a("Could not get adWebView to create the video.");
        }
    }
}
