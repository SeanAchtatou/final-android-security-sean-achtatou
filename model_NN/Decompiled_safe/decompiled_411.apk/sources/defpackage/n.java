package defpackage;

import android.webkit.WebView;
import com.google.ads.g;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: n  reason: default package */
public final class n implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        d.e("Invalid " + ((String) hashMap.get("type")) + " request error: " + ((String) hashMap.get("errors")));
        k g = hVar.g();
        if (g != null) {
            g.a(g.INVALID_REQUEST);
        }
    }
}
