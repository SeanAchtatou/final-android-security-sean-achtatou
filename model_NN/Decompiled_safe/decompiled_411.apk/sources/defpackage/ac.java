package defpackage;

import com.google.ads.util.d;
import java.lang.ref.WeakReference;

/* renamed from: ac  reason: default package */
public final class ac implements Runnable {
    private WeakReference a;

    public ac(h hVar) {
        this.a = new WeakReference(hVar);
    }

    public final void run() {
        h hVar = (h) this.a.get();
        if (hVar == null) {
            d.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            hVar.w();
        }
    }
}
