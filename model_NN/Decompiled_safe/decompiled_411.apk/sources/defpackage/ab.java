package defpackage;

import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;

/* renamed from: ab  reason: default package */
public final class ab implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("url");
        d.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + ((String) hashMap.get("afma_notify_dt")) + "\">");
        k g = hVar.g();
        if (g != null) {
            g.b(str);
        }
    }
}
