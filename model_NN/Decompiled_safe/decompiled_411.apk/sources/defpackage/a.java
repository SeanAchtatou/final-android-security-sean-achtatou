package defpackage;

import android.webkit.WebView;
import com.google.ads.g;

/* renamed from: a  reason: default package */
final class a implements Runnable {
    private final h a;
    private final WebView b;
    private final j c;
    private final g d;
    private final boolean e;
    private /* synthetic */ k f;

    public a(k kVar, h hVar, WebView webView, j jVar, g gVar, boolean z) {
        this.f = kVar;
        this.a = hVar;
        this.b = webView;
        this.c = jVar;
        this.d = gVar;
        this.e = z;
    }

    public final void run() {
        this.b.stopLoading();
        this.b.destroy();
        this.c.a();
        if (this.e) {
            g i = this.a.i();
            i.stopLoading();
            i.setVisibility(8);
        }
        this.a.a(this.d);
    }
}
