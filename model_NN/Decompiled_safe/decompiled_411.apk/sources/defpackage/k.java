package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.a;
import com.google.ads.c;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: k  reason: default package */
public final class k implements Runnable {
    private String a;
    private String b = null;
    private j c;
    private h d;
    private c e;
    private WebView f;
    private String g = null;
    private LinkedList h = new LinkedList();
    private volatile boolean i;
    private g j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;

    public k(h hVar) {
        this.d = hVar;
        Activity e2 = hVar.e();
        if (e2 != null) {
            this.f = new g(e2.getApplicationContext(), null);
            this.f.setWebViewClient(new s(hVar, l.a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new j(this, hVar);
            return;
        }
        this.f = null;
        this.c = null;
        d.e("activity was null while trying to create an AdLoader.");
    }

    private String a(c cVar, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map a2 = cVar.a(applicationContext);
        f l2 = this.d.l();
        long h2 = l2.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = l2.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = l2.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = l2.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(f.i()));
        String j2 = l2.j();
        if (j2 != null) {
            a2.put("pai", j2);
        }
        if (l2.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (l2.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = l2.p();
        if (p != null) {
            a2.put("pit", p);
        }
        l2.a();
        l2.d();
        if (this.d.f() instanceof a) {
            a2.put("format", "interstitial_mb");
        } else {
            com.google.ads.d k2 = this.d.k();
            String dVar = k2.toString();
            if (dVar != null) {
                a2.put("format", dVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(k2.a()));
                hashMap.put("h", Integer.valueOf(k2.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.h());
        a2.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new d(this, "NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            a2.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                a2.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            d.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new b(this, "NameNotFoundException");
        }
    }

    private void a(g gVar, boolean z) {
        this.c.a();
        this.d.a(new a(this, this.d, this.f, this.c, gVar, z));
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        d.a("AdLoader cancelled.");
        this.f.stopLoading();
        this.f.destroy();
        if (this.m != null) {
            this.m.interrupt();
            this.m = null;
        }
        this.c.a();
        this.i = true;
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        this.e = cVar;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    public final synchronized void a(g gVar) {
        this.j = gVar;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.a = str2;
        this.b = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        this.k = true;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a(com.google.ads.g, boolean):void
     arg types: [com.google.ads.g, int]
     candidates:
      k.a(com.google.ads.c, android.app.Activity):java.lang.String
      k.a(java.lang.String, java.lang.String):void
      k.a(com.google.ads.g, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                d.e("adRequestWebView was null while trying to load an ad.");
                a(g.INTERNAL_ERROR, false);
                return;
            }
            Activity e2 = this.d.e();
            if (e2 == null) {
                d.e("activity was null while forming an ad request.");
                a(g.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.d.a(new c(this, this.f, null, a(this.e, e2)));
                long n = this.d.n();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (n > 0) {
                    try {
                        wait(n);
                    } catch (InterruptedException e3) {
                        d.a("AdLoader InterruptedException while getting the URL: " + e3);
                        return;
                    }
                }
                try {
                    if (!this.i) {
                        if (this.j != null) {
                            a(this.j, false);
                            return;
                        } else if (this.g == null) {
                            d.c("AdLoader timed out after " + n + "ms while getting the URL.");
                            a(g.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.c.a(this.g);
                            long elapsedRealtime2 = n - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e4) {
                                    d.a("AdLoader InterruptedException while getting the HTML: " + e4);
                                    return;
                                }
                            }
                            if (!this.i) {
                                if (this.j != null) {
                                    a(this.j, false);
                                    return;
                                } else if (this.b == null) {
                                    d.c("AdLoader timed out after " + n + "ms while getting the HTML.");
                                    a(g.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    g i2 = this.d.i();
                                    this.d.j().a();
                                    this.d.a(new c(this, i2, this.a, this.b));
                                    long elapsedRealtime3 = n - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e5) {
                                            d.a("AdLoader InterruptedException while loading the HTML: " + e5);
                                            return;
                                        }
                                    }
                                    if (this.k) {
                                        this.d.a(new e(this, this.d, this.h, this.l));
                                    } else {
                                        d.c("AdLoader timed out after " + n + "ms while loading the HTML.");
                                        a(g.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e6) {
                    d.a("An unknown error occurred in AdLoader.", e6);
                    a(g.INTERNAL_ERROR, true);
                }
            } catch (d e7) {
                d.c("Unable to connect to network: " + e7);
                a(g.NETWORK_ERROR, false);
                return;
            } catch (b e8) {
                d.c("Caught internal exception: " + e8);
                a(g.INTERNAL_ERROR, false);
                return;
            }
        }
    }
}
