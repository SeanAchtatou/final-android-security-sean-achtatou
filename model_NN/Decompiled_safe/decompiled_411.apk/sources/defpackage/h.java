package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.AdActivity;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.f;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: h  reason: default package */
public final class h {
    private static final Object a = new Object();
    private WeakReference b;
    private b c;
    private f d = null;
    private k e = null;
    private c f = null;
    private d g;
    private f h = new f();
    private String i;
    private g j;
    private s k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private ac r;
    private boolean s = false;
    private LinkedList t;
    private LinkedList u;
    private int v;

    public h(Activity activity, b bVar, d dVar, String str) {
        this.b = new WeakReference(activity);
        this.c = bVar;
        this.g = dVar;
        this.i = str;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new ac(this);
        this.t = new LinkedList();
        this.u = new LinkedList();
        a();
        AdUtil.g(activity.getApplicationContext());
    }

    private synchronized void A() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    private synchronized boolean y() {
        return this.e != null;
    }

    private synchronized void z() {
        Activity activity = (Activity) this.b.get();
        if (activity == null) {
            com.google.ads.util.d.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new u((String) it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            com.google.ads.util.d.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new g(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new s(this, l.b, true, false);
            } else {
                this.k = new s(this, l.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(c cVar) {
        if (y()) {
            com.google.ads.util.d.e("loadAd called while the ad is already loading, so aborting.");
        } else if (AdActivity.c()) {
            com.google.ads.util.d.e("loadAd called while an interstitial or landing page is displayed, so aborting");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                com.google.ads.util.d.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.t.clear();
                this.f = cVar;
                this.e = new k(this);
                this.e.a(cVar);
            }
        }
    }

    public final synchronized void a(f fVar) {
        this.d = fVar;
    }

    public final synchronized void a(g gVar) {
        this.e = null;
        if (this.c instanceof a) {
            if (gVar == g.NO_FILL) {
                this.h.n();
            } else if (gVar == g.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.d.c("onFailedToReceiveAd(" + gVar + ")");
        if (this.d != null) {
            this.d.a(this.c, gVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        com.google.ads.util.d.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList linkedList) {
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            com.google.ads.util.d.a("Adding a click tracking URL: " + ((String) it.next()));
        }
        this.u = linkedList;
    }

    public final synchronized void b() {
        a((f) null);
        x();
        this.j.destroy();
    }

    public final synchronized void c() {
        if (this.o) {
            com.google.ads.util.d.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            com.google.ads.util.d.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            com.google.ads.util.d.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            com.google.ads.util.d.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            com.google.ads.util.d.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.b.get();
    }

    public final b f() {
        return this.c;
    }

    public final synchronized k g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized g i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized s j() {
        return this.k;
    }

    public final d k() {
        return this.g;
    }

    public final f l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.v;
    }

    public final long n() {
        return this.m;
    }

    public final synchronized boolean o() {
        return this.n;
    }

    public final synchronized boolean p() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void q() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            z();
        }
        com.google.ads.util.d.c("onReceiveAd()");
        if (this.d != null) {
            this.d.a();
        }
    }

    public final synchronized void r() {
        this.h.o();
        com.google.ads.util.d.c("onDismissScreen()");
    }

    public final synchronized void s() {
        com.google.ads.util.d.c("onPresentScreen()");
    }

    public final synchronized void t() {
        com.google.ads.util.d.c("onLeaveApplication()");
    }

    public final void u() {
        this.h.b();
        A();
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean v() {
        return !this.u.isEmpty();
    }

    public final synchronized void w() {
        if (this.f == null) {
            com.google.ads.util.d.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.d.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.d.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.d.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void x() {
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        this.j.stopLoading();
    }
}
