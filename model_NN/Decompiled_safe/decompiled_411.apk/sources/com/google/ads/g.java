package com.google.ads;

public enum g {
    INVALID_REQUEST("Invalid Google Ad request."),
    NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
    NETWORK_ERROR("A network error occurred."),
    INTERNAL_ERROR("There was an internal error.");
    
    private String e;

    private g(String str) {
        this.e = str;
    }

    public final String toString() {
        return this.e;
    }
}
