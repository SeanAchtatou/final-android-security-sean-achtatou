package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.util.d;

public class AdView extends RelativeLayout implements b {
    private h a;

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet);
    }

    private void a(Activity activity, d dVar, String str) {
        this.a = new h(activity, this, dVar, str);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.i(), (int) TypedValue.applyDimension(1, (float) dVar.a(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) dVar.b(), activity.getResources().getDisplayMetrics()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x015d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r10 = this;
            r9 = 1
            r8 = 0
            if (r12 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r2 = "adSize"
            java.lang.String r1 = r12.getAttributeValue(r1, r2)
            if (r1 != 0) goto L_0x0017
            java.lang.String r1 = "AdView missing required XML attribute \"adSize\"."
            com.google.ads.d r2 = com.google.ads.d.a
            r10.a(r11, r1, r2, r12)
            goto L_0x0004
        L_0x0017:
            java.lang.String r2 = "BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0032
            com.google.ads.d r1 = com.google.ads.d.a
            r5 = r1
        L_0x0022:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r2 = "adUnitId"
            java.lang.String r1 = r12.getAttributeValue(r1, r2)
            if (r1 != 0) goto L_0x0075
            java.lang.String r1 = "AdView missing required XML attribute \"adUnitId\"."
            r10.a(r11, r1, r5, r12)
            goto L_0x0004
        L_0x0032:
            java.lang.String r2 = "IAB_MRECT"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x003e
            com.google.ads.d r1 = com.google.ads.d.b
            r5 = r1
            goto L_0x0022
        L_0x003e:
            java.lang.String r2 = "IAB_BANNER"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x004a
            com.google.ads.d r1 = com.google.ads.d.c
            r5 = r1
            goto L_0x0022
        L_0x004a:
            java.lang.String r2 = "IAB_LEADERBOARD"
            boolean r2 = r2.equals(r1)
            if (r2 == 0) goto L_0x0056
            com.google.ads.d r1 = com.google.ads.d.d
            r5 = r1
            goto L_0x0022
        L_0x0056:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid \"adSize\" value in XML layout: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.google.ads.d r2 = com.google.ads.d.a
            r10.a(r11, r1, r2, r12)
            goto L_0x0004
        L_0x0075:
            boolean r2 = r10.isInEditMode()
            if (r2 == 0) goto L_0x0085
            java.lang.String r3 = "Ads by Google"
            r4 = -1
            r1 = r10
            r2 = r11
            r6 = r12
            r1.a(r2, r3, r4, r5, r6)
            goto L_0x0004
        L_0x0085:
            java.lang.String r2 = "@string/"
            boolean r2 = r1.startsWith(r2)
            if (r2 == 0) goto L_0x0153
            java.lang.String r2 = "@string/"
            int r2 = r2.length()
            java.lang.String r2 = r1.substring(r2)
            java.lang.String r3 = r11.getPackageName()
            android.util.TypedValue r4 = new android.util.TypedValue
            r4.<init>()
            android.content.res.Resources r6 = r10.getResources()     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NotFoundException -> 0x0118 }
            r7.<init>()     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.String r7 = ":string/"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.String r2 = r2.toString()     // Catch:{ NotFoundException -> 0x0118 }
            r3 = 1
            r6.getValue(r2, r4, r3)     // Catch:{ NotFoundException -> 0x0118 }
            java.lang.CharSequence r2 = r4.string
            if (r2 == 0) goto L_0x0137
            java.lang.CharSequence r1 = r4.string
            java.lang.String r1 = r1.toString()
            r2 = r1
        L_0x00ca:
            java.lang.String r1 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "loadAdOnCreate"
            boolean r3 = r12.getAttributeBooleanValue(r1, r3, r8)
            boolean r1 = r11 instanceof android.app.Activity
            if (r1 == 0) goto L_0x015d
            r0 = r11
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            boolean r4 = com.google.ads.util.AdUtil.b(r11)
            if (r4 != 0) goto L_0x0156
            java.lang.String r4 = "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml."
            r10.a(r11, r4, r5, r12)
            r4 = r8
        L_0x00e6:
            if (r4 == 0) goto L_0x0004
            r10.a(r1, r5, r2)
            if (r3 == 0) goto L_0x0004
            com.google.ads.c r1 = new com.google.ads.c
            r1.<init>()
            java.lang.String r2 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "keywords"
            java.lang.String r2 = r12.getAttributeValue(r2, r3)
            if (r2 == 0) goto L_0x0158
            java.lang.String r3 = ","
            java.lang.String[] r2 = r2.split(r3)
            int r3 = r2.length
            r4 = r8
        L_0x0104:
            if (r4 >= r3) goto L_0x0158
            r5 = r2[r4]
            java.lang.String r5 = r5.trim()
            int r6 = r5.length()
            if (r6 == 0) goto L_0x0115
            r1.a(r5)
        L_0x0115:
            int r4 = r4 + 1
            goto L_0x0104
        L_0x0118:
            r2 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not find resource for \"adUnitId\": \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "\"."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r10.a(r11, r1, r5, r12)
            goto L_0x0004
        L_0x0137:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "\"adUnitId\" was not a string: \""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "\"."
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r10.a(r11, r2, r5, r12)
        L_0x0153:
            r2 = r1
            goto L_0x00ca
        L_0x0156:
            r4 = r9
            goto L_0x00e6
        L_0x0158:
            r10.a(r1)
            goto L_0x0004
        L_0x015d:
            java.lang.String r1 = "AdView was initialized with a Context that wasn't an Activity."
            com.google.ads.util.d.b(r1)
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdView.a(android.content.Context, android.util.AttributeSet):void");
    }

    private void a(Context context, String str, int i, d dVar, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(-16777216);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) dVar.a(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) dVar.b(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private void a(Context context, String str, d dVar, AttributeSet attributeSet) {
        d.b(str);
        a(context, str, -65536, dVar, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, dVar, "");
        } else {
            d.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    public final void a() {
        this.a.x();
    }

    public final void a(c cVar) {
        if (this.a.p()) {
            this.a.c();
        }
        this.a.a(cVar);
    }

    public final void a(f fVar) {
        this.a.a(fVar);
    }

    public final void b() {
        this.a.b();
    }

    public final boolean c() {
        if (this.a == null) {
            return false;
        }
        return this.a.o();
    }
}
