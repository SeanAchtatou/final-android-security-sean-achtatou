package jp.co.kixx.game.casino;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class PlayButton extends Button {
    public int a = 0;

    public PlayButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = attributeSet.getAttributeIntValue(null, "btn_id", 0);
    }

    public PlayButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
