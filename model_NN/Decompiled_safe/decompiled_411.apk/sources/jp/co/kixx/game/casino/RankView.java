package jp.co.kixx.game.casino;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.ads.AdView;
import com.google.ads.c;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;
import jp.co.nobot.libAdMaker.libAdMaker;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class RankView extends Activity implements Runnable {
    private TextView[] A = new TextView[50];
    private TextView B;
    private TextView C;
    private TextView D;
    private TextView E;
    private Button F;
    private Button G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private View.OnClickListener N = new aa(this);
    private View.OnClickListener O = new ab(this);
    private String[] P = {" Daily ", " Weekly ", " Monthly ", " Whole "};
    private String[] Q = {"[ Daily(24h) ]", "[ Weekly ]", "[ Monthly ]", "[ Whole ]"};
    private final int a = 4;
    private final int b = 50;
    private String c = "";
    private int d = 0;
    private int e = 0;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public int g = 0;
    private String[] h;
    private String[] i = new String[4];
    private int[][] j = ((int[][]) Array.newInstance(Integer.TYPE, 4, 50));
    private int[][] k = ((int[][]) Array.newInstance(Integer.TYPE, 4, 50));
    private String[][] l = ((String[][]) Array.newInstance(String.class, 4, 50));
    private String[] m = new String[4];
    /* access modifiers changed from: private */
    public Thread n;
    private ProgressDialog o;
    private AdView p;
    private libAdMaker q;
    private boolean r = false;
    /* access modifiers changed from: private */
    public int s = 1;
    private Handler t = new z(this);
    private ScrollView u;
    private LinearLayout v;
    private TextView w;
    private TextView[] x = new TextView[50];
    private TextView[] y = new TextView[50];
    private TextView[] z = new TextView[50];

    /* access modifiers changed from: private */
    public void a() {
        try {
            this.q = (libAdMaker) findViewById(C0000R.id.admakerview);
            this.q.setVisibility(0);
            this.q.a = "562";
            this.q.b = "1153";
            this.q.a("http://images.ad-maker.info/apps/5dcvv1v4pqra.html");
            this.q.d();
            this.r = true;
        } catch (NullPointerException e2) {
        }
    }

    static /* synthetic */ void a(RankView rankView) {
        if (rankView.o != null && rankView.o.isShowing()) {
            try {
                rankView.o.dismiss();
            } catch (IllegalArgumentException e2) {
            }
        }
    }

    static /* synthetic */ void a(RankView rankView, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        Drawable drawable;
        rankView.g = i2;
        rankView.w.setText(rankView.Q[rankView.g]);
        Drawable drawable2 = rankView.getResources().getDrawable(rankView.I);
        Drawable drawable3 = rankView.getResources().getDrawable(rankView.J);
        Drawable drawable4 = rankView.getResources().getDrawable(rankView.K);
        Drawable drawable5 = rankView.getResources().getDrawable(rankView.L);
        Drawable drawable6 = rankView.getResources().getDrawable(rankView.H);
        Drawable drawable7 = rankView.getResources().getDrawable(rankView.H);
        int color = rankView.getResources().getColor(C0000R.color.text_white);
        int color2 = rankView.getResources().getColor(C0000R.color.text_rank_mid);
        int color3 = rankView.getResources().getColor(C0000R.color.text_rank_low);
        int color4 = rankView.getResources().getColor(C0000R.color.text_yellow);
        int i7 = 0;
        int i8 = -1;
        int i9 = 0;
        while (i9 < 50) {
            if (rankView.j[rankView.g][i9] != i8 || rankView.j[rankView.g][i9] <= 0) {
                i4 = rankView.j[rankView.g][i9];
                i3 = i9 + 1;
            } else {
                i3 = i7;
                i4 = i8;
            }
            rankView.x[i9].setText(String.valueOf(i3));
            if (i3 > 20) {
                i5 = color3;
                i6 = color3;
                drawable = drawable7;
            } else if (i3 > 10) {
                i5 = color2;
                i6 = color2;
                drawable = drawable6;
            } else if (i3 > 3) {
                i5 = color4;
                i6 = color;
                drawable = drawable5;
            } else if (i3 > 2) {
                i5 = color4;
                i6 = color;
                drawable = drawable4;
            } else if (i3 > 1) {
                i5 = color4;
                i6 = color;
                drawable = drawable3;
            } else {
                i5 = color4;
                i6 = color;
                drawable = drawable2;
            }
            ((LinearLayout) rankView.x[i9].getParent()).setBackgroundDrawable(drawable);
            rankView.x[i9].setTextColor(i6);
            rankView.y[i9].setTextColor(i5);
            rankView.z[i9].setTextColor(i6);
            rankView.A[i9].setTextColor(i6);
            if (rankView.j[rankView.g][i9] > 0) {
                rankView.y[i9].setText(String.valueOf(rankView.j[rankView.g][i9]));
                rankView.z[i9].setText(String.valueOf(rankView.k[rankView.g][i9]));
                rankView.A[i9].setText(rankView.l[rankView.g][i9]);
            } else {
                rankView.y[i9].setText("---");
                rankView.z[i9].setText("-");
                rankView.A[i9].setText("");
            }
            i9++;
            i8 = i4;
            i7 = i3;
        }
        String str = " / " + rankView.i[rankView.g];
        if (Integer.parseInt(rankView.m[rankView.g]) > 0) {
            rankView.B.setText(String.valueOf(rankView.m[rankView.g]) + str);
            rankView.C.setText(String.valueOf(rankView.d));
            rankView.D.setText(String.valueOf(rankView.e));
        } else {
            rankView.B.setText("-" + str);
            rankView.C.setText("---");
            rankView.D.setText("-");
        }
        rankView.E.setText(rankView.c);
        rankView.b();
        rankView.u.scrollTo(0, 0);
    }

    private boolean a(String[] strArr) {
        int i2 = 0;
        int i3 = 0;
        while (i2 < 4) {
            if (strArr.length < i3 + 1 + 1) {
                return false;
            }
            int i4 = i3 + 1;
            this.i[i2] = strArr[i3];
            int i5 = i4 + 1;
            try {
                int parseInt = Integer.parseInt(strArr[i4]);
                if (strArr.length < (parseInt * 3) + i5) {
                    return false;
                }
                for (int i6 = 0; i6 < 50; i6++) {
                    if (parseInt > i6) {
                        try {
                            this.j[i2][i6] = Integer.parseInt(strArr[(i6 * 3) + i5 + 0]);
                        } catch (NumberFormatException e2) {
                            this.j[i2][i6] = 0;
                        }
                        try {
                            this.k[i2][i6] = Integer.parseInt(strArr[(i6 * 3) + i5 + 1]);
                        } catch (NumberFormatException e3) {
                            this.k[i2][i6] = 0;
                        }
                        this.l[i2][i6] = strArr[(i6 * 3) + i5 + 2];
                    } else {
                        this.j[i2][i6] = 0;
                        this.k[i2][i6] = 0;
                        this.l[i2][i6] = "";
                    }
                }
                i3 = i5 + (parseInt * 3);
                i2++;
            } catch (NumberFormatException e4) {
                return false;
            } catch (ArrayIndexOutOfBoundsException e5) {
                return false;
            }
        }
        int i7 = 0;
        while (i7 < 4) {
            this.m[i7] = strArr[i3];
            i7++;
            i3++;
        }
        return true;
    }

    private void b() {
        this.F.setText(this.P[((this.g + 4) - 1) % 4]);
        this.G.setText(this.P[(this.g + 1) % 4]);
    }

    static /* synthetic */ void f(RankView rankView) {
        rankView.n = null;
        rankView.finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getResources().getString(C0000R.string.screen_size).equals("xlarge")) {
            setRequestedOrientation(0);
        }
        if (!CasinoGamesActivity.a(this)) {
            setContentView((int) C0000R.layout.rank);
        } else {
            setContentView((int) C0000R.layout.q_rank);
        }
        this.d = getIntent().getIntExtra("extra_credit", 0);
        this.e = getIntent().getIntExtra("extra_combo", 0);
        this.f = getIntent().getIntExtra("extra_entry", 0);
        this.c = Settings.c(getBaseContext());
        setVolumeControlStream(3);
        this.w = (TextView) findViewById(C0000R.id.txt_class);
        this.w.setText(this.Q[this.g]);
        this.u = (ScrollView) findViewById(C0000R.id.rank_scroll);
        this.v = (LinearLayout) findViewById(C0000R.id.scrl_layout);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService("layout_inflater");
        if (!CasinoGamesActivity.a(this)) {
            this.M = C0000R.layout.rank_list_layout;
            this.H = C0000R.drawable.rank_cell_0;
            this.I = C0000R.drawable.rank_cell_1;
            this.J = C0000R.drawable.rank_cell_2;
            this.K = C0000R.drawable.rank_cell_3;
            this.L = C0000R.drawable.rank_cell_10;
        } else {
            this.M = C0000R.layout.q_rank_list_layout;
            this.H = C0000R.drawable.q_rank_cell_0;
            this.I = C0000R.drawable.q_rank_cell_1;
            this.J = C0000R.drawable.q_rank_cell_2;
            this.K = C0000R.drawable.q_rank_cell_3;
            this.L = C0000R.drawable.q_rank_cell_10;
        }
        for (int i2 = 0; i2 < 50; i2++) {
            LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(this.M, (ViewGroup) null);
            linearLayout.setBackgroundDrawable(getResources().getDrawable(this.H));
            this.x[i2] = (TextView) linearLayout.findViewById(C0000R.id.txt_rank_1);
            this.y[i2] = (TextView) linearLayout.findViewById(C0000R.id.txt_score_1);
            this.z[i2] = (TextView) linearLayout.findViewById(C0000R.id.txt_combo_1);
            this.A[i2] = (TextView) linearLayout.findViewById(C0000R.id.txt_uname_1);
            this.x[i2].setText(String.valueOf(i2 + 1));
            this.y[i2].setText("---");
            this.z[i2].setText("-");
            this.A[i2].setText("");
            this.v.addView(linearLayout);
        }
        this.B = (TextView) findViewById(C0000R.id.txt_player_rank);
        this.C = (TextView) findViewById(C0000R.id.txt_player_score);
        this.D = (TextView) findViewById(C0000R.id.txt_player_combo);
        this.E = (TextView) findViewById(C0000R.id.txt_player_uname);
        this.B.setText("---");
        this.C.setText("---");
        this.D.setText("-");
        this.F = (Button) findViewById(C0000R.id.btn_prev);
        this.G = (Button) findViewById(C0000R.id.btn_next);
        b();
        this.F.setOnClickListener(this.N);
        this.G.setOnClickListener(this.O);
        if (!Locale.getDefault().equals(Locale.JAPAN) || ((int) (Math.random() * 10.0d)) >= 9) {
            this.p = (AdView) findViewById(C0000R.id.adView);
            this.p.a(new ad(this));
            this.p.a(new c());
        } else {
            a();
        }
        this.o = new ProgressDialog(this);
        this.o.setProgressStyle(0);
        this.o.setMessage("Connecting...");
        this.o.setCancelable(false);
        this.o.show();
        this.o.setOnCancelListener(new ac(this));
        this.n = new Thread(this);
        this.n.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.q != null) {
            this.q.e();
            this.r = false;
        }
        if (this.p != null) {
            this.p.b();
        }
    }

    public void onPause() {
        super.onPause();
        this.n = null;
        if (this.q != null) {
            this.q.f();
            this.r = false;
        }
        if (this.p != null) {
            this.p.a();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.q != null) {
            if (!this.r) {
                this.q.d();
            }
            this.r = true;
        }
        if (this.p != null && !this.p.c()) {
            this.p.a(new c());
        }
    }

    public void run() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://www.kixx.co.jp/game/casino/db_view.php?" + ((int) (Math.random() * 10000.0d)));
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new BasicNameValuePair("uname", this.c));
        arrayList.add(new BasicNameValuePair("cred", String.valueOf(this.d)));
        arrayList.add(new BasicNameValuePair("combo", String.valueOf(this.e)));
        arrayList.add(new BasicNameValuePair("entry", String.valueOf(this.f)));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            execute.getEntity().writeTo(byteArrayOutputStream);
            if (execute.getStatusLine().getStatusCode() == 200) {
                this.h = byteArrayOutputStream.toString().split(",");
                Integer.parseInt(this.h[0]);
                if (a(this.h)) {
                    this.t.sendEmptyMessage(0);
                } else {
                    this.t.sendEmptyMessage(-1);
                }
            } else {
                this.t.sendEmptyMessage(-1);
            }
        } catch (UnsupportedEncodingException e2) {
            this.t.sendEmptyMessage(-1);
        } catch (NumberFormatException e3) {
            this.t.sendEmptyMessage(-1);
        } catch (IOException e4) {
            this.t.sendEmptyMessage(-1);
        } catch (OutOfMemoryError e5) {
            this.t.sendEmptyMessage(-1);
        }
    }
}
