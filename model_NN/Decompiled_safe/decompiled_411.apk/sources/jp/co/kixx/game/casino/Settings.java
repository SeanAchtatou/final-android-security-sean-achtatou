package jp.co.kixx.game.casino;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.widget.EditText;

public class Settings extends PreferenceActivity {
    private static Boolean a = true;
    private static String[] h = {"", "K", "C", "S", "T", "N", "H", "F", "M", "Y", "R", "L", "W", "G", "Z", "J", "D", "B", "V", "P"};
    private static int[][] i;
    private static String[][] j = {new String[]{"a", "i", "u", "e", "o"}, new String[]{"ya", "yi", "yu", "ye", "yo"}, new String[]{"ha", "hi", "hu", "he", "ho"}};
    /* access modifiers changed from: private */
    public EditTextPreference b;
    private ListPreference c;
    private InputFilter[] d = new InputFilter[2];
    /* access modifiers changed from: private */
    public String[] e;
    /* access modifiers changed from: private */
    public String[] f;
    private Preference.OnPreferenceChangeListener g = new b(this);
    private Preference.OnPreferenceChangeListener k = new c(this);

    static {
        int[] iArr = new int[2];
        iArr[1] = 1;
        int[] iArr2 = new int[2];
        iArr2[1] = 1;
        int[] iArr3 = new int[3];
        iArr3[1] = 1;
        iArr3[2] = 2;
        int[] iArr4 = new int[2];
        iArr4[1] = 2;
        int[] iArr5 = new int[2];
        iArr5[1] = 1;
        int[] iArr6 = new int[2];
        iArr6[1] = 2;
        int[] iArr7 = new int[2];
        iArr7[1] = 1;
        int[] iArr8 = new int[2];
        iArr8[1] = 1;
        int[] iArr9 = new int[2];
        iArr9[1] = 1;
        int[] iArr10 = new int[2];
        iArr10[1] = 2;
        int[] iArr11 = new int[3];
        iArr11[1] = 1;
        iArr11[2] = 2;
        int[] iArr12 = new int[3];
        iArr12[1] = 1;
        iArr12[2] = 2;
        int[] iArr13 = new int[2];
        iArr13[1] = 1;
        int[] iArr14 = new int[2];
        iArr14[1] = 1;
        i = new int[][]{new int[1], iArr, iArr2, iArr3, iArr4, iArr5, iArr6, new int[1], iArr7, new int[1], iArr8, iArr9, iArr10, iArr11, iArr12, new int[1], new int[1], iArr13, new int[1], iArr14};
    }

    public static int a(Context context, String str, int i2) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(str, i2);
    }

    public static long a(Context context, String str, long j2) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(str, j2);
    }

    private static String a() {
        String str = "";
        for (int i2 = 0; i2 < 4; i2++) {
            int random = (int) (Math.random() * ((double) h.length));
            String str2 = String.valueOf(str) + h[random];
            int i3 = i[random][(int) (Math.random() * ((double) i[random].length))];
            str = String.valueOf(str2) + j[i3][(int) (Math.random() * ((double) j[i3].length))];
        }
        return str;
    }

    public static void a(Context context) {
        PreferenceManager.setDefaultValues(context, C0000R.xml.settings, true);
        a = Boolean.valueOf(b(context, "pref_init"));
    }

    public static void a(Context context, String str) {
        try {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(str, true).commit();
        } catch (OutOfMemoryError e2) {
        }
    }

    public static void b(Context context, String str, int i2) {
        try {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(str, i2).commit();
        } catch (OutOfMemoryError e2) {
        }
    }

    public static void b(Context context, String str, long j2) {
        try {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(str, j2).commit();
        } catch (OutOfMemoryError e2) {
        }
    }

    public static boolean b(Context context) {
        return b(context, "name_edit");
    }

    private static boolean b(Context context, String str) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(str, false);
    }

    public static String c(Context context) {
        if (!a.booleanValue()) {
            try {
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString("username", a()).commit();
            } catch (OutOfMemoryError e2) {
            }
            a = true;
            a(context, "pref_init");
        }
        return PreferenceManager.getDefaultSharedPreferences(context).getString("username", "NO NAME");
    }

    public static String d(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("difficulty", "1");
    }

    public static boolean e(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("sounds", true);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Boolean bool;
        super.onCreate(bundle);
        if (getResources().getString(C0000R.string.screen_size).equals("xlarge")) {
            setRequestedOrientation(0);
        }
        addPreferencesFromResource(C0000R.xml.settings);
        this.b = (EditTextPreference) findPreference("username");
        this.c = (ListPreference) findPreference("difficulty");
        this.b.setOnPreferenceChangeListener(this.g);
        this.c.setOnPreferenceChangeListener(this.k);
        EditText editText = this.b.getEditText();
        this.d[0] = new a(this);
        this.d[1] = new InputFilter.LengthFilter(18);
        editText.setFilters(this.d);
        Resources resources = getResources();
        this.e = resources.getStringArray(C0000R.array.difficulty_entries);
        this.f = resources.getStringArray(C0000R.array.difficulty_summary);
        this.c.setSummary("( " + this.e[Integer.parseInt(this.c.getValue())] + " )\n" + this.f[Integer.parseInt(this.c.getValue())]);
        Context baseContext = getBaseContext();
        if (this.b.getText().length() == 0) {
            this.b.setText(a());
            a = true;
            a(baseContext, "pref_init");
            bool = true;
        } else {
            bool = false;
        }
        bool.booleanValue();
        if (!b(getBaseContext(), "name_edit")) {
            this.b.setSummary("( " + this.b.getText() + " ) " + getResources().getString(C0000R.string.username_summary_default));
        } else {
            this.b.setSummary("( " + this.b.getText() + " )");
        }
    }
}
