package jp.co.kixx.game.casino;

import android.content.DialogInterface;

final class ac implements DialogInterface.OnCancelListener {
    private /* synthetic */ RankView a;

    ac(RankView rankView) {
        this.a = rankView;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
        if (this.a.n != null) {
            try {
                this.a.n.join();
            } catch (InterruptedException e) {
            }
            this.a.n = null;
        }
        RankView.f(this.a);
    }
}
