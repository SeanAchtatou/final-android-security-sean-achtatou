package jp.co.kixx.game.casino;

import android.text.InputFilter;
import android.text.Spanned;

final class a implements InputFilter {
    private /* synthetic */ Settings a;

    a(Settings settings) {
        this.a = settings;
    }

    public final CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        return charSequence.toString().matches("^[a-zA-Z_0-9.!?:\\x20]+$") ? charSequence : "";
    }
}
