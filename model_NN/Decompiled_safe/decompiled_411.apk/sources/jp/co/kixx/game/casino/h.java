package jp.co.kixx.game.casino;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

final class h implements View.OnClickListener {
    private /* synthetic */ CasinoGamesActivity a;

    h(CasinoGamesActivity casinoGamesActivity) {
        this.a = casinoGamesActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
     arg types: [jp.co.kixx.game.casino.CasinoGamesActivity, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
     arg types: [jp.co.kixx.game.casino.CasinoGamesActivity, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void */
    public final void onClick(View view) {
        f.a(this.a, 1);
        if (Settings.a((Context) this.a, "ttrial_time", 0L) > 0) {
            CasinoGamesActivity casinoGamesActivity = this.a;
            new AlertDialog.Builder(casinoGamesActivity).setIcon(17301543).setTitle((int) C0000R.string.label_ranking_continue).setPositiveButton((int) C0000R.string.alert_continue_text, new m(casinoGamesActivity)).setNegativeButton((int) C0000R.string.alert_newgame_text, new n(casinoGamesActivity)).setOnCancelListener(new j(casinoGamesActivity)).setCancelable(true).show();
            return;
        }
        Settings.b((Context) this.a, "ttrial_time", 0L);
        CasinoGamesActivity.a(this.a);
    }
}
