package jp.co.kixx.game.casino;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class OddsView extends SurfaceView implements SurfaceHolder.Callback {
    private float A;
    private int B;
    private Rect C;
    private final int a = 1;
    private final float b = 0.3f;
    private Resources c;
    private SurfaceHolder d;
    private Canvas e;
    private String[] f;
    private int[][] g;
    private int h = 1;
    private int i = 0;
    private int j = 0;
    private Boolean k = false;
    private Boolean l = false;
    private Boolean m = false;
    private Boolean n = false;
    private float o;
    private float p;
    private int q;
    private float r;
    private Paint s;
    private Paint t;
    private Paint u;
    private Paint v;
    private Paint w;
    private Paint x;
    private float y = 0.0f;
    private float z = 0.0f;

    public OddsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (!isInEditMode()) {
            this.m = false;
            this.n = false;
            this.c = getResources();
            DisplayMetrics displayMetrics = this.c.getDisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            this.A = displayMetrics.scaledDensity;
            this.d = getHolder();
            this.d.addCallback(this);
        }
    }

    private void a(Canvas canvas) {
        for (int i2 = 0; i2 < this.f.length - 1; i2++) {
            a(canvas, i2);
            b(canvas, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void a(Canvas canvas, int i2) {
        this.B = ((this.f.length - 1) - i2) - 1;
        this.C = new Rect(0, (int) (this.r * ((float) this.B)), (int) (((float) this.q) - (this.A * 1.0f)), (int) (this.r * ((float) (this.B + 1))));
        if (i2 != this.i - 1 || this.l.booleanValue()) {
            this.s.setShader(new LinearGradient(0.0f, (float) this.C.top, 0.0f, (float) this.C.bottom, this.c.getColor(C0000R.color.odds_background_u), this.c.getColor(C0000R.color.odds_background_d), Shader.TileMode.CLAMP));
            canvas.drawRect((float) this.C.left, (float) this.C.top, (float) this.C.right, (float) this.C.bottom, this.s);
        } else {
            this.u.setShader(new LinearGradient(0.0f, (float) this.C.top, 0.0f, (float) this.C.bottom, this.c.getColor(C0000R.color.odds_pay_line_bg_u), this.c.getColor(C0000R.color.odds_pay_line_bg_d), Shader.TileMode.CLAMP));
            canvas.drawRect((float) this.C.left, (float) this.C.top, (float) this.C.right, (float) this.C.bottom, this.u);
        }
        this.v.setTextAlign(Paint.Align.LEFT);
        this.v.setColor(this.c.getColor(C0000R.color.odds_hand_text));
        canvas.drawText(" " + this.f[i2 + 1], 7.0f * this.A, ((((float) this.B) * this.r) + this.y) - (this.A * 1.0f), this.v);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private void b(Canvas canvas, int i2) {
        this.B = ((this.f.length - 1) - i2) - 1;
        this.C = new Rect(this.q, (int) (this.r * ((float) this.B)), (int) this.o, (int) (this.r * ((float) (this.B + 1))));
        if (i2 != this.i - 1 || this.l.booleanValue()) {
            this.t.setShader(new LinearGradient(0.0f, (float) this.C.top, 0.0f, (float) this.C.bottom, this.c.getColor(C0000R.color.odds_background_u), this.c.getColor(C0000R.color.odds_background_d), Shader.TileMode.CLAMP));
            canvas.drawRect((float) this.C.left, (float) this.C.top, (float) this.C.right, (float) this.C.bottom, this.t);
        } else {
            this.u.setShader(new LinearGradient(0.0f, (float) this.C.top, 0.0f, (float) this.C.bottom, this.c.getColor(C0000R.color.odds_pay_line_bg_u), this.c.getColor(C0000R.color.odds_pay_line_bg_d), Shader.TileMode.CLAMP));
            canvas.drawRect((float) this.C.left, (float) this.C.top, (float) this.C.right, (float) this.C.bottom, this.u);
        }
        this.v.setTextAlign(Paint.Align.CENTER);
        this.v.setColor(this.c.getColor(C0000R.color.odds_pay_text));
        canvas.drawText(new StringBuilder(String.valueOf(this.g[this.h - 1][i2 + 1])).toString(), (float) (this.C.left + ((this.C.right - this.C.left) / 2)), ((((float) this.B) * this.r) + this.z) - (1.0f * this.A), this.v);
    }

    /* JADX INFO: finally extract failed */
    public final void a() {
        if (this.n.booleanValue() && this.i > 0 && this.k.booleanValue()) {
            if ((this.j & 7) == 0) {
                this.l = Boolean.valueOf(!this.l.booleanValue());
                this.e = null;
                try {
                    this.e = this.d.lockCanvas();
                    if (this.e != null) {
                        a(this.e);
                    }
                    if (this.e != null) {
                        this.d.unlockCanvasAndPost(this.e);
                    }
                } catch (Throwable th) {
                    if (this.e != null) {
                        this.d.unlockCanvasAndPost(this.e);
                    }
                    throw th;
                }
            }
            this.j++;
        }
    }

    /* JADX INFO: finally extract failed */
    public final void a(int i2) {
        this.k = false;
        this.l = false;
        this.i = 0;
        this.h = i2;
        this.e = null;
        try {
            this.e = this.d.lockCanvas();
            if (this.e != null) {
                for (int i3 = 0; i3 < this.f.length - 1; i3++) {
                    a(this.e, i3);
                    b(this.e, i3);
                }
            }
            if (this.e != null) {
                this.d.unlockCanvasAndPost(this.e);
            }
        } catch (Throwable th) {
            if (this.e != null) {
                this.d.unlockCanvasAndPost(this.e);
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public final void a(int i2, Boolean bool) {
        this.j = 1;
        this.k = bool;
        this.l = false;
        int i3 = i2 - 1;
        if (i3 < 0) {
            i3 = 0;
        }
        if (this.i != i3) {
            this.i = i3;
            this.e = null;
            try {
                this.e = this.d.lockCanvas();
                if (this.e != null) {
                    a(this.e);
                }
                if (this.e != null) {
                    this.d.unlockCanvasAndPost(this.e);
                }
            } catch (Throwable th) {
                if (this.e != null) {
                    this.d.unlockCanvasAndPost(this.e);
                }
                throw th;
            }
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.o = (float) i3;
        this.p = (float) i4;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, float, float, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.n = true;
        if (!this.m.booleanValue()) {
            this.o = (float) getWidth();
            this.p = (float) getHeight();
            this.f = this.c.getStringArray(C0000R.array.hand);
            this.g = new int[][]{this.c.getIntArray(C0000R.array.pay_out1), this.c.getIntArray(C0000R.array.pay_out2), this.c.getIntArray(C0000R.array.pay_out3), this.c.getIntArray(C0000R.array.pay_out4), this.c.getIntArray(C0000R.array.pay_out5)};
            this.q = (int) (this.o * 0.7f);
            this.r = this.p / ((float) (this.f.length - 1));
            this.s = new Paint();
            this.s.setStyle(Paint.Style.FILL);
            this.s.setAntiAlias(false);
            this.t = new Paint();
            this.t.setAntiAlias(false);
            this.u = new Paint();
            this.u.setAntiAlias(false);
            this.w = new Paint();
            this.x = new Paint();
            this.w.setStyle(Paint.Style.STROKE);
            this.x.setStyle(Paint.Style.STROKE);
            this.w.setColor(this.c.getColor(C0000R.color.odds_border));
            this.x.setColor(this.c.getColor(C0000R.color.odds_light));
            this.v = new Paint(1);
            this.v.setColor(this.c.getColor(C0000R.color.odds_hand_text));
            this.v.setStyle(Paint.Style.FILL);
            this.v.setTextSize(this.r * 0.9f);
            this.v.setTextScaleX(1.1f);
            this.v.setTypeface(Typeface.DEFAULT_BOLD);
            this.v.setShadowLayer(1.0f, this.A * 2.0f, this.A * 1.0f, this.c.getColor(C0000R.color.odds_pay_text_shadow));
            Paint.FontMetrics fontMetrics = this.v.getFontMetrics();
            this.y = ((this.r / 2.0f) - ((fontMetrics.ascent + fontMetrics.descent) / 2.0f)) - 0.0f;
            this.z = (fontMetrics.descent / 4.0f) + this.y;
            this.m = true;
        }
        this.e = null;
        try {
            this.e = this.d.lockCanvas();
            if (this.e != null) {
                a(this.e);
            }
            if (this.e != null) {
                this.d.unlockCanvasAndPost(this.e);
            }
            this.e = null;
            try {
                this.e = this.d.lockCanvas();
                if (this.e != null) {
                    a(this.e);
                }
                if (this.e != null) {
                    this.d.unlockCanvasAndPost(this.e);
                }
            } catch (Throwable th) {
                if (this.e != null) {
                    this.d.unlockCanvasAndPost(this.e);
                }
                throw th;
            }
        } catch (Throwable th2) {
            if (this.e != null) {
                this.d.unlockCanvasAndPost(this.e);
            }
            throw th2;
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.n = false;
    }
}
