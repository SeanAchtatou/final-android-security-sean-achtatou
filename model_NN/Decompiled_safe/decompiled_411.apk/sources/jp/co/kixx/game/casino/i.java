package jp.co.kixx.game.casino;

import android.content.Context;
import android.content.Intent;
import android.view.View;

final class i implements View.OnClickListener {
    private /* synthetic */ CasinoGamesActivity a;

    i(CasinoGamesActivity casinoGamesActivity) {
        this.a = casinoGamesActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
     arg types: [jp.co.kixx.game.casino.CasinoGamesActivity, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int */
    public final void onClick(View view) {
        f.a(this.a, 1);
        Intent intent = new Intent(this.a, RankView.class);
        int a2 = Settings.a((Context) this.a, "ranking_credit", 100);
        int a3 = Settings.a((Context) this.a, "ranking_combo", 0);
        intent.putExtra("extra_credit", a2);
        intent.putExtra("extra_combo", a3);
        this.a.startActivity(intent);
    }
}
