package jp.co.kixx.game.casino;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

final class z extends Handler {
    private /* synthetic */ RankView a;

    z(RankView rankView) {
        this.a = rankView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
     arg types: [jp.co.kixx.game.casino.RankView, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void */
    public final void handleMessage(Message message) {
        RankView.a(this.a);
        if (this.a.n != null) {
            if (message.what == 0) {
                if (this.a.f != 0) {
                    RankView rankView = this.a;
                    Settings.b((Context) rankView, "save_ranking_credit", 0);
                    Settings.b((Context) rankView, "save_ranking_combo", 0);
                }
                RankView.a(this.a, 0);
            } else if (message.what == -1) {
                RankView rankView2 = this.a;
                f.a(rankView2, 18);
                new AlertDialog.Builder(rankView2).setIcon(17301543).setTitle((int) C0000R.string.label_connect_error).setPositiveButton((int) C0000R.string.alert_ok_text, new ae(rankView2)).setCancelable(false).show();
            }
        }
    }
}
