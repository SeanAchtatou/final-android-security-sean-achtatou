package jp.co.kixx.game.casino;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import com.google.ads.AdView;
import com.google.ads.c;
import java.util.Locale;
import jp.co.nobot.libAdMaker.libAdMaker;

public class CasinoGamesActivity extends Activity {
    private Button a;
    private Button b;
    private Button c;
    private Button d;
    private f e;
    private AdView f;
    private libAdMaker g;
    private boolean h = false;
    /* access modifiers changed from: private */
    public int i = 1;
    private Animation.AnimationListener j = new d(this);
    private View.OnClickListener k = new g(this);
    private View.OnClickListener l = new h(this);
    private View.OnClickListener m = new i(this);
    private View.OnClickListener n = new k(this);

    /* access modifiers changed from: private */
    public void a() {
        try {
            this.g = (libAdMaker) findViewById(C0000R.id.admakerview);
            this.g.setVisibility(0);
            this.g.a = "562";
            this.g.b = "1153";
            this.g.a("http://images.ad-maker.info/apps/5dcvv1v4pqra.html");
            this.g.d();
            this.h = true;
        } catch (NullPointerException e2) {
        }
    }

    static /* synthetic */ void a(CasinoGamesActivity casinoGamesActivity) {
        Intent intent = new Intent(casinoGamesActivity, Poker.class);
        intent.setFlags(536870912);
        intent.putExtra("extra_mode", 1);
        casinoGamesActivity.startActivity(intent);
    }

    public static boolean a(Context context) {
        if (context.getResources().getString(C0000R.string.screen_size).equals("normal")) {
            Display defaultDisplay = ((Activity) context).getWindowManager().getDefaultDisplay();
            return defaultDisplay.getWidth() == 540 && defaultDisplay.getHeight() == 960;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0) {
            keyEvent.getAction();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getResources().getString(C0000R.string.screen_size).equals("xlarge")) {
            setRequestedOrientation(0);
        }
        if (!a((Context) this)) {
            setContentView((int) C0000R.layout.main);
        } else {
            setContentView((int) C0000R.layout.q_main);
        }
        Settings.a(this);
        setVolumeControlStream(3);
        Poker.a(this);
        this.a = (Button) findViewById(C0000R.id.free_play_btn);
        this.b = (Button) findViewById(C0000R.id.time_trial_btn);
        this.c = (Button) findViewById(C0000R.id.ranking_button);
        this.d = (Button) findViewById(C0000R.id.exit_button);
        if (Settings.b(this)) {
            ((TextView) findViewById(C0000R.id.name_setting)).setText(" ");
        }
        this.a.setOnClickListener(this.k);
        this.b.setOnClickListener(this.l);
        this.c.setOnClickListener(this.m);
        this.d.setOnClickListener(this.n);
        if (!Locale.getDefault().equals(Locale.JAPAN) || ((int) (Math.random() * 10.0d)) >= 9) {
            this.f = (AdView) findViewById(C0000R.id.adView);
            this.f.a(new l(this));
            this.f.a(new c());
            return;
        }
        a();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0000R.menu.menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        f.a();
        this.e = null;
        if (this.g != null) {
            this.g.e();
            this.h = false;
        }
        if (this.f != null) {
            this.f.b();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_settings /*2131492928*/:
                startActivity(new Intent(this, Settings.class));
                return true;
            case C0000R.id.menu_exit /*2131492929*/:
                finish();
                return true;
            default:
                return false;
        }
    }

    public void onPause() {
        if (this.g != null) {
            this.g.f();
            this.h = false;
        }
        if (this.f != null) {
            this.f.a();
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.e == null) {
            this.e = new f(this);
            findViewById(C0000R.id.splash_layout).setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(this, C0000R.anim.splash_fade_out);
            loadAnimation.setAnimationListener(this.j);
            findViewById(C0000R.id.splash_layout).startAnimation(loadAnimation);
        } else {
            findViewById(C0000R.id.splash_layout).setVisibility(8);
        }
        if (this.g != null) {
            if (!this.h) {
                this.g.d();
            }
            this.h = true;
        }
        if (this.f != null && !this.f.c()) {
            this.f.a(new c());
        }
        f.a(this);
    }
}
