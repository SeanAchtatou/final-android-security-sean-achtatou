package jp.co.kixx.game.casino;

import android.preference.Preference;

final class b implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ Settings a;

    b(Settings settings) {
        this.a = settings;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        if (obj != null) {
            String trim = obj.toString().replace(",", " ").replace("'", " ").replace("\"", " ").replace("=", " ").replace("\\", " ").trim();
            if (trim.length() > 0) {
                this.a.b.setText(trim);
                preference.setSummary("( " + trim + " )");
                Settings.a(preference.getContext(), "name_edit");
                return false;
            }
        }
        return false;
    }
}
