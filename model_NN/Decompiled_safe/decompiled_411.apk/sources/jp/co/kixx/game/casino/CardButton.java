package jp.co.kixx.game.casino;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class CardButton extends ImageView implements Animation.AnimationListener {
    /* access modifiers changed from: private */
    public static final Bitmap[] c = new Bitmap[54];
    /* access modifiers changed from: private */
    public static Boolean d = false;
    private static int e = 0;
    private static int f = 0;
    private static int g = 0;
    private static int h = 0;
    private static int i = 0;
    private static int j = 0;
    private static int[] s = {1, 2, 4, 8};
    public Boolean a = false;
    public int b = 0;
    /* access modifiers changed from: private */
    public Boolean k = false;
    private Boolean l = false;
    /* access modifiers changed from: private */
    public Boolean m = false;
    /* access modifiers changed from: private */
    public int n = 0;
    private int o = 0;
    private Animation p;
    private Bitmap q;
    private Canvas r;
    private View.OnClickListener t = new e(this);

    public CardButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CardButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public static void a(Context context) {
        float dimension;
        Resources resources = context.getResources();
        if (!CasinoGamesActivity.a(context)) {
            h = (int) resources.getDimension(C0000R.dimen.card_width);
            g = (int) resources.getDimension(C0000R.dimen.card_height);
            j = (int) resources.getDimension(C0000R.dimen.card_number_width);
            i = (int) resources.getDimension(C0000R.dimen.card_number_height);
            dimension = resources.getDimension(C0000R.dimen.card_pane_height);
        } else {
            h = (int) resources.getDimension(C0000R.dimen.Qcard_width);
            g = (int) resources.getDimension(C0000R.dimen.Qcard_height);
            j = (int) resources.getDimension(C0000R.dimen.Qcard_number_width);
            i = (int) resources.getDimension(C0000R.dimen.Qcard_number_height);
            dimension = resources.getDimension(C0000R.dimen.Qcard_pane_height);
        }
        e = (int) dimension;
        f = (int) ((dimension - ((float) g)) / 2.0f);
    }

    /* access modifiers changed from: private */
    public void a(ImageView imageView) {
        if (this.a.booleanValue()) {
            this.r.drawColor(0, PorterDuff.Mode.CLEAR);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) getDrawable();
            bitmapDrawable.setBounds(0, 0, h, e);
            bitmapDrawable.draw(this.r);
            Paint paint = new Paint(1);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setTextSize(((float) ((e - g) / 2)) * 0.9f);
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            paint.setColor(-2039744);
            this.r.drawText(new String[]{"= HELD =", "DEALER", "      "}[this.o], (float) (h / 2), ((-fontMetrics.ascent) + 0.0f) - 1.0f, paint);
            paint.setColor(-1);
            this.r.drawText(new String[]{"  HELD  ", "      ", "PLAYER"}[this.o], (float) (h / 2), ((-fontMetrics.ascent) + 0.0f) - 1.0f, paint);
            imageView.setImageBitmap(this.q);
        }
    }

    public static void b(Context context) {
        Resources resources = context.getResources();
        a(context);
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(resources.getDisplayMetrics());
        Bitmap createBitmap = Bitmap.createBitmap(h, e, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Drawable drawable = resources.getDrawable(C0000R.drawable.card_0);
        drawable.setBounds(0, f, h, f + g);
        drawable.draw(canvas);
        c[0] = createBitmap;
        Drawable[] drawableArr = {resources.getDrawable(C0000R.drawable.card_h), resources.getDrawable(C0000R.drawable.card_c), resources.getDrawable(C0000R.drawable.card_d), resources.getDrawable(C0000R.drawable.card_s)};
        Drawable[] drawableArr2 = {resources.getDrawable(C0000R.drawable.num_a), resources.getDrawable(C0000R.drawable.num_2), resources.getDrawable(C0000R.drawable.num_3), resources.getDrawable(C0000R.drawable.num_4), resources.getDrawable(C0000R.drawable.num_5), resources.getDrawable(C0000R.drawable.num_6), resources.getDrawable(C0000R.drawable.num_7), resources.getDrawable(C0000R.drawable.num_8), resources.getDrawable(C0000R.drawable.num_9), resources.getDrawable(C0000R.drawable.num_10), resources.getDrawable(C0000R.drawable.num_j), resources.getDrawable(C0000R.drawable.num_q), resources.getDrawable(C0000R.drawable.num_k)};
        for (Drawable bounds : drawableArr2) {
            bounds.setBounds(0, f, j, f + i);
        }
        for (int i2 = 1; i2 <= 52; i2++) {
            int i3 = ((i2 - 1) / 13) + 1;
            Bitmap createBitmap2 = Bitmap.createBitmap(h, e, Bitmap.Config.ARGB_8888);
            Canvas canvas2 = new Canvas(createBitmap2);
            drawableArr[i3 - 1].setBounds(0, f, h, f + g);
            drawableArr[i3 - 1].draw(canvas2);
            drawableArr2[(((i2 - 1) % 13) + 1) - 1].draw(canvas2);
            c[i2] = createBitmap2;
        }
        Bitmap createBitmap3 = Bitmap.createBitmap(h, e, Bitmap.Config.ARGB_8888);
        Canvas canvas3 = new Canvas(createBitmap3);
        Drawable drawable2 = resources.getDrawable(C0000R.drawable.card_jo);
        drawable2.setBounds(0, f, h, f + g);
        drawable2.draw(canvas3);
        c[53] = createBitmap3;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.q = Bitmap.createBitmap(h, e, Bitmap.Config.ARGB_8888);
        this.r = new Canvas(this.q);
        setImageBitmap(c[0]);
        this.p = AnimationUtils.loadAnimation(getContext(), C0000R.anim.card_in);
        this.p.setAnimationListener(this);
        setOnClickListener(this.t);
    }

    public final void a(int i2, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4) {
        int i3;
        this.b = i2;
        int i4 = i2 & 15;
        int i5 = (i2 >> 4) & 15;
        int i6 = 0;
        while (true) {
            if (i6 >= s.length) {
                i3 = -1;
                break;
            } else if (s[i6] == i5) {
                i3 = i6;
                break;
            } else {
                i6++;
            }
        }
        if (i4 < 13 && i3 >= 0) {
            this.n = i4 + (i3 * 13) + 1;
        } else if (i2 == 0) {
            this.n = 0;
        } else {
            this.n = 53;
        }
        setImageBitmap(c[this.n]);
        this.k = bool;
        this.m = bool4;
        if (bool3.booleanValue()) {
            this.a = true;
            a((ImageView) this);
        } else {
            this.a = false;
        }
        if (bool2.booleanValue()) {
            this.l = true;
            startAnimation(this.p);
        }
    }

    public final void a(Boolean bool, int i2, Boolean bool2) {
        this.k = bool;
        this.o = i2;
        this.m = bool2;
        d = false;
    }

    public final Boolean b() {
        return this.l;
    }

    public void onAnimationEnd(Animation animation) {
        this.l = false;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
