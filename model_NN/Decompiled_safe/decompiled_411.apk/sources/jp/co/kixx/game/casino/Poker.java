package jp.co.kixx.game.casino;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.Timer;

public class Poker extends Activity {
    private static boolean D = false;
    private static int[] bd = {8193, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 16383};
    private int[][] A;
    private String[] B;
    private int[] C;
    private int E = 0;
    /* access modifiers changed from: private */
    public Handler F;
    private Thread G;
    private Timer H;
    /* access modifiers changed from: private */
    public boolean I = false;
    /* access modifiers changed from: private */
    public int J = 0;
    /* access modifiers changed from: private */
    public boolean K = false;
    private OddsView L;
    private Animation M;
    private Animation N;
    private Animation O;
    private long P = 0;
    private long Q = 3600000;
    /* access modifiers changed from: private */
    public int R = 0;
    /* access modifiers changed from: private */
    public int S = 0;
    private int T = 0;
    private int U = 0;
    private Runnable V = new s(this);
    private int W = 0;
    private int X = 0;
    private int Y = 0;
    private int Z = 0;
    private final int a = 15360;
    private long aA;
    private int aB;
    private int aC;
    private String aD;
    private String aE;
    private String aF;
    private LinearLayout aG;
    private LinearLayout aH;
    private LinearLayout aI;
    private LinearLayout aJ;
    private LinearLayout aK;
    private LinearLayout aL;
    private LinearLayout aM;
    private TextView aN;
    private TextView aO;
    private TextView aP;
    private TextView aQ;
    private LinearLayout aR;
    private LinearLayout aS;
    private TextView aT;
    private TextView aU;
    private TextView aV;
    private TextView aW;
    private TextView aX;
    private TextView aY;
    private TextView aZ;
    private final int aa = 2;
    private final int ab = 7;
    private final int ac = 1;
    private final int ad = 12;
    private final int ae = 1;
    private final int af = 15;
    private final int ag = 14;
    private final int ah = 11;
    private int ai = 0;
    private int aj = 0;
    private int ak = 0;
    private boolean al = false;
    private boolean am = false;
    private final int[][] an;
    private int ao;
    private final int ap;
    private final int aq;
    /* access modifiers changed from: private */
    public int[] ar;
    private PlayButton as;
    private PlayButton at;
    private PlayButton au;
    private PlayButton av;
    private PlayButton aw;
    private PlayButton ax;
    private View.OnClickListener ay;
    private TextView az;
    private final int b = 1;
    private TextView ba;
    private TextView bb;
    private final int[] bc;
    private final int[] be;
    private final int[][] bf;
    private final int c = 2;
    private final int d = 3;
    private final int e = 4;
    private final int f = 5;
    private final int g = 6;
    private final int h = 7;
    private final int i = 8;
    private final int j = 9;
    private final int k = 10;
    private final int l = 11;
    private final int m = 33;
    private CardButton[] n = new CardButton[5];
    private int o = 0;
    private final int[][] p = ((int[][]) Array.newInstance(Integer.TYPE, 2, 53));
    private int q = 52;
    /* access modifiers changed from: private */
    public int r = 100;
    private int s = 0;
    private int t = 0;
    private int u = 0;
    private int v = 0;
    private int w = 0;
    private int x = 0;
    private int y = 0;
    private int[] z = new int[13];

    public Poker() {
        int[] iArr = new int[2];
        iArr[1] = 12;
        int[] iArr2 = new int[2];
        iArr2[1] = 12;
        this.an = new int[][]{iArr, new int[]{1, 1}, new int[]{1, 2}, new int[]{1, 3}, new int[]{1, 4}, new int[]{1, 5}, new int[]{1, 6}, new int[]{1, 7}, new int[]{1, 8}, new int[]{1, 9}, new int[]{1, 10}, new int[]{1, 11}, new int[]{1, 12}, iArr2};
        this.ao = 0;
        this.ap = 12;
        this.aq = 0;
        this.ar = new int[6];
        this.ay = new r(this);
        this.bc = new int[]{C0000R.id.card_0, C0000R.id.card_1, C0000R.id.card_2, C0000R.id.card_3, C0000R.id.card_4};
        this.be = new int[]{15903, 63, 127, 255, 511, 1022, 2044, 4088, 8176, 16352, 16320, 16256, 16128, 16383};
        int[] iArr3 = new int[3];
        iArr3[0] = 1;
        iArr3[1] = 2;
        int[] iArr4 = new int[3];
        iArr4[0] = 3;
        iArr4[1] = 3;
        int[] iArr5 = new int[3];
        iArr5[0] = 4;
        iArr5[1] = 4;
        int[] iArr6 = new int[3];
        iArr6[0] = 7;
        iArr6[1] = 7;
        int[] iArr7 = new int[3];
        iArr7[0] = 8;
        iArr7[1] = 8;
        int[] iArr8 = new int[3];
        iArr8[0] = 10;
        iArr8[1] = 10;
        this.bf = new int[][]{new int[3], new int[3], iArr3, iArr4, new int[3], new int[]{1, 2, 1}, iArr5, iArr6, new int[]{4, 4, 1}, new int[]{7, 7, 1}, new int[3], new int[3], iArr7, new int[]{8, 8, 1}, new int[3], new int[3], new int[3], new int[3], new int[3], new int[3], iArr8};
    }

    private void a() {
        this.aY.setText(String.valueOf(this.s));
        this.aZ.setText(String.valueOf(this.t));
        this.ba.setText(String.valueOf(this.u));
        this.bb.setText(String.valueOf(this.r));
    }

    public static void a(Context context) {
        if (!D) {
            CardButton.b(context);
        }
        D = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void */
    static /* synthetic */ void a(Poker poker, int i2, int i3) {
        Settings.b((Context) poker, "save_ranking_credit", i2);
        Settings.b((Context) poker, "save_ranking_combo", i3);
        Intent intent = new Intent(poker, RankView.class);
        intent.putExtra("extra_credit", i2);
        intent.putExtra("extra_combo", i3);
        intent.putExtra("extra_entry", 1);
        poker.startActivity(intent);
    }

    private void a(boolean z2) {
        int i2 = this.C[Integer.parseInt(Settings.d(getBaseContext()))];
        if (i2 < 0) {
            this.q = 52;
            return;
        }
        if (!z2) {
            i2 = 52;
        }
        this.q = (int) (Math.random() * ((double) i2));
        this.p[this.o][52] = this.p[this.o][this.q];
        this.p[this.o][this.q] = 253;
    }

    private void b() {
        e();
        if (this.y > 0) {
            this.aT.setText(this.B[this.y - 1]);
        }
        this.aU.setText(String.valueOf(this.A[this.s - 1][this.y - 1]));
        this.aV.setText(String.valueOf(this.t));
        this.aW.setText(String.valueOf(this.v));
        if (this.t < 999999999) {
            this.aX.setText(String.valueOf(this.t * 2));
        } else {
            this.aX.setText(String.valueOf(1999999999));
        }
    }

    static /* synthetic */ void b(Poker poker) {
        poker.I = true;
        while (poker.I && poker.K) {
            int i2 = poker.o ^ 1;
            int random = (int) (Math.random() * 10.0d);
            int random2 = (int) (Math.random() * 52.0d);
            int i3 = poker.p[i2][random];
            poker.p[i2][random] = poker.p[i2][random2];
            poker.p[i2][random2] = i3;
            try {
                Thread.sleep(20);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void b(boolean z2) {
        for (int i2 = 0; i2 < this.z.length; i2++) {
            this.z[i2] = 0;
        }
        int i3 = 0;
        int i4 = 0;
        char c2 = 0;
        int i5 = 0;
        while (i4 < 4) {
            int i6 = i3;
            int i7 = i5;
            char c3 = c2;
            for (int i8 = i4 + 1; i8 < 5; i8++) {
                int i9 = bd[this.n[i4].b & 15] & bd[this.n[i8].b & 15];
                if (i9 != 0) {
                    int i10 = 0;
                    while (true) {
                        if (i10 >= bd.length - 1) {
                            break;
                        } else if (i9 == bd[i10]) {
                            int[] iArr = this.z;
                            iArr[i10] = iArr[i10] + 1;
                            if (this.z[i10] > i6) {
                                i6 = this.z[i10];
                            }
                            i7++;
                            c3 |= (i9 & 15360) != 0 ? (char) 1 : 0;
                        } else {
                            i10++;
                        }
                    }
                }
            }
            i4++;
            c2 = c3;
            i5 = i7;
            i3 = i6;
        }
        this.y = this.bf[i3 + i5][c2];
        if (this.y <= 2 && (this.y <= 0 || this.bf[i3 + i5][2] != 0)) {
            int i11 = 0;
            int i12 = 16383;
            for (int i13 = 0; i13 < 5; i13++) {
                i12 &= this.be[this.n[i13].b & 15];
            }
            int i14 = 0;
            while (true) {
                if (i14 >= 10) {
                    break;
                } else if ((i12 & 31) == 31) {
                    i11 = i14 + 1;
                    break;
                } else {
                    i12 >>>= 1;
                    i14++;
                }
            }
            int i15 = 240;
            for (int i16 = 0; i16 < 5; i16++) {
                i15 &= this.n[i16].b & 240;
            }
            if (i11 > 0) {
                this.y = 5;
                if (i15 > 0) {
                    this.y = 9;
                    if (i11 == 10) {
                        this.y = 11;
                    }
                }
            } else if (i15 > 0) {
                this.y = 6;
            }
        }
        if (z2 && this.y > 0) {
            this.t = this.A[this.s - 1][this.y - 1];
        }
    }

    private boolean c() {
        boolean z2 = false;
        for (int i2 = 0; i2 < 5; i2++) {
            z2 |= this.n[i2].b().booleanValue();
        }
        return z2;
    }

    private void d() {
        if (this.E == 1) {
            this.aB = (int) (this.Q / 60000);
            this.aC = (int) ((this.Q - ((long) ((this.aB * 60) * 1000))) / 1000);
            this.aD = "0" + String.valueOf(this.aB);
            this.aE = this.aD.substring(this.aD.length() - 2, this.aD.length());
            this.aD = "0" + String.valueOf(this.aC);
            this.aF = this.aD.substring(this.aD.length() - 2, this.aD.length());
            this.az.setText(String.valueOf(this.aE) + ":" + this.aF);
        }
    }

    private void e() {
        Resources resources = getResources();
        if (this.A == null) {
            this.A = new int[][]{resources.getIntArray(C0000R.array.pay_out1), resources.getIntArray(C0000R.array.pay_out2), resources.getIntArray(C0000R.array.pay_out3), resources.getIntArray(C0000R.array.pay_out4), resources.getIntArray(C0000R.array.pay_out5)};
        }
        if (this.B == null) {
            this.B = resources.getStringArray(C0000R.array.hand);
        }
        if (this.C == null) {
            this.C = resources.getIntArray(C0000R.array.joker_range);
        }
    }

    private void f() {
        if (this.q != 52) {
            this.p[this.o][this.q] = this.p[this.o][52];
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.OddsView.a(int, java.lang.Boolean):void
     arg types: [int, int]
     candidates:
      jp.co.kixx.game.casino.OddsView.a(android.graphics.Canvas, int):void
      jp.co.kixx.game.casino.OddsView.a(int, java.lang.Boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int */
    static /* synthetic */ void h(Poker poker) {
        int random;
        switch (poker.x) {
            case 0:
                if (poker.E != 1 || poker.Q != 0 || poker.Z != 0) {
                    if (poker.X == 0) {
                        poker.aJ.removeAllViews();
                        poker.aJ.addView(poker.aK);
                        poker.au.setTextColor(poker.getResources().getColor(C0000R.color.button_text_disable));
                        poker.au.setClickable(false);
                        poker.as.setTextColor(poker.getResources().getColor(C0000R.color.button_text_enable));
                        poker.as.setClickable(true);
                        poker.at.setTextColor(poker.getResources().getColor(C0000R.color.button_text_enable));
                        poker.at.setClickable(true);
                        poker.aI.removeAllViews();
                        poker.aI.addView(poker.aO);
                        poker.aO.startAnimation(poker.M);
                        for (int i2 = 0; i2 < 6; i2++) {
                            poker.ar[1] = 0;
                        }
                        poker.Z = 0;
                        poker.s = 0;
                        poker.t = 0;
                        poker.u = 0;
                        poker.v = 1;
                        System.gc();
                        poker.X++;
                        poker.W = 0;
                        poker.Y = 0;
                    } else if (poker.X == 1) {
                        if (poker.ar[0] > 0) {
                            poker.ar[0] = 0;
                            poker.au.setTextColor(poker.getResources().getColor(C0000R.color.button_text_enable));
                            poker.au.setClickable(true);
                            if (poker.r > 0) {
                                poker.Z++;
                                if (poker.Z > poker.r || poker.Z > 5) {
                                    poker.Z = 1;
                                }
                                poker.Y = 0;
                            }
                        }
                        if (poker.ar[1] > 0) {
                            poker.ar[1] = 0;
                            if (poker.r > 0) {
                                poker.aJ.removeAllViews();
                                poker.Z = poker.r >= 5 ? 5 : poker.r;
                                poker.X = 2;
                                if (poker.Z == poker.s) {
                                    f.a(poker, 1);
                                }
                                poker.Y = 0;
                            }
                        }
                        if (poker.ar[2] > 0) {
                            poker.ar[2] = 0;
                            f.a(poker, 1);
                            poker.X = 2;
                        }
                        if (poker.W == 0 && poker.Z > 0) {
                            for (int i3 = 0; i3 < 5; i3++) {
                                poker.n[i3].a(0, false, false, false, false);
                            }
                            poker.aH.setVisibility(8);
                            poker.L.setVisibility(0);
                            poker.aO.clearAnimation();
                            poker.aI.removeAllViews();
                            poker.W++;
                        }
                    }
                    if (poker.Y != 0) {
                        poker.Y--;
                    } else if (poker.s != poker.Z) {
                        poker.s = poker.s < poker.Z ? poker.s + 1 : poker.Z;
                        poker.L.a(poker.s);
                        int i4 = poker.s;
                        poker.aY.setText(String.valueOf(poker.s));
                        poker.aZ.setText(String.valueOf(poker.t));
                        poker.ba.setText(String.valueOf(poker.u));
                        poker.bb.setText(String.valueOf(poker.r - i4));
                        f.a(poker, 9);
                        poker.Y = 2;
                    } else {
                        f.b(poker, 9);
                        poker.Y = 4;
                    }
                    if (poker.s == poker.Z && poker.X == 2) {
                        poker.X = 3;
                    }
                    if (poker.X == 3) {
                        poker.aJ.removeAllViews();
                        poker.av.setTextColor(poker.getResources().getColor(C0000R.color.button_text_disable));
                        poker.av.setClickable(false);
                        poker.o ^= 1;
                        poker.a(true);
                        poker.r -= poker.s;
                        poker.Z = 0;
                        poker.a();
                        poker.x = 1;
                        poker.X = 0;
                        poker.W = 0;
                        poker.Y = 7;
                        break;
                    }
                } else {
                    poker.x = 10;
                    poker.X = 0;
                    poker.W = 0;
                    poker.Y = 0;
                    poker.Z = 0;
                    break;
                }
                break;
            case 1:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X != 2) {
                            if (poker.X == 3) {
                                if (poker.Y != 0) {
                                    int i5 = poker.Y - 1;
                                    poker.Y = i5;
                                    if (i5 == 0 && poker.W == 5) {
                                        poker.x = 3;
                                        poker.X = 0;
                                        poker.W = 5;
                                        poker.Y = 12;
                                        break;
                                    }
                                } else {
                                    while (true) {
                                        if (poker.W < 5) {
                                            if (!poker.n[poker.W].a.booleanValue()) {
                                                CardButton[] cardButtonArr = poker.n;
                                                int i6 = poker.W;
                                                poker.W = i6 + 1;
                                                cardButtonArr[i6].a(0, false, true, false, false);
                                            } else {
                                                poker.W++;
                                            }
                                        }
                                    }
                                    poker.Y = 1;
                                    break;
                                }
                            }
                        } else if (poker.ar[3] > 0) {
                            poker.ar[3] = 0;
                            poker.aJ.removeAllViews();
                            for (int i7 = 0; i7 < 5; i7++) {
                                poker.n[i7].a(false, 0, false);
                            }
                            f.a(poker, 1);
                            poker.X++;
                            poker.W = 0;
                            poker.Y = 0;
                            break;
                        }
                    } else if (!poker.c()) {
                        poker.aJ.addView(poker.aL);
                        poker.av.setTextColor(poker.getResources().getColor(C0000R.color.button_text_enable));
                        poker.av.setClickable(true);
                        for (int i8 = 0; i8 < 5; i8++) {
                            poker.n[i8].a(true, 0, false);
                        }
                        poker.b(false);
                        poker.L.a(poker.y, (Boolean) false);
                        poker.X++;
                        poker.W = 0;
                        break;
                    }
                } else if (poker.Y != 0) {
                    int i9 = poker.Y - 1;
                    poker.Y = i9;
                    if (i9 == 0 && poker.W == 5) {
                        poker.X++;
                        poker.W = 0;
                        break;
                    }
                } else {
                    poker.n[poker.W].a(poker.p[poker.o][poker.W], false, false, false, false);
                    f.a(poker, poker.W + 2);
                    poker.W++;
                    poker.Y = 7;
                    break;
                }
                break;
            case 3:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X == 2) {
                            if (poker.y < 2) {
                                poker.x = 10;
                            } else {
                                poker.x = 5;
                                f.a(poker, 12);
                            }
                            poker.X = 0;
                            break;
                        }
                    } else if (!poker.c()) {
                        poker.b(true);
                        poker.L.a(poker.y, (Boolean) true);
                        poker.a();
                        poker.f();
                        poker.X++;
                        break;
                    }
                } else if (!poker.c()) {
                    if (poker.Y != 0) {
                        int i10 = 0;
                        while (i10 < 5 && poker.n[i10].a.booleanValue()) {
                            i10++;
                        }
                        int i11 = poker.Y - 1;
                        poker.Y = i11;
                        if (i11 == 0 && i10 == 5) {
                            poker.X++;
                            poker.W = 0;
                            break;
                        }
                    } else {
                        int i12 = 0;
                        while (true) {
                            if (i12 < 5) {
                                if (!poker.n[i12].a.booleanValue()) {
                                    poker.n[i12].a(poker.p[poker.o][poker.W], false, false, false, false);
                                    poker.n[i12].a = true;
                                    f.a(poker, (poker.W + 2) - 5);
                                    poker.W++;
                                } else {
                                    i12++;
                                }
                            }
                        }
                        poker.Y = 12;
                        break;
                    }
                }
                break;
            case 5:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X == 2) {
                            if (poker.Y != 0) {
                                int i13 = poker.Y - 1;
                                poker.Y = i13;
                                if (i13 == 0 && poker.W == 5) {
                                    poker.x = 6;
                                    poker.X = 0;
                                    poker.W = 0;
                                    poker.Y = 15;
                                    break;
                                }
                            } else {
                                CardButton[] cardButtonArr2 = poker.n;
                                int i14 = poker.W;
                                poker.W = i14 + 1;
                                cardButtonArr2[i14].a(0, false, true, false, false);
                                poker.Y = 1;
                                break;
                            }
                        }
                    } else {
                        if (poker.ar[4] > 0) {
                            poker.ar[4] = 0;
                            f.b(poker, 11);
                            poker.x = 11;
                            poker.X = 0;
                        }
                        if (poker.ar[5] > 0) {
                            poker.ar[5] = 0;
                            f.a(poker, 1);
                            poker.b();
                            poker.aJ.removeAllViews();
                            poker.L.setVisibility(8);
                            poker.aH.setVisibility(0);
                            poker.aI.removeAllViews();
                            poker.aI.addView(poker.aS);
                            poker.o ^= 1;
                            poker.a(false);
                            poker.X++;
                            break;
                        }
                    }
                } else {
                    poker.aJ.removeAllViews();
                    poker.aJ.addView(poker.aM);
                    poker.aI.removeAllViews();
                    poker.aI.addView(poker.aR);
                    f.b(poker, 11);
                    poker.X++;
                    poker.W = 0;
                    poker.Y = 0;
                    break;
                }
                break;
            case 6:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X == 2) {
                            if (poker.Y != 0) {
                                int i15 = poker.Y - 1;
                                poker.Y = i15;
                                if (i15 == 0 && poker.ak == 5) {
                                    poker.f();
                                    poker.x = 8;
                                    poker.X = 0;
                                    poker.W = 0;
                                    poker.Y = 0;
                                    break;
                                }
                            } else {
                                if (poker.al || poker.ak != 4) {
                                    while (true) {
                                        if (poker.W < 5) {
                                            if (!poker.n[poker.W].a.booleanValue()) {
                                                poker.n[poker.W].a(poker.p[poker.o][poker.W], false, false, false, false);
                                                poker.W++;
                                            } else {
                                                poker.W++;
                                            }
                                        }
                                    }
                                } else {
                                    poker.n[poker.aj].a(poker.p[poker.o][poker.aj], false, false, true, false);
                                }
                                f.a(poker, poker.ak + 1);
                                poker.ak++;
                                poker.Y = 11;
                                break;
                            }
                        }
                    } else if (poker.al) {
                        poker.X++;
                        poker.Y = 22;
                        break;
                    } else {
                        for (int i16 = 0; i16 < 5; i16++) {
                            if (i16 != poker.ai && poker.n[i16].a.booleanValue()) {
                                poker.aj = i16;
                                poker.X++;
                            }
                        }
                        poker.Y = 14;
                        break;
                    }
                } else if (!poker.c()) {
                    int i17 = poker.Y - 1;
                    poker.Y = i17;
                    if (i17 <= 0) {
                        if (poker.am) {
                            poker.am = false;
                            int i18 = 0;
                            for (int i19 = 0; i19 < 5; i19++) {
                                if (i18 < bd[poker.p[poker.o][i19] & 15]) {
                                    i18 = bd[poker.p[poker.o][i19] & 15];
                                }
                            }
                            random = (int) (Math.random() * 5.0d);
                            for (int i20 = 0; i20 < 5 && bd[poker.p[poker.o][random] & 15] >= i18; i20++) {
                                random = (random + 1) % 5;
                            }
                        } else {
                            random = (int) (Math.random() * 5.0d);
                        }
                        poker.ai = random;
                        poker.al = false;
                        if ((poker.p[poker.o][poker.ai] & 15) == 13) {
                            poker.al = true;
                        }
                        for (int i21 = 0; i21 < 5; i21++) {
                            if (i21 == poker.ai) {
                                poker.n[i21].a(false, 1, false);
                                poker.n[i21].a(poker.p[poker.o][i21], false, false, true, false);
                                f.a(poker, 16);
                            } else if (!poker.al) {
                                poker.n[i21].a(true, 2, true);
                            }
                        }
                        poker.W = 0;
                        poker.ak = 1;
                        poker.X++;
                        break;
                    }
                }
                break;
            case 8:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X == 2) {
                            poker.X = 0;
                            poker.W = 0;
                            if (poker.ao != 0) {
                                if (poker.ao <= 0) {
                                    poker.x = 10;
                                    break;
                                } else {
                                    poker.x = 5;
                                    break;
                                }
                            } else {
                                poker.o ^= 1;
                                poker.a(false);
                                poker.x = 5;
                                poker.X = 2;
                                poker.Y = 12;
                                break;
                            }
                        }
                    } else {
                        poker.X++;
                        break;
                    }
                } else {
                    int i22 = poker.n[poker.ai].b & 15;
                    int i23 = !poker.al ? poker.n[poker.aj].b & 15 : 1;
                    if (poker.an[i22][1] >= 12) {
                        poker.am = true;
                    } else {
                        poker.am = false;
                    }
                    if (i23 == i22) {
                        poker.ao = 0;
                        f.a(poker, 15);
                    } else if (poker.an[i22][0] > i23 || poker.an[i22][1] < i23) {
                        poker.ao = 1;
                        if (poker.v > poker.w) {
                            poker.w = poker.v;
                        }
                        if (poker.t < 999999999) {
                            poker.t *= 2;
                        } else {
                            poker.t = 1999999999;
                        }
                        poker.v++;
                        f.a(poker, 8);
                    } else {
                        poker.ao = -1;
                        poker.t = 0;
                    }
                    poker.b();
                    poker.a();
                    poker.X++;
                    break;
                }
                break;
            case 10:
                f.a(poker, 7);
                if (poker.r <= 0 || (poker.E == 1 && poker.Q == 0)) {
                    poker.aO.clearAnimation();
                    poker.aI.removeAllViews();
                    poker.aI.addView(poker.aN);
                    poker.as.setTextColor(poker.getResources().getColor(C0000R.color.button_text_disable));
                    poker.as.setClickable(false);
                    poker.at.setTextColor(poker.getResources().getColor(C0000R.color.button_text_disable));
                    poker.at.setClickable(false);
                    poker.x = 12;
                } else {
                    poker.x = 0;
                }
                poker.X = 0;
                poker.W = 0;
                break;
            case 11:
                if (poker.X != 0) {
                    if (poker.X != 1) {
                        if (poker.X == 2) {
                            poker.aP.clearAnimation();
                            poker.aQ.clearAnimation();
                            poker.x = 0;
                            poker.X = 0;
                            poker.W = 0;
                            poker.Y = 0;
                            break;
                        }
                    } else {
                        int i24 = poker.Y - 1;
                        poker.Y = i24;
                        if (i24 <= 0) {
                            if (poker.t <= poker.u) {
                                poker.X++;
                                break;
                            } else {
                                if ((poker.u & 1) == 0) {
                                    f.a(poker, 11);
                                }
                                poker.u++;
                                poker.r++;
                                if (poker.r > 1999999999) {
                                    poker.r = 1999999999;
                                }
                                poker.a();
                                break;
                            }
                        }
                    }
                } else {
                    poker.aJ.removeAllViews();
                    poker.aI.removeAllViews();
                    if (poker.t < 500) {
                        poker.aI.addView(poker.aP);
                        poker.aP.startAnimation(poker.N);
                    } else {
                        poker.aI.addView(poker.aQ);
                        poker.aQ.startAnimation(poker.O);
                    }
                    f.b(poker, 11);
                    poker.Y = 0;
                    poker.X++;
                    break;
                }
                break;
            case 12:
                if (poker.X == 0) {
                    if (poker.E == 1) {
                        int i25 = poker.r;
                        int i26 = poker.w;
                        int a2 = Settings.a((Context) poker, "ranking_credit", 100);
                        poker.R = i25;
                        poker.S = i26;
                        if (i25 > a2) {
                            f.a(poker, 18);
                            new AlertDialog.Builder(poker).setIcon((int) C0000R.drawable.ic_dialog_menu_generic).setTitle((int) C0000R.string.label_ranking_entry).setPositiveButton((int) C0000R.string.alert_yes_text, new q(poker, i25, i26)).setNegativeButton((int) C0000R.string.alert_no_text, new p(poker)).setCancelable(false).show();
                        } else if (i25 <= 0) {
                            f.a(poker, 18);
                            new AlertDialog.Builder(poker).setIcon(17301543).setTitle((int) C0000R.string.label_no_entry).setPositiveButton((int) C0000R.string.alert_ok_text, new x(poker)).setCancelable(false).show();
                        } else {
                            f.a(poker, 18);
                            new AlertDialog.Builder(poker).setIcon(17301543).setTitle((int) C0000R.string.label_low_score_entry_l).setMessage((int) C0000R.string.label_low_score_entry).setPositiveButton((int) C0000R.string.alert_yes_text, new o(poker)).setNegativeButton((int) C0000R.string.alert_no_text, new y(poker)).setCancelable(false).show();
                        }
                        poker.Q = 0;
                        poker.P = System.currentTimeMillis() - 3600000;
                    } else {
                        f.a(poker, 18);
                        new AlertDialog.Builder(poker).setIcon((int) C0000R.drawable.ic_dialog_menu_generic).setTitle((int) C0000R.string.label_add_credit).setPositiveButton((int) C0000R.string.alert_ok_text, new u(poker)).setNegativeButton((int) C0000R.string.alert_exit_text, new t(poker)).setCancelable(false).show();
                    }
                    poker.X++;
                }
                if (poker.X == 1 && poker.E == 0 && poker.r > 0) {
                    poker.a();
                    poker.x = 0;
                    poker.X = 0;
                    poker.W = 0;
                    break;
                }
        }
        poker.L.a();
        if (poker.E == 1) {
            poker.aA = System.currentTimeMillis() - poker.P;
            poker.Q = 3600000 - poker.aA;
            if (poker.Q < 0) {
                poker.Q = 0;
            }
        }
        poker.d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void */
    static /* synthetic */ void k(Poker poker) {
        if (poker.R > Settings.a((Context) poker, "ranking_credit", 100)) {
            Settings.b((Context) poker, "ranking_credit", poker.R);
        }
        if (poker.S > Settings.a((Context) poker, "ranking_combo", 0)) {
            Settings.b((Context) poker, "ranking_combo", poker.S);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, int):int
      jp.co.kixx.game.casino.Settings.a(android.content.Context, java.lang.String, long):long */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int a2;
        super.onCreate(bundle);
        if (getResources().getString(C0000R.string.screen_size).equals("xlarge")) {
            setRequestedOrientation(0);
        }
        if (!CasinoGamesActivity.a(this)) {
            setContentView((int) C0000R.layout.poker);
        } else {
            setContentView((int) C0000R.layout.q_poker);
        }
        this.E = getIntent().getIntExtra("extra_mode", 0);
        if (this.E == 0) {
            a2 = Settings.a((Context) this, "normal_credit", 100);
            if (a2 <= 0) {
                a2 = 100;
            }
        } else {
            a2 = Settings.a((Context) this, "ttrial_credit", 100);
        }
        this.r = a2;
        this.w = this.E == 0 ? Settings.a((Context) this, "normal_combo", 0) : Settings.a((Context) this, "ttrial_combo", 0);
        this.T = Settings.a((Context) this, "save_ranking_credit", 0);
        this.U = Settings.a((Context) this, "save_ranking_combo", 0);
        Settings.b((Context) this, "save_ranking_credit", 0);
        Settings.b((Context) this, "save_ranking_combo", 0);
        this.Q = this.E == 1 ? Settings.a((Context) this, "ttrial_time", 3600000L) : 0;
        if (this.E == 1 && this.Q == 0) {
            if (this.T == 0) {
                this.r = 100;
                this.Q = 3600000;
                this.w = 0;
            } else {
                this.r = this.T;
                this.w = this.U;
                this.Q = 0;
            }
        }
        for (int i2 = 0; i2 < 52; i2++) {
            int[] iArr = this.p[0];
            int i3 = ((1 << (i2 / 13)) << 4) | (i2 % 13);
            this.p[1][i2] = i3;
            iArr[i2] = i3;
        }
        int[] iArr2 = this.p[0];
        this.p[1][52] = 253;
        iArr2[52] = 253;
        this.aG = (LinearLayout) findViewById(C0000R.id.main_layout);
        this.aI = (LinearLayout) findViewById(C0000R.id.message_block);
        this.aJ = (LinearLayout) findViewById(C0000R.id.button_block);
        this.aK = (LinearLayout) findViewById(C0000R.id.button_step1);
        this.aL = (LinearLayout) findViewById(C0000R.id.button_step2);
        this.aM = (LinearLayout) findViewById(C0000R.id.button_step3);
        this.L = (OddsView) findViewById(C0000R.id.oddsView);
        this.aH = (LinearLayout) findViewById(C0000R.id.doubleup_pane);
        this.aN = (TextView) findViewById(C0000R.id.mesage_text0);
        this.aO = (TextView) findViewById(C0000R.id.mesage_text1);
        this.aP = (TextView) findViewById(C0000R.id.mesage_text2);
        this.aQ = (TextView) findViewById(C0000R.id.mesage_text3);
        this.aR = (LinearLayout) findViewById(C0000R.id.mesage_text4);
        this.aS = (LinearLayout) findViewById(C0000R.id.mesage_text5);
        this.aT = (TextView) findViewById(C0000R.id.hand_name);
        this.aU = (TextView) findViewById(C0000R.id.hand_odds);
        this.aV = (TextView) findViewById(C0000R.id.collect_coin);
        this.aW = (TextView) findViewById(C0000R.id.try_count);
        this.aX = (TextView) findViewById(C0000R.id.next_pay);
        this.aY = (TextView) findViewById(C0000R.id.coin_wager);
        this.aZ = (TextView) findViewById(C0000R.id.coin_win);
        this.ba = (TextView) findViewById(C0000R.id.coin_paid);
        this.bb = (TextView) findViewById(C0000R.id.coin_credit);
        CardButton.a(this);
        for (int i4 = 0; i4 < 5; i4++) {
            this.n[i4] = (CardButton) findViewById(this.bc[i4]);
            this.n[i4].a();
        }
        this.as = (PlayButton) findViewById(C0000R.id.btn_1bet);
        this.at = (PlayButton) findViewById(C0000R.id.btn_maxb);
        this.au = (PlayButton) findViewById(C0000R.id.btn_deal);
        this.av = (PlayButton) findViewById(C0000R.id.btn_draw);
        this.aw = (PlayButton) findViewById(C0000R.id.btn_coll);
        this.ax = (PlayButton) findViewById(C0000R.id.btn_ddwn);
        this.as.setOnClickListener(this.ay);
        this.at.setOnClickListener(this.ay);
        this.au.setOnClickListener(this.ay);
        this.av.setOnClickListener(this.ay);
        this.aw.setOnClickListener(this.ay);
        this.ax.setOnClickListener(this.ay);
        e();
        if (this.E != 1) {
            this.aG.removeView((LinearLayout) findViewById(C0000R.id.timetrial_layout));
        }
        this.P = System.currentTimeMillis();
        this.az = (TextView) findViewById(C0000R.id.time_left);
        this.M = AnimationUtils.loadAnimation(getBaseContext(), C0000R.anim.start_play);
        this.N = AnimationUtils.loadAnimation(getBaseContext(), C0000R.anim.you_win);
        this.O = AnimationUtils.loadAnimation(getBaseContext(), C0000R.anim.congratu);
        setVolumeControlStream(3);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0000R.menu.menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_settings /*2131492928*/:
                startActivity(new Intent(this, Settings.class));
                return true;
            case C0000R.id.menu_exit /*2131492929*/:
                finish();
                return true;
            default:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void
     arg types: [jp.co.kixx.game.casino.Poker, java.lang.String, int]
     candidates:
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, long):void
      jp.co.kixx.game.casino.Settings.b(android.content.Context, java.lang.String, int):void */
    public void onPause() {
        this.K = false;
        this.H.cancel();
        this.H.purge();
        if (this.E == 0) {
            Settings.b((Context) this, "normal_credit", this.r);
            Settings.b((Context) this, "normal_combo", this.w);
        } else {
            Settings.b(this, "ttrial_time", this.Q);
            Settings.b((Context) this, "ttrial_credit", this.r);
            Settings.b((Context) this, "ttrial_combo", this.w);
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        w wVar = new w(this);
        this.H = new Timer();
        this.H.schedule(wVar, 0, 33);
        this.P = System.currentTimeMillis() - (3600000 - this.Q);
        this.F = new v(this);
        f.a(this);
        d();
        a();
        this.K = true;
        this.G = new Thread(this.V);
        this.G.start();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
