package jp.co.kixx.game.casino;

import android.preference.Preference;

final class c implements Preference.OnPreferenceChangeListener {
    private /* synthetic */ Settings a;

    c(Settings settings) {
        this.a = settings;
    }

    public final boolean onPreferenceChange(Preference preference, Object obj) {
        if (obj == null) {
            return false;
        }
        preference.setSummary("( " + this.a.e[Integer.parseInt(obj.toString())] + " )\n" + this.a.f[Integer.parseInt(obj.toString())]);
        return true;
    }
}
