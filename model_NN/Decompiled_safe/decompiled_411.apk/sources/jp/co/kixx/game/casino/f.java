package jp.co.kixx.game.casino;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public final class f {
    private static SoundPool a;
    private static Boolean b = false;
    private static float c = 0.5f;
    private static int[] d;
    private static int[] e;
    private static final int[] f = {-1, C0000R.raw.se_button, C0000R.raw.se_deal1, C0000R.raw.se_deal2, C0000R.raw.se_deal3, C0000R.raw.se_deal4, C0000R.raw.se_deal5, C0000R.raw.se_lose1, C0000R.raw.se_win7, C0000R.raw.se_bet7, C0000R.raw.se_held2, C0000R.raw.se_pay8, C0000R.raw.se_hand2, -1, -1, C0000R.raw.se_pushed1, C0000R.raw.se_deler_open6, C0000R.raw.se_cancel1, C0000R.raw.se_dialog2};

    public f(Context context) {
        b(context);
    }

    public static void a() {
        if (a != null) {
            for (int i = 1; i < f.length; i++) {
                a.unload(f[i]);
            }
            a.release();
            a = null;
        }
    }

    public static void a(Context context) {
        c = (float) ((AudioManager) ((Activity) context).getSystemService("audio")).getStreamVolume(3);
        b = Boolean.valueOf(Settings.e(context));
    }

    static void a(Context context, int i) {
        if (b.booleanValue()) {
            c(context);
            if (d[i] >= 0) {
                a.play(d[i], c, c, 1, 0, 1.0f);
            }
        }
    }

    private static void b(Context context) {
        a = new SoundPool(8, 3, 0);
        d = new int[f.length];
        e = new int[f.length];
        for (int i = 0; i < f.length; i++) {
            if (f[i] > 0) {
                try {
                    d[i] = a.load(context, f[i], 1);
                } catch (NullPointerException e2) {
                    d[i] = -1;
                }
            } else {
                d[i] = f[i];
            }
            e[i] = d[i];
        }
    }

    static void b(Context context, int i) {
        if (b.booleanValue()) {
            c(context);
            if (d[i] >= 0) {
                a.play(d[i], 0.0f, 0.0f, 1, 0, 1.0f);
            }
        }
    }

    private static void c(Context context) {
        if (d == null || e == null || d == null) {
            b(context);
            a(context);
        }
    }
}
