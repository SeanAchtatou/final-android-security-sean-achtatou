package jp.co.kixx.game.casino;

import android.view.View;
import android.widget.ImageView;

final class e implements View.OnClickListener {
    private /* synthetic */ CardButton a;

    e(CardButton cardButton) {
        this.a = cardButton;
    }

    public final void onClick(View view) {
        if (this.a.k.booleanValue() && !CardButton.d.booleanValue()) {
            ((ImageView) view).setImageBitmap(CardButton.c[this.a.n]);
            CardButton cardButton = this.a;
            cardButton.a = Boolean.valueOf(!cardButton.a.booleanValue());
            this.a.a((ImageView) view);
            if (this.a.a.booleanValue() && this.a.m.booleanValue()) {
                CardButton.d = (Boolean) true;
            }
            if (this.a.a.booleanValue()) {
                f.a(this.a.getContext(), 10);
            } else {
                f.a(this.a.getContext(), 17);
            }
        }
    }
}
