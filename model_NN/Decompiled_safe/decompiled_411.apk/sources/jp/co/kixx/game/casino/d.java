package jp.co.kixx.game.casino;

import android.view.animation.Animation;

final class d implements Animation.AnimationListener {
    private /* synthetic */ CasinoGamesActivity a;

    d(CasinoGamesActivity casinoGamesActivity) {
        this.a = casinoGamesActivity;
    }

    public final void onAnimationEnd(Animation animation) {
        CasinoGamesActivity casinoGamesActivity = this.a;
        casinoGamesActivity.findViewById(C0000R.id.splash_layout).clearAnimation();
        casinoGamesActivity.findViewById(C0000R.id.splash_layout).setVisibility(8);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
