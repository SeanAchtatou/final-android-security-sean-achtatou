package jp.co.nobot.libAdMaker;

import java.security.MessageDigest;

public final class b {
    public static String a(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
        }
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        instance.update(str.getBytes());
        return a(instance.digest());
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() < 2) {
                hexString = "0" + hexString;
            }
            stringBuffer.append(hexString);
        }
        return new String(stringBuffer);
    }
}
