package jp.co.nobot.libAdMaker;

import java.security.MessageDigest;

public final class a {
    public static String a(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
        }
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(str.getBytes());
        return a(instance.digest());
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            if ((bArr[i] & 255) < 16) {
                stringBuffer.append("0" + Integer.toHexString(bArr[i] & 255));
            } else {
                stringBuffer.append(Integer.toHexString(bArr[i] & 255));
            }
        }
        return stringBuffer.toString();
    }
}
