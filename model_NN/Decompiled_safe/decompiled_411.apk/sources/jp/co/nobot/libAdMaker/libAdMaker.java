package jp.co.nobot.libAdMaker;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.AttributeSet;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import java.security.NoSuchAlgorithmException;

public class libAdMaker extends FrameLayout {
    public String a;
    public String b;
    /* access modifiers changed from: private */
    public WebView c;
    private WebSettings d;
    private String e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public Context h;
    private LinearLayout i;
    private ImageButton j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    /* access modifiers changed from: private */
    public e u;

    public libAdMaker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h = context;
        g();
    }

    public libAdMaker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = context;
        g();
    }

    private void g() {
        setBackgroundColor(0);
        this.f = false;
        Resources resources = this.h.getResources();
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        this.c = new WebView(this.h);
        this.c.setBackgroundColor(0);
        this.i = new LinearLayout(this.h);
        this.j = new ImageButton(this.h);
        this.j.setBackgroundResource(17301560);
        this.i.setGravity(85);
        this.j.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.i.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (resources.getDisplayMetrics().density * 50.0f)));
        this.i.addView(this.j);
        addView(this.c);
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (resources.getDisplayMetrics().density * 50.0f)));
        addView(this.i);
        this.j.setVisibility(8);
        this.j.setOnClickListener(new c(this));
        CookieSyncManager.createInstance(this.h);
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().removeExpiredCookie();
        this.m = Build.VERSION.RELEASE;
        try {
            if (Settings.Secure.getString(this.h.getContentResolver(), "android_id") == null) {
                this.l = "nofound";
            } else {
                this.l = a.a(Settings.Secure.getString(this.h.getContentResolver(), "android_id"));
            }
        } catch (NoSuchAlgorithmException e2) {
            this.l = "nofound";
        }
        this.n = "Android " + Build.MODEL;
        this.o = this.h.getPackageName();
        this.p = "admaker_appname=" + this.o + "; domain=" + "ad-maker.info";
        this.q = "admaker_did=" + this.l + "; domain=" + "ad-maker.info";
        this.r = "admaker_version=1.1; domain=ad-maker.info";
        this.s = "admaker_osVersion=" + this.m + "; domain=" + "ad-maker.info";
        this.t = "admaker_deviceModel=" + this.n + "; domain=" + "ad-maker.info";
        CookieManager.getInstance().setCookie("ad-maker.info", this.p);
        CookieManager.getInstance().setCookie("ad-maker.info", this.q);
        CookieManager.getInstance().setCookie("ad-maker.info", this.r);
        CookieManager.getInstance().setCookie("ad-maker.info", this.s);
        CookieManager.getInstance().setCookie("ad-maker.info", this.t);
    }

    /* access modifiers changed from: private */
    public boolean h() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.h.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            setVisibility(8);
            return false;
        }
        setVisibility(0);
        return true;
    }

    public final void a() {
        this.g = false;
        if (h()) {
            this.c.loadUrl(this.e);
            this.c.setWebViewClient(new d(this));
        }
    }

    public final void a(String str) {
        this.e = str;
    }

    public final void b() {
        Resources resources = this.h.getResources();
        this.j.setVisibility(8);
        this.i.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (resources.getDisplayMetrics().density * 50.0f)));
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (resources.getDisplayMetrics().density * 50.0f)));
        this.f = false;
        a();
    }

    public final void c() {
        Resources resources = this.h.getResources();
        this.j.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.i.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (270.0f * resources.getDisplayMetrics().density)));
        this.c.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (resources.getDisplayMetrics().density * 250.0f)));
        this.j.setVisibility(0);
    }

    public final void d() {
        this.d = this.c.getSettings();
        this.d.setJavaScriptEnabled(true);
        this.c.clearCache(true);
        CookieSyncManager.getInstance().startSync();
        if (!this.f) {
            this.k = false;
        }
        this.f = false;
        a();
    }

    public final void e() {
        CookieSyncManager.getInstance().stopSync();
        this.c.destroy();
        this.c = null;
        this.i = null;
        this.j = null;
    }

    public final void f() {
        this.f = true;
        this.k = true;
        CookieSyncManager.getInstance().stopSync();
    }
}
