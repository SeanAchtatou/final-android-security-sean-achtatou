package defpackage;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: ah  reason: default package */
public class ah extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity f11a;

    public ah(TransparentActivity transparentActivity) {
        this.f11a = transparentActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.f11a.k == null) {
                    ProgressDialog unused = this.f11a.k = new ProgressDialog(this.f11a.h);
                    this.f11a.k.setMessage("正在处理...");
                    this.f11a.k.setOnCancelListener(new ai(this));
                    this.f11a.k.show();
                    return;
                }
                return;
            case 2:
                if (this.f11a.k != null) {
                    this.f11a.k.dismiss();
                }
                Toast.makeText(this.f11a.h, message.arg1 == 1 ? "执行成功" : "执行失败", 0).show();
                this.f11a.finish();
                return;
            case 3:
                this.f11a.finish();
                return;
            case 4:
                new TransparentActivity.a(message.arg1).start();
                return;
            default:
                return;
        }
    }
}
