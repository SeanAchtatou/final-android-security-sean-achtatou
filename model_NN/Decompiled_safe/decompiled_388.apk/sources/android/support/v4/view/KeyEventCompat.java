package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

/* compiled from: ProGuard */
public class KeyEventCompat {
    static final y IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new x();
        } else {
            IMPL = new w();
        }
    }

    public static int normalizeMetaState(int i) {
        return IMPL.a(i);
    }

    public static boolean metaStateHasModifiers(int i, int i2) {
        return IMPL.a(i, i2);
    }

    public static boolean metaStateHasNoModifiers(int i) {
        return IMPL.b(i);
    }

    public static boolean hasModifiers(KeyEvent keyEvent, int i) {
        return IMPL.a(keyEvent.getMetaState(), i);
    }

    public static boolean hasNoModifiers(KeyEvent keyEvent) {
        return IMPL.b(keyEvent.getMetaState());
    }
}
