package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

/* compiled from: ProGuard */
public class VelocityTrackerCompat {
    static final aw IMPL;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            IMPL = new av();
        } else {
            IMPL = new au();
        }
    }

    public static float getXVelocity(VelocityTracker velocityTracker, int i) {
        return IMPL.a(velocityTracker, i);
    }

    public static float getYVelocity(VelocityTracker velocityTracker, int i) {
        return IMPL.b(velocityTracker, i);
    }
}
