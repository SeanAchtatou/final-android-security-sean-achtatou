package android.support.v4.view;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class u extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ t f98a;

    u(t tVar) {
        this.f98a = tVar;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    u(t tVar, Handler handler) {
        super(handler.getLooper());
        this.f98a = tVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f98a.i.onShowPress(this.f98a.o);
                return;
            case 2:
                this.f98a.d();
                return;
            case 3:
                if (this.f98a.j != null && !this.f98a.k) {
                    this.f98a.j.onSingleTapConfirmed(this.f98a.o);
                    return;
                }
                return;
            default:
                throw new RuntimeException("Unknown message " + message);
        }
    }
}
