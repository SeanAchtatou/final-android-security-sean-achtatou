package android.support.v4.content;

import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/* compiled from: ProGuard */
class e extends FutureTask<Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ModernAsyncTask f74a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    e(ModernAsyncTask modernAsyncTask, Callable callable) {
        super(callable);
        this.f74a = modernAsyncTask;
    }

    /* access modifiers changed from: protected */
    public void done() {
        try {
            this.f74a.c(get());
        } catch (InterruptedException e) {
            Log.w("AsyncTask", e);
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
        } catch (CancellationException e3) {
            this.f74a.c(null);
        } catch (Throwable th) {
            throw new RuntimeException("An error occured while executing doInBackground()", th);
        }
    }
}
