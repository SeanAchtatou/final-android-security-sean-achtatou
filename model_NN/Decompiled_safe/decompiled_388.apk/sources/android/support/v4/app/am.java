package android.support.v4.app;

import android.app.Notification;
import java.util.Iterator;

/* compiled from: ProGuard */
class am implements ai {
    am() {
    }

    public Notification a(ag agVar) {
        aq aqVar = new aq(agVar.f51a, agVar.r, agVar.b, agVar.c, agVar.h, agVar.f, agVar.i, agVar.d, agVar.e, agVar.g, agVar.n, agVar.o, agVar.p, agVar.k, agVar.j, agVar.m);
        Iterator<ad> it = agVar.q.iterator();
        while (it.hasNext()) {
            ad next = it.next();
            aqVar.a(next.f48a, next.b, next.c);
        }
        if (agVar.l != null) {
            if (agVar.l instanceof af) {
                af afVar = (af) agVar.l;
                aqVar.a(afVar.b, afVar.d, afVar.c, afVar.f50a);
            } else if (agVar.l instanceof ah) {
                ah ahVar = (ah) agVar.l;
                aqVar.a(ahVar.b, ahVar.d, ahVar.c, ahVar.f52a);
            } else if (agVar.l instanceof ae) {
                ae aeVar = (ae) agVar.l;
                aqVar.a(aeVar.b, aeVar.d, aeVar.c, aeVar.f49a);
            }
        }
        return aqVar.a();
    }
}
