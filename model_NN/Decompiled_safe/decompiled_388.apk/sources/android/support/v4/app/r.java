package android.support.v4.app;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: ProGuard */
public abstract class r extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private final l f64a;
    private w b = null;
    private Fragment c = null;

    public abstract Fragment a(int i);

    public r(l lVar) {
        this.f64a = lVar;
    }

    public void startUpdate(ViewGroup viewGroup) {
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (this.b == null) {
            this.b = this.f64a.a();
        }
        long b2 = b(i);
        Fragment a2 = this.f64a.a(a(viewGroup.getId(), b2));
        if (a2 != null) {
            this.b.b(a2);
        } else {
            a2 = a(i);
            this.b.a(viewGroup.getId(), a2, a(viewGroup.getId(), b2));
        }
        if (a2 != this.c) {
            a2.b(false);
            a2.c(false);
        }
        return a2;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        if (this.b == null) {
            this.b = this.f64a.a();
        }
        this.b.a((Fragment) obj);
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (fragment != this.c) {
            if (this.c != null) {
                this.c.b(false);
                this.c.c(false);
            }
            if (fragment != null) {
                fragment.b(true);
                fragment.c(true);
            }
            this.c = fragment;
        }
    }

    public void finishUpdate(ViewGroup viewGroup) {
        if (this.b != null) {
            this.b.b();
            this.b = null;
            this.f64a.b();
        }
    }

    public boolean isViewFromObject(View view, Object obj) {
        return ((Fragment) obj).h() == view;
    }

    public Parcelable saveState() {
        return null;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    public long b(int i) {
        return (long) i;
    }

    private static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }
}
