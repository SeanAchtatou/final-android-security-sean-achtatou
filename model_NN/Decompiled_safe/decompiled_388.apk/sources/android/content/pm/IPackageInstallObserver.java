package android.content.pm;

import android.os.IInterface;

/* compiled from: ProGuard */
public interface IPackageInstallObserver extends IInterface {
    void packageInstalled(String str, int i);
}
