package android.content.b.a;

import android.content.pm.a;
import android.os.Process;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Arrays;
import java.util.HashSet;

/* compiled from: ProGuard */
public class b {
    private static int i = -1;
    private static final CharsetDecoder j = Charset.forName("UTF-16LE").newDecoder();
    private static final CharsetDecoder k = Charset.forName("UTF-8").newDecoder();

    /* renamed from: a  reason: collision with root package name */
    public h f23a = new h();
    public c[] b = null;
    public f c = new f();
    private InputStream d = null;
    private short e = -1;
    private int f = 0;
    private int g = 0;
    private boolean h = false;
    private boolean l = false;

    public void a(int i2) {
        this.c.a(i2);
    }

    public void a() {
        this.c.a();
    }

    public b(InputStream inputStream) {
        this.d = inputStream;
    }

    public void a(a aVar, boolean z) {
        this.h = z;
        b();
        if (this.e != 2) {
            throw new Exception("YYY !=2");
        }
        this.g = d.b(this.d);
        a(this.f23a);
        b();
        this.b = new c[this.g];
        for (int i2 = 0; i2 < this.g; i2++) {
            this.b[i2] = new c();
            b(this.b[i2], aVar);
        }
    }

    public int b() {
        if (this.d.available() < 2) {
            this.e = -1;
            return -1;
        }
        this.e = d.a(this.d);
        d.a(this.d, 2);
        this.f = d.b(this.d);
        return 0;
    }

    public void a(h hVar) {
        if (d.b(this.d) != 1835009) {
            throw new Exception("ZZZ1835009");
        }
        hVar.b = d.b(this.d);
        hVar.c = d.b(this.d);
        hVar.d = d.b(this.d);
        int b2 = d.b(this.d);
        hVar.e = d.b(this.d);
        hVar.f = d.b(this.d);
        hVar.f27a = (b2 & Process.PROC_COMBINE) != 0;
        hVar.g = d.b(this.d, hVar.c);
        if (hVar.d != 0) {
            hVar.h = d.b(this.d, hVar.d);
        }
        int i2 = (hVar.f == 0 ? hVar.b : hVar.f) - hVar.e;
        if (i2 % 4 != 0) {
            throw new IOException("String data size is not multiple of 4 (" + i2 + ").");
        }
        hVar.i = new byte[i2];
        d.a(this.d, hVar.i);
        if (hVar.f != 0) {
            int i3 = hVar.b - hVar.f;
            if (i3 % 4 != 0) {
                throw new IOException("Style data size is not multiple of 4 (" + i3 + ").");
            }
            hVar.j = d.b(this.d, i3 / 4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public void a(c cVar, a aVar) {
        byte c2 = d.c(this.d);
        d.a(this.d, 3);
        int b2 = d.b(this.d);
        cVar.c = new boolean[b2];
        Arrays.fill(cVar.c, true);
        d.a(this.d, b2 * 4);
        cVar.f24a = (c2 << 16) | (-16777216 & cVar.f24a);
        b();
        while (this.e == 513) {
            a(cVar);
            if (cVar.f != null) {
                if (cVar.f.d != null || cVar.f.c != null) {
                    if (aVar.m == null) {
                        aVar.m = new HashSet<>();
                    }
                    StringBuilder sb = new StringBuilder();
                    if (cVar.f.c != null) {
                        sb.append(cVar.f.c);
                    } else {
                        sb.append(' ');
                    }
                    sb.append('@');
                    if (cVar.f.d != null) {
                        sb.append(cVar.f.d);
                    } else {
                        sb.append(' ');
                    }
                    aVar.m.add(sb.toString());
                }
            }
            if (b() < 0) {
                this.e = -1;
                return;
            }
        }
    }

    public void b(c cVar, a aVar) {
        if (this.e != 512) {
            throw new Exception("xxxx512");
        }
        int b2 = d.b(this.d);
        String a2 = d.a(this.d, 128, true);
        d.a(this.d, 4);
        d.a(this.d, 4);
        d.a(this.d, 4);
        d.a(this.d, 4);
        a(cVar.d);
        a(cVar.e);
        cVar.f24a = b2 << 24;
        cVar.b = a2;
        System.out.println("package:" + cVar.b + " --" + cVar.f24a);
        b();
        while (this.e == 514) {
            a(cVar, aVar);
        }
    }

    public void a(c cVar) {
        d.a(this.d, 4);
        int b2 = d.b(this.d);
        d.a(this.d, 4);
        cVar.f = c();
        int[] b3 = d.b(this.d, b2);
        if (cVar.f != null) {
            if (cVar.f.p) {
                cVar.b + cVar.f.q;
                if (this.h) {
                }
            }
            cVar.f = (!cVar.f.p || this.h) ? cVar.a(cVar.f) : null;
            if (cVar.f != null) {
                if (!new String(cVar.f.c).equals("ZH") || !new String(cVar.f.d).equals("CN")) {
                    this.l = false;
                } else {
                    this.l = true;
                }
            }
        }
        for (int i2 = 0; i2 < b3.length; i2++) {
            if (b3[i2] != -1) {
                cVar.c[i2] = false;
                int i3 = (cVar.f24a & -65536) | i2;
                if (cVar.f != null) {
                    a(cVar, i3);
                }
            }
        }
    }

    public void a(c cVar, int i2) {
        int i3 = 0;
        d.a(this.d, 2);
        short a2 = d.a(this.d);
        d.b(this.d);
        if ((a2 & 1) == 0) {
            i++;
            String b2 = b(cVar);
            if (this.c.f25a != null) {
                while (true) {
                    if (i3 >= this.c.f25a.length) {
                        i3 = -1;
                        break;
                    } else if (this.c.f25a[i3].f26a == i2) {
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i3 >= 0 && new String(cVar.f.c).equals("ZH") && new String(cVar.f.d).equals("CN")) {
                    this.c.f25a[i3].b = b2;
                } else if (i3 >= 0 && this.c.f25a[i3].b == null) {
                    this.c.f25a[i3].b = b2;
                }
            }
        } else {
            i++;
            String[] c2 = c(cVar);
            if (this.c.f25a != null) {
                int i4 = 0;
                while (true) {
                    if (i4 >= this.c.f25a.length) {
                        i4 = -1;
                        break;
                    } else if (this.c.f25a[i4].f26a == i2) {
                        break;
                    } else {
                        i4++;
                    }
                }
                if (i4 >= 0 && cVar.f != null && new String(cVar.f.c).equals("ZH") && new String(cVar.f.d).equals("CN")) {
                    this.c.f25a[i4].b = c2[0];
                } else if (i4 >= 0 && this.c.f25a[i4].b == null) {
                    this.c.f25a[i4].b = c2[0];
                }
            }
        }
    }

    private a c() {
        int b2 = d.b(this.d);
        if (b2 < 28) {
            throw new Exception("nweeeexx");
        }
        boolean z = false;
        short a2 = d.a(this.d);
        short a3 = d.a(this.d);
        char[] cArr = {(char) d.c(this.d), (char) d.c(this.d)};
        char[] cArr2 = {(char) d.c(this.d), (char) d.c(this.d)};
        byte c2 = d.c(this.d);
        byte c3 = d.c(this.d);
        short a4 = d.a(this.d);
        byte c4 = d.c(this.d);
        byte c5 = d.c(this.d);
        byte c6 = d.c(this.d);
        d.a(this.d, 1);
        short a5 = d.a(this.d);
        short a6 = d.a(this.d);
        short a7 = d.a(this.d);
        d.a(this.d, 2);
        byte b3 = 0;
        byte b4 = 0;
        if (b2 >= 32) {
            b3 = d.c(this.d);
            b4 = d.c(this.d);
            d.a(this.d, 2);
        }
        int i2 = b2 - 32;
        if (i2 > 0) {
            byte[] bArr = new byte[i2];
            d.a(this.d, bArr);
            if (!new BigInteger(bArr).equals(BigInteger.ZERO)) {
                z = true;
            }
        }
        return new a(a2, a3, cArr, cArr2, c2, c3, a4, c4, c5, c6, a5, a6, a7, b3, b4, z);
    }

    public String b(c cVar) {
        d.a(this.d);
        d.c(this.d);
        byte c2 = d.c(this.d);
        int b2 = d.b(this.d);
        if (c2 == 3) {
            return a(b2, this.f23a);
        }
        return null;
    }

    public String[] c(c cVar) {
        d.b(this.d);
        int b2 = d.b(this.d);
        String[] strArr = new String[b2];
        for (int i2 = 0; i2 < b2; i2++) {
            d.b(this.d);
            strArr[i2] = b(cVar);
        }
        return strArr;
    }

    public static String a(int i2, h hVar) {
        int i3;
        int i4;
        if (i2 < 0 || hVar == null || i2 >= hVar.g.length) {
            return null;
        }
        int i5 = hVar.g[i2];
        if (!hVar.f27a) {
            i4 = a(hVar.i, i5) * 2;
            i3 = i5 + 2;
        } else {
            int i6 = b(hVar.i, i5)[1] + i5;
            int[] b2 = b(hVar.i, i6);
            i3 = b2[1] + i6;
            i4 = b2[0];
        }
        return a(hVar, i3, i4);
    }

    private static int a(byte[] bArr, int i2) {
        return ((bArr[i2 + 1] & 255) << 8) | (bArr[i2] & 255);
    }

    private static final int[] b(byte[] bArr, int i2) {
        boolean z;
        byte b2 = bArr[i2];
        if ((b2 & 128) != 0) {
            z = true;
        } else {
            z = false;
        }
        int i3 = b2 & Byte.MAX_VALUE;
        if (!z) {
            return new int[]{i3, 1};
        }
        return new int[]{(i3 << 8) | (bArr[i2 + 1] & 255), 2};
    }

    private static String a(h hVar, int i2, int i3) {
        try {
            return (hVar.f27a ? k : j).decode(ByteBuffer.wrap(hVar.i, i2, i3)).toString();
        } catch (CharacterCodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
