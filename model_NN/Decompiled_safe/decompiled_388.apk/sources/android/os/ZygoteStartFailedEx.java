package android.os;

/* compiled from: ProGuard */
class ZygoteStartFailedEx extends Exception {
    ZygoteStartFailedEx() {
    }

    ZygoteStartFailedEx(String str) {
        super(str);
    }

    ZygoteStartFailedEx(Throwable th) {
        super(th);
    }
}
