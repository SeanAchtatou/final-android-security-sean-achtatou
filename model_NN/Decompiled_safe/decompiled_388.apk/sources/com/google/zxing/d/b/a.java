package com.google.zxing.d.b;

import com.google.zxing.j;

public final class a extends j {

    /* renamed from: a  reason: collision with root package name */
    private final float f185a;

    a(float f, float f2, float f3) {
        super(f, f2);
        this.f185a = f3;
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f, float f2, float f3) {
        if (Math.abs(f2 - b()) > f || Math.abs(f3 - a()) > f) {
            return false;
        }
        float abs = Math.abs(f - this.f185a);
        return abs <= 1.0f || abs / this.f185a <= 1.0f;
    }
}
