package com.google.zxing.d.b;

import com.google.zxing.common.f;

class g implements f {

    /* renamed from: a  reason: collision with root package name */
    private final float f191a;

    public g(float f) {
        this.f191a = f;
    }

    public int a(Object obj, Object obj2) {
        float abs = Math.abs(((d) obj2).c() - this.f191a);
        float abs2 = Math.abs(((d) obj).c() - this.f191a);
        if (abs < abs2) {
            return -1;
        }
        return abs == abs2 ? 0 : 1;
    }
}
