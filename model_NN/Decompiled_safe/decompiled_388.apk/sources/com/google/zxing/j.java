package com.google.zxing;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final float f197a;
    private final float b;

    public j(float f, float f2) {
        this.f197a = f;
        this.b = f2;
    }

    public static float a(j jVar, j jVar2) {
        float a2 = jVar.a() - jVar2.a();
        float b2 = jVar.b() - jVar2.b();
        return (float) Math.sqrt((double) ((a2 * a2) + (b2 * b2)));
    }

    private static float a(j jVar, j jVar2, j jVar3) {
        float f = jVar2.f197a;
        float f2 = jVar2.b;
        return ((jVar3.f197a - f) * (jVar.b - f2)) - ((jVar.f197a - f) * (jVar3.b - f2));
    }

    public static void a(j[] jVarArr) {
        j jVar;
        j jVar2;
        j jVar3;
        float a2 = a(jVarArr[0], jVarArr[1]);
        float a3 = a(jVarArr[1], jVarArr[2]);
        float a4 = a(jVarArr[0], jVarArr[2]);
        if (a3 >= a2 && a3 >= a4) {
            jVar = jVarArr[0];
            jVar2 = jVarArr[1];
            jVar3 = jVarArr[2];
        } else if (a4 < a3 || a4 < a2) {
            jVar = jVarArr[2];
            jVar2 = jVarArr[0];
            jVar3 = jVarArr[1];
        } else {
            jVar = jVarArr[1];
            jVar2 = jVarArr[0];
            jVar3 = jVarArr[2];
        }
        if (a(jVar2, jVar, jVar3) >= 0.0f) {
            j jVar4 = jVar3;
            jVar3 = jVar2;
            jVar2 = jVar4;
        }
        jVarArr[0] = jVar3;
        jVarArr[1] = jVar;
        jVarArr[2] = jVar2;
    }

    public final float a() {
        return this.f197a;
    }

    public final float b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof j)) {
            return false;
        }
        j jVar = (j) obj;
        return this.f197a == jVar.f197a && this.b == jVar.b;
    }

    public int hashCode() {
        return (Float.floatToIntBits(this.f197a) * 31) + Float.floatToIntBits(this.b);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(25);
        stringBuffer.append('(');
        stringBuffer.append(this.f197a);
        stringBuffer.append(',');
        stringBuffer.append(this.b);
        stringBuffer.append(')');
        return stringBuffer.toString();
    }
}
