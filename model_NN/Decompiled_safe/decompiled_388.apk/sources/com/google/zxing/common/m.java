package com.google.zxing.common;

import com.google.zxing.b;
import com.google.zxing.e;
import java.lang.reflect.Array;

public final class m extends k {

    /* renamed from: a  reason: collision with root package name */
    private b f166a = null;

    public m(e eVar) {
        super(eVar);
    }

    private static void a(byte[] bArr, int i, int i2, int i3, int i4, b bVar) {
        for (int i5 = 0; i5 < 8; i5++) {
            int i6 = ((i2 + i5) * i4) + i;
            for (int i7 = 0; i7 < 8; i7++) {
                if ((bArr[i6 + i7] & 255) < i3) {
                    bVar.b(i + i7, i2 + i5);
                }
            }
        }
    }

    private static void a(byte[] bArr, int i, int i2, int i3, int i4, int[][] iArr, b bVar) {
        int i5 = 0;
        while (i5 < i2) {
            int i6 = i5 << 3;
            if (i6 + 8 >= i4) {
                i6 = i4 - 8;
            }
            int i7 = 0;
            while (i7 < i) {
                int i8 = i7 << 3;
                if (i8 + 8 >= i3) {
                    i8 = i3 - 8;
                }
                int i9 = i7 > 1 ? i7 : 2;
                int i10 = i9 < i + -2 ? i9 : i - 3;
                int i11 = i5 > 1 ? i5 : 2;
                if (i11 >= i2 - 2) {
                    i11 = i2 - 3;
                }
                int i12 = 0;
                for (int i13 = -2; i13 <= 2; i13++) {
                    int[] iArr2 = iArr[i11 + i13];
                    i12 = i12 + iArr2[i10 - 2] + iArr2[i10 - 1] + iArr2[i10] + iArr2[i10 + 1] + iArr2[i10 + 2];
                }
                a(bArr, i8, i6, i12 / 25, i3, bVar);
                i7++;
            }
            i5++;
        }
    }

    private static int[][] a(byte[] bArr, int i, int i2, int i3, int i4) {
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, i2, i);
        for (int i5 = 0; i5 < i2; i5++) {
            int i6 = i5 << 3;
            if (i6 + 8 >= i4) {
                i6 = i4 - 8;
            }
            for (int i7 = 0; i7 < i; i7++) {
                int i8 = i7 << 3;
                if (i8 + 8 >= i3) {
                    i8 = i3 - 8;
                }
                int i9 = 0;
                byte b = 255;
                byte b2 = 0;
                int i10 = 0;
                while (i10 < 8) {
                    int i11 = ((i6 + i10) * i3) + i8;
                    int i12 = i9;
                    int i13 = 0;
                    while (i13 < 8) {
                        byte b3 = bArr[i11 + i13] & 255;
                        i12 += b3;
                        if (b3 < b) {
                            b = b3;
                        }
                        if (b3 <= b2) {
                            b3 = b2;
                        }
                        i13++;
                        b2 = b3;
                    }
                    i10++;
                    i9 = i12;
                }
                iArr[i5][i7] = b2 - b > 24 ? i9 >> 6 : b2 == 0 ? 1 : b >> 1;
            }
        }
        return iArr;
    }

    private void c() {
        if (this.f166a == null) {
            e a2 = a();
            if (a2.b() < 40 || a2.c() < 40) {
                this.f166a = super.b();
                return;
            }
            byte[] a3 = a2.a();
            int b = a2.b();
            int c = a2.c();
            int i = b >> 3;
            if ((b & 7) != 0) {
                i++;
            }
            int i2 = c >> 3;
            if ((c & 7) != 0) {
                i2++;
            }
            int[][] a4 = a(a3, i, i2, b, c);
            this.f166a = new b(b, c);
            a(a3, i, i2, b, c, a4, this.f166a);
        }
    }

    public b a(e eVar) {
        return new m(eVar);
    }

    public b b() {
        c();
        return this.f166a;
    }
}
