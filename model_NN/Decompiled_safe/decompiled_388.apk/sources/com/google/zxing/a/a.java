package com.google.zxing.a;

import com.google.zxing.NotFoundException;
import com.google.zxing.a.a.d;
import com.google.zxing.c;
import com.google.zxing.common.b;
import com.google.zxing.common.i;
import com.google.zxing.g;
import com.google.zxing.h;
import com.google.zxing.j;
import java.util.Hashtable;

public final class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final j[] f115a = new j[0];
    private final d b = new d();

    private static b a(b bVar) {
        int c = bVar.c();
        int b2 = bVar.b();
        int min = Math.min(c, b2);
        int[] a2 = bVar.a();
        if (a2 == null) {
            throw NotFoundException.a();
        }
        int i = a2[0];
        int i2 = a2[1];
        int i3 = i;
        while (i3 < min && i2 < min && bVar.a(i3, i2)) {
            i3++;
        }
        if (i3 == min) {
            throw NotFoundException.a();
        }
        int i4 = i3 - a2[0];
        int i5 = b2 - 1;
        while (i5 >= 0 && !bVar.a(i5, i2)) {
            i5--;
        }
        if (i5 < 0) {
            throw NotFoundException.a();
        }
        int i6 = i5 + 1;
        if ((i6 - i3) % i4 != 0) {
            throw NotFoundException.a();
        }
        int i7 = ((i6 - i3) / i4) + 2;
        int i8 = i2 + i4;
        int i9 = i3 - (i4 >> 1);
        int i10 = i8 - (i4 >> 1);
        if (((i7 - 1) * i4) + i9 >= b2 || ((i7 - 1) * i4) + i10 >= c) {
            throw NotFoundException.a();
        }
        b bVar2 = new b(i7);
        for (int i11 = 0; i11 < i7; i11++) {
            int i12 = i10 + (i11 * i4);
            for (int i13 = 0; i13 < i7; i13++) {
                if (bVar.a((i13 * i4) + i9, i12)) {
                    bVar2.b(i13, i11);
                }
            }
        }
        return bVar2;
    }

    public h a(c cVar, Hashtable hashtable) {
        com.google.zxing.common.g a2;
        j[] b2;
        if (hashtable == null || !hashtable.containsKey(com.google.zxing.d.b)) {
            i a3 = new com.google.zxing.a.b.a(cVar.c()).a();
            a2 = this.b.a(a3.a());
            b2 = a3.b();
        } else {
            a2 = this.b.a(a(cVar.c()));
            b2 = f115a;
        }
        h hVar = new h(a2.b(), a2.a(), b2, com.google.zxing.a.b);
        if (a2.c() != null) {
            hVar.a(com.google.zxing.i.c, a2.c());
        }
        if (a2.d() != null) {
            hVar.a(com.google.zxing.i.d, a2.d().toString());
        }
        return hVar;
    }

    public void a() {
    }
}
