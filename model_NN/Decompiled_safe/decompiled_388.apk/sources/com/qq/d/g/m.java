package com.qq.d.g;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import com.tencent.assistant.st.STConst;
import java.io.File;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class m extends r {
    public boolean a(Map<String, String> map, boolean z, s sVar) {
        boolean z2;
        boolean z3;
        PackageManager packageManager = sVar.d().getPackageManager();
        Intent intent = new Intent();
        intent.addCategory("android.intent.category.HOME");
        intent.setAction("android.intent.action.MAIN");
        List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
        if (queryIntentActivities.size() > 0) {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            for (ResolveInfo next : queryIntentActivities) {
                sb.append(next.activityInfo.packageName + ",");
                sb2.append(new File(next.activityInfo.applicationInfo.sourceDir).getName() + ",");
            }
            String sb3 = sb.toString();
            String substring = sb3.substring(0, sb3.length() - 1);
            a(map, "launcherpackages", substring);
            String sb4 = sb2.toString();
            String substring2 = sb4.substring(0, sb4.length() - 1);
            a(map, "launcherapps", substring2);
            z2 = a(sVar.d(), map, substring, substring2, sVar);
        } else {
            a(map, "launcherpackages", STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
            a(map, "launcherapps", STConst.ST_INSTALL_FAIL_STR_UNKNOWN);
            z2 = false;
        }
        if (this.b == null) {
            return false;
        }
        r rVar = this.b;
        if (z || z2) {
            z3 = true;
        } else {
            z3 = false;
        }
        boolean a2 = rVar.a(map, z3, sVar);
        if (z2 || a2) {
            return true;
        }
        return false;
    }

    private boolean a(Context context, Map<String, String> map, String str, String str2, s sVar) {
        String str3 = Build.BRAND;
        if (str.contains("com.htc.launcher") && str3.toLowerCase().contains("htc")) {
            a(map, "rombrand", "htc");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("com.sec.android.app.twlauncher") && str3.toLowerCase().contains("samsung")) {
            a(map, "rombrand", "samsung");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("com.sonyericsson.home") && str3.toLowerCase().contains("semc")) {
            a(map, "rombrand", "sony");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("com.sonyericsson.homescreen") && str3.toLowerCase().contains("semc")) {
            a(map, "rombrand", "sony");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("huawei") && str3.toLowerCase().contains("huawei")) {
            a(map, "rombrand", "huawei");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("com.motorola.blur.home") && str3.toLowerCase().contains("moto")) {
            a(map, "rombrand", "moto");
            a(map, "romversion", sVar.e());
            return true;
        } else if (str.contains("com.android.launcher") && str3.toLowerCase().contains("google")) {
            a(map, "rombrand", "android");
            a(map, "romversion", sVar.e());
            return true;
        } else if (!str.contains("com.android.launcher") || !str2.toLowerCase().contains("oppolauncher.apk") || !str3.toLowerCase().contains("oppo")) {
            if (str.contains("com.android.launcher") || str.contains("com.android.launcher2")) {
                a(map, "rombrand2", "android");
            }
            return false;
        } else {
            a(map, "rombrand", "oppo");
            a(map, "romversion", sVar.e());
            return true;
        }
    }
}
