package com.qq.AppService;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/* compiled from: ProGuard */
public class IPCService extends Service {

    /* renamed from: a  reason: collision with root package name */
    public static IPCService f203a = null;
    public z b = null;
    public Notification c = null;
    public Notification d = null;
    public final x e = new ad(this);

    public void onCreate() {
        super.onCreate();
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public IBinder onBind(Intent intent) {
        Log.d("com.qq.connect", "onBind" + intent.toString());
        if (!intent.getStringExtra("appid").equals(AstApp.i().getPackageName())) {
            Log.d("com.qq.connect", "illeage appid");
            return null;
        }
        f203a = this;
        return this.e;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d("com.qq.connect", "onReBind" + intent.toString());
        this.b = null;
        f203a = this;
        this.c = null;
        this.d = null;
    }

    public boolean onUnbind(Intent intent) {
        super.onUnbind(intent);
        f203a = null;
        this.b = null;
        AppService.f();
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        this.b = null;
        Log.d("com.qq.connect", "IPCService onDestory");
    }
}
