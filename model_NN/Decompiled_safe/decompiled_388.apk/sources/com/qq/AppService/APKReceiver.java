package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.qq.provider.cache2.a;
import com.qq.provider.h;

/* compiled from: ProGuard */
public class APKReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null) {
            if (action.equals("android.intent.action.PACKAGE_ADDED")) {
                String substring = intent.getDataString().substring(8);
                boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
                try {
                    a.a(context, substring);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                if (h.b) {
                    h.a(1, substring, booleanExtra, false);
                }
            } else if (action.equals("android.intent.action.PACKAGE_CHANGED")) {
                String substring2 = intent.getDataString().substring(8);
                try {
                    a.a(context, substring2);
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
                if (h.b) {
                    h.a(2, substring2, false, false);
                }
            } else if (action.equals("android.intent.action.PACKAGE_DATA_CLEARED")) {
                String substring3 = intent.getDataString().substring(8);
                if (h.b) {
                    h.a(3, substring3, false, false);
                }
            } else if (action.equals("android.intent.action.PACKAGE_INSTALL")) {
                String dataString = intent.getDataString();
                if (h.b) {
                    h.a(4, dataString, false, false);
                }
            } else if (action.equals("android.intent.action.PACKAGE_REPLACED")) {
                String substring4 = intent.getDataString().substring(8);
                if (h.b) {
                    h.a(6, substring4, false, false);
                }
            } else if (action.equals("android.intent.action.PACKAGE_REMOVED")) {
                String substring5 = intent.getDataString().substring(8);
                boolean booleanExtra2 = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
                boolean booleanExtra3 = intent.getBooleanExtra("android.intent.extra.DATA_REMOVED", false);
                try {
                    a.a(context, substring5);
                } catch (Throwable th3) {
                    th3.printStackTrace();
                }
                if (h.b) {
                    h.a(5, substring5, booleanExtra2, booleanExtra3);
                }
            } else if (action.equals("android.intent.action.PACKAGE_RESTARTED")) {
                String dataString2 = intent.getDataString();
                if (h.b) {
                    h.a(7, dataString2, false, false);
                }
            }
        }
    }
}
