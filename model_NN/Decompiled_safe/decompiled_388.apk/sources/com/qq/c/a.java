package com.qq.c;

import com.qq.AppService.AstApp;
import com.qq.ndk.Native;
import com.qq.provider.n;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final File f263a = new File("/data/data/" + AstApp.i().getPackageName() + "/root/auth");
    public static final File b = new File("/data/data/" + AstApp.i().getPackageName() + "/root");
    public static final String c = ("/data/data/" + AstApp.i().getPackageName() + "/root/mask");
    public static final File d = new File("/data/data/" + AstApp.i().getPackageName() + "/root/aurora");

    public static void a() {
        Native nativeR = new Native();
        if (!b.exists()) {
            b.mkdir();
        }
        File file = new File("/data/data/" + AstApp.i().getPackageName() + "/lib/libaurora.so");
        if (!d.exists() || d.length() != file.length()) {
            try {
                n.c(file, d);
            } catch (Exception e) {
                e.printStackTrace();
            }
            nativeR.setFilePermission(d.getAbsolutePath(), 509);
        }
        if (nativeR.getFilePermission(b.getAbsolutePath()) != 775) {
            nativeR.setFilePermission(b.getAbsolutePath(), 509);
        }
        nativeR.writeAuth(Constants.STR_EMPTY + System.currentTimeMillis());
        nativeR.uncryptFile(f263a.getAbsolutePath());
    }
}
