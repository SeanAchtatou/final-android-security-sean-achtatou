package com.qq.provider;

import android.util.Log;
import com.qq.d.c;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class s extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f350a;

    private s(q qVar) {
        this.f350a = qVar;
    }

    /* synthetic */ s(q qVar, r rVar) {
        this(qVar);
    }

    public void a(int i, DataEntity dataEntity) {
        c cVar = new c(i, dataEntity);
        if (!this.f350a.a(cVar)) {
            Log.w("com.qq.connect", "onRubbishFound  isRubbishNeed false");
            return;
        }
        if (cVar.g == 2) {
            this.f350a.h.m.add(cVar);
        } else if (cVar.g == 4) {
            this.f350a.h.n.add(cVar);
        } else if (cVar.g == 1) {
            this.f350a.h.o.add(cVar);
        } else if (cVar.g == 3) {
            this.f350a.h.p.add(cVar);
        }
        h.a(cVar);
    }

    public void a() {
        this.f350a.h.i = false;
        this.f350a.h.h = false;
        h.c(1, -1);
    }

    public void b() {
        this.f350a.h.i = false;
        this.f350a.h.h = false;
        h.c(1, 0);
    }

    public void a(int i) {
        this.f350a.h.j = i;
        h.c(2, i);
    }

    public void c() {
        this.f350a.h.h = true;
        if (!(this.f350a.h == null || this.f350a.h.l == null)) {
            this.f350a.h.l.clear();
        }
        if (!(this.f350a.h == null || this.f350a.h.m == null)) {
            this.f350a.h.m.clear();
        }
        if (!(this.f350a.h == null || this.f350a.h.n == null)) {
            this.f350a.h.n.clear();
        }
        if (!(this.f350a.h == null || this.f350a.h.o == null)) {
            this.f350a.h.o.clear();
        }
        if (!(this.f350a.h == null || this.f350a.h.p == null)) {
            this.f350a.h.p.clear();
        }
        h.c(0, 0);
    }
}
