package com.qq.provider.cache2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: ProGuard */
public class CacheDBHelper extends SQLiteOpenHelper {
    public static final String APP_ICON_CACHE = "app_ico";
    public static final String DATABASE_NAME = "caches.db";
    public static final int DATABASE_VERSION = 3;

    private void createAPP_ICO_Cache(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE app_ico (_id integer primary key autoincrement,pkg text,app_name text,version text,version_code integer,flag integer,path text,size long,date long,icon blob,time long);");
    }

    public CacheDBHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 3);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        createAPP_ICO_Cache(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS app_ico");
        onCreate(sQLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS app_ico");
    }
}
