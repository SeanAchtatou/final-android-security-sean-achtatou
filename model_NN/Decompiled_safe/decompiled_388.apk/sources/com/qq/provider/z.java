package com.qq.provider;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import com.qq.AppService.SmsSentReceiver;
import com.qq.AppService.r;
import com.qq.a.a.d;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Vector;

/* compiled from: ProGuard */
class z extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ y f357a;
    private Vector<String> b = new Vector<>(20);
    private Vector<Integer> c = new Vector<>(10);

    public z(y yVar) {
        this.f357a = yVar;
    }

    public void a(String str, String str2, int i) {
        if (!r.b(str) && PhoneNumberUtils.isWellFormedSmsAddress(str) && !r.b(str2)) {
            this.b.add(str);
            this.b.add(str2);
            this.c.add(Integer.valueOf(i));
        }
    }

    public void a() {
        try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean b(String str, String str2, int i) {
        boolean z;
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> divideMessage = smsManager.divideMessage(str2);
        ArrayList arrayList = new ArrayList();
        Intent intent = new Intent(this.f357a.f356a, SmsSentReceiver.class);
        intent.setAction("ACTION_SMS_SENT" + System.currentTimeMillis());
        intent.putExtra("_id", i);
        arrayList.add(PendingIntent.getBroadcast(this.f357a.f356a, 0, intent, 0));
        ArrayList arrayList2 = new ArrayList();
        Intent intent2 = new Intent(this.f357a.f356a, SmsSentReceiver.class);
        intent2.setAction("ACTION_SMS_DELIVERY" + System.currentTimeMillis());
        intent2.putExtra("_id", i);
        arrayList2.add(PendingIntent.getBroadcast(this.f357a.f356a, 0, intent2, 0));
        if (!(str == null || divideMessage == null)) {
            try {
                smsManager.sendMultipartTextMessage(str, null, divideMessage, arrayList, arrayList2);
            } catch (Exception e) {
                e.printStackTrace();
                z = false;
            }
        }
        z = true;
        if (!z) {
            Uri withAppendedPath = Uri.withAppendedPath(d.f258a, Constants.STR_EMPTY + i);
            ContentValues contentValues = new ContentValues();
            contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 6);
            contentValues.put("status", (Integer) 128);
            this.f357a.f356a.getContentResolver().update(withAppendedPath, contentValues, null, null);
        }
        return z;
    }

    public void run() {
        super.run();
        while (this.b.size() >= 2) {
            b(this.b.get(this.b.size() - 2), this.b.get(this.b.size() - 1), this.c.get(this.c.size() - 1).intValue());
            this.b.remove(this.b.size() - 1);
            this.b.remove(this.b.size() - 2);
            this.c.remove(this.c.size() - 1);
        }
    }
}
