package com.tencent.b.d;

import com.tencent.connect.common.Constants;

public class j {
    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(Constants.STR_EMPTY);
        if (bArr == null || bArr.length <= 0) {
            return null;
        }
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() < 2) {
                sb.append(0);
            }
            sb.append(hexString);
        }
        return sb.toString().toUpperCase();
    }
}
