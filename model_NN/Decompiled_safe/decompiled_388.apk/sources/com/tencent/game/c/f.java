package com.tencent.game.c;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GftAppGiftInfo;
import com.tencent.assistant.protocol.jce.GftMydesktopOtherAppInfo;
import com.tencent.game.d.a.b;
import java.util.List;

/* compiled from: ProGuard */
class f implements CallbackHelper.Caller<b> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2644a;
    final /* synthetic */ int b;
    final /* synthetic */ List c;
    final /* synthetic */ GftAppGiftInfo d;
    final /* synthetic */ GftMydesktopOtherAppInfo e;
    final /* synthetic */ e f;

    f(e eVar, int i, int i2, List list, GftAppGiftInfo gftAppGiftInfo, GftMydesktopOtherAppInfo gftMydesktopOtherAppInfo) {
        this.f = eVar;
        this.f2644a = i;
        this.b = i2;
        this.c = list;
        this.d = gftAppGiftInfo;
        this.e = gftMydesktopOtherAppInfo;
    }

    /* renamed from: a */
    public void call(b bVar) {
        bVar.a(this.f2644a, this.b, this.c, this.d, this.e);
    }
}
