package com.tencent.game.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.smartcard.d.y;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class a extends com.tencent.assistant.smartcard.d.a {
    public List<y> e;
    public int f = 0;
    public boolean g = false;
    private byte h;
    private boolean i = false;
    private int v;
    private int w;
    private boolean x = true;
    private int y = 0;
    private String z;

    public boolean a(byte b, JceStruct jceStruct) {
        boolean z2 = false;
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.j = b;
        if (smartCardTitle != null) {
            this.h = smartCardTitle.f1527a;
            this.l = smartCardTitle.b;
            this.m = smartCardTitle.e;
            this.p = smartCardTitle.c;
            this.o = smartCardTitle.d;
            this.f = smartCardTitle.f;
        }
        this.n = smartCardPicTemplate.c;
        this.k = smartCardPicTemplate.f1514a;
        this.g = smartCardPicTemplate.m != 0;
        if (this.e == null) {
            this.e = new ArrayList();
        } else {
            this.e.clear();
        }
        if (smartCardPicTemplate.e != null) {
            Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
            while (it.hasNext()) {
                SmartCardPicDownloadNode next = it.next();
                y yVar = new y();
                yVar.b = next.b;
                yVar.f1768a = k.a(next.f1512a);
                if (yVar.f1768a != null) {
                    k.a(yVar.f1768a);
                }
                this.e.add(yVar);
            }
        }
        this.i = smartCardPicTemplate.a();
        this.v = smartCardPicTemplate.c();
        this.w = smartCardPicTemplate.d();
        if (!smartCardPicTemplate.k) {
            z2 = true;
        }
        this.x = z2;
        this.z = smartCardPicTemplate.f;
        this.y = smartCardPicTemplate.e();
        return true;
    }

    public x c() {
        if (this.v <= 0 || this.w <= 0) {
            return null;
        }
        x xVar = new x();
        xVar.f = this.k;
        xVar.e = this.j;
        xVar.f1767a = this.v;
        xVar.b = this.w;
        return xVar;
    }

    public List<SimpleAppModel> a() {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.e.size());
        for (y yVar : this.e) {
            arrayList.add(yVar.f1768a);
        }
        return arrayList;
    }

    public String d_() {
        return j() + "_" + (this.e == null ? 0 : this.e.size());
    }

    public void b() {
        if (this.x && this.e != null && this.e.size() > 0) {
            Iterator<y> it = this.e.iterator();
            while (it.hasNext()) {
                y next = it.next();
                if (!(next.f1768a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1768a.c) == null)) {
                    it.remove();
                }
            }
        }
    }

    public byte f() {
        return this.h;
    }

    public boolean h() {
        return this.i;
    }

    public int i() {
        return this.y;
    }

    public String e() {
        return this.z;
    }
}
