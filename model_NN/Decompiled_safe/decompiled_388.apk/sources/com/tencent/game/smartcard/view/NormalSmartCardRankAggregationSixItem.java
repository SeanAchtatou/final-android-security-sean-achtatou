package com.tencent.game.smartcard.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.module.k;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.game.smartcard.b.b;
import com.tencent.game.smartcard.component.NormalSmartCardAppTinyNodeWithRank;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardRankAggregationSixItem extends NormalSmartcardBaseItem {
    private RelativeLayout i;
    private TextView l;
    private TextView m;
    private LinearLayout n;
    private LinearLayout o;
    private ImageView p;
    private List<NormalSmartCardAppTinyNodeWithRank> q;
    private View.OnClickListener r;

    public NormalSmartCardRankAggregationSixItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_rank_aggregation_liutong_layout, this);
        this.l = (TextView) this.c.findViewById(R.id.card_title);
        this.m = (TextView) this.c.findViewById(R.id.more_txt);
        this.i = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.n = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        this.o = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout_line2);
        this.p = (ImageView) this.c.findViewById(R.id.divider_line);
        f();
    }

    private void f() {
        int i2 = 6;
        b bVar = (b) this.d;
        if (!TextUtils.isEmpty(bVar.l)) {
            this.l.setText(bVar.l);
            this.l.setCompoundDrawables(null, null, null, null);
            this.l.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
            this.i.setVisibility(0);
            if (!TextUtils.isEmpty(bVar.p)) {
                this.m.setText(bVar.p);
                if (this.r == null) {
                    this.r = new a(this);
                }
                this.m.setOnClickListener(this.r);
                this.m.setVisibility(0);
            } else {
                this.m.setVisibility(8);
            }
        } else {
            this.i.setVisibility(8);
        }
        List<y> list = bVar.e;
        if (list == null || list.size() == 0) {
            this.n.setVisibility(8);
            this.o.setVisibility(8);
            return;
        }
        this.n.setVisibility(0);
        if (list.size() > 3) {
            this.o.setVisibility(0);
            this.p.setVisibility(0);
        } else {
            this.o.setVisibility(8);
            this.p.setVisibility(8);
        }
        if (this.q == null) {
            g();
            int size = list.size();
            if (list.size() <= 6) {
                i2 = size;
            }
            this.q = new ArrayList(i2);
            for (int i3 = 0; i3 < i2; i3++) {
                NormalSmartCardAppTinyNodeWithRank normalSmartCardAppTinyNodeWithRank = new NormalSmartCardAppTinyNodeWithRank(this.f1693a);
                this.q.add(normalSmartCardAppTinyNodeWithRank);
                if (i3 < 3) {
                    this.n.addView(normalSmartCardAppTinyNodeWithRank, new LinearLayout.LayoutParams(0, -2, 1.0f));
                } else {
                    this.o.addView(normalSmartCardAppTinyNodeWithRank, new LinearLayout.LayoutParams(0, -2, 1.0f));
                }
                normalSmartCardAppTinyNodeWithRank.a(list.get(i3).f1768a, a(list.get(i3), i3), a(this.f1693a));
            }
            return;
        }
        int size2 = this.q.size() >= 6 ? 6 : this.q.size();
        for (int i4 = 0; i4 < size2; i4++) {
            try {
                this.q.get(i4).a(list.get(i4).f1768a, a(list.get(i4), i4), a(this.f1693a));
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    private void g() {
        this.q = null;
        this.n.removeAllViews();
    }

    private STInfoV2 a(y yVar, int i2) {
        if (yVar == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1693a, yVar.f1768a, a.a(((b) this.d).f + Constants.STR_EMPTY, i2), 100, a.a(k.d(yVar.f1768a), yVar.f1768a));
        if (buildSTInfo != null) {
            buildSTInfo.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(buildSTInfo);
        return buildSTInfo;
    }

    public void b() {
        f();
    }
}
