package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.jce.GftGetMyDesktopResponse;
import com.tencent.assistant.protocol.jce.IsplayingInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.game.e.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JceStruct f2710a;
    final /* synthetic */ p b;

    s(p pVar, JceStruct jceStruct) {
        this.b = pVar;
        this.f2710a = jceStruct;
    }

    public void run() {
        IsplayingInfo isplayingInfo;
        ArrayList<SimpleAppInfo> arrayList;
        if (this.f2710a != null) {
            isplayingInfo = ((GftGetMyDesktopResponse) this.f2710a).a();
        } else {
            isplayingInfo = null;
        }
        if (isplayingInfo != null) {
            arrayList = isplayingInfo.a();
        } else {
            arrayList = null;
        }
        a.a(arrayList);
    }
}
