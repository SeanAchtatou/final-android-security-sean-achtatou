package com.tencent.game.d;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.m;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.GftGetNavigationRequest;
import com.tencent.assistant.protocol.jce.GftGetNavigationResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.game.d.a.c;

/* compiled from: ProGuard */
public class v extends BaseEngine<c> {

    /* renamed from: a  reason: collision with root package name */
    private static v f2713a = null;
    private static volatile y b = null;
    private static final Object c = new Object();

    public static synchronized v a() {
        v vVar;
        synchronized (v.class) {
            if (f2713a == null) {
                f2713a = new v();
            }
            vVar = f2713a;
        }
        return vVar;
    }

    private v() {
        b();
        TemporaryThreadManager.get().start(new w(this));
    }

    private y c() {
        GftGetNavigationResponse gftGetNavigationResponse;
        byte[] K = m.a().K();
        if (K != null && K.length > 0) {
            try {
                gftGetNavigationResponse = (GftGetNavigationResponse) an.b(K, GftGetNavigationResponse.class);
            } catch (Exception e) {
                XLog.e("GameGetNavigationEngine", "response to navi object fail.typeid:.ex:" + e);
            }
            return y.a(gftGetNavigationResponse);
        }
        gftGetNavigationResponse = null;
        return y.a(gftGetNavigationResponse);
    }

    public y b() {
        if (b == null) {
            synchronized (c) {
                if (b == null) {
                    b = c();
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public int a(long j, int i) {
        GftGetNavigationRequest gftGetNavigationRequest = new GftGetNavigationRequest();
        gftGetNavigationRequest.f1369a = j;
        gftGetNavigationRequest.b = i;
        return send(gftGetNavigationRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GftGetNavigationResponse gftGetNavigationResponse = (GftGetNavigationResponse) jceStruct2;
        GftGetNavigationRequest gftGetNavigationRequest = (GftGetNavigationRequest) jceStruct;
        if (gftGetNavigationResponse == null || gftGetNavigationRequest == null || gftGetNavigationResponse.b == gftGetNavigationRequest.a()) {
            XLog.e("GameGetNavigationEngine", "GetNavigationEngine has null value.seq:" + i + ",request:" + jceStruct + ",response:" + gftGetNavigationResponse);
            return;
        }
        int b2 = gftGetNavigationRequest.b();
        y b3 = y.b(gftGetNavigationResponse);
        if (b3 != null) {
            byte[] a2 = an.a(gftGetNavigationResponse);
            if (a2 != null) {
                m.a().f(a2);
            }
            y yVar = b;
            if (yVar != null && b3.b != null && b3.b.size() > 0) {
                notifyDataChangedInMainThread(new x(this, i, b2, b3));
                b = yVar;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.e("GameGetNavigationEngine", "GetNavigationEngine error code:" + i2 + ".seq:" + i + ",request:" + jceStruct + ",response:" + jceStruct2);
    }
}
