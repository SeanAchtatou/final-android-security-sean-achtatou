package com.tencent.game.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.game.activity.n;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.pangu.adapter.RankNormalListAdapter;
import com.tencent.pangu.component.appdetail.ad;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankNormalListView extends RankRefreshGetMoreListView implements ITXRefreshListViewListener {
    /* access modifiers changed from: private */
    public static String[] O;
    View A;
    SwitchButton B;
    ad C;
    TextView D;
    boolean[] E = new boolean[10];
    int F = 0;
    public boolean G = false;
    protected final int H = 1;
    protected final int I = 2;
    protected final String J = "isFirstPage";
    protected final String K = "key_data";
    protected a L = new o(this);
    protected ViewInvalidateMessageHandler M = new p(this);
    private long N = 0;
    protected GameRankNormalListPage e;
    protected com.tencent.game.d.a f = null;
    protected RankNormalListAdapter u = null;
    protected s v;
    protected int w = 1;
    protected ListViewScrollListener x;
    protected com.tencent.assistantv2.activity.a y;
    LinearLayout z;

    public GameRankNormalListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSelector(new ColorDrawable(0));
    }

    public void a(ListViewScrollListener listViewScrollListener) {
        this.x = listViewScrollListener;
        setOnScrollerListener(this.x);
    }

    public void a(s sVar) {
        this.v = sVar;
    }

    public void a(com.tencent.game.d.a aVar) {
        this.f = aVar;
        this.f.register(this.L);
        setRefreshListViewListener(this);
    }

    public void b(boolean z2) {
        if (this.u == null) {
            u();
        }
        if (this.u.getCount() <= 0 || z2) {
            this.f.a(z2);
            return;
        }
        this.x.sendMessage(new ViewInvalidateMessage(2, null, this.M));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.game.component.GameRankNormalListView.a(com.tencent.game.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void */
    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (scrollState == TXScrollViewBase.ScrollState.ScrollState_FromEnd) {
            this.f.e();
        } else if (scrollState != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
        } else {
            if (this.A != null) {
                a(true, true);
                this.g.findViewById(R.id.tips).setVisibility(0);
                onTopRefreshCompleteNoAnimation();
                return;
            }
            onTopRefreshComplete();
        }
    }

    public void a(boolean z2, boolean z3) {
        if (this.A == null) {
            return;
        }
        if (!z2) {
            this.A.setClickable(false);
            if (!z3) {
                this.A.setVisibility(8);
                this.isHideInstalledAppAreaAdded = false;
                this.D.setVisibility(8);
                if (this.C != null) {
                    this.C.f3563a = false;
                }
            } else if (this.isHideInstalledAppAreaAdded) {
                this.isHideInstalledAppAreaAdded = false;
                if (this.C == null) {
                    this.C = new ad(this.A);
                }
                this.C.f3563a = true;
                this.A.postDelayed(this.C, 5);
            }
        } else if (!this.isHideInstalledAppAreaAdded) {
            if (this.C != null) {
                this.C.f3563a = false;
            }
            this.A.setVisibility(0);
            this.A.setBackgroundResource(R.drawable.v2_button_background_selector);
            this.A.setClickable(true);
            this.A.setPressed(false);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.A.getLayoutParams();
            if (layoutParams.height != getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height)) {
                layoutParams.height = getResources().getDimensionPixelSize(R.dimen.hide_installed_apps_area_height);
                this.A.setLayoutParams(layoutParams);
            }
            this.isHideInstalledAppAreaAdded = true;
            q();
        }
    }

    /* access modifiers changed from: package-private */
    public void q() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.y.G(), "08", this.y.G(), "08", 100);
        if (!this.G || !(this.y instanceof n)) {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a("08", 0);
        } else {
            sTInfoV2.slotId = com.tencent.assistantv2.st.page.a.a(((n) this.y).M(), 0);
        }
        if (!com.tencent.assistantv2.manager.a.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        l.a(sTInfoV2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.game.component.GameRankNormalListView.a(com.tencent.game.component.GameRankNormalListView, java.util.List):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView, boolean):boolean
      com.tencent.assistant.component.txscrollview.RankRefreshGetMoreListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.game.component.GameRankNormalListView.a(boolean, boolean):void */
    public void r() {
        if (this.B != null) {
            this.B.a(com.tencent.assistantv2.manager.a.a().b().c());
        }
        if (this.u != null && this.u.getCount() > 0) {
            this.u.c();
        }
        if (this.B != null && this.u != null && this.u.getCount() > 0) {
            if (!this.G) {
                if (m.a().al()) {
                    a(true, false);
                    m.a().z(false);
                }
            } else if (m.a().am()) {
                a(true, false);
                m.a().A(false);
            }
        }
    }

    public void a(com.tencent.assistantv2.activity.a aVar) {
        this.y = aVar;
    }

    public void s() {
        if (this.A == null) {
            this.z = (LinearLayout) LayoutInflater.from(getContext()).inflate((int) R.layout.v5_hide_installed_apps_area, (ViewGroup) null);
            ((ListView) this.s).addHeaderView(this.z);
            this.A = this.z.findViewById(R.id.header_container);
            this.B = (SwitchButton) this.z.findViewById(R.id.hide_btn);
            this.B.setClickable(false);
            this.D = (TextView) this.z.findViewById(R.id.tips);
            this.B.a(com.tencent.assistantv2.manager.a.a().b().c());
            if (this.G) {
                setRankHeaderPaddingBottomAdded(-this.B.getResources().getDimensionPixelSize(R.dimen.normal_list_margin_lfet));
            }
            this.A.setOnClickListener(new q(this));
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2, int i3) {
        boolean z3 = false;
        if (i2 == 0) {
            this.v.K();
            if (this.u == null) {
                u();
            }
            if (this.u.getCount() == 0) {
                this.v.e(10);
                if (z2) {
                    l.a(y(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                    return;
                }
                return;
            }
            this.u.notifyDataSetChanged();
            this.N = System.currentTimeMillis();
            if (z2) {
                l.a(y(), CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
            }
            if (i3 > 0) {
                z3 = this.f.g();
            }
            onRefreshComplete(z3, true);
        } else if (z2) {
            if (-800 == i2) {
                this.v.e(30);
            } else if (this.w > 0) {
                this.w--;
                this.f.d();
            } else if (c.a()) {
                this.v.e(20);
                return;
            } else {
                this.v.e(30);
                return;
            }
            l.a(y(), CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
        } else {
            onRefreshComplete(this.f.g(), false);
            this.v.L();
        }
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    public com.tencent.game.d.a t() {
        return this.f;
    }

    private int y() {
        if (this.e == null) {
            return 2000;
        }
        return this.e.c();
    }

    /* access modifiers changed from: protected */
    public void u() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.u = (RankNormalListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.u = (RankNormalListAdapter) ((ListView) this.s).getAdapter();
        }
        if (this.u != null && O == null) {
            O = AstApp.i().getResources().getStringArray(R.array.logo_tips3);
        }
    }

    public void recycleData() {
        super.recycleData();
        this.f.unregister(this.L);
    }

    /* access modifiers changed from: private */
    public boolean a(List<SimpleAppModel> list) {
        int i;
        if (list != null && list.size() > 0) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < 20) {
                if (i2 < list.size()) {
                    SimpleAppModel simpleAppModel = list.get(i2);
                    if (e.a(simpleAppModel.c, simpleAppModel.g)) {
                        i = i3 + 1;
                        if (i >= 3) {
                            return true;
                        }
                        i2++;
                        i3 = i;
                    }
                }
                i = i3;
                i2++;
                i3 = i;
            }
        }
        return false;
    }

    public RankNormalListAdapter v() {
        return this.u;
    }

    public boolean w() {
        return this.N == 0 || System.currentTimeMillis() - this.N > m.a().ai();
    }
}
