package com.tencent.game.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.l;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class p extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankNormalListView f2669a;

    p(GameRankNormalListView gameRankNormalListView) {
        this.f2669a = gameRankNormalListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        int i;
        if (viewInvalidateMessage.what == 1) {
            int i2 = viewInvalidateMessage.arg1;
            int i3 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            Object obj = map.get("key_data");
            if (obj != null && this.f2669a.u != null) {
                List<SimpleAppModel> list = (List) obj;
                this.f2669a.u.a(booleanValue, list);
                if (booleanValue) {
                    l.a((int) STConst.ST_PAGE_RANK_CLASSIC, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                }
                this.f2669a.a(i3, i2, booleanValue, ((List) obj).size());
                if (booleanValue) {
                    int i4 = 0;
                    int i5 = 0;
                    for (SimpleAppModel simpleAppModel : list) {
                        i4++;
                        if (i4 > 20) {
                            break;
                        }
                        if (simpleAppModel.u() == AppConst.AppState.INSTALLED) {
                            i = i5 + 1;
                        } else {
                            i = i5;
                        }
                        i5 = i;
                    }
                    if ((i5 < 5 || !m.a().aj()) && ((!this.f2669a.G && !m.a().al()) || (this.f2669a.G && !m.a().am()))) {
                        this.f2669a.isHideInstalledAppAreaAdded = false;
                        this.f2669a.A.setVisibility(8);
                    } else {
                        this.f2669a.isHideInstalledAppAreaAdded = true;
                        this.f2669a.q();
                        m.a().x(false);
                        if (this.f2669a.G) {
                            m.a().A(false);
                        } else {
                            m.a().z(false);
                        }
                    }
                    this.f2669a.u.d();
                }
            } else if (this.f2669a.u != null) {
                this.f2669a.a(i3, i2, booleanValue, 0);
            }
        } else {
            this.f2669a.v.K();
            if (this.f2669a.u != null) {
                this.f2669a.u.notifyDataSetChanged();
            }
        }
    }
}
