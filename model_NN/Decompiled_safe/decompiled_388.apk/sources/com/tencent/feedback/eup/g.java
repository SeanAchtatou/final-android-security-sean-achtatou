package com.tencent.feedback.eup;

import android.content.Context;
import android.os.Environment;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.PlugInInfo;
import com.tencent.feedback.common.b;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.f;
import com.tencent.feedback.proguard.C0005b;
import com.tencent.feedback.proguard.C0008j;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.aj;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* compiled from: ProGuard */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f2556a;
    private int b = 0;

    private void a(String str) {
        for (int i = 0; i < this.b; i++) {
            this.f2556a.append(9);
        }
        if (str != null) {
            this.f2556a.append(str).append(": ");
        }
    }

    public g(StringBuilder sb, int i) {
        this.f2556a = sb;
        this.b = i;
    }

    public static boolean a(Context context, e eVar) {
        String str;
        boolean z;
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.insertEUP() start}", new Object[0]);
        if (context == null || eVar == null) {
            com.tencent.feedback.common.g.c("rqdp{  EUPDAO.insertEUP() have null args}", new Object[0]);
            return false;
        }
        try {
            aj a2 = a(eVar);
            if (context == null || a2 == null) {
                com.tencent.feedback.common.g.a("rqdp{  AnalyticsDAO.insert() have null args}", new Object[0]);
                z = false;
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(a2);
                z = aj.a(context, arrayList);
            }
            if (!z) {
                return false;
            }
            eVar.a(a2.a());
            com.tencent.feedback.common.g.b("rqdp{  EUPDAO.insertEUP() end}", new Object[0]);
            return true;
        } catch (Throwable th) {
            if (!com.tencent.feedback.common.g.a(th)) {
                th.printStackTrace();
            }
            com.tencent.feedback.common.g.d("rqdp{  insert fail!}", new Object[0]);
            return false;
        } finally {
            str = "rqdp{  EUPDAO.insertEUP() end}";
            com.tencent.feedback.common.g.b(str, new Object[0]);
        }
    }

    public g a(boolean z, String str) {
        a(str);
        this.f2556a.append(z ? 'T' : 'F').append(10);
        return this;
    }

    public g a(byte b2, String str) {
        a(str);
        this.f2556a.append((int) b2).append(10);
        return this;
    }

    public g a(char c, String str) {
        a(str);
        this.f2556a.append(c).append(10);
        return this;
    }

    public g a(short s, String str) {
        a(str);
        this.f2556a.append((int) s).append(10);
        return this;
    }

    public g a(int i, String str) {
        a(str);
        this.f2556a.append(i).append(10);
        return this;
    }

    public g a(long j, String str) {
        a(str);
        this.f2556a.append(j).append(10);
        return this;
    }

    public g a(float f, String str) {
        a(str);
        this.f2556a.append(f).append(10);
        return this;
    }

    public g a(double d, String str) {
        a(str);
        this.f2556a.append(d).append(10);
        return this;
    }

    public static int a(Context context, List<e> list) {
        int i = 0;
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.deleteEupList() start}", new Object[0]);
        if (context == null) {
            com.tencent.feedback.common.g.c("rqdp{  deleteEupList() have null args!}", new Object[0]);
            return -1;
        } else if (list.size() <= 0) {
            return 0;
        } else {
            Long[] lArr = new Long[list.size()];
            while (true) {
                int i2 = i;
                if (i2 >= list.size()) {
                    return aj.a(context, lArr);
                }
                lArr[i2] = Long.valueOf(list.get(i2).a());
                i = i2 + 1;
            }
        }
    }

    public g a(String str, String str2) {
        a(str2);
        if (str == null) {
            this.f2556a.append("null\n");
        } else {
            this.f2556a.append(str).append(10);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(byte[] bArr, String str) {
        a(str);
        if (bArr == null) {
            this.f2556a.append("null\n");
        } else if (bArr.length == 0) {
            this.f2556a.append(bArr.length).append(", []\n");
        } else {
            this.f2556a.append(bArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (byte a2 : bArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    public static int b(Context context) {
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.querySum() start}", new Object[0]);
        if (context == null) {
            com.tencent.feedback.common.g.c("rqdp{  querySum() context is null arg}", new Object[0]);
            return -1;
        }
        return aj.a(context, new int[]{2, 1}, -1, Long.MAX_VALUE, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(short[] sArr, String str) {
        a(str);
        if (sArr == null) {
            this.f2556a.append("null\n");
        } else if (sArr.length == 0) {
            this.f2556a.append(sArr.length).append(", []\n");
        } else {
            this.f2556a.append(sArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (short a2 : sArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(int[] iArr, String str) {
        a(str);
        if (iArr == null) {
            this.f2556a.append("null\n");
        } else if (iArr.length == 0) {
            this.f2556a.append(iArr.length).append(", []\n");
        } else {
            this.f2556a.append(iArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (int a2 : iArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(long[] jArr, String str) {
        a(str);
        if (jArr == null) {
            this.f2556a.append("null\n");
        } else if (jArr.length == 0) {
            this.f2556a.append(jArr.length).append(", []\n");
        } else {
            this.f2556a.append(jArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (long a2 : jArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX INFO: finally extract failed */
    public static boolean b(Context context, List<e> list) {
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.insertOrUpdateEupList() start}", new Object[0]);
        if (context == null || list == null || list.size() <= 0) {
            com.tencent.feedback.common.g.c("rqdp{  context == null ||| list == null || list.size() <= 0,pls check}", new Object[0]);
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            for (e a2 : list) {
                aj a3 = a(a2);
                if (a3 != null) {
                    arrayList.add(a3);
                }
            }
            boolean b2 = aj.b(context, arrayList);
            com.tencent.feedback.common.g.b("rqdp{  EUPDAO.insertOrUpdateEupList() end}", new Object[0]);
            return b2;
        } catch (Throwable th) {
            com.tencent.feedback.common.g.b("rqdp{  EUPDAO.insertOrUpdateEupList() end}", new Object[0]);
            throw th;
        }
    }

    protected static boolean a(Context context, e eVar, d dVar) {
        if (dVar == null || !dVar.h()) {
            return false;
        }
        try {
            com.tencent.feedback.common.g.b("save eup logs", new Object[0]);
            e a2 = e.a(context);
            String c = a2.c();
            String B = a2.B();
            String s = eVar.s();
            Locale locale = Locale.US;
            Object[] objArr = new Object[9];
            objArr[0] = c;
            objArr[1] = B;
            objArr[2] = a2.e();
            objArr[3] = s;
            Date date = new Date(eVar.i());
            objArr[4] = date == null ? null : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(date);
            objArr[5] = eVar.e();
            objArr[6] = eVar.f();
            objArr[7] = eVar.h();
            objArr[8] = eVar.x();
            String format = String.format(locale, "#--------\npackage:%s\nversion:%s\nsdk:%s\nprocess:%s\ndate:%s\ntype:%s\nmessage:%s\nstack:\n%s\neupID:%s\n", objArr);
            if (dVar.n() != null) {
                File file = new File(dVar.n());
                if (!file.isFile()) {
                    file = file.getParentFile();
                }
                ac.a(new File(file, "euplog.txt").getAbsolutePath(), format, dVar.i());
                return true;
            } else if (!b.f(context)) {
                return false;
            } else {
                int i = dVar.i();
                com.tencent.feedback.common.g.b("rqdp{  sv sd start}", new Object[0]);
                if (format != null && format.trim().length() > 0) {
                    if (Environment.getExternalStorageState().equals("mounted")) {
                        ac.a(new File(Environment.getExternalStorageDirectory(), "/Tencent/" + b.b(context) + "/euplog.txt").getAbsolutePath(), format, i);
                    }
                    com.tencent.feedback.common.g.b("rqdp{  sv sd end}", new Object[0]);
                }
                return true;
            }
        } catch (Throwable th) {
            com.tencent.feedback.common.g.c("rqdp{  save error} %s", th.toString());
            if (com.tencent.feedback.common.g.a(th)) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(float[] fArr, String str) {
        a(str);
        if (fArr == null) {
            this.f2556a.append("null\n");
        } else if (fArr.length == 0) {
            this.f2556a.append(fArr.length).append(", []\n");
        } else {
            this.f2556a.append(fArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (float a2 : fArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(double[] dArr, String str) {
        a(str);
        if (dArr == null) {
            this.f2556a.append("null\n");
        } else if (dArr.length == 0) {
            this.f2556a.append(dArr.length).append(", []\n");
        } else {
            this.f2556a.append(dArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (double a2 : dArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    protected static aj a(e eVar) {
        int i;
        if (eVar == null) {
            return null;
        }
        try {
            aj ajVar = new aj(eVar.S() != 1 ? 1 : 2, 0, eVar.i(), ac.a(eVar));
            ajVar.b(eVar.l());
            ajVar.a(eVar.o());
            ajVar.a(eVar.q());
            ajVar.a(eVar.a());
            if (eVar.y()) {
                i = 1;
            } else {
                i = 0;
            }
            ajVar.c(i);
            return ajVar;
        } catch (Throwable th) {
            if (!com.tencent.feedback.common.g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public <K, V> g a(Map map, String str) {
        a(str);
        if (map == null) {
            this.f2556a.append("null\n");
        } else if (map.isEmpty()) {
            this.f2556a.append(map.size()).append(", {}\n");
        } else {
            this.f2556a.append(map.size()).append(", {\n");
            g gVar = new g(this.f2556a, this.b + 1);
            g gVar2 = new g(this.f2556a, this.b + 2);
            for (Map.Entry entry : map.entrySet()) {
                gVar.a('(', (String) null);
                gVar2.a(entry.getKey(), (String) null);
                gVar2.a(entry.getValue(), (String) null);
                gVar.a(')', (String) null);
            }
            a('}', (String) null);
        }
        return this;
    }

    private static void a(ArrayList<String> arrayList, Throwable th, int i, int i2, int i3) {
        while (arrayList != null && th != null && i <= i2 && arrayList.size() <= i3) {
            i++;
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    arrayList.add(stackTraceElement.toString());
                }
            }
            if (th.getCause() != null) {
                arrayList.add("cause by:");
                arrayList.add(th.getCause().getClass().getName() + ": " + th.getCause().getMessage());
                th = th.getCause();
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public <T> g a(Object[] objArr, String str) {
        a(str);
        if (objArr == null) {
            this.f2556a.append("null\n");
        } else if (objArr.length == 0) {
            this.f2556a.append(objArr.length).append(", []\n");
        } else {
            this.f2556a.append(objArr.length).append(", [\n");
            g gVar = new g(this.f2556a, this.b + 1);
            for (Object a2 : objArr) {
                gVar.a(a2, (String) null);
            }
            a(']', (String) null);
        }
        return this;
    }

    protected static String a(Throwable th, d dVar) {
        int i = 100;
        int i2 = 3;
        if (dVar != null) {
            i2 = Math.max(3, dVar.o());
            i = Math.max(100, dVar.p());
            com.tencent.feedback.common.g.b("change frame:%d  line:%d", Integer.valueOf(i2), Integer.valueOf(i));
        }
        ArrayList arrayList = new ArrayList();
        a(arrayList, th, 0, i2, i);
        if (arrayList.size() <= 0) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder(arrayList.size());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            sb.append((String) it.next()).append("\n");
        }
        return sb.toString();
    }

    public <T> g a(Collection collection, String str) {
        if (collection != null) {
            return a(collection.toArray(), str);
        }
        a(str);
        this.f2556a.append("null\t");
        return this;
    }

    public static List<e> a(Context context, int i, String str, int i2, String str2, int i3, int i4, int i5, int i6, long j, long j2, Boolean bool) {
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.queryEupRecent() start}", new Object[0]);
        if (context == null || i == 0 || ((j2 > 0 && j > j2) || (i4 > 0 && i3 > i4))) {
            com.tencent.feedback.common.g.c("rqdp{  context == null || limitNum == 0 || (timeEnd > 0 && timeStart > timeEnd) || (maxCount > 0 && miniCount > maxCount ,pls check}", new Object[0]);
            return null;
        }
        int i7 = "asc".equals(str) ? 1 : 2;
        int[] iArr = null;
        if (i2 == 2) {
            iArr = new int[]{2};
        } else if (i2 == 1) {
            iArr = new int[]{1};
        } else if (i2 < 0) {
            iArr = new int[]{1, 2};
        } else {
            com.tencent.feedback.common.g.c("rqdp{  queryEupRecent() seletedRecordType unaccepted}", new Object[0]);
        }
        List<aj> a2 = aj.a(context, iArr, -1, i7, -1, i, str2, i3, i4, i5, i6, j, j2, bool == null ? -1 : bool.booleanValue() ? 1 : 0);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<aj> it = a2.iterator();
        while (it.hasNext()) {
            aj next = it.next();
            try {
                Object b2 = ac.b(next.b());
                if (b2 != null && e.class.isInstance(b2)) {
                    e cast = e.class.cast(b2);
                    cast.a(next.a());
                    arrayList.add(cast);
                    it.remove();
                }
            } catch (Throwable th) {
                if (!com.tencent.feedback.common.g.a(th)) {
                    th.printStackTrace();
                }
                com.tencent.feedback.common.g.d("rqdp{  query have error!}", new Object[0]);
            }
        }
        if (a2.size() > 0) {
            com.tencent.feedback.common.g.b("rqdp{  there are error datas ,should be remove }" + a2.size(), new Object[0]);
            Long[] lArr = new Long[a2.size()];
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= a2.size()) {
                    break;
                }
                lArr[i9] = Long.valueOf(a2.get(i9).a());
                i8 = i9 + 1;
            }
            aj.a(context, lArr);
        }
        com.tencent.feedback.common.g.b("rqdp{  EUPDAO.queryEupRecent() end}", new Object[0]);
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
     arg types: [boolean[], java.lang.String]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g */
    public <T> g a(Object obj, String str) {
        if (obj == null) {
            this.f2556a.append("null\n");
        } else if (obj instanceof Byte) {
            a(((Byte) obj).byteValue(), str);
        } else if (obj instanceof Boolean) {
            a(((Boolean) obj).booleanValue(), str);
        } else if (obj instanceof Short) {
            a(((Short) obj).shortValue(), str);
        } else if (obj instanceof Integer) {
            a(((Integer) obj).intValue(), str);
        } else if (obj instanceof Long) {
            a(((Long) obj).longValue(), str);
        } else if (obj instanceof Float) {
            a(((Float) obj).floatValue(), str);
        } else if (obj instanceof Double) {
            a(((Double) obj).doubleValue(), str);
        } else if (obj instanceof String) {
            a((String) obj, str);
        } else if (obj instanceof Map) {
            a((Map) obj, str);
        } else if (obj instanceof List) {
            a((Collection) ((List) obj), str);
        } else if (obj instanceof C0008j) {
            a((C0008j) obj, str);
        } else if (obj instanceof byte[]) {
            a((byte[]) obj, str);
        } else if (obj instanceof boolean[]) {
            a((Object) ((boolean[]) obj), str);
        } else if (obj instanceof short[]) {
            a((short[]) obj, str);
        } else if (obj instanceof int[]) {
            a((int[]) obj, str);
        } else if (obj instanceof long[]) {
            a((long[]) obj, str);
        } else if (obj instanceof float[]) {
            a((float[]) obj, str);
        } else if (obj instanceof double[]) {
            a((double[]) obj, str);
        } else if (obj.getClass().isArray()) {
            a((Object[]) obj, str);
        } else {
            throw new C0005b("write object error: unsupport type.");
        }
        return this;
    }

    public static boolean a(Context context) {
        if (ac.c() < 0) {
            com.tencent.feedback.common.g.d("rqdp{  today fail?}", new Object[0]);
            new Date().getTime();
        }
        List<e> a2 = m.a(context).a(context, 1);
        if (a2 == null || a2.size() <= 0) {
            return false;
        }
        return true;
    }

    public static e a(Context context, String str, String str2, long j, Map<String, PlugInInfo> map, String str3, String str4, String str5, String str6, String str7, String str8, long j2, String str9, byte[] bArr) {
        e eVar = new e();
        eVar.i(str3);
        eVar.j(str4);
        eVar.b(j2 + j);
        if (str9 != null && str9.length() > 10000) {
            try {
                str9 = str9.substring(str9.length() - 10000, str9.length());
            } catch (Throwable th) {
                if (!com.tencent.feedback.common.g.a(th)) {
                    th.printStackTrace();
                }
            }
        }
        if (bArr != null && bArr.length > 10000) {
            try {
                byte[] bArr2 = new byte[10000];
                int length = bArr2.length - 1;
                int length2 = bArr.length - 1;
                while (length >= 0 && length2 >= 0) {
                    bArr2[length] = bArr[length2];
                    length--;
                    length2--;
                }
                bArr = bArr2;
            } catch (Throwable th2) {
                if (!com.tencent.feedback.common.g.a(th2)) {
                    th2.printStackTrace();
                }
            }
        }
        eVar.k(str9);
        eVar.b(bArr);
        eVar.c(str5);
        if (str7 == null || str7.trim().length() == 0) {
            str7 = "empty message";
        } else if (str7.length() > 1000) {
            str7 = str7.substring(0, 1000);
        }
        eVar.b(str7);
        eVar.a(str6);
        if (str8 == null || str8.trim().length() == 0) {
            str8 = "empty stack";
        }
        eVar.d(str8);
        eVar.a(-1.0f);
        f a2 = f.a(context);
        eVar.c(f.h());
        eVar.e(a2.j());
        eVar.d(f.f());
        eVar.i(f.m());
        eVar.j(f.n());
        com.tencent.feedback.common.g.b("avram:%d,avsd:%d,avrom:%d,avstack:%d,avheap:%d", Long.valueOf(eVar.I()), Long.valueOf(eVar.K()), Long.valueOf(eVar.J()), Long.valueOf(eVar.O()), Long.valueOf(eVar.P()));
        e a3 = e.a(context);
        eVar.f(a3.u());
        eVar.g(a3.t());
        eVar.h(a3.v());
        eVar.r(a3.B());
        eVar.s(a3.z());
        com.tencent.feedback.common.g.b("tram:%d,trom:%d,tsd:%d,v:%s,cn:%s", Long.valueOf(eVar.L()), Long.valueOf(eVar.M()), Long.valueOf(eVar.N()), eVar.Q(), eVar.R());
        eVar.e(str);
        eVar.q(str2);
        eVar.a(map);
        eVar.l(ac.d());
        eVar.m(ac.b("ro.build.fingerprint"));
        eVar.k(eVar.i() - a3.D());
        com.tencent.feedback.common.g.b("record id:%s", eVar.x());
        com.tencent.feedback.common.g.b("rom id %s", eVar.A());
        return eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g */
    public g a(C0008j jVar, String str) {
        a('{', str);
        if (jVar == null) {
            this.f2556a.append(9).append("null");
        } else {
            jVar.a(this.f2556a, this.b + 1);
        }
        a('}', (String) null);
        return this;
    }
}
