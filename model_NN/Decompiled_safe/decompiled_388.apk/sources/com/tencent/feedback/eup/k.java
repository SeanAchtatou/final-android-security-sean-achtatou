package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.feedback.b.h;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.U;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ag;

/* compiled from: ProGuard */
public final class k implements h {

    /* renamed from: a  reason: collision with root package name */
    private Context f2564a;

    public k(Context context) {
        this.f2564a = context;
    }

    public final void a(int i, byte[] bArr, boolean z) {
        d dVar;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        d j;
        if (i == 302 && bArr != null) {
            try {
                l l = l.l();
                if (l == null) {
                    g.c("rqdp{  imposiable handle response ,but no eup instance!}", new Object[0]);
                    return;
                }
                U u = new U();
                u.a(new ag(bArr));
                d p = l.p();
                if (p == null) {
                    d o = l.o();
                    if (o == null) {
                        g.b("rqdp{  init eup sStrategy by default}", new Object[0]);
                        j = new d();
                    } else {
                        g.b("rqdp{  init eup sStrategy by uStrategy}", new Object[0]);
                        j = o.clone();
                    }
                    l.a(j);
                    dVar = j;
                } else {
                    dVar = p;
                }
                if (u == null || dVar == null) {
                    z2 = false;
                } else {
                    if (dVar.e() != u.f2577a) {
                        g.b("rqdp{  is merged changed} %b", Boolean.valueOf(u.f2577a));
                        dVar.a(u.f2577a);
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    if (dVar.k() != u.c) {
                        dVar.d(u.c);
                        g.b("rqdp{ Assert enable changed: } %s", Boolean.valueOf(u.c));
                        z3 = true;
                    }
                    if (dVar.l() != u.d) {
                        dVar.f(u.d);
                        g.b("rqdp{ Assert task interval changed: } %s", Integer.valueOf(u.d));
                        z3 = true;
                    }
                    if (dVar.m() != u.e) {
                        dVar.g(u.e);
                        g.b("rqdp{ Assert limit count changed: } %s", Integer.valueOf(u.e));
                        z3 = true;
                    }
                    Object[] objArr = new Object[1];
                    objArr[0] = Integer.valueOf(u.b == null ? 0 : u.b.size());
                    g.b("crashstrategy vmap size:%d", objArr);
                    if (u.b == null || u.b.size() <= 0) {
                        z2 = z3;
                    } else {
                        for (String str : u.b.keySet()) {
                            g.b("key %s", str);
                        }
                        byte[] bArr2 = u.b.get("isAnr");
                        byte[] bArr3 = u.b.get("isBroadcast");
                        byte[] bArr4 = u.b.get("isReceiveBroadcast");
                        if (bArr2 == null || bArr2.length != 1) {
                            z4 = true;
                        } else {
                            boolean z7 = bArr2[0] != 0;
                            g.b("%b", Boolean.valueOf(z7));
                            z4 = z7;
                        }
                        if (bArr3 == null || bArr3.length != 1) {
                            z5 = false;
                        } else {
                            boolean z8 = bArr3[0] != 0;
                            g.b("%b", Boolean.valueOf(z8));
                            z5 = z8;
                        }
                        if (bArr4 == null || bArr4.length != 1) {
                            z6 = false;
                        } else {
                            boolean z9 = bArr4[0] != 0;
                            g.b("%b", Boolean.valueOf(z9));
                            z6 = z9;
                        }
                        if (dVar.q() != z4) {
                            dVar.e(z4);
                            g.b("rqdp{ anr changed: } %b", Boolean.valueOf(z4));
                            z2 = true;
                        } else {
                            z2 = z3;
                        }
                        if (dVar.r() != z5) {
                            dVar.f(z5);
                            g.b("rqdp{ broad changed: } %b", Boolean.valueOf(z5));
                            z2 = true;
                        }
                        if (dVar.s() != z6) {
                            dVar.g(z6);
                            g.b("rqdp{ receiver changed: } %b", Boolean.valueOf(z6));
                            z2 = true;
                        }
                    }
                }
                if (z2 && z) {
                    g.b("rqdp{  save eup strategy}", new Object[0]);
                    ac.a(this.f2564a, i, bArr);
                }
                g.b("rqdp{  crashStrategy}[%s]", u);
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  process crash strategy error} %s", th.toString());
            }
        }
    }
}
