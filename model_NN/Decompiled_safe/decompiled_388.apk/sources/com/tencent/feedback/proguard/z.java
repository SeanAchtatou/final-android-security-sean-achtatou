package com.tencent.feedback.proguard;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class z implements ab {

    /* renamed from: a  reason: collision with root package name */
    private String f2606a = null;

    public final void a(String str) {
        if (str != null) {
            for (int length = str.length(); length < 16; length++) {
                str = str + "0";
            }
            this.f2606a = str.substring(0, 16);
        }
    }

    public final byte[] a(byte[] bArr) {
        if (this.f2606a == null || bArr == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(((int) bArr[i]) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.f2606a.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, secretKeySpec, new IvParameterSpec(this.f2606a.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        int length2 = doFinal.length;
        for (int i2 = 0; i2 < length2; i2++) {
            stringBuffer2.append(((int) doFinal[i2]) + " ");
        }
        return doFinal;
    }

    public final byte[] b(byte[] bArr) {
        if (this.f2606a == null || bArr == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(((int) bArr[i]) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.f2606a.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, secretKeySpec, new IvParameterSpec(this.f2606a.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        int length2 = doFinal.length;
        for (int i2 = 0; i2 < length2; i2++) {
            stringBuffer2.append(((int) doFinal[i2]) + " ");
        }
        return doFinal;
    }
}
