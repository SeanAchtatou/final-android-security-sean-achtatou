package com.tencent.smtt.sdk;

import android.content.Context;
import java.lang.reflect.Field;

public class CookieSyncManager {
    private static CookieSyncManager sRef;
    private static android.webkit.CookieSyncManager sysCookieSyncManager;

    public static synchronized CookieSyncManager createInstance(Context context) {
        CookieSyncManager cookieSyncManager;
        synchronized (CookieSyncManager.class) {
            sysCookieSyncManager = android.webkit.CookieSyncManager.createInstance(context);
            if (sRef == null) {
                sRef = new CookieSyncManager(context.getApplicationContext());
            }
            cookieSyncManager = sRef;
        }
        return cookieSyncManager;
    }

    public static synchronized CookieSyncManager getInstance() {
        CookieSyncManager cookieSyncManager;
        synchronized (CookieSyncManager.class) {
            if (sRef == null) {
                throw new IllegalStateException("CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()");
            }
            cookieSyncManager = sRef;
        }
        return cookieSyncManager;
    }

    private CookieSyncManager(Context context) {
    }

    public void sync() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            sysCookieSyncManager.sync();
        } else {
            sdkEngine.wizard().cookieSyncManager_Sync();
        }
    }

    public void stopSync() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            sysCookieSyncManager.stopSync();
        } else {
            sdkEngine.wizard().cookieSyncManager_stopSync();
        }
    }

    public void startSync() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            sysCookieSyncManager.startSync();
            try {
                Field f = Class.forName("android.webkit.WebSyncManager").getDeclaredField("mSyncThread");
                f.setAccessible(true);
                ((Thread) f.get(sysCookieSyncManager)).setUncaughtExceptionHandler(new SQLiteUncaughtExceptionHandler());
            } catch (Exception e) {
            }
        } else {
            sdkEngine.wizard().cookieSyncManager_startSync();
        }
    }
}
