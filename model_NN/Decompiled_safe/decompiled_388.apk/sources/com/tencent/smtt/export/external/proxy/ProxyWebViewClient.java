package com.tencent.smtt.export.external.proxy;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.KeyEvent;
import com.tencent.smtt.export.external.interfaces.HttpAuthHandler;
import com.tencent.smtt.export.external.interfaces.IX5WebViewBase;
import com.tencent.smtt.export.external.interfaces.IX5WebViewClient;
import com.tencent.smtt.export.external.interfaces.SslError;
import com.tencent.smtt.export.external.interfaces.SslErrorHandler;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;

public abstract class ProxyWebViewClient implements IX5WebViewClient {
    protected IX5WebViewClient mWebViewClient;

    public void setWebViewClient(IX5WebViewClient webViewClient) {
        this.mWebViewClient = webViewClient;
    }

    public void doUpdateVisitedHistory(IX5WebViewBase view, String url, boolean isReload) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.doUpdateVisitedHistory(view, url, isReload);
        }
    }

    public void onContentSizeChanged(IX5WebViewBase view, int newWidth, int newHeight) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onContentSizeChanged(view, newWidth, newHeight);
        }
    }

    public void onFormResubmission(IX5WebViewBase view, Message dontResend, Message resend) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onFormResubmission(view, dontResend, resend);
        }
    }

    public void onLoadResource(IX5WebViewBase view, String url) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onLoadResource(view, url);
        }
    }

    public void onPageFinished(IX5WebViewBase view, int loadType, int backforwardLoadType, String url) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onPageFinished(view, loadType, backforwardLoadType, url);
        }
    }

    public void onPageStarted(IX5WebViewBase view, int loadType, int backforwardLoadType, String url, Bitmap favicon) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onPageStarted(view, loadType, backforwardLoadType, url, favicon);
        }
    }

    public void onReceivedError(IX5WebViewBase view, int errorCode, String description, String failingUrl) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onReceivedError(view, errorCode, description, failingUrl);
        }
    }

    public void onReceivedHttpAuthRequest(IX5WebViewBase view, HttpAuthHandler handler, String host, String realm) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onReceivedHttpAuthRequest(view, handler, host, realm);
        }
    }

    public void onReceivedSslError(IX5WebViewBase view, SslErrorHandler handler, SslError error) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onReceivedSslError(view, handler, error);
        }
    }

    public void onScaleChanged(IX5WebViewBase view, float oldScale, float newScale) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onScaleChanged(view, oldScale, newScale);
        }
    }

    public void onUnhandledKeyEvent(IX5WebViewBase view, KeyEvent event) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onUnhandledKeyEvent(view, event);
        }
    }

    public boolean shouldOverrideKeyEvent(IX5WebViewBase view, KeyEvent event) {
        return this.mWebViewClient != null && this.mWebViewClient.shouldOverrideKeyEvent(view, event);
    }

    public boolean shouldOverrideUrlLoading(IX5WebViewBase view, String url) {
        return this.mWebViewClient != null && this.mWebViewClient.shouldOverrideUrlLoading(view, url);
    }

    public void onTooManyRedirects(IX5WebViewBase view, Message cancelMsg, Message continueMsg) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onTooManyRedirects(view, cancelMsg, continueMsg);
        }
    }

    public WebResourceResponse shouldInterceptRequest(IX5WebViewBase view, String url) {
        if (this.mWebViewClient != null) {
            return this.mWebViewClient.shouldInterceptRequest(view, url);
        }
        return null;
    }

    public void onReceivedLoginRequest(IX5WebViewBase view, String realm, String account, String args) {
        if (this.mWebViewClient != null) {
            this.mWebViewClient.onReceivedLoginRequest(view, realm, account, args);
        }
    }
}
