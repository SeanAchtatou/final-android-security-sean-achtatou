package com.tencent.nucleus.manager.usagestats;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Parcel;
import android.text.TextUtils;
import android.util.Pair;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.StatOtherApp;
import com.tencent.assistant.protocol.jce.StatOtherAppList;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.manager.root.e;
import com.tencent.nucleus.manager.usagestats.UsagestatsSTManager;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class o {
    private static o j = null;

    /* renamed from: a  reason: collision with root package name */
    final Object f3072a = new Object();
    final Object b = new Object();
    Handler c = ah.a("usagestats");
    Runnable d = new p(this);
    private File e = f();
    private File f = new File(this.e, "usage-history.xml");
    private Context g = AstApp.i();
    private final c<String, m> h = new c<>();
    private final c<String, c<String, Long>> i = new c<>();
    private List<String> k = new ArrayList(100);
    private Object l = new Object();
    /* access modifiers changed from: private */
    public String m = null;

    private o() {
        i();
    }

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (j == null) {
                j = new o();
            }
            oVar = j;
        }
        return oVar;
    }

    private void i() {
        String a2 = m.a().a("usage_reported_date", Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(a2)) {
            synchronized (this.l) {
                for (String str : a2.trim().split(",")) {
                    if (str.startsWith("usage-")) {
                        this.k.add(str);
                    }
                }
                Collections.sort(this.k);
                if (this.k.size() > 100) {
                    this.k = this.k.subList(0, 100);
                }
            }
        }
    }

    private void j() {
        ArrayList<String> l2 = l();
        synchronized (this.l) {
            if (l2 != null) {
                if (l2.size() > 0) {
                    Iterator<String> it = l2.iterator();
                    while (it.hasNext()) {
                        String next = it.next();
                        if (this.k.size() >= 100) {
                            this.k.remove(this.k.size() - 1);
                        }
                        this.k.add(next);
                    }
                }
            }
            if (this.k != null && this.k.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (String str : this.k) {
                    sb.append(str + ",");
                }
                sb.deleteCharAt(sb.length() - 1);
                m.a().b("usage_reported_date", sb.toString());
            }
        }
    }

    private void a(File file) {
        XLog.i("usagestats", "Begin to parse file<" + file.getName() + "> ...");
        synchronized (this.b) {
            try {
                if (file.exists()) {
                    b(file);
                } else {
                    XLog.e("usagestats", "Usagestats file<" + file.getName() + "> not exist !");
                }
            } catch (IOException e2) {
                XLog.w("usagestats", "Error : " + e2 + " reading data from file:" + file);
            }
        }
    }

    private void b(File file) {
        Parcel c2 = c(file);
        int readInt = c2.readInt();
        if (readInt != 1008) {
            XLog.w("usagestats", "Usage stats version changed; vers = " + readInt);
        }
        int readInt2 = c2.readInt();
        while (readInt2 > 0) {
            readInt2--;
            String readString = c2.readString();
            if (!TextUtils.isEmpty(readString) && readString.trim().length() != 0) {
                XLog.v("usagestats", "Reading package #" + readInt2 + ": " + readString);
                m mVar = new m(c2);
                if (mVar != null) {
                    synchronized (this.f3072a) {
                        this.h.put(readString, mVar);
                    }
                } else {
                    continue;
                }
            }
        }
        c2.recycle();
    }

    private Parcel c(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] a2 = a(fileInputStream);
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(a2, 0, a2.length);
        obtain.setDataPosition(0);
        fileInputStream.close();
        return obtain;
    }

    private void k() {
        XLog.i("usagestats", "Begin to parse file<usagestats-history.xml> ...");
        synchronized (this.b) {
            if (this.f.exists()) {
                d(this.f);
            } else {
                XLog.e("usagestats", "History file<usagestats-history.xml> not exist !");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:68:0x00e8 A[SYNTHETIC, Splitter:B:68:0x00e8] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00f1 A[SYNTHETIC, Splitter:B:73:0x00f1] */
    /* JADX WARNING: Removed duplicated region for block: B:89:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(java.io.File r14) {
        /*
            r13 = this;
            r12 = 3
            r11 = 2
            r3 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ XmlPullParserException -> 0x00aa, IOException -> 0x00cc, all -> 0x00ed }
            r2.<init>(r14)     // Catch:{ XmlPullParserException -> 0x00aa, IOException -> 0x00cc, all -> 0x00ed }
            org.xmlpull.v1.XmlPullParser r4 = android.util.Xml.newPullParser()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            r0 = 0
            r4.setInput(r2, r0)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            int r0 = r4.getEventType()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
        L_0x0014:
            if (r0 == r11) goto L_0x001b
            int r0 = r4.next()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            goto L_0x0014
        L_0x001b:
            java.lang.String r0 = r4.getName()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            java.lang.String r1 = "usage-history"
            boolean r0 = r1.equals(r0)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r0 == 0) goto L_0x004b
            r1 = r3
        L_0x0028:
            int r5 = r4.next()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r5 != r11) goto L_0x009a
            java.lang.String r0 = r4.getName()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            int r6 = r4.getDepth()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            java.lang.String r7 = "pkg"
            boolean r7 = r7.equals(r0)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r7 == 0) goto L_0x0051
            if (r6 != r11) goto L_0x0051
            r0 = 0
            java.lang.String r1 = "name"
            java.lang.String r0 = r4.getAttributeValue(r0, r1)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
        L_0x0047:
            r1 = r0
        L_0x0048:
            r0 = 1
            if (r5 != r0) goto L_0x0028
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x00ca }
        L_0x0050:
            return
        L_0x0051:
            java.lang.String r7 = "comp"
            boolean r0 = r7.equals(r0)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r0 == 0) goto L_0x0101
            if (r6 != r12) goto L_0x0101
            if (r1 == 0) goto L_0x0101
            r0 = 0
            java.lang.String r6 = "name"
            java.lang.String r6 = r4.getAttributeValue(r0, r6)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            r0 = 0
            java.lang.String r7 = "lrt"
            java.lang.String r0 = r4.getAttributeValue(r0, r7)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r6 == 0) goto L_0x0101
            if (r0 == 0) goto L_0x0101
            long r7 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x0097 }
            java.lang.Object r9 = r13.f3072a     // Catch:{ NumberFormatException -> 0x0097 }
            monitor-enter(r9)     // Catch:{ NumberFormatException -> 0x0097 }
            com.tencent.nucleus.manager.usagestats.c<java.lang.String, com.tencent.nucleus.manager.usagestats.c<java.lang.String, java.lang.Long>> r0 = r13.i     // Catch:{ all -> 0x0094 }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x0094 }
            com.tencent.nucleus.manager.usagestats.c r0 = (com.tencent.nucleus.manager.usagestats.c) r0     // Catch:{ all -> 0x0094 }
            if (r0 != 0) goto L_0x008a
            com.tencent.nucleus.manager.usagestats.c r0 = new com.tencent.nucleus.manager.usagestats.c     // Catch:{ all -> 0x0094 }
            r0.<init>()     // Catch:{ all -> 0x0094 }
            com.tencent.nucleus.manager.usagestats.c<java.lang.String, com.tencent.nucleus.manager.usagestats.c<java.lang.String, java.lang.Long>> r10 = r13.i     // Catch:{ all -> 0x0094 }
            r10.put(r1, r0)     // Catch:{ all -> 0x0094 }
        L_0x008a:
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0094 }
            r0.put(r6, r7)     // Catch:{ all -> 0x0094 }
            monitor-exit(r9)     // Catch:{ all -> 0x0094 }
            r0 = r1
            goto L_0x0047
        L_0x0094:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0094 }
            throw r0     // Catch:{ NumberFormatException -> 0x0097 }
        L_0x0097:
            r0 = move-exception
            r0 = r1
            goto L_0x0047
        L_0x009a:
            if (r5 != r12) goto L_0x0048
            java.lang.String r0 = "pkg"
            java.lang.String r6 = r4.getName()     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            boolean r0 = r0.equals(r6)     // Catch:{ XmlPullParserException -> 0x00fe, IOException -> 0x00fc }
            if (r0 == 0) goto L_0x0048
            r1 = r3
            goto L_0x0048
        L_0x00aa:
            r0 = move-exception
            r1 = r3
        L_0x00ac:
            java.lang.String r2 = "usagestats"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f9 }
            r3.<init>()     // Catch:{ all -> 0x00f9 }
            java.lang.String r4 = "Error reading history stats: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00f9 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f9 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f9 }
            com.tencent.assistant.utils.XLog.w(r2, r0)     // Catch:{ all -> 0x00f9 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x00ca }
            goto L_0x0050
        L_0x00ca:
            r0 = move-exception
            goto L_0x0050
        L_0x00cc:
            r0 = move-exception
            r2 = r3
        L_0x00ce:
            java.lang.String r1 = "usagestats"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f7 }
            r3.<init>()     // Catch:{ all -> 0x00f7 }
            java.lang.String r4 = "Error reading history stats: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00f7 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f7 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f7 }
            com.tencent.assistant.utils.XLog.w(r1, r0)     // Catch:{ all -> 0x00f7 }
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x00ca }
            goto L_0x0050
        L_0x00ed:
            r0 = move-exception
            r2 = r3
        L_0x00ef:
            if (r2 == 0) goto L_0x00f4
            r2.close()     // Catch:{ IOException -> 0x00f5 }
        L_0x00f4:
            throw r0
        L_0x00f5:
            r1 = move-exception
            goto L_0x00f4
        L_0x00f7:
            r0 = move-exception
            goto L_0x00ef
        L_0x00f9:
            r0 = move-exception
            r2 = r1
            goto L_0x00ef
        L_0x00fc:
            r0 = move-exception
            goto L_0x00ce
        L_0x00fe:
            r0 = move-exception
            r1 = r2
            goto L_0x00ac
        L_0x0101:
            r0 = r1
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.usagestats.o.d(java.io.File):void");
    }

    private ArrayList<String> l() {
        String[] list = this.e.list();
        if (list == null) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        int e2 = e();
        for (String str : list) {
            if (str.startsWith("usage-") && !str.equals("usage-history.xml")) {
                if (str.endsWith(".bak")) {
                    new File(this.e, str).delete();
                } else {
                    try {
                        if (Integer.parseInt(str.substring("usage-".length())) == e2) {
                            continue;
                        }
                    } catch (Exception e3) {
                    }
                    synchronized (this.l) {
                        if (this.k == null || !this.k.contains(str)) {
                            arrayList.add(str);
                        } else {
                            XLog.d("usagestats", "getUsageStatsFileListFLOCK has reproted file <" + str + "> before !!!");
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public List<PkgUsageStats> b() {
        ArrayList arrayList;
        List<PackageInfo> installedPackages = this.g.getPackageManager().getInstalledPackages(0);
        if (installedPackages == null || installedPackages.size() <= 0) {
            return null;
        }
        synchronized (this.f3072a) {
            arrayList = new ArrayList();
            for (PackageInfo next : installedPackages) {
                m mVar = this.h.get(next.packageName);
                if (mVar != null) {
                    long j2 = mVar.c;
                    arrayList.add(new PkgUsageStats(next.packageName, mVar.b, j2, new HashMap()));
                }
            }
        }
        return arrayList;
    }

    static byte[] a(FileInputStream fileInputStream) {
        byte[] bArr = new byte[fileInputStream.available()];
        int i2 = 0;
        while (true) {
            int read = fileInputStream.read(bArr, i2, bArr.length - i2);
            if (read <= 0) {
                return bArr;
            }
            i2 += read;
            int available = fileInputStream.available();
            if (available > bArr.length - i2) {
                byte[] bArr2 = new byte[(available + i2)];
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                bArr = bArr2;
            }
        }
    }

    private void m() {
        int size;
        XLog.i("usagestats", "checkFileLimitFLOCK");
        ArrayList<String> l2 = l();
        if (l2 != null && (size = l2.size()) > 5) {
            Collections.sort(l2);
            int i2 = size - 5;
            for (int i3 = 0; i3 < i2; i3++) {
                String str = l2.get(i3);
                File file = new File(this.e, str);
                XLog.i("usagestats", "Deleting usage file : " + str);
                file.delete();
            }
        }
    }

    private boolean b(boolean z) {
        XLog.i("usagestats", "dumpUsageStatsFile");
        if (z) {
            return o();
        }
        return n();
    }

    private boolean n() {
        String b2;
        String[] strArr;
        boolean z;
        XLog.i("usagestats", "dumpUsageStatsFileWithTempRoot");
        if (!e.a().c()) {
            XLog.i("usagestats", "dumpUsageStatsFileWithTempRoot hasRoot = false");
            return false;
        }
        int i2 = 0;
        do {
            XLog.d("usagestats", "retryTime : " + i2);
            b2 = e.a().b("ls /data/system/usagestats");
            XLog.d("usagestats", "ret : " + b2);
            i2++;
            if (b2 == null || b2.contains("failed") || b2.contains("usage")) {
                if (b2 != null || b2.contains("failed")) {
                    strArr = null;
                } else {
                    strArr = b2.trim().split("\n");
                }
            }
        } while (i2 < 3);
        if (b2 != null) {
        }
        strArr = null;
        if (strArr == null || strArr.length <= 0) {
            XLog.e("usagestats", "<UsagestatsSTManager> dumpUsageStatsFileWithTempRoot ls result is empty !!!");
            UsagestatsSTManager.a().a("app_usage_r_get_data_fail_reason", "lsempty", UsagestatsSTManager.ReportScene.startup, UsagestatsSTManager.ReportType.timely);
            return false;
        }
        boolean z2 = true;
        for (String str : strArr) {
            if (!TextUtils.isEmpty(str) && str.trim().length() != 0) {
                File file = new File(this.e, str);
                if (!file.exists() || file.length() <= 0 || str.equals("usage-history.xml")) {
                    String absolutePath = file.getAbsolutePath();
                    int i3 = 0;
                    while (true) {
                        if (!b(str, absolutePath)) {
                            if (!a(str, absolutePath)) {
                                if (file.exists()) {
                                    file.delete();
                                }
                                XLog.d("usagestats", "dumpUsageStatsFileWithPermRoot copy file<" + str + "> failed, retry times = " + (i3 + 1));
                                i3++;
                                if (i3 >= 3) {
                                    z = z2;
                                    break;
                                }
                            } else {
                                XLog.d("usagestats", "dumpUsageStatsFileWithTempRoot copy(dd) file<" + str + "> success !");
                                z = false;
                                break;
                            }
                        } else {
                            XLog.d("usagestats", "dumpUsageStatsFileWithTempRoot copy(cat) file<" + str + "> success !");
                            z = false;
                            break;
                        }
                    }
                    e.a().b("chmod 755 " + absolutePath);
                    z2 = z;
                } else {
                    XLog.d("usagestats", "dumpUsageStatsFileWithPermRoot file<" + str + "> already exist !");
                    z2 = false;
                }
            }
        }
        if (!z2) {
            return true;
        }
        XLog.e("usagestats", "<UsagestatsSTManager> dumpUsageStatsFileWithTempRoot copy file failed !!!");
        UsagestatsSTManager.a().a("app_usage_r_get_data_fail_reason", "copyfail", UsagestatsSTManager.ReportScene.startup, UsagestatsSTManager.ReportType.timely);
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x0115 A[SYNTHETIC, Splitter:B:40:0x0115] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x011a A[SYNTHETIC, Splitter:B:43:0x011a] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0170 A[SYNTHETIC, Splitter:B:56:0x0170] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0175 A[SYNTHETIC, Splitter:B:59:0x0175] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:81:0x01e6=Splitter:B:81:0x01e6, B:45:0x011d=Splitter:B:45:0x011d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean o() {
        /*
            r14 = this;
            java.lang.String r0 = "usagestats"
            java.lang.String r1 = "dumpUsageStatsFileWithPermRoot"
            com.tencent.assistant.utils.XLog.i(r0, r1)
            java.lang.Process r4 = c()
            if (r4 != 0) goto L_0x000f
            r0 = 0
        L_0x000e:
            return r0
        L_0x000f:
            r1 = 0
            r2 = 0
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x021d, all -> 0x0210 }
            java.io.OutputStream r0 = r4.getOutputStream()     // Catch:{ Exception -> 0x021d, all -> 0x0210 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x021d, all -> 0x0210 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0223, all -> 0x0215 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0223, all -> 0x0215 }
            java.io.InputStream r5 = r4.getInputStream()     // Catch:{ Exception -> 0x0223, all -> 0x0215 }
            java.lang.String r6 = "UTF-8"
            r0.<init>(r5, r6)     // Catch:{ Exception -> 0x0223, all -> 0x0215 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0223, all -> 0x0215 }
            r0 = 512(0x200, float:7.175E-43)
            char[] r2 = new char[r0]     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r0 = 0
        L_0x002f:
            java.lang.String r5 = "usagestats"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r6.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = "retryTime : "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r5, r6)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r5 = "ls /data/system/usagestats\n"
            byte[] r5 = r5.getBytes()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r3.write(r5)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r3.flush()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            int r0 = r0 + 1
            int r5 = r1.read(r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r6 = 1
            if (r5 > r6) goto L_0x005f
            r5 = 3
            if (r0 < r5) goto L_0x002f
        L_0x005f:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r0.<init>(r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r5 = "\n"
            java.lang.String[] r5 = r0.split(r5)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r0 = "usagestats"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r6.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = "usageFiles: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r2 = r6.append(r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r0, r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r5 == 0) goto L_0x01d5
            int r0 = r5.length     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r0 <= 0) goto L_0x01d5
            int r6 = r5.length     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r0 = 0
            r2 = r0
        L_0x0096:
            if (r2 >= r6) goto L_0x01dc
            r7 = r5[r2]     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            boolean r0 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r0 != 0) goto L_0x00aa
            java.lang.String r0 = r7.trim()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            int r0 = r0.length()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r0 != 0) goto L_0x00cc
        L_0x00aa:
            java.lang.String r0 = "usagestats"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r8.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r9 = "fileName:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r8 = ", continue"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r0, r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
        L_0x00c8:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0096
        L_0x00cc:
            java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.io.File r0 = r14.e     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r8.<init>(r0, r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            boolean r0 = r8.exists()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r0 == 0) goto L_0x0123
            long r9 = r8.length()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r11 = 0
            int r0 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x0123
            java.lang.String r0 = "usage-history.xml"
            boolean r0 = r7.equals(r0)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r0 != 0) goto L_0x0123
            java.lang.String r0 = "usagestats"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r8.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r9 = "dumpUsageStatsFileWithPermRoot file<"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r8 = "> already exist !"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r0, r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            goto L_0x00c8
        L_0x010a:
            r0 = move-exception
            r2 = r3
        L_0x010c:
            java.lang.String r3 = "usagestats"
            java.lang.String r5 = "dumpUsageStatsFileWithPermRoot"
            com.tencent.assistant.utils.XLog.e(r3, r5, r0)     // Catch:{ all -> 0x0219 }
            if (r2 == 0) goto L_0x0118
            r2.close()     // Catch:{ IOException -> 0x01fa }
        L_0x0118:
            if (r1 == 0) goto L_0x011d
            r1.close()     // Catch:{ IOException -> 0x0200 }
        L_0x011d:
            r4.destroy()     // Catch:{ Exception -> 0x01eb }
        L_0x0120:
            r0 = 1
            goto L_0x000e
        L_0x0123:
            java.lang.String r9 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r0 = 0
        L_0x0128:
            boolean r10 = b(r3, r1, r7, r9)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r10 == 0) goto L_0x017c
            java.lang.String r0 = "usagestats"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r8.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r10 = "dumpUsageStatsFileWithPermRoot copy(cat) file<"
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r8 = "> success !"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r0, r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
        L_0x014c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r0.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = "chmod 755 "
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r3.writeBytes(r0)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r3.flush()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            goto L_0x00c8
        L_0x016d:
            r0 = move-exception
        L_0x016e:
            if (r3 == 0) goto L_0x0173
            r3.close()     // Catch:{ IOException -> 0x01ee }
        L_0x0173:
            if (r1 == 0) goto L_0x0178
            r1.close()     // Catch:{ IOException -> 0x01f3 }
        L_0x0178:
            r4.destroy()     // Catch:{ Exception -> 0x01f8 }
        L_0x017b:
            throw r0
        L_0x017c:
            boolean r10 = a(r3, r1, r7, r9)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r10 == 0) goto L_0x01a1
            java.lang.String r0 = "usagestats"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r8.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r10 = "dumpUsageStatsFileWithPermRoot copy(dd) file<"
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r8 = "> success !"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r0, r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            goto L_0x014c
        L_0x01a1:
            boolean r10 = r8.exists()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            if (r10 == 0) goto L_0x01aa
            r8.delete()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
        L_0x01aa:
            java.lang.String r10 = "usagestats"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            r11.<init>()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r12 = "dumpUsageStatsFileWithPermRoot copy file<"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.StringBuilder r11 = r11.append(r7)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r12 = "> failed, retry times = "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            int r12 = r0 + 1
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            com.tencent.assistant.utils.XLog.d(r10, r11)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
            int r0 = r0 + 1
            r10 = 3
            if (r0 < r10) goto L_0x0128
            goto L_0x014c
        L_0x01d5:
            java.lang.String r0 = "usagestats"
            java.lang.String r2 = "ls usageFiles is empty"
            com.tencent.assistant.utils.XLog.d(r0, r2)     // Catch:{ Exception -> 0x010a, all -> 0x016d }
        L_0x01dc:
            if (r3 == 0) goto L_0x01e1
            r3.close()     // Catch:{ IOException -> 0x0206 }
        L_0x01e1:
            if (r1 == 0) goto L_0x01e6
            r1.close()     // Catch:{ IOException -> 0x020b }
        L_0x01e6:
            r4.destroy()     // Catch:{ Exception -> 0x01eb }
            goto L_0x0120
        L_0x01eb:
            r0 = move-exception
            goto L_0x0120
        L_0x01ee:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0173
        L_0x01f3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0178
        L_0x01f8:
            r1 = move-exception
            goto L_0x017b
        L_0x01fa:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0118
        L_0x0200:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x011d
        L_0x0206:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01e1
        L_0x020b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x01e6
        L_0x0210:
            r0 = move-exception
            r3 = r1
            r1 = r2
            goto L_0x016e
        L_0x0215:
            r0 = move-exception
            r1 = r2
            goto L_0x016e
        L_0x0219:
            r0 = move-exception
            r3 = r2
            goto L_0x016e
        L_0x021d:
            r0 = move-exception
            r13 = r2
            r2 = r1
            r1 = r13
            goto L_0x010c
        L_0x0223:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x010c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.nucleus.manager.usagestats.o.o():boolean");
    }

    public static Process c() {
        ProcessBuilder processBuilder = new ProcessBuilder("su");
        processBuilder.redirectErrorStream(true);
        try {
            return processBuilder.start();
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static boolean a(String str, String str2) {
        XLog.i("usagestats", "doFileCpyBydd");
        Pair<Integer, String> a2 = e.a().a(c("/data/system/usagestats/" + str, str2));
        if (a2 == null || ((Integer) a2.first).intValue() != 0) {
            return false;
        }
        String[] split = ((String) a2.second).split("\n");
        XLog.d("usagestats", "doFileCpyBydd " + split[split.length - 1]);
        File file = new File(str2);
        if (!file.exists() || file.length() <= 0) {
            return false;
        }
        return true;
    }

    private static boolean b(String str, String str2) {
        XLog.i("usagestats", "doFileCpyBycat");
        Pair<Integer, String> a2 = e.a().a(d("/data/system/usagestats/" + str, str2));
        if (a2 == null || ((Integer) a2.first).intValue() != 0) {
            return false;
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        File file = new File(str2);
        if (!file.exists() || file.length() <= 0) {
            return false;
        }
        return true;
    }

    private static boolean a(DataOutputStream dataOutputStream, BufferedReader bufferedReader, String str, String str2) {
        XLog.i("usagestats", "doFileCpyBydd");
        dataOutputStream.write((c("/data/system/usagestats/" + str, str2) + "\n").getBytes());
        dataOutputStream.flush();
        File file = new File(str2);
        return file.exists() && file.length() > 0;
    }

    private static boolean b(DataOutputStream dataOutputStream, BufferedReader bufferedReader, String str, String str2) {
        XLog.i("usagestats", "doFileCpyBycat");
        dataOutputStream.write((d("/data/system/usagestats/" + str, str2) + "\n").getBytes());
        dataOutputStream.flush();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        File file = new File(str2);
        if (!file.exists() || file.length() <= 0) {
            return false;
        }
        return true;
    }

    private static String c(String str, String str2) {
        return "dd if=" + str + " of=" + str2;
    }

    private static String d(String str, String str2) {
        return "cat " + str + " > " + str2;
    }

    private ArrayList<StatOtherApp> p() {
        XLog.d("usagestats", "parseUsageStatsFile");
        ArrayList<StatOtherApp> arrayList = new ArrayList<>();
        ArrayList<String> l2 = l();
        if (l2 == null || l2.size() <= 0) {
            XLog.e("usagestats", "<UsagestatsSTManager> parseUsageStatsFile getUsageStatsFileList is empty !!!");
            UsagestatsSTManager.a().a("app_usage_r_get_data_fail_reason", "filelistempty", UsagestatsSTManager.ReportScene.startup, UsagestatsSTManager.ReportType.timely);
            return arrayList;
        }
        k();
        Iterator<String> it = l2.iterator();
        while (it.hasNext()) {
            String next = it.next();
            synchronized (this.f3072a) {
                this.h.clear();
            }
            int i2 = 0;
            try {
                i2 = Integer.parseInt(next.substring("usage-".length()));
            } catch (Exception e2) {
            }
            if (i2 == 0) {
                XLog.e("usagestats", "parseUsageStatsFile usagestats file error :" + next);
            } else {
                a(new File(this.e, next));
                List<PkgUsageStats> b2 = b();
                if (b2 != null) {
                    for (PkgUsageStats next2 : b2) {
                        StatOtherApp statOtherApp = new StatOtherApp();
                        statOtherApp.i = i2;
                        statOtherApp.f1556a = next2.f3056a;
                        statOtherApp.j = next2.b;
                        statOtherApp.k = next2.c;
                        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next2.f3056a);
                        if (installedApkInfo != null) {
                            statOtherApp.b(installedApkInfo.signature);
                            statOtherApp.b(installedApkInfo.mInstallDate);
                            statOtherApp.c(installedApkInfo.mLastModified);
                            statOtherApp.a((int) installedApkInfo.getAppType());
                            statOtherApp.a(installedApkInfo.mVersionName);
                            statOtherApp.a((long) installedApkInfo.mVersionCode);
                        }
                        arrayList.add(statOtherApp);
                    }
                }
            }
        }
        if (arrayList.size() == 0) {
            XLog.e("usagestats", "<UsagestatsSTManager> parseUsageStatsFile empty !!!");
            UsagestatsSTManager.a().a("app_usage_r_get_data_fail_reason", "parsefail", UsagestatsSTManager.ReportScene.startup, UsagestatsSTManager.ReportType.timely);
        }
        return arrayList;
    }

    public StatOtherAppList a(boolean z) {
        ArrayList<StatOtherApp> arrayList;
        XLog.d("usagestats", "getStatOtherAppList");
        m();
        q();
        this.m = "dump";
        boolean b2 = b(z);
        this.m = "parse";
        if (b2) {
            arrayList = p();
        } else {
            arrayList = new ArrayList<>();
        }
        if (arrayList.size() > 0) {
            j();
        }
        r();
        if (Global.isDev()) {
            XLog.d("usagestats", "getStatOtherAppList size = " + arrayList.size());
            Iterator<StatOtherApp> it = arrayList.iterator();
            while (it.hasNext()) {
                StatOtherApp next = it.next();
                System.out.println(next.i + " | " + next.f1556a + " | " + next.j + " | " + next.k);
            }
        }
        StatOtherAppList statOtherAppList = new StatOtherAppList();
        statOtherAppList.f1557a = arrayList;
        statOtherAppList.b = arrayList.size();
        return statOtherAppList;
    }

    private void q() {
        this.m = null;
        this.c.postDelayed(this.d, 90000);
    }

    private void r() {
        if (this.c != null && this.d != null) {
            this.m = null;
            this.c.removeCallbacks(this.d);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    public static int d() {
        long a2 = m.a().a("usage_stats_last_root_report_date", 0L);
        if (a2 != 0) {
            try {
                return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date(a2)));
            } catch (NumberFormatException e2) {
            }
        }
        return 0;
    }

    public static void a(long j2) {
        m.a().b("usage_stats_last_root_report_date", Long.valueOf(j2));
    }

    public static int e() {
        try {
            return Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date()));
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    public File f() {
        File file = new File(this.g.getFilesDir(), "usagestats");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    public void g() {
        m.a().b("app_usage_last_collect_task_time", Long.valueOf(System.currentTimeMillis()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private synchronized boolean s() {
        long a2;
        a2 = m.a().a("app_usage_last_collect_task_time", 0L);
        return a2 == 0 || Math.abs(System.currentTimeMillis() - a2) >= 1800000;
    }

    public void a(UsagestatsSTManager.ReportScene reportScene) {
        if (s()) {
            List<String> a2 = com.tencent.assistant.utils.e.a(AstApp.i());
            if (a2 == null) {
                UsagestatsSTManager.a().a("app_usage_n_collect_all", "fail", reportScene, UsagestatsSTManager.ReportType.normal);
                return;
            }
            if (a2.size() > 9) {
                a2 = a2.subList(0, 9);
            }
            StatOtherAppList statOtherAppList = new StatOtherAppList();
            ArrayList<StatOtherApp> arrayList = new ArrayList<>();
            for (String next : a2) {
                StatOtherApp statOtherApp = new StatOtherApp();
                statOtherApp.f1556a = next;
                statOtherApp.i = e();
                statOtherApp.e = h.a();
                LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next);
                if (installedApkInfo != null) {
                    statOtherApp.c = (long) installedApkInfo.mVersionCode;
                    statOtherApp.b = installedApkInfo.mVersionName;
                    statOtherApp.d = installedApkInfo.getAppType();
                    statOtherApp.f = installedApkInfo.signature;
                    statOtherApp.g = installedApkInfo.mInstallDate;
                    statOtherApp.h = installedApkInfo.mLastModified;
                }
                arrayList.add(statOtherApp);
            }
            statOtherAppList.f1557a = arrayList;
            statOtherAppList.b = arrayList.size();
            z.a().a((byte) 17, an.a(statOtherAppList));
            UsagestatsSTManager.a().a("app_usage_n_collect_all", "success", reportScene, UsagestatsSTManager.ReportType.normal);
            g();
        }
    }

    public StatOtherAppList h() {
        StatOtherAppList statOtherAppList = new StatOtherAppList();
        List<String> a2 = com.tencent.assistant.utils.e.a(AstApp.i());
        if (a2 != null) {
            if (a2.size() > 9) {
                a2 = a2.subList(0, 9);
            }
            ArrayList<StatOtherApp> arrayList = new ArrayList<>();
            for (String next : a2) {
                StatOtherApp statOtherApp = new StatOtherApp();
                statOtherApp.f1556a = next;
                statOtherApp.i = e();
                statOtherApp.e = h.a();
                LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next);
                if (installedApkInfo != null) {
                    statOtherApp.c = (long) installedApkInfo.mVersionCode;
                    statOtherApp.b = installedApkInfo.mVersionName;
                    statOtherApp.d = installedApkInfo.getAppType();
                    statOtherApp.f = installedApkInfo.signature;
                    statOtherApp.g = installedApkInfo.mInstallDate;
                    statOtherApp.h = installedApkInfo.mLastModified;
                }
                arrayList.add(statOtherApp);
            }
            statOtherAppList.f1557a = arrayList;
            statOtherAppList.b = arrayList.size();
        }
        return statOtherAppList;
    }
}
