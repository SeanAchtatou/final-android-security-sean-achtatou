package com.tencent.nucleus.manager.spaceclean;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;

/* compiled from: ProGuard */
class aa extends ListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RubbishResultListView f3014a;

    aa(RubbishResultListView rubbishResultListView) {
        this.f3014a = rubbishResultListView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.f3014a.i == null || this.f3014a.h == null) {
            RelativeLayout unused = this.f3014a.i = (RelativeLayout) this.f3014a.g.findViewById(R.id.pop_bar);
            this.f3014a.i.setOnClickListener(new ab(this));
            return;
        }
        if (i == 0) {
            this.f3014a.i.setVisibility(8);
        }
        int pointToPosition = this.f3014a.h.pointToPosition(0, this.f3014a.b() - 10);
        int pointToPosition2 = this.f3014a.h.pointToPosition(0, 0);
        long j = 0;
        if (pointToPosition2 != -1) {
            long expandableListPosition = this.f3014a.h.getExpandableListPosition(pointToPosition2);
            int packedPositionChild = ExpandableListView.getPackedPositionChild(expandableListPosition);
            int packedPositionGroup = ExpandableListView.getPackedPositionGroup(expandableListPosition);
            if (packedPositionChild == -1) {
                View expandChildAt = this.f3014a.h.getExpandChildAt(pointToPosition2 - this.f3014a.h.getFirstVisiblePosition());
                if (expandChildAt == null) {
                    int unused2 = this.f3014a.d = 100;
                } else {
                    int unused3 = this.f3014a.d = expandChildAt.getHeight();
                }
            }
            if (this.f3014a.d != 0) {
                if (this.f3014a.e > 0) {
                    int unused4 = this.f3014a.c = packedPositionGroup;
                    String str = (String) this.f3014a.f.getGroup(packedPositionGroup);
                    if (!TextUtils.isEmpty(str)) {
                        this.f3014a.j.setText(str);
                        this.f3014a.i.setVisibility(0);
                    }
                }
                if (this.f3014a.e == 0) {
                    this.f3014a.i.setVisibility(8);
                }
                j = expandableListPosition;
            } else {
                return;
            }
        }
        if (this.f3014a.c == -1) {
            return;
        }
        if (j == 0 && (pointToPosition == 0 || pointToPosition == -1)) {
            this.f3014a.i.setVisibility(8);
            return;
        }
        int d = this.f3014a.b();
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.f3014a.i.getLayoutParams();
        marginLayoutParams.topMargin = -(this.f3014a.d - d);
        this.f3014a.i.setLayoutParams(marginLayoutParams);
    }
}
