package com.tencent.nucleus.manager.spaceclean;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ProGuard */
public class BigFileFilter implements FileFilter {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f3004a = {"jpg", "jpeg", "bmp", "png"};
    private static final String[] b = {"mp4", "mkv", "flv", "avi", "rmvb", "rm", "3gp"};
    private static final String[] c = {"mp3", "flac", "ape"};
    private static final String[] d = {"doc", "docx", "ppt", "pptx", "xls", "xlsx", "wps", "dps", "et", "rtf", "pdf"};
    private static final String[] e = {"zip", "rar"};
    private Set<String> f;

    /* compiled from: ProGuard */
    public enum FileType {
        IMAGE,
        VIDEO,
        MUSIC,
        DOC,
        ZIP
    }

    public BigFileFilter() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(b));
        arrayList.addAll(Arrays.asList(c));
        arrayList.addAll(Arrays.asList(d));
        arrayList.addAll(Arrays.asList(e));
        this.f = new HashSet(arrayList);
    }

    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf != -1 && lastIndexOf + 1 < name.length() - 1) {
            if ((!this.f.contains(name.substring(lastIndexOf + 1).toLowerCase()) || file.length() < 1048576) && file.length() < 10485760) {
                return false;
            }
            return true;
        } else if (file.length() >= 10485760) {
            return true;
        }
        return false;
    }
}
