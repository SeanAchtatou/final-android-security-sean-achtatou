package com.tencent.nucleus.manager.backgroundscan;

import android.content.Intent;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f2824a;
    final /* synthetic */ a b;

    c(a aVar, Intent intent) {
        this.b = aVar;
        this.f2824a = intent;
    }

    public void run() {
        byte byteExtra = this.f2824a.getByteExtra(SocialConstants.PARAM_TYPE, (byte) 0);
        if (byteExtra > 0) {
            d.b().a(byteExtra, 2);
            d.b().a();
            o.a().a("b_new_scan_push_cancel", byteExtra);
        }
    }
}
