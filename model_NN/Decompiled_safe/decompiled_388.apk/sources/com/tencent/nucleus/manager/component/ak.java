package com.tencent.nucleus.manager.component;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/* compiled from: ProGuard */
class ak extends ClickableSpan implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f2857a;
    private final View.OnClickListener b;

    public ak(TxManagerCommContainView txManagerCommContainView, View.OnClickListener onClickListener) {
        this.f2857a = txManagerCommContainView;
        this.b = onClickListener;
    }

    public void onClick(View view) {
        this.b.onClick(view);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setUnderlineText(false);
    }
}
