package com.tencent.nucleus.manager.about;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistantv2.model.ItemElement;

/* compiled from: ProGuard */
class e implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutActivity f2744a;

    e(AboutActivity aboutActivity) {
        this.f2744a = aboutActivity;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ItemElement itemElement = (ItemElement) this.f2744a.z.getItem(i);
        if (itemElement != null) {
            this.f2744a.a(itemElement);
        }
    }
}
