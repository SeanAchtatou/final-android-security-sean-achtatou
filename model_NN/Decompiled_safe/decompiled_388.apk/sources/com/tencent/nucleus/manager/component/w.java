package com.tencent.nucleus.manager.component;

/* compiled from: ProGuard */
class w implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2877a;
    final /* synthetic */ long b;
    final /* synthetic */ ScaningProgressView c;

    w(ScaningProgressView scaningProgressView, int i, long j) {
        this.c = scaningProgressView;
        this.f2877a = i;
        this.b = j;
    }

    public void run() {
        this.c.e.a(this.f2877a);
        this.c.e.b((((double) this.b) * 1.0d) / 1024.0d);
    }
}
