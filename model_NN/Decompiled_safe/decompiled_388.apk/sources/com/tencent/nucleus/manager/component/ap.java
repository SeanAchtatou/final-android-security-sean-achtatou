package com.tencent.nucleus.manager.component;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ap extends CommonViewInvalidater implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserAppListView f2861a;

    public ap(UserAppListView userAppListView) {
        this.f2861a = userAppListView;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        boolean z = true;
        if (!(i == 0 || i == 1 || (i == 2 && this.f2861a.k()))) {
            z = false;
        }
        this.canHandleMsg = z;
        if (canHandleMessage()) {
            handleQueueMsg();
        }
        if (i == 2) {
            XLog.d("donald", "---yVelocity = " + this.f2861a.j());
        }
    }

    /* access modifiers changed from: protected */
    public boolean canHandleMessage() {
        return this.canHandleMsg;
    }
}
