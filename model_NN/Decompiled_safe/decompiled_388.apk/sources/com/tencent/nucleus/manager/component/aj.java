package com.tencent.nucleus.manager.component;

import android.view.View;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class aj extends ScaleAnimation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f2856a;
    private long b = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(TxManagerCommContainView txManagerCommContainView, float f, float f2, float f3, float f4, int i, float f5, int i2, float f6) {
        super(f, f2, f3, f4, i, f5, i2, f6);
        this.f2856a = txManagerCommContainView;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        this.b++;
        float c = TxManagerCommContainView.k * f;
        this.f2856a.a(f);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.f2856a.h.getLayoutParams();
        layoutParams.height = (int) ((1.0f - c) * ((float) (this.f2856a.getMeasuredHeight() - this.f2856a.j.getMeasuredHeight())));
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.f2856a.i.getLayoutParams();
        layoutParams2.height = (this.f2856a.getMeasuredHeight() - this.f2856a.j.getMeasuredHeight()) - layoutParams.height;
        float measuredHeight = (float) this.f2856a.h.getMeasuredHeight();
        View g = this.f2856a.x;
        XLog.d("Donaldxuuu", "topHeight = " + measuredHeight + " marginTop = ");
        this.f2856a.h.setLayoutParams(layoutParams);
        this.f2856a.i.setLayoutParams(layoutParams2);
        if ((measuredHeight - ((float) by.a(this.f2856a.getContext(), 144.0f))) / 2.0f >= ((float) by.a(this.f2856a.getContext(), 140.0f))) {
            return;
        }
        if (this.f2856a.y) {
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) g.getLayoutParams();
            layoutParams3.addRule(13);
            layoutParams3.width = by.a(this.f2856a.getContext(), 144.0f);
            layoutParams3.height = by.a(this.f2856a.getContext(), 144.0f);
            layoutParams3.topMargin = 0;
            layoutParams3.bottomMargin = 0;
            g.setLayoutParams(layoutParams3);
            boolean unused = this.f2856a.y = false;
            super.applyTransformation(f, transformation);
            return;
        }
        super.applyTransformation(f, transformation);
    }
}
