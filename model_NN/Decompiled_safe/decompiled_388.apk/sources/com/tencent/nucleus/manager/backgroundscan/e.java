package com.tencent.nucleus.manager.backgroundscan;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.q;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f2826a;
    final /* synthetic */ d b;

    e(d dVar, String str) {
        this.b = dVar;
        this.f2826a = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, boolean):boolean
     arg types: [com.tencent.nucleus.manager.backgroundscan.d, int]
     candidates:
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, com.tencent.assistant.protocol.jce.ContextItem):java.lang.String
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, com.tencent.nucleus.manager.backgroundscan.BackgroundScan):java.lang.String
      com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, byte):void
      com.tencent.nucleus.manager.backgroundscan.d.a(byte, int):void
      com.tencent.nucleus.manager.backgroundscan.d.a(com.tencent.nucleus.manager.backgroundscan.d, boolean):boolean */
    public void run() {
        byte a2 = this.b.k();
        long b2 = BackgroundScanTimerJob.b(d.i);
        if (d.d) {
            b2 = 2;
        }
        XLog.d("BackgroundScan", "<push> The random selected push type is : " + ((int) a2) + ", delay = " + b2);
        if (a2 <= 0) {
            XLog.d("BackgroundScan", "<push> There is no push !");
            boolean unused = this.b.b = false;
            o.a().a("b_new_scan_push_unsatisfy", a2, this.b.f, this.b.g, this.f2826a, 0);
            return;
        }
        o.a().a("b_new_scan_push_satisfy", a2, this.b.f, this.b.g, this.f2826a, b2);
        Executors.newSingleThreadScheduledExecutor(new q("backgroupscan")).schedule(new f(this, a2), b2, TimeUnit.MILLISECONDS);
    }
}
