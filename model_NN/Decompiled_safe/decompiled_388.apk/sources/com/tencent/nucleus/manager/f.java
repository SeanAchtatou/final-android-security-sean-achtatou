package com.tencent.nucleus.manager;

import android.os.Handler;
import android.os.Message;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class f extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MobileManagerInstallActivity f2883a;

    f(MobileManagerInstallActivity mobileManagerInstallActivity) {
        this.f2883a = mobileManagerInstallActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.MobileManagerInstallActivity.a(com.tencent.nucleus.manager.MobileManagerInstallActivity, boolean):void
     arg types: [com.tencent.nucleus.manager.MobileManagerInstallActivity, int]
     candidates:
      com.tencent.nucleus.manager.MobileManagerInstallActivity.a(com.tencent.nucleus.manager.MobileManagerInstallActivity, int):int
      com.tencent.nucleus.manager.MobileManagerInstallActivity.a(com.tencent.nucleus.manager.MobileManagerInstallActivity, com.tencent.pangu.download.DownloadInfo):java.lang.CharSequence
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.nucleus.manager.MobileManagerInstallActivity.a(com.tencent.nucleus.manager.MobileManagerInstallActivity, boolean):void */
    public void handleMessage(Message message) {
        DownloadInfo downloadInfo = null;
        super.handleMessage(message);
        int unused = this.f2883a.G = message.what;
        switch (this.f2883a.G) {
            case -10:
                this.f2883a.w.setVisibility(8);
                this.f2883a.x.setVisibility(0);
                XLog.e("miles", "MobileManagerInstallActivity >> get Tencent Mobile Manager[com.tencent.qqpimsecure] from server fail...");
                this.f2883a.b(false);
                return;
            case -2:
            case -1:
                this.f2883a.I.a(this.f2883a.H);
                this.f2883a.I.register(this.f2883a);
                if (this.f2883a.G == -1) {
                    XLog.w("miles", "MobileManagerInstallActivity >> check Tencent Mobile Manager[com.tencent.qqpimsecure] from local : not installed...");
                    return;
                } else {
                    XLog.w("miles", "MobileManagerInstallActivity >> check Tencent Mobile Manager[com.tencent.qqpimsecure] from local : lower version...");
                    return;
                }
            case 10:
                this.f2883a.b(false);
                this.f2883a.w.setVisibility(0);
                this.f2883a.x.setVisibility(8);
                this.f2883a.w.a(this.f2883a.H);
                DownloadInfo a2 = DownloadProxy.a().a(this.f2883a.H);
                StatInfo a3 = a.a(STInfoBuilder.buildSTInfo(this.f2883a, this.f2883a.H, STConst.ST_DEFAULT_SLOT, 200, null));
                if (a2 == null || !a2.needReCreateInfo(this.f2883a.H)) {
                    downloadInfo = a2;
                } else {
                    DownloadProxy.a().b(a2.downloadTicket);
                }
                if (downloadInfo == null) {
                    downloadInfo = DownloadInfo.createDownloadInfo(this.f2883a.H, a3);
                } else {
                    downloadInfo.updateDownloadInfoStatInfo(a3);
                }
                AppConst.AppState d = k.d(this.f2883a.H);
                if (d == AppConst.AppState.DOWNLOADED) {
                    this.f2883a.w.a(this.f2883a.getResources().getString(R.string.appbutton_install));
                } else if (d == AppConst.AppState.DOWNLOAD || d == AppConst.AppState.UPDATE || d == AppConst.AppState.ILLEGAL) {
                    this.f2883a.w.a(this.f2883a.a(downloadInfo));
                    if (d == AppConst.AppState.ILLEGAL) {
                        this.f2883a.w.a(AppConst.AppState.DOWNLOAD);
                    }
                }
                this.f2883a.w.setOnClickListener(new g(this));
                return;
            case 20:
            case 21:
                XLog.d("miles", "MobileManagerInstallActivity >> MOBILE_MANAGER_CHECK_SUCESS or MOBILE_MANAGER_CHECK_SUCESS_AFTER_INSTALL");
                this.f2883a.setResult(-1);
                this.f2883a.finish();
                return;
            default:
                return;
        }
    }
}
