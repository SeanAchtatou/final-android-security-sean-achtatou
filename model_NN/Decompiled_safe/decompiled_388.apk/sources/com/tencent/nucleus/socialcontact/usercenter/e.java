package com.tencent.nucleus.socialcontact.usercenter;

import android.text.Html;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
class e {

    /* renamed from: a  reason: collision with root package name */
    RelativeLayout f3228a;
    ImageView b;
    TXImageView c;
    ImageView d;
    TextView e;
    TextView f;
    TextView g;
    final /* synthetic */ UserCenterAdapter h;

    private e(UserCenterAdapter userCenterAdapter) {
        this.h = userCenterAdapter;
        this.f3228a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
    }

    public void a(int i) {
        if (this.f != null) {
            if (i > 0) {
                this.f.setVisibility(0);
            } else {
                this.f.setVisibility(8);
            }
            if (i > 99) {
                c("99+");
            } else {
                c(String.valueOf(i));
            }
        }
    }

    private void c(String str) {
        if (this.f != null && !TextUtils.isEmpty(str)) {
            this.f.setTextSize(1, 10.0f);
            this.f.setText(str);
        }
    }

    public void a(String str) {
        if (this.e != null) {
            this.e.setText(Html.fromHtml(str));
        }
    }

    public void a(String str, int i, TXImageView.TXImageViewType tXImageViewType) {
        if (this.c != null) {
            this.c.updateImageView(str, i, tXImageViewType);
        }
    }

    public void b(String str) {
        if (this.g != null) {
            this.g.setText(Html.fromHtml(str));
        }
    }

    public void a(boolean z) {
        if (this.d != null) {
            this.d.setVisibility(z ? 0 : 8);
        }
    }

    public void b(int i) {
        if (this.b == null) {
            return;
        }
        if (1 == i) {
            this.b.setVisibility(8);
        } else if (2 == i) {
            this.b.setVisibility(0);
        } else if (3 == i) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(8);
        }
    }

    public void c(int i) {
        if (this.f3228a == null) {
            return;
        }
        if (1 == i) {
            this.f3228a.setBackgroundResource(R.drawable.uc_cardbg_top_selector);
        } else if (2 == i) {
            this.f3228a.setBackgroundResource(R.drawable.uc_cardbg_middle_selector);
        } else if (3 == i) {
            this.f3228a.setBackgroundResource(R.drawable.uc_cardbg_bottom_selector);
        } else {
            this.f3228a.setBackgroundResource(R.drawable.uc_pop_cardbg_selector);
        }
    }
}
