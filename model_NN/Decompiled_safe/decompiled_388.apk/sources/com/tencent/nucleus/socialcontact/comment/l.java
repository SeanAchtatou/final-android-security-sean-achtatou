package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.protocol.jce.GetCommentListResponse;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetCommentListResponse f3128a;
    final /* synthetic */ int b;
    final /* synthetic */ k c;

    l(k kVar, GetCommentListResponse getCommentListResponse, int i) {
        this.c = kVar;
        this.f3128a = getCommentListResponse;
        this.b = i;
    }

    public void run() {
        boolean z = true;
        k kVar = this.c;
        if (this.f3128a.c != 1) {
            z = false;
        }
        boolean unused = kVar.e = z;
        byte[] unused2 = this.c.f = this.f3128a.d;
        this.c.notifyDataChanged(new m(this));
        if (this.c.e) {
            this.c.g.a(this.c.f);
        }
    }
}
