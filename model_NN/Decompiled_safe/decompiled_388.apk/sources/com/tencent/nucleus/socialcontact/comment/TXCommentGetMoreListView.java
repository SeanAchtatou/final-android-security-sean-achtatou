package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.AbsListView;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class TXCommentGetMoreListView extends TXGetMoreListView {
    private static final String u = TXCommentGetMoreListView.class.getSimpleName();
    private AbsListView.OnScrollListener v = null;

    public TXCommentGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TXCommentGetMoreListView(Context context) {
        super(context);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!p()) {
                    return false;
                }
                PointF pointF = this.r;
                PointF pointF2 = this.q;
                float x = motionEvent.getX();
                pointF2.x = x;
                pointF.x = x;
                PointF pointF3 = this.r;
                PointF pointF4 = this.q;
                float y = motionEvent.getY();
                pointF4.y = y;
                pointF3.y = y;
                return true;
            case 1:
            case 3:
                return j();
            case 2:
                if (!this.m) {
                    return false;
                }
                if (this.r.y < motionEvent.getY() && TXScrollViewBase.ScrollMode.PULL_FROM_END == this.o) {
                    return true;
                }
                this.r.x = motionEvent.getX();
                this.r.y = motionEvent.getY();
                if (this.o != TXScrollViewBase.ScrollMode.NOSCROLL) {
                    i();
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public int i() {
        return super.i();
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.v = onScrollListener;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.v != null) {
            this.v.onScroll(absListView, i, i2, i3);
        }
        super.onScroll(absListView, i, i2, i3);
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.v != null) {
            this.v.onScrollStateChanged(absListView, i);
        }
        super.onScrollStateChanged(absListView, i);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.e != null) {
            switch (bp.f3121a[this.f691a.ordinal()]) {
                case 1:
                    if (z) {
                        this.e.loadSuc();
                        return;
                    } else {
                        this.e.loadFail();
                        return;
                    }
                case 2:
                    if (getRawAdapter() != null) {
                        getRawAdapter().getCount();
                    }
                    this.e.loadFinish(Constants.STR_EMPTY);
                    return;
                case 3:
                    this.e.refreshing();
                    return;
                default:
                    return;
            }
        }
    }
}
