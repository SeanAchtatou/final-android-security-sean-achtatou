package com.tencent.nucleus.socialcontact.login;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.Ticket;
import com.tencent.assistant.utils.an;

/* compiled from: ProGuard */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    protected AppConst.IdentityType f3159a;
    protected byte[] b;
    protected Ticket c;

    /* access modifiers changed from: protected */
    public abstract JceStruct a();

    /* access modifiers changed from: protected */
    public abstract byte[] getKey();

    public e(AppConst.IdentityType identityType) {
        this.f3159a = identityType;
    }

    public AppConst.IdentityType getType() {
        return this.f3159a;
    }

    public Ticket getTicket() {
        if (this.c == null) {
            this.c = new Ticket();
            this.c.f1584a = (byte) this.f3159a.ordinal();
            JceStruct a2 = a();
            if (a2 != null) {
                this.c.b = an.a(a2);
            } else {
                this.c.b = new byte[0];
            }
        }
        return this.c;
    }

    public byte[] encryptBody(byte[] bArr) {
        if (this.b == null) {
            this.b = getKey();
        }
        return this.b != null ? an.a(bArr, this.b) : bArr;
    }

    public byte[] decryptBody(byte[] bArr) {
        if (this.b == null) {
            this.b = getKey();
        }
        return this.b != null ? an.b(bArr, this.b) : bArr;
    }
}
