package com.tencent.nucleus.socialcontact.tagpage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.SplashActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetAppFromTagResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class TagPageActivity extends BaseActivity implements View.OnClickListener, AbsListView.OnScrollListener, NetworkMonitor.ConnectivityChangeListener, an, ao {
    private static int L = 29;
    public static int n = -1;
    private int A = 0;
    private ArrayList<SimpleAppModel> B = new ArrayList<>();
    /* access modifiers changed from: private */
    public TagPageCardAdapter C = null;
    private b D = null;
    private TagPageHeaderView E = null;
    private String F = Constants.STR_EMPTY;
    private String G = Constants.STR_EMPTY;
    private String H = Constants.STR_EMPTY;
    private String I = Constants.STR_EMPTY;
    private String J = Constants.STR_EMPTY;
    private String K = Constants.STR_EMPTY;
    private int M = 0;
    private boolean N = true;
    private boolean O = false;
    private ArrayList<AppTagInfo> P = new ArrayList<>();
    private Bitmap Q = null;
    private boolean R = false;
    private RelativeLayout S = null;
    /* access modifiers changed from: private */
    public ai T = null;
    private boolean U = false;
    private boolean V = false;
    private Context u = null;
    private LoadingView v = null;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage w = null;
    private SecondNavigationTitleViewV5 x = null;
    private TagPageListView y = null;
    private int z = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        u();
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFormat(-3);
        this.u = this;
        try {
            setContentView((int) R.layout.tag_page_activity_layout);
            this.P.clear();
            v();
            w();
            x();
            this.D = new b();
            this.D.register(this);
            y();
            t.a().a(this);
            l.c().a();
            a(STConst.ST_DEFAULT_SLOT, this.F, 100);
        } catch (Exception e) {
            this.V = true;
            t.a().b();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        XLog.i("TagPageActivity", "*** onNewIntent ***");
        super.onNewIntent(intent);
    }

    private void u() {
        Intent intent = getIntent();
        this.F = intent.getStringExtra("tagID");
        this.G = intent.getStringExtra("tagName");
        this.H = intent.getStringExtra("appID");
        this.I = intent.getStringExtra("pkgName");
        this.J = intent.getStringExtra("tagSubTitle");
        this.K = intent.getStringExtra("firstIconUrl");
        XLog.i("TagPageActivity", "mTagId = " + this.F + ", mFirstIconUrl = " + this.K);
    }

    private void v() {
        this.v = (LoadingView) findViewById(R.id.loading_view);
        this.S = (RelativeLayout) findViewById(R.id.rl_video_fullscreen_container);
        this.x = (SecondNavigationTitleViewV5) findViewById(R.id.title);
        this.x.b(this.G);
        this.x.c(0);
        this.x.c(false);
        this.x.i();
        this.x.c(this);
        this.x.a((Activity) this);
        this.C = new TagPageCardAdapter(this, this.B);
        this.C.a(this);
        this.C.e();
        this.C.a(this.S);
        this.y = (TagPageListView) findViewById(R.id.list);
        this.E = new TagPageHeaderView(this.u, this, this.K);
        this.y.addHeaderView(this.E);
        this.y.setOnScrollListener(this);
        this.y.setDivider(null);
        this.y.setSelector(new ColorDrawable(0));
        this.y.setCacheColorHint(17170445);
        this.y.addClickLoadMore();
        this.y.setAdapter(this.C);
        this.y.b(false);
        b(true);
    }

    private void w() {
        if (this.E != null) {
            this.E.a(this.G);
            this.E.b(this.J);
        }
    }

    private void x() {
        this.w = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.w.setButtonClickListener(new t(this));
        this.w.setIsAutoLoading(true);
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        if (this.v != null) {
            this.v.setVisibility(z2 ? 0 : 8);
        }
    }

    private void b(int i) {
        if (this.w != null) {
            this.w.setErrorType(i);
            this.w.setVisibility(0);
            this.v.setVisibility(8);
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.z <= 0 && this.x != null) {
            this.z = this.x.getHeight();
        }
        if (this.A <= 0 && absListView.getChildAt(0) != null) {
            this.A = absListView.getChildAt(0).getHeight();
            XLog.i("TagPageActivity", "[onScroll] ---> nHeaderViewHeight = " + this.A);
        }
        if (i == 0 && (absListView.getChildAt(0) instanceof TagPageHeaderView)) {
            int top = ((TagPageHeaderView) absListView.getChildAt(0)).getTop() * -1;
            int i4 = this.A - this.z;
            int i5 = this.A / 3;
            if (this.x == null) {
                return;
            }
            if (top >= i5 && top <= i4) {
                float f = ((float) (top - i5)) * 255.0f;
                if (i4 != i5) {
                    i4 -= i5;
                }
                this.x.c((int) (f / ((float) i4)));
                if (this.R) {
                    this.R = false;
                    this.x.c(false);
                }
            } else if (top > i4) {
                this.x.c(255);
                if (!this.R) {
                    this.R = true;
                    this.x.c(true);
                }
            } else {
                this.x.c(0);
                if (this.R) {
                    this.R = false;
                    this.x.c(false);
                }
            }
        } else if (i >= 1 && this.x != null) {
            this.x.c(255);
            if (!this.R) {
                this.R = true;
                this.x.c(true);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (i == 0) {
            if (n < absListView.getFirstVisiblePosition() - 1 || n > absListView.getLastVisiblePosition()) {
                XLog.e("TagPageActivity", "*** stopVideoPlay ***");
                this.C.b();
            }
            if (absListView.getLastVisiblePosition() == absListView.getCount() - 1 && this.D != null) {
                if (this.N) {
                    y();
                } else {
                    this.y.g();
                }
            }
            ah.a().postDelayed(new u(this, absListView), 100);
            return;
        }
        if (i == 2) {
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        if (this.z <= 0 && this.x != null) {
            this.z = this.x.getHeight();
            XLog.i("TagPageActivity", "[onWindowFocusChanged] ---> naviTitle.getHeight() = " + this.x.getHeight());
        }
        if (this.A <= 0 && this.y.getChildAt(0) != null) {
            this.A = this.y.getChildAt(0).getHeight();
            XLog.i("TagPageActivity", "[onWindowFocusChanged] ---> nHeaderViewHeight = " + this.A);
        }
        super.onWindowFocusChanged(z2);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.V) {
            aq.a();
            getWindow().setFormat(-3);
            if (this.x != null) {
                this.x.l();
            }
            this.C.notifyDataSetChanged();
            this.C.c();
        }
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.H == null || this.I == null) {
            this.D.a(this.F, this.G, 1, "1", this.M, this.M + L);
        } else {
            this.D.a(this.F, this.G, (long) Integer.parseInt(this.H), this.I, this.M, this.M + L);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void t() {
        String string;
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && (string = intent.getExtras().getString("from_activity")) != null && string.equals(SplashActivity.class.getSimpleName())) {
            TemporaryThreadManager.get().start(new v(this));
            Intent intent2 = new Intent(this, MainActivity.class);
            intent2.putExtra("action_key_from_guide", true);
            startActivity(intent2);
            TemporaryThreadManager.get().start(new w(this));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.V) {
            if (this.x != null) {
                this.x.m();
            }
            if (this.C != null) {
                this.C.d();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.V) {
            t.a().b(this);
            if (this.C != null) {
                this.C.f();
            }
            if (this.D != null) {
                this.D.unregister(this);
            }
            if (this.Q != null && !this.Q.isRecycled()) {
                this.Q.recycle();
                this.Q = null;
            }
            XLog.i("TagPageActivity", "*** onDestroy ***");
        }
    }

    public void a(int i, int i2, List<RequestResponePair> list) {
        XLog.i("TagPageActivity", "*** onNotifyUISucceed ***");
        b(false);
        this.y.b(true);
        if (list == null || list.size() <= 0) {
            XLog.i("TagPageActivity", "[GetTagPageEngine] ---> onRequestSuccessed (error)");
        } else {
            XLog.i("TagPageActivity", "[GetTagPageEngine] ---> onRequestSuccessed, (responses.size() = " + list.size() + ")");
            for (RequestResponePair next : list) {
                if (next.response instanceof GetAppFromTagResponse) {
                    GetAppFromTagResponse getAppFromTagResponse = (GetAppFromTagResponse) next.response;
                    XLog.i("TagPageActivity", "@@@ appResponse.totalResult = " + getAppFromTagResponse.e);
                    XLog.i("TagPageActivity", "@@@ appResponse.title = " + getAppFromTagResponse.b);
                    this.M += L + 1;
                    if (this.M < getAppFromTagResponse.e) {
                        this.N = true;
                        this.y.onRefreshComplete(this.N, true);
                    } else {
                        this.N = false;
                        this.y.onRefreshComplete(this.N, true);
                    }
                    Iterator<CardItem> it = getAppFromTagResponse.c.iterator();
                    while (it.hasNext()) {
                        CardItem next2 = it.next();
                        if (next2 != null) {
                            SimpleAppModel a2 = k.a(next2);
                            if (a2 != null && Build.VERSION.SDK_INT < 14) {
                                a2.aD = Constants.STR_EMPTY;
                            }
                            this.B.add(a2);
                        }
                    }
                    if (this.P.size() == 0 && getAppFromTagResponse.f != null && getAppFromTagResponse.f.size() > 0) {
                        this.P.addAll(getAppFromTagResponse.f);
                    }
                    if (!TextUtils.isEmpty(getAppFromTagResponse.b)) {
                        this.G = getAppFromTagResponse.b;
                    }
                    if (!TextUtils.isEmpty(getAppFromTagResponse.h)) {
                        this.J = getAppFromTagResponse.h;
                    }
                }
            }
            if (this.C != null) {
                this.C.a(this.B);
                this.C.notifyDataSetChanged();
            }
            if (this.E != null) {
                if (!this.U && this.P.size() > 0) {
                    this.U = true;
                    this.E.a(this.P);
                }
                if (!TextUtils.isEmpty(this.G)) {
                    this.E.a(this.G);
                }
                if (!TextUtils.isEmpty(this.J)) {
                    this.E.b(this.J);
                }
            }
        }
        XLog.i("TagPageActivity", ">>> mAppInfos.size() = " + this.B.size());
    }

    public void b(int i, int i2, List<RequestResponePair> list) {
        XLog.i("TagPageActivity", "*** onNotifyUIFailed ***");
        if (!c.a()) {
            b(30);
        } else {
            b(20);
        }
        if (this.C != null) {
            this.C.a(new ArrayList());
            this.C.notifyDataSetChanged();
        }
        if (this.y != null) {
            this.y.b(false);
        }
    }

    public void onClick(View view) {
        String str;
        String str2;
        String str3;
        switch (view.getId()) {
            case R.id.setting_network /*2131166297*/:
                XLog.i("TagPageActivity", "setting_network");
                return;
            case R.id.back_layout_all /*2131166443*/:
                t();
                finish();
                return;
            case R.id.tv_tag_left /*2131166586*/:
                if (this.P != null && this.P.size() > 0) {
                    if (this.B == null || this.B.size() <= 0) {
                        str3 = null;
                    } else {
                        str3 = this.B.get(0).e;
                    }
                    ap.a(this.u, this.P.get(0), str3, getIntent());
                }
                a("06_001", Constants.STR_EMPTY, 200);
                finish();
                return;
            case R.id.tv_tag_middle /*2131166587*/:
                if (this.P != null && this.P.size() > 1) {
                    if (this.B == null || this.B.size() <= 0) {
                        str2 = null;
                    } else {
                        str2 = this.B.get(0).e;
                    }
                    ap.a(this.u, this.P.get(1), str2, getIntent());
                }
                a("06_002", Constants.STR_EMPTY, 200);
                finish();
                return;
            case R.id.tv_tag_right /*2131166588*/:
                if (this.P != null && this.P.size() > 2) {
                    if (this.B == null || this.B.size() <= 0) {
                        str = null;
                    } else {
                        str = this.B.get(0).e;
                    }
                    ap.a(this.u, this.P.get(2), str, getIntent());
                }
                a("06_003", Constants.STR_EMPTY, 200);
                finish();
                return;
            default:
                return;
        }
    }

    public void a(TXImageView tXImageView, Bitmap bitmap, int i) {
        if (!this.O && this.E != null && bitmap != null && i == 0) {
            XLog.i("TagPageActivity", "[TagPageActivity] ---> updateBackground");
            this.O = true;
            if (this.Q != null && !this.Q.isRecycled()) {
                this.Q.recycle();
                this.Q = null;
            }
            this.Q = a.a(bitmap, bitmap.getWidth(), bitmap.getHeight());
            this.E.a(this.Q);
            this.E.invalidate();
        }
    }

    public int f() {
        return STConst.ST_TAG_DETAIL_PAGE;
    }

    public STPageInfo n() {
        this.p.f2060a = STConst.ST_TAG_DETAIL_PAGE;
        return this.p;
    }

    public void a(String str, String str2, int i) {
        XLog.i("TagPageActivity", "[logReport] ---> actionId = " + i + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, i);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        l.a(buildSTInfo);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.C == null || !this.C.a()) {
            if (i == 4 && keyEvent.getRepeatCount() == 0) {
                a("03_001", Constants.STR_EMPTY, 200);
            }
            if (i == 4) {
                t();
            }
            return super.onKeyDown(i, keyEvent);
        }
        this.C.a(false);
        return true;
    }

    public void onConnected(APN apn) {
        XLog.i("TagPageActivity", "*** onConnected ***");
        if (this.C != null) {
            this.C.notifyDataSetChanged();
            if (aq.b()) {
                this.C.a(this.T, n);
            }
        }
    }

    public void onDisconnected(APN apn) {
        XLog.i("TagPageActivity", "*** onDisconnected ***");
        if (this.C != null) {
            this.C.notifyDataSetChanged();
        }
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        XLog.i("TagPageActivity", "*** onConnectivityChanged ***");
        if (this.C != null) {
            this.C.notifyDataSetChanged();
            if (aq.b()) {
                this.C.a(this.T, n);
            }
        }
    }
}
