package com.tencent.nucleus.socialcontact.comment;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

/* compiled from: ProGuard */
class aw implements ActionMode.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListFooterView f3104a;

    aw(CommentReplyListFooterView commentReplyListFooterView) {
        this.f3104a = commentReplyListFooterView;
    }

    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    public void onDestroyActionMode(ActionMode actionMode) {
    }

    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return false;
    }
}
