package com.tencent.nucleus.socialcontact.tagpage;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class MiniVideoSetDialog extends Dialog implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f3168a = null;
    private i b = null;
    private RelativeLayout c = null;
    private RelativeLayout d = null;
    private RelativeLayout e = null;
    private RelativeLayout f = null;
    private ImageView g = null;
    private ImageView h = null;
    private ImageView i = null;
    private ItemIndex j = ItemIndex.ONLY_WIFI;

    /* compiled from: ProGuard */
    public enum ItemIndex {
        MOBILE_WIFI,
        ONLY_WIFI,
        CLOSE
    }

    public MiniVideoSetDialog(Context context) {
        super(context);
        this.f3168a = context;
    }

    public MiniVideoSetDialog(Context context, int i2, ItemIndex itemIndex) {
        super(context, i2);
        this.f3168a = context;
        this.j = itemIndex;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mini_video_dialog_layout);
        Window window = getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.861f);
        window.setAttributes(attributes);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        a();
    }

    private void a() {
        this.c = (RelativeLayout) findViewById(R.id.mobile_wifi_network_layout);
        this.c.setOnClickListener(this);
        this.d = (RelativeLayout) findViewById(R.id.only_wifi_network_layout);
        this.d.setOnClickListener(this);
        this.e = (RelativeLayout) findViewById(R.id.close_layout);
        this.e.setOnClickListener(this);
        this.f = (RelativeLayout) findViewById(R.id.confirm_layout);
        this.f.setOnClickListener(this);
        this.g = (ImageView) findViewById(R.id.iv_mobile_wifi_network);
        this.h = (ImageView) findViewById(R.id.iv_only_wifi_network);
        this.i = (ImageView) findViewById(R.id.iv_close);
        a(this.j);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mobile_wifi_network_layout /*2131166284*/:
                XLog.i("MiniVideoSetDialog", "mobile_wifi_network_layout");
                this.j = ItemIndex.MOBILE_WIFI;
                a(this.j);
                return;
            case R.id.only_wifi_network_layout /*2131166287*/:
                XLog.i("MiniVideoSetDialog", "only_wifi_network_layout");
                this.j = ItemIndex.ONLY_WIFI;
                a(this.j);
                return;
            case R.id.close_layout /*2131166290*/:
                XLog.i("MiniVideoSetDialog", "close_layout");
                this.j = ItemIndex.CLOSE;
                a(this.j);
                return;
            case R.id.confirm_layout /*2131166293*/:
                XLog.i("MiniVideoSetDialog", "confirm_layout");
                if (this.b != null) {
                    this.b.a(this.j);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void a(ItemIndex itemIndex) {
        this.g.setImageResource(R.drawable.ic_mini_video_set_normal);
        this.h.setImageResource(R.drawable.ic_mini_video_set_normal);
        this.i.setImageResource(R.drawable.ic_mini_video_set_normal);
        switch (h.f3204a[itemIndex.ordinal()]) {
            case 1:
                this.g.setImageResource(R.drawable.ic_mini_video_set_select);
                return;
            case 2:
                this.h.setImageResource(R.drawable.ic_mini_video_set_select);
                return;
            case 3:
                this.i.setImageResource(R.drawable.ic_mini_video_set_select);
                return;
            default:
                return;
        }
    }

    public void a(i iVar) {
        if (iVar != null) {
            this.b = iVar;
        }
    }
}
