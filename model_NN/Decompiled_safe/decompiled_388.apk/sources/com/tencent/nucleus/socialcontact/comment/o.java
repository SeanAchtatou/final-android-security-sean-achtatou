package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GetCommentListRequest;

/* compiled from: ProGuard */
class o implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3131a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ GetCommentListRequest d;
    final /* synthetic */ k e;

    o(k kVar, int i, int i2, boolean z, GetCommentListRequest getCommentListRequest) {
        this.e = kVar;
        this.f3131a = i;
        this.b = i2;
        this.c = z;
        this.d = getCommentListRequest;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3131a, this.b, this.c, this.d.h, null, null, null, false, null, null);
    }
}
