package com.tencent.nucleus.socialcontact.tagpage;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3217a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    x(TagPageCardAdapter tagPageCardAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = tagPageCardAdapter;
        this.f3217a = simpleAppModel;
        this.b = i;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.c.b, AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.f3217a);
        if (this.c.b instanceof BaseActivity) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.c.b).f());
        }
        this.c.b.startActivity(intent);
        this.c.a(this.c.a(this.b) + "_01", Constants.STR_EMPTY, 200);
    }
}
