package com.tencent.pangu.mediadownload;

import com.tencent.assistant.utils.FileUtil;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
public class q extends d {

    /* renamed from: a  reason: collision with root package name */
    public String f3868a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public int h;
    public String i;
    public String j;

    public int a() {
        return 8;
    }

    public String b() {
        return FileUtil.getDynamicVideoDir();
    }

    public String toString() {
        return "VideoDownInfo{name='" + this.f3868a + '\'' + ", desc='" + this.b + '\'' + ", downUrl='" + this.l + '\'' + ", filename='" + this.k + '\'' + ", downId='" + this.m + '\'' + ", coverUrl='" + this.c + '\'' + ", fileSize=" + this.n + ", cpName='" + this.d + '\'' + ", createTime=" + this.o + ", finishTime=" + this.p + ", downloadingPath='" + this.q + '\'' + ", savePath='" + this.r + '\'' + ", downState=" + this.s + ", openPackageName='" + this.e + '\'' + ", openActivity='" + this.f + '\'' + ", openUri='" + this.g + '\'' + ", minVersionCode=" + this.h + ", playerName='" + this.i + '\'' + ", extraParams='" + this.j + '\'' + ", errorCode=" + this.t + ", videoDownResponse=" + this.u + '}';
    }
}
