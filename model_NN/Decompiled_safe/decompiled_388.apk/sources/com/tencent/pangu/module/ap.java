package com.tencent.pangu.module;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ap extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ao f3907a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ap(ao aoVar, Looper looper) {
        super(looper);
        this.f3907a = aoVar;
    }

    public void handleMessage(Message message) {
        if (6666 == message.what) {
            TemporaryThreadManager.get().start(new aq(this));
        }
    }
}
