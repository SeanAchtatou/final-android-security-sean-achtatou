package com.tencent.pangu.module.wisedownload;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a.s;
import com.tencent.assistant.manager.notification.o;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class l implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static l f3967a;
    private Map<String, AutoDownloadInfo> b;
    private Map<String, AutoDownloadInfo> c;
    private Map<String, AutoDownloadInfo> d;
    private Map<String, AutoDownloadInfo> e;

    public static synchronized l a() {
        l lVar;
        synchronized (l.class) {
            if (f3967a == null) {
                f3967a = new l();
            }
            lVar = f3967a;
        }
        return lVar;
    }

    private l() {
    }

    public void b() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(1007, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_CANCEL_APP_INSTALL_TASK, this);
    }

    private void a(List<AutoDownloadInfo> list) {
        if (list != null && list.size() > 0) {
            if (this.b == null) {
                this.b = new ConcurrentHashMap();
            }
            for (AutoDownloadInfo next : list) {
                if (next != null && !TextUtils.isEmpty(next.f1166a) && !a(next) && !b(next)) {
                    c(next);
                }
            }
        }
    }

    private boolean a(AutoDownloadInfo autoDownloadInfo) {
        AutoDownloadInfo autoDownloadInfo2;
        if (autoDownloadInfo == null || TextUtils.isEmpty(autoDownloadInfo.f1166a)) {
            return false;
        }
        return (this.d == null || (autoDownloadInfo2 = this.d.get(autoDownloadInfo.f1166a)) == null || autoDownloadInfo2.d != autoDownloadInfo.d) ? false : true;
    }

    private boolean b(AutoDownloadInfo autoDownloadInfo) {
        AutoDownloadInfo autoDownloadInfo2;
        if (autoDownloadInfo == null || TextUtils.isEmpty(autoDownloadInfo.f1166a)) {
            return false;
        }
        return (this.e == null || (autoDownloadInfo2 = this.e.get(autoDownloadInfo.f1166a)) == null || autoDownloadInfo2.d != autoDownloadInfo.d) ? false : true;
    }

    private boolean c(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo == null || TextUtils.isEmpty(autoDownloadInfo.f1166a)) {
            return false;
        }
        if (this.b != null) {
            AutoDownloadInfo autoDownloadInfo2 = this.b.get(autoDownloadInfo.f1166a);
            if (autoDownloadInfo2 != null && autoDownloadInfo.equals(autoDownloadInfo2)) {
                return true;
            }
            this.b.put(autoDownloadInfo.f1166a, autoDownloadInfo);
        }
        return false;
    }

    public void handleUIEvent(Message message) {
        AutoDownloadInfo autoDownloadInfo;
        DownloadInfo d2;
        AutoDownloadInfo autoDownloadInfo2;
        AutoDownloadInfo autoDownloadInfo3;
        AutoDownloadInfo autoDownloadInfo4;
        XLog.d("WiseDownloadPushManager", message.what + Constants.STR_EMPTY);
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                if (m.a().k() && (d2 = DownloadProxy.a().d((String) message.obj)) != null && !TextUtils.isEmpty(d2.packageName) && this.b != null && (autoDownloadInfo2 = this.b.get(d2.packageName)) != null && !TextUtils.isEmpty(autoDownloadInfo2.f1166a)) {
                    if (this.c == null) {
                        this.c = new ConcurrentHashMap();
                    }
                    this.c.put(autoDownloadInfo2.f1166a, autoDownloadInfo2);
                    this.b.remove(autoDownloadInfo2.f1166a);
                    return;
                }
                return;
            case 1007:
                DownloadInfo d3 = DownloadProxy.a().d((String) message.obj);
                if (d3 != null && !TextUtils.isEmpty(d3.packageName)) {
                    if (!(this.b == null || (autoDownloadInfo = this.b.get(d3.packageName)) == null || TextUtils.isEmpty(autoDownloadInfo.f1166a))) {
                        if (this.e == null) {
                            this.e = new ConcurrentHashMap();
                        }
                        this.e.put(autoDownloadInfo.f1166a, autoDownloadInfo);
                        this.b.remove(autoDownloadInfo.f1166a);
                    }
                    c();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
                if (installUninstallTaskBean != null && !TextUtils.isEmpty(installUninstallTaskBean.packageName)) {
                    if (!(this.c == null || (autoDownloadInfo4 = this.c.get(installUninstallTaskBean.packageName)) == null || TextUtils.isEmpty(autoDownloadInfo4.f1166a))) {
                        if (this.d == null) {
                            this.d = new ConcurrentHashMap();
                        }
                        this.d.put(autoDownloadInfo4.f1166a, autoDownloadInfo4);
                        this.c.remove(autoDownloadInfo4.f1166a);
                    }
                    c();
                    return;
                }
                return;
            case 1027:
                InstallUninstallTaskBean installUninstallTaskBean2 = (InstallUninstallTaskBean) message.obj;
                if (installUninstallTaskBean2 != null && !TextUtils.isEmpty(installUninstallTaskBean2.packageName)) {
                    if (!(this.c == null || (autoDownloadInfo3 = this.c.get(installUninstallTaskBean2.packageName)) == null || TextUtils.isEmpty(autoDownloadInfo3.f1166a))) {
                        if (this.e == null) {
                            this.e = new ConcurrentHashMap();
                        }
                        this.e.put(autoDownloadInfo3.f1166a, autoDownloadInfo3);
                        this.c.remove(autoDownloadInfo3.f1166a);
                    }
                    c();
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START:
                a(t.a().c());
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE:
                c();
                return;
            default:
                return;
        }
    }

    private void c() {
        long j;
        long j2;
        int c2;
        int i;
        long j3;
        int i2;
        int i3;
        int i4;
        long a2 = (long) m.a().a("key_auto_download_push_max_count_per_day", 1);
        long a3 = (long) m.a().a("key_auto_download_push_max_count_per_week", 3);
        if (a2 < 0 || a3 < 0 || a2 > a3) {
            j2 = 3;
            j = 1;
        } else {
            j2 = a3;
            j = a2;
        }
        Object[] as = m.a().as();
        if (as == null || as.length < 4) {
            long currentTimeMillis = System.currentTimeMillis();
            c2 = bo.c();
            i = 0;
            j3 = currentTimeMillis;
            i2 = 0;
        } else {
            long longValue = ((Long) as[0]).longValue();
            int intValue = ((Integer) as[1]).intValue();
            int intValue2 = ((Integer) as[2]).intValue();
            int intValue3 = ((Integer) as[3]).intValue();
            if (!bo.b(longValue)) {
                j3 = System.currentTimeMillis();
                i4 = 0;
            } else {
                i4 = intValue;
                j3 = longValue;
            }
            if (!bo.a(intValue2)) {
                c2 = bo.c();
                i = 0;
                i2 = i4;
            } else {
                i2 = i4;
                int i5 = intValue2;
                i = intValue3;
                c2 = i5;
            }
        }
        if (((long) i) >= j2 || ((long) i2) > j) {
            o.a(123, s.e(), 9, (byte[]) null);
        } else if (this.c != null && this.c.size() <= 0 && q.a(SimpleDownloadInfo.UIType.WISE_APP_UPDATE) <= 0) {
            int i6 = 0;
            if (this.d != null) {
                i6 = this.d.size();
            }
            if (i6 < 3 || AstApp.i().l()) {
                if (i6 >= 3) {
                    i3 = 2;
                } else {
                    i3 = 10;
                }
                o.a(123, s.e(), i3, (byte[]) null);
            } else {
                m.a().a(j3, i2 + 1, c2, i + 1);
                ArrayList arrayList = new ArrayList();
                Iterator<Map.Entry<String, AutoDownloadInfo>> it = this.d.entrySet().iterator();
                while (it != null && it.hasNext()) {
                    Map.Entry next = it.next();
                    if (!(next == null || next.getValue() == null)) {
                        arrayList.add(next.getValue());
                    }
                }
                o.a().a(123, arrayList);
            }
            d();
        }
    }

    private void d() {
        this.e = null;
        this.d = null;
        this.b = null;
        this.c = null;
    }
}
