package com.tencent.pangu.module.c;

import com.tencent.assistant.db.a.a;
import com.tencent.assistant.protocol.jce.UploadControlRequest;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3931a;

    d(b bVar) {
        this.f3931a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, boolean):boolean
     arg types: [com.tencent.pangu.module.c.b, int]
     candidates:
      com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, int):int
      com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, com.qq.taf.jce.JceStruct):int
      com.tencent.pangu.module.c.b.a(com.tencent.assistant.db.a.a, boolean):void
      com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, com.tencent.assistant.db.a.a):void
      com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, com.tencent.assistant.db.a.a, boolean):void
     arg types: [com.tencent.pangu.module.c.b, com.tencent.assistant.db.a.a, int]
     candidates:
      com.tencent.pangu.module.c.b.a(java.lang.String, java.lang.String, java.util.List<java.lang.String>):boolean
      com.tencent.pangu.module.c.b.a(com.tencent.pangu.module.c.b, com.tencent.assistant.db.a.a, boolean):void */
    public void run() {
        if (this.f3931a.c) {
            boolean unused = this.f3931a.d = false;
            return;
        }
        UploadControlRequest uploadControlRequest = new UploadControlRequest();
        a unused2 = this.f3931a.f = this.f3931a.f();
        if (this.f3931a.f == null) {
            boolean unused3 = this.f3931a.d = false;
        } else if (this.f3931a.c(this.f3931a.f)) {
            this.f3931a.a(this.f3931a.f, true);
            uploadControlRequest.c = this.f3931a.f.c;
            uploadControlRequest.b = this.f3931a.f.b;
            uploadControlRequest.i = this.f3931a.f.m;
            uploadControlRequest.n = this.f3931a.f.n != null ? this.f3931a.f.n : Constants.STR_EMPTY;
            uploadControlRequest.l = this.f3931a.f.k != null ? this.f3931a.f.k : Constants.STR_EMPTY;
            uploadControlRequest.j = this.f3931a.f.g != null ? this.f3931a.f.g : Constants.STR_EMPTY;
            uploadControlRequest.d = this.f3931a.f.i;
            uploadControlRequest.h = this.f3931a.f.f;
            uploadControlRequest.f = this.f3931a.f.d != null ? this.f3931a.f.d : Constants.STR_EMPTY;
            uploadControlRequest.m = this.f3931a.f.l != null ? this.f3931a.f.l : Constants.STR_EMPTY;
            uploadControlRequest.k = this.f3931a.f.h != null ? this.f3931a.f.h : Constants.STR_EMPTY;
            uploadControlRequest.e = this.f3931a.f.j;
            uploadControlRequest.f1600a = this.f3931a.f.f739a;
            uploadControlRequest.g = this.f3931a.f.e;
            uploadControlRequest.o = b.a(this.f3931a.f.b);
            int unused4 = this.f3931a.e = this.f3931a.send(uploadControlRequest);
        } else {
            if (this.f3931a.f != null && !FileUtil.isFileExists(this.f3931a.f.g) && !FileUtil.isFileExists(this.f3931a.f.h)) {
                this.f3931a.b(this.f3931a.f);
            }
            boolean unused5 = this.f3931a.d = false;
        }
    }
}
