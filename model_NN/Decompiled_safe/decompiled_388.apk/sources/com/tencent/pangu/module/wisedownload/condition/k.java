package com.tencent.pangu.module.wisedownload.condition;

import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.pangu.module.wisedownload.b;
import com.tencent.pangu.module.wisedownload.condition.ThresholdCondition;
import com.tencent.pangu.module.wisedownload.s;

/* compiled from: ProGuard */
public class k extends ThresholdCondition {
    private int c;

    public k(b bVar) {
        a(bVar);
    }

    public void a(b bVar) {
        AutoDownloadCfg j;
        if (bVar != null && (j = bVar.j()) != null) {
            this.c = j.c;
        }
    }

    public boolean a() {
        a(ThresholdCondition.CONDITION_RESULT_CODE.OK);
        return i();
    }

    private boolean i() {
        long a2 = s.a();
        if (a2 == 0) {
            a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_NO_APP);
            return false;
        }
        boolean a3 = a(a2 + ((long) (this.c * NativeFileObject.S_IFREG)));
        if (a3) {
            return a3;
        }
        a(ThresholdCondition.CONDITION_RESULT_CODE.FAIL_OTHER_SPACE);
        return a3;
    }

    public boolean b() {
        long a2 = s.a();
        if (a2 == 0) {
            return true;
        }
        return a(a2 + ((long) (this.c * NativeFileObject.S_IFREG)));
    }

    public boolean h() {
        return s.a() != 0;
    }
}
