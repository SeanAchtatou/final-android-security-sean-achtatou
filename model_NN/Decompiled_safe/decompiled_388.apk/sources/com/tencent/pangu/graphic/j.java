package com.tencent.pangu.graphic;

import android.text.TextUtils;
import com.tencent.assistant.utils.r;
import java.io.File;

/* compiled from: ProGuard */
class j implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f3775a;
    final /* synthetic */ i b;

    j(i iVar, g gVar) {
        this.b = iVar;
        this.f3775a = gVar;
    }

    public void run() {
        if (this.f3775a != null && !TextUtils.isEmpty(this.f3775a.f3773a)) {
            int f = (int) (r.f() * 120.0f);
            if (this.f3775a.b == 0 || this.f3775a.b > f) {
                this.f3775a.b = f;
            }
            String a2 = this.b.a(this.f3775a.f3773a);
            if (!TextUtils.isEmpty(a2)) {
                File file = new File(a2);
                if (!file.exists()) {
                    boolean unused = this.b.a(this.f3775a.f3773a, a2);
                }
                e eVar = null;
                if (file.exists()) {
                    eVar = this.b.a(a2, this.f3775a.b, this.f3775a.c);
                }
                this.b.a(this.f3775a, eVar);
            }
        }
    }
}
