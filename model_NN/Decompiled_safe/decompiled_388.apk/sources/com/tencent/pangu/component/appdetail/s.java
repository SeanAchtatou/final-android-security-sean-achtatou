package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class s extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailFloatingDialog f3620a;

    s(AppdetailFloatingDialog appdetailFloatingDialog) {
        this.f3620a = appdetailFloatingDialog;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.tv_share_qq /*2131165717*/:
                if (this.f3620a.g != null && this.f3620a.g()) {
                    this.f3620a.g.shareToQQ();
                    break;
                }
            case R.id.tv_share_qz /*2131165718*/:
                if (this.f3620a.g != null && this.f3620a.f()) {
                    this.f3620a.g.shareToQZ();
                    break;
                }
            case R.id.tv_share_wx /*2131165719*/:
                if (this.f3620a.g != null && this.f3620a.h()) {
                    this.f3620a.g.shareToWX();
                    break;
                }
            case R.id.tv_share_timeline /*2131165722*/:
                if (this.f3620a.g != null && this.f3620a.i()) {
                    this.f3620a.g.shareToTimeLine();
                    break;
                }
            case R.id.tv_permission /*2131165723*/:
                if (this.f3620a.g != null) {
                    this.f3620a.g.showPermission();
                    break;
                }
                break;
            case R.id.tv_report /*2131165724*/:
                if (this.f3620a.g != null) {
                    this.f3620a.g.showReport();
                    break;
                }
                break;
        }
        this.f3620a.dismiss();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 f = this.f3620a.e();
        f.actionId = 200;
        if (this.clickViewId == R.id.tv_share_qq) {
            f.slotId = a.a(this.f3620a.n, "001");
        } else if (this.clickViewId == R.id.tv_share_qz) {
            f.slotId = a.a(this.f3620a.n, "002");
        } else if (this.clickViewId == R.id.tv_share_wx) {
            f.slotId = a.a(this.f3620a.n, "003");
        } else if (this.clickViewId == R.id.tv_share_timeline) {
            f.slotId = a.a(this.f3620a.n, "004");
        } else if (this.clickViewId == R.id.tv_permission) {
            f.slotId = a.a(this.f3620a.n, "005");
        } else if (this.clickViewId == R.id.tv_report) {
            f.slotId = a.a(this.f3620a.n, "006");
        }
        return f;
    }
}
