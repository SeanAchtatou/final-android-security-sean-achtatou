package com.tencent.pangu.component.topbanner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.pangu.component.topbanner.TopBannerView;

/* compiled from: ProGuard */
class h implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TopBannerView f3724a;

    h(TopBannerView topBannerView) {
        this.f3724a = topBannerView;
    }

    public void a(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            XLog.d("topbanner", "fail");
            ah.a().post(new j(this));
            return;
        }
        XLog.d("topbanner", "finish");
        this.f3724a.a(bitmap, TopBannerView.ImageType.BANNER);
        Bitmap unused = this.f3724a.g = this.f3724a.b(bitmap, TopBannerView.ImageType.BANNER);
        ah.a().post(new i(this));
    }
}
