package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppDetailExGift;
import com.tencent.assistant.protocol.jce.AppExCfg;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class AppdetailGiftView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected LayoutInflater f3533a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public ArrayList<AppExCfg> c = new ArrayList<>();
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;

    public AppdetailGiftView(Context context) {
        super(context);
        a(context);
    }

    public AppdetailGiftView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        this.f3533a = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public void a(AppDetailExGift appDetailExGift) {
        if (appDetailExGift != null) {
            this.c.clear();
            if (appDetailExGift.b != null) {
                this.c.addAll(appDetailExGift.b);
            }
            this.d = appDetailExGift.f1146a;
            int size = this.c.size();
            if (size > appDetailExGift.c) {
                int i = appDetailExGift.c - 1;
                a(i, 0);
                this.e = i;
                View a2 = a(appDetailExGift.d + "(" + (size - i) + ")", 0);
                if (a2 != null) {
                    ImageView imageView = new ImageView(this.b);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, by.b(0.5f));
                    imageView.setLayoutParams(layoutParams);
                    imageView.setBackgroundDrawable(getResources().getDrawable(R.color.smart_card_divider));
                    addView(imageView, layoutParams);
                    addView(a2, new LinearLayout.LayoutParams(-1, by.b(44.0f)));
                }
            } else if (size > 0) {
                a(size, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2) {
        boolean z;
        removeAllViews();
        if (this.c.size() < i) {
            i = this.c.size();
        }
        for (int i3 = 0; i3 < i; i3++) {
            AppExCfg appExCfg = this.c.get(i3);
            if (i3 == i - 1) {
                z = true;
            } else {
                z = false;
            }
            View a2 = a(appExCfg, z, i3, i2);
            if (a2 != null) {
                addView(a2, new LinearLayout.LayoutParams(-1, by.b(70.0f)));
            }
        }
    }

    private View a(AppExCfg appExCfg, boolean z, int i, int i2) {
        if (appExCfg == null) {
            return null;
        }
        View inflate = this.f3533a.inflate((int) R.layout.app_detail_gift_item_view, (ViewGroup) null);
        TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.gift_icon);
        TextView textView = (TextView) inflate.findViewById(R.id.gift_title);
        TextView textView2 = (TextView) inflate.findViewById(R.id.gift_desc);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.gift_divider);
        switch (appExCfg.f1150a) {
            case 5:
                tXImageView.updateImageView(appExCfg.b, R.drawable.icon_gift_game, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                break;
            case 6:
                tXImageView.updateImageView(appExCfg.b, R.drawable.icon_youhui, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                break;
            default:
                tXImageView.updateImageView(appExCfg.b, R.drawable.icon_gift_game, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                break;
        }
        textView.setText(appExCfg.c);
        textView2.setText(appExCfg.f);
        if (z) {
            imageView.setVisibility(8);
        }
        inflate.setOnClickListener(new u(this, appExCfg, i));
        if ((this.b instanceof AppDetailActivityV5) && i + 1 > i2) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, 100);
            buildSTInfo.slotId = a.a(this.d == 1 ? Constants.VIA_ACT_TYPE_NINETEEN : "18", i + 1);
            l.a(buildSTInfo);
        }
        return inflate;
    }

    private View a(String str, int i) {
        View inflate = this.f3533a.inflate((int) R.layout.app_detail_gift_item_more_view, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.gift_more);
        textView.setText(str);
        textView.setOnClickListener(new v(this, i));
        return inflate;
    }
}
