package com.tencent.pangu.component.appdetail;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

/* compiled from: ProGuard */
class bg implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    long f3585a = 0;
    final /* synthetic */ InnerScrollView b;
    private long c = -1;
    private long d = 350;
    private int e = 0;
    private Interpolator f = new DecelerateInterpolator();
    private int g = 0;

    public bg(InnerScrollView innerScrollView, int i) {
        this.b = innerScrollView;
        this.g = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void run() {
        if (this.c == -1) {
            this.c = System.currentTimeMillis();
        } else {
            this.f3585a = ((System.currentTimeMillis() - this.c) * 1000) / this.d;
            this.f3585a = Math.max(Math.min(this.f3585a, 1000L), 0L);
            this.b.scrollTo(0, Math.round(((float) this.g) * this.f.getInterpolation(((float) this.f3585a) / 1000.0f)));
        }
        if (this.f3585a < 1000) {
            this.b.post(this);
        }
    }
}
