package com.tencent.pangu.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class bs extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f3659a;
    final /* synthetic */ VideoDownloadButton b;

    bs(VideoDownloadButton videoDownloadButton, STInfoV2 sTInfoV2) {
        this.b = videoDownloadButton;
        this.f3659a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.b, Constants.STR_EMPTY);
    }

    public STInfoV2 getStInfo() {
        if (this.f3659a != null) {
            this.f3659a.actionId = a.a(this.b.b.s);
            this.f3659a.status = a.c(this.b.b.s);
        }
        return this.f3659a;
    }
}
