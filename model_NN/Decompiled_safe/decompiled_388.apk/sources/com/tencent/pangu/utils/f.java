package com.tencent.pangu.utils;

import android.content.Context;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse;
import com.tencent.assistant.protocol.jce.PopupScene;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.as;
import com.tencent.pangu.module.at;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GetPhoneUserAppListResponse f3979a;
    final /* synthetic */ boolean b;
    final /* synthetic */ Context c;
    final /* synthetic */ int d;
    final /* synthetic */ boolean e;

    f(GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z, Context context, int i, boolean z2) {
        this.f3979a = getPhoneUserAppListResponse;
        this.b = z;
        this.c = context;
        this.d = i;
        this.e = z2;
    }

    public void run() {
        XLog.i("PopUpNecessaryAcitivity", "PopWindowUtils showPopWindow:" + this.f3979a);
        if (this.b || this.f3979a == null) {
            List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
            if (localApkInfos == null || localApkInfos.size() == 0) {
                XLog.i("startNewPopWindowActivity", "no local apkInfo!");
                return;
            }
            GetPopupNecessaryResponse c2 = at.a().c();
            if (c2 == null || c2.a().size() <= 0) {
                XLog.i("startNewPopWindowActivity", "no response data!");
                return;
            }
            if (this.e) {
                long c3 = as.c();
                if (c3 > 0 && System.currentTimeMillis() - c3 > 1000) {
                    return;
                }
            }
            if (System.currentTimeMillis() - m.a().W() <= ((long) (c2.c * 24 * 60 * 60 * 1000))) {
                XLog.i("startNewPopWindowActivity", "time out! popWindow shouldn't show~");
                return;
            }
            ArrayList arrayList = new ArrayList();
            for (LocalApkInfo next : localApkInfos) {
                if ((next.flags & 1) == 0 && (next.flags & 128) == 0) {
                    arrayList.add(next.mPackageName);
                }
            }
            ArrayList<PopupScene> a2 = c2.a();
            ArrayList<PopupScene> arrayList2 = new ArrayList<>();
            Iterator<PopupScene> it = a2.iterator();
            while (it.hasNext()) {
                PopupScene next2 = it.next();
                if (next2 != null && next2.d.size() > 4) {
                    int i = next2.b;
                    ArrayList<CardItem> arrayList3 = new ArrayList<>();
                    Iterator<CardItem> it2 = next2.d.iterator();
                    while (it2.hasNext()) {
                        CardItem next3 = it2.next();
                        if (next3 != null && arrayList.contains(next3.f.f)) {
                            XLog.i("installedApp", "installed packageName:" + next3.f.f + " appName:" + next3.f.b);
                        } else if (next3 != null) {
                            arrayList3.add(next3);
                        }
                    }
                    if (arrayList3.size() >= i) {
                        next2.d = arrayList3;
                        arrayList2.add(next2);
                    }
                }
            }
            XLog.i("startNewPopWindowActivity", "popScene:" + arrayList2);
            if (arrayList2.size() > 0) {
                c2.d = arrayList2;
                ah.a().post(new i(this, c2));
                return;
            }
            return;
        }
        ArrayList<CardItem> arrayList4 = new ArrayList<>();
        Iterator<CardItem> it3 = this.f3979a.c.iterator();
        while (it3.hasNext()) {
            CardItem next4 = it3.next();
            XLog.e("zhangyuanchao", "-------cardItemList------:" + next4.f.b);
            if (ApkResourceManager.getInstance().getLocalApkInfo(next4.f.f) == null) {
                arrayList4.add(next4);
            }
        }
        this.f3979a.c = arrayList4;
        if (arrayList4.size() > 0) {
            ah.a().post(new g(this));
            return;
        }
        boolean z = m.a().a("key_re_app_list_state", 0) == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
        m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
        MainActivity.z = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
        ah.a().post(new h(this, z));
    }
}
