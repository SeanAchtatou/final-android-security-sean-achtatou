package com.tencent.pangu.utils.installuninstall;

import android.os.Message;
import com.tencent.assistant.event.listener.UIEventListener;

/* compiled from: ProGuard */
class af implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f3993a;

    private af(ac acVar) {
        this.f3993a = acVar;
    }

    /* synthetic */ af(ac acVar, ad adVar) {
        this(acVar);
    }

    public void handleUIEvent(Message message) {
        if (message.what == 1031) {
            this.f3993a.h();
            this.f3993a.l();
            synchronized (this.f3993a.d) {
                this.f3993a.d.notifyAll();
            }
        }
    }
}
