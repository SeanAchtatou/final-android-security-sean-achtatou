package com.tencent.pangu.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.db.table.u;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.PopUpNecessaryAcitivity;
import com.tencent.pangu.activity.StartPopWindowActivity;
import com.tencent.pangu.manager.au;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3978a = true;
    private static u b = new u();
    private static ArrayList<PopUpInfo> c = new ArrayList<>();

    public static synchronized void a(Context context, boolean z, int i, GetPhoneUserAppListResponse getPhoneUserAppListResponse, boolean z2) {
        synchronized (e.class) {
            TemporaryThreadManager.get().start(new f(getPhoneUserAppListResponse, z2, context, i, z));
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, GetPopupNecessaryResponse getPopupNecessaryResponse) {
        if (!(((Activity) context) instanceof MainActivity) || (!((Activity) context).isFinishing() && ((MainActivity) context).n)) {
            m.a().h(System.currentTimeMillis());
            Intent intent = new Intent(context, PopUpNecessaryAcitivity.class);
            intent.putExtra("param_key_response_data", getPopupNecessaryResponse);
            try {
                context.startActivity(intent);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a() {
        PopUpInfo popUpInfo = new PopUpInfo();
        popUpInfo.d = System.currentTimeMillis();
        popUpInfo.f1436a = 0;
        popUpInfo.f = 889032704;
        b.a(popUpInfo);
        c.add(0, popUpInfo);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, GetPhoneUserAppListResponse getPhoneUserAppListResponse, int i, boolean z) {
        a();
        Intent intent = new Intent(context, StartPopWindowActivity.class);
        intent.putExtra("extra_pop_info", getPhoneUserAppListResponse);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i);
        intent.putExtra("extra_is_recover", true);
        intent.putExtra("extra_special_title", z);
        try {
            context.startActivity(intent);
        } catch (Throwable th) {
        }
    }

    public static void a(Context context, boolean z) {
        j jVar = new j();
        jVar.hasTitle = true;
        jVar.titleRes = context.getResources().getString(R.string.pop_huanji_title);
        if (z) {
            jVar.contentRes = context.getResources().getString(R.string.pop_huanji_content_no_apps);
        } else {
            jVar.contentRes = context.getResources().getString(R.string.pop_huanji_content);
        }
        jVar.lBtnTxtRes = context.getResources().getString(R.string.down_page_dialog_left_del);
        jVar.rBtnTxtRes = context.getResources().getString(R.string.pop_huanji_right_btn);
        jVar.blockCaller = true;
        DialogUtils.show2BtnDialog(jVar);
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, STConst.ST_DEFAULT_SLOT, 0, STConst.ST_DEFAULT_SLOT, 100));
        au.i = false;
    }

    public static void b() {
        PluginInfo.PluginEntry pluginEntryByStartActivity;
        PluginInfo a2 = i.b().a("com.tencent.mobileassistant_wifitransfer");
        if (a2 != null && (pluginEntryByStartActivity = a2.getPluginEntryByStartActivity("SwitchPhoneActivity.SwitchPhoneActivity")) != null) {
            try {
                PluginProxyActivity.a(AstApp.i(), pluginEntryByStartActivity.getHostPlugInfo().getPackageName(), pluginEntryByStartActivity.getHostPlugInfo().getVersion(), pluginEntryByStartActivity.getStartActivity(), pluginEntryByStartActivity.getHostPlugInfo().getInProcess(), null, pluginEntryByStartActivity.getHostPlugInfo().getLaunchApplication());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
