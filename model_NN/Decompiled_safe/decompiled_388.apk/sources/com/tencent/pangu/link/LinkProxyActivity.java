package com.tencent.pangu.link;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import com.tencent.pangu.utils.d;

/* compiled from: ProGuard */
public class LinkProxyActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private Uri f3776a = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3776a = getIntent().getData();
        if (this.f3776a != null) {
            a();
        }
        finish();
    }

    private void a() {
        b.a(this, this.f3776a, d.a(getIntent()));
    }
}
