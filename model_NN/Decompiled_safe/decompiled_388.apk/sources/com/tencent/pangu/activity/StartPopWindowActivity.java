package com.tencent.pangu.activity;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.a.a;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.PopUpInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.adapter.StartPopWindowGridViewAdapter;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;
import com.tencent.pangu.utils.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class StartPopWindowActivity extends BaseActivity {
    private TextView A;
    private TextView B;
    /* access modifiers changed from: private */
    public TextView C;
    private TextView D;
    private TextView E = null;
    /* access modifiers changed from: private */
    public StartPopWindowGridViewAdapter F = null;
    private PopUpInfo G = null;
    private boolean H = false;
    private GetPhoneUserAppListResponse I;
    /* access modifiers changed from: private */
    public List<SimpleAppModel> J = null;
    /* access modifiers changed from: private */
    public Context K;
    private boolean L = false;
    private boolean M = false;
    private boolean N = false;
    private long O = -1;
    cy n = new cy(this, null);
    private View u;
    /* access modifiers changed from: private */
    public View v;
    private View w;
    private View x;
    private TextView y = null;
    /* access modifiers changed from: private */
    public GridView z = null;

    public int f() {
        if (this.H && this.I != null) {
            switch (this.I.b) {
                case 1:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_OLD_CHANGE;
                case 2:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_OLD_POP;
                case 3:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_NEW_CHANGE;
                case 4:
                    return STConst.ST_PAGE_GUIDE_RESULT_BIBEI_NEW_POP;
            }
        }
        return STConst.ST_PAGE_NECESSARY_NORMAL;
    }

    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        x();
        super.onCreate(bundle);
        this.K = this;
        try {
            setContentView((int) R.layout.start_pop_window_layout);
            getWindow().setLayout(-1, -1);
            y();
            v();
            XLog.v("recommendId", "startPop--onCreate--");
            a(new cn(this));
        } catch (Exception e) {
            finish();
            this.L = true;
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        STInfoV2 p = p();
        c.k();
        p.status = c.e() ? "01" : "02";
        l.a(p);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.L) {
            z();
            u();
        }
    }

    private void v() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(this.n, intentFilter);
    }

    /* access modifiers changed from: private */
    public void w() {
        XLog.v("recommendId", "startPop--activityExposureST--");
        if (this.J != null && this.J.size() > 0) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.J.size()) {
                    SimpleAppModel simpleAppModel = this.J.get(i2);
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.K, 100);
                    if (buildSTInfo != null) {
                        if (simpleAppModel != null) {
                            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
                            buildSTInfo.extraData = simpleAppModel.c + "|" + simpleAppModel.g;
                        }
                        buildSTInfo.slotId = b(i2);
                        l.a(buildSTInfo);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private String b(int i) {
        return "03_" + bm.a(i + 1);
    }

    private void x() {
        try {
            this.H = getIntent().getBooleanExtra("extra_is_recover", false);
            if (this.H) {
                this.I = (GetPhoneUserAppListResponse) getIntent().getSerializableExtra("extra_pop_info");
                if (getIntent().getBooleanExtra(a.G, false)) {
                    e.a();
                    com.tencent.assistantv2.st.page.c.a(122, 14, false);
                }
                this.M = m.a().a("key_re_app_list_state", 0) == RecoverAppListReceiver.RecoverAppListState.SECONDLY_SHORT.ordinal();
                this.N = getIntent().getBooleanExtra("extra_special_title", false);
                m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal()));
                MainActivity.z = RecoverAppListReceiver.RecoverAppListState.NOMAL.ordinal();
                return;
            }
            this.G = (PopUpInfo) getIntent().getSerializableExtra("extra_pop_info");
        } catch (Throwable th) {
            finish();
        }
    }

    private void y() {
        String str;
        boolean z2;
        boolean z3;
        this.u = findViewById(R.id.page);
        this.u.setBackgroundColor(getResources().getColor(R.color.start_pop_window_bg_color));
        this.y = (TextView) findViewById(R.id.title);
        this.v = findViewById(R.id.addtion_view);
        this.w = findViewById(R.id.addtion_view_2);
        this.C = (TextView) findViewById(R.id.all_select_txt);
        this.D = (TextView) findViewById(R.id.more_bibei_txt);
        this.x = findViewById(R.id.btnLayout);
        if (this.H) {
            this.J = k.b(this.I.c);
            if (this.I != null) {
                switch (this.I.b) {
                    case 0:
                        this.y.setText((int) R.string.pop_title_download_new_apps);
                        this.C.setVisibility(8);
                        break;
                    case 1:
                        if ((this.I.e == 0 || (System.currentTimeMillis() / 1000) - this.I.e <= 2592000) && !TextUtils.isEmpty(this.I.d)) {
                            this.y.setText(getString(R.string.pop_title_recover_old_phone_apps, new Object[]{this.I.d}));
                        } else {
                            this.y.setText((int) R.string.pop_title_download_old_time_apps);
                        }
                        this.D.setVisibility(8);
                        break;
                    case 2:
                        this.y.setText((int) R.string.pop_title_download_old_apps);
                        this.D.setVisibility(8);
                        break;
                    default:
                        this.y.setText((int) R.string.pop_title_download_new_apps);
                        this.C.setVisibility(8);
                        break;
                }
            }
            if (this.N || !(!this.M || this.I.b == 2 || this.I.b == 1)) {
                this.y.setText((int) R.string.pop_title_download_new_apps_x);
            }
        } else {
            this.C.setVisibility(8);
            TextView textView = this.y;
            if (this.G == null) {
                str = Constants.STR_EMPTY;
            } else {
                str = this.G.b;
            }
            textView.setText(str);
            if (this.G != null) {
                this.J = k.b(this.G.k);
            }
        }
        ImageView imageView = (ImageView) findViewById(R.id.cancel_btn);
        imageView.setTag(R.id.tma_st_slot_tag, "10_002");
        imageView.setOnClickListener(new cp(this));
        this.z = (GridView) findViewById(R.id.grid_view);
        this.z.setNumColumns(3);
        this.z.setSelector(new ColorDrawable(0));
        this.F = new StartPopWindowGridViewAdapter(this);
        if (!this.H || this.I == null || !(this.I.b == 2 || this.I.b == 1)) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (z2) {
            if (by.c() <= 480) {
                if (this.J != null && this.J.size() > 6) {
                    this.J = this.J.subList(0, 6);
                }
            } else if (this.J != null && this.J.size() > 9) {
                this.J = this.J.subList(0, 9);
            }
        }
        if (this.J != null && this.J.size() < 3) {
            this.z.setNumColumns(this.J.size());
        }
        if (this.J == null || this.J.size() <= 9) {
            z3 = false;
        } else {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.z.getLayoutParams();
            layoutParams.height = getResources().getDimensionPixelSize(R.dimen.pop_view_grideview_height);
            this.z.setLayoutParams(layoutParams);
            z3 = true;
        }
        if (this.J != null && this.J.size() > 6 && (((float) t.c) < by.a(546.0f) || (t.c == 854 && t.b == 480))) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.z.getLayoutParams();
            layoutParams2.height = ((getResources().getDimensionPixelSize(R.dimen.pop_view_grideview_height) / 3) * 2) + getResources().getDimensionPixelSize(R.dimen.curcor_add);
            this.z.setLayoutParams(layoutParams2);
            z3 = true;
        }
        if (z3) {
            cx cxVar = new cx(this);
            if (this.z != null) {
                this.z.post(cxVar);
            }
        }
        if (this.J != null) {
            int size = this.J.size();
            long j = 0;
            for (int i = 0; i < size; i++) {
                j += this.J.get(i).k;
            }
            this.F.a(this.J, size, j);
        }
        this.z.setAdapter((ListAdapter) this.F);
        this.z.setOnItemClickListener(new cq(this));
        this.A = (TextView) findViewById(R.id.download_btn);
        this.B = (TextView) findViewById(R.id.download_while_wifi_btn);
        this.A.setTag(R.id.tma_st_slot_tag, "10_003");
        this.A.setOnClickListener(new cr(this));
        this.B.setOnClickListener(new cs(this));
        this.E = (TextView) findViewById(R.id.count_text);
        this.C.setSelected(true);
        this.C.setOnClickListener(new ct(this));
        this.D.setOnClickListener(new cu(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.e.a(android.content.Context, boolean):void
     arg types: [com.tencent.pangu.activity.StartPopWindowActivity, int]
     candidates:
      com.tencent.pangu.utils.e.a(android.content.Context, com.tencent.assistant.protocol.jce.GetPopupNecessaryResponse):void
      com.tencent.pangu.utils.e.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public void t() {
        if (this.H && this.I != null) {
            if (this.I.b == 1 || this.I.b == 3) {
                e.a((Context) this, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void u() {
        if (this.F != null && this.F.getCount() != 0) {
            SpannableString spannableString = new SpannableString("已选" + this.F.a() + "/" + this.F.getCount() + " , 共" + at.a(this.F.b()));
            spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.about_last_version_color)), 2, new String(this.F.a() + Constants.STR_EMPTY).length() + 2, 33);
            this.E.setText(spannableString);
            if (this.F.a() == 0) {
                this.B.setEnabled(false);
                this.A.setEnabled(false);
                this.A.setTextColor(getResources().getColor(R.color.state_disable));
                this.B.setTextColor(getResources().getColor(R.color.state_disable));
                return;
            }
            this.B.setEnabled(true);
            this.A.setEnabled(true);
            this.A.setTextColor(getResources().getColor(R.color.state_normal));
            this.B.setTextColor(getResources().getColor(R.color.state_normal));
        }
    }

    /* access modifiers changed from: private */
    public void z() {
        c.k();
        if (c.e()) {
            ((ImageView) this.v.findViewById(R.id.wifi_image)).setImageResource(R.drawable.pop_wifi);
            ((TextView) this.v.findViewById(R.id.wifi_text)).setText((int) R.string.pop_tip_wifi);
            A();
            return;
        }
        ((ImageView) this.v.findViewById(R.id.wifi_image)).setImageResource(R.drawable.pop_gms);
        ((TextView) this.v.findViewById(R.id.wifi_text)).setText((int) R.string.pop_tip_net);
        B();
    }

    private void A() {
        this.B.setVisibility(8);
        findViewById(R.id.bland).setVisibility(8);
        this.x.setPadding(getResources().getDimensionPixelSize(R.dimen.pop_btn_one_padding_l_r), 0, getResources().getDimensionPixelSize(R.dimen.pop_btn_one_padding_l_r), getResources().getDimensionPixelSize(R.dimen.loading_progress_bar_size));
    }

    private void B() {
        this.B.setVisibility(0);
        findViewById(R.id.bland).setVisibility(0);
        this.x.setPadding(getResources().getDimensionPixelSize(R.dimen.app_down_item_icon_top_margin), 0, getResources().getDimensionPixelSize(R.dimen.app_down_item_icon_top_margin), getResources().getDimensionPixelSize(R.dimen.loading_progress_bar_size));
    }

    /* access modifiers changed from: private */
    public void C() {
        int i;
        View childAt;
        ArrayList<Boolean> c = this.F.c();
        ArrayList arrayList = new ArrayList();
        if (this.J != null) {
            i = -1;
            for (int i2 = 0; i2 < this.J.size(); i2++) {
                if (c.get(i2).booleanValue()) {
                    arrayList.add(this.J.get(i2));
                    a(this.J.get(i2), i2);
                    i = i2;
                }
            }
        } else {
            i = -1;
        }
        if (i > -1) {
            StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this, null);
            if (this.z != null) {
                this.z.postDelayed(new cv(this, arrayList, buildDownloadSTInfo), 500);
            }
            if (!(this.z == null || (childAt = this.z.getChildAt(i)) == null)) {
                com.tencent.assistant.utils.a.a((ImageView) childAt.findViewById(R.id.icon));
            }
            E();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(SimpleAppModel simpleAppModel, int i) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.K, 900);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = simpleAppModel != null ? simpleAppModel.c + "|" + simpleAppModel.g : Constants.STR_EMPTY;
            buildSTInfo.slotId = c(i);
            buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            l.a(buildSTInfo);
        }
    }

    private String c(int i) {
        return "03_" + bm.a(i + 1);
    }

    /* access modifiers changed from: private */
    public void D() {
        ArrayList<Boolean> c = this.F.c();
        StatInfo buildDownloadSTInfo = STInfoBuilder.buildDownloadSTInfo(this, null);
        if (this.J != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.J.size()) {
                    break;
                }
                if (c.get(i2).booleanValue()) {
                    if (this.z != null) {
                        this.z.postDelayed(new cw(this, i2, buildDownloadSTInfo), 500);
                    }
                    a(this.J.get(i2), i2);
                }
                i = i2 + 1;
            }
        }
        E();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.L) {
            unregisterReceiver(this.n);
        }
    }

    /* access modifiers changed from: private */
    public void E() {
        finish();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.O > 0 && System.currentTimeMillis() - this.O < 800) {
            return false;
        }
        E();
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.K, 200);
        buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("06", "000");
        buildSTInfo.status = "01";
        l.a(buildSTInfo);
        return true;
    }

    public boolean k() {
        return false;
    }

    public void overridePendingTransition(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return false;
    }

    public void a(Runnable runnable) {
        ah.a().postDelayed(new co(this, runnable), 2000);
    }
}
