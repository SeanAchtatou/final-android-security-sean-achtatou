package com.tencent.pangu.activity;

import android.util.Log;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class ae implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f3306a;

    ae(DownloadActivity downloadActivity) {
        this.f3306a = downloadActivity;
    }

    public void run() {
        LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(this.f3306a.H.c, true);
        Log.i("icerao", "get current apk info finish." + this.f3306a.H.c + ",local apkinfo:" + installedApkInfo);
        this.f3306a.a(installedApkInfo);
    }
}
