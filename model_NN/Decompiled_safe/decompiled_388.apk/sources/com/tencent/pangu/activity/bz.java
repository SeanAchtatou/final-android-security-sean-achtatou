package com.tencent.pangu.activity;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.st.STConst;

/* compiled from: ProGuard */
class bz implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3350a;

    bz(SearchActivity searchActivity) {
        this.f3350a = searchActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == 66 && 1 == keyEvent.getAction()) {
            if (!TextUtils.isEmpty(this.f3350a.P) && !TextUtils.isEmpty(this.f3350a.Q)) {
                this.f3350a.v.b(this.f3350a.Q);
                String unused = this.f3350a.P = (String) null;
                String unused2 = this.f3350a.Q = (String) null;
                int unused3 = this.f3350a.N = 200705;
            } else if (this.f3350a.z != null) {
                int unused4 = this.f3350a.N = this.f3350a.z.b();
            } else {
                int unused5 = this.f3350a.N = (int) STConst.ST_PAGE_SEARCH;
            }
            String b = this.f3350a.v.b();
            if (TextUtils.isEmpty(b) || TextUtils.isEmpty(b.trim())) {
                Toast.makeText(this.f3350a, (int) R.string.must_input_keyword, 0).show();
                return true;
            }
            this.f3350a.K.removeMessages(0);
            this.f3350a.G.a(this.f3350a.I);
            this.f3350a.a("004", 200);
            this.f3350a.z();
        }
        return false;
    }
}
