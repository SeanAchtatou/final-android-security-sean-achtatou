package com.tencent.pangu.activity;

import android.app.Activity;
import android.content.Intent;
import com.tencent.pangu.component.appdetail.t;

/* compiled from: ProGuard */
class b implements t {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3325a;

    b(AppDetailActivityV5 appDetailActivityV5) {
        this.f3325a = appDetailActivityV5;
    }

    public void showPermission() {
        if (this.f3325a.ae != null) {
            Intent intent = new Intent(this.f3325a.A, PermissionActivity.class);
            intent.putStringArrayListExtra("com.tencent.assistant.PERMISSION_LIST", this.f3325a.ae.f941a.f1149a.a().get(0).o);
            this.f3325a.A.startActivity(intent);
        }
    }

    public void showReport() {
        if (this.f3325a.ae != null) {
            Intent intent = new Intent(this.f3325a.A, ReportActivity.class);
            intent.putExtra("apkid", this.f3325a.ae.f941a.f1149a.b.get(0).a());
            intent.putExtra("appid", this.f3325a.ae.f941a.f1149a.f1144a.f1155a);
            this.f3325a.A.startActivity(intent);
        }
    }

    public void shareToQQ() {
        this.f3325a.v().b((Activity) this.f3325a.A, this.f3325a.a(this.f3325a.ae, this.f3325a.ac));
    }

    public void shareToQZ() {
        this.f3325a.v().a((Activity) this.f3325a.A, this.f3325a.a(this.f3325a.ae, this.f3325a.ac));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [android.content.Context, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void */
    public void shareToWX() {
        this.f3325a.v().a(this.f3325a.A, this.f3325a.a(this.f3325a.ae, this.f3325a.ac), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
     arg types: [android.content.Context, com.tencent.pangu.model.ShareAppModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void */
    public void shareToTimeLine() {
        this.f3325a.v().a(this.f3325a.A, this.f3325a.a(this.f3325a.ae, this.f3325a.ac), true);
    }
}
