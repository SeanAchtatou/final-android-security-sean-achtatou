package com.tencent.pangu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.SelfForceUpdateView;
import com.tencent.pangu.component.SelfNormalUpdateView;
import com.tencent.pangu.component.bk;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class SelfUpdateActivity extends BaseActivity implements bk {
    public View n;
    public RelativeLayout u;
    private boolean v = false;
    private SelfForceUpdateView w;
    private SelfNormalUpdateView x;
    private SelfUpdateManager.SelfUpdateInfo y;

    public int f() {
        if (this.y == null || this.y.y == null || Constants.STR_EMPTY.equals(this.y.y.trim())) {
            return STConst.ST_PAGE_SELF_UPDATE;
        }
        return STConst.ST_PAGE_SELF_UPDATE_GRAY;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setContentView((int) R.layout.dialog_layout);
        getWindow().setLayout(-1, -2);
        this.n = findViewById(R.id.content_root);
        this.u = (RelativeLayout) findViewById(R.id.dialog_root);
        this.n.setOnClickListener(new cl(this));
        b(getIntent());
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b(intent);
    }

    private void b(Intent intent) {
        this.y = SelfUpdateManager.a().d();
        if (this.y != null) {
            this.v = SelfUpdateManager.a().k();
        }
        v();
    }

    public void onResume() {
        super.onResume();
        if (this.w != null) {
            this.w.b();
        }
        if (this.x != null) {
            this.x.c();
        }
        overridePendingTransition(-1, -1);
    }

    public void q() {
        l.a(t());
    }

    public STInfoV2 t() {
        STInfoV2 p = p();
        p.slotId = f() + Constants.STR_EMPTY;
        return p;
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(-1, -1);
    }

    public void onDestroy() {
        if (this.w != null) {
            this.w.c();
        }
        if (this.x != null) {
            this.x.d();
        }
        super.onDestroy();
    }

    private void v() {
        this.u.removeAllViews();
        if (this.y == null) {
            finish();
        } else if (this.v) {
            this.w = new SelfForceUpdateView(this);
            this.w.a(this);
            b(this.w);
            this.w.a();
        } else {
            this.x = new SelfNormalUpdateView(this);
            this.x.a(this);
            b(this.x);
            this.x.a();
        }
    }

    public void finish() {
        super.finish();
        overridePendingTransition(-1, -1);
    }

    public void b(View view) {
        if (view != null) {
            this.u.removeAllViews();
            this.u.addView(view);
            return;
        }
        finish();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (!this.v) {
            if (i == 4) {
                SelfUpdateManager.a().n().a();
            } else if (i == 82) {
                return true;
            }
            return super.onKeyDown(i, keyEvent);
        } else if (i == 4) {
            if (getParent() != null) {
                return getParent().moveTaskToBack(true);
            }
            return moveTaskToBack(true);
        } else if (i != 82) {
            return super.onKeyDown(i, keyEvent);
        } else {
            return true;
        }
    }

    public void u() {
        finish();
    }
}
