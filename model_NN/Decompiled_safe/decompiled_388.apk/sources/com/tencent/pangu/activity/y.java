package com.tencent.pangu.activity;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* compiled from: ProGuard */
class y implements DialogInterface.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f3398a;

    y(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f3398a = appTreasureBoxActivity;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        if (keyEvent.getAction() != 1) {
            return true;
        }
        this.f3398a.t();
        return true;
    }
}
