package com.tencent.pangu.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.TotalTabLayout;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ay;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppDetailWithComment;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.i;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.CommentReplyListActivity;
import com.tencent.nucleus.socialcontact.comment.PopViewDialog;
import com.tencent.nucleus.socialcontact.comment.bo;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.adapter.c;
import com.tencent.pangu.c.e;
import com.tencent.pangu.component.appdetail.AppBarTabView;
import com.tencent.pangu.component.appdetail.AppDetailHeaderViewV5;
import com.tencent.pangu.component.appdetail.AppDetailLinearLayoutV5;
import com.tencent.pangu.component.appdetail.AppDetailViewV5;
import com.tencent.pangu.component.appdetail.AppdetailDownloadBar;
import com.tencent.pangu.component.appdetail.AppdetailRelatedViewV5;
import com.tencent.pangu.component.appdetail.AppdetailScrollView;
import com.tencent.pangu.component.appdetail.AppdetailTabViewV5;
import com.tencent.pangu.component.appdetail.AppdetailViewPager;
import com.tencent.pangu.component.appdetail.CommentDetailTabView;
import com.tencent.pangu.component.appdetail.CustomLinearLayout;
import com.tencent.pangu.component.appdetail.bb;
import com.tencent.pangu.component.appdetail.bh;
import com.tencent.pangu.component.appdetail.f;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.pangu.component.appdetail.process.m;
import com.tencent.pangu.component.appdetail.t;
import com.tencent.pangu.component.l;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.manager.b;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.module.a;
import com.tencent.pangu.module.d;
import com.tencent.pangu.module.s;
import com.tencent.pangu.module.v;
import com.tencent.pangu.module.y;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class AppDetailActivityV5 extends ShareBaseActivity implements UIEventListener, bo {
    public static ArrayList<String> x = new ArrayList<>();
    /* access modifiers changed from: private */
    public Context A;
    /* access modifiers changed from: private */
    public AppdetailViewPager B;
    private c C;
    private List<View> D = new ArrayList();
    private AppdetailScrollView E;
    /* access modifiers changed from: private */
    public AppDetailLinearLayoutV5 F;
    /* access modifiers changed from: private */
    public AppDetailHeaderViewV5 G;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 H;
    /* access modifiers changed from: private */
    public AppdetailTabViewV5 I;
    /* access modifiers changed from: private */
    public AppdetailRelatedViewV5 J;
    private GetRecommendAppListResponse K;
    private v L = new v();
    /* access modifiers changed from: private */
    public CommentDetailTabView M;
    /* access modifiers changed from: private */
    public boolean N = false;
    private CustomLinearLayout O;
    /* access modifiers changed from: private */
    public TotalTabLayout P;
    private int[] Q = {R.string.title_app_detail, R.string.title_user_comment};
    /* access modifiers changed from: private */
    public int R = 0;
    private RelativeLayout S;
    private LoadingView T;
    private NormalErrorRecommendPage U;
    private ViewStub V;
    private ViewStub W;
    /* access modifiers changed from: private */
    public AppdetailDownloadBar X;
    private boolean Y = false;
    /* access modifiers changed from: private */
    public a Z = new a();
    private final int aA = 9;
    private final String aB = "authorModel";
    private final String aC = "hasNext";
    private final String aD = "pageContext";
    private final String aE = "pageText";
    private final String aF = "same_tag_app";
    private final String aG = "selectedCommentList";
    private final String aH = "goodTagList";
    private final String aI = "allTagList";
    private String aJ;
    private boolean aK = false;
    /* access modifiers changed from: private */
    public byte aL = 0;
    /* access modifiers changed from: private */
    public String aM = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String aN;
    /* access modifiers changed from: private */
    public byte aO = 0;
    /* access modifiers changed from: private */
    public byte aP = 0;
    /* access modifiers changed from: private */
    public LocalApkInfo aQ = null;
    private Bundle aR;
    private com.tencent.pangu.component.appdetail.process.a aS;
    /* access modifiers changed from: private */
    public AppdetailActionUIListener aT;
    /* access modifiers changed from: private */
    public TextView aU = null;
    /* access modifiers changed from: private */
    public int aV = 0;
    /* access modifiers changed from: private */
    public boolean aW = false;
    private s aX = new s();
    private y aY = new y();
    private long aZ;
    private ay aa = new ay();
    private u ab = new u(this, null);
    /* access modifiers changed from: private */
    public SimpleAppModel ac;
    /* access modifiers changed from: private */
    public ShareAppModel ad;
    /* access modifiers changed from: private */
    public com.tencent.assistant.model.c ae;
    /* access modifiers changed from: private */
    public PopViewDialog af;
    private final int ag = 3;
    /* access modifiers changed from: private */
    public int ah = 3;
    private final int ai = 0;
    private final int aj = 1;
    private final int ak = 2;
    /* access modifiers changed from: private */
    public String al;
    /* access modifiers changed from: private */
    public int am;
    /* access modifiers changed from: private */
    public int an;
    /* access modifiers changed from: private */
    public boolean ao = true;
    private STCommonInfo ap = null;
    /* access modifiers changed from: private */
    public STInfoV2 aq = null;
    /* access modifiers changed from: private */
    public boolean ar = false;
    /* access modifiers changed from: private */
    public boolean as = false;
    private final int at = 1;
    private final int au = 2;
    private final int av = 3;
    private final int aw = 4;
    private final int ax = 5;
    private final int ay = 6;
    private final int az = 7;
    private boolean ba = false;
    /* access modifiers changed from: private */
    public ViewPageScrollListener bb = new n(this);
    /* access modifiers changed from: private */
    public bh bc = new o(this);
    private View.OnClickListener bd = new r(this);
    private View.OnClickListener be = new s(this);
    private f bf = new t(this);
    private t bg = new b(this);
    /* access modifiers changed from: private */
    public String bh = "12_001";
    private l bi = new c(this);
    /* access modifiers changed from: private */
    public Handler bj = new f(this);
    private bb bk = new g(this);
    private AppdetailActionUIListener bl = new h(this);
    private m bm = new i(this);
    /* access modifiers changed from: private */
    public DialogInterface.OnCancelListener bn = new j(this);
    protected AppBarTabView n;
    protected int u;
    ViewInvalidateMessage[] v = new ViewInvalidateMessage[9];
    int[] w = new int[9];
    public ArrayList<CommentTagInfo> y = new ArrayList<>();
    public v z = new e(this);

    static {
        x.add(com.tencent.assistant.a.a.f382a);
        x.add(com.tencent.assistant.a.a.b);
        x.add(com.tencent.assistant.a.a.d);
        x.add(com.tencent.assistant.a.a.e);
    }

    public int f() {
        if (this.R == 0) {
            return STConst.ST_PAGE_APP_DETAIL;
        }
        if (this.R == 1) {
            return STConst.ST_PAGE_APP_DETAIL_COMMENT;
        }
        if (this.R == 2) {
            return STConst.ST_PAGE_APP_DETAIL_APPBAR;
        }
        return STConst.ST_PAGE_APP_DETAIL;
    }

    public boolean g() {
        return false;
    }

    public void b(boolean z2) {
        t();
        this.aq.isImmediately = h.a(this.ac.au);
        this.aq.actionId = 100;
        if (z2) {
            this.aq.isImmediately = false;
        }
        com.tencent.assistantv2.st.l.a(this.aq);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        w();
        try {
            super.onCreate(bundle);
            O();
            setContentView((int) R.layout.activity_appdetail_layout_v5);
            this.A = this;
            A();
            z();
            N();
        } catch (Throwable th) {
            this.ba = true;
            com.tencent.assistant.manager.t.a().b();
            finish();
        }
    }

    private void w() {
        x();
        if (this.ac == null) {
            finish();
            return;
        }
        this.Z.register(this.ab);
        this.aa.register(this.ab);
        this.L.register(this.ab);
        this.aX.register(this.ab);
        this.aY.register(this.ab);
    }

    private void x() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.aR = extras;
            if (extras.get("st_common_data") instanceof STCommonInfo) {
                this.ap = (STCommonInfo) extras.get("st_common_data");
            }
            this.ao = extras.getBoolean("same_tag_app");
            this.aJ = extras.getString(com.tencent.assistant.a.a.p);
            this.aK = extras.getBoolean(com.tencent.assistant.a.a.t);
            this.R = bm.a(extras.getString(com.tencent.assistant.a.a.U), 0);
            this.aZ = bm.c(extras.getString(com.tencent.assistant.a.a.V));
            if (!this.N && this.R == 1) {
                M();
            }
            if (this.aK) {
                b.a().a(this.aJ);
                this.aM = extras.getString(com.tencent.assistant.a.a.I);
            }
            y();
            this.ac = (SimpleAppModel) extras.get("simpleModeInfo");
            if (this.ac == null) {
                String string = extras.getString(com.tencent.assistant.a.a.C);
                int d = bm.d(extras.getString(com.tencent.assistant.a.a.J));
                String string2 = extras.getString(com.tencent.assistant.a.a.s);
                if (!TextUtils.isEmpty(string2)) {
                    try {
                        this.aL = Byte.valueOf(string2).byteValue();
                    } catch (Exception e) {
                    }
                }
                String string3 = extras.getString(com.tencent.assistant.a.a.h);
                String string4 = extras.getString("com.tencent.assistant.PACKAGE_NAME");
                if (!TextUtils.isEmpty(string4)) {
                    if (this.ac == null) {
                        this.ac = new SimpleAppModel();
                        this.ac.c = string4;
                        this.ac.y = b(extras.getString(com.tencent.assistant.a.a.W));
                    }
                    if (!TextUtils.isEmpty(string3)) {
                        try {
                            this.ac.g = Integer.valueOf(string3).intValue();
                        } catch (Exception e2) {
                        }
                    }
                    String string5 = extras.getString("com.tencent.assistant.APK_ID");
                    if (!TextUtils.isEmpty(string5)) {
                        try {
                            this.ac.b = Long.valueOf(string5).longValue();
                        } catch (Exception e3) {
                        }
                    }
                    if (!"ANDROIDQQ".equals(string)) {
                        this.ac.ac = string;
                    }
                    this.ac.ad = d;
                    return;
                }
                long j = extras.getLong("com.tencent.assistant.APP_ID", -1);
                if (j > 0) {
                    if (this.ac == null) {
                        this.ac = new SimpleAppModel();
                        this.ac.f938a = j;
                        this.ac.y = b(extras.getString(com.tencent.assistant.a.a.W));
                    }
                    if (!TextUtils.isEmpty(string3)) {
                        try {
                            this.ac.g = Integer.valueOf(string3).intValue();
                        } catch (Exception e4) {
                        }
                    }
                    String string6 = extras.getString("com.tencent.assistant.APK_ID");
                    if (!TextUtils.isEmpty(string6)) {
                        try {
                            this.ac.b = Long.valueOf(string6).longValue();
                        } catch (Exception e5) {
                        }
                    }
                    if (!"ANDROIDQQ".equals(string)) {
                        this.ac.ac = string;
                    }
                    this.ac.ad = d;
                }
            } else if (this.ac.d()) {
                this.aL = 1;
            } else if (this.ac.e()) {
                this.aL = 2;
            } else {
                this.aL = this.ac.Q;
            }
        }
    }

    private void y() {
        String string = this.aR.getString(com.tencent.assistant.a.a.g);
        String string2 = this.aR.getString(com.tencent.assistant.a.a.s);
        if (this.aK || !TextUtils.isEmpty(string) || !TextUtils.isEmpty(string2)) {
            String string3 = this.aR.getString(com.tencent.assistant.a.a.A);
            if (!TextUtils.isEmpty(string3)) {
                this.aN = string3;
                this.aO = 1;
                this.aP = 1;
                return;
            }
            String string4 = this.aR.getString(com.tencent.assistant.a.a.B);
            if (!TextUtils.isEmpty(string4)) {
                this.aN = string4;
                this.aO = 2;
                this.aP = 1;
                return;
            }
            String string5 = this.aR.getString(com.tencent.assistant.a.a.y);
            String string6 = this.aR.getString(com.tencent.assistant.a.a.z);
            if (!TextUtils.isEmpty(string5) && !TextUtils.isEmpty(string6)) {
                this.aN = string5;
                if ("game_openId".equals(string6)) {
                    this.aO = 5;
                    byte b = 0;
                    if (!TextUtils.isEmpty(string)) {
                        try {
                            b = Byte.valueOf(string).byteValue();
                        } catch (Exception e) {
                        }
                    }
                    if ((b & 1) > 0) {
                        this.aP = 1;
                        return;
                    } else if ((b & 2) > 0) {
                        this.aP = 2;
                        return;
                    } else {
                        return;
                    }
                } else if ("code".equals(string6)) {
                    this.aO = 1;
                    this.aP = 1;
                    return;
                } else if ("qqNumber".equals(string6)) {
                    this.aO = 3;
                    this.aP = 2;
                    return;
                }
            }
        }
        if (!j.a().j()) {
            return;
        }
        if (j.a().k()) {
            this.aO = 3;
            this.aP = 2;
        } else if (j.a().l()) {
            this.aO = 6;
            this.aP = 1;
        }
    }

    private void z() {
        if (R()) {
            TemporaryThreadManager.get().start(new a(this));
        } else {
            TemporaryThreadManager.get().start(new k(this));
        }
    }

    private void A() {
        if (this.ac != null) {
            this.F = (AppDetailLinearLayoutV5) findViewById(R.id.appdetail_container);
            this.F.a(this.bf);
            this.S = (RelativeLayout) findViewById(R.id.content_view);
            this.W = (ViewStub) findViewById(R.id.loading_stub);
            this.T = (LoadingView) findViewById(R.id.loading_view);
            this.V = (ViewStub) findViewById(R.id.error_page_stub);
            this.U = (NormalErrorRecommendPage) findViewById(R.id.error_page);
            B();
            C();
            this.E = (AppdetailScrollView) findViewById(R.id.parent_scrollview);
            G();
            H();
            this.E.a(this.B.c());
            if (a(this.ae)) {
                c(true);
            } else {
                c(false);
            }
            this.aS = new com.tencent.pangu.component.appdetail.process.a();
            this.aS.a(this.bl);
            this.aU = (TextView) findViewById(R.id.bubble);
            this.aU.setVisibility(8);
            this.F.a(new l(this));
            I();
        }
    }

    private void B() {
        this.H = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        String string = this.aR.getString(com.tencent.assistant.a.a.D);
        if (!this.aK || !TextUtils.isEmpty(string)) {
            this.H.b(getResources().getString(R.string.detail_title));
        } else {
            this.H.a(getResources().getString(R.string.detail_title));
        }
        this.H.a(this.bg);
        if (a(this.ae)) {
            this.H.a(this.ac, t(), this.ae);
            if (this.ac != null && ak.a().c(this.ac.c)) {
                this.H.f();
            }
        }
        this.H.a(this);
        this.H.e();
        this.H.i();
        this.H.a();
    }

    private void C() {
        this.G = (AppDetailHeaderViewV5) findViewById(R.id.simple_msg_view);
        if (a(this.ae)) {
            this.G.a(this.ac, t(), this.ae.f941a.f1149a.b.get(0).b, this.ae.f941a.f1149a.f1144a.f, this.ae.f941a.f1149a.f1144a.e);
        }
    }

    private void D() {
        this.I = new AppdetailTabViewV5(this);
        if (this.ae != null) {
            this.I.a(this.ae, this.ac);
            this.I.a(this.B.a());
            this.I.a(this.bf);
            this.I.a(this.bk);
            this.B.a(this.I.g());
        }
        this.J = this.I.a();
        this.M = new CommentDetailTabView(this, this.bi);
        this.D.add(this.I);
        this.D.add(this.M);
    }

    private void F() {
        this.n = new AppBarTabView(this);
        this.D.add(this.n);
        if (this.C != null) {
            this.C.a(this.D);
            this.C.notifyDataSetChanged();
        }
        this.Q = new int[]{R.string.title_app_detail, R.string.title_user_comment, R.string.title_app_bar};
        if (this.P != null) {
            this.O.removeAllViews();
            G();
        }
    }

    private void G() {
        this.O = (CustomLinearLayout) findViewById(R.id.tab_view);
        this.P = new TotalTabLayout(this, this.Q);
        this.P.getTabLayout().setLayoutParams(new ViewGroup.LayoutParams(-1, by.b(38.0f)));
        this.O.addView(this.P.getTabLayout());
        this.P.init(this.R);
        this.P.setTabClickListener(this.bd);
        if (this.ae != null) {
            this.P.updateTextValue(getResources().getString(R.string.title_user_comment), "评论(" + bm.a(this.ae.f941a.b) + ")");
        }
    }

    private void H() {
        D();
        this.C = new c(this.D);
        this.B = (AppdetailViewPager) findViewById(R.id.appdetail_viewpager);
        this.B.setAdapter(this.C);
        this.B.setCurrentItem(this.R);
        this.B.setOnPageChangeListener(this.bb);
        this.M.a(this.bb);
    }

    private void I() {
        this.X = (AppdetailDownloadBar) findViewById(R.id.floating_layout);
        if (this.X != null) {
            this.X.setOnTouchListener(new m(this));
        }
        this.af = new PopViewDialog(this.A, R.style.dialog, this.ac.c, this.M);
        this.af.a(E());
        this.af.a(this.z);
        K();
        c(1);
        this.aT = this.X.f();
    }

    /* access modifiers changed from: private */
    public void c(int i) {
        switch (i) {
            case 1:
                this.X.setVisibility(0);
                this.X.b(1);
                return;
            case 2:
                this.X.setVisibility(0);
                this.X.b(2);
                return;
            case 3:
                this.X.setVisibility(8);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (z2) {
            this.S.setVisibility(0);
            if (this.T != null) {
                this.T.setVisibility(8);
            }
            if (this.U != null) {
                this.U.setVisibility(8);
                return;
            }
            return;
        }
        this.S.setVisibility(4);
        if (this.T == null) {
            this.T = (LoadingView) this.W.inflate();
        } else {
            this.T.setVisibility(0);
        }
        if (this.U != null) {
            this.U.setVisibility(8);
        }
    }

    private void d(int i) {
        this.S.setVisibility(4);
        if (this.T != null) {
            this.T.setVisibility(8);
        }
        if (this.U == null) {
            this.U = (NormalErrorRecommendPage) this.V.inflate();
            this.U.setButtonClickListener(this.be);
        } else {
            this.U.setVisibility(0);
        }
        this.U.setErrorType(i);
    }

    /* access modifiers changed from: private */
    public void J() {
        if (a(this.ae)) {
            c(true);
            if (this.ac != null && this.ae != null) {
                this.P.updateTextValue(getResources().getString(R.string.title_user_comment), "评论(" + bm.a(this.ae.f941a.b) + ")");
                this.ac.a(this.ae.b);
                this.ac.a(this.ae.f941a);
                P();
                String string = this.aR.getString(com.tencent.assistant.a.a.D);
                if (!this.aK || !TextUtils.isEmpty(string)) {
                    this.H.b(getResources().getString(R.string.detail_title));
                } else {
                    this.H.a(getResources().getString(R.string.detail_title));
                }
                if (!R()) {
                    k.b(this.ac);
                } else if (!k.a(this.ae.f941a.d) || !(this.ac.b == -99 || this.ac.b == this.ae.f941a.d.r)) {
                    k.b(this.ac);
                } else {
                    k.a(this.ae.f941a.d, this.ac);
                }
                this.ac.af = this.aM;
                this.ac.q = this.ae.f941a.f1149a.f1144a.h.b;
                this.G.a(this.ac, t(), this.ae.f941a.f1149a.b.get(0).b, this.ae.f941a.f1149a.f1144a.f, this.ae.f941a.f1149a.f1144a.e);
                this.H.a(this.ac, t(), this.ae);
                if (this.ac != null && ak.a().c(this.ac.c)) {
                    this.H.f();
                }
                this.I.a(this.ae, this.ac);
                this.I.a(this.B.a());
                this.I.a(this.bf);
                this.I.a(this.bk);
                this.B.a(this.I.g());
                K();
                this.H.b(this.ac.g());
                if (!TextUtils.isEmpty(this.ae.f941a.g)) {
                    F();
                }
            }
        }
    }

    private void K() {
        if (this.ae != null) {
            long j = this.ae.f941a.f1149a.b.get(0).f1137a;
            boolean z2 = this.ae.f941a.c;
            this.af.a(this.ac.g, this.ac.f);
            this.af.a(this.ae.b());
            this.aS.a(this.ac, j, z2, this.af, t(), this.aR, this.aK, this.ae.f941a.e, this.A, this.ae.f941a.f, this.bm);
            this.aS.a(a(this.ae, this.ac));
            this.X.a(this.aS);
            this.X.i = this.R + 1;
            this.X.j = this.aV;
            if (!this.Y) {
                this.Y = true;
                this.X.b();
                this.X.a(this.I.d(), this.E, this.I.b(), AppDetailViewV5.a(this.ae, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE);
                String string = this.aR.getString(com.tencent.assistant.a.a.e);
                if (!TextUtils.isEmpty(string)) {
                    String[] split = string.split(";");
                    ArrayList arrayList = new ArrayList();
                    for (String add : split) {
                        arrayList.add(add);
                    }
                    if (arrayList.contains("1") && this.aV != 3) {
                        this.X.c(true);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public ShareAppModel a(com.tencent.assistant.model.c cVar, SimpleAppModel simpleAppModel) {
        if (cVar == null) {
            return null;
        }
        if (this.ad == null) {
            this.ad = new ShareAppModel();
        }
        this.ad.f3879a = cVar.f941a.f1149a.f1144a.e;
        this.ad.c = u();
        this.ad.d = this.aL;
        this.ad.e = simpleAppModel.f938a;
        this.ad.g = simpleAppModel.d;
        this.ad.h = simpleAppModel.q;
        this.ad.i = simpleAppModel.p;
        this.ad.j = simpleAppModel.k;
        this.ad.f = simpleAppModel.e;
        if (!TextUtils.isEmpty(simpleAppModel.ai)) {
            this.ad.l = simpleAppModel.ai;
        } else {
            this.ad.l = cVar.f941a.i;
        }
        if (!TextUtils.isEmpty(simpleAppModel.aj)) {
            this.ad.m = simpleAppModel.aj;
        } else {
            this.ad.m = cVar.f941a.j;
        }
        return this.ad;
    }

    private void L() {
        this.J = this.I.a();
        this.J.a(this.K);
    }

    /* access modifiers changed from: private */
    public void M() {
        if (a(this.ae) && !this.N) {
            HashMap hashMap = new HashMap();
            hashMap.put("simpleModeInfo", this.ac);
            hashMap.put("ratingInfo", this.ae.f941a.f1149a.f1144a.h);
            hashMap.put("apkId", Long.valueOf(this.ae.f941a.f1149a.b.get(0).f1137a));
            hashMap.put("count", Long.valueOf(this.ae.f941a.b));
            hashMap.put("versionCode", Integer.valueOf(this.ae.f941a.f1149a.b.get(0).c));
            hashMap.put("replyId", Long.valueOf(this.aZ));
            hashMap.put("alltaglist", this.y);
            this.M.a(hashMap);
            this.N = true;
        }
    }

    /* access modifiers changed from: private */
    public void N() {
        if (this.ac != null) {
            InstalledAppItem installedAppItem = new InstalledAppItem();
            installedAppItem.b = this.ac.f938a;
            if (this.ac.c == null) {
                installedAppItem.f1395a = Constants.STR_EMPTY;
            } else {
                installedAppItem.f1395a = this.ac.c;
            }
            this.w[1] = this.aa.a(1, installedAppItem, null, null, (byte) 0, Constants.STR_EMPTY);
            this.w[2] = this.L.a(this.ac.c);
            this.w[6] = this.aX.a(this.ac);
            this.w[7] = this.aY.a(this.ac, j.a().j(), this.ac.f938a, j.a().n());
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, com.tencent.assistant.model.c cVar, int i3) {
        d a2;
        if (this.w[3] != i) {
            e(10);
        } else if (i2 == 0) {
            if (cVar == null || !a(cVar)) {
                String string = this.aR.getString(com.tencent.assistant.a.a.D);
                if (!this.aK || !TextUtils.isEmpty(string)) {
                    d(80);
                } else {
                    e(10);
                    d(70);
                }
                this.U.setActivityPageId(STConst.ST_PAGE_APP_DETAIL_APP_NOT_EXIST);
            } else {
                this.aV = i3;
                this.ae = cVar;
                J();
                if (!this.N && this.R == 1) {
                    M();
                }
                this.af.a(a(cVar, this.ac));
                this.J = this.I.a();
                if (this.J != null) {
                    this.J.a(i3);
                    this.J.a(cVar.f941a.f1149a.f1144a.f);
                }
                b(false);
                if (!TextUtils.isEmpty(cVar.f941a.f1149a.f1144a.v)) {
                    this.G.a(cVar.f941a.f1149a.f1144a.v);
                } else if (AppDetailViewV5.a(cVar, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE && cVar.f941a.f1149a.b.get(0).F > 10000) {
                    this.G.a(String.format(getResources().getString(R.string.appdetail_friends_update), new BigDecimal(((double) cVar.f941a.f1149a.b.get(0).F) / 10000.0d).setScale(1, 4)));
                }
                this.aW = true;
                if (this.v[2] != null) {
                    this.bb.sendMessage(this.v[2]);
                }
                if (this.v[1] != null) {
                    this.bb.sendMessage(this.v[1]);
                }
                if (this.v[4] != null) {
                    this.bb.sendMessage(this.v[4]);
                }
                if (this.v[5] != null) {
                    this.bb.sendMessage(this.v[5]);
                }
                if (this.v[6] != null) {
                    this.bb.sendMessage(this.v[6]);
                }
                if (!(this.v[7] == null || AppDetailViewV5.a(cVar, this.ac) == AppDetailViewV5.APPDETAIL_MODE.NEED_UPDATE)) {
                    this.bb.sendMessage(this.v[7]);
                }
            }
            this.ah = 3;
        } else if (-800 == i2) {
            d(30);
            this.U.setActivityPageId(this.U.checkNoNetworkExistInstallApps() ? STConst.ST_PAGE_APP_DETAIL_NO_NETWORK_TO_UPDATE : STConst.ST_PAGE_APP_DETAIL_NO_NETWORK_TO_MANAGER);
            e(10);
        } else if (this.ah <= 0) {
            d(20);
            e(10);
        } else {
            if (R()) {
                a2 = this.Z.a(this.ac, this.aQ, this.aL, this.aN, this.aO, this.aP);
            } else {
                a2 = this.Z.a(this.ac, this.aL, this.aN, this.aO, this.aP);
            }
            this.w[3] = a2.b;
            this.ah--;
        }
    }

    private void O() {
        this.aq = STInfoBuilder.buildSTInfo(this, this.ac, STConst.ST_DEFAULT_SLOT, 100, null);
        if (this.aq == null) {
            this.aq = new STInfoV2(f(), STConst.ST_DEFAULT_SLOT, m(), STConst.ST_DEFAULT_SLOT, 100);
        }
        this.aq.updateWithExternalPara(this.q);
        if (this.ap != null) {
            this.aq.contentId = this.ap.contentId;
            this.aq.searchPreId = this.ap.searchPreId;
            this.aq.searchId = this.ap.searchId;
            this.aq.expatiation = this.ap.expatiation;
            this.aq.pushInfo = this.ap.pushInfo;
        }
    }

    private void P() {
        if (this.aq != null) {
            this.aq.updateWithSimpleAppModel(this.ac);
        }
    }

    public STInfoV2 t() {
        if (this.aq == null) {
            this.aq = STInfoBuilder.buildSTInfo(this, this.ac, STConst.ST_DEFAULT_SLOT, 100, null);
            this.aq.updateWithExternalPara(this.q);
        }
        this.aq.scene = f();
        this.aq.isImmediately = false;
        this.aq.status = STConst.ST_STATUS_DEFAULT;
        return this.aq;
    }

    private void e(int i) {
        String string = this.aR.getString(com.tencent.assistant.a.a.j);
        String string2 = this.aR.getString(com.tencent.assistant.a.a.h);
        String string3 = this.aR.getString("com.tencent.assistant.PACKAGE_NAME");
        if (!TextUtils.isEmpty(string)) {
            com.tencent.assistantv2.st.l.a("StatIpcToAppDetail", this.aR.getString(com.tencent.assistant.a.a.ac), h.a(this.aR.getString(com.tencent.assistant.a.a.n), this.aR.getString(com.tencent.assistant.a.a.o), this.aR.getString(com.tencent.assistant.a.a.C), string, this.aR.getString(com.tencent.assistant.a.a.y), this.ac != null ? String.valueOf(this.ac.f938a) : Constants.STR_EMPTY, this.aR.getString(com.tencent.assistant.a.a.ad), string3, string2) + "_" + i);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, GetRecommendAppListResponse getRecommendAppListResponse) {
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1);
        viewInvalidateMessage.arg1 = i;
        if (i == 0) {
            if (getRecommendAppListResponse == null || getRecommendAppListResponse.b.size() == 0) {
                viewInvalidateMessage.arg1 = -1;
            } else {
                this.K = getRecommendAppListResponse;
            }
        }
        this.bb.sendMessage(viewInvalidateMessage);
    }

    /* access modifiers changed from: private */
    public void f(int i) {
        L();
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2, List<SimpleAppModel> list, boolean z3, byte[] bArr) {
        if (z2) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(2);
            viewInvalidateMessage.arg1 = i2;
            HashMap hashMap = new HashMap();
            viewInvalidateMessage.params = hashMap;
            hashMap.put("hasNext", Boolean.valueOf(z3));
            hashMap.put("pageContext", bArr);
            if (i2 == 0) {
                hashMap.put("authorModel", list);
            }
            this.bb.sendMessage(viewInvalidateMessage);
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, List<SimpleAppModel> list, boolean z2, byte[] bArr) {
        String str;
        this.J = this.I.a();
        if (i != 0 || list == null) {
            this.J.a(null, null, null, null, false);
            return;
        }
        if (this.ae != null) {
            str = this.ae.f941a.f1149a.f1144a.f;
        } else {
            str = null;
        }
        this.J.a(this.ac.c, str, new ArrayList(list), bArr, z2);
    }

    /* access modifiers changed from: private */
    public boolean Q() {
        if (!this.aK || this.aV != 0 || !TextUtils.isEmpty(this.aR.getString(com.tencent.assistant.a.a.D)) || DownloadProxy.a().a(this.ac) == null) {
            return false;
        }
        return true;
    }

    public static boolean a(com.tencent.assistant.model.c cVar) {
        AppDetailWithComment appDetailWithComment;
        if (cVar == null || cVar.f941a == null || (appDetailWithComment = cVar.f941a) == null || appDetailWithComment.f1149a == null || appDetailWithComment.f1149a.f1144a == null || appDetailWithComment.f1149a.b == null || appDetailWithComment.f1149a.b.size() == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.ba) {
            this.G.b();
            this.M.e();
            this.X.d();
            if (!this.as) {
                this.bj.sendEmptyMessageDelayed(1, 100);
            }
            this.H.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.ba) {
            this.G.a();
            if (this.R == 0 && this.I != null && this.ar) {
                this.I.e();
                this.I.requestLayout();
                this.ar = false;
            }
            this.as = false;
            this.M.b();
            this.X.c();
            this.H.l();
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_SUCCESS, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_FAIL, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().addUIEventListener(1013, this);
            AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
            if (this.X != null && this.X.a() == 1) {
                this.X.c(true);
                this.X.a(2);
            }
            if (this.J != null && this.aW) {
                this.J.c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!this.ba) {
            if (this.X != null) {
                this.X.e();
                this.Y = false;
            }
            if (this.J != null) {
                this.J.b();
            }
            if (this.af != null && this.af.isShowing()) {
                this.af.dismiss();
            }
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_SUCCESS, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_COLLECTION_FAIL, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
            AstApp.i().k().removeUIEventListener(1013, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (intent != null) {
            CommentDetail commentDetail = (CommentDetail) intent.getSerializableExtra(CommentReplyListActivity.y);
            j a2 = j.a();
            if (commentDetail != null) {
                switch (i) {
                    case 100:
                        if (this.M == null) {
                            return;
                        }
                        if (a2.j()) {
                            this.M.j();
                            return;
                        } else {
                            this.M.a(commentDetail.h, commentDetail.v, commentDetail.s, commentDetail.r);
                            return;
                        }
                    default:
                        return;
                }
            }
        }
    }

    public void a(String str, int i, int i2, boolean z2) {
        this.al = str;
        this.am = i;
        this.an = i2;
    }

    /* access modifiers changed from: private */
    public boolean R() {
        return this.aK;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (a(this.ae)) {
                    long j = this.ae.f941a.f1149a.b.get(0).f1137a;
                    boolean z2 = this.ae.f941a.c;
                    byte b = this.ae.f941a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.a(this.aS);
                    return;
                }
                return;
            case 1010:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                String str = Constants.STR_EMPTY;
                if (message.obj != null && (message.obj instanceof String)) {
                    str = (String) message.obj;
                }
                if (a(this.ae) && str.equals(this.ae.a().c)) {
                    this.aQ = ApkResourceManager.getInstance().getInstalledApkInfo(this.ac.c, true);
                    long j2 = this.ae.f941a.f1149a.b.get(0).f1137a;
                    boolean z3 = this.ae.f941a.c;
                    byte b2 = this.ae.f941a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.a(this.aS);
                    if (this.aQ == null) {
                        this.X.b(false);
                    } else {
                        this.X.b(this.aQ.mVersionCode == this.ae.a().g);
                    }
                    if (this.X.i == 2) {
                        this.X.b(2);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                String str2 = Constants.STR_EMPTY;
                if (message.obj != null && (message.obj instanceof String)) {
                    str2 = (String) message.obj;
                }
                if (a(this.ae) && str2.equals(this.ae.a().c)) {
                    this.X.b(false);
                    long j3 = this.ae.f941a.f1149a.b.get(0).f1137a;
                    boolean z4 = this.ae.f941a.c;
                    byte b3 = this.ae.f941a.e;
                    this.aS.a(a(this.ae, this.ac));
                    this.X.a(this.aS);
                    if (this.X.i == 2) {
                        this.X.b(2);
                        return;
                    }
                    return;
                }
                return;
        }
    }

    public Bundle u() {
        if (this.aR == null || !a(this.ae)) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString(com.tencent.assistant.a.a.c, this.ac.c);
        bundle.putString(com.tencent.assistant.a.a.h, this.ac.g + Constants.STR_EMPTY);
        bundle.putString(com.tencent.assistant.a.a.C, this.ac.ac);
        bundle.putString(com.tencent.assistant.a.a.s, ((int) this.aL) + Constants.STR_EMPTY);
        if (!this.aR.isEmpty()) {
            for (String next : this.aR.keySet()) {
                if (x.contains(next)) {
                    bundle.putString(next, this.aR.getString(next));
                }
            }
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public e v() {
        e E2 = E();
        E2.b(true);
        E2.a(true);
        return E2;
    }

    private byte[] b(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return i.a(str, 0);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
