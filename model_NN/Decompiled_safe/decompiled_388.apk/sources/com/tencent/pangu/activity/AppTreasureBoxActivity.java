package com.tencent.pangu.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.t;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.treasurebox.AppTreasureBoxView;
import com.tencent.pangu.component.treasurebox.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.model.h;
import com.tencent.pangu.module.wisedownload.u;
import com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.pangu.utils.installuninstall.p;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppTreasureBoxActivity extends BaseActivity implements UIEventListener, a {
    /* access modifiers changed from: private */
    public Dialog A;
    /* access modifiers changed from: private */
    public Dialog B;
    private int C = 50;
    private String D = null;
    private b E = null;
    /* access modifiers changed from: private */
    public Context n;
    private TextView u;
    private TextView v;
    private LinearLayout w;
    private AppTreasureBoxView x;
    /* access modifiers changed from: private */
    public View y;
    private List<h> z = new ArrayList();

    public int f() {
        return STConst.ST_PAGE_APP_TREASURE_BOX;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = this;
        getWindow().setWindowAnimations(2131427367);
        x();
    }

    private void x() {
        this.y = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.treasure_box_inner_page, (ViewGroup) null);
        AutoDownloadCfg j = i.y().j();
        if (j != null) {
            this.D = j.k;
        }
        this.u = (TextView) this.y.findViewById(R.id.title_title);
        this.u.setText((int) R.string.app_treasure_box_title_txt);
        this.v = (TextView) this.y.findViewById(R.id.title_desc);
        if (!TextUtils.isEmpty(this.D)) {
            this.v.setText(this.D);
        }
        this.w = (LinearLayout) this.y.findViewById(R.id.title_close_area);
        this.w.setOnClickListener(new w(this));
        this.w.setOnClickListener(new x(this));
        this.x = (AppTreasureBoxView) this.y.findViewById(R.id.box);
        this.A = new Dialog(this, R.style.TreasureBoxDialog);
        this.A.setContentView(this.y);
        this.A.setCanceledOnTouchOutside(false);
        this.A.setOnKeyListener(new y(this));
        this.A.setOnDismissListener(new z(this));
        this.C = ((int) TypedValue.applyDimension(1, 50.0f, getResources().getDisplayMetrics())) + r.e;
        ah.a().postDelayed(new aa(this), 0);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
    }

    public void onResume() {
        A();
        this.x.a(this.z, this);
        this.x.a();
        this.y.invalidate();
        super.onResume();
    }

    public void onPause() {
        this.x.b();
        super.onPause();
    }

    public void onDestroy() {
        this.x.c();
        if (this.A != null) {
            this.A.dismiss();
            this.A = null;
        }
        if (this.B != null) {
            this.B.dismiss();
            this.B = null;
        }
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(1027, this);
        super.onDestroy();
    }

    public void b(int i) {
        h hVar;
        if (i >= 0 && i < this.z.size() && (hVar = this.z.get(i)) != null) {
            hVar.b = true;
            d(i, 200);
        }
    }

    public void t() {
        if (this.z == null || this.z.isEmpty()) {
            w();
            return;
        }
        if (this.y != null) {
            this.y.setVisibility(4);
        }
        if (this.B == null) {
            this.B = v();
        }
        if (this.B == null || m.a().R() >= 2) {
            w();
            return;
        }
        this.B.setOnKeyListener(new ab(this));
        if (!isFinishing()) {
            this.B.show();
        }
        m.a().Q();
        l.a(new STInfoV2(STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, Constants.STR_EMPTY, m(), STConst.ST_DEFAULT_SLOT, 100));
    }

    public void a(int i, int i2) {
        SimpleAppModel simpleAppModel;
        AppConst.AppState appState = null;
        if (i >= 0 && i < this.z.size()) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.n, 200);
            String str = STConst.ST_STATUS_DEFAULT;
            String str2 = "001";
            if (i == 1) {
                str = "03";
                str2 = "001";
            } else if (i == 2) {
                str = "04";
                str2 = "001";
            } else if (i == 3) {
                str = "05";
                str2 = "001";
            }
            buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a(str, str2);
            h hVar = this.z.get(i);
            if (hVar == null || hVar.f3889a == null) {
                simpleAppModel = null;
            } else {
                simpleAppModel = k.b(this.z.get(i).f3889a);
                appState = k.d(simpleAppModel);
            }
            if (i2 == 0) {
                f(i);
                buildSTInfo.status = com.tencent.assistantv2.st.page.a.a(appState, simpleAppModel);
            } else if (!(i2 != 1 || hVar == null || hVar.f3889a == null)) {
                buildSTInfo.actionId = com.tencent.assistantv2.st.page.a.a(appState);
                if (e.a(hVar.f3889a.f1166a, hVar.f3889a.d)) {
                    e(i);
                } else {
                    d(i);
                }
            }
            l.a(buildSTInfo);
        }
    }

    public void c(int i) {
        if (i >= 0 && i < this.z.size()) {
            a(this.z.get(i), i);
        }
    }

    private List<h> y() {
        List<AutoDownloadInfo> a2 = u.a();
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AutoDownloadInfo autoDownloadInfo : a2) {
            h hVar = new h();
            hVar.f3889a = autoDownloadInfo;
            arrayList.add(hVar);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void z() {
        if (this.z == null) {
            this.z = new ArrayList();
        }
        this.z.clear();
        List<h> y2 = y();
        if (y2 != null && !y2.isEmpty()) {
            this.z.addAll(y2);
        }
        this.x.a(this.z, this);
    }

    private void A() {
        DownloadInfo a2;
        if (!this.z.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (h next : this.z) {
                if (!(next == null || next.f3889a == null || (a2 = DownloadProxy.a().a(next.f3889a.f1166a, next.f3889a.d)) == null || !a2.isDownloaded())) {
                    arrayList.add(next);
                }
            }
            this.z.clear();
            if (!arrayList.isEmpty()) {
                this.z.addAll(arrayList);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.p.a(com.tencent.pangu.download.DownloadInfo, boolean):void
     arg types: [com.tencent.pangu.download.DownloadInfo, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.p.a(com.tencent.pangu.utils.installuninstall.p, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):com.tencent.assistant.AppConst$TwoBtnDialogInfo
      com.tencent.pangu.utils.installuninstall.p.a(int, int):void
      com.tencent.pangu.utils.installuninstall.p.a(com.tencent.pangu.utils.installuninstall.p, java.util.List):void
      com.tencent.pangu.utils.installuninstall.p.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.utils.installuninstall.p.a(com.tencent.pangu.download.DownloadInfo, boolean):void */
    private void d(int i) {
        h hVar = this.z.get(i);
        if (!(hVar == null || hVar.f3889a == null)) {
            DownloadInfo a2 = DownloadProxy.a().a(hVar.f3889a.f1166a, hVar.f3889a.d);
            if (a2 == null || !a2.isDownloaded()) {
                Toast.makeText(this, (int) R.string.app_treasure_box_download_toast_package_deleted, 1).show();
                this.z.remove(i);
                this.x.a(this.z, this);
            } else {
                a2.statInfo.recommendId = hVar.f3889a.m;
                p.a().a(a2, false);
            }
        }
        d(i, 305);
    }

    private void e(int i) {
        h hVar = this.z.get(i);
        if (hVar == null || hVar.f3889a == null || TextUtils.isEmpty(hVar.f3889a.f1166a) || !e.a(hVar.f3889a.f1166a, hVar.f3889a.d)) {
            Toast.makeText(this, (int) R.string.app_treasure_box_download_toast_app_uninstalled, 1).show();
            this.z.remove(i);
            this.x.a(this.z, this);
        } else {
            r.a(hVar.f3889a.f1166a, (Bundle) null);
        }
        d(i, STConstAction.ACTION_HIT_OPEN);
    }

    private void f(int i) {
        h hVar = this.z.get(i);
        if (!(hVar == null || hVar.f3889a == null)) {
            SimpleAppModel a2 = k.a(hVar.f3889a);
            if (a2 != null) {
                Intent intent = new Intent(this, AppDetailActivityV5.class);
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
                intent.putExtra("simpleModeInfo", a2);
                startActivity(intent);
            } else {
                return;
            }
        }
        d(i, 200);
    }

    private void B() {
        ArrayList arrayList = new ArrayList();
        if (this.z != null && !this.z.isEmpty()) {
            for (h next : this.z) {
                if (next.b && !e.a(next.f3889a.f1166a, next.f3889a.d)) {
                    arrayList.add(next.f3889a);
                }
            }
            if (!arrayList.isEmpty()) {
                t.a().a((List<AutoDownloadInfo>) arrayList);
            }
        }
    }

    public int u() {
        return getResources().getDisplayMetrics().heightPixels;
    }

    /* access modifiers changed from: private */
    public void C() {
        this.y.measure(0, 0);
        WindowManager.LayoutParams attributes = this.A.getWindow().getAttributes();
        attributes.x = 0;
        attributes.y = (u() - this.C) - this.y.getMeasuredHeight();
        attributes.dimAmount = 0.0f;
        attributes.alpha = 1.0f;
        attributes.gravity = 48;
        attributes.width = -1;
        attributes.height = -2;
        attributes.horizontalMargin = 16.0f;
        this.A.getWindow().setAttributes(attributes);
        this.A.getWindow().setWindowAnimations(2131427370);
        if (!isFinishing()) {
            this.A.show();
        }
        this.x.a();
    }

    public Dialog v() {
        ac acVar = new ac(this);
        acVar.hasTitle = true;
        acVar.titleRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_title);
        acVar.contentRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_content);
        acVar.lBtnTxtRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_left_btn_txt);
        acVar.rBtnTxtRes = getResources().getString(R.string.app_treasure_box_exit_confirm_dialog_right_btn_txt);
        acVar.blockCaller = true;
        return DialogUtils.get2BtnDialog(acVar);
    }

    public STInfoV2 b(int i, int i2) {
        return new STInfoV2(i, Constants.STR_EMPTY, STConst.ST_PAGE_APP_TREASURE_BOX, com.tencent.assistantv2.st.page.a.a("06", "001"), i2);
    }

    public void w() {
        if (this.y != null) {
            this.y.setVisibility(0);
            this.A.dismiss();
        }
        B();
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SHOW_TREASURE_BOX_ENTRY));
        AstApp.i().j().sendMessageDelayed(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_DISMISS_TREASURE_BOX_ENTRY), 5000);
    }

    /* access modifiers changed from: private */
    public String c(int i, int i2) {
        return '0' + String.valueOf(i) + "_" + bm.a(i2);
    }

    private void a(h hVar, int i) {
        if (hVar != null && hVar.f3889a != null) {
            String c = c(i + 1, 2);
            int f = f();
            int m = m();
            if (this.E == null) {
                this.E = new b();
            }
            STInfoV2 exposureSTInfoV2 = this.E.getExposureSTInfoV2(hVar.f3889a.m, hVar.f3889a.j, f, m, c);
            if (exposureSTInfoV2 != null) {
                l.a(exposureSTInfoV2);
            }
        }
    }

    private void d(int i, int i2) {
        if (i >= 0 && i < this.z.size()) {
            STInfoV2 sTInfoV2 = new STInfoV2(f(), c(i + 1, 1), m(), STConst.ST_DEFAULT_SLOT, i2);
            h hVar = this.z.get(i);
            if (!(hVar == null || hVar.f3889a == null)) {
                sTInfoV2.appId = hVar.f3889a == null ? 0 : hVar.f3889a.j;
                sTInfoV2.recommendId = hVar.f3889a.m;
            }
            l.a(sTInfoV2);
        }
    }

    public void handleUIEvent(Message message) {
        DownloadInfo d;
        InstallUninstallTaskBean installUninstallTaskBean = (InstallUninstallTaskBean) message.obj;
        if (installUninstallTaskBean != null && !TextUtils.isEmpty(installUninstallTaskBean.packageName) && (d = DownloadProxy.a().d(installUninstallTaskBean.downloadTicket)) != null && this.z != null && !this.z.isEmpty()) {
            h hVar = null;
            for (h next : this.z) {
                if (next.f3889a == null || TextUtils.isEmpty(d.packageName) || !d.packageName.endsWith(next.f3889a.f1166a) || d.versionCode != next.f3889a.d) {
                    next = hVar;
                }
                hVar = next;
            }
            if (hVar != null) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
                        hVar.c = true;
                        break;
                    case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                    case 1027:
                        hVar.c = false;
                        break;
                }
                this.x.a(this.z, this);
            }
        }
    }
}
