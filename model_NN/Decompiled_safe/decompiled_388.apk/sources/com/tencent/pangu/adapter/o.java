package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.mediadownload.q;

/* compiled from: ProGuard */
class o extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f3456a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    o(DownloadInfoMultiAdapter downloadInfoMultiAdapter, q qVar, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f3456a = qVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f3456a != null) {
            p pVar = new p(this);
            pVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            pVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            pVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            DialogUtils.show2BtnDialog(pVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        }
        return this.b;
    }
}
