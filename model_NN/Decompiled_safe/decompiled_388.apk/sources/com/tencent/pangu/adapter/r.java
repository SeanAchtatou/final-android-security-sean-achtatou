package com.tencent.pangu.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.model.d;

/* compiled from: ProGuard */
class r extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f3459a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ DownloadInfoMultiAdapter c;

    r(DownloadInfoMultiAdapter downloadInfoMultiAdapter, d dVar, STInfoV2 sTInfoV2) {
        this.c = downloadInfoMultiAdapter;
        this.f3459a = dVar;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.f3459a != null) {
            s sVar = new s(this);
            sVar.titleRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_title);
            sVar.contentRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm);
            sVar.rBtnTxtRes = this.c.m.getResources().getString(R.string.downloaded_delete_confirm_btn);
            DialogUtils.show2BtnDialog(sVar);
        }
    }

    public STInfoV2 getStInfo() {
        if (this.b != null) {
            this.b.actionId = 200;
            this.b.status = Constants.VIA_REPORT_TYPE_MAKE_FRIEND;
        }
        return this.b;
    }
}
