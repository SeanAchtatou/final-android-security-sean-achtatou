package com.tencent.pangu.adapter;

import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.AppConst;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class be extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchMatchAdapter f3434a;

    be(SearchMatchAdapter searchMatchAdapter) {
        this.f3434a = searchMatchAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f3434a.h.findViewWithTag(downloadInfo.downloadTicket));
    }

    public void a(View view, AppConst.AppState appState) {
        if (this.f3434a.g != null) {
            this.f3434a.g.onClick(view);
        }
    }
}
