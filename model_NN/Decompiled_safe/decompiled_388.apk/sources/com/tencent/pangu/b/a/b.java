package com.tencent.pangu.b.a;

import com.tencent.pangu.download.SimpleDownloadInfo;

/* compiled from: ProGuard */
/* synthetic */ class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f3469a = new int[SimpleDownloadInfo.DownloadState.values().length];

    static {
        try {
            f3469a[SimpleDownloadInfo.DownloadState.DOWNLOADING.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.QUEUING.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.FAIL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.PAUSED.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.DELETED.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.SUCC.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f3469a[SimpleDownloadInfo.DownloadState.INSTALLING.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
    }
}
