package com.tencent.pangu.download;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f3755a;
    final /* synthetic */ StatInfo b;
    final /* synthetic */ boolean c;
    final /* synthetic */ a d;

    b(a aVar, SimpleAppModel simpleAppModel, StatInfo statInfo, boolean z) {
        this.d = aVar;
        this.f3755a = simpleAppModel;
        this.b = statInfo;
        this.c = z;
    }

    public void run() {
        if (this.f3755a != null && this.f3755a.h <= r.d()) {
            DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(this.f3755a, this.b);
            createDownloadInfo.scene = this.b.sourceScene;
            createDownloadInfo.statInfo.recommendId = this.b.recommendId;
            if (ak.a().b(createDownloadInfo)) {
                ak.a().d(createDownloadInfo);
                return;
            }
            if (this.c) {
                createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI;
            } else if (createDownloadInfo.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI) {
                createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INIT;
            }
            DownloadProxy.a().c(createDownloadInfo);
        }
    }
}
