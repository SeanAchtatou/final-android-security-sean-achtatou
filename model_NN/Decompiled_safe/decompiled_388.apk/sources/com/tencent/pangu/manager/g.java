package com.tencent.pangu.manager;

import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f3830a;

    g(DownloadProxy downloadProxy) {
        this.f3830a = downloadProxy;
    }

    public void run() {
        InstallUninstallDialogManager unused = this.f3830a.k = new InstallUninstallDialogManager();
        this.f3830a.k.a(true);
        au.a();
        this.f3830a.b();
        this.f3830a.b = new DownloadProxy.DownloadTaskQueue(this.f3830a, null);
        Thread thread = new Thread(this.f3830a.b);
        thread.setName("Thread_DownloadProxy");
        thread.start();
    }
}
