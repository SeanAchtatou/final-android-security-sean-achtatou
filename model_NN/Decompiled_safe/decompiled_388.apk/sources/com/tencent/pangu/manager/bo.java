package com.tencent.pangu.manager;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class bo {

    /* renamed from: a  reason: collision with root package name */
    public int f3823a;
    public int b;
    public long c;
    public int d;
    public long e;
    public String f;
    final /* synthetic */ SelfUpdateManager g;

    public bo(SelfUpdateManager selfUpdateManager) {
        this.g = selfUpdateManager;
        c();
    }

    private void c() {
        b();
        d();
        XLog.e("voken", "tipInfo = " + this.f);
    }

    public void a() {
        this.c = System.currentTimeMillis();
        if (SelfUpdateManager.a().h()) {
            SelfUpdateManager.a().b(false);
        } else {
            this.b++;
        }
        XLog.e("voken", "popTimes = " + this.b);
        e();
    }

    public boolean a(int i) {
        return i == this.f3823a && this.b < this.d && System.currentTimeMillis() - this.c > this.e;
    }

    public void b() {
        this.f = m.a().B();
    }

    private void d() {
        if (!TextUtils.isEmpty(this.f)) {
            Uri parse = Uri.parse(this.f);
            if (parse.getQueryParameter("versionCode") != null) {
                this.f3823a = Integer.valueOf(parse.getQueryParameter("versionCode")).intValue();
            }
            if (parse.getQueryParameter("popTimes") != null) {
                this.b = Integer.valueOf(parse.getQueryParameter("popTimes")).intValue();
            }
            if (parse.getQueryParameter("maxpopTimes") != null) {
                this.d = Integer.valueOf(parse.getQueryParameter("maxpopTimes")).intValue();
            }
            if (parse.getQueryParameter("popTimeStamp") != null) {
                this.c = Long.valueOf(parse.getQueryParameter("popTimeStamp")).longValue();
            }
            if (parse.getQueryParameter("minIntervalTime") != null) {
                this.e = Long.valueOf(parse.getQueryParameter("minIntervalTime")).longValue();
            }
        }
    }

    private void e() {
        m.a().c(f());
    }

    private String f() {
        return "selfUpdate://popInfo?" + "versionCode" + "=" + this.f3823a + "&" + "popTimes" + "=" + this.b + "&" + "maxpopTimes" + "=" + this.d + "&" + "popTimeStamp" + "=" + this.c + "&" + "minIntervalTime" + "=" + this.e;
    }

    /* access modifiers changed from: private */
    public void a(SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        if (TextUtils.isEmpty(this.f) || selfUpdateInfo.e >= this.f3823a) {
            if (selfUpdateInfo.e > this.f3823a || (selfUpdateInfo.e == this.f3823a && this.g.l())) {
                this.f3823a = selfUpdateInfo.e;
                this.b = 0;
                this.c = 0;
            }
            this.d = selfUpdateInfo.v;
            this.e = selfUpdateInfo.w;
            e();
        }
    }
}
