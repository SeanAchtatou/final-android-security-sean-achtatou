package com.tencent.pangu.manager;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.dialog.DialogUtils;

/* compiled from: ProGuard */
final class al extends Handler {
    al(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (message.obj != null && (message.obj instanceof AppConst.TwoBtnDialogInfo)) {
                    DialogUtils.show2BtnDialog((AppConst.TwoBtnDialogInfo) message.obj);
                    return;
                }
                return;
            case 2:
                if (message.obj != null && (message.obj instanceof AppConst.OneBtnDialogInfo)) {
                    DialogUtils.show1BtnDialog((AppConst.OneBtnDialogInfo) message.obj);
                    return;
                }
                return;
            case 3:
                Toast.makeText(AstApp.i(), (int) R.string.qube_network_unavaliable, 0).show();
                return;
            case 4:
                Toast.makeText(AstApp.i(), (int) R.string.qube_is_too_old_in_server, 0).show();
                return;
            default:
                return;
        }
    }
}
