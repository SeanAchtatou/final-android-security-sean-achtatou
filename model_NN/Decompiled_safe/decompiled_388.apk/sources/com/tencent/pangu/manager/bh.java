package com.tencent.pangu.manager;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class bh implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3817a;
    final /* synthetic */ bf b;

    bh(bf bfVar, DownloadInfo downloadInfo) {
        this.b = bfVar;
        this.f3817a = downloadInfo;
    }

    public void onClick(View view) {
        XLog.d("RecommendDownloadManager", "<install> 点击，开始下载推荐应用");
        DownloadProxy.a().d(this.f3817a);
        a.a().a(this.f3817a);
        this.b.b.h();
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        this.b.b.a(STConst.ST_PAGE_INSTALL_RECOMMEND, i, "03_001", 200, null);
        au.a("rec_pop_tips_click", this.f3817a.appId);
    }
}
