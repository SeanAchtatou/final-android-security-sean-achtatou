package com.tencent.pangu.manager;

import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class az implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3808a;
    final /* synthetic */ au b;

    az(au auVar, DownloadInfo downloadInfo) {
        this.b = auVar;
        this.f3808a = downloadInfo;
    }

    public void run() {
        this.b.h = false;
        this.b.f.remove(this.f3808a);
        if (this.b.f.size() == 0) {
            this.b.g = true;
            if (this.b.b != null) {
                this.b.b.postDelayed(new ba(this), 2000);
                return;
            }
            return;
        }
        this.b.c(this.b.f.get(this.b.f.size() - 1));
    }
}
