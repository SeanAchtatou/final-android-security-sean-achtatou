package com.tencent.pangu.manager;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.i;
import com.tencent.assistant.protocol.jce.InstallPopupOrderCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: ProGuard */
public class ac {
    private static ac d = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<DownloadInfo> f3786a;
    /* access modifiers changed from: private */
    public String b = null;
    private ai c = new ai(this, null);
    /* access modifiers changed from: private */
    public ArrayList<LocalApkInfo> e = new ArrayList<>();
    private boolean f = false;
    private final int g = 1800000;
    private final int h = 8;

    public static synchronized ac a() {
        ac acVar;
        synchronized (ac.class) {
            if (d == null) {
                d = new ac();
            }
            acVar = d;
        }
        return acVar;
    }

    private ac() {
        if (this.f3786a == null) {
            this.f3786a = new ArrayList<>();
        }
        if (this.c != null) {
            ApkResourceManager.getInstance().registerApkResCallback(this.c);
        }
        d();
        e();
    }

    private boolean b() {
        return DownloadProxy.a().h() != 0;
    }

    private boolean c() {
        if (!m.a().j() || !m.a().k()) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    private void d() {
        this.f3786a.clear();
        this.f3786a.addAll(DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true));
        if (this.b == null) {
            this.b = m.a().at();
            XLog.d("AreadyRemind", "last save " + this.b);
        }
        XLog.d("AreadyRemind", "current save " + this.b);
    }

    private void a(ArrayList<DownloadInfo> arrayList) {
        ArrayList<Long> arrayList2;
        if (arrayList != null && !arrayList.isEmpty()) {
            ArrayList arrayList3 = new ArrayList();
            arrayList3.addAll(arrayList);
            Iterator<DownloadInfo> it = arrayList.iterator();
            while (it.hasNext()) {
                DownloadInfo next = it.next();
                if (next != null) {
                    long currentTimeMillis = System.currentTimeMillis();
                    if (currentTimeMillis - next.downloadEndTime > 1800000 || currentTimeMillis - next.downloadEndTime <= 0 || e.a(next.packageName, next.versionCode) || next.downloadState != SimpleDownloadInfo.DownloadState.SUCC || !e.d(next.filePath)) {
                        arrayList3.remove(next);
                    }
                }
            }
            arrayList.clear();
            arrayList.addAll(arrayList3);
            if (!arrayList.isEmpty()) {
                arrayList3.clear();
                arrayList3.addAll(arrayList);
                if (!this.f && !this.e.isEmpty()) {
                    Iterator<LocalApkInfo> it2 = this.e.iterator();
                    while (it2.hasNext()) {
                        this.b = b(this.b, it2.next().mPackageName);
                    }
                    this.f = true;
                }
                Iterator<DownloadInfo> it3 = arrayList.iterator();
                while (it3.hasNext()) {
                    DownloadInfo next2 = it3.next();
                    if (a(this.b, next2.packageName)) {
                        arrayList3.remove(next2);
                    }
                }
                arrayList.clear();
                arrayList.addAll(arrayList3);
            }
            if (!arrayList.isEmpty()) {
                try {
                    Collections.sort(arrayList);
                } catch (Exception e2) {
                }
                InstallPopupOrderCfg x = i.y().x();
                if (x != null && (arrayList2 = x.f1394a) != null && !arrayList2.isEmpty()) {
                    arrayList3.clear();
                    arrayList3.addAll(arrayList);
                    Iterator<Long> it4 = arrayList2.iterator();
                    while (it4.hasNext()) {
                        Long next3 = it4.next();
                        Iterator<DownloadInfo> it5 = arrayList.iterator();
                        while (it5.hasNext()) {
                            DownloadInfo next4 = it5.next();
                            if (next4.appId == next3.longValue()) {
                                arrayList3.remove(next4);
                                arrayList3.add(0, next4);
                            }
                        }
                    }
                    arrayList.clear();
                    arrayList.addAll(arrayList3);
                }
            }
        }
    }

    private void e() {
        TemporaryThreadManager.get().start(new ad(this));
    }

    private boolean a(String str, String str2) {
        String[] b2;
        if (TextUtils.isEmpty(str) || (b2 = bm.b(str, "|")) == null) {
            return false;
        }
        for (String str3 : b2) {
            if (!TextUtils.isEmpty(str3) && str3.equals(str2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public String b(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            String[] b2 = bm.b(str, "|");
            ArrayList arrayList = new ArrayList();
            if (b2 != null) {
                for (String str3 : b2) {
                    if (!TextUtils.isEmpty(str3) && !str3.equals(str2)) {
                        arrayList.add(str3);
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                return b(arrayList);
            }
        }
        return Constants.STR_EMPTY;
    }

    private String b(ArrayList<String> arrayList) {
        if (arrayList == null || arrayList.isEmpty()) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= arrayList.size()) {
                return sb.toString();
            }
            if (i2 < arrayList.size()) {
                sb.append(arrayList.get(i2));
                sb.append("|");
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        new Handler(AstApp.i().getBaseContext().getMainLooper()).postDelayed(new ae(this), 500);
    }

    public boolean a(Activity activity) {
        if (b() || !c()) {
            return false;
        }
        d();
        a(this.f3786a);
        if (this.f3786a.isEmpty()) {
            return false;
        }
        b(activity);
        return true;
    }

    private void b(Activity activity) {
        Dialog dialog = new Dialog(activity, R.style.dialog);
        dialog.setCancelable(true);
        dialog.addContentView(a(activity, dialog), new ViewGroup.LayoutParams(-1, -2));
        dialog.setOwnerActivity(activity);
        if (!activity.isFinishing()) {
            dialog.show();
            g();
            l.a(c(activity));
        }
    }

    private void g() {
        ArrayList arrayList = new ArrayList();
        Iterator<DownloadInfo> it = this.f3786a.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (!a(this.b, next.packageName)) {
                arrayList.add(next.packageName);
            }
        }
        String b2 = b(arrayList);
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        XLog.d("AreadyRemind", "save old " + this.b);
        sb.append(b2);
        XLog.d("AreadyRemind", "save  new" + b2);
        this.b = sb.toString();
        m.a().i(this.b);
    }

    private View a(Activity activity, Dialog dialog) {
        View inflate = activity.getLayoutInflater().inflate((int) R.layout.dialog_install_layout, (ViewGroup) null);
        Button button = (Button) inflate.findViewById(R.id.btn_exit);
        Button button2 = (Button) inflate.findViewById(R.id.btn_install);
        TextView textView = (TextView) inflate.findViewById(R.id.title_tip_1);
        String replace = this.f3786a.get(0).name.replace(" ", Constants.STR_EMPTY);
        if (this.f3786a.size() == 1) {
            if (!TextUtils.isEmpty(replace)) {
                if (replace.length() > 8) {
                    replace = replace.substring(0, 7).concat("...");
                }
                textView.setText(String.format(AstApp.i().getString(R.string.dialog_single_install_remind), replace));
            }
        } else if (!TextUtils.isEmpty(replace)) {
            if (replace.length() > 8) {
                replace = replace.substring(0, 7).concat("...");
            }
            textView.setText(String.format(AstApp.i().getString(R.string.dialog_mutil_install_remind), replace, Integer.valueOf(this.f3786a.size())));
        }
        button.setOnClickListener(new ag(this, dialog, activity));
        button2.setOnClickListener(new ah(this, dialog, activity));
        return inflate;
    }

    /* access modifiers changed from: private */
    public STInfoV2 c(Activity activity) {
        return new STInfoV2(STConst.ST_PAGE_INSTALL_REMIND_DIALOG, STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 100);
    }
}
