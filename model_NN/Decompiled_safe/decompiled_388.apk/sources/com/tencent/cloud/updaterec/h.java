package com.tencent.cloud.updaterec;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f2339a;
    final /* synthetic */ UpdateRecOneMoreListView b;

    h(UpdateRecOneMoreListView updateRecOneMoreListView, STInfoV2 sTInfoV2) {
        this.b = updateRecOneMoreListView;
        this.f2339a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.getContext(), AppDetailActivityV5.class);
        intent.putExtra("simpleModeInfo", this.b.f2333a);
        this.b.getContext().startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f2339a != null) {
            this.f2339a.actionId = 200;
            this.f2339a.status = "01";
        }
        return this.f2339a;
    }
}
