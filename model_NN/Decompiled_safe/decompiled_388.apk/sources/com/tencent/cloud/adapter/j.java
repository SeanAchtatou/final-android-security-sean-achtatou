package com.tencent.cloud.adapter;

import com.tencent.assistant.model.SimpleAppModel;
import java.util.Comparator;

/* compiled from: ProGuard */
class j implements Comparator<SimpleAppModel> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f2219a;

    j(g gVar) {
        this.f2219a = gVar;
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        return a(simpleAppModel.f938a) - a(simpleAppModel2.f938a);
    }

    private int a(long j) {
        if (this.f2219a.l == null || !this.f2219a.l.containsKey(Long.valueOf(j))) {
            return 100;
        }
        return ((Integer) this.f2219a.l.get(Long.valueOf(j))).intValue();
    }
}
