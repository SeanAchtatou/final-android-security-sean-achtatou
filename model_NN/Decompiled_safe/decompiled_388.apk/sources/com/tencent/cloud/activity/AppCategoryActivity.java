package com.tencent.cloud.activity;

import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.cloud.b.a;
import com.tencent.cloud.component.CategoryListPage;
import java.util.List;

/* compiled from: ProGuard */
public class AppCategoryActivity extends BaseActivity {
    CategoryListPage n;
    private SecondNavigationTitleViewV5 u;
    private boolean v = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.act_app_category);
            w();
            ah.a().postDelayed(new a(this), 500);
        } catch (Throwable th) {
            th.printStackTrace();
            this.v = true;
            finish();
        }
    }

    private void w() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.i();
        this.u.d(false);
        this.u.a(this);
        this.u.b(getResources().getString(R.string.app_category_title));
        this.u.d(v());
        this.n = (CategoryListPage) findViewById(R.id.clp_act_list);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.v && this.u != null) {
            this.u.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.v && this.u != null) {
            this.u.m();
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.n.a(t());
        this.n.d();
        this.n.setVisibility(0);
        this.n.a(new ViewPageScrollListener());
        List<ColorCardItem> c = a.a().c(t());
        List<AppCategory> a2 = a.a().a(t());
        List<AppCategory> b = a.a().b(t());
        AppCategoryListAdapter appCategoryListAdapter = new AppCategoryListAdapter(this, this.n, u(), null, null, a.a().b());
        appCategoryListAdapter.a(c, a2, b);
        this.n.a(appCategoryListAdapter);
        this.n.c();
    }

    /* access modifiers changed from: protected */
    public long t() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public AppCategoryListAdapter.CategoryType u() {
        return AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;
    }

    /* access modifiers changed from: protected */
    public int v() {
        return 1;
    }

    public int f() {
        return STConst.ST_PAGE_SOFTWARE_CATEGORY;
    }
}
