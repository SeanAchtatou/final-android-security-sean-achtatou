package com.tencent.connector.ipc;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.RemoteException;
import android.provider.Settings;
import com.qq.AppService.AstApp;
import com.qq.AppService.WifiPage;
import com.qq.AppService.aa;
import com.qq.AppService.w;
import com.tencent.assistant.event.EventDispatcher;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2417a = false;
    public static int[] b = null;
    private Context c;
    /* access modifiers changed from: private */
    public w d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public EventDispatcher f;
    /* access modifiers changed from: private */
    public e g;
    private ServiceConnection h;
    /* access modifiers changed from: private */
    public aa i;

    /* synthetic */ a(b bVar) {
        this();
    }

    public static a a() {
        return d.f2420a;
    }

    private a() {
        this.e = false;
        this.h = new b(this);
        this.i = new c(this);
        this.f = AstApp.i().j();
    }

    public void a(Context context) {
        if (context != null && this.d == null) {
            this.c = context.getApplicationContext();
            Intent intent = new Intent();
            intent.setClassName("com.tencent.android.qqdownloader", "com.qq.AppService.IPCService");
            intent.putExtra("appid", "com.tencent.android.qqdownloader");
            context.bindService(intent, this.h, 1);
        }
    }

    public void b() {
        if (this.c != null && this.h != null && this.e) {
            this.c.unbindService(this.h);
            this.d = null;
            this.e = false;
        }
    }

    public boolean c() {
        if (!this.e || this.d == null) {
            return false;
        }
        try {
            return this.d.a();
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean d() {
        return e() != ConnectionType.NONE;
    }

    public ConnectionType e() {
        ConnectionType connectionType = ConnectionType.NONE;
        if (!this.e || this.d == null) {
            return connectionType;
        }
        try {
            if (this.d.t()) {
                return ConnectionType.WIFI;
            }
            if (this.d.s()) {
                return ConnectionType.USB;
            }
            if (this.d.w()) {
                return ConnectionType.TRANS;
            }
            return connectionType;
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return connectionType;
        }
    }

    public long a(int[] iArr) {
        if (iArr == null || iArr.length == 0 || !this.e || this.d == null) {
            return 0;
        }
        try {
            return this.d.a(iArr);
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String f() {
        /*
            r2 = this;
            r1 = 0
            boolean r0 = r2.e
            if (r0 == 0) goto L_0x0018
            com.qq.AppService.w r0 = r2.d
            if (r0 == 0) goto L_0x0018
            com.qq.AppService.w r0 = r2.d     // Catch:{ RemoteException -> 0x0014 }
            java.lang.String r0 = r0.o()     // Catch:{ RemoteException -> 0x0014 }
        L_0x000f:
            if (r0 != 0) goto L_0x0013
            java.lang.String r0 = ""
        L_0x0013:
            return r0
        L_0x0014:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0018:
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connector.ipc.a.f():java.lang.String");
    }

    public String g() {
        if (!this.e || this.d == null) {
            return null;
        }
        try {
            return this.d.p();
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(String str) {
        if (this.e && this.d != null) {
            try {
                this.d.b(str);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void h() {
        if (this.e && this.d != null) {
            try {
                this.d.x();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void i() {
        if (this.e && this.d != null) {
            try {
                this.d.j();
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public int b(String str) {
        if (!this.e || this.d == null) {
            return 0;
        }
        try {
            return this.d.c(str);
        } catch (RemoteException e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static boolean b(Context context) {
        int i2;
        if (context != null) {
            try {
                i2 = Settings.System.getInt(context.getContentResolver(), "adb_enabled");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (i2 == 1 && i2 != -1) {
                return false;
            }
        }
        i2 = -1;
        return i2 == 1 ? true : true;
    }

    public static boolean c(Context context) {
        int d2 = d(context);
        return (d2 == -1 || d2 == 0) ? false : true;
    }

    public static int d(Context context) {
        if (context == null) {
            return -1;
        }
        return WifiPage.a(context);
    }

    public static int e(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.getType();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return -1;
    }
}
