package com.tencent.connector.qrcode.a;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.view.SurfaceHolder;
import java.io.IOException;

/* compiled from: ProGuard */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    static final int f2427a;
    private static final String b = d.class.getSimpleName();
    private static d c;
    private final Context d;
    private final b e;
    private Camera f;
    private Rect g;
    private Rect h;
    private boolean i;
    private boolean j;
    private Point k;
    private final boolean l;
    private final f m;
    private final a n;

    static {
        int i2;
        try {
            i2 = Integer.parseInt(Build.VERSION.SDK);
        } catch (NumberFormatException e2) {
            i2 = 10000;
        }
        f2427a = i2;
    }

    public static void a(Context context) {
        if (c == null) {
            c = new d(context);
        }
    }

    public static d a() {
        return c;
    }

    private d(Context context) {
        this.d = context;
        this.e = new b(context);
        this.l = Integer.parseInt(Build.VERSION.SDK) > 3;
        this.m = new f(this.e, this.l);
        this.n = new a();
    }

    public void a(SurfaceHolder surfaceHolder) {
        if (this.f == null) {
            this.f = Camera.open();
            if (this.f == null) {
                throw new IOException();
            }
            this.f.setPreviewDisplay(surfaceHolder);
            if (!this.i) {
                this.i = true;
                this.e.a(this.f);
            }
            this.e.b(this.f);
        }
    }

    public void b() {
        if (this.f != null) {
            this.f.release();
            this.f = null;
        }
    }

    public void c() {
        if (this.f != null && !this.j) {
            this.f.startPreview();
            this.j = true;
        }
    }

    public void d() {
        if (this.f != null && this.j) {
            if (!this.l) {
                this.f.setPreviewCallback(null);
            }
            this.f.stopPreview();
            this.m.a(null, 0);
            this.n.a(null, 0);
            this.j = false;
        }
    }

    public void a(Handler handler, int i2) {
        if (this.f != null && this.j) {
            this.m.a(handler, i2);
            if (this.l) {
                this.f.setOneShotPreviewCallback(this.m);
            } else {
                this.f.setPreviewCallback(this.m);
            }
        }
    }

    public void b(Handler handler, int i2) {
        if (this.f != null && this.j) {
            this.n.a(handler, i2);
            try {
                this.f.autoFocus(this.n);
            } catch (Exception e2) {
            }
        }
    }

    public Point e() {
        if (this.e != null) {
            return this.e.b();
        }
        return new Point(0, 0);
    }

    public Rect f() {
        Point c2;
        int i2;
        int i3;
        int i4 = 180;
        if (this.k != null) {
            c2 = this.k;
        } else {
            c2 = this.e.c();
        }
        Point b2 = this.e.b();
        if (this.g == null) {
            if (this.f == null || c2 == null) {
                return null;
            }
            int i5 = c2.x;
            int i6 = (int) (((float) c2.x) * ((((float) b2.y) * 1.0f) / ((float) b2.x)));
            if (i6 > c2.y) {
                int i7 = c2.y;
                i3 = (int) (((float) c2.y) * ((((float) b2.x) * 1.0f) / ((float) b2.y)));
                i2 = i7;
            } else {
                i2 = i6;
                i3 = i5;
            }
            int i8 = (i3 * 3) / 4;
            if (i8 < 180) {
                i8 = 180;
            } else if (i8 > 480) {
                i8 = 480;
            }
            int i9 = (i2 * 3) / 4;
            if (i9 >= 180) {
                if (i9 > 480) {
                    i4 = 480;
                } else {
                    i4 = i9;
                }
            }
            if (i4 < i8) {
                i8 = i4;
            }
            int i10 = (i3 - i8) / 2;
            int i11 = (i2 - i8) / 2;
            this.g = new Rect(i10, i11, i10 + i8, i8 + i11);
        }
        return this.g;
    }

    public Rect g() {
        int i2;
        int i3;
        if (this.h == null) {
            if (f() == null) {
                return null;
            }
            Rect rect = new Rect(f());
            Point b2 = this.e.b();
            Point c2 = this.k != null ? this.k : this.e.c();
            int i4 = c2.x;
            int i5 = (int) (((float) c2.x) * ((((float) b2.y) * 1.0f) / ((float) b2.x)));
            if (i5 > c2.y) {
                int i6 = c2.y;
                i3 = (int) (((float) c2.y) * ((((float) b2.x) * 1.0f) / ((float) b2.y)));
                i2 = i6;
            } else {
                i2 = i5;
                i3 = i4;
            }
            rect.left = (rect.left * b2.x) / i3;
            rect.right = (rect.right * b2.x) / i3;
            rect.top = (rect.top * b2.y) / i2;
            rect.bottom = (rect.bottom * b2.y) / i2;
            this.h = rect;
        }
        return this.h;
    }

    public e a(byte[] bArr, int i2, int i3) {
        Rect g2 = g();
        if (g2 == null) {
            throw new IllegalArgumentException("rectInPreview is null");
        }
        int d2 = this.e.d();
        String e2 = this.e.e();
        switch (d2) {
            case 16:
            case 17:
                return new e(bArr, i2, i3, g2.left, g2.top, g2.width(), g2.height());
            default:
                if ("yuv420p".equals(e2)) {
                    return new e(bArr, i2, i3, g2.left, g2.top, g2.width(), g2.height());
                }
                throw new IllegalArgumentException("Unsupported picture format: " + d2 + '/' + e2);
        }
    }
}
