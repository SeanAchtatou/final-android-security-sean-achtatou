package com.tencent.connector.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.am;

/* compiled from: ProGuard */
public class RoundCornerImageView extends TXImageView {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f2395a;
    private Paint b = new Paint();

    public RoundCornerImageView(Context context) {
        super(context);
    }

    public RoundCornerImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.f2395a = am.a(drawable);
        invalidate();
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f2395a = bitmap;
        invalidate();
    }

    public void setBackgroundResource(int i) {
        this.f2395a = am.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void setImageResource(int i) {
        this.f2395a = am.a(getContext().getResources().getDrawable(i));
        invalidate();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.f2395a != null) {
            canvas.drawBitmap(am.a(this.f2395a, getWidth(), getHeight()), 0.0f, 0.0f, this.b);
        }
    }
}
