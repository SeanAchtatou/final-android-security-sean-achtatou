package com.tencent.connector.component;

import android.view.View;
import com.qq.AppService.AppService;
import com.tencent.assistant.st.a;

/* compiled from: ProGuard */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentCurrentConnection f2404a;

    h(ContentCurrentConnection contentCurrentConnection) {
        this.f2404a = contentCurrentConnection;
    }

    public void onClick(View view) {
        a.a().c((byte) 1);
        if (AppService.r()) {
            AppService.h();
        } else if (AppService.u()) {
            AppService.j();
        } else {
            AppService.i();
        }
        AppService.a(AppService.BusinessConnectionType.NONE);
    }
}
