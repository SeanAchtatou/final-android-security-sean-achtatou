package com.tencent.wcs.proxy.b;

import com.tencent.connect.common.Constants;
import com.tencent.wcs.jce.TransDataMessage;

/* compiled from: ProGuard */
public class t extends a<TransDataMessage, Void> {
    public static t b() {
        return v.f4123a;
    }

    private t() {
    }

    /* access modifiers changed from: protected */
    public TransDataMessage a(Void... voidArr) {
        return new TransDataMessage();
    }

    /* access modifiers changed from: protected */
    public boolean b(Void... voidArr) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(TransDataMessage transDataMessage) {
        return transDataMessage != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void c(TransDataMessage transDataMessage) {
        if (transDataMessage != null) {
            transDataMessage.d(0);
            transDataMessage.f(0);
            transDataMessage.e(0);
            transDataMessage.a(0);
            transDataMessage.b(0);
            transDataMessage.c(0);
            transDataMessage.b(Constants.STR_EMPTY);
            transDataMessage.a(Constants.STR_EMPTY);
            transDataMessage.a((byte[]) null);
        }
    }
}
