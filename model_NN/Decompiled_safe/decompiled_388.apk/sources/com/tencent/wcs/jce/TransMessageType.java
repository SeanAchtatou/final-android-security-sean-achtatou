package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class TransMessageType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final TransMessageType f4107a = new TransMessageType(0, 0, "TRANS_MESSAGE_TYPE_DATA");
    public static final TransMessageType b = new TransMessageType(1, 1, "TRANS_MESSAGE_TYPE_CONNECT");
    public static final TransMessageType c = new TransMessageType(2, 2, "TRANS_MESSAGE_TYPE_CLOSE");
    public static final TransMessageType d = new TransMessageType(3, 3, "TRANS_MESSAGE_TYPE_PACKET_LOST");
    public static final TransMessageType e = new TransMessageType(4, 4, "TRANS_MESSAGE_TYPE_ACK");
    public static final TransMessageType f = new TransMessageType(5, 5, "TRANS_MESSAGE_TYPE_STOP_RESEND");
    static final /* synthetic */ boolean g = (!TransMessageType.class.desiredAssertionStatus());
    private static TransMessageType[] h = new TransMessageType[6];
    private int i;
    private String j = new String();

    public String toString() {
        return this.j;
    }

    private TransMessageType(int i2, int i3, String str) {
        this.j = str;
        this.i = i3;
        h[i2] = this;
    }
}
