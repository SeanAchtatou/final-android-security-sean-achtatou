package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class ECOMMANDErrorCode implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final ECOMMANDErrorCode f4082a = new ECOMMANDErrorCode(0, 0, "E_COMMAND_OK");
    public static final ECOMMANDErrorCode b = new ECOMMANDErrorCode(1, 1, "E_COMMAND_ID_INVALID");
    public static final ECOMMANDErrorCode c = new ECOMMANDErrorCode(2, 2, "E_COMMAND_LENGTH_ERROR");
    public static final ECOMMANDErrorCode d = new ECOMMANDErrorCode(3, 3, "E_COMMAND_PARAMETER_ERROR");
    public static final ECOMMANDErrorCode e = new ECOMMANDErrorCode(4, 4, "E_COMMAND_INIT_DCACHE_ERROR");
    public static final ECOMMANDErrorCode f = new ECOMMANDErrorCode(5, 5, "E_COMMAND_DCACHE_ERROR");
    public static final ECOMMANDErrorCode g = new ECOMMANDErrorCode(6, 6, "E_UNKNOWN_ERROR");
    public static final ECOMMANDErrorCode h = new ECOMMANDErrorCode(7, 7, "E_COMMAND_QRCODE_KEY_TIMEOUT");
    public static final ECOMMANDErrorCode i = new ECOMMANDErrorCode(8, 8, "E_COMMAND_TRANS_DATA_NULL_DATA");
    public static final ECOMMANDErrorCode j = new ECOMMANDErrorCode(9, 9, "E_COMMAND_DES_DEVICE_NOT_ONLINE");
    public static final ECOMMANDErrorCode k = new ECOMMANDErrorCode(10, 99, "E_COMMAND_NO_RESP");
    static final /* synthetic */ boolean l = (!ECOMMANDErrorCode.class.desiredAssertionStatus());
    private static ECOMMANDErrorCode[] m = new ECOMMANDErrorCode[11];
    private int n;
    private String o = new String();

    public String toString() {
        return this.o;
    }

    private ECOMMANDErrorCode(int i2, int i3, String str) {
        this.o = str;
        this.n = i3;
        m[i2] = this;
    }
}
