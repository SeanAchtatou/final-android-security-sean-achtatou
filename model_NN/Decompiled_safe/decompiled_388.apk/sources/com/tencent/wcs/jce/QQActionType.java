package com.tencent.wcs.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class QQActionType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final QQActionType f4095a = new QQActionType(0, 0, "QQ_ACTION_LOGOUT");
    public static final QQActionType b = new QQActionType(1, 1, "QQ_ACTION_LOGIN");
    static final /* synthetic */ boolean c = (!QQActionType.class.desiredAssertionStatus());
    private static QQActionType[] d = new QQActionType[2];
    private int e;
    private String f = new String();

    public String toString() {
        return this.f;
    }

    private QQActionType(int i, int i2, String str) {
        this.f = str;
        this.e = i2;
        d[i] = this;
    }
}
