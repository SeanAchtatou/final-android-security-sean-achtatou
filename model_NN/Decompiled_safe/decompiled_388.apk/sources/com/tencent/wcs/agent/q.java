package com.tencent.wcs.agent;

import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.tencent.assistant.protocol.l;
import com.tencent.wcs.b.c;
import com.tencent.wcs.jce.RecvPacketSeqList;
import java.util.ArrayList;
import java.util.Collections;

/* compiled from: ProGuard */
public class q {

    /* renamed from: a  reason: collision with root package name */
    private int f4061a = 0;
    private int b = 0;
    private SparseArray<byte[]> c = new SparseArray<>();
    private int d = 0;
    private boolean e = false;
    private final h f;
    private long g = 0;
    private final Object h = new Object();

    public q(h hVar) {
        this.f = hVar;
    }

    public void a(int i) {
        synchronized (this.c) {
            SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                int keyAt = this.c.keyAt(i2);
                if (keyAt <= i) {
                    sparseBooleanArray.append(keyAt, true);
                }
            }
            int size2 = sparseBooleanArray.size();
            for (int i3 = 0; i3 < size2; i3++) {
                int keyAt2 = sparseBooleanArray.keyAt(i3);
                this.b -= this.c.get(keyAt2).length;
                this.c.remove(keyAt2);
            }
            sparseBooleanArray.clear();
            this.c.notify();
        }
    }

    public void a(int i, byte[] bArr) {
        synchronized (this.c) {
            if (bArr != null) {
                this.c.put(i, bArr);
            }
            if (bArr != null) {
                this.b += bArr.length;
            }
        }
        synchronized (this.c) {
            if (((float) this.b) > c.g * ((float) c.f) && this.c != null) {
                try {
                    this.c.wait();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public void a(int i, int i2) {
        this.f4061a += i2;
        if (this.f4061a >= c.f) {
            this.f4061a = 0;
            this.f.a(null, i, 4);
        }
    }

    public void a(int i, int i2, ArrayList<Integer> arrayList) {
        int i3;
        if (i > i2) {
            synchronized (this.c) {
                this.c.notify();
            }
            return;
        }
        synchronized (this.c) {
            if (arrayList.isEmpty()) {
                i3 = i2 + 1;
            } else {
                Collections.sort(arrayList);
                i3 = arrayList.get(arrayList.size() - 1).intValue();
            }
            while (true) {
                if (i >= i3) {
                    break;
                }
                if (!arrayList.contains(Integer.valueOf(i))) {
                    byte[] bArr = this.c.get(i);
                    if (bArr == null) {
                        this.f.a(bArr, i, 5);
                        break;
                    }
                    this.f.a(bArr, i, 0);
                }
                i++;
            }
        }
    }

    public void a(int i, ArrayList<Integer> arrayList) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.g > ((long) (((c.d / 2) * 1000) - 100))) {
            this.f.a(l.b(new RecvPacketSeqList(arrayList)), i, 3);
            if (i > 1) {
                this.f.a(null, i - 1, 4);
            }
            this.g = currentTimeMillis;
        }
    }

    public void a() {
        synchronized (this.h) {
            this.h.notify();
        }
    }

    public boolean b() {
        long currentTimeMillis = System.currentTimeMillis();
        synchronized (this.h) {
            try {
                this.h.wait((long) (c.d * 1000));
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (System.currentTimeMillis() - currentTimeMillis <= (((long) c.d) * 1000) - 100 || this.e) {
            return false;
        }
        return true;
    }

    public boolean c() {
        this.d++;
        if (this.d <= c.c || this.e) {
            return false;
        }
        return true;
    }

    public boolean d() {
        return this.d >= c.e;
    }

    public void e() {
        this.d = 0;
    }

    public void f() {
        this.e = true;
    }

    public void g() {
        this.e = false;
    }

    public void h() {
        synchronized (this.c) {
            this.b = 0;
            this.c.clear();
            this.c.notify();
        }
        synchronized (this.h) {
            this.h.notify();
        }
    }
}
