package com.tencent.assistantv2.activity;

import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.module.callback.j;
import com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse;
import com.tencent.assistant.protocol.jce.GetUserTagInfoListResponse;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.module.timer.RecoverAppListReceiver;

/* compiled from: ProGuard */
public class g implements j, com.tencent.pangu.module.a.g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f1873a;

    public g(GuideActivity guideActivity) {
        this.f1873a = guideActivity;
    }

    public void a(int i, int i2, GetPhoneUserAppListResponse getPhoneUserAppListResponse) {
        GetPhoneUserAppListResponse unused = this.f1873a.T = getPhoneUserAppListResponse;
        try {
            XLog.i("PopUpNecessaryAcitivity", "guideActivity DataCallback!  resp:" + getPhoneUserAppListResponse);
            if (i2 == 0 && this.f1873a.T != null) {
                m.a().b(Constants.STR_EMPTY, "key_re_app_list_state", Integer.valueOf(RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal()));
                MainActivity.z = RecoverAppListReceiver.RecoverAppListState.FIRST_SHORT.ordinal();
                m.a().a("key_re_app_list_first_response", an.a(getPhoneUserAppListResponse));
                if (this.f1873a.T.b == 1 || this.f1873a.T.b == 3) {
                    boolean unused2 = this.f1873a.W = false;
                    this.f1873a.V.setText((int) R.string.guide_btn_des_switchphone);
                    this.f1873a.A.setText((int) R.string.guide_btn_go_switchphone);
                    this.f1873a.A.setVisibility(0);
                    l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_001", 0, STConst.ST_DEFAULT_SLOT, 100));
                    return;
                }
                boolean unused3 = this.f1873a.W = true;
                this.f1873a.V.setText((int) R.string.guide_btn_des_switchphone_skip);
                this.f1873a.A.setText((int) R.string.guide_btn_go_skip);
                this.f1873a.A.setVisibility(0);
                l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_NO, "04_003", 0, STConst.ST_DEFAULT_SLOT, 100));
            }
        } catch (Exception e) {
        }
    }

    public void a(int i, int i2, GetUserTagInfoListResponse getUserTagInfoListResponse) {
        if (i2 == 0 && getUserTagInfoListResponse != null && getUserTagInfoListResponse.b.size() == 5 && !this.f1873a.G.a()) {
            this.f1873a.Z.clear();
            this.f1873a.Z.addAll(getUserTagInfoListResponse.b);
            this.f1873a.G.a(this.f1873a.Z);
        }
    }
}
