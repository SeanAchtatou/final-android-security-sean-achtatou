package com.tencent.assistantv2.st.business;

import com.tencent.assistant.protocol.jce.StatNetType;
import com.tencent.assistant.protocol.l;
import com.tencent.assistant.st.h;
import org.apache.http.util.ByteArrayBuffer;

/* compiled from: ProGuard */
public class r extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private StatNetType f2043a;

    public r() {
        this.f2043a = null;
        this.f2043a = new StatNetType();
    }

    public void a() {
        this.f2043a.f1555a = 0;
        this.f2043a.b = h.a();
        byte[] b = l.b(this.f2043a);
        byte[] a2 = com.tencent.assistant.utils.r.a(b.length);
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(b.length + 4);
        byteArrayBuffer.append(a2, 0, a2.length);
        byteArrayBuffer.append(b, 0, b.length);
        if (this.b != null) {
            this.b.a(getSTType(), byteArrayBuffer.buffer());
        }
    }

    public byte getSTType() {
        return 22;
    }

    public void flush() {
    }
}
