package com.tencent.assistantv2.st.business;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.model.h;
import com.tencent.assistant.net.b;
import com.tencent.assistant.protocol.jce.AppChunkDownlaod;
import com.tencent.assistant.protocol.jce.Speed;
import com.tencent.assistant.protocol.jce.StatAppDownlaodWithChunk;
import com.tencent.assistant.protocol.jce.StatWifi;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.downloadsdk.ae;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.module.wisedownload.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class c extends BaseSTManagerV2 implements ae {

    /* renamed from: a  reason: collision with root package name */
    private static c f2032a = null;
    private ConcurrentHashMap<String, StatAppDownlaodWithChunk> c = new ConcurrentHashMap<>();
    private ArrayList<h> d = new ArrayList<>();

    public static synchronized c a() {
        c cVar;
        synchronized (c.class) {
            if (f2032a == null) {
                f2032a = new c();
            }
            cVar = f2032a;
        }
        return cVar;
    }

    private c() {
    }

    public void a(String str, long j, long j2, byte b, StatInfo statInfo, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType) {
        XLog.d("NewDownload", "******** downlaod start scene=" + statInfo.scene + " sourceScene=" + statInfo.sourceScene + " action=" + statInfo.actionId + " slotId=" + statInfo.getFinalSlotId() + " sourceslotId=" + statInfo.sourceSceneSlotId + " extraData=" + statInfo.extraData + " via=" + statInfo.callerVia + " traceId=" + statInfo.traceId + " fileType=" + downloadType + " downloadTicket =" + str + " actionFlag=" + statInfo.d);
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = null;
        if (this.c != null && this.c.containsKey(str)) {
            statAppDownlaodWithChunk = this.c.get(str);
            statAppDownlaodWithChunk.a();
        } else if (this.c != null && !this.c.containsKey(str)) {
            statAppDownlaodWithChunk = a(str, j, j2, b, statInfo, uIType, downloadType, new ArrayList());
        }
        if (statAppDownlaodWithChunk != null && this.c != null) {
            this.c.put(str, statAppDownlaodWithChunk);
        }
    }

    public StatAppDownlaodWithChunk a(String str, long j, long j2, byte b, StatInfo statInfo, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType, ArrayList<AppChunkDownlaod> arrayList) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = new StatAppDownlaodWithChunk();
        statAppDownlaodWithChunk.f1539a = j;
        statAppDownlaodWithChunk.b = j2;
        statAppDownlaodWithChunk.m = b;
        if (statInfo != null) {
            statAppDownlaodWithChunk.j = statInfo.scene;
            statAppDownlaodWithChunk.c = statInfo.f2058a;
            statAppDownlaodWithChunk.k = statInfo.sourceScene;
            statAppDownlaodWithChunk.l = statInfo.extraData;
            statAppDownlaodWithChunk.s = statInfo.callerVia;
            try {
                statAppDownlaodWithChunk.r = Long.valueOf(statInfo.callerUin).longValue();
                statAppDownlaodWithChunk.M = Integer.valueOf(statInfo.callerVersionCode).intValue();
            } catch (Throwable th) {
            }
            statAppDownlaodWithChunk.t = statInfo.b;
            statAppDownlaodWithChunk.u = statInfo.c;
            statAppDownlaodWithChunk.v = statInfo.d;
            statAppDownlaodWithChunk.w = statInfo.getFinalSlotId();
            statAppDownlaodWithChunk.O = statInfo.sourceSceneSlotId;
            statAppDownlaodWithChunk.A = statInfo.recommendId;
            statAppDownlaodWithChunk.P = statInfo.contentId;
            statAppDownlaodWithChunk.n = statInfo.searchId;
            statAppDownlaodWithChunk.L = statInfo.expatiation;
            statAppDownlaodWithChunk.K = statInfo.searchPreId + "_" + statInfo.searchId;
            statAppDownlaodWithChunk.N = statInfo.rankGroupId;
            statAppDownlaodWithChunk.E = statInfo.pushInfo;
            statAppDownlaodWithChunk.S = statInfo.traceId;
        }
        if (statInfo == null || statInfo.pushId <= 0) {
            statAppDownlaodWithChunk.D = f.b();
        } else {
            statAppDownlaodWithChunk.D = statInfo.pushId;
        }
        statAppDownlaodWithChunk.J = f.a();
        b i = com.tencent.assistant.net.c.i();
        statAppDownlaodWithChunk.F = new StatWifi(i.e, i.f, com.tencent.assistant.st.h.a());
        statAppDownlaodWithChunk.y = d();
        statAppDownlaodWithChunk.d = Global.getClientIp();
        statAppDownlaodWithChunk.h = arrayList;
        statAppDownlaodWithChunk.H = uIType.ordinal();
        if (DownloadInfo.isUiTypeWiseDownload(uIType)) {
            int a2 = m.a(uIType);
            statAppDownlaodWithChunk.k = a2;
            statAppDownlaodWithChunk.j = a2;
        }
        if (downloadType == SimpleDownloadInfo.DownloadType.PLUGIN) {
            statAppDownlaodWithChunk.G = 6;
            statAppDownlaodWithChunk.j = STConst.ST_PAGE_PLUGIN;
            statAppDownlaodWithChunk.k = STConst.ST_PAGE_PLUGIN;
        } else if (downloadType == SimpleDownloadInfo.DownloadType.VIDEO) {
            statAppDownlaodWithChunk.G = 10;
            statAppDownlaodWithChunk.T = str;
        } else if (downloadType == SimpleDownloadInfo.DownloadType.FILE) {
            statAppDownlaodWithChunk.G = 11;
            statAppDownlaodWithChunk.T = str;
        }
        statAppDownlaodWithChunk.Q = Global.getAppVersion() + "_" + Global.getBuildNo();
        statAppDownlaodWithChunk.R = com.tencent.assistant.st.h.a();
        return statAppDownlaodWithChunk;
    }

    private ArrayList<Speed> a(ArrayList<Speed> arrayList, h hVar) {
        Speed speed;
        if (arrayList == null) {
            arrayList = new ArrayList<>();
        }
        if (!(hVar == null || hVar.b() == null)) {
            Iterator<Speed> it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    speed = null;
                    break;
                }
                speed = it.next();
                if (a(speed, hVar)) {
                    break;
                }
            }
            if (speed != null) {
                speed.f1534a = hVar.b().c;
                speed.b += hVar.c();
                speed.c += hVar.b().g;
            } else {
                Speed speed2 = new Speed();
                speed2.f1534a = hVar.b().c;
                speed2.b = hVar.c();
                speed2.c = hVar.b().g;
                speed2.d = hVar.b().k;
                speed2.e = hVar.b().l;
                speed2.f = hVar.b().a();
                speed2.g = hVar.b().b;
                arrayList.add(speed2);
            }
        }
        return arrayList;
    }

    private boolean a(Speed speed, h hVar) {
        if (speed == null || hVar == null || hVar.b() == null || speed.f1534a != hVar.b().c || !speed.d.equals(hVar.b().k) || speed.e != hVar.b().l || !speed.g.equals(hVar.b().b)) {
            return false;
        }
        return true;
    }

    private void a(StatAppDownlaodWithChunk statAppDownlaodWithChunk, byte b) {
        ArrayList<Speed> arrayList;
        if (statAppDownlaodWithChunk != null) {
            statAppDownlaodWithChunk.f = com.tencent.assistant.st.h.a() - statAppDownlaodWithChunk.e;
            statAppDownlaodWithChunk.i = b;
            ArrayList arrayList2 = new ArrayList();
            ArrayList<Speed> arrayList3 = new ArrayList<>();
            if (this.d != null) {
                Iterator<h> it = this.d.iterator();
                while (true) {
                    arrayList = arrayList3;
                    if (!it.hasNext()) {
                        break;
                    }
                    h next = it.next();
                    if (next.a() == statAppDownlaodWithChunk.f1539a) {
                        arrayList2.add(next.b());
                        arrayList3 = a(arrayList, next);
                    } else {
                        arrayList3 = arrayList;
                    }
                }
            } else {
                arrayList = arrayList3;
            }
            statAppDownlaodWithChunk.b(arrayList);
            statAppDownlaodWithChunk.a(arrayList2);
        }
    }

    private void a(StatAppDownlaodWithChunk statAppDownlaodWithChunk, String str) {
        if (statAppDownlaodWithChunk != null) {
            XLog.d("NewDownload", "******** downlaod end scene=" + statAppDownlaodWithChunk.j + " sourceScene=" + statAppDownlaodWithChunk.k + " slotId=" + statAppDownlaodWithChunk.w + " sourceslotId=" + statAppDownlaodWithChunk.O + " extraData=" + statAppDownlaodWithChunk.l + " idType=" + statAppDownlaodWithChunk.G + " resourceid=" + statAppDownlaodWithChunk.T);
            byte[] a2 = com.tencent.assistant.st.h.a(statAppDownlaodWithChunk);
            if (this.b != null) {
                this.b.a(getSTType(), a2);
            }
            this.c.remove(str);
            ArrayList arrayList = new ArrayList();
            if (this.d != null) {
                Iterator<h> it = this.d.iterator();
                while (it.hasNext()) {
                    h next = it.next();
                    if (next.a() == statAppDownlaodWithChunk.f1539a) {
                        arrayList.add(next);
                    }
                }
                if (arrayList.size() > 0) {
                    this.d.removeAll(arrayList);
                }
            }
        }
    }

    public void a(int i, String str, String str2) {
    }

    public void a(int i, String str) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk;
        if (this.c != null && (statAppDownlaodWithChunk = this.c.get(str)) != null) {
            statAppDownlaodWithChunk.e = com.tencent.assistant.st.h.a();
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
    }

    public void a(int i, String str, long j, long j2, double d2) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            statAppDownlaodWithChunk.g = j;
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 1);
            statAppDownlaodWithChunk.p = i2;
            statAppDownlaodWithChunk.q = bArr;
            a(statAppDownlaodWithChunk, str);
        }
    }

    public void b(int i, String str) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 2);
            a(statAppDownlaodWithChunk, str);
        }
    }

    public void a(int i, String str, String str2, String str3) {
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (i == 100) {
            String[] split = str2.split("\\.");
            if (split.length > 1) {
                statAppDownlaodWithChunk.l = a(statAppDownlaodWithChunk.l, "ext", split[split.length - 1]);
            }
        }
        if (statAppDownlaodWithChunk != null) {
            a(statAppDownlaodWithChunk, (byte) 0);
            a(statAppDownlaodWithChunk, str);
        }
    }

    public byte getSTType() {
        return 14;
    }

    public void flush() {
    }

    public void b(int i, String str, String str2) {
        AppChunkDownlaod appChunkDownlaod;
        StatAppDownlaodWithChunk statAppDownlaodWithChunk = this.c.get(str);
        if (statAppDownlaodWithChunk != null) {
            Iterator<h> it = this.d.iterator();
            while (true) {
                if (!it.hasNext()) {
                    appChunkDownlaod = null;
                    break;
                }
                h next = it.next();
                if (next.a() == statAppDownlaodWithChunk.f1539a) {
                    appChunkDownlaod = next.b();
                    if (appChunkDownlaod != null) {
                        appChunkDownlaod.o = str2;
                    }
                }
            }
            if (appChunkDownlaod == null) {
                AppChunkDownlaod appChunkDownlaod2 = new AppChunkDownlaod();
                appChunkDownlaod2.o = str2;
                h hVar = new h();
                hVar.a(statAppDownlaodWithChunk.f1539a);
                hVar.a(appChunkDownlaod2);
                this.d.add(hVar);
            }
        }
    }

    private String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str) && str.contains("=")) {
            sb.append(str).append("&");
        }
        sb.append(str2).append("=").append(str3);
        return sb.toString();
    }
}
