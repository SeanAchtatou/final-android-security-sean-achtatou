package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.a.a;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class ba extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f1973a;

    ba(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f1973a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        b.b(this.f1973a.getContext(), ("tpmast://search?" + "&" + PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME + "=" + ((BaseActivity) this.f1973a.b).f()) + "&" + a.X + "=" + this.f1973a.q());
    }

    public STInfoV2 getStInfo() {
        return this.f1973a.e(com.tencent.assistantv2.st.page.a.a("01", "001"));
    }
}
