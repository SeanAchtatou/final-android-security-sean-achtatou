package com.tencent.assistantv2.component;

import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class bm {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UserTagAnimationView f1984a;
    private int b = 1;
    private float c = 0.0f;
    private float d = 0.0f;
    private float e = 0.0f;
    private int f = 0;
    private float g = 0.0f;
    private float h = 0.0f;
    private float i = 0.0f;
    private float j = 0.0f;

    public bm(UserTagAnimationView userTagAnimationView, int i2, float f2, float f3, float f4, int i3) {
        this.f1984a = userTagAnimationView;
        this.f = i2;
        this.c = f2;
        this.d = f2;
        this.e = f3;
        this.h = (float) by.a(userTagAnimationView.b, f4);
        this.b = i3;
        this.g = UserTagAnimationView.a(this.f);
        this.i = (float) (((double) userTagAnimationView.i) + (((double) this.g) * Math.cos((((double) this.d) * 3.141592653589793d) / 180.0d)));
        this.j = (float) (((double) userTagAnimationView.j) - (((double) this.g) * Math.sin((((double) this.d) * 3.141592653589793d) / 180.0d)));
    }

    public void a() {
        if (this.b == 1) {
            this.d = (this.d + this.e) % 360.0f;
        } else if (this.b == 2) {
            this.d = (this.d - this.e) % 360.0f;
        }
        double d2 = (((double) this.d) * 3.141592653589793d) / 180.0d;
        this.i = (float) (((double) this.f1984a.i) + (((double) this.g) * Math.cos(d2)));
        this.j = (float) (((double) this.f1984a.j) - (Math.sin(d2) * ((double) this.g)));
    }

    public float b() {
        return this.i;
    }

    public float c() {
        return this.j;
    }

    public float d() {
        return this.h;
    }
}
