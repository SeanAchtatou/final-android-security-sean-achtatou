package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.nucleus.socialcontact.login.j;

/* compiled from: ProGuard */
class ag extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MainActionHeaderView f1954a;

    ag(MainActionHeaderView mainActionHeaderView) {
        this.f1954a = mainActionHeaderView;
    }

    public void onTMAClick(View view) {
        this.f1954a.a(view);
    }

    public STInfoV2 getStInfo(View view) {
        ExplicitHotWord explicitHotWord;
        if (view == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1954a.getContext(), 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = this.f1954a.b(view);
            if (view.getId() == this.f1954a.e.getId()) {
                if (j.a().j()) {
                    buildSTInfo.status = "01";
                } else {
                    buildSTInfo.status = "02";
                }
            } else if (view.getId() == this.f1954a.f.getId() && (explicitHotWord = (ExplicitHotWord) this.f1954a.g.e()) != null) {
                buildSTInfo.extraData = explicitHotWord.b + ";" + explicitHotWord.f1230a + ";" + (this.f1954a.g.d() + 1);
                buildSTInfo.searchPreId = this.f1954a.g.j();
            }
        }
        return buildSTInfo;
    }
}
