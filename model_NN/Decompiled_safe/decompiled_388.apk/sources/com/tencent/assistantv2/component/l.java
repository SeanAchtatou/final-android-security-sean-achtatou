package com.tencent.assistantv2.component;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: ProGuard */
class l {

    /* renamed from: a  reason: collision with root package name */
    public String f2002a;
    public int b;
    boolean c;
    public int d;
    public int e;

    private l() {
    }

    /* synthetic */ l(f fVar) {
        this();
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f2002a = str;
            this.c = true;
        }
    }

    public String a(Context context) {
        return this.c ? this.f2002a : context.getResources().getString(this.b);
    }
}
