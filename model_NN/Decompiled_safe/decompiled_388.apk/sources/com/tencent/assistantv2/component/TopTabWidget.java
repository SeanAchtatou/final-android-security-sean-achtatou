package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class TopTabWidget extends LinearLayout implements View.OnFocusChangeListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public bj f1945a;
    private int b;

    public TopTabWidget(Context context) {
        this(context, null);
    }

    public TopTabWidget(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842883);
    }

    public TopTabWidget(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        this.b = 0;
        a();
    }

    private void a() {
        setOrientation(0);
        setFocusable(true);
        setOnFocusChangeListener(this);
    }

    public void childDrawableStateChanged(View view) {
        if (view == getChildAt(this.b)) {
            invalidate();
        }
        super.childDrawableStateChanged(view);
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
    }

    public void a(int i) {
        if (i >= 0 && i < getChildCount()) {
            getChildAt(this.b).setSelected(false);
            this.b = i;
            getChildAt(this.b).setSelected(true);
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).setEnabled(z);
        }
    }

    public void addView(View view) {
        int i;
        super.addView(view, new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.navigation_curcor_moving_lenth), -1));
        Object tag = view.getTag(R.id.tma_st_slot_tag);
        if (tag == null || !(tag instanceof Integer)) {
            i = 0;
        } else {
            i = ((Integer) tag).intValue();
        }
        view.setOnClickListener(new bk(this, Integer.parseInt(view.getTag().toString()), i, null));
        view.setOnFocusChangeListener(this);
    }

    public View b(int i) {
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            if (getChildAt(i2).getTag() != null && (i + Constants.STR_EMPTY).equals(getChildAt(i2).getTag().toString())) {
                return getChildAt(i2);
            }
        }
        return null;
    }

    public void a(int i, BaseActivity baseActivity) {
        View view = new View(getContext());
        if (view.getLayoutParams() == null) {
            view.setLayoutParams(new LinearLayout.LayoutParams(MainActivity.A, -2));
        }
        view.setTag(Integer.valueOf(i));
        view.setOnClickListener(new bi(this, baseActivity, i));
        super.addView(view);
    }

    public void a(bj bjVar) {
        this.f1945a = bjVar;
    }

    public void onFocusChange(View view, boolean z) {
        if (getChildAt(this.b) != null) {
            if (view == this && z) {
                getChildAt(this.b).requestFocus();
            } else if (z) {
                for (int i = 0; i < getChildCount(); i++) {
                    if (getChildAt(i) == view) {
                        a(i);
                        this.f1945a.a(i, false);
                        return;
                    }
                }
            }
        }
    }
}
