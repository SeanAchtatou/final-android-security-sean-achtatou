package com.tencent.securemodule.jni;

import android.content.Context;
import com.tencent.connect.common.Constants;

public class SecureEngine {
    private int object;

    public SecureEngine(Context context) {
        try {
            this.object = newObject(context);
            if (this.object == 0) {
                throw new VerifyError();
            }
        } catch (Throwable th) {
        }
    }

    public static boolean checkSecureStatus(Context context) {
        try {
            return nativeCheckSecureStatus(context);
        } catch (Throwable th) {
            return false;
        }
    }

    private static native void deleteObject(int i);

    public static String getEngineVersion(Context context) {
        try {
            return nativeGetEngineVersion(context);
        } catch (Throwable th) {
            return Constants.STR_EMPTY;
        }
    }

    private static native boolean nativeCheckSecureStatus(Context context);

    private static native String nativeGetEngineVersion(Context context);

    private static native int newObject(Context context);

    private static native int scanThreatens(int i, int i2, byte[] bArr);

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.object != 0) {
            deleteObject(this.object);
        }
    }

    public int scanThreatens(int i, byte[] bArr) {
        try {
            return scanThreatens(this.object, i, bArr);
        } catch (Throwable th) {
            return -1;
        }
    }
}
