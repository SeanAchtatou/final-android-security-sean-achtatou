package com.tencent.assistant.smartcard.c;

import com.qq.AppService.AstApp;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.t;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import com.tencent.assistant.utils.e;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.manager.bo;
import java.util.List;

/* compiled from: ProGuard */
public class o extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || nVar.j != 6) {
            return false;
        }
        return a((t) nVar, (w) this.f1692a.get(Integer.valueOf(nVar.j)), (x) this.b.get(Integer.valueOf(nVar.j)));
    }

    private boolean a(t tVar, w wVar, x xVar) {
        if (xVar == null) {
            return false;
        }
        if (wVar == null) {
            wVar = new w();
            wVar.f = tVar.k;
            wVar.e = tVar.j;
            this.f1692a.put(Integer.valueOf(wVar.e), wVar);
        }
        SelfUpdateManager.SelfUpdateInfo d = SelfUpdateManager.a().d();
        bo n = SelfUpdateManager.a().n();
        if ((n == null || !com.tencent.assistant.utils.bo.b(n.c)) && !SelfUpdateManager.a().j()) {
            if (d != null) {
                String packageName = AstApp.i().getPackageName();
                if (ApkResourceManager.getInstance().getLocalApkInfoFromCacheOrByFileName(packageName, d.e, 0) == null) {
                    return false;
                }
                LocalApkInfo installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(packageName);
                if (installedApkInfo == null) {
                    installedApkInfo = e.b(packageName);
                }
                if (installedApkInfo == null || installedApkInfo.mVersionCode < d.e) {
                    tVar.b = installedApkInfo;
                    tVar.f1763a = d;
                } else {
                    a(tVar.t, tVar.k + "||" + tVar.j + "|" + 6, tVar.j);
                    return false;
                }
            }
            if (tVar.b == null || tVar.f1763a == null) {
                return false;
            }
            if (wVar.b >= xVar.b) {
                a(tVar.t, tVar.k + "||" + tVar.j + "|" + 1, tVar.j);
                return false;
            } else if (wVar.f1766a >= xVar.f1767a) {
                a(tVar.t, tVar.k + "||" + tVar.j + "|" + 2, tVar.j);
                return false;
            } else if (SelfUpdateManager.a().q()) {
                a(tVar.t, tVar.k + "||" + tVar.j + "|" + 5, tVar.j);
                return false;
            } else {
                SelfUpdateManager.a().c(true);
                return true;
            }
        } else {
            a(tVar.t, tVar.k + "||" + tVar.j + "|" + 5, tVar.j);
            return false;
        }
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 6) {
            t tVar = (t) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(tVar.j));
            if (xVar != null) {
                tVar.q = xVar.d;
            }
        }
    }
}
