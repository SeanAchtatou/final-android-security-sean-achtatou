package com.tencent.assistant.smartcard.c;

import android.util.Pair;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.w;
import java.util.List;

/* compiled from: ProGuard */
public class d extends z {
    public boolean a(n nVar, List<Long> list) {
        Pair<Boolean, w> b = b(nVar);
        if (!((Boolean) b.first).booleanValue()) {
            return false;
        }
        return a(nVar, (w) b.second);
    }

    /* access modifiers changed from: protected */
    public boolean a(n nVar, w wVar) {
        return true;
    }
}
