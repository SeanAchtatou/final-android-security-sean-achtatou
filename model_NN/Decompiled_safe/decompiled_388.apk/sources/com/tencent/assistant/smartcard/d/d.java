package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.SmartCardCpaAdvertise;

/* compiled from: ProGuard */
public class d extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1751a;
    public SimpleAppModel b;
    public boolean c;
    public long d;
    public int e;
    public int f;

    public boolean a() {
        return this.c;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void a(SmartCardCpaAdvertise smartCardCpaAdvertise, int i) {
        this.j = i;
        if (smartCardCpaAdvertise != null) {
            this.b = k.a(smartCardCpaAdvertise.f1506a);
            this.f1751a = smartCardCpaAdvertise.b;
            this.e = smartCardCpaAdvertise.d;
            this.f = smartCardCpaAdvertise.e;
            this.d = smartCardCpaAdvertise.c;
        }
    }
}
