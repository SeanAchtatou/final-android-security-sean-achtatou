package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.smartcard.d.c;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.BookingButton;

/* compiled from: ProGuard */
public class NormalSmartcardBookingItem extends NormalSmartcardBaseItem {
    private TextView i;
    private BookingButton l;
    private TXImageView m;
    private TextView n;
    private ListItemInfoView o;
    private TextView p;

    public NormalSmartcardBookingItem(Context context) {
        this(context, null);
    }

    public NormalSmartcardBookingItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardBookingItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_booking, this);
        this.i = (TextView) findViewById(R.id.title);
        this.m = (TXImageView) findViewById(R.id.iconimg);
        this.l = (BookingButton) findViewById(R.id.bookingbtn);
        this.n = (TextView) findViewById(R.id.appname);
        this.o = (ListItemInfoView) findViewById(R.id.iteminfo);
        this.o.a(ListItemInfoView.InfoType.CATEGORY_SIZE);
        this.p = (TextView) findViewById(R.id.desc);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        c cVar = null;
        if (this.d instanceof c) {
            cVar = (c) this.d;
        }
        if (cVar != null && cVar.f1750a != null) {
            String str = cVar.o;
            STInfoV2 a2 = a("03_001", 200);
            if (a2 != null) {
                a2.updateWithSimpleAppModel(cVar.f1750a);
            }
            setOnClickListener(new p(this, str, a2));
            this.i.setText(cVar.l);
            this.l.a(cVar, a2);
            this.m.updateImageView(cVar.f1750a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.n.setText(cVar.f1750a.d);
            this.o.a(cVar.f1750a);
            this.p.setText(cVar.b);
        }
    }
}
