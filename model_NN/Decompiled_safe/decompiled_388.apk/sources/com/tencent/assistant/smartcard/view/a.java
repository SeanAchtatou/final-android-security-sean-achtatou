package com.tencent.assistant.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1773a;
    final /* synthetic */ NormalSmartCardGameRegisterNode b;

    a(NormalSmartCardGameRegisterNode normalSmartCardGameRegisterNode, STInfoV2 sTInfoV2) {
        this.b = normalSmartCardGameRegisterNode;
        this.f1773a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.g, this.f1773a);
    }

    public STInfoV2 getStInfo() {
        if (this.f1773a == null || !(this.f1773a instanceof STInfoV2)) {
            return null;
        }
        this.f1773a.updateStatus(this.b.g);
        return this.f1773a;
    }
}
