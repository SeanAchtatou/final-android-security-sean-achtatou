package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.b;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.z;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SearchSmartCardAppListItem extends SearchSmartCardBaseItem {
    private View l;
    private TextView m;
    private TextView n;
    private ImageView o;
    private ImageView p;
    private LinearLayout q;
    private z r;
    private final String s = "17";

    public SearchSmartCardAppListItem(Context context) {
        super(context);
    }

    public SearchSmartCardAppListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SearchSmartCardAppListItem(Context context, n nVar, as asVar, z zVar) {
        super(context, nVar, asVar);
        this.r = zVar;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            h();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.search_smartcard_app, this);
        } catch (Throwable th) {
            t.a().b();
            this.c = this.b.inflate((int) R.layout.search_smartcard_app, this);
        }
        this.l = findViewById(R.id.title_ly);
        this.m = (TextView) findViewById(R.id.title);
        this.n = (TextView) findViewById(R.id.desc);
        this.o = (ImageView) findViewById(R.id.close);
        this.p = (ImageView) findViewById(R.id.divider);
        this.q = (LinearLayout) findViewById(R.id.app_list);
        i();
    }

    /* access modifiers changed from: protected */
    public void b() {
        i();
    }

    private void i() {
        this.q.removeAllViews();
        b bVar = (b) this.d;
        if (bVar == null || bVar.f1749a <= 0 || bVar.c == null || bVar.c.size() == 0 || bVar.c.size() < bVar.f1749a) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        ArrayList arrayList = new ArrayList(bVar.c);
        if (bVar.q) {
            this.o.setVisibility(0);
        } else {
            this.o.setVisibility(8);
        }
        this.o.setOnClickListener(this.j);
        this.m.setText(Html.fromHtml(bVar.l));
        int size = arrayList.size() > bVar.f1749a ? bVar.f1749a : arrayList.size();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.p.getLayoutParams();
        if (TextUtils.isEmpty(bVar.n)) {
            layoutParams.setMargins(0, by.b(14.0f), 0, 0);
            this.n.setVisibility(8);
        } else {
            layoutParams.setMargins(0, by.b(9.0f), 0, 0);
            if (bVar.j == 3) {
                long j = 0;
                for (int i = 0; i < arrayList.size(); i++) {
                    j += ((SimpleAppModel) arrayList.get(i)).k;
                }
                this.n.setText(Html.fromHtml(bVar.n + "<font color=\"#5ac65c\">" + at.a(j) + "</font>"));
            } else {
                this.n.setText(bVar.n);
            }
            this.n.setVisibility(0);
        }
        this.q.addView(a(arrayList.subList(0, size)));
        if (bVar.b > bVar.f1749a) {
            View a2 = a(bVar.p + "(" + bVar.b + ")");
            this.q.addView(a2, new LinearLayout.LayoutParams(-1, by.b(40.0f)));
            a2.setOnClickListener(this.k);
        }
    }

    public void a(int i) {
        this.c.setVisibility(i);
        this.l.setVisibility(i);
        this.q.setVisibility(i);
    }

    private View a(List<SimpleAppModel> list) {
        LinearLayout linearLayout = new LinearLayout(this.f1693a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        for (int i = 0; i < list.size(); i++) {
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            SimpleAppModel simpleAppModel = list.get(i);
            ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            ((TextView) inflate.findViewById(R.id.name)).setText(simpleAppModel.d);
            TextView textView = (TextView) inflate.findViewById(R.id.size);
            textView.setText(at.a(simpleAppModel.k));
            TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) inflate.findViewById(R.id.progress);
            tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
            DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
            downloadButton.a(simpleAppModel);
            downloadButton.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            if (s.a(simpleAppModel)) {
                downloadButton.setClickable(false);
            } else {
                downloadButton.setClickable(true);
                STInfoV2 a2 = a(d(i), 200);
                if (a2 != null) {
                    a2.scene = b(0);
                    a2.searchId = this.i;
                    a2.extraData = c(0);
                    a2.updateWithSimpleAppModel(simpleAppModel);
                }
                downloadButton.a(a2, new v(this), (d) null, downloadButton, tXDwonloadProcessBar);
            }
            inflate.setTag(R.id.tma_st_smartcard_tag, STConst.ST_PAGE_TYPE_SMARTCARD);
            inflate.setOnClickListener(new w(this, simpleAppModel, i));
            linearLayout.addView(inflate, layoutParams);
        }
        return linearLayout;
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ((ImageView) inflate.findViewById(R.id.icon)).setImageResource(R.drawable.go);
        return inflate;
    }

    /* access modifiers changed from: private */
    public String d(int i) {
        return c() + bm.a(i + 1);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return a.a("17", this.r == null ? 0 : this.r.a());
    }

    /* access modifiers changed from: protected */
    public int d() {
        return com.tencent.assistantv2.st.page.d.f2062a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        if (this.r == null) {
            return null;
        }
        return this.r.d();
    }

    /* access modifiers changed from: protected */
    public long f() {
        if (this.r == null) {
            return 0;
        }
        return this.r.c();
    }

    /* access modifiers changed from: protected */
    public int g() {
        if (this.r == null) {
            return 2000;
        }
        return this.r.b();
    }
}
