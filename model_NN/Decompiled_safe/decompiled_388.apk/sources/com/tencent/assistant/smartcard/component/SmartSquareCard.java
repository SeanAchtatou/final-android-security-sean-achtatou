package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.d.z;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class SmartSquareCard extends NormalSmartcardBaseItem {
    private TextView i;
    private TextView l;
    private SmartSquareNode m;

    public SmartSquareCard(Context context) {
        this(context, null);
    }

    public SmartSquareCard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SmartSquareCard(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public void a(List<y> list, IViewInvalidater iViewInvalidater) {
        this.m.a(list, iViewInvalidater, new am(this));
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b.inflate((int) R.layout.smartcard_square, this);
        this.i = (TextView) findViewById(R.id.title);
        this.m = (SmartSquareNode) findViewById(R.id.smart_square_node);
        this.l = (TextView) findViewById(R.id.more_txt);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        z zVar = null;
        if (this.d instanceof z) {
            zVar = (z) this.d;
        }
        if (zVar != null) {
            this.i.setText(zVar.l);
            if (zVar.b()) {
                this.i.setBackgroundResource(R.drawable.common_index_tag);
                this.i.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
            } else {
                this.i.setBackgroundResource(0);
                this.i.setTextColor(getResources().getColor(R.color.apk_name_v5));
            }
            if (!TextUtils.isEmpty(zVar.o)) {
                if (TextUtils.isEmpty(zVar.p)) {
                    this.l.setText(zVar.p);
                } else {
                    this.l.setText(getResources().getString(R.string.more));
                }
                String str = zVar.o;
                this.l.setVisibility(0);
                this.l.setOnClickListener(new an(this, str));
            } else {
                this.l.setVisibility(8);
            }
            a(zVar.a(), this.g);
        }
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }
}
