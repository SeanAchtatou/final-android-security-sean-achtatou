package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class ai extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f1710a;
    final /* synthetic */ int b;
    final /* synthetic */ SearchSmartCardYuyiTagItem c;

    ai(SearchSmartCardYuyiTagItem searchSmartCardYuyiTagItem, SimpleAppModel simpleAppModel, int i) {
        this.c = searchSmartCardYuyiTagItem;
        this.f1710a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.c.f1693a, AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.b(0));
        intent.putExtra("statInfo", new StatInfo(this.f1710a.b, this.c.b(0), 0, Constants.STR_EMPTY, this.c.i));
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.c.b(0));
        intent.putExtra("simpleModeInfo", this.f1710a);
        this.c.f1693a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.c.a(this.c.d(this.b), 200);
        if (a2 != null) {
            a2.updateWithSimpleAppModel(this.f1710a);
            a2.searchId = this.c.i;
        }
        return a2;
    }
}
