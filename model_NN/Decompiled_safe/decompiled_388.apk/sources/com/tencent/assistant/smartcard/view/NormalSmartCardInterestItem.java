package com.tencent.assistant.smartcard.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.component.NormalSmartCardAppHorizontalNode;
import com.tencent.assistant.smartcard.component.NormalSmartCardAppVerticalNode;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.m;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.smartcard.f.b;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.smartcard.b.d;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartCardInterestItem extends NormalSmartcardBaseItem {
    private RelativeLayout i;
    private TextView l;
    private TextView m;
    private TextView n;
    private LinearLayout o;
    private LinearLayout p;
    private List<NormalSmartCardAppHorizontalNode> q;

    public NormalSmartCardInterestItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
        setBackgroundResource(R.drawable.common_card_normal);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = inflate(this.f1693a, R.layout.smartcard_interest_frame_layout, this);
        this.l = (TextView) this.c.findViewById(R.id.card_title);
        this.m = (TextView) this.c.findViewById(R.id.card_sub_title);
        this.n = (TextView) this.c.findViewById(R.id.more_txt);
        this.i = (RelativeLayout) this.c.findViewById(R.id.card_title_layout);
        this.o = (LinearLayout) this.c.findViewById(R.id.card_app_list_layout);
        this.p = (LinearLayout) this.c.findViewById(R.id.card_app_vertical_list_layout);
        f();
    }

    private void f() {
        m mVar = (m) this.d;
        if (!TextUtils.isEmpty(mVar.l)) {
            this.l.setText(mVar.l);
            int a2 = b.a(mVar.f());
            if (a2 != 0) {
                Drawable drawable = getResources().getDrawable(a2);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                this.l.setCompoundDrawables(drawable, null, null, null);
                this.l.setCompoundDrawablePadding(by.a(this.f1693a, 7.0f));
                this.l.setPadding(0, 0, 0, 0);
            } else {
                this.l.setCompoundDrawables(null, null, null, null);
                this.l.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
            }
            this.i.setVisibility(0);
            if (!TextUtils.isEmpty(mVar.m)) {
                this.m.setText(mVar.m);
                this.m.setPadding(by.a(this.f1693a, 7.0f), 0, 0, 0);
                this.m.setVisibility(0);
            } else {
                this.m.setVisibility(8);
            }
            if (TextUtils.isEmpty(mVar.p) || TextUtils.isEmpty(mVar.o)) {
                this.n.setVisibility(8);
            } else {
                this.n.setText(mVar.p);
                this.n.setOnClickListener(this.k);
                this.n.setVisibility(0);
            }
        } else {
            this.i.setVisibility(8);
        }
        if (mVar.i() == 2) {
            this.o.setVisibility(8);
            a(mVar);
            return;
        }
        this.p.setVisibility(8);
        b(mVar);
    }

    private void a(m mVar) {
        int i2;
        boolean z;
        List<y> list = mVar.e;
        this.p.setVisibility(0);
        int size = list.size();
        if (size > 3) {
            i2 = 3;
        } else {
            i2 = size;
        }
        int childCount = this.p.getChildCount();
        if (childCount < i2) {
            for (int i3 = 0; i3 < i2 - childCount; i3++) {
                NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode = new NormalSmartCardAppVerticalNode(this.f1693a);
                normalSmartCardAppVerticalNode.setMinimumHeight(by.a(this.f1693a, 90.0f));
                this.p.addView(normalSmartCardAppVerticalNode, new LinearLayout.LayoutParams(-1, -2, 1.0f));
            }
        }
        int childCount2 = this.p.getChildCount();
        for (int i4 = 0; i4 < childCount2; i4++) {
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode2 = (NormalSmartCardAppVerticalNode) this.p.getChildAt(i4);
            if (normalSmartCardAppVerticalNode2 != null) {
                if (i4 < i2) {
                    normalSmartCardAppVerticalNode2.setVisibility(0);
                } else {
                    normalSmartCardAppVerticalNode2.setVisibility(8);
                }
            }
        }
        for (int i5 = 0; i5 < i2; i5++) {
            SimpleAppModel simpleAppModel = list.get(i5).f1768a;
            NormalSmartCardAppVerticalNode normalSmartCardAppVerticalNode3 = (NormalSmartCardAppVerticalNode) this.p.getChildAt(i5);
            Spanned a2 = list.get(i5).a();
            STInfoV2 a3 = a(list.get(i5), i5);
            int c = c(a(this.f1693a));
            if (i5 < i2 - 1) {
                z = true;
            } else {
                z = false;
            }
            normalSmartCardAppVerticalNode3.a(simpleAppModel, a2, a3, c, z, ListItemInfoView.InfoType.CATEGORY_SIZE);
        }
    }

    private void b(m mVar) {
        int i2;
        List<y> list = mVar.e;
        this.o.setVisibility(0);
        if (list.size() >= 3) {
            int size = list.size();
            if (list.size() > 3) {
                i2 = 3;
            } else {
                i2 = size;
            }
            if (this.q == null) {
                g();
                this.q = new ArrayList(3);
                for (int i3 = 0; i3 < i2; i3++) {
                    NormalSmartCardAppHorizontalNode normalSmartCardAppHorizontalNode = new NormalSmartCardAppHorizontalNode(this.f1693a);
                    this.q.add(normalSmartCardAppHorizontalNode);
                    normalSmartCardAppHorizontalNode.setPadding(0, by.a(getContext(), 10.0f), 0, by.a(getContext(), 2.0f));
                    normalSmartCardAppHorizontalNode.setBackgroundResource(R.drawable.v2_button_background_light_selector);
                    this.o.addView(normalSmartCardAppHorizontalNode, new LinearLayout.LayoutParams(0, -2, 1.0f));
                    normalSmartCardAppHorizontalNode.a(list.get(i3).f1768a, list.get(i3).a(), a(list.get(i3), i3), c(a(this.f1693a)));
                }
                return;
            }
            for (int i4 = 0; i4 < i2; i4++) {
                this.q.get(i4).a(list.get(i4).f1768a, list.get(i4).a(), a(list.get(i4), i4), c(a(this.f1693a)));
            }
        }
    }

    private void g() {
        this.q = null;
        this.o.removeAllViews();
    }

    private STInfoV2 a(y yVar, int i2) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || yVar == null)) {
            a2.updateWithSimpleAppModel(yVar.f1768a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.d instanceof d) {
            return ((d) this.d).e();
        }
        return super.d(i2);
    }
}
