package com.tencent.assistant.smartcard.component;

import android.os.Bundle;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1726a;
    final /* synthetic */ NormalSmartCardSelfUpdateItem b;

    i(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, STInfoV2 sTInfoV2) {
        this.b = normalSmartCardSelfUpdateItem;
        this.f1726a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b.c(this.b.a(this.b.f1693a)));
        b.b(this.b.getContext(), "tmast://appdetails?" + a.c + "=" + AstApp.i().getPackageName(), bundle);
    }

    public STInfoV2 getStInfo() {
        if (this.f1726a != null) {
            this.f1726a.actionId = 200;
        }
        return this.f1726a;
    }
}
