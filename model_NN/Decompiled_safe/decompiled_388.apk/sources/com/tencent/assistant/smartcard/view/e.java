package com.tencent.assistant.smartcard.view;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1777a;
    final /* synthetic */ NormalSmartCardGiftNode b;

    e(NormalSmartCardGiftNode normalSmartCardGiftNode, STInfoV2 sTInfoV2) {
        this.b = normalSmartCardGiftNode;
        this.f1777a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.b.a(this.b.m.f1755a, this.f1777a);
    }

    public STInfoV2 getStInfo() {
        if (this.f1777a == null || !(this.f1777a instanceof STInfoV2)) {
            return null;
        }
        this.f1777a.updateStatus(this.b.m.f1755a);
        return this.f1777a;
    }
}
