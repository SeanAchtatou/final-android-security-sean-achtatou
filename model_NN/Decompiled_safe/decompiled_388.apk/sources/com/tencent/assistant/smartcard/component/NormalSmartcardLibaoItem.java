package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.SmartCardLibaoItem;
import com.tencent.assistant.smartcard.d.h;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class NormalSmartcardLibaoItem extends NormalSmartcardBaseItem {
    private View i;
    private TextView l;
    private TextView m;
    private ImageView n;
    private ImageView o;
    private LinearLayout p;

    public NormalSmartcardLibaoItem(Context context) {
        super(context);
    }

    public NormalSmartcardLibaoItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardLibaoItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_libao, this);
        this.i = findViewById(R.id.title_ly);
        this.l = (TextView) findViewById(R.id.title);
        this.m = (TextView) findViewById(R.id.desc);
        this.n = (ImageView) findViewById(R.id.close);
        this.o = (ImageView) findViewById(R.id.divider);
        this.p = (LinearLayout) findViewById(R.id.gift_list);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        boolean z;
        this.p.removeAllViews();
        h hVar = (h) this.d;
        if (hVar == null || hVar.c == null || hVar.c.size() == 0) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        ArrayList<SmartCardLibaoItem> arrayList = hVar.c;
        if (hVar.q) {
            this.n.setVisibility(0);
        } else {
            this.n.setVisibility(8);
        }
        this.l.setText(hVar.l);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.o.getLayoutParams();
        if (TextUtils.isEmpty(hVar.n)) {
            layoutParams.setMargins(0, by.b(14.0f), 0, 0);
            this.m.setVisibility(8);
        } else {
            layoutParams.setMargins(0, by.b(9.0f), 0, 0);
            this.m.setText(hVar.n);
            this.m.setVisibility(0);
        }
        this.n.setOnClickListener(this.j);
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            SmartCardLibaoItem smartCardLibaoItem = arrayList.get(i2);
            if (i2 == size - 1) {
                z = true;
            } else {
                z = false;
            }
            this.p.addView(a(smartCardLibaoItem, z, i2));
        }
        if (hVar.b > size) {
            View a2 = a(hVar.p);
            this.p.addView(a2, new LinearLayout.LayoutParams(-1, by.b(40.0f)));
            a2.setOnClickListener(this.k);
        }
    }

    public void a(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.p.setVisibility(i2);
    }

    private View a(SmartCardLibaoItem smartCardLibaoItem, boolean z, int i2) {
        View inflate = this.b.inflate((int) R.layout.smartcard_libao_item, (ViewGroup) null);
        ((TXImageView) inflate.findViewById(R.id.icon)).updateImageView(smartCardLibaoItem.f1510a, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) inflate.findViewById(R.id.name)).setText(smartCardLibaoItem.b);
        ((TextView) inflate.findViewById(R.id.desc)).setText(smartCardLibaoItem.c);
        TextView textView = (TextView) inflate.findViewById(R.id.btn);
        r rVar = new r(this, smartCardLibaoItem, a(a.a("03", i2), 200));
        textView.setId(11112222);
        textView.setTag(R.id.tma_st_smartcard_tag, e());
        textView.setOnClickListener(rVar);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.divider);
        if (z) {
            imageView.setVisibility(8);
        } else {
            imageView.setVisibility(0);
        }
        inflate.setTag(R.id.tma_st_smartcard_tag, e());
        inflate.setOnClickListener(rVar);
        return inflate;
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ((ImageView) inflate.findViewById(R.id.icon)).setImageResource(R.drawable.go);
        return inflate;
    }
}
