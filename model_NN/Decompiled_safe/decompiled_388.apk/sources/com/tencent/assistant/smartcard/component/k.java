package com.tencent.assistant.smartcard.component;

import com.tencent.assistantv2.component.e;

/* compiled from: ProGuard */
class k implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f1728a;
    final /* synthetic */ NormalSmartCardSelfUpdateItem b;

    k(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, long j) {
        this.b = normalSmartCardSelfUpdateItem;
        this.f1728a = j;
    }

    public void a(boolean z) {
        Long l = (Long) this.b.u.getTag();
        if (l != null && this.f1728a == l.longValue()) {
            if (!z) {
                this.b.s.setVisibility(8);
                this.b.t.setOnClickListener(null);
                return;
            }
            this.b.s.setVisibility(0);
        }
    }
}
