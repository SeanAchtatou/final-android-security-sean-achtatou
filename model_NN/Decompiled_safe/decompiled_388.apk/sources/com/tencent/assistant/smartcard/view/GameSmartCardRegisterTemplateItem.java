package com.tencent.assistant.smartcard.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.g;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
public class GameSmartCardRegisterTemplateItem extends NormalSmartcardBaseItem {
    private TextView i;
    private LinearLayout l;
    private g m;

    public GameSmartCardRegisterTemplateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GameSmartCardRegisterTemplateItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public GameSmartCardRegisterTemplateItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LayoutInflater.from(this.f1693a).inflate((int) R.layout.game_smartcard_register_layout, this);
        this.i = (TextView) findViewById(R.id.title);
        this.l = (LinearLayout) findViewById(R.id.node_area);
        this.m = (g) this.d;
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.m = (g) this.d;
        f();
    }

    private void f() {
        this.i.setText(this.m.l);
        int childCount = this.l.getChildCount() - this.m.g().size();
        if (childCount > 0) {
            for (int i2 = 0; i2 < childCount; i2++) {
                this.l.removeViewAt(i2);
            }
        } else if (childCount < 0) {
            for (int i3 = 0; i3 < (-childCount); i3++) {
                this.l.addView(new NormalSmartCardGameRegisterNode(this.f1693a));
            }
        }
        STInfoV2 sTInfoV2 = null;
        int i4 = 0;
        while (i4 < this.l.getChildCount() && this.m.g() != null && i4 < this.m.g().size()) {
            NormalSmartCardGameRegisterNode normalSmartCardGameRegisterNode = (NormalSmartCardGameRegisterNode) this.l.getChildAt(i4);
            if (this.m.g().get(i4) != null) {
                sTInfoV2 = a(i4, this.m.g().get(i4));
            }
            normalSmartCardGameRegisterNode.a(this.m.g().get(i4), sTInfoV2, i4 == this.l.getChildCount() + -1, c(a(this.f1693a)));
            i4++;
        }
    }

    private STInfoV2 a(int i2, SimpleAppModel simpleAppModel) {
        STInfoV2 a2 = a(a.a("05", i2), 100);
        if (!(a2 == null || simpleAppModel == null)) {
            a2.updateWithSimpleAppModel(simpleAppModel);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }

    /* access modifiers changed from: protected */
    public String d(int i2) {
        if (this.m != null) {
            return this.m.e;
        }
        return super.d(i2);
    }
}
