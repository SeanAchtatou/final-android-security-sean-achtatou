package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.module.ay;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.smartcard.component.NormalSmartcardAppListItem;
import com.tencent.assistant.smartcard.d.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.component.appdetail.RecommendAppViewV5;
import com.tencent.pangu.module.a.i;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class NormalErrorRecommendPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener {
    public static final byte ERRORSCENE_APP_DETAIL = 1;
    public static final byte ERRORSCENE_DOWNLOAD_MANAGER = 2;
    public static final byte ERRORSCENE_FRIENDS_ERROR = 4;
    public static final byte ERRORSCENE_NORMAL_ERROR = 3;
    public static final int ERROR_TYPE_APPDETAIL_EMPTY = 100;
    public static final int ERROR_TYPE_BASE = 0;
    public static final int ERROR_TYPE_DETAIL_EMPTY_TO_HOME = 70;
    public static final int ERROR_TYPE_DETAIL_EMPTY_TO_NO = 80;
    public static final int ERROR_TYPE_EMPTY = 10;
    public static final int ERROR_TYPE_EMPTY_TO_HOME = 50;
    public static final int ERROR_TYPE_EMPTY_TO_RECOMMEND = 60;
    public static final int ERROR_TYPE_FAIL = 20;
    public static final int ERROR_TYPE_FRIENDS_EMPTY_TO_RECOMMEND = 90;
    public static final int ERROR_TYPE_NETWORK_OK = 40;
    public static final int ERROR_TYPE_NO_NETWORK = 30;
    public static final int ERROR_TYPE_SEARCH_RESULT_EMPTY = 110;
    private static final int RECOMMEND_APPLIST_MAX_SUM = 4;
    private static final int RECOMMEND_APPLIST_MIN_SUM = 3;
    private static final String TMA_ST_SLOT_TAG_MULTI_CLICK = "04_001";
    private static final String TMA_ST_SLOT_TAG_SINGLE_CLICK = "03";
    private int activityPageId = 2000;
    private b apkMgrCallback = new y(this);
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int currentState = 20;
    private View.OnClickListener defaultListener = new v(this);
    /* access modifiers changed from: private */
    public RelativeLayout errorHintLayout;
    private ImageView errorImg;
    private TextView errorText;
    private Button freshBtn;
    private LayoutInflater inflater;
    private boolean isAutoLoading = false;
    /* access modifiers changed from: private */
    public View.OnClickListener listener;
    private ApkResCallback.Stub mApkInstallChangeCallback = new aa(this);
    /* access modifiers changed from: private */
    public SparseIntArray mEngineMap = new SparseIntArray();
    private boolean pageHasExposureReport = false;
    private i recommendAppCallback = new w(this);
    private ay recommendAppEngine = new ay();
    /* access modifiers changed from: private */
    public NormalSmartcardAppListItem recommendAppListItemView;
    /* access modifiers changed from: private */
    public RecommendAppViewV5 recommendAppView;
    /* access modifiers changed from: private */
    public RelativeLayout recommendLayout;
    private boolean recommendShow = true;
    /* access modifiers changed from: private */
    public com.tencent.assistant.smartcard.d.b smartCardAppModelGlobal;

    public NormalErrorRecommendPage(Context context2) {
        super(context2);
        initView(context2);
    }

    public NormalErrorRecommendPage(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        initView(context2);
    }

    public NormalErrorRecommendPage(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        initView(context2);
    }

    private void initView(Context context2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        try {
            this.inflater.inflate((int) R.layout.normal_error_recommend_page, this);
        } catch (Throwable th) {
            t.a().b();
            this.inflater.inflate((int) R.layout.normal_error_recommend_page, this);
        }
        this.errorImg = (ImageView) findViewById(R.id.no_wifi_image);
        this.errorText = (TextView) findViewById(R.id.no_wifi_text);
        this.errorHintLayout = (RelativeLayout) findViewById(R.id.layout_no_wifi_tips);
        this.freshBtn = (Button) findViewById(R.id.setting_network);
        this.freshBtn.setOnClickListener(this.defaultListener);
        this.recommendLayout = (RelativeLayout) findViewById(R.id.layout_recommend);
        t.a().a(this);
        ApkResourceManager.getInstance().getLocalApkLoader().a(this.apkMgrCallback);
        if (by.c() < 800) {
            this.recommendShow = false;
        }
    }

    public void setIsAutoLoading(boolean z) {
        this.isAutoLoading = z;
    }

    public void setErrorType(int i) {
        pageExposureReport(i);
        if (this.currentState == 40 && i == 20) {
            i = 40;
        }
        this.currentState = i;
        int i2 = 0;
        switch (i) {
            case 10:
                refreshView(getResources().getString(R.string.empty_content), R.color.common_listiteminfo, getResources().getString(R.string.get_content_fail_btn_txt), 8, 8, 8);
                i2 = R.drawable.icon_empty_nodata;
                break;
            case 20:
                refreshView(getResources().getString(R.string.empty_content), R.color.common_topbar_text_color, getResources().getString(R.string.get_content_fail_btn_txt), 8, 8, 8);
                i2 = R.drawable.icon_empty_nodata;
                break;
            case 30:
                refreshView(getResources().getString(R.string.disconnected_tips), R.color.common_topbar_text_color, getResources().getString(R.string.disconnected_setting), 8, 0, 0);
                i2 = R.drawable.network_disconnected;
                break;
            case 40:
                refreshView(getResources().getString(R.string.network_recovery_tips), R.color.common_topbar_text_color, getResources().getString(R.string.get_content_fail_btn_txt), 8, 8, 8);
                i2 = R.drawable.icon_network_recovery;
                break;
            case ERROR_TYPE_EMPTY_TO_HOME /*50*/:
                refreshView(getResources().getString(R.string.empty_content), R.color.common_listiteminfo, getResources().getString(R.string.to_home_btn_text), 8, 8, 8);
                i2 = R.drawable.icon_empty_nodata;
                break;
            case 60:
                refreshView(getResources().getString(R.string.empty_content), R.color.common_listiteminfo, null, 4, 4, 4);
                i2 = R.drawable.icon_empty_nodata;
                break;
            case ERROR_TYPE_DETAIL_EMPTY_TO_HOME /*70*/:
                refreshView(getResources().getString(R.string.empty_content_fromaction), R.color.common_topbar_text_color, getResources().getString(R.string.to_home_btn_text), 8, 8, 8);
                i2 = R.drawable.icon_empty_fromaction;
                break;
            case ERROR_TYPE_DETAIL_EMPTY_TO_NO /*80*/:
                refreshView(getResources().getString(R.string.empty_content_yyb), R.color.common_topbar_text_color, null, 8, 8, 8);
                i2 = R.drawable.icon_empty_yyb;
                break;
            case ERROR_TYPE_FRIENDS_EMPTY_TO_RECOMMEND /*90*/:
                refreshView(getResources().getString(R.string.rank_friends_empty_text), R.color.common_listiteminfo, null, 8, 4, 4);
                i2 = R.drawable.emptypage_pic_06;
                break;
            case 100:
                refreshView(getResources().getString(R.string.empty_content_appdetail), R.color.common_listiteminfo, null, 8, 8, 8);
                i2 = R.drawable.shafa;
                break;
            case ERROR_TYPE_SEARCH_RESULT_EMPTY /*110*/:
                refreshView(getResources().getString(R.string.empty_content), R.color.common_listiteminfo, getResources().getString(R.string.get_content_fail_btn_txt), 8, 8, 8);
                i2 = R.drawable.icon_empty_nodata;
                break;
        }
        try {
            this.errorImg.setImageResource(i2);
        } catch (Throwable th) {
            t.a().b();
        }
        if (this.recommendShow) {
            refreshPageLayout(i);
        } else {
            this.recommendLayout.setVisibility(8);
        }
    }

    private void refreshView(String str, int i, String str2, int i2, int i3, int i4) {
        if (!TextUtils.isEmpty(str)) {
            this.errorText.setText(str);
            this.errorText.setTextColor(getResources().getColor(i));
            this.errorText.setVisibility(0);
        } else {
            this.errorText.setVisibility(8);
        }
        if (!TextUtils.isEmpty(str2)) {
            this.freshBtn.setText(str2);
            this.freshBtn.setVisibility(0);
        } else {
            this.freshBtn.setVisibility(8);
        }
        this.recommendLayout.setVisibility(i2);
        this.errorHintLayout.setVisibility(4);
    }

    private void refreshPageLayout(int i) {
        switch (i) {
            case 30:
                com.tencent.assistant.smartcard.d.b noNetworkInstallApps = getNoNetworkInstallApps();
                if (!checkNoNetworkExistInstallApps()) {
                    this.recommendLayout.setVisibility(8);
                    return;
                } else if (this.recommendAppListItemView == null) {
                    this.recommendAppListItemView = new NormalSmartcardAppListItem(this.context, noNetworkInstallApps, null, null);
                    this.recommendAppListItemView.b(getActivityPageId());
                    this.recommendAppListItemView.a("03");
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams.addRule(3, R.id.layout_main);
                    this.recommendAppListItemView.setLayoutParams(layoutParams);
                    this.recommendAppListItemView.setVisibility(0);
                    this.recommendAppListItemView.a(false);
                    this.recommendLayout.addView(this.recommendAppListItemView);
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
                    layoutParams2.addRule(12);
                    this.recommendLayout.setLayoutParams(layoutParams2);
                    this.recommendLayout.setVisibility(0);
                    ApkResourceManager.getInstance().registerApkResCallback(this.mApkInstallChangeCallback);
                    return;
                } else {
                    this.recommendAppListItemView.a(noNetworkInstallApps);
                    this.recommendLayout.setVisibility(0);
                    return;
                }
            case 60:
            case ERROR_TYPE_FRIENDS_EMPTY_TO_RECOMMEND /*90*/:
                if (this.recommendAppView == null) {
                    this.recommendAppView = new RecommendAppViewV5(this.context);
                    this.recommendAppView.a(getActivityPageId());
                    this.recommendAppView.a("03");
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
                    int a2 = by.a(this.context, 7.5f);
                    layoutParams3.leftMargin = a2;
                    layoutParams3.rightMargin = a2;
                    this.recommendAppView.setLayoutParams(layoutParams3);
                    this.recommendLayout.addView(this.recommendAppView);
                    this.recommendLayout.setVisibility(8);
                }
                initRecommendApp(i);
                return;
            default:
                return;
        }
    }

    private int getRecommandEngineSence(int i) {
        if (i != 60 && i == 90) {
        }
        return 3;
    }

    /* access modifiers changed from: private */
    public int getRecommandTypeByErrorSence(int i) {
        if (i == 1) {
            return 1;
        }
        if (i == 2) {
            return 2;
        }
        if (i == 3) {
            return 6;
        }
        return i == 4 ? 5 : 6;
    }

    private int getRecomandErrorSence(int i) {
        if (i != 60 && i == 90) {
            return 4;
        }
        return 3;
    }

    public void setErrorImage(int i) {
        if (i > 0) {
            try {
                this.errorImg.setImageResource(i);
            } catch (Throwable th) {
                t.a().b();
            }
        }
    }

    public void setErrorText(String str) {
        this.errorText.setText(str);
        this.errorText.setVisibility(0);
    }

    public void setErrorText(String str, int i) {
        this.errorText.setText(str);
        if (i != 0) {
            this.errorText.setTextColor(getResources().getColor(i));
        }
        this.errorText.setVisibility(0);
    }

    public void setErrorTextSetting(View view) {
        if (view != null) {
            this.errorHintLayout.removeView(view);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            int a2 = by.a(this.context, 7.0f);
            layoutParams.setMargins(a2, 0, a2, a2);
            view.setLayoutParams(layoutParams);
            if (by.c() < 800) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.errorHintLayout.getLayoutParams();
                layoutParams2.addRule(12);
                this.errorHintLayout.setLayoutParams(layoutParams2);
                this.errorHintLayout.setVisibility(0);
            }
            this.errorHintLayout.addView(view);
        }
    }

    public boolean checkNoNetworkExistInstallApps() {
        com.tencent.assistant.smartcard.d.b noNetworkInstallApps = getNoNetworkInstallApps();
        if (noNetworkInstallApps.c == null || noNetworkInstallApps.c.size() < 3) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public com.tencent.assistant.smartcard.d.b getNoNetworkInstallApps() {
        int i = 3;
        if (this.smartCardAppModelGlobal == null) {
            this.smartCardAppModelGlobal = new com.tencent.assistant.smartcard.d.b();
            this.smartCardAppModelGlobal.j = 919;
            this.smartCardAppModelGlobal.l = this.context.getString(R.string.disconnected_recommend_app_list_title);
            this.smartCardAppModelGlobal.a(919, (List<u>) null);
            this.smartCardAppModelGlobal.o = "tmast://update";
            com.tencent.assistant.smartcard.d.b bVar = this.smartCardAppModelGlobal;
            if (this.smartCardAppModelGlobal.c.size() > 3) {
                i = 4;
            }
            bVar.f1749a = i;
        }
        return this.smartCardAppModelGlobal;
    }

    public void setButtonClickListener(View.OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    public void onConnected(APN apn) {
        if (this.currentState != 30 && this.currentState != 20) {
            return;
        }
        if (!this.isAutoLoading || getVisibility() != 0) {
            setErrorType(40);
        } else {
            this.listener.onClick(null);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    public void destory() {
        t.a().b(this);
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.mApkInstallChangeCallback);
        ApkResourceManager.getInstance().getLocalApkLoader().b(this.apkMgrCallback);
    }

    private void initRecommendApp(int i) {
        if (this.recommendAppView != null) {
            this.recommendAppEngine.register(this.recommendAppCallback);
            this.mEngineMap.put(this.recommendAppEngine.a(getRecommandEngineSence(i), new InstalledAppItem(), null, null, (byte) 0, null), getRecomandErrorSence(i));
            XLog.d("NormalErrorRecommendPage", "initRecommendApp begin...");
        }
    }

    /* access modifiers changed from: private */
    public String getTestMsg(ArrayList<RecommendAppInfo> arrayList) {
        String str = Constants.STR_EMPTY;
        Iterator<RecommendAppInfo> it = arrayList.iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            str = str2 + it.next().c + ",";
        }
    }

    /* access modifiers changed from: private */
    public String getRecommendTitle(int i, String str) {
        if (i == 5) {
            return getContext().getString(R.string.recommend_reason_friends);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public ArrayList<RecommendAppInfo> filterRecommendAppInfoListByInstalledList(List<RecommendAppInfo> list) {
        ArrayList<RecommendAppInfo> arrayList = new ArrayList<>();
        for (RecommendAppInfo next : list) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(next.a()) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public ArrayList<SimpleAppInfo> filterSimpleAppInfoListByInstalledList(List<SimpleAppInfo> list) {
        ArrayList<SimpleAppInfo> arrayList = new ArrayList<>();
        for (SimpleAppInfo next : list) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(next.f) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public void setActivityPageId(int i) {
        this.activityPageId = i;
    }

    public int getActivityPageId() {
        return this.activityPageId;
    }

    private void pageExposureReport(int i) {
        if (pageNeedExposureReport(i)) {
            pageExposureReport();
            this.pageHasExposureReport = true;
        }
    }

    private boolean pageNeedExposureReport(int i) {
        if (i != 60 && i != 30 && i != 50) {
            return false;
        }
        if (this.currentState != i) {
            this.pageHasExposureReport = false;
        }
        if (!this.pageHasExposureReport) {
            return true;
        }
        return false;
    }

    private void pageExposureReport() {
        int i = 2000;
        int activityPageId2 = getActivityPageId();
        if (this.context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) this.context;
            if (activityPageId2 == 2000) {
                activityPageId2 = baseActivity.f();
            }
            i = baseActivity.m();
        }
        l.a(new STInfoV2(activityPageId2, STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100));
    }

    public void setVisibility(int i) {
        if (!(this.errorImg == null || i == 0)) {
            this.errorImg.setImageResource(0);
        }
        super.setVisibility(i);
    }

    public int getCurrentState() {
        return this.currentState;
    }
}
