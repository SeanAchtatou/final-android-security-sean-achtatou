package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewInvalidateMessage f674a;
    final /* synthetic */ CommonViewInvalidater b;

    b(CommonViewInvalidater commonViewInvalidater, ViewInvalidateMessage viewInvalidateMessage) {
        this.b = commonViewInvalidater;
        this.f674a = viewInvalidateMessage;
    }

    public void run() {
        if (this.f674a.target != null) {
            this.f674a.target.handleMessage(this.f674a);
        } else {
            this.b.handleMessage(this.f674a);
        }
        this.b.handleQueueMsg();
    }
}
