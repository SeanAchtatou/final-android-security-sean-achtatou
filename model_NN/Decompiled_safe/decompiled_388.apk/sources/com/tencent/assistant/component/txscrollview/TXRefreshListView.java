package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.bd;

/* compiled from: ProGuard */
public class TXRefreshListView extends TXRefreshScrollViewBase<ListView> {
    protected ListAdapter c;
    protected TXLoadingLayoutBase d;
    protected TXLoadingLayoutBase e;
    protected ITXRefreshListLoadingLayoutCreateCallBack f = null;

    public TXRefreshListView(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    public TXRefreshListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public TXRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TXRefreshListView(Context context) {
        super(context);
    }

    public void setAdapter(ListAdapter listAdapter) {
        this.c = listAdapter;
        ((ListView) this.s).setAdapter(listAdapter);
    }

    public ListAdapter getAdapter() {
        return ((ListView) this.s).getAdapter();
    }

    public ListAdapter getRawAdapter() {
        return this.c;
    }

    public void setDivider(Drawable drawable) {
        ((ListView) this.s).setDivider(drawable);
    }

    public void setVerticalScrollBarEnabled(boolean z) {
        ((ListView) this.s).setVerticalScrollBarEnabled(z);
    }

    public void setSelector(Drawable drawable) {
        if (this.s != null && drawable != null) {
            ((ListView) this.s).setSelector(drawable);
        }
    }

    public void addHeaderView(View view) {
        if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.o == TXScrollViewBase.ScrollMode.NONE) {
            ((ListView) this.s).addHeaderView(view);
        }
    }

    public void addFooterView(View view) {
        if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.o == TXScrollViewBase.ScrollMode.NONE) {
            ((ListView) this.s).addFooterView(view);
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((ListView) this.s).setOnItemClickListener(onItemClickListener);
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        ((ListView) this.s).setOnItemSelectedListener(onItemSelectedListener);
    }

    public void reset() {
        if (this.j == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            a(TXRefreshScrollViewBase.RefreshState.RESET);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        int i;
        int i2;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        boolean z;
        TXLoadingLayoutBase tXLoadingLayoutBase2 = null;
        boolean z2 = true;
        boolean z3 = false;
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null && this.d != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase3 = this.g;
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.d;
            int contentSize = 0 - this.g.getContentSize();
            if (Math.abs(((ListView) this.s).getFirstVisiblePosition() - 0) <= 1) {
                z = true;
            } else {
                z = false;
            }
            i = contentSize;
            i2 = 0;
            z3 = z;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase4;
            tXLoadingLayoutBase = tXLoadingLayoutBase3;
        } else if (this.n != TXScrollViewBase.ScrollState.ScrollState_FromEnd || this.h == null || this.e == null) {
            i = 0;
            i2 = 0;
            tXLoadingLayoutBase = null;
        } else {
            TXLoadingLayoutBase tXLoadingLayoutBase5 = this.h;
            TXLoadingLayoutBase tXLoadingLayoutBase6 = this.e;
            int count = ((ListView) this.s).getCount() - 1;
            int contentSize2 = this.h.getContentSize();
            if (Math.abs(((ListView) this.s).getLastVisiblePosition() - count) > 1) {
                z2 = false;
            }
            z3 = z2;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase6;
            i = contentSize2;
            i2 = count;
            tXLoadingLayoutBase = tXLoadingLayoutBase5;
        }
        if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
            tXLoadingLayoutBase2.setVisibility(8);
            tXLoadingLayoutBase.showAllSubViews();
            if (z3) {
                ((ListView) this.s).setSelection(i2);
                a(i);
            }
        }
        super.b();
    }

    /* access modifiers changed from: protected */
    public void c() {
        int i;
        TXLoadingLayoutBase tXLoadingLayoutBase;
        TXLoadingLayoutBase tXLoadingLayoutBase2;
        TXLoadingLayoutBase tXLoadingLayoutBase3;
        int i2;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            super.c();
            return;
        }
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null && this.d != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.g;
            TXLoadingLayoutBase tXLoadingLayoutBase5 = this.d;
            TXLoadingLayoutBase tXLoadingLayoutBase6 = this.e;
            i = getScrollY() + this.g.getContentSize();
            tXLoadingLayoutBase3 = tXLoadingLayoutBase4;
            tXLoadingLayoutBase2 = tXLoadingLayoutBase5;
            tXLoadingLayoutBase = tXLoadingLayoutBase6;
            i2 = 0;
        } else if (this.n != TXScrollViewBase.ScrollState.ScrollState_FromEnd || this.h == null || this.e == null) {
            i = 0;
            tXLoadingLayoutBase = null;
            tXLoadingLayoutBase2 = null;
            tXLoadingLayoutBase3 = null;
            i2 = 0;
        } else {
            tXLoadingLayoutBase3 = this.h;
            tXLoadingLayoutBase2 = this.e;
            tXLoadingLayoutBase = this.d;
            i2 = ((ListView) this.s).getCount() - 1;
            i = getScrollY() - this.h.getContentSize();
        }
        if (tXLoadingLayoutBase3 != null) {
            tXLoadingLayoutBase3.reset();
            tXLoadingLayoutBase3.hideAllSubViews();
        }
        if (tXLoadingLayoutBase != null) {
            tXLoadingLayoutBase.setVisibility(8);
        }
        if (tXLoadingLayoutBase2 != null) {
            tXLoadingLayoutBase2.setVisibility(0);
            tXLoadingLayoutBase2.refreshing();
        }
        this.i = false;
        a(i);
        ((ListView) this.s).setSelection(i2);
        smoothScrollTo(0, new p(this));
    }

    /* access modifiers changed from: protected */
    public void d() {
        int i;
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null && this.d != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase = this.g;
            TXLoadingLayoutBase tXLoadingLayoutBase2 = this.d;
            int contentSize = 0 - this.g.getContentSize();
            boolean z = Math.abs(((ListView) this.s).getFirstVisiblePosition() - 0) <= 1;
            if (tXLoadingLayoutBase2 != null && tXLoadingLayoutBase2.getVisibility() == 0) {
                tXLoadingLayoutBase2.setVisibility(8);
                tXLoadingLayoutBase.showAllSubViews();
                if (z) {
                    ((ListView) this.s).setSelection(0);
                    a(contentSize);
                }
            }
            super.b();
        } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null && this.e != null) {
            TXLoadingLayoutBase tXLoadingLayoutBase3 = this.h;
            TXLoadingLayoutBase tXLoadingLayoutBase4 = this.e;
            int count = ((ListView) this.s).getCount() - 1;
            int scrollY = getScrollY() - this.h.getContentSize();
            if (tXLoadingLayoutBase3 != null) {
                tXLoadingLayoutBase3.reset();
                tXLoadingLayoutBase3.hideAllSubViews();
            }
            if (tXLoadingLayoutBase4 != null) {
                tXLoadingLayoutBase4.setVisibility(0);
                if (getRawAdapter() != null) {
                    i = getRawAdapter().getCount();
                } else {
                    i = 0;
                }
                tXLoadingLayoutBase4.loadFinish(bd.b(getContext(), i));
            }
            this.i = false;
            smoothScrollTo(0);
        }
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, TXScrollViewBase.ScrollMode scrollMode) {
        TXLoadingLayoutBase tXLoadingLayoutBase = null;
        if (this.f != null) {
            tXLoadingLayoutBase = this.f.createLoadingLayout(context, scrollMode);
        }
        return tXLoadingLayoutBase != null ? tXLoadingLayoutBase : new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, scrollMode);
    }

    /* access modifiers changed from: protected */
    public TXLoadingLayoutBase a(Context context, ListView listView, TXScrollViewBase.ScrollMode scrollMode) {
        FrameLayout frameLayout = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -2, 1);
        TXLoadingLayoutBase a2 = a(context, scrollMode);
        a2.setVisibility(8);
        frameLayout.addView(a2, layoutParams);
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            listView.addHeaderView(frameLayout, null, false);
        } else {
            listView.addFooterView(frameLayout, null, false);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ListView b(Context context) {
        ListView listView = new ListView(context);
        if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.d = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        } else if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END) {
            this.e = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        } else if (this.o == TXScrollViewBase.ScrollMode.BOTH) {
            this.d = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.e = a(context, listView, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        }
        return listView;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ListView) this.s).getFirstVisiblePosition() > 1 || (childAt = ((ListView) this.s).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ListView) this.s).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        View childAt;
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ListView) this.s).getCount() - 1;
        int lastVisiblePosition = ((ListView) this.s).getLastVisiblePosition();
        if (lastVisiblePosition < count - 1 || (childAt = ((ListView) this.s).getChildAt(lastVisiblePosition - ((ListView) this.s).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ListView) this.s).getBottom();
    }

    public void setCreateLoadingLayoutCallBack(ITXRefreshListLoadingLayoutCreateCallBack iTXRefreshListLoadingLayoutCreateCallBack) {
        this.f = iTXRefreshListLoadingLayoutCreateCallBack;
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        if (this.s != null) {
            ((ListView) this.s).setOnScrollListener(onScrollListener);
        }
    }

    public View getListViewChildAt(int i) {
        if (this.s != null) {
            return ((ListView) this.s).getChildAt(i);
        }
        return null;
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    public int getFirstVisiblePosition() {
        if (this.s != null) {
            return ((ListView) this.s).getFirstVisiblePosition();
        }
        return 0;
    }

    public int pointToPosition(int i, int i2) {
        if (this.s != null) {
            return ((ListView) this.s).pointToPosition(i, i2);
        }
        return 0;
    }

    public void setOverscrollFooter(Drawable drawable) {
        if (this.s != null) {
            try {
                ((ListView) this.s).getClass().getMethod("setOverscrollFooter", Drawable.class).invoke(this.s, drawable);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setSelection(int i) {
        ((ListView) this.s).setSelection(i);
    }

    public void setCacheColorHint(int i) {
        ((ListView) this.s).setCacheColorHint(0);
    }
}
