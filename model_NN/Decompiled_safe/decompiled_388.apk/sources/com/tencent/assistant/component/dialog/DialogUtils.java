package com.tencent.assistant.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingDialogView;
import com.tencent.assistant.utils.ah;
import com.tencent.assistantv2.activity.MainActivity;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.component.ShareAppDialog;
import com.tencent.pangu.component.ShareQzView;
import com.tencent.pangu.model.ShareAppModel;
import com.tencent.pangu.model.ShareBaseModel;
import com.tencent.pangu.model.ShareModel;

/* compiled from: ProGuard */
public class DialogUtils {
    public static final float COMMON_DIALOG_WIDTH_PERCENT = 0.867f;
    public static DialogInterface.OnClickListener defaultDismissListener = new a();

    public static void show2BtnDialog(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        Activity n = AstApp.n();
        if ((n != null && !n.isFinishing()) || ((n = MainActivity.t()) != null && !n.isFinishing())) {
            try {
                Dialog dialog = new Dialog(n, R.style.dialog);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new k(twoBtnDialogInfo));
                TwoButtonDialogView twoButtonDialogView = new TwoButtonDialogView(n);
                if (twoButtonDialogView.isInflateSucc()) {
                    twoButtonDialogView.setHasTitle(twoBtnDialogInfo.hasTitle);
                    twoButtonDialogView.setTitleAndMsg(twoBtnDialogInfo.titleRes, twoBtnDialogInfo.contentRes);
                    if (twoBtnDialogInfo.extraMsgView != null) {
                        twoButtonDialogView.addExtraMsgView(twoBtnDialogInfo.extraMsgView);
                    }
                    twoButtonDialogView.setButton(twoBtnDialogInfo.lBtnTxtRes, twoBtnDialogInfo.rBtnTxtRes, new l(twoBtnDialogInfo, dialog), new m(twoBtnDialogInfo, dialog));
                    twoButtonDialogView.setRightButtonTailText(twoBtnDialogInfo.rBtnTailTxtRes);
                    if (!(twoBtnDialogInfo.rBtnTextColorResId == 0 || twoBtnDialogInfo.rBtnBackgroundResId == 0)) {
                        twoButtonDialogView.setButtonStyle(false, twoBtnDialogInfo.rBtnTextColorResId, twoBtnDialogInfo.rBtnBackgroundResId);
                    }
                    dialog.addContentView(twoButtonDialogView, new ViewGroup.LayoutParams(-1, -2));
                    dialog.setOwnerActivity(n);
                    Window window = dialog.getWindow();
                    window.setLayout(-1, -2);
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.gravity = 17;
                    attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
                    window.setAttributes(attributes);
                    if (!n.isFinishing()) {
                        dialog.show();
                        if (!twoBtnDialogInfo.blockCaller && (n instanceof BaseActivity)) {
                            ((BaseActivity) n).a(dialog);
                        }
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void show2BtnDialog(Activity activity, AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        if (activity != null && !activity.isFinishing()) {
            try {
                Dialog dialog = new Dialog(activity, R.style.dialog);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new n(twoBtnDialogInfo));
                TwoButtonDialogView twoButtonDialogView = new TwoButtonDialogView(activity);
                if (twoButtonDialogView.isInflateSucc()) {
                    twoButtonDialogView.setHasTitle(twoBtnDialogInfo.hasTitle);
                    twoButtonDialogView.setTitleAndMsg(twoBtnDialogInfo.titleRes, twoBtnDialogInfo.contentRes);
                    if (twoBtnDialogInfo.extraMsgView != null) {
                        twoButtonDialogView.addExtraMsgView(twoBtnDialogInfo.extraMsgView);
                    }
                    twoButtonDialogView.setButton(twoBtnDialogInfo.lBtnTxtRes, twoBtnDialogInfo.rBtnTxtRes, new o(twoBtnDialogInfo, dialog), new p(twoBtnDialogInfo, dialog));
                    if (!(twoBtnDialogInfo.rBtnTextColorResId == 0 || twoBtnDialogInfo.rBtnBackgroundResId == 0)) {
                        twoButtonDialogView.setButtonStyle(false, twoBtnDialogInfo.rBtnTextColorResId, twoBtnDialogInfo.rBtnBackgroundResId);
                    }
                    dialog.addContentView(twoButtonDialogView, new ViewGroup.LayoutParams(-1, -2));
                    dialog.setOwnerActivity(activity);
                    Window window = dialog.getWindow();
                    window.setLayout(-1, -2);
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.gravity = 17;
                    attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
                    window.setAttributes(attributes);
                    if (!activity.isFinishing()) {
                        dialog.show();
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static Dialog get2BtnDialog(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        return a(AstApp.n(), twoBtnDialogInfo);
    }

    public static Dialog get2BtnDialog(Context context, AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        if (context instanceof Activity) {
            return a((Activity) context, twoBtnDialogInfo);
        }
        return get2BtnDialog(twoBtnDialogInfo);
    }

    private static Dialog a(Activity activity, AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }
        Dialog dialog = new Dialog(activity, R.style.dialog);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new q(twoBtnDialogInfo));
        TwoButtonDialogView twoButtonDialogView = new TwoButtonDialogView(activity);
        if (!twoButtonDialogView.isInflateSucc()) {
            return null;
        }
        twoButtonDialogView.setHasTitle(twoBtnDialogInfo.hasTitle);
        twoButtonDialogView.setTitleAndMsg(twoBtnDialogInfo.titleRes, twoBtnDialogInfo.contentRes);
        if (twoBtnDialogInfo.extraMsgView != null) {
            twoButtonDialogView.addExtraMsgView(twoBtnDialogInfo.extraMsgView);
        }
        twoButtonDialogView.setButton(twoBtnDialogInfo.lBtnTxtRes, twoBtnDialogInfo.rBtnTxtRes, new r(twoBtnDialogInfo, dialog), new b(twoBtnDialogInfo, dialog));
        dialog.addContentView(twoButtonDialogView, new ViewGroup.LayoutParams(-1, -2));
        dialog.setOwnerActivity(activity);
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
        window.setAttributes(attributes);
        if (!twoBtnDialogInfo.blockCaller && (activity instanceof BaseActivity)) {
            ((BaseActivity) activity).a(dialog);
        }
        return dialog;
    }

    public static Dialog show1BtnDialog(AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        Activity n = AstApp.n();
        if (n == null || n.isFinishing()) {
            return null;
        }
        Dialog dialog = new Dialog(n, R.style.dialog);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new c(oneBtnDialogInfo));
        try {
            OneButtonDialogView oneButtonDialogView = new OneButtonDialogView(n);
            oneButtonDialogView.setTitleAndMsg(oneBtnDialogInfo.hasTitle, oneBtnDialogInfo.titleRes, oneBtnDialogInfo.contentRes);
            oneButtonDialogView.setButton(oneBtnDialogInfo.btnTxtRes, new d(oneBtnDialogInfo, dialog));
            dialog.addContentView(oneButtonDialogView, new ViewGroup.LayoutParams(-1, -2));
            dialog.setOwnerActivity(n);
            Window window = dialog.getWindow();
            window.setLayout(-1, -2);
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.gravity = 17;
            attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
            window.setAttributes(attributes);
            if (!n.isFinishing()) {
                try {
                    dialog.show();
                    if (!oneBtnDialogInfo.blockCaller && (n instanceof BaseActivity)) {
                        ((BaseActivity) n).a(dialog);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return dialog;
        } catch (Throwable th) {
            return null;
        }
    }

    public static void show1BtnDialog(Activity activity, AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        if (activity != null && !activity.isFinishing()) {
            try {
                Dialog dialog = new Dialog(activity, R.style.dialog);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new e(oneBtnDialogInfo));
                OneButtonDialogView oneButtonDialogView = new OneButtonDialogView(activity);
                oneButtonDialogView.setTitleAndMsg(oneBtnDialogInfo.hasTitle, oneBtnDialogInfo.titleRes, oneBtnDialogInfo.contentRes);
                oneButtonDialogView.setButton(oneBtnDialogInfo.btnTxtRes, new f(oneBtnDialogInfo, dialog));
                dialog.addContentView(oneButtonDialogView, new ViewGroup.LayoutParams(-1, -2));
                dialog.setOwnerActivity(activity);
                dialog.getWindow().setLayout(-1, -2);
                if (!activity.isFinishing()) {
                    dialog.show();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static Dialog showLoadingDialog(AppConst.LoadingDialogInfo loadingDialogInfo) {
        return showLoadingDialog(AstApp.n(), loadingDialogInfo);
    }

    public static Dialog showLoadingDialog(Activity activity, AppConst.LoadingDialogInfo loadingDialogInfo) {
        if (activity == null || activity.isFinishing()) {
            return null;
        }
        Dialog dialog = new Dialog(activity, R.style.dialog);
        dialog.setCancelable(true);
        LoadingDialogView loadingDialogView = new LoadingDialogView(activity);
        loadingDialogView.setVisibility(0);
        loadingDialogView.setLoadingText(loadingDialogInfo.loadingText);
        dialog.addContentView(loadingDialogView, new ViewGroup.LayoutParams(-1, -1));
        dialog.setOwnerActivity(activity);
        if (!activity.isFinishing()) {
            try {
                dialog.show();
                if ((activity instanceof BaseActivity) && !loadingDialogInfo.blockCaller) {
                    ((BaseActivity) activity).a(dialog);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return dialog;
    }

    public static void showShareQzDialog(Activity activity, ShareModel shareModel, AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        if (activity != null && !activity.isFinishing() && twoBtnDialogInfo != null) {
            Dialog dialog = new Dialog(activity, R.style.dialog);
            dialog.setCancelable(true);
            ShareQzView shareQzView = new ShareQzView(activity);
            shareQzView.a(shareModel);
            shareQzView.a(new g(twoBtnDialogInfo, dialog));
            dialog.setOnCancelListener(new h(twoBtnDialogInfo));
            dialog.addContentView(shareQzView, new ViewGroup.LayoutParams(-1, -2));
            dialog.setOwnerActivity(activity);
            if (!activity.isFinishing()) {
                dialog.show();
            }
            Window window = dialog.getWindow();
            window.setLayout(-1, -2);
            WindowManager.LayoutParams attributes = window.getAttributes();
            attributes.gravity = 17;
            attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
            window.setAttributes(attributes);
            ah.a().postDelayed(new i(dialog), 500);
        }
    }

    public static ShareAppDialog showShareYYBDialog(ShareBaseActivity shareBaseActivity, ShareAppModel shareAppModel, int i) {
        return a(shareBaseActivity, shareAppModel, null, i, 0, 0.861d);
    }

    public static ShareAppDialog showShareDialog(ShareBaseActivity shareBaseActivity, ShareAppModel shareAppModel, View view, int i, int i2) {
        if (shareBaseActivity == null || shareBaseActivity.isFinishing()) {
            return null;
        }
        ShareAppDialog a2 = a(shareBaseActivity, view, i, i2);
        if (a2 == null) {
            return a2;
        }
        a2.a(shareAppModel);
        a2.b();
        return a2;
    }

    private static ShareAppDialog a(ShareBaseActivity shareBaseActivity, ShareAppModel shareAppModel, View view, int i, int i2, double d) {
        if (shareBaseActivity == null || shareBaseActivity.isFinishing()) {
            return null;
        }
        ShareAppDialog a2 = a(shareBaseActivity, view, i, i2, d);
        if (a2 == null) {
            return a2;
        }
        a2.a(shareAppModel);
        a2.b();
        return a2;
    }

    public static ShareAppDialog showShareDialog(ShareBaseActivity shareBaseActivity, ShareBaseModel shareBaseModel, View view, int i, int i2) {
        if (shareBaseActivity == null || shareBaseActivity.isFinishing()) {
            return null;
        }
        ShareAppDialog a2 = a(shareBaseActivity, view, i, i2);
        if (a2 == null) {
            return a2;
        }
        a2.a(shareBaseModel);
        a2.b();
        return a2;
    }

    private static ShareAppDialog a(ShareBaseActivity shareBaseActivity, View view, int i, int i2) {
        ShareAppDialog shareAppDialog = new ShareAppDialog(shareBaseActivity, R.style.dialog, view);
        shareAppDialog.setCancelable(true);
        shareAppDialog.setOwnerActivity(shareBaseActivity);
        shareAppDialog.a(i);
        shareAppDialog.b(i2);
        if (!shareBaseActivity.isFinishing()) {
            try {
                shareAppDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        Window window = shareAppDialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        attributes.width = (int) (((double) shareAppDialog.getWindow().getWindowManager().getDefaultDisplay().getWidth()) * 0.861d);
        window.setAttributes(attributes);
        return shareAppDialog;
    }

    private static ShareAppDialog a(ShareBaseActivity shareBaseActivity, View view, int i, int i2, double d) {
        ShareAppDialog shareAppDialog = new ShareAppDialog(shareBaseActivity, R.style.dialog, view);
        shareAppDialog.setCancelable(true);
        shareAppDialog.setOwnerActivity(shareBaseActivity);
        shareAppDialog.a(i);
        shareAppDialog.b(i2);
        if (!shareBaseActivity.isFinishing()) {
            try {
                shareAppDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        Window window = shareAppDialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.gravity = 17;
        attributes.width = (int) (((double) shareAppDialog.getWindow().getWindowManager().getDefaultDisplay().getWidth()) * d);
        window.setAttributes(attributes);
        return shareAppDialog;
    }

    public static void showNoWIFIDialog() {
        Activity n = AstApp.n();
        if (n != null && !n.isFinishing()) {
            show2BtnDialog(getNoWIFIDialogInfo(n));
        }
    }

    public static AppConst.TwoBtnDialogInfo getNoWIFIDialogInfo(Activity activity) {
        j jVar = new j(activity);
        jVar.titleRes = activity.getString(R.string.dialog_title_network_unavaliable);
        jVar.contentRes = activity.getString(R.string.dialog_content_network_unavaliable_tips);
        jVar.rBtnTxtRes = activity.getString(R.string.disconnected_setting);
        jVar.lBtnTxtRes = activity.getString(R.string.cancel);
        return jVar;
    }

    public static void showContentViewDialog(Activity activity, View view) {
        if (activity != null && !activity.isFinishing()) {
            try {
                Dialog dialog = new Dialog(activity, R.style.dialog);
                dialog.setCancelable(true);
                dialog.addContentView(view, new ViewGroup.LayoutParams(-1, -2));
                dialog.setOwnerActivity(activity);
                Window window = dialog.getWindow();
                window.setLayout(-1, -2);
                WindowManager.LayoutParams attributes = window.getAttributes();
                attributes.gravity = 17;
                attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
                window.setAttributes(attributes);
                if (!activity.isFinishing()) {
                    dialog.show();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
