package com.tencent.assistant.component;

import android.os.Message;
import android.view.animation.Animation;

/* compiled from: ProGuard */
class ao implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f636a;

    ao(TotalTabLayout totalTabLayout) {
        this.f636a = totalTabLayout;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f636a.mIsAniming = false;
        if (this.f636a.mCurrentTabIndex != this.f636a.mLastAnimIndex) {
            Message obtain = Message.obtain();
            obtain.arg1 = this.f636a.mCurrentTabIndex;
            obtain.arg2 = this.f636a.mLastAnimIndex;
            this.f636a.animationHandler.sendMessage(obtain);
        }
    }
}
