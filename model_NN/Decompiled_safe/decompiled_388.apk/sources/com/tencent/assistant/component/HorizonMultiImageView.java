package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.by;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class HorizonMultiImageView extends LinearLayout {
    private int MAX_SIZE = 3;
    private ArrayList<String> mUrls = new ArrayList<>();
    private int mWidth = 0;

    public HorizonMultiImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public HorizonMultiImageView(Context context) {
        super(context);
        init();
    }

    private void init() {
    }

    public void setImageViewUrls(List<String> list) {
        if (list != null && list.size() != 0) {
            this.mUrls.clear();
            this.mUrls.addAll(list);
            refreshView();
        }
    }

    private void refreshView() {
        removeAllViews();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(by.a(getContext(), 6.0f), -1);
        layoutParams.weight = 1.0f;
        int size = this.mUrls.size();
        int i = 0;
        while (i < size && i < this.MAX_SIZE) {
            TXImageView tXImageView = new TXImageView(getContext());
            tXImageView.updateImageView(this.mUrls.get(i), R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            tXImageView.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(tXImageView, layoutParams);
            if (i < this.MAX_SIZE - 1) {
                addView(new View(getContext()), layoutParams2);
            }
            i++;
        }
        for (int i2 = 0; i2 < this.MAX_SIZE - size; i2++) {
            TXImageView tXImageView2 = new TXImageView(getContext());
            tXImageView2.updateImageView(Constants.STR_EMPTY, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            tXImageView2.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(tXImageView2, layoutParams);
            if (i2 < (this.MAX_SIZE - size) - 1) {
                addView(new View(getContext()), layoutParams2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
    }
}
