package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class LoadingView extends RelativeLayout {
    private TextView loadingInfo = null;

    public LoadingView(Context context) {
        super(context);
        init(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.loading_layout, this);
        this.loadingInfo = (TextView) findViewById(R.id.loading_info);
    }

    public void setLoadingInfoVisibile(boolean z) {
        if (this.loadingInfo != null) {
            this.loadingInfo.setVisibility(z ? 0 : 8);
        }
    }

    public void setLoadingInfo(String str) {
        if (this.loadingInfo != null) {
            this.loadingInfo.setText(str);
        }
    }
}
