package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public abstract class TXLoadingLayoutBase extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    protected TXScrollViewBase.ScrollDirection f693a;
    protected TXScrollViewBase.ScrollMode b;

    public abstract int getContentSize();

    public abstract int getTriggerSize();

    public abstract void hideAllSubViews();

    public abstract void loadFail();

    public abstract void loadFinish(String str);

    public abstract void loadSuc();

    public abstract void onPull(int i);

    public abstract void pullToRefresh();

    public abstract void refreshFail(String str);

    public abstract void refreshSuc();

    public abstract void refreshing();

    public abstract void releaseToRefresh();

    public abstract void reset();

    public abstract void setHeight(int i);

    public abstract void setWidth(int i);

    public abstract void showAllSubViews();

    public TXLoadingLayoutBase(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context);
        this.f693a = scrollDirection;
        this.b = scrollMode;
    }

    public TXLoadingLayoutBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void draw(Canvas canvas) {
        try {
            super.draw(canvas);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}
