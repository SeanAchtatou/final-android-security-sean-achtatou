package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;

/* compiled from: ProGuard */
public class a extends SoftReference<Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    public String f1788a = null;

    public a(String str, Bitmap bitmap, ReferenceQueue<Bitmap> referenceQueue) {
        super(bitmap, referenceQueue);
        this.f1788a = str;
    }
}
