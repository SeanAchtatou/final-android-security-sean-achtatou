package com.tencent.assistant.thumbnailCache;

import java.util.concurrent.Callable;

/* compiled from: ProGuard */
class i implements Callable<o> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f1796a;
    final /* synthetic */ f b;

    i(f fVar, o oVar) {
        this.b = fVar;
        this.f1796a = oVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:70:0x0222 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:54:0x01b9 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:28:0x0135 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: byte[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v28, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v29, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v30, resolved type: byte[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v32, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v42, resolved type: byte[]} */
    /* JADX WARN: Type inference failed for: r2v1 */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v33 */
    /* JADX WARN: Type inference failed for: r2v43 */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0034, code lost:
        if (r3.isRecycled() != false) goto L_0x0036;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0151 A[SYNTHETIC, Splitter:B:33:0x0151] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0234  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x02ce  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0306  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:70:0x0222=Splitter:B:70:0x0222, B:54:0x01b9=Splitter:B:54:0x01b9} */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.thumbnailCache.o call() {
        /*
            r10 = this;
            r9 = 10
            r2 = 0
            r8 = 0
            r7 = 2
            android.os.Process.setThreadPriority(r9)
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            long r5 = r1.c
            long r3 = r3 - r5
            int r1 = (int) r3
            r0.d = r1
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            int r1 = r1.d()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r0 = r0[r1]     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            java.lang.String r1 = r1.c()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            android.graphics.Bitmap r3 = r0.a(r1)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            if (r3 == 0) goto L_0x0036
            boolean r0 = r3.isRecycled()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r0 == 0) goto L_0x0188
        L_0x0036:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ all -> 0x01ab }
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d     // Catch:{ all -> 0x01ab }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ all -> 0x01ab }
            int r1 = r1.d()     // Catch:{ all -> 0x01ab }
            r0 = r0[r1]     // Catch:{ all -> 0x01ab }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ all -> 0x01ab }
            java.lang.String r1 = r1.c()     // Catch:{ all -> 0x01ab }
            byte[] r1 = r0.d(r1)     // Catch:{ all -> 0x01ab }
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a     // Catch:{ all -> 0x0300 }
            int r4 = r4.d()     // Catch:{ all -> 0x0300 }
            r0 = r0[r4]     // Catch:{ all -> 0x0300 }
            int r0 = r0.h     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.f r4 = r10.b     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.b[] r4 = r4.d     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.o r5 = r10.f1796a     // Catch:{ all -> 0x0300 }
            int r5 = r5.d()     // Catch:{ all -> 0x0300 }
            r4 = r4[r5]     // Catch:{ all -> 0x0300 }
            int r4 = r4.h     // Catch:{ all -> 0x0300 }
            android.graphics.Bitmap r3 = com.tencent.assistant.utils.m.a(r1, r0, r4)     // Catch:{ all -> 0x0300 }
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.utils.n r0 = r0.g     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            r0.a(r1)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r3 != 0) goto L_0x030c
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            int r0 = r0.d()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            switch(r0) {
                case 3: goto L_0x01e2;
                case 4: goto L_0x01e2;
                case 5: goto L_0x01e2;
                case 6: goto L_0x01e2;
                case 7: goto L_0x01e2;
                case 8: goto L_0x01e2;
                default: goto L_0x0086;
            }     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
        L_0x0086:
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r0 = r0.c()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r1 = "http"
            boolean r0 = r0.startsWith(r1)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r0 != 0) goto L_0x00a2
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r0 = r0.c()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r1 = "https"
            boolean r0 = r0.startsWith(r1)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r0 == 0) goto L_0x030c
        L_0x00a2:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r1 = r1.c()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.module.nac.d r4 = com.tencent.assistant.module.nac.d.a()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.thumbnailCache.o r5 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r5 = r5.c()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.module.nac.f r4 = r4.a(r5)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            java.lang.String r4 = r4.a()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.thumbnailCache.o r5 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            int r5 = r5.d()     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            byte[] r0 = r0.a(r1, r4, r5)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            r1 = 1
            int r4 = r0.length     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r1 == r4) goto L_0x030c
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.b[] r1 = r1.d     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a     // Catch:{ all -> 0x0215 }
            int r4 = r4.d()     // Catch:{ all -> 0x0215 }
            r1 = r1[r4]     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a     // Catch:{ all -> 0x0215 }
            java.lang.String r4 = r4.c()     // Catch:{ all -> 0x0215 }
            byte[] r2 = r1.d(r4)     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.b[] r1 = r1.d     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a     // Catch:{ all -> 0x0215 }
            int r4 = r4.d()     // Catch:{ all -> 0x0215 }
            r1 = r1[r4]     // Catch:{ all -> 0x0215 }
            int r1 = r1.h     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.f r4 = r10.b     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.b[] r4 = r4.d     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.o r5 = r10.f1796a     // Catch:{ all -> 0x0215 }
            int r5 = r5.d()     // Catch:{ all -> 0x0215 }
            r4 = r4[r5]     // Catch:{ all -> 0x0215 }
            int r4 = r4.h     // Catch:{ all -> 0x0215 }
            android.graphics.Bitmap r1 = com.tencent.assistant.utils.m.a(r2, r1, r4)     // Catch:{ all -> 0x0215 }
            com.tencent.assistant.thumbnailCache.f r3 = r10.b     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.utils.n r3 = r3.g     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            r3.a(r2)     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            if (r1 != 0) goto L_0x0309
            com.tencent.assistant.thumbnailCache.f r2 = r10.b     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.thumbnailCache.b[] r2 = r2.d     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r3 = r3.d()     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            r2 = r2[r3]     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r2 = r2.h     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.thumbnailCache.f r3 = r10.b     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.thumbnailCache.b[] r3 = r3.d     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r4 = r4.d()     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            r3 = r3[r4]     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r3 = r3.h     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            android.graphics.Bitmap r2 = com.tencent.assistant.utils.m.a(r0, r2, r3)     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
        L_0x0135:
            if (r2 == 0) goto L_0x014e
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.b[] r1 = r1.d     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            int r3 = r3.d()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1 = r1[r3]     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            java.lang.String r3 = r3.c()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1.a(r3, r0)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
        L_0x014e:
            r1 = r2
        L_0x014f:
            if (r1 == 0) goto L_0x0306
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r0 = r0.d()     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            if (r0 != r9) goto L_0x0303
            int r0 = r1.getWidth()     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r2 = r1.getHeight()     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            int r0 = java.lang.Math.min(r0, r2)     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            android.graphics.Bitmap$Config r2 = android.graphics.Bitmap.Config.ARGB_4444     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            android.graphics.Bitmap r2 = com.tencent.assistant.utils.am.a(r1, r0, r0, r2)     // Catch:{ Exception -> 0x02fc, OutOfMemoryError -> 0x02f5, all -> 0x02ee }
            if (r2 == r1) goto L_0x0170
            r1.recycle()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
        L_0x0170:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            int r1 = r1.d()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r0 = r0[r1]     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            java.lang.String r1 = r1.c()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r3 = r2
        L_0x0188:
            if (r3 == 0) goto L_0x02ce
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            long r1 = java.lang.System.currentTimeMillis()
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a
            long r4 = r4.c
            long r1 = r1 - r4
            int r1 = (int) r1
            com.tencent.assistant.thumbnailCache.o r2 = r10.f1796a
            int r2 = r2.d
            int r1 = r1 - r2
            r0.e = r1
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            r0.f = r3
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
        L_0x01a5:
            r0.a(r1, r8)
        L_0x01a8:
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            return r0
        L_0x01ab:
            r0 = move-exception
            r1 = r2
        L_0x01ad:
            com.tencent.assistant.thumbnailCache.f r2 = r10.b     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.utils.n r2 = r2.g     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            r2.a(r1)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            throw r0     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
        L_0x01b7:
            r0 = move-exception
            r2 = r3
        L_0x01b9:
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ all -> 0x0251 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ all -> 0x0251 }
            r4 = 2
            r1.a(r3, r4)     // Catch:{ all -> 0x0251 }
            r0.printStackTrace()     // Catch:{ all -> 0x0251 }
            if (r2 == 0) goto L_0x0292
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            long r5 = r1.c
            long r3 = r3 - r5
            int r1 = (int) r3
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a
            int r3 = r3.d
            int r1 = r1 - r3
            r0.e = r1
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            r0.f = r2
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            goto L_0x01a5
        L_0x01e2:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            android.graphics.Bitmap r2 = r0.c(r1)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            if (r2 == 0) goto L_0x014e
            com.tencent.assistant.thumbnailCache.f r0 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            byte[] r0 = r0.a(r2)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.b[] r1 = r1.d     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            int r3 = r3.d()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1 = r1[r3]     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            java.lang.String r3 = r3.c()     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1.a(r3, r0)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            com.tencent.assistant.utils.n r1 = r1.g     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1.a(r0)     // Catch:{ Exception -> 0x02f9, OutOfMemoryError -> 0x02f2 }
            r1 = r2
            goto L_0x014f
        L_0x0215:
            r0 = move-exception
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            com.tencent.assistant.utils.n r1 = r1.g     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            r1.a(r2)     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
            throw r0     // Catch:{ Exception -> 0x01b7, OutOfMemoryError -> 0x0220, all -> 0x02ea }
        L_0x0220:
            r0 = move-exception
            r2 = r3
        L_0x0222:
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ all -> 0x0251 }
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a     // Catch:{ all -> 0x0251 }
            r4 = 2
            r1.a(r3, r4)     // Catch:{ all -> 0x0251 }
            com.tencent.assistant.thumbnailCache.f r1 = r10.b     // Catch:{ all -> 0x0251 }
            r1.d()     // Catch:{ all -> 0x0251 }
            r0.printStackTrace()     // Catch:{ all -> 0x0251 }
            if (r2 == 0) goto L_0x02b2
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            long r5 = r1.c
            long r3 = r3 - r5
            int r1 = (int) r3
            com.tencent.assistant.thumbnailCache.o r3 = r10.f1796a
            int r3 = r3.d
            int r1 = r1 - r3
            r0.e = r1
            com.tencent.assistant.thumbnailCache.o r0 = r10.f1796a
            r0.f = r2
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            goto L_0x01a5
        L_0x0251:
            r0 = move-exception
        L_0x0252:
            if (r2 == 0) goto L_0x0273
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            long r3 = java.lang.System.currentTimeMillis()
            com.tencent.assistant.thumbnailCache.o r5 = r10.f1796a
            long r5 = r5.c
            long r3 = r3 - r5
            int r3 = (int) r3
            com.tencent.assistant.thumbnailCache.o r4 = r10.f1796a
            int r4 = r4.d
            int r3 = r3 - r4
            r1.e = r3
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            r1.f = r2
            com.tencent.assistant.thumbnailCache.f r1 = r10.b
            com.tencent.assistant.thumbnailCache.o r2 = r10.f1796a
            r1.a(r2, r8)
        L_0x0272:
            throw r0
        L_0x0273:
            com.tencent.assistant.thumbnailCache.f r1 = r10.b
            com.tencent.assistant.thumbnailCache.b[] r1 = r1.d
            com.tencent.assistant.thumbnailCache.o r2 = r10.f1796a
            int r2 = r2.d()
            r1 = r1[r2]
            com.tencent.assistant.thumbnailCache.o r2 = r10.f1796a
            java.lang.String r2 = r2.c()
            r1.f(r2)
            com.tencent.assistant.thumbnailCache.f r1 = r10.b
            com.tencent.assistant.thumbnailCache.o r2 = r10.f1796a
            r1.a(r2, r7)
            goto L_0x0272
        L_0x0292:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            int r1 = r1.d()
            r0 = r0[r1]
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            java.lang.String r1 = r1.c()
            r0.f(r1)
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
        L_0x02ad:
            r0.a(r1, r7)
            goto L_0x01a8
        L_0x02b2:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            int r1 = r1.d()
            r0 = r0[r1]
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            java.lang.String r1 = r1.c()
            r0.f(r1)
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            goto L_0x02ad
        L_0x02ce:
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.b[] r0 = r0.d
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            int r1 = r1.d()
            r0 = r0[r1]
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            java.lang.String r1 = r1.c()
            r0.f(r1)
            com.tencent.assistant.thumbnailCache.f r0 = r10.b
            com.tencent.assistant.thumbnailCache.o r1 = r10.f1796a
            goto L_0x02ad
        L_0x02ea:
            r0 = move-exception
            r2 = r3
            goto L_0x0252
        L_0x02ee:
            r0 = move-exception
            r2 = r1
            goto L_0x0252
        L_0x02f2:
            r0 = move-exception
            goto L_0x0222
        L_0x02f5:
            r0 = move-exception
            r2 = r1
            goto L_0x0222
        L_0x02f9:
            r0 = move-exception
            goto L_0x01b9
        L_0x02fc:
            r0 = move-exception
            r2 = r1
            goto L_0x01b9
        L_0x0300:
            r0 = move-exception
            goto L_0x01ad
        L_0x0303:
            r2 = r1
            goto L_0x0170
        L_0x0306:
            r3 = r1
            goto L_0x0188
        L_0x0309:
            r2 = r1
            goto L_0x0135
        L_0x030c:
            r1 = r3
            goto L_0x014f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.thumbnailCache.i.call():com.tencent.assistant.thumbnailCache.o");
    }
}
