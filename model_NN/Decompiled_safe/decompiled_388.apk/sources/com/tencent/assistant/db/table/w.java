package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.ExternalDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public class w implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f754a = Uri.parse("content://com.tencent.android.qqdownloader.external.provider/qube_app_update_info");

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "qube_app_update_info";
    }

    public String createTableSQL() {
        return "CREATE TABLE qube_app_update_info (key TEXT UNIQUE ON CONFLICT REPLACE PRIMARY KEY,value INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return ExternalDbHelper.get(AstApp.i());
    }
}
