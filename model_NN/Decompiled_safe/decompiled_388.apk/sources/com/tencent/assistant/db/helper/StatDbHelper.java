package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.ah;
import com.tencent.assistant.db.table.e;
import com.tencent.assistant.db.table.g;
import com.tencent.assistant.db.table.y;
import com.tencent.assistant.db.table.z;

/* compiled from: ProGuard */
public class StatDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_ast_stat.db";
    private static final int DB_VERSION = 5;
    private static final Class<?>[] TABLESS = {z.class, ah.class, g.class, e.class, y.class};
    private static final String TAG = StatDbHelper.class.getSimpleName();
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (StatDbHelper.class) {
            if (instance == null) {
                instance = new StatDbHelper(context, DB_NAME, null, 5);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public StatDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 5;
    }
}
