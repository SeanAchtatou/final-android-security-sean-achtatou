package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.db.helper.StatDbHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.open.SocialConstants;
import java.util.List;

/* compiled from: ProGuard */
public class z implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    private static volatile z f756a;

    public z() {
    }

    public z(Context context) {
    }

    public static synchronized z a() {
        z zVar;
        synchronized (z.class) {
            if (f756a == null) {
                f756a = new z(AstApp.i());
            }
            zVar = f756a;
        }
        return zVar;
    }

    public boolean a(byte b, byte[] bArr) {
        if (Global.isDev()) {
            XLog.d("DonaldXu", "save type = " + ((int) b) + " data.length = " + bArr.length);
        }
        if (bArr != null && ((long) bArr.length) > 2097152) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(SocialConstants.PARAM_TYPE, Byte.valueOf(b));
        contentValues.put("data", bArr);
        if (getHelper().getWritableDatabaseWrapper().insert("st_data", null, contentValues) > 0) {
            return true;
        }
        return false;
    }

    public boolean a(byte b, List<byte[]> list) {
        SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
        writableDatabaseWrapper.beginTransaction();
        try {
            SQLiteStatement compileStatement = writableDatabaseWrapper.compileStatement("INSERT INTO st_data (type, data) values (?, ?)");
            for (byte[] next : list) {
                if (next == null || ((long) next.length) <= 2097152) {
                    compileStatement.bindLong(1, (long) b);
                    compileStatement.bindBlob(2, next);
                    compileStatement.executeInsert();
                }
            }
            writableDatabaseWrapper.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            writableDatabaseWrapper.endTransaction();
            throw th;
        }
        writableDatabaseWrapper.endTransaction();
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.Integer> b() {
        /*
            r5 = this;
            r1 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r2 = r5.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r2 = r2.getReadableDatabaseWrapper()
            java.lang.String r3 = "select distinct type from st_data"
            r4 = 0
            android.database.Cursor r2 = r2.rawQuery(r3, r4)     // Catch:{ Exception -> 0x003a, all -> 0x0043 }
            if (r2 == 0) goto L_0x0034
            boolean r3 = r2.moveToFirst()     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            if (r3 == 0) goto L_0x0034
        L_0x001d:
            java.lang.String r3 = "type"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            int r3 = r2.getInt(r3)     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            r0.add(r3)     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            boolean r3 = r2.moveToNext()     // Catch:{ Exception -> 0x004d, all -> 0x004b }
            if (r3 != 0) goto L_0x001d
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()
        L_0x0039:
            return r0
        L_0x003a:
            r0 = move-exception
            r0 = r1
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()
        L_0x0041:
            r0 = r1
            goto L_0x0039
        L_0x0043:
            r0 = move-exception
            r2 = r1
        L_0x0045:
            if (r2 == 0) goto L_0x004a
            r2.close()
        L_0x004a:
            throw r0
        L_0x004b:
            r0 = move-exception
            goto L_0x0045
        L_0x004d:
            r0 = move-exception
            r0 = r2
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.z.b():java.util.List");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0133  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.assistant.db.table.aa a(byte r16, int r17) {
        /*
            r15 = this;
            com.tencent.assistant.db.table.aa r11 = new com.tencent.assistant.db.table.aa
            r11.<init>()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r11.b = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r11.f747a = r1
            if (r17 > 0) goto L_0x00fc
            r1 = 50
            r10 = r1
        L_0x0018:
            boolean r1 = com.tencent.assistant.Global.isDev()
            if (r1 == 0) goto L_0x004e
            java.lang.String r1 = "DonaldXu"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDatas type = "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " count = "
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " limit = "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            com.tencent.assistant.utils.XLog.d(r1, r2)
        L_0x004e:
            com.tencent.assistant.db.helper.SqliteHelper r1 = r15.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r1 = r1.getReadableDatabaseWrapper()
            r12 = 0
            r13 = 0
            java.lang.String r2 = "st_data"
            r3 = 0
            java.lang.String r4 = "type = ?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            r6 = 0
            java.lang.String r7 = java.lang.Byte.toString(r16)     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            r6 = 0
            r7 = 0
            java.lang.String r8 = "_id asc"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            r9.<init>()     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            java.lang.String r14 = "0,"
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            java.lang.String r9 = r9.toString()     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            android.database.Cursor r2 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Throwable -> 0x0107, all -> 0x012f }
            if (r2 == 0) goto L_0x00f5
            boolean r1 = r2.moveToFirst()     // Catch:{ Throwable -> 0x0139 }
            if (r1 == 0) goto L_0x00f5
            r1 = r13
        L_0x008b:
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0139 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r5 = "data"
            int r5 = r2.getColumnIndex(r5)     // Catch:{ Throwable -> 0x0139 }
            byte[] r5 = r2.getBlob(r5)     // Catch:{ Throwable -> 0x0139 }
            java.util.List<java.lang.Long> r6 = r11.f747a     // Catch:{ Throwable -> 0x0139 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ Throwable -> 0x0139 }
            r6.add(r3)     // Catch:{ Throwable -> 0x0139 }
            java.util.List<byte[]> r3 = r11.b     // Catch:{ Throwable -> 0x0139 }
            r3.add(r5)     // Catch:{ Throwable -> 0x0139 }
            int r3 = r5.length     // Catch:{ Throwable -> 0x0139 }
            long r3 = (long) r3     // Catch:{ Throwable -> 0x0139 }
            boolean r5 = com.tencent.assistant.Global.isDev()     // Catch:{ Throwable -> 0x0139 }
            if (r5 == 0) goto L_0x00ed
            java.lang.String r5 = "DonaldXu"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0139 }
            r6.<init>()     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r7 = "getDatas type = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0139 }
            r0 = r16
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r7 = " "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0139 }
            java.lang.StringBuilder r6 = r6.append(r1)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r7 = "/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0139 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r7 = " data.length = "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0139 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ Throwable -> 0x0139 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0139 }
            com.tencent.assistant.utils.XLog.d(r5, r3)     // Catch:{ Throwable -> 0x0139 }
        L_0x00ed:
            int r1 = r1 + 1
            boolean r3 = r2.moveToNext()     // Catch:{ Throwable -> 0x0139 }
            if (r3 != 0) goto L_0x008b
        L_0x00f5:
            if (r2 == 0) goto L_0x00fa
            r2.close()
        L_0x00fa:
            r1 = r11
        L_0x00fb:
            return r1
        L_0x00fc:
            r1 = 500(0x1f4, float:7.0E-43)
            r0 = r17
            int r1 = java.lang.Math.min(r1, r0)
            r10 = r1
            goto L_0x0018
        L_0x0107:
            r1 = move-exception
            r2 = r12
        L_0x0109:
            r1.printStackTrace()     // Catch:{ all -> 0x0137 }
            java.lang.String r3 = "DonaldXu"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0137 }
            r4.<init>()     // Catch:{ all -> 0x0137 }
            java.lang.String r5 = "getDatas Exception: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0137 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0137 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0137 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0137 }
            com.tencent.assistant.utils.XLog.d(r3, r1)     // Catch:{ all -> 0x0137 }
            r1 = 0
            if (r2 == 0) goto L_0x00fb
            r2.close()
            goto L_0x00fb
        L_0x012f:
            r1 = move-exception
            r2 = r12
        L_0x0131:
            if (r2 == 0) goto L_0x0136
            r2.close()
        L_0x0136:
            throw r1
        L_0x0137:
            r1 = move-exception
            goto L_0x0131
        L_0x0139:
            r1 = move-exception
            goto L_0x0109
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.z.a(byte, int):com.tencent.assistant.db.table.aa");
    }

    public boolean a(List<Long> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer("(");
        for (Long append : list) {
            stringBuffer.append(append);
            stringBuffer.append(",");
        }
        if (stringBuffer.length() > 1) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        stringBuffer.append(")");
        getHelper().getWritableDatabaseWrapper().delete("st_data", "_id in " + stringBuffer.toString(), null);
        return true;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "st_data";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists st_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER,data BLOB);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 3) {
            return null;
        }
        return new String[]{"delete from st_data where type in (5, 14, 15)"};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return StatDbHelper.get(AstApp.i());
    }
}
