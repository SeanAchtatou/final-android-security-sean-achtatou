package com.tencent.assistant.sdk;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.sdk.param.jce.BatchDownloadActionResponse;
import com.tencent.assistant.sdk.param.jce.IPCDownloadParam;
import com.tencent.assistant.sdk.param.jce.IPCQueryDownloadInfo;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.pangu.b.a.a;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class g extends r {
    private final String n = "SDKBatchQueryResolver";

    public g(Context context, IPCRequest iPCRequest) {
        super(context, iPCRequest);
    }

    /* access modifiers changed from: protected */
    public void a(JceStruct jceStruct) {
        super.a(jceStruct);
    }

    /* access modifiers changed from: protected */
    public JceStruct a() {
        if (this.l == null || this.l.b == null) {
            return null;
        }
        this.m = new BatchDownloadActionResponse();
        this.m.f1659a = this.k;
        ArrayList arrayList = new ArrayList();
        Iterator<IPCDownloadParam> it = this.l.b.iterator();
        while (it.hasNext()) {
            IPCDownloadParam next = it.next();
            if (next != null) {
                DownloadInfo a2 = n.a(next.a());
                IPCQueryDownloadInfo iPCQueryDownloadInfo = new IPCQueryDownloadInfo();
                if (a2 == null || a2.response == null) {
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(next.a().d, Integer.valueOf(next.a().c).intValue(), 0);
                    if (localApkInfo != null) {
                        iPCQueryDownloadInfo.d = localApkInfo.occupySize;
                        iPCQueryDownloadInfo.e = localApkInfo.occupySize;
                        iPCQueryDownloadInfo.b = localApkInfo.mLocalFilePath;
                        iPCQueryDownloadInfo.c = 4;
                    }
                } else {
                    iPCQueryDownloadInfo.d = a2.response.f3766a;
                    iPCQueryDownloadInfo.e = a2.response.b;
                    iPCQueryDownloadInfo.b = a2.getCurrentValidPath();
                    iPCQueryDownloadInfo.c = a.a(a2);
                }
                iPCQueryDownloadInfo.f = DownloadProxy.a().l();
                iPCQueryDownloadInfo.g = DownloadProxy.a().k();
                arrayList.add(iPCQueryDownloadInfo);
            }
        }
        this.m.a(arrayList);
        return this.m;
    }
}
