package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class e implements j {

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager[] f784a = null;

    public Object a(int i, Context context) {
        char c;
        if (this.f784a == null) {
            try {
                this.f784a = new TelephonyManager[2];
                this.f784a[0] = (TelephonyManager) context.getSystemService("phone");
                this.f784a[1] = (TelephonyManager) context.getSystemService("phone2");
            } catch (Exception e) {
            }
        }
        if (this.f784a == null) {
            return null;
        }
        TelephonyManager[] telephonyManagerArr = this.f784a;
        if (i <= 0) {
            c = 0;
        } else {
            c = 1;
        }
        return telephonyManagerArr[c];
    }

    public String b(int i, Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) a(i, context);
        if (i <= 0) {
            try {
                return telephonyManager.getSubscriberId();
            } catch (Exception e) {
                return null;
            }
        } else {
            if (telephonyManager != null) {
                try {
                    Method declaredMethod = telephonyManager.getClass().getDeclaredMethod("getSubscriberId", null);
                    if (declaredMethod != null) {
                        declaredMethod.setAccessible(true);
                        str = (String) declaredMethod.invoke(telephonyManager, null);
                        return str;
                    }
                } catch (Exception e2) {
                    return null;
                }
            }
            str = null;
            return str;
        }
    }

    public String c(int i, Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }
}
