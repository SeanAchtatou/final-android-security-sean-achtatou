package com.tencent.assistant.kapalaiadapter.a;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.tencent.assistant.kapalaiadapter.g;

/* compiled from: ProGuard */
public class b implements j {

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager f781a = null;

    public Object a(int i, Context context) {
        if (this.f781a == null) {
            try {
                this.f781a = (TelephonyManager) g.a("android.telephony.TelephonyManager", "getDefault", (Object[]) null, (Class<?>[]) null);
            } catch (Exception e) {
            }
        }
        return this.f781a;
    }

    public String b(int i, Context context) {
        try {
            return (String) g.a((TelephonyManager) a(i, context), "getSubscriberIdGemini", new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
            return null;
        }
    }

    public String c(int i, Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }
}
