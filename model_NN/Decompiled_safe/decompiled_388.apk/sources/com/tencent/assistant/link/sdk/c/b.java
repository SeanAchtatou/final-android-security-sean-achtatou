package com.tencent.assistant.link.sdk.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.link.sdk.a.a;

/* compiled from: ProGuard */
public class b extends e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f800a = b.class.getSimpleName();
    private static volatile e b;
    private static final Class<?>[] c = {a.class};

    public static synchronized e a(Context context) {
        e eVar;
        synchronized (b.class) {
            if (b == null) {
                b = new b(context, "mobile_app_link.db", null, 1);
            }
            eVar = b;
        }
        return eVar;
    }

    public b(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, a.a() + "/" + str, null, i);
    }

    public Class<?>[] a() {
        return c;
    }

    public int b() {
        return 1;
    }
}
