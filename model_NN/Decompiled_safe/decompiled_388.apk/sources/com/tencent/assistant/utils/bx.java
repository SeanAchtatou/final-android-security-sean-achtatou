package com.tencent.assistant.utils;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.connect.common.Constants;
import com.tencent.d.a.a;
import com.tencent.nucleus.socialcontact.login.p;
import com.tencent.pangu.mediadownload.q;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class bx {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1841a = bx.class.getSimpleName();

    public static String a() {
        String h5ServerAddress = Global.getH5ServerAddress();
        if (TextUtils.isEmpty(h5ServerAddress)) {
            return "http://qzs.qq.com/open/video/index.html";
        }
        String str = Constants.STR_EMPTY;
        if (!h5ServerAddress.startsWith("http://")) {
            str = str + "http://";
        }
        String str2 = str + h5ServerAddress;
        int lastIndexOf = str2.lastIndexOf("/");
        if (lastIndexOf >= 0 && lastIndexOf == str2.length() - 1) {
            str2 = str2.substring(0, str2.length() - 1);
        }
        return str2 + "/open/video/index.html";
    }

    public static int a(q qVar) {
        if (qVar == null || qVar.u == null || 0 == qVar.u.f3881a) {
            return 0;
        }
        XLog.i(f1841a, "[getVideoDownloadPercent] ---> length = " + qVar.u.b + " , totalLength = " + qVar.u.f3881a);
        return (int) ((((double) qVar.u.b) / ((double) qVar.u.f3881a)) * 100.0d);
    }

    public static String b(q qVar) {
        if (qVar == null) {
            return Constants.STR_EMPTY;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("videoDownloadState", qVar.s);
        jSONObject.put("videoId", qVar.m);
        jSONObject.put("percent", a(qVar));
        return jSONObject.toString();
    }

    public static String a(List<q> list) {
        if (list == null) {
            return Constants.STR_EMPTY;
        }
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("videoDownloadState", list.get(i).s);
            jSONObject2.put("videoId", list.get(i).m);
            jSONObject2.put("percent", a(list.get(i)));
            jSONArray.put(jSONObject2);
        }
        jSONObject.put("videoDownloadInfoListSize", size);
        jSONObject.put("videoDownloadInfoList", jSONArray);
        return jSONObject.toString();
    }

    private static String b() {
        return "MIICXQIBAAKBgQCv1UJVKMxJt4J4d8lSIjgVkPkiU4wb+c09zCR9bT/W6WmCE/IQiRIh/xB23AJs35F6qo/4l3BsfVvvD9xXfmz7s6SKJ5zWPyHimKpZcNfzuwMCos4F3GWu9hPtyD5b0SIBiCdCUgfSx8WfKOvkTnVevv9VM6P0T+DbUEYodo2F+wIDAQABAoGAWC+oHhfhJZOxDwRc5yGcaUyVdt7aJWnKwxSwtGtnmjzkmsWwIRTuEDjhpAtB+CLEzvXeUTp6ux7ATgzxYLxaoalWMMW5cr5KhY0UgkY9hNuXVDsPj3qVuTE7J+SHHB5i/i3KCYUUzik08ktFJcGTf4/DH/djkxuFPNIzqEcIIuECQQDggL0k01djIZIfVGY+UELad0TRyTfNkakQb9ygT7D3//bUbik15rjJ8qhLXNRLnhlDGxImSG1jgna3Z8IteSKrAkEAyIB9p5MG5yz3jktK5dSf+amMpwhaLfkLIZoOzmtLN5lHfDJ6tuBYmmU0eIPowl2jL2838xcr3gcE24jW1uOp8QJBAITUpxS61uGYY1SWI0iLRILuPpHBdHr0zAslpGxVumeB0xEtfMSfloYmRN0SN6nmCRxjUAHGqLJP5t3tTj6JhbECQQCUfIwSSVGIbQXdDKeoM+JRzB4NKaNXfqOeu/ARMOagg1SshtnYi6cymJbWVaJQJ3aNz4kY72UeZgZT6zBoLmyRAkA+06BGYDyGEkONQYHQ+U86h2SRUnVcxDd02g7RLygNmgIbVGQwXPqVoL/isbkvNNaRmGdo37SX6yLPelzlL2C2";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.login.p.a(java.lang.String, java.lang.String, boolean):byte[]
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.nucleus.socialcontact.login.p.a(byte[], java.lang.String, boolean):java.lang.String
      com.tencent.nucleus.socialcontact.login.p.a(java.lang.String, java.lang.String, boolean):byte[] */
    public static String a(String str) {
        byte[] c;
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append("vid:" + str + "[" + (System.currentTimeMillis() / 1000) + "]");
        }
        String k = t.k();
        if (!TextUtils.isEmpty(k)) {
            sb.append(";mac:" + k);
        }
        String h = t.h();
        if (!TextUtils.isEmpty(h)) {
            sb.append(";imsi:" + h);
        }
        XLog.i(f1841a, "[getTencentVideoRSAKey] : " + sb.toString());
        byte[] a2 = p.a(sb.toString(), b(), false);
        if (a2 == null || (c = a.c(a2, 0)) == null) {
            return Constants.STR_EMPTY;
        }
        return new String(c);
    }
}
