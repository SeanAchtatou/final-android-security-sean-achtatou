package com.tencent.assistant.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: ProGuard */
public class ak {
    public static al a(Context context, String str, String str2, Bundle bundle) {
        Bundle bundle2;
        int i;
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        HttpGet httpGet = null;
        if (context == null || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || ((activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable())) {
            if (bundle != null) {
                bundle2 = new Bundle(bundle);
            } else {
                bundle2 = null;
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            if (str2.equals(Constants.HTTP_GET)) {
                String a2 = a(bundle2);
                i = a2.length() + 0;
                if (str.indexOf("?") == -1 && a2.length() > 0) {
                    str = str + "?";
                } else if (a2.length() > 0) {
                    str = str + "&";
                }
                String str3 = str + a2;
                Log.d("HttpUtil", "request url = " + str3);
                httpGet = new HttpGet(str3);
                httpGet.addHeader("Accept-Encoding", "gzip");
            } else if (str2.equals(Constants.HTTP_POST)) {
                HttpGet httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                if (bundle2 == null || bundle2.isEmpty()) {
                    i = 0;
                } else {
                    Bundle bundle3 = new Bundle();
                    for (String next : bundle2.keySet()) {
                        Object obj = bundle2.get(next);
                        if (obj instanceof byte[]) {
                            bundle3.putByteArray(next, (byte[]) obj);
                        }
                    }
                    if (!bundle2.containsKey("method")) {
                        bundle2.putString("method", str2);
                    }
                    httpPost.setHeader("Connection", "Keep-Alive");
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byteArrayOutputStream.write(b(bundle2).getBytes());
                    if (!bundle3.isEmpty()) {
                        for (String next2 : bundle3.keySet()) {
                            byteArrayOutputStream.write(("Content-Disposition: form-data; name=\"" + next2 + "\"; filename=\"" + next2 + "\"" + "\r\n").getBytes());
                            byteArrayOutputStream.write("Content-Type: content/unknown\r\n\r\n".getBytes());
                            byteArrayOutputStream.write(bundle3.getByteArray(next2));
                        }
                    }
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    i = byteArray.length + 0;
                    byteArrayOutputStream.close();
                    httpPost.setEntity(new ByteArrayEntity(byteArray));
                }
                httpGet = httpPost;
            } else {
                i = 0;
            }
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new al(a(execute), i);
            }
            Log.d("HttpUtil", "Http response error. status code : " + statusCode);
            throw new Exception("Http response exception.. status code : " + statusCode);
        }
        Log.e("HttpUtil", "network unavalible...");
        Toast.makeText(context, "亲，你没联网啊！", 0).show();
        throw new Exception("network unavaliable exception..");
    }

    public static String a(Bundle bundle) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if ((obj instanceof String) || (obj instanceof String[])) {
                if (obj instanceof String[]) {
                    String[] stringArray = bundle.getStringArray(next);
                    if (stringArray != null) {
                        if (z) {
                            z = false;
                        } else {
                            sb.append("&");
                        }
                        sb.append(URLEncoder.encode(next) + "=");
                        for (int i = 0; i < stringArray.length; i++) {
                            if (i == 0) {
                                sb.append(URLEncoder.encode(stringArray[i]));
                            } else {
                                sb.append(URLEncoder.encode("," + stringArray[i]));
                            }
                        }
                    }
                } else {
                    if (z) {
                        z = false;
                    } else {
                        sb.append("&");
                    }
                    sb.append(URLEncoder.encode(next) + "=" + URLEncoder.encode(bundle.getString(next)));
                }
                z = z;
            }
        }
        return sb.toString();
    }

    public static String b(Bundle bundle) {
        if (bundle == null) {
            return Constants.STR_EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        for (String next : bundle.keySet()) {
            Object obj = bundle.get(next);
            if (obj instanceof String) {
                sb.append("Content-Disposition: form-data; name=\"" + next + "\"\r\n\r\n" + obj);
                sb.append("\r\n");
            }
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0043 A[SYNTHETIC, Splitter:B:22:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048 A[Catch:{ Exception -> 0x0079 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x006b A[SYNTHETIC, Splitter:B:39:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0070 A[Catch:{ Exception -> 0x0074 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(org.apache.http.HttpResponse r7) {
        /*
            r3 = 0
            r6 = -1
            java.lang.String r1 = ""
            org.apache.http.HttpEntity r0 = r7.getEntity()
            java.io.InputStream r4 = r0.getContent()     // Catch:{ Exception -> 0x0086, all -> 0x0066 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0089, all -> 0x007e }
            r2.<init>()     // Catch:{ Exception -> 0x0089, all -> 0x007e }
            java.lang.String r0 = "Content-Encoding"
            org.apache.http.Header r0 = r7.getFirstHeader(r0)     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
            if (r0 == 0) goto L_0x0090
            java.lang.String r0 = r0.getValue()     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
            java.lang.String r3 = "gzip"
            int r0 = r0.indexOf(r3)     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
            if (r0 <= r6) goto L_0x0090
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x008d, all -> 0x0081 }
        L_0x002e:
            r0 = 512(0x200, float:7.175E-43)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x003d }
        L_0x0032:
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x003d }
            if (r4 == r6) goto L_0x004d
            r5 = 0
            r2.write(r0, r5, r4)     // Catch:{ Exception -> 0x003d }
            goto L_0x0032
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            r0.printStackTrace()     // Catch:{ all -> 0x0083 }
            if (r3 == 0) goto L_0x0046
            r3.close()     // Catch:{ Exception -> 0x0079 }
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ Exception -> 0x0079 }
        L_0x004b:
            r0 = r1
        L_0x004c:
            return r0
        L_0x004d:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x003d }
            byte[] r4 = r2.toByteArray()     // Catch:{ Exception -> 0x003d }
            r0.<init>(r4)     // Catch:{ Exception -> 0x003d }
            if (r3 == 0) goto L_0x005b
            r3.close()     // Catch:{ Exception -> 0x0061 }
        L_0x005b:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ Exception -> 0x0061 }
            goto L_0x004c
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004c
        L_0x0066:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x0069:
            if (r4 == 0) goto L_0x006e
            r4.close()     // Catch:{ Exception -> 0x0074 }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()     // Catch:{ Exception -> 0x0074 }
        L_0x0073:
            throw r0
        L_0x0074:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0073
        L_0x0079:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004b
        L_0x007e:
            r0 = move-exception
            r2 = r3
            goto L_0x0069
        L_0x0081:
            r0 = move-exception
            goto L_0x0069
        L_0x0083:
            r0 = move-exception
            r4 = r3
            goto L_0x0069
        L_0x0086:
            r0 = move-exception
            r2 = r3
            goto L_0x003e
        L_0x0089:
            r0 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x003e
        L_0x008d:
            r0 = move-exception
            r3 = r4
            goto L_0x003e
        L_0x0090:
            r3 = r4
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.ak.a(org.apache.http.HttpResponse):java.lang.String");
    }
}
