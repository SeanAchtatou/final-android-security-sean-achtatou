package com.tencent.assistant.utils;

import android.os.Environment;
import android.os.StatFs;
import com.tencent.connect.common.Constants;
import java.text.DecimalFormat;

/* compiled from: ProGuard */
public class at {
    public static long a() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }

    public static String a(float f, boolean z) {
        StringBuilder sb = new StringBuilder(new DecimalFormat("0.00").format((double) ((f / 1024.0f) / 1024.0f)));
        if (z) {
            return sb.append("MB").toString();
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.at.a(float, boolean):java.lang.String
     arg types: [float, int]
     candidates:
      com.tencent.assistant.utils.at.a(float, float):java.lang.String
      com.tencent.assistant.utils.at.a(long, int):java.lang.String
      com.tencent.assistant.utils.at.a(float, boolean):java.lang.String */
    public static String a(float f, float f2) {
        StringBuilder sb = new StringBuilder();
        sb.append(a(f, false)).append("/").append(a(f2, true));
        return sb.toString();
    }

    public static String a(long j) {
        float f = (float) (j / 1024);
        if (f < 1000.0f) {
            return ((int) f) + "KB";
        }
        float f2 = f / 1024.0f;
        String str = "MB";
        if (f2 >= 1000.0f) {
            f2 /= 1024.0f;
            str = "GB";
        }
        return (Constants.STR_EMPTY + (((double) Math.round(f2 * 100.0f)) / 100.0d)) + str;
    }

    public static String a(long j, int i) {
        float f;
        String str;
        DecimalFormat decimalFormat;
        float f2 = ((((float) j) * 1.0f) / 1024.0f) / 1024.0f;
        if (f2 >= 1024.0f) {
            f = f2 / 1024.0f;
            str = "GB";
        } else {
            f = f2;
            str = "MB";
        }
        switch (i) {
            case 0:
                decimalFormat = new DecimalFormat("#0");
                break;
            case 1:
                decimalFormat = new DecimalFormat("#0.0");
                break;
            default:
                decimalFormat = new DecimalFormat("#0.00");
                break;
        }
        String format = decimalFormat.format((double) f);
        if (format.equals("0.0")) {
            format = "0.1";
        }
        return format + str;
    }

    public static String b(long j) {
        float f = ((float) (j / 1024)) / 1024.0f;
        String str = "M";
        if (f >= 1024.0f) {
            f /= 1024.0f;
            str = "G";
        }
        return new DecimalFormat("#0.0").format((double) f) + str;
    }

    public static String c(long j) {
        float f;
        String str;
        DecimalFormat decimalFormat;
        float f2 = (((float) j) * 1.0f) / 1024.0f;
        if (f2 < 1000.0f) {
            f = f2;
            str = "KB";
        } else {
            float f3 = f2 / 1024.0f;
            if (f3 >= 1000.0f) {
                f = f3 / 1024.0f;
                str = "GB";
            } else {
                f = f3;
                str = "MB";
            }
        }
        if (f < 100.0f) {
            decimalFormat = new DecimalFormat("#0.0");
        } else {
            decimalFormat = new DecimalFormat("#0");
        }
        return decimalFormat.format((double) f) + str;
    }
}
