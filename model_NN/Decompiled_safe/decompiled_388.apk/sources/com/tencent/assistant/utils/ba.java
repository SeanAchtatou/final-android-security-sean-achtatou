package com.tencent.assistant.utils;

import android.text.TextUtils;
import java.util.Comparator;
import java.util.HashMap;

/* compiled from: ProGuard */
class ba implements Comparator<HashMap<String, String>> {
    private ba() {
    }

    /* renamed from: a */
    public int compare(HashMap<String, String> hashMap, HashMap<String, String> hashMap2) {
        int i;
        if (hashMap == null || hashMap2 == null) {
            return 0;
        }
        try {
            int longValue = (int) (Long.valueOf(hashMap2.get("lastModifiedTime")).longValue() - Long.valueOf(hashMap.get("lastModifiedTime")).longValue());
            if (longValue == 0) {
                String str = hashMap.get("packageName");
                String str2 = hashMap.get("versionCode");
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                    if (!str.equals(hashMap2.get("packageName")) || !str2.equals(hashMap2.get("versionCode"))) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    return i;
                }
            }
            i = longValue;
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
