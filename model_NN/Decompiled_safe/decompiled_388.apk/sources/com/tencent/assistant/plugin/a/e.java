package com.tencent.assistant.plugin.a;

import android.text.TextUtils;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.sdk.param.jce.IPCCmd;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.IPCResponse;
import com.tencent.c.a.a;

/* compiled from: ProGuard */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static int f1080a = 0;

    public static synchronized int a() {
        int i;
        synchronized (e.class) {
            i = f1080a;
            f1080a = i + 1;
        }
        return i;
    }

    public static byte[] a(JceStruct jceStruct) {
        IPCRequest iPCRequest = new IPCRequest();
        IPCHead iPCHead = new IPCHead();
        iPCHead.f1665a = a();
        iPCHead.b = b(jceStruct);
        iPCRequest.a(iPCHead);
        iPCRequest.b = a.a(jceStruct);
        return iPCRequest.toByteArray("utf-8");
    }

    public static JceStruct a(IPCResponse iPCResponse) {
        JceStruct a2 = a(IPCCmd.a(iPCResponse.f1668a.b).toString());
        if (a2 != null && iPCResponse.b.length > 0) {
            try {
                byte[] b = a.b(iPCResponse.b, "ji*9^&43U0X-~./(".getBytes());
                if (b != null) {
                    JceInputStream jceInputStream = new JceInputStream(b);
                    jceInputStream.setServerEncoding("utf-8");
                    a2.readFrom(jceInputStream);
                    return a2;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    private static JceStruct a(String str) {
        JceStruct jceStruct;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            jceStruct = (JceStruct) Class.forName((com.tencent.assistant.sdk.param.a.class.getPackage().getName() + ".jce." + str) + "Response").newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            jceStruct = null;
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
            jceStruct = null;
        } catch (InstantiationException e3) {
            e3.printStackTrace();
            jceStruct = null;
        }
        return jceStruct;
    }

    public static int b(JceStruct jceStruct) {
        if (jceStruct == null) {
            return -1;
        }
        String simpleName = jceStruct.getClass().getSimpleName();
        return IPCCmd.a(simpleName.substring(0, simpleName.length() - "Request".length())).a();
    }
}
