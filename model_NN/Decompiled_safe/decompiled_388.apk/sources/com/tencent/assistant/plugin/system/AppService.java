package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class AppService extends BaseAppService {
    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getAppServiceImpl();
        }
        return null;
    }
}
