package com.tencent.assistant.plugin.proxy;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.PluginLoaderInfo;
import com.tencent.assistant.plugin.annotation.PluginActivityAnnotaion;
import com.tencent.assistant.plugin.mgr.h;
import com.tencent.assistant.plugin.mgr.i;

/* compiled from: ProGuard */
public class PluginProxyActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    static ClassLoader f1107a;
    Toast b = null;
    private ClassLoader c;
    private Class<?> d = null;
    private PluginActivity e = null;
    private String f = null;
    private String g = null;
    private int h = 0;
    private int i = 0;
    private String j = null;

    public static void a(Context context, String str, int i2, String str2, int i3, Intent intent, String str3) {
        a(context, str, i2, str2, i3, intent, -1, str3);
    }

    public static void a(Context context, String str, int i2, String str2, int i3, Intent intent, int i4, String str3) {
        PluginInfo a2 = i.b().a(str, i2);
        PluginLoaderInfo a3 = h.a(context, a2);
        if (TextUtils.isEmpty(str2) && a2.getPluginEntryList() != null && a2.getPluginEntryList().size() > 0) {
            str2 = a2.getPluginEntryList().get(0).getStartActivity();
        }
        if (a3 == null) {
            Toast.makeText(context, context.getResources().getString(R.string.plugin_not_exist), 0).show();
            return;
        }
        PluginActivityAnnotaion a4 = h.a(a3, str2);
        f1107a = a3.getClassLoader();
        Intent intent2 = new Intent(context, h.a(i3, a4));
        intent2.putExtra("_plugin_PluginPakcageName", str);
        intent2.putExtra("_plugin_PluginVersionCode", i2);
        intent2.putExtra("_plugin_LaunchActivity", str2);
        intent2.putExtra("_plugin_InProcess", i3);
        intent2.putExtra("_plugin_LaunchApplication", str3);
        if (intent != null) {
            intent2.putExtras(intent);
            intent2.setSourceBounds(intent.getSourceBounds());
            intent2.setFlags(intent.getFlags());
        }
        if (!(context instanceof Activity)) {
            intent2.addFlags(268435456);
        }
        if (i4 < 0) {
            context.startActivity(intent2);
        } else if (context instanceof Activity) {
            ((Activity) context).startActivityForResult(intent2, i4);
        } else {
            throw new RuntimeException("call startActivityForResult must have acitivty context.");
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (this.c == null) {
            this.c = f1107a;
        }
        super.onCreate(bundle);
        Intent intent = getIntent();
        if (intent == null) {
            a("插件启动失败，请稍后重试或者重新安装插件");
            finish();
            return;
        }
        if (this.c != null) {
            intent.setExtrasClassLoader(this.c);
        }
        try {
            if (intent.hasExtra(AppConst.KEY_LOGIN_ANIM)) {
                overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            } else {
                overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);
            }
            this.g = intent.getStringExtra("_plugin_PluginPakcageName");
            this.h = intent.getIntExtra("_plugin_PluginVersionCode", 0);
            this.f = intent.getStringExtra("_plugin_LaunchActivity");
            this.i = intent.getIntExtra("_plugin_InProcess", 0);
            this.j = intent.getStringExtra("_plugin_LaunchApplication");
            a(this.g, this.h);
            AstApp.a(this);
            this.e.setIntent(intent);
            try {
                this.e.onCreate(bundle);
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            a("插件启动失败，请稍后重试或者重新安装插件");
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.e != null) {
            try {
                this.e.onStart();
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (this.e != null) {
            try {
                this.e.onRestart();
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (!(intent == null || this.c == null)) {
            intent.setExtrasClassLoader(this.c);
        }
        if (this.e != null) {
            try {
                this.e.onPluginActivityResult(i2, i3, intent);
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        boolean z = false;
        if (this.e != null) {
            z = this.e.onKeyDown(i2, keyEvent);
        }
        if (!z) {
            return super.onKeyDown(i2, keyEvent);
        }
        return z;
    }

    public boolean onKeyLongPress(int i2, KeyEvent keyEvent) {
        boolean z = false;
        if (this.e != null) {
            z = this.e.onKeyLongPress(i2, keyEvent);
        }
        if (!z) {
            return super.onKeyDown(i2, keyEvent);
        }
        return z;
    }

    public void onBackPressed() {
        if (this.e != null && this.e.onPluginBackPressed()) {
            super.onBackPressed();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = this.e.onTouchEvent(motionEvent);
        if (!onTouchEvent) {
            return super.onTouchEvent(motionEvent);
        }
        return onTouchEvent;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.e != null) {
            try {
                this.e.onWindowFocusChanged(z);
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        if (this.e != null) {
            this.e.onPluginRestoreInstanceState(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.e != null) {
            this.e.onPluginSaveInstanceState(bundle);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AstApp.a(this);
        if (this.e != null) {
            try {
                this.e.onResume();
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
                return;
            }
        }
        Log.d("com.qq.connect", getClass().getSimpleName() + " onResume taskID: " + getTaskId());
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (this.e != null) {
            try {
                this.e.onNewIntent(intent);
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.e != null) {
            try {
                this.e.onStop();
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.e != null) {
            try {
                this.e.onPause();
            } catch (Throwable th) {
                this.e.onActivityException(th);
                super.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (AstApp.n() == this) {
            AstApp.a((Activity) null);
        }
        if (this.e != null) {
            try {
                this.e.onDestroy();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.e = null;
        this.d = null;
    }

    private boolean a(String str, int i2) {
        PluginInfo a2 = i.b().a(str, i2);
        PluginLoaderInfo a3 = h.a(getBaseContext(), a2);
        if (TextUtils.isEmpty(this.f) && a2.getPluginEntryList() != null && a2.getPluginEntryList().size() > 0) {
            this.f = a2.getPluginEntryList().get(0).getStartActivity();
        }
        this.c = a3.getClassLoader();
        this.d = a3.loadClass(this.f);
        this.e = (PluginActivity) this.d.newInstance();
        this.e.init(a2, this, a3);
        this.e.setIntent(getIntent());
        return true;
    }

    public void startActivity(Intent intent) {
        startActivityForResult(intent, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void startActivityForResult(Intent intent, int i2) {
        String className;
        if (intent.getBooleanExtra("_plugin_IsPluginActivity", false)) {
            intent.putExtra("_plugin_IsPluginActivity", false);
            String stringExtra = intent.getStringExtra("_plugin_PluginPakcageName");
            int intExtra = intent.getIntExtra("_plugin_PluginVersionCode", 0);
            intent.removeExtra("_plugin_PluginPakcageName");
            intent.removeExtra("_plugin_PluginVersionCode");
            ComponentName component = intent.getComponent();
            if (component != null && (className = component.getClassName()) != null && className.length() > 0) {
                a(this, stringExtra, intExtra, className, this.i, intent, i2, null);
                return;
            }
            return;
        }
        super.startActivityForResult(intent, i2);
    }

    public void a(CharSequence charSequence) {
        if (this.b == null) {
            this.b = Toast.makeText(this, charSequence, 0);
        }
        this.b.setText(charSequence);
        this.b.show();
    }

    public void finish() {
        if (this.e != null) {
            setResult(this.e.getResultCode(), this.e.getResultData());
        }
        super.finish();
    }

    public String a() {
        try {
            if (this.e != null) {
                return this.e.getClass().getSimpleName();
            }
            return getClass().getSimpleName();
        } catch (Exception e2) {
            return "unknown_pluginActivity";
        }
    }
}
