package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public class PluginDownloadInfo implements Comparable<PluginDownloadInfo> {
    public String actionUrl;
    public String desc;
    public int displayOrder;
    public String downUrl;
    public String downloadTicket;
    public long fileSize;
    public String iconUrl;
    public String imgUrl;
    public int minApiLevel;
    public int minBaoVersion;
    public int minPluginVersion;
    public String name;
    public int needPreDownload;
    public int pluginForBaoType;
    public int pluginId;
    public String pluginPackageName;
    public String startActivity;
    public int type;
    public int version;

    public String getDownloadTicket() {
        return String.valueOf(this.pluginPackageName);
    }

    public int compareTo(PluginDownloadInfo pluginDownloadInfo) {
        return this.displayOrder - pluginDownloadInfo.displayOrder;
    }

    public String toString() {
        return "PluginDownloadInfo{pluginId=" + this.pluginId + ", downloadTicket='" + this.downloadTicket + '\'' + ", pluginPackageName='" + this.pluginPackageName + '\'' + ", version=" + this.version + ", name='" + this.name + '\'' + ", desc='" + this.desc + '\'' + ", displayOrder=" + this.displayOrder + ", iconUrl='" + this.iconUrl + '\'' + ", imgUrl='" + this.imgUrl + '\'' + ", fileSize=" + this.fileSize + ", minApiLevel=" + this.minApiLevel + ", minPluginVersion=" + this.minPluginVersion + ", minBaoVersion=" + this.minBaoVersion + ", needPreDownload=" + this.needPreDownload + ", downUrl='" + this.downUrl + '\'' + ", startActivity='" + this.startActivity + '\'' + '}';
    }
}
