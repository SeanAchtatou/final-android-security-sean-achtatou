package com.tencent.assistant.plugin.mgr;

import android.content.Intent;
import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bundle f1106a;
    final /* synthetic */ k b;

    l(k kVar, Bundle bundle) {
        this.b = kVar;
        this.f1106a = bundle;
    }

    public void run() {
        PluginInfo a2 = i.b().a("com.qqreader");
        Intent intent = new Intent();
        intent.putExtras(this.f1106a);
        PluginProxyActivity.a(AstApp.i(), a2.getPackageName(), a2.getVersion(), "com.qqreader.AppSplashActivity", a2.getInProcess(), intent, null);
        com.tencent.assistant.manager.webview.js.l.a(2);
    }
}
