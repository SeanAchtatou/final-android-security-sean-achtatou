package com.tencent.assistant.plugin.a;

import android.os.Bundle;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.login.model.MoblieQIdentityInfo;
import com.tencent.assistant.plugin.PluginIPCClient;
import com.tencent.assistant.plugin.UserLoginInfo;
import com.tencent.assistant.plugin.UserStateInfo;
import com.tencent.assistant.sdk.SDKIPCBroadcaster;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.an;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.login.m;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static d f1078a = new d(null);

    public static UserLoginInfo a(String str) {
        UserLoginInfo userLoginInfo = new UserLoginInfo();
        if (PluginIPCClient.LOGIN_ADDTION_INFO_GET_USER_INFO.equals(str)) {
            j a2 = j.a();
            if (a2.k()) {
                userLoginInfo.setState(0);
                a(a2, userLoginInfo);
            }
        } else if (PluginIPCClient.LOGIN_ADDTION_INFO_LOGIN.equals(str)) {
            AstApp.i().k();
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 7);
            ah.a().post(new c(bundle));
        }
        return userLoginInfo;
    }

    private static void a(j jVar, UserLoginInfo userLoginInfo) {
        MoblieQIdentityInfo moblieQIdentityInfo = (MoblieQIdentityInfo) jVar.c();
        m f = l.f();
        if (moblieQIdentityInfo != null) {
            int i = jVar.w() > 83 ? 2 : 3;
            userLoginInfo.setA2(an.b(moblieQIdentityInfo.getGTKey_ST_A2(), moblieQIdentityInfo.getKey()));
            userLoginInfo.setState(i);
            userLoginInfo.setUin(moblieQIdentityInfo.getUin());
            if (f != null) {
                userLoginInfo.setNickName(f.b);
                userLoginInfo.setPic(f.f3165a);
                return;
            }
            return;
        }
        userLoginInfo.setState(0);
    }

    public static void a(UserStateInfo userStateInfo) {
        SDKIPCBroadcaster.a().a(userStateInfo);
    }

    public static void a() {
        a(new UserStateInfo(4));
    }
}
