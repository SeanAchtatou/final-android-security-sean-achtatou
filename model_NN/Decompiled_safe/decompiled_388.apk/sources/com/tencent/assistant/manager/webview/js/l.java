package com.tencent.assistant.manager.webview.js;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;

/* compiled from: ProGuard */
public class l {

    /* renamed from: a  reason: collision with root package name */
    private static int f933a = 1;

    public static synchronized void a(Context context, String str, String str2) {
        synchronized (l.class) {
            if (!TextUtils.isEmpty(str)) {
                CookieSyncManager.createInstance(context);
                CookieManager instance = CookieManager.getInstance();
                instance.setAcceptCookie(true);
                Uri parse = Uri.parse(str);
                if (!(parse == null || parse.getHost() == null)) {
                    String lowerCase = parse.getHost().toLowerCase();
                    String str3 = null;
                    if (lowerCase.endsWith(".qq.com")) {
                        str3 = ".qq.com";
                    }
                    if ((Constants.SOURCE_QQ.equals(str2) || "ALL".equals(str2)) && j.a().k()) {
                        instance.setCookie(str, a("logintype", j.a().d().name(), str3));
                        instance.setCookie(str, a("skey", j.a().m(), str3));
                        instance.setCookie(str, a("uin", "o0" + j.a().p(), str3));
                        instance.setCookie(str, a("sid", j.a().n(), str3));
                        instance.setCookie(str, a("vkey", j.a().o(), str3));
                    } else if (("WX".equals(str2) || "ALL".equals(str2)) && j.a().l()) {
                        instance.setCookie(str, a("logintype", j.a().d().name(), str3));
                        instance.setCookie(str, a("openid", j.a().s(), str3));
                        instance.setCookie(str, a("accesstoken", j.a().r(), str3));
                    } else {
                        instance.setCookie(str, a("logintype", AppConst.IdentityType.NONE.name(), str3));
                        instance.setCookie(str, a("skey", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("uin", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("openid", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("accesstoken", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("sid", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("vkey", Constants.STR_EMPTY, str3));
                    }
                    instance.setCookie(str, a("imei", t.g(), str3));
                    instance.setCookie(str, a("guid", Global.getPhoneGuid(), str3));
                    CookieSyncManager.getInstance().sync();
                }
            }
        }
    }

    public static synchronized void a(Context context, String str, m mVar) {
        synchronized (l.class) {
            if (!TextUtils.isEmpty(str)) {
                CookieSyncManager.createInstance(context);
                CookieManager instance = CookieManager.getInstance();
                instance.setAcceptCookie(true);
                Uri parse = Uri.parse(str);
                if (!(parse == null || parse.getHost() == null)) {
                    String lowerCase = parse.getHost().toLowerCase();
                    String str2 = null;
                    if (lowerCase.endsWith(".qq.com")) {
                        str2 = ".qq.com";
                    }
                    instance.setCookie(str, a("qopenid", mVar.f934a, str2));
                    instance.setCookie(str, a("qaccesstoken", mVar.b, str2));
                    instance.setCookie(str, a("openappid", mVar.c + Constants.STR_EMPTY, str2));
                    CookieSyncManager.getInstance().sync();
                }
            }
        }
    }

    private static String a(String str, String str2, String str3) {
        String str4 = str + "=" + str2;
        if (str3 == null) {
            return str4;
        }
        return (str4 + "; path=/") + "; domain=" + str3;
    }

    public static boolean a(String str) {
        if (str != null) {
            return str.toLowerCase().endsWith(".qq.com");
        }
        return false;
    }

    public static synchronized void a() {
        synchronized (l.class) {
            CookieSyncManager.createInstance(AstApp.i().getApplicationContext());
            CookieManager.getInstance().removeAllCookie();
            CookieSyncManager.getInstance().sync();
        }
    }

    public static void a(int i) {
        f933a = i;
    }

    public static int b() {
        int i = f933a;
        f933a = 1;
        return i;
    }
}
