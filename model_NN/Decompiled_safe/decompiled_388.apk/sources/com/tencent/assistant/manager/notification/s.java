package com.tencent.assistant.manager.notification;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.qq.AppService.AstApp;

/* compiled from: ProGuard */
class s extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f894a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(r rVar, Looper looper) {
        super(looper);
        this.f894a = rVar;
    }

    public void handleMessage(Message message) {
        if (message.what == 1113) {
            this.f894a.a(AstApp.i(), true);
            synchronized (this.f894a.h) {
                this.f894a.h.notifyAll();
            }
        }
    }
}
