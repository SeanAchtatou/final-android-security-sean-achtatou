package com.tencent.assistant.manager.notification.a;

import android.app.PendingIntent;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.qq.AppService.AstApp;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.NotificationService;
import com.tencent.assistant.manager.notification.a.a.f;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.connect.common.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class s extends a {
    protected static final int[] c = {R.id.icon1, R.id.icon2, R.id.icon3};
    private List<AutoDownloadInfo> d;
    private RemoteViews e;
    private long f = 0;
    private int g = 11;
    private int h = 12;

    public s(int i, List<AutoDownloadInfo> list) {
        super(i);
        this.d = list;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        if (this.d == null || this.d.size() < 3) {
            return false;
        }
        PendingIntent service = PendingIntent.getService(AstApp.i(), this.f864a, a(String.format("http://qzs.qq.com/open/yyb/yyb_zero_traffic/index.html?apps=%s", a(this.d))), 268435456);
        this.e = new RemoteViews(AstApp.i().getPackageName(), (int) R.layout.notification_wise_update_push);
        this.e.setImageViewResource(R.id.big_icon, R.drawable.logo72);
        Spanned fromHtml = Html.fromHtml(AstApp.i().getString(R.string.wise_download_apps_had_been_updated_tips_format, new Object[]{"<font color=\"#f18818\">" + this.d.size() + "</font>"}));
        this.e.setTextViewText(R.id.title, fromHtml);
        this.e.setFloat(R.id.title, "setTextSize", 18.0f);
        ArrayList arrayList = new ArrayList();
        for (AutoDownloadInfo next : this.d) {
            if (next != null && !TextUtils.isEmpty(next.f1166a)) {
                PushIconInfo pushIconInfo = new PushIconInfo();
                pushIconInfo.b = next.f1166a;
                pushIconInfo.f1445a = 2;
                arrayList.add(pushIconInfo);
            }
        }
        a(this.e, arrayList, c);
        this.b = y.a(AstApp.i(), R.drawable.logo32, this.e, fromHtml, System.currentTimeMillis(), service, null, true, false);
        return true;
    }

    private Intent a(String str) {
        Intent intent = new Intent(AstApp.i(), NotificationService.class);
        intent.putExtra("notification_id", this.f864a);
        intent.putExtra("notification_push_id", this.f);
        intent.putExtra("notification_push_extra", a(0));
        intent.putExtra("notification_push_type", this.g);
        intent.putExtra("notification_action", str);
        return intent;
    }

    public String a(int i) {
        return this.f + "|" + this.g + "|" + this.h + "|" + i;
    }

    /* access modifiers changed from: protected */
    public void a(RemoteViews remoteViews, List<PushIconInfo> list, int[] iArr) {
        if (remoteViews != null && list != null && iArr != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < iArr.length && i2 < list.size()) {
                    PushIconInfo pushIconInfo = list.get(i2);
                    if (pushIconInfo != null && !TextUtils.isEmpty(pushIconInfo.b)) {
                        f fVar = new f(pushIconInfo);
                        fVar.a(new t(this, remoteViews, iArr, i2));
                        a(fVar);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private String a(List<AutoDownloadInfo> list) {
        String str;
        if (list != null) {
            str = Constants.STR_EMPTY;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null) {
                    if (i > 0) {
                        str = str + "|";
                    }
                    str = str + list.get(i).f1166a + "," + list.get(i).j + "," + list.get(i).d;
                }
            }
        } else {
            str = Constants.STR_EMPTY;
        }
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static PushInfo e() {
        PushInfo pushInfo = new PushInfo();
        pushInfo.f1446a = 0;
        pushInfo.k = 11;
        pushInfo.g = JceStruct.ZERO_TAG;
        return pushInfo;
    }

    public static String f() {
        return "0|11|12|0";
    }
}
