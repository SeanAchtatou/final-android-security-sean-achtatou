package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.notification.v;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PushInfo f862a;
    final /* synthetic */ l b;

    m(l lVar, PushInfo pushInfo) {
        this.b = lVar;
        this.f862a = pushInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void
     arg types: [int, com.tencent.assistant.protocol.jce.PushInfo, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.assistant.manager.notification.v.a(int, android.app.Notification, int, boolean):void
      com.tencent.assistant.manager.notification.v.a(int, com.tencent.assistant.protocol.jce.PushInfo, byte[], boolean):void */
    public void run() {
        if (this.f862a != null) {
            String a2 = com.tencent.assistant.m.a().a("key_push_info_id_list", Constants.STR_EMPTY);
            String[] split = (a2 + (TextUtils.isEmpty(a2) ? Constants.STR_EMPTY : " ") + this.f862a.a()).split(" ");
            ArrayList arrayList = new ArrayList();
            for (int length = split.length - 1; length >= 0; length--) {
                String str = split[length];
                if (bm.c(str) > 0 && !arrayList.contains(str)) {
                    arrayList.add(str);
                    if (arrayList.size() >= 10) {
                        break;
                    }
                }
            }
            StringBuffer stringBuffer = new StringBuffer(Constants.STR_EMPTY);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                stringBuffer.append(" " + ((String) arrayList.get(size)));
            }
            com.tencent.assistant.m.a().b("key_push_info_id_list", stringBuffer.toString().replaceFirst(" ", Constants.STR_EMPTY));
            if (this.f862a == null) {
                return;
            }
            if (this.f862a.e() < 1) {
                this.b.b(this.f862a);
                AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_PUSH_MESSAGE_NEW));
                return;
            }
            v.a().a(115, this.f862a, (byte[]) null, false);
        }
    }
}
