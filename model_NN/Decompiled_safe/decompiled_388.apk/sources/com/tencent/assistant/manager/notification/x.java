package com.tencent.assistant.manager.notification;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class x extends ApkResCallback.Stub {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f899a;

    x(v vVar) {
        this.f899a = vVar;
    }

    public void onInstalledApkDataChanged(LocalApkInfo localApkInfo, int i) {
        if (localApkInfo != null && i == 2 && this.f899a.f != null && !this.f899a.f.b() && this.f899a.f.a(localApkInfo.mPackageName)) {
            v.a().b(false);
        }
    }
}
