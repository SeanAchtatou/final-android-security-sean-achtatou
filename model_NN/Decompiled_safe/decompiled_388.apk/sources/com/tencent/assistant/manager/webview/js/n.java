package com.tencent.assistant.manager.webview.js;

import android.content.SharedPreferences;
import com.qq.AppService.AstApp;
import com.tencent.assistant.login.PluginLoginIn;
import com.tencent.assistant.utils.aq;

/* compiled from: ProGuard */
public class n {
    private static SharedPreferences a() {
        return AstApp.i().getApplicationContext().getSharedPreferences("openId_map", 0);
    }

    public static void a(long j, String str) {
        SharedPreferences.Editor edit = a().edit();
        edit.putString(aq.b(PluginLoginIn.getUin() + "&" + j), str);
        edit.commit();
    }
}
