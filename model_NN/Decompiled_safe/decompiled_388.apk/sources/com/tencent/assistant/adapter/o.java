package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;

/* compiled from: ProGuard */
class o {

    /* renamed from: a  reason: collision with root package name */
    TXAppIconView f599a;
    TextView b;
    DownloadButton c;
    View d;
    TextView e;
    TextView f;
    ImageView g;
    ListItemInfoView h;
    final /* synthetic */ OneMoreAdapter i;

    private o(OneMoreAdapter oneMoreAdapter) {
        this.i = oneMoreAdapter;
    }

    /* synthetic */ o(OneMoreAdapter oneMoreAdapter, m mVar) {
        this(oneMoreAdapter);
    }
}
