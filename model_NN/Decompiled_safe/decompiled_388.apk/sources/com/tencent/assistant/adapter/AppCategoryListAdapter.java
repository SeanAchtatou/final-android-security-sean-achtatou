package com.tencent.assistant.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.ColorCardView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.component.CommonCategoryView;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AppCategoryListAdapter extends BaseAdapter {
    private View.OnClickListener A = new e(this);

    /* renamed from: a  reason: collision with root package name */
    private final String f571a = "AppCategoryListAdapter";
    private List<ColorCardItem> b = new ArrayList();
    private List<AppCategory> c = new ArrayList();
    private List<AppCategory> d = new ArrayList();
    private LayoutInflater e;
    /* access modifiers changed from: private */
    public CategoryType f;
    /* access modifiers changed from: private */
    public Context g;
    private final int h = 0;
    private final int i = 1;
    private final int j = 2;
    private ColorCardView k = null;
    private CommonCategoryView l = null;
    private final int m = 3;
    private final int n = 3;
    private final int o = 3;
    private final int p = 11;
    private final int q = 20;
    private final int r = 2;
    private final int s = 6;
    private final int t = 2;
    private final int u = 4;
    private int[] v;
    private Drawable w;
    private b x = null;
    private View.OnClickListener y = new c(this);
    /* access modifiers changed from: private */
    public View.OnClickListener z = new d(this);

    /* compiled from: ProGuard */
    public enum CategoryType {
        CATEGORYTYPEGAME,
        CATEGORYTYPESOFTWARE
    }

    public AppCategoryListAdapter(Context context, View view, CategoryType categoryType, List<AppCategory> list, List<AppCategory> list2, long j2) {
        this.g = context;
        this.e = LayoutInflater.from(context);
        this.f = categoryType;
        if (list != null) {
            b(list);
        }
        if (list2 != null) {
            this.d.addAll(list2);
        }
        TypedArray obtainTypedArray = context.getResources().obtainTypedArray(R.array.app_category_parent_txt_array);
        this.v = new int[obtainTypedArray.length()];
        for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
            this.v[i2] = obtainTypedArray.getColor(i2, 0);
        }
        obtainTypedArray.recycle();
        this.w = context.getResources().getDrawable(R.drawable.app_category_icon_operation);
        this.w.setBounds(0, 0, by.a(context, 10.0f), by.a(context, 12.0f));
    }

    public void a(List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        this.b.clear();
        if (list != null) {
            a(list);
        }
        this.c.clear();
        if (list2 != null) {
            b(list2);
        }
        this.d.clear();
        if (list3 != null) {
            this.d.addAll(list3);
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        int i2 = 1;
        int i3 = 0;
        int i4 = a() ? 1 : 0;
        if (!b()) {
            i2 = 0;
        }
        int i5 = i4 + i2;
        if (this.d != null) {
            i3 = this.d.size();
        }
        return i5 + i3;
    }

    /* renamed from: a */
    public AppCategory getItem(int i2) {
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        View view2;
        View inflate;
        g gVar;
        int i3;
        if (getItemViewType(i2) == 0) {
            if (this.k == null) {
                this.k = new ColorCardView(this.g, null, this.y);
                view2 = this.k;
                this.k.refreshData(this.b);
            } else {
                view2 = this.k;
            }
        } else if (getItemViewType(i2) == 1) {
            if (this.l == null) {
                this.l = new CommonCategoryView(this.g, null, this.f, this.z);
                view2 = this.l;
                this.l.a(this.c);
            } else {
                view2 = this.l;
            }
        } else if (getItemViewType(i2) == 2) {
            if (view == null || view.getTag(R.id.category_view_horder) == null) {
                inflate = this.e.inflate((int) R.layout.category_software_item, (ViewGroup) null);
                gVar = null;
            } else {
                gVar = (g) view.getTag(R.id.category_view_horder);
                inflate = view;
            }
            if (gVar == null) {
                g gVar2 = new g(null);
                gVar2.f591a = (ViewGroup) inflate.findViewById(R.id.item_l);
                gVar2.b = (TXImageView) inflate.findViewById(R.id.item_l_icon);
                gVar2.c = (TextView) inflate.findViewById(R.id.item_l_title);
                gVar2.d = inflate.findViewById(R.id.item_l_cover);
                gVar2.e = (LinearLayout) inflate.findViewById(R.id.item_r);
                inflate.setTag(R.id.category_view_horder, gVar2);
                gVar = gVar2;
            }
            if (a()) {
                i3 = i2 - 1;
            } else {
                i3 = i2;
            }
            if (b()) {
                i3--;
            }
            a(i3, gVar);
            view2 = inflate;
        } else {
            view2 = null;
        }
        if (i2 == 0) {
            view2.setPadding(by.a(this.g, 8.0f), by.a(this.g, 8.0f), by.a(this.g, 8.0f), by.a(this.g, 3.0f));
        } else if (i2 == getCount() - 1) {
            view2.setPadding(by.a(this.g, 8.0f), by.a(this.g, 4.0f), by.a(this.g, 8.0f), by.a(this.g, 7.0f));
        } else {
            view2.setPadding(by.a(this.g, 8.0f), by.a(this.g, 4.0f), by.a(this.g, 8.0f), by.a(this.g, 3.0f));
        }
        return view2;
    }

    private void a(int i2, g gVar) {
        AppCategory appCategory = this.d.get(i2);
        a(gVar, appCategory, i2);
        b(gVar, appCategory, i2);
    }

    private int a(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7 = 11;
        if (i2 < 1) {
            i4 = 3;
        } else {
            i4 = i2;
        }
        if (i4 > 3) {
            i5 = 3;
        } else {
            i5 = i4;
        }
        if (i3 < 0) {
            i6 = 0;
        } else {
            i6 = i3;
        }
        if (i6 <= 11) {
            i7 = i6;
        }
        int i8 = ((i7 + i5) - 1) / i5;
        if (i8 <= 3) {
            return i8;
        }
        return 3;
    }

    private void a(g gVar, AppCategory appCategory, int i2) {
        if (appCategory != null && gVar != null) {
            if (gVar.b != null) {
                if (this.v != null && this.v.length > 0) {
                    gVar.b.setBackgroundColor(this.v[i2 % this.v.length]);
                }
                if (appCategory.c != null && !TextUtils.isEmpty(appCategory.c.f1430a)) {
                    gVar.b.updateImageView(appCategory.c.f1430a, R.color.app_category_parent_cover, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                }
            }
            if (gVar.c != null) {
                if (this.v != null && this.v.length > 0) {
                    gVar.c.setTextColor(this.v[i2 % this.v.length]);
                }
                gVar.c.setText(appCategory.b);
            }
            f fVar = new f(appCategory.f1140a, 0, appCategory.b, 0, null, appCategory.b);
            if (gVar.d != null) {
                gVar.d.setTag(R.id.category_pos, Integer.valueOf(i2 * 20));
                gVar.d.setTag(R.id.category_data, fVar);
                gVar.d.setOnClickListener(this.A);
            }
            a(fVar, i2, 0, 100);
        }
    }

    private void b(g gVar, AppCategory appCategory, int i2) {
        ArrayList<TagGroup> arrayList;
        int i3;
        if (appCategory != null && gVar != null && gVar.e != null && (arrayList = appCategory.f) != null) {
            int i4 = appCategory.g;
            if (i4 < 1) {
                i4 = 3;
            }
            if (i4 > 3) {
                i3 = 3;
            } else {
                i3 = i4;
            }
            int a2 = a(i3, arrayList.size());
            gVar.e.removeAllViews();
            for (int i5 = 0; i5 < a2; i5++) {
                LinearLayout linearLayout = new LinearLayout(this.g);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
                if (i5 > 0) {
                    layoutParams.topMargin = by.a(AstApp.i(), 9.0f);
                }
                linearLayout.setLayoutParams(layoutParams);
                linearLayout.setWeightSum((float) i3);
                linearLayout.setOrientation(0);
                for (int i6 = 0; i6 < i3; i6++) {
                    int i7 = (i5 * i3) + i6;
                    if (i7 < arrayList.size() && i7 < 11 && arrayList.get(i7) != null) {
                        LinearLayout linearLayout2 = new LinearLayout(this.g);
                        linearLayout2.setOrientation(1);
                        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2);
                        layoutParams2.weight = 1.0f;
                        layoutParams2.gravity = 19;
                        linearLayout2.setLayoutParams(layoutParams2);
                        linearLayout.addView(linearLayout2);
                        TextView textView = new TextView(this.g);
                        textView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                        textView.setText(arrayList.get(i7).b);
                        textView.setTextSize(2, 14.0f);
                        textView.setTag(R.id.category_pos, Integer.valueOf((i2 * 20) + i7 + 1));
                        String str = null;
                        if (arrayList.get(i7).c != null) {
                            str = arrayList.get(i7).c.f1125a;
                        }
                        f fVar = new f(appCategory.f1140a, arrayList.get(i7).f1577a, appCategory.b, arrayList.get(i7).d, str, arrayList.get(i7).b);
                        textView.setTag(R.id.category_data, fVar);
                        textView.setSingleLine();
                        textView.setEllipsize(TextUtils.TruncateAt.END);
                        textView.setGravity(16);
                        textView.setPadding(by.a(AstApp.i(), 3.0f), by.a(AstApp.i(), 3.0f), by.a(AstApp.i(), 3.0f), by.a(AstApp.i(), 2.0f));
                        textView.setBackgroundResource(R.drawable.bg_app_category_selector);
                        textView.setOnClickListener(this.A);
                        if (arrayList.get(i7).d == 1) {
                            textView.setTextColor(this.v[i2 % this.v.length]);
                        } else if (arrayList.get(i7).d == 2) {
                            textView.setTextColor(this.g.getResources().getColorStateList(R.color.app_category_operation_txt));
                            textView.setCompoundDrawables(null, null, this.w, null);
                        } else {
                            textView.setTextColor(this.g.getResources().getColorStateList(R.color.app_category_child_txt));
                        }
                        linearLayout2.addView(textView);
                        a(fVar, i2, i7 + 1, 100);
                    }
                }
                if (linearLayout.getChildCount() >= 0) {
                    gVar.e.addView(linearLayout);
                }
            }
            gVar.e.measure(0, 0);
            gVar.e.setLayoutParams(new LinearLayout.LayoutParams(-1, gVar.e.getMeasuredHeight()));
        }
    }

    public int getItemViewType(int i2) {
        if (a() && i2 == 0) {
            return 0;
        }
        if (b()) {
            if (a()) {
                if (i2 != 1) {
                    return 2;
                }
                return 1;
            } else if (i2 == 0) {
                return 1;
            }
        }
        return 2;
    }

    private boolean a() {
        return this.b != null && !this.b.isEmpty();
    }

    private boolean b() {
        return this.c != null && this.c.size() >= 2;
    }

    private void a(List<ColorCardItem> list) {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        this.b.clear();
        if (list != null && list.size() >= 2) {
            if (list.size() <= 6) {
                int size = list.size();
                if (size % 2 != 0) {
                    size--;
                }
                this.b.addAll(list.subList(0, size));
                return;
            }
            this.b.addAll(list.subList(0, 6));
        }
    }

    private void b(List<AppCategory> list) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.clear();
        if (list != null && list.size() >= 2) {
            if (list.size() <= 4) {
                this.c.addAll(list);
            } else {
                this.c.addAll(list.subList(0, 4));
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(f fVar, int i2, int i3, int i4) {
        if (this.g instanceof BaseActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.g, i4);
            buildSTInfo.slotId = a.a("05", (i2 * 20) + i3);
            if (fVar != null) {
                buildSTInfo.recommendId = (String.valueOf(fVar.f590a) + "," + String.valueOf(fVar.b)).getBytes();
                buildSTInfo.extraData = fVar.f;
                buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, fVar.f590a + "_" + fVar.b);
            }
            if (i4 == 100) {
                if (this.x == null) {
                    this.x = new b();
                }
                this.x.exposure(buildSTInfo);
                return;
            }
            l.a(buildSTInfo);
        }
    }
}
