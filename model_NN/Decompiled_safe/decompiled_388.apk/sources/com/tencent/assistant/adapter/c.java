package com.tencent.assistant.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f587a;

    c(AppCategoryListAdapter appCategoryListAdapter) {
        this.f587a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        ColorCardItem colorCardItem = (ColorCardItem) view.getTag(R.id.category_data);
        if (colorCardItem != null) {
            Bundle bundle = new Bundle();
            if (this.f587a.g instanceof BaseActivity) {
                bundle.putSerializable(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, Integer.valueOf(((BaseActivity) this.f587a.g).f()));
            }
            bundle.putSerializable("com.tencent.assistant.ACTION_URL", colorCardItem.a());
            if (colorCardItem.a() != null) {
                XLog.d("AppCategoryListAdapter", "colorCardListener:(url)" + colorCardItem.a().f1125a);
                b.b(this.f587a.g, colorCardItem.a().f1125a, bundle);
            }
        }
    }
}
