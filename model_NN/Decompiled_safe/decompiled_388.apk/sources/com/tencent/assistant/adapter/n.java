package com.tencent.assistant.adapter;

import android.widget.ImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class n extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f598a;
    final /* synthetic */ OneMoreAdapter b;

    n(OneMoreAdapter oneMoreAdapter, SimpleAppModel simpleAppModel) {
        this.b = oneMoreAdapter;
        this.f598a = simpleAppModel;
    }

    public void a(DownloadInfo downloadInfo) {
        ImageView imageView = (ImageView) this.b.e.findViewWithTag(downloadInfo.downloadTicket);
        if (downloadInfo.needReCreateInfo(this.f598a)) {
            DownloadProxy.a().b(downloadInfo.downloadTicket);
            downloadInfo = DownloadInfo.createDownloadInfo(this.f598a, null);
        }
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a(imageView);
    }
}
