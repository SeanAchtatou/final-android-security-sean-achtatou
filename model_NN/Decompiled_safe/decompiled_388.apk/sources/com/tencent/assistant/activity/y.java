package com.tencent.assistant.activity;

import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.appbackup.BackupAppListAdapter;

/* compiled from: ProGuard */
class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f567a;

    y(x xVar) {
        this.f567a = xVar;
    }

    public void run() {
        BackupAppListAdapter a2;
        if (this.f567a.f566a.F != null && this.f567a.f566a.F.isShowing() && (a2 = this.f567a.f566a.F.a()) != null) {
            XLog.d("AppBackupActivity", "call --notifyDataSetChanged");
            a2.notifyDataSetChanged();
        }
    }
}
