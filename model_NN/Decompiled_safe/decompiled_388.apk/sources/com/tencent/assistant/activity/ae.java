package com.tencent.assistant.activity;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ae implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ad f390a;

    ae(ad adVar) {
        this.f390a = adVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f390a.f389a.K();
        this.f390a.f389a.n.setEnabled(true);
        this.f390a.f389a.n.setText(this.f390a.f389a.getResources().getString(R.string.app_backup_list));
    }
}
