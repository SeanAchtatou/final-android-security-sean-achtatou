package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class w extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBackupActivity f565a;

    w(AppBackupActivity appBackupActivity) {
        this.f565a = appBackupActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 11901:
                this.f565a.a(this.f565a.aa, 0.0f, 90.0f, false, this.f565a.am);
                return;
            case 11902:
                this.f565a.S.setVisibility(0);
                this.f565a.T.setVisibility(0);
                this.f565a.S.startAnimation(this.f565a.W);
                this.f565a.T.startAnimation(this.f565a.X);
                return;
            default:
                return;
        }
    }
}
