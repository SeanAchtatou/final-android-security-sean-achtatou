package com.tencent.assistant.activity;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f438a;

    c(ApkMgrActivity apkMgrActivity) {
        this.f438a = apkMgrActivity;
    }

    public void run() {
        List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
        if (localApkInfos != null && localApkInfos.size() > 0) {
            long d = bo.d();
            Iterator<LocalApkInfo> it = localApkInfos.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                LocalApkInfo next = it.next();
                if (next.mLastLaunchTime != 0 && bo.a(next.mLastLaunchTime, d) >= 30) {
                    boolean unused = this.f438a.M = true;
                    break;
                }
            }
        }
        this.f438a.S.sendMessage(this.f438a.S.obtainMessage(110010));
        XLog.i("ApkMgrActivity", "isSkipToInstalledAppManager in ApkMgrActivity");
    }
}
