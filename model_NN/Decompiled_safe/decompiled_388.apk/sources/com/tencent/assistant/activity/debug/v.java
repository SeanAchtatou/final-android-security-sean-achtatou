package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f521a;

    v(DActivity dActivity) {
        this.f521a = dActivity;
    }

    public void onClick(View view) {
        TemporaryThreadManager.get().start(new w(this));
    }
}
