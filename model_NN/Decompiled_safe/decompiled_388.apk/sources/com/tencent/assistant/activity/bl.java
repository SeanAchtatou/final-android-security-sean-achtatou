package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class bl extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstalledAppManagerActivity f423a;

    bl(InstalledAppManagerActivity installedAppManagerActivity) {
        this.f423a = installedAppManagerActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 10701:
                List list = (List) message.obj;
                if (list != null) {
                    this.f423a.a(list);
                }
                XLog.d("miles", "MSG_LOAG_APK_SUCCESS received");
                this.f423a.A();
                return;
            case 10702:
                int i = message.arg1;
                this.f423a.a((LocalApkInfo) message.obj, i);
                this.f423a.A();
                return;
            case 10703:
                this.f423a.A();
                return;
            case 10704:
            default:
                return;
            case 10705:
                if (message.obj != null && (message.obj instanceof LocalApkInfo)) {
                    this.f423a.a((LocalApkInfo) message.obj);
                    return;
                }
                return;
            case 10706:
                if (this.f423a.H != null) {
                    this.f423a.H.f();
                }
                if (this.f423a.G != null) {
                    this.f423a.G.f();
                    return;
                }
                return;
        }
    }
}
