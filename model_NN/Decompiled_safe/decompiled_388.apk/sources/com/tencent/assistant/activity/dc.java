package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.ar;

/* compiled from: ProGuard */
class dc extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f468a;

    dc(StartScanActivity startScanActivity) {
        this.f468a = startScanActivity;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                LocalApkInfo localApkInfo = (LocalApkInfo) message.obj;
                if (this.f468a.Q != null && this.f468a.a(localApkInfo).booleanValue() && this.f468a.Q.n(localApkInfo.mPackageName).booleanValue()) {
                    this.f468a.Q.o(localApkInfo.mPackageName);
                    if (!this.f468a.Q.p(localApkInfo.mPackageName).booleanValue()) {
                        this.f468a.Q.q(localApkInfo.mPackageName);
                        Log.i("StartScanActivity", "install offical PackageName= " + localApkInfo.mPackageName + "from PC");
                    }
                    this.f468a.Q.h(localApkInfo.mPackageName);
                    this.f468a.b(this.f468a.Q.b());
                    this.f468a.Q.f(localApkInfo.mPackageName);
                    Log.i("StartScanActivity", "install offical PackageName= " + localApkInfo.mPackageName);
                }
                if (localApkInfo != null) {
                    ar.b(localApkInfo.mPackageName);
                    ar.a(0, StartScanActivity.class);
                    return;
                }
                return;
            case 2:
                LocalApkInfo localApkInfo2 = (LocalApkInfo) message.obj;
                if (this.f468a.Q != null && this.f468a.Q.a() > 0) {
                    if (!this.f468a.a(localApkInfo2).booleanValue()) {
                        this.f468a.Q.g(localApkInfo2.mPackageName);
                        Log.i("StartScanActivity", "uninstall no-offical PackageName= " + localApkInfo2.mPackageName);
                    } else if (this.f468a.Q.l(localApkInfo2.mPackageName).booleanValue()) {
                        this.f468a.Q.m(localApkInfo2.mPackageName);
                        Message message2 = new Message();
                        message2.obj = localApkInfo2;
                        message2.what = 11;
                        this.f468a.Q.f2983a.sendMessage(message2);
                        this.f468a.Q.e(localApkInfo2.mPackageName);
                        Log.i("StartScanActivity", "uninstall offical PackageName= " + localApkInfo2.mPackageName);
                    } else {
                        if (!this.f468a.Q.n(localApkInfo2.mPackageName).booleanValue()) {
                            this.f468a.Q.g(localApkInfo2.mPackageName);
                        }
                        Log.i("StartScanActivity", "no userAction,uninstall offical PackageName= " + localApkInfo2.mPackageName);
                    }
                }
                if (localApkInfo2 != null) {
                    ar.b(localApkInfo2.mPackageName);
                    ar.b(0, StartScanActivity.class);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
