package com.tencent.assistant.activity;

import android.view.View;
import android.widget.ExpandableListView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h implements ExpandableListView.OnChildClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f536a;

    h(ApkMgrActivity apkMgrActivity) {
        this.f536a = apkMgrActivity;
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        LocalApkInfo localApkInfo = (LocalApkInfo) expandableListView.getExpandableListAdapter().getChild(i, i2);
        if (localApkInfo != null) {
            String str = STConst.ST_DEFAULT_SLOT;
            if (this.f536a.x != null) {
                str = this.f536a.x.a(i, i2);
            }
            STInfoV2 sTInfoV2 = new STInfoV2(this.f536a.f(), str, this.f536a.m(), STConst.ST_DEFAULT_SLOT, 200);
            if (localApkInfo != null) {
                sTInfoV2.appId = localApkInfo.mAppid;
            }
            l.a(sTInfoV2);
        }
        return false;
    }
}
