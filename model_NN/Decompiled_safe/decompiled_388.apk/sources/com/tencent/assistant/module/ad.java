package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.protocol.jce.AppDetailParam;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.GetAppSimpleDetailRequest;
import com.tencent.assistant.protocol.jce.GetAppSimpleDetailResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ad extends BaseEngine<h> {
    public int a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return -1;
        }
        GetAppSimpleDetailRequest getAppSimpleDetailRequest = new GetAppSimpleDetailRequest();
        AppDetailParam appDetailParam = new AppDetailParam();
        appDetailParam.f1148a = simpleAppModel.f938a;
        appDetailParam.b = simpleAppModel.c;
        if (!TextUtils.isEmpty(simpleAppModel.m)) {
            appDetailParam.c = simpleAppModel.m;
        }
        appDetailParam.h = simpleAppModel.g;
        appDetailParam.g = simpleAppModel.b;
        appDetailParam.i = simpleAppModel.ac;
        appDetailParam.k = simpleAppModel.ad;
        appDetailParam.j = simpleAppModel.Q;
        appDetailParam.d = simpleAppModel.D;
        if (!TextUtils.isEmpty(simpleAppModel.ak)) {
            appDetailParam.e = simpleAppModel.ak;
        }
        ArrayList<AppDetailParam> arrayList = new ArrayList<>();
        arrayList.add(appDetailParam);
        getAppSimpleDetailRequest.f1248a = arrayList;
        return send(getAppSimpleDetailRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ae(this, i, a((GetAppSimpleDetailResponse) jceStruct2)));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new af(this, i, i2));
    }

    private AppSimpleDetail a(GetAppSimpleDetailResponse getAppSimpleDetailResponse) {
        if (getAppSimpleDetailResponse == null || getAppSimpleDetailResponse.b == null || getAppSimpleDetailResponse.b.size() <= 0) {
            return null;
        }
        return getAppSimpleDetailResponse.b.get(0);
    }
}
