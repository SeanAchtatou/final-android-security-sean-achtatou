package com.tencent.assistant.module;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AutoDownloadNewAppUserProfile;
import com.tencent.assistant.protocol.jce.AutoDownloadUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.protocol.jce.UserProfile;
import com.tencent.assistant.utils.an;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bc implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.WISE_DOWNLOAD_SWITCH_TYPE f978a;
    final /* synthetic */ boolean b;
    final /* synthetic */ bb c;

    bc(bb bbVar, AppConst.WISE_DOWNLOAD_SWITCH_TYPE wise_download_switch_type, boolean z) {
        this.c = bbVar;
        this.f978a = wise_download_switch_type;
        this.b = z;
    }

    public void run() {
        UserProfile userProfile;
        int i = 0;
        int i2 = 1;
        if (this.f978a == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE) {
            if (this.b) {
                i = 1;
            }
            userProfile = new UserProfile((byte) 1, an.a(new AutoDownloadUserProfile(i)), 0);
        } else if (this.f978a == AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD) {
            if (!this.b) {
                i2 = 0;
            }
            userProfile = new UserProfile((byte) 3, an.a(new AutoDownloadNewAppUserProfile(i2)), 0);
            i2 = 3;
        } else {
            return;
        }
        ArrayList<UserProfile> arrayList = new ArrayList<>();
        arrayList.add(userProfile);
        SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest();
        setUserProfileRequest.f1490a = arrayList;
        this.c.b.put(Integer.valueOf(this.c.send(setUserProfileRequest)), Integer.valueOf(i2));
    }
}
