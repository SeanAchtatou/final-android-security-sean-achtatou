package com.tencent.assistant.module.a;

import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class l extends a {
    public boolean d() {
        try {
            int a2 = m.a().a("bao_current_version_code", 0);
            int appVersionCode = Global.getAppVersionCode();
            if (appVersionCode != a2) {
                AstApp.a(true);
                TemporaryThreadManager.get().start(new m(this, a2, appVersionCode));
            } else {
                AstApp.a(false);
                i.a(true);
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        i.b().a(AstApp.i());
    }
}
