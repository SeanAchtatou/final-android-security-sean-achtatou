package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public abstract class BaseEngine<T extends ActionCallback> extends p {
    protected CallbackHelper<T> mCallbacks = new CallbackHelper<>();

    public void register(T t) {
        this.mCallbacks.register(t);
    }

    public void unregister(T t) {
        this.mCallbacks.unregister(t);
    }

    public void unregisterAll() {
        this.mCallbacks.unregisterAll();
    }

    /* access modifiers changed from: protected */
    public void notifyDataChangedInMainThread(CallbackHelper.Caller<T> caller) {
        runOnUiThread(new n(this, caller));
    }

    /* access modifiers changed from: protected */
    public void delayNotifyDataChangedInMainThread(CallbackHelper.Caller<T> caller, long j) {
        ah.a().postDelayed(new o(this, caller), j);
    }

    /* access modifiers changed from: protected */
    public void notifyDataChanged(CallbackHelper.Caller<T> caller) {
        this.mCallbacks.broadcast(caller);
    }

    /* access modifiers changed from: protected */
    public void runOnUiThread(Runnable runnable) {
        ah.a().post(runnable);
    }
}
