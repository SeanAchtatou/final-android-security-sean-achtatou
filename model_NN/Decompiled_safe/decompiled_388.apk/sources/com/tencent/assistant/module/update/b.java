package com.tencent.assistant.module.update;

import android.os.Handler;
import com.qq.AppService.AstApp;
import com.tencent.assistant.b.c;
import com.tencent.assistant.b.i;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.m;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bo;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class b {
    private static b e = null;
    /* access modifiers changed from: private */
    public static int g = 2;

    /* renamed from: a  reason: collision with root package name */
    UIEventListener f1028a;
    /* access modifiers changed from: private */
    public AppUpdateConst.RequestLaunchType b;
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public LbsData d;
    /* access modifiers changed from: private */
    public int f;
    private i h;
    private boolean i;
    private ApkResCallback.Stub j;
    /* access modifiers changed from: private */
    public Handler k;

    private b() {
        this.b = AppUpdateConst.RequestLaunchType.TYPE_DEFAULT;
        this.c = null;
        this.d = null;
        this.f1028a = new c(this);
        this.f = g;
        this.h = new f(this);
        this.i = false;
        this.j = new h(this);
        this.k = new i(this, AstApp.i().getMainLooper());
        this.c = new c(AstApp.i(), this.h);
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (e == null) {
                e = new b();
                e.c();
            }
            bVar = e;
        }
        return bVar;
    }

    private void c() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_GOFRONT, this.f1028a);
        AstApp.i().k().addUIEventListener(1032, this.f1028a);
    }

    /* access modifiers changed from: private */
    public boolean d() {
        if (this.f <= 0) {
            return false;
        }
        this.f--;
        TemporaryThreadManager.get().start(new g(this));
        return true;
    }

    private void e() {
        if (this.c != null) {
            this.c.d();
        }
    }

    public synchronized void a(AppUpdateConst.RequestLaunchType requestLaunchType) {
        XLog.d("AppUpdateEngine", "触发更新...... type =" + requestLaunchType);
        if (!this.i) {
            ApkResourceManager.getInstance().registerApkResCallback(this.j, false);
            this.i = true;
        }
        this.b = requestLaunchType;
        if (ApkResourceManager.getInstance().isLocalApkDataReady()) {
            d();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, boolean z, String str, int i3) {
        HashMap hashMap = new HashMap();
        hashMap.put("packagename", str);
        hashMap.put("versioncode", String.valueOf(i3));
        if (i2 == 2) {
            XLog.d("AppUpdateEngine", "卸载 packageName =" + str + " isReplacing=" + z);
            if (!z) {
                a(AppUpdateConst.RequestLaunchType.TYPE_APP_UNINSTALL, this.d, hashMap);
            }
            j.b().a(str, i3);
        } else if (i2 == 3) {
            XLog.d("AppUpdateEngine", "覆盖 packageName =" + str + " isReplacing=" + z);
            a(AppUpdateConst.RequestLaunchType.TYPE_APP_REPLACED, this.d, hashMap);
        } else if (i2 == 1) {
            XLog.d("AppUpdateEngine", "安装 packageName =" + str + " isReplacing=" + z);
            if (!z) {
                a(AppUpdateConst.RequestLaunchType.TYPE_APP_INSTALLED, this.d, hashMap);
            }
            j.b().b(str, i3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: private */
    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, LbsData lbsData, Map<String, String> map) {
        XLog.d("AppUpdateEngine", "dispatchUpdateRequest type =" + requestLaunchType + " type=" + requestLaunchType.ordinal());
        if (requestLaunchType == AppUpdateConst.RequestLaunchType.TYPE_STARTUP) {
            m.a().k(0);
            j.f1036a = System.currentTimeMillis();
            j.b().a(requestLaunchType, bo.b(m.a().a("app_update_refresh_suc_time", 0L)));
            XLog.d("AppUpdateEngine", "应用宝启动时发起更新列表请求，初始化了更新拉取失败当前重试次数为0");
        }
        j.b().a(requestLaunchType, lbsData, map);
        e();
    }

    public void a(AppUpdateConst.RequestLaunchType requestLaunchType, Map<String, String> map) {
        XLog.d("AppUpdateEngine", "sendUpdateRequest type =" + requestLaunchType + " type=" + requestLaunchType.ordinal());
        if (this.d == null && this.c != null) {
            this.d = this.c.c();
        }
        j.b().a(requestLaunchType, this.d, map);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.update.j.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void
     arg types: [com.tencent.assistant.protocol.jce.LbsData, int, int]
     candidates:
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.module.update.j, com.tencent.assistant.protocol.jce.LbsData, int):int
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.protocol.jce.LbsData, int, java.util.ArrayList<com.tencent.assistant.protocol.jce.AppInfoForUpdate>):int
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.module.update.AppUpdateConst$RequestLaunchType, com.tencent.assistant.protocol.jce.LbsData, java.util.Map<java.lang.String, java.lang.String>):void
      com.tencent.assistant.module.update.j.a(com.tencent.assistant.protocol.jce.LbsData, int, boolean):void */
    public void b(AppUpdateConst.RequestLaunchType requestLaunchType, Map<String, String> map) {
        XLog.d("AppUpdateEngine", "sendDirectUpdateRequest type =" + requestLaunchType + " type=" + requestLaunchType.ordinal());
        if (this.d == null && this.c != null) {
            this.d = this.c.c();
        }
        j.b().a(this.d, requestLaunchType.ordinal(), false);
    }
}
