package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.g;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/* compiled from: ProGuard */
class z implements CallbackHelper.Caller<g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1055a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ boolean d;
    final /* synthetic */ y e;

    z(y yVar, int i, boolean z, ArrayList arrayList, boolean z2) {
        this.e = yVar;
        this.f1055a = i;
        this.b = z;
        this.c = arrayList;
        this.d = z2;
    }

    /* renamed from: a */
    public void call(g gVar) {
        g gVar2 = gVar;
        gVar2.a(this.f1055a, 0, this.b, new LinkedHashMap(), this.c, this.d);
    }
}
