package com.tencent.assistant.module.timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.notification.a;
import com.tencent.assistant.manager.notification.z;
import com.tencent.assistant.module.timer.job.AppUpdateTimerJob;
import com.tencent.assistant.module.timer.job.GetAppExInfoScheduleJob;
import com.tencent.assistant.module.timer.job.GetOtherPushUpdateInfoTimerJob;
import com.tencent.assistant.utils.XLog;
import com.tencent.nucleus.manager.backgroundscan.BackgroundScanTimerJob;
import com.tencent.nucleus.manager.uninstallwatch.e;

/* compiled from: ProGuard */
public class ScheduleJobReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static c f1011a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, android.content.Context):void
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, boolean):boolean
      com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void */
    public void onReceive(Context context, Intent intent) {
        XLog.d("ScheduleJobReceiver", "onReceive, action:" + intent.getAction());
        try {
            String action = intent.getAction();
            if (action.equals("com.tencent.android.qqdownloader.action.SCHEDULE_JOB")) {
                String stringExtra = intent.getStringExtra("com.tencent.android.qqdownloader.key.SCHEDULE_JOB");
                XLog.i("ScheduleJobReceiver", "on receive,action:" + action + ",this:" + this + ",clazz:" + stringExtra + ",mQueue=" + f1011a);
                if (!TextUtils.isEmpty(stringExtra)) {
                    if (f1011a == null) {
                        f1011a = new c();
                    }
                    f1011a.a(stringExtra);
                }
            } else if (action.equals("android.intent.action.BOOT_COMPLETED")) {
                e.a().a(context.getApplicationContext(), true);
                a();
            } else if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                a();
            } else if (action.equals("android.intent.action.SCREEN_ON")) {
                a();
            } else if (action.equals("android.intent.action.USER_PRESENT")) {
                a.a().b();
                z.a().b();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a() {
        GetAppExInfoScheduleJob.i().d();
        b();
        GetOtherPushUpdateInfoTimerJob.h().e();
        BackgroundScanTimerJob.h().e();
    }

    public static void b() {
        if (m.a().q()) {
            AppUpdateTimerJob.h().e();
        } else {
            AppUpdateTimerJob.h().f();
        }
    }
}
