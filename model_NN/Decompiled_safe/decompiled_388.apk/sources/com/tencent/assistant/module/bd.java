package com.tencent.assistant.module;

import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.protocol.jce.SetUserProfileRequest;
import com.tencent.assistant.protocol.jce.UserProfile;
import com.tencent.assistant.utils.an;
import java.util.ArrayList;

/* compiled from: ProGuard */
class bd implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f979a;
    final /* synthetic */ boolean b;
    final /* synthetic */ bb c;

    bd(bb bbVar, int i, boolean z) {
        this.c = bbVar;
        this.f979a = i;
        this.b = z;
    }

    public void run() {
        if (this.f979a == 4) {
            UserProfile userProfile = new UserProfile((byte) 4, an.a(new AppSecretUserProfile(this.b ? 1 : 0)), 0);
            ArrayList<UserProfile> arrayList = new ArrayList<>();
            arrayList.clear();
            arrayList.add(userProfile);
            SetUserProfileRequest setUserProfileRequest = new SetUserProfileRequest();
            setUserProfileRequest.f1490a = arrayList;
            this.c.b.put(Integer.valueOf(this.c.send(setUserProfileRequest)), 4);
        }
    }
}
