package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.pangu.module.a.i;

/* compiled from: ProGuard */
class az implements CallbackHelper.Caller<i> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f974a;
    final /* synthetic */ GetRecommendAppListResponse b;
    final /* synthetic */ ay c;

    az(ay ayVar, int i, GetRecommendAppListResponse getRecommendAppListResponse) {
        this.c = ayVar;
        this.f974a = i;
        this.b = getRecommendAppListResponse;
    }

    /* renamed from: a */
    public void call(i iVar) {
        iVar.a(this.f974a, 0, this.b.a(), this.b);
    }
}
