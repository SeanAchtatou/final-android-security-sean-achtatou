package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class TicketType implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final TicketType f1588a = new TicketType(0, 0, "TICKET_TYPE_NULL");
    public static final TicketType b = new TicketType(1, 1, "TICKET_TYPE_QQ_WTLOGIN");
    public static final TicketType c = new TicketType(2, 2, "TICKET_TYPE_WX_OAUTH2");
    public static final TicketType d = new TicketType(3, 3, "TICKET_TYPE_WX_OAUTH2_CODE");
    public static final TicketType e = new TicketType(4, 4, "TICKET_TYPE_YYB_AUTH");
    public static final TicketType f = new TicketType(5, 11, "TICKET_TYPE_QQ_WTLOGIN_INTERNAL");
    public static final TicketType g = new TicketType(6, 12, "TICKET_TYPE_WX_OAUTH2_INTERNAL");
    static final /* synthetic */ boolean h = (!TicketType.class.desiredAssertionStatus());
    private static TicketType[] i = new TicketType[7];
    private int j;
    private String k = new String();

    public String toString() {
        return this.k;
    }

    private TicketType(int i2, int i3, String str) {
        this.k = str;
        this.j = i3;
        i[i2] = this;
    }
}
