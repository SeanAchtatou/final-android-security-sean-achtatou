package com.tencent.assistant.protocol.jce;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: ProGuard */
public final class AppUpdateInfo extends JceStruct implements Cloneable {
    static PicInfo J;
    static RatingInfo K;
    static ArrayList<ApkDownUrl> L;
    static byte[] M;
    static final /* synthetic */ boolean N = (!AppUpdateInfo.class.desiredAssertionStatus());
    public short A = 0;
    public String B = Constants.STR_EMPTY;
    public int C = 0;
    public String D = Constants.STR_EMPTY;
    public String E = Constants.STR_EMPTY;
    public int F = 0;
    public int G = 0;
    public long H = 0;
    public byte I = 0;

    /* renamed from: a  reason: collision with root package name */
    public String f1162a = Constants.STR_EMPTY;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public int d = 0;
    public PicInfo e = null;
    public String f = Constants.STR_EMPTY;
    public RatingInfo g = null;
    public String h = Constants.STR_EMPTY;
    public String i = Constants.STR_EMPTY;
    public long j = 0;
    public String k = Constants.STR_EMPTY;
    public String l = Constants.STR_EMPTY;
    public long m = 0;
    public String n = Constants.STR_EMPTY;
    public long o = 0;
    public long p = 0;
    public int q = 0;
    public long r = 0;
    public ArrayList<ApkDownUrl> s = null;
    public byte t = 0;
    public byte[] u = null;
    public String v = Constants.STR_EMPTY;
    public int w = 0;
    public String x = Constants.STR_EMPTY;
    public long y = 0;
    public String z = Constants.STR_EMPTY;

    public String a() {
        return this.f1162a;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        AppUpdateInfo appUpdateInfo = (AppUpdateInfo) obj;
        if (!JceUtil.equals(this.f1162a, appUpdateInfo.f1162a) || !JceUtil.equals(this.b, appUpdateInfo.b) || !JceUtil.equals(this.c, appUpdateInfo.c) || !JceUtil.equals(this.d, appUpdateInfo.d) || !JceUtil.equals(this.e, appUpdateInfo.e) || !JceUtil.equals(this.f, appUpdateInfo.f) || !JceUtil.equals(this.g, appUpdateInfo.g) || !JceUtil.equals(this.h, appUpdateInfo.h) || !JceUtil.equals(this.i, appUpdateInfo.i) || !JceUtil.equals(this.j, appUpdateInfo.j) || !JceUtil.equals(this.k, appUpdateInfo.k) || !JceUtil.equals(this.l, appUpdateInfo.l) || !JceUtil.equals(this.m, appUpdateInfo.m) || !JceUtil.equals(this.n, appUpdateInfo.n) || !JceUtil.equals(this.o, appUpdateInfo.o) || !JceUtil.equals(this.p, appUpdateInfo.p) || !JceUtil.equals(this.q, appUpdateInfo.q) || !JceUtil.equals(this.r, appUpdateInfo.r) || !JceUtil.equals(this.s, appUpdateInfo.s) || !JceUtil.equals(this.t, appUpdateInfo.t) || !JceUtil.equals(this.u, appUpdateInfo.u) || !JceUtil.equals(this.v, appUpdateInfo.v) || !JceUtil.equals(this.w, appUpdateInfo.w) || !JceUtil.equals(this.x, appUpdateInfo.x) || !JceUtil.equals(this.y, appUpdateInfo.y) || !JceUtil.equals(this.z, appUpdateInfo.z) || !JceUtil.equals(this.A, appUpdateInfo.A) || !JceUtil.equals(this.B, appUpdateInfo.B) || !JceUtil.equals(this.C, appUpdateInfo.C) || !JceUtil.equals(this.D, appUpdateInfo.D) || !JceUtil.equals(this.E, appUpdateInfo.E) || !JceUtil.equals(this.F, appUpdateInfo.F) || !JceUtil.equals(this.G, appUpdateInfo.G) || !JceUtil.equals(this.H, appUpdateInfo.H) || !JceUtil.equals(this.I, appUpdateInfo.I)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (N) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [com.tencent.assistant.protocol.jce.PicInfo, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
     arg types: [com.tencent.assistant.protocol.jce.RatingInfo, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.ApkDownUrl>, int]
     candidates:
      com.qq.taf.jce.JceOutputStream.write(byte, int):void
      com.qq.taf.jce.JceOutputStream.write(double, int):void
      com.qq.taf.jce.JceOutputStream.write(float, int):void
      com.qq.taf.jce.JceOutputStream.write(int, int):void
      com.qq.taf.jce.JceOutputStream.write(long, int):void
      com.qq.taf.jce.JceOutputStream.write(com.qq.taf.jce.JceStruct, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Byte, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Double, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Float, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Integer, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Long, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Short, int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.String, int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Map, int):void
      com.qq.taf.jce.JceOutputStream.write(short, int):void
      com.qq.taf.jce.JceOutputStream.write(boolean, int):void
      com.qq.taf.jce.JceOutputStream.write(byte[], int):void
      com.qq.taf.jce.JceOutputStream.write(double[], int):void
      com.qq.taf.jce.JceOutputStream.write(float[], int):void
      com.qq.taf.jce.JceOutputStream.write(int[], int):void
      com.qq.taf.jce.JceOutputStream.write(long[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.lang.Object[], int):void
      com.qq.taf.jce.JceOutputStream.write(short[], int):void
      com.qq.taf.jce.JceOutputStream.write(boolean[], int):void
      com.qq.taf.jce.JceOutputStream.write(java.util.Collection, int):void */
    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f1162a, 0);
        jceOutputStream.write(this.b, 1);
        jceOutputStream.write(this.c, 2);
        jceOutputStream.write(this.d, 3);
        jceOutputStream.write((JceStruct) this.e, 4);
        jceOutputStream.write(this.f, 5);
        jceOutputStream.write((JceStruct) this.g, 6);
        jceOutputStream.write(this.h, 7);
        jceOutputStream.write(this.i, 8);
        jceOutputStream.write(this.j, 9);
        if (this.k != null) {
            jceOutputStream.write(this.k, 10);
        }
        if (this.l != null) {
            jceOutputStream.write(this.l, 11);
        }
        jceOutputStream.write(this.m, 12);
        if (this.n != null) {
            jceOutputStream.write(this.n, 13);
        }
        jceOutputStream.write(this.o, 14);
        jceOutputStream.write(this.p, 15);
        jceOutputStream.write(this.q, 16);
        jceOutputStream.write(this.r, 18);
        if (this.s != null) {
            jceOutputStream.write((Collection) this.s, 19);
        }
        jceOutputStream.write(this.t, 21);
        if (this.u != null) {
            jceOutputStream.write(this.u, 22);
        }
        if (this.v != null) {
            jceOutputStream.write(this.v, 23);
        }
        jceOutputStream.write(this.w, 24);
        if (this.x != null) {
            jceOutputStream.write(this.x, 25);
        }
        jceOutputStream.write(this.y, 26);
        if (this.z != null) {
            jceOutputStream.write(this.z, 27);
        }
        jceOutputStream.write(this.A, 28);
        if (this.B != null) {
            jceOutputStream.write(this.B, 29);
        }
        jceOutputStream.write(this.C, 30);
        if (this.D != null) {
            jceOutputStream.write(this.D, 31);
        }
        if (this.E != null) {
            jceOutputStream.write(this.E, 32);
        }
        jceOutputStream.write(this.F, 33);
        jceOutputStream.write(this.G, 34);
        jceOutputStream.write(this.H, 35);
        jceOutputStream.write(this.I, 36);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [com.tencent.assistant.protocol.jce.PicInfo, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
     arg types: [com.tencent.assistant.protocol.jce.RatingInfo, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.ApkDownUrl>, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    public void readFrom(JceInputStream jceInputStream) {
        this.f1162a = jceInputStream.readString(0, true);
        this.b = jceInputStream.readString(1, true);
        this.c = jceInputStream.readString(2, true);
        this.d = jceInputStream.read(this.d, 3, true);
        if (J == null) {
            J = new PicInfo();
        }
        this.e = (PicInfo) jceInputStream.read((JceStruct) J, 4, true);
        this.f = jceInputStream.readString(5, true);
        if (K == null) {
            K = new RatingInfo();
        }
        this.g = (RatingInfo) jceInputStream.read((JceStruct) K, 6, true);
        this.h = jceInputStream.readString(7, true);
        this.i = jceInputStream.readString(8, true);
        this.j = jceInputStream.read(this.j, 9, true);
        this.k = jceInputStream.readString(10, false);
        this.l = jceInputStream.readString(11, false);
        this.m = jceInputStream.read(this.m, 12, false);
        this.n = jceInputStream.readString(13, false);
        this.o = jceInputStream.read(this.o, 14, false);
        this.p = jceInputStream.read(this.p, 15, false);
        this.q = jceInputStream.read(this.q, 16, false);
        this.r = jceInputStream.read(this.r, 18, false);
        if (L == null) {
            L = new ArrayList<>();
            L.add(new ApkDownUrl());
        }
        this.s = (ArrayList) jceInputStream.read((Object) L, 19, false);
        this.t = jceInputStream.read(this.t, 21, false);
        if (M == null) {
            M = new byte[1];
            M[0] = 0;
        }
        this.u = jceInputStream.read(M, 22, false);
        this.v = jceInputStream.readString(23, false);
        this.w = jceInputStream.read(this.w, 24, false);
        this.x = jceInputStream.readString(25, false);
        this.y = jceInputStream.read(this.y, 26, false);
        this.z = jceInputStream.readString(27, false);
        this.A = jceInputStream.read(this.A, 28, false);
        this.B = jceInputStream.readString(29, false);
        this.C = jceInputStream.read(this.C, 30, false);
        this.D = jceInputStream.readString(31, false);
        this.E = jceInputStream.readString(32, false);
        this.F = jceInputStream.read(this.F, 33, false);
        this.G = jceInputStream.read(this.G, 34, false);
        this.H = jceInputStream.read(this.H, 35, false);
        this.I = jceInputStream.read(this.I, 36, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.PicInfo, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.RatingInfo, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.ApkDownUrl>, java.lang.String]
     candidates:
      com.qq.taf.jce.JceDisplayer.display(byte, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(com.qq.taf.jce.JceStruct, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.String, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Map, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(boolean, java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(byte[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(char[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(double[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(float[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(int[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(long[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.lang.Object[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(short[], java.lang.String):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.display(java.util.Collection, java.lang.String):com.qq.taf.jce.JceDisplayer */
    public void display(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.display(this.f1162a, "packageName");
        jceDisplayer.display(this.b, "appName");
        jceDisplayer.display(this.c, "signatureMd5");
        jceDisplayer.display(this.d, "versionCode");
        jceDisplayer.display((JceStruct) this.e, "iconUrl");
        jceDisplayer.display(this.f, "newFeature");
        jceDisplayer.display((JceStruct) this.g, "rating");
        jceDisplayer.display(this.h, "apkMd5");
        jceDisplayer.display(this.i, "apkUrl");
        jceDisplayer.display(this.j, "fileSize");
        jceDisplayer.display(this.k, "diffApkMd5");
        jceDisplayer.display(this.l, "diffApkUrl");
        jceDisplayer.display(this.m, "diffFileSize");
        jceDisplayer.display(this.n, "versionName");
        jceDisplayer.display(this.o, "appId");
        jceDisplayer.display(this.p, "downCount");
        jceDisplayer.display(this.q, "flag");
        jceDisplayer.display(this.r, "apkId");
        jceDisplayer.display((Collection) this.s, "apkDownUrl");
        jceDisplayer.display(this.t, "isAdded");
        jceDisplayer.display(this.u, "recommendedId");
        jceDisplayer.display(this.v, "suggestReason");
        jceDisplayer.display(this.w, "grayVersionCode");
        jceDisplayer.display(this.x, "hasUpdateInfo");
        jceDisplayer.display(this.y, "categoryId");
        jceDisplayer.display(this.z, "signatureMd5Molo");
        jceDisplayer.display(this.A, "patchAlgorithm");
        jceDisplayer.display(this.B, "localVersionName");
        jceDisplayer.display(this.C, "localVersionCode");
        jceDisplayer.display(this.D, "localFileListMd5");
        jceDisplayer.display(this.E, "localManifestMd5");
        jceDisplayer.display(this.F, "localGrayVersionCode");
        jceDisplayer.display(this.G, "publishTime");
        jceDisplayer.display(this.H, "downloadCount");
        jceDisplayer.display(this.I, "overwriteChannelid");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.lang.String, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [int, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.PicInfo, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [com.tencent.assistant.protocol.jce.RatingInfo, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [long, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [java.util.ArrayList<com.tencent.assistant.protocol.jce.ApkDownUrl>, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
     arg types: [byte[], int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer
     arg types: [short, int]
     candidates:
      com.qq.taf.jce.JceDisplayer.displaySimple(byte, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(com.qq.taf.jce.JceStruct, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.String, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Collection, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.util.Map, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(boolean, boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(byte[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(char[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(double[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(float[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(int[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(long[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(java.lang.Object[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short[], boolean):com.qq.taf.jce.JceDisplayer
      com.qq.taf.jce.JceDisplayer.displaySimple(short, boolean):com.qq.taf.jce.JceDisplayer */
    public void displaySimple(StringBuilder sb, int i2) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i2);
        jceDisplayer.displaySimple(this.f1162a, true);
        jceDisplayer.displaySimple(this.b, true);
        jceDisplayer.displaySimple(this.c, true);
        jceDisplayer.displaySimple(this.d, true);
        jceDisplayer.displaySimple((JceStruct) this.e, true);
        jceDisplayer.displaySimple(this.f, true);
        jceDisplayer.displaySimple((JceStruct) this.g, true);
        jceDisplayer.displaySimple(this.h, true);
        jceDisplayer.displaySimple(this.i, true);
        jceDisplayer.displaySimple(this.j, true);
        jceDisplayer.displaySimple(this.k, true);
        jceDisplayer.displaySimple(this.l, true);
        jceDisplayer.displaySimple(this.m, true);
        jceDisplayer.displaySimple(this.n, true);
        jceDisplayer.displaySimple(this.o, true);
        jceDisplayer.displaySimple(this.p, true);
        jceDisplayer.displaySimple(this.q, true);
        jceDisplayer.displaySimple(this.r, true);
        jceDisplayer.displaySimple((Collection) this.s, true);
        jceDisplayer.displaySimple(this.t, true);
        jceDisplayer.displaySimple(this.u, true);
        jceDisplayer.displaySimple(this.v, true);
        jceDisplayer.displaySimple(this.w, true);
        jceDisplayer.displaySimple(this.x, true);
        jceDisplayer.displaySimple(this.y, true);
        jceDisplayer.displaySimple(this.z, true);
        jceDisplayer.displaySimple(this.A, true);
        jceDisplayer.displaySimple(this.B, true);
        jceDisplayer.displaySimple(this.C, true);
        jceDisplayer.displaySimple(this.D, true);
        jceDisplayer.displaySimple(this.E, true);
        jceDisplayer.displaySimple(this.F, true);
        jceDisplayer.displaySimple(this.G, true);
        jceDisplayer.displaySimple(this.H, true);
        jceDisplayer.displaySimple(this.I, false);
    }
}
