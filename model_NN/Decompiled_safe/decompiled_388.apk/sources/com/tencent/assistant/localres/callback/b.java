package com.tencent.assistant.localres.callback;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.callback.ActionCallback;
import java.util.List;

/* compiled from: ProGuard */
public interface b extends ActionCallback {
    void a(int i, int i2, long j);

    void a(List<LocalApkInfo> list);

    void a(List<LocalApkInfo> list, int i, long j);

    void a(List<LocalApkInfo> list, LocalApkInfo localApkInfo, boolean z, boolean z2);

    void a(List<LocalApkInfo> list, boolean z, boolean z2, int i);

    void b(List<LocalApkInfo> list, int i, long j);
}
