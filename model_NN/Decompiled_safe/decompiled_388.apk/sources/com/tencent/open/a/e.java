package com.tencent.open.a;

import java.io.File;
import java.util.Comparator;

/* compiled from: ProGuard */
class e implements Comparator<File> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3241a;

    e(b bVar) {
        this.f3241a = bVar;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        return b.f(file) - b.f(file2);
    }
}
