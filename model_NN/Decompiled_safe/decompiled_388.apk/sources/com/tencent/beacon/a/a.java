package com.tencent.beacon.a;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Process;
import com.tencent.connect.common.Constants;
import java.util.Date;
import java.util.List;

/* compiled from: ProGuard */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    public static int f2082a = 0;
    private static String b = null;
    private static int c = 0;

    public static synchronized String a(Context context) {
        String str;
        synchronized (a.class) {
            if (context == null) {
                str = Constants.STR_EMPTY;
            } else {
                try {
                    Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("APPKEY_DENGTA");
                    if (obj != null) {
                        str = obj.toString().trim();
                    }
                } catch (Throwable th) {
                    com.tencent.beacon.d.a.d("no appkey !! ", new Object[0]);
                }
                str = Constants.STR_EMPTY;
            }
        }
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ff, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0100, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x010f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x011b, code lost:
        r8 = r1;
        r1 = r0;
        r0 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bc A[Catch:{ Throwable -> 0x0104 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fb A[SYNTHETIC, Splitter:B:60:0x00fb] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x010f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:17:0x005e] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x00a4=Splitter:B:36:0x00a4, B:62:0x00fe=Splitter:B:62:0x00fe} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.lang.String b(android.content.Context r9) {
        /*
            java.lang.Class<com.tencent.beacon.a.a> r3 = com.tencent.beacon.a.a.class
            monitor-enter(r3)
            if (r9 != 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0007:
            monitor-exit(r3)
            return r0
        L_0x0009:
            java.lang.String r2 = ""
            r0 = 0
            android.content.res.AssetManager r4 = r9.getAssets()     // Catch:{ all -> 0x0098 }
            java.lang.String r1 = "key_channelpath"
            java.lang.String r5 = ""
            java.lang.String r6 = "DENGTA_META"
            r7 = 0
            android.content.SharedPreferences r6 = r9.getSharedPreferences(r6, r7)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.String r1 = r6.getString(r1, r5)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.String r5 = ""
            boolean r5 = r1.equals(r5)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            if (r5 == 0) goto L_0x003d
            java.lang.String r1 = "channel.ini"
            java.lang.String r5 = "key_channelpath"
            java.lang.String r6 = "DENGTA_META"
            r7 = 0
            android.content.SharedPreferences r6 = r9.getSharedPreferences(r6, r7)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            android.content.SharedPreferences$Editor r6 = r6.edit()     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            android.content.SharedPreferences$Editor r5 = r6.putString(r5, r1)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            r5.commit()     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
        L_0x003d:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.String r6 = "channel path!! "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            com.tencent.beacon.d.a.a(r5, r6)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.lang.String r5 = ""
            boolean r5 = r1.equals(r5)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            if (r5 != 0) goto L_0x0121
            java.io.InputStream r1 = r4.open(r1)     // Catch:{ Exception -> 0x00c8, all -> 0x00f5 }
            java.util.Properties r0 = new java.util.Properties     // Catch:{ Exception -> 0x0116, all -> 0x010f }
            r0.<init>()     // Catch:{ Exception -> 0x0116, all -> 0x010f }
            r0.load(r1)     // Catch:{ Exception -> 0x0116, all -> 0x010f }
            java.lang.String r4 = "CHANNEL"
            java.lang.String r5 = ""
            java.lang.String r0 = r0.getProperty(r4, r5)     // Catch:{ Exception -> 0x0116, all -> 0x010f }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            java.lang.String r4 = "channel !! "
            r2.<init>(r4)     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            com.tencent.beacon.d.a.a(r2, r4)     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            java.lang.String r2 = ""
            boolean r2 = r2.equals(r0)     // Catch:{ Exception -> 0x011a, all -> 0x010f }
            if (r2 != 0) goto L_0x009b
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0007
        L_0x0092:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x0007
        L_0x0098:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x009b:
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x009e:
            if (r0 == 0) goto L_0x011f
            r0.close()     // Catch:{ IOException -> 0x00c2 }
            r0 = r1
        L_0x00a4:
            android.content.pm.PackageManager r1 = r9.getPackageManager()     // Catch:{ Throwable -> 0x0104 }
            java.lang.String r2 = r9.getPackageName()     // Catch:{ Throwable -> 0x0104 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r1 = r1.getApplicationInfo(r2, r4)     // Catch:{ Throwable -> 0x0104 }
            android.os.Bundle r1 = r1.metaData     // Catch:{ Throwable -> 0x0104 }
            java.lang.String r2 = "CHANNEL_DENGTA"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Throwable -> 0x0104 }
            if (r1 == 0) goto L_0x0007
            java.lang.String r0 = r1.toString()     // Catch:{ Throwable -> 0x0104 }
            goto L_0x0007
        L_0x00c2:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            r0 = r1
            goto L_0x00a4
        L_0x00c8:
            r1 = move-exception
            r1 = r2
        L_0x00ca:
            java.lang.String r2 = "key_channelpath"
            java.lang.String r4 = ""
            java.lang.String r5 = "DENGTA_META"
            r6 = 0
            android.content.SharedPreferences r5 = r9.getSharedPreferences(r5, r6)     // Catch:{ all -> 0x0111 }
            android.content.SharedPreferences$Editor r5 = r5.edit()     // Catch:{ all -> 0x0111 }
            android.content.SharedPreferences$Editor r2 = r5.putString(r2, r4)     // Catch:{ all -> 0x0111 }
            r2.commit()     // Catch:{ all -> 0x0111 }
            java.lang.String r2 = "get app channel fail!"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0111 }
            com.tencent.beacon.d.a.c(r2, r4)     // Catch:{ all -> 0x0111 }
            if (r0 == 0) goto L_0x011f
            r0.close()     // Catch:{ IOException -> 0x00ef }
            r0 = r1
            goto L_0x00a4
        L_0x00ef:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            r0 = r1
            goto L_0x00a4
        L_0x00f5:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00f9:
            if (r1 == 0) goto L_0x00fe
            r1.close()     // Catch:{ IOException -> 0x00ff }
        L_0x00fe:
            throw r0     // Catch:{ all -> 0x0098 }
        L_0x00ff:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0098 }
            goto L_0x00fe
        L_0x0104:
            r1 = move-exception
            java.lang.String r1 = "no channel !!"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0098 }
            com.tencent.beacon.d.a.a(r1, r2)     // Catch:{ all -> 0x0098 }
            goto L_0x0007
        L_0x010f:
            r0 = move-exception
            goto L_0x00f9
        L_0x0111:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00f9
        L_0x0116:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x00ca
        L_0x011a:
            r2 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ca
        L_0x011f:
            r0 = r1
            goto L_0x00a4
        L_0x0121:
            r1 = r2
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.a.b(android.content.Context):java.lang.String");
    }

    public static synchronized boolean c(Context context) {
        boolean z = false;
        synchronized (a.class) {
            if (context == null) {
                com.tencent.beacon.d.a.d("context == null return null", new Object[0]);
            } else {
                try {
                    String string = context.getSharedPreferences("DENGTA_META", 4).getString("APPKEY_DENGTA", null);
                    String a2 = a(context);
                    if (string == null || !string.equals(a2)) {
                        z = true;
                        SharedPreferences.Editor edit = context.getSharedPreferences("DENGTA_META", 0).edit();
                        edit.putString("APPKEY_DENGTA", a2);
                        edit.commit();
                    }
                } catch (Exception e) {
                    com.tencent.beacon.d.a.b("updateLocalAPPKEY fail!", new Object[0]);
                    e.printStackTrace();
                }
            }
        }
        return z;
    }

    public static boolean d(Context context) {
        Exception e;
        boolean z;
        if (context == null) {
            com.tencent.beacon.d.a.d("context == null return null", new Object[0]);
            return false;
        }
        try {
            String string = context.getSharedPreferences("DENGTA_META", 0).getString("APPVER_DENGTA", null);
            String f = f(context);
            if (string != null && string.equals(f)) {
                return false;
            }
            z = true;
            try {
                SharedPreferences.Editor edit = context.getSharedPreferences("DENGTA_META", 0).edit();
                edit.putString("APPVER_DENGTA", f);
                edit.commit();
                return true;
            } catch (Exception e2) {
                e = e2;
                com.tencent.beacon.d.a.b("updateLocalAPPKEY fail!", new Object[0]);
                e.printStackTrace();
                return z;
            }
        } catch (Exception e3) {
            e = e3;
            z = false;
            com.tencent.beacon.d.a.b("updateLocalAPPKEY fail!", new Object[0]);
            e.printStackTrace();
            return z;
        }
    }

    public static String e(Context context) {
        if (context == null) {
            return null;
        }
        return context.getPackageName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static synchronized String f(Context context) {
        String str;
        synchronized (a.class) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context == null ? null : context.getPackageName(), 0);
                String str2 = packageInfo.versionName;
                int i = packageInfo.versionCode;
                if (str2 == null || str2.trim().length() <= 0) {
                    str = new StringBuilder().append(i).toString();
                } else {
                    String replace = str2.trim().replace(10, ' ').replace(13, ' ').replace("|", "%7C");
                    char[] charArray = replace.toCharArray();
                    int i2 = 0;
                    for (char c2 : charArray) {
                        if (c2 == '.') {
                            i2++;
                        }
                    }
                    if (i2 < 3) {
                        com.tencent.beacon.d.a.a("add versionCode: %s", Integer.valueOf(i));
                        str = replace + "." + i;
                    } else {
                        str = replace;
                    }
                    com.tencent.beacon.d.a.a("version: %s", str);
                }
            } catch (Exception e) {
                e.printStackTrace();
                com.tencent.beacon.d.a.d(e.toString(), new Object[0]);
                str = Constants.STR_EMPTY;
            }
        }
        return str;
    }

    public static boolean g(Context context) {
        return c(context, context.getPackageName());
    }

    public static String a() {
        try {
            if (f2082a == 0) {
                f2082a = Process.myPid();
            }
            return (Constants.STR_EMPTY + f2082a + "_") + new Date().getTime();
        } catch (Exception e) {
            return Constants.STR_EMPTY;
        }
    }

    public static int h(Context context) {
        try {
            if (f2082a == 0) {
                f2082a = Process.myPid();
            }
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
                if (next.pid == f2082a) {
                    return next.importance;
                }
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public static String i(Context context) {
        try {
            if (f2082a == 0) {
                f2082a = Process.myPid();
            }
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
                if (next.pid == f2082a) {
                    return next.processName;
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return Constants.STR_EMPTY;
    }

    private static boolean c(Context context, String str) {
        if (context == null || str == null || str.trim().length() <= 0) {
            return false;
        }
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses == null || runningAppProcesses.size() == 0) {
                com.tencent.beacon.d.a.b("no running proc", new Object[0]);
                return false;
            }
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.importance == 100) {
                    for (String equals : next.pkgList) {
                        if (str.equals(equals)) {
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("Failed to judge }[%s]", th.getLocalizedMessage());
        }
    }

    public static boolean a(Context context, String str, String str2) {
        return context.getSharedPreferences("DENGTA_META", 0).edit().putString(str, str2).commit();
    }

    public static String b(Context context, String str, String str2) {
        return context.getSharedPreferences("DENGTA_META", 0).getString(str, str2);
    }

    public static void a(Context context, String str) {
        context.getSharedPreferences("DENGTA_META", 0).edit().putString("key_initsdktimes", str).commit();
    }

    public static void b(Context context, String str) {
        context.getSharedPreferences("DENGTA_META", 0).edit().putString("key_initsdkdate", str).commit();
    }
}
