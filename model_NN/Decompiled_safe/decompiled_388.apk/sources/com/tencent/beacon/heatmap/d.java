package com.tencent.beacon.heatmap;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/* compiled from: ProGuard */
public final class d extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final GestureDetector f2154a;

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        this.f2154a.onTouchEvent(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }
}
