package com.tencent.beacon.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/* compiled from: ProGuard */
public final class g implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f2136a = 12;
    private int b = 60;
    private int c = 12;
    private int d = 60;
    private int e = 20;
    private int f = 2097152;
    private int g = 60;
    private int h = 600;
    private boolean i = true;
    private boolean j = false;
    private boolean k = false;
    private int l = 30;
    private boolean m = true;
    private boolean n = true;
    private Set<String> o = null;
    private Map<String, Float> p = null;
    private boolean q = false;
    private int r = 600;
    private boolean s = false;
    private int t = 25;
    private int u = 600;

    public final synchronized void a(Map<String, String> map) {
        int intValue;
        int intValue2;
        int intValue3;
        int intValue4;
        int intValue5;
        int intValue6;
        int intValue7;
        int intValue8;
        int intValue9;
        int intValue10;
        int intValue11;
        int intValue12;
        if (map != null) {
            try {
                String str = map.get("realNumUp");
                if (str != null && (intValue12 = Integer.valueOf(str).intValue()) > 0 && intValue12 <= 50) {
                    this.f2136a = intValue12;
                }
                String str2 = map.get("realDelayUp");
                if (str2 != null && (intValue11 = Integer.valueOf(str2).intValue()) >= 10 && intValue11 <= 600) {
                    this.b = intValue11;
                }
                String str3 = map.get("comNumDB");
                if (str3 != null && (intValue10 = Integer.valueOf(str3).intValue()) > 0 && intValue10 <= 50) {
                    this.c = intValue10;
                }
                String str4 = map.get("comDelayDB");
                if (str4 != null && (intValue9 = Integer.valueOf(str4).intValue()) >= 60 && intValue9 <= 600) {
                    this.d = intValue9;
                }
                String str5 = map.get("comNumUp");
                if (str5 != null && (intValue8 = Integer.valueOf(str5).intValue()) > 0 && intValue8 <= 100) {
                    this.e = intValue8;
                }
                String str6 = map.get("dailyNetFlowLimit");
                if (str6 != null && (intValue7 = Integer.valueOf(str6).intValue()) >= 204800 && intValue7 <= 10485760) {
                    this.f = intValue7;
                }
                String str7 = map.get("runInfoPeriod");
                if (str7 != null && (intValue6 = Integer.valueOf(str7).intValue()) >= 30 && intValue6 <= 300) {
                    this.g = intValue6;
                }
                String str8 = map.get("useTimeUpPeriod");
                if (str8 != null && (intValue5 = Integer.valueOf(str8).intValue()) >= 300 && intValue5 <= 1800) {
                    this.h = intValue5;
                }
                String str9 = map.get("useTimeOnOff");
                if (str9 != null) {
                    if (str9.toLowerCase().equals("y")) {
                        this.i = true;
                    } else if (str9.toLowerCase().equals("n")) {
                        this.i = false;
                    }
                }
                String str10 = map.get("proChangePeriod");
                if (str10 != null && (intValue4 = Integer.valueOf(str10).intValue()) >= 10 && intValue4 <= 300) {
                    this.l = intValue4;
                }
                String str11 = map.get("proChangeOnOff");
                if (str11 != null) {
                    if (str11.toLowerCase().equals("y")) {
                        this.m = true;
                    } else if (str11.toLowerCase().equals("n")) {
                        this.m = false;
                    }
                }
                String str12 = map.get("heartOnOff");
                if (str12 != null) {
                    if (str12.toLowerCase().equals("y")) {
                        this.n = true;
                    } else if (str12.toLowerCase().equals("n")) {
                        this.n = false;
                    }
                }
                String str13 = map.get("appLogUploadOnOff");
                if (str13 != null) {
                    if (str13.toLowerCase().equals("y")) {
                        String str14 = map.get("appLogRealTimeUpload");
                        if (str14 != null && !str14.toLowerCase().equals("y")) {
                            str14.toLowerCase().equals("n");
                        }
                        String str15 = map.get("appLogOutDay");
                        if (str15 != null) {
                            Long.valueOf(str15).longValue();
                        }
                        if (map.get("logDailyConsumeLimit") != null) {
                            Integer.valueOf(str15).intValue();
                        }
                        String str16 = map.get("appLogSizeLimit");
                        if (str16 != null) {
                            Integer.valueOf(str16).intValue();
                        }
                        String str17 = map.get("appLogRecordMax");
                        if (str17 != null) {
                            Integer.valueOf(str17).intValue();
                        }
                        String str18 = map.get("appLogFileTotalMaxSize");
                        if (str18 != null) {
                            Integer.valueOf(str18).intValue();
                        }
                    } else {
                        str13.toLowerCase().equals("n");
                    }
                }
                String str19 = map.get("appNetConOnOff");
                if (str19 != null) {
                    if (str19.toLowerCase().equals("y")) {
                        this.q = true;
                    } else if (str19.toLowerCase().equals("n")) {
                        this.q = false;
                    }
                }
                String str20 = map.get("netConQuePeriod");
                if (str20 != null && (intValue3 = Integer.valueOf(str20).intValue()) >= 60 && intValue3 <= 1200) {
                    this.r = intValue3;
                }
                String str21 = map.get("memOnOff");
                if (str21 != null) {
                    if (str21.toLowerCase().equals("y")) {
                        this.j = true;
                    } else if (str21.toLowerCase().equals("n")) {
                        this.j = false;
                    }
                }
                String str22 = map.get("cpuOnOff");
                if (str22 != null) {
                    if (str22.toLowerCase().equals("y")) {
                        this.k = true;
                    } else if (str22.toLowerCase().equals("n")) {
                        this.k = false;
                    }
                }
                String str23 = map.get("heatmapOnOff");
                if (str23 != null) {
                    if (str23.toLowerCase().equals("y")) {
                        this.s = true;
                    } else if (str23.toLowerCase().equals("n")) {
                        this.s = false;
                    }
                }
                String str24 = map.get("heatmapUpMax");
                if (str24 != null && (intValue2 = Integer.valueOf(str24).intValue()) >= 10 && intValue2 <= 500) {
                    this.t = intValue2;
                }
                String str25 = map.get("netConQuePeriod");
                if (str25 != null && (intValue = Integer.valueOf(str25).intValue()) >= 300 && intValue <= 1800) {
                    this.u = intValue;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    public final synchronized int a() {
        return this.f2136a;
    }

    private synchronized void a(int i2) {
        if (i2 > 0) {
            this.f2136a = i2;
        }
    }

    public final synchronized int b() {
        return this.b;
    }

    private synchronized void b(int i2) {
        if (i2 > 0) {
            this.b = i2;
        }
    }

    public final synchronized int c() {
        return this.c;
    }

    private synchronized void c(int i2) {
        if (i2 > 0) {
            this.c = i2;
        }
    }

    public final synchronized int d() {
        return this.d;
    }

    private synchronized void d(int i2) {
        if (i2 > 0) {
            this.d = i2;
        }
    }

    public final synchronized int e() {
        return this.e;
    }

    private synchronized void e(int i2) {
        if (i2 > 0) {
            this.e = i2;
        }
    }

    public final synchronized int f() {
        return this.f;
    }

    private synchronized void f(int i2) {
        if (i2 > 0) {
            this.f = i2;
        }
    }

    public final synchronized int g() {
        return this.h;
    }

    public final int h() {
        return this.g;
    }

    public final boolean i() {
        return this.i;
    }

    public final int j() {
        return this.l;
    }

    public final boolean k() {
        return this.m;
    }

    public final boolean l() {
        return this.n;
    }

    public final synchronized void a(Set<String> set) {
        this.o = set;
    }

    public final synchronized boolean a(String str) {
        boolean z;
        z = false;
        if (this.o != null && this.o.size() > 0) {
            z = this.o.contains(str);
        }
        return z;
    }

    public final synchronized void b(Set<String> set) {
        if (this.p == null) {
            this.p = new HashMap();
        }
        for (String split : set) {
            String[] split2 = split.split(",");
            if (split2.length == 3) {
                try {
                    this.p.put(split2[0].toLowerCase(), Float.valueOf(Float.valueOf(split2[1]).floatValue() / Float.valueOf(split2[2]).floatValue()));
                } catch (Exception e2) {
                }
            }
        }
    }

    public final synchronized boolean b(String str) {
        boolean z;
        if (this.p == null || this.p.get(str) == null) {
            z = true;
        } else {
            if (new Random().nextInt(1000) + 1 > ((int) (this.p.get(str.toLowerCase()).floatValue() * 1000.0f))) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public final boolean m() {
        return this.q;
    }

    public final int n() {
        return this.r;
    }

    public final boolean o() {
        return this.k;
    }

    public final boolean p() {
        return this.j;
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public synchronized g clone() {
        g gVar;
        gVar = new g();
        gVar.d(this.d);
        gVar.c(this.c);
        gVar.e(this.e);
        gVar.f(this.f);
        gVar.b(this.b);
        gVar.a(this.f2136a);
        return gVar;
    }
}
