package doodle.bubblepro;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

class GameView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public Context mContext;
    private GameThread thread;

    class GameThread extends Thread {
        public static final int EXTENDED_GAMEFIELD_WIDTH = 640;
        private static final int FRAME_DELAY = 40;
        public static final int GAMEFIELD_HEIGHT = 480;
        public static final int GAMEFIELD_WIDTH = 320;
        public static final int STATE_ABOUT = 4;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_RUNNING = 1;
        private static final double TOUCH_COEFFICIENT = 0.2d;
        private static final double TOUCH_FIRE_Y_THRESHOLD = 350.0d;
        private static final double TRACKBALL_COEFFICIENT = 5.0d;
        private BmpWrap mBackground;
        private Bitmap mBackgroundOrig;
        private BmpWrap mBubbleBlink;
        private Bitmap mBubbleBlinkOrig;
        private BmpWrap[] mBubbles;
        private BmpWrap[] mBubblesBlind;
        private Bitmap[] mBubblesBlindOrig;
        private Bitmap[] mBubblesOrig;
        private int mCanvasHeight = 1;
        private int mCanvasWidth = 1;
        private BmpWrap mCompressor;
        private BmpWrap mCompressorHead;
        private Bitmap mCompressorHeadOrig;
        private Bitmap mCompressorOrig;
        private int mDisplayDX;
        private int mDisplayDY;
        private double mDisplayScale;
        private boolean mFire = false;
        private BubbleFont mFont;
        private BmpWrap mFontImage;
        private Bitmap mFontImageOrig;
        private BmpWrap[] mFrozenBubbles;
        private Bitmap[] mFrozenBubblesOrig;
        private FrozenGame mFrozenGame;
        private BmpWrap mGameLost;
        private Bitmap mGameLostOrig;
        private BmpWrap mGameWon;
        private Bitmap mGameWonOrig;
        private BmpWrap mHurry;
        private Bitmap mHurryOrig;
        Vector mImageList;
        private boolean mImagesReady = false;
        private long mLastTime;
        private Drawable mLauncher;
        private boolean mLeft = false;
        private LevelManager mLevelManager;
        private BmpWrap mLife;
        private Bitmap mLifeOrig;
        private int mMode;
        private BmpWrap mPenguins;
        private Bitmap mPenguinsOrig;
        private boolean mRight = false;
        private boolean mRun = false;
        private SoundManager mSoundManager;
        private SurfaceHolder mSurfaceHolder;
        private boolean mSurfaceOK = false;
        private BmpWrap[] mTargetedBubbles;
        private Bitmap[] mTargetedBubblesOrig;
        private double mTouchDX = 0.0d;
        private boolean mTouchFire = false;
        private double mTouchLastX;
        private double mTouchLastY;
        private double mTrackballDX = 0.0d;
        private boolean mUp = false;
        private boolean mWasFire = false;
        private boolean mWasLeft = false;
        private boolean mWasRight = false;
        private boolean mWasUp = false;

        public int getCurrentLevelIndex() {
            int levelIndex;
            synchronized (this.mSurfaceHolder) {
                levelIndex = this.mLevelManager.getLevelIndex();
            }
            return levelIndex;
        }

        private BmpWrap NewBmpWrap() {
            BmpWrap new_img = new BmpWrap(this.mImageList.size());
            this.mImageList.addElement(new_img);
            return new_img;
        }

        public GameThread(SurfaceHolder surfaceHolder, byte[] customLevels, int startingLevel) {
            this.mSurfaceHolder = surfaceHolder;
            Resources res = GameView.this.mContext.getResources();
            setState(2);
            BitmapFactory.Options options = new BitmapFactory.Options();
            try {
                options.getClass().getField("inScaled").set(options, Boolean.FALSE);
            } catch (Exception e) {
            }
            this.mBackgroundOrig = BitmapFactory.decodeResource(res, R.drawable.background, options);
            this.mBubblesOrig = new Bitmap[8];
            this.mBubblesOrig[0] = BitmapFactory.decodeResource(res, R.drawable.bubble_1, options);
            this.mBubblesOrig[1] = BitmapFactory.decodeResource(res, R.drawable.bubble_2, options);
            this.mBubblesOrig[2] = BitmapFactory.decodeResource(res, R.drawable.bubble_3, options);
            this.mBubblesOrig[3] = BitmapFactory.decodeResource(res, R.drawable.bubble_4, options);
            this.mBubblesOrig[4] = BitmapFactory.decodeResource(res, R.drawable.bubble_5, options);
            this.mBubblesOrig[5] = BitmapFactory.decodeResource(res, R.drawable.bubble_6, options);
            this.mBubblesOrig[6] = BitmapFactory.decodeResource(res, R.drawable.bubble_7, options);
            this.mBubblesOrig[7] = BitmapFactory.decodeResource(res, R.drawable.bubble_8, options);
            this.mBubblesBlindOrig = new Bitmap[8];
            this.mBubblesBlindOrig[0] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_1, options);
            this.mBubblesBlindOrig[1] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_2, options);
            this.mBubblesBlindOrig[2] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_3, options);
            this.mBubblesBlindOrig[3] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_4, options);
            this.mBubblesBlindOrig[4] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_5, options);
            this.mBubblesBlindOrig[5] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_6, options);
            this.mBubblesBlindOrig[6] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_7, options);
            this.mBubblesBlindOrig[7] = BitmapFactory.decodeResource(res, R.drawable.bubble_colourblind_8, options);
            this.mFrozenBubblesOrig = new Bitmap[8];
            this.mFrozenBubblesOrig[0] = BitmapFactory.decodeResource(res, R.drawable.frozen_1, options);
            this.mFrozenBubblesOrig[1] = BitmapFactory.decodeResource(res, R.drawable.frozen_2, options);
            this.mFrozenBubblesOrig[2] = BitmapFactory.decodeResource(res, R.drawable.frozen_3, options);
            this.mFrozenBubblesOrig[3] = BitmapFactory.decodeResource(res, R.drawable.frozen_4, options);
            this.mFrozenBubblesOrig[4] = BitmapFactory.decodeResource(res, R.drawable.frozen_5, options);
            this.mFrozenBubblesOrig[5] = BitmapFactory.decodeResource(res, R.drawable.frozen_6, options);
            this.mFrozenBubblesOrig[6] = BitmapFactory.decodeResource(res, R.drawable.frozen_7, options);
            this.mFrozenBubblesOrig[7] = BitmapFactory.decodeResource(res, R.drawable.frozen_8, options);
            this.mTargetedBubblesOrig = new Bitmap[6];
            this.mTargetedBubblesOrig[0] = BitmapFactory.decodeResource(res, R.drawable.fixed_1, options);
            this.mTargetedBubblesOrig[1] = BitmapFactory.decodeResource(res, R.drawable.fixed_2, options);
            this.mTargetedBubblesOrig[2] = BitmapFactory.decodeResource(res, R.drawable.fixed_3, options);
            this.mTargetedBubblesOrig[3] = BitmapFactory.decodeResource(res, R.drawable.fixed_4, options);
            this.mTargetedBubblesOrig[4] = BitmapFactory.decodeResource(res, R.drawable.fixed_5, options);
            this.mTargetedBubblesOrig[5] = BitmapFactory.decodeResource(res, R.drawable.fixed_6, options);
            this.mBubbleBlinkOrig = BitmapFactory.decodeResource(res, R.drawable.bubble_blink, options);
            this.mGameWonOrig = BitmapFactory.decodeResource(res, R.drawable.win_panel, options);
            this.mGameLostOrig = BitmapFactory.decodeResource(res, R.drawable.lose_panel, options);
            this.mHurryOrig = BitmapFactory.decodeResource(res, R.drawable.hurry, options);
            this.mPenguinsOrig = BitmapFactory.decodeResource(res, R.drawable.penguins, options);
            this.mCompressorHeadOrig = BitmapFactory.decodeResource(res, R.drawable.compressor, options);
            this.mCompressorOrig = BitmapFactory.decodeResource(res, R.drawable.compressor_body, options);
            this.mLifeOrig = BitmapFactory.decodeResource(res, R.drawable.life, options);
            this.mFontImageOrig = BitmapFactory.decodeResource(res, R.drawable.bubble_font, options);
            this.mImageList = new Vector();
            this.mBackground = NewBmpWrap();
            this.mBubbles = new BmpWrap[8];
            for (int i = 0; i < this.mBubbles.length; i++) {
                this.mBubbles[i] = NewBmpWrap();
            }
            this.mBubblesBlind = new BmpWrap[8];
            for (int i2 = 0; i2 < this.mBubblesBlind.length; i2++) {
                this.mBubblesBlind[i2] = NewBmpWrap();
            }
            this.mFrozenBubbles = new BmpWrap[8];
            for (int i3 = 0; i3 < this.mFrozenBubbles.length; i3++) {
                this.mFrozenBubbles[i3] = NewBmpWrap();
            }
            this.mTargetedBubbles = new BmpWrap[6];
            for (int i4 = 0; i4 < this.mTargetedBubbles.length; i4++) {
                this.mTargetedBubbles[i4] = NewBmpWrap();
            }
            this.mBubbleBlink = NewBmpWrap();
            this.mGameWon = NewBmpWrap();
            this.mGameLost = NewBmpWrap();
            this.mHurry = NewBmpWrap();
            this.mPenguins = NewBmpWrap();
            this.mCompressorHead = NewBmpWrap();
            this.mCompressor = NewBmpWrap();
            this.mLife = NewBmpWrap();
            this.mFontImage = NewBmpWrap();
            this.mFont = new BubbleFont(this.mFontImage);
            this.mLauncher = res.getDrawable(R.drawable.launcher);
            this.mSoundManager = new SoundManager(GameView.this.mContext);
            if (customLevels == null) {
                try {
                    InputStream is = GameView.this.mContext.getAssets().open("levels.txt");
                    byte[] levels = new byte[is.available()];
                    is.read(levels);
                    is.close();
                    this.mLevelManager = new LevelManager(levels, GameView.this.mContext.getSharedPreferences(FrozenBubble.PREFS_NAME, 0).getInt("level", 0));
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            } else {
                this.mLevelManager = new LevelManager(customLevels, startingLevel);
            }
            this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameWon, this.mGameLost, this.mHurry, this.mPenguins, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager);
        }

        private void scaleFrom(BmpWrap image, Bitmap bmp) {
            if (!(image.bmp == null || image.bmp == bmp)) {
                image.bmp.recycle();
            }
            if (this.mDisplayScale <= 0.99999d || this.mDisplayScale >= 1.00001d) {
                image.bmp = Bitmap.createScaledBitmap(bmp, (int) (((double) bmp.getWidth()) * this.mDisplayScale), (int) (((double) bmp.getHeight()) * this.mDisplayScale), true);
            } else {
                image.bmp = bmp;
            }
        }

        private void resizeBitmaps() {
            scaleFrom(this.mBackground, this.mBackgroundOrig);
            for (int i = 0; i < this.mBubblesOrig.length; i++) {
                scaleFrom(this.mBubbles[i], this.mBubblesOrig[i]);
            }
            for (int i2 = 0; i2 < this.mBubblesBlind.length; i2++) {
                scaleFrom(this.mBubblesBlind[i2], this.mBubblesBlindOrig[i2]);
            }
            for (int i3 = 0; i3 < this.mFrozenBubbles.length; i3++) {
                scaleFrom(this.mFrozenBubbles[i3], this.mFrozenBubblesOrig[i3]);
            }
            for (int i4 = 0; i4 < this.mTargetedBubbles.length; i4++) {
                scaleFrom(this.mTargetedBubbles[i4], this.mTargetedBubblesOrig[i4]);
            }
            scaleFrom(this.mBubbleBlink, this.mBubbleBlinkOrig);
            scaleFrom(this.mGameWon, this.mGameWonOrig);
            scaleFrom(this.mGameLost, this.mGameLostOrig);
            scaleFrom(this.mHurry, this.mHurryOrig);
            scaleFrom(this.mPenguins, this.mPenguinsOrig);
            scaleFrom(this.mCompressorHead, this.mCompressorHeadOrig);
            scaleFrom(this.mCompressor, this.mCompressorOrig);
            scaleFrom(this.mLife, this.mLifeOrig);
            scaleFrom(this.mFontImage, this.mFontImageOrig);
            this.mImagesReady = true;
        }

        public void pause() {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode == 1) {
                    setState(2);
                }
            }
        }

        public void newGame() {
            synchronized (this.mSurfaceHolder) {
                this.mLevelManager.goToFirstLevel();
                this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameWon, this.mGameLost, this.mHurry, this.mPenguins, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager);
            }
        }

        public void run() {
            while (this.mRun) {
                long now = System.currentTimeMillis();
                long delay = (40 + this.mLastTime) - now;
                if (delay > 0) {
                    try {
                        sleep(delay);
                    } catch (InterruptedException e) {
                    }
                }
                this.mLastTime = now;
                Canvas c = null;
                try {
                    if (surfaceOK() && (c = this.mSurfaceHolder.lockCanvas(null)) != null) {
                        synchronized (this.mSurfaceHolder) {
                            if (this.mRun) {
                                if (this.mMode == 4) {
                                    drawAboutScreen(c);
                                } else {
                                    if (this.mMode == 1) {
                                        updateGameState();
                                    }
                                    doDraw(c);
                                }
                            }
                        }
                    }
                } finally {
                    if (c != null) {
                        this.mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        public Bundle saveState(Bundle map) {
            synchronized (this.mSurfaceHolder) {
                if (map != null) {
                    this.mFrozenGame.saveState(map);
                    this.mLevelManager.saveState(map);
                }
            }
            return map;
        }

        public synchronized void restoreState(Bundle map) {
            synchronized (this.mSurfaceHolder) {
                setState(2);
                this.mFrozenGame.restoreState(map, this.mImageList);
                this.mLevelManager.restoreState(map);
            }
        }

        public void setRunning(boolean b) {
            this.mRun = b;
        }

        public void setState(int mode) {
            synchronized (this.mSurfaceHolder) {
                this.mMode = mode;
            }
        }

        public void setSurfaceOK(boolean ok) {
            synchronized (this.mSurfaceHolder) {
                this.mSurfaceOK = ok;
            }
        }

        public boolean surfaceOK() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mSurfaceOK;
            }
            return z;
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this.mSurfaceHolder) {
                this.mCanvasWidth = width;
                this.mCanvasHeight = height;
                if (width / height >= 0) {
                    this.mDisplayScale = (((double) height) * 1.0d) / 480.0d;
                    this.mDisplayDX = (int) ((((double) width) - (this.mDisplayScale * 640.0d)) / 2.0d);
                    this.mDisplayDY = 0;
                } else {
                    this.mDisplayScale = (((double) width) * 1.0d) / 320.0d;
                    this.mDisplayDX = (int) (((-this.mDisplayScale) * 320.0d) / 2.0d);
                    this.mDisplayDY = (int) ((((double) height) - (this.mDisplayScale * 480.0d)) / 2.0d);
                }
                resizeBitmaps();
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doKeyDown(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r2 = 1
                android.view.SurfaceHolder r0 = r3.mSurfaceHolder
                monitor-enter(r0)
                int r1 = r3.mMode     // Catch:{ all -> 0x0047 }
                if (r1 == r2) goto L_0x000c
                r1 = 1
                r3.setState(r1)     // Catch:{ all -> 0x0047 }
            L_0x000c:
                int r1 = r3.mMode     // Catch:{ all -> 0x0047 }
                if (r1 != r2) goto L_0x0044
                r1 = 21
                if (r4 != r1) goto L_0x001d
                r1 = 1
                r3.mLeft = r1     // Catch:{ all -> 0x0047 }
                r1 = 1
                r3.mWasLeft = r1     // Catch:{ all -> 0x0047 }
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                r0 = r2
            L_0x001c:
                return r0
            L_0x001d:
                r1 = 22
                if (r4 != r1) goto L_0x002a
                r1 = 1
                r3.mRight = r1     // Catch:{ all -> 0x0047 }
                r1 = 1
                r3.mWasRight = r1     // Catch:{ all -> 0x0047 }
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                r0 = r2
                goto L_0x001c
            L_0x002a:
                r1 = 23
                if (r4 != r1) goto L_0x0037
                r1 = 1
                r3.mFire = r1     // Catch:{ all -> 0x0047 }
                r1 = 1
                r3.mWasFire = r1     // Catch:{ all -> 0x0047 }
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                r0 = r2
                goto L_0x001c
            L_0x0037:
                r1 = 19
                if (r4 != r1) goto L_0x0044
                r1 = 1
                r3.mUp = r1     // Catch:{ all -> 0x0047 }
                r1 = 1
                r3.mWasUp = r1     // Catch:{ all -> 0x0047 }
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                r0 = r2
                goto L_0x001c
            L_0x0044:
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                r0 = 0
                goto L_0x001c
            L_0x0047:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0047 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: doodle.bubblepro.GameView.GameThread.doKeyDown(int, android.view.KeyEvent):boolean");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doKeyUp(int r5, android.view.KeyEvent r6) {
            /*
                r4 = this;
                r3 = 1
                r2 = 0
                android.view.SurfaceHolder r0 = r4.mSurfaceHolder
                monitor-enter(r0)
                int r1 = r4.mMode     // Catch:{ all -> 0x0034 }
                if (r1 != r3) goto L_0x0031
                r1 = 21
                if (r5 != r1) goto L_0x0013
                r1 = 0
                r4.mLeft = r1     // Catch:{ all -> 0x0034 }
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                r0 = r3
            L_0x0012:
                return r0
            L_0x0013:
                r1 = 22
                if (r5 != r1) goto L_0x001d
                r1 = 0
                r4.mRight = r1     // Catch:{ all -> 0x0034 }
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                r0 = r3
                goto L_0x0012
            L_0x001d:
                r1 = 23
                if (r5 != r1) goto L_0x0027
                r1 = 0
                r4.mFire = r1     // Catch:{ all -> 0x0034 }
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                r0 = r3
                goto L_0x0012
            L_0x0027:
                r1 = 19
                if (r5 != r1) goto L_0x0031
                r1 = 0
                r4.mUp = r1     // Catch:{ all -> 0x0034 }
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                r0 = r3
                goto L_0x0012
            L_0x0031:
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                r0 = r2
                goto L_0x0012
            L_0x0034:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0034 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: doodle.bubblepro.GameView.GameThread.doKeyUp(int, android.view.KeyEvent):boolean");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doTrackballEvent(android.view.MotionEvent r9) {
            /*
                r8 = this;
                r7 = 1
                android.view.SurfaceHolder r0 = r8.mSurfaceHolder
                monitor-enter(r0)
                int r1 = r8.mMode     // Catch:{ all -> 0x002a }
                if (r1 == r7) goto L_0x000c
                r1 = 1
                r8.setState(r1)     // Catch:{ all -> 0x002a }
            L_0x000c:
                int r1 = r8.mMode     // Catch:{ all -> 0x002a }
                if (r1 != r7) goto L_0x0027
                int r1 = r9.getAction()     // Catch:{ all -> 0x002a }
                r2 = 2
                if (r1 != r2) goto L_0x0027
                double r1 = r8.mTrackballDX     // Catch:{ all -> 0x002a }
                float r3 = r9.getX()     // Catch:{ all -> 0x002a }
                double r3 = (double) r3     // Catch:{ all -> 0x002a }
                r5 = 4617315517961601024(0x4014000000000000, double:5.0)
                double r3 = r3 * r5
                double r1 = r1 + r3
                r8.mTrackballDX = r1     // Catch:{ all -> 0x002a }
                monitor-exit(r0)     // Catch:{ all -> 0x002a }
                r0 = r7
            L_0x0026:
                return r0
            L_0x0027:
                monitor-exit(r0)     // Catch:{ all -> 0x002a }
                r0 = 0
                goto L_0x0026
            L_0x002a:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x002a }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: doodle.bubblepro.GameView.GameThread.doTrackballEvent(android.view.MotionEvent):boolean");
        }

        private double xFromScr(float x) {
            return ((double) (x - ((float) this.mDisplayDX))) / this.mDisplayScale;
        }

        private double yFromScr(float y) {
            return ((double) (y - ((float) this.mDisplayDY))) / this.mDisplayScale;
        }

        /* access modifiers changed from: package-private */
        public boolean doTouchEvent(MotionEvent event) {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode != 1) {
                    setState(1);
                }
                double x = xFromScr(event.getX());
                double y = yFromScr(event.getY());
                if (event.getAction() == 0) {
                    if (y < TOUCH_FIRE_Y_THRESHOLD) {
                        this.mTouchFire = true;
                    }
                    this.mTouchLastX = x;
                    this.mTouchLastY = y;
                } else if (event.getAction() == 2) {
                    if (y >= TOUCH_FIRE_Y_THRESHOLD) {
                        this.mTouchDX = (x - this.mTouchLastX) * TOUCH_COEFFICIENT;
                    }
                    this.mTouchLastX = x;
                }
            }
            return true;
        }

        private void drawBackground(Canvas c) {
            Sprite.drawImage(this.mBackground, 0, 0, c, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
        }

        private void drawLevelNumber(Canvas canvas) {
            int level = this.mLevelManager.getLevelIndex() + 1;
            if (level < 10) {
                this.mFont.paintChar(Character.forDigit(level, 10), 185, 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else if (level < 100) {
                this.mFont.paintChar(Character.forDigit(level % 10, 10), 178 + this.mFont.paintChar(Character.forDigit(level / 10, 10), 178, 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else {
                int x = 173 + this.mFont.paintChar(Character.forDigit(level / 100, 10), 173, 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
                int level2 = level - ((level / 100) * 100);
                this.mFont.paintChar(Character.forDigit(level2 % 10, 10), x + this.mFont.paintChar(Character.forDigit(level2 / 10, 10), x, 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), 433, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            }
        }

        private void drawAboutScreen(Canvas canvas) {
            canvas.drawRGB(0, 0, 0);
            this.mFont.print("original frozen bubble:", 168, 20, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("guillaume cottenceau", 168 + 10, 20 + 26, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("alexis younes", 168 + 10, 26 + 46, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("amaury amblard-ladurantie", 168 + 10, 26 + 72, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("matthias le bidan", 168 + 10, 26 + 98, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            int y = 26 + 124;
            this.mFont.print("java version:", 168, 26 + 150, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("glenn sanson", 168 + 10, 26 + 176, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            int i = 26 + 202;
            this.mFont.print("android port:", 168, 26 + 228, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            int y2 = 26 + 254;
            this.mFont.print("aleksander fedorynski", 168 + 10, y2, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            int i2 = 26 * 2;
            this.mFont.print("android port source code", 168, y2 + 52, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("is available at:", 168, 26 + 332, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("http://code.google.com", 168, 26 + 358, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            this.mFont.print("/p/frozenbubbleandroid", 168, 26 + 384, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
        }

        private void doDraw(Canvas canvas) {
            if (this.mImagesReady) {
                if (this.mDisplayDX > 0 || this.mDisplayDY > 0) {
                    canvas.drawRGB(0, 0, 0);
                }
                drawBackground(canvas);
                drawLevelNumber(canvas);
                this.mFrozenGame.paint(canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            }
        }

        private void updateGameState() {
            if (this.mFrozenGame.play(this.mLeft || this.mWasLeft, this.mRight || this.mWasRight, this.mFire || this.mUp || this.mWasFire || this.mWasUp || this.mTouchFire, this.mTrackballDX, this.mTouchDX, this.mTouchLastX, this.mTouchLastY, this.mTouchFire)) {
                this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameWon, this.mGameLost, this.mHurry, this.mPenguins, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager);
            }
            this.mWasLeft = false;
            this.mWasRight = false;
            this.mWasFire = false;
            this.mWasUp = false;
            this.mTrackballDX = 0.0d;
            this.mTouchFire = false;
            this.mTouchDX = 0.0d;
        }

        public void cleanUp() {
            boolean imagesScaled;
            synchronized (this.mSurfaceHolder) {
                this.mImagesReady = false;
                if (this.mBackgroundOrig == this.mBackground.bmp) {
                    imagesScaled = true;
                } else {
                    imagesScaled = false;
                }
                this.mBackgroundOrig.recycle();
                this.mBackgroundOrig = null;
                for (int i = 0; i < this.mBubblesOrig.length; i++) {
                    this.mBubblesOrig[i].recycle();
                    this.mBubblesOrig[i] = null;
                }
                this.mBubblesOrig = null;
                for (int i2 = 0; i2 < this.mBubblesBlindOrig.length; i2++) {
                    this.mBubblesBlindOrig[i2].recycle();
                    this.mBubblesBlindOrig[i2] = null;
                }
                this.mBubblesBlindOrig = null;
                for (int i3 = 0; i3 < this.mFrozenBubblesOrig.length; i3++) {
                    this.mFrozenBubblesOrig[i3].recycle();
                    this.mFrozenBubblesOrig[i3] = null;
                }
                this.mFrozenBubblesOrig = null;
                for (int i4 = 0; i4 < this.mTargetedBubblesOrig.length; i4++) {
                    this.mTargetedBubblesOrig[i4].recycle();
                    this.mTargetedBubblesOrig[i4] = null;
                }
                this.mTargetedBubblesOrig = null;
                this.mBubbleBlinkOrig.recycle();
                this.mBubbleBlinkOrig = null;
                this.mGameWonOrig.recycle();
                this.mGameWonOrig = null;
                this.mGameLostOrig.recycle();
                this.mGameLostOrig = null;
                this.mHurryOrig.recycle();
                this.mHurryOrig = null;
                this.mPenguinsOrig.recycle();
                this.mPenguinsOrig = null;
                this.mCompressorHeadOrig.recycle();
                this.mCompressorHeadOrig = null;
                this.mCompressorOrig.recycle();
                this.mCompressorOrig = null;
                this.mLifeOrig.recycle();
                this.mLifeOrig = null;
                if (imagesScaled) {
                    this.mBackground.bmp.recycle();
                    for (BmpWrap bmpWrap : this.mBubbles) {
                        bmpWrap.bmp.recycle();
                    }
                    for (BmpWrap bmpWrap2 : this.mBubblesBlind) {
                        bmpWrap2.bmp.recycle();
                    }
                    for (BmpWrap bmpWrap3 : this.mFrozenBubbles) {
                        bmpWrap3.bmp.recycle();
                    }
                    for (BmpWrap bmpWrap4 : this.mTargetedBubbles) {
                        bmpWrap4.bmp.recycle();
                    }
                    this.mBubbleBlink.bmp.recycle();
                    this.mGameWon.bmp.recycle();
                    this.mGameLost.bmp.recycle();
                    this.mHurry.bmp.recycle();
                    this.mPenguins.bmp.recycle();
                    this.mCompressorHead.bmp.recycle();
                    this.mCompressor.bmp.recycle();
                    this.mLife.bmp.recycle();
                }
                this.mBackground.bmp = null;
                this.mBackground = null;
                for (int i5 = 0; i5 < this.mBubbles.length; i5++) {
                    this.mBubbles[i5].bmp = null;
                    this.mBubbles[i5] = null;
                }
                this.mBubbles = null;
                for (int i6 = 0; i6 < this.mBubblesBlind.length; i6++) {
                    this.mBubblesBlind[i6].bmp = null;
                    this.mBubblesBlind[i6] = null;
                }
                this.mBubblesBlind = null;
                for (int i7 = 0; i7 < this.mFrozenBubbles.length; i7++) {
                    this.mFrozenBubbles[i7].bmp = null;
                    this.mFrozenBubbles[i7] = null;
                }
                this.mFrozenBubbles = null;
                for (int i8 = 0; i8 < this.mTargetedBubbles.length; i8++) {
                    this.mTargetedBubbles[i8].bmp = null;
                    this.mTargetedBubbles[i8] = null;
                }
                this.mTargetedBubbles = null;
                this.mBubbleBlink.bmp = null;
                this.mBubbleBlink = null;
                this.mGameWon.bmp = null;
                this.mGameWon = null;
                this.mGameLost.bmp = null;
                this.mGameLost = null;
                this.mHurry.bmp = null;
                this.mHurry = null;
                this.mPenguins.bmp = null;
                this.mPenguins = null;
                this.mCompressorHead.bmp = null;
                this.mCompressorHead = null;
                this.mCompressor.bmp = null;
                this.mCompressor = null;
                this.mLife.bmp = null;
                this.mLife = null;
                this.mImageList = null;
                this.mSoundManager.cleanUp();
                this.mSoundManager = null;
                this.mLevelManager = null;
                this.mFrozenGame = null;
            }
        }
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.thread = new GameThread(holder, null, 0);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.thread.setRunning(true);
        this.thread.start();
    }

    public GameView(Context context, byte[] levels, int startingLevel) {
        super(context);
        this.mContext = context;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.thread = new GameThread(holder, levels, startingLevel);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.thread.setRunning(true);
        this.thread.start();
    }

    public GameThread getThread() {
        return this.thread;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return this.thread.doKeyDown(keyCode, msg);
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return this.thread.doKeyUp(keyCode, msg);
    }

    public boolean onTrackballEvent(MotionEvent event) {
        return this.thread.doTrackballEvent(event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.thread.doTouchEvent(event);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            this.thread.pause();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.thread.setSurfaceOK(true);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.thread.setSurfaceOK(false);
    }

    public void cleanUp() {
        this.thread.cleanUp();
        this.mContext = null;
    }
}
