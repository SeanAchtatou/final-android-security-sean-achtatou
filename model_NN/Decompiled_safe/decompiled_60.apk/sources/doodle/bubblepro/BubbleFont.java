package doodle.bubblepro;

import android.graphics.Canvas;
import android.graphics.Rect;

public class BubbleFont {
    public int SEPARATOR_WIDTH;
    public int SPACE_CHAR_WIDTH;
    private char[] characters = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    private Rect clipRect;
    private BmpWrap fontMap;
    private int[] position;

    public BubbleFont(BmpWrap fontMap2) {
        int[] iArr = new int[11];
        iArr[1] = 22;
        iArr[2] = 44;
        iArr[3] = 74;
        iArr[4] = 102;
        iArr[5] = 128;
        iArr[6] = 152;
        iArr[7] = 180;
        iArr[8] = 206;
        iArr[9] = 234;
        iArr[10] = 258;
        this.position = iArr;
        this.SEPARATOR_WIDTH = 1;
        this.SPACE_CHAR_WIDTH = 6;
        this.fontMap = fontMap2;
        this.clipRect = new Rect();
    }

    public final void print(String s, int x, int y, Canvas canvas, double scale, int dx, int dy) {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            x += paintChar(s.charAt(i), x, y, canvas, scale, dx, dy);
        }
    }

    public final int paintChar(char c, int x, int y, Canvas canvas, double scale, int dx, int dy) {
        if (c == ' ') {
            return this.SPACE_CHAR_WIDTH + this.SEPARATOR_WIDTH;
        }
        int index = getCharIndex(c);
        if (index == -1) {
            return 0;
        }
        int imageWidth = this.position[index + 1] - this.position[index];
        this.clipRect.left = x;
        this.clipRect.right = x + imageWidth;
        this.clipRect.top = y;
        this.clipRect.bottom = y + 44;
        Sprite.drawImageClipped(this.fontMap, x - this.position[index], y, this.clipRect, canvas, scale, dx, dy);
        return this.SEPARATOR_WIDTH + imageWidth;
    }

    private final int getCharIndex(char c) {
        for (int i = 0; i < this.characters.length; i++) {
            if (this.characters[i] == c) {
                return i;
            }
        }
        return -1;
    }
}
