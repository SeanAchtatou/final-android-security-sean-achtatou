package doodle.bubblepro;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import doodle.bubblepro.GameView;

public class FrozenBubble extends Activity {
    private static final String EDITORACTION = "doodle.bubblepro.GAME";
    public static final int GAME_COLORBLIND = 1;
    public static final int GAME_NORMAL = 0;
    public static final int MENU_ABOUT = 10;
    public static final int MENU_COLORBLIND_MODE_OFF = 2;
    public static final int MENU_COLORBLIND_MODE_ON = 1;
    public static final int MENU_DONT_RUSH_ME = 7;
    public static final int MENU_EDITOR = 11;
    public static final int MENU_FULLSCREEN_OFF = 4;
    public static final int MENU_FULLSCREEN_ON = 3;
    public static final int MENU_NEW_GAME = 9;
    public static final int MENU_RUSH_ME = 8;
    public static final int MENU_SOUND_OFF = 6;
    public static final int MENU_SOUND_ON = 5;
    public static final int NUM_SOUNDS = 9;
    public static final String PREFS_NAME = "frozenbubble";
    public static final int SOUND_DESTROY = 3;
    public static final int SOUND_HURRY = 6;
    public static final int SOUND_LAUNCH = 2;
    public static final int SOUND_LOST = 1;
    public static final int SOUND_NEWROOT = 7;
    public static final int SOUND_NOH = 8;
    public static final int SOUND_REBOUND = 4;
    public static final int SOUND_STICK = 5;
    public static final int SOUND_WON = 0;
    private static boolean dontRushMe = false;
    private static int gameMode = 0;
    private static boolean soundOn = true;
    private boolean activityCustomStarted = false;
    AdView adview;
    private boolean fullscreen = true;
    int jjj;
    private AudioManager mAudioManager = null;
    /* access modifiers changed from: private */
    public GameView.GameThread mGameThread;
    private GameView mGameView;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_colorblind_mode_on);
        menu.add(0, 2, 0, (int) R.string.menu_colorblind_mode_off);
        menu.add(0, 5, 0, (int) R.string.menu_sound_on);
        menu.add(0, 6, 0, (int) R.string.menu_sound_off);
        menu.add(0, 9, 0, (int) R.string.menu_new_game);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        boolean z2;
        super.onPrepareOptionsMenu(menu);
        menu.findItem(5).setVisible(!getSoundOn());
        menu.findItem(6).setVisible(getSoundOn());
        MenuItem findItem = menu.findItem(1);
        if (getMode() == 0) {
            z = true;
        } else {
            z = false;
        }
        findItem.setVisible(z);
        MenuItem findItem2 = menu.findItem(2);
        if (getMode() != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        findItem2.setVisible(z2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                setMode(1);
                return true;
            case 2:
                setMode(0);
                return true;
            case 3:
            case 4:
            case 7:
            case 8:
            default:
                return false;
            case 5:
                setSoundOn(true);
                return true;
            case 6:
                setSoundOn(false);
                return true;
            case 9:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle("New game").setMessage("Start new game from level 1, are you sure?").setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        FrozenBubble.this.mGameThread.newGame();
                    }
                }).show();
                return true;
        }
    }

    private void setFullscreen() {
        if (this.fullscreen) {
            getWindow().addFlags(1024);
            getWindow().clearFlags(2048);
        } else {
            getWindow().clearFlags(1024);
            getWindow().addFlags(2048);
        }
        this.mGameView.requestLayout();
    }

    public static synchronized void setMode(int newMode) {
        synchronized (FrozenBubble.class) {
            gameMode = newMode;
        }
    }

    public static synchronized int getMode() {
        int i;
        synchronized (FrozenBubble.class) {
            i = gameMode;
        }
        return i;
    }

    public static synchronized boolean getSoundOn() {
        boolean z;
        synchronized (FrozenBubble.class) {
            z = soundOn;
        }
        return z;
    }

    public static synchronized void setSoundOn(boolean so) {
        synchronized (FrozenBubble.class) {
            soundOn = so;
        }
    }

    public static synchronized boolean getDontRushMe() {
        boolean z;
        synchronized (FrozenBubble.class) {
            z = dontRushMe;
        }
        return z;
    }

    public static synchronized void setDontRushMe(boolean dont) {
        synchronized (FrozenBubble.class) {
            dontRushMe = dont;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mAudioManager = (AudioManager) getSystemService("audio");
        Intent i = getIntent();
        if (i == null || i.getExtras() == null || !i.getExtras().containsKey("levels")) {
            this.activityCustomStarted = false;
            setContentView((int) R.layout.main);
            this.adview = (AdView) findViewById(R.id.adView);
            this.adview.loadAd(new AdRequest());
            this.mGameView = (GameView) findViewById(R.id.game);
        } else {
            int startingLevel = getSharedPreferences(PREFS_NAME, 0).getInt("levelCustom", 0);
            int startingLevelIntent = i.getIntExtra("startingLevel", -2);
            if (startingLevelIntent != -2) {
                startingLevel = startingLevelIntent;
            }
            this.activityCustomStarted = true;
            this.mGameView = new GameView(this, i.getExtras().getByteArray("levels"), startingLevel);
            setContentView(this.mGameView);
            this.adview = (AdView) findViewById(R.id.adView);
            this.adview.loadAd(new AdRequest());
        }
        this.mGameThread = this.mGameView.getThread();
        if (savedInstanceState != null) {
            this.mGameThread.restoreState(savedInstanceState);
        }
        this.mGameView.requestFocus();
        setFullscreen();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mGameView.getThread().pause();
        if (getIntent() == null || !this.activityCustomStarted) {
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
            editor.putInt("level", this.mGameThread.getCurrentLevelIndex());
            editor.commit();
            return;
        }
        SharedPreferences.Editor editor2 = getSharedPreferences(PREFS_NAME, 0).edit();
        editor2.putInt("levelCustom", this.mGameThread.getCurrentLevelIndex());
        editor2.commit();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mGameView != null) {
            this.mGameView.cleanUp();
        }
        this.mGameView = null;
        this.mGameThread = null;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.mGameThread.saveState(outState);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if (intent != null && EDITORACTION.equals(intent.getAction()) && !this.activityCustomStarted) {
            this.activityCustomStarted = true;
            int startingLevel = getSharedPreferences(PREFS_NAME, 0).getInt("levelCustom", 0);
            int startingLevelIntent = intent.getIntExtra("startingLevel", -2);
            if (startingLevelIntent != -2) {
                startingLevel = startingLevelIntent;
            }
            this.mGameView = null;
            this.mGameView = new GameView(this, intent.getExtras().getByteArray("levels"), startingLevel);
            setContentView(this.mGameView);
            this.mGameThread = this.mGameView.getThread();
            this.mGameThread.newGame();
            this.mGameView.requestFocus();
            setFullscreen();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 24:
                    this.mAudioManager.adjustStreamVolume(3, 1, 1);
                    return true;
                case 25:
                    this.mAudioManager.adjustStreamVolume(3, -1, 1);
                    return true;
            }
        }
        return false;
    }

    private void startEditor() {
        Intent i = new Intent();
        i.setClassName("sk.halmi.fbeditplus", "sk.halmi.fbeditplus.EditorActivity");
        try {
            startActivity(i);
            finish();
        } catch (ActivityNotFoundException e) {
            i.setClassName("sk.halmi.fbedit", "sk.halmi.fbedit.EditorActivity");
            try {
                startActivity(i);
                finish();
            } catch (ActivityNotFoundException e2) {
                try {
                    Toast.makeText(getApplicationContext(), (int) R.string.install_editor, 1000).show();
                    try {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=frozen bubble level editor")));
                    } catch (Exception e3) {
                        Toast.makeText(getApplicationContext(), (int) R.string.market_missing, 1000).show();
                    }
                } catch (Exception e4) {
                    Toast.makeText(getApplicationContext(), (int) R.string.market_missing, 1000).show();
                }
            }
        }
    }
}
