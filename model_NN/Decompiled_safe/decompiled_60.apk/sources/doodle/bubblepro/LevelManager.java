package doodle.bubblepro;

import android.os.Bundle;
import java.lang.reflect.Array;
import java.util.Vector;

public class LevelManager {
    private int currentLevel;
    private Vector levelList = new Vector();

    public void saveState(Bundle map) {
        map.putInt("LevelManager-currentLevel", this.currentLevel);
    }

    public void restoreState(Bundle map) {
        this.currentLevel = map.getInt("LevelManager-currentLevel");
    }

    public LevelManager(byte[] levels, int startingLevel) {
        int nextLevel;
        String allLevels = new String(levels);
        this.currentLevel = startingLevel;
        int nextLevel2 = allLevels.indexOf("\n\n");
        if (nextLevel2 == -1 && allLevels.trim().length() != 0) {
            nextLevel2 = allLevels.length();
        }
        while (nextLevel != -1) {
            this.levelList.addElement(getLevel(allLevels.substring(0, nextLevel).trim()));
            allLevels = allLevels.substring(nextLevel).trim();
            if (allLevels.length() == 0) {
                nextLevel = -1;
            } else {
                nextLevel = allLevels.indexOf("\n\n");
                if (nextLevel == -1) {
                    nextLevel = allLevels.length();
                }
            }
        }
        if (this.currentLevel >= this.levelList.size()) {
            this.currentLevel = 0;
        }
    }

    private byte[][] getLevel(String data) {
        byte[][] temp = (byte[][]) Array.newInstance(Byte.TYPE, 8, 12);
        for (int j = 0; j < 12; j++) {
            for (int i = 0; i < 8; i++) {
                temp[i][j] = -1;
            }
        }
        int tempX = 0;
        int tempY = 0;
        for (int i2 = 0; i2 < data.length(); i2++) {
            if (data.charAt(i2) >= '0' && data.charAt(i2) <= '7') {
                temp[tempX][tempY] = (byte) (data.charAt(i2) - '0');
                tempX++;
            } else if (data.charAt(i2) == '-') {
                temp[tempX][tempY] = -1;
                tempX++;
            }
            if (tempX == 8) {
                tempY++;
                if (tempY == 12) {
                    break;
                }
                tempX = tempY % 2;
            }
        }
        return temp;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public byte[][] getCurrentLevel() {
        if (this.currentLevel < this.levelList.size()) {
            return (byte[][]) this.levelList.elementAt(this.currentLevel);
        }
        return null;
    }

    public void goToNextLevel() {
        this.currentLevel++;
        if (this.currentLevel >= this.levelList.size()) {
            this.currentLevel--;
        }
    }

    public void goToFirstLevel() {
        this.currentLevel = 0;
    }

    public int getLevelIndex() {
        return this.currentLevel;
    }
}
