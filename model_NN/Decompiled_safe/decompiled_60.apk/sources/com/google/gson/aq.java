package com.google.gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

final class aq<T> {
    private static final Logger a = Logger.getLogger(aq.class.getName());
    private final Map<Type, T> b = new HashMap();
    private final List<ap<Class<?>, T>> c = new ArrayList();
    private boolean d = true;

    aq() {
    }

    private int a(Class<?> cls) {
        for (int size = this.c.size() - 1; size >= 0; size--) {
            if (cls.isAssignableFrom((Class) this.c.get(size).a)) {
                return size;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
     arg types: [java.util.logging.Level, java.lang.String, FIRST]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void} */
    private synchronized void a(ap<Class<?>, InstanceCreator<?>> apVar) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        int b2 = b((Class<?>) ((Class) apVar.a));
        if (b2 >= 0) {
            a.log(Level.WARNING, "Overriding the existing type handler for {0}", (Object) apVar.a);
            this.c.remove(b2);
        }
        int a2 = a((Class<?>) ((Class) apVar.a));
        if (a2 >= 0) {
            throw new IllegalArgumentException("The specified type handler for type " + ((Object) apVar.a) + " hides the previously registered type hierarchy handler for " + ((Object) this.c.get(a2).a) + ". Gson does not allow this.");
        }
        this.c.add(0, apVar);
    }

    private synchronized int b(Class<?> cls) {
        int i;
        int size = this.c.size() - 1;
        while (true) {
            if (size < 0) {
                i = -1;
                break;
            } else if (cls.equals(this.c.get(size).a)) {
                i = size;
                break;
            } else {
                size--;
            }
        }
        return i;
    }

    public final synchronized T a(Type type) {
        Object obj;
        obj = this.b.get(type);
        if (obj == null) {
            Class<?> b2 = bg.b(type);
            if (b2 != type) {
                obj = a((Type) b2);
            }
            if (obj == null) {
                Iterator<ap<Class<?>, T>> it = this.c.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    ap next = it.next();
                    if (((Class) next.a).isAssignableFrom(b2)) {
                        obj = next.b;
                        break;
                    }
                }
            }
        }
        return obj;
    }

    public final synchronized void a() {
        this.d = false;
    }

    public final synchronized void a(aq<InstanceCreator<?>> aqVar) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        for (Map.Entry next : aqVar.b.entrySet()) {
            if (!this.b.containsKey(next.getKey())) {
                a((Type) next.getKey(), next.getValue());
            }
        }
        for (int size = aqVar.c.size() - 1; size >= 0; size--) {
            ap apVar = aqVar.c.get(size);
            if (b((Class<?>) ((Class) apVar.a)) < 0) {
                a(apVar);
            }
        }
    }

    public final synchronized void a(Class<?> cls, T t) {
        a(new ap(cls, t));
    }

    public final synchronized void a(Type type, T t) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        if (b(type)) {
            a.log(Level.WARNING, "Overriding the existing type handler for {0}", type);
        }
        this.b.put(type, t);
    }

    public final synchronized aq<T> b() {
        aq<T> aqVar;
        aqVar = new aq<>();
        for (Map.Entry next : this.b.entrySet()) {
            aqVar.a((Type) next.getKey(), next.getValue());
        }
        for (ap<Class<?>, T> a2 : this.c) {
            aqVar.a(a2);
        }
        return aqVar;
    }

    public final synchronized void b(Type type, T t) {
        if (!this.d) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        } else if (!this.b.containsKey(type)) {
            a(type, t);
        }
    }

    public final synchronized boolean b(Type type) {
        return this.b.containsKey(type);
    }

    public final String toString() {
        boolean z;
        boolean z2;
        StringBuilder sb = new StringBuilder("{mapForTypeHierarchy:{");
        boolean z3 = true;
        for (ap next : this.c) {
            if (z3) {
                z2 = false;
            } else {
                sb.append(',');
                z2 = z3;
            }
            sb.append(bg.b((Type) next.a).getSimpleName()).append(':');
            sb.append((Object) next.b);
            z3 = z2;
        }
        sb.append("},map:{");
        boolean z4 = true;
        for (Map.Entry next2 : this.b.entrySet()) {
            if (z4) {
                z = false;
            } else {
                sb.append(',');
                z = z4;
            }
            sb.append(bg.b((Type) next2.getKey()).getSimpleName()).append(':');
            sb.append(next2.getValue());
            z4 = z;
        }
        sb.append("}");
        return sb.toString();
    }
}
