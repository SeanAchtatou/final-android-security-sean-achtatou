package com.admob.android.ads;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import com.admob.android.ads.AdView;
import com.admob.android.ads.j;
import com.admob.android.ads.q;
import com.wallpaperpuzzle.batman.db.WallpaperDbAdapter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdMobOpenerInfo */
public final class r implements n {
    public j.a a = j.a.CLICK_TO_BROWSER;
    public String b = "";
    public Vector<w> c = new Vector<>();
    public String d = null;
    public q.a e = q.a.ANY;
    public boolean f = false;
    public Point g = new Point(4, 4);
    public p h = null;
    public String i = null;
    public String j = null;
    public Bundle k = new Bundle();
    public boolean l = false;
    private boolean m = false;
    private Point n = new Point(0, 0);
    private String o = null;

    public final void a(String str, boolean z) {
        if (str != null && !"".equals(str)) {
            this.c.add(new w(str, z));
        }
    }

    public final Hashtable<String, Bitmap> b() {
        Set<String> keySet = this.k.keySet();
        Hashtable<String, Bitmap> hashtable = new Hashtable<>();
        for (String next : keySet) {
            Parcelable parcelable = this.k.getParcelable(next);
            if (parcelable instanceof Bitmap) {
                hashtable.put(next, (Bitmap) parcelable);
            }
        }
        return hashtable;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("a", this.a.toString());
        bundle.putString("t", this.b);
        bundle.putParcelableArrayList("c", AdView.a.a(this.c));
        bundle.putString("u", this.d);
        bundle.putInt("or", this.e.ordinal());
        bundle.putByte("tr", a(this.m));
        bundle.putByte("sc", a(this.f));
        bundle.putIntArray("cbo", a(this.g));
        bundle.putIntArray("cs", a(this.n));
        bundle.putBundle("mi", AdView.a.a(this.h));
        bundle.putString("su", this.i);
        bundle.putString("si", this.j);
        bundle.putString("json", this.o);
        bundle.putBundle("$", this.k);
        bundle.putByte("int", a(this.l));
        return bundle;
    }

    public final boolean a(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.a = j.a.a(bundle.getString("a"));
        this.b = bundle.getString("t");
        this.c = new Vector<>();
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("c");
        if (parcelableArrayList != null) {
            Iterator it = parcelableArrayList.iterator();
            while (it.hasNext()) {
                Bundle bundle2 = (Bundle) it.next();
                if (bundle2 != null) {
                    w wVar = new w();
                    wVar.a = bundle2.getString("u");
                    wVar.b = bundle2.getBoolean("p", false);
                    this.c.add(wVar);
                }
            }
        }
        this.d = bundle.getString("u");
        this.e = q.a.a(bundle.getInt("or"));
        this.m = a(bundle.getByte("tr"));
        this.f = a(bundle.getByte("sc"));
        this.g = a(bundle.getIntArray("cbo"));
        if (this.g == null) {
            this.g = new Point(4, 4);
        }
        this.n = a(bundle.getIntArray("cs"));
        p pVar = new p();
        if (pVar.a(bundle.getBundle("mi"))) {
            this.h = pVar;
        } else {
            this.h = null;
        }
        this.i = bundle.getString("su");
        this.j = bundle.getString("si");
        this.o = bundle.getString("json");
        this.k = bundle.getBundle("$");
        this.l = a(bundle.getByte("int"));
        return true;
    }

    public final void a(JSONObject jSONObject, u uVar, String str) {
        boolean z;
        this.a = j.a.a(jSONObject.optString("a"));
        a(jSONObject.optString("au"), true);
        a(jSONObject.optString("tu"), false);
        JSONObject optJSONObject = jSONObject.optJSONObject("stats");
        if (optJSONObject != null) {
            this.i = optJSONObject.optString("url");
            this.j = optJSONObject.optString(WallpaperDbAdapter.ITEM_ID);
        }
        String optString = jSONObject.optString("or");
        if (optString != null && !optString.equals("")) {
            if ("l".equals(optString)) {
                this.e = q.a.LANDSCAPE;
            } else {
                this.e = q.a.PORTRAIT;
            }
        }
        if (jSONObject.opt("t") != null) {
            z = true;
        } else {
            z = false;
        }
        this.m = z;
        this.b = jSONObject.optString("title");
        if (this.a == j.a.CLICK_TO_INTERACTIVE_VIDEO) {
            this.h = new p();
            JSONObject optJSONObject2 = jSONObject.optJSONObject("$");
            if (uVar != null) {
                try {
                    uVar.a(optJSONObject2, str);
                } catch (JSONException e2) {
                }
            }
            this.h.a = jSONObject.optString("u");
            this.h.b = jSONObject.optString("title");
            this.h.c = jSONObject.optInt("mc", 2);
            this.h.d = jSONObject.optInt("msm", 0);
            this.h.e = jSONObject.optString("stats");
            this.h.f = jSONObject.optString("splash");
            this.h.g = jSONObject.optDouble("splash_duration", 1.5d);
            this.h.h = jSONObject.optString("skip_down");
            this.h.i = jSONObject.optString("skip_up");
            this.h.j = jSONObject.optBoolean("no_splash_skip");
            this.h.k = jSONObject.optString("replay_down");
            this.h.l = jSONObject.optString("replay_up");
            JSONArray optJSONArray = jSONObject.optJSONArray("buttons");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    JSONObject optJSONObject3 = optJSONArray.optJSONObject(i2);
                    o oVar = new o();
                    oVar.a = optJSONObject3.optString("$");
                    oVar.b = optJSONObject3.optString("h");
                    oVar.c = optJSONObject3.optString("x");
                    oVar.e = optJSONObject3.optString("analytics_page_name");
                    oVar.d.a(optJSONObject3.optJSONObject("o"), uVar, str);
                    oVar.f = optJSONObject3.optJSONObject("o").toString();
                    this.h.m.add(oVar);
                }
            }
        }
        this.f = jSONObject.optInt("sc", 0) != 0;
        JSONArray optJSONArray2 = jSONObject.optJSONArray("co");
        if (optJSONArray2 != null && optJSONArray2.length() >= 2) {
            this.g = new Point(optJSONArray2.optInt(0), optJSONArray2.optInt(1));
        }
        this.o = jSONObject.toString();
    }

    private static int[] a(Point point) {
        if (point == null) {
            return null;
        }
        return new int[]{point.x, point.y};
    }

    private static Point a(int[] iArr) {
        if (iArr == null || iArr.length == 2) {
            return null;
        }
        return new Point(iArr[0], iArr[1]);
    }

    public static byte a(boolean z) {
        return z ? (byte) 1 : 0;
    }

    public static boolean a(byte b2) {
        return b2 == 1;
    }
}
