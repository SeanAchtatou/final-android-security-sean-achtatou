package com.wallpaperpuzzle.batman.audio;

import android.content.Context;
import com.wallpaperpuzzle.batman.audio.sound.Sound;
import com.wallpaperpuzzle.batman.audio.sound.SoundManager;

public class AudioManager {
    private static Context context = null;

    public static void init(Context context2) {
        if (context == null) {
            context = context2;
            SoundManager.getInstance().initSounds(context);
            SoundManager.getInstance().loadSounds();
        }
    }

    public static void playSound(Sound sound) {
        SoundManager.getInstance().playSound(sound);
    }

    public static void stopSound(Sound sound) {
        SoundManager.getInstance().stopSound(sound);
    }

    public static void stopAudio() {
        SoundManager.getInstance().stopSounds();
    }
}
