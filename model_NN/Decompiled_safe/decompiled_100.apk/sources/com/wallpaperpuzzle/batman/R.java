package com.wallpaperpuzzle.batman;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_bottom = 2130837505;
        public static final int background_puzzle_menu = 2130837506;
        public static final int background_top = 2130837507;
        public static final int button = 2130837508;
        public static final int button_menu = 2130837509;
        public static final int button_pressed = 2130837510;
        public static final int icon = 2130837511;
        public static final int image_1 = 2130837512;
        public static final int image_10 = 2130837513;
        public static final int image_11 = 2130837514;
        public static final int image_12 = 2130837515;
        public static final int image_13 = 2130837516;
        public static final int image_14 = 2130837517;
        public static final int image_15 = 2130837518;
        public static final int image_16 = 2130837519;
        public static final int image_17 = 2130837520;
        public static final int image_18 = 2130837521;
        public static final int image_19 = 2130837522;
        public static final int image_2 = 2130837523;
        public static final int image_20 = 2130837524;
        public static final int image_21 = 2130837525;
        public static final int image_22 = 2130837526;
        public static final int image_23 = 2130837527;
        public static final int image_24 = 2130837528;
        public static final int image_25 = 2130837529;
        public static final int image_26 = 2130837530;
        public static final int image_27 = 2130837531;
        public static final int image_28 = 2130837532;
        public static final int image_29 = 2130837533;
        public static final int image_3 = 2130837534;
        public static final int image_30 = 2130837535;
        public static final int image_31 = 2130837536;
        public static final int image_32 = 2130837537;
        public static final int image_33 = 2130837538;
        public static final int image_34 = 2130837539;
        public static final int image_35 = 2130837540;
        public static final int image_36 = 2130837541;
        public static final int image_37 = 2130837542;
        public static final int image_38 = 2130837543;
        public static final int image_39 = 2130837544;
        public static final int image_4 = 2130837545;
        public static final int image_40 = 2130837546;
        public static final int image_41 = 2130837547;
        public static final int image_42 = 2130837548;
        public static final int image_43 = 2130837549;
        public static final int image_44 = 2130837550;
        public static final int image_45 = 2130837551;
        public static final int image_46 = 2130837552;
        public static final int image_47 = 2130837553;
        public static final int image_48 = 2130837554;
        public static final int image_49 = 2130837555;
        public static final int image_5 = 2130837556;
        public static final int image_50 = 2130837557;
        public static final int image_6 = 2130837558;
        public static final int image_7 = 2130837559;
        public static final int image_8 = 2130837560;
        public static final int image_9 = 2130837561;
        public static final int sound_off = 2130837562;
        public static final int sound_on = 2130837563;
    }

    public static final class id {
        public static final int ad = 2131165184;
        public static final int bottom_bgr = 2131165198;
        public static final int gallery = 2131165192;
        public static final int image = 2131165186;
        public static final int layout_root = 2131165193;
        public static final int menu = 2131165195;
        public static final int next = 2131165197;
        public static final int peek = 2131165196;
        public static final int play = 2131165189;
        public static final int puzzle = 2131165194;
        public static final int puzzleLevel = 2131165187;
        public static final int root = 2131165185;
        public static final int savesd = 2131165191;
        public static final int sound = 2131165188;
        public static final int wallpaper = 2131165190;
    }

    public static final class layout {
        public static final int ads = 2130903040;
        public static final int list_game_type = 2130903041;
        public static final int menu = 2130903042;
        public static final int peek = 2130903043;
        public static final int puzzle = 2130903044;
    }

    public static final class raw {
        public static final int levelcompleted = 2130968576;
        public static final int pressbutton = 2130968577;
        public static final int startpuzzle = 2130968578;
        public static final int switchpuzzle = 2130968579;
    }

    public static final class string {
        public static final int app_loading = 2131099650;
        public static final int app_name = 2131099649;
        public static final int app_publisher = 2131099648;
        public static final int app_quit = 2131099651;
        public static final int app_quit_text = 2131099652;
        public static final int dialog_apps = 2131099661;
        public static final int dialog_no = 2131099660;
        public static final int dialog_ok = 2131099662;
        public static final int dialog_yes = 2131099659;
        public static final int puzzle_level = 2131099670;
        public static final int savesd_loading = 2131099656;
        public static final int savesd_nosaved = 2131099658;
        public static final int savesd_saved = 2131099657;
        public static final int wallpaper_action_gallery = 2131099664;
        public static final int wallpaper_action_next = 2131099666;
        public static final int wallpaper_action_peek = 2131099665;
        public static final int wallpaper_action_play = 2131099669;
        public static final int wallpaper_action_savesd = 2131099667;
        public static final int wallpaper_action_wallpaper = 2131099668;
        public static final int wallpaper_choose_set = 2131099663;
        public static final int wallpaper_dialog = 2131099653;
        public static final int wallpaper_loading = 2131099654;
        public static final int wallpaper_set = 2131099655;
        /* added by JADX */

        /* renamed from: app.publisher  reason: not valid java name */
        public static final int f0apppublisher = 2131099648;
        /* added by JADX */

        /* renamed from: app.name  reason: not valid java name */
        public static final int f1appname = 2131099649;
        /* added by JADX */

        /* renamed from: app.loading  reason: not valid java name */
        public static final int f2apploading = 2131099650;
        /* added by JADX */

        /* renamed from: app.quit  reason: not valid java name */
        public static final int f3appquit = 2131099651;
        /* added by JADX */

        /* renamed from: app.quit.text  reason: not valid java name */
        public static final int f4appquittext = 2131099652;
        /* added by JADX */

        /* renamed from: wallpaper.dialog  reason: not valid java name */
        public static final int f5wallpaperdialog = 2131099653;
        /* added by JADX */

        /* renamed from: wallpaper.loading  reason: not valid java name */
        public static final int f6wallpaperloading = 2131099654;
        /* added by JADX */

        /* renamed from: wallpaper.set  reason: not valid java name */
        public static final int f7wallpaperset = 2131099655;
        /* added by JADX */

        /* renamed from: savesd.loading  reason: not valid java name */
        public static final int f8savesdloading = 2131099656;
        /* added by JADX */

        /* renamed from: savesd.saved  reason: not valid java name */
        public static final int f9savesdsaved = 2131099657;
        /* added by JADX */

        /* renamed from: savesd.nosaved  reason: not valid java name */
        public static final int f10savesdnosaved = 2131099658;
        /* added by JADX */

        /* renamed from: dialog.yes  reason: not valid java name */
        public static final int f11dialogyes = 2131099659;
        /* added by JADX */

        /* renamed from: dialog.no  reason: not valid java name */
        public static final int f12dialogno = 2131099660;
        /* added by JADX */

        /* renamed from: dialog.apps  reason: not valid java name */
        public static final int f13dialogapps = 2131099661;
        /* added by JADX */

        /* renamed from: dialog.ok  reason: not valid java name */
        public static final int f14dialogok = 2131099662;
        /* added by JADX */

        /* renamed from: wallpaper.choose.set  reason: not valid java name */
        public static final int f15wallpaperchooseset = 2131099663;
        /* added by JADX */

        /* renamed from: wallpaper.action.gallery  reason: not valid java name */
        public static final int f16wallpaperactiongallery = 2131099664;
        /* added by JADX */

        /* renamed from: wallpaper.action.peek  reason: not valid java name */
        public static final int f17wallpaperactionpeek = 2131099665;
        /* added by JADX */

        /* renamed from: wallpaper.action.next  reason: not valid java name */
        public static final int f18wallpaperactionnext = 2131099666;
        /* added by JADX */

        /* renamed from: wallpaper.action.savesd  reason: not valid java name */
        public static final int f19wallpaperactionsavesd = 2131099667;
        /* added by JADX */

        /* renamed from: wallpaper.action.wallpaper  reason: not valid java name */
        public static final int f20wallpaperactionwallpaper = 2131099668;
        /* added by JADX */

        /* renamed from: wallpaper.action.play  reason: not valid java name */
        public static final int f21wallpaperactionplay = 2131099669;
        /* added by JADX */

        /* renamed from: puzzle.level  reason: not valid java name */
        public static final int f22puzzlelevel = 2131099670;
    }

    public static final class style {
        public static final int FullHeightDialog = 2131034112;
    }

    public static final class styleable {
        public static final int[] MenuTheme = {16842828};
        public static final int MenuTheme_android_galleryItemBackground = 0;
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
