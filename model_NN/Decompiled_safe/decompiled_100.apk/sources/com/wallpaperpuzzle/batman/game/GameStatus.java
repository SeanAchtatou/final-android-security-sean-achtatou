package com.wallpaperpuzzle.batman.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
