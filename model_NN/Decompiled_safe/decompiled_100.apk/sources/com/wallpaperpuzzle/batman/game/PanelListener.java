package com.wallpaperpuzzle.batman.game;

public interface PanelListener {
    void onComplete();

    void onNext();
}
