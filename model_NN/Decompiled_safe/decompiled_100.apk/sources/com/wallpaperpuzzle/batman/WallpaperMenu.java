package com.wallpaperpuzzle.batman;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import com.wallpaperpuzzle.batman.audio.AudioManager;
import com.wallpaperpuzzle.batman.audio.sound.Sound;
import com.wallpaperpuzzle.batman.db.WallpaperDbManager;
import com.wallpaperpuzzle.batman.game.ResourceLauncher;
import com.wallpaperpuzzle.batman.utils.GlobalUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class WallpaperMenu extends Activity {
    public static String FILE_TYPE = ".png";
    public static final String ITEM_POSITION = "itemPosition";
    private static final String TAG = "WallpaperMenu";
    public static ProgressDialog mProgressDialog;
    public static AlertDialog mWallpaperDialog;
    /* access modifiers changed from: private */
    public ImageView imageView;
    /* access modifiers changed from: private */
    public Boolean isSaved;
    /* access modifiers changed from: private */
    public int itemPosition = 0;
    private Button mSaveSdButton;
    private Button mWallpaperButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.menu);
        setVolumeControlStream(3);
        GlobalUtils.init(this);
        if (ResourceLauncher.getBackgroundMenu() == null) {
            ResourceLauncher.setBackgroundMenu(new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.background)));
        }
        ((LinearLayout) findViewById(R.id.root)).setBackgroundDrawable(ResourceLauncher.getBackgroundMenu());
        Bundle extras = getIntent().getExtras();
        this.itemPosition = extras != null ? extras.getInt(ITEM_POSITION) : 0;
        initCreation();
    }

    private void initCreation() {
        this.imageView = (ImageView) findViewById(R.id.image);
        ImageLevelAdapter imageLevelAdapter = ImageLevelAdapter.getImageLevelAdapter();
        WallpaperPuzzle.updatePuzzleLevel(this, this.itemPosition);
        initButtons();
        if (imageLevelAdapter != null) {
            initWallpaperDialog();
            if (imageLevelAdapter.getCount() != 0) {
                this.imageView.setImageBitmap(getImageBitmap());
                this.imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        WallpaperMenu.this.startPuzzle();
                    }
                });
            }
            Gallery g = (Gallery) findViewById(R.id.gallery);
            g.setAdapter((SpinnerAdapter) imageLevelAdapter);
            g.setSelection(this.itemPosition);
            g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == WallpaperMenu.this.itemPosition) {
                        WallpaperMenu.this.startPuzzle();
                    }
                }
            });
            g.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    ResourceLauncher.invalidateResources();
                    if (position != WallpaperMenu.this.itemPosition) {
                        int unused = WallpaperMenu.this.itemPosition = position;
                        WallpaperMenu.this.imageView.setImageBitmap(WallpaperMenu.this.getImageBitmap());
                        WallpaperPuzzle.updatePuzzleLevel(WallpaperMenu.this, WallpaperMenu.this.itemPosition);
                        WallpaperMenu.this.updateButtons();
                    }
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void startPuzzle() {
        AudioManager.playSound(Sound.pressbutton);
        ResourceLauncher.invalidateResources();
        Intent intent = new Intent();
        intent.putExtra(ITEM_POSITION, this.itemPosition);
        GlobalUtils.startActivity(this, WallpaperPuzzle.class, intent);
    }

    /* access modifiers changed from: private */
    public Bitmap getImageBitmap() {
        return ImageLevelAdapter.getBitmapByPosition(this.itemPosition);
    }

    public void initWallpaperDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.f15wallpaperchooseset);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage((int) R.string.f5wallpaperdialog).setCancelable(true).setPositiveButton((int) R.string.f11dialogyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AudioManager.playSound(Sound.pressbutton);
                final Bitmap bitmap = ImageLevelAdapter.getBitmapByPosition(WallpaperMenu.this.itemPosition);
                WallpaperMenu.mProgressDialog = ProgressDialog.show(WallpaperMenu.this, "", WallpaperMenu.this.getString(R.string.f6wallpaperloading), true);
                dialog.dismiss();
                final AsyncTask asyncTask = new AsyncTask() {
                    /* access modifiers changed from: protected */
                    public Object doInBackground(Object... objects) {
                        return null;
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Object result) {
                        if (WallpaperMenu.mProgressDialog.isShowing()) {
                            WallpaperMenu.mProgressDialog.dismiss();
                            Toast.makeText(WallpaperMenu.this, WallpaperMenu.this.getString(R.string.f7wallpaperset), 0).show();
                        }
                    }
                };
                new Thread() {
                    public void run() {
                        try {
                            WallpaperMenu.this.getApplicationContext().setWallpaper(bitmap);
                            asyncTask.execute(new Object[0]);
                        } catch (Exception e) {
                            Log.e(WallpaperMenu.TAG, e.getMessage());
                        }
                    }
                }.start();
            }
        }).setNegativeButton((int) R.string.f12dialogno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AudioManager.playSound(Sound.pressbutton);
                dialog.cancel();
            }
        });
        mWallpaperDialog = builder.create();
    }

    private void initButtons() {
        ((Button) findViewById(R.id.play)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WallpaperMenu.this.startPuzzle();
            }
        });
        this.mWallpaperButton = (Button) findViewById(R.id.wallpaper);
        this.mWallpaperButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AudioManager.playSound(Sound.pressbutton);
                WallpaperMenu.mWallpaperDialog.show();
            }
        });
        this.mSaveSdButton = (Button) findViewById(R.id.savesd);
        this.mSaveSdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Boolean unused = WallpaperMenu.this.isSaved = null;
                WallpaperMenu.mProgressDialog = ProgressDialog.show(WallpaperMenu.this, "", WallpaperMenu.this.getString(R.string.f8savesdloading), true);
                WallpaperMenu.mProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialogInterface) {
                        new Thread() {
                            public void run() {
                                synchronized (WallpaperMenu.this) {
                                    if (WallpaperMenu.this.isSaved == null) {
                                        try {
                                            WallpaperMenu.this.wait();
                                        } catch (InterruptedException e) {
                                            Log.e(WallpaperMenu.TAG, e.getMessage());
                                        }
                                    }
                                }
                                Handler h = new Handler(WallpaperMenu.this.getMainLooper());
                                if (WallpaperMenu.this.isSaved.booleanValue()) {
                                    h.post(new Runnable() {
                                        public void run() {
                                            CharSequence appName = WallpaperMenu.this.getString(R.string.f1appname);
                                            Toast.makeText(WallpaperMenu.this, WallpaperMenu.this.getString(R.string.f9savesdsaved, new Object[]{appName}), 1).show();
                                        }
                                    });
                                } else {
                                    h.post(new Runnable() {
                                        public void run() {
                                            Toast.makeText(WallpaperMenu.this, WallpaperMenu.this.getText(R.string.f10savesdnosaved), 1).show();
                                        }
                                    });
                                }
                            }
                        }.start();
                    }
                });
                new Thread() {
                    public void run() {
                        synchronized (WallpaperMenu.this) {
                            try {
                                AudioManager.playSound(Sound.pressbutton);
                                File fileDir = new File(Environment.getExternalStorageDirectory(), WallpaperMenu.this.getString(R.string.f1appname));
                                fileDir.mkdirs();
                                File file = new File(fileDir, ImageLevelAdapter.IMAGE + (WallpaperMenu.this.itemPosition + 1) + WallpaperMenu.FILE_TYPE);
                                Bitmap bm = ImageLevelAdapter.getBitmapByPosition(WallpaperMenu.this.itemPosition);
                                try {
                                    OutputStream outStream = new FileOutputStream(file);
                                    bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                                    outStream.flush();
                                    outStream.close();
                                    Boolean unused = WallpaperMenu.this.isSaved = true;
                                } catch (Exception e) {
                                    Exception e2 = e;
                                    Boolean unused2 = WallpaperMenu.this.isSaved = false;
                                    Log.e(WallpaperMenu.TAG, e2.getMessage());
                                }
                                WallpaperMenu.mProgressDialog.dismiss();
                                WallpaperMenu.this.notify();
                            } catch (Exception e3) {
                                Log.e(WallpaperMenu.TAG, e3.getMessage());
                            }
                        }
                    }
                }.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateButtons() {
        if (WallpaperDbManager.getItemById(this.itemPosition) != null) {
            this.mWallpaperButton.setVisibility(0);
            this.mSaveSdButton.setVisibility(0);
            return;
        }
        this.mWallpaperButton.setVisibility(4);
        this.mSaveSdButton.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ITEM_POSITION, this.itemPosition);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        this.itemPosition = ((Integer) inState.get(ITEM_POSITION)).intValue();
        if (this.imageView != null) {
            this.imageView.setImageBitmap(getImageBitmap());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        updateButtons();
        WallpaperPuzzle.initSound(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }
}
