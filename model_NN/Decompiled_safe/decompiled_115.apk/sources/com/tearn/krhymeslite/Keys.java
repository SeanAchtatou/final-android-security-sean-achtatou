package com.tearn.krhymeslite;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Keys extends Activity {
    private static final int MENU_BLOG = 1;
    private static final int MENU_HOME = 0;
    private static final int MENU_KEYS = 2;
    private static final int MENU_QUIT = 3;
    private String kapp;
    private String kpack;
    private String target;
    private WebView webview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(MENU_KEYS);
        setContentView((int) R.layout.main);
        this.webview = (WebView) findViewById(R.id.webview);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.setWebViewClient(new KeysClient(this, null));
        this.kapp = getString(R.string.app);
        this.kpack = getString(R.string.pack);
        this.target = "http://r.tearn.com" + this.kpack + "/pack.html?k=" + this.kapp;
        setProgressBarVisibility(true);
        this.webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                this.setProgress(progress * 100);
            }
        });
        this.webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(this, "Oh no! " + description, (int) Keys.MENU_HOME).show();
            }
        });
        this.webview.loadUrl(this.target);
    }

    private class KeysClient extends WebViewClient {
        private KeysClient() {
        }

        /* synthetic */ KeysClient(Keys keys, KeysClient keysClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.webview.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.webview.goBack();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add((int) MENU_HOME, (int) MENU_HOME, (int) MENU_HOME, "Home");
        menu.add((int) MENU_HOME, (int) MENU_BLOG, (int) MENU_HOME, "Blog");
        menu.add((int) MENU_HOME, (int) MENU_KEYS, (int) MENU_HOME, "Keys");
        menu.add((int) MENU_HOME, (int) MENU_QUIT, (int) MENU_HOME, "Tablet");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_HOME /*0*/:
                this.webview.loadUrl(this.target);
                return MENU_BLOG;
            case MENU_BLOG /*1*/:
                this.webview.loadUrl("http://blog.tearn.com/");
                return MENU_BLOG;
            case MENU_KEYS /*2*/:
                this.webview.loadUrl("http://keys.tearn.com/");
                return MENU_BLOG;
            case MENU_QUIT /*3*/:
                this.webview.loadUrl("http://the.tearn.com/2010/06/pack.html?k=" + this.kapp);
                return MENU_BLOG;
            default:
                return false;
        }
    }
}
