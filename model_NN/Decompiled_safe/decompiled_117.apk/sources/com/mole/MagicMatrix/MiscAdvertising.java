package com.mole.MagicMatrix;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MiscAdvertising {
    public static void configureAdMob(Activity activity) {
        AdView adView = new AdView(activity, AdSize.BANNER, activity.getString(R.string.AD_UNIT_PUBLISHER_ID));
        LinearLayout layout = (LinearLayout) activity.findViewById(R.id.bannerLayout);
        if (layout == null) {
            Misc.ShowMessage(activity.getApplicationContext(), "bannerLayout for advertising not found");
            return;
        }
        layout.addView(adView);
        AdRequest adreq = new AdRequest();
        if (activity instanceof AdListener) {
            adView.setAdListener((AdListener) activity);
        }
        adView.loadAd(adreq);
    }

    public static void configureAdWhirl(Activity activity) {
        LinearLayout layout = (LinearLayout) activity.findViewById(R.id.bannerLayout);
        if (layout == null) {
            Misc.ShowMessage(activity.getApplicationContext(), "bannerLayout for advertising not found");
            return;
        }
        AdWhirlManager.setConfigExpireTimeout(300000);
        int density = (int) activity.getResources().getDisplayMetrics().density;
        AdWhirlLayout adWhirlLayout2 = new AdWhirlLayout(activity, activity.getString(R.string.ADWHIRL_PUBLISHER_KEY));
        if (activity instanceof AdWhirlLayout.AdWhirlInterface) {
            adWhirlLayout2.setAdWhirlInterface((AdWhirlLayout.AdWhirlInterface) activity);
        }
        adWhirlLayout2.setMaxWidth(320 * density);
        adWhirlLayout2.setMaxHeight(52 * density);
        layout.addView(adWhirlLayout2, new RelativeLayout.LayoutParams(320, 52));
        layout.invalidate();
    }
}
