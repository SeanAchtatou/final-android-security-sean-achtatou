package com.mole.MagicMatrix;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class SpeechHelper {
    final int SPEECH_REQCODE = 3;
    public boolean canSpeak = false;
    public TextToSpeech mTts = null;
    public boolean positivemotivation = false;
    String[] praisetexts;
    Random random;
    public long speaktime = 0;

    public boolean waittimedone(long t) {
        if (this.mTts == null) {
            return true;
        }
        if (this.mTts.isSpeaking()) {
            return false;
        }
        if (new Date().getTime() - this.speaktime < t) {
            return false;
        }
        return true;
    }

    public SpeechHelper(Context context) {
        this.praisetexts = context.getResources().getStringArray(R.array.praisetexts);
        this.random = new Random();
    }

    public boolean speakIt(String mes) {
        if (this.canSpeak) {
            this.mTts.speak(mes, 0, null);
            this.speaktime = new Date().getTime();
        }
        return this.canSpeak;
    }

    public boolean speakItIfWaittimeDone(String mes, long t) {
        if (!this.canSpeak || !waittimedone(t)) {
            return false;
        }
        return speakIt(mes);
    }

    public void praise(long t) {
        if (this.positivemotivation) {
            speakItIfWaittimeDone(this.praisetexts[this.random.nextInt(this.praisetexts.length)], ((long) (this.random.nextInt(10) * 1000)) + t);
        }
    }

    public void shutdownSpeech() {
        if (this.mTts != null) {
            this.mTts.shutdown();
        }
        this.mTts = null;
        this.canSpeak = false;
    }

    public void stopSpeak() {
        if (this.mTts != null) {
            this.mTts.stop();
        }
    }

    public void startupSpeech(Activity activity) {
        if (this.mTts == null) {
            Intent checkIntent = new Intent();
            checkIntent.setAction("android.speech.tts.engine.CHECK_TTS_DATA");
            activity.startActivityForResult(checkIntent, 3);
            return;
        }
        iniCanSpeak();
    }

    public void returnRequestTTS(int resultCode, Activity activity) {
        if (resultCode == 1) {
            this.mTts = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
                public void onInit(int status) {
                    if (status == 0) {
                        SpeechHelper.this.iniCanSpeak();
                    }
                }
            });
            return;
        }
        Intent installIntent = new Intent();
        installIntent.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
        activity.startActivity(installIntent);
    }

    /* access modifiers changed from: private */
    public void iniCanSpeak() {
        if (this.mTts == null) {
            this.canSpeak = false;
            return;
        }
        int result = this.mTts.setLanguage(Locale.getDefault());
        this.canSpeak = (result == -1 || result == -2) ? false : true;
    }
}
