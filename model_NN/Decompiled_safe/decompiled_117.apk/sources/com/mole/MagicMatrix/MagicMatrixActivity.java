package com.mole.MagicMatrix;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import java.lang.reflect.Array;
import java.util.Date;
import java.util.Random;

public class MagicMatrixActivity extends Activity implements AdListener, TextToSpeech.OnInitListener, AdWhirlLayout.AdWhirlInterface {
    final int EINSTELLUNGEN_ID = 3;
    final int EINSTELLUNGEN_REQCODE = 1;
    final int HELP_ID = 2;
    final int HELP_REQCODE = 2;
    final int INFO_ID = 1;
    final int MAXMENUITEM_ID = 5;
    final int SCORES_ID = 4;
    final int SPEECH_REQCODE = 3;
    Button bt;
    int fieldsize;
    int gamestate = 0;
    int groundsize = 6;
    LayoutInflater mInflater;
    View.OnClickListener onbtclk = new View.OnClickListener() {
        public void onClick(View v) {
            MagicMatrixActivity.this.startNew();
        }
    };
    int[][] playground = ((int[][]) Array.newInstance(Integer.TYPE, 6, 6));
    /* access modifiers changed from: private */
    public SharedPreferences preferences;
    boolean principalspeech = false;
    Random random;
    ViewHolderRow[] rows = new ViewHolderRow[6];
    /* access modifiers changed from: private */
    public SpeechHelper speech = null;
    private long starttime;
    int symbolcnt = 6;
    TableLayout tl;
    TextView tv;

    class ViewHolderSingle {
        ImageView iv;
        View.OnClickListener oncllst = new View.OnClickListener() {
            public void onClick(View v) {
                ViewHolderSingle vhs = (ViewHolderSingle) v.getTag();
                MagicMatrixActivity.this.draw(vhs.x, vhs.y, true);
                MagicMatrixActivity.this.speech.praise(15000);
                if (MagicMatrixActivity.this.checkwin()) {
                    MagicMatrixActivity.this.won();
                }
            }
        };
        Animation rotate;
        int x;
        int y;

        public ViewHolderSingle(View v, int thex, int they) {
            this.iv = (ImageView) v;
            v.setMinimumHeight(MagicMatrixActivity.this.fieldsize);
            v.setMinimumWidth(MagicMatrixActivity.this.fieldsize);
            this.iv.setOnClickListener(this.oncllst);
            this.x = thex;
            this.y = they;
            this.iv.setTag(this);
            this.rotate = AnimationUtils.loadAnimation(MagicMatrixActivity.this.getApplicationContext(), R.anim.rotate);
        }
    }

    class ViewHolderRow {
        TableRow tr;
        ViewHolderSingle[] vh = new ViewHolderSingle[6];

        public ViewHolderRow(View parentView, int y) {
            this.tr = (TableRow) parentView;
            this.vh[0] = new ViewHolderSingle(parentView.findViewById(R.id.imageView1), 0, y);
            this.vh[1] = new ViewHolderSingle(parentView.findViewById(R.id.imageView2), 1, y);
            this.vh[2] = new ViewHolderSingle(parentView.findViewById(R.id.imageView3), 2, y);
            this.vh[3] = new ViewHolderSingle(parentView.findViewById(R.id.imageView4), 3, y);
            this.vh[4] = new ViewHolderSingle(parentView.findViewById(R.id.imageView5), 4, y);
            this.vh[5] = new ViewHolderSingle(parentView.findViewById(R.id.imageView6), 5, y);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iniPrincipals();
        requestWindowFeature(3);
        this.mInflater = getLayoutInflater();
        this.speech = new SpeechHelper(this);
        iniEinstellungen();
        setContentView((int) R.layout.main);
        getWindow().setFeatureDrawableResource(3, R.drawable.magicmatrix3);
        if (!Misc.debugmode) {
            MiscAdvertising.configureAdMob(this);
        }
        this.fieldsize = calFieldSize();
        iniImages();
        iniButton();
        if (!iniLastNonConfigurationInstance()) {
            startNew();
        }
        checkfirstrun();
        checkEvaluationQuestion();
    }

    private void iniPrincipals() {
        this.principalspeech = getResources().getBoolean(R.bool.principalspeech);
    }

    public Object onRetainNonConfigurationInstance() {
        Misc.ShowDebugMessage(getApplicationContext(), "onRetainNonConfigurationInstance");
        String code = "";
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                code = String.valueOf(code) + String.format("%d", Integer.valueOf(this.playground[x][y]));
            }
        }
        return code;
    }

    private boolean iniLastNonConfigurationInstance() {
        int i;
        Object data = getLastNonConfigurationInstance();
        Misc.ShowDebugMessage(getApplicationContext(), "getLastNonConfigurationInstance");
        if (data == null) {
            return false;
        }
        if (data instanceof String) {
            String code = (String) data;
            int i2 = 0;
            int x = 0;
            while (x < 6) {
                int y = 0;
                int i3 = i2;
                while (y < 6) {
                    if (i3 < code.length()) {
                        i = i3 + 1;
                        this.playground[x][y] = Integer.parseInt(new StringBuilder(String.valueOf(code.charAt(i3))).toString());
                    } else {
                        i = i3;
                    }
                    y++;
                    i3 = i;
                }
                x++;
                i2 = i3;
            }
            displayPlayground();
        }
        return true;
    }

    private void checkEvaluationQuestion() {
        int cntusagedays = this.preferences.getInt("cntusagedays", 0);
        int askafterdays = this.preferences.getInt("askafterdays", 4);
        if (!this.preferences.getBoolean("askedforeval", false) && cntusagedays >= askafterdays) {
            this.preferences.edit().putInt("askafterdays", askafterdays + 7).commit();
            Misc.YesDialog(this, R.string.do_you_want_to_rate_this_app_, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    MagicMatrixActivity.this.preferences.edit().putBoolean("askedforeval", true).commit();
                    Misc.showEvaluationSupport(MagicMatrixActivity.this.getApplicationContext());
                }
            });
        }
    }

    private void checkUsageTime() {
        long timediff = new Date().getTime() - this.starttime;
        long timeoflastusage = this.preferences.getLong("timeoflastusage", -1);
        if (timediff > 30000 && timeoflastusage < this.starttime - 86400000) {
            this.preferences.edit().putLong("timeoflastusage", this.starttime).commit();
            this.preferences.edit().putInt("cntusagedays", this.preferences.getInt("cntusagedays", 0) + 1).commit();
        }
    }

    private void checkfirstrun() {
        if (this.preferences.getBoolean("veryfirststart", true)) {
            if (this.principalspeech && !this.preferences.getBoolean("chusespeech", false)) {
                Misc.YesDialog(this, R.string.do_you_want_this_app_to_use_speech_, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MagicMatrixActivity.this.preferences.edit().putBoolean("chusespeech", true).commit();
                        MagicMatrixActivity.this.iniEinstellungen();
                    }
                }, null);
            }
            this.preferences.edit().putBoolean("veryfirststart", false).commit();
        }
    }

    /* access modifiers changed from: private */
    public void iniEinstellungen() {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.symbolcnt = Integer.parseInt(this.preferences.getString("numberofsymbols", getString(R.string.def_numberofsymbols)));
        this.groundsize = Integer.parseInt(this.preferences.getString("boardsizes", getString(R.string.def_boardsize)));
        if (this.principalspeech) {
            if (this.preferences.getBoolean("chusespeech", false)) {
                this.speech.startupSpeech(this);
            } else {
                this.speech.shutdownSpeech();
            }
            this.speech.positivemotivation = this.preferences.getBoolean("positivemotivation", false);
        }
    }

    private void iniCode() {
        boolean z;
        this.fieldsize = calFieldSize();
        this.random = new Random();
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                this.playground[x][y] = 0;
            }
        }
        displayPlayground();
        for (int j = 0; j < this.random.nextInt(15) + 10; j++) {
            draw(this.random.nextInt(this.groundsize), this.random.nextInt(this.groundsize), true);
        }
        for (int y2 = 0; y2 < 6; y2++) {
            if (y2 >= this.groundsize) {
                this.rows[y2].tr.setVisibility(8);
            } else {
                this.rows[y2].tr.setVisibility(0);
            }
        }
        for (int x2 = 0; x2 < 6; x2++) {
            TableLayout tableLayout = this.tl;
            if (x2 >= this.groundsize) {
                z = true;
            } else {
                z = false;
            }
            tableLayout.setColumnCollapsed(x2, z);
        }
        this.tl.requestLayout();
        findViewById(R.id.relativeLayout2).requestLayout();
    }

    private void displayPlayground() {
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 6; y++) {
                ImageView iv = this.rows[y].vh[x].iv;
                iv.setImageLevel(this.playground[x][y]);
                if (x >= this.groundsize || y >= this.groundsize) {
                    this.rows[y].vh[x].rotate.reset();
                    iv.setVisibility(8);
                } else {
                    iv.setVisibility(0);
                    iv.setMinimumHeight(this.fieldsize);
                    iv.setMinimumWidth(this.fieldsize);
                }
            }
        }
    }

    private int calFieldSize() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int xwidth = metrics.widthPixels / this.groundsize;
        int yheight = ((metrics.heightPixels - 180) - 80) / this.groundsize;
        if (xwidth > yheight) {
            return yheight;
        }
        return xwidth;
    }

    private void incmod(int x, int y, boolean display) {
        this.playground[x][y] = (this.playground[x][y] + 1) % this.symbolcnt;
        this.rows[y].vh[x].iv.setImageLevel(this.playground[x][y]);
        if (x < this.groundsize && y < this.groundsize) {
            this.rows[y].vh[x].rotate.reset();
            this.rows[y].vh[x].rotate.setRepeatCount(0);
            this.rows[y].vh[x].iv.startAnimation(this.rows[y].vh[x].rotate);
        }
    }

    /* access modifiers changed from: private */
    public void draw(int x, int y, boolean display) {
        incmod(x, y, display);
        if (x > 0) {
            incmod(x - 1, y, display);
        }
        if (x < this.groundsize - 1) {
            incmod(x + 1, y, display);
        }
        if (y > 0) {
            incmod(x, y - 1, display);
        }
        if (y < this.groundsize - 1) {
            incmod(x, y + 1, display);
        }
    }

    /* access modifiers changed from: private */
    public boolean checkwin() {
        int symb = this.playground[0][0];
        for (int x = 0; x < this.groundsize; x++) {
            for (int y = 0; y < this.groundsize; y++) {
                if (this.playground[x][y] != symb) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void won() {
        this.gamestate = 1;
        this.preferences.edit().putInt("cntwins", this.preferences.getInt("cntwins", 0) + 1).commit();
        this.tl.setBackgroundColor(-16711936);
        this.tv.setText((int) R.string.won);
        this.bt.setText((int) R.string.newtext);
        this.speech.speakIt(getString(R.string.won));
    }

    /* access modifiers changed from: private */
    public void startNew() {
        this.gamestate = 0;
        this.tl.setBackgroundColor(0);
        this.tv.setText("");
        this.bt.setText((int) R.string.newtext);
        iniCode();
    }

    private void iniButton() {
        this.tv = (TextView) findViewById(R.id.textView1);
        this.bt = (Button) findViewById(R.id.button1);
        this.bt.setOnClickListener(this.onbtclk);
        this.bt.setText((int) R.string.newtext);
    }

    private void iniImages() {
        this.fieldsize = calFieldSize();
        this.tl = (TableLayout) findViewById(R.id.tableLayout1);
        this.rows[0] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow1), 0);
        this.rows[1] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow2), 1);
        this.rows[2] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow3), 2);
        this.rows[3] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow4), 3);
        this.rows[4] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow5), 4);
        this.rows[5] = new ViewHolderRow((TableRow) findViewById(R.id.tableRow6), 5);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(262144, 3, 0, (int) R.string.einstellungen).setIcon(17301577);
        menu.add(262144, 4, 0, (int) R.string.scores).setIcon(17301578);
        menu.add(262144, 2, 0, (int) R.string.hilf).setIcon(17301568);
        menu.add(262144, 1, 0, (int) R.string.info).setIcon(17301569);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Misc.InfoDialog(this, R.string.info, R.array.infotext);
                break;
            case 2:
                Misc.OkDialog(this, getString(R.string.hilf), String.valueOf(getString(R.string.help)) + "\n");
                break;
            case AdWhirlUtil.NETWORK_TYPE_VIDEOEGG:
                startActivityForResult(new Intent(this, Einstellungen.class), 1);
                break;
            case AdWhirlUtil.NETWORK_TYPE_MEDIALETS:
                Misc.OkDialog(this, getString(R.string.scores), String.valueOf(String.format(getString(R.string.you_solved_d_puzzles), Integer.valueOf(this.preferences.getInt("cntwins", 0)))) + "\n");
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                Misc.ShowDebugMessage(getApplicationContext(), getString(R.string.neue_einstellungen));
                iniEinstellungen();
                return;
            case 2:
            default:
                return;
            case AdWhirlUtil.NETWORK_TYPE_VIDEOEGG:
                this.speech.returnRequestTTS(resultCode, this);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        checkUsageTime();
        this.speech.stopSpeak();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.starttime = new Date().getTime();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Misc.ShowDebugMessage(getApplicationContext(), "onDestroy");
        this.speech.shutdownSpeech();
        this.speech = null;
    }

    public void adWhirlGeneric() {
    }

    public void onInit(int arg0) {
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
    }
}
