package com.mole.MagicMatrix;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Einstellungen extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
