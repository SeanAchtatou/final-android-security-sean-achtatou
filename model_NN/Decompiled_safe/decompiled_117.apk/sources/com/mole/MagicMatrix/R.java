package com.mole.MagicMatrix;

public final class R {

    public static final class anim {
        public static final int rotate = 2130968576;
    }

    public static final class array {
        public static final int captionsBoardSize = 2131230723;
        public static final int captionsNumberOfSymbols = 2131230725;
        public static final int eula = 2131230722;
        public static final int infotext = 2131230721;
        public static final int praisetexts = 2131230720;
        public static final int valuesBoardSize = 2131230724;
        public static final int valuesNumberOfSymbols = 2131230726;
    }

    public static final class attr {
    }

    public static final class bool {
        public static final int def_praise = 2131165186;
        public static final int def_usespeech = 2131165185;
        public static final int principalspeech = 2131165184;
    }

    public static final class drawable {
        public static final int code1 = 2130837504;
        public static final int code2 = 2130837505;
        public static final int code3 = 2130837506;
        public static final int code4 = 2130837507;
        public static final int code5 = 2130837508;
        public static final int code6 = 2130837509;
        public static final int code7 = 2130837510;
        public static final int code8 = 2130837511;
        public static final int colorlevel = 2130837512;
        public static final int magicmatrix = 2130837513;
        public static final int magicmatrix2 = 2130837514;
        public static final int magicmatrix3 = 2130837515;
    }

    public static final class id {
        public static final int bannerLayout = 2131296257;
        public static final int button1 = 2131296258;
        public static final int imageView1 = 2131296263;
        public static final int imageView2 = 2131296264;
        public static final int imageView3 = 2131296265;
        public static final int imageView4 = 2131296266;
        public static final int imageView5 = 2131296267;
        public static final int imageView6 = 2131296268;
        public static final int relativeLayout1 = 2131296256;
        public static final int relativeLayout2 = 2131296260;
        public static final int tableLayout1 = 2131296261;
        public static final int tableRow1 = 2131296262;
        public static final int tableRow2 = 2131296269;
        public static final int tableRow3 = 2131296270;
        public static final int tableRow4 = 2131296271;
        public static final int tableRow5 = 2131296272;
        public static final int tableRow6 = 2131296273;
        public static final int textView1 = 2131296259;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int ADWHIRL_PUBLISHER_KEY = 2131099649;
        public static final int AD_UNIT_PUBLISHER_ID = 2131099650;
        public static final int app_name = 2131099668;
        public static final int boardSize = 2131099677;
        public static final int boardSizeSummary = 2131099678;
        public static final int def_boardsize = 2131099651;
        public static final int def_numberofsymbols = 2131099652;
        public static final int do_you_want_this_app_to_use_speech_ = 2131099660;
        public static final int do_you_want_to_rate_this_app_ = 2131099661;
        public static final int einstellungen = 2131099655;
        public static final int gameoptions = 2131099672;
        public static final int help = 2131099670;
        public static final int hilf = 2131099654;
        public static final int info = 2131099653;
        public static final int ja = 2131099664;
        public static final int lost = 2131099673;
        public static final int nein = 2131099663;
        public static final int neue_einstellungen = 2131099656;
        public static final int newtext = 2131099671;
        public static final int no_market_installed = 2131099662;
        public static final int numberOfSymbols = 2131099680;
        public static final int numberofsymbolscaption = 2131099679;
        public static final int numberofsymbolssummary = 2131099681;
        public static final int positivemotivationsummary = 2131099667;
        public static final int scores = 2131099675;
        public static final int speechoption = 2131099657;
        public static final int speechoptionsummary = 2131099658;
        public static final int use_positivemotivation = 2131099666;
        public static final int use_speech = 2131099659;
        public static final int usespeechsummary = 2131099669;
        public static final int versionName = 2131099648;
        public static final int welcome_to_s_version_s = 2131099665;
        public static final int won = 2131099674;
        public static final int you_solved_d_puzzles = 2131099676;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
