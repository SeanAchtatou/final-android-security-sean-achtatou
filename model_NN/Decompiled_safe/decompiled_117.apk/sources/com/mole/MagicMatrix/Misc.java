package com.mole.MagicMatrix;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.util.Linkify;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Pattern;

public class Misc {
    public static boolean debugmode = false;

    public static void ShowMessage(Context ctx, String txt) {
        Toast.makeText(ctx, txt, 0).show();
    }

    public static void ShowDebugMessage(Context ctx, String txt) {
        if (debugmode) {
            Toast.makeText(ctx, txt, 0).show();
        }
    }

    public static String intToString(int i) {
        return String.format("%d", Integer.valueOf(i));
    }

    public static void fillTextFromStringArray(TextView tv, Resources res, int arrayid) {
        String[] texte = res.getStringArray(arrayid);
        for (int i = 0; i < texte.length; i++) {
            tv.append(String.valueOf(texte[i]) + "\n");
        }
    }

    public static void OkDialog(Context context, String title, String message) {
        Dialog dialog = new Dialog(context);
        TextView tv = new TextView(context);
        tv.setPadding(20, 0, 20, 0);
        tv.setTextAppearance(context, 16973894);
        tv.setText(message);
        dialog.setContentView(tv);
        dialog.setTitle(title);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public static void InfoDialog(Context context, int titleid, int messagearrayid) {
        Dialog dialog = new Dialog(context);
        TextView tv = new TextView(context);
        Resources res = context.getResources();
        tv.append(String.format(res.getString(R.string.welcome_to_s_version_s), res.getText(R.string.app_name), res.getText(R.string.versionName)));
        tv.append("\n\n");
        tv.setPadding(20, 0, 20, 0);
        tv.setTextAppearance(context, 16973892);
        fillTextFromStringArray(tv, res, messagearrayid);
        Linkify.addLinks(tv, 15);
        Linkify.addLinks(tv, Pattern.compile("\\bmole.software\\b"), "https://market.android.com/developer?pub=");
        dialog.setContentView(tv);
        dialog.setTitle(titleid);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public static void YesDialog(Context context, int messageid, DialogInterface.OnClickListener positivocl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(messageid).setCancelable(false).setPositiveButton((int) R.string.ja, positivocl).setNegativeButton((int) R.string.nein, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public static void YesDialog(Context context, int messageid, DialogInterface.OnClickListener positivocl, DialogInterface.OnClickListener negativocl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(messageid).setCancelable(false).setPositiveButton((int) R.string.ja, positivocl);
        if (negativocl == null) {
            builder.setNegativeButton((int) R.string.nein, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        } else {
            builder.setNegativeButton((int) R.string.nein, negativocl);
        }
        builder.create().show();
    }

    public static void YesDialog(Context context, int titleid, DialogInterface.OnClickListener positivocl, DialogInterface.OnClickListener negativocl, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleid).setMessage(message).setCancelable(false).setPositiveButton((int) R.string.ja, positivocl);
        if (negativocl == null) {
            builder.setNegativeButton((int) R.string.nein, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
        } else {
            builder.setNegativeButton((int) R.string.nein, negativocl);
        }
        builder.create().show();
    }

    public static void showEvaluationSupport(Context context) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + context.getPackageName())));
        } catch (Exception e) {
            ShowMessage(context, context.getString(R.string.no_market_installed));
        }
    }
}
