package com.adwhirl.adapters;

import android.app.Activity;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.text.SimpleDateFormat;

public class GoogleAdMobAdsAdapter extends AdWhirlAdapter implements AdListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$adwhirl$AdWhirlTargeting$Gender;

    static /* synthetic */ int[] $SWITCH_TABLE$com$adwhirl$AdWhirlTargeting$Gender() {
        int[] iArr = $SWITCH_TABLE$com$adwhirl$AdWhirlTargeting$Gender;
        if (iArr == null) {
            iArr = new int[AdWhirlTargeting.Gender.values().length];
            try {
                iArr[AdWhirlTargeting.Gender.FEMALE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdWhirlTargeting.Gender.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdWhirlTargeting.Gender.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$adwhirl$AdWhirlTargeting$Gender = iArr;
        }
        return iArr;
    }

    public GoogleAdMobAdsAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    /* access modifiers changed from: protected */
    public String birthdayForAdWhirlTargeting() {
        if (AdWhirlTargeting.getBirthDate() != null) {
            return new SimpleDateFormat("yyyyMMdd").format(AdWhirlTargeting.getBirthDate().getTime());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public AdRequest.Gender genderForAdWhirlTargeting() {
        switch ($SWITCH_TABLE$com$adwhirl$AdWhirlTargeting$Gender()[AdWhirlTargeting.getGender().ordinal()]) {
            case 2:
                return AdRequest.Gender.MALE;
            case AdWhirlUtil.NETWORK_TYPE_VIDEOEGG /*3*/:
                return AdRequest.Gender.FEMALE;
            default:
                return null;
        }
    }

    public void handle() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            AdView adView = new AdView(activity, AdSize.BANNER, this.ration.key);
            adView.setAdListener(this);
            adView.loadAd(requestForAdWhirlLayout(adWhirlLayout));
        }
    }

    /* access modifiers changed from: protected */
    public void log(String message) {
        Log.d(AdWhirlUtil.ADWHIRL, "GoogleAdapter " + message);
    }

    /* access modifiers changed from: protected */
    public AdRequest requestForAdWhirlLayout(AdWhirlLayout layout) {
        AdRequest result = new AdRequest();
        result.setTesting(AdWhirlTargeting.getTestMode());
        result.setGender(genderForAdWhirlTargeting());
        result.setBirthday(birthdayForAdWhirlTargeting());
        if (layout.extra.locationOn == 1) {
            result.setLocation(layout.adWhirlManager.location);
        }
        result.setKeywords(AdWhirlTargeting.getKeywordSet());
        return result;
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        log("failure (" + arg1 + ")");
        arg0.setAdListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        log("success");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            if (!(arg0 instanceof AdView)) {
                log("invalid AdView");
                return;
            }
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, (AdView) arg0));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }
}
