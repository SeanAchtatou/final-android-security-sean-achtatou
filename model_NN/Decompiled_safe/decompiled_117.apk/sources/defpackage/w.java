package defpackage;

import android.content.Context;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: w  reason: default package */
public final class w implements Runnable {
    private Context a;
    private String b;

    public w(String str, Context context) {
        this.b = str;
        this.a = context;
    }

    public final void run() {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.b).openConnection();
            AdUtil.a(httpURLConnection, this.a);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() != 200) {
                a.e("Did not receive HTTP_OK from URL: " + this.b);
            }
        } catch (IOException e) {
            a.c("Unable to ping the URL: " + this.b, e);
        }
    }
}
