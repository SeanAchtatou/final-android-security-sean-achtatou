package com.image.fysitones;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseContactActivity extends ListActivity implements TextWatcher {
    private SimpleCursorAdapter mAdapter;
    private Uri mCONTENT_URI;
    private TextView mFilter;
    private Uri mRingtoneUri;

    public static void startActivity(Activity activity, Uri uri) {
        if (uri != null) {
            Intent intent = new Intent(activity, ChooseContactActivity.class);
            intent.setData(uri);
            activity.startActivity(intent);
        }
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getContactClass();
        this.mRingtoneUri = getIntent().getData();
        setContentView((int) R.layout.choose_contact);
        try {
            this.mAdapter = new SimpleCursorAdapter(this, R.layout.contact_row, createCursor(""), new String[]{"custom_ringtone", "starred", "display_name"}, new int[]{R.id.row_ringtone, R.id.row_starred, R.id.row_display_name});
            this.mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    String name = cursor.getColumnName(columnIndex);
                    String value = cursor.getString(columnIndex);
                    if (name.equals("custom_ringtone")) {
                        if (value == null || value.length() <= 0) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                        }
                        return true;
                    } else if (!name.equals("starred")) {
                        return false;
                    } else {
                        if (value == null || !value.equals("1")) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                        }
                        return true;
                    }
                }
            });
            setListAdapter(this.mAdapter);
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    ChooseContactActivity.this.assignRingtoneToContact();
                }
            });
        } catch (SecurityException e) {
            Log.e("ChooseContactActivity", e.toString());
        }
        this.mFilter = (TextView) findViewById(R.id.search_filter);
        if (this.mFilter != null) {
            this.mFilter.addTextChangedListener(this);
        }
    }

    /* access modifiers changed from: private */
    public void assignRingtoneToContact() {
        Cursor c = this.mAdapter.getCursor();
        String contactId = c.getString(c.getColumnIndexOrThrow("_id"));
        String displayName = c.getString(c.getColumnIndexOrThrow("display_name"));
        Uri uri = Uri.withAppendedPath(this.mCONTENT_URI, contactId);
        ContentValues values = new ContentValues();
        values.put("custom_ringtone", this.mRingtoneUri.toString());
        getContentResolver().update(uri, values, null, null);
        Toast.makeText(this, ((Object) getResources().getText(R.string.success_contact_ringtone)) + " " + displayName, 0).show();
        finish();
    }

    private Cursor createCursor(String filter) {
        String selection;
        if (filter == null || filter.length() <= 0) {
            selection = null;
        } else {
            selection = "(DISPLAY_NAME LIKE \"%" + filter + "%\")";
        }
        Cursor cursor = managedQuery(this.mCONTENT_URI, new String[]{"_id", "custom_ringtone", "display_name", "last_time_contacted", "starred", "times_contacted"}, selection, null, "STARRED DESC, TIMES_CONTACTED DESC, LAST_TIME_CONTACTED DESC");
        Log.i("ChooseContactActivity", String.valueOf(cursor.getCount()) + " contacts");
        return cursor;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void afterTextChanged(Editable s) {
        this.mAdapter.changeCursor(createCursor(this.mFilter.getText().toString()));
    }

    public void getContactClass() {
        if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
            this.mCONTENT_URI = Uri.parse("content://com.android.contacts/contacts");
        } else {
            this.mCONTENT_URI = Uri.parse("content://contacts/people");
        }
    }
}
