package com.image.fysitones;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;
import com.millennialmedia.android.R;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageSoundboard extends Activity {
    private static final int DLG_RATEME = 2;
    private static final int POPUP_MENU = 1;
    public View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            ImageSoundboard.this.play((Sample) ((ImageButton) v).getTag());
        }
    };
    MediaPlayer.OnCompletionListener listener;
    MediaPlayer.OnCompletionListener listenter = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            mp.release();
            ImageSoundboard.this.player = null;
        }
    };
    public View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        public boolean onLongClick(View v) {
            ImageSoundboard.this.setAsRingtone((Sample) ((ImageButton) v).getTag());
            return false;
        }
    };
    /* access modifiers changed from: private */
    public Sample mSample;
    /* access modifiers changed from: private */
    public MediaPlayer player;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        AdsView1.createAdWhirl(this);
        AdsView2.createAdWhirl(this);
        AdsView3.createAdWhirl(this);
        AdsView4.createAdWhirl(this);
        initialize();
    }

    public void prepare(Sample sample) {
    }

    public void play(Sample sample) {
        Log.v(getClass().getName(), "Playing: " + sample.getName() + " (" + sample.getResId() + ")");
        if (this.player != null) {
            this.player.stop();
            this.player.release();
        }
        this.player = MediaPlayer.create(this, sample.getResId());
        this.player.setVolume(1.0f, 1.0f);
        this.player.setOnCompletionListener(this.listener);
        if (this.player != null) {
            this.player.start();
        }
    }

    public void stop() {
        if (this.player != null) {
            try {
                this.player.stop();
                this.player.release();
                this.player = null;
            } catch (IllegalStateException e) {
            }
        }
    }

    public void setAsRingtone(Sample sample) {
        this.mSample = sample;
        showDialog(1);
    }

    public boolean assignRingtone(Sample sample) {
        ChooseContactActivity.startActivity(this, Ringtone.insertRingtone(getContentResolver(), insertMedia(sample).getAbsolutePath(), sample.getName()));
        return true;
    }

    public boolean setAsDefaultRingtone(Sample sample) {
        Uri mediaUri = Ringtone.insertRingtone(getContentResolver(), insertMedia(sample).getAbsolutePath(), sample.getName());
        if (mediaUri != null) {
            Log.i(getClass().getCanonicalName(), "Set " + mediaUri + " as default ringtone");
            RingtoneManager.setActualDefaultRingtoneUri(this, 1, mediaUri);
            Toast.makeText(this, String.valueOf(sample.getName()) + " " + ((String) getResources().getText(R.string.success_default_ringtone)), 0).show();
            Log.i(getClass().getCanonicalName(), RingtoneManager.getDefaultUri(1).toString());
        }
        return true;
    }

    public boolean setAsNotification(Sample sample) {
        Uri mediaUri = Ringtone.insertNotification(getContentResolver(), insertMedia(sample).getAbsolutePath(), sample.getName());
        if (mediaUri == null) {
            return true;
        }
        RingtoneManager.setActualDefaultRingtoneUri(this, 2, mediaUri);
        return true;
    }

    public boolean setAsAlarm(Sample sample) {
        Uri mediaUri = Ringtone.insertAlarm(getContentResolver(), insertMedia(sample).getAbsolutePath(), sample.getName());
        if (mediaUri == null) {
            return true;
        }
        RingtoneManager.setActualDefaultRingtoneUri(this, 4, mediaUri);
        return true;
    }

    private File insertMedia(Sample sample) {
        InputStream input = getBaseContext().getResources().openRawResource(sample.getResId());
        try {
            byte[] buffer = new byte[input.available()];
            input.read(buffer);
            input.close();
            File ringtoneFile = new File("/sdcard/media/audio/ringtones/", String.valueOf(sample.getName()) + ".ogg");
            File baseDirectory = ringtoneFile.getParentFile();
            if (!baseDirectory.exists()) {
                baseDirectory.mkdirs();
            }
            if (!ringtoneFile.exists()) {
                try {
                    OutputStream output = new BufferedOutputStream(new FileOutputStream(ringtoneFile));
                    output.write(buffer);
                    output.close();
                } catch (FileNotFoundException e) {
                    Log.e(getClass().getCanonicalName(), "File " + ringtoneFile + " does not exist", e);
                    return null;
                } catch (IOException e2) {
                    Log.e(getClass().getCanonicalName(), "Could not save resource on SD card", e2);
                    return null;
                }
            }
            return ringtoneFile;
        } catch (IOException e3) {
            Log.e(getClass().getCanonicalName(), "Could not read resource", e3);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.player != null) {
            this.player.release();
            this.player = null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.layout.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem itemStop = menu.getItem(1);
        if (this.player == null) {
            itemStop.setVisible(false);
            Log.e("create menu", "player is null");
        }
        if (this.player != null) {
            if (!this.player.isPlaying()) {
                itemStop.setVisible(false);
                Log.e("create menu", "player is not playing");
            } else {
                itemStop.setVisible(true);
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                startActivity(new Intent(this, About.class));
                return true;
            case R.id.stop:
                stop();
                return true;
            case R.id.apps:
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"Marino Music Studio\"")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.rating:
                showDialog(2);
                return true;
            case R.id.quit:
                finish();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.set_as).setItems((int) R.array.SETRINGTONE_POPMENU_LIST, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case R.styleable.MMAdView_apid /*0*/:
                                ImageSoundboard.this.setAsDefaultRingtone(ImageSoundboard.this.mSample);
                                return;
                            case 1:
                                ImageSoundboard.this.assignRingtone(ImageSoundboard.this.mSample);
                                return;
                            case 2:
                                ImageSoundboard.this.setAsNotification(ImageSoundboard.this.mSample);
                                return;
                            case 3:
                                ImageSoundboard.this.setAsAlarm(ImageSoundboard.this.mSample);
                                return;
                            default:
                                return;
                        }
                    }
                }).create();
            case 2:
                return new AlertDialog.Builder(this).setTitle("If you like me, please rate me five star.").setPositiveButton("Rate", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        try {
                            ImageSoundboard.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:" + ImageSoundboard.class.getPackage().getName())));
                        } catch (Exception e) {
                        }
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).setCancelable(true).create();
            default:
                return null;
        }
    }

    private void initialize() {
        ImageButton imgButton1 = (ImageButton) ((FrameLayout) findViewById(R.id.sb1_button)).findViewById(R.id.sound_button);
        imgButton1.setImageResource(R.drawable.sb1);
        imgButton1.setTag(new Sample("1", R.raw.sb1));
        imgButton1.setOnClickListener(this.clickListener);
        imgButton1.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton = (ImageButton) ((FrameLayout) findViewById(R.id.sb2_button)).findViewById(R.id.sound_button);
        imgButton.setImageResource(R.drawable.sb2);
        imgButton.setTag(new Sample("2", R.raw.sb2));
        imgButton.setOnClickListener(this.clickListener);
        imgButton.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton2 = (ImageButton) ((FrameLayout) findViewById(R.id.sb3_button)).findViewById(R.id.sound_button);
        imgButton2.setImageResource(R.drawable.sb3);
        imgButton2.setTag(new Sample("3", R.raw.sb3));
        imgButton2.setOnClickListener(this.clickListener);
        imgButton2.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton3 = (ImageButton) ((FrameLayout) findViewById(R.id.sb4_button)).findViewById(R.id.sound_button);
        imgButton3.setImageResource(R.drawable.sb4);
        imgButton3.setTag(new Sample("4", R.raw.sb4));
        imgButton3.setOnClickListener(this.clickListener);
        imgButton3.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton4 = (ImageButton) ((FrameLayout) findViewById(R.id.sb5_button)).findViewById(R.id.sound_button);
        imgButton4.setImageResource(R.drawable.sb5);
        imgButton4.setTag(new Sample("5", R.raw.sb5));
        imgButton4.setOnClickListener(this.clickListener);
        imgButton4.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton5 = (ImageButton) ((FrameLayout) findViewById(R.id.sb6_button)).findViewById(R.id.sound_button);
        imgButton5.setImageResource(R.drawable.sb6);
        imgButton5.setTag(new Sample("6", R.raw.sb6));
        imgButton5.setOnClickListener(this.clickListener);
        imgButton5.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton6 = (ImageButton) ((FrameLayout) findViewById(R.id.sb7_button)).findViewById(R.id.sound_button);
        imgButton6.setImageResource(R.drawable.sb7);
        imgButton6.setTag(new Sample("7", R.raw.sb7));
        imgButton6.setOnClickListener(this.clickListener);
        imgButton6.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton7 = (ImageButton) ((FrameLayout) findViewById(R.id.sb8_button)).findViewById(R.id.sound_button);
        imgButton7.setImageResource(R.drawable.sb8);
        imgButton7.setTag(new Sample("8", R.raw.sb8));
        imgButton7.setOnClickListener(this.clickListener);
        imgButton7.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton8 = (ImageButton) ((FrameLayout) findViewById(R.id.sb9_button)).findViewById(R.id.sound_button);
        imgButton8.setImageResource(R.drawable.sb9);
        imgButton8.setTag(new Sample("9", R.raw.sb9));
        imgButton8.setOnClickListener(this.clickListener);
        imgButton8.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton9 = (ImageButton) ((FrameLayout) findViewById(R.id.sb10_button)).findViewById(R.id.sound_button);
        imgButton9.setImageResource(R.drawable.sb10);
        imgButton9.setTag(new Sample("10", R.raw.sb10));
        imgButton9.setOnClickListener(this.clickListener);
        imgButton9.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton10 = (ImageButton) ((FrameLayout) findViewById(R.id.sb11_button)).findViewById(R.id.sound_button);
        imgButton10.setImageResource(R.drawable.sb11);
        imgButton10.setTag(new Sample("11", R.raw.sb11));
        imgButton10.setOnClickListener(this.clickListener);
        imgButton10.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton11 = (ImageButton) ((FrameLayout) findViewById(R.id.sb12_button)).findViewById(R.id.sound_button);
        imgButton11.setImageResource(R.drawable.sb12);
        imgButton11.setTag(new Sample("12", R.raw.sb12));
        imgButton11.setOnClickListener(this.clickListener);
        imgButton11.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton12 = (ImageButton) ((FrameLayout) findViewById(R.id.sb13_button)).findViewById(R.id.sound_button);
        imgButton12.setImageResource(R.drawable.sb13);
        imgButton12.setTag(new Sample("13", R.raw.sb13));
        imgButton12.setOnClickListener(this.clickListener);
        imgButton12.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton13 = (ImageButton) ((FrameLayout) findViewById(R.id.sb14_button)).findViewById(R.id.sound_button);
        imgButton13.setImageResource(R.drawable.sb14);
        imgButton13.setTag(new Sample("14", R.raw.sb14));
        imgButton13.setOnClickListener(this.clickListener);
        imgButton13.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton14 = (ImageButton) ((FrameLayout) findViewById(R.id.sb15_button)).findViewById(R.id.sound_button);
        imgButton14.setImageResource(R.drawable.sb15);
        imgButton14.setTag(new Sample("15", R.raw.sb15));
        imgButton14.setOnClickListener(this.clickListener);
        imgButton14.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton15 = (ImageButton) ((FrameLayout) findViewById(R.id.sb16_button)).findViewById(R.id.sound_button);
        imgButton15.setImageResource(R.drawable.sb16);
        imgButton15.setTag(new Sample("16", R.raw.sb16));
        imgButton15.setOnClickListener(this.clickListener);
        imgButton15.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton16 = (ImageButton) ((FrameLayout) findViewById(R.id.sb17_button)).findViewById(R.id.sound_button);
        imgButton16.setImageResource(R.drawable.sb17);
        imgButton16.setTag(new Sample("17", R.raw.sb17));
        imgButton16.setOnClickListener(this.clickListener);
        imgButton16.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton17 = (ImageButton) ((FrameLayout) findViewById(R.id.sb18_button)).findViewById(R.id.sound_button);
        imgButton17.setImageResource(R.drawable.sb18);
        imgButton17.setTag(new Sample("18", R.raw.sb18));
        imgButton17.setOnClickListener(this.clickListener);
        imgButton17.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton18 = (ImageButton) ((FrameLayout) findViewById(R.id.sb19_button)).findViewById(R.id.sound_button);
        imgButton18.setImageResource(R.drawable.sb19);
        imgButton18.setTag(new Sample("19", R.raw.sb19));
        imgButton18.setOnClickListener(this.clickListener);
        imgButton18.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton19 = (ImageButton) ((FrameLayout) findViewById(R.id.sb20_button)).findViewById(R.id.sound_button);
        imgButton19.setImageResource(R.drawable.sb20);
        imgButton19.setTag(new Sample("20", R.raw.sb20));
        imgButton19.setOnClickListener(this.clickListener);
        imgButton19.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton20 = (ImageButton) ((FrameLayout) findViewById(R.id.sb21_button)).findViewById(R.id.sound_button);
        imgButton20.setImageResource(R.drawable.sb21);
        imgButton20.setTag(new Sample("21", R.raw.sb21));
        imgButton20.setOnClickListener(this.clickListener);
        imgButton20.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton21 = (ImageButton) ((FrameLayout) findViewById(R.id.sb22_button)).findViewById(R.id.sound_button);
        imgButton21.setImageResource(R.drawable.sb22);
        imgButton21.setTag(new Sample("22", R.raw.sb22));
        imgButton21.setOnClickListener(this.clickListener);
        imgButton21.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton22 = (ImageButton) ((FrameLayout) findViewById(R.id.sb23_button)).findViewById(R.id.sound_button);
        imgButton22.setImageResource(R.drawable.sb23);
        imgButton22.setTag(new Sample("23", R.raw.sb23));
        imgButton22.setOnClickListener(this.clickListener);
        imgButton22.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton23 = (ImageButton) ((FrameLayout) findViewById(R.id.sb24_button)).findViewById(R.id.sound_button);
        imgButton23.setImageResource(R.drawable.sb24);
        imgButton23.setTag(new Sample("24", R.raw.sb24));
        imgButton23.setOnClickListener(this.clickListener);
        imgButton23.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton24 = (ImageButton) ((FrameLayout) findViewById(R.id.sb25_button)).findViewById(R.id.sound_button);
        imgButton24.setImageResource(R.drawable.sb25);
        imgButton24.setTag(new Sample("25", R.raw.sb25));
        imgButton24.setOnClickListener(this.clickListener);
        imgButton24.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton25 = (ImageButton) ((FrameLayout) findViewById(R.id.sb26_button)).findViewById(R.id.sound_button);
        imgButton25.setImageResource(R.drawable.sb26);
        imgButton25.setTag(new Sample("26", R.raw.sb26));
        imgButton25.setOnClickListener(this.clickListener);
        imgButton25.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton26 = (ImageButton) ((FrameLayout) findViewById(R.id.sb27_button)).findViewById(R.id.sound_button);
        imgButton26.setImageResource(R.drawable.sb27);
        imgButton26.setTag(new Sample("27", R.raw.sb27));
        imgButton26.setOnClickListener(this.clickListener);
        imgButton26.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton27 = (ImageButton) ((FrameLayout) findViewById(R.id.sb28_button)).findViewById(R.id.sound_button);
        imgButton27.setImageResource(R.drawable.sb28);
        imgButton27.setTag(new Sample("28", R.raw.sb28));
        imgButton27.setOnClickListener(this.clickListener);
        imgButton27.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton28 = (ImageButton) ((FrameLayout) findViewById(R.id.sb29_button)).findViewById(R.id.sound_button);
        imgButton28.setImageResource(R.drawable.sb29);
        imgButton28.setTag(new Sample("29", R.raw.sb29));
        imgButton28.setOnClickListener(this.clickListener);
        imgButton28.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton29 = (ImageButton) ((FrameLayout) findViewById(R.id.sb30_button)).findViewById(R.id.sound_button);
        imgButton29.setImageResource(R.drawable.sb30);
        imgButton29.setTag(new Sample("30", R.raw.sb30));
        imgButton29.setOnClickListener(this.clickListener);
        imgButton29.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton30 = (ImageButton) ((FrameLayout) findViewById(R.id.sb31_button)).findViewById(R.id.sound_button);
        imgButton30.setImageResource(R.drawable.sb31);
        imgButton30.setTag(new Sample("31", R.raw.sb31));
        imgButton30.setOnClickListener(this.clickListener);
        imgButton30.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton31 = (ImageButton) ((FrameLayout) findViewById(R.id.sb32_button)).findViewById(R.id.sound_button);
        imgButton31.setImageResource(R.drawable.sb32);
        imgButton31.setTag(new Sample("32", R.raw.sb32));
        imgButton31.setOnClickListener(this.clickListener);
        imgButton31.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton32 = (ImageButton) ((FrameLayout) findViewById(R.id.sb33_button)).findViewById(R.id.sound_button);
        imgButton32.setImageResource(R.drawable.sb33);
        imgButton32.setTag(new Sample("33", R.raw.sb33));
        imgButton32.setOnClickListener(this.clickListener);
        imgButton32.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton33 = (ImageButton) ((FrameLayout) findViewById(R.id.sb34_button)).findViewById(R.id.sound_button);
        imgButton33.setImageResource(R.drawable.sb34);
        imgButton33.setTag(new Sample("34", R.raw.sb34));
        imgButton33.setOnClickListener(this.clickListener);
        imgButton33.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton34 = (ImageButton) ((FrameLayout) findViewById(R.id.sb35_button)).findViewById(R.id.sound_button);
        imgButton34.setImageResource(R.drawable.sb35);
        imgButton34.setTag(new Sample("35", R.raw.sb35));
        imgButton34.setOnClickListener(this.clickListener);
        imgButton34.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton35 = (ImageButton) ((FrameLayout) findViewById(R.id.sb36_button)).findViewById(R.id.sound_button);
        imgButton35.setImageResource(R.drawable.sb36);
        imgButton35.setTag(new Sample("36", R.raw.sb36));
        imgButton35.setOnClickListener(this.clickListener);
        imgButton35.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton36 = (ImageButton) ((FrameLayout) findViewById(R.id.sb37_button)).findViewById(R.id.sound_button);
        imgButton36.setImageResource(R.drawable.sb37);
        imgButton36.setTag(new Sample("37", R.raw.sb37));
        imgButton36.setOnClickListener(this.clickListener);
        imgButton36.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton37 = (ImageButton) ((FrameLayout) findViewById(R.id.sb38_button)).findViewById(R.id.sound_button);
        imgButton37.setImageResource(R.drawable.sb38);
        imgButton37.setTag(new Sample("38", R.raw.sb38));
        imgButton37.setOnClickListener(this.clickListener);
        imgButton37.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton38 = (ImageButton) ((FrameLayout) findViewById(R.id.sb39_button)).findViewById(R.id.sound_button);
        imgButton38.setImageResource(R.drawable.sb39);
        imgButton38.setTag(new Sample("39", R.raw.sb39));
        imgButton38.setOnClickListener(this.clickListener);
        imgButton38.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton39 = (ImageButton) ((FrameLayout) findViewById(R.id.sb40_button)).findViewById(R.id.sound_button);
        imgButton39.setImageResource(R.drawable.sb40);
        imgButton39.setTag(new Sample("40", R.raw.sb40));
        imgButton39.setOnClickListener(this.clickListener);
        imgButton39.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton40 = (ImageButton) ((FrameLayout) findViewById(R.id.sb41_button)).findViewById(R.id.sound_button);
        imgButton40.setImageResource(R.drawable.sb41);
        imgButton40.setTag(new Sample("41", R.raw.sb41));
        imgButton40.setOnClickListener(this.clickListener);
        imgButton40.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton41 = (ImageButton) ((FrameLayout) findViewById(R.id.sb42_button)).findViewById(R.id.sound_button);
        imgButton41.setImageResource(R.drawable.sb42);
        imgButton41.setTag(new Sample("42", R.raw.sb42));
        imgButton41.setOnClickListener(this.clickListener);
        imgButton41.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton42 = (ImageButton) ((FrameLayout) findViewById(R.id.sb43_button)).findViewById(R.id.sound_button);
        imgButton42.setImageResource(R.drawable.sb43);
        imgButton42.setTag(new Sample("43", R.raw.sb43));
        imgButton42.setOnClickListener(this.clickListener);
        imgButton42.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton43 = (ImageButton) ((FrameLayout) findViewById(R.id.sb44_button)).findViewById(R.id.sound_button);
        imgButton43.setImageResource(R.drawable.sb44);
        imgButton43.setTag(new Sample("44", R.raw.sb44));
        imgButton43.setOnClickListener(this.clickListener);
        imgButton43.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton44 = (ImageButton) ((FrameLayout) findViewById(R.id.sb45_button)).findViewById(R.id.sound_button);
        imgButton44.setImageResource(R.drawable.sb45);
        imgButton44.setTag(new Sample("45", R.raw.sb45));
        imgButton44.setOnClickListener(this.clickListener);
        imgButton44.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton45 = (ImageButton) ((FrameLayout) findViewById(R.id.sb46_button)).findViewById(R.id.sound_button);
        imgButton45.setImageResource(R.drawable.sb46);
        imgButton45.setTag(new Sample("46", R.raw.sb46));
        imgButton45.setOnClickListener(this.clickListener);
        imgButton45.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton46 = (ImageButton) ((FrameLayout) findViewById(R.id.sb47_button)).findViewById(R.id.sound_button);
        imgButton46.setImageResource(R.drawable.sb47);
        imgButton46.setTag(new Sample("47", R.raw.sb47));
        imgButton46.setOnClickListener(this.clickListener);
        imgButton46.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton47 = (ImageButton) ((FrameLayout) findViewById(R.id.sb48_button)).findViewById(R.id.sound_button);
        imgButton47.setImageResource(R.drawable.sb48);
        imgButton47.setTag(new Sample("48", R.raw.sb48));
        imgButton47.setOnClickListener(this.clickListener);
        imgButton47.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton48 = (ImageButton) ((FrameLayout) findViewById(R.id.sb49_button)).findViewById(R.id.sound_button);
        imgButton48.setImageResource(R.drawable.sb49);
        imgButton48.setTag(new Sample("49", R.raw.sb49));
        imgButton48.setOnClickListener(this.clickListener);
        imgButton48.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton49 = (ImageButton) ((FrameLayout) findViewById(R.id.sb50_button)).findViewById(R.id.sound_button);
        imgButton49.setImageResource(R.drawable.sb50);
        imgButton49.setTag(new Sample("50", R.raw.sb50));
        imgButton49.setOnClickListener(this.clickListener);
        imgButton49.setOnLongClickListener(this.longClickListener);
        ImageButton imgButton50 = (ImageButton) ((FrameLayout) findViewById(R.id.sb51_button)).findViewById(R.id.sound_button);
        imgButton50.setImageResource(R.drawable.sb51);
        imgButton50.setTag(new Sample("51", R.raw.sb51));
        imgButton50.setOnClickListener(this.clickListener);
        imgButton50.setOnLongClickListener(this.longClickListener);
    }
}
