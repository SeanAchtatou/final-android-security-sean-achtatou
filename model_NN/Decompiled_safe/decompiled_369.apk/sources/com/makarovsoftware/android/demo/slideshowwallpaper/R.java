package com.makarovsoftware.android.demo.slideshowwallpaper;

public final class R {

    public static final class array {
        public static final int settings_layoutnames = 2131165184;
        public static final int settings_layoutprefix = 2131165185;
        public static final int settings_unitnames = 2131165186;
        public static final int settings_unitprefix = 2131165187;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int solid_blue = 2131099649;
        public static final int solid_green = 2131099650;
        public static final int solid_red = 2131099648;
        public static final int solid_yellow = 2131099651;
    }

    public static final class drawable {
        public static final int blue = 2130837507;
        public static final int gray = 2130837505;
        public static final int green = 2130837508;
        public static final int icon = 2130837504;
        public static final int red = 2130837506;
        public static final int screen_background_black = 2130837510;
        public static final int translucent_background = 2130837511;
        public static final int transparent_background = 2130837512;
        public static final int yellow = 2130837509;
    }

    public static final class id {
        public static final int button1 = 2131296261;
        public static final int button2 = 2131296262;
        public static final int listView1 = 2131296257;
        public static final int textView1 = 2131296256;
        public static final int textView2 = 2131296258;
        public static final int textView3 = 2131296259;
        public static final int textView4 = 2131296260;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int solid_gray = 2131034112;
    }

    public static final class string {
        public static final int PurchaseButton = 2131230734;
        public static final int SetWallpaperButton = 2131230733;
        public static final int Text1 = 2131230729;
        public static final int Text2 = 2131230730;
        public static final int Text3 = 2131230731;
        public static final int Text4 = 2131230732;
        public static final int app_name = 2131230720;
        public static final int getFullVersionTitle = 2131230746;
        public static final int interval_summary = 2131230723;
        public static final int interval_title = 2131230724;
        public static final int pref_localFolders = 2131230737;
        public static final int pref_localFoldersEnabledSummaryOff = 2131230740;
        public static final int pref_localFoldersEnabledSummaryOn = 2131230739;
        public static final int pref_localFoldersEnabledTitle = 2131230738;
        public static final int pref_picasa = 2131230741;
        public static final int pref_picasaEnabledSummaryOff = 2131230744;
        public static final int pref_picasaEnabledSummaryOn = 2131230743;
        public static final int pref_picasaEnabledTitle = 2131230742;
        public static final int pref_picasaTitle = 2131230745;
        public static final int pref_selectFoldersSummary = 2131230736;
        public static final int pref_selectFoldersTitle = 2131230735;
        public static final int slideshowwallpaper_LayoutsSummary = 2131230727;
        public static final int slideshowwallpaper_LayoutsTitle = 2131230728;
        public static final int slideshowwallpaper_UnitsSummary = 2131230726;
        public static final int slideshowwallpaper_UnitsTitle = 2131230725;
        public static final int slideshowwallpaper_settings = 2131230722;
        public static final int wallpaper_description = 2131230721;
    }

    public static final class xml {
        public static final int slideshow_settings = 2130968576;
        public static final int slideshowwallpaper = 2130968577;
    }
}
