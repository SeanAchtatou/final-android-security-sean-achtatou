package com.makarovsoftware.android.demo.slideshowwallpaper;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.service.wallpaper.WallpaperService;
import android.util.Xml;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class BitmapLoader {
    public static final int IMAGE_MAX_SIZE = 1200;
    public static boolean isLoading = false;
    public static boolean isLoadingFiles = false;
    public static int loadedFiles = 0;
    public static ArrayList<String> loadedUrls;
    public static HashSet<String> loadedUrlsSet;
    public static ArrayList<String> urls;
    private HashSet<Integer> badNumbers = new HashSet<>();
    private Bitmap bm = null;
    private int currentNumber = 0;
    public String folders = "";
    private int imagesCount = 0;
    private HashSet<Integer> loadedImages = new HashSet<>();
    private Activity mActivity = null;
    private int mBitmapsCount = 0;
    HttpClient mClient;
    private String mCurrentImageURI = "";
    private boolean mExternalStorageAvailable = false;
    private boolean mExternalStorageWriteable = false;
    public boolean mImageLoadNeeded = false;
    public boolean mLocalFoldersEnabled = true;
    public boolean mOneStaticImage = false;
    public boolean mPicasaFoldersEnabled = false;
    public String mPicasaTags = "";
    private ArrayList<Bitmap> mResizedBitmaps = null;
    public String mSelectedImageURI = "";
    private WallpaperService mService = null;
    private long mStartTime = 0;
    public int mStatusBarHeight = 0;
    public boolean mUseSolidBackground = false;
    HashMap<Integer, String> map = new HashMap<>();
    private String packageName = "";
    /* access modifiers changed from: private */
    public Random random = new Random();
    private Bitmap resizedBitmap = null;
    private HashSet<String> tryedUrls = new HashSet<>();

    public BitmapLoader(WallpaperService service, Activity activity, ArrayList<Bitmap> resizedBitmaps, int count) {
        this.mService = service;
        this.mActivity = activity;
        this.mResizedBitmaps = resizedBitmaps;
        this.mBitmapsCount = count;
        this.mStartTime = SystemClock.elapsedRealtime();
        this.random.setSeed(this.mStartTime);
        if (urls == null) {
            urls = new ArrayList<>();
        }
        if (loadedUrls == null) {
            loadedUrls = new ArrayList<>();
        }
        if (loadedUrlsSet == null) {
            loadedUrlsSet = new HashSet<>();
        }
        initHTTPS();
    }

    private void readStorageState() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            this.mExternalStorageWriteable = true;
            this.mExternalStorageAvailable = true;
        } else if ("mounted_ro".equals(state)) {
            this.mExternalStorageAvailable = true;
            this.mExternalStorageWriteable = false;
        } else {
            this.mExternalStorageWriteable = false;
            this.mExternalStorageAvailable = false;
        }
    }

    private Bitmap clockDecodeSolidBackground() {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            Bitmap b = BitmapFactory.decodeResource(this.mService.getResources(), R.raw.solid_gray, o);
            int scale = 1;
            if (o.outHeight > 1200 || o.outWidth > 1200) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(1200.0d / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeResource(this.mService.getResources(), R.raw.solid_gray, o2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean notReady() {
        readStorageState();
        if (!this.mExternalStorageAvailable) {
            return true;
        }
        try {
            getImages().close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    public void prepareGrayBitmap(Canvas c, long now, String layout) {
        this.mResizedBitmaps.add(clockDecodeSolidBackground());
    }

    public void prepareResizedBitmaps(Canvas c, long now, String layout) {
        if (this.mBitmapsCount != 0) {
            this.badNumbers.clear();
            this.loadedImages.clear();
            this.imagesCount = 0;
            this.resizedBitmap = null;
            if (!this.mOneStaticImage) {
                for (int i = 0; i < this.mBitmapsCount; i++) {
                    try {
                        this.mResizedBitmaps.add(PrepareOneResizedBitmap(c, now, layout));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    this.mResizedBitmaps.add(clockPrepareOneResizedBitmap(c, now, layout));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            System.gc();
        }
    }

    public void clockPrepareBitmapsNoSlideshow(Canvas c, long now, String layout) {
        try {
            this.mResizedBitmaps.add(clockPrepareOneResizedBitmap(c, now, layout));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap PrepareOneResizedBitmap(Canvas c, long now, String layout) {
        Bitmap lResizedBitmap = null;
        this.bm = null;
        while (this.bm == null && (this.badNumbers.size() <= this.imagesCount || this.imagesCount == 0)) {
            try {
                getRandomBitmap();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (this.bm == null) {
                this.badNumbers.add(new Integer(this.currentNumber));
            }
            if (this.bm == null && this.mPicasaFoldersEnabled && !this.mLocalFoldersEnabled && this.tryedUrls.size() >= loadedUrls.size()) {
                break;
            }
        }
        if (this.bm == null || this.bm.getHeight() == 0 || this.bm.getWidth() == 0) {
            return null;
        }
        this.loadedImages.add(new Integer(this.currentNumber));
        double bmH = 0.0d;
        double bmW = 0.0d;
        double cH = 0.0d;
        double cW = 0.0d;
        if ("".length() == 0 && this.bm != null) {
            bmH = (double) this.bm.getHeight();
            bmW = (double) this.bm.getWidth();
            cH = (double) (c.getHeight() - this.mStatusBarHeight);
            cW = (double) c.getWidth();
        }
        if (layout.equals("two")) {
            if (cW > cH) {
                cW /= 2.0d;
            } else {
                cH /= 2.0d;
            }
        }
        if (layout.equals("four")) {
            cH /= 2.0d;
            cW /= 2.0d;
        }
        double ww = 1.0d;
        double hh = 1.0d;
        int newW = 0;
        int newH = 0;
        if (!(cH == 0.0d || cW == 0.0d)) {
            ww = bmW / cW;
            hh = bmH / cH;
            if (ww > hh) {
                newW = (int) ((bmH / cH) * cW);
                newH = (int) bmH;
            } else {
                newW = (int) bmW;
                newH = (int) ((bmW / cW) * cH);
            }
        }
        String str = String.valueOf("") + ", " + newW + "x" + newH + " w" + ww + "h" + hh;
        Matrix matrix = new Matrix();
        matrix.postScale(((float) cW) / ((float) newW), ((float) cH) / ((float) newH));
        double cropX = (bmW - ((double) newW)) / 2.0d;
        double cropY = (bmH - ((double) newH)) / 2.0d;
        if (newW > 0 && newH > 0) {
            lResizedBitmap = Bitmap.createBitmap(this.bm, (int) cropX, (int) cropY, newW, newH, matrix, true);
        }
        this.bm = null;
        return lResizedBitmap;
    }

    private void getRandomBitmap() {
        try {
            this.mCurrentImageURI = getRandomImageURI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.bm = decodeFile(this.mService.getContentResolver(), Uri.parse(this.mCurrentImageURI));
    }

    private void clockGetBitmap() {
        this.bm = decodeFile(this.mService.getContentResolver(), Uri.parse(this.mSelectedImageURI));
    }

    private String getfirstword(String val) {
        String str = val.trim();
        try {
            String[] tags = str.split(",");
            if (tags.length <= 0) {
                return str;
            }
            String str2 = tags[0];
            String[] tags2 = str2.split(".");
            if (tags2.length <= 0) {
                return str2;
            }
            String str3 = tags2[0];
            String[] tags3 = str3.split(";");
            if (tags3.length <= 0) {
                return str3;
            }
            String str4 = tags3[0];
            String[] tags4 = str4.split(":");
            if (tags4.length > 0) {
                return tags4[0];
            }
            return str4;
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    private Bitmap decodeFile(ContentResolver cr, Uri uri) {
        InputStream fis;
        InputStream fis2;
        Bitmap b = null;
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            String sPath = uri.getPath();
            if (sPath.length() == 0) {
                return null;
            }
            if (sPath.indexOf(getPackageName()) > 0) {
                fis = new FileInputStream(sPath);
            } else {
                fis = cr.openInputStream(uri);
            }
            BitmapFactory.decodeStream(fis, null, o);
            if (fis != null) {
                fis.close();
            }
            int scale = 1;
            if (o.outHeight > 1200 || o.outWidth > 1200) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(1200.0d / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            if (sPath.indexOf(getPackageName()) > 0) {
                fis2 = new FileInputStream(sPath);
            } else {
                fis2 = cr.openInputStream(uri);
            }
            b = BitmapFactory.decodeStream(fis2, null, o2);
            if (fis2 != null) {
                fis2.close();
            }
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0008 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap clockPrepareOneResizedBitmap(android.graphics.Canvas r33, long r34, java.lang.String r36) {
        /*
            r32 = this;
            r25 = 0
            r3 = 0
            r0 = r3
            r1 = r32
            r1.bm = r0
        L_0x0008:
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            if (r3 != 0) goto L_0x0026
            r0 = r32
            java.util.HashSet<java.lang.Integer> r0 = r0.badNumbers
            r3 = r0
            int r3 = r3.size()
            r0 = r32
            int r0 = r0.imagesCount
            r4 = r0
            if (r3 <= r4) goto L_0x0046
            r0 = r32
            int r0 = r0.imagesCount
            r3 = r0
            if (r3 == 0) goto L_0x0046
        L_0x0026:
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            if (r3 == 0) goto L_0x0043
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            int r3 = r3.getHeight()
            if (r3 == 0) goto L_0x0043
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            int r3 = r3.getWidth()
            if (r3 != 0) goto L_0x009e
        L_0x0043:
            r26 = r25
        L_0x0045:
            return r26
        L_0x0046:
            r32.readStorageState()     // Catch:{ Exception -> 0x0097 }
            r0 = r32
            boolean r0 = r0.mExternalStorageAvailable     // Catch:{ Exception -> 0x0097 }
            r3 = r0
            if (r3 != 0) goto L_0x0056
            r3 = 1
            r0 = r3
            r1 = r32
            r1.mImageLoadNeeded = r0     // Catch:{ Exception -> 0x0097 }
        L_0x0056:
            r0 = r32
            boolean r0 = r0.mUseSolidBackground     // Catch:{ Exception -> 0x0097 }
            r3 = r0
            if (r3 != 0) goto L_0x006f
            r0 = r32
            java.lang.String r0 = r0.mSelectedImageURI     // Catch:{ Exception -> 0x0097 }
            r3 = r0
            int r3 = r3.length()     // Catch:{ Exception -> 0x0097 }
            if (r3 == 0) goto L_0x006f
            r0 = r32
            boolean r0 = r0.mExternalStorageAvailable     // Catch:{ Exception -> 0x0097 }
            r3 = r0
            if (r3 != 0) goto L_0x008d
        L_0x006f:
            r32.clockGetGrayBitmap()     // Catch:{ Exception -> 0x0097 }
        L_0x0072:
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            if (r3 != 0) goto L_0x0008
            r0 = r32
            java.util.HashSet<java.lang.Integer> r0 = r0.badNumbers
            r3 = r0
            java.lang.Integer r4 = new java.lang.Integer
            r0 = r32
            int r0 = r0.currentNumber
            r5 = r0
            r4.<init>(r5)
            r3.add(r4)
            goto L_0x0008
        L_0x008d:
            r32.clockGetBitmap()     // Catch:{ Exception -> 0x0097 }
            r3 = 0
            r0 = r3
            r1 = r32
            r1.mImageLoadNeeded = r0     // Catch:{ Exception -> 0x0097 }
            goto L_0x0072
        L_0x0097:
            r3 = move-exception
            r22 = r3
            r22.printStackTrace()
            goto L_0x0072
        L_0x009e:
            r0 = r32
            java.util.HashSet<java.lang.Integer> r0 = r0.loadedImages
            r3 = r0
            java.lang.Integer r4 = new java.lang.Integer
            r0 = r32
            int r0 = r0.currentNumber
            r5 = r0
            r4.<init>(r5)
            r3.add(r4)
            r10 = 0
            r12 = 0
            r14 = 0
            r16 = 0
            java.lang.String r29 = ""
            int r3 = r29.length()
            if (r3 != 0) goto L_0x0116
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            if (r3 == 0) goto L_0x0116
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            int r3 = r3.getHeight()
            double r10 = (double) r3
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            int r3 = r3.getWidth()
            double r12 = (double) r3
            int r3 = r33.getHeight()
            double r14 = (double) r3
            int r3 = r33.getWidth()
            r0 = r3
            double r0 = (double) r0
            r16 = r0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r12)
            java.lang.String r4 = "x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r10)
            java.lang.String r4 = " to "
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r3
            r1 = r16
            java.lang.StringBuilder r3 = r0.append(r1)
            java.lang.String r4 = "x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r14)
            java.lang.String r29 = r3.toString()
        L_0x0116:
            java.lang.String r3 = "two"
            r0 = r36
            r1 = r3
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x0124
            r3 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r14 = r14 / r3
        L_0x0124:
            java.lang.String r3 = "four"
            r0 = r36
            r1 = r3
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x0136
            r3 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r14 = r14 / r3
            r3 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r16 = r16 / r3
        L_0x0136:
            r30 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r23 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r6 = 0
            r7 = 0
            r3 = 0
            int r3 = (r14 > r3 ? 1 : (r14 == r3 ? 0 : -1))
            if (r3 == 0) goto L_0x0156
            r3 = 0
            int r3 = (r16 > r3 ? 1 : (r16 == r3 ? 0 : -1))
            if (r3 == 0) goto L_0x0156
            double r30 = r12 / r16
            double r23 = r10 / r14
            int r3 = (r30 > r23 ? 1 : (r30 == r23 ? 0 : -1))
            if (r3 <= 0) goto L_0x01d9
            double r3 = r10 / r14
            double r3 = r3 * r16
            int r6 = (int) r3
            int r7 = (int) r10
        L_0x0156:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r29)
            r3.<init>(r4)
            java.lang.String r4 = ", "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = "x"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = " w"
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r3
            r1 = r30
            java.lang.StringBuilder r3 = r0.append(r1)
            java.lang.String r4 = "h"
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r3
            r1 = r23
            java.lang.StringBuilder r3 = r0.append(r1)
            java.lang.String r29 = r3.toString()
            r0 = r16
            float r0 = (float) r0
            r3 = r0
            float r4 = (float) r6
            float r28 = r3 / r4
            float r3 = (float) r14
            float r4 = (float) r7
            float r27 = r3 / r4
            android.graphics.Matrix r8 = new android.graphics.Matrix
            r8.<init>()
            r0 = r8
            r1 = r28
            r2 = r27
            r0.postScale(r1, r2)
            double r3 = (double) r6
            double r3 = r12 - r3
            r12 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r18 = r3 / r12
            double r3 = (double) r7
            double r3 = r10 - r3
            r9 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r20 = r3 / r9
            if (r6 <= 0) goto L_0x01cf
            if (r7 <= 0) goto L_0x01cf
            r25 = 0
            r0 = r32
            android.graphics.Bitmap r0 = r0.bm
            r3 = r0
            r0 = r18
            int r0 = (int) r0
            r4 = r0
            r0 = r20
            int r0 = (int) r0
            r5 = r0
            r9 = 1
            android.graphics.Bitmap r25 = android.graphics.Bitmap.createBitmap(r3, r4, r5, r6, r7, r8, r9)
        L_0x01cf:
            r3 = 0
            r0 = r3
            r1 = r32
            r1.bm = r0
            r26 = r25
            goto L_0x0045
        L_0x01d9:
            int r6 = (int) r12
            double r3 = r12 / r16
            double r3 = r3 * r14
            int r7 = (int) r3
            goto L_0x0156
        */
        throw new UnsupportedOperationException("Method not decompiled: com.makarovsoftware.android.demo.slideshowwallpaper.BitmapLoader.clockPrepareOneResizedBitmap(android.graphics.Canvas, long, java.lang.String):android.graphics.Bitmap");
    }

    private void clockGetGrayBitmap() {
        this.bm = clockDecodeSolidBackground();
    }

    private int getNextImageNumber(int num) {
        int loops = 0;
        int localNum = num;
        if (num == this.imagesCount) {
        }
        while (true) {
            if ((this.badNumbers.contains(new Integer(localNum)) || this.loadedImages.contains(new Integer(localNum))) && loops < this.imagesCount) {
                localNum++;
                if (localNum == this.imagesCount) {
                    localNum = 0;
                }
                loops++;
            }
        }
        if (loops < this.imagesCount) {
            return localNum;
        }
        return -1;
    }

    public void loadPicasaFolders() {
        if (!isLoading) {
            isLoading = true;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        BitmapLoader.this.filesToList();
                        BitmapLoader.this.getPicturesListXMLFromPicasa();
                        BitmapLoader.this.loadSomePictures();
                        BitmapLoader.this.filesToList();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BitmapLoader.isLoading = false;
                }
            }).start();
        }
    }

    public void filesToList() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    BitmapLoader.loadedUrlsSet.clear();
                    BitmapLoader.loadedUrls.clear();
                    String sPath = BitmapLoader.this.getPathForPicasa();
                    String[] files = new File(sPath).list();
                    if (files != null) {
                        for (String str : files) {
                            if (!str.equals(".nomedia")) {
                                BitmapLoader.loadedUrlsSet.add(String.valueOf(sPath) + str);
                            }
                        }
                    }
                    BitmapLoader.loadedUrls.addAll(BitmapLoader.loadedUrlsSet);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void loadSomePictures() {
        if (!isLoadingFiles) {
            try {
                isLoadingFiles = true;
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            if (BitmapLoader.urls.size() != 0) {
                                for (int i = 0; i < 10; i++) {
                                    BitmapLoader.loadedUrls.add(BitmapLoader.this.loadImageFromPicasaByIndex(BitmapLoader.this.random.nextInt(BitmapLoader.urls.size())));
                                }
                                BitmapLoader.isLoadingFiles = false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
            }
        }
    }

    private String getRandomImageURI() {
        int nextInt = this.random.nextInt(100);
        if (this.mPicasaFoldersEnabled && !loadedUrls.isEmpty()) {
            String qwe = loadedUrls.get(this.random.nextInt(loadedUrls.size()));
            if (!this.tryedUrls.contains(qwe)) {
                this.tryedUrls.add(qwe);
                return qwe;
            }
            ArrayList<String> localArray = new ArrayList<>();
            localArray.addAll(loadedUrls);
            localArray.removeAll(this.tryedUrls);
            if (localArray.size() > 0) {
                String qwe2 = localArray.get(this.random.nextInt(localArray.size()));
                this.tryedUrls.add(qwe2);
                return qwe2;
            }
        }
        if (!this.mLocalFoldersEnabled && this.mPicasaFoldersEnabled) {
            return "";
        }
        try {
            Cursor cursor = getImages();
            if (cursor == null) {
                return "";
            }
            int count = cursor.getCount();
            if (this.imagesCount == 0) {
                this.imagesCount = count;
            }
            int randomNumber = this.random.nextInt(count);
            if (this.badNumbers.contains(new Integer(randomNumber)) || this.loadedImages.contains(new Integer(randomNumber))) {
                randomNumber = getNextImageNumber(randomNumber);
            }
            this.currentNumber = randomNumber;
            if (randomNumber == -1) {
                cursor.close();
                return "";
            }
            String id = "";
            if (cursor.moveToPosition(randomNumber)) {
                id = cursor.getString(0);
                cursor.close();
            }
            return MediaStore.Images.Media.EXTERNAL_CONTENT_URI + "/" + id;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private Cursor getImages() {
        List<String> list = Arrays.asList(this.folders.split(";"));
        new HashSet(list);
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {"_id"};
        String selection = "";
        if (this.folders.length() > 0) {
            String strOr = "";
            for (String str : list) {
                selection = String.valueOf(selection) + strOr + "bucket_id" + " = " + str;
                strOr = " or ";
            }
        }
        if (this.mService != null) {
            return this.mService.getContentResolver().query(uri, projection, selection, null, null);
        }
        if (this.mActivity != null) {
            return this.mActivity.managedQuery(uri, projection, selection, null, null);
        }
        return null;
    }

    private void getFoldersMap() {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {"bucket_id", "bucket_display_name"};
        Cursor cursor = null;
        if (this.mService != null) {
            cursor = this.mService.getContentResolver().query(uri, projection, null, null, null);
        } else if (this.mActivity != null) {
            cursor = this.mActivity.managedQuery(uri, projection, null, null, null);
        }
        if (cursor != null) {
            this.map.clear();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Integer id = Integer.valueOf(cursor.getInt(0));
                String val = cursor.getString(1);
                if (!this.map.containsKey(id)) {
                    this.map.put(id, val);
                }
                cursor.moveToNext();
            }
            cursor.close();
        }
    }

    /* access modifiers changed from: package-private */
    public void initHTTPS() {
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        this.mClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
    }

    /* access modifiers changed from: package-private */
    public void getPicturesListXMLFromPicasa() {
        Uri.Builder uri = new Uri.Builder();
        uri.path("/data/feed/api/all");
        uri.appendQueryParameter("kind", "photo");
        if (urls.size() > 0) {
            uri.appendQueryParameter("max-results", new StringBuilder().append(urls.size()).append(loadedUrls.size()).append(1).toString());
        }
        uri.appendQueryParameter("start-index", new StringBuilder().append(loadedFiles + 1).toString());
        uri.appendQueryParameter("max-results", "200");
        uri.appendQueryParameter("tag", this.mPicasaTags);
        uri.appendQueryParameter("fields", "openSearch:itemsPerPage,entry(media:group(media:content))");
        uri.appendQueryParameter("imgmax", "800");
        try {
            executeRequest(new HttpGet(uri.build().toString()), new ResponseHandler() {
                public Object handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    BitmapLoader.this.parseXML(response.getEntity().getContent());
                    BitmapLoader.loadedFiles += 200;
                    return null;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getPackageName() {
        if (this.packageName.length() != 0) {
            return this.packageName;
        }
        if (this.mService != null) {
            this.packageName = this.mService.getPackageName();
        } else if (this.mActivity != null) {
            this.packageName = this.mActivity.getPackageName();
        }
        return this.packageName;
    }

    /* access modifiers changed from: private */
    public String getPathForPicasa() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + getPackageName() + "/cache/picasa/";
    }

    public String loadImageFromPicasaByIndex(int i) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        String urlStr = urls.get(i);
        try {
            URL url = new URL(urlStr);
            String path = getPathForPicasa();
            String fname = md5(urlStr);
            File file = new File(path, String.valueOf(fname) + ".jpg");
            if (file.exists()) {
                return String.valueOf(path) + fname + ".jpg";
            }
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            File parent = file.getParentFile();
            if (parent.exists() || parent.mkdirs()) {
                File file2 = new File(path, ".nomedia");
                if (!file2.exists()) {
                    file2.createNewFile();
                }
                FileOutputStream fileOutput = new FileOutputStream(file);
                InputStream inputStream = urlConnection.getInputStream();
                int contentLength = urlConnection.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                while (true) {
                    int bufferLength = inputStream.read(buffer);
                    if (bufferLength <= 0) {
                        fileOutput.close();
                        urls.remove(urlStr);
                        return String.valueOf(path) + fname + ".jpg";
                    }
                    fileOutput.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                }
            } else {
                throw new IllegalStateException("Couldn't create dir: " + parent);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private String md5(String in) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(in.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 240) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 15, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void executeRequest(HttpGet get, ResponseHandler handler) throws IOException {
        HttpEntity entity = null;
        try {
            HttpResponse response = this.mClient.execute(new HttpHost("picasaweb.google.com", 443, "https"), get);
            if (response.getStatusLine().getStatusCode() == 200) {
                handler.handleResponse(response);
            }
        } finally {
            if (entity != null) {
                entity.consumeContent();
            }
        }
    }

    /* access modifiers changed from: private */
    public void parseXML(InputStream in) {
        int i = 0;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(in, null);
            for (int eventType = parser.getEventType(); eventType != 1 && 0 == 0; eventType = parser.next()) {
                switch (eventType) {
                    case 2:
                        String name = parser.getName();
                        if (!name.equalsIgnoreCase("entry") && name.equalsIgnoreCase("content") && parser.getAttributeValue(null, "url") != null) {
                            urls.add(parser.getAttributeValue(null, "url"));
                            i++;
                            break;
                        }
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
