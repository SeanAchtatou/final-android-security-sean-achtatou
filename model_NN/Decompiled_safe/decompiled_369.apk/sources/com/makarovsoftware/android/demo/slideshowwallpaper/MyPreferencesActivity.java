package com.makarovsoftware.android.demo.slideshowwallpaper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class MyPreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getPreferenceManager().setSharedPreferencesName(SlideshowWallpaperService.SHARED_PREFS_NAME);
        addPreferencesFromResource(R.xml.slideshow_settings);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        findPreference("SelectFolders").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                try {
                    MyPreferencesActivity.this.startActivity(new Intent(MyPreferencesActivity.this, ChooseFolders.class));
                } catch (Exception e) {
                    Toast.makeText(MyPreferencesActivity.this.getBaseContext(), e.getMessage(), 1).show();
                }
                return true;
            }
        });
        findPreference("GetFull").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                try {
                    MyPreferencesActivity.this.goPurchase();
                } catch (Exception e) {
                    Toast.makeText(MyPreferencesActivity.this.getBaseContext(), e.getMessage(), 1).show();
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("demoslideshowwallpaper_Layouts") && !sharedPreferences.getString(key, "").equals("random")) {
            Toast.makeText(getBaseContext(), "only 'RANDOM' layout is available in DEMO version", 1).show();
        }
        if (key.equals("demoslideshowwallpaper_Units") || key.equals("slideshowwallpaper_AnimationInterval")) {
            Toast.makeText(getBaseContext(), "only '6 seconds' interval is available in DEMO version", 1).show();
        }
    }

    /* access modifiers changed from: private */
    public void goPurchase() {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse("market://details?id=com.makarovsoftware.android.slideshowwallpaper"));
        startActivity(i);
    }
}
