package com.makarovsoftware.android.demo.slideshowwallpaper;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ChooseFolders extends ListActivity {
    ImageFolder[] array = new ImageFolder[4];
    String folders = "";
    ArrayList<ImageFolder> list = new ArrayList<>();
    HashMap<Integer, String> map = new HashMap<>();

    class ImageFolder {
        public int id;
        public String name;

        public ImageFolder(int id2, String name2) {
            this.id = id2;
            this.name = name2;
        }

        public String toString() {
            return this.name;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(4, 4);
        fillMap();
        setListAdapter(new ArrayAdapter(this, 17367056, this.array));
        ListView listView = getListView();
        listView.setItemsCanFocus(false);
        listView.setChoiceMode(2);
        this.folders = getSharedPreferences(SlideshowWallpaperService.SHARED_PREFS_NAME, 0).getString("pref_folders", "");
        if (this.folders.length() > 0) {
            Set<String> idsSet = new HashSet<>(Arrays.asList(this.folders.split(";")));
            for (int i = 0; i < this.array.length; i++) {
                if (idsSet.contains(new StringBuilder().append(this.array[i].id).toString())) {
                    listView.setItemChecked(i, true);
                }
            }
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                long[] unused = ChooseFolders.this.getCheckItemIds();
            }
        });
    }

    /* access modifiers changed from: private */
    public long[] getCheckItemIds() {
        String zp = "";
        this.folders = "";
        ArrayList<Integer> checkedIds = new ArrayList<>();
        ListView listView = getListView();
        int childCount = listView.getChildCount();
        for (int childIndex = 0; childIndex < childCount; childIndex++) {
            if (listView.isItemChecked(childIndex)) {
                this.folders = String.valueOf(this.folders) + zp + this.array[childIndex].id;
                zp = ";";
                checkedIds.add(Integer.valueOf(childIndex));
            }
        }
        int childCount2 = checkedIds.size();
        long[] ids = new long[checkedIds.size()];
        for (int childIndex2 = 0; childIndex2 < childCount2; childIndex2++) {
            ids[childIndex2] = (long) ((Integer) checkedIds.get(childIndex2)).intValue();
        }
        SharedPreferences.Editor editor = getSharedPreferences(SlideshowWallpaperService.SHARED_PREFS_NAME, 0).edit();
        editor.putString("pref_folders", this.folders);
        editor.commit();
        return ids;
    }

    private void fillMap() {
        try {
            getFoldersMap();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private void getFoldersMap() {
        Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"bucket_id", "bucket_display_name"}, null, null, null);
        if (cursor != null) {
            this.map.clear();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Integer id = Integer.valueOf(cursor.getInt(0));
                String val = cursor.getString(1);
                if (!this.map.containsKey(id)) {
                    this.map.put(id, val);
                    this.list.add(new ImageFolder(id.intValue(), val));
                }
                cursor.moveToNext();
            }
            cursor.close();
            if (this.list.size() > 0) {
                this.array = new ImageFolder[this.list.size()];
                for (int i = 0; i < this.list.size(); i++) {
                    this.array[i] = this.list.get(i);
                }
            }
        }
    }
}
