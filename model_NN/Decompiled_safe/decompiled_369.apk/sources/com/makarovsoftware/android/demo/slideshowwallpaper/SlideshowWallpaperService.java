package com.makarovsoftware.android.demo.slideshowwallpaper;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class SlideshowWallpaperService extends WallpaperService {
    public static final int IMAGE_MAX_SIZE = 1200;
    public static final String SHARED_PREFS_NAME = "demomakarovsoftwareslideshowwallpapersettings";
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new SlideShowEngine(this);
    }

    class SlideShowEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        private static final int HIGH_DPI_STATUS_BAR_HEIGHT = 38;
        private static final int LOW_DPI_STATUS_BAR_HEIGHT = 19;
        private static final int MEDIUM_DPI_STATUS_BAR_HEIGHT = 25;
        private String currentLayout = "four";
        int mBitmapsCount = 4;
        private int mDpiMode = 0;
        private final Runnable mDrawCube = new Runnable() {
            public void run() {
                SlideShowEngine.this.drawFrame();
            }
        };
        public String mFolders = "";
        private int mHeight = 0;
        private int mInterval = 6;
        private boolean mIsLoaded = false;
        private long mLastLoadTime = 0;
        private long mLastTime;
        private String mLayout = "random";
        /* access modifiers changed from: private */
        public boolean mLocalFoldersEnabled = true;
        private float mOffset;
        private final Paint mPaint = new Paint();
        /* access modifiers changed from: private */
        public boolean mPicasaFoldersEnabled = false;
        /* access modifiers changed from: private */
        public String mPicasaTags = null;
        private SharedPreferences mPrefs;
        private boolean mReloadImages = false;
        /* access modifiers changed from: private */
        public SlideshowWallpaperService mService = null;
        private long mStartTime;
        /* access modifiers changed from: private */
        public int mStatusBarHeight = 0;
        private float mTouchX = -1.0f;
        private float mTouchY = -1.0f;
        private String mUnit = "seconds";
        private boolean mVisible;
        private int mWidth = 0;
        private String msgStr = "";
        private String noImages = "no images found";
        private Random random = new Random();
        /* access modifiers changed from: private */
        public ArrayList<Bitmap> resizedBitmaps = null;

        SlideShowEngine(SlideshowWallpaperService service) {
            super(SlideshowWallpaperService.this);
            this.mService = service;
            Paint paint = this.mPaint;
            paint.setColor(-1);
            paint.setAntiAlias(true);
            this.mStartTime = SystemClock.elapsedRealtime();
            this.random.setSeed(this.mStartTime);
            this.mLastTime = 0;
            this.mPrefs = SlideshowWallpaperService.this.getSharedPreferences(SlideshowWallpaperService.SHARED_PREFS_NAME, 0);
            this.mPrefs.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(this.mPrefs, null);
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
        }

        public void onDestroy() {
            super.onDestroy();
            SlideshowWallpaperService.this.mHandler.removeCallbacks(this.mDrawCube);
        }

        public void onVisibilityChanged(boolean visible) {
            this.mVisible = visible;
            if (visible) {
                try {
                    drawFrame();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                SlideshowWallpaperService.this.mHandler.removeCallbacks(this.mDrawCube);
            }
            SlideshowWallpaperService.this.mHandler.postDelayed(this.mDrawCube, 40);
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            this.mStatusBarHeight = getStatusBarHeight();
            if (!(this.mWidth == width && this.mHeight == height)) {
                this.mReloadImages = true;
            }
            this.mWidth = width;
            this.mHeight = height;
            drawFrame();
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.mVisible = false;
            SlideshowWallpaperService.this.mHandler.removeCallbacks(this.mDrawCube);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xStep, float yStep, int xPixels, int yPixels) {
            this.mOffset = xOffset;
            drawFrame();
        }

        /* access modifiers changed from: package-private */
        public void drawFrame() {
            long now = SystemClock.elapsedRealtime();
            if (isVisible() || (!this.mIsLoaded && now > 120000)) {
                SurfaceHolder holder = getSurfaceHolder();
                try {
                    Canvas c = holder.lockCanvas();
                    if (c != null) {
                        drawImage(c);
                    }
                    if (c != null) {
                        holder.unlockCanvasAndPost(c);
                    }
                } catch (Exception e) {
                    Exception e2 = e;
                    this.msgStr = e2.getMessage();
                    e2.printStackTrace();
                    if (0 != 0) {
                        holder.unlockCanvasAndPost(null);
                    }
                } catch (Throwable th) {
                    if (0 != 0) {
                        holder.unlockCanvasAndPost(null);
                    }
                    throw th;
                }
            }
            SlideshowWallpaperService.this.mHandler.removeCallbacks(this.mDrawCube);
            if (this.mVisible) {
                SlideshowWallpaperService.this.mHandler.postDelayed(this.mDrawCube, 500);
            }
        }

        private int getStatusBarHeight() {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) SlideshowWallpaperService.this.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            switch (displayMetrics.densityDpi) {
                case 120:
                    this.mDpiMode = 0;
                    return LOW_DPI_STATUS_BAR_HEIGHT;
                case 160:
                    this.mDpiMode = 1;
                    return MEDIUM_DPI_STATUS_BAR_HEIGHT;
                case 240:
                    this.mDpiMode = 2;
                    return HIGH_DPI_STATUS_BAR_HEIGHT;
                default:
                    return MEDIUM_DPI_STATUS_BAR_HEIGHT;
            }
        }

        private String getRandomLayout() {
            switch (this.random.nextInt(3)) {
                case 0:
                    return "one";
                case 1:
                    return "two";
                case 2:
                    return "four";
                default:
                    return "one";
            }
        }

        private boolean isResizedBitmapsNull() {
            return this.resizedBitmaps == null || this.resizedBitmaps.size() == 0 || (this.currentLayout.equals("one") && this.resizedBitmaps.get(0) == null) || ((this.currentLayout.equals("two") && this.resizedBitmaps.get(0) == null && this.resizedBitmaps.get(1) == null) || (this.currentLayout.equals("four") && this.resizedBitmaps.get(0) == null && this.resizedBitmaps.get(1) == null && this.resizedBitmaps.get(2) == null && this.resizedBitmaps.get(3) == null));
        }

        private void initArray(int count) {
            if (this.resizedBitmaps != null) {
                this.resizedBitmaps.clear();
            }
            this.resizedBitmaps = new ArrayList<>(count);
        }

        /* access modifiers changed from: package-private */
        public void drawImage(Canvas c) {
            long now = SystemClock.elapsedRealtime();
            long interval = (long) (this.mInterval * (this.mUnit.equals("minutes") ? 60 : 1) * (this.mUnit.equals("hours") ? 60 : 1));
            BitmapLoader bl = new BitmapLoader(this.mService, null, this.resizedBitmaps, this.mBitmapsCount);
            bl.folders = this.mFolders;
            bl.mStatusBarHeight = this.mStatusBarHeight;
            bl.mLocalFoldersEnabled = this.mLocalFoldersEnabled;
            bl.mPicasaFoldersEnabled = this.mPicasaFoldersEnabled;
            bl.mPicasaTags = this.mPicasaTags;
            if (!bl.notReady()) {
                if ((now - this.mLastLoadTime) / 1000 >= 60 || this.mReloadImages || BitmapLoader.urls.size() == 0 || BitmapLoader.loadedUrls.size() == 0) {
                    BitmapLoader.isLoading = false;
                    BitmapLoader.isLoadingFiles = false;
                    bl.loadPicasaFolders();
                    this.mLastLoadTime = now;
                }
                if ((now - this.mLastTime) / 1000 >= 6 || this.mReloadImages || (!this.mIsLoaded && (now - this.mLastTime) / 1000 >= 6)) {
                    this.mReloadImages = false;
                    this.currentLayout = getRandomLayout();
                    initArray(this.mBitmapsCount);
                    BitmapLoader bl2 = new BitmapLoader(this.mService, null, this.resizedBitmaps, this.mBitmapsCount);
                    bl2.folders = this.mFolders;
                    bl2.mStatusBarHeight = this.mStatusBarHeight;
                    bl2.mLocalFoldersEnabled = this.mLocalFoldersEnabled;
                    bl2.mPicasaFoldersEnabled = this.mPicasaFoldersEnabled;
                    bl2.mPicasaTags = this.mPicasaTags;
                    bl2.prepareResizedBitmaps(c, now, this.currentLayout);
                    if (!this.mIsLoaded && !isResizedBitmapsNull()) {
                        this.mIsLoaded = true;
                    }
                    if (isResizedBitmapsNull()) {
                        this.mIsLoaded = false;
                    }
                    this.mLastTime = now;
                }
                c.save();
                c.drawColor(-16777216);
                Paint p = this.mPaint;
                p.setColor(-1);
                p.setTextAlign(Paint.Align.LEFT);
                p.setAntiAlias(true);
                p.setTextSize(15.0f);
                p.setTypeface(Typeface.SERIF);
                if (this.currentLayout.equals("one")) {
                    if (this.resizedBitmaps.get(0) == null || this.msgStr.length() != 0) {
                        c.drawText(this.noImages, 100.0f, 100.0f, p);
                    } else {
                        c.drawBitmap(this.resizedBitmaps.get(0), 0.0f, (float) this.mStatusBarHeight, p);
                    }
                }
                if (this.currentLayout.equals("two")) {
                    if (this.mWidth > this.mHeight) {
                        if (this.resizedBitmaps.get(0) != null) {
                            c.drawBitmap(this.resizedBitmaps.get(0), 0.0f, (float) this.mStatusBarHeight, p);
                        } else {
                            c.drawText(this.noImages, (float) (this.mWidth / 4), (float) (this.mHeight / 4), p);
                        }
                        if (this.resizedBitmaps.get(1) != null) {
                            c.drawBitmap(this.resizedBitmaps.get(1), (float) (this.mWidth / 2), (float) this.mStatusBarHeight, p);
                        } else {
                            c.drawText(this.noImages, (float) (this.mWidth / 4), (float) ((this.mHeight / 4) * 3), p);
                        }
                    } else {
                        if (this.resizedBitmaps.get(0) != null) {
                            c.drawBitmap(this.resizedBitmaps.get(0), 0.0f, (float) this.mStatusBarHeight, p);
                        } else {
                            c.drawText(this.noImages, (float) (this.mWidth / 4), (float) (this.mHeight / 4), p);
                        }
                        if (this.resizedBitmaps.get(1) != null) {
                            c.drawBitmap(this.resizedBitmaps.get(1), 0.0f, (float) (this.mStatusBarHeight + ((this.mHeight - this.mStatusBarHeight) / 2)), p);
                        } else {
                            c.drawText(this.noImages, (float) (this.mWidth / 4), (float) ((this.mHeight / 4) * 3), p);
                        }
                    }
                }
                if (this.currentLayout.equals("four")) {
                    if (this.resizedBitmaps.get(0) != null) {
                        c.drawBitmap(this.resizedBitmaps.get(0), 0.0f, (float) this.mStatusBarHeight, p);
                    } else {
                        c.drawText(this.noImages, (float) (this.mWidth / 4), (float) (this.mHeight / 4), p);
                    }
                    if (this.resizedBitmaps.get(1) != null) {
                        c.drawBitmap(this.resizedBitmaps.get(1), (float) (this.mWidth / 2), (float) this.mStatusBarHeight, p);
                    } else {
                        c.drawText(this.noImages, (float) ((this.mWidth / 4) * 3), (float) (this.mHeight / 4), p);
                    }
                    if (this.resizedBitmaps.get(2) != null) {
                        c.drawBitmap(this.resizedBitmaps.get(2), 0.0f, (float) (this.mStatusBarHeight + ((this.mHeight - this.mStatusBarHeight) / 2)), p);
                    } else {
                        c.drawText(this.noImages, (float) (this.mWidth / 4), (float) ((this.mHeight / 4) * 3), p);
                    }
                    if (this.resizedBitmaps.get(3) != null) {
                        c.drawBitmap(this.resizedBitmaps.get(3), (float) (this.mWidth / 2), (float) (this.mStatusBarHeight + ((this.mHeight - this.mStatusBarHeight) / 2)), p);
                    } else {
                        c.drawText(this.noImages, (float) ((this.mWidth / 4) * 3), (float) ((this.mHeight / 4) * 3), p);
                    }
                }
                c.restore();
            } else if (now >= 120000) {
                this.mLastTime -= interval;
                this.mLastTime -= 10000;
            }
        }

        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
            loadPrefs(prefs);
        }

        /* access modifiers changed from: private */
        public String getPathForPicasa() {
            return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + SlideshowWallpaperService.this.getPackageName() + "/cache/picasa/";
        }

        private void clearFiles() {
            new Thread(new Runnable() {
                public void run() {
                    String[] files;
                    try {
                        String picasaPath = SlideShowEngine.this.getPathForPicasa();
                        File picasaFolder = new File(SlideShowEngine.this.getPathForPicasa());
                        BitmapLoader.urls.clear();
                        BitmapLoader.loadedUrlsSet.clear();
                        BitmapLoader.loadedUrls.clear();
                        BitmapLoader.loadedFiles = 0;
                        BitmapLoader.isLoading = false;
                        BitmapLoader.isLoadingFiles = false;
                        if (!picasaFolder.delete() && (files = picasaFolder.list()) != null) {
                            for (String str : files) {
                                if (!str.equals(".nomedia")) {
                                    try {
                                        File del = new File(String.valueOf(picasaPath) + str);
                                        if (!del.delete()) {
                                            del.deleteOnExit();
                                        }
                                    } catch (Exception e) {
                                    }
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        if (SlideShowEngine.this.mPicasaFoldersEnabled) {
                            BitmapLoader bl = new BitmapLoader(SlideShowEngine.this.mService, null, SlideShowEngine.this.resizedBitmaps, SlideShowEngine.this.mBitmapsCount);
                            bl.folders = SlideShowEngine.this.mFolders;
                            bl.mStatusBarHeight = SlideShowEngine.this.mStatusBarHeight;
                            bl.mLocalFoldersEnabled = SlideShowEngine.this.mLocalFoldersEnabled;
                            bl.mPicasaFoldersEnabled = SlideShowEngine.this.mPicasaFoldersEnabled;
                            bl.mPicasaTags = SlideShowEngine.this.mPicasaTags;
                            bl.loadPicasaFolders();
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            }).start();
        }

        public void loadPrefs(SharedPreferences prefs) {
            String str = prefs.getString("pref_folders", "");
            if (this.mFolders != null && !this.mFolders.equalsIgnoreCase(str)) {
                this.mReloadImages = true;
            }
            this.mFolders = str;
            boolean b = prefs.getBoolean("localFoldersEnabled", true);
            if (b != this.mLocalFoldersEnabled) {
                this.mReloadImages = true;
            }
            this.mLocalFoldersEnabled = b;
            String str2 = prefs.getString("picasaTags", "");
            if (this.mPicasaTags != null && !str2.equalsIgnoreCase(this.mPicasaTags)) {
                clearFiles();
            }
            this.mPicasaFoldersEnabled = prefs.getBoolean("picasaEnabled", false);
            this.mPicasaTags = prefs.getString("picasaTags", "");
        }
    }
}
