package com.makarovsoftware.android.demo.slideshowwallpaper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
    private int REQUEST_CODE = 1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.button1 /*2131296261*/:
                Toast.makeText(this, "Choose \"Slideshow Wallpaper\" in the list to start it.", 1).show();
                Intent intent = new Intent();
                intent.setAction("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER");
                startActivityForResult(intent, this.REQUEST_CODE);
                return;
            case R.id.button2 /*2131296262*/:
                try {
                    goPurchase();
                    return;
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), e.getMessage(), 1).show();
                    return;
                }
            default:
                return;
        }
    }

    private void goPurchase() {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse("market://details?id=com.makarovsoftware.android.slideshowwallpaper"));
        startActivity(i);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == this.REQUEST_CODE) {
            finish();
        }
    }
}
