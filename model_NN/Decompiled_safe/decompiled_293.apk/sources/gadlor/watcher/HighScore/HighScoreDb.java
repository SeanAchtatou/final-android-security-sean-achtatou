package gadlor.watcher.HighScore;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import gadlor.watcher.MainApplication;
import java.util.ArrayList;
import java.util.UUID;

public class HighScoreDb {
    public static ArrayList<HighScore> getHighScores() {
        SQLiteDatabase db = new HighScoreDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * from HighScores order by score DESC", null);
        ArrayList<HighScore> scoreList = new ArrayList<>();
        while (c.moveToNext()) {
            HighScore score = new HighScore();
            score.CharacterName = c.getString(c.getColumnIndex("CharacterName"));
            score.Score = c.getInt(c.getColumnIndex("Score"));
            score.Epitaph = c.getString(c.getColumnIndex("Epitath"));
            score.UUID = c.getString(c.getColumnIndex("Id"));
            scoreList.add(score);
        }
        c.close();
        db.close();
        return scoreList;
    }

    public static String insertHighScore(int score, String characterName, String Epitath) {
        SQLiteDatabase db = new HighScoreDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getWritableDatabase();
        UUID pKey = UUID.randomUUID();
        ContentValues values = new ContentValues();
        values.put("Score", Integer.valueOf(score));
        values.put("CharacterName", characterName);
        values.put("Epitath", Epitath);
        values.put("Id", pKey.toString());
        db.insert("HighScores", "", values);
        db.close();
        return pKey.toString();
    }
}
