package gadlor.watcher.HighScore;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class HighScoreDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "hiscore.db";
    private static final int DATABASE_VERSION = 2;

    HighScoreDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE HighScores (Id NVARCHAR(36) PRIMARY KEY,CharacterName TEXT,Epitath TEXT,Score INTEGER);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("NWBD", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS HighScores");
        onCreate(db);
    }
}
