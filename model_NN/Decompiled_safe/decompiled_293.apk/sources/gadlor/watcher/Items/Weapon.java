package gadlor.watcher.Items;

import java.util.Random;

public class Weapon extends Item {
    public int damageDie;
    public int damageModifier;
    private Random generator;
    public int hitBonus;
    public int numberofDice;
    public boolean twoHanded;
    public WeaponType wtype;

    public enum WeaponType {
        DAGGER,
        SWORD,
        MACE,
        FLAILCHAIN,
        CLUBHAMMER,
        SPEAR,
        STAFF,
        AXE,
        POLEARM,
        UNARMED
    }

    public Weapon() {
        this.Heading = "IJ";
    }

    public int getDmg() {
        this.generator = new Random();
        int dmg = 0;
        for (int i = 0; i < this.numberofDice; i++) {
            dmg += this.generator.nextInt(this.damageDie + 1);
        }
        return this.damageModifier + dmg;
    }

    public Weapon clone() {
        Weapon clone = new Weapon();
        clone.Description = this.Description;
        clone.Type = this.Type;
        clone.Identified = this.Identified;
        clone.Rarity = this.Rarity;
        clone.Heading = this.Heading;
        clone.Armor = this.Armor;
        clone.Evade = this.Evade;
        clone.HP = this.HP;
        clone.ArmorDie = this.ArmorDie;
        clone.EvadeDie = this.EvadeDie;
        clone.HPDie = this.HPDie;
        clone.StrengthMod = this.StrengthMod;
        clone.DexterityMod = this.DexterityMod;
        clone.ConstitutionMod = this.ConstitutionMod;
        clone.WisdomMod = this.WisdomMod;
        clone.IntelligenceMod = this.IntelligenceMod;
        clone.CharismaMod = this.CharismaMod;
        clone.SpeedMod = this.SpeedMod;
        clone.PerceptionMod = this.PerceptionMod;
        clone.damageDie = this.damageDie;
        clone.numberofDice = this.numberofDice;
        clone.damageModifier = this.damageModifier;
        clone.hitBonus = this.hitBonus;
        clone.twoHanded = this.twoHanded;
        clone.wtype = this.wtype;
        clone.Plural = this.Plural;
        return clone;
    }

    public String toString() {
        return this.Description + "\t" + "{" + this.numberofDice + "d" + this.damageDie + " + " + this.damageModifier + "}, +" + this.Evade;
    }
}
