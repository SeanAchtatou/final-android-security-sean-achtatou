package gadlor.watcher.Items;

public class Bracers extends Item {
    public Bracers() {
        this.Heading = "L";
    }

    public Bracers clone() {
        Bracers clone = new Bracers();
        clone.Description = this.Description;
        clone.Type = this.Type;
        clone.Identified = this.Identified;
        clone.Rarity = this.Rarity;
        clone.Heading = this.Heading;
        clone.Armor = this.Armor;
        clone.Evade = this.Evade;
        clone.HP = this.HP;
        clone.ArmorDie = this.ArmorDie;
        clone.EvadeDie = this.EvadeDie;
        clone.HPDie = this.HPDie;
        clone.StrengthMod = this.StrengthMod;
        clone.DexterityMod = this.DexterityMod;
        clone.ConstitutionMod = this.ConstitutionMod;
        clone.WisdomMod = this.WisdomMod;
        clone.IntelligenceMod = this.IntelligenceMod;
        clone.CharismaMod = this.CharismaMod;
        clone.SpeedMod = this.SpeedMod;
        clone.PerceptionMod = this.PerceptionMod;
        clone.Plural = this.Plural;
        return clone;
    }

    public String toString() {
        return this.Description + "\t" + "+{" + this.Evade + "," + this.Armor + "," + this.HP + "}";
    }
}
