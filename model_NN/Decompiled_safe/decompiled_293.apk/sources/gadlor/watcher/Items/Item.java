package gadlor.watcher.Items;

import com.millennialmedia.android.R;
import java.util.UUID;

public class Item {
    private static /* synthetic */ int[] $SWITCH_TABLE$gadlor$watcher$Items$Item$ItemType;
    public int Armor = 0;
    public int ArmorDie = 0;
    public int CharismaMod;
    public int ConstitutionMod;
    public String Description = "-none-";
    public int DexterityMod;
    public int Evade = 0;
    public int EvadeDie = 0;
    public int HP = 0;
    public int HPDie = 0;
    public String Heading = "-";
    public int HitBonus;
    public UUID ID = UUID.randomUUID();
    public boolean Identified;
    public int IntelligenceMod;
    public int PerceptionMod;
    public boolean Plural;
    public int Rarity;
    public int SpeedMod;
    public int StrengthMod;
    public ItemType Type = ItemType.NONE;
    public int WisdomMod;
    public int X;
    public int Y;

    public enum ItemType {
        HELMET,
        AMULET,
        ARMOR,
        CLOAK,
        BELT,
        GAUNTLET,
        WATCH,
        TATTOO,
        WEAPON,
        SHIELD,
        BOOTS,
        BRACERS,
        TOOL,
        RING,
        POTION,
        NONE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$gadlor$watcher$Items$Item$ItemType() {
        int[] iArr = $SWITCH_TABLE$gadlor$watcher$Items$Item$ItemType;
        if (iArr == null) {
            iArr = new int[ItemType.values().length];
            try {
                iArr[ItemType.AMULET.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ItemType.ARMOR.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ItemType.BELT.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ItemType.BOOTS.ordinal()] = 11;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ItemType.BRACERS.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ItemType.CLOAK.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ItemType.GAUNTLET.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ItemType.HELMET.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ItemType.NONE.ordinal()] = 16;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[ItemType.POTION.ordinal()] = 15;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[ItemType.RING.ordinal()] = 14;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[ItemType.SHIELD.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[ItemType.TATTOO.ordinal()] = 8;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[ItemType.TOOL.ordinal()] = 13;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[ItemType.WATCH.ordinal()] = 7;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[ItemType.WEAPON.ordinal()] = 9;
            } catch (NoSuchFieldError e16) {
            }
            $SWITCH_TABLE$gadlor$watcher$Items$Item$ItemType = iArr;
        }
        return iArr;
    }

    public Item clone() {
        Item clone = new Item();
        clone.Description = this.Description;
        clone.Type = this.Type;
        clone.Identified = this.Identified;
        clone.Rarity = this.Rarity;
        clone.Heading = this.Heading;
        clone.Armor = this.Armor;
        clone.Evade = this.Evade;
        clone.HP = this.HP;
        clone.ArmorDie = this.ArmorDie;
        clone.EvadeDie = this.EvadeDie;
        clone.HPDie = this.HPDie;
        clone.StrengthMod = this.StrengthMod;
        clone.DexterityMod = this.DexterityMod;
        clone.ConstitutionMod = this.ConstitutionMod;
        clone.WisdomMod = this.WisdomMod;
        clone.IntelligenceMod = this.IntelligenceMod;
        clone.CharismaMod = this.CharismaMod;
        clone.SpeedMod = this.SpeedMod;
        clone.PerceptionMod = this.PerceptionMod;
        clone.HitBonus = this.HitBonus;
        clone.Plural = this.Plural;
        clone.X = this.X;
        clone.Y = this.Y;
        return clone;
    }

    public static Item castToItemByType(ItemType type) {
        switch ($SWITCH_TABLE$gadlor$watcher$Items$Item$ItemType()[type.ordinal()]) {
            case 1:
                return new Helmet();
            case 2:
                return new Amulet();
            case R.styleable.MMAdView_refreshInterval:
                return new Armor();
            case R.styleable.MMAdView_accelerate:
                return new Cloak();
            case R.styleable.MMAdView_ignoreDensityScaling:
                return new Belt();
            case R.styleable.MMAdView_age:
                return new Gauntlet();
            case R.styleable.MMAdView_gender:
                return new Watch();
            case R.styleable.MMAdView_zip:
                return new Tattoo();
            case R.styleable.MMAdView_income:
                return new Weapon();
            case R.styleable.MMAdView_keywords:
                return new Shield();
            case R.styleable.MMAdView_ethnicity:
                return new Boots();
            case R.styleable.MMAdView_orientation:
                return new Bracers();
            case R.styleable.MMAdView_marital:
                return new Tool();
            case R.styleable.MMAdView_children:
                return new Ring();
            case R.styleable.MMAdView_education:
                return new Potion();
            default:
                return new Item();
        }
    }

    public String toString() {
        return this.Description;
    }
}
