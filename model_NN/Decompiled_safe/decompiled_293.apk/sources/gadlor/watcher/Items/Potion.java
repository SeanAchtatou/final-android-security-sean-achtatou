package gadlor.watcher.Items;

import gadlor.watcher.Effects.Effect;

public class Potion extends Item {
    public Effect e;

    public Potion() {
        this.Heading = "P";
    }

    public Potion clone() {
        Potion clone = new Potion();
        clone.Description = this.Description;
        clone.Type = this.Type;
        clone.Identified = this.Identified;
        clone.Rarity = this.Rarity;
        clone.Heading = this.Heading;
        clone.Armor = this.Armor;
        clone.Evade = this.Evade;
        clone.HP = this.HP;
        clone.ArmorDie = this.ArmorDie;
        clone.EvadeDie = this.EvadeDie;
        clone.HPDie = this.HPDie;
        clone.StrengthMod = this.StrengthMod;
        clone.DexterityMod = this.DexterityMod;
        clone.ConstitutionMod = this.ConstitutionMod;
        clone.WisdomMod = this.WisdomMod;
        clone.IntelligenceMod = this.IntelligenceMod;
        clone.CharismaMod = this.CharismaMod;
        clone.SpeedMod = this.SpeedMod;
        clone.PerceptionMod = this.PerceptionMod;
        clone.Plural = this.Plural;
        clone.e = this.e;
        return clone;
    }

    public String toString() {
        return this.Description;
    }
}
