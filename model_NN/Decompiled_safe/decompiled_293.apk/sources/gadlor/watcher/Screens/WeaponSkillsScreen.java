package gadlor.watcher.Screens;

import gadlor.watcher.MainApplication;
import gadlor.watcher.WeaponSkills;

public class WeaponSkillsScreen extends GameScreen {
    public String GetMainText() {
        return WeaponSkills.weaponTable();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (unicodeChar != 10) {
            return false;
        }
        MainApplication.getDStack().Pop();
        return false;
    }

    public boolean WrapMainText() {
        return false;
    }
}
