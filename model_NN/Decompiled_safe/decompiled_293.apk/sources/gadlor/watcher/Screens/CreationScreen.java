package gadlor.watcher.Screens;

import gadlor.watcher.MainApplication;
import gadlor.watcher.MapMaker;
import gadlor.watcher.R;
import java.io.IOException;
import java.io.InputStreamReader;

public class CreationScreen extends GameScreen {
    public CreationState CState = CreationState.NAME;
    private String Name = "";

    public enum CreationState {
        NAME,
        RACE,
        CLASS,
        SUMMARY
    }

    public String GetMainText() {
        if (this.CState == CreationState.NAME) {
            return "How are you called?\n" + this.Name;
        }
        if (this.CState == CreationState.RACE) {
            return "Choose a race.";
        }
        if (this.CState == CreationState.CLASS) {
            return "Choose a class.";
        }
        if (this.CState != CreationState.SUMMARY) {
            return "";
        }
        char[] buf = new char[20000];
        try {
            new InputStreamReader(MainApplication.getInstance().getResources().openRawResource(R.raw.beginning)).read(buf, 0, 20000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(buf);
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (this.CState == CreationState.NAME) {
            if ((keyCode == 67 || unicodeChar == 8) && this.Name.length() > 0) {
                this.Name = this.Name.substring(0, this.Name.length() - 1);
            }
            if ((unicodeChar > 64 && unicodeChar < 91) || (unicodeChar > 96 && unicodeChar < 123)) {
                if (this.Name.length() == 0) {
                    this.Name = String.valueOf(this.Name) + Character.toUpperCase((char) unicodeChar);
                } else {
                    this.Name = String.valueOf(this.Name) + ((char) unicodeChar);
                }
            }
            if (unicodeChar == 10 && this.Name.length() != 0) {
                MainApplication.getPlayer().Name = this.Name;
                this.CState = CreationState.SUMMARY;
            }
        } else if (this.CState == CreationState.SUMMARY) {
            MapMaker m = MainApplication.getMapMaker();
            m.makeMap();
            m.setVisible();
            MainApplication.getDStack().Pop();
        }
        return false;
    }

    public boolean WrapMainText() {
        if (this.CState == CreationState.SUMMARY) {
            return true;
        }
        return false;
    }
}
