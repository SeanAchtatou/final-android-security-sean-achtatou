package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;
import gadlor.watcher.StatusMessage;

public class RollingLogScreen extends GameScreen {
    public String GetMainText() {
        return StatusMessage.getLog();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        DisplayStack theStack = MainApplication.getDStack();
        if (unicodeChar != 10) {
            return false;
        }
        theStack.Pop();
        return false;
    }

    public boolean WrapMainText() {
        return true;
    }
}
