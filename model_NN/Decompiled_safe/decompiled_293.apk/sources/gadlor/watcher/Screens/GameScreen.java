package gadlor.watcher.Screens;

public abstract class GameScreen {
    public float Height;
    public String Name;
    public float Width;

    public abstract String GetHUDText();

    public abstract String GetMainText();

    public abstract String GetStatusText();

    public abstract boolean HandleInput(int i, int i2);

    public abstract boolean WrapMainText();
}
