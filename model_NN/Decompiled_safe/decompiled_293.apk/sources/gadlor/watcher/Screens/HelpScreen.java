package gadlor.watcher.Screens;

import gadlor.watcher.MainApplication;
import gadlor.watcher.R;
import gadlor.watcher.ScrollableList;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class HelpScreen extends GameScreen {
    ScrollableList<String> helpList = new ScrollableList<>();

    public HelpScreen() {
        BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(MainApplication.getInstance().getResources().openRawResource(R.raw.keybindings))));
        while (true) {
            try {
                String strLine = br.readLine();
                if (strLine == null) {
                    break;
                }
                this.helpList.add(String.valueOf(strLine) + "\n");
            } catch (IOException e) {
            }
        }
        this.helpList.setLimit(10);
    }

    public String GetMainText() {
        return this.helpList.getText();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[+-] - Scroll [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (unicodeChar == 10) {
            MainApplication.getDStack().Pop();
            return false;
        } else if (unicodeChar == 43) {
            this.helpList.scrollDown();
            return false;
        } else if (unicodeChar != 45) {
            return false;
        } else {
            this.helpList.scrollUp();
            return false;
        }
    }

    public boolean WrapMainText() {
        return true;
    }
}
