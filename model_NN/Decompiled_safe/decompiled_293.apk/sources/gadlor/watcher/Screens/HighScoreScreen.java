package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.HighScore.HighScore;
import gadlor.watcher.HighScore.HighScoreDb;
import gadlor.watcher.MainApplication;
import java.util.ArrayList;

public class HighScoreScreen extends GameScreen {
    private int offset = 0;
    private String playerUUID;
    private ArrayList<HighScore> scoreList;
    private int scorePosition = 0;
    private int scrollOffset = 0;

    public HighScoreScreen(String id) {
        this.playerUUID = id;
    }

    public String GetMainText() {
        this.scoreList = HighScoreDb.getHighScores();
        this.scorePosition = 0;
        this.offset = 0;
        int i = 0;
        while (true) {
            if (i >= this.scoreList.size()) {
                break;
            } else if (this.scoreList.get(i).UUID.compareTo(this.playerUUID) == 0) {
                this.scorePosition = i;
                break;
            } else {
                i++;
            }
        }
        if (this.scorePosition == 0) {
            this.offset = 0;
        } else {
            this.offset = this.scorePosition - 1;
        }
        StringBuilder sb = new StringBuilder();
        int i2 = this.offset + this.scrollOffset;
        while (i2 < this.scoreList.size() && i2 <= this.offset + 1 + this.scrollOffset) {
            if (i2 == this.scorePosition && this.playerUUID != "") {
                sb.append("** ");
            }
            sb.append(String.valueOf(i2 + 1) + ") ");
            sb.append(String.valueOf(this.scoreList.get(i2).CharacterName) + ". " + this.scoreList.get(i2).Score + " pts.\n");
            sb.append(String.valueOf(this.scoreList.get(i2).Epitaph) + "\n");
            i2++;
        }
        return sb.toString();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[+-] - Scroll list [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (unicodeChar == 43) {
            if (this.scrollOffset + 1 + this.offset < this.scoreList.size()) {
                this.scrollOffset++;
            }
        } else if (unicodeChar == 45) {
            if ((this.scrollOffset - 1) + this.offset >= 0) {
                this.scrollOffset--;
            }
        } else if (unicodeChar == 10) {
            DisplayStack dStack = MainApplication.getDStack();
            if (this.playerUUID == "") {
                dStack.Pop();
            } else {
                System.exit(0);
            }
        }
        return false;
    }

    public boolean WrapMainText() {
        return false;
    }
}
