package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;

public class MainMenuScreen extends GameScreen {
    public String GetMainText() {
        StringBuffer sb = new StringBuffer();
        sb.append("Welcome to No Way But Down.\n\n");
        sb.append("Would you like to: \n");
        sb.append("[c] create a new character\n");
        sb.append("[l] load a saved game\n");
        sb.append("[v] view the high scores list\n");
        sb.append("[r] read the manual\n");
        sb.append("[q] quit");
        return sb.toString();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (unicodeChar == 99) {
            DisplayStack theStack = MainApplication.getDStack();
            theStack.Pop();
            theStack.Push(new CreationScreen());
            return false;
        } else if (unicodeChar == 108) {
            MainApplication.getDStack().Push(new LoadSaveScreen());
            return false;
        } else if (unicodeChar == 118) {
            MainApplication.getDStack().Push(new HighScoreScreen(""));
            return false;
        } else if (unicodeChar == 114) {
            MainApplication.getDStack().Push(new ManualScreen());
            return false;
        } else {
            if (unicodeChar == 113) {
                System.exit(0);
            }
            return false;
        }
    }

    public boolean WrapMainText() {
        return true;
    }
}
