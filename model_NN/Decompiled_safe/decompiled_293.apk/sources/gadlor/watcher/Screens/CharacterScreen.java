package gadlor.watcher.Screens;

import gadlor.watcher.MainApplication;
import gadlor.watcher.Player;

public class CharacterScreen extends GameScreen {
    public String GetMainText() {
        Player p = MainApplication.getPlayer();
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(p.Name) + "\n");
        sb.append("Level " + p.Level + "\n");
        sb.append("Strength:      " + p.Strength() + "\n");
        sb.append("Dexterity:     " + p.Dexterity() + "\n");
        sb.append("Constitution:  " + p.Constitution() + "\n");
        sb.append("Intelligence:  " + p.Intelligence() + "\n");
        sb.append("Wisdom:        " + p.Wisdom() + "\n");
        sb.append("Charisma:      " + p.Charisma() + "\n");
        sb.append("Speed:         " + p.Speed() + "\n");
        sb.append("Perception:    " + p.Perception() + "\n");
        sb.append("Experience:    " + p.Exp + "\n");
        sb.append("Next level at: " + ((int) (Math.pow(2.0d, (double) (p.Level - 1)) * 250.0d)) + "\n");
        return sb.toString();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        MainApplication.getDStack().Pop();
        return false;
    }

    public boolean WrapMainText() {
        return false;
    }
}
