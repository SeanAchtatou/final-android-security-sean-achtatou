package gadlor.watcher.Screens;

import gadlor.watcher.GameState;
import gadlor.watcher.HighScore.HighScore;
import gadlor.watcher.HighScore.HighScoreDb;
import gadlor.watcher.MainApplication;
import gadlor.watcher.Player;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DeathScreen extends GameScreen {
    private ArrayList<HighScore> scoreList;
    private String uuid;

    public DeathScreen() {
        Player p = MainApplication.getPlayer();
        int score = (GameState.CurrentLevel * 75) + (p.Level * 100) + (GameState.TurnsElapsed / 20) + (p.Exp / 5);
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(p.Name) + " died after " + GameState.TurnsElapsed + " turns.\n");
        Date date = new Date();
        sb.append(String.valueOf(p.Name) + " died on " + new SimpleDateFormat("yyyy/MM/dd").format(date) + ".\n");
        sb.append("He had advanced to level " + p.Level + ".\n");
        sb.append("He was killed by ");
        char c = GameState.KillerMonster.charAt(0);
        if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
            sb.append("an " + GameState.KillerMonster + ".\n");
        } else {
            sb.append("a " + GameState.KillerMonster + ".\n");
        }
        sb.append("He was on level " + GameState.CurrentLevel + " of the dungeon.\n");
        this.uuid = HighScoreDb.insertHighScore(score, p.Name, sb.toString());
    }

    public String GetMainText() {
        Player p = MainApplication.getPlayer();
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(p.Name) + " died after " + GameState.TurnsElapsed + " turns.\n");
        sb.append("He was on level " + GameState.CurrentLevel + " of the dungeon.\n");
        sb.append("He had gained " + (p.Level - 1) + " levels and earned " + p.Exp + " experience.\n");
        sb.append("We will never forget him.");
        sb.append("\n\n\n[press enter to view high scores]");
        return sb.toString();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        if (unicodeChar != 10) {
            return false;
        }
        MainApplication.getDStack().Push(new HighScoreScreen(this.uuid));
        return false;
    }

    public boolean WrapMainText() {
        return true;
    }
}
