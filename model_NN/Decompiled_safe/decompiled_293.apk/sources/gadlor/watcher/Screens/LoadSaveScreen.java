package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;
import gadlor.watcher.SaveGame.SaveGame;
import gadlor.watcher.SaveGame.SaveGameDb;
import java.util.ArrayList;

public class LoadSaveScreen extends GameScreen {
    public ArrayList<SaveGame> savedGames;

    public LoadSaveScreen() {
        this.savedGames = new ArrayList<>();
        this.savedGames = SaveGameDb.getSavedChars();
    }

    public String GetMainText() {
        StringBuffer sb = new StringBuffer();
        sb.append("Choose a saved game to load: \n\n");
        for (int i = 0; i < this.savedGames.size(); i++) {
            sb.append(String.valueOf(i + 1) + ") ");
            sb.append(this.savedGames.get(i).CharacterName);
            sb.append(". Level " + this.savedGames.get(i).CharacterLevel + ". Dungeon level " + this.savedGames.get(i).DungeonLevel + ".");
            sb.append("\n");
        }
        return sb.toString();
    }

    public String GetStatusText() {
        return null;
    }

    public String GetHUDText() {
        return "[1-n] - Load game [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        int size;
        int index;
        DisplayStack theStack = MainApplication.getDStack();
        if (unicodeChar == 10) {
            theStack.Pop();
            return false;
        } else if (unicodeChar < 48 || unicodeChar > 57 || (index = (unicodeChar - 48) - 1) >= (size = this.savedGames.size()) || size == 0) {
            return false;
        } else {
            this.savedGames.get(index).Load();
            theStack.RevertToBase();
            return false;
        }
    }

    public boolean WrapMainText() {
        return false;
    }
}
