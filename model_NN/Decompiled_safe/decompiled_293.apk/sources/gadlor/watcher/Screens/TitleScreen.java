package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.GameState;
import gadlor.watcher.MainApplication;
import gadlor.watcher.R;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TitleScreen extends GameScreen {
    public String GetMainText() {
        InputStream file;
        if (!GameState.Portrait) {
            file = MainApplication.getInstance().getResources().openRawResource(R.raw.titlelandscape);
        } else {
            file = MainApplication.getInstance().getResources().openRawResource(R.raw.titleportrait);
        }
        char[] buf = new char[20000];
        try {
            new InputStreamReader(file).read(buf, 0, 20000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(buf);
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        DisplayStack theStack = MainApplication.getDStack();
        theStack.Pop();
        theStack.Push(new MainMenuScreen());
        return false;
    }

    public boolean WrapMainText() {
        return false;
    }
}
