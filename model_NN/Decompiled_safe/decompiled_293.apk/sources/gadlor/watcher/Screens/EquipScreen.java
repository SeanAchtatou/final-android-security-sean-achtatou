package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;
import gadlor.watcher.Player;

public class EquipScreen extends GameScreen {
    private int EquipCategory;
    private int InventoryOffset = 0;

    EquipScreen(int category) {
        this.EquipCategory = category;
    }

    public String GetMainText() {
        return MainApplication.getPlayer().Inv.printByCategory(this.EquipCategory, this.InventoryOffset);
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[a-o] - Equip item [+-] - Scroll items [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        Player p = MainApplication.getPlayer();
        DisplayStack theStack = MainApplication.getDStack();
        if (unicodeChar >= 97 && unicodeChar <= 111) {
            p.Inv.equipByCategoryAndIndex(this.EquipCategory, unicodeChar - 97, this.InventoryOffset);
            theStack.RevertToBase();
            return true;
        } else if (unicodeChar == 10) {
            theStack.Pop();
            return false;
        } else if (unicodeChar == 43) {
            if (this.InventoryOffset + 1 < p.Inv.Items.size()) {
                this.InventoryOffset++;
            }
            return false;
        } else {
            if (unicodeChar == 45 && this.InventoryOffset > 0) {
                this.InventoryOffset--;
            }
            return false;
        }
    }

    public boolean WrapMainText() {
        return false;
    }
}
