package gadlor.watcher.Screens;

import android.util.DisplayMetrics;
import android.view.WindowManager;
import gadlor.watcher.CombatEngine;
import gadlor.watcher.ConfirmationDialogue;
import gadlor.watcher.DisplayStack;
import gadlor.watcher.GameState;
import gadlor.watcher.MainApplication;
import gadlor.watcher.MapMaker;
import gadlor.watcher.Monster;
import gadlor.watcher.Player;
import gadlor.watcher.SaveGame.SaveGameDb;
import gadlor.watcher.StatusMessage;
import gadlor.watcher.WalkNode;
import java.util.ArrayList;

public class DungeonScreen extends GameScreen {
    boolean gameSaving = false;
    ArrayList<ConfirmationDialogue> openDialogue = new ArrayList<>();
    boolean scrollStatus = false;

    public DungeonScreen(String n, int w, int h) {
        this.Name = n;
        this.Width = (float) w;
        this.Height = (float) h;
    }

    public String GetMainText() {
        MapMaker m = MainApplication.getMapMaker();
        Player p = MainApplication.getPlayer();
        char[][] tmpmap = m.getMap();
        tmpmap[p.getX()][p.getY()] = '@';
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) MainApplication.getInstance().getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        this.Width = ((float) metrics.widthPixels) * metrics.density;
        this.Height = ((float) metrics.heightPixels) * metrics.density;
        float fontSize = 12.0f * metrics.density;
        if (((float) p.getX()) > ((this.Width / fontSize) / 2.0f) + ((float) GameState.ScreenShiftX)) {
            GameState.ScreenShiftX = (int) (((float) p.getX()) - ((this.Width / fontSize) / 2.0f));
        }
        if (((float) p.getX()) < ((this.Width / fontSize) / 2.0f) + ((float) GameState.ScreenShiftX)) {
            GameState.ScreenShiftX = (int) (((float) p.getX()) - ((this.Width / fontSize) / 2.0f));
        }
        if (GameState.ScreenShiftX < 0) {
            GameState.ScreenShiftX = 0;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < m.getHeight(); i++) {
            for (int j = GameState.ScreenShiftX; j < m.getWidth(); j++) {
                sb.append(tmpmap[j][i]);
            }
            if (i + 1 < m.getHeight()) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public String GetStatusText() {
        if (!this.openDialogue.isEmpty()) {
            return this.openDialogue.get(0).getMessage();
        }
        String msg = StatusMessage.getMessage();
        if (((float) (msg.length() * 14)) > this.Width * 2.0f) {
            this.scrollStatus = true;
            int lastSpace = msg.substring(0, ((int) ((this.Width * 2.0f) / 14.0f)) - 6).lastIndexOf(" ");
            String substr = msg.substring(0, lastSpace);
            StatusMessage.delete(0, lastSpace);
            return String.valueOf(substr) + "-more-";
        }
        this.scrollStatus = false;
        StatusMessage.clear();
        return msg;
    }

    public String GetHUDText() {
        Player p = MainApplication.getPlayer();
        return String.valueOf(p.Name) + " " + "HP:" + p.CurrentHealth + "(" + p.HP() + ")" + " Level:" + p.Level + " DL:" + GameState.CurrentLevel + " XP:" + p.Exp + " Evade: " + p.Evade() + " Armor: " + p.Armor();
    }

    /* Debug info: failed to restart local var, previous not found, register: 19 */
    public boolean HandleInput(int unicodeChar, int keyCode) {
        boolean updateWorld = false;
        Player p = MainApplication.getPlayer();
        MapMaker m = MainApplication.getMapMaker();
        DisplayStack theStack = MainApplication.getDStack();
        CombatEngine cEngine = CombatEngine.getInstance();
        WalkNode[][] walkMap = m.getWalkMap();
        if (!this.openDialogue.isEmpty()) {
            this.openDialogue.get(0).inputChar(unicodeChar);
            unicodeChar = this.openDialogue.get(0).character();
        }
        if (this.scrollStatus) {
            return false;
        }
        if (this.gameSaving) {
            System.exit(0);
        }
        if (unicodeChar == 10) {
            updateWorld = true;
        }
        if (p.CurrentHealth <= 0) {
            theStack.Push(new DeathScreen());
            updateWorld = false;
        }
        if (keyCode == 20) {
            if (!cEngine.checkAndHandleCollision(p.getX(), p.getY() + 1)) {
                p.setY(p.getY() + 1);
            }
            updateWorld = true;
        } else if (keyCode == 21) {
            if (!cEngine.checkAndHandleCollision(p.getX() - 1, p.getY())) {
                p.setX(p.getX() - 1);
            }
            updateWorld = true;
        } else if (keyCode == 22) {
            if (!cEngine.checkAndHandleCollision(p.getX() + 1, p.getY())) {
                p.setX(p.getX() + 1);
            }
            updateWorld = true;
        } else if (keyCode == 19) {
            if (!cEngine.checkAndHandleCollision(p.getX(), p.getY() - 1)) {
                p.setY(p.getY() - 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 113) {
            if (!cEngine.checkAndHandleCollision(p.getX() - 1, p.getY() - 1)) {
                p.setY(p.getY() - 1);
                p.setX(p.getX() - 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 119) {
            if (!cEngine.checkAndHandleCollision(p.getX(), p.getY() - 1)) {
                p.setY(p.getY() - 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 101) {
            if (!cEngine.checkAndHandleCollision(p.getX() + 1, p.getY() - 1)) {
                p.setY(p.getY() - 1);
                p.setX(p.getX() + 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 97) {
            if (!cEngine.checkAndHandleCollision(p.getX() - 1, p.getY())) {
                p.setX(p.getX() - 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 100) {
            if (!cEngine.checkAndHandleCollision(p.getX() + 1, p.getY())) {
                p.setX(p.getX() + 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 122) {
            if (!cEngine.checkAndHandleCollision(p.getX() - 1, p.getY() + 1)) {
                p.setX(p.getX() - 1);
                p.setY(p.getY() + 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 120) {
            if (!cEngine.checkAndHandleCollision(p.getX(), p.getY() + 1)) {
                p.setY(p.getY() + 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 99) {
            if (!cEngine.checkAndHandleCollision(p.getX() + 1, p.getY() + 1)) {
                p.setX(p.getX() + 1);
                p.setY(p.getY() + 1);
            }
            updateWorld = true;
        } else if (unicodeChar == 109) {
            theStack.Push(new RollingLogScreen());
            updateWorld = false;
        } else if (unicodeChar == 68) {
            theStack.Push(new DropItemScreen());
            updateWorld = false;
        } else if (unicodeChar == 87) {
            if (p.DualWielding()) {
                StatusMessage.append("Left hand - Damage: " + p.getLWeaponDamageDescription() + " To hit: +" + (p.getLHitBonus() - 5) + "\n");
                StatusMessage.append("Right hand - Damage: " + p.getRWeaponDamageDescription() + " To hit: +" + (p.getRHitBonus() - 3));
            } else {
                StatusMessage.append("Damage: " + p.getWeaponDamageDescription() + " To hit: +" + p.HitBonus());
            }
            updateWorld = false;
        } else if (unicodeChar == 77) {
            theStack.Push(new WeaponSkillsScreen());
            updateWorld = false;
        } else if (unicodeChar == 62) {
            if (m.walkmap[p.getX()][p.getY()].Stairs) {
                StatusMessage.append("You descend further.");
                m.makeMap();
                GameState.CurrentLevel++;
                updateWorld = false;
                m.setVisible();
            }
        } else if (unicodeChar == 105) {
            theStack.Push(new InventoryScreen());
            updateWorld = false;
        } else if (unicodeChar == 63) {
            theStack.Push(new HelpScreen());
            updateWorld = false;
        } else if (unicodeChar == 33) {
            theStack.Push(new CharacterScreen());
            updateWorld = false;
        } else if (unicodeChar == 44) {
            int numberOfItems = m.walkmap[p.getX()][p.getY()].Items.size();
            if (numberOfItems > 0) {
                for (int i = 0; i < numberOfItems; i++) {
                    p.Inv.Items.add(0, m.walkmap[p.getX()][p.getY()].Items.get(i));
                    m.itemList.remove(m.walkmap[p.getX()][p.getY()].Items.get(i));
                    StatusMessage.append("You pick up the " + m.walkmap[p.getX()][p.getY()].Items.get(i).Description + ".");
                }
                m.walkmap[p.getX()][p.getY()].Items.clear();
                if (m.walkmap[p.getX()][p.getY()].Stairs) {
                    m.walkmap[p.getX()][p.getY()].DisplayChar = '>';
                } else {
                    m.walkmap[p.getX()][p.getY()].DisplayChar = '.';
                }
                updateWorld = true;
            } else {
                updateWorld = false;
            }
        } else if (unicodeChar == 85) {
            theStack.Push(new UseScreen());
            updateWorld = false;
        } else if (unicodeChar == 81) {
            if (this.openDialogue.size() == 0) {
                this.openDialogue.add(new ConfirmationDialogue("Do you really want to quit? [y/n]", unicodeChar));
            } else if (this.openDialogue.get(0).set) {
                if (this.openDialogue.get(0).confirmed()) {
                    System.exit(0);
                } else {
                    this.openDialogue.remove(0);
                }
            }
        } else if (unicodeChar == 82) {
            boolean awareMonster = false;
            ArrayList<Monster> monsterList = MainApplication.getMonsterList();
            for (int i2 = 0; i2 < monsterList.size(); i2++) {
                if (monsterList.get(i2).Aware) {
                    if (monsterList.get(i2).Name.contains("bat")) {
                        StatusMessage.append("Can't rest here. This is bat country.");
                    } else {
                        StatusMessage.append("You cannot rest while monsters are nearby.");
                    }
                    awareMonster = true;
                }
            }
            if (!awareMonster) {
                StatusMessage.append("You rest.");
                GameState.PlayerResting = true;
                updateWorld = true;
            }
        } else if (unicodeChar == 83) {
            SaveGameDb.writeNewSave();
            StatusMessage.append("Game saved. Quitting...");
            this.gameSaving = true;
            updateWorld = false;
        }
        return updateWorld;
    }

    public boolean WrapMainText() {
        return false;
    }
}
