package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;
import gadlor.watcher.Player;
import gadlor.watcher.StatusMessage;

public class InventoryScreen extends GameScreen {
    public String GetMainText() {
        return MainApplication.getPlayer().Inv.printEquipped();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[v] - View items [a-o] - Equip / unequip [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        Player p = MainApplication.getPlayer();
        DisplayStack theStack = MainApplication.getDStack();
        boolean updateWorld = false;
        if (unicodeChar >= 97 && unicodeChar <= 111) {
            if (!p.Inv.isEquipped(unicodeChar - 97)) {
                theStack.Push(new EquipScreen(unicodeChar - 97));
                updateWorld = false;
            } else if (unicodeChar == 104) {
                updateWorld = false;
                StatusMessage.append("You cannot remove a tattoo. ");
                theStack.Pop();
            } else {
                p.Inv.unequipItem(unicodeChar - 97);
                updateWorld = false;
            }
        }
        if (unicodeChar == 10) {
            updateWorld = false;
            theStack.Pop();
        }
        if (unicodeChar != 118) {
            return updateWorld;
        }
        theStack.Push(new CarriedItemsScreen());
        return false;
    }

    public boolean WrapMainText() {
        return true;
    }
}
