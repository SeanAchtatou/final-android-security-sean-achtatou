package gadlor.watcher.Screens;

import gadlor.watcher.DisplayStack;
import gadlor.watcher.MainApplication;
import gadlor.watcher.Player;
import gadlor.watcher.ScrollableList;

public class CarriedItemsScreen extends GameScreen {
    ScrollableList<String> itemList = new ScrollableList<>();

    CarriedItemsScreen() {
        Player p = MainApplication.getPlayer();
        this.itemList.setLimit(10);
        this.itemList.setCollection(p.Inv.printCarried());
    }

    public String GetMainText() {
        return this.itemList.getTextWithHeading();
    }

    public String GetStatusText() {
        return "";
    }

    public String GetHUDText() {
        return "[+-] - Scroll items [enter] - exit";
    }

    public boolean HandleInput(int unicodeChar, int keyCode) {
        DisplayStack theStack = MainApplication.getDStack();
        if (unicodeChar == 10) {
            theStack.Pop();
            return false;
        } else if (unicodeChar == 43) {
            this.itemList.scrollDown();
            return false;
        } else if (unicodeChar != 45) {
            return false;
        } else {
            this.itemList.scrollUp();
            return false;
        }
    }

    public boolean WrapMainText() {
        return false;
    }
}
