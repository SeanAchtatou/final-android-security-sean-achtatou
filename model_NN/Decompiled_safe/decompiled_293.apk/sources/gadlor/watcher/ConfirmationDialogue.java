package gadlor.watcher;

public class ConfirmationDialogue {
    public boolean confirmed = false;
    private String message;
    int originalCharacter;
    public boolean set = false;

    public ConfirmationDialogue(String _message, int unicodeChar) {
        this.message = _message;
        this.originalCharacter = unicodeChar;
    }

    public String getMessage() {
        return this.message;
    }

    public void inputChar(int unicodeChar) {
        if (unicodeChar == 121) {
            this.confirmed = true;
            this.set = true;
        } else if (unicodeChar == 110) {
            this.confirmed = false;
            this.set = true;
        }
    }

    public boolean confirmed() {
        return this.confirmed;
    }

    public boolean set() {
        return this.set;
    }

    public int character() {
        return this.originalCharacter;
    }
}
