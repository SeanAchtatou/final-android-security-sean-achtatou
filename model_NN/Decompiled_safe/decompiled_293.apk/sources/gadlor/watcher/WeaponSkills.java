package gadlor.watcher;

import gadlor.watcher.Items.Weapon;

public class WeaponSkills {
    private static int axeCounter = 0;
    private static int[] axeDmg;
    private static int[] axeEv;
    private static int[] axeHit;
    private static int axeLevel = 0;
    private static int[] axeLvl = {50, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};
    private static long daggerCounter = 0;
    private static int[] daggerDmg;
    private static int[] daggerEv;
    private static int[] daggerHit;
    private static int daggerLevel = 0;
    private static int[] daggerLvl = {50, 200, 400, 600, 900, 1200, 1500, 2000, 3000, 4000, 5500, 7000, 8500, 11000, 15000, 20000, 30000, 40000, 50000};
    private static int[] daggerTwo;
    private static int flailCounter = 0;
    private static int[] flailDmg;
    private static int[] flailEv;
    private static int[] flailHit;
    private static int flailLevel = 0;
    private static int[] flailLvl = {50, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};
    private static long hammerCounter = 0;
    private static int[] hammerDmg;
    private static int[] hammerEv;
    private static int[] hammerHit;
    private static int hammerLevel = 0;
    private static int[] hammerLvl = {50, 200, 300, 500, 750, 1100, 1400, 1750, 2200, 2700, 3500, 5000, 7500, 10000, 13000, 17000, 22500, 30000, 40000};
    private static int maceCounter = 0;
    private static int[] maceDmg;
    private static int[] maceEv;
    private static int[] maceHit;
    private static int maceLevel = 0;
    private static int[] maceLvl = {50, 200, 300, 500, 750, 1100, 1400, 1750, 2200, 2700, 3500, 5000, 7500, 10000, 13000, 17000, 22500, 30000, 40000};
    private static int polearmCounter = 0;
    private static int[] polearmDmg;
    private static int[] polearmEv;
    private static int[] polearmHit;
    private static int polearmLevel = 0;
    private static int[] polearmLvl = {50, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};
    private static int spearCounter = 0;
    private static int[] spearDmg;
    private static int[] spearEv;
    private static int[] spearHit;
    private static int spearLevel = 0;
    private static int[] spearLvl = {50, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};
    private static int staffCounter = 0;
    private static int[] staffDmg;
    private static int[] staffEv;
    private static int[] staffHit;
    private static int staffLevel = 0;
    private static int[] staffLvl = {50, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};
    private static long swordCounter = 100;
    private static int[] swordDmg;
    private static int[] swordEv;
    private static int[] swordHit;
    private static int swordLevel = 1;
    private static int[] swordLvl = {100, 250, 500, 750, 1000, 1300, 1700, 2250, 3100, 4300, 6000, 7500, 10000, 13000, 17000, 22500, 35000, 50000, 75000};

    static {
        int[] iArr = new int[22];
        iArr[1] = 1;
        iArr[2] = 1;
        iArr[3] = 2;
        iArr[4] = 2;
        iArr[5] = 3;
        iArr[6] = 3;
        iArr[7] = 4;
        iArr[8] = 4;
        iArr[9] = 5;
        iArr[10] = 5;
        iArr[11] = 6;
        iArr[12] = 6;
        iArr[13] = 7;
        iArr[14] = 8;
        iArr[15] = 9;
        iArr[16] = 10;
        iArr[17] = 12;
        iArr[18] = 14;
        iArr[19] = 15;
        iArr[20] = 17;
        iArr[21] = 20;
        daggerDmg = iArr;
        int[] iArr2 = new int[22];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 3;
        iArr2[5] = 4;
        iArr2[6] = 4;
        iArr2[7] = 5;
        iArr2[8] = 5;
        iArr2[9] = 6;
        iArr2[10] = 6;
        iArr2[11] = 7;
        iArr2[12] = 7;
        iArr2[13] = 8;
        iArr2[14] = 8;
        iArr2[15] = 9;
        iArr2[16] = 10;
        iArr2[17] = 11;
        iArr2[18] = 12;
        iArr2[19] = 13;
        iArr2[20] = 14;
        iArr2[21] = 15;
        daggerHit = iArr2;
        int[] iArr3 = new int[22];
        iArr3[2] = 1;
        iArr3[3] = 1;
        iArr3[4] = 2;
        iArr3[5] = 2;
        iArr3[6] = 3;
        iArr3[7] = 3;
        iArr3[8] = 3;
        iArr3[9] = 4;
        iArr3[10] = 4;
        iArr3[11] = 4;
        iArr3[12] = 5;
        iArr3[13] = 5;
        iArr3[14] = 5;
        iArr3[15] = 6;
        iArr3[16] = 6;
        iArr3[17] = 6;
        iArr3[18] = 7;
        iArr3[19] = 8;
        iArr3[20] = 9;
        iArr3[21] = 10;
        daggerEv = iArr3;
        int[] iArr4 = new int[20];
        iArr4[0] = -4;
        iArr4[1] = -3;
        iArr4[2] = -2;
        iArr4[3] = -1;
        iArr4[5] = 1;
        iArr4[6] = 2;
        iArr4[7] = 3;
        iArr4[8] = 4;
        iArr4[9] = 5;
        iArr4[10] = 6;
        iArr4[11] = 7;
        iArr4[12] = 8;
        iArr4[13] = 9;
        iArr4[14] = 10;
        iArr4[15] = 11;
        iArr4[16] = 12;
        iArr4[17] = 13;
        iArr4[18] = 14;
        iArr4[19] = 15;
        daggerTwo = iArr4;
        int[] iArr5 = new int[21];
        iArr5[1] = 1;
        iArr5[2] = 2;
        iArr5[3] = 3;
        iArr5[4] = 4;
        iArr5[5] = 5;
        iArr5[6] = 6;
        iArr5[7] = 7;
        iArr5[8] = 8;
        iArr5[9] = 9;
        iArr5[10] = 10;
        iArr5[11] = 12;
        iArr5[12] = 14;
        iArr5[13] = 16;
        iArr5[14] = 18;
        iArr5[15] = 20;
        iArr5[16] = 22;
        iArr5[17] = 24;
        iArr5[18] = 26;
        iArr5[19] = 28;
        iArr5[20] = 30;
        swordDmg = iArr5;
        int[] iArr6 = new int[20];
        iArr6[1] = 1;
        iArr6[2] = 1;
        iArr6[3] = 2;
        iArr6[4] = 2;
        iArr6[5] = 3;
        iArr6[6] = 3;
        iArr6[7] = 4;
        iArr6[8] = 4;
        iArr6[9] = 5;
        iArr6[10] = 5;
        iArr6[11] = 6;
        iArr6[12] = 6;
        iArr6[13] = 7;
        iArr6[14] = 8;
        iArr6[15] = 9;
        iArr6[16] = 10;
        iArr6[17] = 11;
        iArr6[18] = 12;
        iArr6[19] = 13;
        swordHit = iArr6;
        int[] iArr7 = new int[21];
        iArr7[1] = 1;
        iArr7[2] = 2;
        iArr7[3] = 3;
        iArr7[4] = 4;
        iArr7[5] = 5;
        iArr7[6] = 5;
        iArr7[7] = 6;
        iArr7[8] = 6;
        iArr7[9] = 7;
        iArr7[10] = 7;
        iArr7[11] = 8;
        iArr7[12] = 8;
        iArr7[13] = 9;
        iArr7[14] = 10;
        iArr7[15] = 11;
        iArr7[16] = 13;
        iArr7[17] = 14;
        iArr7[18] = 16;
        iArr7[19] = 18;
        iArr7[20] = 22;
        swordEv = iArr7;
        int[] iArr8 = new int[21];
        iArr8[1] = 1;
        iArr8[2] = 2;
        iArr8[3] = 3;
        iArr8[4] = 4;
        iArr8[5] = 5;
        iArr8[6] = 6;
        iArr8[7] = 7;
        iArr8[8] = 8;
        iArr8[9] = 9;
        iArr8[10] = 10;
        iArr8[11] = 11;
        iArr8[12] = 12;
        iArr8[13] = 13;
        iArr8[14] = 14;
        iArr8[15] = 15;
        iArr8[16] = 17;
        iArr8[17] = 18;
        iArr8[18] = 19;
        iArr8[19] = 21;
        iArr8[20] = 25;
        hammerDmg = iArr8;
        int[] iArr9 = new int[20];
        iArr9[1] = 1;
        iArr9[2] = 1;
        iArr9[3] = 2;
        iArr9[4] = 3;
        iArr9[5] = 4;
        iArr9[6] = 4;
        iArr9[7] = 5;
        iArr9[8] = 5;
        iArr9[9] = 6;
        iArr9[10] = 7;
        iArr9[11] = 8;
        iArr9[12] = 9;
        iArr9[13] = 10;
        iArr9[14] = 11;
        iArr9[15] = 12;
        iArr9[16] = 13;
        iArr9[17] = 14;
        iArr9[18] = 14;
        iArr9[19] = 15;
        hammerHit = iArr9;
        int[] iArr10 = new int[21];
        iArr10[1] = 1;
        iArr10[2] = 2;
        iArr10[3] = 3;
        iArr10[4] = 4;
        iArr10[5] = 5;
        iArr10[6] = 6;
        iArr10[7] = 7;
        iArr10[8] = 8;
        iArr10[9] = 9;
        iArr10[10] = 10;
        iArr10[11] = 10;
        iArr10[12] = 11;
        iArr10[13] = 11;
        iArr10[14] = 12;
        iArr10[15] = 12;
        iArr10[16] = 13;
        iArr10[17] = 13;
        iArr10[18] = 14;
        iArr10[19] = 14;
        iArr10[20] = 15;
        hammerEv = iArr10;
        int[] iArr11 = new int[21];
        iArr11[1] = 1;
        iArr11[2] = 2;
        iArr11[3] = 3;
        iArr11[4] = 4;
        iArr11[5] = 5;
        iArr11[6] = 6;
        iArr11[7] = 7;
        iArr11[8] = 8;
        iArr11[9] = 9;
        iArr11[10] = 10;
        iArr11[11] = 11;
        iArr11[12] = 12;
        iArr11[13] = 13;
        iArr11[14] = 14;
        iArr11[15] = 15;
        iArr11[16] = 17;
        iArr11[17] = 18;
        iArr11[18] = 19;
        iArr11[19] = 19;
        iArr11[20] = 20;
        maceDmg = iArr11;
        int[] iArr12 = new int[21];
        iArr12[1] = 1;
        iArr12[2] = 2;
        iArr12[3] = 3;
        iArr12[4] = 4;
        iArr12[5] = 5;
        iArr12[6] = 6;
        iArr12[7] = 7;
        iArr12[8] = 8;
        iArr12[9] = 9;
        iArr12[10] = 10;
        iArr12[11] = 11;
        iArr12[12] = 12;
        iArr12[13] = 13;
        iArr12[14] = 14;
        iArr12[15] = 15;
        iArr12[16] = 17;
        iArr12[17] = 18;
        iArr12[18] = 19;
        iArr12[19] = 19;
        iArr12[20] = 20;
        maceHit = iArr12;
        int[] iArr13 = new int[21];
        iArr13[1] = 1;
        iArr13[2] = 2;
        iArr13[3] = 3;
        iArr13[4] = 4;
        iArr13[5] = 5;
        iArr13[6] = 6;
        iArr13[7] = 7;
        iArr13[8] = 8;
        iArr13[9] = 9;
        iArr13[10] = 10;
        iArr13[11] = 10;
        iArr13[12] = 11;
        iArr13[13] = 11;
        iArr13[14] = 12;
        iArr13[15] = 12;
        iArr13[16] = 13;
        iArr13[17] = 13;
        iArr13[18] = 14;
        iArr13[19] = 14;
        iArr13[20] = 15;
        maceEv = iArr13;
        int[] iArr14 = new int[21];
        iArr14[1] = 1;
        iArr14[2] = 2;
        iArr14[3] = 3;
        iArr14[4] = 4;
        iArr14[5] = 5;
        iArr14[6] = 6;
        iArr14[7] = 7;
        iArr14[8] = 8;
        iArr14[9] = 9;
        iArr14[10] = 10;
        iArr14[11] = 11;
        iArr14[12] = 11;
        iArr14[13] = 12;
        iArr14[14] = 12;
        iArr14[15] = 13;
        iArr14[16] = 13;
        iArr14[17] = 14;
        iArr14[18] = 14;
        iArr14[19] = 15;
        iArr14[20] = 15;
        staffDmg = iArr14;
        int[] iArr15 = new int[21];
        iArr15[1] = 1;
        iArr15[2] = 2;
        iArr15[3] = 3;
        iArr15[4] = 4;
        iArr15[5] = 5;
        iArr15[6] = 6;
        iArr15[7] = 7;
        iArr15[8] = 8;
        iArr15[9] = 9;
        iArr15[10] = 10;
        iArr15[11] = 11;
        iArr15[12] = 12;
        iArr15[13] = 13;
        iArr15[14] = 14;
        iArr15[15] = 15;
        iArr15[16] = 16;
        iArr15[17] = 17;
        iArr15[18] = 18;
        iArr15[19] = 19;
        iArr15[20] = 20;
        staffHit = iArr15;
        int[] iArr16 = new int[21];
        iArr16[1] = 1;
        iArr16[2] = 2;
        iArr16[3] = 3;
        iArr16[4] = 4;
        iArr16[5] = 5;
        iArr16[6] = 6;
        iArr16[7] = 7;
        iArr16[8] = 8;
        iArr16[9] = 9;
        iArr16[10] = 10;
        iArr16[11] = 12;
        iArr16[12] = 14;
        iArr16[13] = 16;
        iArr16[14] = 18;
        iArr16[15] = 20;
        iArr16[16] = 22;
        iArr16[17] = 24;
        iArr16[18] = 26;
        iArr16[19] = 28;
        iArr16[20] = 30;
        staffEv = iArr16;
        int[] iArr17 = new int[21];
        iArr17[1] = 1;
        iArr17[2] = 2;
        iArr17[3] = 3;
        iArr17[4] = 4;
        iArr17[5] = 5;
        iArr17[6] = 6;
        iArr17[7] = 7;
        iArr17[8] = 8;
        iArr17[9] = 9;
        iArr17[10] = 10;
        iArr17[11] = 11;
        iArr17[12] = 11;
        iArr17[13] = 12;
        iArr17[14] = 12;
        iArr17[15] = 13;
        iArr17[16] = 13;
        iArr17[17] = 14;
        iArr17[18] = 14;
        iArr17[19] = 15;
        iArr17[20] = 15;
        flailDmg = iArr17;
        int[] iArr18 = new int[21];
        iArr18[1] = 1;
        iArr18[2] = 2;
        iArr18[3] = 3;
        iArr18[4] = 4;
        iArr18[5] = 5;
        iArr18[6] = 6;
        iArr18[7] = 7;
        iArr18[8] = 8;
        iArr18[9] = 9;
        iArr18[10] = 10;
        iArr18[11] = 11;
        iArr18[12] = 12;
        iArr18[13] = 13;
        iArr18[14] = 14;
        iArr18[15] = 15;
        iArr18[16] = 16;
        iArr18[17] = 17;
        iArr18[18] = 18;
        iArr18[19] = 19;
        iArr18[20] = 20;
        flailHit = iArr18;
        int[] iArr19 = new int[21];
        iArr19[1] = 1;
        iArr19[2] = 2;
        iArr19[3] = 3;
        iArr19[4] = 4;
        iArr19[5] = 5;
        iArr19[6] = 6;
        iArr19[7] = 7;
        iArr19[8] = 8;
        iArr19[9] = 9;
        iArr19[10] = 10;
        iArr19[11] = 11;
        iArr19[12] = 12;
        iArr19[13] = 13;
        iArr19[14] = 14;
        iArr19[15] = 15;
        iArr19[16] = 16;
        iArr19[17] = 17;
        iArr19[18] = 18;
        iArr19[19] = 19;
        iArr19[20] = 20;
        flailEv = iArr19;
        int[] iArr20 = new int[21];
        iArr20[1] = 1;
        iArr20[2] = 2;
        iArr20[3] = 3;
        iArr20[4] = 4;
        iArr20[5] = 5;
        iArr20[6] = 6;
        iArr20[7] = 7;
        iArr20[8] = 8;
        iArr20[9] = 9;
        iArr20[10] = 10;
        iArr20[11] = 11;
        iArr20[12] = 11;
        iArr20[13] = 12;
        iArr20[14] = 12;
        iArr20[15] = 13;
        iArr20[16] = 13;
        iArr20[17] = 14;
        iArr20[18] = 14;
        iArr20[19] = 15;
        iArr20[20] = 15;
        spearDmg = iArr20;
        int[] iArr21 = new int[21];
        iArr21[1] = 1;
        iArr21[2] = 2;
        iArr21[3] = 3;
        iArr21[4] = 4;
        iArr21[5] = 5;
        iArr21[6] = 6;
        iArr21[7] = 7;
        iArr21[8] = 8;
        iArr21[9] = 9;
        iArr21[10] = 10;
        iArr21[11] = 11;
        iArr21[12] = 12;
        iArr21[13] = 13;
        iArr21[14] = 14;
        iArr21[15] = 15;
        iArr21[16] = 16;
        iArr21[17] = 17;
        iArr21[18] = 18;
        iArr21[19] = 19;
        iArr21[20] = 20;
        spearHit = iArr21;
        int[] iArr22 = new int[21];
        iArr22[1] = 1;
        iArr22[2] = 2;
        iArr22[3] = 3;
        iArr22[4] = 4;
        iArr22[5] = 5;
        iArr22[6] = 6;
        iArr22[7] = 7;
        iArr22[8] = 8;
        iArr22[9] = 9;
        iArr22[10] = 10;
        iArr22[11] = 12;
        iArr22[12] = 14;
        iArr22[13] = 16;
        iArr22[14] = 18;
        iArr22[15] = 20;
        iArr22[16] = 22;
        iArr22[17] = 24;
        iArr22[18] = 26;
        iArr22[19] = 28;
        iArr22[20] = 30;
        spearEv = iArr22;
        int[] iArr23 = new int[21];
        iArr23[1] = 1;
        iArr23[2] = 2;
        iArr23[3] = 3;
        iArr23[4] = 4;
        iArr23[5] = 5;
        iArr23[6] = 6;
        iArr23[7] = 7;
        iArr23[8] = 8;
        iArr23[9] = 9;
        iArr23[10] = 10;
        iArr23[11] = 12;
        iArr23[12] = 14;
        iArr23[13] = 16;
        iArr23[14] = 18;
        iArr23[15] = 20;
        iArr23[16] = 24;
        iArr23[17] = 28;
        iArr23[18] = 32;
        iArr23[19] = 34;
        iArr23[20] = 40;
        axeDmg = iArr23;
        int[] iArr24 = new int[21];
        iArr24[1] = 1;
        iArr24[2] = 2;
        iArr24[3] = 3;
        iArr24[4] = 4;
        iArr24[5] = 5;
        iArr24[6] = 6;
        iArr24[7] = 7;
        iArr24[8] = 8;
        iArr24[9] = 9;
        iArr24[10] = 10;
        iArr24[11] = 11;
        iArr24[12] = 12;
        iArr24[13] = 13;
        iArr24[14] = 14;
        iArr24[15] = 15;
        iArr24[16] = 16;
        iArr24[17] = 17;
        iArr24[18] = 18;
        iArr24[19] = 19;
        iArr24[20] = 20;
        axeHit = iArr24;
        int[] iArr25 = new int[21];
        iArr25[1] = 1;
        iArr25[2] = 2;
        iArr25[3] = 3;
        iArr25[4] = 4;
        iArr25[5] = 5;
        iArr25[6] = 5;
        iArr25[7] = 6;
        iArr25[8] = 6;
        iArr25[9] = 7;
        iArr25[10] = 7;
        iArr25[11] = 8;
        iArr25[12] = 8;
        iArr25[13] = 8;
        iArr25[14] = 9;
        iArr25[15] = 9;
        iArr25[16] = 9;
        iArr25[17] = 9;
        iArr25[18] = 10;
        iArr25[19] = 10;
        iArr25[20] = 10;
        axeEv = iArr25;
        int[] iArr26 = new int[21];
        iArr26[1] = 1;
        iArr26[2] = 2;
        iArr26[3] = 3;
        iArr26[4] = 4;
        iArr26[5] = 5;
        iArr26[6] = 6;
        iArr26[7] = 7;
        iArr26[8] = 8;
        iArr26[9] = 9;
        iArr26[10] = 10;
        iArr26[11] = 11;
        iArr26[12] = 11;
        iArr26[13] = 12;
        iArr26[14] = 12;
        iArr26[15] = 13;
        iArr26[16] = 13;
        iArr26[17] = 14;
        iArr26[18] = 14;
        iArr26[19] = 15;
        iArr26[20] = 15;
        polearmDmg = iArr26;
        int[] iArr27 = new int[21];
        iArr27[1] = 1;
        iArr27[2] = 2;
        iArr27[3] = 3;
        iArr27[4] = 4;
        iArr27[5] = 5;
        iArr27[6] = 6;
        iArr27[7] = 7;
        iArr27[8] = 8;
        iArr27[9] = 9;
        iArr27[10] = 10;
        iArr27[11] = 11;
        iArr27[12] = 12;
        iArr27[13] = 13;
        iArr27[14] = 14;
        iArr27[15] = 15;
        iArr27[16] = 16;
        iArr27[17] = 17;
        iArr27[18] = 18;
        iArr27[19] = 19;
        iArr27[20] = 20;
        polearmHit = iArr27;
        int[] iArr28 = new int[21];
        iArr28[1] = 1;
        iArr28[2] = 2;
        iArr28[3] = 3;
        iArr28[4] = 4;
        iArr28[5] = 5;
        iArr28[6] = 6;
        iArr28[7] = 7;
        iArr28[8] = 8;
        iArr28[9] = 9;
        iArr28[10] = 10;
        iArr28[11] = 12;
        iArr28[12] = 14;
        iArr28[13] = 16;
        iArr28[14] = 18;
        iArr28[15] = 20;
        iArr28[16] = 22;
        iArr28[17] = 24;
        iArr28[18] = 26;
        iArr28[19] = 28;
        iArr28[20] = 30;
        polearmEv = iArr28;
    }

    public static void populateMaps() {
    }

    public static void setLevelForType(Weapon.WeaponType type, int level) {
        if (type == Weapon.WeaponType.SWORD) {
            swordLevel = level;
        } else if (type == Weapon.WeaponType.DAGGER) {
            daggerLevel = level;
        } else if (type == Weapon.WeaponType.CLUBHAMMER) {
            hammerLevel = level;
        } else if (type == Weapon.WeaponType.MACE) {
            maceLevel = level;
        } else if (type == Weapon.WeaponType.STAFF) {
            staffLevel = level;
        } else if (type == Weapon.WeaponType.FLAILCHAIN) {
            flailLevel = level;
        } else if (type == Weapon.WeaponType.POLEARM) {
            polearmLevel = level;
        } else if (type == Weapon.WeaponType.SPEAR) {
            spearLevel = level;
        } else if (type == Weapon.WeaponType.AXE) {
            axeLevel = level;
        }
    }

    public static void setCounterForType(Weapon.WeaponType type, int counter) {
        if (type == Weapon.WeaponType.SWORD) {
            swordCounter = (long) counter;
        } else if (type == Weapon.WeaponType.DAGGER) {
            daggerCounter = (long) counter;
        } else if (type == Weapon.WeaponType.CLUBHAMMER) {
            hammerCounter = (long) counter;
        } else if (type == Weapon.WeaponType.MACE) {
            maceCounter = counter;
        } else if (type == Weapon.WeaponType.STAFF) {
            staffCounter = counter;
        } else if (type == Weapon.WeaponType.FLAILCHAIN) {
            flailCounter = counter;
        } else if (type == Weapon.WeaponType.POLEARM) {
            polearmCounter = counter;
        } else if (type == Weapon.WeaponType.SPEAR) {
            spearCounter = counter;
        } else if (type == Weapon.WeaponType.AXE) {
            axeCounter = counter;
        }
    }

    public static long getLevelForType(Weapon.WeaponType type) {
        if (type == Weapon.WeaponType.SWORD) {
            return (long) swordLevel;
        }
        if (type == Weapon.WeaponType.DAGGER) {
            return (long) daggerLevel;
        }
        if (type == Weapon.WeaponType.CLUBHAMMER) {
            return (long) hammerLevel;
        }
        if (type == Weapon.WeaponType.MACE) {
            return (long) maceLevel;
        }
        if (type == Weapon.WeaponType.STAFF) {
            return (long) staffLevel;
        }
        if (type == Weapon.WeaponType.FLAILCHAIN) {
            return (long) flailLevel;
        }
        if (type == Weapon.WeaponType.POLEARM) {
            return (long) polearmLevel;
        }
        if (type == Weapon.WeaponType.SPEAR) {
            return (long) spearLevel;
        }
        if (type == Weapon.WeaponType.AXE) {
            return (long) axeLevel;
        }
        return 0;
    }

    public static long getCounterForType(Weapon.WeaponType type) {
        if (type == Weapon.WeaponType.SWORD) {
            return swordCounter;
        }
        if (type == Weapon.WeaponType.DAGGER) {
            return daggerCounter;
        }
        if (type == Weapon.WeaponType.CLUBHAMMER) {
            return hammerCounter;
        }
        if (type == Weapon.WeaponType.MACE) {
            return (long) maceCounter;
        }
        if (type == Weapon.WeaponType.STAFF) {
            return (long) staffCounter;
        }
        if (type == Weapon.WeaponType.FLAILCHAIN) {
            return (long) flailCounter;
        }
        if (type == Weapon.WeaponType.POLEARM) {
            return (long) polearmCounter;
        }
        if (type == Weapon.WeaponType.SPEAR) {
            return (long) spearCounter;
        }
        if (type == Weapon.WeaponType.AXE) {
            return (long) axeCounter;
        }
        return 0;
    }

    public static String weaponTable() {
        StringBuilder sb = new StringBuilder();
        sb.append("Weapon Skills Table\n\n");
        sb.append("Weapon    Level   Dmg  Hit  Ev LvlIn\n");
        sb.append("------------------------------------\n");
        sb.append("Dagger      " + daggerLevel + "      " + daggerDmg[daggerLevel] + "    " + daggerHit[daggerLevel] + "    " + daggerEv[daggerLevel] + " " + (((long) daggerLvl[daggerLevel]) - daggerCounter) + "\n");
        sb.append("Sword       " + swordLevel + "      " + swordDmg[swordLevel] + "    " + swordHit[swordLevel] + "    " + swordEv[swordLevel] + " " + (((long) swordLvl[swordLevel]) - swordCounter) + "\n");
        sb.append("Hammer/Club " + hammerLevel + "      " + hammerDmg[hammerLevel] + "    " + hammerHit[hammerLevel] + "    " + hammerEv[hammerLevel] + " " + (((long) hammerLvl[hammerLevel]) - hammerCounter) + "\n");
        sb.append("Mace        " + maceLevel + "      " + maceDmg[maceLevel] + "    " + maceHit[maceLevel] + "    " + maceEv[maceLevel] + " " + (maceLvl[maceLevel] - maceCounter) + "\n");
        sb.append("Staff       " + staffLevel + "      " + staffDmg[staffLevel] + "    " + staffHit[staffLevel] + "    " + staffEv[staffLevel] + " " + (staffLvl[staffLevel] - staffCounter) + "\n");
        sb.append("Flail/Chain " + flailLevel + "      " + flailDmg[flailLevel] + "    " + flailHit[flailLevel] + "    " + flailEv[flailLevel] + " " + (flailLvl[flailLevel] - flailCounter) + "\n");
        sb.append("Axe         " + axeLevel + "      " + axeDmg[axeLevel] + "    " + axeHit[axeLevel] + "    " + axeEv[axeLevel] + " " + (axeLvl[axeLevel] - axeCounter) + "\n");
        sb.append("Polearm     " + polearmLevel + "      " + polearmDmg[polearmLevel] + "    " + polearmHit[polearmLevel] + "    " + polearmEv[polearmLevel] + " " + (polearmLvl[polearmLevel] - polearmCounter) + "\n");
        return sb.toString();
    }

    public static void incrementByType(Weapon w) {
        if (w.wtype == Weapon.WeaponType.SWORD) {
            swordCounter++;
            if (swordCounter >= ((long) swordLvl[swordLevel]) && swordLevel < 21) {
                swordLevel++;
                StatusMessage.append("You feel your abilities with your " + w.Description + " improving.");
            }
        } else if (w.wtype == Weapon.WeaponType.DAGGER) {
            daggerCounter++;
            if (daggerCounter >= ((long) daggerLvl[daggerLevel]) && daggerLevel < 21) {
                daggerLevel++;
                StatusMessage.append("You feel your abilities with your " + w.Description + " improving.");
            }
        } else if (w.wtype == Weapon.WeaponType.CLUBHAMMER) {
            hammerCounter++;
            if (hammerCounter >= ((long) hammerLvl[hammerLevel]) && hammerLevel < 21) {
                hammerLevel++;
                StatusMessage.append("You feel your abilities with your " + w.Description + " improving.");
            }
        } else if (w.wtype == Weapon.WeaponType.MACE) {
            maceCounter++;
            if (maceCounter >= maceLvl[maceLevel] && maceLevel < 21) {
                maceLevel++;
                StatusMessage.append("You feel your abilities with your " + w.Description + " improving.");
            }
        } else if (w.wtype == Weapon.WeaponType.STAFF) {
            staffCounter++;
            if (staffCounter >= staffLvl[staffLevel] && staffLevel < 21) {
                staffLevel++;
                StatusMessage.append("You feel your abilities with your " + w.Description + " improving.");
            }
        }
    }

    public static int GetHitBonus(Weapon.WeaponType type) {
        if (type == Weapon.WeaponType.SWORD) {
            return swordHit[swordLevel];
        }
        if (type == Weapon.WeaponType.DAGGER) {
            return daggerHit[daggerLevel];
        }
        if (type == Weapon.WeaponType.CLUBHAMMER) {
            return hammerHit[hammerLevel];
        }
        if (type == Weapon.WeaponType.MACE) {
            return maceHit[maceLevel];
        }
        if (type == Weapon.WeaponType.STAFF) {
            return staffHit[staffLevel];
        }
        return 0;
    }

    public static int GetDmgBonus(Weapon.WeaponType type) {
        if (type == Weapon.WeaponType.SWORD) {
            return swordDmg[swordLevel];
        }
        if (type == Weapon.WeaponType.DAGGER) {
            return daggerDmg[daggerLevel];
        }
        if (type == Weapon.WeaponType.CLUBHAMMER) {
            return hammerDmg[hammerLevel];
        }
        if (type == Weapon.WeaponType.MACE) {
            return maceDmg[maceLevel];
        }
        if (type == Weapon.WeaponType.STAFF) {
            return staffDmg[staffLevel];
        }
        return 0;
    }

    public static int GetEvadeBonus(Weapon.WeaponType type) {
        if (type == Weapon.WeaponType.SWORD) {
            return swordEv[swordLevel];
        }
        if (type == Weapon.WeaponType.DAGGER) {
            return daggerEv[daggerLevel];
        }
        if (type == Weapon.WeaponType.CLUBHAMMER) {
            return hammerEv[hammerLevel];
        }
        if (type == Weapon.WeaponType.MACE) {
            return maceEv[maceLevel];
        }
        if (type == Weapon.WeaponType.STAFF) {
            return staffEv[staffLevel];
        }
        return 0;
    }
}
