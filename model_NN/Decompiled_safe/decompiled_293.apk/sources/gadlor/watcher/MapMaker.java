package gadlor.watcher;

import android.util.Log;
import gadlor.watcher.Items.Item;
import gadlor.watcher.Items.ItemGenerator;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Random;

public class MapMaker {
    public int StairsDownX;
    public int StairsDownY;
    public ArrayList<Item> itemList;
    private int mapHeight;
    private int mapWidth;
    public ArrayList<Monster> monsterList;
    public ArrayList<NPC> npcList;
    public ArrayList<Room> roomList;
    public WalkNode[][] walkmap;

    public int getHeight() {
        return this.mapHeight;
    }

    public int getWidth() {
        return this.mapWidth;
    }

    public void resetVisible() {
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                this.walkmap[j][i].Visible = false;
            }
        }
    }

    public boolean isNodeSurrounded(WalkNode currentNode) {
        boolean notAtMaxHeight;
        boolean notAtMaxWidth;
        if (currentNode.Y + 1 <= this.mapHeight - 1) {
            notAtMaxHeight = true;
        } else {
            notAtMaxHeight = false;
        }
        if (currentNode.X + 1 <= this.mapWidth - 1) {
            notAtMaxWidth = true;
        } else {
            notAtMaxWidth = false;
        }
        if (notAtMaxWidth && this.walkmap[currentNode.X + 1][currentNode.Y].DisplayChar != '#') {
            return false;
        }
        if (currentNode.X != 0 && this.walkmap[currentNode.X - 1][currentNode.Y].DisplayChar != '#') {
            return false;
        }
        if (notAtMaxHeight && this.walkmap[currentNode.X][currentNode.Y + 1].DisplayChar != '#') {
            return false;
        }
        if (currentNode.Y != 0 && this.walkmap[currentNode.X][currentNode.Y - 1].DisplayChar != '#') {
            return false;
        }
        if (notAtMaxHeight && notAtMaxWidth && this.walkmap[currentNode.X + 1][currentNode.Y + 1].DisplayChar != '#') {
            return false;
        }
        if (notAtMaxHeight && currentNode.X != 0 && this.walkmap[currentNode.X - 1][currentNode.Y + 1].DisplayChar != '#') {
            return false;
        }
        if (notAtMaxWidth && currentNode.Y != 0 && this.walkmap[currentNode.X + 1][currentNode.Y - 1].DisplayChar != '#') {
            return false;
        }
        if (currentNode.X == 0 || currentNode.Y == 0 || this.walkmap[currentNode.X - 1][currentNode.Y - 1].DisplayChar == '#') {
            return true;
        }
        return false;
    }

    public String getMapForSave() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                sb.append(this.walkmap[j][i].DisplayChar);
            }
        }
        return sb.toString();
    }

    public String getViewedMapForSave() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                if (this.walkmap[j][i].Viewed) {
                    sb.append('Y');
                } else {
                    sb.append('N');
                }
            }
        }
        return sb.toString();
    }

    public char[][] getMap() {
        char[][] clone = (char[][]) Array.newInstance(Character.TYPE, this.mapWidth, this.mapHeight);
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                clone[j][i] = this.walkmap[j][i].DisplayChar;
                if (this.walkmap[j][i].Items.size() > 0) {
                    clone[j][i] = '&';
                }
                if (!this.walkmap[j][i].Viewed) {
                    clone[j][i] = ' ';
                }
            }
        }
        for (int i2 = 0; i2 < this.monsterList.size(); i2++) {
            Monster m = this.monsterList.get(i2);
            if (this.walkmap[m.X][m.Y].Visible) {
                clone[m.X][m.Y] = m.Appearance;
            }
        }
        for (int i3 = 0; i3 < this.npcList.size(); i3++) {
            NPC n = this.npcList.get(i3);
            if (this.walkmap[n.X][n.Y].Visible) {
                clone[n.X][n.Y] = n.Appearance;
            }
        }
        return clone;
    }

    public WalkNode[][] getWalkMap() {
        WalkNode[][] clone = (WalkNode[][]) Array.newInstance(WalkNode.class, this.mapWidth, this.mapHeight);
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                clone[j][i] = this.walkmap[j][i].clone();
            }
        }
        return clone;
    }

    public DialogueTree loadDialogueTree(int dialogueName) {
        char[] buf = new char[20000];
        int charsRead = 0;
        try {
            charsRead = new InputStreamReader(MainApplication.getInstance().getResources().openRawResource(R.raw.merlindialogue)).read(buf, 0, 20000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] convoArray = CharBuffer.wrap(buf, 0, charsRead).toString().split("\n");
        ArrayList<String> convoArrayList = new ArrayList<>();
        for (String add : convoArray) {
            convoArrayList.add(add);
        }
        for (int i = 0; i < convoArrayList.size(); i++) {
        }
        return new DialogueTree(null);
    }

    public void loadLevel(int levelName) {
        InputStreamReader reader = new InputStreamReader(MainApplication.getInstance().getResources().openRawResource(R.raw.merlin));
        this.mapWidth = 60;
        this.mapHeight = 13;
        this.monsterList = new ArrayList<>();
        this.npcList = new ArrayList<>();
        this.walkmap = (WalkNode[][]) Array.newInstance(WalkNode.class, this.mapWidth, this.mapHeight);
        new Random();
        char[] buf = new char[1];
        for (int i = 0; i < this.mapHeight; i++) {
            int j = 0;
            while (j < this.mapWidth) {
                try {
                    int charsRead = reader.read(buf, 0, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                char c = buf[0];
                if (c == 13 || c == 10) {
                    Log.d("NWBD", "found a space character");
                    Log.d("NWBD", "at j: " + j + " i:" + i);
                    j--;
                } else {
                    if (c == '#') {
                        this.walkmap[j][i] = new WalkNode();
                        this.walkmap[j][i].X = j;
                        this.walkmap[j][i].Y = i;
                        this.walkmap[j][i].DisplayChar = c;
                        this.walkmap[j][i].Walkable = false;
                    }
                    if (c == '.') {
                        this.walkmap[j][i] = new WalkNode();
                        this.walkmap[j][i].X = j;
                        this.walkmap[j][i].Y = i;
                        this.walkmap[j][i].DisplayChar = c;
                        this.walkmap[j][i].Walkable = true;
                    }
                    if (c == '|') {
                        this.walkmap[j][i] = new WalkNode();
                        this.walkmap[j][i].X = j;
                        this.walkmap[j][i].Y = i;
                        this.walkmap[j][i].DisplayChar = c;
                        this.walkmap[j][i].Walkable = false;
                    }
                    if (c == '>') {
                        this.walkmap[j][i] = new WalkNode();
                        this.walkmap[j][i].X = j;
                        this.walkmap[j][i].Y = i;
                        this.walkmap[j][i].DisplayChar = c;
                        this.walkmap[j][i].Walkable = true;
                        this.walkmap[j][i].Stairs = true;
                    }
                }
                j++;
            }
        }
    }

    public void resetLevel() {
        this.mapWidth = 100;
        this.mapHeight = 12;
        this.monsterList = new ArrayList<>();
        this.npcList = new ArrayList<>();
        this.itemList = new ArrayList<>();
        this.walkmap = (WalkNode[][]) Array.newInstance(WalkNode.class, this.mapWidth, this.mapHeight);
        GameState.MonsterSpawn = true;
    }

    public void loadLevelLayoutFromSave(String layout) {
        if (this.mapWidth * this.mapHeight == layout.length()) {
            for (int i = 0; i < this.mapHeight; i++) {
                for (int j = 0; j < this.mapWidth; j++) {
                    this.walkmap[j][i] = new WalkNode();
                    this.walkmap[j][i].X = j;
                    this.walkmap[j][i].Y = i;
                    char c = layout.charAt(0);
                    this.walkmap[j][i].DisplayChar = c;
                    if (c == '.') {
                        this.walkmap[j][i].Walkable = true;
                    } else {
                        this.walkmap[j][i].Walkable = false;
                    }
                    layout = layout.substring(1);
                }
            }
        }
    }

    public void loadVisibleFromSave(String visible) {
        if (this.mapWidth * this.mapHeight == visible.length()) {
            for (int i = 0; i < this.mapHeight; i++) {
                for (int j = 0; j < this.mapWidth; j++) {
                    if (visible.charAt(0) == 'Y') {
                        this.walkmap[j][i].Viewed = true;
                    } else {
                        this.walkmap[j][i].Viewed = false;
                    }
                    visible = visible.substring(1);
                }
            }
        }
    }

    public void setStairsLocation(int x, int y) {
        this.StairsDownX = x;
        this.StairsDownY = y;
        this.walkmap[x][y].Walkable = true;
        this.walkmap[x][y].Stairs = true;
    }

    public void makeMap() {
        int monsterX;
        int monsterY;
        int itemX;
        int itemY;
        int x;
        int y;
        this.mapWidth = 100;
        this.mapHeight = 12;
        this.monsterList = new ArrayList<>();
        this.npcList = new ArrayList<>();
        this.roomList = new ArrayList<>();
        this.itemList = new ArrayList<>();
        this.walkmap = (WalkNode[][]) Array.newInstance(WalkNode.class, this.mapWidth, this.mapHeight);
        Random generator = new Random();
        GameState.MonsterSpawn = true;
        for (int i = 0; i < this.mapHeight; i++) {
            for (int j = 0; j < this.mapWidth; j++) {
                this.walkmap[j][i] = new WalkNode();
                this.walkmap[j][i].X = j;
                this.walkmap[j][i].Y = i;
                if (i == 0 || i == this.mapHeight - 1) {
                    this.walkmap[j][i].DisplayChar = '#';
                    this.walkmap[j][i].Walkable = false;
                } else if (j == 0 || j == this.mapWidth - 1) {
                    this.walkmap[j][i].DisplayChar = '#';
                    this.walkmap[j][i].Walkable = false;
                } else {
                    this.walkmap[j][i].DisplayChar = '.';
                    this.walkmap[j][i].Walkable = true;
                }
            }
        }
        Player p = MainApplication.getPlayer();
        if (generator.nextInt(10) >= 1) {
            for (int i2 = 0; i2 < 6; i2++) {
                makeRoom();
            }
            connectRooms();
            p.setX(this.roomList.get(0).HalfwayX());
            p.setY(this.roomList.get(0).HalfwayY());
        } else {
            for (int i3 = 0; i3 < 0; i3++) {
                int x2 = generator.nextInt(this.mapWidth - 1);
                int y2 = generator.nextInt(this.mapHeight - 1);
                this.walkmap[x2][y2].DisplayChar = '#';
                this.walkmap[x2][y2].Walkable = false;
            }
            do {
                x = generator.nextInt(this.mapWidth - 1);
                y = generator.nextInt(this.mapHeight - 1);
            } while (!this.walkmap[x][y].Walkable);
            p.setX(x);
            p.setY(y);
        }
        do {
            this.StairsDownX = generator.nextInt(this.mapWidth - 2) + 1;
            this.StairsDownY = generator.nextInt(this.mapHeight - 2) + 1;
        } while (!this.walkmap[this.StairsDownX][this.StairsDownY].Walkable);
        this.walkmap[this.StairsDownX][this.StairsDownY].DisplayChar = '>';
        this.walkmap[this.StairsDownX][this.StairsDownY].Walkable = true;
        this.walkmap[this.StairsDownX][this.StairsDownY].Stairs = true;
        int randomItems = DieRoller.RollDieWithMin(8, 4);
        for (int i4 = 0; i4 < randomItems; i4++) {
            Item randomItem = ItemGenerator.generateRarity(GameState.CurrentLevel);
            while (true) {
                itemX = generator.nextInt(this.mapWidth - 2) + 1;
                itemY = generator.nextInt(this.mapHeight - 2) + 1;
                if (!this.walkmap[itemX][itemY].Stairs && this.walkmap[itemX][itemY].Walkable) {
                    break;
                }
            }
            this.walkmap[itemX][itemY].Items.add(randomItem);
            randomItem.X = itemX;
            randomItem.Y = itemY;
            this.itemList.add(randomItem);
        }
        if (GameState.CurrentLevel == 13) {
            StatusMessage.append("Something feels out of place here...");
            for (int i5 = 0; i5 < this.mapHeight; i5++) {
                for (int j2 = 0; j2 < this.mapWidth; j2++) {
                    WalkNode w = this.walkmap[j2][i5];
                    int chance = DieRoller.RollDie("1d4");
                    if (!isNodeSurrounded(w) && !isBoundary(w) && w.DisplayChar == '#' && chance >= 2) {
                        w.DisplayChar = '.';
                        w.Walkable = true;
                        Monster m = MonsterGenerator.generateMonsterByName("living wall");
                        m.setHealth();
                        m.hitBonus = 7;
                        m.damageDie = 5;
                        m.damageDieNumber = 4;
                        m.X = w.X;
                        m.Y = w.Y;
                        this.monsterList.add(m);
                    }
                }
            }
            return;
        }
        int spawnedMonsters = DieRoller.RollDieWithMin(12, 6);
        for (int i6 = 0; i6 < spawnedMonsters; i6++) {
            Monster m2 = MonsterGenerator.getMonsterByLevel(GameState.CurrentLevel);
            while (true) {
                monsterX = generator.nextInt(this.mapWidth - 2) + 1;
                monsterY = generator.nextInt(this.mapHeight - 2) + 1;
                if (!this.walkmap[monsterX][monsterY].Stairs && this.walkmap[monsterX][monsterY].Walkable) {
                    if (p.getX() != monsterX || p.getY() != monsterY) {
                        boolean monsterPresent = false;
                        int j3 = 0;
                        while (true) {
                            if (j3 < this.monsterList.size()) {
                                if (this.monsterList.get(j3).X == monsterX && this.monsterList.get(j3).Y == monsterY) {
                                    monsterPresent = true;
                                    break;
                                }
                                j3++;
                            } else {
                                break;
                            }
                        }
                        if (!monsterPresent) {
                            break;
                        }
                    }
                }
            }
            m2.X = monsterX;
            m2.Y = monsterY;
            this.monsterList.add(m2);
        }
    }

    public void setVisible() {
        resetVisible();
        Player p = MainApplication.getPlayer();
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                if (Math.pow((double) Math.abs(p.getX() - j), 2.0d) + Math.pow((double) Math.abs(p.getY() - i), 2.0d) <= Math.pow((double) p.Perception(), 2.0d)) {
                    if (!isNodeSurrounded(this.walkmap[j][i])) {
                        this.walkmap[j][i].Visible = true;
                        this.walkmap[j][i].Viewed = true;
                    }
                    for (int k = 0; k < this.monsterList.size(); k++) {
                        Monster m = this.monsterList.get(k);
                        if (m.X == j && m.Y == i) {
                            this.monsterList.get(k).Aware = true;
                            GameState.PlayerResting = false;
                        }
                    }
                }
            }
        }
    }

    public void makeRoom() {
        int w;
        int h;
        int x;
        int y;
        Room r;
        Random generator = new Random();
        do {
            w = DieRoller.RollDieWithMin("1d10", 5);
            h = DieRoller.RollDieWithMin("1d7", 5);
            x = DieRoller.RollDieWithMin((this.mapWidth - w) - 1, 1);
            y = DieRoller.RollDieWithMin((this.mapHeight - h) - 1, 1);
            r = new Room(w, h, x, y);
        } while (overlapsExistingRooms(r));
        this.roomList.add(r);
        int opening = 0;
        while (opening < 2) {
            for (int i = x; i < x + w; i++) {
                for (int j = y; j < y + h; j++) {
                    if (i != x && i != (x + w) - 1 && j != y && j != (y + h) - 1) {
                        this.walkmap[i][j].DisplayChar = ' ';
                        this.walkmap[i][j].Walkable = false;
                    } else if (isAdjacentToBoundary(i, j) || r.isAdjacentToOpening(i, j) || isCorner(i, j, r) || generator.nextInt(15) <= 5 || opening >= 2) {
                        this.walkmap[i][j].DisplayChar = '#';
                        this.walkmap[i][j].Walkable = false;
                    } else {
                        r.addOpening(new Point(i, j));
                        this.walkmap[i][j].DisplayChar = '.';
                        this.walkmap[i][j].Walkable = true;
                        opening++;
                    }
                }
            }
        }
    }

    public void connectRooms() {
        int count = 0;
        while (!allConnected(this.roomList)) {
            count++;
            if (count <= 50) {
                ArrayList<Room> partialLink = getPartiallyUnconnectedRooms(this.roomList);
                ArrayList<Room> noLink = getFullyUnconnectedRooms(this.roomList);
                Room r1 = null;
                Room r2 = null;
                if (partialLink.size() == 0) {
                    r1 = noLink.get(0);
                    r2 = noLink.get(1);
                } else if (partialLink.size() > 0 && noLink.size() > 0) {
                    r1 = noLink.get(0);
                    r2 = partialLink.get(0);
                } else if (partialLink.size() <= 1 || noLink.size() != 0) {
                    if (partialLink.size() == 0 && noLink.size() == 0) {
                        break;
                    }
                } else {
                    r1 = partialLink.get(0);
                    r2 = partialLink.get(1);
                }
                if (r1 == null || r2 == null) {
                    Log.d("NWBD", "logical error yoooooo, no rooms found");
                }
                Point p1 = r1.getUnconnected().get(0);
                Point p2 = r2.getUnconnected().get(0);
                ArrayList<WalkNode> pathBetweenRooms = PathFinder.findRoomPath(p1.X, p1.Y, getWalkMap(), this.mapHeight, this.mapWidth, p2.X, p2.Y);
                if (pathBetweenRooms.size() != 0) {
                    for (int i = 0; i < pathBetweenRooms.size(); i++) {
                        WalkNode n = pathBetweenRooms.get(i);
                        this.walkmap[n.X][n.Y].Walkable = true;
                        this.walkmap[n.X][n.Y].DisplayChar = '_';
                    }
                    p1.Connected = true;
                    p2.Connected = true;
                }
            } else {
                return;
            }
        }
        for (int i2 = 0; i2 < this.mapWidth; i2++) {
            for (int j = 0; j < this.mapHeight; j++) {
                if (this.walkmap[i2][j].DisplayChar == '.') {
                    this.walkmap[i2][j].DisplayChar = '#';
                    this.walkmap[i2][j].Walkable = false;
                } else if (this.walkmap[i2][j].DisplayChar == '_') {
                    this.walkmap[i2][j].DisplayChar = '.';
                    this.walkmap[i2][j].Walkable = true;
                } else if (this.walkmap[i2][j].DisplayChar == ' ') {
                    this.walkmap[i2][j].DisplayChar = '.';
                    this.walkmap[i2][j].Walkable = true;
                }
            }
        }
    }

    private boolean overlapsExistingRooms(Room r) {
        int size = this.roomList.size();
        for (int i = 0; i < size; i++) {
            Room r2 = this.roomList.get(i);
            if (r2.xpos <= r.xpos + r.width && r2.xpos + r2.width + 1 >= r.xpos && r2.ypos <= r.ypos + r.height && r2.ypos + r2.height >= r.ypos) {
                return true;
            }
        }
        return false;
    }

    private boolean allConnected(ArrayList<Room> roomList2) {
        for (int i = 0; i < roomList2.size(); i++) {
            if (!roomList2.get(i).Connected()) {
                return false;
            }
        }
        return true;
    }

    private ArrayList<Room> getUnconnectedRooms(ArrayList<Room> roomList2) {
        ArrayList<Room> unconnected = new ArrayList<>();
        for (int i = 0; i < roomList2.size(); i++) {
            if (!roomList2.get(i).Connected()) {
                unconnected.add(roomList2.get(i));
            }
        }
        return unconnected;
    }

    private ArrayList<Room> getPartiallyUnconnectedRooms(ArrayList<Room> roomList2) {
        ArrayList<Room> partial = new ArrayList<>();
        for (int i = 0; i < roomList2.size(); i++) {
            if (roomList2.get(i).PartiallyConnected()) {
                partial.add(roomList2.get(i));
            }
        }
        return partial;
    }

    private ArrayList<Room> getFullyUnconnectedRooms(ArrayList<Room> roomList2) {
        ArrayList<Room> unconnected = new ArrayList<>();
        for (int i = 0; i < roomList2.size(); i++) {
            if (roomList2.get(i).FullyUnconnected()) {
                unconnected.add(roomList2.get(i));
            }
        }
        return unconnected;
    }

    private boolean isAdjacentToBoundary(int x, int y) {
        if (x + 1 == this.mapWidth - 1 || x - 1 == 0 || y + 1 == this.mapHeight - 1 || y - 1 == 0) {
            return true;
        }
        return false;
    }

    private boolean isBoundary(WalkNode node) {
        if (node.X == this.mapWidth - 1 || node.X == 0 || node.Y == this.mapHeight - 1 || node.Y == 0) {
            return true;
        }
        return false;
    }

    private boolean isCorner(int i, int j, Room r) {
        if ((i == r.xpos && j == r.ypos) || ((i == (r.xpos + r.width) - 1 && j == r.ypos) || ((i == (r.xpos + r.width) - 1 && j == (r.ypos + r.height) - 1) || (i == r.xpos && j == (r.ypos + r.height) - 1)))) {
            return true;
        }
        Log.d("NWBD", "i " + i + " j " + j + " x " + r.xpos + " y " + r.ypos + " w " + r.width + " h " + r.height);
        return false;
    }
}
