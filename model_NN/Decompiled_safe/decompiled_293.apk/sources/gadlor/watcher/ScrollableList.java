package gadlor.watcher;

import java.util.ArrayList;

public class ScrollableList<E> extends ArrayList<E> {
    private static final long serialVersionUID = 1;
    private int limit = 0;
    private int offset = 0;
    private int selected = 0;

    public void setLimit(int _limit) {
        this.limit = _limit;
    }

    public int getLimit() {
        return this.limit;
    }

    public void setOffset(int _offset) {
        this.offset = _offset;
    }

    public int getOffset() {
        return this.offset;
    }

    public void scrollDown() {
        if (this.limit + this.offset < size()) {
            this.offset++;
        }
    }

    public void scrollUp() {
        if (this.offset >= 1) {
            this.offset--;
        }
    }

    public String getText() {
        StringBuffer sb = new StringBuffer();
        int i = this.offset;
        while (i < this.offset + this.limit && i < size()) {
            sb.append(get(i).toString());
            i++;
        }
        return sb.toString();
    }

    public String getTextWithHeading() {
        StringBuffer sb = new StringBuffer();
        int i = this.offset;
        while (i < this.offset + this.limit && i < size()) {
            sb.append((char) ((i - this.offset) + 97));
            sb.append(") ");
            sb.append(get(i).toString());
            i++;
        }
        return sb.toString();
    }

    public E select(int index) {
        this.selected = index;
        return get(index);
    }

    public void setCollection(ArrayList<E> list) {
        addAll(list);
    }
}
