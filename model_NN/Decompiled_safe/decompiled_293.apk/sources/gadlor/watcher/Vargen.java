package gadlor.watcher;

public class Vargen {
    public int baseVal;
    public String dieInfo;
    public boolean gaussian;
    public int maxVal;
    public int minVal;

    public Vargen(int base, int min, int max, String die, boolean g) {
        this.baseVal = base;
        this.minVal = min;
        this.maxVal = max;
        this.dieInfo = die;
        this.gaussian = g;
    }

    public int generate() {
        int result = 0;
        while (result < this.minVal && result > this.maxVal) {
            result = this.baseVal;
            if (!this.gaussian) {
                result += DieRoller.RollDieWithMin(this.dieInfo, this.minVal);
            }
        }
        return result;
    }
}
