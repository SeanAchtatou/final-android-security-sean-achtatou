package gadlor.watcher;

import gadlor.watcher.Items.Item;
import gadlor.watcher.Items.ItemGenerator;
import java.util.ArrayList;

public class CombatEngine {
    private static CombatEngine theEngine = new CombatEngine();

    private CombatEngine() {
    }

    public static CombatEngine getInstance() {
        return theEngine;
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    public boolean checkAndHandleCollision(int proposedX, int proposedY) {
        ArrayList<Monster> monsterList = MainApplication.getMonsterList();
        Player p = MainApplication.getPlayer();
        MapMaker map = MainApplication.getMapMaker();
        WalkNode proposedNode = map.walkmap[proposedX][proposedY];
        for (int i = 0; i < monsterList.size(); i++) {
            if (proposedX == monsterList.get(i).X && proposedY == monsterList.get(i).Y) {
                Monster m = monsterList.get(i);
                if (p.DualWielding()) {
                    int dieRoll1 = DieRoller.RollDie("1d20");
                    int dieRoll2 = DieRoller.RollDie("1d20");
                    boolean deadMonster = false;
                    if (dieRoll1 == 20) {
                        m.Health -= p.getLDamage() * 2;
                        StatusMessage.append("You critically hit the " + m.Name + "!");
                        p.incrementLWeaponSkill();
                    } else {
                        if ((p.getLHitBonus() + dieRoll1) - 5 >= m.Evade) {
                            m.Health -= Math.max(p.getLDamage() - m.Armor, 0);
                            StatusMessage.append("You hit the " + m.Name + ".");
                            p.incrementLWeaponSkill();
                        } else {
                            StatusMessage.append("You miss the " + m.Name + ".");
                        }
                    }
                    if (m.Health <= 0) {
                        StatusMessage.append("The " + m.Name + " dies!");
                        grantExperience(m);
                        generateItem(m);
                        monsterList.remove(i);
                        deadMonster = true;
                    }
                    if (!deadMonster) {
                        if (dieRoll2 == 20) {
                            m.Health -= p.getRDamage() * 2;
                            StatusMessage.append("You critically hit the " + m.Name + "!");
                            p.incrementRWeaponSkill();
                        } else {
                            if ((p.getRHitBonus() + dieRoll2) - 3 >= m.Evade) {
                                m.Health -= Math.max(p.getRDamage() - m.Armor, 0);
                                StatusMessage.append("You hit the " + m.Name + ".");
                                p.incrementRWeaponSkill();
                            } else {
                                StatusMessage.append("You miss the " + m.Name + ".");
                            }
                        }
                        if (m.Health <= 0) {
                            StatusMessage.append("The " + m.Name + " dies!");
                            grantExperience(m);
                            generateItem(m);
                            monsterList.remove(i);
                        }
                    }
                    return true;
                }
                int newDieRoll = DieRoller.RollDie("1d20");
                if (newDieRoll == 20) {
                    m.Health -= p.getDamage() * 2;
                    StatusMessage.append("You critically hit the " + m.Name + "!");
                    p.incrementWeaponSkill();
                } else {
                    if (p.HitBonus() + newDieRoll >= m.Evade) {
                        m.Health -= Math.max(p.getDamage() - m.Armor, 0);
                        StatusMessage.append("You hit the " + m.Name + ".");
                        p.incrementWeaponSkill();
                    } else {
                        StatusMessage.append("You miss the " + m.Name + ".");
                    }
                }
                if (m.Health <= 0) {
                    StatusMessage.append("The " + m.Name + " dies!");
                    grantExperience(m);
                    generateItem(m);
                    monsterList.remove(i);
                }
                return true;
            }
        }
        if (!proposedNode.Walkable) {
            StatusMessage.append("You walk directly into the wall - for reasons unknown.");
            return true;
        }
        for (int i2 = 0; i2 < map.npcList.size(); i2++) {
            if (proposedX == map.npcList.get(i2).X && proposedY == map.npcList.get(i2).Y) {
                return true;
            }
        }
        int itemSize = proposedNode.Items.size();
        if (itemSize == 1) {
            String itemName = proposedNode.Items.get(0).Description;
            if (proposedNode.Items.get(0).Plural) {
                StatusMessage.append(String.valueOf(itemName) + " are lying here.");
            } else if (proposedNode.Items.get(0).Type == Item.ItemType.ARMOR) {
                StatusMessage.append(String.valueOf(itemName) + " is lying here.");
            } else {
                char beginChar = itemName.charAt(0);
                if (beginChar == 'a' || beginChar == 'e' || beginChar == 'i' || beginChar == 'o' || beginChar == 'u') {
                    StatusMessage.append("An " + itemName + " is lying here.");
                } else {
                    StatusMessage.append("A " + itemName + " is lying here.");
                }
            }
        }
        if (itemSize > 1) {
            StatusMessage.append("A pile of items is lying here.");
        }
        if (proposedNode.Stairs) {
            StatusMessage.append("Stairs of stone leading downwards.");
        }
        return false;
    }

    public void generateItem(Monster mon) {
        int modifier;
        MapMaker m = MainApplication.getMapMaker();
        int itemChance = mon.XP / 2;
        int itemLevel = GameState.CurrentLevel;
        if (DieRoller.RollDie("1d6") >= 4) {
            modifier = -1;
        } else {
            modifier = 1;
        }
        int itemLevel2 = Math.max(1, itemLevel + (DieRoller.RollDie("1d3") * modifier));
        if (itemChance >= DieRoller.RollDie("1d100")) {
            Item i = ItemGenerator.generateRarity(itemLevel2);
            i.X = mon.X;
            i.Y = mon.Y;
            m.walkmap[mon.X][mon.Y].Items.add(i);
        }
    }

    public void grantExperience(Monster m) {
        Player p = MainApplication.getPlayer();
        p.Exp += m.XP;
        if (((double) p.Exp) >= Math.pow(2.0d, (double) (p.Level - 1)) * 250.0d) {
            p.Level++;
            p.MaxHealth += 5;
            p.CurrentHealth += 5;
            StatusMessage.append("You advance to level " + p.Level + ".");
        }
    }
}
