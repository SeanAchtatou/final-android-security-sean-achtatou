package gadlor.watcher;

import android.util.Log;
import com.millennialmedia.android.R;
import gadlor.watcher.Effects.PoisonEffect;
import gadlor.watcher.Monster;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MonsterGenerator {
    private static /* synthetic */ int[] $SWITCH_TABLE$gadlor$watcher$Monster$RARITY;
    private static ArrayList<Monster> monsterList = new ArrayList<>();
    private static Random randomGen = new Random();
    private static MonsterGenerator theGenerator = new MonsterGenerator();

    static /* synthetic */ int[] $SWITCH_TABLE$gadlor$watcher$Monster$RARITY() {
        int[] iArr = $SWITCH_TABLE$gadlor$watcher$Monster$RARITY;
        if (iArr == null) {
            iArr = new int[Monster.RARITY.values().length];
            try {
                iArr[Monster.RARITY.COMMON.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Monster.RARITY.LEGENDARY.ordinal()] = 6;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Monster.RARITY.RARE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Monster.RARITY.SUPERRARE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Monster.RARITY.UNCOMMON.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Monster.RARITY.UNIQUE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$gadlor$watcher$Monster$RARITY = iArr;
        }
        return iArr;
    }

    private MonsterGenerator() {
        createMonsterList();
    }

    public static Monster getMonster(int XP) {
        Monster m = getMonsterFromXP(XP).clone();
        m.setHealth();
        return m;
    }

    public static Monster getMonsterByLevel(int Level) {
        int modifier;
        if (DieRoller.RollDie("1d10") == 10) {
            modifier = 1;
        } else {
            modifier = -1;
        }
        int Level2 = Math.min(Math.max(1, Level + (DieRoller.RollDieWithMin("1d3-2", 0) * modifier)), 25);
        Monster.RARITY monsterRarity = Monster.RARITY.COMMON;
        int rarityRoll = DieRoller.RollDie("1d100");
        if (rarityRoll < 61) {
            monsterRarity = Monster.RARITY.COMMON;
        } else if (rarityRoll < 81) {
            monsterRarity = Monster.RARITY.UNCOMMON;
        } else if (rarityRoll < 91) {
            monsterRarity = Monster.RARITY.RARE;
        } else if (rarityRoll < 98) {
            monsterRarity = Monster.RARITY.SUPERRARE;
        } else if (rarityRoll < 100) {
            monsterRarity = Monster.RARITY.UNIQUE;
        } else if (rarityRoll == 100) {
            monsterRarity = Monster.RARITY.LEGENDARY;
        }
        while (true) {
            Log.d("NWBD", "Monster level selected: " + Level2);
            Log.d("NWBD", "Monster rarity: " + monsterRarity.name());
            int attempts = 0;
            while (attempts <= 30) {
                attempts++;
                Monster m = monsterList.get(randomGen.nextInt(monsterList.size()));
                if (m.Level <= Level2 && m.Rarity == monsterRarity) {
                    Log.d("NWBD", "Selected " + m.Name);
                    Monster clone = m.clone();
                    clone.setHealth();
                    return clone;
                }
            }
            monsterRarity = reduceMonsterRarity(monsterRarity);
        }
    }

    public static Monster.RARITY reduceMonsterRarity(Monster.RARITY monsterRarity) {
        if (monsterRarity == Monster.RARITY.COMMON) {
            return monsterRarity;
        }
        switch ($SWITCH_TABLE$gadlor$watcher$Monster$RARITY()[monsterRarity.ordinal()]) {
            case 2:
                return Monster.RARITY.COMMON;
            case R.styleable.MMAdView_refreshInterval:
                return Monster.RARITY.UNCOMMON;
            case R.styleable.MMAdView_accelerate:
                return Monster.RARITY.RARE;
            case R.styleable.MMAdView_ignoreDensityScaling:
                return Monster.RARITY.SUPERRARE;
            case R.styleable.MMAdView_age:
                return Monster.RARITY.UNIQUE;
            default:
                return Monster.RARITY.COMMON;
        }
    }

    public static MonsterGenerator getInstance() {
        return theGenerator;
    }

    public static Monster getMonsterFromXP(int xpLimit) {
        int position = randomGen.nextInt(monsterList.size());
        int XP = monsterList.get(position).XP;
        while (XP > xpLimit) {
            position = randomGen.nextInt(position);
            XP = monsterList.get(position).XP;
        }
        Monster m = monsterList.get(position).clone();
        m.setHealth();
        return m;
    }

    private void createMonsterList() {
        Monster monster = new Monster('k', "kobold");
        monster.damageDie = 2;
        monster.damageDieNumber = 1;
        monster.healthDie = 6;
        monster.healthDieNumber = 3;
        monster.Armor = 0;
        monster.Evade = 10;
        monster.hitBonus = 0;
        monster.numAttacks = 1;
        monster.XP = 10;
        monster.Level = 1;
        monster.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster);
        Monster monster2 = new Monster('r', "giant rat");
        monster2.damageDie = 2;
        monster2.damageDieNumber = 1;
        monster2.healthDie = 4;
        monster2.healthDieNumber = 3;
        monster2.Armor = 0;
        monster2.Evade = 13;
        monster2.hitBonus = 0;
        monster2.numAttacks = 1;
        monster2.XP = 10;
        monster2.Level = 1;
        monster2.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster2);
        Monster monster3 = new Monster('h', "disembodied hand");
        monster3.damageDie = 2;
        monster3.damageDieNumber = 2;
        monster3.healthDie = 3;
        monster3.healthDieNumber = 3;
        monster3.Armor = 0;
        monster3.Evade = 15;
        monster3.hitBonus = 0;
        monster3.numAttacks = 1;
        monster3.XP = 30;
        monster3.Level = 1;
        monster3.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster3);
        Monster monster4 = new Monster('b', "giant bat");
        monster4.damageDieNumber = 2;
        monster4.damageDie = 2;
        monster4.healthDie = 4;
        monster4.healthDieNumber = 2;
        monster4.Armor = 0;
        monster4.Evade = 13;
        monster4.hitBonus = 0;
        monster4.numAttacks = 1;
        monster4.XP = 15;
        monster4.Level = 1;
        monster4.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster4);
        Monster monster5 = new Monster('g', "goblin");
        monster5.damageDie = 4;
        monster5.damageDieNumber = 1;
        monster5.healthDie = 3;
        monster5.healthDieNumber = 4;
        monster5.Armor = 1;
        monster5.Evade = 10;
        monster5.hitBonus = 0;
        monster5.numAttacks = 1;
        monster5.XP = 25;
        monster5.Level = 1;
        monster5.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster5);
        Monster monster6 = new Monster('b', "bandit");
        monster6.damageDie = 8;
        monster6.damageDieNumber = 1;
        monster6.healthDie = 4;
        monster6.healthDieNumber = 3;
        monster6.Armor = 0;
        monster6.Evade = 10;
        monster6.hitBonus = 1;
        monster6.numAttacks = 1;
        monster6.XP = 50;
        monster6.Level = 1;
        monster6.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster6);
        Monster monster7 = new Monster('o', "orc");
        monster7.damageDie = 6;
        monster7.damageDieNumber = 1;
        monster7.healthDie = 7;
        monster7.healthDieNumber = 2;
        monster7.Armor = 0;
        monster7.Evade = 10;
        monster7.hitBonus = 1;
        monster7.numAttacks = 1;
        monster7.XP = 30;
        monster7.Level = 1;
        monster7.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster7);
        Monster monster8 = new Monster('h', "hobgoblin");
        monster8.damageDie = 4;
        monster8.damageDieNumber = 2;
        monster8.healthDie = 8;
        monster8.healthDieNumber = 2;
        monster8.Armor = 1;
        monster8.Evade = 10;
        monster8.hitBonus = 2;
        monster8.numAttacks = 1;
        monster8.XP = 55;
        monster8.Level = 2;
        monster8.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster8);
        Monster monster9 = new Monster('*', "will-o'-wisp");
        monster9.damageDie = 3;
        monster9.damageDieNumber = 3;
        monster9.healthDie = 6;
        monster9.healthDieNumber = 2;
        monster9.Armor = 0;
        monster9.Evade = 22;
        monster9.hitBonus = 1;
        monster9.numAttacks = 1;
        monster9.XP = 100;
        monster9.Level = 3;
        monster9.Rarity = Monster.RARITY.RARE;
        monsterList.add(monster9);
        Monster monster10 = new Monster('#', "living wall");
        monster10.damageDie = 4;
        monster10.damageDieNumber = 2;
        monster10.healthDie = 6;
        monster10.healthDieNumber = 3;
        monster10.Armor = 3;
        monster10.Evade = 5;
        monster10.hitBonus = 0;
        monster10.numAttacks = 1;
        monster10.XP = 75;
        monster10.Level = 2;
        monster10.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster10);
        Monster monster11 = new Monster('&', "mimic");
        monster11.damageDie = 4;
        monster11.damageDieNumber = 2;
        monster11.healthDie = 4;
        monster11.healthDieNumber = 3;
        monster11.Armor = 1;
        monster11.Evade = 10;
        monster11.hitBonus = 1;
        monster11.numAttacks = 2;
        monster11.XP = 70;
        monster11.Level = 2;
        monster11.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster11);
        Monster monster12 = new Monster('s', "skeleton");
        monster12.damageDie = 8;
        monster12.damageDieNumber = 1;
        monster12.healthDie = 8;
        monster12.healthDieNumber = 1;
        monster12.Armor = 2;
        monster12.Evade = 10;
        monster12.hitBonus = 0;
        monster12.numAttacks = 1;
        monster12.XP = 65;
        monster12.Level = 2;
        monster12.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster12);
        Monster zombie = new Monster('z', "zombie");
        zombie.damageDie = 8;
        zombie.damageDieNumber = 1;
        zombie.healthDie = 8;
        zombie.healthDieNumber = 2;
        zombie.Armor = 2;
        zombie.Evade = 8;
        zombie.hitBonus = 0;
        zombie.numAttacks = 1;
        zombie.XP = 70;
        zombie.Level = 2;
        zombie.Rarity = Monster.RARITY.COMMON;
        monsterList.add(zombie);
        Monster monster13 = new Monster('f', "fell");
        monster13.damageDie = 4;
        monster13.damageDieNumber = 1;
        monster13.healthDie = 6;
        monster13.healthDieNumber = 6;
        monster13.Armor = 1;
        monster13.Evade = 10;
        monster13.hitBonus = 2;
        monster13.numAttacks = 2;
        monster13.XP = 90;
        monster13.Level = 3;
        monster13.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster13);
        Monster monster14 = new Monster('c', "redcap");
        monster14.damageDie = 5;
        monster14.damageDieNumber = 1;
        monster14.healthDie = 3;
        monster14.healthDieNumber = 3;
        monster14.Armor = 0;
        monster14.Evade = 12;
        monster14.hitBonus = 0;
        monster14.numAttacks = 1;
        monster14.XP = 40;
        monster14.Level = 1;
        monster14.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster14);
        Monster monster15 = new Monster('d', "dire wolf");
        monster15.damageDie = 6;
        monster15.damageDieNumber = 2;
        monster15.healthDie = 8;
        monster15.healthDieNumber = 5;
        monster15.Armor = 1;
        monster15.Evade = 13;
        monster15.hitBonus = 3;
        monster15.numAttacks = 2;
        monster15.XP = 105;
        monster15.Level = 3;
        monster15.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster15);
        Monster monster16 = new Monster('O', "ogre");
        monster16.damageDie = 8;
        monster16.damageDieNumber = 2;
        monster16.healthDie = 10;
        monster16.healthDieNumber = 4;
        monster16.Armor = 3;
        monster16.Evade = 10;
        monster16.hitBonus = 4;
        monster16.numAttacks = 1;
        monster16.XP = 155;
        monster16.Level = 4;
        monster16.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster16);
        Monster monster17 = new Monster('S', "giant snake");
        monster17.damageDie = 7;
        monster17.damageDieNumber = 2;
        monster17.healthDie = 15;
        monster17.healthDieNumber = 3;
        monster17.Armor = 2;
        monster17.Evade = 12;
        monster17.hitBonus = 4;
        monster17.numAttacks = 2;
        monster17.hasSpecialAttack = true;
        monster17.XP = 170;
        SpecialAttack snakePoison = new SpecialAttack();
        snakePoison.DoesDamage = false;
        snakePoison.Magical = false;
        snakePoison.Probability = 85;
        snakePoison.StatusEffect = true;
        snakePoison.RequiresHit = true;
        snakePoison.Description = "The snake's fangs sink into your skin, and a searing pain screams throughout your body!";
        snakePoison.effect = new PoisonEffect("1d8", "1d6");
        monster17.Attack = snakePoison;
        monster17.Level = 5;
        monster17.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster17);
        Monster monster18 = new Monster('M', "sorcerer");
        monster18.damageDie = 6;
        monster18.damageDieNumber = 1;
        monster18.healthDie = 5;
        monster18.healthDieNumber = 13;
        monster18.Armor = 2;
        monster18.Evade = 13;
        monster18.hitBonus = 2;
        monster18.XP = 110;
        monster18.numAttacks = 1;
        monster18.hasSpecialAttack = true;
        SpecialAttack sorcererAttack = new SpecialAttack();
        sorcererAttack.DoesDamage = true;
        sorcererAttack.Magical = true;
        sorcererAttack.dieSize = 6;
        sorcererAttack.numDice = 3;
        sorcererAttack.dmgBonus = 3;
        sorcererAttack.PermanentEffect = false;
        sorcererAttack.Probability = 85;
        sorcererAttack.RequiresHit = false;
        sorcererAttack.Description = "The sorcerer mumbles arcane words, and you are seared by jets of flame!";
        monster18.Attack = sorcererAttack;
        monster18.Level = 5;
        monster18.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster18);
        Monster monster19 = new Monster('S', "aged sellsword");
        monster19.damageDie = 6;
        monster19.damageDieNumber = 3;
        monster19.healthDie = 7;
        monster19.healthDieNumber = 5;
        monster19.Armor = 2;
        monster19.Evade = 17;
        monster19.hitBonus = 6;
        monster19.numAttacks = 2;
        monster19.XP = 510;
        monster19.Level = 6;
        monster19.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster19);
        Monster monster20 = new Monster('K', "undead knight");
        monster20.damageDie = 8;
        monster20.damageDieNumber = 3;
        monster20.healthDie = 8;
        monster20.healthDieNumber = 10;
        monster20.Armor = 4;
        monster20.Evade = 12;
        monster20.hitBonus = 4;
        monster20.numAttacks = 1;
        monster20.XP = 650;
        monster20.Level = 7;
        monster20.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster20);
        Monster monster21 = new Monster('B', "giant bear");
        monster21.damageDie = 6;
        monster21.damageDieNumber = 3;
        monster21.healthDie = 10;
        monster21.healthDieNumber = 7;
        monster21.Armor = 2;
        monster21.Evade = 11;
        monster21.hitBonus = 2;
        monster21.numAttacks = 2;
        monster21.XP = 300;
        monster21.Level = 4;
        monster21.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster21);
        Monster monster22 = new Monster('F', "fire elemental");
        monster22.damageDie = 4;
        monster22.damageDieNumber = 5;
        monster22.healthDie = 6;
        monster22.healthDieNumber = 5;
        monster22.Armor = 2;
        monster22.Evade = 15;
        monster22.hitBonus = 1;
        monster22.numAttacks = 1;
        monster22.XP = 350;
        monster22.Level = 4;
        monster22.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster22);
        Monster monster23 = new Monster('T', "troll");
        monster23.damageDie = 8;
        monster23.damageDieNumber = 2;
        monster23.healthDie = 8;
        monster23.healthDieNumber = 25;
        monster23.Armor = 4;
        monster23.Evade = 13;
        monster23.hitBonus = 4;
        monster23.numAttacks = 2;
        monster23.XP = 1250;
        monster23.Level = 8;
        monster23.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster23);
        Monster monster24 = new Monster('M', "writhing mass of maggots");
        monster24.damageDie = 15;
        monster24.damageDieNumber = 1;
        monster24.healthDie = 9;
        monster24.healthDieNumber = 10;
        monster24.Armor = 3;
        monster24.Evade = 16;
        monster24.hitBonus = 5;
        monster24.numAttacks = 2;
        monster24.XP = 600;
        monster24.Level = 7;
        monster24.Rarity = Monster.RARITY.COMMON;
        monsterList.add(monster24);
        Monster monster25 = new Monster('G', "clay golem");
        monster25.damageDie = 6;
        monster25.damageDieNumber = 3;
        monster25.healthDie = 10;
        monster25.healthDieNumber = 5;
        monster25.Armor = 5;
        monster25.Evade = 10;
        monster25.hitBonus = 4;
        monster25.numAttacks = 1;
        monster25.XP = 800;
        monster25.Level = 7;
        monster25.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster25);
        Monster monster26 = new Monster('G', "stone golem");
        monster26.damageDie = 8;
        monster26.damageDieNumber = 5;
        monster26.healthDie = 12;
        monster26.healthDieNumber = 8;
        monster26.Armor = 8;
        monster26.Evade = 10;
        monster26.hitBonus = 6;
        monster26.numAttacks = 1;
        monster26.XP = 1250;
        monster26.Level = 10;
        monster26.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster26);
        Monster monster27 = new Monster('G', "iron golem");
        monster27.damageDie = 10;
        monster27.damageDieNumber = 6;
        monster27.healthDie = 14;
        monster27.healthDieNumber = 8;
        monster27.Armor = 10;
        monster27.Evade = 10;
        monster27.hitBonus = 10;
        monster27.numAttacks = 1;
        monster27.XP = 2000;
        monster27.Level = 13;
        monster27.Rarity = Monster.RARITY.RARE;
        monsterList.add(monster27);
        Monster monster28 = new Monster('G', "steel golem");
        monster28.damageDie = 12;
        monster28.damageDieNumber = 8;
        monster28.healthDie = 16;
        monster28.healthDieNumber = 12;
        monster28.Armor = 12;
        monster28.Evade = 10;
        monster28.hitBonus = 15;
        monster28.numAttacks = 1;
        monster28.XP = 3500;
        monster28.Level = 16;
        monster28.Rarity = Monster.RARITY.SUPERRARE;
        monsterList.add(monster28);
        Monster monster29 = new Monster('M', "minotaur");
        monster29.damageDie = 7;
        monster29.damageDieNumber = 5;
        monster29.healthDie = 8;
        monster29.healthDieNumber = 7;
        monster29.Armor = 3;
        monster29.Evade = 12;
        monster29.hitBonus = 9;
        monster29.numAttacks = 2;
        monster29.XP = 1250;
        monster29.Level = 11;
        monster29.Rarity = Monster.RARITY.RARE;
        monsterList.add(monster29);
        Monster monster30 = new Monster('W', "wight");
        monster30.damageDie = 3;
        monster30.damageDieNumber = 5;
        monster30.healthDie = 6;
        monster30.healthDieNumber = 7;
        monster30.Armor = 2;
        monster30.Evade = 16;
        monster30.hitBonus = 6;
        monster30.XP = 400;
        monster30.numAttacks = 1;
        monster30.Level = 5;
        monster30.Rarity = Monster.RARITY.RARE;
        monsterList.add(monster30);
        Monster monster31 = new Monster('W', "wraith");
        monster31.damageDie = 4;
        monster31.damageDieNumber = 4;
        monster31.healthDie = 4;
        monster31.healthDieNumber = 6;
        monster31.Armor = 4;
        monster31.Evade = 19;
        monster31.hitBonus = 4;
        monster31.hasSpecialAttack = true;
        monster31.numAttacks = 1;
        SpecialAttack wraithAttack = new SpecialAttack();
        wraithAttack.StrengthDamage = 1;
        wraithAttack.PermanentEffect = true;
        wraithAttack.Probability = 96;
        wraithAttack.RequiresHit = true;
        wraithAttack.Description = "You feel your strength draining away!";
        monster31.Attack = wraithAttack;
        monster31.Level = 6;
        monster31.Rarity = Monster.RARITY.RARE;
        monster31.XP = 350;
        monsterList.add(monster31);
        Monster monster32 = new Monster('E', "water elemental");
        monster32.damageDie = 6;
        monster32.damageDieNumber = 4;
        monster32.healthDie = 6;
        monster32.healthDieNumber = 15;
        monster32.Evade = 15;
        monster32.Armor = 6;
        monster32.hitBonus = 4;
        monster32.numAttacks = 1;
        monster32.XP = 1050;
        monster32.Level = 7;
        monster32.Rarity = Monster.RARITY.UNCOMMON;
        monsterList.add(monster32);
        Monster monster33 = new Monster('H', "stone giant");
        monster33.damageDie = 10;
        monster33.damageDieNumber = 3;
        monster33.healthDie = 10;
        monster33.healthDieNumber = 10;
        monster33.Armor = 10;
        monster33.Evade = 8;
        monster33.hitBonus = 13;
        monster33.numAttacks = 1;
        monster33.XP = 7500;
        monster33.Level = 15;
        monster33.Rarity = Monster.RARITY.RARE;
        monsterList.add(monster33);
        Monster monster34 = new Monster('D', "dragon");
        monster34.damageDie = 12;
        monster34.damageDieNumber = 4;
        monster34.healthDie = 12;
        monster34.healthDieNumber = 8;
        monster34.Armor = 13;
        monster34.Evade = 12;
        monster34.hitBonus = 17;
        monster34.numAttacks = 3;
        monster34.XP = 10000;
        monster34.Level = 25;
        monster34.Rarity = Monster.RARITY.SUPERRARE;
        monsterList.add(monster34);
        Collections.sort(monsterList, new MonsterSorter());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < monsterList.size(); i++) {
            sb.append("Monster: ");
            sb.append(monsterList.get(i).Name);
            sb.append(" is worth ");
            sb.append(String.valueOf(monsterList.get(i).XP) + "\n");
        }
        return sb.toString();
    }

    public static Monster generateMonsterByName(String monsterName) {
        for (int i = 0; i < monsterList.size(); i++) {
            if (monsterName.equals(monsterList.get(i).Name)) {
                return monsterList.get(i).clone();
            }
        }
        return new Monster(' ', " ");
    }
}
