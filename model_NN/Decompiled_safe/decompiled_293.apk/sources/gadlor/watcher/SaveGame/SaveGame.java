package gadlor.watcher.SaveGame;

import gadlor.watcher.GameState;
import gadlor.watcher.Items.Item;
import gadlor.watcher.MainApplication;
import gadlor.watcher.MapMaker;
import gadlor.watcher.Monster;
import gadlor.watcher.Player;
import java.util.ArrayList;
import java.util.Hashtable;

public class SaveGame {
    public int CharacterLevel;
    public String CharacterName;
    public int DungeonLevel;
    public String ID;

    public void Load() {
        loadMap();
        loadMapItems();
        loadMonsters();
        loadPlayer();
        loadEquippedItems();
        loadCarriedItems();
        loadWeaponSkills();
        MainApplication.getMapMaker().setVisible();
        deleteSave();
    }

    /* access modifiers changed from: package-private */
    public void loadMap() {
        MapMaker m = MainApplication.getMapMaker();
        m.resetLevel();
        m.loadLevelLayoutFromSave(SaveGameDb.getMapText(this.ID));
        m.loadVisibleFromSave(SaveGameDb.getMapVisible(this.ID));
        m.setStairsLocation(SaveGameDb.getMapStairsX(this.ID), SaveGameDb.getMapStairsY(this.ID));
        GameState.TurnsElapsed = SaveGameDb.getTurnsElapsed(this.ID);
    }

    /* access modifiers changed from: package-private */
    public void loadMapItems() {
        MapMaker m = MainApplication.getMapMaker();
        ArrayList<Item> mapItems = SaveGameDb.getMapItems(this.ID);
        for (int i = 0; i < mapItems.size(); i++) {
            Item item = mapItems.get(i);
            m.walkmap[item.X][item.Y].Items.add(item);
        }
    }

    /* access modifiers changed from: package-private */
    public void loadCarriedItems() {
        Player p = MainApplication.getPlayer();
        ArrayList<Item> carriedItems = SaveGameDb.getCarriedItems(this.ID);
        for (int i = 0; i < carriedItems.size(); i++) {
            p.Inv.Items.add(carriedItems.get(i));
        }
    }

    /* access modifiers changed from: package-private */
    public void loadEquippedItems() {
        Player p = MainApplication.getPlayer();
        Hashtable<String, Item> inv = SaveGameDb.getEquippedItems(this.ID);
        for (int i = 65; i < 80; i++) {
            String key = new StringBuilder().append((char) i).toString();
            p.Inv.itemTable.put(key, inv.get(key));
        }
    }

    /* access modifiers changed from: package-private */
    public void loadPlayer() {
        SaveGameDb.setPlayerInfo(MainApplication.getPlayer(), this.ID);
    }

    /* access modifiers changed from: package-private */
    public void loadMonsters() {
        MapMaker m = MainApplication.getMapMaker();
        ArrayList<Monster> monsterList = SaveGameDb.getMonsters(this.ID);
        for (int i = 0; i < monsterList.size(); i++) {
            m.monsterList.add(monsterList.get(i));
        }
    }

    /* access modifiers changed from: package-private */
    public void loadWeaponSkills() {
        SaveGameDb.getWeaponSkills(this.ID);
    }

    /* access modifiers changed from: package-private */
    public void deleteSave() {
        SaveGameDb.deleteSave(this.ID);
    }
}
