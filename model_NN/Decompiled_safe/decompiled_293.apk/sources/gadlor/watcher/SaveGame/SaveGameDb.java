package gadlor.watcher.SaveGame;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import gadlor.watcher.GameState;
import gadlor.watcher.Items.Item;
import gadlor.watcher.Items.ItemGenerator;
import gadlor.watcher.Items.Weapon;
import gadlor.watcher.MainApplication;
import gadlor.watcher.MapMaker;
import gadlor.watcher.Monster;
import gadlor.watcher.MonsterGenerator;
import gadlor.watcher.Player;
import gadlor.watcher.WeaponSkills;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

public class SaveGameDb {
    /* JADX INFO: Multiple debug info for r1v3 android.content.ContentValues: [D('skills' android.content.ContentValues), D('gamevalues' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r1v8 android.content.ContentValues: [D('skills' android.content.ContentValues), D('gamelevel' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r2v77 android.content.ContentValues: [D('item' android.content.ContentValues), D('mapItemValues' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r4v119 android.content.ContentValues: [D('weaponTable' android.content.ContentValues), D('mapItem' gadlor.watcher.Items.Item)] */
    /* JADX INFO: Multiple debug info for r1v25 java.lang.String: [D('key' char), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v27 gadlor.watcher.Items.Item: [D('s' java.lang.String), D('equipped' gadlor.watcher.Items.Item)] */
    /* JADX INFO: Multiple debug info for r2v87 android.content.ContentValues: [D('equippedSlot' android.content.ContentValues), D('item' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r1v29 gadlor.watcher.Items.Weapon: [D('equipped' gadlor.watcher.Items.Item), D('w' gadlor.watcher.Items.Weapon)] */
    /* JADX INFO: Multiple debug info for r1v36 android.content.ContentValues: [D('item' android.content.ContentValues), D('carried' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r2v92 android.content.ContentValues: [D('weaponTable' android.content.ContentValues), D('carriedItem' gadlor.watcher.Items.Item)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void writeNewSave() {
        Player p = MainApplication.getPlayer();
        MapMaker map = MainApplication.getMapMaker();
        ArrayList<Monster> monsterList = map.monsterList;
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getWritableDatabase();
        db.execSQL("BEGIN");
        UUID pKey = UUID.randomUUID();
        ContentValues gamevalues = new ContentValues();
        gamevalues.put("Id", pKey.toString());
        gamevalues.put("CharacterName", p.Name);
        gamevalues.put("TurnsElapsed", Integer.valueOf(GameState.TurnsElapsed));
        gamevalues.put("XP", Integer.valueOf(p.Exp));
        gamevalues.put("Level", Integer.valueOf(p.Level));
        gamevalues.put("DungeonLevel", Integer.valueOf(GameState.CurrentLevel));
        gamevalues.put("X", Integer.valueOf(p.getX()));
        gamevalues.put("Y", Integer.valueOf(p.getY()));
        gamevalues.put("Strength", Integer.valueOf(p.BaseStrength));
        gamevalues.put("Dexterity", Integer.valueOf(p.BaseDexterity));
        gamevalues.put("Constitution", Integer.valueOf(p.BaseConstitution));
        gamevalues.put("Intelligence", Integer.valueOf(p.BaseIntelligence));
        gamevalues.put("Wisdom", Integer.valueOf(p.BaseWisdom));
        gamevalues.put("Charisma", Integer.valueOf(p.BaseCharisma));
        gamevalues.put("Perception", Integer.valueOf(p.BasePerception));
        gamevalues.put("Speed", Integer.valueOf(p.BaseSpeed));
        gamevalues.put("MaxHealth", Integer.valueOf(p.MaxHealth));
        gamevalues.put("CurrentHealth", Integer.valueOf(p.CurrentHealth));
        db.insert("SaveGame", "", gamevalues);
        ContentValues gamevalues2 = new ContentValues();
        gamevalues2.put("GameId", pKey.toString());
        gamevalues2.put("WeaponCategory", Weapon.WeaponType.DAGGER.name());
        gamevalues2.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.DAGGER)));
        gamevalues2.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.DAGGER)));
        db.insert("WeaponSkills", "", gamevalues2);
        ContentValues skills = new ContentValues();
        skills.put("GameId", pKey.toString());
        skills.put("WeaponCategory", Weapon.WeaponType.SWORD.name());
        skills.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.SWORD)));
        skills.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.SWORD)));
        db.insert("WeaponSkills", "", skills);
        ContentValues skills2 = new ContentValues();
        skills2.put("GameId", pKey.toString());
        skills2.put("WeaponCategory", Weapon.WeaponType.MACE.name());
        skills2.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.MACE)));
        skills2.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.MACE)));
        db.insert("WeaponSkills", "", skills2);
        ContentValues skills3 = new ContentValues();
        skills3.put("GameId", pKey.toString());
        skills3.put("WeaponCategory", Weapon.WeaponType.FLAILCHAIN.name());
        skills3.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.FLAILCHAIN)));
        skills3.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.FLAILCHAIN)));
        db.insert("WeaponSkills", "", skills3);
        ContentValues skills4 = new ContentValues();
        skills4.put("GameId", pKey.toString());
        skills4.put("WeaponCategory", Weapon.WeaponType.CLUBHAMMER.name());
        skills4.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.CLUBHAMMER)));
        skills4.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.CLUBHAMMER)));
        db.insert("WeaponSkills", "", skills4);
        skills4.put("GameId", pKey.toString());
        skills4.put("WeaponCategory", Weapon.WeaponType.SPEAR.name());
        skills4.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.SPEAR)));
        skills4.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.SPEAR)));
        db.insert("WeaponSkills", "", skills4);
        skills4.put("GameId", pKey.toString());
        skills4.put("WeaponCategory", Weapon.WeaponType.STAFF.name());
        skills4.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.STAFF)));
        skills4.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.STAFF)));
        db.insert("WeaponSkills", "", skills4);
        skills4.put("GameId", pKey.toString());
        skills4.put("WeaponCategory", Weapon.WeaponType.AXE.name());
        skills4.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.AXE)));
        skills4.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.AXE)));
        db.insert("WeaponSkills", "", skills4);
        skills4.put("GameId", pKey.toString());
        skills4.put("WeaponCategory", Weapon.WeaponType.POLEARM.name());
        skills4.put("Level", Long.valueOf(WeaponSkills.getLevelForType(Weapon.WeaponType.POLEARM)));
        skills4.put("HitCount", Long.valueOf(WeaponSkills.getCounterForType(Weapon.WeaponType.POLEARM)));
        db.insert("WeaponSkills", "", skills4);
        ContentValues gamelevel = new ContentValues();
        gamelevel.put("GameId", pKey.toString());
        gamelevel.put("Layout", map.getMapForSave());
        gamelevel.put("Visible", map.getViewedMapForSave());
        gamelevel.put("StairsX", Integer.valueOf(map.StairsDownX));
        gamelevel.put("StairsY", Integer.valueOf(map.StairsDownY));
        db.insert("SaveGameLevel", "", gamelevel);
        for (int i = 0; i < monsterList.size(); i++) {
            Monster m = monsterList.get(i);
            ContentValues monster = new ContentValues();
            monster.put("GameId", pKey.toString());
            monster.put("MonsterName", m.Name);
            monster.put("CurrentHP", Integer.valueOf(m.Health));
            monster.put("X", Integer.valueOf(m.X));
            monster.put("Y", Integer.valueOf(m.Y));
            db.insert("SaveGameMonsters", "", monster);
        }
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= p.Inv.Items.size()) {
                break;
            }
            Item carriedItem = p.Inv.Items.get(i3);
            ContentValues item = new ContentValues();
            item.put("GameId", pKey.toString());
            item.put("ItemId", carriedItem.ID.toString());
            item.put("ItemDesc", carriedItem.Description);
            item.put("ItemType", carriedItem.Type.name());
            item.put("Evade", Integer.valueOf(carriedItem.Evade));
            item.put("Armor", Integer.valueOf(carriedItem.Armor));
            item.put("HP", Integer.valueOf(carriedItem.HP));
            item.put("Strength", Integer.valueOf(carriedItem.StrengthMod));
            item.put("Dexterity", Integer.valueOf(carriedItem.DexterityMod));
            item.put("Constitution", Integer.valueOf(carriedItem.ConstitutionMod));
            item.put("Intelligence", Integer.valueOf(carriedItem.IntelligenceMod));
            item.put("Wisdom", Integer.valueOf(carriedItem.WisdomMod));
            item.put("Charisma", Integer.valueOf(carriedItem.CharismaMod));
            item.put("Perception", Integer.valueOf(carriedItem.PerceptionMod));
            item.put("Speed", Integer.valueOf(carriedItem.SpeedMod));
            if (carriedItem.Plural) {
                item.put("Plural", (Integer) 1);
            } else {
                item.put("Plural", (Integer) 0);
            }
            db.insert("SaveGameItems", "", item);
            ContentValues carried = new ContentValues();
            carried.put("ItemId", carriedItem.ID.toString());
            carried.put("GameId", pKey.toString());
            db.insert("CarriedItems", "", carried);
            if (carriedItem.Type == Item.ItemType.WEAPON) {
                Weapon w = (Weapon) carriedItem;
                ContentValues weaponTable = new ContentValues();
                weaponTable.put("GameId", pKey.toString());
                weaponTable.put("ItemId", w.ID.toString());
                weaponTable.put("WeaponType", w.wtype.name());
                weaponTable.put("DamageDie", Integer.valueOf(w.damageDie));
                weaponTable.put("DieNumber", Integer.valueOf(w.numberofDice));
                weaponTable.put("DamageBonus", Integer.valueOf(w.damageModifier));
                weaponTable.put("HitBonus", Integer.valueOf(w.hitBonus));
                if (w.twoHanded) {
                    weaponTable.put("TwoHanded", (Integer) 1);
                } else {
                    weaponTable.put("TwoHanded", (Integer) 0);
                }
                db.insert("SaveGameWeapon", "", weaponTable);
            }
            i2 = i3 + 1;
        }
        int i4 = 65;
        while (true) {
            int i5 = i4;
            if (i5 >= 80) {
                break;
            }
            Item equipped = p.Inv.itemTable.get(new StringBuilder().append((char) i5).toString());
            ContentValues item2 = new ContentValues();
            item2.put("GameId", pKey.toString());
            item2.put("ItemId", equipped.ID.toString());
            item2.put("ItemDesc", equipped.Description);
            item2.put("ItemType", equipped.Type.name());
            item2.put("Evade", Integer.valueOf(equipped.Evade));
            item2.put("Armor", Integer.valueOf(equipped.Armor));
            item2.put("HP", Integer.valueOf(equipped.HP));
            item2.put("Strength", Integer.valueOf(equipped.StrengthMod));
            item2.put("Dexterity", Integer.valueOf(equipped.DexterityMod));
            item2.put("Constitution", Integer.valueOf(equipped.ConstitutionMod));
            item2.put("Intelligence", Integer.valueOf(equipped.IntelligenceMod));
            item2.put("Wisdom", Integer.valueOf(equipped.WisdomMod));
            item2.put("Charisma", Integer.valueOf(equipped.CharismaMod));
            item2.put("Perception", Integer.valueOf(equipped.PerceptionMod));
            item2.put("Speed", Integer.valueOf(equipped.SpeedMod));
            if (equipped.Plural) {
                item2.put("Plural", (Integer) 1);
            } else {
                item2.put("Plural", (Integer) 0);
            }
            db.insert("SaveGameItems", "", item2);
            ContentValues item3 = new ContentValues();
            item3.put("GameId", pKey.toString());
            item3.put("ItemId", equipped.ID.toString());
            item3.put("Position", Integer.valueOf(i5));
            db.insert("EquippedItems", "", item3);
            if (equipped.Type == Item.ItemType.WEAPON) {
                Weapon w2 = (Weapon) equipped;
                ContentValues weaponTable2 = new ContentValues();
                weaponTable2.put("GameId", pKey.toString());
                weaponTable2.put("ItemId", w2.ID.toString());
                weaponTable2.put("WeaponType", w2.wtype.name());
                weaponTable2.put("DamageDie", Integer.valueOf(w2.damageDie));
                weaponTable2.put("DieNumber", Integer.valueOf(w2.numberofDice));
                weaponTable2.put("DamageBonus", Integer.valueOf(w2.damageModifier));
                weaponTable2.put("HitBonus", Integer.valueOf(w2.hitBonus));
                weaponTable2.put("TwoHanded", Boolean.valueOf(w2.twoHanded));
                db.insert("SaveGameWeapon", "", weaponTable2);
            }
            i4 = i5 + 1;
        }
        for (int i6 = 0; i6 < map.itemList.size(); i6++) {
            Item mapItem = map.itemList.get(i6);
            ContentValues item4 = new ContentValues();
            item4.put("GameId", pKey.toString());
            item4.put("ItemId", mapItem.ID.toString());
            item4.put("ItemDesc", mapItem.Description);
            item4.put("ItemType", mapItem.Type.name());
            item4.put("Evade", Integer.valueOf(mapItem.Evade));
            item4.put("Armor", Integer.valueOf(mapItem.Armor));
            item4.put("HP", Integer.valueOf(mapItem.HP));
            item4.put("Strength", Integer.valueOf(mapItem.StrengthMod));
            item4.put("Dexterity", Integer.valueOf(mapItem.DexterityMod));
            item4.put("Constitution", Integer.valueOf(mapItem.ConstitutionMod));
            item4.put("Intelligence", Integer.valueOf(mapItem.IntelligenceMod));
            item4.put("Wisdom", Integer.valueOf(mapItem.WisdomMod));
            item4.put("Charisma", Integer.valueOf(mapItem.CharismaMod));
            item4.put("Perception", Integer.valueOf(mapItem.PerceptionMod));
            item4.put("Speed", Integer.valueOf(mapItem.SpeedMod));
            if (mapItem.Plural) {
                item4.put("Plural", (Integer) 1);
            } else {
                item4.put("Plural", (Integer) 0);
            }
            db.insert("SaveGameItems", "", item4);
            ContentValues mapItemValues = new ContentValues();
            mapItemValues.put("GameId", pKey.toString());
            mapItemValues.put("ItemId", mapItem.ID.toString());
            mapItemValues.put("X", Integer.valueOf(mapItem.X));
            mapItemValues.put("Y", Integer.valueOf(mapItem.Y));
            db.insert("LevelItems", "", mapItemValues);
            if (mapItem.Type == Item.ItemType.WEAPON) {
                Weapon w3 = (Weapon) mapItem;
                ContentValues weaponTable3 = new ContentValues();
                weaponTable3.put("GameId", pKey.toString());
                weaponTable3.put("ItemId", w3.ID.toString());
                weaponTable3.put("WeaponType", w3.wtype.name());
                weaponTable3.put("DamageDie", Integer.valueOf(w3.damageDie));
                weaponTable3.put("DieNumber", Integer.valueOf(w3.numberofDice));
                weaponTable3.put("DamageBonus", Integer.valueOf(w3.damageModifier));
                weaponTable3.put("HitBonus", Integer.valueOf(w3.hitBonus));
                weaponTable3.put("TwoHanded", Boolean.valueOf(w3.twoHanded));
                db.insert("SaveGameWeapon", "", weaponTable3);
            }
        }
        db.execSQL("COMMIT");
        db.close();
    }

    public static ArrayList<SaveGame> getSavedChars() {
        ArrayList<SaveGame> games = new ArrayList<>();
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * from SaveGame", null);
        while (c.moveToNext()) {
            SaveGame game = new SaveGame();
            game.CharacterLevel = c.getInt(c.getColumnIndex("Level"));
            game.CharacterName = c.getString(c.getColumnIndex("CharacterName"));
            game.DungeonLevel = c.getInt(c.getColumnIndex("DungeonLevel"));
            game.ID = c.getString(c.getColumnIndex("Id"));
            games.add(game);
        }
        c.close();
        db.close();
        return games;
    }

    public static String getMapText(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select Layout from SaveGameLevel WHERE GameId = ?", new String[]{gameId});
        String levelLayout = "";
        while (c.moveToNext()) {
            levelLayout = c.getString(c.getColumnIndex("Layout"));
        }
        c.close();
        db.close();
        return levelLayout;
    }

    public static String getMapVisible(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select Visible from SaveGameLevel WHERE GameId = ?", new String[]{gameId});
        String levelLayout = "";
        while (c.moveToNext()) {
            levelLayout = c.getString(c.getColumnIndex("Visible"));
        }
        c.close();
        db.close();
        return levelLayout;
    }

    public static int getMapStairsX(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select StairsX from SaveGameLevel WHERE GameId = ?", new String[]{gameId});
        int stairsX = 0;
        while (c.moveToNext()) {
            stairsX = c.getInt(c.getColumnIndex("StairsX"));
        }
        c.close();
        db.close();
        return stairsX;
    }

    public static int getMapStairsY(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select StairsY from SaveGameLevel WHERE GameId = ?", new String[]{gameId});
        int stairsY = 0;
        while (c.moveToNext()) {
            stairsY = c.getInt(c.getColumnIndex("StairsY"));
        }
        c.close();
        db.close();
        return stairsY;
    }

    public static int getTurnsElapsed(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select TurnsElapsed from SaveGame WHERE Id = ?", new String[]{gameId});
        int turns = 0;
        while (c.moveToNext()) {
            turns = c.getInt(c.getColumnIndex("TurnsElapsed"));
        }
        c.close();
        db.close();
        return turns;
    }

    public static void setPlayerInfo(Player p, String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * from SaveGame WHERE Id = ?", new String[]{gameId});
        while (c.moveToNext()) {
            p.setX(c.getInt(c.getColumnIndex("X")));
            p.setY(c.getInt(c.getColumnIndex("Y")));
            p.Name = c.getString(c.getColumnIndex("CharacterName"));
            p.Level = c.getInt(c.getColumnIndex("Level"));
            p.BaseStrength = c.getInt(c.getColumnIndex("Strength"));
            p.BaseDexterity = c.getInt(c.getColumnIndex("Dexterity"));
            p.BaseConstitution = c.getInt(c.getColumnIndex("Constitution"));
            p.BaseIntelligence = c.getInt(c.getColumnIndex("Intelligence"));
            p.BaseWisdom = c.getInt(c.getColumnIndex("Wisdom"));
            p.BaseCharisma = c.getInt(c.getColumnIndex("Charisma"));
            p.BasePerception = c.getInt(c.getColumnIndex("Perception"));
            p.BaseSpeed = c.getInt(c.getColumnIndex("Speed"));
            p.MaxHealth = c.getInt(c.getColumnIndex("MaxHealth"));
            p.CurrentHealth = c.getInt(c.getColumnIndex("CurrentHealth"));
            p.Exp = c.getInt(c.getColumnIndex("XP"));
            GameState.CurrentLevel = c.getInt(c.getColumnIndex("DungeonLevel"));
        }
        c.close();
        db.close();
    }

    /* JADX INFO: Multiple debug info for r0v1 android.database.sqlite.SQLiteDatabase: [D('helper' gadlor.watcher.SaveGame.SaveGameDatabaseHelper), D('db' android.database.sqlite.SQLiteDatabase)] */
    /* JADX INFO: Multiple debug info for r6v2 android.database.Cursor: [D('c' android.database.Cursor), D('gameId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v6 android.database.Cursor: [D('weaponId' java.lang.String[]), D('c' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r2v3 gadlor.watcher.Items.Weapon: [D('item' gadlor.watcher.Items.Item), D('w' gadlor.watcher.Items.Weapon)] */
    /* JADX INFO: Multiple debug info for r1v15 gadlor.watcher.Items.Item: [D('pot' gadlor.watcher.Items.Item), D('description' java.lang.String)] */
    public static ArrayList<Item> getMapItems(String gameId) {
        ArrayList<Item> itemList = new ArrayList<>();
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * FROM SaveGameItems JOIN LevelItems ON LevelItems.ItemId=SaveGameItems.ItemId  WHERE SaveGameItems.GameId = ?", new String[]{gameId});
        while (c.moveToNext()) {
            Item.ItemType tempItemType = Item.ItemType.valueOf(c.getString(c.getColumnIndex("ItemType")));
            Item item = Item.castToItemByType(tempItemType);
            if (tempItemType == Item.ItemType.POTION) {
                Item pot = ItemGenerator.generateItemByDesc(c.getString(c.getColumnIndex("ItemDesc")));
                pot.X = c.getInt(c.getColumnIndex("X"));
                pot.Y = c.getInt(c.getColumnIndex("Y"));
                itemList.add(pot);
            } else {
                item.Type = tempItemType;
                item.Description = c.getString(c.getColumnIndex("ItemDesc"));
                item.ID = UUID.fromString(c.getString(c.getColumnIndex("ItemId")));
                item.Evade = c.getInt(c.getColumnIndex("Evade"));
                item.Armor = c.getInt(c.getColumnIndex("Armor"));
                item.HP = c.getInt(c.getColumnIndex("HP"));
                item.StrengthMod = c.getInt(c.getColumnIndex("Strength"));
                item.DexterityMod = c.getInt(c.getColumnIndex("Dexterity"));
                item.ConstitutionMod = c.getInt(c.getColumnIndex("Constitution"));
                item.IntelligenceMod = c.getInt(c.getColumnIndex("Intelligence"));
                item.WisdomMod = c.getInt(c.getColumnIndex("Wisdom"));
                item.CharismaMod = c.getInt(c.getColumnIndex("Charisma"));
                item.PerceptionMod = c.getInt(c.getColumnIndex("Perception"));
                item.SpeedMod = c.getInt(c.getColumnIndex("Speed"));
                item.Plural = c.getInt(c.getColumnIndex("Plural")) > 0;
                item.X = c.getInt(c.getColumnIndex("X"));
                item.Y = c.getInt(c.getColumnIndex("Y"));
                itemList.add(item);
            }
        }
        c.close();
        for (int i = 0; i < itemList.size(); i++) {
            Item item2 = itemList.get(i);
            Cursor c2 = db.rawQuery("Select * FROM SaveGameWeapon WHERE ItemId = ?", new String[]{item2.ID.toString()});
            if (c2.moveToNext() && item2.Type == Item.ItemType.WEAPON) {
                Weapon w = (Weapon) item2;
                w.damageDie = c2.getInt(c2.getColumnIndex("DamageDie"));
                w.numberofDice = c2.getInt(c2.getColumnIndex("DieNumber"));
                w.damageModifier = c2.getInt(c2.getColumnIndex("DamageBonus"));
                w.hitBonus = c2.getInt(c2.getColumnIndex("HitBonus"));
                w.twoHanded = c2.getInt(c2.getColumnIndex("TwoHanded")) > 0;
                w.wtype = Weapon.WeaponType.valueOf(c2.getString(c2.getColumnIndex("WeaponType")));
                itemList.set(i, w);
            }
            c2.close();
        }
        db.close();
        return itemList;
    }

    /* JADX INFO: Multiple debug info for r0v1 android.database.sqlite.SQLiteDatabase: [D('helper' gadlor.watcher.SaveGame.SaveGameDatabaseHelper), D('db' android.database.sqlite.SQLiteDatabase)] */
    /* JADX INFO: Multiple debug info for r6v2 android.database.Cursor: [D('c' android.database.Cursor), D('gameId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v6 android.database.Cursor: [D('weaponId' java.lang.String[]), D('c' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r2v3 gadlor.watcher.Items.Weapon: [D('item' gadlor.watcher.Items.Item), D('w' gadlor.watcher.Items.Weapon)] */
    /* JADX INFO: Multiple debug info for r1v15 gadlor.watcher.Items.Item: [D('pot' gadlor.watcher.Items.Item), D('description' java.lang.String)] */
    public static ArrayList<Item> getCarriedItems(String gameId) {
        ArrayList<Item> itemList = new ArrayList<>();
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * FROM SaveGameItems JOIN CarriedItems ON CarriedItems.ItemId=SaveGameItems.ItemId  WHERE SaveGameItems.GameId = ?", new String[]{gameId});
        while (c.moveToNext()) {
            Item.ItemType tempItemType = Item.ItemType.valueOf(c.getString(c.getColumnIndex("ItemType")));
            Item item = Item.castToItemByType(tempItemType);
            if (tempItemType == Item.ItemType.POTION) {
                itemList.add(ItemGenerator.generateItemByDesc(c.getString(c.getColumnIndex("ItemDesc"))));
            } else {
                item.Type = tempItemType;
                item.Description = c.getString(c.getColumnIndex("ItemDesc"));
                item.ID = UUID.fromString(c.getString(c.getColumnIndex("ItemId")));
                item.Evade = c.getInt(c.getColumnIndex("Evade"));
                item.Armor = c.getInt(c.getColumnIndex("Armor"));
                item.HP = c.getInt(c.getColumnIndex("HP"));
                item.StrengthMod = c.getInt(c.getColumnIndex("Strength"));
                item.DexterityMod = c.getInt(c.getColumnIndex("Dexterity"));
                item.ConstitutionMod = c.getInt(c.getColumnIndex("Constitution"));
                item.IntelligenceMod = c.getInt(c.getColumnIndex("Intelligence"));
                item.WisdomMod = c.getInt(c.getColumnIndex("Wisdom"));
                item.CharismaMod = c.getInt(c.getColumnIndex("Charisma"));
                item.PerceptionMod = c.getInt(c.getColumnIndex("Perception"));
                item.SpeedMod = c.getInt(c.getColumnIndex("Speed"));
                item.Plural = c.getInt(c.getColumnIndex("Plural")) > 0;
                itemList.add(item);
            }
        }
        c.close();
        for (int i = 0; i < itemList.size(); i++) {
            Item item2 = itemList.get(i);
            Cursor c2 = db.rawQuery("Select * FROM SaveGameWeapon WHERE ItemId = ?", new String[]{item2.ID.toString()});
            if (c2.moveToNext() && item2.Type == Item.ItemType.WEAPON) {
                Weapon w = (Weapon) item2;
                w.damageDie = c2.getInt(c2.getColumnIndex("DamageDie"));
                w.numberofDice = c2.getInt(c2.getColumnIndex("DieNumber"));
                w.damageModifier = c2.getInt(c2.getColumnIndex("DamageBonus"));
                w.hitBonus = c2.getInt(c2.getColumnIndex("HitBonus"));
                w.twoHanded = c2.getInt(c2.getColumnIndex("TwoHanded")) > 0;
                w.wtype = Weapon.WeaponType.valueOf(c2.getString(c2.getColumnIndex("WeaponType")));
                itemList.set(i, w);
            }
            c2.close();
        }
        db.close();
        return itemList;
    }

    /* JADX INFO: Multiple debug info for r0v1 android.database.sqlite.SQLiteDatabase: [D('helper' gadlor.watcher.SaveGame.SaveGameDatabaseHelper), D('db' android.database.sqlite.SQLiteDatabase)] */
    /* JADX INFO: Multiple debug info for r7v2 android.database.Cursor: [D('c' android.database.Cursor), D('gameId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v4 char: [D('c' android.database.Cursor), D('keyChar' char)] */
    /* JADX INFO: Multiple debug info for r7v8 android.database.Cursor: [D('weaponId' java.lang.String[]), D('c' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r3v4 gadlor.watcher.Items.Weapon: [D('item' gadlor.watcher.Items.Item), D('w' gadlor.watcher.Items.Weapon)] */
    /* JADX INFO: Multiple debug info for r3v54 int: [D('key' java.lang.String), D('keyint' int)] */
    /* JADX INFO: Multiple debug info for r3v55 char: [D('keyint' int), D('keyChar' char)] */
    /* JADX INFO: Multiple debug info for r3v57 java.lang.String: [D('key' java.lang.String), D('keyChar' char)] */
    public static Hashtable<String, Item> getEquippedItems(String gameId) {
        Hashtable<String, Item> inv = new Hashtable<>();
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * FROM SaveGameItems JOIN EquippedItems ON EquippedItems.ItemId=SaveGameItems.ItemId  WHERE SaveGameItems.GameId = ?", new String[]{gameId});
        while (c.moveToNext()) {
            Item.ItemType tempItemType = Item.ItemType.valueOf(c.getString(c.getColumnIndex("ItemType")));
            Item item = Item.castToItemByType(tempItemType);
            item.Type = tempItemType;
            item.Description = c.getString(c.getColumnIndex("ItemDesc"));
            item.ID = UUID.fromString(c.getString(c.getColumnIndex("ItemId")));
            item.Evade = c.getInt(c.getColumnIndex("Evade"));
            item.Armor = c.getInt(c.getColumnIndex("Armor"));
            item.HP = c.getInt(c.getColumnIndex("HP"));
            item.StrengthMod = c.getInt(c.getColumnIndex("Strength"));
            item.DexterityMod = c.getInt(c.getColumnIndex("Dexterity"));
            item.ConstitutionMod = c.getInt(c.getColumnIndex("Constitution"));
            item.IntelligenceMod = c.getInt(c.getColumnIndex("Intelligence"));
            item.WisdomMod = c.getInt(c.getColumnIndex("Wisdom"));
            item.CharismaMod = c.getInt(c.getColumnIndex("Charisma"));
            item.PerceptionMod = c.getInt(c.getColumnIndex("Perception"));
            item.SpeedMod = c.getInt(c.getColumnIndex("Speed"));
            item.Plural = c.getInt(c.getColumnIndex("Plural")) > 0;
            inv.put(new StringBuilder().append((char) Integer.parseInt(c.getString(c.getColumnIndex("Position")))).toString(), item);
        }
        c.close();
        for (int i = 65; i < 80; i++) {
            String key = new StringBuilder().append((char) i).toString();
            Item item2 = inv.get(key);
            Cursor c2 = db.rawQuery("Select * FROM SaveGameWeapon WHERE ItemId = ?", new String[]{item2.ID.toString()});
            if (c2.moveToNext() && item2.Type == Item.ItemType.WEAPON) {
                Weapon w = (Weapon) item2;
                w.damageDie = c2.getInt(c2.getColumnIndex("DamageDie"));
                w.numberofDice = c2.getInt(c2.getColumnIndex("DieNumber"));
                w.damageModifier = c2.getInt(c2.getColumnIndex("DamageBonus"));
                w.hitBonus = c2.getInt(c2.getColumnIndex("HitBonus"));
                w.twoHanded = c2.getInt(c2.getColumnIndex("TwoHanded")) > 0;
                w.wtype = Weapon.WeaponType.valueOf(c2.getString(c2.getColumnIndex("WeaponType")));
                inv.put(key, w);
            }
            c2.close();
        }
        db.close();
        return inv;
    }

    public static ArrayList<Monster> getMonsters(String gameId) {
        ArrayList<Monster> monsterList = new ArrayList<>();
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * FROM SaveGameMonsters WHERE SaveGameMonsters.GameId = ?", new String[]{gameId});
        while (c.moveToNext()) {
            Monster m = MonsterGenerator.generateMonsterByName(c.getString(c.getColumnIndex("MonsterName")));
            m.Health = c.getInt(c.getColumnIndex("CurrentHP"));
            m.X = c.getInt(c.getColumnIndex("X"));
            m.Y = c.getInt(c.getColumnIndex("Y"));
            monsterList.add(m);
        }
        c.close();
        db.close();
        return monsterList;
    }

    public static void getWeaponSkills(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getReadableDatabase();
        Cursor c = db.rawQuery("Select * FROM WeaponSkills WHERE WeaponSkills.GameId = ?", new String[]{gameId});
        while (c.moveToNext()) {
            Weapon.WeaponType type = Weapon.WeaponType.valueOf(c.getString(c.getColumnIndex("WeaponCategory")));
            int counter = c.getInt(c.getColumnIndex("HitCount"));
            int level = c.getInt(c.getColumnIndex("Level"));
            WeaponSkills.setCounterForType(type, counter);
            WeaponSkills.setLevelForType(type, level);
        }
        c.close();
        db.close();
    }

    public static void deleteSave(String gameId) {
        SQLiteDatabase db = new SaveGameDatabaseHelper(MainApplication.getInstance().getApplicationContext()).getWritableDatabase();
        String[] id = {gameId};
        db.delete("WeaponSkills", "WeaponSkills.GameId = ?", id);
        db.delete("EquippedItems", "EquippedItems.GameId = ?", id);
        db.delete("LevelItems", "LevelItems.GameId = ?", id);
        db.delete("CarriedItems", "CarriedItems.GameId = ?", id);
        db.delete("SaveGameMonsters", "SaveGameMonsters.GameId = ?", id);
        db.delete("SaveGameLevel", "SaveGameLevel.GameId = ?", id);
        db.delete("SaveGameWeapon", "SaveGameWeapon.GameId = ?", id);
        db.delete("SaveGameItems", "SaveGameItems.GameId = ?", id);
        db.delete("SaveGame", "SaveGame.Id = ?", id);
        db.close();
    }
}
