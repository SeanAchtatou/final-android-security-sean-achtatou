package gadlor.watcher.SaveGame;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class SaveGameDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "savegame.db";
    private static final int DATABASE_VERSION = 2;

    SaveGameDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE SaveGame (Id NVARCHAR(36) PRIMARY KEY,CharacterName NVARCHAR(150),TurnsElapsed INTEGER ,XP INTEGER ,Level INTEGER,DungeonLevel INTEGER,X INTEGER,Y INTEGER,Strength INTEGER,Dexterity INTEGER,Constitution INTEGER,Intelligence INTEGER,Wisdom INTEGER,Charisma INTEGER,Speed INTEGER,Perception INTEGER,MaxHealth INTEGER,CurrentHealth INTEGER);");
        db.execSQL("CREATE TABLE SaveGameItems(ItemId NVARCHAR(36) PRIMARY KEY,GameId NVARCHAR(36), ItemDesc NVARCHAR(100), ItemType NVARCHAR(20), Evade INTEGER, Armor INTEGER, HP INTEGER, Strength INTEGER, Dexterity INTEGER, Constitution INTEGER, Intelligence INTEGER, Wisdom INTEGER, Charisma INTEGER, Perception INTEGER, Speed INTEGER, Plural INTEGER, FOREIGN KEY(GameId) REFERENCES SaveGame(Id));");
        db.execSQL("CREATE TABLE SaveGameWeapon(ItemId NVARCHAR(36) PRIMARY KEY,GameId NVARCHAR(36), WeaponType CHAR(20) ,DamageDie INTEGER ,DieNumber INTEGER ,DamageBonus INTEGER ,HitBonus INTEGER ,TwoHanded INTEGER ,FOREIGN KEY(ItemId) REFERENCES SaveGameItems(ItemId));");
        db.execSQL("CREATE TABLE SaveGameLevel(GameId NVARCHAR(36) PRIMARY KEY,Layout NVARCHAR(130), Visible NVARCHAR(130), StairsX INTEGER, StairsY INTEGER );");
        db.execSQL("CREATE TABLE SaveGameMonsters(GameId NVARCHAR(36) ,MonsterName STRING, CurrentHP INTEGER, X INTEGER, Y INTEGER, FOREIGN KEY(GameId) REFERENCES SaveGame(Id));");
        db.execSQL("CREATE TABLE CarriedItems(ItemId NVARCHAR(36) PRIMARY KEY,GameId NVARCHAR(36));");
        db.execSQL("CREATE TABLE LevelItems(ItemId NVARCHAR(36) PRIMARY KEY,GameId NVARCHAR(36),X INTEGER,Y INTEGER);");
        db.execSQL("CREATE TABLE EquippedItems(ItemId NVARCHAR(36) PRIMARY KEY,GameId NVARCHAR(36),Position CHAR);");
        db.execSQL("CREATE TABLE WeaponSkills(GameId NVARCHAR(36),WeaponCategory NVARCHAR(25),Level INTEGER,HitCount INTEGER);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("NWBD", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS HighScores");
        onCreate(db);
    }
}
