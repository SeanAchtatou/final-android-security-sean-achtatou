package gadlor.watcher;

public class Monster {
    public char Appearance;
    public int Armor;
    public SpecialAttack Attack;
    public boolean Aware = false;
    public int Evade;
    public int Health;
    public int Level;
    public String Name;
    public RARITY Rarity;
    public int X;
    public int XP;
    public int Y;
    public int damageDie;
    public int damageDieNumber;
    public boolean hasSpecialAttack = false;
    public int healthDie;
    public int healthDieNumber;
    public int hitBonus;
    public int numAttacks;

    public enum RARITY {
        COMMON,
        UNCOMMON,
        RARE,
        SUPERRARE,
        UNIQUE,
        LEGENDARY
    }

    public Monster(char display, String n) {
        this.Appearance = display;
        this.Name = n;
    }

    public Monster clone() {
        Monster m = new Monster(this.Appearance, this.Name);
        m.damageDie = this.damageDie;
        m.damageDieNumber = this.damageDieNumber;
        m.healthDie = this.healthDie;
        m.healthDieNumber = this.healthDieNumber;
        m.Armor = this.Armor;
        m.Evade = this.Evade;
        m.Aware = this.Aware;
        m.XP = this.XP;
        m.hitBonus = this.hitBonus;
        m.hasSpecialAttack = this.hasSpecialAttack;
        m.Attack = this.Attack;
        m.numAttacks = this.numAttacks;
        m.Level = this.Level;
        m.Rarity = this.Rarity;
        return m;
    }

    public void setHealth() {
        this.Health = DieRoller.RollDieWithMin(String.valueOf(String.valueOf(this.healthDieNumber)) + "d" + String.valueOf(this.healthDie), (this.healthDieNumber * this.healthDie) / 2);
    }

    public int getDmg() {
        return DieRoller.RollDie(String.valueOf(String.valueOf(this.damageDieNumber)) + "d" + String.valueOf(this.damageDie));
    }

    public String printLocation() {
        return "Monster " + this.Name + " is at X: " + Integer.toString(this.X) + " Y: " + Integer.toString(this.Y);
    }
}
