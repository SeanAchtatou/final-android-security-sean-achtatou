package gadlor.watcher;

import android.util.Log;
import gadlor.watcher.Effects.Effect;
import java.util.ArrayList;
import java.util.Random;

public class SpecialAttack {
    ArrayList<String> Animation;
    int CharismaDamage = 0;
    int ConstitutionDamage = 0;
    String Description;
    int DexterityDamage = 0;
    boolean DoesDamage = false;
    int IntelligenceDamage = 0;
    int LevelDamage = 0;
    boolean Magical = false;
    int PerceptionDamage = 0;
    boolean PermanentEffect = false;
    int Probability;
    boolean RequiresHit;
    int SpeedDamage = 0;
    boolean StatusEffect = false;
    int StrengthDamage = 0;
    int WisdomDamage = 0;
    int dieSize;
    int dmgBonus;
    Effect effect;
    int numDice;

    public void inflictAttack(Player p) {
        Random generator = new Random();
        int chance = generator.nextInt(101);
        Log.d("NWBD", "specialattack chance: " + chance);
        if (chance >= this.Probability) {
            if (this.PermanentEffect) {
                p.BaseStrength -= this.StrengthDamage;
                p.BaseDexterity -= this.DexterityDamage;
                p.BaseConstitution -= this.ConstitutionDamage;
                p.BaseIntelligence -= this.IntelligenceDamage;
                p.BaseWisdom -= this.WisdomDamage;
                p.BaseCharisma -= this.CharismaDamage;
                p.BaseSpeed -= this.SpeedDamage;
                p.BasePerception -= this.PerceptionDamage;
                p.Level -= this.LevelDamage;
            }
            if (this.StatusEffect) {
                p.Effects.add(this.effect);
            }
            if (this.DoesDamage && this.Magical) {
                int dmg = 0;
                for (int i = 0; i < this.numDice; i++) {
                    dmg += generator.nextInt(this.dieSize + 1);
                }
                p.CurrentHealth -= dmg + this.dmgBonus;
            }
            StatusMessage.append(this.Description);
        }
    }
}
