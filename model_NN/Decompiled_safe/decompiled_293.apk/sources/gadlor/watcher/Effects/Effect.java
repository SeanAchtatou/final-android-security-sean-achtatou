package gadlor.watcher.Effects;

import gadlor.watcher.Player;

public abstract class Effect {
    public abstract void ApplyEffect(Player player);

    public abstract boolean CheckEffectForRemoval();
}
