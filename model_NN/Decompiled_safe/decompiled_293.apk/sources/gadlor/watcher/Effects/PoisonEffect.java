package gadlor.watcher.Effects;

import gadlor.watcher.DieRoller;
import gadlor.watcher.Player;
import gadlor.watcher.StatusMessage;

public class PoisonEffect extends Effect {
    public String PoisonDieInfo;
    public String TurnsDieInfo;
    private int turnsLeft = DieRoller.RollDie(this.TurnsDieInfo);

    public PoisonEffect(String poisonDie, String turnsDie) {
        this.PoisonDieInfo = poisonDie;
        this.TurnsDieInfo = turnsDie;
    }

    public void ApplyEffect(Player p) {
        StatusMessage.append("Venom painfully wracks your body.");
        p.CurrentHealth -= DieRoller.RollDie(this.PoisonDieInfo);
    }

    public boolean CheckEffectForRemoval() {
        if (this.turnsLeft <= 0) {
            return true;
        }
        this.turnsLeft--;
        return false;
    }
}
