package gadlor.watcher.Effects;

import gadlor.watcher.DieRoller;
import gadlor.watcher.Player;
import gadlor.watcher.StatusMessage;

public class HealingEffect extends Effect {
    public String HealingDieInfo;

    public void ApplyEffect(Player p) {
        p.CurrentHealth = Math.min(p.HP(), p.CurrentHealth + DieRoller.RollDie(this.HealingDieInfo));
        StatusMessage.append("You feel better.");
    }

    public boolean CheckEffectForRemoval() {
        return true;
    }
}
