package gadlor.watcher;

import java.util.Comparator;

public class MonsterSorter implements Comparator<Monster> {
    public int compare(Monster m1, Monster m2) {
        return m1.XP - m2.XP;
    }
}
