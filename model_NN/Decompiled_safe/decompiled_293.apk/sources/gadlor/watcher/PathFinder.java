package gadlor.watcher;

import android.util.Log;
import java.util.ArrayList;

public class PathFinder {
    /* JADX INFO: Multiple debug info for r9v10 java.util.ArrayList<gadlor.watcher.WalkNode>: [D('finalNode' gadlor.watcher.WalkNode), D('finalPath' java.util.ArrayList<gadlor.watcher.WalkNode>)] */
    /* JADX INFO: Multiple debug info for r9v18 long: [D('t' long), D('elapsed' long)] */
    public static ArrayList<WalkNode> findRoomPath(int srcX, int srcY, WalkNode[][] wMap, int mapHeight, int mapWidth, int destX, int destY) {
        ArrayList<WalkNode> finalPath = new ArrayList<>();
        ArrayList<WalkNode> openList = new ArrayList<>();
        ArrayList<WalkNode> closedList = new ArrayList<>();
        WalkNode dest = wMap[destX][destY];
        openList.add(wMap[srcX][srcY]);
        WalkNode currentNode = openList.get(0);
        currentNode.G = 0;
        currentNode.parent = null;
        currentNode.Open = true;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mapHeight; i++) {
            for (int j = 0; j < mapWidth; j++) {
                if (j == srcX && i == srcY) {
                    sb.append('B');
                } else if (j == destX && i == destY) {
                    sb.append('X');
                } else {
                    sb.append(wMap[j][i].DisplayChar);
                }
            }
            if (i + 1 < mapHeight) {
                sb.append("\n");
            }
        }
        Log.d("NWBD", sb.toString());
        while (!dest.Closed) {
            if (openList.size() == 0) {
                Log.d("NWBD", "No path available.");
                return finalPath;
            }
            moveNodeToClosed(currentNode, openList, closedList);
            long t = System.currentTimeMillis();
            addAdjacentNodes(openList, closedList, wMap, mapHeight, mapWidth, currentNode);
            long t2 = System.currentTimeMillis() - t;
            long elapsed = System.currentTimeMillis();
            currentNode = findLowestF(openList, dest);
            long t3 = System.currentTimeMillis() - elapsed;
        }
        Log.d("NWBD", "We're there.");
        if (closedList.size() == 0) {
            return finalPath;
        }
        WalkNode finalNode = (WalkNode) closedList.get(closedList.size() - 1);
        while (finalNode.parent != null) {
            finalPath.add(finalNode);
            finalNode = finalNode.parent;
        }
        finalPath.add(finalNode);
        return finalPath;
    }

    /* JADX INFO: Multiple debug info for r2v3 long: [D('t' long), D('elapsed' long)] */
    public static ArrayList<WalkNode> findMonsterPath(Monster monster, WalkNode[][] wMap, int mapHeight, int mapWidth, ArrayList<Monster> arrayList, Player player) {
        ArrayList<WalkNode> finalPath = new ArrayList<>();
        ArrayList<WalkNode> openList = new ArrayList<>();
        ArrayList<WalkNode> closedList = new ArrayList<>();
        WalkNode dest = wMap[player.getX()][player.getY()];
        openList.add(wMap[monster.X][monster.Y]);
        WalkNode currentNode = openList.get(0);
        currentNode.G = 0;
        currentNode.parent = null;
        currentNode.Open = true;
        while (!dest.Closed) {
            if (openList.size() == 0) {
                return null;
            }
            moveNodeToClosed(currentNode, openList, closedList);
            long t = System.currentTimeMillis();
            addAdjacentNodes(openList, closedList, wMap, mapHeight, mapWidth, currentNode);
            long t2 = System.currentTimeMillis() - t;
            long elapsed = System.currentTimeMillis();
            currentNode = findLowestF(openList, dest);
            long t3 = System.currentTimeMillis() - elapsed;
        }
        WalkNode finalNode = (WalkNode) closedList.get(closedList.size() - 1);
        while (finalNode.parent != null) {
            finalPath.add(finalNode);
            finalNode = finalNode.parent;
        }
        finalPath.add(finalNode);
        return finalPath;
    }

    public static WalkNode findLowestF(ArrayList<WalkNode> openList, WalkNode dest) {
        WalkNode lowest = null;
        int fMin = 0;
        for (int i = 0; i < openList.size(); i++) {
            int xDistance = Math.abs(openList.get(i).X - dest.X);
            int yDistance = Math.abs(openList.get(i).Y - dest.Y);
            int currentG = openList.get(i).parent.G + openList.get(i).Weight;
            openList.get(i).G = currentG;
            int tempF = xDistance + yDistance + currentG;
            if (fMin == 0) {
                fMin = tempF;
                lowest = openList.get(i);
            }
            if (tempF < fMin) {
                fMin = tempF;
                lowest = openList.get(i);
            }
        }
        return lowest;
    }

    public static void moveNodeToClosed(WalkNode currentNode, ArrayList<WalkNode> openList, ArrayList<WalkNode> closedList) {
        int i = 0;
        while (i < openList.size()) {
            if (currentNode.X == openList.get(i).X && currentNode.Y == openList.get(i).Y) {
                closedList.add(currentNode);
                currentNode.Closed = true;
                currentNode.Open = false;
                openList.remove(i);
            } else {
                i++;
            }
        }
    }

    public static boolean checkAndSwitchParent(ArrayList<WalkNode> arrayList, ArrayList<WalkNode> arrayList2, WalkNode candidate, WalkNode currentNode) {
        if (candidate.Closed) {
            return true;
        }
        if (!candidate.Open) {
            return false;
        }
        if (candidate.Weight + currentNode.G < candidate.G) {
            candidate.G = currentNode.G + candidate.Weight;
            candidate.parent = currentNode;
        }
        return true;
    }

    public static void addAdjacentNodes(ArrayList<WalkNode> openList, ArrayList<WalkNode> closedList, WalkNode[][] wMap, int mapHeight, int mapWidth, WalkNode currentNode) {
        boolean notAtMaxHeight;
        boolean notAtMaxWidth;
        if (currentNode.Y + 1 <= mapHeight - 1) {
            notAtMaxHeight = true;
        } else {
            notAtMaxHeight = false;
        }
        if (currentNode.X + 1 <= mapWidth - 1) {
            notAtMaxWidth = true;
        } else {
            notAtMaxWidth = false;
        }
        WalkNode testNode = wMap[currentNode.X + 1][currentNode.Y];
        if (notAtMaxWidth && testNode.Walkable && !checkAndSwitchParent(openList, closedList, testNode, currentNode)) {
            testNode.parent = currentNode;
            testNode.Open = true;
            openList.add(testNode);
        }
        WalkNode testNode2 = wMap[currentNode.X - 1][currentNode.Y];
        if (currentNode.X != 0 && testNode2.Walkable && !checkAndSwitchParent(openList, closedList, testNode2, currentNode)) {
            testNode2.parent = currentNode;
            testNode2.Open = true;
            openList.add(testNode2);
        }
        WalkNode testNode3 = wMap[currentNode.X][currentNode.Y + 1];
        if (notAtMaxHeight && testNode3.Walkable && !checkAndSwitchParent(openList, closedList, testNode3, currentNode)) {
            testNode3.parent = currentNode;
            testNode3.Open = true;
            openList.add(testNode3);
        }
        WalkNode testNode4 = wMap[currentNode.X][currentNode.Y - 1];
        if (currentNode.Y != 0 && testNode4.Walkable && !checkAndSwitchParent(openList, closedList, testNode4, currentNode)) {
            testNode4.parent = currentNode;
            testNode4.Open = true;
            openList.add(testNode4);
        }
        WalkNode testNode5 = wMap[currentNode.X + 1][currentNode.Y + 1];
        if (notAtMaxHeight && notAtMaxWidth && testNode5.Walkable && !checkAndSwitchParent(openList, closedList, testNode5, currentNode)) {
            testNode5.parent = currentNode;
            testNode5.Open = true;
            openList.add(testNode5);
        }
        WalkNode testNode6 = wMap[currentNode.X - 1][currentNode.Y + 1];
        if (notAtMaxHeight && currentNode.X != 0 && testNode6.Walkable && !checkAndSwitchParent(openList, closedList, testNode6, currentNode)) {
            testNode6.parent = currentNode;
            testNode6.Open = true;
            openList.add(testNode6);
        }
        WalkNode testNode7 = wMap[currentNode.X + 1][currentNode.Y - 1];
        if (notAtMaxWidth && currentNode.Y != 0 && testNode7.Walkable && !checkAndSwitchParent(openList, closedList, testNode7, currentNode)) {
            testNode7.parent = currentNode;
            testNode7.Open = true;
            openList.add(testNode7);
        }
        WalkNode testNode8 = wMap[currentNode.X - 1][currentNode.Y - 1];
        if (currentNode.X != 0 && currentNode.Y != 0 && testNode8.Walkable && !checkAndSwitchParent(openList, closedList, testNode8, currentNode)) {
            testNode8.parent = currentNode;
            testNode8.Open = true;
            openList.add(testNode8);
        }
        if (openList.size() == 0) {
            Log.d("NWBD", "No nodes added from X: " + currentNode.X + " Y: " + currentNode.Y);
        }
    }

    public static boolean isNodeInList(WalkNode node, ArrayList<WalkNode> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            WalkNode walkNode = list.get(i);
            if (node.X == walkNode.X && node.Y == walkNode.Y) {
                return true;
            }
        }
        return false;
    }
}
