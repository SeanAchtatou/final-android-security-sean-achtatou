package gadlor.watcher;

public class GameState {
    public static boolean CharacterView = false;
    public static int CurrentLevel = 1;
    public static boolean DeadPlayer = false;
    public static int EquipCategory = -1;
    public static boolean HelpScreen = false;
    public static InventoryMode Inventory = InventoryMode.OFF;
    public static int InventoryOffset = 0;
    public static boolean KeyboardShift = false;
    public static String KillerMonster = "";
    public static boolean MonsterSpawn = false;
    public static boolean PlayerResting = false;
    public static boolean Portrait = false;
    public static int ScreenShiftX = 0;
    public static int StrangeEventChance = 1;
    public static int TurnsElapsed = 0;

    public enum InventoryMode {
        OFF,
        EQUIPPED,
        CARRIED,
        EQUIPITEM,
        DROPITEM
    }
}
