package gadlor.watcher;

import org.junit.Assert;
import org.junit.Test;

public class MonsterGeneratorTest {
    @Test
    public void testGetMonsterFromXP() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        Monster m25 = MonsterGenerator.getMonsterFromXP(25);
        Monster m50 = MonsterGenerator.getMonsterFromXP(50);
        Monster m100 = MonsterGenerator.getMonsterFromXP(100);
        Monster m1000 = MonsterGenerator.getMonsterFromXP(1000);
        if (m25.XP <= 25) {
            z = true;
        } else {
            z = false;
        }
        Assert.assertTrue(z);
        if (m50.XP <= 50) {
            z2 = true;
        } else {
            z2 = false;
        }
        Assert.assertTrue(z2);
        if (m100.XP <= 100) {
            z3 = true;
        } else {
            z3 = false;
        }
        Assert.assertTrue(z3);
        if (m1000.XP <= 1000) {
            z4 = true;
        } else {
            z4 = false;
        }
        Assert.assertTrue(z4);
    }
}
