package gadlor.watcher;

import gadlor.watcher.Screens.GameScreen;
import java.util.ArrayList;

public class DisplayStack {
    private ArrayList<GameScreen> Screens = new ArrayList<>();

    public boolean HandleInput(int unicodeChar, int keyCode) {
        return this.Screens.get(0).HandleInput(unicodeChar, keyCode);
    }

    public void Pop() {
        this.Screens.remove(0);
    }

    public void Push(GameScreen g) {
        this.Screens.add(0, g);
    }

    public void RevertToBase() {
        this.Screens.clear();
        this.Screens.add(0, this.Screens.get(this.Screens.size() - 1));
    }

    public String getMainText() {
        return this.Screens.get(0).GetMainText();
    }

    public String getHUDText() {
        return this.Screens.get(0).GetHUDText();
    }

    public String getStatusText() {
        return this.Screens.get(0).GetStatusText();
    }

    public boolean WrapMainText() {
        return this.Screens.get(0).WrapMainText();
    }
}
