package gadlor.watcher;

import java.util.LinkedList;

public class StatusMessage {
    private static LinkedList<String> rollingLog = new LinkedList<>();
    private static StringBuffer sb = new StringBuffer();

    public static void append(String stringToAppend) {
        String stringToAppend2 = String.format("%s%s", Character.valueOf(Character.toUpperCase(stringToAppend.charAt(0))), stringToAppend.substring(1));
        if (sb.length() != 0) {
            sb.append(" ");
            sb.append(stringToAppend2);
            return;
        }
        sb.append(stringToAppend2);
    }

    public static String getMessage() {
        if (sb.toString().length() != 0) {
            if (rollingLog.size() < 10) {
                rollingLog.addLast(sb.toString());
            } else {
                rollingLog.removeFirst();
                rollingLog.addLast(sb.toString());
            }
        }
        return sb.toString();
    }

    public static String getLog() {
        StringBuffer sb2 = new StringBuffer();
        for (int i = 0; i < rollingLog.size(); i++) {
            sb2.append(rollingLog.get(i));
            sb2.append("\n");
        }
        return sb2.toString();
    }

    public static void clear() {
        if (sb.length() != 0) {
            sb.delete(0, sb.length());
        }
    }

    public static void delete(int begin, int end) {
        if (sb.length() != 0) {
            sb.delete(begin, end);
        }
    }
}
