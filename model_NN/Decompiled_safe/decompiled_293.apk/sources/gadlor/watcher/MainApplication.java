package gadlor.watcher;

import android.app.Application;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import gadlor.watcher.Items.Item;
import gadlor.watcher.Items.Weapon;
import gadlor.watcher.Screens.DungeonScreen;
import gadlor.watcher.Screens.TitleScreen;
import java.util.ArrayList;

public class MainApplication extends Application {
    private static MainApplication appInstance = new MainApplication();
    private static MapMaker m = new MapMaker();
    private static Player p;
    private static DisplayStack theStack = new DisplayStack();

    public void onCreate() {
        super.onCreate();
        appInstance.attachBaseContext(this);
        createPlayer();
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        theStack.Push(new DungeonScreen("Dungeon", metrics.widthPixels, metrics.heightPixels));
        theStack.Push(new TitleScreen());
    }

    public static MainApplication getInstance() {
        return appInstance;
    }

    public static Player getPlayer() {
        return p;
    }

    public static ArrayList<Monster> getMonsterList() {
        return m.monsterList;
    }

    public static MapMaker getMapMaker() {
        return m;
    }

    public static DisplayStack getDStack() {
        return theStack;
    }

    public void createPlayer() {
        p = new Player(12, 5, 30, "Barnabas");
        p.CurrentHealth = p.MaxHealth;
        p.Level = 1;
        p.Inv = new Inventory();
        Weapon sword = new Weapon();
        sword.Heading = "IJ";
        sword.Description = "blunt blade";
        sword.damageDie = 4;
        sword.damageModifier = 1;
        sword.numberofDice = 1;
        sword.Evade = 0;
        sword.Type = Item.ItemType.WEAPON;
        sword.wtype = Weapon.WeaponType.SWORD;
        p.Inv.equipForTest(8, sword);
    }
}
