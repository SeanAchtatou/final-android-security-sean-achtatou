package gadlor.watcher;

import com.millennialmedia.android.R;
import gadlor.watcher.Effects.Effect;
import gadlor.watcher.Items.Weapon;
import java.util.ArrayList;

public class Player {
    public int BaseArmor;
    public int BaseCharisma;
    public int BaseConstitution;
    public int BaseDexterity;
    public int BaseEvade;
    public int BaseIntelligence;
    public int BasePerception;
    public int BaseSpeed;
    public int BaseStrength;
    public int BaseWisdom;
    public PlayerClass Class;
    public int CurrentHealth;
    public ArrayList<Effect> Effects;
    public int Exp;
    public int HealingFactor = 4;
    public Inventory Inv;
    public int Level;
    public int MaxHealth;
    public String Name;
    public PlayerRace Race;
    private int xpos;
    private int ypos;

    public Player(int x, int y, int h, String n) {
        this.xpos = x;
        this.ypos = y;
        this.MaxHealth = h;
        this.Name = n;
        this.BaseArmor = 0;
        this.BaseEvade = 10;
        this.BaseStrength = 10;
        this.BaseDexterity = 10;
        this.BaseConstitution = 10;
        this.BaseIntelligence = 10;
        this.BaseWisdom = 10;
        this.BaseCharisma = 10;
        this.BaseSpeed = 10;
        this.BasePerception = 4;
        this.Effects = new ArrayList<>();
        this.Level = 1;
        this.Inv = new Inventory();
    }

    public int getX() {
        return this.xpos;
    }

    public int getY() {
        return this.ypos;
    }

    public void setX(int x) {
        this.xpos = x;
    }

    public void setY(int y) {
        this.ypos = y;
    }

    public boolean StatZeroCheck() {
        if (Strength() < 1 || Constitution() < 1) {
            return true;
        }
        return false;
    }

    public void ApplyEffects() {
        int i = 0;
        while (i < this.Effects.size()) {
            Effect effect = this.Effects.get(i);
            if (effect.CheckEffectForRemoval()) {
                this.Effects.remove(i);
                i--;
            } else {
                effect.ApplyEffect(this);
            }
            i++;
        }
    }

    public boolean DualWielding() {
        if (!(this.Inv.itemTable.get("I") instanceof Weapon) || !(this.Inv.itemTable.get("J") instanceof Weapon)) {
            return false;
        }
        return true;
    }

    public boolean BareHanded() {
        if ((this.Inv.itemTable.get("I") instanceof Weapon) || (this.Inv.itemTable.get("J") instanceof Weapon)) {
            return false;
        }
        return true;
    }

    public String getWeaponDamageDescription() {
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            return getLWeaponDamageDescription();
        }
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            return getRWeaponDamageDescription();
        }
        return getBareHandedDescription();
    }

    public String getLWeaponDamageDescription() {
        Weapon w = (Weapon) this.Inv.itemTable.get("I");
        return String.valueOf(w.numberofDice) + "d" + w.damageDie + " + " + (w.damageModifier + getLDamageBonus());
    }

    public String getRWeaponDamageDescription() {
        Weapon w = (Weapon) this.Inv.itemTable.get("J");
        return String.valueOf(w.numberofDice) + "d" + w.damageDie + " + " + (w.damageModifier + getRDamageBonus());
    }

    public String getBareHandedDescription() {
        return "1d1 + " + (StrengthDmgBonus() + this.Level);
    }

    public void incrementLWeaponSkill() {
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            WeaponSkills.incrementByType((Weapon) this.Inv.itemTable.get("I"));
        }
    }

    public void incrementRWeaponSkill() {
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            WeaponSkills.incrementByType((Weapon) this.Inv.itemTable.get("J"));
        }
    }

    public void incrementWeaponSkill() {
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            WeaponSkills.incrementByType((Weapon) this.Inv.itemTable.get("I"));
        }
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            WeaponSkills.incrementByType((Weapon) this.Inv.itemTable.get("J"));
        }
    }

    public int HitBonus() {
        int bonus = 0 + StrengthHitBonus() + this.Level;
        if (DualWielding() || BareHanded()) {
            return bonus;
        }
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            return bonus + WeaponSkills.GetHitBonus(((Weapon) this.Inv.itemTable.get("I")).wtype);
        }
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            return bonus + WeaponSkills.GetHitBonus(((Weapon) this.Inv.itemTable.get("J")).wtype);
        }
        return bonus;
    }

    public int DamageBonus() {
        int bonus = 0 + StrengthDmgBonus();
        if (!DualWielding() && !BareHanded()) {
            if (this.Inv.itemTable.get("I") instanceof Weapon) {
                return WeaponSkills.GetDmgBonus(((Weapon) this.Inv.itemTable.get("I")).wtype) + bonus;
            }
            if (this.Inv.itemTable.get("J") instanceof Weapon) {
                return WeaponSkills.GetDmgBonus(((Weapon) this.Inv.itemTable.get("J")).wtype) + bonus;
            }
        }
        return bonus;
    }

    public int getDamage() {
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            return getLDamage();
        }
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            return getRDamage();
        }
        return this.Level + 1 + DamageBonus();
    }

    public int getLDamage() {
        return ((Weapon) this.Inv.itemTable.get("I")).getDmg() + getLDamageBonus();
    }

    public int getRDamage() {
        return ((Weapon) this.Inv.itemTable.get("J")).getDmg() + getRDamageBonus();
    }

    public int getLDamageBonus() {
        return 0 + StrengthDmgBonus() + WeaponSkills.GetDmgBonus(((Weapon) this.Inv.itemTable.get("I")).wtype);
    }

    public int getRDamageBonus() {
        return 0 + StrengthDmgBonus() + WeaponSkills.GetDmgBonus(((Weapon) this.Inv.itemTable.get("J")).wtype);
    }

    public int getLHitBonus() {
        Weapon w = (Weapon) this.Inv.itemTable.get("I");
        return 0 + StrengthHitBonus() + this.Level + w.hitBonus + WeaponSkills.GetHitBonus(w.wtype);
    }

    public int getRHitBonus() {
        Weapon w = (Weapon) this.Inv.itemTable.get("J");
        return 0 + StrengthHitBonus() + this.Level + w.hitBonus + WeaponSkills.GetHitBonus(w.wtype);
    }

    public int Armor() {
        return Math.max(this.BaseArmor + this.Inv.Armor(), 0);
    }

    public int Evade() {
        int bonus = 0;
        if (this.Inv.itemTable.get("I") instanceof Weapon) {
            bonus = 0 + WeaponSkills.GetEvadeBonus(((Weapon) this.Inv.itemTable.get("I")).wtype);
        }
        if (this.Inv.itemTable.get("J") instanceof Weapon) {
            bonus += WeaponSkills.GetEvadeBonus(((Weapon) this.Inv.itemTable.get("J")).wtype);
        }
        if (DualWielding()) {
            bonus /= 2;
        }
        return Math.max(this.BaseEvade + this.Inv.Evade() + bonus, 0);
    }

    public int HP() {
        return this.MaxHealth + this.Inv.HP() + (ConstitutionHPBonus() * this.Level);
    }

    public int Strength() {
        return this.BaseStrength + this.Inv.Strength();
    }

    public int StrengthHitBonus() {
        switch (Strength()) {
            case 1:
                return -12;
            case 2:
                return -10;
            case R.styleable.MMAdView_refreshInterval:
                return -8;
            case R.styleable.MMAdView_accelerate:
                return -7;
            case R.styleable.MMAdView_ignoreDensityScaling:
                return -5;
            case R.styleable.MMAdView_age:
                return -4;
            case R.styleable.MMAdView_gender:
                return -3;
            case R.styleable.MMAdView_zip:
                return -2;
            case R.styleable.MMAdView_income:
                return -1;
            case R.styleable.MMAdView_keywords:
            case R.styleable.MMAdView_ethnicity:
            default:
                return 0;
            case R.styleable.MMAdView_orientation:
                return 1;
            case R.styleable.MMAdView_marital:
                return 1;
            case R.styleable.MMAdView_children:
                return 2;
            case R.styleable.MMAdView_education:
                return 2;
            case R.styleable.MMAdView_politics:
                return 3;
            case R.styleable.MMAdView_goalId:
                return 3;
            case 18:
                return 3;
            case 19:
                return 4;
            case 20:
                return 4;
            case 21:
                return 4;
            case 22:
                return 5;
            case 23:
                return 5;
            case 24:
                return 5;
            case 25:
                return 6;
        }
    }

    public int StrengthDmgBonus() {
        switch (Strength()) {
            case 1:
                return -15;
            case 2:
                return -12;
            case R.styleable.MMAdView_refreshInterval:
                return -10;
            case R.styleable.MMAdView_accelerate:
                return -8;
            case R.styleable.MMAdView_ignoreDensityScaling:
                return -6;
            case R.styleable.MMAdView_age:
                return -4;
            case R.styleable.MMAdView_gender:
                return -3;
            case R.styleable.MMAdView_zip:
                return -2;
            case R.styleable.MMAdView_income:
                return -1;
            case R.styleable.MMAdView_keywords:
            case R.styleable.MMAdView_ethnicity:
            default:
                return 0;
            case R.styleable.MMAdView_orientation:
                return 1;
            case R.styleable.MMAdView_marital:
                return 2;
            case R.styleable.MMAdView_children:
                return 2;
            case R.styleable.MMAdView_education:
                return 3;
            case R.styleable.MMAdView_politics:
                return 3;
            case R.styleable.MMAdView_goalId:
                return 4;
            case 18:
                return 4;
            case 19:
                return 5;
            case 20:
                return 5;
            case 21:
                return 6;
            case 22:
                return 6;
            case 23:
                return 7;
            case 24:
                return 7;
            case 25:
                return 8;
        }
    }

    public int Dexterity() {
        return this.BaseDexterity + this.Inv.Dexterity();
    }

    public int Constitution() {
        return this.BaseConstitution + this.Inv.Constitution();
    }

    public int ConstitutionHPBonus() {
        switch (Constitution()) {
            case 1:
                return -9;
            case 2:
                return -8;
            case R.styleable.MMAdView_refreshInterval:
                return -7;
            case R.styleable.MMAdView_accelerate:
                return -6;
            case R.styleable.MMAdView_ignoreDensityScaling:
                return -5;
            case R.styleable.MMAdView_age:
                return -4;
            case R.styleable.MMAdView_gender:
                return -3;
            case R.styleable.MMAdView_zip:
                return -2;
            case R.styleable.MMAdView_income:
                return -1;
            case R.styleable.MMAdView_keywords:
            default:
                return 0;
            case R.styleable.MMAdView_ethnicity:
                return 1;
            case R.styleable.MMAdView_orientation:
                return 1;
            case R.styleable.MMAdView_marital:
                return 2;
            case R.styleable.MMAdView_children:
                return 2;
            case R.styleable.MMAdView_education:
                return 3;
            case R.styleable.MMAdView_politics:
                return 3;
            case R.styleable.MMAdView_goalId:
                return 4;
            case 18:
                return 4;
            case 19:
                return 5;
            case 20:
                return 5;
            case 21:
                return 6;
            case 22:
                return 6;
            case 23:
                return 7;
            case 24:
                return 7;
            case 25:
                return 8;
        }
    }

    public int Intelligence() {
        return this.BaseIntelligence + this.Inv.Intelligence();
    }

    public int Wisdom() {
        return this.BaseWisdom + this.Inv.Wisdom();
    }

    public int Charisma() {
        return this.BaseCharisma + this.Inv.Charisma();
    }

    public int Speed() {
        return this.BaseSpeed + this.Inv.Speed();
    }

    public int Perception() {
        return this.BasePerception + this.Inv.Perception();
    }

    public String printLocation() {
        return "Player is at X: " + Integer.toString(this.xpos) + " Y: " + Integer.toString(this.ypos);
    }
}
