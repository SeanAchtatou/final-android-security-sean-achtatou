package gadlor.watcher;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.inputmethodservice.Keyboard;
import com.millennialmedia.android.R;

public class LatinKeyboard extends Keyboard {
    private Keyboard.Key mEnterKey;

    public LatinKeyboard(Context context, int xmlLayoutResId) {
        super(context, xmlLayoutResId);
    }

    public LatinKeyboard(Context context, int layoutTemplateResId, CharSequence characters, int columns, int horizontalPadding) {
        super(context, layoutTemplateResId, characters, columns, horizontalPadding);
    }

    /* access modifiers changed from: protected */
    public Keyboard.Key createKeyFromXml(Resources res, Keyboard.Row parent, int x, int y, XmlResourceParser parser) {
        Keyboard.Key key = new LatinKey(res, parent, x, y, parser);
        if (key.codes[0] == 10) {
            this.mEnterKey = key;
        }
        return key;
    }

    /* access modifiers changed from: package-private */
    public void setImeOptions(Resources res, int options) {
        if (this.mEnterKey != null) {
            switch (1073742079 & options) {
                case 2:
                    this.mEnterKey.iconPreview = null;
                    this.mEnterKey.icon = null;
                    this.mEnterKey.label = res.getText(R.string.label_go_key);
                    return;
                case R.styleable.MMAdView_refreshInterval:
                    this.mEnterKey.icon = res.getDrawable(R.drawable.sym_keyboard_search);
                    this.mEnterKey.label = null;
                    return;
                case R.styleable.MMAdView_accelerate:
                    this.mEnterKey.iconPreview = null;
                    this.mEnterKey.icon = null;
                    this.mEnterKey.label = res.getText(R.string.label_send_key);
                    return;
                case R.styleable.MMAdView_ignoreDensityScaling:
                    this.mEnterKey.iconPreview = null;
                    this.mEnterKey.icon = null;
                    this.mEnterKey.label = res.getText(R.string.label_next_key);
                    return;
                default:
                    this.mEnterKey.icon = res.getDrawable(R.drawable.sym_keyboard_return);
                    this.mEnterKey.label = null;
                    return;
            }
        }
    }

    static class LatinKey extends Keyboard.Key {
        public LatinKey(Resources res, Keyboard.Row parent, int x, int y, XmlResourceParser parser) {
            super(res, parent, x, y, parser);
        }

        public boolean isInside(int x, int y) {
            return super.isInside(x, this.codes[0] == -3 ? y - 10 : y);
        }
    }
}
