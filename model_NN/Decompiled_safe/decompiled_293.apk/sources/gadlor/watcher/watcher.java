package gadlor.watcher;

import android.app.Activity;
import android.content.res.Configuration;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Random;

public class watcher extends Activity implements View.OnKeyListener, KeyboardView.OnKeyboardActionListener {
    public static String Tag = "NWBD";
    public static float scrollX = 0.0f;
    public static String statusMsg = "";
    public static float touchX = 0.0f;
    public MapMaker m;
    public ArrayList<Monster> monsterList;
    public Player p;
    public LatinKeyboard qwertyKeyboard;
    public LatinKeyboard symbolsKeyboard;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        TextView mapView = (TextView) findViewById(R.id.mapText);
        mapView.setOnKeyListener(this);
        mapView.requestFocus();
        mapView.setFocusableInTouchMode(true);
        this.monsterList = MainApplication.getMapMaker().monsterList;
        this.m = MainApplication.getMapMaker();
        this.p = MainApplication.getPlayer();
        if (MainApplication.getInstance().getResources().getConfiguration().orientation == 1) {
            setContentView((int) R.layout.main);
            this.qwertyKeyboard = new LatinKeyboard(MainApplication.getInstance().getApplicationContext(), R.xml.qwerty);
            this.symbolsKeyboard = new LatinKeyboard(MainApplication.getInstance().getApplicationContext(), R.xml.symbols);
            LatinKeyboardView keyboard = (LatinKeyboardView) findViewById(R.id.keyboard);
            keyboard.setKeyboard(this.qwertyKeyboard);
            keyboard.setOnKeyboardActionListener(this);
            this.monsterList = MainApplication.getMapMaker().monsterList;
            this.m = MainApplication.getMapMaker();
            this.p = MainApplication.getPlayer();
            GameState.Portrait = true;
        }
        this.m.setVisible();
        drawEverything();
        mapView.requestFocus();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 1) {
            setContentView((int) R.layout.main);
            this.qwertyKeyboard = new LatinKeyboard(MainApplication.getInstance().getApplicationContext(), R.xml.qwerty);
            this.symbolsKeyboard = new LatinKeyboard(MainApplication.getInstance().getApplicationContext(), R.xml.symbols);
            LatinKeyboardView keyboard = (LatinKeyboardView) findViewById(R.id.keyboard);
            keyboard.setKeyboard(this.qwertyKeyboard);
            keyboard.setOnKeyboardActionListener(this);
            this.monsterList = MainApplication.getMapMaker().monsterList;
            this.m = MainApplication.getMapMaker();
            this.p = MainApplication.getPlayer();
            GameState.Portrait = true;
            drawEverything();
        }
        if (newConfig.orientation == 2) {
            setContentView((int) R.layout.main);
            this.monsterList = MainApplication.getMapMaker().monsterList;
            this.m = MainApplication.getMapMaker();
            this.p = MainApplication.getPlayer();
            GameState.Portrait = false;
            drawEverything();
        }
    }

    public boolean checkMonsterWanderCollision(int proposedX, int proposedY) {
        if (proposedY < 1 || proposedY > this.m.getHeight() - 2) {
            return true;
        }
        if (proposedX < 1 || proposedX > this.m.getWidth() - 2) {
            return true;
        }
        if (!this.m.walkmap[proposedX][proposedY].Walkable) {
            return true;
        }
        for (int i = 0; i < this.monsterList.size(); i++) {
            if (proposedX == this.monsterList.get(i).X && proposedY == this.monsterList.get(i).Y) {
                return true;
            }
        }
        return false;
    }

    public void onKey(int primaryCode, int[] keyCodes) {
        boolean z;
        boolean z2;
        Keyboard current;
        LatinKeyboardView keyboard = (LatinKeyboardView) findViewById(R.id.keyboard);
        if (primaryCode == -2 && keyboard != null) {
            if (keyboard.getKeyboard() == this.symbolsKeyboard) {
                current = this.qwertyKeyboard;
            } else {
                current = this.symbolsKeyboard;
            }
            keyboard.setKeyboard(current);
            if (current == this.symbolsKeyboard) {
                current.setShifted(false);
            }
        } else if (primaryCode == -1 && keyboard != null && keyboard.getKeyboard() == this.qwertyKeyboard) {
            if (keyboard.isShifted()) {
                z = false;
            } else {
                z = true;
            }
            keyboard.setShifted(z);
            if (GameState.KeyboardShift) {
                z2 = false;
            } else {
                z2 = true;
            }
            GameState.KeyboardShift = z2;
        } else {
            handleKeys(primaryCode, -1, KeyCharacterMap.load(0).getDisplayLabel(primaryCode));
        }
    }

    public void onPress(int primaryCode) {
    }

    public void onRelease(int primaryCode) {
    }

    public void onText(CharSequence text) {
    }

    public void swipeDown() {
    }

    public void swipeLeft() {
    }

    public void swipeRight() {
    }

    public void swipeUp() {
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        handleKeys(event.getUnicodeChar(), keyCode, event.getDisplayLabel());
        return true;
    }

    public void handleKeys(int unicodeChar, int keyCode, char displayLabel) {
        if (GameState.KeyboardShift) {
            unicodeChar -= 32;
            GameState.KeyboardShift = false;
            ((LatinKeyboardView) findViewById(R.id.keyboard)).setShifted(false);
        }
        if (MainApplication.getDStack().HandleInput(unicodeChar, keyCode)) {
            if (GameState.PlayerResting) {
                int restCount = 0;
                while (true) {
                    if (GameState.PlayerResting) {
                        updateWorld();
                        restCount++;
                        if (restCount > 50) {
                            GameState.PlayerResting = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            } else {
                updateWorld();
            }
        }
        drawEverything();
    }

    public void wanderMonster(Monster m2) {
        Random generator = new Random();
        int negativeX = generator.nextInt(101);
        int negativeY = generator.nextInt(101);
        int xMove = generator.nextInt(2);
        int yMove = generator.nextInt(2);
        if (negativeX > 50) {
            xMove *= -1;
        }
        if (negativeY > 50) {
            yMove *= -1;
        }
        if (!(xMove == 0 && yMove == 0) && !checkMonsterWanderCollision(m2.X + xMove, m2.Y + yMove)) {
            m2.X += xMove;
            m2.Y += yMove;
        }
    }

    public void updateWorld() {
        GameState.TurnsElapsed++;
        MapMaker map = MainApplication.getMapMaker();
        this.monsterList = map.monsterList;
        map.setVisible();
        Random generator = new Random();
        if (generator.nextInt(100) < this.p.HealingFactor && this.p.CurrentHealth < this.p.HP()) {
            this.p.CurrentHealth++;
        }
        this.p.ApplyEffects();
        if (GameState.MonsterSpawn && generator.nextInt(101) > 97) {
            addGeneratedMonster(MonsterGenerator.getMonsterByLevel(GameState.CurrentLevel));
            int monsterExplosionChance = generator.nextInt(1001);
            Log.d("NWBD", "Monster Explosion chance: " + monsterExplosionChance);
            if (monsterExplosionChance > 975) {
                int monstersInExplosion = generator.nextInt(15);
                Log.d("NWBD", "Monster Explosion: " + monstersInExplosion);
                for (int i = 0; i < monstersInExplosion; i++) {
                    addGeneratedMonster(MonsterGenerator.getMonsterByLevel(GameState.CurrentLevel));
                }
            }
        }
        for (int i2 = 0; i2 < this.monsterList.size(); i2++) {
            if (!this.monsterList.get(i2).Aware) {
                wanderMonster(this.monsterList.get(i2));
            } else {
                int monsterX = this.monsterList.get(i2).X;
                int monsterY = this.monsterList.get(i2).Y;
                WalkNode[][] monsterWalkMap = this.m.getWalkMap();
                for (int j = 0; j < this.monsterList.size(); j++) {
                    if (i2 != j) {
                        monsterWalkMap[this.monsterList.get(j).X][this.monsterList.get(j).Y].Walkable = false;
                    }
                }
                ArrayList<WalkNode> path = PathFinder.findMonsterPath(this.monsterList.get(i2), monsterWalkMap, this.m.getHeight(), this.m.getWidth(), this.monsterList, this.p);
                if (Math.abs(this.p.getX() - monsterX) <= 1 && Math.abs(this.p.getY() - monsterY) <= 1) {
                    Monster m2 = this.monsterList.get(i2);
                    for (int j2 = 0; j2 < m2.numAttacks; j2++) {
                        if (m2.hitBonus + DieRoller.RollDie("1d20") >= this.p.Evade()) {
                            StatusMessage.append("The " + m2.Name + " hits you!");
                            this.p.CurrentHealth -= Math.max(m2.getDmg() - this.p.Armor(), 0);
                            if (m2.hasSpecialAttack && m2.Attack.RequiresHit) {
                                m2.Attack.inflictAttack(this.p);
                            }
                        } else {
                            StatusMessage.append("The " + m2.Name + " misses you!");
                        }
                    }
                    if (m2.hasSpecialAttack && !m2.Attack.RequiresHit) {
                        m2.Attack.inflictAttack(this.p);
                    }
                    if ((this.p.CurrentHealth <= 0 || this.p.StatZeroCheck()) && GameState.KillerMonster == "") {
                        GameState.KillerMonster = m2.Name;
                    }
                } else if (path == null) {
                    wanderMonster(this.monsterList.get(i2));
                } else if (path.size() > 1) {
                    this.monsterList.get(i2).X = path.get(path.size() - 2).X;
                    this.monsterList.get(i2).Y = path.get(path.size() - 2).Y;
                }
            }
        }
        if (this.p.CurrentHealth <= 0 || this.p.StatZeroCheck()) {
            StatusMessage.append("You die...");
        }
    }

    public void addGeneratedMonster(Monster generatedMonster) {
        Random generator = new Random();
        while (true) {
            int mX = generator.nextInt(this.m.getWidth() - 2) + 1;
            int mY = generator.nextInt(this.m.getHeight() - 2) + 1;
            if (!(mX == this.p.getX() && mY == this.p.getY()) && this.m.walkmap[mX][mY].Walkable && !this.m.walkmap[mX][mY].Visible) {
                generatedMonster.X = mX;
                generatedMonster.Y = mY;
                this.monsterList.add(generatedMonster);
                return;
            }
        }
    }

    public void drawEverything() {
        DisplayStack stack = MainApplication.getDStack();
        TextView mapTextView = (TextView) findViewById(R.id.mapText);
        if (stack.WrapMainText()) {
            mapTextView.setHorizontallyScrolling(false);
        } else {
            mapTextView.setHorizontallyScrolling(true);
        }
        mapTextView.setText(stack.getMainText());
        ((TextView) findViewById(R.id.HUD)).setText(stack.getHUDText());
        ((TextView) findViewById(R.id.status)).setText(stack.getStatusText());
    }
}
