package gadlor.watcher;

import java.util.Random;

public class DieRoller {
    public static Random generator = new Random();

    public static int RollDie(int die) {
        return generator.nextInt(die) + 1;
    }

    public static int RollDie(String dieInfo) {
        int dieAmount;
        int dPosition = dieInfo.indexOf("d");
        int modPlusPosition = dieInfo.indexOf("+");
        int modMinusPosition = dieInfo.indexOf("-");
        int dieNumber = Integer.parseInt(dieInfo.substring(0, dPosition));
        int modifier = 0;
        if (modPlusPosition > 0) {
            dieAmount = Integer.parseInt(dieInfo.substring(dPosition + 1, modPlusPosition));
            modifier = Integer.parseInt(dieInfo.substring(modPlusPosition + 1));
        } else if (modMinusPosition > 0) {
            dieAmount = Integer.parseInt(dieInfo.substring(dPosition + 1, modMinusPosition));
            modifier = Integer.parseInt(dieInfo.substring(modMinusPosition + 1));
        } else {
            dieAmount = Integer.parseInt(dieInfo.substring(dPosition + 1));
        }
        int amount = 0;
        for (int i = 0; i < dieNumber; i++) {
            amount += generator.nextInt(dieAmount) + 1;
        }
        if (modPlusPosition > 0) {
            return amount + modifier;
        }
        return amount - modifier;
    }

    public static int RollDieWithMin(String dieInfo, int minimum) {
        int amount = 0;
        while (amount < minimum) {
            amount = RollDie(dieInfo);
        }
        return amount;
    }

    public static int RollDieGaussian(String dieInfo) {
        return 0;
    }

    public static int RollDieWithMin(int die, int minimum) {
        int amount = 0;
        while (amount < minimum) {
            amount = RollDie(die);
        }
        return amount;
    }
}
