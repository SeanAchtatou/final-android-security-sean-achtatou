package gadlor.watcher;

import gadlor.watcher.Items.Item;
import java.util.Comparator;

public class ItemSorter implements Comparator<Item> {
    public int compare(Item i1, Item i2) {
        return i1.Rarity - i2.Rarity;
    }
}
