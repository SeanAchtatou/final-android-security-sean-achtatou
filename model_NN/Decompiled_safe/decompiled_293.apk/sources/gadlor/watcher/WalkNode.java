package gadlor.watcher;

import gadlor.watcher.Items.Item;
import java.util.ArrayList;

public class WalkNode {
    public boolean Closed = false;
    public char DisplayChar;
    public int G;
    public ArrayList<Item> Items = new ArrayList<>();
    public boolean Open = false;
    public boolean Stairs = false;
    public boolean Viewed = false;
    public boolean Visible = false;
    public boolean Walkable;
    public int Weight = 1;
    public int X;
    public int Y;
    public WalkNode parent;

    public String printNode() {
        String location = "X: " + Integer.toString(this.X) + " Y: " + Integer.toString(this.Y);
        if (this.parent == null) {
            return String.valueOf(location) + "\nnull parent";
        }
        WalkNode p = this.parent;
        return String.valueOf(location) + "Parent X: " + Integer.toString(p.X) + " Y: " + Integer.toString(p.Y);
    }

    public WalkNode clone() {
        WalkNode clone = new WalkNode();
        clone.Walkable = this.Walkable;
        clone.X = this.X;
        clone.Y = this.Y;
        clone.parent = this.parent;
        clone.G = this.G;
        clone.DisplayChar = this.DisplayChar;
        return clone;
    }
}
