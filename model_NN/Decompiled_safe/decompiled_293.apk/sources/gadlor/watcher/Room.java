package gadlor.watcher;

import java.util.ArrayList;

public class Room {
    public int height;
    public ArrayList<Point> openings = new ArrayList<>();
    public int width;
    public int xpos;
    public int ypos;

    public Room(int w, int h, int x, int y) {
        this.width = w;
        this.height = h;
        this.xpos = x;
        this.ypos = y;
    }

    public void addOpening(Point p) {
        this.openings.add(p);
    }

    public boolean Connected() {
        for (int i = 0; i < this.openings.size(); i++) {
            if (!this.openings.get(i).Connected) {
                return false;
            }
        }
        return true;
    }

    public boolean PartiallyConnected() {
        int connected = 0;
        int unconnected = 0;
        for (int i = 0; i < this.openings.size(); i++) {
            if (this.openings.get(i).Connected) {
                connected++;
            } else {
                unconnected++;
            }
        }
        if (connected <= 0 || unconnected <= 0) {
            return false;
        }
        return true;
    }

    public boolean FullyUnconnected() {
        for (int i = 0; i < this.openings.size(); i++) {
            if (this.openings.get(i).Connected) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<Point> getUnconnected() {
        ArrayList<Point> pointList = new ArrayList<>();
        for (int i = 0; i < this.openings.size(); i++) {
            if (!this.openings.get(i).Connected) {
                pointList.add(this.openings.get(i));
            }
        }
        return pointList;
    }

    public boolean Equals(Room r) {
        if (r.xpos == this.xpos && r.ypos == this.ypos) {
            return true;
        }
        return false;
    }

    public boolean isAdjacentToOpening(int x, int y) {
        for (int i = 0; i < this.openings.size(); i++) {
            Point p = this.openings.get(i);
            if (Math.abs(p.X - y) == 1 || Math.abs(p.Y - y) == 1) {
                return true;
            }
        }
        return false;
    }

    public int HalfwayX() {
        return this.xpos + (this.width / 2);
    }

    public int HalfwayY() {
        return this.ypos + (this.height / 2);
    }
}
