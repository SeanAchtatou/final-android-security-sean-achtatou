package gadlor.watcher;

import android.util.Log;
import gadlor.watcher.Items.Item;
import gadlor.watcher.Items.Potion;
import gadlor.watcher.Items.Shield;
import gadlor.watcher.Items.Weapon;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

public class Inventory {
    public ArrayList<Item> Items = new ArrayList<>();
    public String[] itemAlphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S"};
    public Hashtable<String, Item> itemTable = new Hashtable<>();

    public Inventory() {
        this.itemTable.put("A", new Item());
        this.itemTable.put("B", new Item());
        this.itemTable.put("C", new Item());
        this.itemTable.put("D", new Item());
        this.itemTable.put("E", new Item());
        this.itemTable.put("F", new Item());
        this.itemTable.put("G", new Item());
        this.itemTable.put("H", new Item());
        this.itemTable.put("I", new Item());
        this.itemTable.put("J", new Item());
        this.itemTable.put("K", new Item());
        this.itemTable.put("L", new Item());
        this.itemTable.put("M", new Item());
        this.itemTable.put("N", new Item());
        this.itemTable.put("O", new Item());
    }

    public ArrayList<String> printCarried() {
        ArrayList<String> itemList = new ArrayList<>();
        for (int i = 0; i < this.Items.size(); i++) {
            itemList.add(this.Items.get(i).toString() + "\n");
        }
        return itemList;
    }

    public String printCarriedForDrop(int offset) {
        StringBuilder sb = new StringBuilder();
        int i = offset;
        while (i < this.Items.size() && i - offset <= 12) {
            sb.append(this.itemAlphabet[i - offset]);
            sb.append(")");
            sb.append(this.Items.get(i).toString());
            sb.append("\n");
            i++;
        }
        return sb.toString();
    }

    public void equipForTest(int category, Item item) {
        this.itemTable.put(this.itemAlphabet[category], item);
    }

    public void equipByCategoryAndIndex(int category, int index, int offset) {
        ArrayList<Item> temp = new ArrayList<>();
        for (int i = 0; i < this.Items.size(); i++) {
            if (this.Items.get(i).Heading.contains(this.itemAlphabet[category])) {
                temp.add(this.Items.get(i));
            }
        }
        if (index + 1 + offset <= temp.size() && category <= 14) {
            Item itemToEquip = (Item) temp.get(index + offset);
            if (itemToEquip instanceof Weapon) {
                if (((Weapon) itemToEquip).twoHanded) {
                    if ((this.itemTable.get(this.itemAlphabet[8]) instanceof Weapon) || (this.itemTable.get(this.itemAlphabet[9]) instanceof Weapon) || (this.itemTable.get(this.itemAlphabet[8]) instanceof Shield) || (this.itemTable.get(this.itemAlphabet[9]) instanceof Shield)) {
                        StatusMessage.append("You need two hands free to use a two-handed weapon.");
                        return;
                    }
                } else if ((this.itemTable.get(this.itemAlphabet[8]) instanceof Weapon) && ((Weapon) this.itemTable.get(this.itemAlphabet[8])).twoHanded) {
                    StatusMessage.append("You need two hands free to use a two-handed weapon.");
                    return;
                } else if ((this.itemTable.get(this.itemAlphabet[9]) instanceof Weapon) && ((Weapon) this.itemTable.get(this.itemAlphabet[9])).twoHanded) {
                    StatusMessage.append("You need two hands free to use a two-handed weapon.");
                    return;
                }
            }
            if (itemToEquip instanceof Shield) {
                if ((this.itemTable.get(this.itemAlphabet[8]) instanceof Weapon) && ((Weapon) this.itemTable.get(this.itemAlphabet[8])).twoHanded) {
                    StatusMessage.append("You cannot wield a shield and use a two-handed weapon.");
                    return;
                } else if ((this.itemTable.get(this.itemAlphabet[9]) instanceof Weapon) && ((Weapon) this.itemTable.get(this.itemAlphabet[9])).twoHanded) {
                    StatusMessage.append("You cannot wield a shield and use a two-handed weapon.");
                    return;
                } else if ((this.itemTable.get(this.itemAlphabet[8]) instanceof Shield) || (this.itemTable.get(this.itemAlphabet[9]) instanceof Shield)) {
                    StatusMessage.append("You cannot use two shields.");
                    return;
                }
            }
            Item oldItem = this.itemTable.get(this.itemAlphabet[category]);
            this.itemTable.remove(this.itemAlphabet[category]);
            this.itemTable.put(this.itemAlphabet[category], (Item) temp.get(index + offset));
            if (oldItem.Heading != "-") {
                this.Items.add(oldItem);
            }
            removeItemByUUID(((Item) temp.get(index + offset)).ID);
        }
    }

    public void useByCategoryAndIndex(int category, int index, int inventoryOffset) {
        ArrayList<Item> temp = new ArrayList<>();
        for (int i = 0; i < this.Items.size(); i++) {
            if (this.Items.get(i).Heading.contains(this.itemAlphabet[category])) {
                temp.add(this.Items.get(i));
            }
        }
        if (index + 1 + inventoryOffset <= temp.size()) {
            Potion pot = (Potion) temp.get(index + inventoryOffset);
            Player p = MainApplication.getPlayer();
            StatusMessage.append("You drink the " + pot.Description + ". ");
            pot.e.ApplyEffect(p);
            removeItemByUUID(((Item) temp.get(index + inventoryOffset)).ID);
        }
    }

    public boolean isEquipped(int category) {
        if (this.itemTable.get(this.itemAlphabet[category]).Heading == "-") {
            return false;
        }
        return true;
    }

    public void unequipItem(int category) {
        this.itemTable.remove(this.itemAlphabet[category]);
        this.Items.add(this.itemTable.get(this.itemAlphabet[category]));
        this.itemTable.put(this.itemAlphabet[category], new Item());
    }

    public void dropItem(int item, int offset) {
        int position = item + offset;
        if (position <= this.Items.size() - 1) {
            Log.d("NWBD", "item offset position: " + item + " " + offset + " " + position);
            Item droppedItem = this.Items.get(position);
            this.Items.remove(position);
            Player p = MainApplication.getPlayer();
            MapMaker m = MainApplication.getMapMaker();
            m.walkmap[p.getX()][p.getY()].Items.add(droppedItem);
            droppedItem.X = p.getX();
            droppedItem.Y = p.getY();
            m.itemList.add(droppedItem);
            StatusMessage.append("You drop the " + droppedItem.Description + ".");
        }
    }

    public void removeItemByUUID(UUID id) {
        for (int i = 0; i < this.Items.size(); i++) {
            if (this.Items.get(i).ID.equals(id)) {
                this.Items.remove(i);
                return;
            }
        }
    }

    public String printByCategory(int category, int offset) {
        StringBuilder sb = new StringBuilder();
        int matches = 0;
        for (int i = offset; i < this.Items.size() && matches <= 12; i++) {
            if (this.Items.get(i).Heading.contains(this.itemAlphabet[category])) {
                sb.append(this.itemAlphabet[matches]);
                sb.append(")");
                sb.append(this.Items.get(i).toString());
                sb.append("\n");
                matches++;
            }
        }
        return sb.toString();
    }

    public String printEquipped() {
        return "a: Head: \t\t" + this.itemTable.get("A").toString() + "\n" + "b: Neck: \t\t" + this.itemTable.get("B").toString() + "\n" + "c: Chest: \t\t" + this.itemTable.get("C").toString() + "\n" + "d: Cloak: \t\t" + this.itemTable.get("D").toString() + "\n" + "e: Belt: \t\t" + this.itemTable.get("E").toString() + "\n" + "f: Gauntlet: \t\t" + this.itemTable.get("F").toString() + "\n" + "g: Timepiece: \t\t" + this.itemTable.get("G").toString() + "\n" + "h: Tattoo: \t\t" + this.itemTable.get("H").toString() + "\n" + "i: Left hand: \t\t" + this.itemTable.get("I").toString() + "\n" + "j: Right hand: \t\t" + this.itemTable.get("J").toString() + "\n" + "k: Boots: \t\t" + this.itemTable.get("K").toString() + "\n" + "l: Bracers: \t\t" + this.itemTable.get("L").toString() + "\n" + "m: Tool: \t\t" + this.itemTable.get("M").toString() + "\n" + "n: Left ring: \t\t" + this.itemTable.get("N").toString() + "\n" + "o: Right ring: \t\t" + this.itemTable.get("O").toString();
    }

    public int Armor() {
        int armor = 0;
        for (int i = 0; i <= 14; i++) {
            armor += this.itemTable.get(this.itemAlphabet[i]).Armor;
        }
        return armor;
    }

    public int Evade() {
        int evade = 0;
        for (int i = 0; i <= 14; i++) {
            evade += this.itemTable.get(this.itemAlphabet[i]).Evade;
        }
        return evade;
    }

    public int HP() {
        int hp = 0;
        for (int i = 0; i <= 14; i++) {
            hp += this.itemTable.get(this.itemAlphabet[i]).HP;
        }
        return hp;
    }

    public int Strength() {
        int s = 0;
        for (int i = 0; i <= 14; i++) {
            s += this.itemTable.get(this.itemAlphabet[i]).StrengthMod;
        }
        return s;
    }

    public int Dexterity() {
        int d = 0;
        for (int i = 0; i <= 14; i++) {
            d += this.itemTable.get(this.itemAlphabet[i]).DexterityMod;
        }
        return d;
    }

    public int Constitution() {
        int c = 0;
        for (int i = 0; i <= 14; i++) {
            c += this.itemTable.get(this.itemAlphabet[i]).ConstitutionMod;
        }
        return c;
    }

    public int Intelligence() {
        int intelligence = 0;
        for (int i = 0; i <= 14; i++) {
            intelligence += this.itemTable.get(this.itemAlphabet[i]).IntelligenceMod;
        }
        return intelligence;
    }

    public int Wisdom() {
        int w = 0;
        for (int i = 0; i <= 14; i++) {
            w += this.itemTable.get(this.itemAlphabet[i]).WisdomMod;
        }
        return w;
    }

    public int Charisma() {
        int c = 0;
        for (int i = 0; i <= 14; i++) {
            c += this.itemTable.get(this.itemAlphabet[i]).CharismaMod;
        }
        return c;
    }

    public int Speed() {
        int s = 0;
        for (int i = 0; i <= 14; i++) {
            s += this.itemTable.get(this.itemAlphabet[i]).SpeedMod;
        }
        return s;
    }

    public int Perception() {
        int p = 0;
        for (int i = 0; i <= 14; i++) {
            p += this.itemTable.get(this.itemAlphabet[i]).PerceptionMod;
        }
        return p;
    }
}
