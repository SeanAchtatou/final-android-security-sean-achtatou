package gadlor.watcher;

public class NPC {
    public char Appearance;
    public String Name;
    public boolean Wandering;
    public int X;
    public int Y;
    public DialogueTree dialogue;

    public NPC(char A, String n, int x, int y) {
        this.Appearance = A;
        this.Name = n;
        this.X = x;
        this.Y = y;
    }
}
