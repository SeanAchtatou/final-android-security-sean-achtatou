package gadlor.watcher;

public class Point {
    public boolean Connected = false;
    public int X;
    public int Y;

    public Point(int _x, int _y) {
        this.X = _x;
        this.Y = _y;
    }
}
