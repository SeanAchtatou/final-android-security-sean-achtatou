package gadlor.watcher;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int candidate_background = 2131165187;
        public static final int candidate_normal = 2131165184;
        public static final int candidate_other = 2131165186;
        public static final int candidate_recommended = 2131165185;
    }

    public static final class dimen {
        public static final int candidate_font_height = 2131230721;
        public static final int candidate_vertical_padding = 2131230722;
        public static final int key_height = 2131230720;
    }

    public static final class drawable {
        public static final int icon_high = 2130837504;
        public static final int icon_low = 2130837505;
        public static final int icon_med = 2130837506;
        public static final int sym_keyboard_delete = 2130837507;
        public static final int sym_keyboard_done = 2130837508;
        public static final int sym_keyboard_return = 2130837509;
        public static final int sym_keyboard_search = 2130837510;
        public static final int sym_keyboard_shift = 2130837511;
        public static final int sym_keyboard_space = 2130837512;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int HUD = 2131099654;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int adView = 2131099652;
        public static final int keyboard = 2131099656;
        public static final int mapText = 2131099653;
        public static final int status = 2131099655;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int beginning = 2131034112;
        public static final int keybindings = 2131034113;
        public static final int manual = 2131034114;
        public static final int merlin = 2131034115;
        public static final int merlindialogue = 2131034116;
        public static final int titlelandscape = 2131034117;
        public static final int titleportrait = 2131034118;
    }

    public static final class string {
        public static final int app_name = 2131296256;
        public static final int label_go_key = 2131296258;
        public static final int label_next_key = 2131296259;
        public static final int label_send_key = 2131296260;
        public static final int word_separators = 2131296257;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }

    public static final class xml {
        public static final int merlindialogue = 2130968576;
        public static final int method = 2130968577;
        public static final int qwerty = 2130968578;
        public static final int symbols = 2130968579;
        public static final int symbols_shift = 2130968580;
    }
}
