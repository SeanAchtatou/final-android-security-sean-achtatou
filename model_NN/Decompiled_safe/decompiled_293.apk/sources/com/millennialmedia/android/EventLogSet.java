package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable;

class EventLogSet extends MetaData implements Parcelable {
    public static final Parcelable.Creator<EventLogSet> CREATOR = new Parcelable.Creator<EventLogSet>() {
        public EventLogSet createFromParcel(Parcel in) {
            return new EventLogSet(in);
        }

        public EventLogSet[] newArray(int size) {
            return new EventLogSet[size];
        }
    };
    String[] endActivity;
    String[] startActivity;

    EventLogSet() {
    }

    EventLogSet(Parcel in) {
        if (in != null) {
            this.startActivity = new String[in.readInt()];
            in.readStringArray(this.startActivity);
            this.endActivity = new String[in.readInt()];
            in.readStringArray(this.endActivity);
        }
    }

    EventLogSet(VideoAd videoAd) {
        if (videoAd != null) {
            this.startActivity = videoAd.startActivity;
            this.endActivity = videoAd.endActivity;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.startActivity.length);
        dest.writeStringArray(this.startActivity);
        dest.writeInt(this.endActivity.length);
        dest.writeStringArray(this.endActivity);
    }
}
