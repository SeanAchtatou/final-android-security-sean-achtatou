package org.hamcrest.core;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class IsSame<T> extends BaseMatcher<T> {
    private final T object;

    public IsSame(T object2) {
        this.object = object2;
    }

    public boolean matches(Object arg) {
        return arg == this.object;
    }

    public void describeTo(Description description) {
        description.appendText("same(").appendValue(this.object).appendText(")");
    }

    @Factory
    public static <T> Matcher<T> sameInstance(T object2) {
        return new IsSame(object2);
    }
}
