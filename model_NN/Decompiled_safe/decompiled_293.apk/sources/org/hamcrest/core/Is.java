package org.hamcrest.core;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class Is<T> extends BaseMatcher<T> {
    private final Matcher<T> matcher;

    public Is(Matcher<T> matcher2) {
        this.matcher = matcher2;
    }

    public boolean matches(Object arg) {
        return this.matcher.matches(arg);
    }

    public void describeTo(Description description) {
        description.appendText("is ").appendDescriptionOf(this.matcher);
    }

    @Factory
    public static <T> Matcher<T> is(Matcher matcher2) {
        return new Is(matcher2);
    }

    @Factory
    public static <T> Matcher<T> is(Object obj) {
        return is(IsEqual.equalTo(obj));
    }

    @Factory
    public static Matcher<Object> is(Class<?> type) {
        return is((Matcher) IsInstanceOf.instanceOf(type));
    }
}
