package org.hamcrest;

public interface Matcher<T> extends SelfDescribing {
    void _dont_implement_Matcher___instead_extend_BaseMatcher_();

    boolean matches(Object obj);
}
