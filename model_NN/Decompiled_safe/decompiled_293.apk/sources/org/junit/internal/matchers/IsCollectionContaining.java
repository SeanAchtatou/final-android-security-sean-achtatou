package org.junit.internal.matchers;

import java.util.ArrayList;
import java.util.Collection;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.core.AllOf;
import org.hamcrest.core.IsEqual;

public class IsCollectionContaining<T> extends TypeSafeMatcher<Iterable<T>> {
    private final Matcher<? extends T> elementMatcher;

    public /* bridge */ /* synthetic */ boolean matchesSafely(Object x0) {
        return matchesSafely((Iterable) ((Iterable) x0));
    }

    public IsCollectionContaining(Matcher<? extends T> elementMatcher2) {
        this.elementMatcher = elementMatcher2;
    }

    public boolean matchesSafely(Iterable<T> collection) {
        for (T item : collection) {
            if (this.elementMatcher.matches(item)) {
                return true;
            }
        }
        return false;
    }

    public void describeTo(Description description) {
        description.appendText("a collection containing ").appendDescriptionOf(this.elementMatcher);
    }

    @Factory
    public static <T> Matcher<Iterable<T>> hasItem(Matcher matcher) {
        return new IsCollectionContaining(matcher);
    }

    @Factory
    public static <T> Matcher<Iterable<T>> hasItem(Object obj) {
        return hasItem(IsEqual.equalTo(obj));
    }

    @Factory
    public static <T> Matcher<Iterable<T>> hasItems(Matcher<? extends T>... elementMatchers) {
        Collection<Matcher<? extends Iterable<T>>> all = new ArrayList<>(elementMatchers.length);
        for (Matcher<? extends T> elementMatcher2 : elementMatchers) {
            all.add(hasItem((Matcher) elementMatcher2));
        }
        return AllOf.allOf(all);
    }

    @Factory
    public static <T> Matcher<Iterable<T>> hasItems(T... elements) {
        Collection<Matcher<? extends Iterable<T>>> all = new ArrayList<>(elements.length);
        for (T element : elements) {
            all.add(hasItem((Object) element));
        }
        return AllOf.allOf(all);
    }
}
