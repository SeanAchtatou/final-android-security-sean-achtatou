package org.junit.rules;

import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class Verifier extends TestRule {
    /* access modifiers changed from: protected */
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            public void evaluate() throws Throwable {
                base.evaluate();
                Verifier.this.verify();
            }
        };
    }

    /* access modifiers changed from: protected */
    public void verify() throws Throwable {
    }
}
