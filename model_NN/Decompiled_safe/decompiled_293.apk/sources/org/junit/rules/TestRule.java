package org.junit.rules;

import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public abstract class TestRule {
    /* access modifiers changed from: protected */
    public abstract Statement apply(Statement statement, Description description);
}
