package org.junit.rules;

import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public abstract class TestWatcher extends TestRule {
    /* access modifiers changed from: protected */
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            public void evaluate() throws Throwable {
                TestWatcher.this.starting(description);
                try {
                    base.evaluate();
                    TestWatcher.this.succeeded(description);
                    TestWatcher.this.finished(description);
                } catch (Throwable th) {
                    TestWatcher.this.finished(description);
                    throw th;
                }
            }
        };
    }

    public void succeeded(Description description) {
    }

    public void failed(Throwable e, Description description) {
    }

    public void starting(Description description) {
    }

    public void finished(Description description) {
    }
}
