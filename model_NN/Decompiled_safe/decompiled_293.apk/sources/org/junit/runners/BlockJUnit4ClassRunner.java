package org.junit.runners;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.internal.runners.statements.Fail;
import org.junit.internal.runners.statements.FailOnTimeout;
import org.junit.internal.runners.statements.InvokeMethod;
import org.junit.internal.runners.statements.RunAfters;
import org.junit.internal.runners.statements.RunBefores;
import org.junit.rules.MethodRule;
import org.junit.rules.RunRules;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.FrameworkField;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

public class BlockJUnit4ClassRunner extends ParentRunner<FrameworkMethod> {
    public BlockJUnit4ClassRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    /* access modifiers changed from: protected */
    public void runChild(FrameworkMethod method, RunNotifier notifier) {
        Description description = describeChild(method);
        if (method.getAnnotation(Ignore.class) != null) {
            notifier.fireTestIgnored(description);
        } else {
            runLeaf(methodBlock(method), description, notifier);
        }
    }

    /* access modifiers changed from: protected */
    public Description describeChild(FrameworkMethod method) {
        return Description.createTestDescription(getTestClass().getJavaClass(), testName(method), method.getAnnotations());
    }

    /* access modifiers changed from: protected */
    public List<FrameworkMethod> getChildren() {
        return computeTestMethods();
    }

    /* access modifiers changed from: protected */
    public List<FrameworkMethod> computeTestMethods() {
        return getTestClass().getAnnotatedMethods(Test.class);
    }

    /* access modifiers changed from: protected */
    public void collectInitializationErrors(List<Throwable> errors) {
        super.collectInitializationErrors(errors);
        validateConstructor(errors);
        validateInstanceMethods(errors);
        validateFields(errors);
    }

    /* access modifiers changed from: protected */
    public void validateConstructor(List<Throwable> errors) {
        validateOnlyOneConstructor(errors);
        validateZeroArgConstructor(errors);
    }

    /* access modifiers changed from: protected */
    public void validateOnlyOneConstructor(List<Throwable> errors) {
        if (!hasOneConstructor()) {
            errors.add(new Exception("Test class should have exactly one public constructor"));
        }
    }

    /* access modifiers changed from: protected */
    public void validateZeroArgConstructor(List<Throwable> errors) {
        if (hasOneConstructor() && getTestClass().getOnlyConstructor().getParameterTypes().length != 0) {
            errors.add(new Exception("Test class should have exactly one public zero-argument constructor"));
        }
    }

    private boolean hasOneConstructor() {
        return getTestClass().getJavaClass().getConstructors().length == 1;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void validateInstanceMethods(List<Throwable> errors) {
        validatePublicVoidNoArgMethods(After.class, false, errors);
        validatePublicVoidNoArgMethods(Before.class, false, errors);
        validateTestMethods(errors);
        if (computeTestMethods().size() == 0) {
            errors.add(new Exception("No runnable methods"));
        }
    }

    private void validateFields(List<Throwable> errors) {
        for (FrameworkField each : getTestClass().getAnnotatedFields(Rule.class)) {
            validateRuleField(each.getField(), errors);
        }
    }

    private void validateRuleField(Field field, List<Throwable> errors) {
        Class<?> type = field.getType();
        if (!isMethodRule(type) && !isTestRule(type)) {
            errors.add(new Exception("Field " + field.getName() + " must implement MethodRule"));
        }
        if (!Modifier.isPublic(field.getModifiers())) {
            errors.add(new Exception("Field " + field.getName() + " must be public"));
        }
    }

    private boolean isTestRule(Class<?> type) {
        return TestRule.class.isAssignableFrom(type);
    }

    private boolean isMethodRule(Class<?> type) {
        return MethodRule.class.isAssignableFrom(type);
    }

    /* access modifiers changed from: protected */
    public void validateTestMethods(List<Throwable> errors) {
        validatePublicVoidNoArgMethods(Test.class, false, errors);
    }

    /* access modifiers changed from: protected */
    public Object createTest() throws Exception {
        return getTestClass().getOnlyConstructor().newInstance(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public String testName(FrameworkMethod method) {
        return method.getName();
    }

    /* access modifiers changed from: protected */
    public Statement methodBlock(FrameworkMethod method) {
        try {
            Object test = new ReflectiveCallable() {
                /* access modifiers changed from: protected */
                public Object runReflectiveCall() throws Throwable {
                    return BlockJUnit4ClassRunner.this.createTest();
                }
            }.run();
            return withRules(method, test, withAfters(method, test, withBefores(method, test, withPotentialTimeout(method, test, possiblyExpectingExceptions(method, test, methodInvoker(method, test))))));
        } catch (Throwable th) {
            return new Fail(th);
        }
    }

    /* access modifiers changed from: protected */
    public Statement methodInvoker(FrameworkMethod method, Object test) {
        return new InvokeMethod(method, test);
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public Statement possiblyExpectingExceptions(FrameworkMethod method, Object test, Statement next) {
        Test annotation = (Test) method.getAnnotation(Test.class);
        return expectsException(annotation) ? new ExpectException(next, getExpectedException(annotation)) : next;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public Statement withPotentialTimeout(FrameworkMethod method, Object test, Statement next) {
        long timeout = getTimeout((Test) method.getAnnotation(Test.class));
        return timeout > 0 ? new FailOnTimeout(next, timeout) : next;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public Statement withBefores(FrameworkMethod method, Object target, Statement statement) {
        List<FrameworkMethod> befores = getTestClass().getAnnotatedMethods(Before.class);
        return befores.isEmpty() ? statement : new RunBefores(statement, befores, target);
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public Statement withAfters(FrameworkMethod method, Object target, Statement statement) {
        List<FrameworkMethod> afters = getTestClass().getAnnotatedMethods(After.class);
        return afters.isEmpty() ? statement : new RunAfters(statement, afters, target);
    }

    private Statement withRules(FrameworkMethod method, Object target, Statement statement) {
        return withTestRules(method, target, withMethodRules(method, target, statement));
    }

    private Statement withMethodRules(FrameworkMethod method, Object target, Statement result) {
        List<TestRule> testRules = getTestRules(target);
        for (MethodRule each : getMethodRules(target)) {
            if (!testRules.contains(each)) {
                result = each.apply(result, method, target);
            }
        }
        return result;
    }

    private List<MethodRule> getMethodRules(Object target) {
        return getTestClass().getAnnotatedFieldValues(target, Rule.class, MethodRule.class);
    }

    private Statement withTestRules(FrameworkMethod method, Object target, Statement statement) {
        List<TestRule> testRules = getTestRules(target);
        return testRules.isEmpty() ? statement : new RunRules(statement, testRules, describeChild(method));
    }

    private List<TestRule> getTestRules(Object target) {
        return getTestClass().getAnnotatedFieldValues(target, Rule.class, TestRule.class);
    }

    private Class<? extends Throwable> getExpectedException(Test annotation) {
        if (annotation == null || annotation.expected() == Test.None.class) {
            return null;
        }
        return annotation.expected();
    }

    private boolean expectsException(Test annotation) {
        return getExpectedException(annotation) != null;
    }

    private long getTimeout(Test annotation) {
        if (annotation == null) {
            return 0;
        }
        return annotation.timeout();
    }
}
