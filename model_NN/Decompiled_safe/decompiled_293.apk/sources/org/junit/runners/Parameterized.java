package org.junit.runners;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestClass;

public class Parameterized extends Suite {
    private final ArrayList<Runner> runners = new ArrayList<>();

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Parameters {
    }

    private class TestClassRunnerForParameters extends BlockJUnit4ClassRunner {
        private final List<Object[]> fParameterList;
        private final int fParameterSetNumber;

        TestClassRunnerForParameters(Class<?> type, List<Object[]> parameterList, int i) throws InitializationError {
            super(type);
            this.fParameterList = parameterList;
            this.fParameterSetNumber = i;
        }

        public Object createTest() throws Exception {
            return getTestClass().getOnlyConstructor().newInstance(computeParams());
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        private Object[] computeParams() throws Exception {
            try {
                return this.fParameterList.get(this.fParameterSetNumber);
            } catch (ClassCastException e) {
                throw new Exception(String.format("%s.%s() must return a Collection of arrays.", this.getTestClass().getName(), Parameterized.this.getParametersMethod(this.getTestClass()).getName()));
            }
        }

        /* access modifiers changed from: protected */
        public String getName() {
            return String.format("[%s]", Integer.valueOf(this.fParameterSetNumber));
        }

        /* access modifiers changed from: protected */
        public String testName(FrameworkMethod method) {
            return String.format("%s[%s]", method.getName(), Integer.valueOf(this.fParameterSetNumber));
        }

        /* access modifiers changed from: protected */
        public void validateConstructor(List<Throwable> errors) {
            validateOnlyOneConstructor(errors);
        }

        /* access modifiers changed from: protected */
        public Statement classBlock(RunNotifier notifier) {
            return childrenInvoker(notifier);
        }
    }

    public Parameterized(Class<?> klass) throws Throwable {
        super(klass, Collections.emptyList());
        List<Object[]> parametersList = getParametersList(getTestClass());
        for (int i = 0; i < parametersList.size(); i++) {
            this.runners.add(new TestClassRunnerForParameters(getTestClass().getJavaClass(), parametersList, i));
        }
    }

    /* access modifiers changed from: protected */
    public List<Runner> getChildren() {
        return this.runners;
    }

    private List<Object[]> getParametersList(TestClass klass) throws Throwable {
        return (List) getParametersMethod(klass).invokeExplosively(null, new Object[0]);
    }

    /* access modifiers changed from: private */
    public FrameworkMethod getParametersMethod(TestClass testClass) throws Exception {
        for (FrameworkMethod each : testClass.getAnnotatedMethods(Parameters.class)) {
            int modifiers = each.getMethod().getModifiers();
            if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
                return each;
            }
        }
        throw new Exception("No public static parameters method on class " + testClass.getName());
    }
}
