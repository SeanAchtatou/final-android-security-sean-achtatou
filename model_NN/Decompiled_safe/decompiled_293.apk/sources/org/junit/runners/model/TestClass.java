package org.junit.runners.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;

public class TestClass {
    private final Class<?> fClass;
    private Map<Class<?>, List<FrameworkField>> fFieldsForAnnotations = new HashMap();
    private Map<Class<?>, List<FrameworkMethod>> fMethodsForAnnotations = new HashMap();

    /* JADX INFO: Multiple debug info for r0v1 java.lang.reflect.Field[]: [D('arr$' java.lang.reflect.Method[]), D('arr$' java.lang.reflect.Field[])] */
    public TestClass(Class<?> klass) {
        this.fClass = klass;
        if (klass == null || klass.getConstructors().length <= 1) {
            for (Class<?> eachClass : getSuperClasses(this.fClass)) {
                for (Method eachMethod : eachClass.getDeclaredMethods()) {
                    addToAnnotationLists(new FrameworkMethod(eachMethod), this.fMethodsForAnnotations);
                }
                for (Field eachField : eachClass.getDeclaredFields()) {
                    addToAnnotationLists(new FrameworkField(eachField), this.fFieldsForAnnotations);
                }
            }
            return;
        }
        throw new IllegalArgumentException("Test class can only have one constructor");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private <T extends org.junit.runners.model.FrameworkMember<T>> void addToAnnotationLists(T r8, java.util.Map<java.lang.Class<?>, java.util.List<T>> r9) {
        /*
            r7 = this;
            java.lang.annotation.Annotation[] r0 = r8.getAnnotations()
            int r3 = r0.length
            r2 = 0
        L_0x0006:
            if (r2 >= r3) goto L_0x0018
            r1 = r0[r2]
            java.lang.Class r5 = r1.annotationType()
            java.util.List r4 = r7.getAnnotatedMembers(r9, r5)
            boolean r6 = r8.isShadowedBy(r4)
            if (r6 == 0) goto L_0x0019
        L_0x0018:
            return
        L_0x0019:
            boolean r6 = r7.runsTopToBottom(r5)
            if (r6 == 0) goto L_0x0026
            r6 = 0
            r4.add(r6, r8)
        L_0x0023:
            int r2 = r2 + 1
            goto L_0x0006
        L_0x0026:
            r4.add(r8)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: org.junit.runners.model.TestClass.addToAnnotationLists(org.junit.runners.model.FrameworkMember, java.util.Map):void");
    }

    public List<FrameworkMethod> getAnnotatedMethods(Class<? extends Annotation> annotationClass) {
        return getAnnotatedMembers(this.fMethodsForAnnotations, annotationClass);
    }

    public List<FrameworkField> getAnnotatedFields(Class<? extends Annotation> annotationClass) {
        return getAnnotatedMembers(this.fFieldsForAnnotations, annotationClass);
    }

    private <T> List<T> getAnnotatedMembers(Map<Class<?>, List<T>> map, Class<? extends Annotation> type) {
        if (!map.containsKey(type)) {
            map.put(type, new ArrayList());
        }
        return map.get(type);
    }

    private boolean runsTopToBottom(Class<? extends Annotation> annotation) {
        return annotation.equals(Before.class) || annotation.equals(BeforeClass.class);
    }

    private List<Class<?>> getSuperClasses(Class<?> testClass) {
        ArrayList<Class<?>> results = new ArrayList<>();
        for (Class<?> current = testClass; current != null; current = current.getSuperclass()) {
            results.add(current);
        }
        return results;
    }

    public Class<?> getJavaClass() {
        return this.fClass;
    }

    public String getName() {
        if (this.fClass == null) {
            return "null";
        }
        return this.fClass.getName();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.junit.Assert.assertEquals(long, long):void
     arg types: [int, long]
     candidates:
      org.junit.Assert.assertEquals(double, double):void
      org.junit.Assert.assertEquals(java.lang.Object, java.lang.Object):void
      org.junit.Assert.assertEquals(java.lang.Object[], java.lang.Object[]):void
      org.junit.Assert.assertEquals(long, long):void */
    public Constructor<?> getOnlyConstructor() {
        Constructor<?>[] constructors = this.fClass.getConstructors();
        Assert.assertEquals(1L, (long) constructors.length);
        return constructors[0];
    }

    public Annotation[] getAnnotations() {
        if (this.fClass == null) {
            return new Annotation[0];
        }
        return this.fClass.getAnnotations();
    }

    public <T> List<T> getAnnotatedFieldValues(Object test, Class<? extends Annotation> annotationClass, Class<T> valueClass) {
        List<T> results = new ArrayList<>();
        for (FrameworkField each : getAnnotatedFields(annotationClass)) {
            try {
                Object fieldValue = each.get(test);
                if (valueClass.isInstance(fieldValue)) {
                    results.add(valueClass.cast(fieldValue));
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException("How did getFields return a field we couldn't access?");
            }
        }
        return results;
    }
}
