package org.junit.experimental.theories;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParameterSignature {
    private final Annotation[] annotations;
    private final Class<?> type;

    public static ArrayList<ParameterSignature> signatures(Method method) {
        return signatures(method.getParameterTypes(), method.getParameterAnnotations());
    }

    public static List<ParameterSignature> signatures(Constructor<?> constructor) {
        return signatures(constructor.getParameterTypes(), constructor.getParameterAnnotations());
    }

    private static ArrayList<ParameterSignature> signatures(Class<?>[] parameterTypes, Annotation[][] parameterAnnotations) {
        ArrayList<ParameterSignature> sigs = new ArrayList<>();
        for (int i = 0; i < parameterTypes.length; i++) {
            sigs.add(new ParameterSignature(parameterTypes[i], parameterAnnotations[i]));
        }
        return sigs;
    }

    private ParameterSignature(Class<?> type2, Annotation[] annotations2) {
        this.type = type2;
        this.annotations = annotations2;
    }

    public boolean canAcceptType(Class<?> candidate) {
        return this.type.isAssignableFrom(candidate);
    }

    public Class<?> getType() {
        return this.type;
    }

    public List<Annotation> getAnnotations() {
        return Arrays.asList(this.annotations);
    }

    public boolean canAcceptArrayType(Class<?> type2) {
        return type2.isArray() && canAcceptType(type2.getComponentType());
    }

    public boolean hasAnnotation(Class<? extends Annotation> type2) {
        return getAnnotation(type2) != null;
    }

    public <T extends Annotation> T findDeepAnnotation(Class<T> annotationType) {
        return findDeepAnnotation(this.annotations, annotationType, 3);
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    private <T extends Annotation> T findDeepAnnotation(Annotation[] annotations2, Class<T> annotationType, int depth) {
        if (depth == 0) {
            return null;
        }
        for (Annotation each : annotations2) {
            if (annotationType.isInstance(each)) {
                return (Annotation) annotationType.cast(each);
            }
            Annotation candidate = findDeepAnnotation(each.annotationType().getAnnotations(), annotationType, depth - 1);
            if (candidate != null) {
                return (Annotation) annotationType.cast(candidate);
            }
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
        for (Annotation each : getAnnotations()) {
            if (annotationType.isInstance(each)) {
                return (Annotation) annotationType.cast(each);
            }
        }
        return null;
    }
}
