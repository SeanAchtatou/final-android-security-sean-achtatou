package org.junit.experimental.theories;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.experimental.theories.PotentialAssignment;
import org.junit.experimental.theories.internal.Assignments;
import org.junit.experimental.theories.internal.ParameterizedAssertionError;
import org.junit.internal.AssumptionViolatedException;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestClass;

public class Theories extends BlockJUnit4ClassRunner {
    public Theories(Class<?> klass) throws InitializationError {
        super(klass);
    }

    /* access modifiers changed from: protected */
    public void collectInitializationErrors(List<Throwable> errors) {
        super.collectInitializationErrors(errors);
        validateDataPointFields(errors);
    }

    private void validateDataPointFields(List<Throwable> errors) {
        for (Field each : getTestClass().getJavaClass().getDeclaredFields()) {
            if (each.getAnnotation(DataPoint.class) != null && !Modifier.isStatic(each.getModifiers())) {
                errors.add(new Error("DataPoint field " + each.getName() + " must be static"));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void validateConstructor(List<Throwable> errors) {
        validateOnlyOneConstructor(errors);
    }

    /* access modifiers changed from: protected */
    public void validateTestMethods(List<Throwable> errors) {
        for (FrameworkMethod each : computeTestMethods()) {
            if (each.getAnnotation(Theory.class) != null) {
                each.validatePublicVoid(false, errors);
            } else {
                each.validatePublicVoidNoArg(false, errors);
            }
        }
    }

    /* access modifiers changed from: protected */
    public List<FrameworkMethod> computeTestMethods() {
        List<FrameworkMethod> testMethods = super.computeTestMethods();
        List<FrameworkMethod> theoryMethods = getTestClass().getAnnotatedMethods(Theory.class);
        testMethods.removeAll(theoryMethods);
        testMethods.addAll(theoryMethods);
        return testMethods;
    }

    public Statement methodBlock(FrameworkMethod method) {
        return new TheoryAnchor(method, getTestClass());
    }

    public static class TheoryAnchor extends Statement {
        private List<AssumptionViolatedException> fInvalidParameters = new ArrayList();
        private TestClass fTestClass;
        private FrameworkMethod fTestMethod;
        private int successes = 0;

        public TheoryAnchor(FrameworkMethod method, TestClass testClass) {
            this.fTestMethod = method;
            this.fTestClass = testClass;
        }

        private TestClass getTestClass() {
            return this.fTestClass;
        }

        public void evaluate() throws Throwable {
            runWithAssignment(Assignments.allUnassigned(this.fTestMethod.getMethod(), getTestClass()));
            if (this.successes == 0) {
                Assert.fail("Never found parameters that satisfied method assumptions.  Violated assumptions: " + this.fInvalidParameters);
            }
        }

        /* access modifiers changed from: protected */
        public void runWithAssignment(Assignments parameterAssignment) throws Throwable {
            if (!parameterAssignment.isComplete()) {
                runWithIncompleteAssignment(parameterAssignment);
            } else {
                runWithCompleteAssignment(parameterAssignment);
            }
        }

        /* access modifiers changed from: protected */
        public void runWithIncompleteAssignment(Assignments incomplete) throws InstantiationException, IllegalAccessException, Throwable {
            for (PotentialAssignment source : incomplete.potentialsForNextUnassigned()) {
                runWithAssignment(incomplete.assignNext(source));
            }
        }

        /* access modifiers changed from: protected */
        public void runWithCompleteAssignment(final Assignments complete) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, Throwable {
            new BlockJUnit4ClassRunner(getTestClass().getJavaClass()) {
                /* access modifiers changed from: protected */
                public void collectInitializationErrors(List<Throwable> list) {
                }

                public Statement methodBlock(FrameworkMethod method) {
                    final Statement statement = super.methodBlock(method);
                    return new Statement() {
                        public void evaluate() throws Throwable {
                            try {
                                statement.evaluate();
                                TheoryAnchor.this.handleDataPointSuccess();
                            } catch (AssumptionViolatedException e) {
                                TheoryAnchor.this.handleAssumptionViolation(e);
                            } catch (Throwable th) {
                                TheoryAnchor.this.reportParameterizedError(th, complete.getArgumentStrings(TheoryAnchor.this.nullsOk()));
                            }
                        }
                    };
                }

                /* access modifiers changed from: protected */
                public Statement methodInvoker(FrameworkMethod method, Object test) {
                    return TheoryAnchor.this.methodCompletesWithParameters(method, complete, test);
                }

                public Object createTest() throws Exception {
                    return getTestClass().getOnlyConstructor().newInstance(complete.getConstructorArguments(TheoryAnchor.this.nullsOk()));
                }
            }.methodBlock(this.fTestMethod).evaluate();
        }

        /* access modifiers changed from: private */
        public Statement methodCompletesWithParameters(final FrameworkMethod method, final Assignments complete, final Object freshInstance) {
            return new Statement() {
                public void evaluate() throws Throwable {
                    try {
                        method.invokeExplosively(freshInstance, complete.getMethodArguments(TheoryAnchor.this.nullsOk()));
                    } catch (PotentialAssignment.CouldNotGenerateValueException e) {
                    }
                }
            };
        }

        /* access modifiers changed from: protected */
        public void handleAssumptionViolation(AssumptionViolatedException e) {
            this.fInvalidParameters.add(e);
        }

        /* access modifiers changed from: protected */
        public void reportParameterizedError(Throwable e, Object... params) throws Throwable {
            if (params.length == 0) {
                throw e;
            }
            throw new ParameterizedAssertionError(e, this.fTestMethod.getName(), params);
        }

        /* access modifiers changed from: private */
        public boolean nullsOk() {
            Theory annotation = (Theory) this.fTestMethod.getMethod().getAnnotation(Theory.class);
            if (annotation == null) {
                return false;
            }
            return annotation.nullsAccepted();
        }

        /* access modifiers changed from: protected */
        public void handleDataPointSuccess() {
            this.successes++;
        }
    }
}
