package org.junit.experimental.theories;

public abstract class PotentialAssignment {

    public static class CouldNotGenerateValueException extends Exception {
        private static final long serialVersionUID = 1;
    }

    public abstract String getDescription() throws CouldNotGenerateValueException;

    public abstract Object getValue() throws CouldNotGenerateValueException;

    public static PotentialAssignment forValue(final String name, final Object value) {
        return new PotentialAssignment() {
            public Object getValue() throws CouldNotGenerateValueException {
                return value;
            }

            public String toString() {
                return String.format("[%s]", value);
            }

            public String getDescription() throws CouldNotGenerateValueException {
                return name;
            }
        };
    }
}
