package org.junit.experimental.categories;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.runner.Description;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerBuilder;

public class Categories extends Suite {

    @Retention(RetentionPolicy.RUNTIME)
    public @interface ExcludeCategory {
        Class<?> value();
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface IncludeCategory {
        Class<?> value();
    }

    public static class CategoryFilter extends Filter {
        private final Class<?> fExcluded;
        private final Class<?> fIncluded;

        public static CategoryFilter include(Class<?> categoryType) {
            return new CategoryFilter(categoryType, null);
        }

        public CategoryFilter(Class<?> includedCategory, Class<?> excludedCategory) {
            this.fIncluded = includedCategory;
            this.fExcluded = excludedCategory;
        }

        public String describe() {
            return "category " + this.fIncluded;
        }

        public boolean shouldRun(Description description) {
            if (hasCorrectCategoryAnnotation(description)) {
                return true;
            }
            Iterator i$ = description.getChildren().iterator();
            while (i$.hasNext()) {
                if (shouldRun(i$.next())) {
                    return true;
                }
            }
            return false;
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x003c  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private boolean hasCorrectCategoryAnnotation(org.junit.runner.Description r7) {
            /*
                r6 = this;
                r5 = 1
                r4 = 0
                java.util.List r0 = r6.categories(r7)
                boolean r3 = r0.isEmpty()
                if (r3 == 0) goto L_0x0014
                java.lang.Class<?> r3 = r6.fIncluded
                if (r3 != 0) goto L_0x0012
                r3 = r5
            L_0x0011:
                return r3
            L_0x0012:
                r3 = r4
                goto L_0x0011
            L_0x0014:
                java.util.Iterator r2 = r0.iterator()
            L_0x0018:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x0032
                java.lang.Object r1 = r2.next()
                java.lang.Class r1 = (java.lang.Class) r1
                java.lang.Class<?> r3 = r6.fExcluded
                if (r3 == 0) goto L_0x0018
                java.lang.Class<?> r3 = r6.fExcluded
                boolean r3 = r3.isAssignableFrom(r1)
                if (r3 == 0) goto L_0x0018
                r3 = r4
                goto L_0x0011
            L_0x0032:
                java.util.Iterator r2 = r0.iterator()
            L_0x0036:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x0050
                java.lang.Object r1 = r2.next()
                java.lang.Class r1 = (java.lang.Class) r1
                java.lang.Class<?> r3 = r6.fIncluded
                if (r3 == 0) goto L_0x004e
                java.lang.Class<?> r3 = r6.fIncluded
                boolean r3 = r3.isAssignableFrom(r1)
                if (r3 == 0) goto L_0x0036
            L_0x004e:
                r3 = r5
                goto L_0x0011
            L_0x0050:
                r3 = r4
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: org.junit.experimental.categories.Categories.CategoryFilter.hasCorrectCategoryAnnotation(org.junit.runner.Description):boolean");
        }

        private List<Class<?>> categories(Description description) {
            ArrayList<Class<?>> categories = new ArrayList<>();
            categories.addAll(Arrays.asList(directCategories(description)));
            categories.addAll(Arrays.asList(directCategories(parentDescription(description))));
            return categories;
        }

        private Description parentDescription(Description description) {
            Class<?> testClass = description.getTestClass();
            if (testClass == null) {
                return null;
            }
            return Description.createSuiteDescription(testClass);
        }

        private Class<?>[] directCategories(Description description) {
            if (description == null) {
                return new Class[0];
            }
            Category annotation = (Category) description.getAnnotation(Category.class);
            if (annotation == null) {
                return new Class[0];
            }
            return annotation.value();
        }
    }

    public Categories(Class<?> klass, RunnerBuilder builder) throws InitializationError {
        super(klass, builder);
        try {
            filter(new CategoryFilter(getIncludedCategory(klass), getExcludedCategory(klass)));
            assertNoCategorizedDescendentsOfUncategorizeableParents(getDescription());
        } catch (NoTestsRemainException e) {
            throw new InitializationError(e);
        }
    }

    private Class<?> getIncludedCategory(Class<?> klass) {
        IncludeCategory annotation = (IncludeCategory) klass.getAnnotation(IncludeCategory.class);
        if (annotation == null) {
            return null;
        }
        return annotation.value();
    }

    private Class<?> getExcludedCategory(Class<?> klass) {
        ExcludeCategory annotation = (ExcludeCategory) klass.getAnnotation(ExcludeCategory.class);
        if (annotation == null) {
            return null;
        }
        return annotation.value();
    }

    private void assertNoCategorizedDescendentsOfUncategorizeableParents(Description description) throws InitializationError {
        if (!canHaveCategorizedChildren(description)) {
            assertNoDescendantsHaveCategoryAnnotations(description);
        }
        Iterator i$ = description.getChildren().iterator();
        while (i$.hasNext()) {
            assertNoCategorizedDescendentsOfUncategorizeableParents(i$.next());
        }
    }

    private void assertNoDescendantsHaveCategoryAnnotations(Description description) throws InitializationError {
        Iterator i$ = description.getChildren().iterator();
        while (i$.hasNext()) {
            Description each = i$.next();
            if (each.getAnnotation(Category.class) != null) {
                throw new InitializationError("Category annotations on Parameterized classes are not supported on individual methods.");
            }
            assertNoDescendantsHaveCategoryAnnotations(each);
        }
    }

    private static boolean canHaveCategorizedChildren(Description description) {
        Iterator i$ = description.getChildren().iterator();
        while (i$.hasNext()) {
            if (i$.next().getTestClass() == null) {
                return false;
            }
        }
        return true;
    }
}
