package klwinkel.huiswerk;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import klwinkel.huiswerk.HuisWerkListBase;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkList2 extends HuisWerkListBase {
    public HuiswerkList2Adapter mAdapter;

    public void onCreate(Bundle savedInstanceState) {
        this.db = new HuiswerkDatabase(this);
        this.groupCursor = this.db.getHuiswerkDates(1);
        startManagingCursor(this.groupCursor);
        this.mAdapter = new HuiswerkList2Adapter(this.groupCursor, this, 17367046, 17367046, new String[]{"datum"}, new int[]{16908308}, new String[]{"datum"}, new int[]{16908308});
        setListAdapter(this.mAdapter);
        super.onCreate(savedInstanceState);
    }

    public class HuiswerkList2Adapter extends HuisWerkListBase.ExpandableListBaseAdapter {
        public HuiswerkList2Adapter(HuiswerkDatabase.HuiswerkDateCursor cursor, Context context, int groupLayout, int childLayout, String[] groupFrom, int[] groupTo, String[] childrenFrom, int[] childrenTo) {
            super(cursor, context, groupLayout, childLayout, groupFrom, groupTo, childrenFrom, childrenTo);
        }

        /* access modifiers changed from: protected */
        public Cursor getChildrenCursor(Cursor grpCursor) {
            Cursor childrenCursor = HuisWerkList2.this.db.getHuiswerkToetsDay((long) ((HuiswerkDatabase.HuiswerkDateCursor) grpCursor).getColDatum());
            HuisWerkList2.this.startManagingCursor(childrenCursor);
            return childrenCursor;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: SimpleMethodDetails{klwinkel.huiswerk.HuisWerkList2.HuiswerkList2Adapter.getChild(int, int):android.database.Cursor}
         arg types: [int, int]
         candidates:
          ClspMth{android.widget.CursorTreeAdapter.getChild(int, int):java.lang.Object}
          SimpleMethodDetails{klwinkel.huiswerk.HuisWerkList2.HuiswerkList2Adapter.getChild(int, int):android.database.Cursor} */
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            boolean z;
            boolean z2;
            HuiswerkDatabase.HuiswerkToetsDayCursor childCursor = (HuiswerkDatabase.HuiswerkToetsDayCursor) getChild(groupPosition, childPosition);
            this.strHoofdstuk = childCursor.getColHoofdstuk();
            this.strPagina = childCursor.getColPagina();
            this.strOmschrijving = childCursor.getColOmschrijving();
            this.strVak = childCursor.getColVakNaam();
            if (childCursor.getColKlaar() > 0) {
                z = true;
            } else {
                z = false;
            }
            this.bKlaar = Boolean.valueOf(z);
            if (childCursor.getColToets() > 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.bToets = Boolean.valueOf(z2);
            this.hwId = childCursor.getColHuiswerkId();
            HuiswerkDatabase.VakInfoCursor viC = HuisWerkList2.this.db.getVakInfo((long) childCursor.getColVakId());
            this.iVakKleur = -1;
            if (viC.getCount() > 0) {
                this.iVakKleur = viC.getColKleur().intValue();
            }
            viC.close();
            HuiswerkDatabase.HuiswerkFotoCursor chwf = HuisWerkList2.this.db.getHuiswerkFoto((long) this.hwId);
            if (chwf.getCount() > 0) {
                this.bFoto = true;
            } else {
                this.bFoto = false;
            }
            chwf.close();
            return super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            this.localCursor.moveToPosition(groupPosition);
            this.groupDatum = this.localCursor.getColDatum();
            this.iGroupCount = HuisWerkList2.this.db.getToetsCountDateNietAf(this.groupDatum);
            return super.getGroupView(groupPosition, isExpanded, convertView, parent);
        }
    }

    public void onResume() {
        this.bToetsTab = true;
        super.onResume();
    }
}
