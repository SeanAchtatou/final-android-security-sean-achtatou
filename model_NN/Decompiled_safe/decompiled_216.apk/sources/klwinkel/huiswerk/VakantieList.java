package klwinkel.huiswerk;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class VakantieList extends ListActivity {
    private static HuiswerkDatabase db;
    private static ImageButton lblNieuw;
    private static Context myContext;
    private static HuiswerkDatabase.VakantieCursor vakantieCursor;
    private final View.OnClickListener lblNieuwOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(VakantieList.this, Vakantie.class);
            Bundle b = new Bundle();
            b.putInt("_begin", 0);
            i.putExtras(b);
            VakantieList.this.startActivity(i);
        }
    };

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.vakantielist);
        myContext = this;
        lblNieuw = (ImageButton) findViewById(R.id.lblNieuw);
        lblNieuw.setOnClickListener(this.lblNieuwOnClick);
        db = new HuiswerkDatabase(this);
        vakantieCursor = db.getVakanties();
        startManagingCursor(vakantieCursor);
        setListAdapter(new VakantieDataAdapter(this, 17367043, vakantieCursor, new String[]{"_id"}, new int[]{16908308}));
        getListView().setDividerHeight(0);
    }

    public class VakantieDataAdapter extends SimpleCursorAdapter {
        private Context context;
        private HuiswerkDatabase.VakantieCursor localCursor;

        public VakantieDataAdapter(Context context2, int layout, Cursor c, String[] from, int[] to) {
            super(context2, layout, c, from, to);
            this.context = context2;
            this.localCursor = (HuiswerkDatabase.VakantieCursor) c;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.vakantielistrow, (ViewGroup) null);
            }
            this.localCursor.moveToPosition(position);
            RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.row);
            TextView txtNaam = (TextView) v.findViewById(R.id.lblNaam);
            TextView txtDatumBegin = (TextView) v.findViewById(R.id.lblDatumBegin);
            TextView txtTotEnMet = (TextView) v.findViewById(R.id.lblTotEnMet);
            TextView txtDatumEinde = (TextView) v.findViewById(R.id.lblDatumEinde);
            if (txtNaam != null) {
                txtNaam.setText(this.localCursor.getColNaam());
                if (this.localCursor.getColNaam().length() > 0) {
                    txtNaam.setVisibility(0);
                } else {
                    txtNaam.setVisibility(8);
                }
            }
            if (txtDatumBegin != null) {
                Integer iDatum = Integer.valueOf(this.localCursor.getColDatumBegin());
                Integer iYear = Integer.valueOf(iDatum.intValue() / 10000);
                Integer iMonth = Integer.valueOf((iDatum.intValue() % 10000) / 100);
                Integer iDay = Integer.valueOf(iDatum.intValue() % 100);
                txtDatumBegin.setText((String) DateFormat.format("EEEE dd MMMM yyyy", new Date(iYear.intValue() - 1900, iMonth.intValue(), iDay.intValue())));
            }
            if (this.localCursor.getColDatumEinde() <= 0 || this.localCursor.getColDatumEinde() == this.localCursor.getColDatumBegin()) {
                if (txtDatumEinde != null) {
                    txtDatumEinde.setVisibility(8);
                }
                if (txtTotEnMet != null) {
                    txtTotEnMet.setVisibility(8);
                }
            } else {
                if (txtDatumEinde != null) {
                    Integer iDatum2 = Integer.valueOf(this.localCursor.getColDatumEinde());
                    Integer iYear2 = Integer.valueOf(iDatum2.intValue() / 10000);
                    Integer iMonth2 = Integer.valueOf((iDatum2.intValue() % 10000) / 100);
                    Integer iDay2 = Integer.valueOf(iDatum2.intValue() % 100);
                    txtDatumEinde.setText((String) DateFormat.format("EEEE dd MMMM yyyy", new Date(iYear2.intValue() - 1900, iMonth2.intValue(), iDay2.intValue())));
                    txtDatumEinde.setVisibility(0);
                }
                if (txtTotEnMet != null) {
                    txtTotEnMet.setVisibility(0);
                }
            }
            return v;
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        vakantieCursor.moveToPosition(position);
        Intent i = new Intent(this, Vakantie.class);
        Bundle b = new Bundle();
        b.putInt("_begin", vakantieCursor.getColDatumBegin());
        i.putExtras(b);
        startActivity(i);
        super.onListItemClick(l, v, position, id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, getString(R.string.vakantie_menu_nieuw));
        return supRetVal;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent i = new Intent(this, Vakantie.class);
                Bundle b = new Bundle();
                b.putInt("_begin", 0);
                i.putExtras(b);
                startActivity(i);
                return true;
            default:
                return false;
        }
    }
}
