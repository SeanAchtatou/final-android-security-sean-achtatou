package klwinkel.huiswerk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class RoosterDagNew extends Activity implements View.OnTouchListener {
    public static Context MainContext = null;
    private static float fMinimumCelHeight = 50.0f;
    private static int iKortsteLes = 0;
    private static LinearLayout mDagData = null;
    private static LinearLayout mMain = null;
    private static ScrollView mScrollData = null;
    private Calendar calRoosterDag;
    private HuiswerkDatabase db;
    private float downXValue = 0.0f;
    private HuiswerkDatabase.HuiswerkVakCursor hwvCursor;
    private int iPrefNumHours = 0;
    private ImageButton lblAchteruit;
    private final View.OnClickListener lblAchteruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterDagNew.this.PreviousDay();
        }
    };
    private ImageButton lblVooruit;
    private final View.OnClickListener lblVooruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterDagNew.this.NextDay();
        }
    };
    private final View.OnClickListener lessonOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            LesUur item = RoosterDagNew.this.roosterData[v.getId()];
            if (item.les) {
                Intent i = new Intent(RoosterDagNew.this, EditHuiswerk.class);
                Bundle b = new Bundle();
                if (item.hwid != 0) {
                    b.putInt("_id", item.hwid);
                    i.putExtras(b);
                } else {
                    b.putInt("_vakid", item.vakid);
                    b.putInt("_datum", item.datum);
                    i.putExtras(b);
                }
                RoosterDagNew.this.startActivity(i);
            }
        }
    };
    LesUur[] roosterData;
    private Button roosterDatum;
    private final View.OnClickListener roosterDatumOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterDagNew.this.FirstDay();
        }
    };

    private class LesUur {
        public int begin;
        public int datum;
        public int einde;
        public String email;
        public boolean foto;
        public int hwid;
        public boolean klaar;
        public int kleur;
        public String leraar;
        public boolean les;
        public String lok;
        public String telefoon;
        public int temproosterid;
        public boolean tmp;
        public boolean toets;
        public int uur;
        public boolean vakantie;
        public int vakid;
        public String vakkort;
        public String vaklang;

        LesUur(boolean les2, boolean tmp2, boolean vakantie2, int uur2, int begin2, int einde2, String vakkort2, String vaklang2, String lok2, int vakid2, int datum2, int hwid2, boolean klaar2, boolean toets2, int temproosterid2, int kleur2, String leraar2, String telefoon2, String email2, boolean foto2) {
            this.les = les2;
            this.tmp = tmp2;
            this.vakantie = vakantie2;
            this.uur = uur2;
            this.begin = begin2;
            this.einde = einde2;
            this.vakkort = vakkort2;
            this.vaklang = vaklang2;
            this.lok = lok2;
            this.vakid = vakid2;
            this.datum = datum2;
            this.hwid = hwid2;
            this.klaar = klaar2;
            this.toets = toets2;
            this.temproosterid = temproosterid2;
            this.kleur = kleur2;
            this.leraar = leraar2;
            this.telefoon = telefoon2;
            this.email = email2;
            this.foto = foto2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.roosterdagnew);
        MainContext = getParent();
        this.roosterData = new LesUur[20];
        this.lblVooruit = (ImageButton) findViewById(R.id.lblVooruit);
        this.lblAchteruit = (ImageButton) findViewById(R.id.lblAchteruit);
        this.roosterDatum = (Button) findViewById(R.id.roosterdatum);
        this.lblVooruit.setOnClickListener(this.lblVooruitOnClick);
        this.lblAchteruit.setOnClickListener(this.lblAchteruitOnClick);
        this.roosterDatum.setOnClickListener(this.roosterDatumOnClick);
        mMain = (LinearLayout) findViewById(R.id.roostermain);
        mDagData = (LinearLayout) findViewById(R.id.lldag);
        mDagData.setOnTouchListener(this);
        mScrollData = (ScrollView) findViewById(R.id.scrolldag);
        mScrollData.setOnTouchListener(this);
        this.db = new HuiswerkDatabase(this);
        this.calRoosterDag = Calendar.getInstance();
        FirstDay();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int iuur = v.getId();
        LesUur item = this.roosterData[iuur];
        menu.setHeaderTitle(String.valueOf(item.vaklang) + " (" + item.leraar + ")");
        menu.add(0, iuur, 0, getString(R.string.huiswerkwijzigen));
        menu.add(0, iuur, 1, getString(R.string.roostereenmaligwijzigen));
        if (item.telefoon.length() > 0) {
            menu.add(0, iuur, 2, String.valueOf(getString(R.string.contextdial)) + item.telefoon);
        }
        if (item.email.length() > 0) {
            menu.add(0, iuur, 3, String.valueOf(getString(R.string.contextmail)) + item.email);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getOrder()) {
            case 0:
                function1(item.getItemId());
                break;
            case 1:
                function2(item.getItemId());
                break;
            case 2:
                function3(item.getItemId());
                break;
            case 3:
                function4(item.getItemId());
                break;
            default:
                return false;
        }
        return true;
    }

    public void function1(int iUur) {
        LesUur item = this.roosterData[iUur];
        if (item.les) {
            Intent i = new Intent(this, EditHuiswerk.class);
            Bundle b = new Bundle();
            if (item.hwid != 0) {
                b.putInt("_id", item.hwid);
                i.putExtras(b);
            } else {
                b.putInt("_vakid", item.vakid);
                b.putInt("_datum", item.datum);
                i.putExtras(b);
            }
            startActivity(i);
        }
    }

    public void function2(int iUur) {
        LesUur item = this.roosterData[iUur];
        if (item.tmp) {
            Intent i = new Intent(this, EditRoosterWijzigingen.class);
            Bundle b = new Bundle();
            b.putInt("_id", item.temproosterid);
            i.putExtras(b);
            startActivity(i);
            return;
        }
        Intent i2 = new Intent(this, EditRoosterWijzigingen.class);
        Bundle b2 = new Bundle();
        b2.putInt("_datum", item.datum);
        b2.putInt("_uur", item.uur);
        i2.putExtras(b2);
        startActivity(i2);
    }

    public void function3(int iUur) {
        LesUur item = this.roosterData[iUur];
        Intent CallIntent = new Intent("android.intent.action.DIAL");
        CallIntent.setData(Uri.parse("tel:" + item.telefoon));
        startActivity(CallIntent);
    }

    public void function4(int iUur) {
        LesUur item = this.roosterData[iUur];
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf((String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(item.datum / 10000).intValue() - 1900, Integer.valueOf((item.datum % 10000) / 100).intValue(), Integer.valueOf(item.datum % 100).intValue()))) + "  " + getString(R.string.lesuur) + ": " + item.uur + "  " + item.vaklang + "  " + getString(R.string.lokaal) + ": " + item.lok);
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{item.email});
        startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

    private void FillRooster() {
        this.roosterDatum.setText((String) DateFormat.format("  EEEE dd MMMM", new Date(this.calRoosterDag.get(1) - 1900, this.calRoosterDag.get(2), this.calRoosterDag.get(5))));
        Integer iDatum = Integer.valueOf((this.calRoosterDag.get(1) * 10000) + (this.calRoosterDag.get(2) * 100) + this.calRoosterDag.get(5));
        boolean bVakantie = VakantieDagen.IsVakantieDag(this.calRoosterDag);
        for (int iuur = 0; iuur < 20; iuur++) {
            this.roosterData[iuur] = new LesUur(false, false, bVakantie, iuur + 1, 0, 0, "", "", "", 0, iDatum.intValue(), 0, true, false, 0, -12303292, "", "", "", false);
        }
        if (!bVakantie) {
            HuiswerkDatabase.RoosterCursorNu nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(getApplicationContext(), this.calRoosterDag));
            while (!nuCursor.isAfterLast()) {
                int iUur = nuCursor.getColUur();
                this.roosterData[iUur - 1].les = true;
                this.roosterData[iUur - 1].kleur = -1;
                this.roosterData[iUur - 1].uur = iUur;
                this.roosterData[iUur - 1].tmp = false;
                this.roosterData[iUur - 1].vakkort = nuCursor.getColVakKort();
                this.roosterData[iUur - 1].vaklang = nuCursor.getColVakNaam();
                this.roosterData[iUur - 1].vakid = (int) nuCursor.getColVakId();
                this.roosterData[iUur - 1].lok = nuCursor.getColLokaal();
                this.roosterData[iUur - 1].begin = nuCursor.getColBegin();
                this.roosterData[iUur - 1].einde = nuCursor.getColEinde();
                this.hwvCursor = this.db.getHuiswerkVak((long) iDatum.intValue(), nuCursor.getColVakId());
                if (this.hwvCursor.getCount() > 0) {
                    this.roosterData[iUur - 1].hwid = (int) this.hwvCursor.getColHuiswerkId();
                    this.roosterData[iUur - 1].klaar = this.hwvCursor.getColKlaar() == 1;
                    this.roosterData[iUur - 1].toets = this.hwvCursor.getColToets() == 1;
                }
                this.hwvCursor.close();
                nuCursor.moveToNext();
            }
            nuCursor.close();
            HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = this.db.getRoosterWijzigingDatum((long) iDatum.intValue());
            while (!rwCursor.isAfterLast()) {
                int iUur2 = rwCursor.getColUur();
                this.roosterData[iUur2 - 1].les = true;
                this.roosterData[iUur2 - 1].kleur = -3355444;
                this.roosterData[iUur2 - 1].uur = iUur2;
                this.roosterData[iUur2 - 1].tmp = true;
                this.roosterData[iUur2 - 1].temproosterid = (int) rwCursor.getColRoosterId();
                this.roosterData[iUur2 - 1].vakkort = rwCursor.getColVakKort();
                this.roosterData[iUur2 - 1].vaklang = rwCursor.getColVakNaam();
                this.roosterData[iUur2 - 1].vakid = (int) rwCursor.getColVakId();
                this.roosterData[iUur2 - 1].lok = rwCursor.getColLokaal();
                this.roosterData[iUur2 - 1].begin = rwCursor.getColBegin();
                this.roosterData[iUur2 - 1].einde = rwCursor.getColEinde();
                this.hwvCursor = this.db.getHuiswerkVak((long) iDatum.intValue(), rwCursor.getColVakId());
                if (this.hwvCursor.getCount() > 0) {
                    this.roosterData[iUur2 - 1].hwid = (int) this.hwvCursor.getColHuiswerkId();
                    this.roosterData[iUur2 - 1].klaar = this.hwvCursor.getColKlaar() == 1;
                    this.roosterData[iUur2 - 1].toets = this.hwvCursor.getColToets() == 1;
                } else {
                    this.roosterData[iUur2 - 1].hwid = 0;
                    this.roosterData[iUur2 - 1].klaar = true;
                    this.roosterData[iUur2 - 1].toets = false;
                }
                this.hwvCursor.close();
                rwCursor.moveToNext();
            }
            rwCursor.close();
            for (int iuur2 = 0; iuur2 < 20; iuur2++) {
                if (this.roosterData[iuur2].vakid > 0) {
                    HuiswerkDatabase.VakInfoCursor viC = this.db.getVakInfo((long) this.roosterData[iuur2].vakid);
                    if (viC.getCount() > 0) {
                        this.roosterData[iuur2].kleur = viC.getColKleur().intValue();
                        this.roosterData[iuur2].leraar = viC.getColLeraar();
                        this.roosterData[iuur2].telefoon = viC.getColTelefoon();
                        this.roosterData[iuur2].email = viC.getColEmail();
                    }
                    viC.close();
                }
                if (this.roosterData[iuur2].hwid > 0) {
                    HuiswerkDatabase.HuiswerkFotoCursor chwf = this.db.getHuiswerkFoto((long) this.roosterData[iuur2].hwid);
                    if (chwf.getCount() > 0) {
                        this.roosterData[iuur2].foto = true;
                    }
                    chwf.close();
                }
            }
        }
        ZoekKortsteLes();
    }

    private void ZoekKortsteLes() {
        int iDuur;
        iKortsteLes = 9999;
        for (int iUur = 0; iUur < this.iPrefNumHours; iUur++) {
            if (this.roosterData[iUur].les && this.roosterData[iUur].begin != 0 && this.roosterData[iUur].einde != 0 && this.roosterData[iUur].begin < this.roosterData[iUur].einde && (iDuur = (((this.roosterData[iUur].einde / 100) * 60) + (this.roosterData[iUur].einde % 100)) - (((this.roosterData[iUur].begin / 100) * 60) + (this.roosterData[iUur].begin % 100))) < iKortsteLes) {
                iKortsteLes = iDuur;
            }
        }
    }

    private void ToonRooster() {
        LinearLayout llRow = (LinearLayout) findViewById(R.id.lldag);
        llRow.removeAllViews();
        int iLastLessonEndTime = 0;
        for (int iUur = 0; iUur < this.iPrefNumHours; iUur++) {
            if (this.roosterData[iUur].les) {
                if (iLastLessonEndTime > 0 && this.roosterData[iUur].begin > iLastLessonEndTime) {
                    llRow.addView(MaakLegeView(iLastLessonEndTime, this.roosterData[iUur].begin));
                }
                llRow.addView(MaakUurView(iUur));
                if (this.roosterData[iUur].einde > iLastLessonEndTime) {
                    iLastLessonEndTime = this.roosterData[iUur].einde;
                } else {
                    iLastLessonEndTime += iKortsteLes;
                }
            }
        }
    }

    private View MaakLegeView(int iBeginTime, int iEndTime) {
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.roosterdagrowleeg, (ViewGroup) null);
        RelativeLayout rlcel = (RelativeLayout) v.findViewById(R.id.rlrow);
        if (rlcel != null) {
            float scale = getResources().getDisplayMetrics().density;
            rlcel.setMinimumHeight((int) (((((float) ((((iEndTime / 100) * 60) + (iEndTime % 100)) - (((iBeginTime / 100) * 60) + (iBeginTime % 100)))) / ((float) iKortsteLes)) * fMinimumCelHeight * scale) + 0.5f));
        }
        return v;
    }

    private View MaakUurView(int iUur) {
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.roosterdagrow, (ViewGroup) null);
        LesUur item = this.roosterData[iUur];
        RelativeLayout row = (RelativeLayout) v.findViewById(R.id.rlrow);
        TextView lesuur = (TextView) v.findViewById(R.id.lesuur);
        TextView lesuurstart = (TextView) v.findViewById(R.id.lesuurstart);
        TextView lesuurstop = (TextView) v.findViewById(R.id.lesuurstop);
        TextView trt = (TextView) v.findViewById(R.id.toprighttext);
        TextView brt = (TextView) v.findViewById(R.id.bottomrighttext);
        ImageButton fit = (ImageButton) v.findViewById(R.id.fotoindicator);
        ImageView hit = (ImageView) v.findViewById(R.id.huiswerkindicator);
        ImageView tit = (ImageView) v.findViewById(R.id.toetsindicator);
        if (row != null) {
            GradientDrawable shape = (GradientDrawable) getResources().getDrawable(R.drawable.rounded);
            shape.setColor(item.kleur);
            row.setBackgroundDrawable(shape);
            float fDuur = (float) ((((item.einde / 100) * 60) + (item.einde % 100)) - (((item.begin / 100) * 60) + (item.begin % 100)));
            if (fDuur < ((float) iKortsteLes)) {
                fDuur = (float) iKortsteLes;
            }
            row.setLayoutParams(new RelativeLayout.LayoutParams(-1, (int) (((fDuur / ((float) iKortsteLes)) * fMinimumCelHeight * getResources().getDisplayMetrics().density) + 0.5f)));
            row.setOnClickListener(this.lessonOnClick);
            row.setId(iUur);
            registerForContextMenu(row);
            row.setOnTouchListener(this);
        }
        if (lesuur != null) {
            if (item.tmp) {
                lesuur.setText(String.format("*%d", Integer.valueOf(item.uur)));
            } else {
                lesuur.setText(String.format("%d", Integer.valueOf(item.uur)));
            }
        }
        if (lesuurstart != null) {
            lesuurstart.setText(HwUtl.Time2String(item.begin));
        }
        if (lesuurstop != null) {
            lesuurstop.setText(HwUtl.Time2String(item.einde));
        }
        if (trt != null) {
            trt.setText(item.vaklang);
        }
        if (brt != null) {
            if (item.lok.length() > 0) {
                brt.setText(String.valueOf(getString(R.string.lokaal)) + ": " + item.lok);
            } else {
                brt.setText("");
            }
        }
        if (hit != null) {
            if (!item.klaar) {
                hit.setImageResource(R.drawable.roosterhuiswerk);
            } else {
                hit.setImageResource(0);
            }
        }
        if (tit != null) {
            if (item.toets) {
                tit.setImageResource(R.drawable.roostertoets);
            } else {
                tit.setImageResource(0);
            }
        }
        if (fit != null) {
            fit.setFocusable(false);
            if (item.foto) {
                fit.setVisibility(0);
                fit.setImageResource(R.drawable.roostercamera);
                fit.setOnClickListener(new OnFotoClickListener(item.hwid));
            } else {
                fit.setVisibility(8);
            }
        }
        return v;
    }

    public boolean onTouch(View arg0, MotionEvent arg1) {
        switch (arg1.getAction()) {
            case 0:
                this.downXValue = arg1.getX();
                break;
            case 1:
                float currentX = arg1.getX();
                if (this.downXValue < currentX && currentX - this.downXValue > 100.0f) {
                    PreviousDay();
                    return true;
                } else if (this.downXValue > currentX && this.downXValue - currentX > 100.0f) {
                    NextDay();
                    return true;
                }
                break;
        }
        return false;
    }

    public class OnFotoClickListener implements View.OnClickListener {
        private int mHwid = 0;

        public OnFotoClickListener(int hwid) {
            this.mHwid = hwid;
        }

        public void onClick(View v) {
            Intent i = new Intent(RoosterDagNew.this, HuiswerkFoto.class);
            Bundle b = new Bundle();
            b.putInt("_id", this.mHwid);
            i.putExtras(b);
            RoosterDagNew.this.startActivity(i);
        }
    }

    public void onResume() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int iBackgroundColor = prefs.getInt("HW_PREF_ACHTERGROND", 0);
        this.iPrefNumHours = prefs.getInt("HW_PREF_NUMHOURS", 10);
        mMain.setBackgroundColor(iBackgroundColor);
        ((HuisWerkMain) MainContext).setHuisWerkCount(this.db.getHuiswerkCountNietAf());
        ((HuisWerkMain) MainContext).setToetsCount(this.db.getToetsCountNietAf());
        this.calRoosterDag = (Calendar) ((HuisWerkMain) MainContext).getCurViewDate().clone();
        FillRooster();
        ToonRooster();
        super.onResume();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ((HuisWerkMain) getParent()).ShowMainMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    public void FirstDay() {
        this.calRoosterDag = Calendar.getInstance();
        int iCurTime = (this.calRoosterDag.get(11) * 100) + this.calRoosterDag.get(12);
        boolean bFound = false;
        int day = 0;
        while (true) {
            if (day >= 14) {
                break;
            }
            if (!VakantieDagen.IsVakantieDag(this.calRoosterDag)) {
                HuiswerkDatabase.RoosterCursorNu nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(getApplicationContext(), this.calRoosterDag));
                if (nuCursor.getCount() > 0) {
                    if (day != 0) {
                        nuCursor.close();
                        bFound = true;
                        break;
                    }
                    while (!nuCursor.isLast()) {
                        nuCursor.moveToNext();
                    }
                    if (iCurTime < nuCursor.getColEinde()) {
                        nuCursor.close();
                        bFound = true;
                        break;
                    }
                }
                nuCursor.close();
            }
            this.calRoosterDag.add(5, 1);
            day++;
        }
        if (!bFound) {
            this.calRoosterDag = Calendar.getInstance();
        }
        ((HuisWerkMain) MainContext).setCurViewDate(this.calRoosterDag);
        FillRooster();
        ToonRooster();
        mDagData.startAnimation(AnimationUtils.loadAnimation(this, R.anim.top_to_bottom));
    }

    public void NextDay() {
        this.calRoosterDag.add(5, 1);
        Calendar calRoosterDagTemp = (Calendar) this.calRoosterDag.clone();
        boolean bFound = false;
        int day = 0;
        while (true) {
            if (day >= 14) {
                break;
            }
            if (!VakantieDagen.IsVakantieDag(this.calRoosterDag)) {
                HuiswerkDatabase.RoosterCursorNu nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(getApplicationContext(), this.calRoosterDag));
                if (nuCursor.getCount() > 0) {
                    nuCursor.close();
                    bFound = true;
                    break;
                }
                nuCursor.close();
                HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = this.db.getRoosterWijzigingDatum((long) Integer.valueOf((this.calRoosterDag.get(1) * 10000) + (this.calRoosterDag.get(2) * 100) + this.calRoosterDag.get(5)).intValue());
                if (rwCursor.getCount() > 0) {
                    rwCursor.close();
                    bFound = true;
                    break;
                }
                rwCursor.close();
            }
            this.calRoosterDag.add(5, 1);
            day++;
        }
        if (!bFound) {
            this.calRoosterDag = (Calendar) calRoosterDagTemp.clone();
        }
        ((HuisWerkMain) MainContext).setCurViewDate(this.calRoosterDag);
        FillRooster();
        ToonRooster();
        mDagData.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_to_left));
    }

    public void PreviousDay() {
        this.calRoosterDag.add(5, -1);
        Calendar calRoosterDagTemp = (Calendar) this.calRoosterDag.clone();
        boolean bFound = false;
        int day = 0;
        while (true) {
            if (day >= 14) {
                break;
            }
            if (!VakantieDagen.IsVakantieDag(this.calRoosterDag)) {
                HuiswerkDatabase.RoosterCursorNu nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(getApplicationContext(), this.calRoosterDag));
                if (nuCursor.getCount() > 0) {
                    nuCursor.close();
                    bFound = true;
                    break;
                }
                nuCursor.close();
                HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = this.db.getRoosterWijzigingDatum((long) Integer.valueOf((this.calRoosterDag.get(1) * 10000) + (this.calRoosterDag.get(2) * 100) + this.calRoosterDag.get(5)).intValue());
                if (rwCursor.getCount() > 0) {
                    rwCursor.close();
                    bFound = true;
                    break;
                }
                rwCursor.close();
            }
            this.calRoosterDag.add(5, -1);
            day++;
        }
        if (!bFound) {
            this.calRoosterDag = (Calendar) calRoosterDagTemp.clone();
        }
        ((HuisWerkMain) MainContext).setCurViewDate(this.calRoosterDag);
        FillRooster();
        ToonRooster();
        mDagData.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
    }
}
