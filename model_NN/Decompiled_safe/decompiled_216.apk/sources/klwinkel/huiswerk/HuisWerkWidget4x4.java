package klwinkel.huiswerk;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkWidget4x4 extends AppWidgetProvider {
    public static final String REFRESH = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4";
    public static boolean bAutoUpdate;
    public static boolean bTransparant;
    public static Calendar calRoosterDag;
    public static Integer curTime = 0;
    public static HuiswerkDatabase db;
    public static HuiswerkDatabase.HuiswerkVakCursor hwvCursor = null;
    public static Integer iDatum = 0;
    public static Integer iLesson = 0;
    public static LessonData[] lessonData;
    public static HuiswerkDatabase.RoosterCursorNu nuCursor = null;
    public static HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = null;
    public AlarmManager am;

    public class LessonData {
        public boolean klaar;
        public int kleur;
        public boolean les;
        public String lok;
        public int start;
        public int stop;
        public boolean tmp;
        public boolean toets;
        public int uur;
        public String vak;
        public long vakid;

        LessonData(boolean les2, boolean tmp2, int uur2, int start2, int stop2, String vak2, String lok2, boolean klaar2, boolean toets2, int kleur2, long vakid2) {
            this.les = les2;
            this.tmp = tmp2;
            this.uur = uur2;
            this.start = start2;
            this.stop = stop2;
            this.vak = vak2;
            this.lok = lok2;
            this.klaar = klaar2;
            this.toets = toets2;
            this.kleur = kleur2;
            this.vakid = vakid2;
        }
    }

    public void onReceive(Context ctxt, Intent intent) {
        if (REFRESH.equals(intent.getAction())) {
            DoUpdate(ctxt);
        } else {
            super.onReceive(ctxt, intent);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        DoUpdate(context);
    }

    public void DoUpdate(Context ctxt) {
        RemoteViews updateViews;
        db = new HuiswerkDatabase(ctxt, false);
        if (db.getRelNotesVersionFirstTime() < 33) {
            updateViews = new RemoteViews(ctxt.getPackageName(), (int) R.layout.widget4x4);
        } else {
            db.Init(ctxt);
            updateViews = buildUpdate(ctxt);
        }
        db.close();
        AppWidgetManager.getInstance(ctxt).updateAppWidget(new ComponentName(ctxt, HuisWerkWidget4x4.class), updateViews);
    }

    private RemoteViews buildUpdate(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget4x4);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        bAutoUpdate = prefs.getBoolean("HW_PREF_AUTOUPDATE", true);
        bTransparant = prefs.getBoolean("HW_PREF_TRANSPARANT", false);
        if (!VakantieDagen.IsInitialised()) {
            VakantieDagen.LaadVakantieDagen(db);
        }
        lessonData = new LessonData[21];
        iLesson = 20;
        calRoosterDag = Calendar.getInstance();
        curTime = Integer.valueOf((calRoosterDag.get(11) * 100) + calRoosterDag.get(12));
        boolean bFoundLesson = false;
        int day = 0;
        while (true) {
            if (day >= 200) {
                break;
            }
            if (!VakantieDagen.IsVakantieDag(calRoosterDag)) {
                iDatum = Integer.valueOf((calRoosterDag.get(1) * 10000) + (calRoosterDag.get(2) * 100) + calRoosterDag.get(5));
                nuCursor = db.getRoosterNu(db.GetRoosterDagLong(context.getApplicationContext(), calRoosterDag));
                rwCursor = db.getRoosterWijzigingDatum((long) iDatum.intValue());
                if (rwCursor.getCount() > 0 || nuCursor.getCount() > 0) {
                    if (day != 0) {
                        bFoundLesson = true;
                        iLesson = 1;
                        nuCursor.close();
                        rwCursor.close();
                        break;
                    }
                    if (nuCursor.getCount() > 0) {
                        while (!bFoundLesson) {
                            if (curTime.intValue() > nuCursor.getColBegin()) {
                                if (nuCursor.isLast()) {
                                    break;
                                }
                                nuCursor.moveToNext();
                            } else {
                                if (nuCursor.getColUur() < iLesson.intValue()) {
                                    iLesson = Integer.valueOf(nuCursor.getColUur());
                                }
                                bFoundLesson = true;
                            }
                        }
                    }
                    if (rwCursor.getCount() > 0) {
                        while (!bFoundLesson) {
                            if (curTime.intValue() > rwCursor.getColBegin()) {
                                if (rwCursor.isLast()) {
                                    break;
                                }
                                rwCursor.moveToNext();
                            } else {
                                if (rwCursor.getColUur() < iLesson.intValue()) {
                                    iLesson = Integer.valueOf(rwCursor.getColUur());
                                }
                                bFoundLesson = true;
                            }
                        }
                        rwCursor.close();
                    }
                    if (bFoundLesson) {
                        nuCursor.close();
                        rwCursor.close();
                        break;
                    }
                }
                nuCursor.close();
                rwCursor.close();
            }
            calRoosterDag.add(5, 1);
            day++;
        }
        if (!bFoundLesson) {
            iLesson = 1;
            calRoosterDag = Calendar.getInstance();
            iDatum = Integer.valueOf((calRoosterDag.get(1) * 10000) + (calRoosterDag.get(2) * 100) + calRoosterDag.get(5));
        }
        for (int uur = 0; uur < 21; uur++) {
            lessonData[uur] = new LessonData(false, false, 0, 0, 0, "", "", true, false, -1, 0);
        }
        nuCursor = db.getRoosterNu(db.GetRoosterDagLong(context.getApplicationContext(), calRoosterDag));
        while (!nuCursor.isAfterLast()) {
            int iUur = nuCursor.getColUur();
            lessonData[iUur].les = true;
            lessonData[iUur].uur = iUur;
            lessonData[iUur].start = nuCursor.getColBegin();
            lessonData[iUur].stop = nuCursor.getColEinde();
            lessonData[iUur].vak = nuCursor.getColVakNaam();
            lessonData[iUur].vakid = nuCursor.getColVakId();
            lessonData[iUur].lok = nuCursor.getColLokaal();
            hwvCursor = db.getHuiswerkVak((long) iDatum.intValue(), nuCursor.getColVakId());
            if (hwvCursor.getCount() > 0) {
                lessonData[iUur].klaar = hwvCursor.getColKlaar() == 1;
                lessonData[iUur].toets = hwvCursor.getColToets() == 1;
            } else {
                lessonData[iUur].klaar = true;
                lessonData[iUur].toets = false;
            }
            hwvCursor.close();
            nuCursor.moveToNext();
        }
        nuCursor.close();
        rwCursor = db.getRoosterWijzigingDatum((long) iDatum.intValue());
        while (!rwCursor.isAfterLast()) {
            int iUur2 = rwCursor.getColUur();
            lessonData[iUur2].les = true;
            lessonData[iUur2].tmp = true;
            lessonData[iUur2].uur = iUur2;
            lessonData[iUur2].start = rwCursor.getColBegin();
            lessonData[iUur2].stop = rwCursor.getColEinde();
            lessonData[iUur2].vak = rwCursor.getColVakNaam();
            lessonData[iUur2].vakid = rwCursor.getColVakId();
            lessonData[iUur2].lok = rwCursor.getColLokaal();
            hwvCursor = db.getHuiswerkVak((long) iDatum.intValue(), rwCursor.getColVakId());
            if (hwvCursor.getCount() > 0) {
                lessonData[iUur2].klaar = hwvCursor.getColKlaar() == 1;
                lessonData[iUur2].toets = hwvCursor.getColToets() == 1;
            } else {
                lessonData[iUur2].klaar = true;
                lessonData[iUur2].toets = false;
            }
            hwvCursor.close();
            rwCursor.moveToNext();
        }
        rwCursor.close();
        for (int iuur = 0; iuur < 21; iuur++) {
            if (lessonData[iuur].vakid > 0 && db.getRelNotesVersion() >= 16) {
                HuiswerkDatabase.VakInfoCursor viC = db.getVakInfo(lessonData[iuur].vakid);
                if (viC.getCount() > 0) {
                    lessonData[iuur].kleur = viC.getColKleur().intValue();
                }
                viC.close();
            }
        }
        remoteViews.setImageViewResource(R.id.rowiconWidget, R.drawable.icon);
        remoteViews.setTextViewText(R.id.lblWidgetKop, (String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(iDatum.intValue() / 10000).intValue() - 1900, Integer.valueOf((iDatum.intValue() % 10000) / 100).intValue(), Integer.valueOf(iDatum.intValue() % 100).intValue())));
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur1, R.id.lblWidgetTijd1, R.id.lblWidgetVak1, R.id.imgWidgetHuiswerk1, R.id.imgWidgetToets1, R.id.lblWidgetLokaal1);
        if (bAutoUpdate && iLesson.intValue() <= 20) {
            this.am = (AlarmManager) context.getSystemService("alarm");
            Intent intent = new Intent(context, HuisWerkWidget4x4.class);
            intent.setAction(REFRESH);
            this.am.set(1, calRoosterDag.getTimeInMillis() + 300000, PendingIntent.getBroadcast(context, 0, intent, 0));
        }
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur2, R.id.lblWidgetTijd2, R.id.lblWidgetVak2, R.id.imgWidgetHuiswerk2, R.id.imgWidgetToets2, R.id.lblWidgetLokaal2);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur3, R.id.lblWidgetTijd3, R.id.lblWidgetVak3, R.id.imgWidgetHuiswerk3, R.id.imgWidgetToets3, R.id.lblWidgetLokaal3);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur4, R.id.lblWidgetTijd4, R.id.lblWidgetVak4, R.id.imgWidgetHuiswerk4, R.id.imgWidgetToets4, R.id.lblWidgetLokaal4);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur5, R.id.lblWidgetTijd5, R.id.lblWidgetVak5, R.id.imgWidgetHuiswerk5, R.id.imgWidgetToets5, R.id.lblWidgetLokaal5);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur6, R.id.lblWidgetTijd6, R.id.lblWidgetVak6, R.id.imgWidgetHuiswerk6, R.id.imgWidgetToets6, R.id.lblWidgetLokaal6);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur7, R.id.lblWidgetTijd7, R.id.lblWidgetVak7, R.id.imgWidgetHuiswerk7, R.id.imgWidgetToets7, R.id.lblWidgetLokaal7);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur8, R.id.lblWidgetTijd8, R.id.lblWidgetVak8, R.id.imgWidgetHuiswerk8, R.id.imgWidgetToets8, R.id.lblWidgetLokaal8);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur9, R.id.lblWidgetTijd9, R.id.lblWidgetVak9, R.id.imgWidgetHuiswerk9, R.id.imgWidgetToets9, R.id.lblWidgetLokaal9);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur10, R.id.lblWidgetTijd10, R.id.lblWidgetVak10, R.id.imgWidgetHuiswerk10, R.id.imgWidgetToets10, R.id.lblWidgetLokaal10);
        remoteViews.setOnClickPendingIntent(R.id.llText, PendingIntent.getActivity(context, 0, new Intent(context, HuisWerkMain.class), 0));
        return remoteViews;
    }

    private void buildRoosterLine(Context context, RemoteViews views, int id_uur, int id_tijd, int id_vak, int id_huiswerk, int id_toets, int id_lokaal) {
        String strUur;
        views.setTextViewText(id_uur, new StringBuilder().append(""));
        views.setTextViewText(id_tijd, new StringBuilder().append(""));
        views.setTextViewText(id_vak, new StringBuilder().append(""));
        views.setTextViewText(id_lokaal, new StringBuilder().append(""));
        views.setImageViewResource(id_huiswerk, 0);
        views.setImageViewResource(id_toets, 0);
        if (bTransparant) {
            views.setImageViewResource(R.id.background, R.drawable.frame4x4trans);
        } else {
            views.setImageViewResource(R.id.background, R.drawable.frame4x4);
        }
        while (iLesson.intValue() <= 20 && !lessonData[iLesson.intValue()].les) {
            iLesson = Integer.valueOf(iLesson.intValue() + 1);
        }
        if (iLesson.intValue() <= 20 && lessonData[iLesson.intValue()].les) {
            int iStartTijd = lessonData[iLesson.intValue()].start;
            int iStopTijd = lessonData[iLesson.intValue()].stop;
            calRoosterDag.set(11, iStartTijd / 100);
            calRoosterDag.set(12, iStartTijd % 100);
            calRoosterDag.set(13, 0);
            if (lessonData[iLesson.intValue()].tmp) {
                strUur = String.format("*%d", Integer.valueOf(lessonData[iLesson.intValue()].uur));
            } else {
                strUur = String.format("%d", Integer.valueOf(lessonData[iLesson.intValue()].uur));
            }
            views.setTextViewText(id_uur, new StringBuilder().append(strUur));
            views.setTextColor(id_uur, -1);
            views.setTextViewText(id_tijd, new StringBuilder().append(String.valueOf(HwUtl.Time2StringWidget(context, iStartTijd)) + "~" + HwUtl.Time2StringWidget(context, iStopTijd)));
            views.setTextColor(id_tijd, -1);
            views.setTextViewText(id_vak, new StringBuilder().append(lessonData[iLesson.intValue()].vak));
            views.setTextColor(id_vak, lessonData[iLesson.intValue()].kleur);
            views.setTextViewText(id_lokaal, new StringBuilder().append(lessonData[iLesson.intValue()].lok));
            views.setTextColor(id_lokaal, -1);
            if (!lessonData[iLesson.intValue()].klaar) {
                views.setImageViewResource(id_huiswerk, R.drawable.huiswerk);
            }
            if (lessonData[iLesson.intValue()].toets) {
                views.setImageViewResource(id_toets, R.drawable.toets);
            }
        }
        iLesson = Integer.valueOf(iLesson.intValue() + 1);
    }
}
