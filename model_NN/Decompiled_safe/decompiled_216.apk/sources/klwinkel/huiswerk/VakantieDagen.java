package klwinkel.huiswerk;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import klwinkel.huiswerk.HuiswerkDatabase;

public class VakantieDagen {
    private static boolean bInit = false;
    private static List<VakantiePeriode> vakantieList = new ArrayList();

    public static class VakantiePeriode {
        public int begin;
        public int einde;

        VakantiePeriode(int begin2, int einde2) {
            this.begin = begin2;
            this.einde = einde2;
        }
    }

    public static boolean IsVakantieDag(Calendar cal) {
        int iDatum = (cal.get(1) * 10000) + (cal.get(2) * 100) + cal.get(5);
        for (VakantiePeriode periode : vakantieList) {
            if (iDatum >= periode.begin) {
                if (periode.einde > 0 && iDatum <= periode.einde) {
                    return true;
                }
                if (iDatum == periode.begin) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void LaadVakantieDagen(HuiswerkDatabase db) {
        vakantieList.clear();
        HuiswerkDatabase.VakantieCursor vakantieCursor = db.getVakanties();
        for (int i = 0; i < vakantieCursor.getCount(); i++) {
            vakantieCursor.moveToPosition(i);
            vakantieList.add(new VakantiePeriode(vakantieCursor.getColDatumBegin(), vakantieCursor.getColDatumEinde()));
        }
        vakantieCursor.close();
        bInit = true;
    }

    public static int GemisteDagenRoterend(Calendar cal, int iRotateFirstDate) {
        int iCount = 0;
        Calendar calRotate = Calendar.getInstance();
        if (iRotateFirstDate > 0) {
            calRotate.set(1, iRotateFirstDate / 10000);
            calRotate.set(2, (iRotateFirstDate % 10000) / 100);
            calRotate.set(5, iRotateFirstDate % 100);
        }
        while (calRotate.before(cal)) {
            long iWeekDay = (long) calRotate.get(7);
            if (!(iWeekDay == 7 || iWeekDay == 1 || !IsVakantieDag(calRotate))) {
                iCount++;
            }
            calRotate.add(5, 1);
        }
        return iCount;
    }

    public static boolean IsInitialised() {
        return bInit;
    }
}
