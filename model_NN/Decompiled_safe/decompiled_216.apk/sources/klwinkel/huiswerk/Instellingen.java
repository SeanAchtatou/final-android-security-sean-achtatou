package klwinkel.huiswerk;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import klwinkel.huiswerk.AmbilWarnaDialog;

public class Instellingen extends Activity {
    static final String HW_PREF_2WEEK = "HW_PREF_2WEEK";
    static final boolean HW_PREF_2WEEK_NO = false;
    static final boolean HW_PREF_2WEEK_YES = true;
    static final String HW_PREF_ACHTERGROND = "HW_PREF_ACHTERGROND";
    static final int HW_PREF_ACHTERGROND_DEFAULT = 0;
    static final String HW_PREF_APPMODE = "HW_PREF_APPMODE";
    static final int HW_PREF_APPMODE_DOCENT = 2;
    static final int HW_PREF_APPMODE_GEEN = 0;
    static final int HW_PREF_APPMODE_STUDENT = 1;
    static final String HW_PREF_AUTOUPDATE = "HW_PREF_AUTOUPDATE";
    static final boolean HW_PREF_AUTOUPDATE_NO = false;
    static final boolean HW_PREF_AUTOUPDATE_YES = true;
    static final String HW_PREF_FSDOW = "HW_PREF_FSDOW";
    static final int HW_PREF_FSDOW_AUTOMATIC = 0;
    static final String HW_PREF_NUMDAYS = "HW_PREF_NUMDAYS";
    static final String HW_PREF_NUMDAYSROTATING = "HW_PREF_NUMDAYSROTATING";
    static final int HW_PREF_NUMDAYSROTATING_DEFAULT = 8;
    static final int HW_PREF_NUMDAYS_DEFAULT = 7;
    static final String HW_PREF_NUMHOURS = "HW_PREF_NUMHOURS";
    static final int HW_PREF_NUMHOURS_DEFAULT = 10;
    static final String HW_PREF_ROTATEFIRSTDATE = "HW_PREF_ROTATEFIRSTDATE";
    static final int HW_PREF_ROTATEFIRSTDATE_DEFAULT = 0;
    static final String HW_PREF_STARTSCHERM = "HW_PREF_STARTSCHERM";
    static final int HW_PREF_STARTSCHERM_DAG = 3;
    static final int HW_PREF_STARTSCHERM_HUISWERK = 1;
    static final int HW_PREF_STARTSCHERM_MENU = 0;
    static final int HW_PREF_STARTSCHERM_TOETS = 2;
    static final int HW_PREF_STARTSCHERM_WEEK = 4;
    static final String HW_PREF_TIMETABLETYPE = "HW_PREF_TIMETABLETYPE";
    static final int HW_PREF_TIMETABLETYPE_1WEEK = 0;
    static final int HW_PREF_TIMETABLETYPE_2WEEK = 1;
    static final int HW_PREF_TIMETABLETYPE_DEFAULT = 0;
    static final int HW_PREF_TIMETABLETYPE_ROTATING = 2;
    static final String HW_PREF_TRANSPARANT = "HW_PREF_TRANSPARANT";
    static final boolean HW_PREF_TRANSPARANT_NO = false;
    static final boolean HW_PREF_TRANSPARANT_YES = true;
    static final String HW_PREF_VOLGORDE = "HW_PREF_VOLGORDE";
    static final int HW_PREF_VOLGORDE_12 = 0;
    static final int HW_PREF_VOLGORDE_21 = 1;
    private static Button btnAchtergrond;
    /* access modifiers changed from: private */
    public static Button btnRotateFirstDate;
    /* access modifiers changed from: private */
    public static Calendar calRotateFirst;
    private static CheckBox chkAutoUpdate;
    private static CheckBox chkTransparant;
    /* access modifiers changed from: private */
    public static int iBackgroundColor = 0;
    /* access modifiers changed from: private */
    public static TextView lblNumDaysRotating;
    /* access modifiers changed from: private */
    public static TextView lblRotateFirst;
    /* access modifiers changed from: private */
    public static TextView lblVolgorde;
    private static Context myContext;
    private static RadioButton radioDocent;
    private static RadioButton radioStudent;
    /* access modifiers changed from: private */
    public static Spinner spnDag;
    /* access modifiers changed from: private */
    public static Spinner spnNumDays;
    /* access modifiers changed from: private */
    public static Spinner spnNumDaysRotating;
    private static Spinner spnNumHours;
    private static Spinner spnStartScherm;
    /* access modifiers changed from: private */
    public static Spinner spnTimetableType;
    /* access modifiers changed from: private */
    public static Spinner spnVolgorde;
    /* access modifiers changed from: private */
    public static ScrollView svInstellingen;
    private final View.OnClickListener achtergrondOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new AmbilWarnaDialog(Instellingen.this, Instellingen.iBackgroundColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    Instellingen.iBackgroundColor = color;
                    Instellingen.svInstellingen.setBackgroundColor(Instellingen.iBackgroundColor);
                }

                public void onCancel(AmbilWarnaDialog dialog) {
                }
            }).show();
        }
    };
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Instellingen.calRotateFirst.set(1, year);
            Instellingen.calRotateFirst.set(2, monthOfYear);
            Instellingen.calRotateFirst.set(5, dayOfMonth);
            Instellingen.btnRotateFirstDate.setText((String) DateFormat.format("dd MMMM yyyy", new Date(year - 1900, monthOfYear, dayOfMonth)));
        }
    };
    SharedPreferences prefs;
    private final View.OnClickListener rotateFirstOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new DatePickerDialog(Instellingen.this, Instellingen.this.mDateSetListener, Instellingen.calRotateFirst.get(1), Instellingen.calRotateFirst.get(2), Instellingen.calRotateFirst.get(5)).show();
        }
    };

    private class ListItem {
        public long id;
        public String text;

        ListItem(long id2, String text2) {
            this.id = id2;
            this.text = text2;
        }

        public String toString() {
            return this.text;
        }
    }

    public class spnTTTypeOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public spnTTTypeOnItemSelectedListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            switch (Instellingen.spnTimetableType.getSelectedItemPosition()) {
                case 0:
                    Instellingen.spnVolgorde.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblVolgorde.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblRotateFirst.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.btnRotateFirstDate.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblNumDaysRotating.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.spnNumDaysRotating.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.spnNumDays.setEnabled(true);
                    Instellingen.spnDag.setEnabled(true);
                    return;
                case 1:
                    Instellingen.spnVolgorde.setVisibility(0);
                    Instellingen.lblVolgorde.setVisibility(0);
                    Instellingen.lblRotateFirst.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.btnRotateFirstDate.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblNumDaysRotating.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.spnNumDaysRotating.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.spnNumDays.setEnabled(true);
                    Instellingen.spnDag.setEnabled(true);
                    return;
                case 2:
                    Instellingen.spnVolgorde.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblVolgorde.setVisibility(Instellingen.HW_PREF_NUMDAYSROTATING_DEFAULT);
                    Instellingen.lblRotateFirst.setVisibility(0);
                    Instellingen.btnRotateFirstDate.setVisibility(0);
                    Instellingen.lblNumDaysRotating.setVisibility(0);
                    Instellingen.spnNumDaysRotating.setVisibility(0);
                    Instellingen.spnNumDays.setSelection(Instellingen.HW_PREF_STARTSCHERM_WEEK);
                    Instellingen.spnDag.setSelection(2);
                    Instellingen.spnNumDays.setEnabled(false);
                    Instellingen.spnDag.setEnabled(false);
                    return;
                default:
                    return;
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.instellingen);
        myContext = this;
        calRotateFirst = Calendar.getInstance();
        svInstellingen = (ScrollView) findViewById(R.id.svInstellingen);
        btnAchtergrond = (Button) findViewById(R.id.btnAchtergrond);
        btnAchtergrond.setOnClickListener(this.achtergrondOnClick);
        spnDag = (Spinner) findViewById(R.id.spnDag);
        spnVolgorde = (Spinner) findViewById(R.id.spnVolgorde);
        lblVolgorde = (TextView) findViewById(R.id.lblVolgorde);
        lblRotateFirst = (TextView) findViewById(R.id.lblRotateFirst);
        btnRotateFirstDate = (Button) findViewById(R.id.btnRotateFirstDate);
        chkAutoUpdate = (CheckBox) findViewById(R.id.chkautoupdate);
        chkTransparant = (CheckBox) findViewById(R.id.chktransparant);
        spnNumHours = (Spinner) findViewById(R.id.spnNumHours);
        spnNumDays = (Spinner) findViewById(R.id.spnNumDays);
        radioStudent = (RadioButton) findViewById(R.id.radio_student);
        radioDocent = (RadioButton) findViewById(R.id.radio_docent);
        spnStartScherm = (Spinner) findViewById(R.id.spnstartscherm);
        spnTimetableType = (Spinner) findViewById(R.id.spntimetabletype);
        lblNumDaysRotating = (TextView) findViewById(R.id.lblNumDaysRotating);
        spnNumDaysRotating = (Spinner) findViewById(R.id.spnNumDaysRotating);
        btnRotateFirstDate.setOnClickListener(this.rotateFirstOnClick);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ListItem(0, getString(R.string.automatisch)));
        arrayList.add(new ListItem(1, DateUtils.getDayOfWeekString(1, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(2, DateUtils.getDayOfWeekString(2, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(3, DateUtils.getDayOfWeekString(HW_PREF_STARTSCHERM_DAG, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(4, DateUtils.getDayOfWeekString(HW_PREF_STARTSCHERM_WEEK, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(5, DateUtils.getDayOfWeekString(5, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(6, DateUtils.getDayOfWeekString(6, HW_PREF_NUMHOURS_DEFAULT)));
        arrayList.add(new ListItem(7, DateUtils.getDayOfWeekString(HW_PREF_NUMDAYS_DEFAULT, HW_PREF_NUMHOURS_DEFAULT)));
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, arrayList);
        arrayAdapter.setDropDownViewResource(17367049);
        spnDag.setAdapter((SpinnerAdapter) arrayAdapter);
        List<ListItem> volgordeList = new ArrayList<>();
        volgordeList.add(new ListItem(0, "1 - 2 - 1 - 2 - 1 - 2 - 1 - 2"));
        volgordeList.add(new ListItem(1, "2 - 1 - 2 - 1 - 2 - 1 - 2 - 1"));
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, volgordeList);
        arrayAdapter2.setDropDownViewResource(17367049);
        spnVolgorde.setAdapter((SpinnerAdapter) arrayAdapter2);
        List<ListItem> numhoursList = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            numhoursList.add(new ListItem((long) i, String.format("   %d   ", Integer.valueOf(i))));
        }
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this, 17367048, numhoursList);
        arrayAdapter3.setDropDownViewResource(17367049);
        spnNumHours.setAdapter((SpinnerAdapter) arrayAdapter3);
        List<ListItem> numdaysList = new ArrayList<>();
        for (int i2 = 1; i2 <= HW_PREF_NUMDAYS_DEFAULT; i2++) {
            numdaysList.add(new ListItem((long) i2, String.format("   %d   ", Integer.valueOf(i2))));
        }
        ArrayAdapter arrayAdapter4 = new ArrayAdapter(this, 17367048, numdaysList);
        arrayAdapter4.setDropDownViewResource(17367049);
        spnNumDays.setAdapter((SpinnerAdapter) arrayAdapter4);
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new ListItem(3, getString(R.string.mainmenu)));
        arrayList2.add(new ListItem(0, getString(R.string.app_name)));
        arrayList2.add(new ListItem(1, getString(R.string.toets)));
        arrayList2.add(new ListItem(2, getString(R.string.dag)));
        arrayList2.add(new ListItem(3, getString(R.string.week)));
        ArrayAdapter arrayAdapter5 = new ArrayAdapter(this, 17367048, arrayList2);
        arrayAdapter5.setDropDownViewResource(17367049);
        spnStartScherm.setAdapter((SpinnerAdapter) arrayAdapter5);
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new ListItem(0, getString(R.string.timetable1week)));
        arrayList3.add(new ListItem(1, getString(R.string.timetable2week)));
        arrayList3.add(new ListItem(2, getString(R.string.timetablerotating)));
        ArrayAdapter arrayAdapter6 = new ArrayAdapter(this, 17367048, arrayList3);
        arrayAdapter6.setDropDownViewResource(17367049);
        spnTimetableType.setAdapter((SpinnerAdapter) arrayAdapter6);
        spnTimetableType.setOnItemSelectedListener(new spnTTTypeOnItemSelectedListener());
        List<ListItem> numdaysrotatingList = new ArrayList<>();
        for (int i3 = 1; i3 <= HW_PREF_NUMHOURS_DEFAULT; i3++) {
            numdaysrotatingList.add(new ListItem((long) i3, String.format("   %d   ", Integer.valueOf(i3))));
        }
        ArrayAdapter arrayAdapter7 = new ArrayAdapter(this, 17367048, numdaysrotatingList);
        arrayAdapter7.setDropDownViewResource(17367049);
        spnNumDaysRotating.setAdapter((SpinnerAdapter) arrayAdapter7);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        UpdateUIFromPreferences();
    }

    private void UpdateUIFromPreferences() {
        iBackgroundColor = this.prefs.getInt(HW_PREF_ACHTERGROND, 0);
        svInstellingen.setBackgroundColor(iBackgroundColor);
        spnDag.setSelection(this.prefs.getInt(HW_PREF_FSDOW, 0));
        chkAutoUpdate.setChecked(this.prefs.getBoolean(HW_PREF_AUTOUPDATE, true));
        chkTransparant.setChecked(this.prefs.getBoolean(HW_PREF_TRANSPARANT, false));
        int iTimetableType = this.prefs.getInt(HW_PREF_TIMETABLETYPE, 0);
        spnTimetableType.setSelection(iTimetableType);
        switch (iTimetableType) {
            case 0:
                spnVolgorde.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblVolgorde.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblRotateFirst.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                btnRotateFirstDate.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblNumDaysRotating.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                spnNumDaysRotating.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                spnNumDays.setEnabled(true);
                spnDag.setEnabled(true);
                break;
            case 1:
                spnVolgorde.setVisibility(0);
                lblVolgorde.setVisibility(0);
                lblRotateFirst.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                btnRotateFirstDate.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblNumDaysRotating.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                spnNumDaysRotating.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                spnNumDays.setEnabled(true);
                spnDag.setEnabled(true);
                break;
            case 2:
                spnVolgorde.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblVolgorde.setVisibility(HW_PREF_NUMDAYSROTATING_DEFAULT);
                lblRotateFirst.setVisibility(0);
                btnRotateFirstDate.setVisibility(0);
                lblNumDaysRotating.setVisibility(0);
                spnNumDaysRotating.setVisibility(0);
                spnNumDays.setSelection(HW_PREF_STARTSCHERM_WEEK);
                spnDag.setSelection(2);
                spnNumDays.setEnabled(false);
                spnDag.setEnabled(false);
                break;
        }
        calRotateFirst = Calendar.getInstance();
        int iDatum = this.prefs.getInt(HW_PREF_ROTATEFIRSTDATE, 0);
        if (iDatum > 0) {
            calRotateFirst.set(1, iDatum / 10000);
            calRotateFirst.set(2, (iDatum % 10000) / 100);
            calRotateFirst.set(5, iDatum % 100);
        }
        int cyear = calRotateFirst.get(1);
        btnRotateFirstDate.setText((String) DateFormat.format("dd MMMM yyyy", new Date(cyear - 1900, calRotateFirst.get(2), calRotateFirst.get(5))));
        spnNumDaysRotating.setSelection(this.prefs.getInt(HW_PREF_NUMDAYSROTATING, HW_PREF_NUMDAYSROTATING_DEFAULT) - 1);
        spnVolgorde.setSelection(this.prefs.getInt(HW_PREF_VOLGORDE, 0));
        spnNumHours.setSelection(this.prefs.getInt(HW_PREF_NUMHOURS, HW_PREF_NUMHOURS_DEFAULT) - 1);
        spnNumDays.setSelection(this.prefs.getInt(HW_PREF_NUMDAYS, HW_PREF_NUMDAYS_DEFAULT) - 1);
        int iAppMode = this.prefs.getInt(HW_PREF_APPMODE, 0);
        if (iAppMode == 1) {
            radioDocent.setChecked(false);
            radioStudent.setChecked(true);
        }
        if (iAppMode == 2) {
            radioStudent.setChecked(false);
            radioDocent.setChecked(true);
        }
        spnStartScherm.setSelection(this.prefs.getInt(HW_PREF_STARTSCHERM, 0));
    }

    private void SavePreferences() {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(HW_PREF_ACHTERGROND, iBackgroundColor);
        editor.putInt(HW_PREF_FSDOW, spnDag.getSelectedItemPosition());
        editor.putBoolean(HW_PREF_AUTOUPDATE, chkAutoUpdate.isChecked());
        editor.putBoolean(HW_PREF_TRANSPARANT, chkTransparant.isChecked());
        editor.putInt(HW_PREF_TIMETABLETYPE, spnTimetableType.getSelectedItemPosition());
        editor.putInt(HW_PREF_ROTATEFIRSTDATE, (calRotateFirst.get(1) * 10000) + (calRotateFirst.get(2) * 100) + calRotateFirst.get(5));
        editor.putInt(HW_PREF_NUMDAYSROTATING, spnNumDaysRotating.getSelectedItemPosition() + 1);
        editor.putInt(HW_PREF_VOLGORDE, spnVolgorde.getSelectedItemPosition());
        editor.putInt(HW_PREF_NUMHOURS, spnNumHours.getSelectedItemPosition() + 1);
        editor.putInt(HW_PREF_NUMDAYS, spnNumDays.getSelectedItemPosition() + 1);
        if (radioStudent.isChecked()) {
            editor.putInt(HW_PREF_APPMODE, 1);
        }
        if (radioDocent.isChecked()) {
            editor.putInt(HW_PREF_APPMODE, 2);
        }
        editor.putInt(HW_PREF_STARTSCHERM, spnStartScherm.getSelectedItemPosition());
        editor.commit();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        SavePreferences();
        HuisWerkMain.UpdateWidgets(myContext);
        super.onPause();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
