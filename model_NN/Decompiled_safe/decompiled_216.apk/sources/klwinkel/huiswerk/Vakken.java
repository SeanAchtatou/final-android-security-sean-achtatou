package klwinkel.huiswerk;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import klwinkel.huiswerk.HuiswerkDatabase;

public class Vakken extends ListActivity {
    private static ImageButton lblNieuw;
    private HuiswerkDatabase.VakkenCursor cursorVakken;
    /* access modifiers changed from: private */
    public HuiswerkDatabase db;
    private final View.OnClickListener lblNieuwOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Vakken.this.startActivity(new Intent(Vakken.this, EditVak.class));
        }
    };

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.vakken);
        lblNieuw = (ImageButton) findViewById(R.id.lblNieuw);
        lblNieuw.setOnClickListener(this.lblNieuwOnClick);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            setTitle(getString(R.string.vakken_name));
        } else {
            setTitle(getString(R.string.klassen_name));
        }
        this.db = new HuiswerkDatabase(this);
        this.cursorVakken = this.db.getVakken(HuiswerkDatabase.VakkenCursor.SortBy.naam);
        startManagingCursor(this.cursorVakken);
        setListAdapter(new VakkenAdapter(this, 17367043, this.cursorVakken, new String[]{"naam"}, new int[]{16908308}));
        getListView().setDividerHeight(0);
    }

    public class VakkenAdapter extends SimpleCursorAdapter {
        private Context context;
        private HuiswerkDatabase.VakkenCursor localCursor;

        public VakkenAdapter(Context context2, int layout, Cursor c, String[] from, int[] to) {
            super(context2, layout, c, from, to);
            this.context = context2;
            this.localCursor = (HuiswerkDatabase.VakkenCursor) c;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.vakkenrow, (ViewGroup) null);
            }
            this.localCursor.moveToPosition(position);
            RelativeLayout row = (RelativeLayout) v.findViewById(R.id.row);
            TextView txtVak = (TextView) v.findViewById(R.id.lblVak);
            TextView txtLeraar = (TextView) v.findViewById(R.id.lblLeraar);
            TextView txtTelefoon = (TextView) v.findViewById(R.id.lblTelefoon);
            TextView txtEmail = (TextView) v.findViewById(R.id.lblEmail);
            HuiswerkDatabase.VakInfoCursor viC = Vakken.this.db.getVakInfo((long) this.localCursor.getColVakkenId().intValue());
            if (txtVak != null) {
                txtVak.setText(this.localCursor.getColNaam());
                if (viC.getCount() > 0) {
                    txtVak.setTextColor(viC.getColKleur().intValue());
                }
            }
            GradientDrawable shape = (GradientDrawable) Vakken.this.getResources().getDrawable(R.drawable.rounded);
            if (viC.getCount() > 0) {
                txtLeraar.setText(viC.getColLeraar());
                txtTelefoon.setText(viC.getColTelefoon());
                txtEmail.setText(viC.getColEmail());
            } else {
                shape.setColor(-1);
                row.setBackgroundDrawable(shape);
                txtLeraar.setText("-");
                txtTelefoon.setText("-");
                txtEmail.setText("-");
            }
            viC.close();
            return v;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        String str;
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str = getString(R.string.vakken_menu_toevoegen);
        } else {
            str = getString(R.string.klassen_menu_toevoegen);
        }
        menu.add(0, 0, 0, str);
        return supRetVal;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, EditVak.class));
                return true;
            default:
                return false;
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        this.cursorVakken.moveToPosition(position);
        Intent i = new Intent(this, EditVak.class);
        Bundle b = new Bundle();
        b.putInt("_id", this.cursorVakken.getColVakkenId().intValue());
        i.putExtras(b);
        startActivity(i);
        super.onListItemClick(l, v, position, id);
    }
}
