package klwinkel.huiswerk;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

public class AmbilWarnaKotak extends View {
    Shader dalam;
    float hue;
    Shader luar;
    Paint paint;
    float satudp;
    float[] tmp00;
    float ukuranUiDp;
    float ukuranUiPx;

    public AmbilWarnaKotak(Context context) {
        this(context, null);
    }

    public AmbilWarnaKotak(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AmbilWarnaKotak(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.ukuranUiDp = 240.0f;
        this.tmp00 = new float[3];
        this.satudp = context.getResources().getDimension(R.dimen.ambilwarna_satudp);
        this.ukuranUiPx = this.ukuranUiDp * this.satudp;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.paint == null) {
            this.paint = new Paint();
            this.luar = new LinearGradient(0.0f, 0.0f, 0.0f, this.ukuranUiPx, -1, -16777216, Shader.TileMode.CLAMP);
        }
        float[] fArr = this.tmp00;
        this.tmp00[2] = 1.0f;
        fArr[1] = 1.0f;
        this.tmp00[0] = this.hue;
        this.dalam = new LinearGradient(0.0f, 0.0f, this.ukuranUiPx, 0.0f, -1, Color.HSVToColor(this.tmp00), Shader.TileMode.CLAMP);
        this.paint.setShader(new ComposeShader(this.luar, this.dalam, PorterDuff.Mode.MULTIPLY));
        canvas.drawRect(0.0f, 0.0f, this.ukuranUiPx, this.ukuranUiPx, this.paint);
    }

    /* access modifiers changed from: package-private */
    public void setHue(float hue2) {
        this.hue = hue2;
        invalidate();
    }
}
