package klwinkel.huiswerk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.text.format.DateFormat;
import android.webkit.WebView;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuiswerkFoto extends Activity {
    private HuiswerkDatabase huiswerkDb;
    private WebView wvFoto;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.huiswerkfoto);
        this.huiswerkDb = new HuiswerkDatabase(this);
        this.wvFoto = (WebView) findViewById(R.id.wvFoto);
        this.wvFoto.getSettings().setBuiltInZoomControls(true);
        this.wvFoto.getSettings().setUseWideViewPort(true);
        this.wvFoto.setInitialScale(1);
        this.wvFoto.setBackgroundColor(0);
        int hwid = 0;
        String strFoto = "";
        String strTitle = "";
        Bundle bIn = getIntent().getExtras();
        if (bIn != null) {
            hwid = Integer.valueOf(bIn.getInt("_id")).intValue();
            strFoto = String.valueOf(bIn.getString("_foto"));
            strTitle = String.valueOf(bIn.getString("_title"));
        }
        if (hwid > 0) {
            HuiswerkDatabase.HuiswerkDetailCursor hwC = this.huiswerkDb.getHuiswerkDetails((long) hwid);
            if (hwC.getCount() > 0) {
                Integer iDatum = Integer.valueOf(hwC.getColDatum());
                strTitle = String.valueOf(hwC.getColVakNaam()) + " - " + ((String) DateFormat.format("  EEEE dd MMMM", new Date(Integer.valueOf(iDatum.intValue() / 10000).intValue() - 1900, Integer.valueOf((iDatum.intValue() % 10000) / 100).intValue(), Integer.valueOf(iDatum.intValue() % 100).intValue())));
            }
            hwC.close();
            HuiswerkDatabase.HuiswerkFotoCursor chwf = this.huiswerkDb.getHuiswerkFoto((long) hwid);
            if (chwf.getCount() > 0) {
                strFoto = chwf.getColFoto();
            }
            chwf.close();
        }
        setTitle(strTitle);
        if (strFoto.length() > 0) {
            new String();
            this.wvFoto.loadDataWithBaseURL("file://" + Environment.getExternalStorageDirectory() + getPackageName(), "<html><body><header><h1>Title</h1></header><section><img src=\"file://" + strFoto + "\" align=left><p>Content</p></section></body></html>", "text/html", "utf-8", "");
        }
    }
}
