package klwinkel.huiswerk;

import android.app.Activity;
import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ResourceUtils {
    public static String loadResToString(int resId, Activity ctx) {
        try {
            InputStream is = ctx.getResources().openRawResource(resId);
            byte[] buffer = new byte[4096];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while (true) {
                int read = is.read(buffer);
                if (read == -1) {
                    baos.close();
                    is.close();
                    return baos.toString();
                }
                baos.write(buffer, 0, read);
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static int getResourceIdForDrawable(Context _context, String resPackage, String resName) {
        return _context.getResources().getIdentifier(resName, "drawable", resPackage);
    }
}
