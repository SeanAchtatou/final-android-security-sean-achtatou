package klwinkel.huiswerk;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class RoosterWijzigingen extends ListActivity {
    /* access modifiers changed from: private */
    public static HuiswerkDatabase db;
    private static ImageButton lblOpruimen;
    private static LinearLayout llMain;
    /* access modifiers changed from: private */
    public static Context myContext;
    /* access modifiers changed from: private */
    public static HuiswerkDatabase.RoosterWijzigingCursorAll rwCursor;
    private static HuiswerkDatabase.RoosterWijzigingCursorAll rwCursorDelete;
    private final View.OnClickListener lblOpruimenOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterWijzigingen.this.OpruimenTempRooster();
        }
    };

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.roosterwijzigingen);
        myContext = this;
        llMain = (LinearLayout) findViewById(R.id.llMain);
        lblOpruimen = (ImageButton) findViewById(R.id.lblOpruimen);
        db = new HuiswerkDatabase(this);
        rwCursor = db.getRoosterWijzigingAll();
        startManagingCursor(rwCursor);
        setListAdapter(new RoosterDataAdapter(this, 17367043, rwCursor, new String[]{"lokaal"}, new int[]{16908308}));
        getListView().setDividerHeight(2);
        lblOpruimen.setOnClickListener(this.lblOpruimenOnClick);
    }

    public class RoosterDataAdapter extends SimpleCursorAdapter {
        private Context context;
        private HuiswerkDatabase.RoosterWijzigingCursorAll localCursor;

        public RoosterDataAdapter(Context context2, int layout, Cursor c, String[] from, int[] to) {
            super(context2, layout, c, from, to);
            this.context = context2;
            this.localCursor = (HuiswerkDatabase.RoosterWijzigingCursorAll) c;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.roosterwijzigingenrow, (ViewGroup) null);
            }
            this.localCursor.moveToPosition(position);
            LinearLayout row = (LinearLayout) v.findViewById(R.id.row);
            TextView txtDatum = (TextView) v.findViewById(R.id.lblDatum);
            TextView lblUur = (TextView) v.findViewById(R.id.lblUur);
            TextView lblLokaal = (TextView) v.findViewById(R.id.lblLokaal);
            TextView txtUur = (TextView) v.findViewById(R.id.txtUur);
            TextView txtVak = (TextView) v.findViewById(R.id.txtVak);
            TextView txtLokaal = (TextView) v.findViewById(R.id.txtLokaal);
            if (row != null) {
                HuiswerkDatabase.VakInfoCursor viC = RoosterWijzigingen.db.getVakInfo(this.localCursor.getColVakId());
                if (viC.getCount() > 0) {
                    GradientDrawable shape = (GradientDrawable) RoosterWijzigingen.this.getResources().getDrawable(R.drawable.rounded);
                    shape.setColor(viC.getColKleur().intValue());
                    row.setBackgroundDrawable(shape);
                }
                viC.close();
            }
            if (txtDatum != null) {
                Integer iDatum = Integer.valueOf(this.localCursor.getColDatum());
                Integer iYear = Integer.valueOf(iDatum.intValue() / 10000);
                Integer iMonth = Integer.valueOf((iDatum.intValue() % 10000) / 100);
                Integer iDay = Integer.valueOf(iDatum.intValue() % 100);
                txtDatum.setText((String) DateFormat.format("EEEE dd MMMM", new Date(iYear.intValue() - 1900, iMonth.intValue(), iDay.intValue())));
            }
            if (lblUur != null) {
                lblUur.setText(RoosterWijzigingen.this.getString(R.string.lesuur));
            }
            if (lblLokaal != null) {
                lblLokaal.setText(RoosterWijzigingen.this.getString(R.string.lokaal));
            }
            if (txtUur != null) {
                txtUur.setText(String.format("%d    (%s - %s)", Integer.valueOf(this.localCursor.getColUur()), HwUtl.Time2String(this.localCursor.getColBegin()), HwUtl.Time2String(this.localCursor.getColEinde())));
            }
            if (txtVak != null) {
                txtVak.setText(this.localCursor.getColVakNaam());
            }
            if (txtLokaal != null) {
                txtLokaal.setText(this.localCursor.getColLokaal());
            }
            return v;
        }
    }

    public void onResume() {
        llMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        rwCursor.moveToPosition(position);
        Intent i = new Intent(this, EditRoosterWijzigingen.class);
        Bundle b = new Bundle();
        b.putInt("_id", (int) rwCursor.getColRoosterId());
        i.putExtras(b);
        startActivity(i);
        super.onListItemClick(l, v, position, id);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, getString(R.string.roosterwijzigingen_menu_nieuw));
        return supRetVal;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Intent i = new Intent(this, EditRoosterWijzigingen.class);
                Bundle b = new Bundle();
                b.putInt("_id", 0);
                i.putExtras(b);
                startActivity(i);
                return true;
            default:
                return false;
        }
    }

    public void OpruimenTempRooster() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -1:
                        Calendar cal = Calendar.getInstance();
                        int curDatum = (cal.get(1) * 10000) + (cal.get(2) * 100) + cal.get(5);
                        if (RoosterWijzigingen.rwCursor.getCount() > 0) {
                            RoosterWijzigingen.rwCursor.moveToFirst();
                            while (!RoosterWijzigingen.rwCursor.isLast()) {
                                if (RoosterWijzigingen.rwCursor.getColDatum() <= curDatum) {
                                    RoosterWijzigingen.db.deleteRoosterWijziging(RoosterWijzigingen.rwCursor.getColRoosterId());
                                    RoosterWijzigingen.rwCursor.requery();
                                    RoosterWijzigingen.rwCursor.moveToFirst();
                                } else {
                                    RoosterWijzigingen.rwCursor.moveToNext();
                                }
                            }
                            if (RoosterWijzigingen.rwCursor.getCount() > 0 && RoosterWijzigingen.rwCursor.getColDatum() <= curDatum) {
                                RoosterWijzigingen.db.deleteRoosterWijziging(RoosterWijzigingen.rwCursor.getColRoosterId());
                                RoosterWijzigingen.rwCursor.requery();
                            }
                        }
                        HuisWerkMain.UpdateWidgets(RoosterWijzigingen.myContext);
                        return;
                    default:
                        return;
                }
            }
        };
        new AlertDialog.Builder(this).setMessage(getString(R.string.opruimenroosterwijzigingen)).setPositiveButton(getString(R.string.ja), dialogClickListener).setNegativeButton(getString(R.string.nee), dialogClickListener).show();
    }
}
