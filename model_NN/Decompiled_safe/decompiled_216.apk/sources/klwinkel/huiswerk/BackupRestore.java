package klwinkel.huiswerk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.IOException;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class BackupRestore extends Activity {
    private static Button btnBackup;
    private static Button btnRestore;
    /* access modifiers changed from: private */
    public static TextView lblRestore;
    /* access modifiers changed from: private */
    public static Context myContext;
    private static ScrollView svMain;
    private final View.OnClickListener btnBackupOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String strVak;
            String strBody = "";
            HuiswerkDatabase.HuiswerkCursor c = BackupRestore.this.db.getHuiswerk(HuiswerkDatabase.HuiswerkCursor.SortBy.datum);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                Integer iDatum = Integer.valueOf(c.getColDatum());
                String strDatum = (String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(iDatum.intValue() / 10000).intValue() - 1900, Integer.valueOf((iDatum.intValue() % 10000) / 100).intValue(), Integer.valueOf(iDatum.intValue() % 100).intValue()));
                if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                    strVak = BackupRestore.this.getString(R.string.vak);
                } else {
                    strVak = BackupRestore.this.getString(R.string.klas);
                }
                strBody = String.valueOf(strBody) + String.format("%s: %s \n%s: %s \n%s: %s \n%s: %s \n%s \n%s: %d\n\n", BackupRestore.this.getString(R.string.datum), strDatum, strVak, c.getColVakNaam(), BackupRestore.this.getString(R.string.hoofdstuk), c.getColHoofdstuk(), BackupRestore.this.getString(R.string.pagina), c.getColPagina(), c.getColOmschrijving(), BackupRestore.this.getString(R.string.toets), Long.valueOf(c.getColToets()));
            }
            c.close();
            String strMessageBody = strBody;
            try {
                BackupRestore.this.db.backupDataBase();
                final String str = strMessageBody;
                new AlertDialog.Builder(v.getContext()).setTitle(BackupRestore.this.getString(R.string.succes)).setMessage("").setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BackupRestore.lblRestore.setText(String.valueOf(BackupRestore.this.getString(R.string.backuptekst)) + BackupRestore.this.db.getBackupNaam() + "\n(" + BackupRestore.this.db.getBackupDate() + ")");
                        Intent emailIntent = new Intent("android.intent.action.SEND");
                        emailIntent.setType("plain/text");
                        emailIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(BackupRestore.this.getString(R.string.app_name)) + "Backup");
                        emailIntent.putExtra("android.intent.extra.TEXT", str);
                        emailIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + BackupRestore.this.db.getBackupNaam()));
                        BackupRestore.this.startActivity(Intent.createChooser(emailIntent, "Send Backup"));
                    }
                }).show();
            } catch (IOException e) {
                IOException e2 = e;
                e2.printStackTrace();
                new AlertDialog.Builder(v.getContext()).setTitle(BackupRestore.this.getString(R.string.failed)).setMessage(e2.getMessage()).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }
    };
    private final View.OnClickListener btnRestoreOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                BackupRestore.this.db.restoreDataBase();
                new AlertDialog.Builder(v.getContext()).setTitle(BackupRestore.this.getString(R.string.succes)).setMessage("").setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        HuisWerkMain.UpdateWidgets(BackupRestore.myContext);
                    }
                }).show();
            } catch (IOException e) {
                IOException e2 = e;
                e2.printStackTrace();
                new AlertDialog.Builder(v.getContext()).setTitle(BackupRestore.this.getString(R.string.failed)).setMessage(e2.getMessage()).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
            }
        }
    };
    /* access modifiers changed from: private */
    public HuiswerkDatabase db;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.backuprestore);
        myContext = this;
        this.db = new HuiswerkDatabase(this);
        svMain = (ScrollView) findViewById(R.id.svMain);
        lblRestore = (TextView) findViewById(R.id.lblRestore);
        lblRestore.setText(String.valueOf(getString(R.string.backuptekst)) + this.db.getBackupNaam() + "\n(" + this.db.getBackupDate() + ")");
        btnBackup = (Button) findViewById(R.id.btnBackup);
        btnRestore = (Button) findViewById(R.id.btnRestore);
        btnBackup.setOnClickListener(this.btnBackupOnClick);
        btnRestore.setOnClickListener(this.btnRestoreOnClick);
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
