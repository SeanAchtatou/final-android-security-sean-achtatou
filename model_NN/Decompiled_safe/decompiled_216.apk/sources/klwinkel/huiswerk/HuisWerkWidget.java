package klwinkel.huiswerk;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkWidget extends AppWidgetProvider {
    public static final String REFRESH = "klwinkel.huiswerk.huiswerkwidget.REFRESH";
    public static boolean bAutoUpdate;
    public static boolean bTransparant;
    public AlarmManager am;
    public Calendar calRoosterDag;
    public Integer curTime = 0;
    public HuiswerkDatabase db;
    public HuiswerkDatabase.HuiswerkVakCursor hwvCursor = null;
    public Integer iDatum = 0;
    public Integer iLesson = 0;
    public LessonData[] lessonData;
    public HuiswerkDatabase.RoosterCursorNu nuCursor = null;
    public HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = null;

    public class LessonData {
        public boolean klaar;
        public int kleur;
        public boolean les;
        public String lok;
        public int start;
        public int stop;
        public boolean tmp;
        public boolean toets;
        public int uur;
        public String vak;
        public long vakid;

        LessonData(boolean les2, boolean tmp2, int uur2, int start2, int stop2, String vak2, String lok2, boolean klaar2, boolean toets2, int kleur2, long vakid2) {
            this.les = les2;
            this.start = start2;
            this.stop = stop2;
            this.uur = uur2;
            this.vak = vak2;
            this.lok = lok2;
            this.klaar = klaar2;
            this.toets = toets2;
            this.tmp = tmp2;
            this.kleur = kleur2;
            this.vakid = vakid2;
        }
    }

    public void onReceive(Context ctxt, Intent intent) {
        if (REFRESH.equals(intent.getAction())) {
            DoUpdate(ctxt);
        } else {
            super.onReceive(ctxt, intent);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        DoUpdate(context);
    }

    public void DoUpdate(Context ctxt) {
        RemoteViews updateViews;
        this.db = new HuiswerkDatabase(ctxt, false);
        if (this.db.getRelNotesVersionFirstTime() < 33) {
            updateViews = new RemoteViews(ctxt.getPackageName(), (int) R.layout.widget);
        } else {
            this.db.Init(ctxt);
            updateViews = buildUpdate(ctxt);
        }
        this.db.close();
        AppWidgetManager.getInstance(ctxt).updateAppWidget(new ComponentName(ctxt, HuisWerkWidget.class), updateViews);
    }

    private RemoteViews buildUpdate(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        bAutoUpdate = prefs.getBoolean("HW_PREF_AUTOUPDATE", true);
        bTransparant = prefs.getBoolean("HW_PREF_TRANSPARANT", false);
        if (!VakantieDagen.IsInitialised()) {
            VakantieDagen.LaadVakantieDagen(this.db);
        }
        this.lessonData = new LessonData[21];
        this.iLesson = 20;
        this.calRoosterDag = Calendar.getInstance();
        this.curTime = Integer.valueOf((this.calRoosterDag.get(11) * 100) + this.calRoosterDag.get(12));
        boolean bFoundLesson = false;
        int day = 0;
        while (true) {
            if (day >= 200) {
                break;
            }
            if (!VakantieDagen.IsVakantieDag(this.calRoosterDag)) {
                this.iDatum = Integer.valueOf((this.calRoosterDag.get(1) * 10000) + (this.calRoosterDag.get(2) * 100) + this.calRoosterDag.get(5));
                this.nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(context.getApplicationContext(), this.calRoosterDag));
                this.rwCursor = this.db.getRoosterWijzigingDatum((long) this.iDatum.intValue());
                if (this.rwCursor.getCount() > 0 || this.nuCursor.getCount() > 0) {
                    if (day != 0) {
                        bFoundLesson = true;
                        this.iLesson = 1;
                        this.nuCursor.close();
                        this.rwCursor.close();
                        break;
                    }
                    if (this.nuCursor.getCount() > 0) {
                        while (!bFoundLesson) {
                            if (this.curTime.intValue() > this.nuCursor.getColBegin()) {
                                if (this.nuCursor.isLast()) {
                                    break;
                                }
                                this.nuCursor.moveToNext();
                            } else {
                                if (this.nuCursor.getColUur() < this.iLesson.intValue()) {
                                    this.iLesson = Integer.valueOf(this.nuCursor.getColUur());
                                }
                                bFoundLesson = true;
                            }
                        }
                    }
                    if (this.rwCursor.getCount() > 0) {
                        while (!bFoundLesson) {
                            if (this.curTime.intValue() > this.rwCursor.getColBegin()) {
                                if (this.rwCursor.isLast()) {
                                    break;
                                }
                                this.rwCursor.moveToNext();
                            } else {
                                if (this.rwCursor.getColUur() < this.iLesson.intValue()) {
                                    this.iLesson = Integer.valueOf(this.rwCursor.getColUur());
                                }
                                bFoundLesson = true;
                            }
                        }
                    }
                    if (bFoundLesson) {
                        this.nuCursor.close();
                        this.rwCursor.close();
                        break;
                    }
                }
                this.nuCursor.close();
                this.rwCursor.close();
            }
            this.calRoosterDag.add(5, 1);
            day++;
        }
        if (!bFoundLesson) {
            this.iLesson = 1;
            this.calRoosterDag = Calendar.getInstance();
            this.iDatum = Integer.valueOf((this.calRoosterDag.get(1) * 10000) + (this.calRoosterDag.get(2) * 100) + this.calRoosterDag.get(5));
        }
        for (int uur = 0; uur < 21; uur++) {
            this.lessonData[uur] = new LessonData(false, false, 0, 0, 0, "", "", true, false, -1, 0);
        }
        this.nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(context.getApplicationContext(), this.calRoosterDag));
        while (!this.nuCursor.isAfterLast()) {
            int iUur = this.nuCursor.getColUur();
            this.lessonData[iUur].les = true;
            this.lessonData[iUur].uur = iUur;
            this.lessonData[iUur].start = this.nuCursor.getColBegin();
            this.lessonData[iUur].stop = this.nuCursor.getColEinde();
            this.lessonData[iUur].vak = this.nuCursor.getColVakNaam();
            this.lessonData[iUur].vakid = this.nuCursor.getColVakId();
            this.lessonData[iUur].lok = this.nuCursor.getColLokaal();
            this.hwvCursor = this.db.getHuiswerkVak((long) this.iDatum.intValue(), this.nuCursor.getColVakId());
            if (this.hwvCursor.getCount() > 0) {
                this.lessonData[iUur].klaar = this.hwvCursor.getColKlaar() == 1;
                this.lessonData[iUur].toets = this.hwvCursor.getColToets() == 1;
            } else {
                this.lessonData[iUur].klaar = true;
                this.lessonData[iUur].toets = false;
            }
            this.hwvCursor.close();
            this.nuCursor.moveToNext();
        }
        this.nuCursor.close();
        this.rwCursor = this.db.getRoosterWijzigingDatum((long) this.iDatum.intValue());
        while (!this.rwCursor.isAfterLast()) {
            int iUur2 = this.rwCursor.getColUur();
            this.lessonData[iUur2].les = true;
            this.lessonData[iUur2].tmp = true;
            this.lessonData[iUur2].uur = iUur2;
            this.lessonData[iUur2].start = this.rwCursor.getColBegin();
            this.lessonData[iUur2].stop = this.rwCursor.getColEinde();
            this.lessonData[iUur2].vak = this.rwCursor.getColVakNaam();
            this.lessonData[iUur2].vakid = this.rwCursor.getColVakId();
            this.lessonData[iUur2].lok = this.rwCursor.getColLokaal();
            this.hwvCursor = this.db.getHuiswerkVak((long) this.iDatum.intValue(), this.rwCursor.getColVakId());
            if (this.hwvCursor.getCount() > 0) {
                this.lessonData[iUur2].klaar = this.hwvCursor.getColKlaar() == 1;
                this.lessonData[iUur2].toets = this.hwvCursor.getColToets() == 1;
            } else {
                this.lessonData[iUur2].klaar = true;
                this.lessonData[iUur2].toets = false;
            }
            this.hwvCursor.close();
            this.rwCursor.moveToNext();
        }
        this.rwCursor.close();
        for (int iuur = 0; iuur < 21; iuur++) {
            if (this.lessonData[iuur].vakid > 0 && this.db.getRelNotesVersion() >= 16) {
                HuiswerkDatabase.VakInfoCursor viC = this.db.getVakInfo(this.lessonData[iuur].vakid);
                if (viC.getCount() > 0) {
                    this.lessonData[iuur].kleur = viC.getColKleur().intValue();
                }
                viC.close();
            }
        }
        remoteViews.setImageViewResource(R.id.rowiconWidget, R.drawable.icon);
        remoteViews.setTextViewText(R.id.lblWidgetKop, (String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(this.iDatum.intValue() / 10000).intValue() - 1900, Integer.valueOf((this.iDatum.intValue() % 10000) / 100).intValue(), Integer.valueOf(this.iDatum.intValue() % 100).intValue())));
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur1, R.id.lblWidgetTijd1, R.id.lblWidgetVak1, R.id.imgWidgetHuiswerk1, R.id.imgWidgetToets1, R.id.lblWidgetLokaal1);
        if (bAutoUpdate && this.iLesson.intValue() <= 20) {
            this.am = (AlarmManager) context.getSystemService("alarm");
            Intent intent = new Intent(context, HuisWerkWidget.class);
            intent.setAction(REFRESH);
            this.am.set(1, this.calRoosterDag.getTimeInMillis() + 300000, PendingIntent.getBroadcast(context, 0, intent, 0));
        }
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur2, R.id.lblWidgetTijd2, R.id.lblWidgetVak2, R.id.imgWidgetHuiswerk2, R.id.imgWidgetToets2, R.id.lblWidgetLokaal2);
        buildRoosterLine(context, remoteViews, R.id.lblWidgetUur3, R.id.lblWidgetTijd3, R.id.lblWidgetVak3, R.id.imgWidgetHuiswerk3, R.id.imgWidgetToets3, R.id.lblWidgetLokaal3);
        remoteViews.setOnClickPendingIntent(R.id.llText, PendingIntent.getActivity(context, 0, new Intent(context, HuisWerkMain.class), 0));
        remoteViews.setOnClickPendingIntent(R.id.rowiconWidget, PendingIntent.getActivity(context, 0, new Intent(context, HuisWerkMain.class), 0));
        return remoteViews;
    }

    private void buildRoosterLine(Context context, RemoteViews views, int id_uur, int id_tijd, int id_vak, int id_huiswerk, int id_toets, int id_lokaal) {
        String strUur;
        views.setTextViewText(id_uur, new StringBuilder().append(""));
        views.setTextViewText(id_tijd, new StringBuilder().append(""));
        views.setTextViewText(id_vak, new StringBuilder().append(""));
        views.setTextViewText(id_lokaal, new StringBuilder().append(""));
        views.setImageViewResource(id_huiswerk, 0);
        views.setImageViewResource(id_toets, 0);
        if (bTransparant) {
            views.setImageViewResource(R.id.background, R.drawable.frametrans);
        } else {
            views.setImageViewResource(R.id.background, R.drawable.frame);
        }
        while (this.iLesson.intValue() <= 20 && !this.lessonData[this.iLesson.intValue()].les) {
            this.iLesson = Integer.valueOf(this.iLesson.intValue() + 1);
        }
        if (this.iLesson.intValue() <= 20 && this.lessonData[this.iLesson.intValue()].les) {
            int iStartTijd = this.lessonData[this.iLesson.intValue()].start;
            int iStopTijd = this.lessonData[this.iLesson.intValue()].stop;
            this.calRoosterDag.set(11, iStartTijd / 100);
            this.calRoosterDag.set(12, iStartTijd % 100);
            this.calRoosterDag.set(13, 0);
            String strTijd = String.valueOf(HwUtl.Time2StringWidget(context, iStartTijd)) + "~" + HwUtl.Time2StringWidget(context, iStopTijd);
            if (this.lessonData[this.iLesson.intValue()].tmp) {
                strUur = String.format("*%d", Integer.valueOf(this.lessonData[this.iLesson.intValue()].uur));
            } else {
                strUur = String.format("%d", Integer.valueOf(this.lessonData[this.iLesson.intValue()].uur));
            }
            views.setTextViewText(id_uur, new StringBuilder().append(strUur));
            views.setTextColor(id_uur, -1);
            views.setTextViewText(id_tijd, new StringBuilder().append(strTijd));
            views.setTextColor(id_tijd, -1);
            views.setTextViewText(id_vak, new StringBuilder().append(this.lessonData[this.iLesson.intValue()].vak));
            views.setTextColor(id_vak, this.lessonData[this.iLesson.intValue()].kleur);
            views.setTextViewText(id_lokaal, new StringBuilder().append(this.lessonData[this.iLesson.intValue()].lok));
            views.setTextColor(id_lokaal, -1);
            if (!this.lessonData[this.iLesson.intValue()].klaar) {
                views.setImageViewResource(id_huiswerk, R.drawable.huiswerk);
            }
            if (this.lessonData[this.iLesson.intValue()].toets) {
                views.setImageViewResource(id_toets, R.drawable.toets);
            }
        }
        this.iLesson = Integer.valueOf(this.iLesson.intValue() + 1);
    }
}
