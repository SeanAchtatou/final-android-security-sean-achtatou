package klwinkel.huiswerk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.Calendar;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditRoosterWeek extends Activity implements View.OnTouchListener {
    private long copyRoosterid = 0;
    private HuiswerkDatabase db;
    private float downXValue;
    private Calendar firstDayOfWeek;
    /* access modifiers changed from: private */
    public int iNumColumns = 0;
    private int iPrefNumDaysRotating = 0;
    /* access modifiers changed from: private */
    public int iPrefNumHours = 0;
    /* access modifiers changed from: private */
    public int iPrefTimetableType = 0;
    /* access modifiers changed from: private */
    public int iWeek = 1;
    private ImageView lblAchteruit;
    private final View.OnClickListener lblAchteruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditRoosterWeek.this.PreviousWeek();
        }
    };
    private ImageView lblVooruit;
    private final View.OnClickListener lblVooruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditRoosterWeek.this.NextWeek();
        }
    };
    private int mCurDate = 0;
    GridView mGrid;
    private HuiswerkDatabase.RoosterCursorNu nuCursor = null;
    private RelativeLayout rlHeader;
    VakLokaal[][] roosterData;
    private TextView roosterDatum;

    private class VakLokaal {
        public int begin;
        public int dow;
        public int einde;
        public int kleur;
        public boolean les;
        public String lok;
        public long roosterid;
        public int uur;
        public String vak;
        public int vakid;

        VakLokaal(boolean les2, int dow2, int uur2, int begin2, int einde2, String vak2, String lok2, int vakid2, int kleur2, long roosterid2) {
            this.les = les2;
            this.dow = dow2;
            this.uur = uur2;
            this.begin = begin2;
            this.einde = einde2;
            this.vak = vak2;
            this.lok = lok2;
            this.vakid = vakid2;
            this.kleur = kleur2;
            this.roosterid = roosterid2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.editroosterweek);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.iPrefNumHours = prefs.getInt("HW_PREF_NUMHOURS", 10);
        this.iPrefTimetableType = prefs.getInt("HW_PREF_TIMETABLETYPE", 0);
        this.iPrefNumDaysRotating = prefs.getInt("HW_PREF_NUMDAYSROTATING", 8);
        switch (this.iPrefTimetableType) {
            case 0:
                this.iNumColumns = 7;
                break;
            case 1:
                this.iNumColumns = 7;
                break;
            case 2:
                this.iNumColumns = this.iPrefNumDaysRotating;
                break;
        }
        this.rlHeader = (RelativeLayout) findViewById(R.id.rlHeader);
        this.roosterDatum = (TextView) findViewById(R.id.roosterdatum);
        this.lblVooruit = (ImageView) findViewById(R.id.lblVooruit);
        this.lblAchteruit = (ImageView) findViewById(R.id.lblAchteruit);
        this.lblVooruit.setOnClickListener(this.lblVooruitOnClick);
        this.lblAchteruit.setOnClickListener(this.lblAchteruitOnClick);
        this.db = new HuiswerkDatabase(this);
        this.roosterData = (VakLokaal[][]) Array.newInstance(VakLokaal.class, this.iNumColumns, 20);
        this.firstDayOfWeek = Calendar.getInstance();
        this.mCurDate = (this.firstDayOfWeek.get(1) * 10000) + (this.firstDayOfWeek.get(2) * 100) + this.firstDayOfWeek.get(5);
        FindFirstDateOfWeek();
        this.mGrid = (GridView) findViewById(R.id.rGrid);
        this.mGrid.setNumColumns(this.iNumColumns);
        this.mGrid.setOnTouchListener(this);
        this.mGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                if (position >= EditRoosterWeek.this.iNumColumns) {
                    int idag = position % EditRoosterWeek.this.iNumColumns;
                    VakLokaal item = EditRoosterWeek.this.roosterData[idag][(position / EditRoosterWeek.this.iNumColumns) - 1];
                    Intent i = new Intent(EditRoosterWeek.this, EditRoosterUur.class);
                    Bundle b = new Bundle();
                    b.putInt("_week", EditRoosterWeek.this.iWeek);
                    b.putInt("_dag", item.dow);
                    b.putInt("_uur", item.uur);
                    if (EditRoosterWeek.this.iPrefTimetableType == 2) {
                        b.putInt("_rotating", 1);
                    } else {
                        b.putInt("_rotating", 0);
                    }
                    i.putExtras(b);
                    EditRoosterWeek.this.startActivity(i);
                }
            }
        });
        registerForContextMenu(this.mGrid);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        VakLokaal item = (VakLokaal) ((RoosterAdapter) this.mGrid.getAdapter()).getItem(info.position);
        if (item != null) {
            if (item.les) {
                menu.add(0, info.position, 0, getString(R.string.kopieren));
            }
            if (this.copyRoosterid > 0) {
                menu.add(0, info.position, 1, getString(R.string.plakken));
            }
            if (item.les) {
                menu.add(0, info.position, 2, getString(R.string.verwijderen));
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getOrder() == 0) {
            function1(info.position);
        } else if (item.getOrder() == 1) {
            function2(info.position);
        } else if (item.getOrder() != 2) {
            return false;
        } else {
            function3(info.position);
        }
        return true;
    }

    public void function1(int id) {
        VakLokaal item = (VakLokaal) ((RoosterAdapter) this.mGrid.getAdapter()).getItem(id);
        if (item != null && item.les) {
            this.copyRoosterid = item.roosterid;
        }
    }

    public void function2(int id) {
        VakLokaal item = (VakLokaal) ((RoosterAdapter) this.mGrid.getAdapter()).getItem(id);
        if (item != null && this.copyRoosterid > 0) {
            HuiswerkDatabase.RoosterCursorId rcCopy = this.db.getRoosterId(this.copyRoosterid);
            if (rcCopy.getCount() > 0) {
                long lPasteDag = (long) (item.dow + ((this.iWeek - 1) * 7));
                long lPasteUur = (long) item.uur;
                HuiswerkDatabase.RoosterCursor rcPaste = this.db.getRooster(lPasteDag, lPasteUur);
                if (rcPaste.getCount() > 0) {
                    this.db.editRooster(rcPaste.getColRoosterId(), lPasteDag, lPasteUur, (long) rcPaste.getColBegin(), (long) rcPaste.getColEinde(), rcCopy.getColVakId(), rcCopy.getColLokaal(), 0);
                } else {
                    HuiswerkDatabase.LesUurCursor uC = this.db.getLesUur(lPasteUur);
                    long lBegin = (long) uC.getColStart();
                    long lEinde = (long) uC.getColStop();
                    if (rcCopy.getColUur() == item.uur) {
                        if (rcCopy.getColBegin() != lBegin) {
                            lBegin = rcCopy.getColBegin();
                        }
                        if (rcCopy.getColEinde() != lEinde) {
                            lEinde = rcCopy.getColEinde();
                        }
                    }
                    this.db.addRooster(lPasteDag, lPasteUur, lBegin, lEinde, rcCopy.getColVakId(), rcCopy.getColLokaal(), 0);
                }
                rcPaste.close();
                rcCopy.close();
                FillRooster();
                HuisWerkMain.UpdateWidgets(HuisWerkMain.AppContext);
            }
        }
    }

    public void function3(int id) {
        VakLokaal item = (VakLokaal) ((RoosterAdapter) this.mGrid.getAdapter()).getItem(id);
        if (item != null) {
            HuiswerkDatabase.RoosterCursor rC = this.db.getRooster((long) (item.dow + ((this.iWeek - 1) * 7)), (long) item.uur);
            if (rC.getCount() > 0) {
                if (rC.getColRoosterId() == this.copyRoosterid) {
                    this.copyRoosterid = 0;
                }
                this.db.deleteRooster(rC.getColRoosterId());
                FillRooster();
            }
            rC.close();
            HuisWerkMain.UpdateWidgets(HuisWerkMain.AppContext);
        }
    }

    private void FindFirstDateOfWeek() {
        int iFirstSchoolDayOfWeek = 1;
        if (this.iPrefTimetableType != 2) {
            iFirstSchoolDayOfWeek = this.db.FindFirstSchoolDayOfWeek(getApplicationContext());
        }
        Calendar calRoosterDag = (Calendar) this.firstDayOfWeek.clone();
        int iDay = this.firstDayOfWeek.get(7);
        if (iFirstSchoolDayOfWeek > iDay) {
            if (iFirstSchoolDayOfWeek - iDay <= 2) {
                calRoosterDag.add(5, iFirstSchoolDayOfWeek - iDay);
            } else {
                calRoosterDag.add(5, (iFirstSchoolDayOfWeek - iDay) - 7);
            }
        }
        if (iFirstSchoolDayOfWeek < iDay) {
            if (iDay - iFirstSchoolDayOfWeek > 4) {
                calRoosterDag.add(5, 7 - (iDay - iFirstSchoolDayOfWeek));
            } else {
                calRoosterDag.add(5, iFirstSchoolDayOfWeek - iDay);
            }
        }
        this.firstDayOfWeek = (Calendar) calRoosterDag.clone();
    }

    private void FillRooster() {
        if (this.iPrefTimetableType == 1) {
            this.roosterDatum.setText(String.valueOf(getString(R.string.week)) + String.format(" %d", Integer.valueOf(this.iWeek)));
        } else {
            this.roosterDatum.setVisibility(8);
        }
        if (this.iPrefTimetableType != 1) {
            this.rlHeader.setVisibility(8);
        } else if (this.iWeek == 1) {
            this.lblVooruit.setVisibility(0);
            this.lblAchteruit.setVisibility(4);
        } else {
            this.lblVooruit.setVisibility(4);
            this.lblAchteruit.setVisibility(0);
        }
        Calendar calRoosterDag = (Calendar) this.firstDayOfWeek.clone();
        for (int iDag = 0; iDag < this.iNumColumns; iDag++) {
            int i = calRoosterDag.get(7);
            for (int iuur = 0; iuur < 20; iuur++) {
                this.roosterData[iDag][iuur] = new VakLokaal(false, iDag + 1, iuur + 1, 0, 0, "", "", 0, -12303292, 0);
            }
            this.nuCursor = this.db.getRoosterNu((long) (iDag + 1 + ((this.iWeek - 1) * 7)));
            while (!this.nuCursor.isAfterLast()) {
                int iUur = this.nuCursor.getColUur();
                this.roosterData[iDag][iUur - 1].les = true;
                this.roosterData[iDag][iUur - 1].kleur = -1;
                this.roosterData[iDag][iUur - 1].uur = iUur;
                this.roosterData[iDag][iUur - 1].begin = this.nuCursor.getColBegin();
                this.roosterData[iDag][iUur - 1].einde = this.nuCursor.getColEinde();
                this.roosterData[iDag][iUur - 1].vak = this.nuCursor.getColVakKort();
                this.roosterData[iDag][iUur - 1].vakid = (int) this.nuCursor.getColVakId();
                this.roosterData[iDag][iUur - 1].lok = this.nuCursor.getColLokaal();
                this.roosterData[iDag][iUur - 1].roosterid = this.nuCursor.getColRoosterId();
                this.nuCursor.moveToNext();
            }
            this.nuCursor.close();
            for (int iuur2 = 0; iuur2 < 20; iuur2++) {
                if (this.roosterData[iDag][iuur2].vakid > 0) {
                    HuiswerkDatabase.VakInfoCursor viC = this.db.getVakInfo((long) this.roosterData[iDag][iuur2].vakid);
                    if (viC.getCount() > 0) {
                        this.roosterData[iDag][iuur2].kleur = viC.getColKleur().intValue();
                    }
                    viC.close();
                }
            }
            calRoosterDag.add(5, 1);
        }
        this.mGrid.setAdapter((ListAdapter) new RoosterAdapter(this, this.mCurDate));
    }

    public class RoosterAdapter extends BaseAdapter {
        private static final int TYPE_CEL = 1;
        private static final int TYPE_KOP = 0;
        private static final int TYPE_MAX_COUNT = 2;
        private LayoutInflater mInflater;

        public RoosterAdapter(Context context, int today) {
            this.mInflater = (LayoutInflater) EditRoosterWeek.this.getSystemService("layout_inflater");
        }

        public int getViewTypeCount() {
            return TYPE_MAX_COUNT;
        }

        public int getItemViewType(int position) {
            if (position < EditRoosterWeek.this.iNumColumns) {
                return TYPE_KOP;
            }
            return TYPE_CEL;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            int type = getItemViewType(position);
            if (v == null) {
                switch (type) {
                    case TYPE_KOP /*0*/:
                        v = this.mInflater.inflate((int) R.layout.editroosterweekkop, (ViewGroup) null);
                        break;
                    case TYPE_CEL /*1*/:
                        v = this.mInflater.inflate((int) R.layout.editroosterweekcel, (ViewGroup) null);
                        break;
                }
            }
            if (position < EditRoosterWeek.this.iNumColumns) {
                TextView tvDag = (TextView) v.findViewById(R.id.dag);
                if (EditRoosterWeek.this.iPrefTimetableType == TYPE_MAX_COUNT) {
                    if (tvDag != null) {
                        Object[] objArr = new Object[TYPE_CEL];
                        objArr[TYPE_KOP] = Integer.valueOf(EditRoosterWeek.this.roosterData[position][TYPE_KOP].dow);
                        tvDag.setText(String.format("%d", objArr));
                        tvDag.setTextColor(-1);
                    }
                } else if (tvDag != null) {
                    switch (EditRoosterWeek.this.roosterData[position][TYPE_KOP].dow) {
                        case TYPE_CEL /*1*/:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_zo));
                            break;
                        case TYPE_MAX_COUNT /*2*/:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_ma));
                            break;
                        case 3:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_di));
                            break;
                        case 4:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_wo));
                            break;
                        case 5:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_do));
                            break;
                        case 6:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_vr));
                            break;
                        case 7:
                            tvDag.setText(EditRoosterWeek.this.getString(R.string.rooster_za));
                            break;
                    }
                    tvDag.setTextColor(-1);
                }
            } else {
                VakLokaal item = (VakLokaal) getItem(position);
                LinearLayout cel = (LinearLayout) v.findViewById(R.id.rlcel);
                TextView tvBegin = (TextView) v.findViewById(R.id.begin);
                TextView tvEinde = (TextView) v.findViewById(R.id.einde);
                TextView tvVak = (TextView) v.findViewById(R.id.vak);
                TextView tvLokaal = (TextView) v.findViewById(R.id.lokaal);
                if (cel != null) {
                    GradientDrawable shape = (GradientDrawable) EditRoosterWeek.this.getResources().getDrawable(R.drawable.roundedgrid);
                    shape.setColor(item.kleur);
                    cel.setBackgroundDrawable(shape);
                }
                if (tvVak != null) {
                    tvVak.setText(item.vak);
                }
                if (tvLokaal != null) {
                    tvLokaal.setText(item.lok);
                }
                if (item.les) {
                    if (tvBegin != null) {
                        tvBegin.setText(HwUtl.Time2String(item.begin));
                    }
                    if (tvEinde != null) {
                        tvEinde.setText(HwUtl.Time2String(item.einde));
                    }
                } else {
                    if (tvBegin != null) {
                        tvBegin.setText("");
                    }
                    if (tvEinde != null) {
                        tvEinde.setText("");
                    }
                }
            }
            return v;
        }

        public int getCount() {
            return EditRoosterWeek.this.iNumColumns * (EditRoosterWeek.this.iPrefNumHours + TYPE_CEL);
        }

        public Object getItem(int position) {
            if (position < EditRoosterWeek.this.iNumColumns) {
                return null;
            }
            int idag = position % EditRoosterWeek.this.iNumColumns;
            return EditRoosterWeek.this.roosterData[idag][(position / EditRoosterWeek.this.iNumColumns) - TYPE_CEL];
        }

        public long getItemId(int position) {
            return (long) position;
        }
    }

    public void onResume() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.iPrefNumHours = prefs.getInt("HW_PREF_NUMHOURS", 10);
        this.iPrefTimetableType = prefs.getInt("HW_PREF_TIMETABLETYPE", 0);
        this.iPrefNumDaysRotating = prefs.getInt("HW_PREF_NUMDAYSROTATING", 8);
        FindFirstDateOfWeek();
        FillRooster();
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    public boolean onTouch(View arg0, MotionEvent arg1) {
        switch (arg1.getAction()) {
            case 0:
                this.downXValue = arg1.getX();
                return false;
            case 1:
                float currentX = arg1.getX();
                float y = arg1.getY();
                long eventTime = arg1.getEventTime();
                if (this.downXValue < currentX && currentX - this.downXValue > 100.0f) {
                    PreviousWeek();
                    return false;
                } else if (this.downXValue <= currentX || this.downXValue - currentX <= 100.0f) {
                    return false;
                } else {
                    NextWeek();
                    return false;
                }
            default:
                return false;
        }
    }

    public void NextWeek() {
        if (this.iPrefTimetableType == 1 && this.iWeek == 1) {
            this.iWeek = 2;
            FillRooster();
            this.mGrid.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_to_left));
        }
    }

    public void PreviousWeek() {
        if (this.iPrefTimetableType == 1 && this.iWeek == 2) {
            this.iWeek = 1;
            FillRooster();
            this.mGrid.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
        }
    }
}
