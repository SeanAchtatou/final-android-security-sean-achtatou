package klwinkel.huiswerk;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class Vakantie extends Activity {
    private static Button btnCancel;
    private static Button btnDatumBegin;
    private static Button btnDatumEinde;
    private static Button btnDelete;
    private static Button btnUpdate;
    /* access modifiers changed from: private */
    public static CheckBox chkTotEnMet;
    /* access modifiers changed from: private */
    public static HuiswerkDatabase huiswerkDb;
    private static Calendar mCal = null;
    /* access modifiers changed from: private */
    public static Integer mDatumBegin = 0;
    /* access modifiers changed from: private */
    public static Integer mDatumEinde = 0;
    /* access modifiers changed from: private */
    public static Context myContext;
    private static ScrollView svMain;
    /* access modifiers changed from: private */
    public static EditText txtNaam;
    private final View.OnClickListener btnCancelOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Vakantie.this.finish();
        }
    };
    private final View.OnClickListener btnDeleteOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Vakantie.huiswerkDb.deleteVakantie((long) Vakantie.mDatumBegin.intValue());
            VakantieDagen.LaadVakantieDagen(Vakantie.huiswerkDb);
            HuisWerkMain.UpdateWidgets(Vakantie.myContext);
            Vakantie.this.finish();
        }
    };
    private final View.OnClickListener btnUpdateOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (!Vakantie.chkTotEnMet.isChecked()) {
                Vakantie.mDatumEinde = 0;
            }
            if (Vakantie.mDatumEinde.compareTo(Vakantie.mDatumBegin) == 0) {
                Vakantie.mDatumEinde = 0;
            }
            HuiswerkDatabase.VakantieDatumCursor vakantieCursor = Vakantie.huiswerkDb.getVakantieDatum((long) Vakantie.mDatumBegin.intValue());
            if (vakantieCursor.getCount() > 0) {
                Vakantie.huiswerkDb.editVakantie((long) Vakantie.mDatumBegin.intValue(), (long) Vakantie.mDatumEinde.intValue(), Vakantie.txtNaam.getText().toString());
            } else {
                Vakantie.huiswerkDb.addVakantie((long) Vakantie.mDatumBegin.intValue(), (long) Vakantie.mDatumEinde.intValue(), Vakantie.txtNaam.getText().toString());
            }
            vakantieCursor.close();
            VakantieDagen.LaadVakantieDagen(Vakantie.huiswerkDb);
            HuisWerkMain.UpdateWidgets(Vakantie.myContext);
            Vakantie.this.finish();
        }
    };
    private final View.OnClickListener datumBeginOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new DatePickerDialog(Vakantie.this, Vakantie.this.mDateBeginSetListener, Vakantie.mDatumBegin.intValue() / 10000, (Vakantie.mDatumBegin.intValue() % 10000) / 100, Vakantie.mDatumBegin.intValue() % 100).show();
        }
    };
    private final View.OnClickListener datumEindeOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (Vakantie.mDatumEinde.intValue() == 0) {
                Vakantie.mDatumEinde = Vakantie.mDatumBegin;
            }
            new DatePickerDialog(Vakantie.this, Vakantie.this.mDateEindeSetListener, Vakantie.mDatumEinde.intValue() / 10000, (Vakantie.mDatumEinde.intValue() % 10000) / 100, Vakantie.mDatumEinde.intValue() % 100).show();
        }
    };
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener mDateBeginSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Vakantie.mDatumBegin = Integer.valueOf((year * 10000) + (monthOfYear * 100) + dayOfMonth);
            String str = (String) DateFormat.format("EEEE dd MMMM yyyy", new Date(year - 1900, monthOfYear, dayOfMonth));
            Vakantie.this.ShowDatumBegin();
        }
    };
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener mDateEindeSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Vakantie.mDatumEinde = Integer.valueOf((year * 10000) + (monthOfYear * 100) + dayOfMonth);
            String str = (String) DateFormat.format("EEEE dd MMMM yyyy", new Date(year - 1900, monthOfYear, dayOfMonth));
            Vakantie.this.ShowDatumEinde();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.vakantie);
        myContext = this;
        huiswerkDb = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        mCal = Calendar.getInstance();
        svMain = (ScrollView) findViewById(R.id.svMain);
        btnDatumBegin = (Button) findViewById(R.id.btnDatumBegin);
        btnDatumEinde = (Button) findViewById(R.id.btnDatumEinde);
        chkTotEnMet = (CheckBox) findViewById(R.id.chkTotEnMet);
        txtNaam = (EditText) findViewById(R.id.txtNaam);
        btnUpdate = (Button) findViewById(R.id.btn1);
        btnDelete = (Button) findViewById(R.id.btn2);
        btnCancel = (Button) findViewById(R.id.btn3);
        btnUpdate.setText(getString(R.string.opslaan));
        btnDelete.setText(getString(R.string.verwijderen));
        btnCancel.setText(getString(R.string.annuleren));
        btnUpdate.setOnClickListener(this.btnUpdateOnClick);
        btnDelete.setOnClickListener(this.btnDeleteOnClick);
        btnCancel.setOnClickListener(this.btnCancelOnClick);
        btnDatumBegin.setOnClickListener(this.datumBeginOnClick);
        btnDatumEinde.setOnClickListener(this.datumEindeOnClick);
        chkTotEnMet.setOnClickListener(new chkTotEnMetOnClick());
        if (savedInstanceState == null) {
            Bundle bIn = getIntent().getExtras();
            mDatumBegin = 0;
            mDatumEinde = 0;
            if (bIn != null) {
                mDatumBegin = Integer.valueOf(bIn.getInt("_begin"));
            }
            HuiswerkDatabase.VakantieDatumCursor vakantieCursor = huiswerkDb.getVakantieDatum((long) mDatumBegin.intValue());
            if (vakantieCursor.getCount() > 0) {
                mDatumEinde = Integer.valueOf(vakantieCursor.getColDatumEinde());
                txtNaam.setText(vakantieCursor.getColNaam());
                Log.e("--------------HUISWERK", String.format("%d  %d", mDatumBegin, mDatumEinde));
                if (mDatumEinde.intValue() == 0 || mDatumEinde.compareTo(mDatumBegin) == 0) {
                    chkTotEnMet.setChecked(false);
                } else {
                    chkTotEnMet.setChecked(true);
                }
            } else {
                mCal = Calendar.getInstance();
                int cyear = mCal.get(1);
                int cmonth = mCal.get(2);
                mDatumBegin = Integer.valueOf((cyear * 10000) + (cmonth * 100) + mCal.get(5));
                mDatumEinde = 0;
                txtNaam.setText("");
                chkTotEnMet.setChecked(false);
            }
            vakantieCursor.close();
        }
        ShowDatumBegin();
        ShowDatumEinde();
    }

    public class chkTotEnMetOnClick implements View.OnClickListener {
        public chkTotEnMetOnClick() {
        }

        public void onClick(View v) {
            Vakantie.this.ShowDatumEinde();
        }
    }

    /* access modifiers changed from: private */
    public void ShowDatumBegin() {
        int cyear = mDatumBegin.intValue() / 10000;
        int cmonth = (mDatumBegin.intValue() % 10000) / 100;
        int cday = mDatumBegin.intValue() % 100;
        btnDatumBegin.setText((String) DateFormat.format("EEEE dd MMMM yyyy", new Date(cyear - 1900, cmonth, cday)));
    }

    /* access modifiers changed from: private */
    public void ShowDatumEinde() {
        if (chkTotEnMet.isChecked()) {
            btnDatumEinde.setEnabled(true);
        } else {
            mDatumEinde = 0;
            btnDatumEinde.setEnabled(false);
        }
        if (mDatumEinde.intValue() != 0) {
            int cyear = mDatumEinde.intValue() / 10000;
            int cmonth = (mDatumEinde.intValue() % 10000) / 100;
            int cday = mDatumEinde.intValue() % 100;
            btnDatumEinde.setText((String) DateFormat.format("EEEE dd MMMM yyyy", new Date(cyear - 1900, cmonth, cday)));
            return;
        }
        btnDatumEinde.setText("               ");
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }
}
