package klwinkel.huiswerk;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TimePicker;
import java.lang.reflect.Array;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditUren extends Activity {
    private static Button btnBeginTijd1;
    private static Button btnBeginTijd10;
    private static Button btnBeginTijd11;
    private static Button btnBeginTijd12;
    private static Button btnBeginTijd13;
    private static Button btnBeginTijd14;
    private static Button btnBeginTijd15;
    private static Button btnBeginTijd16;
    private static Button btnBeginTijd17;
    private static Button btnBeginTijd18;
    private static Button btnBeginTijd19;
    private static Button btnBeginTijd2;
    private static Button btnBeginTijd20;
    private static Button btnBeginTijd3;
    private static Button btnBeginTijd4;
    private static Button btnBeginTijd5;
    private static Button btnBeginTijd6;
    private static Button btnBeginTijd7;
    private static Button btnBeginTijd8;
    private static Button btnBeginTijd9;
    private static Button btnEindTijd1;
    private static Button btnEindTijd10;
    private static Button btnEindTijd11;
    private static Button btnEindTijd12;
    private static Button btnEindTijd13;
    private static Button btnEindTijd14;
    private static Button btnEindTijd15;
    private static Button btnEindTijd16;
    private static Button btnEindTijd17;
    private static Button btnEindTijd18;
    private static Button btnEindTijd19;
    private static Button btnEindTijd2;
    private static Button btnEindTijd20;
    private static Button btnEindTijd3;
    private static Button btnEindTijd4;
    private static Button btnEindTijd5;
    private static Button btnEindTijd6;
    private static Button btnEindTijd7;
    private static Button btnEindTijd8;
    private static Button btnEindTijd9;
    private static Button[][] buttons;
    private static Integer iPrefNumHours = 0;
    private static LinearLayout[][] linLayout;
    private static LinearLayout ll1;
    private static LinearLayout ll10;
    private static LinearLayout ll11;
    private static LinearLayout ll12;
    private static LinearLayout ll13;
    private static LinearLayout ll14;
    private static LinearLayout ll15;
    private static LinearLayout ll16;
    private static LinearLayout ll17;
    private static LinearLayout ll18;
    private static LinearLayout ll19;
    private static LinearLayout ll2;
    private static LinearLayout ll20;
    private static LinearLayout ll3;
    private static LinearLayout ll4;
    private static LinearLayout ll5;
    private static LinearLayout ll6;
    private static LinearLayout ll7;
    private static LinearLayout ll8;
    private static LinearLayout ll9;
    /* access modifiers changed from: private */
    public static Button mSelectedButton;
    private static ScrollView svUren;
    /* access modifiers changed from: private */
    public HuiswerkDatabase huiswerkDb;
    /* access modifiers changed from: private */
    public TimePickerDialog.OnTimeSetListener mTijdSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hour, int minute) {
            EditUren.mSelectedButton.setText(HwUtl.Time2String(hour, minute));
            int iId = EditUren.mSelectedButton.getId();
            int iLesUur = iId % 100;
            HuiswerkDatabase.LesUurCursor uC = EditUren.this.huiswerkDb.getLesUur((long) iLesUur);
            int iStart = uC.getColStart();
            int iStop = uC.getColStop();
            if (iId < 100) {
                iStart = (hour * 100) + minute;
            } else {
                iStop = (hour * 100) + minute;
            }
            uC.close();
            EditUren.this.huiswerkDb.editLesUren((long) iLesUur, (long) iStart, (long) iStop);
        }
    };
    private final View.OnClickListener tijdOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            int iHour;
            int iMinute;
            EditUren.mSelectedButton = (Button) v;
            int iId = EditUren.mSelectedButton.getId();
            int iLesUur = iId % 100;
            if (iId < 100) {
                iHour = EditUren.this.huiswerkDb.getLesUur((long) iLesUur).getColStart() / 100;
                iMinute = EditUren.this.huiswerkDb.getLesUur((long) iLesUur).getColStart() % 100;
            } else {
                iHour = EditUren.this.huiswerkDb.getLesUur((long) iLesUur).getColStop() / 100;
                iMinute = EditUren.this.huiswerkDb.getLesUur((long) iLesUur).getColStop() % 100;
            }
            new TimePickerDialog(EditUren.this, EditUren.this.mTijdSetListener, iHour, iMinute, DateFormat.is24HourFormat(EditUren.this.getApplicationContext())).show();
        }
    };

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.edituren);
        this.huiswerkDb = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        svUren = (ScrollView) findViewById(R.id.svUren);
        btnBeginTijd1 = (Button) findViewById(R.id.btnBeginTijd1);
        btnEindTijd1 = (Button) findViewById(R.id.btnEindTijd1);
        btnBeginTijd2 = (Button) findViewById(R.id.btnBeginTijd2);
        btnEindTijd2 = (Button) findViewById(R.id.btnEindTijd2);
        btnBeginTijd3 = (Button) findViewById(R.id.btnBeginTijd3);
        btnEindTijd3 = (Button) findViewById(R.id.btnEindTijd3);
        btnBeginTijd4 = (Button) findViewById(R.id.btnBeginTijd4);
        btnEindTijd4 = (Button) findViewById(R.id.btnEindTijd4);
        btnBeginTijd5 = (Button) findViewById(R.id.btnBeginTijd5);
        btnEindTijd5 = (Button) findViewById(R.id.btnEindTijd5);
        btnBeginTijd6 = (Button) findViewById(R.id.btnBeginTijd6);
        btnEindTijd6 = (Button) findViewById(R.id.btnEindTijd6);
        btnBeginTijd7 = (Button) findViewById(R.id.btnBeginTijd7);
        btnEindTijd7 = (Button) findViewById(R.id.btnEindTijd7);
        btnBeginTijd8 = (Button) findViewById(R.id.btnBeginTijd8);
        btnEindTijd8 = (Button) findViewById(R.id.btnEindTijd8);
        btnBeginTijd9 = (Button) findViewById(R.id.btnBeginTijd9);
        btnEindTijd9 = (Button) findViewById(R.id.btnEindTijd9);
        btnBeginTijd10 = (Button) findViewById(R.id.btnBeginTijd10);
        btnEindTijd10 = (Button) findViewById(R.id.btnEindTijd10);
        btnBeginTijd11 = (Button) findViewById(R.id.btnBeginTijd11);
        btnEindTijd11 = (Button) findViewById(R.id.btnEindTijd11);
        btnBeginTijd12 = (Button) findViewById(R.id.btnBeginTijd12);
        btnEindTijd12 = (Button) findViewById(R.id.btnEindTijd12);
        btnBeginTijd13 = (Button) findViewById(R.id.btnBeginTijd13);
        btnEindTijd13 = (Button) findViewById(R.id.btnEindTijd13);
        btnBeginTijd14 = (Button) findViewById(R.id.btnBeginTijd14);
        btnEindTijd14 = (Button) findViewById(R.id.btnEindTijd14);
        btnBeginTijd15 = (Button) findViewById(R.id.btnBeginTijd15);
        btnEindTijd15 = (Button) findViewById(R.id.btnEindTijd15);
        btnBeginTijd16 = (Button) findViewById(R.id.btnBeginTijd16);
        btnEindTijd16 = (Button) findViewById(R.id.btnEindTijd16);
        btnBeginTijd17 = (Button) findViewById(R.id.btnBeginTijd17);
        btnEindTijd17 = (Button) findViewById(R.id.btnEindTijd17);
        btnBeginTijd18 = (Button) findViewById(R.id.btnBeginTijd18);
        btnEindTijd18 = (Button) findViewById(R.id.btnEindTijd18);
        btnBeginTijd19 = (Button) findViewById(R.id.btnBeginTijd19);
        btnEindTijd19 = (Button) findViewById(R.id.btnEindTijd19);
        btnBeginTijd20 = (Button) findViewById(R.id.btnBeginTijd20);
        btnEindTijd20 = (Button) findViewById(R.id.btnEindTijd20);
        buttons = (Button[][]) Array.newInstance(Button.class, 21, 2);
        buttons[1][0] = btnBeginTijd1;
        buttons[1][1] = btnEindTijd1;
        buttons[2][0] = btnBeginTijd2;
        buttons[2][1] = btnEindTijd2;
        buttons[3][0] = btnBeginTijd3;
        buttons[3][1] = btnEindTijd3;
        buttons[4][0] = btnBeginTijd4;
        buttons[4][1] = btnEindTijd4;
        buttons[5][0] = btnBeginTijd5;
        buttons[5][1] = btnEindTijd5;
        buttons[6][0] = btnBeginTijd6;
        buttons[6][1] = btnEindTijd6;
        buttons[7][0] = btnBeginTijd7;
        buttons[7][1] = btnEindTijd7;
        buttons[8][0] = btnBeginTijd8;
        buttons[8][1] = btnEindTijd8;
        buttons[9][0] = btnBeginTijd9;
        buttons[9][1] = btnEindTijd9;
        buttons[10][0] = btnBeginTijd10;
        buttons[10][1] = btnEindTijd10;
        buttons[11][0] = btnBeginTijd11;
        buttons[11][1] = btnEindTijd11;
        buttons[12][0] = btnBeginTijd12;
        buttons[12][1] = btnEindTijd12;
        buttons[13][0] = btnBeginTijd13;
        buttons[13][1] = btnEindTijd13;
        buttons[14][0] = btnBeginTijd14;
        buttons[14][1] = btnEindTijd14;
        buttons[15][0] = btnBeginTijd15;
        buttons[15][1] = btnEindTijd15;
        buttons[16][0] = btnBeginTijd16;
        buttons[16][1] = btnEindTijd16;
        buttons[17][0] = btnBeginTijd17;
        buttons[17][1] = btnEindTijd17;
        buttons[18][0] = btnBeginTijd18;
        buttons[18][1] = btnEindTijd18;
        buttons[19][0] = btnBeginTijd19;
        buttons[19][1] = btnEindTijd19;
        buttons[20][0] = btnBeginTijd20;
        buttons[20][1] = btnEindTijd20;
        ll1 = (LinearLayout) findViewById(R.id.ll1);
        ll2 = (LinearLayout) findViewById(R.id.ll2);
        ll3 = (LinearLayout) findViewById(R.id.ll3);
        ll4 = (LinearLayout) findViewById(R.id.ll4);
        ll5 = (LinearLayout) findViewById(R.id.ll5);
        ll6 = (LinearLayout) findViewById(R.id.ll6);
        ll7 = (LinearLayout) findViewById(R.id.ll7);
        ll8 = (LinearLayout) findViewById(R.id.ll8);
        ll9 = (LinearLayout) findViewById(R.id.ll9);
        ll10 = (LinearLayout) findViewById(R.id.ll10);
        ll11 = (LinearLayout) findViewById(R.id.ll11);
        ll12 = (LinearLayout) findViewById(R.id.ll12);
        ll13 = (LinearLayout) findViewById(R.id.ll13);
        ll14 = (LinearLayout) findViewById(R.id.ll14);
        ll15 = (LinearLayout) findViewById(R.id.ll15);
        ll16 = (LinearLayout) findViewById(R.id.ll16);
        ll17 = (LinearLayout) findViewById(R.id.ll17);
        ll18 = (LinearLayout) findViewById(R.id.ll18);
        ll19 = (LinearLayout) findViewById(R.id.ll19);
        ll20 = (LinearLayout) findViewById(R.id.ll20);
        linLayout = (LinearLayout[][]) Array.newInstance(LinearLayout.class, 21, 1);
        linLayout[1][0] = ll1;
        linLayout[2][0] = ll2;
        linLayout[3][0] = ll3;
        linLayout[4][0] = ll4;
        linLayout[5][0] = ll5;
        linLayout[6][0] = ll6;
        linLayout[7][0] = ll7;
        linLayout[8][0] = ll8;
        linLayout[9][0] = ll9;
        linLayout[10][0] = ll10;
        linLayout[11][0] = ll11;
        linLayout[12][0] = ll12;
        linLayout[13][0] = ll13;
        linLayout[14][0] = ll14;
        linLayout[15][0] = ll15;
        linLayout[16][0] = ll16;
        linLayout[17][0] = ll17;
        linLayout[18][0] = ll18;
        linLayout[19][0] = ll19;
        linLayout[20][0] = ll20;
        iPrefNumHours = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_NUMHOURS", 10));
        int i = 1;
        while (i <= iPrefNumHours.intValue()) {
            HuiswerkDatabase.LesUurCursor uC = this.huiswerkDb.getLesUur((long) i);
            buttons[i][0].setText(HwUtl.Time2String(uC.getColStart()));
            buttons[i][1].setText(HwUtl.Time2String(uC.getColStop()));
            buttons[i][0].setOnClickListener(this.tijdOnClick);
            buttons[i][1].setOnClickListener(this.tijdOnClick);
            buttons[i][0].setId(i);
            buttons[i][1].setId(i + 100);
            linLayout[i][0].setVisibility(0);
            uC.close();
            i++;
        }
        for (int j = i; j <= 20; j++) {
            linLayout[j][0].setVisibility(8);
        }
    }

    public void onResume() {
        svUren.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.huiswerkDb.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
