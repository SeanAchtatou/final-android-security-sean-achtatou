package klwinkel.huiswerk;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WhatsNew extends Activity {
    WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.webview);
        this.mWebView = (WebView) findViewById(R.id.wv);
        this.mWebView.setInitialScale(1);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.getSettings().setUseWideViewPort(true);
    }

    public void onStart() {
        this.mWebView.loadUrl("file:///android_asset/whatsnew.html");
        super.onStart();
    }
}
