package klwinkel.huiswerk;

import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkListBase extends ExpandableListActivity {
    static final String LOG_TAG = "HuisWerk";
    public Context MainContext = null;
    public Boolean bToetsTab = false;
    public Integer currGroup;
    public HuiswerkDatabase db = null;
    public HuiswerkDatabase.HuiswerkDateCursor groupCursor = null;
    public ImageButton lblNieuw;
    private final View.OnClickListener lblNieuwOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(HuisWerkListBase.this.myContext, EditHuiswerk.class);
            Bundle b = new Bundle();
            b.putBoolean("_toets", HuisWerkListBase.this.bToetsTab.booleanValue());
            i.putExtras(b);
            HuisWerkListBase.this.startActivity(i);
        }
    };
    public ImageButton lblOpruimen;
    private final View.OnClickListener lblOpruimenOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            HuisWerkListBase.this.OpruimenHuiswerk();
        }
    };
    public LinearLayout llMain;
    public Context myContext;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.huiswerk);
        this.myContext = this;
        this.MainContext = getParent();
        this.llMain = (LinearLayout) findViewById(R.id.llMain);
        this.lblNieuw = (ImageButton) findViewById(R.id.lblNieuw);
        this.lblOpruimen = (ImageButton) findViewById(R.id.lblOpruimen);
        this.currGroup = 0;
        this.lblNieuw.setOnClickListener(this.lblNieuwOnClick);
        this.lblOpruimen.setOnClickListener(this.lblOpruimenOnClick);
    }

    public class OnHuiswerkClickListener implements View.OnClickListener {
        private long mHwid = 0;

        public OnHuiswerkClickListener(long hwid) {
            this.mHwid = hwid;
        }

        public void onClick(View v) {
            HuiswerkDatabase.HuiswerkDetailCursor hwC = HuisWerkListBase.this.db.getHuiswerkDetails(this.mHwid);
            if (hwC.getColKlaar() == 0) {
                HuisWerkListBase.this.db.editHuiswerk(this.mHwid, hwC.getColVakId(), hwC.getColHoofdstuk(), hwC.getColPagina(), hwC.getColOmschrijving(), 1, hwC.getColToets(), (long) hwC.getColDatum());
            } else {
                HuisWerkListBase.this.db.editHuiswerk(this.mHwid, hwC.getColVakId(), hwC.getColHoofdstuk(), hwC.getColPagina(), hwC.getColOmschrijving(), 0, hwC.getColToets(), (long) hwC.getColDatum());
            }
            hwC.close();
            ((HuisWerkMain) HuisWerkListBase.this.getParent()).setHuisWerkCount(HuisWerkListBase.this.db.getHuiswerkCountNietAf());
            ((HuisWerkMain) HuisWerkListBase.this.getParent()).setToetsCount(HuisWerkListBase.this.db.getToetsCountNietAf());
            HuisWerkMain.UpdateWidgets(HuisWerkListBase.this.myContext);
            HuisWerkListBase.this.groupCursor.requery();
        }
    }

    public class OnFotoClickListener implements View.OnClickListener {
        private int mHwid = 0;

        public OnFotoClickListener(int hwid) {
            this.mHwid = hwid;
        }

        public void onClick(View v) {
            Intent i = new Intent(HuisWerkListBase.this, HuiswerkFoto.class);
            Bundle b = new Bundle();
            b.putInt("_id", this.mHwid);
            i.putExtras(b);
            HuisWerkListBase.this.startActivity(i);
        }
    }

    public class ExpandableListBaseAdapter extends SimpleCursorTreeAdapter {
        Boolean bFoto = false;
        Boolean bKlaar = false;
        Boolean bToets = false;
        public Context context;
        public int groupDatum;
        int hwId = 0;
        public int iGroupCount;
        int iVakKleur = -1;
        public HuiswerkDatabase.HuiswerkDateCursor localCursor;
        String strHoofdstuk = "";
        String strOmschrijving = "";
        String strPagina = "";
        String strVak = "";

        public ExpandableListBaseAdapter(HuiswerkDatabase.HuiswerkDateCursor cursor, Context context2, int groupLayout, int childLayout, String[] groupFrom, int[] groupTo, String[] childrenFrom, int[] childrenTo) {
            super(context2, cursor, groupLayout, groupFrom, groupTo, childLayout, childrenFrom, childrenTo);
            this.context = context2;
            this.localCursor = cursor;
        }

        /* access modifiers changed from: protected */
        public Cursor getChildrenCursor(Cursor grpCursor) {
            return null;
        }

        public TextView getGenericView() {
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(-1, 90);
            TextView textView = new TextView(HuisWerkListBase.this.myContext);
            textView.setLayoutParams(lp);
            textView.setGravity(19);
            textView.setPadding(50, 0, 0, 0);
            return textView;
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.huiswerklistrow, (ViewGroup) null);
            }
            RelativeLayout row = (RelativeLayout) v.findViewById(R.id.row);
            TextView txtVak = (TextView) v.findViewById(R.id.txtVak);
            TextView lblHoofdstuk = (TextView) v.findViewById(R.id.lblHoofdstuk);
            TextView txtHoofdstuk = (TextView) v.findViewById(R.id.txtHoofdstuk);
            TextView lblPagina = (TextView) v.findViewById(R.id.lblPagina);
            TextView txtPagina = (TextView) v.findViewById(R.id.txtPagina);
            TextView txtOmschrijving = (TextView) v.findViewById(R.id.txtOmschrijving);
            ImageButton imgFoto = (ImageButton) v.findViewById(R.id.fotoindicator);
            CheckBox chkKlaar = (CheckBox) v.findViewById(R.id.donebutton);
            if (row != null) {
                row.setBackgroundColor(this.iVakKleur);
            }
            if (imgFoto != null) {
                imgFoto.setFocusable(false);
                if (this.bFoto.booleanValue()) {
                    imgFoto.setVisibility(0);
                    imgFoto.setImageResource(R.drawable.roostercamera);
                    imgFoto.setOnClickListener(new OnFotoClickListener(this.hwId));
                } else {
                    imgFoto.setVisibility(8);
                }
            }
            if (chkKlaar != null) {
                chkKlaar.setChecked(this.bKlaar.booleanValue());
                chkKlaar.setOnClickListener(new OnHuiswerkClickListener((long) this.hwId));
            }
            if (lblHoofdstuk != null) {
                lblHoofdstuk.setText(HuisWerkListBase.this.getString(R.string.hoofdstuk));
            }
            if (lblPagina != null) {
                lblPagina.setText(HuisWerkListBase.this.getString(R.string.pagina));
            }
            if (txtVak != null) {
                txtVak.setText(this.strVak);
            }
            if (txtHoofdstuk != null) {
                txtHoofdstuk.setText(this.strHoofdstuk);
                if (this.strHoofdstuk.length() == 0) {
                    lblHoofdstuk.setVisibility(8);
                    txtHoofdstuk.setVisibility(8);
                } else {
                    lblHoofdstuk.setVisibility(0);
                    txtHoofdstuk.setVisibility(0);
                }
            }
            if (txtPagina != null) {
                txtPagina.setText(this.strPagina);
                if (this.strPagina.length() == 0) {
                    lblPagina.setVisibility(8);
                    txtPagina.setVisibility(8);
                } else {
                    lblPagina.setVisibility(0);
                    txtPagina.setVisibility(0);
                }
            }
            if (txtOmschrijving != null) {
                txtOmschrijving.setText(this.strOmschrijving);
                if (this.strOmschrijving.length() == 0) {
                    txtOmschrijving.setVisibility(8);
                } else {
                    txtOmschrijving.setVisibility(0);
                }
            }
            if (this.bKlaar.booleanValue()) {
                lblPagina.setVisibility(8);
                txtPagina.setVisibility(8);
                lblHoofdstuk.setVisibility(8);
                txtHoofdstuk.setVisibility(8);
                txtOmschrijving.setVisibility(8);
                row.setBackgroundColor(-12303292);
                imgFoto.setVisibility(8);
            }
            return v;
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.huiswerklistgroup, (ViewGroup) null);
            }
            TextView textView = (TextView) v.findViewById(R.id.group);
            TextView counterView = (TextView) v.findViewById(R.id.countertext);
            ImageView icon = (ImageView) v.findViewById(R.id.rowicon);
            if (icon != null) {
                if (this.bToets.booleanValue()) {
                    icon.setImageResource(R.drawable.toets);
                } else {
                    icon.setImageResource(R.drawable.huiswerk);
                }
            }
            textView.setText((String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(this.groupDatum / 10000).intValue() - 1900, Integer.valueOf((this.groupDatum % 10000) / 100).intValue(), Integer.valueOf(this.groupDatum % 100).intValue())));
            counterView.setText("(" + this.iGroupCount + ")");
            if (this.iGroupCount == 0) {
                counterView.setTextColor(-16777216);
            } else {
                counterView.setTextColor(-65536);
            }
            return v;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ((HuisWerkMain) getParent()).ShowMainMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onStop() {
        super.onStop();
    }

    public void onStart() {
        super.onStart();
    }

    public void onResume() {
        this.llMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        this.groupCursor.requery();
        for (int i = 0; i < getExpandableListView().getCount(); i++) {
            getExpandableListView().expandGroup(i);
        }
        ((HuisWerkMain) getParent()).setHuisWerkCount(this.db.getHuiswerkCountNietAf());
        ((HuisWerkMain) getParent()).setToetsCount(this.db.getToetsCountNietAf());
        super.onResume();
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent i = new Intent(this.myContext, EditHuiswerk.class);
        Bundle b = new Bundle();
        b.putInt("_id", (int) id);
        i.putExtras(b);
        this.currGroup = Integer.valueOf(groupPosition);
        startActivity(i);
        return super.onChildClick(parent, v, groupPosition, childPosition, id);
    }

    public void OpruimenHuiswerk() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case -1:
                        Calendar cal = Calendar.getInstance();
                        int i = (cal.get(1) * 10000) + (cal.get(2) * 100) + cal.get(5);
                        HuiswerkDatabase.HuiswerkCursorCleanup CleanCursor = HuisWerkListBase.this.db.getHuiswerkCleanup(HuiswerkDatabase.HuiswerkCursor.SortBy.datum);
                        if (CleanCursor.getCount() > 0) {
                            CleanCursor.moveToFirst();
                            while (!CleanCursor.isAfterLast()) {
                                boolean bVakFound = true;
                                HuiswerkDatabase.VakDetailCursor vdc = HuisWerkListBase.this.db.getVakDetails(CleanCursor.getColVakId());
                                if (vdc.getCount() == 0) {
                                    bVakFound = false;
                                }
                                vdc.close();
                                if (CleanCursor.getColKlaar() > 0 || !bVakFound) {
                                    long hwId = CleanCursor.getColHuiswerkId();
                                    HuisWerkListBase.this.db.deleteHuiswerk(hwId);
                                    HuiswerkDatabase.HuiswerkFotoCursor chwf = HuisWerkListBase.this.db.getHuiswerkFoto(hwId);
                                    if (chwf.getCount() > 0) {
                                        File file = new File(chwf.getColFoto());
                                        if (file.exists()) {
                                            file.delete();
                                        }
                                        HuisWerkListBase.this.db.deleteHuiswerkFoto(hwId);
                                    }
                                    chwf.close();
                                    CleanCursor.requery();
                                    CleanCursor.moveToFirst();
                                } else {
                                    CleanCursor.moveToNext();
                                }
                            }
                        }
                        CleanCursor.close();
                        HuisWerkListBase.this.groupCursor.requery();
                        for (int i2 = 0; i2 < HuisWerkListBase.this.getExpandableListView().getCount(); i2++) {
                            HuisWerkListBase.this.getExpandableListView().expandGroup(i2);
                        }
                        HuisWerkMain.UpdateWidgets(HuisWerkListBase.this.myContext);
                        return;
                    default:
                        return;
                }
            }
        };
        new AlertDialog.Builder(this).setMessage(getString(R.string.opruimenhuiswerk)).setPositiveButton(getString(R.string.ja), dialogClickListener).setNegativeButton(getString(R.string.nee), dialogClickListener).show();
    }
}
