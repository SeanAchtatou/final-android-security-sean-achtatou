package klwinkel.huiswerk;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

public class HuiswerkDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "huiswerk";
    private static String DATABASE_PATH = "/data/data/klwinkel.huiswerk/databases/";
    private static final int DATABASE_VERSION = 2;
    private static int mFirstSchoolDayOfWeek = 1;
    private static boolean mInit = false;
    private final Context mContext;

    public HuiswerkDatabase(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) DATABASE_VERSION);
        this.mContext = context;
        if (!mInit) {
            FindFirstSchoolDayOfWeek(context);
            mInit = true;
        }
    }

    public HuiswerkDatabase(Context context, boolean bInit) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) DATABASE_VERSION);
        this.mContext = context;
    }

    public void Init(Context context) {
        if (!mInit) {
            FindFirstSchoolDayOfWeek(context);
            mInit = true;
        }
    }

    public long GetRoosterDagLong(Context ctxt, Calendar cal) {
        long iDayofWeek = (long) cal.get(7);
        Calendar calWeek = (Calendar) cal.clone();
        while (calWeek.get(7) != mFirstSchoolDayOfWeek) {
            calWeek.add(5, -1);
        }
        int iWeekNr = calWeek.get(3);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctxt);
        int iPrefTimetableType = prefs.getInt("HW_PREF_TIMETABLETYPE", 0);
        int iVolgorde = prefs.getInt("HW_PREF_VOLGORDE", 0);
        int iRotateFirstDate = prefs.getInt("HW_PREF_ROTATEFIRSTDATE", 0);
        int iPrefNumDaysRotating = prefs.getInt("HW_PREF_NUMDAYSROTATING", 8);
        if (iPrefTimetableType == 1) {
            if (iVolgorde == 0) {
                if (iWeekNr % DATABASE_VERSION == 0) {
                    iDayofWeek += 7;
                }
            } else if (iWeekNr % DATABASE_VERSION != 0) {
                iDayofWeek += 7;
            }
        }
        if (iPrefTimetableType != DATABASE_VERSION) {
            return iDayofWeek;
        }
        long iWeekDay = (long) cal.get(7);
        Calendar calRotateFirst = Calendar.getInstance();
        if (iRotateFirstDate > 0) {
            calRotateFirst.set(1, iRotateFirstDate / 10000);
            calRotateFirst.set(DATABASE_VERSION, (iRotateFirstDate % 10000) / 100);
            calRotateFirst.set(5, iRotateFirstDate % 100);
        }
        long iDaysTotal = ((long) cal.get(6)) - ((long) (calRotateFirst.get(6) - 1));
        long iDaynr = ((((iDaysTotal / 7) * 5) + (iDaysTotal % 7)) - ((long) VakantieDagen.GemisteDagenRoterend(cal, iRotateFirstDate))) % ((long) iPrefNumDaysRotating);
        if (iDaynr == 0) {
            iDaynr = (long) iPrefNumDaysRotating;
        }
        if (iWeekDay == 7 || iWeekDay == 1) {
            return 0;
        }
        return iDaynr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if (r0 != false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = klwinkel.huiswerk.HuiswerkDatabase.DATABASE_VERSION;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003f, code lost:
        r3 = getRoosterNu((long) klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r3.getCount() <= 0) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        r0 = true;
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        r3.close();
        klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek <= 7) goto L_0x003f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int FindFirstSchoolDayOfWeek(android.content.Context r8) {
        /*
            r7 = this;
            r6 = 7
            r4 = 1
            klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = r4
            r0 = 0
            android.content.SharedPreferences r2 = android.preference.PreferenceManager.getDefaultSharedPreferences(r8)
            java.lang.String r4 = "HW_PREF_FSDOW"
            r5 = 0
            int r1 = r2.getInt(r4, r5)
            if (r1 == 0) goto L_0x003a
            klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = r1
        L_0x0014:
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            return r4
        L_0x0017:
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            long r4 = (long) r4
            klwinkel.huiswerk.HuiswerkDatabase$RoosterCursorNu r3 = r7.getRoosterNu(r4)
            int r4 = r3.getCount()
            if (r4 != 0) goto L_0x0031
            r3.close()
        L_0x0027:
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            if (r4 <= r6) goto L_0x003f
        L_0x002b:
            if (r0 != 0) goto L_0x0014
            r4 = 2
            klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = r4
            goto L_0x0014
        L_0x0031:
            r3.close()
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            int r4 = r4 + 1
            klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = r4
        L_0x003a:
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            if (r4 <= r6) goto L_0x0017
            goto L_0x0027
        L_0x003f:
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            long r4 = (long) r4
            klwinkel.huiswerk.HuiswerkDatabase$RoosterCursorNu r3 = r7.getRoosterNu(r4)
            int r4 = r3.getCount()
            if (r4 <= 0) goto L_0x0051
            r0 = 1
            r3.close()
            goto L_0x002b
        L_0x0051:
            r3.close()
            int r4 = klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek
            int r4 = r4 + 1
            klwinkel.huiswerk.HuiswerkDatabase.mFirstSchoolDayOfWeek = r4
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: klwinkel.huiswerk.HuiswerkDatabase.FindFirstSchoolDayOfWeek(android.content.Context):int");
    }

    public VakantieDatumCursor getVakantieDatum(long datumBegin) {
        VakantieDatumCursor c = (VakantieDatumCursor) getReadableDatabase().rawQueryWithFactory(new VakantieDatumCursor.Factory(null), "SELECT datumbegin, datumeinde, naam FROM vakantie WHERE datumbegin = " + datumBegin, null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakantieDatumCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT datumbegin, datumeinde, naam FROM vakantie WHERE datumbegin = ";

        /* synthetic */ VakantieDatumCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakantieDatumCursor vakantieDatumCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakantieDatumCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakantieDatumCursor(db, driver, editTable, query, null);
            }
        }

        public int getColDatumBegin() {
            return getInt(getColumnIndexOrThrow("datumbegin"));
        }

        public int getColDatumEinde() {
            return getInt(getColumnIndexOrThrow("datumeinde"));
        }

        public String getColNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }
    }

    public void addVakantie(long datumbegin, long datumeinde, String naam) {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(datumbegin);
        objArr[1] = Long.valueOf(datumeinde);
        objArr[DATABASE_VERSION] = DatabaseUtils.sqlEscapeString(naam);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO vakantie (datumbegin, datumeinde, naam) VALUES ( %d, %d, %s)", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new vakantie", e.toString());
        }
    }

    public void editVakantie(long datumbegin, long datumeinde, String naam) {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(datumeinde);
        objArr[1] = DatabaseUtils.sqlEscapeString(naam);
        objArr[DATABASE_VERSION] = Long.valueOf(datumbegin);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE vakantie SET datumeinde = %d,  naam = %s WHERE datumbegin = %d ", objArr));
        } catch (SQLException e) {
            Log.e("Error updating vakantie", e.toString());
        }
    }

    public void deleteVakantie(long datumbegin) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM vakantie WHERE datumbegin = '%d' ", Long.valueOf(datumbegin)));
        } catch (SQLException e) {
            Log.e("Error deleteing vakantie", e.toString());
        }
    }

    public VakDetailCursor getVakDetails(long vakId) {
        VakDetailCursor c = (VakDetailCursor) getReadableDatabase().rawQueryWithFactory(new VakDetailCursor.Factory(null), "SELECT _id, naam, kort FROM vakken WHERE _id = " + vakId, null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakDetailCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, naam, kort FROM vakken WHERE _id = ";

        /* synthetic */ VakDetailCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakDetailCursor vakDetailCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakDetailCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakDetailCursor(db, driver, editTable, query, null);
            }
        }

        public long getColVakkenId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public String getColNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }
    }

    public VakkenCursor getVakken(VakkenCursor.SortBy sortBy) {
        VakkenCursor c = (VakkenCursor) getReadableDatabase().rawQueryWithFactory(new VakkenCursor.Factory(null), "SELECT _id, naam, kort FROM vakken ORDER BY " + sortBy.toString(), null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakkenCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, naam, kort FROM vakken ORDER BY ";

        public enum SortBy {
            naam
        }

        /* synthetic */ VakkenCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakkenCursor vakkenCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakkenCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakkenCursor(db, driver, editTable, query, null);
            }
        }

        public Integer getColVakkenId() {
            return Integer.valueOf(getInt(getColumnIndexOrThrow("_id")));
        }

        public String getColNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }
    }

    public void addVak(String naam, String kort) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = DatabaseUtils.sqlEscapeString(naam);
        objArr[1] = DatabaseUtils.sqlEscapeString(kort);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO vakken (_id, naam, kort) VALUES (          NULL, %s,  %s)", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new vak", e.toString());
        }
    }

    public void editVak(long vak_id, String naam, String kort) {
        Object[] objArr = new Object[3];
        objArr[0] = DatabaseUtils.sqlEscapeString(naam);
        objArr[1] = DatabaseUtils.sqlEscapeString(kort);
        objArr[DATABASE_VERSION] = Long.valueOf(vak_id);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE vakken SET naam = %s,  kort = %s WHERE _id = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new vak", e.toString());
        }
    }

    public void deleteVak(long vak_id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM vakken WHERE _id = '%d' ", Long.valueOf(vak_id)));
        } catch (SQLException e) {
            Log.e("Error deleteing vak", e.toString());
        }
    }

    public int getVakkenCount() {
        Cursor c = null;
        try {
            c = getReadableDatabase().rawQuery("SELECT count(*) FROM vakken", null);
            if (c.getCount() <= 0) {
                return 0;
            }
            c.moveToFirst();
            int i = c.getInt(0);
            if (c == null) {
                return i;
            }
            try {
                c.close();
                return i;
            } catch (SQLException e) {
                return i;
            }
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e2) {
                }
            }
        }
    }

    public HuiswerkToetsDayCursor getHuiswerkToetsDay(long datum) {
        HuiswerkToetsDayCursor c = (HuiswerkToetsDayCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkToetsDayCursor.Factory(null), "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND toets = 1 AND datum = " + datum, null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkToetsDayCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND toets = 1 AND datum = ";

        /* synthetic */ HuiswerkToetsDayCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkToetsDayCursor huiswerkToetsDayCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkToetsDayCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkToetsDayCursor(db, driver, editTable, query, null);
            }
        }

        public int getColHuiswerkId() {
            return getInt(getColumnIndexOrThrow("_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColHoofdstuk() {
            return getString(getColumnIndexOrThrow("hoofdstuk"));
        }

        public String getColPagina() {
            return getString(getColumnIndexOrThrow("pagina"));
        }

        public String getColOmschrijving() {
            return getString(getColumnIndexOrThrow("omschrijving"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColToets() {
            return getLong(getColumnIndexOrThrow("toets"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }

        public int getColVakId() {
            return getInt(getColumnIndexOrThrow("vak_id"));
        }
    }

    public HuiswerkDayCursor getHuiswerkDay(long datum) {
        HuiswerkDayCursor c = (HuiswerkDayCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkDayCursor.Factory(null), "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND toets = 0 AND datum = " + datum, null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkDayCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND toets = 0 AND datum = ";

        /* synthetic */ HuiswerkDayCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkDayCursor huiswerkDayCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkDayCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkDayCursor(db, driver, editTable, query, null);
            }
        }

        public int getColHuiswerkId() {
            return getInt(getColumnIndexOrThrow("_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColHoofdstuk() {
            return getString(getColumnIndexOrThrow("hoofdstuk"));
        }

        public String getColPagina() {
            return getString(getColumnIndexOrThrow("pagina"));
        }

        public String getColOmschrijving() {
            return getString(getColumnIndexOrThrow("omschrijving"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColToets() {
            return getLong(getColumnIndexOrThrow("toets"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }

        public int getColVakId() {
            return getInt(getColumnIndexOrThrow("vak_id"));
        }
    }

    public HuiswerkDetailCursor getHuiswerkDetails(long huiswerk_id) {
        HuiswerkDetailCursor c = (HuiswerkDetailCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkDetailCursor.Factory(null), "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND huiswerk._id = " + huiswerk_id, null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkDetailCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND huiswerk._id = ";

        /* synthetic */ HuiswerkDetailCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkDetailCursor huiswerkDetailCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkDetailCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkDetailCursor(db, driver, editTable, query, null);
            }
        }

        public long getColHuiswerkId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColHoofdstuk() {
            return getString(getColumnIndexOrThrow("hoofdstuk"));
        }

        public String getColPagina() {
            return getString(getColumnIndexOrThrow("pagina"));
        }

        public String getColOmschrijving() {
            return getString(getColumnIndexOrThrow("omschrijving"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColToets() {
            return getLong(getColumnIndexOrThrow("toets"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public HuiswerkVakCursor getHuiswerkVak(long datum, long vak_id) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = Long.valueOf(datum);
        objArr[1] = Long.valueOf(vak_id);
        HuiswerkVakCursor c = (HuiswerkVakCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkVakCursor.Factory(null), String.format("SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND datum = %d AND vak_id = %d ", objArr), null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkVakCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id AND datum = %d AND vak_id = %d ";

        /* synthetic */ HuiswerkVakCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkVakCursor huiswerkVakCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkVakCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkVakCursor(db, driver, editTable, query, null);
            }
        }

        public long getColHuiswerkId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColHoofdstuk() {
            return getString(getColumnIndexOrThrow("hoofdstuk"));
        }

        public String getColPagina() {
            return getString(getColumnIndexOrThrow("pagina"));
        }

        public String getColOmschrijving() {
            return getString(getColumnIndexOrThrow("omschrijving"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColToets() {
            return getLong(getColumnIndexOrThrow("toets"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public HuiswerkCursorCleanup getHuiswerkCleanup(HuiswerkCursor.SortBy sortBy) {
        HuiswerkCursorCleanup c = (HuiswerkCursorCleanup) getReadableDatabase().rawQueryWithFactory(new HuiswerkCursorCleanup.Factory(null), "SELECT huiswerk._id, vak_id, klaar FROM huiswerk ORDER BY " + sortBy.toString(), null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkCursorCleanup extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, klaar FROM huiswerk ORDER BY ";

        public enum SortBy {
            datum
        }

        /* synthetic */ HuiswerkCursorCleanup(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkCursorCleanup huiswerkCursorCleanup) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkCursorCleanup(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkCursorCleanup(db, driver, editTable, query, null);
            }
        }

        public long getColHuiswerkId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }
    }

    public HuiswerkCursor getHuiswerk(HuiswerkCursor.SortBy sortBy) {
        HuiswerkCursor c = (HuiswerkCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkCursor.Factory(null), "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id ORDER BY " + sortBy.toString(), null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT huiswerk._id, vak_id, naam, kort, hoofdstuk, pagina, omschrijving, klaar, toets, datum FROM huiswerk, vakken WHERE vak_id = vakken._id ORDER BY ";

        public enum SortBy {
            datum
        }

        /* synthetic */ HuiswerkCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkCursor huiswerkCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkCursor(db, driver, editTable, query, null);
            }
        }

        public long getColHuiswerkId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColHoofdstuk() {
            return getString(getColumnIndexOrThrow("hoofdstuk"));
        }

        public String getColPagina() {
            return getString(getColumnIndexOrThrow("pagina"));
        }

        public String getColOmschrijving() {
            return getString(getColumnIndexOrThrow("omschrijving"));
        }

        public long getColKlaar() {
            return getLong(getColumnIndexOrThrow("klaar"));
        }

        public long getColToets() {
            return getLong(getColumnIndexOrThrow("toets"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }

        public int getColVakId() {
            return getInt(getColumnIndexOrThrow("vak_id"));
        }
    }

    public RoosterCursorNu getRoosterNu(long dag) {
        RoosterCursorNu c = (RoosterCursorNu) getReadableDatabase().rawQueryWithFactory(new RoosterCursorNu.Factory(null), String.format("SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND dag = %d AND datum = 0 ORDER BY uur ", Long.valueOf(dag)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterCursorNu extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND dag = %d AND datum = 0 ORDER BY uur ";

        /* synthetic */ RoosterCursorNu(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterCursorNu roosterCursorNu) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterCursorNu(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterCursorNu(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public int getColBegin() {
            return getInt(getColumnIndexOrThrow("begin"));
        }

        public int getColEinde() {
            return getInt(getColumnIndexOrThrow("einde"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public static class RelNotesCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, relcode FROM relnotes ";

        /* synthetic */ RelNotesCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RelNotesCursor relNotesCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RelNotesCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RelNotesCursor(db, driver, editTable, query, null);
            }
        }

        public int getVersion() {
            return getInt(getColumnIndexOrThrow("relcode"));
        }
    }

    public void CreateVakInfoTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE vakinfo (_id INTEGER PRIMARY KEY, kleur INTEGER, leraar TEXT, contact TEXT, lokaal);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating table vakinfo", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public void DeleteVakInfoTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("DROP TABLE IF EXISTS vakinfo");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error deleting table vakinfo", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public void CreateVakInfoNewTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE vakinfonew (_id INTEGER PRIMARY KEY, kleur INTEGER, leraar TEXT, email TEXT, phone TEXT);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating table vakinfonew", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public Boolean CreateLesUrenIfNotExist() {
        boolean bReturn = false;
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE lesuren (uur INTEGER PRIMARY KEY , start INTEGER, stop INTEGER);");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (1, 830, 920 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (2, 920, 1010 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (3, 1030, 1120 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (4, 1120, 1210 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (5, 1240, 1330 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (6, 1330, 1420 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (7, 1430, 1520 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (8, 1520, 1610 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (9, 1610, 1700 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (10, 1700, 1750 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (11, 1750, 1800 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (12, 1800, 1810 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (13, 1810, 1820 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (14, 1820, 1830 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (15, 1830, 1840 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (16, 1840, 1850 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (17, 1850, 1900 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (18, 1900, 1910 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (19, 1910, 1920 );");
            db.execSQL("INSERT INTO lesuren (uur, start, stop) VALUES (20, 1920, 1930 );");
            db.setTransactionSuccessful();
            bReturn = true;
        } catch (SQLException e) {
            Log.e("Lesuren table already exists, no problem", e.toString());
        }
        db.endTransaction();
        return bReturn;
    }

    public int getRelNotesVersionFirstTime() {
        int iVersion = 0;
        SQLiteDatabase d = getReadableDatabase();
        RelNotesCursor c1 = (RelNotesCursor) d.rawQueryWithFactory(new RelNotesCursor.Factory(null), "SELECT _id, relcode FROM relnotes ", null, null);
        int icnt = c1.getCount();
        c1.close();
        if (icnt == 0) {
            try {
                getWritableDatabase().execSQL("INSERT INTO relnotes (_id, relcode) VALUES (1, 0 );");
            } catch (SQLException e) {
                Log.e("Error writing relnotes", e.toString());
            }
        }
        RelNotesCursor c2 = (RelNotesCursor) d.rawQueryWithFactory(new RelNotesCursor.Factory(null), "SELECT _id, relcode FROM relnotes ", null, null);
        if (c2.getCount() == 1) {
            c2.moveToFirst();
            iVersion = c2.getVersion();
        }
        c2.close();
        d.close();
        return iVersion;
    }

    public int getRelNotesVersion() {
        int iVersion = 0;
        RelNotesCursor c = (RelNotesCursor) getReadableDatabase().rawQueryWithFactory(new RelNotesCursor.Factory(null), "SELECT _id, relcode FROM relnotes ", null, null);
        if (c.getCount() == 1) {
            c.moveToFirst();
            iVersion = c.getVersion();
        }
        c.close();
        return iVersion;
    }

    public void setRelNotesVersion(int iRelcode) {
        try {
            getWritableDatabase().execSQL(String.format("UPDATE relnotes SET relcode = '%d' WHERE _id = 1 ", Integer.valueOf(iRelcode)));
        } catch (SQLException e) {
            Log.e("Error writing relnotes", e.toString());
        }
    }

    public static class LesUurCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT uur, start, stop FROM lesuren WHERE uur = %d ";

        /* synthetic */ LesUurCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, LesUurCursor lesUurCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private LesUurCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new LesUurCursor(db, driver, editTable, query, null);
            }
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public int getColStart() {
            return getInt(getColumnIndexOrThrow("start"));
        }

        public int getColStop() {
            return getInt(getColumnIndexOrThrow("stop"));
        }
    }

    public void editLesUren(long uur, long start, long stop) {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(start);
        objArr[1] = Long.valueOf(stop);
        objArr[DATABASE_VERSION] = Long.valueOf(uur);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE lesuren SET start = '%d', stop = '%d' WHERE uur = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new lesuren", e.toString());
        }
    }

    public RoosterCursor getRoosterWijziging(long uur, long datum) {
        Object[] objArr = new Object[3];
        objArr[0] = -1;
        objArr[1] = Long.valueOf(uur);
        objArr[DATABASE_VERSION] = Long.valueOf(datum);
        RoosterCursor c = (RoosterCursor) getReadableDatabase().rawQueryWithFactory(new RoosterCursor.Factory(null), String.format("SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND dag = %d AND uur = %d AND datum = %d ", objArr), null, null);
        c.moveToFirst();
        return c;
    }

    public RoosterCursor getRooster(long dag, long uur) {
        Object[] objArr = new Object[3];
        objArr[0] = Long.valueOf(dag);
        objArr[1] = Long.valueOf(uur);
        objArr[DATABASE_VERSION] = 0;
        RoosterCursor c = (RoosterCursor) getReadableDatabase().rawQueryWithFactory(new RoosterCursor.Factory(null), String.format("SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND dag = %d AND uur = %d AND datum = %d ", objArr), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND dag = %d AND uur = %d AND datum = %d ";

        /* synthetic */ RoosterCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterCursor roosterCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterCursor(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public int getColBegin() {
            return getInt(getColumnIndexOrThrow("begin"));
        }

        public int getColEinde() {
            return getInt(getColumnIndexOrThrow("einde"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public RoosterCursorId getRoosterId(long id) {
        RoosterCursorId c = (RoosterCursorId) getReadableDatabase().rawQueryWithFactory(new RoosterCursorId.Factory(null), String.format("SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum, begin, einde FROM roosterles, vakken WHERE vak_id = vakken._id AND roosterles._id = %d", Long.valueOf(id)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterCursorId extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum, begin, einde FROM roosterles, vakken WHERE vak_id = vakken._id AND roosterles._id = %d";

        /* synthetic */ RoosterCursorId(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterCursorId roosterCursorId) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterCursorId(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterCursorId(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public long getColBegin() {
            return getLong(getColumnIndexOrThrow("begin"));
        }

        public long getColEinde() {
            return getLong(getColumnIndexOrThrow("einde"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public VakantieCursor getVakanties() {
        VakantieCursor c = (VakantieCursor) getReadableDatabase().rawQueryWithFactory(new VakantieCursor.Factory(null), "SELECT datumbegin as _id, datumeinde, naam FROM vakantie ORDER BY datumbegin", null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakantieCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT datumbegin as _id, datumeinde, naam FROM vakantie ORDER BY datumbegin";

        /* synthetic */ VakantieCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakantieCursor vakantieCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakantieCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakantieCursor(db, driver, editTable, query, null);
            }
        }

        public int getColDatumBegin() {
            return getInt(getColumnIndexOrThrow("_id"));
        }

        public int getColDatumEinde() {
            return getInt(getColumnIndexOrThrow("datumeinde"));
        }

        public String getColNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }
    }

    public RoosterWijzigingCursorAll getRoosterWijzigingAll() {
        RoosterWijzigingCursorAll c = (RoosterWijzigingCursorAll) getReadableDatabase().rawQueryWithFactory(new RoosterWijzigingCursorAll.Factory(null), "SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum > 0 ORDER BY datum,uur", null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterWijzigingCursorAll extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum > 0 ORDER BY datum,uur";

        /* synthetic */ RoosterWijzigingCursorAll(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterWijzigingCursorAll roosterWijzigingCursorAll) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterWijzigingCursorAll(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterWijzigingCursorAll(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public int getColBegin() {
            return getInt(getColumnIndexOrThrow("begin"));
        }

        public int getColEinde() {
            return getInt(getColumnIndexOrThrow("einde"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public RoosterWijzigingCursorId getRoosterWijzigingId(long id) {
        RoosterWijzigingCursorId c = (RoosterWijzigingCursorId) getReadableDatabase().rawQueryWithFactory(new RoosterWijzigingCursorId.Factory(null), String.format("SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND roosterles._id = %d AND datum > 0 ", Long.valueOf(id)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterWijzigingCursorId extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND roosterles._id = %d AND datum > 0 ";

        /* synthetic */ RoosterWijzigingCursorId(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterWijzigingCursorId roosterWijzigingCursorId) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterWijzigingCursorId(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterWijzigingCursorId(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public RoosterWijzigingCursorDatum getRoosterWijzigingDatum(long datum) {
        RoosterWijzigingCursorDatum c = (RoosterWijzigingCursorDatum) getReadableDatabase().rawQueryWithFactory(new RoosterWijzigingCursorDatum.Factory(null), String.format("SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum = %d ORDER BY uur", Long.valueOf(datum)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterWijzigingCursorDatum extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, begin, einde, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum = %d ORDER BY uur";

        /* synthetic */ RoosterWijzigingCursorDatum(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterWijzigingCursorDatum roosterWijzigingCursorDatum) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterWijzigingCursorDatum(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterWijzigingCursorDatum(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public int getColBegin() {
            return getInt(getColumnIndexOrThrow("begin"));
        }

        public int getColEinde() {
            return getInt(getColumnIndexOrThrow("einde"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public RoosterWijzigingCursorDatumVak getRoosterWijzigingDatumVak(long datum, long vakid) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = Long.valueOf(datum);
        objArr[1] = Long.valueOf(vakid);
        RoosterWijzigingCursorDatumVak c = (RoosterWijzigingCursorDatumVak) getReadableDatabase().rawQueryWithFactory(new RoosterWijzigingCursorDatumVak.Factory(null), String.format("SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum = %d AND vak_id = %d ORDER BY uur", objArr), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterWijzigingCursorDatumVak extends SQLiteCursor {
        private static final String QUERY = "SELECT roosterles._id, dag, uur, vak_id, naam, kort, lokaal, datum FROM roosterles, vakken WHERE vak_id = vakken._id AND datum = %d AND vak_id = %d ORDER BY uur";

        /* synthetic */ RoosterWijzigingCursorDatumVak(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterWijzigingCursorDatumVak roosterWijzigingCursorDatumVak) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterWijzigingCursorDatumVak(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterWijzigingCursorDatumVak(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColVakNaam() {
            return getString(getColumnIndexOrThrow("naam"));
        }

        public String getColVakKort() {
            return getString(getColumnIndexOrThrow("kort"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public RoosterCursorDagVak getRoosterDagVak(long dag, long vak_id) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = Long.valueOf(dag);
        objArr[1] = Long.valueOf(vak_id);
        RoosterCursorDagVak c = (RoosterCursorDagVak) getReadableDatabase().rawQueryWithFactory(new RoosterCursorDagVak.Factory(null), String.format("SELECT _id, vak_id, dag, datum FROM roosterles WHERE dag = %d AND vak_id = %d AND datum = 0 ", objArr), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterCursorDagVak extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, vak_id, dag, datum FROM roosterles WHERE dag = %d AND vak_id = %d AND datum = 0 ";

        /* synthetic */ RoosterCursorDagVak(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterCursorDagVak roosterCursorDagVak) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterCursorDagVak(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterCursorDagVak(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }
    }

    public RoosterCursorVak getRoosterVak(long vak_id) {
        RoosterCursorVak c = (RoosterCursorVak) getReadableDatabase().rawQueryWithFactory(new RoosterCursorVak.Factory(null), String.format("SELECT _id, vak_id FROM roosterles WHERE vak_id = %d and datum = 0 ", Long.valueOf(vak_id)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class RoosterCursorVak extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, vak_id FROM roosterles WHERE vak_id = %d and datum = 0 ";

        /* synthetic */ RoosterCursorVak(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, RoosterCursorVak roosterCursorVak) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private RoosterCursorVak(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new RoosterCursorVak(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }
    }

    public HuiswerkDateCursor getHuiswerkDates(long _toets) {
        HuiswerkDateCursor c = (HuiswerkDateCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkDateCursor.Factory(null), String.format("SELECT _id, datum, toets FROM huiswerk WHERE toets = '%d' GROUP BY datum ORDER BY datum", Long.valueOf(_toets)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkDateCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, datum, toets FROM huiswerk WHERE toets = '%d' GROUP BY datum ORDER BY datum";

        /* synthetic */ HuiswerkDateCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkDateCursor huiswerkDateCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkDateCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkDateCursor(db, driver, editTable, query, null);
            }
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    private void execMultipleSQL(SQLiteDatabase db, String[] sql) {
        for (String s : sql) {
            if (s.trim().length() > 0) {
                db.execSQL(s);
            }
        }
    }

    public void onCreate(SQLiteDatabase db) {
        String[] sql = this.mContext.getString(R.string.HuiswerkDatabase_onCreate).split("\n");
        db.beginTransaction();
        try {
            execMultipleSQL(db, sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating tables and debug data", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("HuisWerk", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        String[] sql = this.mContext.getString(R.string.HuiswerkDatabase_onUpgrade).split("\n");
        db.beginTransaction();
        try {
            execMultipleSQL(db, sql);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating tables and debug data", e.toString());
        } finally {
            db.endTransaction();
        }
        onCreate(db);
    }

    public void addRooster(long dag, long uur, long begin, long einde, long vak_id, String lokaal, long datum) {
        Object[] objArr = new Object[7];
        objArr[0] = Long.valueOf(dag);
        objArr[1] = Long.valueOf(uur);
        objArr[DATABASE_VERSION] = Long.valueOf(begin);
        objArr[3] = Long.valueOf(einde);
        objArr[4] = Long.valueOf(vak_id);
        objArr[5] = DatabaseUtils.sqlEscapeString(lokaal);
        objArr[6] = Long.valueOf(datum);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO roosterles (_id, dag, uur, begin, einde, vak_id, lokaal, datum) VALUES (             NULL, '%d','%d','%d','%d','%d',   %s,   '%d')", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new rooster", e.toString());
        }
    }

    public void editRooster(long _id, long dag, long uur, long begin, long einde, long vak_id, String lokaal, long datum) {
        Object[] objArr = new Object[8];
        objArr[0] = Long.valueOf(dag);
        objArr[1] = Long.valueOf(uur);
        objArr[DATABASE_VERSION] = Long.valueOf(begin);
        objArr[3] = Long.valueOf(einde);
        objArr[4] = Long.valueOf(vak_id);
        objArr[5] = DatabaseUtils.sqlEscapeString(lokaal);
        objArr[6] = Long.valueOf(datum);
        objArr[7] = Long.valueOf(_id);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE roosterles SET  dag = '%d',  uur = '%d',  begin = '%d',  einde = '%d',  vak_id = '%d',  lokaal = %s,  datum = '%d' WHERE _id = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error updating rooste", e.toString());
        }
    }

    public void deleteRooster(long _id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM roosterles WHERE _id = '%d' ", Long.valueOf(_id)));
        } catch (SQLException e) {
            Log.e("Error deleting rooster", e.toString());
        }
    }

    public void addHuiswerk(long vak_id, String hoofdstuk, String pagina, String omschrijving, long klaar, long toets, long datum) {
        Object[] objArr = new Object[7];
        objArr[0] = Long.valueOf(vak_id);
        objArr[1] = DatabaseUtils.sqlEscapeString(hoofdstuk);
        objArr[DATABASE_VERSION] = DatabaseUtils.sqlEscapeString(pagina);
        objArr[3] = DatabaseUtils.sqlEscapeString(omschrijving);
        objArr[4] = Long.valueOf(klaar);
        objArr[5] = Long.valueOf(toets);
        objArr[6] = Long.valueOf(datum);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO huiswerk (_id, vak_id, hoofdstuk, pagina, omschrijving, klaar, toets, datum) VALUES (               NULL, '%d',   %s,    %s,     %s,    '%d',    '%d',   '%d')", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new job", e.toString());
        }
    }

    public void editHuiswerk(long huiswerk_id, long vak_id, String hoofdstuk, String pagina, String omschrijving, long klaar, long toets, long datum) {
        Object[] objArr = new Object[8];
        objArr[0] = Long.valueOf(vak_id);
        objArr[1] = DatabaseUtils.sqlEscapeString(hoofdstuk);
        objArr[DATABASE_VERSION] = DatabaseUtils.sqlEscapeString(pagina);
        objArr[3] = DatabaseUtils.sqlEscapeString(omschrijving);
        objArr[4] = Long.valueOf(klaar);
        objArr[5] = Long.valueOf(toets);
        objArr[6] = Long.valueOf(datum);
        objArr[7] = Long.valueOf(huiswerk_id);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE huiswerk SET vak_id = '%d', hoofdstuk = %s,  pagina = %s,  omschrijving = %s,  klaar = '%d',  toets = '%d',  datum = '%d' WHERE _id = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new huiswerk", e.toString());
        }
    }

    public void deleteRoosterWijziging(long rooster_id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM roosterles WHERE _id = '%d' ", Long.valueOf(rooster_id)));
        } catch (SQLException e) {
            Log.e("Error deleting Rooster wijziging", e.toString());
        }
    }

    public void deleteHuiswerk(long huiswerk_id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM huiswerk WHERE _id = '%d' ", Long.valueOf(huiswerk_id)));
        } catch (SQLException e) {
            Log.e("Error deleting huiswerk", e.toString());
        }
    }

    public int getHuiswerkCountNietAf() {
        Cursor c = null;
        try {
            c = getReadableDatabase().rawQuery("SELECT count(*) FROM huiswerk WHERE klaar = 0 and toets = 0", null);
            if (c.getCount() <= 0) {
                return 0;
            }
            c.moveToFirst();
            int iCnt = c.getInt(0);
            c.close();
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                }
            }
            return iCnt;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e2) {
                }
            }
        }
    }

    public int getToetsCountNietAf() {
        Cursor c = null;
        try {
            c = getReadableDatabase().rawQuery("SELECT count(*) FROM huiswerk WHERE klaar = 0 and toets = 1", null);
            if (c.getCount() <= 0) {
                return 0;
            }
            c.moveToFirst();
            int iCnt = c.getInt(0);
            c.close();
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                }
            }
            return iCnt;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e2) {
                }
            }
        }
    }

    public int getHuiswerkCount() {
        Cursor c = null;
        try {
            c = getReadableDatabase().rawQuery("SELECT count(*) FROM huiswerk", null);
            if (c.getCount() <= 0) {
                return 0;
            }
            c.moveToFirst();
            int iCnt = c.getInt(0);
            c.close();
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                }
            }
            return iCnt;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e2) {
                }
            }
        }
    }

    public int getHuiswerkCountDateNietAf(int datum) {
        int iCount;
        Cursor c = getReadableDatabase().rawQuery(String.format("SELECT count(*) FROM huiswerk WHERE klaar = 0 AND toets = 0 AND datum = %d", Integer.valueOf(datum)), null);
        if (c.getCount() <= 0) {
            iCount = 0;
        } else {
            c.moveToFirst();
            iCount = c.getInt(0);
        }
        c.deactivate();
        c.close();
        return iCount;
    }

    public int getToetsCountDateNietAf(int datum) {
        int iCount;
        Cursor c = getReadableDatabase().rawQuery(String.format("SELECT count(*) FROM huiswerk WHERE klaar = 0 AND toets = 1 AND datum = %d", Integer.valueOf(datum)), null);
        if (c.getCount() <= 0) {
            iCount = 0;
        } else {
            c.moveToFirst();
            iCount = c.getInt(0);
        }
        c.deactivate();
        c.close();
        return iCount;
    }

    public LesUurCursor getLesUur(long uur) {
        LesUurCursor c = (LesUurCursor) getReadableDatabase().rawQueryWithFactory(new LesUurCursor.Factory(null), String.format("SELECT uur, start, stop FROM lesuren WHERE uur = %d ", Long.valueOf(uur)), null, null);
        c.moveToFirst();
        return c;
    }

    public VakInfoOldCursor getVakInfoOld() {
        VakInfoOldCursor c = (VakInfoOldCursor) getReadableDatabase().rawQueryWithFactory(new VakInfoOldCursor.Factory(null), "SELECT _id, kleur FROM vakinfo ", null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakInfoOldCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, kleur FROM vakinfo ";

        /* synthetic */ VakInfoOldCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakInfoOldCursor vakInfoOldCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakInfoOldCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakInfoOldCursor(db, driver, editTable, query, null);
            }
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public Integer getColKleur() {
            return Integer.valueOf(getInt(getColumnIndexOrThrow("kleur")));
        }
    }

    public VakInfoCursorAll getVakInfoAll() {
        VakInfoCursorAll c = (VakInfoCursorAll) getReadableDatabase().rawQueryWithFactory(new VakInfoCursorAll.Factory(null), "SELECT _id, kleur, leraar, phone, email FROM vakinfonew ", null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakInfoCursorAll extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, kleur, leraar, phone, email FROM vakinfonew ";

        /* synthetic */ VakInfoCursorAll(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakInfoCursorAll vakInfoCursorAll) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakInfoCursorAll(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakInfoCursorAll(db, driver, editTable, query, null);
            }
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public Integer getColKleur() {
            return Integer.valueOf(getInt(getColumnIndexOrThrow("kleur")));
        }

        public String getColLeraar() {
            return getString(getColumnIndexOrThrow("leraar"));
        }

        public String getColTelefoon() {
            return getString(getColumnIndexOrThrow("phone"));
        }

        public String getColEmail() {
            return getString(getColumnIndexOrThrow("email"));
        }
    }

    public VakInfoCursor getVakInfo(long id) {
        VakInfoCursor c = (VakInfoCursor) getReadableDatabase().rawQueryWithFactory(new VakInfoCursor.Factory(null), String.format("SELECT _id, kleur, leraar, phone, email FROM vakinfonew WHERE _id = %d ", Long.valueOf(id)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class VakInfoCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, kleur, leraar, phone, email FROM vakinfonew WHERE _id = %d ";

        /* synthetic */ VakInfoCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, VakInfoCursor vakInfoCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private VakInfoCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new VakInfoCursor(db, driver, editTable, query, null);
            }
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public Integer getColKleur() {
            return Integer.valueOf(getInt(getColumnIndexOrThrow("kleur")));
        }

        public String getColLeraar() {
            return getString(getColumnIndexOrThrow("leraar"));
        }

        public String getColTelefoon() {
            return getString(getColumnIndexOrThrow("phone"));
        }

        public String getColEmail() {
            return getString(getColumnIndexOrThrow("email"));
        }
    }

    public void addVakInfo(long id, long kleur, String leraar, String telefoon, String email) {
        Object[] objArr = new Object[5];
        objArr[0] = Long.valueOf(id);
        objArr[1] = Long.valueOf(kleur);
        objArr[DATABASE_VERSION] = DatabaseUtils.sqlEscapeString(leraar);
        objArr[3] = DatabaseUtils.sqlEscapeString(telefoon);
        objArr[4] = DatabaseUtils.sqlEscapeString(email);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO vakinfonew (_id, kleur, leraar, phone, email) VALUES ( '%d',  '%d',  %s,  %s,  %s)", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new vakinfo", e.toString());
        }
    }

    public void editVakInfo(long id, long kleur, String leraar, String telefoon, String email) {
        Object[] objArr = new Object[5];
        objArr[0] = Long.valueOf(kleur);
        objArr[1] = DatabaseUtils.sqlEscapeString(leraar);
        objArr[DATABASE_VERSION] = DatabaseUtils.sqlEscapeString(telefoon);
        objArr[3] = DatabaseUtils.sqlEscapeString(email);
        objArr[4] = Long.valueOf(id);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE vakinfonew SET kleur = '%d', leraar = %s, phone = %s, email = %s WHERE _id = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new vakinfo", e.toString());
        }
    }

    public void deleteVakInfo(long _id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM vakinfonew WHERE _id = '%d' ", Long.valueOf(_id)));
        } catch (SQLException e) {
            Log.e("Error deleting vakinfonew", e.toString());
        }
    }

    public HuiswerkFotoCursor getHuiswerkFoto(long id) {
        HuiswerkFotoCursor c = (HuiswerkFotoCursor) getReadableDatabase().rawQueryWithFactory(new HuiswerkFotoCursor.Factory(null), String.format("SELECT _id, foto FROM huiswerkfoto WHERE _id = %d ", Long.valueOf(id)), null, null);
        c.moveToFirst();
        return c;
    }

    public static class HuiswerkFotoCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, foto FROM huiswerkfoto WHERE _id = %d ";

        /* synthetic */ HuiswerkFotoCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, HuiswerkFotoCursor huiswerkFotoCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private HuiswerkFotoCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new HuiswerkFotoCursor(db, driver, editTable, query, null);
            }
        }

        public long getColHuiswerkId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public String getColFoto() {
            return getString(getColumnIndexOrThrow("foto"));
        }
    }

    public void addHuiswerkFoto(long id, String foto) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = Long.valueOf(id);
        objArr[1] = DatabaseUtils.sqlEscapeString(foto);
        try {
            getWritableDatabase().execSQL(String.format("INSERT INTO huiswerkfoto (_id, foto) VALUES ( '%d', %s)", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new huiswerkfoto", e.toString());
        }
    }

    public void editHuiswerkFoto(long id, String foto) {
        Object[] objArr = new Object[DATABASE_VERSION];
        objArr[0] = DatabaseUtils.sqlEscapeString(foto);
        objArr[1] = Long.valueOf(id);
        try {
            getWritableDatabase().execSQL(String.format("UPDATE huiswerkfoto SET foto = %s WHERE _id = '%d' ", objArr));
        } catch (SQLException e) {
            Log.e("Error writing new huiswerkfoto", e.toString());
        }
    }

    public void deleteHuiswerkFoto(long _id) {
        try {
            getWritableDatabase().execSQL(String.format("DELETE FROM huiswerkfoto WHERE _id = '%d' ", Long.valueOf(_id)));
        } catch (SQLException e) {
            Log.e("Error deleting huiswerkfoto", e.toString());
        }
    }

    public void CreateHuiswerkFotoTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE huiswerkfoto (_id INTEGER PRIMARY KEY, foto TEXT);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating table huiswerkfoto", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public OldRoosterCursor getOldRooster() {
        OldRoosterCursor c = (OldRoosterCursor) getReadableDatabase().rawQueryWithFactory(new OldRoosterCursor.Factory(null), "SELECT _id, dag, uur, vak_id, lokaal, datum FROM rooster", null, null);
        c.moveToFirst();
        return c;
    }

    public static class OldRoosterCursor extends SQLiteCursor {
        private static final String QUERY = "SELECT _id, dag, uur, vak_id, lokaal, datum FROM rooster";

        /* synthetic */ OldRoosterCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery, OldRoosterCursor oldRoosterCursor) {
            this(sQLiteDatabase, sQLiteCursorDriver, str, sQLiteQuery);
        }

        private OldRoosterCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
            super(db, driver, editTable, query);
        }

        private static class Factory implements SQLiteDatabase.CursorFactory {
            private Factory() {
            }

            /* synthetic */ Factory(Factory factory) {
                this();
            }

            public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver driver, String editTable, SQLiteQuery query) {
                return new OldRoosterCursor(db, driver, editTable, query, null);
            }
        }

        public long getColRoosterId() {
            return getLong(getColumnIndexOrThrow("_id"));
        }

        public long getColDag() {
            return getLong(getColumnIndexOrThrow("dag"));
        }

        public int getColUur() {
            return getInt(getColumnIndexOrThrow("uur"));
        }

        public long getColVakId() {
            return getLong(getColumnIndexOrThrow("vak_id"));
        }

        public String getColLokaal() {
            return getString(getColumnIndexOrThrow("lokaal"));
        }

        public int getColDatum() {
            return getInt(getColumnIndexOrThrow("datum"));
        }
    }

    public void CreateVakantieTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE vakantie (datumbegin INTEGER PRIMARY KEY , datumeinde INTEGER, naam TEXT);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating table vakantie", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public void CreateRoosterLesTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("CREATE TABLE roosterles (_id INTEGER PRIMARY KEY AUTOINCREMENT, dag INTEGER, uur INTEGER, begin INTEGER, einde INTEGER, vak_id INTEGER, lokaal TEXT, datum INTEGER);");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.e("Error creating table roosterles", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public String getBackupNaam() {
        return String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/" + DATABASE_NAME + ".backup";
    }

    public String getBackupDate() {
        File file = new File(getBackupNaam());
        if (file.exists()) {
            return new Date(file.lastModified()).toLocaleString();
        }
        return "--";
    }

    public Boolean deleteBackup() {
        File file = new File(getBackupNaam());
        if (!file.exists()) {
            return false;
        }
        file.delete();
        return true;
    }

    public void backupDataBase() throws IOException {
        String outFileName = getBackupNaam();
        InputStream myInput = new FileInputStream(String.valueOf(DATABASE_PATH) + DATABASE_NAME);
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        while (true) {
            int length = myInput.read(buffer);
            if (length <= 0) {
                myOutput.flush();
                myOutput.close();
                myInput.close();
                return;
            }
            myOutput.write(buffer, 0, length);
        }
    }

    public void restoreDataBase() throws IOException {
        InputStream myInput = new FileInputStream(getBackupNaam());
        OutputStream myOutput = new FileOutputStream(String.valueOf(DATABASE_PATH) + DATABASE_NAME);
        byte[] buffer = new byte[1024];
        while (true) {
            int length = myInput.read(buffer);
            if (length <= 0) {
                myOutput.flush();
                myOutput.close();
                myInput.close();
                return;
            }
            myOutput.write(buffer, 0, length);
        }
    }
}
