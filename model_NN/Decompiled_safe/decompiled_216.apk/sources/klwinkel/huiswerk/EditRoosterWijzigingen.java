package klwinkel.huiswerk;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditRoosterWijzigingen extends Activity {
    /* access modifiers changed from: private */
    public static Button btnBeginTijd;
    private static Button btnCancelRooster;
    /* access modifiers changed from: private */
    public static Button btnDatum;
    private static Button btnDeleteRooster;
    /* access modifiers changed from: private */
    public static Button btnEindTijd;
    private static Button btnUpdateRooster;
    /* access modifiers changed from: private */
    public static Integer mBegin = 0;
    /* access modifiers changed from: private */
    public static Calendar mCal = null;
    private static Integer mDatum = 0;
    /* access modifiers changed from: private */
    public static Integer mEinde = 0;
    private static Integer mRoosterId = 0;
    private static Integer mUur = 0;
    /* access modifiers changed from: private */
    public static Context myContext;
    /* access modifiers changed from: private */
    public static Spinner spnUur;
    /* access modifiers changed from: private */
    public static Spinner spnVak;
    private static ScrollView svMain;
    /* access modifiers changed from: private */
    public static EditText txtLokaal;
    private final View.OnClickListener beginTijdOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new TimePickerDialog(EditRoosterWijzigingen.this, EditRoosterWijzigingen.this.mBeginTijdSetListener, EditRoosterWijzigingen.mBegin.intValue() / 100, EditRoosterWijzigingen.mBegin.intValue() % 100, DateFormat.is24HourFormat(EditRoosterWijzigingen.this.getApplicationContext())).show();
        }
    };
    private final View.OnClickListener btnCancelRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditRoosterWijzigingen.this.finish();
        }
    };
    private final View.OnClickListener btnDeleteRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            HuiswerkDatabase.RoosterCursor roosterCursor = EditRoosterWijzigingen.this.huiswerkDb.getRoosterWijziging(((Uur) EditRoosterWijzigingen.spnUur.getSelectedItem()).uur, (long) Integer.valueOf((EditRoosterWijzigingen.mCal.get(1) * 10000) + (EditRoosterWijzigingen.mCal.get(2) * 100) + EditRoosterWijzigingen.mCal.get(5)).intValue());
            if (roosterCursor.getCount() > 0) {
                EditRoosterWijzigingen.this.huiswerkDb.deleteRoosterWijziging(roosterCursor.getColRoosterId());
            }
            roosterCursor.close();
            HuisWerkMain.UpdateWidgets(EditRoosterWijzigingen.myContext);
            EditRoosterWijzigingen.this.finish();
        }
    };
    private final View.OnClickListener btnUpdateRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String str1;
            Integer iDatum = Integer.valueOf((EditRoosterWijzigingen.mCal.get(1) * 10000) + (EditRoosterWijzigingen.mCal.get(2) * 100) + EditRoosterWijzigingen.mCal.get(5));
            Vak vak = (Vak) EditRoosterWijzigingen.spnVak.getSelectedItem();
            Uur uur = (Uur) EditRoosterWijzigingen.spnUur.getSelectedItem();
            if (vak.id <= 0) {
                if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                    str1 = EditRoosterWijzigingen.this.getString(R.string.geenvakgekozen);
                } else {
                    str1 = EditRoosterWijzigingen.this.getString(R.string.geenklasgekozen);
                }
                Toast.makeText(EditRoosterWijzigingen.this, str1, 1).show();
            } else {
                HuiswerkDatabase.RoosterCursor roosterWijzCursor = EditRoosterWijzigingen.this.huiswerkDb.getRoosterWijziging(uur.uur, (long) iDatum.intValue());
                if (roosterWijzCursor.getCount() > 0) {
                    EditRoosterWijzigingen.this.huiswerkDb.editRooster(roosterWijzCursor.getColRoosterId(), -1, uur.uur, (long) roosterWijzCursor.getColBegin(), (long) roosterWijzCursor.getColEinde(), vak.id, EditRoosterWijzigingen.txtLokaal.getText().toString(), (long) iDatum.intValue());
                } else {
                    EditRoosterWijzigingen.this.huiswerkDb.addRooster(-1, uur.uur, (long) EditRoosterWijzigingen.mBegin.intValue(), (long) EditRoosterWijzigingen.mEinde.intValue(), vak.id, EditRoosterWijzigingen.txtLokaal.getText().toString(), (long) iDatum.intValue());
                }
                roosterWijzCursor.close();
                HuisWerkMain.UpdateWidgets(EditRoosterWijzigingen.myContext);
            }
            EditRoosterWijzigingen.this.finish();
        }
    };
    private final View.OnClickListener datumOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new DatePickerDialog(EditRoosterWijzigingen.this, EditRoosterWijzigingen.this.mDateSetListener, EditRoosterWijzigingen.mCal.get(1), EditRoosterWijzigingen.mCal.get(2), EditRoosterWijzigingen.mCal.get(5)).show();
        }
    };
    private final View.OnClickListener eindTijdOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new TimePickerDialog(EditRoosterWijzigingen.this, EditRoosterWijzigingen.this.mEindTijdSetListener, EditRoosterWijzigingen.mEinde.intValue() / 100, EditRoosterWijzigingen.mEinde.intValue() % 100, DateFormat.is24HourFormat(EditRoosterWijzigingen.this.getApplicationContext())).show();
        }
    };
    /* access modifiers changed from: private */
    public HuiswerkDatabase huiswerkDb;
    /* access modifiers changed from: private */
    public TimePickerDialog.OnTimeSetListener mBeginTijdSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditRoosterWijzigingen.mBegin = Integer.valueOf((hourOfDay * 100) + minute);
            EditRoosterWijzigingen.btnBeginTijd.setText(HwUtl.Time2String(EditRoosterWijzigingen.mBegin.intValue()));
        }
    };
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            EditRoosterWijzigingen.mCal.set(1, year);
            EditRoosterWijzigingen.mCal.set(2, monthOfYear);
            EditRoosterWijzigingen.mCal.set(5, dayOfMonth);
            EditRoosterWijzigingen.btnDatum.setText((String) DateFormat.format("dd MMMM yyyy", new Date(year - 1900, monthOfYear, dayOfMonth)));
        }
    };
    /* access modifiers changed from: private */
    public TimePickerDialog.OnTimeSetListener mEindTijdSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditRoosterWijzigingen.mEinde = Integer.valueOf((hourOfDay * 100) + minute);
            EditRoosterWijzigingen.btnEindTijd.setText(HwUtl.Time2String(EditRoosterWijzigingen.mEinde.intValue()));
        }
    };

    private class Vak {
        public long id;
        public String vakName;

        Vak(long id2, String vakName2) {
            this.id = id2;
            this.vakName = vakName2;
        }

        public String toString() {
            return this.vakName;
        }
    }

    private class Uur {
        public String strUur;
        public long uur;

        Uur(long uur2, String strUur2) {
            this.uur = uur2;
            this.strUur = strUur2;
        }

        public String toString() {
            return this.strUur;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        String str1;
        String str12;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editroosterwijziging);
        myContext = this;
        this.huiswerkDb = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        mCal = Calendar.getInstance();
        TextView tv = (TextView) findViewById(R.id.lblVak);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str1 = getString(R.string.vak);
        } else {
            str1 = getString(R.string.klas);
        }
        tv.setText(str1);
        svMain = (ScrollView) findViewById(R.id.svMain);
        btnDatum = (Button) findViewById(R.id.btnDatum);
        btnDatum.setOnClickListener(this.datumOnClick);
        btnBeginTijd = (Button) findViewById(R.id.btnBeginTijd);
        btnEindTijd = (Button) findViewById(R.id.btnEindTijd);
        btnBeginTijd.setOnClickListener(this.beginTijdOnClick);
        btnEindTijd.setOnClickListener(this.eindTijdOnClick);
        spnUur = (Spinner) findViewById(R.id.spnUur);
        spnVak = (Spinner) findViewById(R.id.spnVak);
        txtLokaal = (EditText) findViewById(R.id.txtLokaal);
        btnUpdateRooster = (Button) findViewById(R.id.btnUpdate);
        btnDeleteRooster = (Button) findViewById(R.id.btnDelete);
        btnCancelRooster = (Button) findViewById(R.id.btnCancel);
        btnUpdateRooster.setOnClickListener(this.btnUpdateRoosterOnClick);
        btnDeleteRooster.setOnClickListener(this.btnDeleteRoosterOnClick);
        btnCancelRooster.setOnClickListener(this.btnCancelRoosterOnClick);
        Integer iPrefNumHours = Integer.valueOf(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_NUMHOURS", 10));
        List<Uur> urenList = new ArrayList<>();
        for (int i = 1; i <= iPrefNumHours.intValue(); i++) {
            String str = String.format(": %d", Integer.valueOf(i));
            urenList.add(new Uur((long) i, String.valueOf(getString(R.string.lesuur)) + str));
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, urenList);
        arrayAdapter.setDropDownViewResource(17367049);
        spnUur.setAdapter((SpinnerAdapter) arrayAdapter);
        spnUur.setOnItemSelectedListener(new spnUurOnItemSelectedListener());
        List<Vak> vakkenList = new ArrayList<>();
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str12 = getString(R.string.kieseenvak);
        } else {
            str12 = getString(R.string.kieseenklas);
        }
        vakkenList.add(new Vak(-1, str12));
        HuiswerkDatabase.VakkenCursor cVak = this.huiswerkDb.getVakken(HuiswerkDatabase.VakkenCursor.SortBy.naam);
        for (int i2 = 0; i2 < cVak.getCount(); i2++) {
            cVak.moveToPosition(i2);
            vakkenList.add(new Vak((long) cVak.getColVakkenId().intValue(), cVak.getColNaam()));
        }
        cVak.close();
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, vakkenList);
        arrayAdapter2.setDropDownViewResource(17367049);
        spnVak.setAdapter((SpinnerAdapter) arrayAdapter2);
        if (savedInstanceState == null) {
            Bundle bIn = getIntent().getExtras();
            mRoosterId = 0;
            mDatum = 0;
            mUur = 0;
            if (bIn != null) {
                mRoosterId = Integer.valueOf(bIn.getInt("_id"));
                mDatum = Integer.valueOf(bIn.getInt("_datum"));
                mUur = Integer.valueOf(bIn.getInt("_uur"));
            }
            if (mRoosterId.intValue() != 0) {
                HuiswerkDatabase.RoosterWijzigingCursorId rwC = this.huiswerkDb.getRoosterWijzigingId(mRoosterId.longValue());
                if (rwC.getCount() > 0) {
                    mDatum = Integer.valueOf(rwC.getColDatum());
                    mUur = Integer.valueOf(rwC.getColUur());
                }
                rwC.close();
            }
            if (mDatum.intValue() > 0) {
                Integer iYear = Integer.valueOf(mDatum.intValue() / 10000);
                Integer iMonth = Integer.valueOf((mDatum.intValue() % 10000) / 100);
                Integer iDay = Integer.valueOf(mDatum.intValue() % 100);
                mCal.set(1, iYear.intValue());
                mCal.set(2, iMonth.intValue());
                mCal.set(5, iDay.intValue());
                showDatum();
            } else {
                mCal = Calendar.getInstance();
                showDatum();
            }
            if (mUur.intValue() > 0) {
                int i3 = 0;
                while (true) {
                    if (i3 >= spnUur.getCount()) {
                        break;
                    } else if (((Uur) spnUur.getItemAtPosition(i3)).uur == ((long) mUur.intValue())) {
                        spnUur.setSelection(i3);
                        break;
                    } else {
                        i3++;
                    }
                }
            }
            FillFields();
        }
    }

    public void showDatum() {
        btnDatum.setText((String) DateFormat.format("  EEEE dd MMMM", new Date(mCal.get(1) - 1900, mCal.get(2), mCal.get(5))));
    }

    /* access modifiers changed from: private */
    public void FillFields() {
        Integer iDatum = Integer.valueOf((mCal.get(1) * 10000) + (mCal.get(2) * 100) + mCal.get(5));
        Uur uur = (Uur) spnUur.getSelectedItem();
        HuiswerkDatabase.RoosterCursor roosterWijzigingCursor = this.huiswerkDb.getRoosterWijziging(uur.uur, (long) iDatum.intValue());
        if (roosterWijzigingCursor.getCount() > 0) {
            int i = 0;
            while (true) {
                if (i >= spnVak.getCount()) {
                    break;
                } else if (((Vak) spnVak.getItemAtPosition(i)).id == roosterWijzigingCursor.getColVakId()) {
                    spnVak.setSelection(i);
                    break;
                } else {
                    i++;
                }
            }
            txtLokaal.setText(roosterWijzigingCursor.getColLokaal());
            mBegin = Integer.valueOf(roosterWijzigingCursor.getColBegin());
            btnBeginTijd.setText(HwUtl.Time2String(mBegin.intValue()));
            mEinde = Integer.valueOf(roosterWijzigingCursor.getColEinde());
            btnEindTijd.setText(HwUtl.Time2String(mEinde.intValue()));
        } else {
            spnVak.setSelection(0);
            txtLokaal.setText("");
            HuiswerkDatabase.RoosterCursor roosterCursor = this.huiswerkDb.getRooster(this.huiswerkDb.GetRoosterDagLong(this, mCal), uur.uur);
            if (roosterCursor.getCount() > 0) {
                mBegin = Integer.valueOf(roosterCursor.getColBegin());
                btnBeginTijd.setText(HwUtl.Time2String(mBegin.intValue()));
                mEinde = Integer.valueOf(roosterCursor.getColEinde());
                btnEindTijd.setText(HwUtl.Time2String(mEinde.intValue()));
            } else {
                HuiswerkDatabase.LesUurCursor uC = this.huiswerkDb.getLesUur(uur.uur);
                mBegin = Integer.valueOf(uC.getColStart());
                btnBeginTijd.setText(HwUtl.Time2String(mBegin.intValue()));
                mEinde = Integer.valueOf(uC.getColStop());
                btnEindTijd.setText(HwUtl.Time2String(mEinde.intValue()));
                uC.close();
            }
            roosterCursor.close();
        }
        roosterWijzigingCursor.close();
    }

    public class spnDatumOnDateChangedListener implements DatePicker.OnDateChangedListener {
        public spnDatumOnDateChangedListener() {
        }

        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            EditRoosterWijzigingen.this.FillFields();
        }
    }

    public class spnUurOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public spnUurOnItemSelectedListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            EditRoosterWijzigingen.this.FillFields();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        if (mRoosterId.intValue() != 0) {
            spnUur.setEnabled(false);
            btnDatum.setEnabled(false);
            btnBeginTijd.setEnabled(false);
            btnEindTijd.setEnabled(false);
        } else {
            btnDeleteRooster.setEnabled(false);
        }
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.huiswerkDb.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
