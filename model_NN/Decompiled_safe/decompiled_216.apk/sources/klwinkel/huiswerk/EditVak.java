package klwinkel.huiswerk;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import klwinkel.huiswerk.AmbilWarnaDialog;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditVak extends Activity {
    private static Button btnCancel;
    private static Button btnDelete;
    private static Button btnKleur;
    private static Button btnUpdateVak;
    /* access modifiers changed from: private */
    public static ImageView imgKleur;
    /* access modifiers changed from: private */
    public static Integer mVakId = 0;
    /* access modifiers changed from: private */
    public static Context myContext;
    private static ScrollView svMain;
    /* access modifiers changed from: private */
    public static EditText txtEmail;
    /* access modifiers changed from: private */
    public static EditText txtKort;
    /* access modifiers changed from: private */
    public static EditText txtLeraar;
    /* access modifiers changed from: private */
    public static TextView txtNaam;
    /* access modifiers changed from: private */
    public static EditText txtTelefoon;
    /* access modifiers changed from: private */
    public static Integer vakKleur;
    private final View.OnClickListener btnCancelOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditVak.this.finish();
        }
    };
    private final View.OnClickListener btnDeleteOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String str1;
            HuiswerkDatabase.RoosterCursorVak rcv = EditVak.this.db.getRoosterVak((long) EditVak.mVakId.intValue());
            if (rcv.getCount() > 0) {
                if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                    str1 = EditVak.this.getString(R.string.vakingebruik);
                } else {
                    str1 = EditVak.this.getString(R.string.klasingebruik);
                }
                Toast.makeText(EditVak.this, str1, 1).show();
            } else {
                EditVak.this.db.deleteVak((long) EditVak.mVakId.intValue());
                HuiswerkDatabase.VakInfoCursor viC = EditVak.this.db.getVakInfo((long) EditVak.mVakId.intValue());
                if (viC.getCount() > 0) {
                    EditVak.this.db.deleteVakInfo((long) EditVak.mVakId.intValue());
                }
                viC.close();
            }
            rcv.close();
            EditVak.this.finish();
        }
    };
    private final View.OnClickListener btnKleurOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new AmbilWarnaDialog(EditVak.this, EditVak.vakKleur.intValue(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    EditVak.vakKleur = Integer.valueOf(color);
                    EditVak.imgKleur.setBackgroundColor(EditVak.vakKleur.intValue());
                }

                public void onCancel(AmbilWarnaDialog dialog) {
                }
            }).show();
        }
    };
    private final View.OnClickListener btnUpdateOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (EditVak.mVakId.intValue() > 0) {
                EditVak.this.db.editVak((long) EditVak.mVakId.intValue(), EditVak.txtNaam.getText().toString(), EditVak.txtKort.getText().toString());
            } else {
                EditVak.this.db.addVak(EditVak.txtNaam.getText().toString(), EditVak.txtKort.getText().toString());
                HuiswerkDatabase.VakkenCursor vC = EditVak.this.db.getVakken(HuiswerkDatabase.VakkenCursor.SortBy.naam);
                while (true) {
                    if (!vC.isAfterLast()) {
                        if (vC.getColNaam().compareTo(EditVak.txtNaam.getText().toString()) == 0 && vC.getColKort().compareTo(EditVak.txtKort.getText().toString()) == 0) {
                            EditVak.mVakId = vC.getColVakkenId();
                            break;
                        }
                        vC.moveToNext();
                    } else {
                        break;
                    }
                }
                vC.close();
            }
            if (EditVak.mVakId.intValue() > 0) {
                HuiswerkDatabase.VakInfoCursor viC = EditVak.this.db.getVakInfo((long) EditVak.mVakId.intValue());
                if (viC.getCount() > 0) {
                    EditVak.this.db.editVakInfo((long) EditVak.mVakId.intValue(), (long) EditVak.vakKleur.intValue(), EditVak.txtLeraar.getText().toString(), EditVak.txtTelefoon.getText().toString(), EditVak.txtEmail.getText().toString());
                } else {
                    EditVak.this.db.addVakInfo((long) EditVak.mVakId.intValue(), (long) EditVak.vakKleur.intValue(), EditVak.txtLeraar.getText().toString(), EditVak.txtTelefoon.getText().toString(), EditVak.txtEmail.getText().toString());
                }
                viC.close();
            }
            HuisWerkMain.UpdateWidgets(EditVak.myContext);
            if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                String string = EditVak.this.getString(R.string.vakopgeslagen);
            } else {
                String string2 = EditVak.this.getString(R.string.klasopgeslagen);
            }
            EditVak.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public HuiswerkDatabase db;

    public void onCreate(Bundle savedInstanceState) {
        String str1;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.editvak);
        myContext = this;
        this.db = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        TextView tv = (TextView) findViewById(R.id.lblNaam);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str1 = getString(R.string.vak);
            setTitle(getString(R.string.vakwijzigen));
        } else {
            str1 = getString(R.string.klas);
            setTitle(getString(R.string.klaswijzigen));
        }
        tv.setText(str1);
        svMain = (ScrollView) findViewById(R.id.svMain);
        btnKleur = (Button) findViewById(R.id.btnKleur);
        imgKleur = (ImageView) findViewById(R.id.imgKleur);
        btnUpdateVak = (Button) findViewById(R.id.btn1);
        btnDelete = (Button) findViewById(R.id.btn2);
        btnCancel = (Button) findViewById(R.id.btn3);
        txtNaam = (TextView) findViewById(R.id.txtNaam);
        txtKort = (EditText) findViewById(R.id.txtKort);
        txtLeraar = (EditText) findViewById(R.id.txtLeraar);
        txtTelefoon = (EditText) findViewById(R.id.txtTelefoon);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnUpdateVak.setText(getString(R.string.opslaan));
        btnDelete.setText(getString(R.string.verwijderen));
        btnCancel.setText(getString(R.string.annuleren));
        btnUpdateVak.setOnClickListener(this.btnUpdateOnClick);
        btnCancel.setOnClickListener(this.btnCancelOnClick);
        btnDelete.setOnClickListener(this.btnDeleteOnClick);
        btnKleur.setOnClickListener(this.btnKleurOnClick);
        imgKleur.setOnClickListener(this.btnKleurOnClick);
        if (savedInstanceState == null) {
            Log.e("---------------------->EditVak()", "savedInstanceState == null");
            vakKleur = -1;
            Bundle bIn = getIntent().getExtras();
            mVakId = 0;
            if (bIn != null) {
                mVakId = Integer.valueOf(bIn.getInt("_id"));
            }
            if (mVakId.intValue() > 0) {
                HuiswerkDatabase.VakDetailCursor cVak = this.db.getVakDetails(mVakId.longValue());
                txtNaam.setText(cVak.getColNaam());
                txtKort.setText(cVak.getColKort());
                txtNaam.setEnabled(false);
                txtNaam.setFocusable(false);
                cVak.close();
                HuiswerkDatabase.VakInfoCursor viC = this.db.getVakInfo((long) mVakId.intValue());
                if (viC.getCount() > 0) {
                    vakKleur = viC.getColKleur();
                    imgKleur.setBackgroundColor(vakKleur.intValue());
                    txtLeraar.setText(viC.getColLeraar());
                    txtTelefoon.setText(viC.getColTelefoon());
                    txtEmail.setText(viC.getColEmail());
                }
                viC.close();
            } else {
                btnDelete.setEnabled(false);
            }
        }
        imgKleur.setBackgroundColor(vakKleur.intValue());
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        if (mVakId.intValue() > 0) {
            txtNaam.setEnabled(false);
            txtNaam.setFocusable(false);
        } else {
            btnDelete.setEnabled(false);
        }
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
