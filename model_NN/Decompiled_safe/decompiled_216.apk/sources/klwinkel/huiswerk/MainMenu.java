package klwinkel.huiswerk;

import android.app.ActivityGroup;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainMenu extends ActivityGroup {
    private LinearLayout btn1;
    private LinearLayout btn10;
    private final View.OnClickListener btn10OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, Handleiding.class));
        }
    };
    private final View.OnClickListener btn1OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, EditRoosterWeek.class));
        }
    };
    private LinearLayout btn2;
    private final View.OnClickListener btn2OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, Vakken.class));
        }
    };
    private TextView btn2text;
    private LinearLayout btn3;
    private final View.OnClickListener btn3OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, EditUren.class));
        }
    };
    private LinearLayout btn4;
    private final View.OnClickListener btn4OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, RoosterWijzigingen.class));
        }
    };
    private LinearLayout btn5;
    private final View.OnClickListener btn5OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, EditHuiswerk.class));
        }
    };
    private LinearLayout btn6;
    private final View.OnClickListener btn6OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, Instellingen.class));
        }
    };
    private LinearLayout btn7;
    private final View.OnClickListener btn7OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, BackupRestore.class));
        }
    };
    private LinearLayout btn8;
    private final View.OnClickListener btn8OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, VakantieList.class));
        }
    };
    private LinearLayout btn9;
    private final View.OnClickListener btn9OnClick = new View.OnClickListener() {
        public void onClick(View v) {
            MainMenu.this.startActivity(new Intent(MainMenu.this.myContext, WhatsNew.class));
        }
    };
    private HuiswerkDatabase db;
    private LinearLayout mainmenu;
    public Context myContext;

    public void onCreate(Bundle savedInstanceState) {
        String strVakken;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mainmenu);
        this.myContext = this;
        this.db = new HuiswerkDatabase(this.myContext);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            strVakken = getString(R.string.rooster_menu_vakken);
        } else {
            strVakken = getString(R.string.rooster_menu_klassen);
        }
        this.mainmenu = (LinearLayout) findViewById(R.id.MainMenu);
        this.btn1 = (LinearLayout) findViewById(R.id.btn1);
        this.btn1.setOnClickListener(this.btn1OnClick);
        this.btn2 = (LinearLayout) findViewById(R.id.btn2);
        this.btn2text = (TextView) findViewById(R.id.btn2text);
        this.btn2text.setText(strVakken);
        this.btn2.setOnClickListener(this.btn2OnClick);
        this.btn3 = (LinearLayout) findViewById(R.id.btn3);
        this.btn3.setOnClickListener(this.btn3OnClick);
        this.btn4 = (LinearLayout) findViewById(R.id.btn4);
        this.btn4.setOnClickListener(this.btn4OnClick);
        this.btn5 = (LinearLayout) findViewById(R.id.btn5);
        this.btn5.setOnClickListener(this.btn5OnClick);
        this.btn6 = (LinearLayout) findViewById(R.id.btn6);
        this.btn6.setOnClickListener(this.btn6OnClick);
        this.btn7 = (LinearLayout) findViewById(R.id.btn7);
        this.btn7.setOnClickListener(this.btn7OnClick);
        this.btn8 = (LinearLayout) findViewById(R.id.btn8);
        this.btn8.setOnClickListener(this.btn8OnClick);
        this.btn9 = (LinearLayout) findViewById(R.id.btn9);
        this.btn9.setOnClickListener(this.btn9OnClick);
        this.btn10 = (LinearLayout) findViewById(R.id.btn10);
        this.btn10.setOnClickListener(this.btn10OnClick);
    }

    public void onResume() {
        this.mainmenu.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }
}
