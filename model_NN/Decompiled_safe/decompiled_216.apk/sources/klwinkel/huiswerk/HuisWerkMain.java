package klwinkel.huiswerk;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import java.util.Calendar;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkMain extends TabActivity {
    public static Context AppContext;
    /* access modifiers changed from: private */
    public static int mAppMode = 0;
    private static Calendar mCurViewDate;
    /* access modifiers changed from: private */
    public static Context myContext;
    private int RelNotesVersion = 0;
    private HuiswerkDatabase db;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.huiswerkmain);
        AppContext = getApplicationContext();
        myContext = this;
        this.db = new HuiswerkDatabase(this, false);
        AskAppMode();
        AddMissingDBRecords();
        this.db.Init(this);
        VakantieDagen.LaadVakantieDagen(this.db);
        mCurViewDate = Calendar.getInstance();
        TabHost tabHost = (TabHost) findViewById(16908306);
        TabHost.TabSpec tabSpec1 = tabHost.newTabSpec("tab1");
        TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("tab2");
        TabHost.TabSpec tabSpec3 = tabHost.newTabSpec("tab3");
        TabHost.TabSpec tabSpec4 = tabHost.newTabSpec("tab4");
        TabHost.TabSpec tabSpec5 = tabHost.newTabSpec("tab5");
        Resources resources = getResources();
        tabSpec1.setIndicator(getString(R.string.mainmenu), resources.getDrawable(R.drawable.mainmenu)).setContent(new Intent(this, MainMenu.class));
        tabSpec2.setIndicator(getString(R.string.app_name), resources.getDrawable(R.drawable.huiswerk)).setContent(new Intent(this, HuisWerkList1.class));
        tabSpec3.setIndicator(getString(R.string.toets), resources.getDrawable(R.drawable.toets)).setContent(new Intent(this, HuisWerkList2.class));
        tabSpec4.setIndicator(getString(R.string.dag), resources.getDrawable(R.drawable.roostertab)).setContent(new Intent(this, RoosterDagNew.class));
        tabSpec5.setIndicator(getString(R.string.week), resources.getDrawable(R.drawable.roostertab)).setContent(new Intent(this, RoosterWeekNew.class));
        tabHost.addTab(tabSpec1);
        tabHost.addTab(tabSpec2);
        tabHost.addTab(tabSpec3);
        tabHost.addTab(tabSpec4);
        tabHost.addTab(tabSpec5);
        ((TabWidget) findViewById(16908307)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
        ((FrameLayout) findViewById(16908305)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_to_left));
        tabHost.setCurrentTab(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_STARTSCHERM", 0));
        ToonEersteKeerReleaseNotes();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onResume() {
        super.onResume();
    }

    public void setHuisWerkCount(int iCnt) {
        ((TextView) ((RelativeLayout) getTabWidget().getChildAt(1)).getChildAt(1)).setText(" (" + iCnt + ")" + getString(R.string.app_name));
    }

    public void setToetsCount(int iCnt) {
        ((TextView) ((RelativeLayout) getTabWidget().getChildAt(2)).getChildAt(1)).setText(" (" + iCnt + ")" + getString(R.string.toets));
    }

    public Calendar getCurViewDate() {
        return mCurViewDate;
    }

    public void setCurViewDate(Calendar newViewDate) {
        mCurViewDate = (Calendar) newViewDate.clone();
    }

    public void ToonReleaseNotes() {
        startActivity(new Intent(myContext, WhatsNew.class));
    }

    public void ToonBackupDeleted() {
        new AlertDialog.Builder(this).setMessage(getString(R.string.backupdeleted)).setTitle("Backup").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HuisWerkMain.this.startActivity(new Intent(HuisWerkMain.myContext, BackupRestore.class));
            }
        }).show();
    }

    public void ToonEersteKeerReleaseNotes() {
        int newRelNotesVersion = 0;
        try {
            newRelNotesVersion = getPackageManager().getPackageInfo(getApplicationInfo().packageName, 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (this.RelNotesVersion < newRelNotesVersion) {
            this.RelNotesVersion = newRelNotesVersion;
            this.db.setRelNotesVersion(this.RelNotesVersion);
            UpdateWidgets(getApplicationContext());
        }
    }

    public void AddMissingDBRecords() {
        this.RelNotesVersion = this.db.getRelNotesVersionFirstTime();
        if (this.RelNotesVersion < 18 && this.RelNotesVersion > 0) {
            this.db.CreateVakInfoNewTable();
            HuiswerkDatabase.VakInfoOldCursor vioC = this.db.getVakInfoOld();
            while (!vioC.isAfterLast()) {
                this.db.addVakInfo(vioC.getColVakId(), (long) vioC.getColKleur().intValue(), "", "", "");
                vioC.moveToNext();
            }
            vioC.close();
            if (this.db.deleteBackup().booleanValue()) {
                ToonBackupDeleted();
            }
        }
        if (this.RelNotesVersion < 24 && this.RelNotesVersion > 0) {
            HuiswerkDatabase.VakInfoCursorAll viC = this.db.getVakInfoAll();
            while (!viC.isAfterLast()) {
                this.db.editVakInfo(viC.getColVakId(), (long) viC.getColKleur().intValue(), "", "", "");
                viC.moveToNext();
            }
            viC.close();
        }
        if (this.RelNotesVersion < 25 && this.RelNotesVersion > 0) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
            editor.putInt("HW_PREF_STARTSCHERM", 0);
            editor.commit();
            this.db.CreateHuiswerkFotoTable();
            if (this.db.deleteBackup().booleanValue()) {
                ToonBackupDeleted();
            }
        }
        if (this.RelNotesVersion < 28 && this.RelNotesVersion > 0) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (prefs.getBoolean("HW_PREF_2WEEK", false)) {
                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putInt("HW_PREF_TIMETABLETYPE", 1);
                editor2.commit();
            } else {
                SharedPreferences.Editor editor3 = prefs.edit();
                editor3.putInt("HW_PREF_TIMETABLETYPE", 0);
                editor3.commit();
            }
        }
        if (this.RelNotesVersion < 30 && this.RelNotesVersion > 0) {
            this.db.CreateRoosterLesTable();
            HuiswerkDatabase.OldRoosterCursor oldRC = this.db.getOldRooster();
            while (!oldRC.isAfterLast()) {
                HuiswerkDatabase.LesUurCursor uC = this.db.getLesUur((long) oldRC.getColUur());
                this.db.addRooster(oldRC.getColDag(), (long) oldRC.getColUur(), (long) uC.getColStart(), (long) uC.getColStop(), oldRC.getColVakId(), oldRC.getColLokaal(), (long) oldRC.getColDatum());
                uC.close();
                oldRC.moveToNext();
            }
            oldRC.close();
            if (this.db.deleteBackup().booleanValue()) {
                ToonBackupDeleted();
            }
        }
        if (this.RelNotesVersion < 32 && this.RelNotesVersion > 0 && this.db.CreateLesUrenIfNotExist().booleanValue() && this.db.deleteBackup().booleanValue()) {
            ToonBackupDeleted();
        }
        if (this.RelNotesVersion < 33 && this.RelNotesVersion > 0) {
            this.db.CreateVakantieTable();
            if (this.db.deleteBackup().booleanValue()) {
                ToonBackupDeleted();
            }
        }
    }

    private void AskAppMode() {
        mAppMode = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_APPMODE", 0);
        if (mAppMode == 0) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(HuisWerkMain.this.getApplicationContext()).edit();
                    switch (which) {
                        case -2:
                            editor.putInt("HW_PREF_APPMODE", 2);
                            editor.commit();
                            HuisWerkMain.mAppMode = 2;
                            return;
                        case -1:
                            editor.putInt("HW_PREF_APPMODE", 1);
                            editor.commit();
                            HuisWerkMain.mAppMode = 1;
                            return;
                        default:
                            return;
                    }
                }
            };
            new AlertDialog.Builder(this).setMessage(getString(R.string.modedialog)).setPositiveButton(getString(R.string.student), dialogClickListener).setNegativeButton(getString(R.string.docent), dialogClickListener).show();
        }
    }

    public static Boolean isAppModeStudent() {
        return mAppMode == 1;
    }

    public static void UpdateWidgets(Context ctxt) {
        ctxt.sendBroadcast(new Intent(ctxt, HuisWerkWidget.class).setAction(HuisWerkWidget.REFRESH));
        ctxt.sendBroadcast(new Intent(ctxt, HuisWerkWidget4x2.class).setAction(HuisWerkWidget4x2.REFRESH));
        ctxt.sendBroadcast(new Intent(ctxt, HuisWerkWidget4x4.class).setAction(HuisWerkWidget4x4.REFRESH));
        ctxt.sendBroadcast(new Intent(ctxt, HuisWerkWidget4x2HuisWerk.class).setAction(HuisWerkWidget4x2HuisWerk.REFRESH));
        ctxt.sendBroadcast(new Intent(ctxt, HuisWerkWidget4x4HuisWerk.class).setAction(HuisWerkWidget4x4HuisWerk.REFRESH));
    }

    public void ShowMainMenu() {
        getTabHost().setCurrentTab(0);
    }
}
