package klwinkel.huiswerk;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.widget.RemoteViews;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class HuisWerkWidget4x4HuisWerk extends AppWidgetProvider {
    public static final String DONE1 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_1";
    public static final String DONE2 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_2";
    public static final String DONE3 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_3";
    public static final String DONE4 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_4";
    public static final String DONE5 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_5";
    public static final String DONE6 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_6";
    public static final String DONE7 = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK_7";
    public static final String REFRESH = "klwinkel.huiswerk.huiswerkwidget.REFRESH4x4HUISWERK";
    public static boolean bTransparant;
    public static Calendar calRoosterDag;
    public static HuiswerkDatabase db;
    public static HuiswerkDatabase.HuiswerkDetailCursor hwC;
    public static HuiswerkDatabase.HuiswerkCursor hwCursor;
    public static long hw_id1 = 0;
    public static long hw_id2 = 0;
    public static long hw_id3 = 0;
    public static long hw_id4 = 0;
    public static long hw_id5 = 0;
    public static long hw_id6 = 0;
    public static long hw_id7 = 0;
    public static Integer iDatum = 0;
    public static Integer iVorigeDatum = 0;

    public void onReceive(Context ctxt, Intent intent) {
        if (REFRESH.equals(intent.getAction())) {
            DoUpdate(ctxt);
            return;
        }
        db = new HuiswerkDatabase(ctxt, false);
        if (!RefreshHuisWerk(ctxt, intent.getAction())) {
            super.onReceive(ctxt, intent);
        } else {
            HuisWerkMain.UpdateWidgets(ctxt);
        }
        db.close();
    }

    private void SetHuiswerkAf(long id) {
        if (id != 0) {
            hwC = db.getHuiswerkDetails(id);
            if (hwC.getCount() == 1) {
                db.editHuiswerk(hwC.getColHuiswerkId(), hwC.getColVakId(), hwC.getColHoofdstuk(), hwC.getColPagina(), hwC.getColOmschrijving(), 1, hwC.getColToets(), (long) hwC.getColDatum());
            }
            hwC.close();
        }
    }

    private boolean RefreshHuisWerk(Context context, String strDone) {
        if (DONE1.equals(strDone)) {
            SetHuiswerkAf(hw_id1);
            hw_id1 = 0;
            return true;
        } else if (DONE2.equals(strDone)) {
            SetHuiswerkAf(hw_id2);
            hw_id2 = 0;
            return true;
        } else if (DONE3.equals(strDone)) {
            SetHuiswerkAf(hw_id3);
            hw_id3 = 0;
            return true;
        } else if (DONE4.equals(strDone)) {
            SetHuiswerkAf(hw_id4);
            hw_id4 = 0;
            return true;
        } else if (DONE5.equals(strDone)) {
            SetHuiswerkAf(hw_id5);
            hw_id5 = 0;
            return true;
        } else if (DONE6.equals(strDone)) {
            SetHuiswerkAf(hw_id6);
            hw_id6 = 0;
            return true;
        } else if (!DONE7.equals(strDone)) {
            return false;
        } else {
            SetHuiswerkAf(hw_id7);
            hw_id7 = 0;
            return true;
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        DoUpdate(context);
    }

    public void DoUpdate(Context ctxt) {
        RemoteViews updateViews;
        db = new HuiswerkDatabase(ctxt, false);
        if (db.getRelNotesVersionFirstTime() < 33) {
            updateViews = new RemoteViews(ctxt.getPackageName(), (int) R.layout.widget4x4huiswerk);
        } else {
            db.Init(ctxt);
            updateViews = buildUpdate(ctxt);
        }
        db.close();
        AppWidgetManager.getInstance(ctxt).updateAppWidget(new ComponentName(ctxt, HuisWerkWidget4x4HuisWerk.class), updateViews);
    }

    private RemoteViews buildUpdate(Context context) {
        RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.widget4x4huiswerk);
        bTransparant = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getBoolean("HW_PREF_TRANSPARANT", false);
        calRoosterDag = Calendar.getInstance();
        hwCursor = db.getHuiswerk(HuiswerkDatabase.HuiswerkCursor.SortBy.datum);
        iVorigeDatum = 0;
        hw_id1 = buildHuiswerkLine(context, views, DONE1, R.id.h1_done, R.id.h1_1, R.id.h1_2, R.id.h1_3, R.id.h1_toets);
        hw_id2 = buildHuiswerkLine(context, views, DONE2, R.id.h2_done, R.id.h2_1, R.id.h2_2, R.id.h2_3, R.id.h2_toets);
        hw_id3 = buildHuiswerkLine(context, views, DONE3, R.id.h3_done, R.id.h3_1, R.id.h3_2, R.id.h3_3, R.id.h3_toets);
        hw_id4 = buildHuiswerkLine(context, views, DONE4, R.id.h4_done, R.id.h4_1, R.id.h4_2, R.id.h4_3, R.id.h4_toets);
        hw_id5 = buildHuiswerkLine(context, views, DONE5, R.id.h5_done, R.id.h5_1, R.id.h5_2, R.id.h5_3, R.id.h5_toets);
        hw_id6 = buildHuiswerkLine(context, views, DONE6, R.id.h6_done, R.id.h6_1, R.id.h6_2, R.id.h6_3, R.id.h6_toets);
        hw_id7 = buildHuiswerkLine(context, views, DONE7, R.id.h7_done, R.id.h7_1, R.id.h7_2, R.id.h7_3, R.id.h7_toets);
        hwCursor.close();
        if (hw_id1 == 0) {
            views.setTextViewText(R.id.h1_1, new StringBuilder().append(context.getString(R.string.geenhuiswerkpopup)));
            views.setViewVisibility(R.id.h1_1, 0);
        }
        views.setOnClickPendingIntent(R.id.llText, PendingIntent.getActivity(context, 0, new Intent(context, HuisWerkMain.class), 0));
        db.close();
        return views;
    }

    private long buildHuiswerkLine(Context context, RemoteViews views, String done, int id_done, int id_line_1, int id_line_2, int id_line_3, int id_toets) {
        if (bTransparant) {
            views.setImageViewResource(R.id.background, R.drawable.frame4x4trans);
        } else {
            views.setImageViewResource(R.id.background, R.drawable.frame4x4);
        }
        views.setImageViewResource(id_toets, 0);
        while (!hwCursor.isAfterLast() && hwCursor.getColKlaar() != 0) {
            hwCursor.moveToNext();
        }
        if (!hwCursor.isAfterLast()) {
            Integer iDatum2 = Integer.valueOf(hwCursor.getColDatum());
            String strDatum = (String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(iDatum2.intValue() / 10000).intValue() - 1900, Integer.valueOf((iDatum2.intValue() % 10000) / 100).intValue(), Integer.valueOf(iDatum2.intValue() % 100).intValue()));
            String strHoofdstuk = "";
            if (hwCursor.getColHoofdstuk().length() > 0) {
                strHoofdstuk = String.valueOf(context.getString(R.string.letterhoofdstuk)) + hwCursor.getColHoofdstuk();
            }
            String strPagina = "";
            if (hwCursor.getColPagina().length() > 0) {
                strPagina = String.valueOf(context.getString(R.string.letterpagina)) + hwCursor.getColPagina();
            }
            if (hwCursor.getColToets() > 0) {
                views.setImageViewResource(id_toets, R.drawable.toets);
            }
            if (iDatum2.intValue() > iVorigeDatum.intValue()) {
                iVorigeDatum = iDatum2;
                views.setTextViewText(id_line_1, new StringBuilder().append(strDatum));
            } else {
                views.setTextViewText(id_line_1, new StringBuilder().append(""));
            }
            views.setTextViewText(id_line_2, new StringBuilder().append(String.valueOf(hwCursor.getColVakNaam()) + " " + strHoofdstuk + " " + strPagina));
            views.setTextViewText(id_line_3, new StringBuilder().append(hwCursor.getColOmschrijving()));
            views.setTextColor(id_line_2, -1);
            if (db.getRelNotesVersion() >= 16) {
                HuiswerkDatabase.VakInfoCursor viC = db.getVakInfo((long) hwCursor.getColVakId());
                if (viC.getCount() > 0) {
                    views.setTextColor(id_line_2, viC.getColKleur().intValue());
                }
                viC.close();
            }
            long hw_id = hwCursor.getColHuiswerkId();
            Intent intent = new Intent(context, HuisWerkWidget4x4HuisWerk.class);
            intent.setAction(done);
            views.setOnClickPendingIntent(id_done, PendingIntent.getBroadcast(context, 0, intent, 0));
            views.setViewVisibility(id_done, 0);
            views.setViewVisibility(id_line_1, 0);
            views.setViewVisibility(id_line_2, 0);
            views.setViewVisibility(id_line_3, 0);
            views.setViewVisibility(id_toets, 0);
            hwCursor.moveToNext();
            return hw_id;
        }
        views.setTextViewText(id_line_1, new StringBuilder().append(""));
        views.setTextViewText(id_line_2, new StringBuilder().append(""));
        views.setTextViewText(id_line_3, new StringBuilder().append(""));
        views.setViewVisibility(id_done, 4);
        views.setViewVisibility(id_line_1, 4);
        views.setViewVisibility(id_line_2, 4);
        views.setViewVisibility(id_line_3, 4);
        views.setViewVisibility(id_toets, 4);
        return 0;
    }
}
