package klwinkel.huiswerk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;

public class Handleiding extends Activity {
    private Handler mHandler = new Handler();
    private Runnable mUpdateKaart = new Runnable() {
        public void run() {
            Handleiding.this.mWebView.setInitialScale(1);
            Handleiding.this.mWebView.loadUrl("http://www.klwinkel.com/homework/index.html");
        }
    };
    WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.webview);
        this.mWebView = (WebView) findViewById(R.id.wv);
        this.mWebView.setInitialScale(200);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.getSettings().setUseWideViewPort(true);
    }

    public void GetKaart() {
        this.mWebView.loadData("\nLoading User Manual...", "text/html", "utf-8");
        this.mHandler.postDelayed(this.mUpdateKaart, 500);
    }

    public void onStart() {
        GetKaart();
        super.onStart();
    }
}
