package klwinkel.huiswerk;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditHuiswerk extends Activity {
    protected static final int CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE = 1;
    protected static final int SELECT_IMAGE_HOMEWORK_REQUEST_CODE = 2;
    private static Button btnCancelHuiswerk;
    /* access modifiers changed from: private */
    public static Button btnDatum;
    private static Button btnDeleteHuiswerk;
    private static Button btnNext;
    private static Button btnPicture;
    private static Button btnPrev;
    private static Button btnShowPicture;
    private static Button btnUpdateHuiswerk;
    /* access modifiers changed from: private */
    public static CheckBox chkKlaar;
    /* access modifiers changed from: private */
    public static CheckBox chkToets;
    /* access modifiers changed from: private */
    public static File huiswerkPhotoFile;
    /* access modifiers changed from: private */
    public static Calendar mCal = null;
    /* access modifiers changed from: private */
    public static String mFoto = "";
    /* access modifiers changed from: private */
    public static boolean mFotoSaved = true;
    /* access modifiers changed from: private */
    public static int mHuiswerkDatum = 0;
    /* access modifiers changed from: private */
    public static Integer mHuiswerkId = 0;
    /* access modifiers changed from: private */
    public static long mHuiswerkVakId = 0;
    private static int mPrefNumHours = 0;
    /* access modifiers changed from: private */
    public static Context myContext;
    /* access modifiers changed from: private */
    public static Spinner spnVak;
    private static ScrollView svMain;
    /* access modifiers changed from: private */
    public static EditText txtHoofdstuk;
    /* access modifiers changed from: private */
    public static EditText txtOmschrijving;
    /* access modifiers changed from: private */
    public static EditText txtPagina;
    private final View.OnClickListener btnCancelHuiswerkOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (EditHuiswerk.mFoto.length() > 0) {
                if (EditHuiswerk.mHuiswerkId.intValue() == 0) {
                    File file = new File(EditHuiswerk.mFoto);
                    if (file.exists()) {
                        file.delete();
                    }
                } else {
                    HuiswerkDatabase.HuiswerkFotoCursor chwf = EditHuiswerk.this.huiswerkDb.getHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
                    if (chwf.getCount() <= 0) {
                        File file2 = new File(EditHuiswerk.mFoto);
                        if (file2.exists()) {
                            file2.delete();
                        }
                    } else if (chwf.getColFoto().compareTo(EditHuiswerk.mFoto) != 0) {
                        File file3 = new File(EditHuiswerk.mFoto);
                        if (file3.exists()) {
                            file3.delete();
                        }
                    }
                    chwf.close();
                }
            }
            EditHuiswerk.this.finish();
        }
    };
    private final View.OnClickListener btnDeleteHuiswerkOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditHuiswerk.this.huiswerkDb.deleteHuiswerk((long) EditHuiswerk.mHuiswerkId.intValue());
            HuiswerkDatabase.HuiswerkFotoCursor chwf = EditHuiswerk.this.huiswerkDb.getHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
            if (chwf.getCount() > 0) {
                File file = new File(chwf.getColFoto());
                if (file.exists()) {
                    file.delete();
                }
                EditHuiswerk.this.huiswerkDb.deleteHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
            }
            chwf.close();
            HuisWerkMain.UpdateWidgets(EditHuiswerk.myContext);
            EditHuiswerk.this.finish();
        }
    };
    private final View.OnClickListener btnNextOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditHuiswerk.mHuiswerkVakId = ((Vak) EditHuiswerk.spnVak.getSelectedItem()).id;
            if (EditHuiswerk.this.zoekVolgendeVakDatum().booleanValue()) {
                EditHuiswerk.this.InitVak();
                EditHuiswerk.this.InitDatum();
            }
        }
    };
    private final View.OnClickListener btnPictureOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case -2:
                            EditHuiswerk.this.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), EditHuiswerk.SELECT_IMAGE_HOMEWORK_REQUEST_CODE);
                            return;
                        case -1:
                            File path = new File(Environment.getExternalStorageDirectory(), EditHuiswerk.this.getPackageName());
                            if (!path.exists()) {
                                path.mkdir();
                            }
                            EditHuiswerk.huiswerkPhotoFile = new File(path, String.valueOf(String.valueOf(System.currentTimeMillis())) + ".jpg");
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            intent.putExtra("output", Uri.fromFile(EditHuiswerk.huiswerkPhotoFile));
                            EditHuiswerk.this.startActivityForResult(intent, EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE);
                            return;
                        default:
                            return;
                    }
                }
            };
            new AlertDialog.Builder(EditHuiswerk.myContext).setMessage(EditHuiswerk.this.getString(R.string.maakfoto)).setPositiveButton(EditHuiswerk.this.getString(R.string.camera), dialogClickListener).setNegativeButton(EditHuiswerk.this.getString(R.string.gallery), dialogClickListener).show();
        }
    };
    private final View.OnClickListener btnPrevOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditHuiswerk.mHuiswerkVakId = ((Vak) EditHuiswerk.spnVak.getSelectedItem()).id;
            if (EditHuiswerk.this.zoekVorigeVakDatum().booleanValue()) {
                EditHuiswerk.this.InitVak();
                EditHuiswerk.this.InitDatum();
            }
        }
    };
    private final View.OnClickListener btnShowPictureOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (EditHuiswerk.mFoto.length() > 0) {
                Intent i = new Intent(EditHuiswerk.this, HuiswerkFoto.class);
                Bundle b = new Bundle();
                b.putString("_foto", EditHuiswerk.mFoto);
                b.putString("_title", String.valueOf(((Vak) EditHuiswerk.spnVak.getSelectedItem()).vakName) + " - " + ((Object) EditHuiswerk.btnDatum.getText()));
                i.putExtras(b);
                EditHuiswerk.this.startActivity(i);
            }
        }
    };
    private final View.OnClickListener btnUpdateHuiswerkOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String str1;
            Vak vak = (Vak) EditHuiswerk.spnVak.getSelectedItem();
            EditHuiswerk.mHuiswerkVakId = vak.id;
            Integer iDatum = Integer.valueOf((EditHuiswerk.mCal.get(EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) * 10000) + (EditHuiswerk.mCal.get(EditHuiswerk.SELECT_IMAGE_HOMEWORK_REQUEST_CODE) * 100) + EditHuiswerk.mCal.get(5));
            Integer iKlaar = 0;
            if (EditHuiswerk.chkKlaar.isChecked()) {
                iKlaar = Integer.valueOf((int) EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE);
            }
            Integer iToets = 0;
            if (EditHuiswerk.chkToets.isChecked()) {
                iToets = Integer.valueOf((int) EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE);
            }
            if (vak.id <= 0) {
                if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                    str1 = EditHuiswerk.this.getString(R.string.geenvakgekozen);
                } else {
                    str1 = EditHuiswerk.this.getString(R.string.geenvakgekozen);
                }
                Toast.makeText(EditHuiswerk.this, str1, EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE).show();
                return;
            }
            if (EditHuiswerk.mHuiswerkId.intValue() > 0) {
                EditHuiswerk.this.huiswerkDb.editHuiswerk((long) EditHuiswerk.mHuiswerkId.intValue(), vak.id, EditHuiswerk.txtHoofdstuk.getText().toString(), EditHuiswerk.txtPagina.getText().toString(), EditHuiswerk.txtOmschrijving.getText().toString(), (long) iKlaar.intValue(), (long) iToets.intValue(), (long) iDatum.intValue());
            } else {
                EditHuiswerk.this.huiswerkDb.addHuiswerk(vak.id, EditHuiswerk.txtHoofdstuk.getText().toString(), EditHuiswerk.txtPagina.getText().toString(), EditHuiswerk.txtOmschrijving.getText().toString(), (long) iKlaar.intValue(), (long) iToets.intValue(), (long) iDatum.intValue());
                HuiswerkDatabase.HuiswerkDayCursor hwC = EditHuiswerk.this.huiswerkDb.getHuiswerkDay((long) iDatum.intValue());
                while (true) {
                    if (hwC.isAfterLast()) {
                        break;
                    } else if (((long) hwC.getColVakId()) == vak.id) {
                        EditHuiswerk.mHuiswerkId = Integer.valueOf(hwC.getColHuiswerkId());
                        break;
                    } else {
                        hwC.moveToNext();
                    }
                }
                hwC.close();
            }
            if (EditHuiswerk.mFoto.length() <= 0) {
                HuiswerkDatabase.HuiswerkFotoCursor chwf = EditHuiswerk.this.huiswerkDb.getHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
                if (chwf.getCount() > 0) {
                    File file = new File(chwf.getColFoto());
                    if (file.exists()) {
                        file.delete();
                    }
                    EditHuiswerk.this.huiswerkDb.deleteHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
                }
                chwf.close();
            } else if (EditHuiswerk.mHuiswerkId.intValue() > 0) {
                HuiswerkDatabase.HuiswerkFotoCursor chwf2 = EditHuiswerk.this.huiswerkDb.getHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue());
                if (chwf2.getCount() <= 0) {
                    EditHuiswerk.this.huiswerkDb.addHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue(), EditHuiswerk.mFoto);
                } else if (chwf2.getColFoto().compareTo(EditHuiswerk.mFoto) != 0) {
                    File file2 = new File(chwf2.getColFoto());
                    if (file2.exists()) {
                        file2.delete();
                    }
                    EditHuiswerk.this.huiswerkDb.editHuiswerkFoto((long) EditHuiswerk.mHuiswerkId.intValue(), EditHuiswerk.mFoto);
                }
                chwf2.close();
                EditHuiswerk.mFotoSaved = true;
            }
            HuisWerkMain.UpdateWidgets(EditHuiswerk.myContext);
            EditHuiswerk.this.finish();
        }
    };
    private final View.OnClickListener datumOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new DatePickerDialog(EditHuiswerk.this, EditHuiswerk.this.mDateSetListener, EditHuiswerk.mCal.get(EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE), EditHuiswerk.mCal.get(EditHuiswerk.SELECT_IMAGE_HOMEWORK_REQUEST_CODE), EditHuiswerk.mCal.get(5)).show();
        }
    };
    /* access modifiers changed from: private */
    public HuiswerkDatabase huiswerkDb;
    /* access modifiers changed from: private */
    public DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            EditHuiswerk.mCal.set(EditHuiswerk.CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE, year);
            EditHuiswerk.mCal.set(EditHuiswerk.SELECT_IMAGE_HOMEWORK_REQUEST_CODE, monthOfYear);
            EditHuiswerk.mCal.set(5, dayOfMonth);
            EditHuiswerk.btnDatum.setText((String) DateFormat.format("dd MMMM yyyy", new Date(year - 1900, monthOfYear, dayOfMonth)));
        }
    };

    private class Vak {
        public long id;
        public String vakName;

        Vak(long id2, String vakName2) {
            this.id = id2;
            this.vakName = vakName2;
        }

        public String toString() {
            return this.vakName;
        }
    }

    public class spnVakOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public spnVakOnItemSelectedListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            Vak vak = (Vak) EditHuiswerk.spnVak.getSelectedItem();
            if (vak.id == -2) {
                EditHuiswerk.this.startActivity(new Intent(EditHuiswerk.this, EditVak.class));
            } else if (vak.id != EditHuiswerk.mHuiswerkVakId) {
                EditHuiswerk.mHuiswerkVakId = vak.id;
                EditHuiswerk.mCal = Calendar.getInstance();
                EditHuiswerk.mHuiswerkDatum = 0;
                if (EditHuiswerk.this.zoekVolgendeVakDatum().booleanValue()) {
                    EditHuiswerk.this.InitDatum();
                }
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        String str1;
        String str12;
        String str2;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edithuiswerk);
        myContext = this;
        this.huiswerkDb = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        mCal = Calendar.getInstance();
        mFotoSaved = true;
        TextView tv = (TextView) findViewById(R.id.lblVak);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str1 = getString(R.string.vak);
        } else {
            str1 = getString(R.string.klas);
        }
        tv.setText(str1);
        svMain = (ScrollView) findViewById(R.id.svMain);
        spnVak = (Spinner) findViewById(R.id.spnVak);
        btnDatum = (Button) findViewById(R.id.btnDatum);
        txtHoofdstuk = (EditText) findViewById(R.id.txtHoofdstuk);
        txtPagina = (EditText) findViewById(R.id.txtPagina);
        txtOmschrijving = (EditText) findViewById(R.id.txtOmschrijving);
        chkKlaar = (CheckBox) findViewById(R.id.checkKlaar);
        chkToets = (CheckBox) findViewById(R.id.checkToets);
        btnUpdateHuiswerk = (Button) findViewById(R.id.btn1);
        btnDeleteHuiswerk = (Button) findViewById(R.id.btn2);
        btnCancelHuiswerk = (Button) findViewById(R.id.btn3);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrev = (Button) findViewById(R.id.btnPrev);
        btnNext.setText(" >> ");
        btnPrev.setText(" << ");
        btnPicture = (Button) findViewById(R.id.btnTakePicture);
        btnShowPicture = (Button) findViewById(R.id.btnShowPicture);
        btnUpdateHuiswerk.setText(getString(R.string.opslaan));
        btnDeleteHuiswerk.setText(getString(R.string.verwijderen));
        btnCancelHuiswerk.setText(getString(R.string.annuleren));
        btnUpdateHuiswerk.setOnClickListener(this.btnUpdateHuiswerkOnClick);
        btnDeleteHuiswerk.setOnClickListener(this.btnDeleteHuiswerkOnClick);
        btnCancelHuiswerk.setOnClickListener(this.btnCancelHuiswerkOnClick);
        btnPicture.setOnClickListener(this.btnPictureOnClick);
        btnShowPicture.setOnClickListener(this.btnShowPictureOnClick);
        btnNext.setOnClickListener(this.btnNextOnClick);
        btnPrev.setOnClickListener(this.btnPrevOnClick);
        btnDatum.setOnClickListener(this.datumOnClick);
        List<Vak> vakkenList = new ArrayList<>();
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str12 = getString(R.string.kieseenvak);
        } else {
            str12 = getString(R.string.kieseenklas);
        }
        vakkenList.add(new Vak(-1, str12));
        HuiswerkDatabase.VakkenCursor cVak = this.huiswerkDb.getVakken(HuiswerkDatabase.VakkenCursor.SortBy.naam);
        for (int i = 0; i < cVak.getCount(); i += CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) {
            cVak.moveToPosition(i);
            vakkenList.add(new Vak((long) cVak.getColVakkenId().intValue(), cVak.getColNaam()));
        }
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str2 = getString(R.string.vaktoevoegen);
        } else {
            str2 = getString(R.string.klastoevoegen);
        }
        vakkenList.add(new Vak(-2, str2));
        cVak.close();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, vakkenList);
        arrayAdapter.setDropDownViewResource(17367049);
        spnVak.setAdapter((SpinnerAdapter) arrayAdapter);
        spnVak.setOnItemSelectedListener(new spnVakOnItemSelectedListener());
        mPrefNumHours = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_NUMHOURS", 10);
        if (savedInstanceState == null) {
            Bundle bIn = getIntent().getExtras();
            mFoto = "";
            mHuiswerkId = 0;
            mHuiswerkVakId = 0;
            mHuiswerkDatum = 0;
            Boolean bToets = false;
            if (bIn != null) {
                mHuiswerkId = Integer.valueOf(bIn.getInt("_id"));
                mHuiswerkVakId = (long) Integer.valueOf(bIn.getInt("_vakid")).intValue();
                mHuiswerkDatum = Integer.valueOf(bIn.getInt("_datum")).intValue();
                bToets = Boolean.valueOf(bIn.getBoolean("_toets"));
            }
            if (mHuiswerkId.intValue() > 0) {
                HuiswerkDatabase.HuiswerkDetailCursor hwC = this.huiswerkDb.getHuiswerkDetails(mHuiswerkId.longValue());
                txtHoofdstuk.setText(hwC.getColHoofdstuk());
                txtPagina.setText(hwC.getColPagina());
                txtOmschrijving.setText(hwC.getColOmschrijving());
                if (hwC.getColToets() != 0) {
                    chkToets.setChecked(true);
                }
                if (hwC.getColKlaar() != 0) {
                    chkKlaar.setChecked(true);
                }
                mHuiswerkDatum = hwC.getColDatum();
                mHuiswerkVakId = hwC.getColVakId();
                hwC.close();
                HuiswerkDatabase.HuiswerkFotoCursor chwf = this.huiswerkDb.getHuiswerkFoto((long) mHuiswerkId.intValue());
                if (chwf.getCount() > 0) {
                    mFoto = chwf.getColFoto();
                }
                chwf.close();
                InitVak();
                InitDatum();
                InitFoto();
            } else {
                if (mHuiswerkVakId == 0) {
                    mHuiswerkDatum = 0;
                    zoekHuidigVak();
                }
                if (mHuiswerkDatum == 0) {
                    zoekVolgendeVakDatum();
                }
                InitVak();
                InitDatum();
                InitFoto();
            }
            if (bToets.booleanValue()) {
                chkToets.setChecked(true);
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 14 */
    /* access modifiers changed from: private */
    public Boolean zoekVolgendeVakDatum() {
        Calendar tempCal = (Calendar) mCal.clone();
        if (mHuiswerkVakId == 0) {
            return false;
        }
        for (int i = 0; i < 31; i += CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) {
            tempCal.add(5, CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE);
            int temp_datum = (tempCal.get(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) * 10000) + (tempCal.get(SELECT_IMAGE_HOMEWORK_REQUEST_CODE) * 100) + tempCal.get(5);
            HuiswerkDatabase.RoosterCursorDagVak rdvC = this.huiswerkDb.getRoosterDagVak(this.huiswerkDb.GetRoosterDagLong(getApplicationContext(), tempCal), mHuiswerkVakId);
            if (rdvC.getCount() > 0) {
                rdvC.close();
                mCal = (Calendar) tempCal.clone();
                mHuiswerkDatum = temp_datum;
                return true;
            }
            rdvC.close();
            HuiswerkDatabase.RoosterWijzigingCursorDatumVak rwCdv = this.huiswerkDb.getRoosterWijzigingDatumVak((long) temp_datum, mHuiswerkVakId);
            if (rwCdv.getCount() > 0) {
                rwCdv.close();
                mCal = (Calendar) tempCal.clone();
                mHuiswerkDatum = temp_datum;
                return true;
            }
            rwCdv.close();
        }
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 14 */
    /* access modifiers changed from: private */
    public Boolean zoekVorigeVakDatum() {
        Calendar tempCal = (Calendar) mCal.clone();
        if (mHuiswerkVakId == 0) {
            return false;
        }
        for (int i = 0; i < 31; i += CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) {
            tempCal.add(5, -1);
            int temp_datum = (tempCal.get(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) * 10000) + (tempCal.get(SELECT_IMAGE_HOMEWORK_REQUEST_CODE) * 100) + tempCal.get(5);
            HuiswerkDatabase.RoosterCursorDagVak rdvC = this.huiswerkDb.getRoosterDagVak(this.huiswerkDb.GetRoosterDagLong(getApplicationContext(), tempCal), mHuiswerkVakId);
            if (rdvC.getCount() > 0) {
                rdvC.close();
                mCal = (Calendar) tempCal.clone();
                mHuiswerkDatum = temp_datum;
                return true;
            }
            rdvC.close();
            HuiswerkDatabase.RoosterWijzigingCursorDatumVak rwCdv = this.huiswerkDb.getRoosterWijzigingDatumVak((long) temp_datum, mHuiswerkVakId);
            if (rwCdv.getCount() > 0) {
                rwCdv.close();
                mCal = (Calendar) tempCal.clone();
                mHuiswerkDatum = temp_datum;
                return true;
            }
            rwCdv.close();
        }
        return false;
    }

    private void zoekHuidigVak() {
        Calendar cal = Calendar.getInstance();
        Integer currDatum = Integer.valueOf((cal.get(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) * 10000) + (cal.get(SELECT_IMAGE_HOMEWORK_REQUEST_CODE) * 100) + cal.get(5));
        int iCurLesson = 0;
        Integer curTime = Integer.valueOf((cal.get(11) * 100) + cal.get(12));
        HuiswerkDatabase.RoosterCursorNu nuCursor = this.huiswerkDb.getRoosterNu(this.huiswerkDb.GetRoosterDagLong(this, cal));
        while (true) {
            if (!nuCursor.isAfterLast()) {
                if (curTime.intValue() >= nuCursor.getColBegin() && curTime.intValue() <= nuCursor.getColEinde()) {
                    iCurLesson = nuCursor.getColUur();
                    break;
                }
                nuCursor.moveToNext();
            } else {
                break;
            }
        }
        if (iCurLesson > 0) {
            HuiswerkDatabase.RoosterCursor roosterWijzigingCursor = this.huiswerkDb.getRoosterWijziging((long) iCurLesson, (long) currDatum.intValue());
            if (roosterWijzigingCursor.getCount() > 0) {
                mHuiswerkVakId = roosterWijzigingCursor.getColVakId();
            } else {
                HuiswerkDatabase.RoosterCursor rC = this.huiswerkDb.getRooster(this.huiswerkDb.GetRoosterDagLong(getApplicationContext(), cal), (long) iCurLesson);
                if (rC.getCount() > 0) {
                    mHuiswerkVakId = rC.getColVakId();
                }
                rC.close();
            }
            roosterWijzigingCursor.close();
        }
    }

    public void InitFoto() {
        if (mFoto.length() > 0) {
            btnShowPicture.setVisibility(0);
        } else {
            btnShowPicture.setVisibility(8);
        }
    }

    public void InitVak() {
        Vak vak = (Vak) spnVak.getSelectedItem();
        if (mHuiswerkVakId > 0 && mHuiswerkVakId != vak.id) {
            for (int i = 0; i < spnVak.getCount(); i += CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) {
                if (((Vak) spnVak.getItemAtPosition(i)).id == mHuiswerkVakId) {
                    spnVak.setSelection(i);
                    return;
                }
            }
        }
    }

    public void InitDatum() {
        if (mHuiswerkDatum > 0) {
            Integer iYear = Integer.valueOf(mHuiswerkDatum / 10000);
            Integer iMonth = Integer.valueOf((mHuiswerkDatum % 10000) / 100);
            Integer iDay = Integer.valueOf(mHuiswerkDatum % 100);
            mCal.set(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE, iYear.intValue());
            mCal.set(SELECT_IMAGE_HOMEWORK_REQUEST_CODE, iMonth.intValue());
            mCal.set(5, iDay.intValue());
        } else {
            Integer valueOf = Integer.valueOf(mCal.get(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE));
            Integer valueOf2 = Integer.valueOf(mCal.get(SELECT_IMAGE_HOMEWORK_REQUEST_CODE));
            Integer.valueOf(mCal.get(5));
        }
        showDatum();
    }

    public void showDatum() {
        btnDatum.setText((String) DateFormat.format("  EEEE dd MMMM", new Date(mCal.get(CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE) - 1900, mCal.get(SELECT_IMAGE_HOMEWORK_REQUEST_CODE), mCal.get(5))));
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        if (mHuiswerkId.intValue() == 0) {
            btnDeleteHuiswerk.setEnabled(false);
        }
        InitFoto();
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
        this.huiswerkDb.close();
        System.gc();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE && resultCode == -1) {
            if (!mFotoSaved) {
                File file = new File(mFoto);
                if (file.exists()) {
                    file.delete();
                }
            }
            mFoto = huiswerkPhotoFile.getAbsolutePath();
            mFotoSaved = false;
            InitFoto();
        }
        if (requestCode == SELECT_IMAGE_HOMEWORK_REQUEST_CODE && resultCode == -1) {
            if (!mFotoSaved) {
                File file2 = new File(mFoto);
                if (file2.exists()) {
                    file2.delete();
                }
            }
            Uri selectedImage = data.getData();
            String[] filePathColumn = new String[CAPTURE_IMAGE_HOMEWORK_REQUEST_CODE];
            filePathColumn[0] = "_data";
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            String filePath = cursor.getString(cursor.getColumnIndex(filePathColumn[0]));
            cursor.close();
            File file3 = new File(Environment.getExternalStorageDirectory(), getPackageName());
            if (!file3.exists()) {
                file3.mkdir();
            }
            huiswerkPhotoFile = new File(file3, String.valueOf(String.valueOf(System.currentTimeMillis())) + ".jpg");
            try {
                FileInputStream fileInputStream = new FileInputStream(filePath);
                FileOutputStream fileOutputStream = new FileOutputStream(huiswerkPhotoFile.getAbsolutePath());
                byte[] buffer = new byte[1024];
                while (true) {
                    int length = fileInputStream.read(buffer);
                    if (length <= 0) {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        fileInputStream.close();
                        mFoto = huiswerkPhotoFile.getAbsolutePath();
                        mFotoSaved = false;
                        InitFoto();
                        return;
                    }
                    fileOutputStream.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
