package klwinkel.huiswerk;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import klwinkel.huiswerk.HuiswerkDatabase;

public class EditRoosterUur extends Activity {
    /* access modifiers changed from: private */
    public static Button btnBeginTijd;
    private static Button btnCancelRooster;
    private static Button btnDeleteRooster;
    /* access modifiers changed from: private */
    public static Button btnEindTijd;
    private static Button btnUpdateRooster;
    /* access modifiers changed from: private */
    public static Integer mBegin = 0;
    /* access modifiers changed from: private */
    public static Integer mDag = 0;
    /* access modifiers changed from: private */
    public static Integer mEinde = 0;
    private static Integer mRotating = 0;
    /* access modifiers changed from: private */
    public static Integer mUur = 0;
    /* access modifiers changed from: private */
    public static Integer mWeek = 0;
    /* access modifiers changed from: private */
    public static Context myContext;
    /* access modifiers changed from: private */
    public static Spinner spnDag;
    /* access modifiers changed from: private */
    public static Spinner spnUur;
    /* access modifiers changed from: private */
    public static Spinner spnVak;
    private static ScrollView svMain;
    /* access modifiers changed from: private */
    public static EditText txtLokaal;
    private final View.OnClickListener beginTijdOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new TimePickerDialog(EditRoosterUur.this, EditRoosterUur.this.mBeginTijdSetListener, EditRoosterUur.mBegin.intValue() / 100, EditRoosterUur.mBegin.intValue() % 100, DateFormat.is24HourFormat(EditRoosterUur.this.getApplicationContext())).show();
        }
    };
    private final View.OnClickListener btnCancelRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            EditRoosterUur.this.finish();
        }
    };
    private final View.OnClickListener btnDeleteRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            HuiswerkDatabase.RoosterCursor rC = EditRoosterUur.this.huiswerkDb.getRooster((long) (EditRoosterUur.mDag.intValue() + ((EditRoosterUur.mWeek.intValue() - 1) * 7)), (long) EditRoosterUur.mUur.intValue());
            if (rC.getCount() > 0) {
                EditRoosterUur.this.huiswerkDb.deleteRooster(rC.getColRoosterId());
            }
            rC.close();
            HuisWerkMain.UpdateWidgets(EditRoosterUur.myContext);
            EditRoosterUur.this.finish();
        }
    };
    private final View.OnClickListener btnUpdateRoosterOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            String str1;
            listItem vak = (listItem) EditRoosterUur.spnVak.getSelectedItem();
            EditRoosterUur.mDag = Integer.valueOf(EditRoosterUur.spnDag.getSelectedItemPosition() + 1);
            EditRoosterUur.mUur = Integer.valueOf(EditRoosterUur.spnUur.getSelectedItemPosition() + 1);
            if (vak.id <= 0) {
                if (HuisWerkMain.isAppModeStudent().booleanValue()) {
                    str1 = EditRoosterUur.this.getString(R.string.geenvakgekozen);
                } else {
                    str1 = EditRoosterUur.this.getString(R.string.geenklasgekozen);
                }
                Toast.makeText(EditRoosterUur.this, str1, 1).show();
                return;
            }
            HuiswerkDatabase.RoosterCursor rC = EditRoosterUur.this.huiswerkDb.getRooster((long) (EditRoosterUur.mDag.intValue() + ((EditRoosterUur.mWeek.intValue() - 1) * 7)), (long) EditRoosterUur.mUur.intValue());
            if (rC.getCount() > 0) {
                EditRoosterUur.this.huiswerkDb.editRooster(rC.getColRoosterId(), (long) (EditRoosterUur.mDag.intValue() + ((EditRoosterUur.mWeek.intValue() - 1) * 7)), (long) EditRoosterUur.mUur.intValue(), (long) EditRoosterUur.mBegin.intValue(), (long) EditRoosterUur.mEinde.intValue(), vak.id, EditRoosterUur.txtLokaal.getText().toString(), 0);
            } else {
                EditRoosterUur.this.huiswerkDb.addRooster((long) (EditRoosterUur.mDag.intValue() + ((EditRoosterUur.mWeek.intValue() - 1) * 7)), (long) EditRoosterUur.mUur.intValue(), (long) EditRoosterUur.mBegin.intValue(), (long) EditRoosterUur.mEinde.intValue(), vak.id, EditRoosterUur.txtLokaal.getText().toString(), 0);
            }
            rC.close();
            HuisWerkMain.UpdateWidgets(EditRoosterUur.myContext);
            EditRoosterUur.this.finish();
        }
    };
    private final View.OnClickListener eindTijdOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            new TimePickerDialog(EditRoosterUur.this, EditRoosterUur.this.mEindTijdSetListener, EditRoosterUur.mEinde.intValue() / 100, EditRoosterUur.mEinde.intValue() % 100, DateFormat.is24HourFormat(EditRoosterUur.this.getApplicationContext())).show();
        }
    };
    /* access modifiers changed from: private */
    public HuiswerkDatabase huiswerkDb;
    /* access modifiers changed from: private */
    public TimePickerDialog.OnTimeSetListener mBeginTijdSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditRoosterUur.mBegin = Integer.valueOf((hourOfDay * 100) + minute);
            EditRoosterUur.btnBeginTijd.setText(HwUtl.Time2String(hourOfDay, minute));
        }
    };
    /* access modifiers changed from: private */
    public TimePickerDialog.OnTimeSetListener mEindTijdSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            EditRoosterUur.mEinde = Integer.valueOf((hourOfDay * 100) + minute);
            EditRoosterUur.btnEindTijd.setText(HwUtl.Time2String(hourOfDay, minute));
        }
    };

    private class listItem {
        public long id;
        public String str;

        listItem(long id2, String str2) {
            this.id = id2;
            this.str = str2;
        }

        public String toString() {
            return this.str;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        String str1;
        String str12;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editroosteruur);
        myContext = this;
        this.huiswerkDb = new HuiswerkDatabase(this);
        getWindow().setSoftInputMode(3);
        TextView tv = (TextView) findViewById(R.id.lblVak);
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str1 = getString(R.string.vak);
        } else {
            str1 = getString(R.string.klas);
        }
        tv.setText(str1);
        svMain = (ScrollView) findViewById(R.id.svMain);
        spnDag = (Spinner) findViewById(R.id.spnDag);
        spnUur = (Spinner) findViewById(R.id.spnUur);
        spnVak = (Spinner) findViewById(R.id.spnVak);
        txtLokaal = (EditText) findViewById(R.id.txtLokaal);
        btnBeginTijd = (Button) findViewById(R.id.btnBeginTijd);
        btnEindTijd = (Button) findViewById(R.id.btnEindTijd);
        btnUpdateRooster = (Button) findViewById(R.id.btnUpdate);
        btnDeleteRooster = (Button) findViewById(R.id.btnDelete);
        btnCancelRooster = (Button) findViewById(R.id.btnCancel);
        btnBeginTijd.setOnClickListener(this.beginTijdOnClick);
        btnEindTijd.setOnClickListener(this.eindTijdOnClick);
        btnUpdateRooster.setOnClickListener(this.btnUpdateRoosterOnClick);
        btnDeleteRooster.setOnClickListener(this.btnDeleteRoosterOnClick);
        btnCancelRooster.setOnClickListener(this.btnCancelRoosterOnClick);
        if (savedInstanceState == null) {
            Bundle bIn = getIntent().getExtras();
            mWeek = 0;
            mDag = 0;
            mUur = 0;
            mBegin = 0;
            mEinde = 0;
            mRotating = 0;
            if (bIn != null) {
                mWeek = Integer.valueOf(bIn.getInt("_week"));
                mDag = Integer.valueOf(bIn.getInt("_dag"));
                mUur = Integer.valueOf(bIn.getInt("_uur"));
                mRotating = Integer.valueOf(bIn.getInt("_rotating"));
            }
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Integer iPrefNumHours = Integer.valueOf(prefs.getInt("HW_PREF_NUMHOURS", 10));
        Integer iPrefDaysRotating = Integer.valueOf(prefs.getInt("HW_PREF_NUMDAYSROTATING", 8));
        ArrayList arrayList = new ArrayList();
        if (mRotating.intValue() > 0) {
            for (int i = 1; i <= iPrefDaysRotating.intValue(); i++) {
                String str = String.format(": %d", Integer.valueOf(i));
                arrayList.add(new listItem((long) i, String.valueOf(getString(R.string.dag)) + str));
            }
        } else {
            arrayList.add(new listItem(1, DateUtils.getDayOfWeekString(1, 10)));
            arrayList.add(new listItem(2, DateUtils.getDayOfWeekString(2, 10)));
            arrayList.add(new listItem(3, DateUtils.getDayOfWeekString(3, 10)));
            arrayList.add(new listItem(4, DateUtils.getDayOfWeekString(4, 10)));
            arrayList.add(new listItem(5, DateUtils.getDayOfWeekString(5, 10)));
            arrayList.add(new listItem(6, DateUtils.getDayOfWeekString(6, 10)));
            arrayList.add(new listItem(7, DateUtils.getDayOfWeekString(7, 10)));
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, arrayList);
        arrayAdapter.setDropDownViewResource(17367049);
        spnDag.setAdapter((SpinnerAdapter) arrayAdapter);
        spnDag.setOnItemSelectedListener(new spnDagOnItemSelectedListener());
        List<listItem> urenList = new ArrayList<>();
        for (int i2 = 1; i2 <= iPrefNumHours.intValue(); i2++) {
            String str2 = String.format(": %d", Integer.valueOf(i2));
            urenList.add(new listItem((long) i2, String.valueOf(getString(R.string.lesuur)) + str2));
        }
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, urenList);
        arrayAdapter2.setDropDownViewResource(17367049);
        spnUur.setAdapter((SpinnerAdapter) arrayAdapter2);
        spnUur.setOnItemSelectedListener(new spnUurOnItemSelectedListener());
        List<listItem> vakkenList = new ArrayList<>();
        if (HuisWerkMain.isAppModeStudent().booleanValue()) {
            str12 = getString(R.string.kieseenvak);
        } else {
            str12 = getString(R.string.kieseenklas);
        }
        vakkenList.add(new listItem(-1, str12));
        HuiswerkDatabase.VakkenCursor cVak = this.huiswerkDb.getVakken(HuiswerkDatabase.VakkenCursor.SortBy.naam);
        for (int i3 = 0; i3 < cVak.getCount(); i3++) {
            cVak.moveToPosition(i3);
            vakkenList.add(new listItem((long) cVak.getColVakkenId().intValue(), cVak.getColNaam()));
        }
        cVak.close();
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this, 17367048, vakkenList);
        arrayAdapter3.setDropDownViewResource(17367049);
        spnVak.setAdapter((SpinnerAdapter) arrayAdapter3);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            FillFields();
        }
    }

    public class spnUurOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public spnUurOnItemSelectedListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            EditRoosterUur.mUur = Integer.valueOf(EditRoosterUur.spnUur.getSelectedItemPosition() + 1);
            EditRoosterUur.this.SetBeginEinde();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class spnDagOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        public spnDagOnItemSelectedListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            EditRoosterUur.mDag = Integer.valueOf(EditRoosterUur.spnDag.getSelectedItemPosition() + 1);
            EditRoosterUur.this.SetBeginEinde();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* access modifiers changed from: private */
    public void SetBeginEinde() {
        HuiswerkDatabase.RoosterCursor rC = this.huiswerkDb.getRooster((long) (mDag.intValue() + ((mWeek.intValue() - 1) * 7)), (long) mUur.intValue());
        if (rC.getCount() > 0) {
            mEinde = Integer.valueOf(rC.getColBegin());
            btnBeginTijd.setText(HwUtl.Time2String(mBegin.intValue()));
            mEinde = Integer.valueOf(rC.getColEinde());
            btnEindTijd.setText(HwUtl.Time2String(mEinde.intValue()));
        } else {
            HuiswerkDatabase.LesUurCursor uC = this.huiswerkDb.getLesUur((long) mUur.intValue());
            mBegin = Integer.valueOf(uC.getColStart());
            btnBeginTijd.setText(HwUtl.Time2String(mBegin.intValue()));
            mEinde = Integer.valueOf(uC.getColStop());
            btnEindTijd.setText(HwUtl.Time2String(mEinde.intValue()));
            uC.close();
        }
        rC.close();
    }

    private void FillFields() {
        if (mDag.intValue() > 0) {
            spnDag.setSelection(mDag.intValue() - 1);
        }
        if (mUur.intValue() > 0) {
            spnUur.setSelection(mUur.intValue() - 1);
        }
        SetBeginEinde();
        SetLokaalVak();
    }

    private void SetLokaalVak() {
        HuiswerkDatabase.RoosterCursor rC = this.huiswerkDb.getRooster((long) (mDag.intValue() + ((mWeek.intValue() - 1) * 7)), (long) mUur.intValue());
        if (rC.getCount() > 0) {
            int i = 0;
            while (true) {
                if (i >= spnVak.getCount()) {
                    break;
                } else if (((listItem) spnVak.getItemAtPosition(i)).id == rC.getColVakId()) {
                    spnVak.setSelection(i);
                    break;
                } else {
                    i++;
                }
            }
            txtLokaal.setText(rC.getColLokaal());
            mBegin = Integer.valueOf(rC.getColBegin());
        } else {
            spnVak.setSelection(0);
            txtLokaal.setText("");
        }
        rC.close();
    }

    public void onResume() {
        svMain.setBackgroundColor(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("HW_PREF_ACHTERGROND", 0));
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.huiswerkDb.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
}
