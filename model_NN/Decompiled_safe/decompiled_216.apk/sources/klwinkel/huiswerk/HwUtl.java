package klwinkel.huiswerk;

import android.content.Context;
import android.text.format.DateFormat;
import java.util.Date;

public class HwUtl {
    public static String Time2StringContext(Context ctxt, int hour, int minute) {
        Date d = new Date();
        d.setHours(hour);
        d.setMinutes(minute);
        return DateFormat.getTimeFormat(ctxt).format(d);
    }

    public static String Time2String(int hour, int minute) {
        return Time2StringContext(HuisWerkMain.AppContext, hour, minute);
    }

    public static String Time2String(int hourminute) {
        return Time2String(hourminute / 100, hourminute % 100);
    }

    public static String Time2StringWidget(Context ctxtWidget, int hourminute) {
        return Time2StringContext(ctxtWidget.getApplicationContext(), hourminute / 100, hourminute % 100).replaceAll(" AM", "").replaceAll(" PM", "");
    }
}
