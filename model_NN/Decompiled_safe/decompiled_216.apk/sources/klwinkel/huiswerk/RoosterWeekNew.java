package klwinkel.huiswerk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Date;
import klwinkel.huiswerk.HuiswerkDatabase;

public class RoosterWeekNew extends Activity implements View.OnTouchListener {
    public static Context MainContext = null;
    private static float fMinimumCelHeight = 52.0f;
    private static int iKortsteLes = 0;
    private static int iLastLessonEndTime = 0;
    private static int iPrefNumDays = 7;
    private static int iPrefNumHours = 10;
    private static int iStartVroegsteLes = 0;
    private static LinearLayout mMain = null;
    private static ScrollView mScrollData = null;
    /* access modifiers changed from: private */
    public static LinearLayout mWeekData = null;
    private HuiswerkDatabase db;
    private float downXValue = 0.0f;
    /* access modifiers changed from: private */
    public Calendar firstDayOfWeek;
    private HuiswerkDatabase.HuiswerkVakCursor hwvCursor;
    private ImageButton lblAchteruit;
    private final View.OnClickListener lblAchteruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterWeekNew.this.PreviousWeek();
        }
    };
    private ImageButton lblVooruit;
    private final View.OnClickListener lblVooruitOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterWeekNew.this.NextWeek();
        }
    };
    private final View.OnClickListener lessonOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            int position = v.getId();
            int idag = position / 100;
            VakLokaal item = RoosterWeekNew.this.roosterData[idag][position - (idag * 100)];
            if (item.les) {
                Intent i = new Intent(RoosterWeekNew.this, EditHuiswerk.class);
                Bundle b = new Bundle();
                if (item.hwid != 0) {
                    b.putInt("_id", item.hwid);
                    i.putExtras(b);
                } else {
                    b.putInt("_vakid", item.vakid);
                    b.putInt("_datum", item.datum);
                    i.putExtras(b);
                }
                RoosterWeekNew.this.startActivity(i);
            }
        }
    };
    private int mCurDate = 0;
    private HuiswerkDatabase.RoosterCursorNu nuCursor = null;
    VakLokaal[][] roosterData;
    private Button roosterDatum;
    private final View.OnClickListener roosterDatumOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            RoosterWeekNew.this.firstDayOfWeek = Calendar.getInstance();
            ((HuisWerkMain) RoosterWeekNew.MainContext).setCurViewDate(RoosterWeekNew.this.firstDayOfWeek);
            RoosterWeekNew.this.FindFirstDateOfWeek();
            RoosterWeekNew.this.FillRooster();
            RoosterWeekNew.this.ToonRooster();
            RoosterWeekNew.mWeekData.startAnimation(AnimationUtils.loadAnimation(RoosterWeekNew.MainContext, R.anim.top_to_bottom));
        }
    };
    private HuiswerkDatabase.RoosterWijzigingCursorDatum rwCursor = null;

    private class VakLokaal {
        public int begin;
        public int dag;
        public int datum;
        public int dow;
        public int einde;
        public String email;
        public int hwid;
        public boolean klaar;
        public int kleur;
        public String leraar;
        public boolean les;
        public String lok;
        public String telefoon;
        public int temproosterid;
        public boolean tmp;
        public boolean toets;
        public int uur;
        public boolean vakantie;
        public int vakid;
        public String vakkort;
        public String vaklang;

        VakLokaal(boolean les2, boolean tmp2, boolean vakantie2, int dag2, int dow2, int uur2, int begin2, int einde2, String vakkort2, String vaklang2, String lok2, int vakid2, int datum2, int hwid2, boolean klaar2, boolean toets2, int temproosterid2, int kleur2, String leraar2, String telefoon2, String email2) {
            this.les = les2;
            this.tmp = tmp2;
            this.vakantie = vakantie2;
            this.dag = dag2;
            this.dow = dow2;
            this.uur = uur2;
            this.begin = begin2;
            this.einde = einde2;
            this.vakkort = vakkort2;
            this.vaklang = vaklang2;
            this.lok = lok2;
            this.vakid = vakid2;
            this.datum = datum2;
            this.hwid = hwid2;
            this.klaar = klaar2;
            this.toets = toets2;
            this.temproosterid = temproosterid2;
            this.kleur = kleur2;
            this.leraar = leraar2;
            this.telefoon = telefoon2;
            this.email = email2;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.roosterweeknew);
        MainContext = getParent();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        iPrefNumHours = prefs.getInt("HW_PREF_NUMHOURS", 10);
        iPrefNumDays = prefs.getInt("HW_PREF_NUMDAYS", 7);
        this.lblVooruit = (ImageButton) findViewById(R.id.lblVooruit);
        this.lblAchteruit = (ImageButton) findViewById(R.id.lblAchteruit);
        this.roosterDatum = (Button) findViewById(R.id.roosterdatum);
        mMain = (LinearLayout) findViewById(R.id.roostermain);
        mWeekData = (LinearLayout) findViewById(R.id.week);
        mWeekData.setOnTouchListener(this);
        mScrollData = (ScrollView) findViewById(R.id.scrollweek);
        mScrollData.setOnTouchListener(this);
        this.lblVooruit.setOnClickListener(this.lblVooruitOnClick);
        this.lblAchteruit.setOnClickListener(this.lblAchteruitOnClick);
        this.roosterDatum.setOnClickListener(this.roosterDatumOnClick);
        this.db = new HuiswerkDatabase(this);
        this.roosterData = (VakLokaal[][]) Array.newInstance(VakLokaal.class, 7, 20);
        this.firstDayOfWeek = Calendar.getInstance();
        this.mCurDate = (this.firstDayOfWeek.get(1) * 10000) + (this.firstDayOfWeek.get(2) * 100) + this.firstDayOfWeek.get(5);
        FindFirstDateOfWeek();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int position = v.getId();
        int idag = position / 100;
        VakLokaal item = this.roosterData[idag][position - (idag * 100)];
        if (item != null) {
            menu.setHeaderTitle(String.valueOf(item.vaklang) + " (" + item.leraar + ")");
            menu.add(0, position, 0, getString(R.string.huiswerkwijzigen));
            menu.add(0, position, 1, getString(R.string.roostereenmaligwijzigen));
            if (item.telefoon.length() > 0) {
                menu.add(0, position, 2, String.valueOf(getString(R.string.contextdial)) + item.telefoon);
            }
            if (item.email.length() > 0) {
                menu.add(0, position, 3, String.valueOf(getString(R.string.contextmail)) + item.email);
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getOrder()) {
            case 0:
                function1(item.getItemId());
                break;
            case 1:
                function2(item.getItemId());
                break;
            case 2:
                function3(item.getItemId());
                break;
            case 3:
                function4(item.getItemId());
                break;
            default:
                return false;
        }
        return true;
    }

    public void function1(int position) {
        int idag = position / 100;
        VakLokaal item = this.roosterData[idag][position - (idag * 100)];
        if (item.les) {
            Intent i = new Intent(this, EditHuiswerk.class);
            Bundle b = new Bundle();
            if (item.hwid != 0) {
                b.putInt("_id", item.hwid);
                i.putExtras(b);
            } else {
                b.putInt("_vakid", item.vakid);
                b.putInt("_datum", item.datum);
                i.putExtras(b);
            }
            startActivity(i);
        }
    }

    public void function2(int position) {
        int idag = position / 100;
        VakLokaal item = this.roosterData[idag][position - (idag * 100)];
        if (item.tmp) {
            Intent i = new Intent(this, EditRoosterWijzigingen.class);
            Bundle b = new Bundle();
            b.putInt("_id", item.temproosterid);
            i.putExtras(b);
            startActivity(i);
            return;
        }
        Intent i2 = new Intent(this, EditRoosterWijzigingen.class);
        Bundle b2 = new Bundle();
        b2.putInt("_datum", item.datum);
        b2.putInt("_uur", item.uur);
        i2.putExtras(b2);
        startActivity(i2);
    }

    public void function3(int position) {
        int idag = position / 100;
        VakLokaal item = this.roosterData[idag][position - (idag * 100)];
        Intent CallIntent = new Intent("android.intent.action.DIAL");
        CallIntent.setData(Uri.parse("tel:" + item.telefoon));
        startActivity(CallIntent);
    }

    public void function4(int position) {
        int idag = position / 100;
        VakLokaal item = this.roosterData[idag][position - (idag * 100)];
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType("plain/text");
        emailIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf((String) DateFormat.format("EEEE dd MMMM", new Date(Integer.valueOf(item.datum / 10000).intValue() - 1900, Integer.valueOf((item.datum % 10000) / 100).intValue(), Integer.valueOf(item.datum % 100).intValue()))) + "  " + getString(R.string.lesuur) + ": " + item.uur + "  " + item.vaklang + "  " + getString(R.string.lokaal) + ": " + item.lok);
        emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{item.email});
        startActivity(Intent.createChooser(emailIntent, "Send Email"));
    }

    /* access modifiers changed from: private */
    public void FindFirstDateOfWeek() {
        int iFirstSchoolDayOfWeek = this.db.FindFirstSchoolDayOfWeek(getApplicationContext());
        Calendar calRoosterDag = (Calendar) this.firstDayOfWeek.clone();
        int iDay = this.firstDayOfWeek.get(7);
        if (iFirstSchoolDayOfWeek > iDay) {
            if (iFirstSchoolDayOfWeek - iDay <= 2) {
                calRoosterDag.add(5, iFirstSchoolDayOfWeek - iDay);
            } else {
                calRoosterDag.add(5, (iFirstSchoolDayOfWeek - iDay) - 7);
            }
        }
        if (iFirstSchoolDayOfWeek < iDay) {
            if (iDay - iFirstSchoolDayOfWeek > 4) {
                calRoosterDag.add(5, 7 - (iDay - iFirstSchoolDayOfWeek));
            } else {
                calRoosterDag.add(5, iFirstSchoolDayOfWeek - iDay);
            }
        }
        this.firstDayOfWeek = (Calendar) calRoosterDag.clone();
    }

    /* access modifiers changed from: private */
    public void FillRooster() {
        this.roosterDatum.setText(String.valueOf(getString(R.string.week)) + String.format(" %d", Integer.valueOf(this.firstDayOfWeek.get(3))) + ((String) DateFormat.format(" (MMMM)", new Date(this.firstDayOfWeek.get(1) - 1900, this.firstDayOfWeek.get(2), this.firstDayOfWeek.get(5)))));
        Calendar calRoosterDag = (Calendar) this.firstDayOfWeek.clone();
        for (int iDag = 0; iDag < 7; iDag++) {
            int idayofweek = calRoosterDag.get(7);
            int idayofmonth = calRoosterDag.get(5);
            Integer iDatum = Integer.valueOf((calRoosterDag.get(1) * 10000) + (calRoosterDag.get(2) * 100) + calRoosterDag.get(5));
            boolean bVakantie = VakantieDagen.IsVakantieDag(calRoosterDag);
            for (int iuur = 0; iuur < 20; iuur++) {
                this.roosterData[iDag][iuur] = new VakLokaal(false, false, bVakantie, idayofmonth, idayofweek, iuur + 1, 0, 0, "", "", "", 0, iDatum.intValue(), 0, true, false, 0, -12303292, "", "", "");
            }
            if (!bVakantie) {
                this.nuCursor = this.db.getRoosterNu(this.db.GetRoosterDagLong(getApplicationContext(), calRoosterDag));
                while (!this.nuCursor.isAfterLast()) {
                    int iUur = this.nuCursor.getColUur();
                    this.roosterData[iDag][iUur - 1].les = true;
                    this.roosterData[iDag][iUur - 1].kleur = -1;
                    this.roosterData[iDag][iUur - 1].uur = iUur;
                    this.roosterData[iDag][iUur - 1].tmp = false;
                    this.roosterData[iDag][iUur - 1].begin = this.nuCursor.getColBegin();
                    this.roosterData[iDag][iUur - 1].einde = this.nuCursor.getColEinde();
                    this.roosterData[iDag][iUur - 1].vakkort = this.nuCursor.getColVakKort();
                    this.roosterData[iDag][iUur - 1].vaklang = this.nuCursor.getColVakNaam();
                    this.roosterData[iDag][iUur - 1].vakid = (int) this.nuCursor.getColVakId();
                    this.roosterData[iDag][iUur - 1].lok = this.nuCursor.getColLokaal();
                    this.hwvCursor = this.db.getHuiswerkVak((long) iDatum.intValue(), this.nuCursor.getColVakId());
                    if (this.hwvCursor.getCount() > 0) {
                        this.roosterData[iDag][iUur - 1].hwid = (int) this.hwvCursor.getColHuiswerkId();
                        this.roosterData[iDag][iUur - 1].klaar = this.hwvCursor.getColKlaar() == 1;
                        this.roosterData[iDag][iUur - 1].toets = this.hwvCursor.getColToets() == 1;
                    }
                    this.hwvCursor.close();
                    this.nuCursor.moveToNext();
                }
                this.nuCursor.close();
                this.rwCursor = this.db.getRoosterWijzigingDatum((long) iDatum.intValue());
                while (!this.rwCursor.isAfterLast()) {
                    int iUur2 = this.rwCursor.getColUur();
                    this.roosterData[iDag][iUur2 - 1].les = true;
                    this.roosterData[iDag][iUur2 - 1].kleur = -3355444;
                    this.roosterData[iDag][iUur2 - 1].uur = iUur2;
                    this.roosterData[iDag][iUur2 - 1].tmp = true;
                    this.roosterData[iDag][iUur2 - 1].begin = this.rwCursor.getColBegin();
                    this.roosterData[iDag][iUur2 - 1].einde = this.rwCursor.getColEinde();
                    this.roosterData[iDag][iUur2 - 1].temproosterid = (int) this.rwCursor.getColRoosterId();
                    this.roosterData[iDag][iUur2 - 1].vakkort = this.rwCursor.getColVakKort();
                    this.roosterData[iDag][iUur2 - 1].vaklang = this.rwCursor.getColVakNaam();
                    this.roosterData[iDag][iUur2 - 1].vakid = (int) this.rwCursor.getColVakId();
                    this.roosterData[iDag][iUur2 - 1].lok = this.rwCursor.getColLokaal();
                    this.hwvCursor = this.db.getHuiswerkVak((long) iDatum.intValue(), this.rwCursor.getColVakId());
                    if (this.hwvCursor.getCount() > 0) {
                        this.roosterData[iDag][iUur2 - 1].hwid = (int) this.hwvCursor.getColHuiswerkId();
                        this.roosterData[iDag][iUur2 - 1].klaar = this.hwvCursor.getColKlaar() == 1;
                        this.roosterData[iDag][iUur2 - 1].toets = this.hwvCursor.getColToets() == 1;
                    } else {
                        this.roosterData[iDag][iUur2 - 1].hwid = 0;
                        this.roosterData[iDag][iUur2 - 1].klaar = true;
                        this.roosterData[iDag][iUur2 - 1].toets = false;
                    }
                    this.hwvCursor.close();
                    this.rwCursor.moveToNext();
                }
                this.rwCursor.close();
                for (int iuur2 = 0; iuur2 < 20; iuur2++) {
                    if (this.roosterData[iDag][iuur2].vakid > 0) {
                        HuiswerkDatabase.VakInfoCursor viC = this.db.getVakInfo((long) this.roosterData[iDag][iuur2].vakid);
                        if (viC.getCount() > 0) {
                            this.roosterData[iDag][iuur2].kleur = viC.getColKleur().intValue();
                            this.roosterData[iDag][iuur2].leraar = viC.getColLeraar();
                            this.roosterData[iDag][iuur2].telefoon = viC.getColTelefoon();
                            this.roosterData[iDag][iuur2].email = viC.getColEmail();
                        }
                        viC.close();
                    }
                }
            }
            calRoosterDag.add(5, 1);
        }
        ZoekKortsteLes();
        ZoekEersteLes();
    }

    private void ZoekKortsteLes() {
        int iDuur;
        iKortsteLes = 9999;
        for (int iDag = 0; iDag < iPrefNumDays; iDag++) {
            for (int iUur = 0; iUur < iPrefNumHours; iUur++) {
                if (this.roosterData[iDag][iUur].les && this.roosterData[iDag][iUur].begin != 0 && this.roosterData[iDag][iUur].einde != 0 && this.roosterData[iDag][iUur].begin < this.roosterData[iDag][iUur].einde && (iDuur = (((this.roosterData[iDag][iUur].einde / 100) * 60) + (this.roosterData[iDag][iUur].einde % 100)) - (((this.roosterData[iDag][iUur].begin / 100) * 60) + (this.roosterData[iDag][iUur].begin % 100))) < iKortsteLes) {
                    iKortsteLes = iDuur;
                }
            }
        }
    }

    private void ZoekEersteLes() {
        iStartVroegsteLes = 9999;
        for (int iDag = 0; iDag < iPrefNumDays; iDag++) {
            int iUur = 0;
            while (true) {
                if (iUur < iPrefNumHours) {
                    if (this.roosterData[iDag][iUur].les && this.roosterData[iDag][iUur].begin < iStartVroegsteLes && this.roosterData[iDag][iUur].begin != 0) {
                        iStartVroegsteLes = this.roosterData[iDag][iUur].begin;
                        break;
                    }
                    iUur++;
                } else {
                    break;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void ToonRooster() {
        for (int iDag = 0; iDag < 7; iDag++) {
            int id_kopdag = 0;
            int id_coldag = 0;
            switch (iDag) {
                case 0:
                    id_coldag = R.id.coldag1;
                    id_kopdag = R.id.kopdag1;
                    break;
                case 1:
                    id_coldag = R.id.coldag2;
                    id_kopdag = R.id.kopdag2;
                    break;
                case 2:
                    id_coldag = R.id.coldag3;
                    id_kopdag = R.id.kopdag3;
                    break;
                case 3:
                    id_coldag = R.id.coldag4;
                    id_kopdag = R.id.kopdag4;
                    break;
                case 4:
                    id_coldag = R.id.coldag5;
                    id_kopdag = R.id.kopdag5;
                    break;
                case 5:
                    id_coldag = R.id.coldag6;
                    id_kopdag = R.id.kopdag6;
                    break;
                case 6:
                    id_coldag = R.id.coldag7;
                    id_kopdag = R.id.kopdag7;
                    break;
            }
            LinearLayout llKop = (LinearLayout) findViewById(id_kopdag);
            LinearLayout llCol = (LinearLayout) findViewById(id_coldag);
            llKop.removeAllViews();
            llCol.removeAllViews();
            if (iDag < iPrefNumDays) {
                llKop.addView(MaakKopView(iDag));
                iLastLessonEndTime = iStartVroegsteLes;
                for (int iUur = 0; iUur < iPrefNumHours; iUur++) {
                    if (this.roosterData[iDag][iUur].les) {
                        if (iLastLessonEndTime > 0 && this.roosterData[iDag][iUur].begin > iLastLessonEndTime) {
                            llCol.addView(MaakLegeView(iLastLessonEndTime, this.roosterData[iDag][iUur].begin));
                        }
                        llCol.addView(MaakUurView(iDag, iUur));
                        if (this.roosterData[iDag][iUur].einde > iLastLessonEndTime) {
                            iLastLessonEndTime = this.roosterData[iDag][iUur].einde;
                        } else {
                            iLastLessonEndTime += iKortsteLes;
                        }
                    }
                }
            } else {
                llKop.setVisibility(8);
                llCol.setVisibility(8);
            }
        }
    }

    private View MaakLegeView(int iBeginTime, int iEndTime) {
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.roosterweekcelleeg, (ViewGroup) null);
        RelativeLayout rlcel = (RelativeLayout) v.findViewById(R.id.rlcel);
        if (rlcel != null) {
            float scale = getResources().getDisplayMetrics().density;
            rlcel.setMinimumHeight((int) (((((float) ((((iEndTime / 100) * 60) + (iEndTime % 100)) - (((iBeginTime / 100) * 60) + (iBeginTime % 100)))) / ((float) iKortsteLes)) * fMinimumCelHeight * scale) + 0.5f));
        }
        return v;
    }

    private View MaakUurView(int iDag, int iUur) {
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate(R.layout.roosterweekcel, (ViewGroup) null);
        VakLokaal item = this.roosterData[iDag][iUur];
        RelativeLayout rlcel = (RelativeLayout) v.findViewById(R.id.rlcel);
        ImageView ivImagetop = (ImageView) v.findViewById(R.id.imagetop);
        TextView tvVak = (TextView) v.findViewById(R.id.vak);
        TextView tvLokaal = (TextView) v.findViewById(R.id.lokaal);
        TextView tvBegin = (TextView) v.findViewById(R.id.begin);
        TextView tvEinde = (TextView) v.findViewById(R.id.einde);
        if (rlcel != null) {
            GradientDrawable shape = (GradientDrawable) getResources().getDrawable(R.drawable.roundedgrid);
            shape.setColor(item.kleur);
            rlcel.setBackgroundDrawable(shape);
            float fDuur = (float) ((((item.einde / 100) * 60) + (item.einde % 100)) - (((item.begin / 100) * 60) + (item.begin % 100)));
            if (fDuur < ((float) iKortsteLes)) {
                fDuur = (float) iKortsteLes;
            }
            rlcel.setLayoutParams(new RelativeLayout.LayoutParams(-1, (int) (((fDuur / ((float) iKortsteLes)) * fMinimumCelHeight * getResources().getDisplayMetrics().density) + 0.5f)));
            rlcel.setOnClickListener(this.lessonOnClick);
            rlcel.setId((iDag * 100) + iUur);
            registerForContextMenu(rlcel);
            rlcel.setOnTouchListener(this);
        }
        if (!(tvVak == null || tvLokaal == null || ivImagetop == null)) {
            if (item.tmp) {
                tvVak.setText("*" + item.vakkort);
            } else {
                tvVak.setText(item.vakkort);
            }
            tvLokaal.setText(item.lok);
            if (item.les) {
                if (tvBegin != null) {
                    tvBegin.setText(HwUtl.Time2String(item.begin));
                }
                if (tvEinde != null) {
                    tvEinde.setText(HwUtl.Time2String(item.einde));
                }
                if (item.toets) {
                    if (!item.klaar) {
                        ivImagetop.setBackgroundResource(R.drawable.grid_huiswerk_toets);
                    } else {
                        ivImagetop.setBackgroundResource(R.drawable.grid_toets);
                    }
                } else if (!item.klaar) {
                    ivImagetop.setBackgroundResource(R.drawable.grid_huiswerk);
                } else {
                    ivImagetop.setBackgroundResource(0);
                }
            } else {
                ivImagetop.setBackgroundResource(0);
                if (tvBegin != null) {
                    tvBegin.setText("");
                }
                if (tvEinde != null) {
                    tvEinde.setText("");
                }
            }
        }
        return v;
    }

    private View MaakKopView(int iDag) {
        View v = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.roosterweekkop, (ViewGroup) null);
        TextView tvDag = (TextView) v.findViewById(R.id.dag);
        if (tvDag != null) {
            switch (this.roosterData[iDag][0].dow) {
                case 1:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_zo)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 2:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_ma)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 3:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_di)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 4:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_wo)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 5:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_do)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 6:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_vr)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
                case 7:
                    tvDag.setText(String.valueOf(getString(R.string.rooster_za)) + String.format("-%d", Integer.valueOf(this.roosterData[iDag][0].dag)));
                    break;
            }
            if (this.mCurDate == this.roosterData[iDag][0].datum) {
                tvDag.setTextColor(-256);
            } else {
                tvDag.setTextColor(-1);
            }
        }
        return v;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ((HuisWerkMain) getParent()).ShowMainMenu();
        return super.onPrepareOptionsMenu(menu);
    }

    public void onResume() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int iBackgroundColor = prefs.getInt("HW_PREF_ACHTERGROND", 0);
        iPrefNumHours = prefs.getInt("HW_PREF_NUMHOURS", 10);
        iPrefNumDays = prefs.getInt("HW_PREF_NUMDAYS", 7);
        mMain.setBackgroundColor(iBackgroundColor);
        ((HuisWerkMain) MainContext).setHuisWerkCount(this.db.getHuiswerkCountNietAf());
        ((HuisWerkMain) MainContext).setToetsCount(this.db.getToetsCountNietAf());
        this.firstDayOfWeek = (Calendar) ((HuisWerkMain) MainContext).getCurViewDate().clone();
        FindFirstDateOfWeek();
        FillRooster();
        ToonRooster();
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        this.db.close();
        super.onDestroy();
    }

    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                this.downXValue = event.getRawX();
                break;
            case 1:
                float currentX = event.getRawX();
                if (this.downXValue < currentX && currentX - this.downXValue > 100.0f) {
                    PreviousWeek();
                    return true;
                } else if (this.downXValue > currentX && this.downXValue - currentX > 100.0f) {
                    NextWeek();
                    return true;
                }
                break;
        }
        return false;
    }

    public void NextWeek() {
        this.firstDayOfWeek.add(5, 7);
        ((HuisWerkMain) MainContext).setCurViewDate(this.firstDayOfWeek);
        FillRooster();
        ToonRooster();
        mWeekData.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_to_left));
    }

    public void PreviousWeek() {
        this.firstDayOfWeek.add(5, -7);
        ((HuisWerkMain) MainContext).setCurViewDate(this.firstDayOfWeek);
        FillRooster();
        ToonRooster();
        mWeekData.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_to_right));
    }
}
