package jp.co.arttec.satbox.scoreranklib;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

public class RegistScoreController extends AsyncTask<Void, Integer, Boolean> implements GAEController {
    private Context _context;
    private int _kindGames;
    private HttpCommunicationListener _listener;
    private String _name;
    private ProgressDialog _progress;
    private long _score;
    private boolean _success;

    public RegistScoreController() {
        this._kindGames = -1;
        this._score = 0;
        this._name = null;
        this._context = null;
        this._progress = null;
        this._success = false;
        this._listener = null;
    }

    public RegistScoreController(int kindGames, long score, String name) {
        this._kindGames = kindGames;
        this._score = score;
        this._name = name;
        this._context = null;
        this._progress = null;
        this._success = false;
        this._listener = null;
    }

    public void setActivity(Context context) {
        this._context = context;
    }

    public void setOnFinishListener(HttpCommunicationListener listener) {
        this._listener = listener;
    }

    public void getHighScore() {
    }

    public void registScore() {
        execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... params) {
        this._success = false;
        List<NameValuePair> nvps = new ArrayList<>();
        nvps.add(new BasicNameValuePair(GAECommonData.KIND_GAMES, Integer.toString(this._kindGames)));
        nvps.add(new BasicNameValuePair(GAECommonData.SCORE, Long.toString(this._score)));
        nvps.add(new BasicNameValuePair(GAECommonData.NAME, this._name));
        try {
            HttpPost httpPost = new HttpPost(GAECommonData.GAE_URI);
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), GAECommonData.CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpClient.getParams(), GAECommonData.SO_TIMEOUT);
            if (httpClient.execute(httpPost).getStatusLine().getStatusCode() < 400) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this._context != null) {
            this._progress = new ProgressDialog(this._context);
            this._progress.setTitle("");
            this._progress.setMessage("");
            this._progress.setProgressStyle(0);
            this._progress.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        closeDialog();
        if (this._listener != null) {
            this._listener.onFinish(result.booleanValue());
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        closeDialog();
        if (this._listener != null) {
            this._listener.onFinish(false);
        }
    }

    private void closeDialog() {
        if (this._progress != null) {
            this._progress.dismiss();
            this._progress = null;
        }
    }
}
