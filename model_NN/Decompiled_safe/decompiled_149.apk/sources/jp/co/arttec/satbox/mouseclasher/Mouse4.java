package jp.co.arttec.satbox.mouseclasher;

import android.graphics.Rect;

public class Mouse4 {
    protected int anime = 0;
    protected int baku_time = 0;
    protected int critical = 0;
    public int h = 80;
    public int position;
    protected int speed;
    protected MyView view;
    public int w = 100;
    public int wx = 60;
    public int wx2 = 180;
    public int wx3 = 300;
    public int wx4 = 420;
    public int x;
    public int y;

    public Mouse4(MyView myView, int mousespeed, int _position) {
        this.view = myView;
        this.speed = mousespeed;
        this.x = _position;
        this.y = 100;
    }

    public void move() {
        this.y += this.speed;
        this.y = Math.min(this.view.dispY, Math.max(0, this.y));
    }

    public boolean hitTest(int tx, int ty) {
        return this.x - 25 <= tx && tx < this.x + this.w && this.y - 10 <= ty && ty < this.y + this.h;
    }

    public boolean rakkatest(int zy) {
        return zy < this.y;
    }

    public Rect getRect() {
        return new Rect(this.x, this.y, this.x + this.w, this.y + this.h);
    }
}
