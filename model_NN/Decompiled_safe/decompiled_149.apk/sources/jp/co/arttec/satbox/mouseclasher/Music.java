package jp.co.arttec.satbox.mouseclasher;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
    private static MediaPlayer sMediaPlayer = null;

    public static void play(Context context, int res, boolean looping) {
        stop();
        sMediaPlayer = MediaPlayer.create(context, res);
        sMediaPlayer.setLooping(looping);
        sMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });
        sMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                if (!mp.isLooping()) {
                    Music.stop();
                }
            }
        });
    }

    public static void stop() {
        if (sMediaPlayer != null) {
            sMediaPlayer.stop();
            sMediaPlayer.release();
            sMediaPlayer = null;
        }
    }
}
