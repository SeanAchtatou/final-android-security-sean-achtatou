package jp.co.arttec.satbox.mouseclasher;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: MouseClasher */
class MyView extends View {
    public static int characterH = 0;
    public static int characterW = 0;
    public static float cx = 200.0f;
    public static float cy = 400.0f;
    public static int mouseH = 0;
    public static int mouseH2 = 0;
    public static int mouseW = 0;
    public static int mouseW2 = 0;
    private static final float seVolume = 0.99f;
    public static int touchX = 40;
    public static int touchY = 50;
    private int GAME_TIME;
    int MSCNT;
    private int Touch_flg;
    public MediaPlayer _mediaPlayer;
    int attack;
    boolean bDrawable;
    private Bitmap bmpHaikei;
    private Bitmap bmpHammer;
    private Bitmap bmpMouse;
    private Bitmap bmpMouseGreen;
    private Bitmap bmpMouseGreenDmg;
    private Bitmap bmpMouseYellow;
    private Bitmap bmpMouseYellowDmg;
    private Bitmap bmpMousehit;
    private Bitmap bmpMousepink;
    private Bitmap bmpMousepinkyarare;
    private int bonus;
    private boolean bouns_flg;
    private int combo;
    private Boolean combo_10;
    private Boolean combo_100;
    private Boolean combo_20;
    private Boolean combo_30;
    private Boolean combo_40;
    private Boolean combo_50;
    private Boolean combo_60;
    private Boolean combo_70;
    private Boolean combo_80;
    private Boolean combo_90;
    private int combo_time;
    public int dispSX;
    public int dispX;
    public int dispY;
    private boolean flg_vibrator;
    private int frame;
    int game_flg;
    private int game_level;
    RedrawHandler handler;
    private boolean handler_flg;
    private ArrayList<Mouse> mouse;
    private ArrayList<Mouse2> mouse2;
    private ArrayList<Mouse3> mouse3;
    private ArrayList<Mouse4> mouse4;
    private int mousespeed;
    private OnGameOverListener onGameOverListener;
    private OnSoundTouchListener onSoundTouchListener;
    private long p_time;
    private long s_time;
    private int score;
    SoundPool soundPool;
    private Vibrator vibrator;

    public void setOnGameOverListener(OnGameOverListener listener) {
        this.onGameOverListener = listener;
    }

    public void dispatchOnGameOverEvent() {
        if (this.onGameOverListener != null) {
            this.onGameOverListener.onGameOver(String.valueOf(this.score));
        }
    }

    public void setOnSoundTouchListener(OnSoundTouchListener listener) {
        this.onSoundTouchListener = listener;
    }

    private void dispatchSoundTouchEvent() {
        if (this.onSoundTouchListener != null) {
            this.onSoundTouchListener.onSoundTouch();
        }
    }

    public MyView(Context context, int level) {
        super(context);
        this.game_level = 1;
        this.GAME_TIME = 30000;
        this.mousespeed = 8;
        this.s_time = 0;
        this.p_time = 0;
        this.handler_flg = false;
        this.bouns_flg = false;
        this.combo = 0;
        this.bonus = 0;
        this.combo_time = 0;
        this.combo_10 = false;
        this.combo_20 = false;
        this.combo_30 = false;
        this.combo_40 = false;
        this.combo_50 = false;
        this.combo_60 = false;
        this.combo_70 = false;
        this.combo_80 = false;
        this.combo_90 = false;
        this.combo_100 = false;
        this.score = 0;
        this.frame = 0;
        this.Touch_flg = 0;
        this.dispSX = 480;
        this.MSCNT = 1;
        this.game_flg = 0;
        this.bDrawable = false;
        this.frame = 0;
        this.score = 0;
        this.game_level = level;
        this.handler = new RedrawHandler(this, 30);
        this.handler.start();
        setFocusable(true);
        Resources r = context.getResources();
        this.mouse = new ArrayList<>();
        this.mouse2 = new ArrayList<>();
        this.mouse3 = new ArrayList<>();
        this.mouse4 = new ArrayList<>();
        this.vibrator = (Vibrator) getContext().getSystemService("vibrator");
        this.flg_vibrator = context.getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        this.soundPool = new SoundPool(this.MSCNT, 3, 0);
        this.attack = this.soundPool.load(context, R.raw.hit_s14, 1);
        this.bmpMouse = BitmapFactory.decodeResource(r, R.drawable.nezumi);
        this.bmpHammer = BitmapFactory.decodeResource(r, R.drawable.hammer3);
        this.bmpMousehit = BitmapFactory.decodeResource(r, R.drawable.nezumiyarare);
        switch (this.game_level) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.bmpHaikei = BitmapFactory.decodeResource(r, R.drawable.back_easy);
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation:
                this.bmpHaikei = BitmapFactory.decodeResource(r, R.drawable.back_normal);
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility:
                this.bmpHaikei = BitmapFactory.decodeResource(r, R.drawable.back_hard);
                break;
        }
        this.bmpMousepink = BitmapFactory.decodeResource(r, R.drawable.mouse_pink);
        this.bmpMousepinkyarare = BitmapFactory.decodeResource(r, R.drawable.mouse_pinkyarare);
        this.bmpMouseGreen = BitmapFactory.decodeResource(r, R.drawable.mouse_green);
        this.bmpMouseGreenDmg = BitmapFactory.decodeResource(r, R.drawable.mouse_green_dmg);
        this.bmpMouseYellow = BitmapFactory.decodeResource(r, R.drawable.mouse_yellow);
        this.bmpMouseYellowDmg = BitmapFactory.decodeResource(r, R.drawable.mouse_yellow_dmg);
        mouseW = this.bmpMouse.getWidth();
        mouseH = this.bmpMouse.getHeight();
        mouseW2 = this.bmpMousepink.getWidth();
        mouseH2 = this.bmpMousepink.getHeight();
        this.s_time = System.currentTimeMillis();
    }

    public void MyView1(Context c) {
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.dispX = w;
        this.dispY = h;
        this.dispSX = w - 40;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        int tx = (int) event.getX();
        int ty = (int) event.getY();
        dispatchSoundTouchEvent();
        cx = (float) tx;
        cy = (float) ty;
        if (event.getAction() == 0) {
            if (this.Touch_flg == 0) {
                for (int i = 0; i < this.mouse.size(); i++) {
                    Mouse p = this.mouse.get(i);
                    if (p.hitTest(tx, ty) && p.anime == 0) {
                        p.anime++;
                        this.soundPool.play(this.attack, seVolume, seVolume, 1, 0, 1.0f);
                        if (p.critical <= 1) {
                            p.critical++;
                        }
                    }
                }
            }
            if (this.Touch_flg == 0) {
                for (int ii = 0; ii < this.mouse2.size(); ii++) {
                    Mouse2 pp = this.mouse2.get(ii);
                    if (pp.hitTest(tx, ty)) {
                        pp.anime++;
                        this.soundPool.play(this.attack, seVolume, seVolume, 1, 0, 1.0f);
                        if (pp.critical <= 1) {
                            pp.critical++;
                        }
                    }
                }
            }
            if (this.Touch_flg == 0) {
                for (int mouse3_i = 0; mouse3_i < this.mouse3.size(); mouse3_i++) {
                    Mouse3 pp2 = this.mouse3.get(mouse3_i);
                    if (pp2.hitTest(tx, ty)) {
                        pp2.anime++;
                        this.soundPool.play(this.attack, seVolume, seVolume, 1, 0, 1.0f);
                        if (pp2.critical <= 1) {
                            pp2.critical++;
                        }
                    }
                }
            }
            if (this.Touch_flg == 0) {
                for (int mouse4_i = 0; mouse4_i < this.mouse4.size(); mouse4_i++) {
                    Mouse4 pp3 = this.mouse4.get(mouse4_i);
                    if (pp3.hitTest(tx, ty)) {
                        pp3.anime++;
                        this.soundPool.play(this.attack, seVolume, seVolume, 1, 0, 1.0f);
                        if (pp3.critical <= 1) {
                            pp3.critical++;
                        }
                    }
                }
            }
            z = true;
        } else {
            event.getAction();
            z = true;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        String sscore;
        if (this.dispX != 0 && this.dispY != 0) {
            super.onDraw(canvas);
            Paint ptext = new Paint();
            ptext.setColor(-16777216);
            ptext.setTextSize(25.0f);
            this.p_time = ((long) this.GAME_TIME) - (System.currentTimeMillis() - this.s_time);
            if (this.p_time > 0) {
                sscore = "TIME:" + String.valueOf(((this.p_time - (this.p_time % 1000)) / 1000) + 1);
            } else {
                this.p_time = 0;
                sscore = "TIME:0";
            }
            canvas.drawBitmap(this.bmpHaikei, new Rect(0, 0, this.bmpHaikei.getWidth(), this.bmpHaikei.getHeight()), new Rect(getLeft(), getTop(), getRight(), getBottom()), (Paint) null);
            drawTexts(canvas, ptext, sscore);
            MouseUpdate(canvas);
            Mouse2Update(canvas);
            Mouse3Update(canvas);
            Mouse4Update(canvas);
            ComboUpdate();
            if (!this.handler_flg && this.p_time <= 0) {
                this.handler.stop();
                this.frame = 0;
                this.mouse.clear();
                this.mouse2.clear();
                this.handler_flg = true;
                dispatchOnGameOverEvent();
            }
            if (this.score <= 100) {
                if (this.frame % 23 == 0) {
                    MouseAdd(this.mousespeed);
                }
            } else if (this.score <= 100 || this.score > 200) {
                if (this.score <= 200 || this.score > 320) {
                    if (this.score <= 320 || this.score > 450) {
                        if (this.score <= 450 || this.score > 700) {
                            if (this.score <= 700 || this.score > 1000) {
                                if (this.score <= 1000 || this.score > 60000) {
                                    MouseAdd(this.mousespeed + 12);
                                } else if (this.frame % 8 == 0) {
                                    MouseAdd(this.mousespeed + 7);
                                }
                            } else if (this.frame % 9 == 0) {
                                MouseAdd(this.mousespeed + 5);
                            }
                        } else if (this.frame % 12 == 0) {
                            MouseAdd(this.mousespeed + 4);
                        }
                    } else if (this.frame % 15 == 0) {
                        MouseAdd(this.mousespeed + 3);
                    }
                } else if (this.frame % 18 == 0) {
                    MouseAdd(this.mousespeed + 2);
                }
            } else if (this.frame % 21 == 0) {
                MouseAdd(this.mousespeed + 1);
            }
            if (this.score > 100 && this.score <= 300 && this.frame % 26 == 0) {
                Mouse2Add(this.mousespeed + 7);
            }
            if (this.score > 300 && this.score <= 1000 && this.frame % 20 == 0) {
                Mouse2Add(this.mousespeed + 8);
            }
            if (this.score > 1000 && this.score <= 100000 && this.frame % 15 == 0) {
                Mouse2Add(this.mousespeed + 8);
            }
            if (this.game_level >= 2) {
                if (this.score > 1000 && this.score <= 2000 && this.frame % 26 == 0) {
                    Mouse3Add(this.mousespeed + 8);
                }
                if (this.score > 2000 && this.score <= 3000 && this.frame % 20 == 0) {
                    Mouse3Add(this.mousespeed + 9);
                }
                if (this.score > 3000 && this.score <= 100000 && this.frame % 15 == 0) {
                    Mouse3Add(this.mousespeed + 10);
                }
            }
            if (this.game_level >= 3) {
                if (this.score > 5000 && this.score <= 10000 && this.frame % 26 == 0) {
                    Mouse4Add(this.mousespeed + 9);
                }
                if (this.score > 10000 && this.score <= 20000 && this.frame % 20 == 0) {
                    Mouse4Add(this.mousespeed + 10);
                }
                if (this.score > 20000 && this.score <= 100000 && this.frame % 15 == 0) {
                    Mouse4Add(this.mousespeed + 11);
                }
            }
            this.frame++;
        }
    }

    private void drawTexts(Canvas canvas, Paint ptext, String sscore) {
        canvas.drawText(sscore, 10.0f, 30.0f, ptext);
        canvas.drawText("SCORE:" + String.valueOf(this.score), 140.0f, 30.0f, ptext);
        canvas.drawText("COMBO:" + String.valueOf(this.combo), 320.0f, 30.0f, ptext);
        if (this.combo == 10) {
            canvas.drawText("COMBO BONUS SCORE + 100", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 11) {
            canvas.drawText("COMBO BONUS SCORE + 100", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 12) {
            canvas.drawText("COMBO BONUS SCORE + 100", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 20) {
            canvas.drawText("COMBO BONUS SCORE + 200", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 21) {
            canvas.drawText("COMBO BONUS SCORE + 200", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 22) {
            canvas.drawText("COMBO BONUS SCORE + 200", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 30) {
            canvas.drawText("COMBO BONUS SCORE + 300", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 31) {
            canvas.drawText("COMBO BONUS SCORE + 300", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 32) {
            canvas.drawText("COMBO BONUS SCORE + 300", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 40) {
            canvas.drawText("COMBO BONUS SCORE + 400", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 41) {
            canvas.drawText("COMBO BONUS SCORE + 400", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 42) {
            canvas.drawText("COMBO BONUS SCORE + 400", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 50) {
            canvas.drawText("COMBO BONUS SCORE + 500", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 51) {
            canvas.drawText("COMBO BONUS SCORE + 500", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 52) {
            canvas.drawText("COMBO BONUS SCORE + 500", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 60) {
            canvas.drawText("COMBO BONUS SCORE + 600", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 61) {
            canvas.drawText("COMBO BONUS SCORE + 600", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 62) {
            canvas.drawText("COMBO BONUS SCORE + 600", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 70) {
            canvas.drawText("COMBO BONUS SCORE + 700", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 71) {
            canvas.drawText("COMBO BONUS SCORE + 700", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 72) {
            canvas.drawText("COMBO BONUS SCORE + 700", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 80) {
            canvas.drawText("COMBO BONUS SCORE + 800", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 81) {
            canvas.drawText("COMBO BONUS SCORE + 800", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 82) {
            canvas.drawText("COMBO BONUS SCORE + 800", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 90) {
            canvas.drawText("COMBO BONUS SCORE + 900", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 91) {
            canvas.drawText("COMBO BONUS SCORE + 900", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 92) {
            canvas.drawText("COMBO BONUS SCORE + 900", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 100) {
            canvas.drawText("COMBO BONUS SCORE + 1000", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 101) {
            canvas.drawText("COMBO BONUS SCORE + 1000", 10.0f, 60.0f, ptext);
        }
        if (this.combo == 102) {
            canvas.drawText("COMBO BONUS SCORE + 1000", 10.0f, 60.0f, ptext);
        }
    }

    private void MouseUpdate(Canvas canvas) {
        int i = 0;
        while (i < this.mouse.size()) {
            Mouse p = this.mouse.get(i);
            if (p.y > 630) {
                this.score -= 10;
                this.combo = 0;
                this.mouse.remove(i);
            } else {
                if (p.anime > 0) {
                    p.anime++;
                    if (p.anime <= 6) {
                        canvas.drawBitmap(this.bmpMousehit, (float) p.x, (float) p.y, (Paint) null);
                        if (this.flg_vibrator) {
                            this.vibrator.vibrate(30);
                        }
                    } else if (p.anime >= 6) {
                        this.score += 30;
                        this.combo++;
                        this.mouse.remove(i);
                    }
                } else {
                    p.move();
                    canvas.drawBitmap(this.bmpMouse, (float) p.x, (float) p.y, (Paint) null);
                }
                i++;
            }
        }
        if (i <= 30) {
        }
    }

    private void Mouse2Update(Canvas canvas) {
        int ii = 0;
        while (ii < this.mouse2.size()) {
            Mouse2 pp = this.mouse2.get(ii);
            if (pp.y > 630) {
                this.score -= 20;
                this.combo = 0;
                this.mouse2.remove(ii);
            } else {
                if (pp.anime > 0) {
                    pp.anime++;
                    if (pp.anime <= 6) {
                        canvas.drawBitmap(this.bmpMousepinkyarare, (float) pp.x, (float) pp.y, (Paint) null);
                        if (this.flg_vibrator) {
                            this.vibrator.vibrate(30);
                        }
                    } else if (pp.anime >= 6) {
                        this.score += 70;
                        this.combo++;
                        this.mouse2.remove(ii);
                    }
                } else {
                    pp.move();
                    canvas.drawBitmap(this.bmpMousepink, (float) pp.x, (float) pp.y, (Paint) null);
                }
                ii++;
            }
        }
        if (ii <= 30) {
        }
    }

    private void Mouse3Update(Canvas canvas) {
        int ii = 0;
        while (ii < this.mouse3.size()) {
            Mouse3 pp = this.mouse3.get(ii);
            if (pp.y > 630) {
                this.score -= 30;
                this.combo = 0;
                this.mouse3.remove(ii);
            } else {
                if (pp.anime > 0) {
                    pp.anime++;
                    if (pp.anime <= 6) {
                        canvas.drawBitmap(this.bmpMouseGreenDmg, (float) pp.x, (float) pp.y, (Paint) null);
                        if (this.flg_vibrator) {
                            this.vibrator.vibrate(30);
                        }
                    } else if (pp.anime >= 6) {
                        this.score += 100;
                        this.combo++;
                        this.mouse3.remove(ii);
                    }
                } else {
                    pp.move();
                    canvas.drawBitmap(this.bmpMouseGreen, (float) pp.x, (float) pp.y, (Paint) null);
                }
                ii++;
            }
        }
        if (ii <= 30) {
        }
    }

    private void Mouse4Update(Canvas canvas) {
        int ii = 0;
        while (ii < this.mouse4.size()) {
            Mouse4 pp = this.mouse4.get(ii);
            if (pp.y > 630) {
                this.score -= 50;
                this.combo = 0;
                this.mouse4.remove(ii);
            } else {
                if (pp.anime > 0) {
                    pp.anime++;
                    if (pp.anime <= 6) {
                        canvas.drawBitmap(this.bmpMouseYellowDmg, (float) pp.x, (float) pp.y, (Paint) null);
                        if (this.flg_vibrator) {
                            this.vibrator.vibrate(30);
                        }
                    } else if (pp.anime >= 6) {
                        this.score += 150;
                        this.combo++;
                        this.mouse4.remove(ii);
                    }
                } else {
                    pp.move();
                    canvas.drawBitmap(this.bmpMouseYellow, (float) pp.x, (float) pp.y, (Paint) null);
                }
                ii++;
            }
        }
        if (ii <= 30) {
        }
    }

    public void MouseAdd(int _mouse_speed) {
        int pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
        int cnt = 0;
        boolean pos_flg = false;
        if (this.mouse.size() != 0) {
            pos_flg = true;
            while (cnt <= 100 && pos_flg) {
                pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
                Iterator<Mouse> it = this.mouse.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else if (hitCheckBox(it.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
                if (!pos_flg) {
                    Iterator<Mouse2> it2 = this.mouse2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        } else if (hitCheckBox(it2.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                            pos_flg = true;
                            break;
                        } else {
                            pos_flg = false;
                        }
                    }
                }
                if (!pos_flg) {
                    Iterator<Mouse3> it3 = this.mouse3.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        } else if (hitCheckBox(it3.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                            pos_flg = true;
                            break;
                        } else {
                            pos_flg = false;
                        }
                    }
                }
                if (!pos_flg) {
                    Iterator<Mouse4> it4 = this.mouse4.iterator();
                    while (true) {
                        if (!it4.hasNext()) {
                            break;
                        } else if (hitCheckBox(it4.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                            pos_flg = true;
                            break;
                        } else {
                            pos_flg = false;
                        }
                    }
                }
                cnt++;
            }
        }
        if (cnt < 100 && !pos_flg) {
            this.mouse.add(new Mouse(this, _mouse_speed, pos));
        }
    }

    public void Mouse2Add(int _mouse_speed) {
        int pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
        int cnt = 0;
        boolean pos_flg = true;
        while (cnt <= 100 && pos_flg) {
            pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
            Iterator<Mouse> it = this.mouse.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (hitCheckBox(it.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                    pos_flg = true;
                    break;
                } else {
                    pos_flg = false;
                }
            }
            if (!pos_flg) {
                Iterator<Mouse2> it2 = this.mouse2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else if (hitCheckBox(it2.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse3> it3 = this.mouse3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    } else if (hitCheckBox(it3.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse4> it4 = this.mouse4.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    } else if (hitCheckBox(it4.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            cnt++;
        }
        if (cnt < 100 && !pos_flg) {
            this.mouse2.add(new Mouse2(this, _mouse_speed, pos));
        }
    }

    public void Mouse3Add(int _mouse_speed) {
        int pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
        int cnt = 0;
        boolean pos_flg = true;
        while (cnt <= 100 && pos_flg) {
            pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
            Iterator<Mouse> it = this.mouse.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (hitCheckBox(it.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                    pos_flg = true;
                    break;
                } else {
                    pos_flg = false;
                }
            }
            if (!pos_flg) {
                Iterator<Mouse2> it2 = this.mouse2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else if (hitCheckBox(it2.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse3> it3 = this.mouse3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    } else if (hitCheckBox(it3.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse4> it4 = this.mouse4.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    } else if (hitCheckBox(it4.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            cnt++;
        }
        if (cnt < 100 && !pos_flg) {
            this.mouse3.add(new Mouse3(this, _mouse_speed, pos));
        }
    }

    public void Mouse4Add(int _mouse_speed) {
        int pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
        int cnt = 0;
        boolean pos_flg = true;
        while (cnt <= 100 && pos_flg) {
            pos = ((int) Math.floor(Math.random() * ((double) (this.dispSX - (Mouse.w * 2))))) + Mouse.w;
            Iterator<Mouse> it = this.mouse.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (hitCheckBox(it.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                    pos_flg = true;
                    break;
                } else {
                    pos_flg = false;
                }
            }
            if (!pos_flg) {
                Iterator<Mouse2> it2 = this.mouse2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    } else if (hitCheckBox(it2.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse3> it3 = this.mouse3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    } else if (hitCheckBox(it3.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            if (!pos_flg) {
                Iterator<Mouse4> it4 = this.mouse4.iterator();
                while (true) {
                    if (!it4.hasNext()) {
                        break;
                    } else if (hitCheckBox(it4.next().x, 0, pos, 0, Mouse.w, Mouse.h, Mouse.w, Mouse.h)) {
                        pos_flg = true;
                        break;
                    } else {
                        pos_flg = false;
                    }
                }
            }
            cnt++;
        }
        if (cnt < 100 && !pos_flg) {
            this.mouse4.add(new Mouse4(this, _mouse_speed, pos));
        }
    }

    private void ComboUpdate() {
        if (this.bouns_flg) {
            this.combo_time++;
            if (this.combo_time >= 80) {
                this.combo_time = 0;
                this.bouns_flg = false;
            }
        }
        if (this.combo >= 10 && !this.combo_10.booleanValue()) {
            this.bonus = 100;
            this.combo_10 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 20 && !this.combo_20.booleanValue()) {
            this.bonus = 200;
            this.combo_20 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 30 && !this.combo_30.booleanValue()) {
            this.bonus = 300;
            this.combo_30 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 40 && !this.combo_40.booleanValue()) {
            this.bonus = 400;
            this.combo_40 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 50 && !this.combo_50.booleanValue()) {
            this.bonus = 500;
            this.combo_50 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 60 && !this.combo_60.booleanValue()) {
            this.bonus = 600;
            this.combo_60 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 70 && !this.combo_70.booleanValue()) {
            this.bonus = 700;
            this.combo_70 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 80 && !this.combo_80.booleanValue()) {
            this.bonus = 800;
            this.combo_80 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 90 && !this.combo_90.booleanValue()) {
            this.bonus = 900;
            this.combo_90 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo >= 100 && !this.combo_100.booleanValue()) {
            this.bonus = 1000;
            this.combo_100 = true;
            this.bouns_flg = true;
            this.score += this.bonus;
        }
        if (this.combo < 10) {
            this.combo_10 = false;
            this.combo_20 = false;
            this.combo_30 = false;
            this.combo_40 = false;
            this.combo_50 = false;
            this.combo_60 = false;
            this.combo_70 = false;
            this.combo_80 = false;
            this.combo_90 = false;
            this.combo_100 = false;
        }
    }

    public int getScore() {
        return this.score;
    }

    public void destroyBITMAP() {
        if (this.bmpMouse != null) {
            this.bmpMouse.recycle();
            this.bmpMouse = null;
        }
        if (this.bmpHammer != null) {
            this.bmpHammer.recycle();
            this.bmpHammer = null;
        }
        if (this.bmpMousehit != null) {
            this.bmpMousehit.recycle();
            this.bmpMousehit = null;
        }
        if (this.bmpHaikei != null) {
            this.bmpHaikei.recycle();
            this.bmpHaikei = null;
        }
        if (this.bmpMousepink != null) {
            this.bmpMousepink.recycle();
            this.bmpMousepink = null;
        }
        if (this.bmpMousepinkyarare != null) {
            this.bmpMousepinkyarare.recycle();
            this.bmpMousepinkyarare = null;
        }
    }

    public void destroyBGM() {
        if (this.soundPool != null) {
            this.soundPool.release();
            this.soundPool = null;
        }
    }

    public void setGameLevel(int _level) {
        this.game_level = _level;
        switch (this.game_level) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.GAME_TIME = 30000;
                return;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation:
                this.GAME_TIME = 45000;
                return;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility:
                this.GAME_TIME = 60000;
                return;
            default:
                return;
        }
    }

    private boolean hitCheckBox(int _x1, int _y1, int _x2, int _y2, int _size_x1, int _size_y1, int _size_x2, int _size_y2) {
        if (_x1 + _size_x1 < _x2 || _x1 > _x2 + _size_x2 || _y1 + _size_y1 < _y2 || _y1 > _y2 + _size_y2) {
            return false;
        }
        return true;
    }
}
