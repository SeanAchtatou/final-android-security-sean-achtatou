package jp.co.arttec.satbox.mouseclasher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MouseClasherActivity extends Activity {
    public static int cx = 0;
    private static final float seVolume = 0.99f;
    int MSCNT = 1;
    int[] attack = new int[this.MSCNT];
    private boolean flg_vibrator;
    SoundPool soundPool;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.title);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        this.soundPool = new SoundPool(this.MSCNT, 3, 0);
        this.attack[0] = this.soundPool.load(this, R.raw.hit_s14, 1);
        Music.play(this, R.raw.mainmusic, true);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.abouttitle);
        builder.setMessage((int) R.string.aboutmsg);
        builder.setPositiveButton((int) R.string.close, (DialogInterface.OnClickListener) null);
        ((Button) findViewById(R.id.sat_link_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MouseClasherActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
            }
        });
        ((Button) findViewById(R.id.startbtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MouseClasherActivity.this, GameMenu.class);
                MouseClasherActivity.this.soundPool.play(MouseClasherActivity.this.attack[0], MouseClasherActivity.seVolume, MouseClasherActivity.seVolume, 1, 0, 1.0f);
                MouseClasherActivity.this.startActivity(intent);
                MouseClasherActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.aboutbtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MouseClasherActivity.this.soundPool.play(MouseClasherActivity.this.attack[0], MouseClasherActivity.seVolume, MouseClasherActivity.seVolume, 1, 0, 1.0f);
                builder.create();
                builder.show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Music.play(this, R.raw.mainmusic, true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        switch (item.getItemId()) {
            case R.id.menu_vib_switch /*2131230750*/:
                this.flg_vibrator = !this.flg_vibrator;
                SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
                editor.putBoolean("vibflg", this.flg_vibrator);
                editor.commit();
                if (this.flg_vibrator) {
                    this.vibrator.vibrate(30);
                    break;
                }
                break;
            case R.id.menu_apk /*2131230751*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
                break;
            case R.id.menu_hp /*2131230752*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
                break;
            case R.id.menu_end /*2131230753*/:
                finish();
                break;
        }
        return result;
    }
}
