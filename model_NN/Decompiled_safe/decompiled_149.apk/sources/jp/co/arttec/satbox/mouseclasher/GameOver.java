package jp.co.arttec.satbox.mouseclasher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import jp.co.arttec.satbox.rankresource.GameMyEntry;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.RegistScoreController;

public class GameOver extends Activity {
    private AlertDialog _alertDialog;
    /* access modifiers changed from: private */
    public EditText _editText;
    private Button btn_end;
    /* access modifiers changed from: private */
    public Button btn_score;
    /* access modifiers changed from: private */
    public boolean flg_up = false;
    private int game_level;
    private String level_str;
    private int score = 0;
    private int server_no = 34;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_over);
        Bundle extras = getIntent().getExtras();
        this.score = Integer.parseInt(extras.getString("SCORE"));
        this.game_level = extras.getInt("GameLevel");
        switch (this.game_level) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval /*1*/:
                this.level_str = "level1";
                this.server_no = 34;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation /*2*/:
                this.level_str = "level2";
                this.server_no = 50;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility /*3*/:
                this.level_str = "level3";
                this.server_no = 51;
                break;
        }
        new GameMyEntry(this, this.level_str, this.score).onMyScoreEntry();
        this.btn_end = (Button) findViewById(R.id.end_bt_end);
        this.btn_score = (Button) findViewById(R.id.end_bt_score);
        ((TextView) findViewById(R.id.result)).setText(String.valueOf(this.score));
        this._editText = new EditText(this);
        this._editText.setFocusable(true);
        this._editText.setFocusableInTouchMode(true);
        this._editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        this._alertDialog = new AlertDialog.Builder(this).setTitle("Score entry").setMessage("Enter your name").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameOver.this.registHighScore(GameOver.this._editText.getText().toString());
            }
        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(this._editText).create();
        this.btn_score.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                if (!GameOver.this.flg_up) {
                    GameOver.this.onScoreEntry();
                }
            }
        });
        this.btn_end.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameOver.this.startActivity(new Intent(GameOver.this, GameMenu.class));
                GameOver.this.finish();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        startActivity(new Intent(this, GameMenu.class));
        finish();
        return false;
    }

    /* access modifiers changed from: private */
    public void onScoreEntry() {
        this._alertDialog.show();
    }

    /* access modifiers changed from: private */
    public void registHighScore(String name) {
        RegistScoreController controller = new RegistScoreController(this.server_no, (long) this.score, name);
        controller.setActivity(this);
        controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                if (result) {
                    GameOver.this.btn_score.setEnabled(false);
                    GameOver.this.flg_up = true;
                    toast = Toast.makeText(GameOver.this, "Score entry completion.", 1);
                } else {
                    toast = Toast.makeText(GameOver.this, "Score entry failure.", 1);
                }
                toast.show();
            }
        });
        controller.registScore();
    }
}
