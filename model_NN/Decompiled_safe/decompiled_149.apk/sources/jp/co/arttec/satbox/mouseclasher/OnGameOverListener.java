package jp.co.arttec.satbox.mouseclasher;

public interface OnGameOverListener {
    void onGameOver(String str);
}
