package jp.co.arttec.satbox.mouseclasher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import jp.co.arttec.satbox.rankresource.GameRankMy;

public class GameMenu extends BaseActivity {
    private static final float seVolume = 0.99f;
    /* access modifiers changed from: private */
    public int attackID = 0;
    private ImageButton btn_play_easy;
    private ImageButton btn_play_hard;
    private ImageButton btn_play_normal;
    private ImageButton btn_rank_easy;
    private ImageButton btn_rank_hard;
    private ImageButton btn_rank_normal;
    private boolean flg_onCreate;
    private boolean flg_vibrator;
    private MediaPlayer mediaplayer;
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mediaplayer = null;
        setContentView((int) R.layout.game_menu);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        this.soundPool = new SoundPool(1, 3, 0);
        this.attackID = this.soundPool.load(this, R.raw.hit_s14, 1);
        this.flg_onCreate = true;
        this.mediaplayer = null;
        viewInitialize(this);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        startActivity(new Intent(this, MouseClasherActivity.class));
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mediaplayer != null && !this.mediaplayer.isPlaying()) {
            if (this.flg_onCreate) {
                this.flg_onCreate = false;
            } else {
                this.mediaplayer.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mediaplayer != null && this.mediaplayer.isPlaying()) {
            this.mediaplayer.pause();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        stopSound();
        if (this.soundPool != null) {
            this.soundPool.release();
            this.soundPool = null;
        }
    }

    private void stopSound() {
        if (this.mediaplayer != null) {
            if (this.mediaplayer.isPlaying()) {
                this.mediaplayer.stop();
            }
            this.mediaplayer.release();
            this.mediaplayer = null;
        }
    }

    private void playSound() {
        stopSound();
        this.mediaplayer = MediaPlayer.create(this, (int) R.raw.titlemusic);
        this.mediaplayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.seekTo(0);
                mp.start();
            }
        });
    }

    private void viewInitialize(Context context) {
        this.btn_play_easy = new ImageButton(context);
        this.btn_play_easy = (ImageButton) findViewById(R.id.play_easy_button);
        this.btn_play_easy.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, MouseClasher.class);
                intent.putExtra("GameLevel", 1);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
                GameMenu.this.finish();
            }
        });
        this.btn_play_normal = new ImageButton(context);
        this.btn_play_normal = (ImageButton) findViewById(R.id.play_normal_button);
        this.btn_play_normal.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, MouseClasher.class);
                intent.putExtra("GameLevel", 2);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
                GameMenu.this.finish();
            }
        });
        this.btn_play_hard = new ImageButton(context);
        this.btn_play_hard = (ImageButton) findViewById(R.id.play_hard_button);
        this.btn_play_hard.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, MouseClasher.class);
                intent.putExtra("GameLevel", 3);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
                GameMenu.this.finish();
            }
        });
        this.btn_rank_easy = new ImageButton(context);
        this.btn_rank_easy = (ImageButton) findViewById(R.id.rank_easy_button);
        this.btn_rank_easy.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, GameRankMy.class);
                intent.putExtra("GameLevel", 1);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
            }
        });
        this.btn_rank_normal = new ImageButton(context);
        this.btn_rank_normal = (ImageButton) findViewById(R.id.rank_normal_button);
        this.btn_rank_normal.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, GameRankMy.class);
                intent.putExtra("GameLevel", 2);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
            }
        });
        this.btn_rank_hard = new ImageButton(context);
        this.btn_rank_hard = (ImageButton) findViewById(R.id.rank_hard_button);
        this.btn_rank_hard.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameMenu.this, GameRankMy.class);
                intent.putExtra("GameLevel", 3);
                GameMenu.this.soundPool.play(GameMenu.this.attackID, GameMenu.seVolume, GameMenu.seVolume, 1, 0, 1.0f);
                GameMenu.this.startActivity(intent);
            }
        });
        playSound();
        gameInitialize();
    }

    private void gameInitialize() {
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        this.flg_vibrator = getSharedPreferences("prefkey", 0).getBoolean("vibflg", true);
        if (this.flg_vibrator) {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(Off)");
        } else {
            menu.findItem(R.id.menu_vib_switch).setTitle("Vibrator(On)");
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public synchronized boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean result;
        result = super.onMenuItemSelected(featureId, item);
        if (item.getItemId() == R.id.menu_vib_switch) {
            this.flg_vibrator = !this.flg_vibrator;
            SharedPreferences.Editor editor = getSharedPreferences("prefkey", 0).edit();
            editor.putBoolean("vibflg", this.flg_vibrator);
            editor.commit();
            if (this.flg_vibrator) {
                this.vibrator.vibrate(100);
            }
        } else if (item.getItemId() == R.id.menu_apk) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } else if (item.getItemId() == R.id.menu_hp) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box/")));
        } else if (item.getItemId() == R.id.menu_end) {
            finish();
        }
        return result;
    }
}
