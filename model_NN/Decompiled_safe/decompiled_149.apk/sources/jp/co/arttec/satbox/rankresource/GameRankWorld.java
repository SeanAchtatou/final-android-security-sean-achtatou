package jp.co.arttec.satbox.rankresource;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.mouseclasher.BaseActivity;
import jp.co.arttec.satbox.mouseclasher.R;
import jp.co.arttec.satbox.scoreranklib.GetScoreController;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.Score;

public class GameRankWorld extends BaseActivity {
    /* access modifiers changed from: private */
    public GetScoreController controller;
    /* access modifiers changed from: private */
    public boolean flg_end = false;
    int game_level;
    private ListView mListView;
    private int server_no = 34;
    /* access modifiers changed from: private */
    public TextView txtRankName;
    private TextView txtRankTouch;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.game_level = getIntent().getExtras().getInt("GameLevel");
        switch (this.game_level) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.server_no = 34;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation:
                this.server_no = 50;
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility:
                this.server_no = 51;
                break;
        }
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (!GameRankWorld.this.flg_end) {
                    GameRankWorld.this.flg_end = true;
                    GameRankWorld.this.finish();
                }
            }
        });
        this.txtRankTouch = new TextView(getApplication());
        this.txtRankTouch = (TextView) findViewById(R.id.rank_tx_touch);
        this.txtRankTouch.setVisibility(8);
        onScoreEntry();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Intent intent = new Intent(this, GameRankMy.class);
        intent.putExtra("GameLevel", this.game_level);
        startActivity(intent);
        finish();
        return true;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.flg_end) {
            this.flg_end = true;
            if (event.getAction() != 0) {
                z = false;
            } else {
                finish();
                z = true;
            }
        } else {
            z = false;
        }
        return z;
    }

    private synchronized void onScoreEntry() {
        this.controller = new GetScoreController(this.server_no);
        this.controller.setActivity(this);
        this.controller.setRank(99);
        this.controller.setReverse(false);
        this.controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                GameRankWorld.this.txtRankName = (TextView) GameRankWorld.this.findViewById(R.id.rankname);
                GameRankWorld.this.txtRankName.setText("World Ranking");
                if (result) {
                    toast = Toast.makeText(GameRankWorld.this, "High score acquisition completion.", 1);
                    List<Score> _score = GameRankWorld.this.controller.getHighScoreList();
                    if (_score != null) {
                        List<RankStrDelivery> dataList = new ArrayList<>();
                        int j = 0;
                        for (Score data : _score) {
                            dataList.add(new RankStrDelivery(j + 1, data.getName(), String.valueOf(data.getScore()), ""));
                            j++;
                        }
                        ((ListView) GameRankWorld.this.findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(GameRankWorld.this, R.layout.row_world, dataList));
                    }
                } else {
                    toast = Toast.makeText(GameRankWorld.this, "High score acquisition failure.", 1);
                    GameRankWorld.this.finish();
                }
                toast.show();
            }
        });
        this.controller.getHighScore();
    }
}
