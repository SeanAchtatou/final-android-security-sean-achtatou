package jp.co.arttec.satbox.mouseclasher;

import android.graphics.Rect;

public class Hammer {
    public int anime;
    public int h;
    protected int speed;
    protected MyView view;
    public int w;
    public int x;
    public int y;

    public boolean hittest(int tx, int ty) {
        return this.x - 25 <= tx && tx < this.x + this.w && this.y - 10 <= ty && ty < this.y + this.h;
    }

    public void move() {
    }

    public boolean rakkatest(int zy) {
        return zy < this.y;
    }

    public Rect getRect() {
        return new Rect(this.x, this.y, this.x + this.w, this.y + this.h);
    }
}
