package jp.co.arttec.satbox.mouseclasher;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;

public class MouseClasher extends Activity {
    int MSCNT = 1;
    public MediaPlayer _mediaPlayer;
    int[] attack = new int[this.MSCNT];
    int game_level;
    SoundPool soundPool;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.game_level = getIntent().getExtras().getInt("GameLevel");
        MyView myView = new MyView(this, this.game_level);
        setContentView(myView);
        myView.setGameLevel(this.game_level);
        myView.setOnGameOverListener(new OnGameOverListener() {
            public void onGameOver(String score) {
                Intent intent = new Intent(MouseClasher.this, GameOver.class);
                intent.putExtra("SCORE", score);
                intent.putExtra("GameLevel", MouseClasher.this.game_level);
                MouseClasher.this.startActivity(intent);
                MouseClasher.this.finish();
            }
        });
        this.soundPool = new SoundPool(this.MSCNT, 3, 0);
        Music.play(this, R.raw.titlemusic, true);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        startActivity(new Intent(this, GameMenu.class));
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Music.play(this, R.raw.titlemusic, true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Music.stop();
    }
}
