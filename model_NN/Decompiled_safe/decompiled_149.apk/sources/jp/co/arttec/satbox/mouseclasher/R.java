package jp.co.arttec.satbox.mouseclasher;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int refreshAnimation = 2130771970;
        public static final int requestInterval = 2130771969;
        public static final int testMode = 2130771972;
        public static final int visibility = 2130771971;
    }

    public static final class drawable {
        public static final int back_easy = 2130837504;
        public static final int back_hard = 2130837505;
        public static final int back_normal = 2130837506;
        public static final int button = 2130837507;
        public static final int haikei = 2130837508;
        public static final int haikei2 = 2130837509;
        public static final int haikei3 = 2130837510;
        public static final int haikei_new = 2130837511;
        public static final int hammer = 2130837512;
        public static final int hammer2 = 2130837513;
        public static final int hammer3 = 2130837514;
        public static final int icon = 2130837515;
        public static final int menu_back = 2130837516;
        public static final int mouse_green = 2130837517;
        public static final int mouse_green_back = 2130837518;
        public static final int mouse_green_back_dmg = 2130837519;
        public static final int mouse_green_dmg = 2130837520;
        public static final int mouse_pink = 2130837521;
        public static final int mouse_pinkyarare = 2130837522;
        public static final int mouse_yellow = 2130837523;
        public static final int mouse_yellow_back = 2130837524;
        public static final int mouse_yellow_back_dmg = 2130837525;
        public static final int mouse_yellow_dmg = 2130837526;
        public static final int nezumi = 2130837527;
        public static final int nezumi2 = 2130837528;
        public static final int nezumiyarare = 2130837529;
        public static final int nezumiyarare2 = 2130837530;
        public static final int pc_satbox = 2130837531;
        public static final int play_logo = 2130837532;
        public static final int rank_logo = 2130837533;
        public static final int sat_logo = 2130837534;
        public static final int sozai_blue = 2130837535;
        public static final int star = 2130837536;
        public static final int star_red = 2130837537;
    }

    public static final class id {
        public static final int TextView01 = 2131230732;
        public static final int aboutbtn = 2131230748;
        public static final int adproxy = 2131230742;
        public static final int credit = 2131230720;
        public static final int end_bt_end = 2131230735;
        public static final int end_bt_score = 2131230734;
        public static final int footer = 2131230740;
        public static final int haikei2 = 2131230721;
        public static final int header = 2131230737;
        public static final int menu_apk = 2131230751;
        public static final int menu_end = 2131230753;
        public static final int menu_hp = 2131230752;
        public static final int menu_vib_switch = 2131230750;
        public static final int play_easy_button = 2131230724;
        public static final int play_font = 2131230722;
        public static final int play_hard_button = 2131230726;
        public static final int play_menu = 2131230723;
        public static final int play_normal_button = 2131230725;
        public static final int play_view = 2131230736;
        public static final int rank_easy_button = 2131230729;
        public static final int rank_font = 2131230727;
        public static final int rank_hard_button = 2131230731;
        public static final int rank_menu = 2131230728;
        public static final int rank_normal_button = 2131230730;
        public static final int rank_tx_touch = 2131230741;
        public static final int ranklistview = 2131230739;
        public static final int rankname = 2131230738;
        public static final int result = 2131230733;
        public static final int sat_link_button = 2131230749;
        public static final int startbtn = 2131230747;
        public static final int txtItem1 = 2131230744;
        public static final int txtItem21 = 2131230745;
        public static final int txtItem22 = 2131230746;
        public static final int txtRankNo = 2131230743;
    }

    public static final class layout {
        public static final int credit = 2130903040;
        public static final int game_menu = 2130903041;
        public static final int game_over = 2130903042;
        public static final int hammertouch = 2130903043;
        public static final int main = 2130903044;
        public static final int rank = 2130903045;
        public static final int row_my = 2130903046;
        public static final int row_world = 2130903047;
        public static final int setting = 2130903048;
        public static final int title = 2130903049;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int baku = 2130968576;
        public static final int ball_se = 2130968577;
        public static final int bat_se = 2130968578;
        public static final int bosu15 = 2130968579;
        public static final int hit_s11_c = 2130968580;
        public static final int hit_s14 = 2130968581;
        public static final int kachi16 = 2130968582;
        public static final int mainmusic = 2130968583;
        public static final int pen_botan = 2130968584;
        public static final int player_shot = 2130968585;
        public static final int se_hit = 2130968586;
        public static final int soft_tyu = 2130968587;
        public static final int timer = 2130968588;
        public static final int titlemusic = 2130968589;
    }

    public static final class string {
        public static final int about = 2131034115;
        public static final int aboutmsg = 2131034117;
        public static final int abouttitle = 2131034116;
        public static final int app_name = 2131034113;
        public static final int close = 2131034118;
        public static final int gamestartbtn = 2131034114;
        public static final int hello = 2131034112;
    }

    public static final class style {
        public static final int RankTextStyle = 2131099648;
    }

    public static final class styleable {
        public static final int[] mediba_ad_sdk_android_MasAdView = {R.attr.backgroundColor, R.attr.requestInterval, R.attr.refreshAnimation, R.attr.visibility, R.attr.testMode};
        public static final int mediba_ad_sdk_android_MasAdView_backgroundColor = 0;
        public static final int mediba_ad_sdk_android_MasAdView_refreshAnimation = 2;
        public static final int mediba_ad_sdk_android_MasAdView_requestInterval = 1;
        public static final int mediba_ad_sdk_android_MasAdView_testMode = 4;
        public static final int mediba_ad_sdk_android_MasAdView_visibility = 3;
    }
}
