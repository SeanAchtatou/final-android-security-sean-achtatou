package jp.co.arttec.satbox.rankresource;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.mouseclasher.BaseActivity;
import jp.co.arttec.satbox.mouseclasher.R;

public class GameRankMy extends BaseActivity {
    private static final float seVolume = 0.99f;
    /* access modifiers changed from: private */
    public int attackID = 0;
    private Button btn_world_ranking;
    /* access modifiers changed from: private */
    public boolean flg_end = false;
    /* access modifiers changed from: private */
    public int game_level;
    private ListView mListView;
    private String rankno = "";
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    private TextView txtRankName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.game_level = getIntent().getExtras().getInt("GameLevel");
        switch (this.game_level) {
            case R.styleable.mediba_ad_sdk_android_MasAdView_requestInterval:
                this.rankno = "level1";
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_refreshAnimation:
                this.rankno = "level2";
                break;
            case R.styleable.mediba_ad_sdk_android_MasAdView_visibility:
                this.rankno = "level3";
                break;
        }
        this.soundPool = new SoundPool(1, 3, 0);
        this.attackID = this.soundPool.load(this, R.raw.hit_s14, 1);
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (!GameRankMy.this.flg_end) {
                    GameRankMy.this.flg_end = true;
                    Intent intent = new Intent(GameRankMy.this, GameRankWorld.class);
                    intent.putExtra("GameLevel", GameRankMy.this.game_level);
                    GameRankMy.this.startActivity(intent);
                    GameRankMy.this.finish();
                }
            }
        });
        this.btn_world_ranking = (Button) findViewById(R.id.rank_tx_touch);
        this.btn_world_ranking.setOnClickListener(new View.OnClickListener() {
            public synchronized void onClick(View v) {
                Intent intent = new Intent(GameRankMy.this, GameRankWorld.class);
                GameRankMy.this.soundPool.play(GameRankMy.this.attackID, GameRankMy.seVolume, GameRankMy.seVolume, 1, 0, 1.0f);
                intent.putExtra("GameLevel", GameRankMy.this.game_level);
                GameRankMy.this.startActivity(intent);
                GameRankMy.this.finish();
            }
        });
        onScoreEntry(this.rankno);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.soundPool != null) {
            this.soundPool.release();
            this.soundPool = null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    public synchronized boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.flg_end) {
            this.flg_end = true;
            if (event.getAction() != 0) {
                z = false;
            } else {
                Intent intent = new Intent(this, GameRankWorld.class);
                intent.putExtra("GameLevel", this.game_level);
                this.soundPool.play(this.attackID, seVolume, seVolume, 1, 0, 1.0f);
                startActivity(intent);
                finish();
                z = true;
            }
        } else {
            z = false;
        }
        return z;
    }

    private synchronized void onScoreEntry(String subPrefID) {
        this.txtRankName = (TextView) findViewById(R.id.rankname);
        this.txtRankName.setText("My Ranking Best99");
        SharedPreferences pref = getSharedPreferences("prefkey", 0);
        List<RankStrDelivery> dataList = new ArrayList<>();
        for (int i = 0; i < 99; i++) {
            int intRankNo = i + 1;
            String strRankNo = String.format("%02d", Integer.valueOf(intRankNo));
            String strScore = String.valueOf(pref.getInt("MyScore" + subPrefID + strRankNo, 0));
            String strDate = pref.getString("MyDate" + subPrefID + strRankNo, "9999/99/99 99:99:99");
            dataList.add(new RankStrDelivery(intRankNo, strScore, strDate.substring(0, 10), strDate.substring(11, 19)));
        }
        ((ListView) findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(this, R.layout.row_my, dataList));
    }
}
