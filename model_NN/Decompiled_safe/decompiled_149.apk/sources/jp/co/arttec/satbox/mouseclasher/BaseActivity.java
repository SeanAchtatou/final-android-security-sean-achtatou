package jp.co.arttec.satbox.mouseclasher;

import android.app.Activity;
import android.os.Bundle;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
