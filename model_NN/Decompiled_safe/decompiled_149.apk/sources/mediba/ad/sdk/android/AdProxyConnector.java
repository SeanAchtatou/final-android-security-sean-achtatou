package mediba.ad.sdk.android;

import android.os.Build;
import android.util.Log;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class AdProxyConnector implements Runnable {
    private static final String _TAG = "AdProxyConnector";
    private static Executor m = null;
    private static String ua;
    protected AdProxyConnectorListener adproxyConnectorListener;
    protected byte[] buffer;
    protected Exception c = null;
    protected Map d;
    protected String g;
    protected boolean k;
    protected int max_retry;
    private String o;
    protected String request_header;
    protected String request_query;
    protected int retry_count;
    protected int timeout;
    protected URL url;

    public abstract boolean connect();

    public abstract void disconnect();

    protected AdProxyConnector(String s, String s1, AdProxyConnectorListener h1, int i1, Map map, String s2) {
        this.o = s;
        this.g = s1;
        this.adproxyConnectorListener = h1;
        this.timeout = i1;
        this.d = map;
        this.k = true;
        this.retry_count = 0;
        this.max_retry = 3;
        if (s2 != null) {
            this.request_query = s2;
            this.request_header = "application/x-www-form-urlencoded";
            return;
        }
        this.request_query = null;
        this.request_header = null;
    }

    public final void g() {
        if (m == null) {
            m = Executors.newCachedThreadPool();
        }
        m.execute(this);
    }

    /* access modifiers changed from: protected */
    public final void a(String s) {
        this.request_header = s;
    }

    public final void setMaxRetry(int i) {
        this.max_retry = i;
    }

    public byte[] d() {
        return this.buffer;
    }

    public static String user_agent() {
        if (ua == null) {
            StringBuffer stringbuffer = new StringBuffer();
            String release = Build.VERSION.RELEASE;
            if (release.length() > 0) {
                stringbuffer.append(release);
            } else {
                stringbuffer.append("1.0");
            }
            stringbuffer.append("; ");
            String s = Locale.getDefault().getLanguage();
            if (s != null) {
                stringbuffer.append(s.toLowerCase());
                if (Locale.getDefault().getCountry() != null) {
                    stringbuffer.append("-");
                    stringbuffer.append(Locale.getDefault().getCountry().toLowerCase());
                }
            } else {
                stringbuffer.append("en");
            }
            String model = Build.MODEL;
            if (model.length() > 0) {
                stringbuffer.append("; ");
                stringbuffer.append(model);
            }
            String build_id = Build.ID;
            if (build_id.length() > 0) {
                stringbuffer.append(" Build/");
                stringbuffer.append(build_id);
            }
            ua = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1", stringbuffer);
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, "user-agent :  " + ua);
            }
        }
        return ua;
    }

    public final String e() {
        return this.o;
    }

    public final URL f() {
        return this.url;
    }
}
