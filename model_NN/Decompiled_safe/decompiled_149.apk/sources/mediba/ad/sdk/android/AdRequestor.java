package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.util.Log;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import mediba.ad.sdk.android.AdProxy;
import org.json.JSONException;

final class AdRequestor {
    private static final String _TAG = "AdRequest";
    private static int b;
    private static long c;
    private static boolean permission_ANS;
    private static boolean permission_I;
    private static boolean permission_RPS;
    private static String query = null;
    private static String url = "http://api.ad.mediba.jp/webapi";

    AdRequestor() {
    }

    /* JADX INFO: Multiple debug info for r17v1 mediba.ad.sdk.android.AdProxyConnector: [D('context' android.content.Context), D('adproxyconnector' mediba.ad.sdk.android.AdProxyConnector)] */
    /* JADX INFO: Multiple debug info for r9v1 java.lang.String[]: [D('adproxy' mediba.ad.sdk.android.AdProxy), D('response' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r17v9 byte[]: [D('abyte0' byte[]), D('adproxyconnector' mediba.ad.sdk.android.AdProxyConnector)] */
    static AdProxy request(AdProxy.a a1, Context context, int primaryTextColor, int secondaryTextColor, int backgroundColor, AdContainer adcontainer, int l) throws JSONException {
        AdProxy adproxy;
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
            MasAdManager.clientError("INTERNET Permission missing in manifest");
            return null;
        }
        permission_ANS = context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != -1;
        permission_RPS = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != -1;
        long timestamp = SystemClock.uptimeMillis();
        String s = CreateAdRequestURL(context, l);
        AdProxyConnector adproxyconnector = AdProxyConnectorFactory.a(url, null, MasAdManager.getAndroidId(context), null, 3000, null, s);
        if (AdUtil.isLogEnable() != 0) {
            Log.d(_TAG, "POST params:  " + s);
        }
        String s2 = null;
        boolean connect_result = adproxyconnector.connect();
        if (connect_result) {
            s2 = new String(adproxyconnector.d());
        }
        if (!connect_result) {
            return null;
        }
        if (!s2.equals("")) {
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, "Ad response: " + s2);
            }
            adproxy = AdProxy.a(a1, s2, s2.split(" "), primaryTextColor, secondaryTextColor, backgroundColor, adcontainer);
        } else {
            adproxy = null;
        }
        long uptimeMillis = SystemClock.uptimeMillis() - timestamp;
        return adproxy;
    }

    /* JADX INFO: Multiple debug info for r4v2 java.lang.String: [D('timestamp' long), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v1 android.content.Context: [D('context1' android.content.Context), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r11v5 android.content.pm.PackageManager: [D('context1' android.content.Context), D('packagemanager' android.content.pm.PackageManager)] */
    /* JADX INFO: Multiple debug info for r11v6 java.util.List<android.content.pm.ResolveInfo>: [D('list' java.util.List), D('packagemanager' android.content.pm.PackageManager)] */
    private static String CreateAdRequestURL(Context context, int l) {
        String type;
        String type2;
        if (AdUtil.isLogEnable() != 0) {
            Log.d(_TAG, "Ad Request");
        }
        StringBuilder stringbuilder = new StringBuilder();
        String aid = MasAdManager.getADID(context);
        String ua = MasAdView.getUserAgent();
        String ip = MasAdManager.getIpAddress(context) != null ? MasAdManager.getIpAddress(context) : "127.0.0.1";
        String param = MasAdManager.getParams(context, permission_ANS);
        String time = Long.toString(System.currentTimeMillis() / 1000);
        String key = md5(MasAdManager.getAPIKEY(context) + time);
        if (MasAdManager.getDeviceId(context, permission_RPS) != null) {
            type2 = MasAdManager.getDeviceId(context, permission_RPS);
            type = "android_did";
        } else {
            type = "android";
            type2 = null;
        }
        if (type2 == null && MasAdManager.getAndroidId(context) != null) {
            type2 = MasAdManager.getAndroidId(context);
            type = "android_id";
        }
        if (type2 == null) {
            type2 = md5(ua + ip);
            type = "android_oid";
        }
        stringbuilder.append("aid").append("=").append(aid);
        extend_request_param(stringbuilder, "uid", type2);
        extend_request_param(stringbuilder, "type", type);
        extend_request_param(stringbuilder, "ua", ua);
        extend_request_param(stringbuilder, "ip", ip);
        extend_request_param(stringbuilder, "param", param);
        extend_request_param(stringbuilder, "time", time);
        extend_request_param(stringbuilder, "key", key);
        Context context2 = context;
        if (query == null) {
            StringBuilder stringbuilder2 = new StringBuilder();
            List list = context2.getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:jp.mediba")), 65563);
            if (list == null || list.size() == 0) {
                if (stringbuilder2.length() > 0) {
                    stringbuilder2.append(",");
                }
                stringbuilder2.append("a");
            }
            query = stringbuilder2.toString();
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, stringbuilder.toString());
        }
        return stringbuilder.toString();
    }

    private static void extend_request_param(StringBuilder stringbuilder, String string, String string2) {
        if (string2 != null && string2.length() > 0) {
            try {
                stringbuilder.append("&").append(URLEncoder.encode(string, "UTF-8")).append("=").append(URLEncoder.encode(string2, "UTF-8"));
                if (AdUtil.isLogEnable()) {
                    Log.d(_TAG, "    " + string + ": " + string2);
                }
            } catch (Exception e) {
            }
        }
    }

    public static String md5(String str) {
        if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
        }
        byte[] hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            hash = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashByte2MD5(hash);
    }

    private static String hashByte2MD5(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            if ((hash[i] & 255) < 16) {
                hexString.append("0" + Integer.toHexString(hash[i] & 255));
            } else {
                hexString.append(Integer.toHexString(hash[i] & 255));
            }
        }
        return hexString.toString();
    }
}
