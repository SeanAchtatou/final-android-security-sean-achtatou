package com.mobclick.android;

import android.content.Context;
import android.util.Log;
import javax.microedition.khronos.opengles.GL10;

public class l {
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            java.lang.String r4 = "MobclickAgent"
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x003d }
            java.lang.String r2 = "/proc/cpuinfo"
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x003d }
            if (r1 == 0) goto L_0x001d
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0030 }
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x0030 }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x0030 }
            r2.close()     // Catch:{ IOException -> 0x0051, FileNotFoundException -> 0x004a }
            r1.close()     // Catch:{ IOException -> 0x0051, FileNotFoundException -> 0x004a }
        L_0x001d:
            if (r0 == 0) goto L_0x002b
            r1 = 58
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
        L_0x002b:
            java.lang.String r0 = r0.trim()
            return r0
        L_0x0030:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0034:
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "Could not read from file /proc/cpuinfo"
            android.util.Log.e(r2, r3, r0)     // Catch:{ FileNotFoundException -> 0x004f }
            r0 = r1
            goto L_0x001d
        L_0x003d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0041:
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r2 = "Could not open file /proc/cpuinfo"
            android.util.Log.e(r4, r2, r0)
            r0 = r1
            goto L_0x001d
        L_0x004a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0041
        L_0x004f:
            r0 = move-exception
            goto L_0x0041
        L_0x0051:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.l.a():java.lang.String");
    }

    public static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static String[] a(GL10 gl10) {
        try {
            return new String[]{gl10.glGetString(7936), gl10.glGetString(7937)};
        } catch (Exception e) {
            Log.e("MobclickAgent", "Could not read gpu infor:", e);
            return new String[0];
        }
    }
}
