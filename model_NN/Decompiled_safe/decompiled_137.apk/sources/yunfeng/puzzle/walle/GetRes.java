package yunfeng.puzzle.walle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

public class GetRes {
    private Context mContext;

    public GetRes(Context context) {
        this.mContext = context;
    }

    public Bitmap getImageFromAssetFile(String fileName) {
        Bitmap image = null;
        try {
            InputStream is = this.mContext.getAssets().open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
            return image;
        } catch (Exception e) {
            return image;
        }
    }

    public Document getAdXml() {
        Document doc = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            doc = factory.newDocumentBuilder().parse(this.mContext.getAssets().open("ad.xml"));
            if (doc.getDocumentElement() != null) {
                return doc;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }
}
