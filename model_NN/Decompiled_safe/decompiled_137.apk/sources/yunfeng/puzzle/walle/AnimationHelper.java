package yunfeng.puzzle.walle;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

public class AnimationHelper {

    public interface ImageViewAMCallback {
        void imageViewAMCallback(View view);
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        inFromRight.setDuration(350);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public static Animation outToLeftAnimation() {
        Animation outtoLeft = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
        outtoLeft.setDuration(350);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        inFromLeft.setDuration(350);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outtoRight = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
        outtoRight.setDuration(350);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

    public static Animation showAnimation() {
        Animation aAnimation = new TranslateAnimation(1.0f, 0.0f, 1.0f, 0.0f);
        aAnimation.setDuration(500);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        return aAnimation;
    }

    public static Animation hideAnimation() {
        Animation aAnimation = new TranslateAnimation(1.0f, 0.0f, 1.0f, -1.0f);
        aAnimation.setDuration(500);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        return aAnimation;
    }

    public static Animation alphaHideAnimation() {
        Animation aAnimation = new AlphaAnimation(1.0f, 0.0f);
        aAnimation.setDuration(2000);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        return aAnimation;
    }

    public static Animation swapLeftSwapAnimation(final View iv, final float x2, final float y2, final ImageViewAMCallback imageViewAMCallback) {
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                if (message.what == 0 && message != null && message.obj != null) {
                    ImageViewAMCallback.this.imageViewAMCallback(iv);
                }
            }
        };
        Animation aAnimation = new TranslateAnimation(1.0f, x2, 1.0f, y2);
        aAnimation.setDuration(1000);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        aAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) iv.getLayoutParams();
                lp.leftMargin = (int) (((float) lp.leftMargin) + x2);
                lp.topMargin = (int) (((float) lp.topMargin) + y2);
                iv.setLayoutParams(lp);
                handler.sendMessage(handler.obtainMessage(0, iv));
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        return aAnimation;
    }

    public static Animation showAnimation(float endPos, float startPos, boolean isFillAfter) {
        Animation aAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, startPos, 1, endPos);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        aAnimation.setFillAfter(isFillAfter);
        aAnimation.setDuration(350);
        return aAnimation;
    }

    public static Animation hideAnimation(float startPos, float endPos, boolean isFillAfter) {
        Animation aAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, startPos, 1, endPos);
        aAnimation.setDuration(350);
        aAnimation.setFillAfter(isFillAfter);
        aAnimation.setInterpolator(new AccelerateInterpolator());
        return aAnimation;
    }
}
