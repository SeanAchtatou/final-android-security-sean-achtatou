package yunfeng.puzzle.walle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.mobclick.android.MobclickAgent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import yunfeng.puzzle.walle.AnimationHelper;
import yunfeng.puzzle.walle.SelectLevelDialog;

public class PuzzleActivity extends Activity {
    AdView ad;
    Button btnCurrentLevel;
    DisplayMetrics dm = new DisplayMetrics();
    Handler handlerLevelTime = new Handler() {
        public void handleMessage(Message msg) {
            if (!PuzzleActivity.this.levelFinish && !PuzzleActivity.this.isGamePause) {
                PuzzleActivity.this.levelTime++;
                ((TextView) PuzzleActivity.this.findViewById(R.id.tvLevelTimeInfo)).setText(String.valueOf(PuzzleActivity.this.levelTime / 60) + ":" + (PuzzleActivity.this.levelTime % 60));
            }
        }
    };
    boolean isGamePause = false;
    boolean isTask = false;
    boolean ivdest = true;
    boolean ivsrc = true;
    boolean levelFinish = false;
    int levelStep;
    int levelTime;
    int levelViewCount = 0;
    RelativeLayout ll;
    LinearLayout llad;
    LinearLayout lladlayer;
    LinearLayout llfinish;
    int lw;
    Timer mTimer = new Timer(true);
    TimerTask mTimerTask = new TimerTask() {
        public void run() {
            PuzzleActivity.this.handlerLevelTime.sendEmptyMessage(0);
        }
    };
    int[] picPuzzleIds = new int[54];
    int puzzleElemIdValue = 132341234;
    int rectangleSize;
    HashMap<String, String> rndPos = new HashMap<>();
    /* access modifiers changed from: private */
    public UserScore userScore = new UserScore();
    Vibrator vibrator;
    int w;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        readUserScore();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.lladlayer = (LinearLayout) findViewById(R.id.lladlayer);
        this.ad = new AdView(this, AdSize.BANNER, "a14e3fea3abda95");
        this.lladlayer.addView(this.ad);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.llad = (LinearLayout) findViewById(R.id.llad);
        this.llfinish = (LinearLayout) findViewById(R.id.llfinish);
        this.ll = (RelativeLayout) findViewById(R.id.llMain);
        for (int i = 0; i < this.picPuzzleIds.length; i++) {
            int idFlag = getResources().getIdentifier("p_" + Utility.padLeft(String.valueOf(i + 1), 3, '0'), "drawable", getPackageName());
            if (idFlag > 0) {
                this.picPuzzleIds[i] = idFlag;
            }
        }
        initControlBtn();
        this.ll.post(new Runnable() {
            public void run() {
                PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, true, GradientDrawable.Orientation.RIGHT_LEFT);
            }
        });
    }

    /* access modifiers changed from: private */
    public void newGate(int gateID, boolean isFirst, GradientDrawable.Orientation orientation) {
        int i;
        int irnd;
        int jrnd;
        this.levelFinish = false;
        this.levelTime = 0;
        this.levelStep = 0;
        this.levelViewCount = 0;
        setLevelStep();
        this.btnCurrentLevel.setText(String.valueOf(gateID) + "/" + 54);
        if (!this.isTask) {
            this.mTimer.schedule(this.mTimerTask, 1000, 1000);
            this.isTask = true;
        }
        if (!isFirst) {
            if (orientation == GradientDrawable.Orientation.RIGHT_LEFT) {
                this.ll.startAnimation(AnimationHelper.outToLeftAnimation());
            } else {
                this.ll.startAnimation(AnimationHelper.outToRightAnimation());
            }
            this.ll.removeAllViews();
            System.gc();
        }
        this.rectangleSize = GameConfig.getDifficultySize(gateID);
        this.llfinish.setVisibility(8);
        this.lladlayer.setVisibility(0);
        this.ad.loadAd(new AdRequest());
        this.rndPos.clear();
        Bitmap aBitmap = BitmapFactory.decodeResource(getResources(), this.picPuzzleIds[gateID - 1]);
        this.w = (int) (((float) (aBitmap.getWidth() / this.rectangleSize)) / this.dm.density);
        if (this.dm.widthPixels < this.dm.heightPixels) {
            i = this.dm.widthPixels;
        } else {
            i = this.dm.heightPixels;
        }
        this.lw = i / this.rectangleSize;
        for (int i2 = 0; i2 < this.rectangleSize; i2++) {
            for (int j = 0; j < this.rectangleSize; j++) {
                Bitmap bitmap = getRowColBitmap(aBitmap, i2, j, this.rectangleSize);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.lw, this.lw);
                ImageView imageView = new ImageView(this);
                imageView.setId(this.puzzleElemIdValue + (this.rectangleSize * i2) + j);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(bitmap);
                int pos = (this.rectangleSize * i2) + j;
                while (true) {
                    Random random = new Random(System.currentTimeMillis());
                    irnd = Math.abs(random.nextInt()) % this.rectangleSize;
                    jrnd = Math.abs(random.nextInt()) % this.rectangleSize;
                    if (!this.rndPos.containsKey(String.valueOf(String.valueOf(irnd) + "x" + jrnd)) && !this.rndPos.containsValue(String.valueOf(irnd) + "x" + jrnd)) {
                        break;
                    }
                }
                this.rndPos.put(String.valueOf(pos), String.valueOf(irnd) + "x" + jrnd);
                layoutParams.leftMargin = this.lw * irnd;
                layoutParams.topMargin = this.lw * jrnd;
                imageView.setTag(String.valueOf(irnd) + "x" + jrnd);
                imageView.setLayoutParams(layoutParams);
                imageView.setOnTouchListener(new View.OnTouchListener() {
                    float new_x;
                    float new_y;
                    private int oldLeft;
                    private int oldTop;
                    private float vRelX;
                    private float vRelY;

                    public boolean onTouch(View v, MotionEvent event) {
                        if (PuzzleActivity.this.levelFinish) {
                            return false;
                        }
                        switch (event.getAction()) {
                            case 0:
                                this.oldLeft = ((RelativeLayout.LayoutParams) v.getLayoutParams()).leftMargin;
                                this.oldTop = ((RelativeLayout.LayoutParams) v.getLayoutParams()).topMargin;
                                ((RelativeLayout.LayoutParams) v.getLayoutParams()).getRules();
                                this.vRelX = event.getX();
                                this.vRelY = event.getY();
                                PuzzleActivity.this.ll.bringChildToFront(v);
                                break;
                            case 1:
                                for (int i = 0; i < PuzzleActivity.this.rectangleSize; i++) {
                                    int j = 0;
                                    while (j < PuzzleActivity.this.rectangleSize) {
                                        View iv = PuzzleActivity.this.ll.findViewById(PuzzleActivity.this.puzzleElemIdValue + (PuzzleActivity.this.rectangleSize * i) + j);
                                        int left = ((RelativeLayout.LayoutParams) iv.getLayoutParams()).leftMargin;
                                        int top = ((RelativeLayout.LayoutParams) iv.getLayoutParams()).topMargin;
                                        int nx = (int) this.new_x;
                                        int ny = (int) this.new_y;
                                        if (v.getId() == iv.getId() || nx < left || nx > PuzzleActivity.this.lw + left || ny < top || ny > PuzzleActivity.this.lw + top) {
                                            j++;
                                        } else {
                                            Object o = iv.getTag();
                                            RelativeLayout.LayoutParams olp = (RelativeLayout.LayoutParams) iv.getLayoutParams();
                                            olp.leftMargin = this.oldLeft;
                                            olp.topMargin = this.oldTop;
                                            iv.setLayoutParams(olp);
                                            iv.setTag(v.getTag());
                                            RelativeLayout.LayoutParams nlp = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                            nlp.leftMargin = left;
                                            nlp.topMargin = top;
                                            v.setLayoutParams(nlp);
                                            v.setTag(o);
                                            PuzzleActivity.this.playStepMedia();
                                            PuzzleActivity.this.checkOK();
                                            return false;
                                        }
                                    }
                                }
                                RelativeLayout.LayoutParams olp2 = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                olp2.leftMargin = this.oldLeft;
                                olp2.topMargin = this.oldTop;
                                v.setLayoutParams(olp2);
                                break;
                            case 2:
                                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) v.getLayoutParams();
                                int tx = (int) ((((float) lp.leftMargin) + event.getX()) - this.vRelX);
                                int ty = (int) ((((float) lp.topMargin) + event.getY()) - this.vRelY);
                                if (tx < 0) {
                                    tx = 0;
                                }
                                if (tx > PuzzleActivity.this.dm.widthPixels - PuzzleActivity.this.lw) {
                                    tx = PuzzleActivity.this.dm.widthPixels - PuzzleActivity.this.lw;
                                }
                                if (ty < 0) {
                                    ty = 0;
                                }
                                if (ty > PuzzleActivity.this.dm.widthPixels - PuzzleActivity.this.lw) {
                                    ty = PuzzleActivity.this.dm.widthPixels - PuzzleActivity.this.lw;
                                }
                                this.new_x = ((float) tx) + this.vRelX;
                                this.new_y = ((float) ty) + this.vRelY;
                                lp.leftMargin = tx;
                                lp.topMargin = ty;
                                v.setLayoutParams(lp);
                                break;
                            case 3:
                                Log.d("ACTION_CANCEL", "ACTION_CANCEL3333333");
                                break;
                        }
                        return true;
                    }
                });
                this.ll.addView(imageView);
            }
        }
        if (orientation == GradientDrawable.Orientation.RIGHT_LEFT) {
            this.ll.startAnimation(AnimationHelper.inFromRightAnimation());
        } else {
            this.ll.startAnimation(AnimationHelper.inFromLeftAnimation());
        }
    }

    /* access modifiers changed from: private */
    public void helpAutoPuzzle() {
        if (this.ivsrc && this.ivdest) {
            if (this.levelViewCount < 3) {
                this.levelViewCount++;
                List<int[]> nofinish = new Vector<>();
                for (int i = 0; i < this.rectangleSize; i++) {
                    for (int j = 0; j < this.rectangleSize; j++) {
                        if (!this.ll.findViewById(this.puzzleElemIdValue + (this.rectangleSize * i) + j).getTag().toString().equalsIgnoreCase(String.valueOf(i) + "x" + j)) {
                            nofinish.add(new int[]{i, j});
                        }
                    }
                }
                if (nofinish.size() > 0) {
                    int rndloc = Math.abs(new Random(System.currentTimeMillis()).nextInt()) % nofinish.size();
                    swapPuzzleElem(((int[]) nofinish.get(rndloc))[0], ((int[]) nofinish.get(rndloc))[1]);
                    return;
                }
                return;
            }
            alert(getResources().getString(R.string.alertoverviewcount).replace("{count}", "3"));
        }
    }

    private void swapPuzzleElem(int xpos, int ypos) {
        this.ivsrc = false;
        this.ivdest = false;
        View iv = this.ll.findViewById(this.puzzleElemIdValue + (this.rectangleSize * xpos) + ypos);
        iv.bringToFront();
        int okposx = xpos * this.lw;
        int okposy = ypos * this.lw;
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) iv.getLayoutParams();
        int relxt = lp.leftMargin;
        int relyt = lp.topMargin;
        int relx = okposx - lp.leftMargin;
        iv.startAnimation(AnimationHelper.swapLeftSwapAnimation(iv, (float) relx, (float) (okposy - lp.topMargin), new AnimationHelper.ImageViewAMCallback() {
            public void imageViewAMCallback(View iv) {
                PuzzleActivity.this.ivsrc = true;
            }
        }));
        View ivt = this.ll.findViewWithTag(String.valueOf(xpos) + "x" + ypos);
        ivt.bringToFront();
        RelativeLayout.LayoutParams lpt = (RelativeLayout.LayoutParams) ivt.getLayoutParams();
        ivt.startAnimation(AnimationHelper.swapLeftSwapAnimation(ivt, (float) (relxt - lpt.leftMargin), (float) (relyt - lpt.topMargin), new AnimationHelper.ImageViewAMCallback() {
            public void imageViewAMCallback(View iv) {
                PuzzleActivity.this.ivdest = true;
            }
        }));
        Object o = iv.getTag();
        iv.setTag(ivt.getTag());
        ivt.setTag(o);
        checkOK();
    }

    private int getOKs() {
        int c = 0;
        for (int i = 0; i < this.rectangleSize; i++) {
            for (int j = 0; j < this.rectangleSize; j++) {
                if (this.ll.findViewById(this.puzzleElemIdValue + (this.rectangleSize * i) + j).getTag().toString().equalsIgnoreCase(String.valueOf(i) + "x" + j)) {
                    c++;
                }
            }
        }
        return c;
    }

    /* access modifiers changed from: private */
    public void checkOK() {
        setLevelStep();
        if (getOKs() == this.rectangleSize * this.rectangleSize) {
            if (this.userScore.level >= this.userScore.finishLevel) {
                this.userScore.finishLevel = this.userScore.level;
            }
            alert(getResources().getString(R.string.alertfinishlevel));
            writeUserScore();
            this.levelFinish = true;
            playFinishMedia();
            this.llfinish.setVisibility(0);
            this.lladlayer.setVisibility(8);
        }
    }

    private void playFinishMedia() {
        this.vibrator.vibrate(100);
    }

    /* access modifiers changed from: private */
    public void playStepMedia() {
        this.vibrator.vibrate(30);
    }

    private void setLevelStep() {
        ((TextView) findViewById(R.id.tvLevelMoveInfo)).setText(new StringBuilder().append(this.levelStep).toString());
        this.levelStep++;
    }

    /* access modifiers changed from: private */
    public void alert(String mess) {
        Toast toast = Toast.makeText(this, mess, 0);
        toast.setGravity(17, 0, 0);
        toast.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap getRowColBitmap(Bitmap aBitmap, int row, int col, int rectangleSize2) {
        int width = aBitmap.getWidth() / rectangleSize2;
        return new BitmapDrawable(Bitmap.createBitmap(aBitmap, row * width, col * width, width, width, new Matrix(), true)).getBitmap();
    }

    /* access modifiers changed from: protected */
    public void dialog() {
        List<MoreGameItem> gameList = MoreGame.getMoreGame(this);
        final String pname = gameList.get(Math.abs(new Random(System.currentTimeMillis()).nextInt()) % gameList.size()).PackageName;
        Bitmap amap = new GetRes(this).getImageFromAssetFile(String.valueOf(pname) + ".png");
        ImageView image = new ImageView(this);
        image.setImageBitmap(amap);
        DisplayMetrics dm1 = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm1);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(-2, -2);
        llp.weight = (float) ((int) (((float) dm1.widthPixels) - (120.0f / dm1.density)));
        llp.height = (int) (((float) dm1.widthPixels) - (120.0f / dm1.density));
        image.setLayoutParams(llp);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(image);
        builder.setIcon((int) R.drawable.icon);
        builder.setMessage(getString(R.string.alertexit));
        builder.setTitle(17039380);
        builder.setNeutralButton((int) R.string.installinfo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PuzzleActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + pname)));
            }
        });
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                File dir = PuzzleActivity.this.getCacheDir();
                if (dir != null) {
                    dir.isDirectory();
                }
                PuzzleActivity.this.finish();
                Process.killProcess(Process.myPid());
            }
        });
        builder.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        dialog();
        return true;
    }

    private void readUserScore() {
        SharedPreferences settings = getSharedPreferences("score", 0);
        int level = settings.getInt("level", 1);
        int countStep = settings.getInt("countStep", 0);
        int countTime = settings.getInt("countTime", 0);
        int finishLevel = settings.getInt("finishLevel", 0);
        if (level == 1 && finishLevel == 1) {
            settings.edit().putInt("level", 1).commit();
            settings.edit().putInt("countStep", 0).commit();
            settings.edit().putInt("countTime", 0).commit();
            settings.edit().putInt("finishLevel", 0).commit();
        }
        this.userScore.countStep = countStep;
        this.userScore.countTime = countTime;
        this.userScore.level = level;
        this.userScore.finishLevel = finishLevel;
    }

    private void writeUserScore() {
        SharedPreferences settings = getSharedPreferences("score", 0);
        settings.edit().putInt("level", this.userScore.level).commit();
        settings.edit().putInt("countStep", this.userScore.countStep).commit();
        settings.edit().putInt("countTime", this.userScore.countTime).commit();
        settings.edit().putInt("finishLevel", this.userScore.finishLevel).commit();
    }

    public void onRestart() {
        this.isGamePause = false;
        super.onRestart();
    }

    public void onStart() {
        this.isGamePause = false;
        super.onStart();
    }

    public void onStop() {
        this.isGamePause = true;
        super.onStop();
    }

    public void onResume() {
        this.isGamePause = false;
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        this.isGamePause = true;
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private void initControlBtn() {
        ((Button) findViewById(R.id.btnshare)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.SUBJECT", PuzzleActivity.this.getResources().getString(R.string.sharetitle).replace("{app_name}", PuzzleActivity.this.getResources().getString(R.string.app_name)));
                intent.putExtra("android.intent.extra.TEXT", PuzzleActivity.this.getResources().getString(R.string.sharecontent).replace("{app_name}", PuzzleActivity.this.getResources().getString(R.string.app_name)));
                PuzzleActivity.this.startActivity(Intent.createChooser(intent, PuzzleActivity.this.getTitle()));
            }
        });
        ((Button) findViewById(R.id.btnwp)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    PuzzleActivity.this.getApplicationContext().setWallpaper(BitmapFactory.decodeResource(PuzzleActivity.this.getResources(), PuzzleActivity.this.picPuzzleIds[PuzzleActivity.this.userScore.level - 1]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        ((Button) findViewById(R.id.btnnext)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PuzzleActivity.this.userScore.level < 54) {
                    PuzzleActivity.this.userScore.level++;
                    PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, false, GradientDrawable.Orientation.RIGHT_LEFT);
                    return;
                }
                PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertfinishallnotcontinue));
            }
        });
        ((Button) findViewById(R.id.btnsave)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String fn = Utility.SaveSDCard(PuzzleActivity.this.picPuzzleIds[PuzzleActivity.this.userScore.level - 1], PuzzleActivity.this);
                if (fn == null) {
                    PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertsavefail));
                } else {
                    PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertsavesecc).replace("{sdcardfn}", fn));
                }
            }
        });
        ((ImageButton) findViewById(R.id.imgBtnNextLevel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PuzzleActivity.this.userScore.level >= 54) {
                    PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertfinishallnotcontinue));
                } else if (PuzzleActivity.this.userScore.level > PuzzleActivity.this.userScore.finishLevel) {
                    PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertnotfinishnotcontinue));
                } else {
                    PuzzleActivity.this.userScore.level++;
                    PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, false, GradientDrawable.Orientation.RIGHT_LEFT);
                }
            }
        });
        ((ImageButton) findViewById(R.id.imgBtnPrevLevel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PuzzleActivity.this.userScore.level > 1) {
                    PuzzleActivity.this.userScore.level--;
                    PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, false, GradientDrawable.Orientation.LEFT_RIGHT);
                    return;
                }
                PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertisfirstnotprev));
            }
        });
        ((ImageButton) findViewById(R.id.imgBtnViewPic)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PuzzleActivity.this.levelViewCount < 3) {
                    PuzzleActivity.this.levelViewCount++;
                    ViewPicDialog vpDialog = new ViewPicDialog(PuzzleActivity.this);
                    vpDialog.setImageResrources(PuzzleActivity.this.picPuzzleIds[PuzzleActivity.this.userScore.level - 1]);
                    vpDialog.setTextInfo(String.valueOf(PuzzleActivity.this.levelViewCount) + "/" + 3);
                    vpDialog.show();
                    return;
                }
                PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertoverviewcount).replace("{count}", "3"));
            }
        });
        ((ImageButton) findViewById(R.id.imgBtnRestarLevel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, false, GradientDrawable.Orientation.RIGHT_LEFT);
            }
        });
        ((ImageButton) findViewById(R.id.imgBtnAutoPuzzle)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PuzzleActivity.this.helpAutoPuzzle();
            }
        });
        TextView textView = (TextView) findViewById(R.id.tvLevelCounts);
        this.btnCurrentLevel = (Button) findViewById(R.id.btnCurrentLevel);
        this.btnCurrentLevel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectLevelDialog slDialog = new SelectLevelDialog(PuzzleActivity.this, 54, new SelectLevelDialog.SelectDialogListener() {
                    public void onOkClick(int level) {
                        if (level > PuzzleActivity.this.userScore.finishLevel || level > 54) {
                            PuzzleActivity.this.alert(PuzzleActivity.this.getResources().getString(R.string.alertnotselectnotfinishlevel));
                            return;
                        }
                        GradientDrawable.Orientation o = GradientDrawable.Orientation.LEFT_RIGHT;
                        if (PuzzleActivity.this.userScore.level < level) {
                            o = GradientDrawable.Orientation.RIGHT_LEFT;
                        }
                        PuzzleActivity.this.userScore.level = level;
                        PuzzleActivity.this.newGate(PuzzleActivity.this.userScore.level, false, o);
                    }
                });
                slDialog.setTitle(PuzzleActivity.this.getResources().getString(R.string.selectlevel));
                slDialog.show();
            }
        });
    }
}
