package yunfeng.puzzle.walle;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;
import java.util.Vector;

public class SelectLevelDialog extends Dialog implements View.OnClickListener {
    private Context context;
    private ExpandableListView exListView;
    /* access modifiers changed from: private */
    public List<LevelGroupData> levelGroup = new Vector();
    private SelectDialogListener listener;

    public interface SelectDialogListener {
        void onOkClick(int i);
    }

    public SelectLevelDialog(Context context2, int levelCount, SelectDialogListener listener2) {
        super(context2);
        this.context = context2;
        View rowView = getLayoutInflater().inflate((int) R.layout.levelselect, (ViewGroup) null);
        setContentView(rowView);
        this.listener = listener2;
        int groups = levelCount / 10;
        groups = levelCount % 10 != 0 ? groups + 1 : groups;
        for (int i = 0; i < groups; i++) {
            LevelGroupData lgd = new LevelGroupData();
            int startLevel = (i * 10) + 1;
            int endLevel = (i + 1) * 10;
            endLevel = endLevel > levelCount ? levelCount : endLevel;
            List<String> groupInLevel = new Vector<>();
            for (int j = startLevel; j <= endLevel; j++) {
                groupInLevel.add(new StringBuilder().append(j).toString());
            }
            lgd.startLevel = startLevel;
            lgd.endLevel = endLevel;
            lgd.groupName = String.valueOf(startLevel) + "-" + endLevel;
            lgd.subLevel = groupInLevel;
            this.levelGroup.add(lgd);
        }
        this.exListView = (ExpandableListView) rowView.findViewById(R.id.expandable_list_view);
        this.exListView.setAdapter(new InfoDetailsAdapter(this.context));
    }

    public class LevelGroupData {
        public int endLevel;
        public String groupName;
        public int startLevel;
        public List<String> subLevel = new Vector();

        public LevelGroupData() {
        }
    }

    class InfoDetailsAdapter extends BaseExpandableListAdapter {
        private Context context;

        public InfoDetailsAdapter(Context context2) {
            this.context = context2;
        }

        public Object getChild(int groupPosition, int childPosition) {
            return ((LevelGroupData) SelectLevelDialog.this.levelGroup.get(groupPosition)).subLevel.get(childPosition);
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(-1, 64);
            RelativeLayout re = new RelativeLayout(this.context);
            re.setLayoutParams(lp);
            TextView text = new TextView(this.context);
            text.setTextSize(20.0f);
            text.setLayoutParams(lp);
            text.setGravity(19);
            text.setPadding(36, 0, 0, 0);
            text.setText(this.context.getResources().getString(R.string.selectlevelinfoformat).replace("{Level}", ((LevelGroupData) SelectLevelDialog.this.levelGroup.get(groupPosition)).subLevel.get(childPosition)));
            re.setTag(((LevelGroupData) SelectLevelDialog.this.levelGroup.get(groupPosition)).subLevel.get(childPosition));
            re.setOnClickListener(SelectLevelDialog.this);
            re.addView(text);
            return re;
        }

        public int getChildrenCount(int groupPosition) {
            return ((LevelGroupData) SelectLevelDialog.this.levelGroup.get(groupPosition)).subLevel.size();
        }

        public Object getGroup(int groupPosition) {
            return SelectLevelDialog.this.levelGroup.get(groupPosition);
        }

        public int getGroupCount() {
            return SelectLevelDialog.this.levelGroup.size();
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = SelectLevelDialog.this.getLayoutInflater().inflate((int) R.layout.group_bar, (ViewGroup) null);
            }
            RelativeLayout re = (RelativeLayout) view.findViewById(R.id.layout_013);
            ((TextView) view.findViewById(R.id.content_text)).setText(((LevelGroupData) SelectLevelDialog.this.levelGroup.get(groupPosition)).groupName);
            if (isExpanded) {
                re.setBackgroundColor(-1);
            } else {
                re.setBackgroundColor(-7829368);
            }
            return view;
        }

        public boolean hasStableIds() {
            return false;
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    public void onClick(View v) {
        if (v.getTag() != null) {
            this.listener.onOkClick(Integer.parseInt(v.getTag().toString()));
            dismiss();
        }
    }
}
