package com.selticeapps.funfunminigolflite;

public class Movers {
    private static final int PLATFORM_E = 39;
    private static final int PLATFORM_EW = 3;
    private static final int PLATFORM_N = 37;
    private static final int PLATFORM_NS = 4;
    private static final int PLATFORM_S = 38;
    private static final int PLATFORM_W = 40;
    private static final int WALL_E = 43;
    private static final int WALL_EW = 1;
    private static final int WALL_N = 41;
    private static final int WALL_NS = 2;
    private static final int WALL_S = 42;
    private static final int WALL_W = 44;
    private int basecolor;
    private int blocktype;
    private int bottom_most = 340;
    private int direction;
    private int height = 20;
    private int left_most = 0;
    private int mFPS;
    long mFrameTimer;
    private int right_most = 480;
    private int top_most = 0;
    private int width = 20;
    private int x;
    private int y;

    public Movers(int mX, int mY, int mBlocktype, int mBasecolor, int mDirection, int mTopMost, int mBottomMost, int mLeftMost, int mRightMost) {
        this.x = mX;
        this.y = mY;
        this.width = 20;
        this.height = 20;
        this.direction = mDirection;
        this.blocktype = mBlocktype;
        this.basecolor = mBasecolor;
        this.top_most = mTopMost;
        this.bottom_most = mBottomMost;
        this.left_most = mLeftMost;
        this.right_most = mRightMost;
        this.mFrameTimer = 0;
        this.mFPS = 20;
    }

    public boolean checkCollision(int x2, int y2) {
        if (x2 < this.x || x2 > this.x + this.width || y2 < this.y || y2 > this.y + this.height) {
            return false;
        }
        return true;
    }

    public void update(long GameTime) {
        if (GameTime > this.mFrameTimer + ((long) this.mFPS)) {
            this.mFrameTimer = GameTime;
            if (this.left_most == this.right_most) {
                this.y += this.direction;
                if (this.y > this.bottom_most) {
                    this.y = this.bottom_most - 1;
                    this.direction = -1;
                } else if (this.y < this.top_most) {
                    this.y = this.top_most + 1;
                    this.direction = 1;
                }
            } else {
                this.x += this.direction;
                if (this.x > this.right_most) {
                    this.x = this.right_most - 1;
                    this.direction = -1;
                } else if (this.x < this.left_most) {
                    this.x = this.left_most + 1;
                    this.direction = 1;
                }
            }
        }
    }

    public int getDirection() {
        return this.direction;
    }

    public int getBlocktype() {
        return this.blocktype;
    }

    public int getBasecolor() {
        return this.basecolor;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }
}
