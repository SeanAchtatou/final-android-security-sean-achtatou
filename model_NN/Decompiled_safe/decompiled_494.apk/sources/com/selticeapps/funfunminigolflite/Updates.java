package com.selticeapps.funfunminigolflite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

class Updates {
    private static final String GAME_ASSET_UPDATES = "UPDATES";
    private static final String GAME_PREFERENCES_UPDATES = "updates426a";
    private static final String GAME_PREFERENCE_UPDATES_ACCEPTED = "updates426a.accepted";

    interface OnUpdatesAgreement {
        void onUpdatesAgreement();
    }

    Updates() {
    }

    static boolean show(final Activity activity, boolean forceShow) {
        final SharedPreferences preferences = activity.getSharedPreferences(GAME_PREFERENCES_UPDATES, 0);
        if (preferences.getBoolean(GAME_PREFERENCE_UPDATES_ACCEPTED, false) && !forceShow) {
            return true;
        }
        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(activity);
        alertbuilder.setTitle((int) R.string.updates_title);
        alertbuilder.setCancelable(true);
        alertbuilder.setPositiveButton((int) R.string.updates_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Updates.accept(preferences);
                if (activity instanceof OnUpdatesAgreement) {
                    ((OnUpdatesAgreement) activity).onUpdatesAgreement();
                }
            }
        });
        alertbuilder.setMessage(loadEula(activity));
        alertbuilder.create().show();
        return false;
    }

    private static CharSequence loadEula(Activity activity) {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(activity.getAssets().open(GAME_ASSET_UPDATES)));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        closeFileStream(in2);
                        return buffer;
                    }
                    buffer.append(line).append(10);
                }
            } catch (IOException e) {
                in = in2;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeFileStream(in);
                throw th;
            }
        } catch (IOException e2) {
            closeFileStream(in);
            return "";
        } catch (Throwable th2) {
            th = th2;
            closeFileStream(in);
            throw th;
        }
    }

    private static void closeFileStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static void accept(SharedPreferences preferences) {
        preferences.edit().putBoolean(GAME_PREFERENCE_UPDATES_ACCEPTED, true).commit();
    }
}
