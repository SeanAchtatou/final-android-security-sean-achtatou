package com.selticeapps.funfunminigolflite;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "highscores.db";
    private static String DB_PATH = "/data/data/com.selticeapps.funfunminigolflite/databases/";
    private final Context myContext;
    private SQLiteDatabase myDataBase;
    private boolean noDB = false;

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
        this.noDB = false;
        if (!dbExist) {
            getWritableDatabase();
        }
    }

    public boolean getNoDB() {
        return this.noDB;
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(String.valueOf(DB_PATH) + DB_NAME, null, 268435456);
        } catch (SQLiteException e) {
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    private void copyDataBase() throws IOException {
        try {
            InputStream myInput = this.myContext.getAssets().open(DB_NAME);
            OutputStream myOutput = new FileOutputStream("/root/" + DB_PATH + DB_NAME);
            byte[] buffer = new byte[1024];
            while (true) {
                int length = myInput.read(buffer);
                if (length <= 0) {
                    myOutput.flush();
                    myOutput.close();
                    myInput.close();
                    return;
                }
                myOutput.write(buffer, 0, length);
            }
        } catch (Exception e) {
            this.noDB = true;
        }
    }

    public void openDataBase() throws SQLException {
        try {
            this.myDataBase = SQLiteDatabase.openDatabase(String.valueOf(DB_PATH) + DB_NAME, null, 0);
            this.myDataBase.execSQL("CREATE TABLE IF NOT EXISTS highscores (course TEXT, playdate TEXT, uid TEXT, _id INTEGER PRIMARY KEY, player TEXT, score INTEGER)");
        } catch (SQLiteException e) {
        }
    }

    public void updateScore(String mPlayerName, int score, int mOnCourse) {
        new String();
        if (!this.myDataBase.isOpen()) {
            try {
                openDataBase();
            } catch (Exception e) {
            }
        }
        if (this.myDataBase.isOpen()) {
            try {
                Calendar cal = new GregorianCalendar();
                this.myDataBase.execSQL("INSERT INTO highscores (playdate,player,score,course) VALUES ('" + Integer.toString(cal.get(2) + 1) + "/" + Integer.toString(cal.get(5)) + "/" + Integer.toString(cal.get(1)) + "', '" + mPlayerName.replace("'", "''") + "', '" + Integer.toString(score) + "', '" + Integer.toString(mOnCourse) + "')");
            } catch (Exception e2) {
            }
        }
    }

    public void clearScores(int mOnCourse) {
        new String();
        if (this.myDataBase.isOpen()) {
            try {
                this.myDataBase.execSQL("DELETE FROM highscores WHERE course='" + mOnCourse + "'");
            } catch (Exception e) {
            }
        }
    }

    public String readHighscores(int mOnCourse) throws SQLException {
        new String();
        String tmp = "";
        if (!this.myDataBase.isOpen()) {
            try {
                openDataBase();
            } catch (Exception e) {
            }
        }
        if (!this.myDataBase.isOpen()) {
            return tmp;
        }
        try {
            Cursor c = this.myDataBase.query("highscores", new String[]{"_id", "playdate", "score", "player"}, "score<>'' AND course='" + mOnCourse + "'", null, null, null, "score ASC LIMIT 5");
            int playdate = c.getColumnIndex("playdate");
            int player = c.getColumnIndex("player");
            int score = c.getColumnIndex("score");
            int idcolumn = c.getColumnIndex("_id");
            int i = 0;
            if (c.getCount() > 0) {
                new String();
                new String();
                c.moveToFirst();
                do {
                    i++;
                    int gID = c.getInt(idcolumn);
                    int scores = c.getInt(score);
                    String playdates = c.getString(playdate);
                    String players = c.getString(player);
                    if (players == null) {
                        players = "Unknown";
                    }
                    if (playdates == null) {
                        playdates = "";
                    }
                    tmp = String.valueOf(tmp) + playdates + ":::" + scores + ":::" + players + "::::::";
                    if (!c.moveToNext()) {
                        break;
                    }
                } while (i <= 4);
            } else {
                tmp = "";
                for (int i2 = 1; i2 <= 5; i2++) {
                    tmp = String.valueOf(tmp) + " ::: ::::::";
                }
            }
            c.close();
            return tmp;
        } catch (Exception e2) {
            return "Error Getting Scores";
        }
    }

    public int lowestScore(int mOnCourse) throws SQLException {
        int scores;
        new String();
        if (!this.myDataBase.isOpen()) {
            try {
                openDataBase();
            } catch (Exception e) {
            }
        }
        if (!this.myDataBase.isOpen()) {
            return 50000;
        }
        try {
            Cursor c = this.myDataBase.query("highscores", new String[]{"_id", "score"}, "score<>'' AND course='" + mOnCourse + "'", null, null, null, "score ASC LIMIT 5");
            int score = c.getColumnIndex("score");
            if (c.getCount() > 0) {
                c.moveToLast();
                scores = c.getInt(score);
                if (c.getCount() < 5) {
                    scores = 50000;
                }
            } else {
                scores = 50000;
            }
            c.close();
            return scores;
        } catch (Exception e2) {
            return 50000;
        }
    }

    public int lowestScoreOfAll(int mOnCourse) throws SQLException {
        int scores;
        new String();
        if (!this.myDataBase.isOpen()) {
            try {
                openDataBase();
            } catch (Exception e) {
            }
        }
        if (!this.myDataBase.isOpen()) {
            return 50000;
        }
        try {
            Cursor c = this.myDataBase.query("highscores", new String[]{"_id", "score"}, "score<>'' AND course='" + mOnCourse + "'", null, null, null, "score ASC LIMIT 1");
            int score = c.getColumnIndex("score");
            if (c.getCount() > 0) {
                c.moveToFirst();
                scores = c.getInt(score);
            } else {
                scores = 50000;
            }
            c.close();
            return scores;
        } catch (Exception e2) {
            return 50000;
        }
    }

    public synchronized void close() {
        if (this.myDataBase != null) {
            this.myDataBase.close();
        }
        super.close();
    }

    public void onCreate(SQLiteDatabase db) {
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
