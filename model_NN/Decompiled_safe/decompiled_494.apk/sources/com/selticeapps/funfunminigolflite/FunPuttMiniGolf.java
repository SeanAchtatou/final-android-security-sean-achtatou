package com.selticeapps.funfunminigolflite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import com.selticeapps.funfunminigolflite.GameView;

public class FunPuttMiniGolf extends Activity {
    public static final int LSMODE_DEFAULT = 3;
    public static final int LSMODE_LARGE = 2;
    private static final int MENU_AUTOAIMOFF = 25;
    private static final int MENU_AUTOAIMON = 24;
    private static final int MENU_CLEAR = 21;
    private static final int MENU_CODE = 28;
    private static final int MENU_EXIT = 1;
    private static final int MENU_LSMODEOFF = 26;
    private static final int MENU_LSMODEON = 27;
    private static final int MENU_RESTART = 20;
    private static final int MENU_RESTARTMULLIGAN = 22;
    private static final int MENU_RESTARTNULLSCORE = 23;
    private static final int MENU_SOUNDSOFF = 5;
    private static final int MENU_SOUNDSON = 3;
    private static final int MENU_SPLASHSCREEN = 19;
    private static final int MENU_VIEWTOS = 801;
    private static final int MENU_VIEWUPDATES = 802;
    public static final int NEVERSET = 1;
    public static final String PREFS_NAME = "FunFunMiniGolfLite";
    private AlertDialog.Builder alert;
    private float canvasHeight = 320.0f;
    private float canvasWidth = 480.0f;
    public DisplayMetrics dm;
    /* access modifiers changed from: private */
    public EditText input;
    public boolean lsmode = false;
    private boolean mFirstRun = true;
    /* access modifiers changed from: private */
    public GameView.GameThread mGameThread;
    private GameView mGameView;
    /* access modifiers changed from: private */
    public boolean mReleased = true;
    public int mSetLS = -1;

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.clear();
        if (this.mGameThread.getGameMode() == 3) {
            menu.add(0, 19, 0, (int) R.string.menu_splashscreen);
            menu.add(0, (int) MENU_RESTARTMULLIGAN, 0, (int) R.string.menu_restartmulligan);
            menu.add(0, 23, 0, (int) R.string.menu_restartnullscore);
            if (this.mGameThread.getAimMethod() == 1) {
                if (this.mGameThread.getAutoAim()) {
                    menu.add(0, (int) MENU_AUTOAIMON, 0, (int) R.string.menu_autoaimon);
                } else {
                    menu.add(0, (int) MENU_AUTOAIMOFF, 0, (int) R.string.menu_autoaimoff);
                }
            }
            if (this.mGameThread.getSounds()) {
                menu.add(0, 3, 0, (int) R.string.menu_soundson);
            } else {
                menu.add(0, 5, 0, (int) R.string.menu_soundsoff);
            }
        } else if (this.mGameThread.getGameMode() == 4 || this.mGameThread.getGameMode() == 18 || this.mGameThread.getGameMode() == 21) {
            menu.add(0, 21, 0, (int) R.string.menu_clear);
        }
        if (this.mGameThread.getGameMode() == 1) {
            menu.add(0, (int) MENU_VIEWTOS, 0, (int) R.string.viewtos);
            menu.add(0, (int) MENU_VIEWUPDATES, 0, (int) R.string.viewupdates);
        }
        menu.add(0, 1, 0, (int) R.string.menu_exit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                finish();
                return true;
            case 3:
                this.mGameThread.setSounds(false);
                return true;
            case 5:
                this.mGameThread.setSounds(true);
                return true;
            case 19:
                this.mGameThread.setGameMode(1);
                return true;
            case 21:
                this.mGameThread.clearHighScores();
                return true;
            case MENU_RESTARTMULLIGAN /*22*/:
                this.mGameThread.doMulligan();
                return true;
            case 23:
                this.mGameThread.doNullScoreRestart();
                return true;
            case MENU_AUTOAIMON /*24*/:
                this.mGameThread.setAutoAim(false);
                return true;
            case MENU_AUTOAIMOFF /*25*/:
                this.mGameThread.setAutoAim(true);
                return true;
            case MENU_CODE /*28*/:
                int mDeviceCode = this.mGameThread.getDeviceCode();
                this.alert = new AlertDialog.Builder(this);
                this.alert.setTitle("Enter Code");
                this.alert.setMessage("If the developer has given you a code for this game, please enter it below and click OK button.\n\nYour device code is " + mDeviceCode + ".");
                this.input = new EditText(this);
                this.input.setSingleLine(true);
                this.input.setText("");
                this.alert.setView(this.input);
                this.alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int mCode;
                        try {
                            mCode = Integer.parseInt(FunPuttMiniGolf.this.input.getText().toString());
                        } catch (Exception e) {
                            mCode = 0;
                        }
                        SharedPreferences.Editor editor = FunPuttMiniGolf.this.getSharedPreferences(GameView.GameThread.PREFS_NAME, 0).edit();
                        editor.putInt("GCODE", mCode);
                        editor.commit();
                        FunPuttMiniGolf.this.mGameThread.checkGameCode(mCode);
                    }
                });
                this.alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                this.alert.show();
                return true;
            case MENU_VIEWTOS /*801*/:
                UserAgreement.show(this, true);
                return true;
            case MENU_VIEWUPDATES /*802*/:
                Updates.show(this, true);
                return true;
            default:
                return false;
        }
    }

    public void setLSMode(int which) {
        SharedPreferences.Editor editor = getSharedPreferences("FUNPUTTAPPPREFS", 0).edit();
        editor.putInt("LARGESCREENTEST", which);
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        this.mFirstRun = UserAgreement.show(this, false);
        this.mReleased = true;
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        this.mGameView = (GameView) findViewById(R.id.gameview);
        this.mGameThread = this.mGameView.getThread();
        this.mGameThread.setPriority(10);
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.mSetLS = getSharedPreferences("FUNPUTTAPPPREFS", 0).getInt("LARGESCREENTEST", 1);
        this.canvasHeight = 1.0f;
        this.canvasWidth = 1.0f;
        this.mGameThread.setSurfaceSizeNew((double) this.dm.widthPixels, (double) this.dm.heightPixels, this.dm.density, false);
        this.lsmode = false;
        this.mGameView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0 && FunPuttMiniGolf.this.mReleased) {
                    FunPuttMiniGolf.this.mReleased = false;
                    FunPuttMiniGolf.this.mGameThread.doMakeMove(event.getX(), event.getY());
                } else if (event.getAction() == 2) {
                    FunPuttMiniGolf.this.mGameThread.moveFXY(event.getX(), event.getY(), false);
                } else if (event.getAction() == 1) {
                    if (FunPuttMiniGolf.this.mGameThread.getLoadMarket()) {
                        FunPuttMiniGolf.this.mGameThread.setLoadMarket(false);
                        FunPuttMiniGolf.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.selticeapps.funfunminigolf")));
                    } else if (FunPuttMiniGolf.this.mGameThread.getLoadMarketRate()) {
                        FunPuttMiniGolf.this.mGameThread.setLoadMarketRate(false);
                        FunPuttMiniGolf.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.selticeapps.funfunminigolflite")));
                    } else if (FunPuttMiniGolf.this.mGameThread.getLoadEmail()) {
                        FunPuttMiniGolf.this.mGameThread.setLoadEmail(false);
                        FunPuttMiniGolf.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mailto:selticesystems@gmail.com?subject=Fun-Putt Mini Golf Lite Support")));
                    } else {
                        FunPuttMiniGolf.this.mReleased = true;
                        FunPuttMiniGolf.this.mGameThread.setEndXY(event.getX(), event.getY());
                    }
                }
                return true;
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            int gm = this.mGameThread.getGameMode();
            if (gm == 3) {
                this.mGameThread.backButton();
                return true;
            } else if (gm == 18) {
                this.mGameThread.setGameMode(1);
                return true;
            } else if (gm == 21) {
                this.mGameThread.setGameMode(18);
                return true;
            } else if (gm == 17) {
                this.mGameThread.setGameMode(1);
                return true;
            } else if (gm == 4) {
                this.mGameThread.setGameMode(1);
                return true;
            } else if (gm == 19) {
                this.mGameThread.setGameMode(1);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onDestroy() {
        super.onDestroy();
        this.mGameThread.myDbHelper.close();
        this.mGameThread.interrupt();
        System.exit(0);
    }

    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        this.mGameThread.myDbHelper.close();
        this.mGameThread.interrupt();
    }

    public void onStop() {
        super.onStop();
        this.mGameThread.myDbHelper.close();
        this.mGameThread.interrupt();
        System.exit(0);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mGameView.getThread().pause();
    }
}
