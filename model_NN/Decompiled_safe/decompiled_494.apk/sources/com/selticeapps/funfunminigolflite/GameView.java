package com.selticeapps.funfunminigolflite;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;
import android.widget.Toast;
import java.io.IOException;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;
import java.util.StringTokenizer;

class GameView extends SurfaceView implements SurfaceHolder.Callback {
    /* access modifiers changed from: private */
    public Context mContext;
    private GameThread thread;

    class GameThread extends Thread {
        public static final int AIM_STANDARD = 1;
        public static final int AIM_SWIPE = 0;
        private static final int BALLRAD_GROUND = 3;
        public static final int GAME_MODE_COURSES = 18;
        public static final int GAME_MODE_COURSES2 = 21;
        public static final int GAME_MODE_HIGHSCORES = 4;
        public static final int GAME_MODE_HIGHSCORES_2 = 5;
        public static final int GAME_MODE_HIGHSCORES_COURSES = 19;
        public static final int GAME_MODE_INSTRUCTIONS = 17;
        public static final int GAME_MODE_MAINMENU = 2;
        public static final int GAME_MODE_PLAY = 3;
        public static final int GAME_MODE_PRACTICE_DRIVING = 20;
        public static final int GAME_MODE_SPLASHSCREEN = 1;
        public static final int GAME_MODE_TROPHIES = 23;
        public static final int GROUND_FRINGE = -9398966;
        public static final int GROUND_GREEN = -11638988;
        public static final int GROUND_OB = -16777216;
        public static final int GROUND_ROUGH = -13153756;
        public static final int GROUND_TREES = -3407872;
        private static final int MAX_COURSE = 17;
        private static final int MAX_HIGHSCORES = 5;
        private static final int MAX_POWER_LEVEL = 395;
        public static final String PREFS_NAME = "FunPuttDeluxeMiniGolfLite";
        private static final int RANDOM_COURSE = 17;
        private static final int RANDOM_COURSE_HIGH = 16;
        private static final int RANDOM_COURSE_LOW = 4;
        public static final String SAVE_PREFS_NAME = "FunPuttDeluxeMiniGolfLiteSave";
        public static final int SOUND_1 = 1;
        public static final int SOUND_2 = 2;
        private static final int SOUND_BALLHITDRIVE = 3;
        private static final int SOUND_BALLIN = 1;
        private static final int SOUND_BALLIN2 = 11;
        private static final int SOUND_BALLINSAND = 6;
        private static final int SOUND_BALLINWATER = 5;
        private static final int SOUND_OB = 9;
        private static final int TILE_HOLE = 29;
        private static final int TILE_START = 31;
        private static final String mHazardStr = "Water Hazard Penalty +1 Stroke";
        private static final String mOffCourseStr = "The ball went off course, please try again.";
        private static final String mSNA = "N/A";
        private int COURSE_TOTAL = 0;
        private int[] DHolePosX;
        private int[] DHolePosY;
        /* access modifiers changed from: private */
        public int[][] DHoleStarCnt;
        private int[] DHoleStarX;
        private int[] DHoleStarY;
        private int DRAW_INCUP_TEXT_CT_X;
        private int DRAW_INCUP_TEXT_CT_Y;
        private int DRAW_INCUP_TEXT_ONHOLE_X;
        private int DRAW_INCUP_TEXT_ONHOLE_Y;
        private int DRAW_INCUP_TEXT_PAR_X;
        private int DRAW_INCUP_TEXT_PAR_Y;
        private int DRAW_INCUP_TEXT_PM_X;
        private int DRAW_INCUP_TEXT_PM_Y;
        private int DRAW_INCUP_TEXT_STROKES_X;
        private int DRAW_INCUP_TEXT_STROKES_Y;
        public int GROUND_FACE_NE = 12;
        public int GROUND_FACE_NW = SOUND_BALLIN2;
        public int GROUND_FACE_RANDOM = 7;
        public int GROUND_FACE_SE = 10;
        public int GROUND_FACE_SW = SOUND_OB;
        public int GROUND_FAIRWAY = 5;
        public int GROUND_FAIRWAY_EAST = 15;
        public int GROUND_FAIRWAY_NE = 18;
        public int GROUND_FAIRWAY_NORTH = 14;
        public int GROUND_FAIRWAY_NW = 20;
        public int GROUND_FAIRWAY_SE = 19;
        public int GROUND_FAIRWAY_SOUTH = RANDOM_COURSE_HIGH;
        public int GROUND_FAIRWAY_SW = 17;
        public int GROUND_FAIRWAY_WEST = 13;
        public int GROUND_HOLE = 8;
        public int GROUND_MOVING_PLATFORM_E = 39;
        public int GROUND_MOVING_PLATFORM_N = 37;
        public int GROUND_MOVING_PLATFORM_S = 38;
        public int GROUND_MOVING_PLATFORM_W = 40;
        public int GROUND_MOVING_WALL_E = 43;
        public int GROUND_MOVING_WALL_N = 41;
        public int GROUND_MOVING_WALL_S = 42;
        public int GROUND_MOVING_WALL_W = 44;
        public int GROUND_OB_EW = 0;
        public int GROUND_OB_NS = 0;
        public int GROUND_OFF_GRID = 0;
        public int GROUND_SAND = SOUND_BALLINSAND;
        public int GROUND_WATER = 4;
        private long GameTime;
        private int HIGHSCORE_DATE_X;
        private int HIGHSCORE_DATE_Y;
        private int HOLE_COUNT = 18;
        private int[] HoleTopX;
        private int[] HoleTopY;
        private Movers[] Mover;
        private int MoverCount = 0;
        private int[] OHolePosX;
        private int[] OHolePosY;
        /* access modifiers changed from: private */
        public int[][] OHoleStarCnt;
        private int[] OHoleStarX;
        private int[] OHoleStarY;
        private int START_HOLE = 1;
        private Bitmap[] Tiles;
        private Bitmap[] TilesU;
        private AlertDialog.Builder alert;
        int b;
        private int bailout;
        private int ballRad = 3;
        private double ballRealX = 0.0d;
        private double ballRealY = 0.0d;
        private int ballX = 0;
        private int ballY = 0;
        private Paint ballpaint;
        private Paint bgpaint;
        private Paint blackpaint;
        private Paint blacktextpaint;
        private Paint blacktextpaint2;
        private Paint blacktextpaint2b;
        private Paint blacktextpaint2b2;
        private Paint blacktextpaint3;
        private Bitmap bmpCanvas;
        private Bitmap bmpColors;
        private Bitmap[] bmpFlagTrophy;
        private Bitmap bmpHole;
        private Bitmap bmpHoleTop;
        private Bitmap bmpHoleU;
        private Bitmap bmpIncup;
        private Bitmap[] bmpParTrophy;
        private Bitmap bmpPause;
        private Bitmap[] bmpStars;
        private Bitmap bmpSwing1;
        private int btnHoleHeight;
        private int btnHoleWidth;
        private Canvas ca;
        private int canvasHeight;
        private int canvasHeightFull;
        private int canvasWidth;
        private int[] courseBestScores;
        /* access modifiers changed from: private */
        public int[] courseFlagTrophy;
        private String[] courseHash;
        private int[] courseIDs;
        /* access modifiers changed from: private */
        public int[] courseParTrophy;
        /* access modifiers changed from: private */
        public int[] courseStars;
        private Bitmap cs;
        int d;
        float d2;
        private Paint darkgreenpaint;
        Rect dstRect;
        private double dx;
        private double dxtmp;
        private double dy;
        private Paint fairwaypaint;
        /* access modifiers changed from: private */
        public int[] gameIDsTmp;
        private CharSequence[] gameModes;
        private String[] gameModesS;
        /* access modifiers changed from: private */
        public int[] gameModesVis;
        private int gcv1;
        private int gcvd;
        private int gcvx;
        Random generator;
        private Paint goldpaint;
        private String highscores;
        private String[] highscores_tmp;
        private String[] highscores_tmp2;
        private int holeX;
        private int holeY;
        private String[] hs_normaltimes;
        private String[] hs_normaltimes2;
        private String[] hs_players;
        private String[] hs_shifterstimes;
        /* access modifiers changed from: private */
        public EditText input;
        private float lastAimX;
        private float lastAimY;
        private Paint lightgreenpaint;
        private int loadBallX;
        private int loadBallY;
        private boolean loadInCup = false;
        private double mAPower = 0.0d;
        private int mAimMethod = 0;
        private boolean mAimSet;
        private boolean mAiming = false;
        private boolean mAutoAim;
        private Bitmap mBackgroundImage;
        private boolean mBallMoving = false;
        private int mBallOriginalX;
        private int mBallOriginalY;
        private boolean mBelowTrees;
        private boolean mBelowWater;
        private String mBestStr;
        /* access modifiers changed from: private */
        public int mCourseBest;
        private int mCourseTotalTmp;
        private int mDelay;
        private int mDeviceCode;
        private int mGameMode;
        private Handler mHandler;
        private int[][] mHoleA;
        private String[] mHoleBottom;
        private int[] mHoleStartX;
        private int[] mHoleStartY;
        private String[] mHoleTop;
        private int[] mHoleXs;
        private int[] mHoleYs;
        private boolean mInCup;
        /* access modifiers changed from: private */
        public boolean mIsLite;
        private boolean mIsPaused = false;
        private boolean mIsSwinging = false;
        private boolean mLoadEmail = false;
        private boolean mLoadInstructions;
        private boolean mLoadMarket;
        private boolean mLoadMarketRate;
        /* access modifiers changed from: private */
        public boolean mLoadedGame = false;
        private float mMultiplierHeight;
        private float mMultiplierWidth;
        private int mMyCoursePlusMinus;
        /* access modifiers changed from: private */
        public int mMyCourseTotal;
        /* access modifiers changed from: private */
        public int mOnCourse;
        private int mOnHole;
        private int[] mPars;
        private int mPixelColor;
        private boolean mPlaySounds = true;
        private boolean mPlayedSandSound;
        /* access modifiers changed from: private */
        public String mPlayerName;
        private int mPowerLevel = 75;
        private boolean mPre;
        /* access modifiers changed from: private */
        public boolean mRandomMode = false;
        private boolean[] mRandomPlayed;
        /* access modifiers changed from: private */
        public boolean mResetCourseStrokes = false;
        private double mResistanceX = 0.3d;
        private double mResistanceY = 0.3d;
        /* access modifiers changed from: private */
        public int mRestartCount;
        /* access modifiers changed from: private */
        public boolean mRestartedHole;
        private boolean mRun = false;
        private String mSOnHole;
        private String mSPar;
        private String mSStrokes;
        private boolean mSetRestartedHole = false;
        private boolean mSettingPower = false;
        private boolean mShowCup;
        /* access modifiers changed from: private */
        public boolean mShowingMessage = false;
        /* access modifiers changed from: private */
        public boolean mShowingPre;
        private boolean mStarted;
        private String mStrokeStr;
        /* access modifiers changed from: private */
        public int mStrokes;
        /* access modifiers changed from: private */
        public int mStrokesPlus;
        private SurfaceHolder mSurfaceHolder;
        private boolean mSwinging = false;
        private double mVelX;
        private double mVelY;
        private boolean mWentBack;
        private Matrix matrix;
        DataBaseHelper myDbHelper;
        private double ndx;
        private double ndy;
        private int oX;
        private int oY;
        BitmapFactory.Options options;
        private Path path;
        private double pballX;
        private double pballY;
        private double pdx;
        private double pdy;
        /* access modifiers changed from: private */
        public int playSingleHole;
        private Paint redpaint;
        private boolean safeToRun = true;
        private SoundPool soundPool;
        private HashMap<Integer, Integer> soundPoolMap;
        private int starCount;
        private int startX;
        private int startY;
        double theta;
        private int tmpX;
        private int tmpY;
        private double tmptheta;
        private Paint whitepaint;
        private Paint whitepaintc;
        private Paint whitepaintd;
        int x1;
        int x2;
        int x3;
        int y1;
        int y2;
        int y3;

        public GameThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            int[] iArr = new int[18];
            iArr[1] = -1;
            iArr[2] = -1;
            iArr[3] = -1;
            iArr[4] = R.xml.course4;
            iArr[5] = R.xml.course5;
            iArr[SOUND_BALLINSAND] = R.xml.course6;
            iArr[7] = R.xml.course7;
            iArr[8] = R.xml.course8;
            iArr[SOUND_OB] = R.xml.course9;
            iArr[10] = R.xml.course10;
            iArr[SOUND_BALLIN2] = R.xml.course11;
            iArr[12] = R.xml.course12;
            iArr[13] = R.xml.course13;
            iArr[14] = R.xml.course14;
            iArr[15] = R.xml.course15;
            iArr[RANDOM_COURSE_HIGH] = R.xml.course16;
            iArr[17] = -1;
            this.courseIDs = iArr;
            String[] strArr = new String[18];
            strArr[0] = "";
            strArr[1] = "";
            strArr[2] = "";
            strArr[3] = "";
            strArr[4] = "";
            strArr[5] = "";
            strArr[SOUND_BALLINSAND] = "";
            strArr[7] = "";
            strArr[8] = "";
            strArr[SOUND_OB] = "";
            strArr[10] = "c197c48102af29284d16da743461e325";
            strArr[SOUND_BALLIN2] = "76bffad30b95e5fcd55d11519b497d90";
            strArr[12] = "";
            strArr[13] = "";
            strArr[14] = "";
            strArr[15] = "";
            strArr[RANDOM_COURSE_HIGH] = "";
            strArr[17] = "";
            this.courseHash = strArr;
            this.d2 = 0.0f;
            this.b = 0;
            this.dstRect = new Rect();
            this.canvasHeightFull = 320;
            this.canvasWidth = 480;
            this.canvasHeight = 320;
            this.mCourseBest = 50000;
            this.mShowingPre = false;
            this.mRestartedHole = false;
            this.mLoadMarketRate = false;
            this.btnHoleWidth = 73;
            this.btnHoleHeight = 38;
            this.starCount = 0;
            this.mWentBack = false;
            this.playSingleHole = -1;
            this.mAutoAim = true;
            this.mAimSet = false;
            this.mRestartCount = 0;
            this.mStrokesPlus = 0;
            this.gcv1 = 67;
            this.gcvx = 983;
            this.gcvd = 3000;
            this.mIsLite = false;
            this.mDeviceCode = 0;
            this.mSurfaceHolder = surfaceHolder;
            this.mHandler = handler;
            GameView.this.mContext = context;
            this.mPlayedSandSound = false;
            this.mPre = false;
            this.mPlaySounds = true;
            this.mStarted = false;
            this.mLoadMarket = false;
            this.mIsPaused = false;
            this.mIsSwinging = false;
            this.mAPower = 0.0d;
            this.mBallMoving = false;
            this.theta = 0.0d;
            this.ballRealX = 0.0d;
            this.ballRealY = 0.0d;
            this.mDelay = 10;
            this.mBelowTrees = false;
            this.mBelowWater = false;
            this.mLoadInstructions = false;
            this.mStrokes = 0;
            this.mSStrokes = new String();
            this.mSOnHole = new String();
            this.mBestStr = new String();
            this.mPixelColor = this.GROUND_FAIRWAY;
            this.mInCup = false;
            this.mMyCourseTotal = 0;
            this.mMyCoursePlusMinus = 0;
            this.mShowCup = false;
            this.mStrokeStr = new String();
            this.mStrokeStr = "";
            this.courseStars = new int[17];
            this.courseFlagTrophy = new int[17];
            this.courseParTrophy = new int[17];
            this.OHolePosX = new int[18];
            this.OHolePosY = new int[18];
            this.OHoleStarX = new int[18];
            this.OHoleStarY = new int[18];
            this.OHoleStarCnt = (int[][]) Array.newInstance(Integer.TYPE, 20, 18);
            this.DHolePosX = new int[18];
            this.DHolePosY = new int[18];
            this.DHoleStarX = new int[18];
            this.DHoleStarY = new int[18];
            this.DHoleStarCnt = (int[][]) Array.newInstance(Integer.TYPE, 20, 18);
            this.OHolePosX[0] = 3;
            this.OHolePosY[0] = 123;
            this.OHolePosX[1] = 83;
            this.OHolePosY[1] = 123;
            this.OHolePosX[2] = 163;
            this.OHolePosY[2] = 123;
            this.OHolePosX[3] = 243;
            this.OHolePosY[3] = 123;
            this.OHolePosX[4] = 323;
            this.OHolePosY[4] = 123;
            this.OHolePosX[5] = 403;
            this.OHolePosY[5] = 123;
            this.OHolePosX[SOUND_BALLINSAND] = 3;
            this.OHolePosY[SOUND_BALLINSAND] = 163;
            this.OHolePosX[7] = 83;
            this.OHolePosY[7] = 163;
            this.OHolePosX[8] = 163;
            this.OHolePosY[8] = 163;
            this.OHolePosX[SOUND_OB] = 243;
            this.OHolePosY[SOUND_OB] = 163;
            this.OHolePosX[10] = 323;
            this.OHolePosY[10] = 163;
            this.OHolePosX[SOUND_BALLIN2] = 403;
            this.OHolePosY[SOUND_BALLIN2] = 163;
            this.OHolePosX[12] = 3;
            this.OHolePosY[12] = 203;
            this.OHolePosX[13] = 83;
            this.OHolePosY[13] = 203;
            this.OHolePosX[14] = 163;
            this.OHolePosY[14] = 203;
            this.OHolePosX[15] = 243;
            this.OHolePosY[15] = 203;
            this.OHolePosX[RANDOM_COURSE_HIGH] = 323;
            this.OHolePosY[RANDOM_COURSE_HIGH] = 203;
            this.OHolePosX[17] = 403;
            this.OHolePosY[17] = 203;
            for (int i = 0; i < 18; i++) {
                this.OHoleStarX[i] = this.OHolePosX[i] + 27;
                this.OHoleStarY[i] = this.OHolePosY[i] + SOUND_BALLIN2;
            }
            this.DHolePosX[0] = 38;
            this.DHolePosY[0] = 142;
            this.DHolePosX[1] = 121;
            this.DHolePosY[1] = 142;
            this.DHolePosX[2] = 205;
            this.DHolePosY[2] = 142;
            this.DHolePosX[3] = 289;
            this.DHolePosY[3] = 142;
            this.DHolePosX[4] = 373;
            this.DHolePosY[4] = 142;
            this.DHolePosX[5] = 80;
            this.DHolePosY[5] = 187;
            this.DHolePosX[SOUND_BALLINSAND] = 164;
            this.DHolePosY[SOUND_BALLINSAND] = 187;
            this.DHolePosX[7] = 248;
            this.DHolePosY[7] = 187;
            this.DHolePosX[8] = 332;
            this.DHolePosY[8] = 187;
            for (int i2 = 0; i2 < SOUND_OB; i2++) {
                this.DHoleStarX[i2] = this.DHolePosX[i2] + 25;
                this.DHoleStarY[i2] = this.DHolePosY[i2] + SOUND_BALLIN2;
            }
            this.mPars = new int[19];
            this.mHoleStartX = new int[19];
            this.mHoleStartY = new int[19];
            this.mHoleXs = new int[19];
            this.mHoleYs = new int[19];
            this.HoleTopX = new int[19];
            this.HoleTopY = new int[19];
            this.mHoleBottom = new String[10];
            this.mHoleTop = new String[10];
            this.mHoleA = new int[400][];
            for (int i3 = 0; i3 <= 360; i3++) {
                this.mHoleA[i3] = new int[800];
            }
            this.HIGHSCORE_DATE_X = 151;
            this.HIGHSCORE_DATE_Y = 107;
            this.path = new Path();
            this.matrix = new Matrix();
            this.gameModes = new CharSequence[18];
            this.gameIDsTmp = new int[RANDOM_COURSE_HIGH];
            this.gameModesS = new String[18];
            this.gameModesS[0] = "";
            this.gameModesS[1] = "Course 1 - Original (9 Holes) ";
            this.gameModesS[2] = "Course 2 - Original (BUY FULL)";
            this.gameModesS[3] = "Course 3 - Original (BUY FULL)";
            this.gameModesS[4] = "Course 1 Updated (Holes 1-9) (BUY FULL)";
            this.gameModesS[5] = "Course 1 Updated (Holes 10-18) (BUY FULL)";
            this.gameModesS[SOUND_BALLINSAND] = "Course 2 Updated (Holes 1-9) (BUY FULL)";
            this.gameModesS[7] = "Course 2 Updated (Holes 10-18) (BUY FULL)";
            this.gameModesS[8] = "Course 3 Updated (Holes 1-9) (BUY FULL)";
            this.gameModesS[SOUND_OB] = "Course 3 Updated (Holes 10-18) (BUY FULL)";
            this.gameModesS[10] = "Deluxe Mode Training Course";
            this.gameModesS[SOUND_BALLIN2] = "Deluxe Course 1 (9 Holes)";
            this.gameModesS[12] = "Deluxe Course 2 (9 Holes) (BUY FULL)";
            this.gameModesS[13] = "Deluxe Course 3 (9 Holes) (BUY FULL)";
            this.gameModesS[14] = "Deluxe Course 4 (9 Holes) (BUY FULL)";
            this.gameModesS[15] = "Deluxe Course 5 (9 Holes) (BUY FULL)";
            this.gameModesS[RANDOM_COURSE_HIGH] = "Deluxe Course 6 (9 Holes) (BUY FULL)";
            this.gameModesS[17] = "Quick Play Random Deluxe Holes (BUY FULL)";
            for (int i4 = 0; i4 <= 17; i4++) {
                this.gameModes[i4] = this.gameModesS[i4];
            }
            this.gameModesVis = new int[18];
            this.gameModesVis[0] = 0;
            this.gameModesVis[1] = 1;
            this.gameModesVis[2] = 10;
            this.gameModesVis[3] = SOUND_BALLIN2;
            this.gameModesVis[4] = 2;
            this.gameModesVis[5] = 3;
            this.gameModesVis[SOUND_BALLINSAND] = 4;
            this.gameModesVis[7] = 5;
            this.gameModesVis[8] = SOUND_BALLINSAND;
            this.gameModesVis[SOUND_OB] = 7;
            this.gameModesVis[10] = 8;
            this.gameModesVis[SOUND_BALLIN2] = SOUND_OB;
            this.gameModesVis[12] = 12;
            this.gameModesVis[13] = 13;
            this.gameModesVis[14] = 14;
            this.gameModesVis[15] = 15;
            this.gameModesVis[RANDOM_COURSE_HIGH] = RANDOM_COURSE_HIGH;
            this.gameModesVis[17] = 17;
            this.mRandomPlayed = new boolean[250];
            resetRandomPlayed();
            this.generator = new Random();
            SharedPreferences settings = GameView.this.mContext.getSharedPreferences(PREFS_NAME, 0);
            String playername = settings.getString("PlayerName", "Player");
            this.mDeviceCode = settings.getInt("DEVICECODE", 0);
            if (this.mDeviceCode < 10000) {
                SharedPreferences.Editor meditor = settings.edit();
                this.mDeviceCode = this.generator.nextInt(80000) + 10000;
                meditor.putInt("DEVICECODE", this.mDeviceCode);
                meditor.commit();
            }
            this.mIsLite = false;
            this.mOnHole = 1;
            if (!this.mIsLite) {
                this.bmpIncup = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.incup);
            } else {
                this.bmpIncup = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.incuplite);
            }
            this.bmpSwing1 = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.swing1);
            this.bmpPause = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.pause);
            this.bmpStars = new Bitmap[4];
            this.bmpStars[0] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.star0);
            this.bmpStars[1] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.star1);
            this.bmpStars[2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.star2);
            this.bmpStars[3] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.star3);
            this.bmpFlagTrophy = new Bitmap[4];
            this.bmpParTrophy = new Bitmap[4];
            this.bmpFlagTrophy[0] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_flag_fade);
            this.bmpFlagTrophy[1] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_flag);
            this.bmpFlagTrophy[2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_flag_fade_small);
            this.bmpFlagTrophy[3] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_flag_small);
            this.bmpParTrophy[0] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_par_fade);
            this.bmpParTrophy[1] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_par);
            this.bmpParTrophy[2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_par_fade_small);
            this.bmpParTrophy[3] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.trophy_par_small);
            this.TilesU = new Bitmap[90];
            this.Tiles = new Bitmap[90];
            this.options = new BitmapFactory.Options();
            this.options.inDither = false;
            this.options.inScaled = false;
            this.bmpColors = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.colors, this.options);
            this.Tiles[0] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_0);
            this.Tiles[1] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_2);
            this.Tiles[3] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_3);
            this.Tiles[4] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_4);
            this.Tiles[5] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_5);
            this.Tiles[SOUND_BALLINSAND] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_6);
            this.Tiles[7] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_7);
            this.Tiles[8] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_8);
            this.Tiles[SOUND_OB] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_9);
            this.Tiles[10] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_10);
            this.Tiles[SOUND_BALLIN2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_11);
            this.Tiles[12] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_12);
            this.Tiles[13] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_13);
            this.Tiles[14] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_14);
            this.Tiles[15] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_15);
            this.Tiles[RANDOM_COURSE_HIGH] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_16);
            this.Tiles[17] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_17);
            this.Tiles[18] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_18);
            this.Tiles[19] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_19);
            this.Tiles[20] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_20);
            this.Tiles[21] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_21);
            this.Tiles[22] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_22);
            this.Tiles[23] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_23);
            this.Tiles[24] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_24);
            this.Tiles[25] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_25);
            this.Tiles[26] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_26);
            this.Tiles[27] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_27);
            this.Tiles[28] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_28);
            this.Tiles[TILE_HOLE] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_29);
            this.Tiles[30] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_30);
            this.Tiles[TILE_START] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_31);
            this.Tiles[32] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_32);
            this.Tiles[33] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_33);
            this.Tiles[34] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_34);
            this.Tiles[35] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_35);
            this.Tiles[36] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_36);
            this.Tiles[37] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_37);
            this.Tiles[38] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_38);
            this.Tiles[39] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_39);
            this.Tiles[40] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_40);
            this.Tiles[41] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_41);
            this.Tiles[42] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_42);
            this.Tiles[43] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_43);
            this.Tiles[44] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_44);
            this.Tiles[45] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_45);
            this.Tiles[46] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[47] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[48] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[49] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[50] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[51] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[52] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[53] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[54] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[55] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[56] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[57] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[58] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[59] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[60] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[61] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[62] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[63] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[64] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[65] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[66] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[67] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[68] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[69] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[70] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[71] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[72] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[73] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[74] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[75] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[76] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[77] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[78] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[79] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[80] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[81] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[82] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[83] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[84] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[85] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[86] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[87] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_1);
            this.Tiles[88] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_88);
            this.Tiles[89] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.tile_89);
            this.TilesU[0] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_0);
            this.TilesU[1] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_2);
            this.TilesU[3] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_3);
            this.TilesU[4] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_4);
            this.TilesU[5] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_5);
            this.TilesU[SOUND_BALLINSAND] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_6);
            this.TilesU[7] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_7);
            this.TilesU[8] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_8);
            this.TilesU[SOUND_OB] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_9);
            this.TilesU[10] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_10);
            this.TilesU[SOUND_BALLIN2] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_11);
            this.TilesU[12] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_12);
            this.TilesU[13] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_13);
            this.TilesU[14] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_14);
            this.TilesU[15] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_15);
            this.TilesU[RANDOM_COURSE_HIGH] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_16);
            this.TilesU[17] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_17);
            this.TilesU[18] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_18);
            this.TilesU[19] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_19);
            this.TilesU[20] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_20);
            this.TilesU[21] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_21);
            this.TilesU[22] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_22);
            this.TilesU[23] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_23);
            this.TilesU[24] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_24);
            this.TilesU[25] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_25);
            this.TilesU[26] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_26);
            this.TilesU[27] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_27);
            this.TilesU[28] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_28);
            this.TilesU[TILE_HOLE] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_29);
            this.TilesU[30] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_30);
            this.TilesU[TILE_START] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_31);
            this.TilesU[32] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_32);
            this.TilesU[33] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_33);
            this.TilesU[34] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_34);
            this.TilesU[35] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_35);
            this.TilesU[36] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_36);
            this.TilesU[37] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_37);
            this.TilesU[38] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_38);
            this.TilesU[39] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_39);
            this.TilesU[40] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_40);
            this.TilesU[41] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_87);
            this.TilesU[42] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_87);
            this.TilesU[43] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_87);
            this.TilesU[44] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_87);
            this.TilesU[45] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_45);
            this.TilesU[46] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_46);
            this.TilesU[47] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_47);
            this.TilesU[48] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_48);
            this.TilesU[49] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_49);
            this.TilesU[50] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_50);
            this.TilesU[51] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_51);
            this.TilesU[52] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_52);
            this.TilesU[53] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_53);
            this.TilesU[54] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_54);
            this.TilesU[54] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_54);
            this.TilesU[55] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[56] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[57] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[58] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[59] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[60] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[61] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[62] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[63] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[64] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[65] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[66] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[67] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[68] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[69] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[70] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[71] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[72] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[73] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[74] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[75] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[76] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[77] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[78] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[79] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[80] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[81] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[82] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[83] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[84] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[85] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[86] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_1);
            this.TilesU[87] = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.utile_87);
            this.DRAW_INCUP_TEXT_ONHOLE_X = 245;
            this.DRAW_INCUP_TEXT_ONHOLE_Y = 59;
            this.DRAW_INCUP_TEXT_PAR_X = 245;
            this.DRAW_INCUP_TEXT_PAR_Y = 82;
            this.DRAW_INCUP_TEXT_STROKES_X = 245;
            this.DRAW_INCUP_TEXT_STROKES_Y = 106;
            this.DRAW_INCUP_TEXT_PM_X = 245;
            this.DRAW_INCUP_TEXT_PM_Y = 139;
            this.DRAW_INCUP_TEXT_CT_X = 245;
            this.DRAW_INCUP_TEXT_CT_Y = 162;
            this.mAutoAim = settings.getBoolean("AUTOAIM", true);
            this.mAimMethod = settings.getInt("AIM_TYPE", 1);
            this.mPlayerName = new String();
            this.mPlayerName = playername;
            this.mSPar = new String();
            this.soundPool = new SoundPool(4, 2, 100);
            this.soundPoolMap = new HashMap<>();
            this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.putt1, 1)));
            this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.ballin, 1)));
            this.soundPoolMap.put(Integer.valueOf((int) SOUND_BALLIN2), Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.ballin2, 1)));
            this.soundPoolMap.put(5, Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.inwater, 1)));
            this.soundPoolMap.put(Integer.valueOf((int) SOUND_BALLINSAND), Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.insand, 1)));
            this.soundPoolMap.put(Integer.valueOf((int) SOUND_OB), Integer.valueOf(this.soundPool.load(GameView.this.getContext(), R.raw.ob, 1)));
            this.highscores = new String();
            this.myDbHelper = new DataBaseHelper(GameView.this.mContext);
            this.hs_normaltimes = new String[15];
            this.hs_normaltimes2 = new String[15];
            this.hs_shifterstimes = new String[15];
            this.hs_players = new String[15];
            try {
                this.myDbHelper.createDataBase();
                try {
                    this.myDbHelper.openDataBase();
                    this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.welcome);
                    this.whitepaint = new Paint();
                    this.whitepaint.setStyle(Paint.Style.FILL);
                    this.whitepaint.setTextSize(16.0f);
                    this.whitepaint.setColor(-1);
                    this.whitepaint.setAntiAlias(true);
                    this.whitepaint.setARGB(255, 255, 255, 255);
                    this.whitepaintc = new Paint();
                    this.whitepaintc.setStyle(Paint.Style.FILL);
                    this.whitepaintc.setTextSize(14.0f);
                    this.whitepaintc.setColor(-1);
                    this.whitepaintc.setAntiAlias(true);
                    this.whitepaintc.setARGB(255, 255, 255, 255);
                    this.whitepaintc.setTextAlign(Paint.Align.CENTER);
                    this.whitepaintd = new Paint();
                    this.whitepaintd.setStyle(Paint.Style.FILL);
                    this.whitepaintd.setTextSize(13.0f);
                    this.whitepaintd.setColor(-1);
                    this.whitepaintd.setAntiAlias(true);
                    this.whitepaintd.setARGB(255, 255, 255, 255);
                    this.whitepaintd.setTextAlign(Paint.Align.CENTER);
                    this.ballpaint = new Paint();
                    this.ballpaint.setStyle(Paint.Style.FILL);
                    this.ballpaint.setTextSize(16.0f);
                    this.ballpaint.setColor(-1);
                    this.ballpaint.setAntiAlias(false);
                    this.ballpaint.setARGB(255, 255, 255, 255);
                    this.blackpaint = new Paint();
                    this.blackpaint.setStyle(Paint.Style.FILL);
                    this.blackpaint.setTextSize(16.0f);
                    this.blackpaint.setAntiAlias(true);
                    this.blackpaint.setColor((int) GROUND_OB);
                    this.blackpaint.setARGB(255, 0, 0, 0);
                    this.blacktextpaint = new Paint();
                    this.blacktextpaint.setStyle(Paint.Style.FILL);
                    this.blacktextpaint.setTextSize(11.0f);
                    this.blacktextpaint.setAntiAlias(true);
                    this.blacktextpaint.setColor((int) GROUND_OB);
                    this.blacktextpaint.setARGB(255, 0, 0, 0);
                    this.blacktextpaint.setTextAlign(Paint.Align.CENTER);
                    this.blacktextpaint2 = new Paint();
                    this.blacktextpaint2.setStyle(Paint.Style.FILL);
                    this.blacktextpaint2.setTextSize(18.0f);
                    this.blacktextpaint2.setAntiAlias(true);
                    this.blacktextpaint2.setARGB(255, 255, 255, 255);
                    this.blacktextpaint2.setTextAlign(Paint.Align.CENTER);
                    this.blacktextpaint2b2 = new Paint();
                    this.blacktextpaint2b2.setStyle(Paint.Style.FILL);
                    this.blacktextpaint2b2.setTextSize(14.0f);
                    this.blacktextpaint2b2.setAntiAlias(true);
                    this.blacktextpaint2b2.setARGB(255, 255, 255, 255);
                    this.blacktextpaint2b2.setTextAlign(Paint.Align.LEFT);
                    this.blacktextpaint2b = new Paint();
                    this.blacktextpaint2b.setStyle(Paint.Style.FILL);
                    this.blacktextpaint2b.setTextSize(11.0f);
                    this.blacktextpaint2b.setAntiAlias(false);
                    this.blacktextpaint2b.setUnderlineText(true);
                    this.blacktextpaint2b.setColor(-16776961);
                    this.blacktextpaint2b.setARGB(255, 0, 0, 220);
                    this.blacktextpaint2b.setTextAlign(Paint.Align.LEFT);
                    this.blacktextpaint3 = new Paint();
                    this.blacktextpaint3.setStyle(Paint.Style.FILL);
                    this.blacktextpaint3.setTextSize(15.0f);
                    this.blacktextpaint3.setAntiAlias(true);
                    this.blacktextpaint3.setColor((int) GROUND_OB);
                    this.blacktextpaint3.setARGB(255, 0, 0, 0);
                    this.blacktextpaint3.setTextAlign(Paint.Align.LEFT);
                    this.redpaint = new Paint();
                    this.redpaint.setStyle(Paint.Style.FILL);
                    this.redpaint.setTextSize(18.0f);
                    this.redpaint.setAntiAlias(true);
                    this.redpaint.setARGB(255, 100, 0, 0);
                    this.goldpaint = new Paint();
                    this.goldpaint.setStyle(Paint.Style.FILL);
                    this.goldpaint.setTextSize(16.0f);
                    this.goldpaint.setAntiAlias(true);
                    this.goldpaint.setARGB(100, 255, 255, 255);
                    this.lightgreenpaint = new Paint();
                    this.lightgreenpaint.setStyle(Paint.Style.FILL);
                    this.lightgreenpaint.setAntiAlias(true);
                    this.lightgreenpaint.setARGB(255, 112, 149, 74);
                    this.darkgreenpaint = new Paint();
                    this.darkgreenpaint.setStyle(Paint.Style.FILL);
                    this.darkgreenpaint.setARGB(255, 70, 70, 70);
                    this.fairwaypaint = new Paint();
                    this.fairwaypaint.setStyle(Paint.Style.FILL);
                    this.fairwaypaint.setAntiAlias(true);
                    this.fairwaypaint.setARGB(255, 99, 128, 70);
                    this.bgpaint = new Paint();
                    this.bgpaint.setStyle(Paint.Style.FILL);
                    this.bgpaint.setARGB(255, 48, 118, 46);
                    this.bgpaint.setARGB(255, 0, 0, 0);
                    setGameMode(1);
                    doStart();
                } catch (SQLException e) {
                    throw e;
                }
            } catch (IOException e2) {
                throw new Error("Unable to create database");
            }
        }

        public void checkGameCode(int gcode) {
            synchronized (this.mSurfaceHolder) {
                if (getUnlockLiteCode() == gcode) {
                    this.mIsLite = false;
                }
            }
        }

        public int getUnlockLiteCode() {
            int gcode;
            synchronized (this.mSurfaceHolder) {
                gcode = calccode(((this.mDeviceCode - (this.gcv1 * 73)) * 2) + this.gcvx);
            }
            return gcode;
        }

        public int calccode(int gcode) {
            int gcode2;
            synchronized (this.mSurfaceHolder) {
                gcode2 = (gcode * 3) - this.gcvd;
            }
            return gcode2;
        }

        public int getDeviceCode() {
            int i;
            synchronized (this.mSurfaceHolder) {
                i = GameView.this.mContext.getSharedPreferences(PREFS_NAME, 0).getInt("DEVICECODE", 0);
            }
            return i;
        }

        public void checkIsLite() {
            synchronized (this.mSurfaceHolder) {
                SharedPreferences settings = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0);
                int i = settings.getInt("ISLITE", 0);
                int tmpLite = 2;
                for (int i2 = 0; i2 < 17; i2++) {
                    int j = 1;
                    while (true) {
                        if (j > SOUND_OB) {
                            break;
                        } else if (settings.getInt("HOLESTARCNT_" + i2 + "_" + j, 0) > 0) {
                            tmpLite = 1;
                            break;
                        } else {
                            j++;
                        }
                    }
                    if (tmpLite == 1) {
                        break;
                    }
                }
                if (tmpLite == 1) {
                    this.mIsLite = false;
                } else if (tmpLite == 2) {
                    this.mIsLite = true;
                }
            }
        }

        public void resetRandomPlayed() {
            synchronized (this.mSurfaceHolder) {
                for (int i = 0; i < 25; i++) {
                    this.mRandomPlayed[i] = false;
                }
            }
        }

        public void setLoadInstructions(boolean which) {
            this.mLoadInstructions = which;
        }

        public boolean getLoadInstructions() {
            return this.mLoadInstructions;
        }

        public String md5(String s) {
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");
                digest.update(s.getBytes());
                byte[] messageDigest = digest.digest();
                StringBuffer hexString = new StringBuffer();
                for (byte b2 : messageDigest) {
                    String h = Integer.toHexString(b2 & 255);
                    while (h.length() < 2) {
                        h = "0" + h;
                    }
                    hexString.append(h);
                }
                return hexString.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return "";
            }
        }

        public void setHoleC123() {
            synchronized (this.mSurfaceHolder) {
                try {
                    this.bmpHole.recycle();
                } catch (Exception e) {
                }
                try {
                    this.bmpHoleTop.recycle();
                } catch (Exception e2) {
                }
                try {
                    this.bmpHoleU.recycle();
                } catch (Exception e3) {
                }
                if (this.mOnCourse == 1) {
                    if (this.mOnHole == 1) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1u, this.options);
                    } else if (this.mOnHole == 2) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole2);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole2u, this.options);
                    } else if (this.mOnHole == 3) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole3);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole3u, this.options);
                    } else if (this.mOnHole == 4) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole4);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole4u, this.options);
                    } else if (this.mOnHole == 5) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole5);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole5u, this.options);
                    } else if (this.mOnHole == SOUND_BALLINSAND) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole6);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole6u, this.options);
                    } else if (this.mOnHole == 7) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole7);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole7u, this.options);
                    } else if (this.mOnHole == 8) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole8);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole8top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole8u, this.options);
                    } else if (this.mOnHole == SOUND_OB) {
                        this.bmpHole = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole9);
                        this.bmpHoleTop = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole1top);
                        this.bmpHoleU = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.hole9u, this.options);
                    }
                }
            }
        }

        public void setHole() {
            synchronized (this.mSurfaceHolder) {
                try {
                    this.bmpHole.recycle();
                } catch (Exception e) {
                }
                try {
                    this.bmpHoleTop.recycle();
                } catch (Exception e2) {
                }
                try {
                    this.bmpHoleU.recycle();
                } catch (Exception e3) {
                }
            }
        }

        public int getOnCourse() {
            int i;
            synchronized (this.mSurfaceHolder) {
                i = this.mOnCourse;
            }
            return i;
        }

        public void checkProfile() {
            synchronized (this.mSurfaceHolder) {
                if (this.mRestartedHole) {
                    clearProgress();
                    loadHighscores();
                    setGameMode(4);
                } else {
                    SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0).edit();
                    int mLowestScore = this.myDbHelper.lowestScoreOfAll(this.mOnCourse);
                    int mLowScore = this.myDbHelper.lowestScore(this.mOnCourse);
                    if (this.mMyCourseTotal < mLowestScore) {
                        editor.putInt("COURSEBEST_" + this.mOnCourse, this.mMyCourseTotal);
                        editor.commit();
                    } else if (mLowestScore != 50000) {
                        editor.putInt("COURSEBEST_" + this.mOnCourse, mLowestScore);
                        editor.commit();
                    }
                    if (this.mRestartCount == 0 && this.mMyCourseTotal < this.COURSE_TOTAL) {
                        editor.putInt("COURSEPERFECT_" + this.mOnCourse, 1);
                        editor.commit();
                    }
                    if (this.mMyCourseTotal <= mLowScore) {
                        this.alert = new AlertDialog.Builder(GameView.this.mContext);
                        this.alert.setTitle("Player's Name");
                        this.alert.setMessage("Your name is saved with your score. Only use letters and numbers.");
                        this.input = new EditText(GameView.this.mContext);
                        this.input.setSingleLine(true);
                        this.input.setText(this.mPlayerName);
                        this.alert.setView(this.input);
                        this.alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Editable value = GameThread.this.input.getText();
                                SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(GameThread.PREFS_NAME, 0).edit();
                                editor.putString("PlayerName", value.toString());
                                editor.commit();
                                GameThread.this.mPlayerName = value.toString();
                                GameThread.this.myDbHelper.updateScore(GameThread.this.mPlayerName, GameThread.this.mMyCourseTotal, GameThread.this.mOnCourse);
                                GameThread.this.clearProgress();
                                GameThread.this.loadHighscores();
                                GameThread.this.setGameMode(5);
                            }
                        });
                        this.alert.show();
                    } else {
                        clearProgress();
                        loadHighscores();
                        setGameMode(4);
                    }
                }
            }
        }

        public void doStart() {
            synchronized (this.mSurfaceHolder) {
                if (this.mGameMode == 1) {
                    this.mDelay = 10;
                } else if (this.mGameMode == 3) {
                    startGame();
                }
                setRunning(true);
            }
        }

        public void startGame() {
            synchronized (this.mSurfaceHolder) {
                this.COURSE_TOTAL = 0;
                for (int i = 1; i <= SOUND_OB; i++) {
                    this.COURSE_TOTAL += this.mPars[i];
                }
                if (this.playSingleHole != -1) {
                    this.mOnHole = this.playSingleHole;
                }
                this.mAPower = 0.0d;
                resetAutoAim();
                this.mBallMoving = false;
                this.mIsSwinging = false;
                this.mBelowTrees = false;
                this.mBelowWater = false;
                if (!this.mLoadedGame) {
                    this.mStrokes = 0;
                    this.mSStrokes = "0";
                    if (!this.mSetRestartedHole) {
                        this.mRestartedHole = false;
                    }
                } else {
                    makeStrokeString();
                }
                this.mStrokesPlus = this.mStrokes + 1;
                this.mSetRestartedHole = false;
                this.mPixelColor = this.GROUND_FAIRWAY;
                this.ballRealX = 0.0d;
                this.ballRealY = 0.0d;
                this.mShowCup = false;
                this.mSOnHole = Integer.toString(this.mOnHole);
                this.mSPar = Integer.toString(this.mPars[this.mOnHole]);
                this.mInCup = false;
                setHole();
                this.MoverCount = 0;
                this.Mover = null;
                this.Mover = new Movers[50];
                if (this.mOnCourse < 1 || this.mOnCourse > 3) {
                    this.bmpHole = combineBitmaps(this.mOnHole);
                    this.bmpHoleU = combineBitmapsU(this.mOnHole);
                } else {
                    this.GROUND_OFF_GRID = this.bmpColors.getPixel(0, 1);
                    this.GROUND_OB_EW = this.bmpColors.getPixel(2, 1);
                    this.GROUND_OB_NS = this.bmpColors.getPixel(4, 1);
                    this.GROUND_WATER = this.bmpColors.getPixel(SOUND_BALLINSAND, 1);
                    this.GROUND_FAIRWAY = this.bmpColors.getPixel(8, 1);
                    this.GROUND_SAND = this.bmpColors.getPixel(10, 1);
                    this.GROUND_FACE_RANDOM = this.bmpColors.getPixel(12, 1);
                    this.GROUND_HOLE = this.bmpColors.getPixel(14, 1);
                    this.GROUND_FACE_SW = this.bmpColors.getPixel(RANDOM_COURSE_HIGH, 1);
                    this.GROUND_FACE_SE = this.bmpColors.getPixel(18, 1);
                    this.GROUND_FACE_NW = this.bmpColors.getPixel(20, 1);
                    this.GROUND_FACE_NE = this.bmpColors.getPixel(22, 1);
                    this.GROUND_FAIRWAY_WEST = this.bmpColors.getPixel(24, 1);
                    this.GROUND_FAIRWAY_NORTH = this.bmpColors.getPixel(26, 1);
                    this.GROUND_FAIRWAY_EAST = this.bmpColors.getPixel(28, 1);
                    this.GROUND_FAIRWAY_SOUTH = this.bmpColors.getPixel(30, 1);
                    this.GROUND_FAIRWAY_SW = this.bmpColors.getPixel(32, 1);
                    this.GROUND_FAIRWAY_NE = this.bmpColors.getPixel(34, 1);
                    this.GROUND_FAIRWAY_SE = this.bmpColors.getPixel(36, 1);
                    setHoleC123();
                }
                this.oX = this.mHoleStartX[this.mOnHole];
                this.oY = this.mHoleStartY[this.mOnHole];
                this.startX = this.oX;
                this.startY = this.oY;
                this.holeX = this.mHoleXs[this.mOnHole];
                this.holeY = this.mHoleYs[this.mOnHole];
                this.ballX = this.startX;
                this.ballY = this.startY;
                this.x1 = this.startX - 1;
                this.x2 = this.startX;
                this.x3 = this.startX + 1;
                this.y1 = this.startY;
                this.y2 = this.startY;
                this.y3 = this.startY;
                this.ballX = this.startX;
                this.ballY = this.startY;
                if (this.mLoadedGame && !this.loadInCup) {
                    this.ballX = this.loadBallX;
                    this.ballY = this.loadBallY;
                } else if (this.loadInCup && this.mLoadedGame) {
                    this.mShowCup = true;
                    this.mInCup = true;
                }
                this.mLoadedGame = false;
                this.ballRad = 3;
                if (this.mAimMethod == 0) {
                    setFXY((float) this.holeX, (float) this.holeY, false);
                } else {
                    setFXYStandard((float) this.holeX, (float) this.holeY, true);
                }
                this.mDelay = 10;
                makeScoreStr();
            }
        }

        public void run() {
            if (this.mRun) {
                Canvas c = null;
                try {
                    this.GameTime = System.currentTimeMillis();
                    c = this.mSurfaceHolder.lockCanvas(null);
                    synchronized (this.mSurfaceHolder) {
                        if (this.mGameMode == 3) {
                            if (this.mBallMoving) {
                                if (Math.abs(this.dx) > 0.2d || Math.abs(this.dy) > 0.2d || this.mPixelColor == this.GROUND_FAIRWAY_WEST || this.mPixelColor == this.GROUND_FAIRWAY_EAST || this.mPixelColor == this.GROUND_FAIRWAY_NORTH || this.mPixelColor == this.GROUND_FAIRWAY_NE || this.mPixelColor == this.GROUND_FAIRWAY_SW || this.mPixelColor == this.GROUND_FAIRWAY_NW || this.mPixelColor == this.GROUND_FAIRWAY_SE || this.mPixelColor == this.GROUND_FAIRWAY_SOUTH || !this.mBallMoving) {
                                    ballIsMoving();
                                } else {
                                    ballStoppedMoving();
                                }
                            }
                            if (this.MoverCount > 0) {
                                this.b = 0;
                                while (this.b < this.MoverCount) {
                                    this.Mover[this.b].update(this.GameTime);
                                    if (!this.mBallMoving && this.Mover[this.b].checkCollision(this.ballX, this.ballY)) {
                                        if (this.Mover[this.b].getBlocktype() < 37 || this.Mover[this.b].getBlocktype() > 40) {
                                            this.tmpX = this.ballX - this.Mover[this.b].getX();
                                            this.tmpY = (this.ballY - this.Mover[this.b].getY()) + 320;
                                            if (this.tmpX == 20) {
                                                this.tmpX = 19;
                                            }
                                            if (this.tmpY == 340) {
                                                this.tmpY = 339;
                                            }
                                            this.mPixelColor = this.bmpHoleU.getPixel(this.tmpX, this.tmpY);
                                        } else {
                                            this.mPixelColor = this.GROUND_FAIRWAY;
                                            if (this.Mover[this.b].getBlocktype() == this.GROUND_MOVING_PLATFORM_N || this.Mover[this.b].getBlocktype() == this.GROUND_MOVING_PLATFORM_S) {
                                                this.ballY += this.Mover[this.b].getDirection();
                                            } else if (this.Mover[this.b].getBlocktype() == this.GROUND_MOVING_PLATFORM_E || this.Mover[this.b].getBlocktype() == this.GROUND_MOVING_PLATFORM_W) {
                                                this.ballX += this.Mover[this.b].getDirection();
                                            }
                                        }
                                        if (this.mPixelColor == this.GROUND_OB_NS) {
                                            if (this.ballY < this.Mover[this.b].getY() + 10) {
                                                this.ballY = this.Mover[this.b].getY() - this.ballRad;
                                            } else {
                                                this.ballY = this.Mover[this.b].getY() + 20 + this.ballRad;
                                            }
                                        } else if (this.mPixelColor == this.GROUND_OB_EW) {
                                            if (this.ballX < this.Mover[this.b].getX() + 10) {
                                                this.ballX = this.Mover[this.b].getX() - this.ballRad;
                                            } else {
                                                this.ballX = this.Mover[this.b].getX() + 20 + this.ballRad;
                                            }
                                        }
                                        this.mPixelColor = this.bmpHoleU.getPixel(this.ballX, this.ballY);
                                        if (this.mAimMethod == 0) {
                                            setStartXY(this.ballX, this.ballY);
                                            moveFXY((float) this.holeX, (float) this.holeY, true);
                                        } else {
                                            setFXYStandard((float) this.holeX, (float) this.holeY, true);
                                        }
                                    }
                                    this.b++;
                                }
                            }
                            if (this.mPixelColor == this.GROUND_HOLE && !this.mInCup) {
                                ballInCup();
                                if (this.mInCup) {
                                    ballStoppedMoving();
                                }
                            }
                        }
                    }
                    doDrawNoResize(c);
                    this.mHandler.postDelayed(this, (long) this.mDelay);
                } finally {
                    if (c != null) {
                        this.mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0349, code lost:
            r9.ballRealY = (double) (r9.Mover[r1].getY() + 20);
            r9.dy *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x035f, code lost:
            r9.dx *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x0372, code lost:
            if (r9.Mover[r1].getBlocktype() == r9.GROUND_MOVING_WALL_E) goto L_0x0380;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x037e, code lost:
            if (r9.Mover[r1].getBlocktype() != r9.GROUND_MOVING_WALL_W) goto L_0x042a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0389, code lost:
            if (r9.Mover[r1].getDirection() != 1) goto L_0x042a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x038f, code lost:
            if (r9.mPixelColor != r9.GROUND_OB_EW) goto L_0x0421;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x0397, code lost:
            if (r9.dx <= 0.0d) goto L_0x03dd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x03a8, code lost:
            if (r9.pballX >= ((double) (r9.Mover[r1].getX() + 10))) goto L_0x03c1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x03aa, code lost:
            r9.ballRealX = (double) (r9.Mover[r1].getX() - r9.ballRad);
            r9.dx *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:115:0x03c1, code lost:
            r9.ballRealX = (double) ((r9.Mover[r1].getX() + 20) + r9.ballRad);
            r9.dx += 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x03ec, code lost:
            if (r9.pballX >= ((double) (r9.Mover[r1].getX() + 10))) goto L_0x03fe;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x03ee, code lost:
            r9.ballRealX = (double) (r9.Mover[r1].getX() - r9.ballRad);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x03fe, code lost:
            r9.ballRealX = (double) ((r9.Mover[r1].getX() + 20) + r9.ballRad);
            r9.dx *= -1.0d;
            r9.dx += 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0421, code lost:
            r9.dy *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x0434, code lost:
            if (r9.Mover[r1].getBlocktype() == r9.GROUND_MOVING_WALL_E) goto L_0x0442;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x0440, code lost:
            if (r9.Mover[r1].getBlocktype() != r9.GROUND_MOVING_WALL_W) goto L_0x0130;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x044b, code lost:
            if (r9.Mover[r1].getDirection() != -1) goto L_0x0130;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x0451, code lost:
            if (r9.mPixelColor != r9.GROUND_OB_EW) goto L_0x04e0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:130:0x0459, code lost:
            if (r9.dx <= 0.0d) goto L_0x049f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x046a, code lost:
            if (r9.pballX >= ((double) (r9.Mover[r1].getX() + 10))) goto L_0x048d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x046c, code lost:
            r9.ballRealX = (double) (r9.Mover[r1].getX() - r9.ballRad);
            r9.dx += 0.1d;
            r9.dx *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x048d, code lost:
            r9.ballRealX = (double) ((r9.Mover[r1].getX() + 20) + r9.ballRad);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x04ae, code lost:
            if (r9.pballX >= ((double) (r9.Mover[r1].getX() + 10))) goto L_0x04ca;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x04b0, code lost:
            r9.ballRealX = (double) (r9.Mover[r1].getX() - r9.ballRad);
            r9.dx -= 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x04ca, code lost:
            r9.ballRealX = (double) (r9.Mover[r1].getX() + 20);
            r9.dx *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:0x04e0, code lost:
            r9.dy *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x00af, code lost:
            if (r9.Mover[r1].getBlocktype() < 37) goto L_0x0200;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x00bb, code lost:
            if (r9.Mover[r1].getBlocktype() > 40) goto L_0x0200;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00bd, code lost:
            r9.mPixelColor = r9.GROUND_FAIRWAY;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00cb, code lost:
            if (r9.Mover[r1].getBlocktype() < 41) goto L_0x014a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00d7, code lost:
            if (r9.Mover[r1].getBlocktype() > 44) goto L_0x014a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x00e3, code lost:
            if (r9.Mover[r1].getBlocktype() == r9.GROUND_MOVING_WALL_N) goto L_0x00f1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ef, code lost:
            if (r9.Mover[r1].getBlocktype() != r9.GROUND_MOVING_WALL_S) goto L_0x02a9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00fa, code lost:
            if (r9.Mover[r1].getDirection() != 1) goto L_0x02a9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0100, code lost:
            if (r9.mPixelColor != r9.GROUND_OB_NS) goto L_0x02a0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0108, code lost:
            if (r9.dy <= 0.0d) goto L_0x025c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0119, code lost:
            if (r9.pballY >= ((double) (r9.Mover[r1].getY() + 10))) goto L_0x0240;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x011b, code lost:
            r9.ballRealY = (double) (r9.Mover[r1].getY() - r9.ballRad);
            r9.dy *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0130, code lost:
            r9.mPixelColor = r9.bmpHoleU.getPixel((int) r9.pballX, (int) r9.pballY);
            r9.ndx = (double) ((int) r9.pballX);
            r9.ndy = (double) ((int) r9.pballY);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
            r9.tmpX = ((int) r9.pballX) - r9.Mover[r1].getX();
            r9.tmpY = (((int) r9.pballY) - r9.Mover[r1].getY()) + 320;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x0222, code lost:
            if (r9.tmpX != 20) goto L_0x0228;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0224, code lost:
            r9.tmpX = 19;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x022c, code lost:
            if (r9.tmpY != 340) goto L_0x0232;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x022e, code lost:
            r9.tmpY = 339;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0232, code lost:
            r9.mPixelColor = r9.bmpHoleU.getPixel(r9.tmpX, r9.tmpY);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x0240, code lost:
            r9.ballRealY = (double) ((r9.Mover[r1].getY() + 20) + r9.ballRad);
            r9.dy += 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x026b, code lost:
            if (r9.pballY >= ((double) (r9.Mover[r1].getY() + 10))) goto L_0x027d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x026d, code lost:
            r9.ballRealY = (double) (r9.Mover[r1].getY() - r9.ballRad);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x027d, code lost:
            r9.ballRealY = (double) ((r9.Mover[r1].getY() + 20) + r9.ballRad);
            r9.dy *= -1.0d;
            r9.dy += 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x02a0, code lost:
            r9.dx *= -1.0d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x02b3, code lost:
            if (r9.Mover[r1].getBlocktype() == r9.GROUND_MOVING_WALL_N) goto L_0x02c1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x02bf, code lost:
            if (r9.Mover[r1].getBlocktype() != r9.GROUND_MOVING_WALL_S) goto L_0x0368;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x02ca, code lost:
            if (r9.Mover[r1].getDirection() != -1) goto L_0x0368;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x02d0, code lost:
            if (r9.mPixelColor != r9.GROUND_OB_NS) goto L_0x035f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x02d8, code lost:
            if (r9.dy <= 0.0d) goto L_0x031e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x02e9, code lost:
            if (r9.pballY >= ((double) (r9.Mover[r1].getY() + 10))) goto L_0x030c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x02eb, code lost:
            r9.ballRealY = (double) (r9.Mover[r1].getY() - r9.ballRad);
            r9.dy *= -1.0d;
            r9.dy -= 0.1d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x030c, code lost:
            r9.ballRealY = (double) ((r9.Mover[r1].getY() + 20) + r9.ballRad);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x032d, code lost:
            if (r9.pballY >= ((double) (r9.Mover[r1].getY() + 10))) goto L_0x0349;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x032f, code lost:
            r9.ballRealY = (double) (r9.Mover[r1].getY() - r9.ballRad);
            r9.dy -= 0.1d;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void ballIsMoving() {
            /*
                r9 = this;
                android.view.SurfaceHolder r2 = r9.mSurfaceHolder
                monitor-enter(r2)
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4606957238818648883(0x3fef333333333333, double:0.975)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4606957238818648883(0x3fef333333333333, double:0.975)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                r1 = -1
                double r3 = r9.ballRealX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                double r3 = r3 + r5
                r9.ndx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.ballRealY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                double r3 = r3 + r5
                r9.ndy = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.ndx     // Catch:{ Exception -> 0x01f5 }
                int r5 = r9.ballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                double r3 = r3 - r5
                int r5 = r9.ballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                double r7 = r9.ndy     // Catch:{ Exception -> 0x01f5 }
                double r5 = r5 - r7
                double r3 = java.lang.Math.atan2(r3, r5)     // Catch:{ Exception -> 0x01f5 }
                r5 = 4640537203540230144(0x4066800000000000, double:180.0)
                double r3 = r3 * r5
                r5 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
                double r3 = r3 / r5
                r9.tmptheta = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.tmptheta     // Catch:{ Exception -> 0x01f5 }
                r5 = 4640537203540230144(0x4066800000000000, double:180.0)
                double r3 = r3 / r5
                r5 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
                double r3 = r3 * r5
                double r3 = java.lang.Math.sin(r3)     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 * r5
                r9.pdx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.tmptheta     // Catch:{ Exception -> 0x01f5 }
                r5 = 4640537203540230144(0x4066800000000000, double:180.0)
                double r3 = r3 / r5
                r5 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
                double r3 = r3 * r5
                double r3 = java.lang.Math.cos(r3)     // Catch:{ Exception -> 0x01f5 }
                double r3 = -r3
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 * r5
                r9.pdy = r3     // Catch:{ Exception -> 0x01f5 }
                int r3 = r9.ballX     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.pballX = r3     // Catch:{ Exception -> 0x01f5 }
                int r3 = r9.ballY     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.pballY = r3     // Catch:{ Exception -> 0x01f5 }
                r3 = 0
                r9.bailout = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x0086:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.pdx     // Catch:{ Exception -> 0x01f5 }
                double r3 = r3 + r5
                r9.pballX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.pdy     // Catch:{ Exception -> 0x01f5 }
                double r3 = r3 + r5
                r9.pballY = r3     // Catch:{ Exception -> 0x01f5 }
                r1 = -1
                int r3 = r9.MoverCount     // Catch:{ Exception -> 0x01f5 }
                if (r3 <= 0) goto L_0x00a2
                r3 = 0
                r9.b = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x009c:
                int r3 = r9.b     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.MoverCount     // Catch:{ Exception -> 0x01f5 }
                if (r3 < r4) goto L_0x01d7
            L_0x00a2:
                r3 = -1
                if (r1 == r3) goto L_0x04e9
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                r4 = 37
                if (r3 < r4) goto L_0x0200
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                r4 = 40
                if (r3 > r4) goto L_0x0200
                int r3 = r9.GROUND_FAIRWAY     // Catch:{ Exception -> 0x01f5 }
                r9.mPixelColor = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x00c1:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                r4 = 41
                if (r3 < r4) goto L_0x014a
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                r4 = 44
                if (r3 > r4) goto L_0x014a
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_N     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x00f1
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_S     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x02a9
            L_0x00f1:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getDirection()     // Catch:{ Exception -> 0x01f5 }
                r4 = 1
                if (r3 != r4) goto L_0x02a9
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_OB_NS     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x02a0
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x025c
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getY()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x0240
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x0130:
                android.graphics.Bitmap r3 = r9.bmpHoleU     // Catch:{ Exception -> 0x01f5 }
                double r4 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r4 = (int) r4     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r5 = (int) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getPixel(r4, r5)     // Catch:{ Exception -> 0x01f5 }
                r9.mPixelColor = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ndx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ndy = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x014a:
                r3 = -1
                if (r1 != r3) goto L_0x016d
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_OB_NS     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x05b6
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x05ad
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
            L_0x0166:
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
            L_0x016d:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FACE_SW     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x05e0
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r9.dxtmp = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dxtmp     // Catch:{ all -> 0x01fd }
                r9.dy = r3     // Catch:{ all -> 0x01fd }
            L_0x018d:
                double r3 = r9.ballRealX     // Catch:{ all -> 0x01fd }
                double r5 = r9.dx     // Catch:{ all -> 0x01fd }
                double r3 = r3 + r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ballRealY     // Catch:{ all -> 0x01fd }
                double r5 = r9.dy     // Catch:{ all -> 0x01fd }
                double r3 = r3 + r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_WEST     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x065c
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
            L_0x01ab:
                double r3 = r9.ballRealX     // Catch:{ all -> 0x01fd }
                int r3 = (int) r3     // Catch:{ all -> 0x01fd }
                r9.ballX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ballRealY     // Catch:{ all -> 0x01fd }
                int r3 = (int) r3     // Catch:{ all -> 0x01fd }
                r9.ballY = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.mBelowWater = r3     // Catch:{ all -> 0x01fd }
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_WATER     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x0702
                r3 = 1
                r9.mBelowWater = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.mVelX = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.mVelY = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                r3 = 5
                r9.playSound(r3)     // Catch:{ all -> 0x01fd }
            L_0x01d5:
                monitor-exit(r2)     // Catch:{ all -> 0x01fd }
                return
            L_0x01d7:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.b     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r4]     // Catch:{ Exception -> 0x01f5 }
                double r4 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r4 = (int) r4     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r5 = (int) r5     // Catch:{ Exception -> 0x01f5 }
                boolean r3 = r3.checkCollision(r4, r5)     // Catch:{ Exception -> 0x01f5 }
                if (r3 == 0) goto L_0x01ed
                int r1 = r9.b     // Catch:{ Exception -> 0x01f5 }
                goto L_0x00a2
            L_0x01ed:
                int r3 = r9.b     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 1
                r9.b = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x009c
            L_0x01f5:
                r3 = move-exception
                r0 = r3
                int r3 = r9.GROUND_FAIRWAY     // Catch:{ all -> 0x01fd }
                r9.mPixelColor = r3     // Catch:{ all -> 0x01fd }
                goto L_0x014a
            L_0x01fd:
                r3 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x01fd }
                throw r3
            L_0x0200:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r4 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r4 = r4[r1]     // Catch:{ Exception -> 0x01f5 }
                int r4 = r4.getX()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                r9.tmpX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r4 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r4 = r4[r1]     // Catch:{ Exception -> 0x01f5 }
                int r4 = r4.getY()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                int r3 = r3 + 320
                r9.tmpY = r3     // Catch:{ Exception -> 0x01f5 }
                int r3 = r9.tmpX     // Catch:{ Exception -> 0x01f5 }
                r4 = 20
                if (r3 != r4) goto L_0x0228
                r3 = 19
                r9.tmpX = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x0228:
                int r3 = r9.tmpY     // Catch:{ Exception -> 0x01f5 }
                r4 = 340(0x154, float:4.76E-43)
                if (r3 != r4) goto L_0x0232
                r3 = 339(0x153, float:4.75E-43)
                r9.tmpY = r3     // Catch:{ Exception -> 0x01f5 }
            L_0x0232:
                android.graphics.Bitmap r3 = r9.bmpHoleU     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.tmpX     // Catch:{ Exception -> 0x01f5 }
                int r5 = r9.tmpY     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getPixel(r4, r5)     // Catch:{ Exception -> 0x01f5 }
                r9.mPixelColor = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x00c1
            L_0x0240:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 + r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x025c:
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getY()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x027d
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x027d:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 + r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x02a0:
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x02a9:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_N     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x02c1
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_S     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x0368
            L_0x02c1:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getDirection()     // Catch:{ Exception -> 0x01f5 }
                r4 = -1
                if (r3 != r4) goto L_0x0368
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_OB_NS     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x035f
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x031e
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getY()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x030c
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 - r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x030c:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x031e:
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getY()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x0349
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 - r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x0349:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getY()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealY = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x035f:
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x0368:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_E     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0380
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_W     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x042a
            L_0x0380:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getDirection()     // Catch:{ Exception -> 0x01f5 }
                r4 = 1
                if (r3 != r4) goto L_0x042a
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_OB_EW     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x0421
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x03dd
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getX()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x03c1
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x03c1:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x03dd:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getX()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x03fe
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x03fe:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x0421:
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x042a:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_E     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0442
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getBlocktype()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_MOVING_WALL_W     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x0130
            L_0x0442:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getDirection()     // Catch:{ Exception -> 0x01f5 }
                r4 = -1
                if (r3 != r4) goto L_0x0130
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_OB_EW     // Catch:{ Exception -> 0x01f5 }
                if (r3 != r4) goto L_0x04e0
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x049f
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getX()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x048d
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x048d:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x049f:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                com.selticeapps.funfunminigolflite.Movers[] r5 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r5 = r5[r1]     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5.getX()     // Catch:{ Exception -> 0x01f5 }
                int r5 = r5 + 10
                double r5 = (double) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 >= 0) goto L_0x04ca
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.ballRad     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 - r4
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r3 = r3 - r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x04ca:
                com.selticeapps.funfunminigolflite.Movers[] r3 = r9.Mover     // Catch:{ Exception -> 0x01f5 }
                r3 = r3[r1]     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getX()     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 20
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ballRealX = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.dx     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x04e0:
                double r3 = r9.dy     // Catch:{ Exception -> 0x01f5 }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x0130
            L_0x04e9:
                android.graphics.Bitmap r3 = r9.bmpHoleU     // Catch:{ Exception -> 0x01f5 }
                double r4 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r4 = (int) r4     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r5 = (int) r5     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3.getPixel(r4, r5)     // Catch:{ Exception -> 0x01f5 }
                r9.mPixelColor = r3     // Catch:{ Exception -> 0x01f5 }
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_SAND     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_WEST     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_EAST     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_NORTH     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_NE     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_SW     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_NW     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_SE     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                int r3 = r9.mPixelColor     // Catch:{ Exception -> 0x01f5 }
                int r4 = r9.GROUND_FAIRWAY_SOUTH     // Catch:{ Exception -> 0x01f5 }
                if (r3 == r4) goto L_0x0541
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ndx = r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                int r3 = (int) r3     // Catch:{ Exception -> 0x01f5 }
                double r3 = (double) r3     // Catch:{ Exception -> 0x01f5 }
                r9.ndy = r3     // Catch:{ Exception -> 0x01f5 }
                goto L_0x014a
            L_0x0541:
                double r3 = r9.pdx     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x0581
                double r3 = r9.pdy     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x056f
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndx     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x0561
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndy     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 <= 0) goto L_0x014a
            L_0x0561:
                int r3 = r9.bailout     // Catch:{ Exception -> 0x01f5 }
                int r3 = r3 + 1
                r9.bailout = r3     // Catch:{ Exception -> 0x01f5 }
                int r3 = r9.bailout     // Catch:{ Exception -> 0x01f5 }
                r4 = 100
                if (r3 <= r4) goto L_0x0086
                goto L_0x014a
            L_0x056f:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndx     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x0561
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndy     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x0561
                goto L_0x014a
            L_0x0581:
                double r3 = r9.pdy     // Catch:{ Exception -> 0x01f5 }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x059b
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndx     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x0561
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndy     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 > 0) goto L_0x0561
                goto L_0x014a
            L_0x059b:
                double r3 = r9.pballX     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndx     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x0561
                double r3 = r9.pballY     // Catch:{ Exception -> 0x01f5 }
                double r5 = r9.ndy     // Catch:{ Exception -> 0x01f5 }
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x0561
                goto L_0x014a
            L_0x05ad:
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                goto L_0x0166
            L_0x05b6:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_OB_EW     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x016d
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 0
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 < 0) goto L_0x05d8
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
            L_0x05cb:
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x016d
            L_0x05d8:
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                goto L_0x05cb
            L_0x05e0:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FACE_NW     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x0604
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r9.dxtmp = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dxtmp     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x018d
            L_0x0604:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FACE_SE     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x0628
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r9.dxtmp = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dxtmp     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x018d
            L_0x0628:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FACE_NE     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x064a
                double r3 = r9.ndx     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 + r5
                r9.ballRealX = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.ndy     // Catch:{ all -> 0x01fd }
                r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                double r3 = r3 - r5
                r9.ballRealY = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r9.dxtmp = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dxtmp     // Catch:{ all -> 0x01fd }
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x018d
            L_0x064a:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FACE_RANDOM     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x018d
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                double r3 = -r3
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x018d
            L_0x065c:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_EAST     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x066e
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x066e:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_NORTH     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x0680
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x0680:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_SOUTH     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x0692
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x0692:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_NE     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x06ae
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x06ae:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_NW     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x06ca
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x06ca:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_SW     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x06e6
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 - r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x06e6:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_FAIRWAY_SE     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x01ab
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4594212051873190380(0x3fc1eb851eb851ec, double:0.14)
                double r3 = r3 + r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01ab
            L_0x0702:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_OFF_GRID     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x071f
                r3 = 0
                r9.mVelX = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.mVelY = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                r3 = 0
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                r3 = 9
                r9.playSound(r3)     // Catch:{ all -> 0x01fd }
                goto L_0x01d5
            L_0x071f:
                int r3 = r9.mPixelColor     // Catch:{ all -> 0x01fd }
                int r4 = r9.GROUND_SAND     // Catch:{ all -> 0x01fd }
                if (r3 != r4) goto L_0x01d5
                double r3 = r9.dx     // Catch:{ all -> 0x01fd }
                r5 = 4605831338911806259(0x3feb333333333333, double:0.85)
                double r3 = r3 * r5
                r9.dx = r3     // Catch:{ all -> 0x01fd }
                double r3 = r9.dy     // Catch:{ all -> 0x01fd }
                r5 = 4605831338911806259(0x3feb333333333333, double:0.85)
                double r3 = r3 * r5
                r9.dy = r3     // Catch:{ all -> 0x01fd }
                boolean r3 = r9.mPlayedSandSound     // Catch:{ all -> 0x01fd }
                if (r3 != 0) goto L_0x01d5
                r3 = 6
                r9.playSound(r3)     // Catch:{ all -> 0x01fd }
                r3 = 1
                r9.mPlayedSandSound = r3     // Catch:{ all -> 0x01fd }
                goto L_0x01d5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.selticeapps.funfunminigolflite.GameView.GameThread.ballIsMoving():void");
        }

        public void ballInCup() {
            synchronized (this.mSurfaceHolder) {
                if (Math.abs(this.dx) > 6.0d || Math.abs(this.dy) > 6.0d) {
                    Toast toast = Toast.makeText(GameView.this.mContext, "Hit too hard.", 0);
                    toast.setGravity(49, 0, 5);
                    toast.show();
                    this.mPixelColor = this.GROUND_FAIRWAY;
                    this.mBallMoving = true;
                } else {
                    this.mInCup = true;
                    if (this.mStrokes + 1 <= this.mPars[this.mOnHole]) {
                        playSound(1);
                    } else {
                        playSound(SOUND_BALLIN2);
                    }
                    this.mVelX = 0.0d;
                    this.mVelY = 0.0d;
                    this.dx = 0.0d;
                    this.dy = 0.0d;
                    this.ballX = this.mHoleXs[this.mOnHole];
                    this.ballY = this.mHoleXs[this.mOnHole];
                    this.mBallMoving = false;
                }
            }
        }

        public void ballStoppedMoving() {
            synchronized (this.mSurfaceHolder) {
                boolean wasHazard = false;
                if (this.mPixelColor == this.GROUND_WATER) {
                    wasHazard = true;
                    this.mBelowWater = false;
                    this.ballX = this.mBallOriginalX;
                    this.ballY = this.mBallOriginalY;
                    this.mPixelColor = this.GROUND_FAIRWAY;
                    this.mStrokes++;
                    Toast toast = Toast.makeText(GameView.this.mContext, mHazardStr, 0);
                    toast.setGravity(49, 0, 5);
                    toast.show();
                } else if (!(this.mPixelColor == this.GROUND_FAIRWAY || this.mPixelColor == this.GROUND_SAND || this.mPixelColor == this.GROUND_FAIRWAY_WEST || this.mPixelColor == this.GROUND_FAIRWAY_EAST || this.mPixelColor == this.GROUND_FAIRWAY_NORTH || this.mPixelColor == this.GROUND_FAIRWAY_NE || this.mPixelColor == this.GROUND_FAIRWAY_SW || this.mPixelColor == this.GROUND_FAIRWAY_NW || this.mPixelColor == this.GROUND_FAIRWAY_SE || this.mPixelColor == this.GROUND_FAIRWAY_SOUTH || this.mInCup || this.mPixelColor != this.GROUND_OFF_GRID)) {
                    Toast toast2 = Toast.makeText(GameView.this.mContext, mOffCourseStr, 0);
                    toast2.setGravity(49, 0, 5);
                    toast2.show();
                    this.ballX = this.mBallOriginalX;
                    this.ballY = this.mBallOriginalY;
                    this.mPixelColor = this.GROUND_FAIRWAY;
                }
                resetAutoAim();
                this.mBallMoving = false;
                this.mIsSwinging = false;
                this.x1 = this.ballX;
                this.y1 = this.ballY;
                this.mPlayedSandSound = false;
                if (this.mAimMethod == 0) {
                    setStartXY(this.ballX, this.ballY);
                    moveFXY((float) this.holeX, (float) this.holeY, true);
                } else if (wasHazard) {
                    setFXYStandard((float) this.holeX, (float) this.holeY, false);
                } else {
                    setFXYStandard((float) this.holeX, (float) this.holeY, true);
                }
                if (this.mInCup) {
                    makeStrokeString();
                    this.mMyCourseTotal += this.mStrokes;
                    this.mCourseTotalTmp = 0;
                    for (int i = 1; i <= this.mOnHole; i++) {
                        this.mCourseTotalTmp += this.mPars[i];
                    }
                    this.mMyCoursePlusMinus = this.mMyCourseTotal - this.mCourseTotalTmp;
                    this.mShowCup = true;
                    if (this.starCount > this.DHoleStarCnt[this.mOnCourse][this.mOnHole - 1]) {
                        setNewHoleCount();
                    }
                }
                makeScoreStr();
                if (!this.mRandomMode) {
                    saveProgress();
                }
                this.mStrokesPlus = this.mStrokes + 1;
            }
        }

        public void resetAutoAim() {
            synchronized (this.mSurfaceHolder) {
                if (this.mAutoAim) {
                    this.mAimSet = true;
                } else {
                    this.mAimSet = false;
                }
            }
        }

        public void setNewHoleCount() {
            synchronized (this.mSurfaceHolder) {
                SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0).edit();
                if (!this.mIsLite) {
                    editor.putInt("HOLESTARCNT_" + this.mOnCourse + "_" + this.mOnHole, this.starCount);
                    editor.commit();
                }
            }
        }

        public void backButton() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setMessage("Are you sure you want to quit?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.setGameMode(1);
                            GameThread.this.mShowingMessage = false;
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.mShowingMessage = false;
                        }
                    }).show();
                }
            }
        }

        public void areYouSure() {
            synchronized (this.mSurfaceHolder) {
                new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle("Are you sure?").setMessage("Clicking Yes means the hole will restart, BUT your score WILL NOT SAVE at the end of this round.").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GameThread.this.mRestartedHole = true;
                        GameThread.this.mShowingMessage = false;
                        GameThread.this.setRestart(true);
                    }
                }).setNeutralButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GameThread.this.mShowingMessage = false;
                    }
                }).show();
            }
        }

        public void areYouSureQuit() {
            synchronized (this.mSurfaceHolder) {
                new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle("Warning").setMessage("Are you sure you want to quit?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GameThread.this.mShowingMessage = false;
                        GameThread.this.setGameMode(1);
                    }
                }).setNeutralButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        GameThread.this.mShowingMessage = false;
                    }
                }).show();
            }
        }

        public void doMulligan() {
            synchronized (this.mSurfaceHolder) {
                if (this.mResetCourseStrokes) {
                    this.mMyCourseTotal -= this.mStrokes;
                }
                this.mRestartCount++;
                this.mStrokes = 0;
                this.mStrokesPlus = 0;
                this.mResetCourseStrokes = false;
                this.mShowingMessage = false;
                setRestart(true);
            }
        }

        public void doNullScoreRestart() {
            synchronized (this.mSurfaceHolder) {
                if (this.mResetCourseStrokes) {
                    this.mMyCourseTotal -= this.mStrokes;
                }
                this.mRestartCount++;
                this.mStrokes = 0;
                this.mStrokesPlus = 0;
                this.mResetCourseStrokes = false;
                areYouSure();
            }
        }

        public void restartHow() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle("Hole Restart Options").setMessage("A Mulligan just restarts the hole. A Null-Score Restart means YOUR SCORE WILL NOT SAVE at the end of the course.").setCancelable(false).setPositiveButton("Mulligan", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.doMulligan();
                        }
                    }).setNeutralButton("Null-Score Restart", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.doNullScoreRestart();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.mResetCourseStrokes = false;
                            GameThread.this.mShowingMessage = false;
                        }
                    }).show();
                }
            }
        }

        public void showMessagePause() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setMessage("Game Options: What would you like to do?").setCancelable(false).setPositiveButton("Quit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.areYouSureQuit();
                        }
                    }).setNeutralButton("Restart Hole", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (GameThread.this.playSingleHole != -1 || GameThread.this.mRandomMode) {
                                if (GameThread.this.mResetCourseStrokes) {
                                    GameThread gameThread = GameThread.this;
                                    gameThread.mMyCourseTotal = gameThread.mMyCourseTotal - GameThread.this.mStrokes;
                                }
                                GameThread.this.mStrokes = 0;
                                GameThread.this.mStrokesPlus = 0;
                                GameThread.this.mResetCourseStrokes = false;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.setRestart(true);
                                return;
                            }
                            GameThread.this.mShowingMessage = false;
                            GameThread.this.restartHow();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.mShowingMessage = false;
                        }
                    }).show();
                }
            }
        }

        public void makeStrokeString() {
            synchronized (this.mSurfaceHolder) {
                if (this.mStrokes == 1) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Hole In One!";
                    this.starCount = 3;
                } else if (this.mStrokes < this.mPars[this.mOnHole] - 1) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Eagle";
                    this.starCount = 3;
                } else if (this.mStrokes == this.mPars[this.mOnHole] - 1) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Birdie";
                    this.starCount = 3;
                } else if (this.mStrokes == this.mPars[this.mOnHole]) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Par";
                    this.starCount = 2;
                } else if (this.mStrokes == this.mPars[this.mOnHole] + 1) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Bogey";
                    this.starCount = 1;
                } else if (this.mStrokes == this.mPars[this.mOnHole] + 2) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Double Bogey";
                    this.starCount = 0;
                } else if (this.mStrokes == this.mPars[this.mOnHole] + 3) {
                    this.mSStrokes = String.valueOf(Integer.toString(this.mStrokes)) + ", Triple Bogey";
                    this.starCount = 0;
                } else {
                    this.mSStrokes = Integer.toString(this.mStrokes);
                    this.starCount = 0;
                }
            }
        }

        public void saveProgress() {
            synchronized (this.mSurfaceHolder) {
                if (this.HOLE_COUNT != -1 && this.playSingleHole == -1) {
                    SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0).edit();
                    editor.putInt("HOLE_" + this.mOnCourse, this.mOnHole);
                    editor.putInt("STROKES_" + this.mOnCourse, this.mStrokes);
                    editor.putInt("MYCOURSETOTAL_" + this.mOnCourse, this.mMyCourseTotal);
                    editor.putInt("MYCOURSEPLUSMINUS_" + this.mOnCourse, this.mMyCoursePlusMinus);
                    editor.putInt("COURSERESTARTS_" + this.mOnCourse, this.mRestartCount);
                    editor.putInt("BALLX_" + this.mOnCourse, this.ballX);
                    editor.putInt("BALLY_" + this.mOnCourse, this.ballY);
                    editor.putBoolean("INCUP_" + this.mOnCourse, this.mInCup);
                    editor.putBoolean("RESTARTEDHOLE_" + this.mOnCourse, this.mRestartedHole);
                    editor.commit();
                }
            }
        }

        public void setAim() {
            synchronized (this.mSurfaceHolder) {
                SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(PREFS_NAME, 0).edit();
                editor.putInt("AIM_TYPE", this.mAimMethod);
                editor.commit();
            }
        }

        public void clearProgress() {
            synchronized (this.mSurfaceHolder) {
                SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0).edit();
                editor.putInt("HOLE_" + this.mOnCourse, 0);
                editor.putInt("STROKES_" + this.mOnCourse, 0);
                editor.putInt("MYCOURSETOTAL_" + this.mOnCourse, this.mMyCourseTotal);
                editor.putInt("MYCOURSEPLUSMINUS_" + this.mOnCourse, this.mMyCoursePlusMinus);
                editor.putInt("COURSERESTARTS_" + this.mOnCourse, 0);
                editor.putInt("BALLX_" + this.mOnCourse, 0);
                editor.putInt("BALLY_" + this.mOnCourse, 0);
                editor.putBoolean("INCUP_" + this.mOnCourse, false);
                editor.putBoolean("RESTARTEDHOLE_" + this.mOnCourse, false);
                editor.commit();
            }
        }

        public void loadProgress() {
            synchronized (this.mSurfaceHolder) {
                SharedPreferences settings = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0);
                this.mOnHole = settings.getInt("HOLE_" + this.mOnCourse, 0);
                this.mStrokes = settings.getInt("STROKES_" + this.mOnCourse, 0);
                this.mMyCourseTotal = settings.getInt("MYCOURSETOTAL_" + this.mOnCourse, 0);
                this.mMyCoursePlusMinus = settings.getInt("MYCOURSEPLUSMINUS_" + this.mOnCourse, 0);
                this.mRestartCount = settings.getInt("COURSERESTARTS_" + this.mOnCourse, 0);
                this.loadBallX = settings.getInt("BALLX_" + this.mOnCourse, 0);
                this.loadBallY = settings.getInt("BALLY_" + this.mOnCourse, 0);
                this.loadInCup = settings.getBoolean("INCUP_" + this.mOnCourse, false);
                this.mRestartedHole = settings.getBoolean("RESTARTEDHOLE_" + this.mOnCourse, false);
            }
        }

        public void setBallRad(int rad) {
            synchronized (this.mSurfaceHolder) {
                this.ballRad = rad;
            }
        }

        public void setBallXY(int x, int y) {
            synchronized (this.mSurfaceHolder) {
                this.ballX = x;
                this.ballY = y;
            }
        }

        public void loadHighscores() {
            synchronized (this.mSurfaceHolder) {
                try {
                    this.highscores = this.myDbHelper.readHighscores(this.mOnCourse);
                    this.highscores_tmp = new String[20];
                    this.highscores_tmp2 = new String[5];
                    this.highscores_tmp = this.highscores.split("::::::");
                    for (int i = 0; i < 5; i++) {
                        this.hs_players[i] = "";
                        this.hs_shifterstimes[i] = "";
                        this.hs_normaltimes[i] = "";
                        this.hs_normaltimes2[i] = "";
                    }
                    if (this.mOnCourse > 3) {
                        this.COURSE_TOTAL = getCoursePar(this.mOnCourse);
                    } else if (this.mOnCourse == 1) {
                        setCourseOnePar();
                    } else if (this.mOnCourse == 2) {
                        setCourseTwoPar();
                    } else if (this.mOnCourse == 3) {
                        setCourseThreePar();
                    }
                    for (int i2 = 0; i2 < 5; i2++) {
                        this.highscores_tmp2 = this.highscores_tmp[i2].split(":::");
                        try {
                            this.hs_players[i2] = this.highscores_tmp2[2];
                            this.hs_shifterstimes[i2] = this.highscores_tmp2[0];
                            try {
                                int k = Integer.parseInt(this.highscores_tmp2[1]);
                                int j = k - this.COURSE_TOTAL;
                                if (k == 0) {
                                    this.hs_normaltimes[i2] = "";
                                    this.hs_normaltimes2[i2] = "";
                                } else {
                                    this.hs_normaltimes[i2] = this.highscores_tmp2[1];
                                    if (j > 0) {
                                        this.hs_normaltimes2[i2] = "(+" + Integer.toString(j) + ")";
                                    } else if (j == 0) {
                                        this.hs_normaltimes2[i2] = "(Even)";
                                    } else {
                                        this.hs_normaltimes2[i2] = "(" + Integer.toString(j) + ")";
                                    }
                                }
                                this.hs_shifterstimes[i2] = this.hs_shifterstimes[i2].trim();
                            } catch (Exception e) {
                                this.hs_normaltimes[i2] = "";
                                this.hs_normaltimes2[i2] = "";
                            }
                        } catch (Exception e2) {
                            this.hs_shifterstimes[i2] = "";
                            this.hs_normaltimes[i2] = "";
                        }
                    }
                } catch (Exception e3) {
                }
            }
        }

        public void playSound(int sound) {
            synchronized (this.mSurfaceHolder) {
                if (this.mPlaySounds) {
                    float streamVolume = (float) ((AudioManager) GameView.this.getContext().getSystemService("audio")).getStreamVolume(3);
                    this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), streamVolume, streamVolume, 1, 0, 1.0f);
                }
            }
        }

        public void getCourseBests() {
            synchronized (this.mSurfaceHolder) {
                this.courseBestScores = new int[17];
                SharedPreferences settings = GameView.this.mContext.getSharedPreferences(SAVE_PREFS_NAME, 0);
                for (int i = 0; i < 17; i++) {
                    this.courseBestScores[i] = settings.getInt("COURSEBEST_" + i, 50000);
                    this.courseStars[i] = 0;
                    this.courseFlagTrophy[i] = 0;
                    this.courseParTrophy[i] = settings.getInt("COURSEPERFECT_" + i, 0);
                    for (int j = 0; j < SOUND_OB; j++) {
                        this.DHoleStarCnt[i][j] = settings.getInt("HOLESTARCNT_" + i + "_" + (j + 1), 0);
                        this.courseStars[i] = this.courseStars[i] + this.DHoleStarCnt[i][j];
                    }
                    this.courseStars[i] = (int) Math.floor((double) (this.courseStars[i] / SOUND_OB));
                    if (this.courseStars[i] == 3) {
                        this.courseFlagTrophy[i] = 1;
                    } else {
                        this.courseFlagTrophy[i] = 0;
                    }
                }
            }
        }

        public int getCourseBest(int whichCourse) {
            int mCourseBestTmp;
            synchronized (this.mSurfaceHolder) {
                mCourseBestTmp = 50000;
                if (whichCourse != 17) {
                    mCourseBestTmp = this.courseBestScores[whichCourse];
                    this.mBestStr = "";
                    if (mCourseBestTmp == 50000) {
                        this.mBestStr = "No Scores Recorded";
                    } else {
                        if (whichCourse == 1) {
                            setCourseOnePar();
                        } else if (whichCourse == 2) {
                            setCourseTwoPar();
                        } else if (whichCourse == 3) {
                            setCourseThreePar();
                        }
                        int j = mCourseBestTmp - this.COURSE_TOTAL;
                        if (j > 0) {
                            this.mBestStr = "Best Score: " + mCourseBestTmp + " (+" + Integer.toString(j) + ")";
                        } else if (j == 0) {
                            this.mBestStr = "Best Score: " + mCourseBestTmp + " (Even)";
                        } else {
                            this.mBestStr = "Best Score: " + mCourseBestTmp + " (" + Integer.toString(j) + ")";
                        }
                    }
                }
            }
            return mCourseBestTmp;
        }

        public void setGameMode(int mode) {
            synchronized (this.mSurfaceHolder) {
                try {
                    this.mBackgroundImage.recycle();
                } catch (Exception e) {
                }
                if (mode == 18) {
                    if (!this.mWentBack) {
                        this.mOnCourse = 1;
                        this.mShowingPre = false;
                    }
                    getCourseBests();
                    this.playSingleHole = -1;
                    this.mWentBack = false;
                    if (this.mAimMethod == 0) {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.coursesstandard);
                    } else if (this.mAutoAim) {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.courses);
                    } else {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.courses2);
                    }
                } else if (mode == 21) {
                    if (this.playSingleHole != -1) {
                        getNewOnCourse();
                        this.mShowingPre = false;
                        getCourseBests();
                        this.playSingleHole = -1;
                    }
                    if (!this.mIsLite) {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.coursepage2);
                    } else {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.coursepage2lite);
                    }
                } else if (mode == 1) {
                    this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.welcome);
                } else if (mode == 17) {
                    this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.instructions);
                } else if (mode == 23) {
                    if (!this.mIsLite) {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.flagsandtrophies);
                    } else {
                        this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.flagsandtrophieslite);
                    }
                } else if (mode == 4) {
                    this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.bestscores);
                } else if (mode == 5) {
                    this.mBackgroundImage = BitmapFactory.decodeResource(GameView.this.getResources(), R.drawable.bestscores2);
                }
                this.mGameMode = mode;
            }
        }

        public void getNewOnCourse() {
            synchronized (this.mSurfaceHolder) {
                int i = 0;
                while (true) {
                    if (i >= this.gameModesVis.length) {
                        break;
                    } else if (this.gameModesVis[i] == this.mOnCourse) {
                        this.mOnCourse = i;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }

        public int getGameMode() {
            int i;
            synchronized (this.mSurfaceHolder) {
                i = this.mGameMode;
            }
            return i;
        }

        public int getAimMethod() {
            int i;
            synchronized (this.mSurfaceHolder) {
                i = this.mAimMethod;
            }
            return i;
        }

        public boolean getAutoAim() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mAutoAim;
            }
            return z;
        }

        public void setAutoAim(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mAutoAim = which;
                SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(PREFS_NAME, 0).edit();
                editor.putBoolean("AUTOAIM", which);
                editor.commit();
            }
        }

        public boolean getSounds() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mPlaySounds;
            }
            return z;
        }

        public void setSounds(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mPlaySounds = which;
            }
        }

        public void setRestart(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mInCup = false;
                this.mSetRestartedHole = true;
                doStart();
            }
        }

        public void setRunning(boolean b2) {
            synchronized (this.mSurfaceHolder) {
                this.mRun = b2;
            }
        }

        public void doRunning(boolean b2) {
            synchronized (this.mSurfaceHolder) {
                if (this.safeToRun) {
                    this.mRun = b2;
                }
            }
        }

        public void showMessage(String title, String msg) {
            synchronized (this.mSurfaceHolder) {
                new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle(title).setMessage(msg).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        }

        public void showMessageMustBuy() {
            synchronized (this.mSurfaceHolder) {
                new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setMessage("This course is only available in the Full Version of Fun-Putt Mini Golf.").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
            }
        }

        public double roundTwoDecimals(double d3) {
            double doubleValue;
            synchronized (this.mSurfaceHolder) {
                doubleValue = Double.valueOf(new DecimalFormat("#.##").format(d3)).doubleValue();
            }
            return doubleValue;
        }

        public void setSurfaceSizeNew(double width, double height, float density, boolean mOKScale) {
            synchronized (this.mSurfaceHolder) {
                this.canvasWidth = ((int) (height * 480.0d)) / 320;
                this.canvasHeight = (int) height;
                this.canvasHeightFull = (int) ((width * height) / 480.0d);
                this.cs = null;
                if (height <= 320.0d || !mOKScale) {
                    this.mMultiplierHeight = 1.0f;
                    this.mMultiplierWidth = 1.0f;
                } else {
                    this.mMultiplierHeight = ((float) this.canvasHeight) / 320.0f;
                    this.mMultiplierWidth = ((float) this.canvasWidth) / 480.0f;
                }
            }
        }

        public void setSurfaceSize(double width, double height) {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public boolean getPaused() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mIsPaused;
            }
            return z;
        }

        public void setLoadMarketRate(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mLoadMarketRate = which;
            }
        }

        public boolean getLoadMarketRate() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mLoadMarketRate;
            }
            return z;
        }

        public void setLoadMarket(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mLoadMarket = which;
            }
        }

        public boolean getLoadMarket() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mLoadMarket;
            }
            return z;
        }

        public void setLoadEmail(boolean which) {
            synchronized (this.mSurfaceHolder) {
                this.mLoadEmail = which;
            }
        }

        public boolean getLoadEmail() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mLoadEmail;
            }
            return z;
        }

        public boolean getmPre() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mPre;
            }
            return z;
        }

        public int getCoursePar(int mThisCourse) {
            int tmpTotal;
            synchronized (this.mSurfaceHolder) {
                this.HOLE_COUNT = SOUND_OB;
                this.COURSE_TOTAL = 0;
                tmpTotal = 0;
                String onName = new String();
                new String();
                XmlResourceParser xpp = GameView.this.mContext.getResources().getXml(this.courseIDs[mThisCourse]);
                try {
                    xpp.next();
                    for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                        if (eventType == 2) {
                            onName = xpp.getName();
                        } else if (eventType == 4) {
                            if (onName.equals("CourseName")) {
                                String courseName = xpp.getText();
                            } else if (onName.equals("Hole1Par")) {
                                this.mPars[1] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole2Par")) {
                                this.mPars[2] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole3Par")) {
                                this.mPars[3] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole4Par")) {
                                this.mPars[4] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole5Par")) {
                                this.mPars[5] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole6Par")) {
                                this.mPars[SOUND_BALLINSAND] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole7Par")) {
                                this.mPars[7] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole8Par")) {
                                this.mPars[8] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole9Par")) {
                                this.mPars[SOUND_OB] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole1Bottom")) {
                                this.mHoleBottom[1] = xpp.getText();
                            } else if (onName.equals("Hole2Bottom")) {
                                this.mHoleBottom[2] = xpp.getText();
                            } else if (onName.equals("Hole3Bottom")) {
                                this.mHoleBottom[3] = xpp.getText();
                            } else if (onName.equals("Hole4Bottom")) {
                                this.mHoleBottom[4] = xpp.getText();
                            } else if (onName.equals("Hole5Bottom")) {
                                this.mHoleBottom[5] = xpp.getText();
                            } else if (onName.equals("Hole6Bottom")) {
                                this.mHoleBottom[SOUND_BALLINSAND] = xpp.getText();
                            } else if (onName.equals("Hole7Bottom")) {
                                this.mHoleBottom[7] = xpp.getText();
                            } else if (onName.equals("Hole8Bottom")) {
                                this.mHoleBottom[8] = xpp.getText();
                            } else if (onName.equals("Hole9Bottom")) {
                                this.mHoleBottom[SOUND_OB] = xpp.getText();
                            } else if (onName.equals("Hole1Top")) {
                                this.mHoleTop[1] = xpp.getText();
                            } else if (onName.equals("Hole2Top")) {
                                this.mHoleTop[2] = xpp.getText();
                            } else if (onName.equals("Hole3Top")) {
                                this.mHoleTop[3] = xpp.getText();
                            } else if (onName.equals("Hole4Top")) {
                                this.mHoleTop[4] = xpp.getText();
                            } else if (onName.equals("Hole5Top")) {
                                this.mHoleTop[5] = xpp.getText();
                            } else if (onName.equals("Hole6Top")) {
                                this.mHoleTop[SOUND_BALLINSAND] = xpp.getText();
                            } else if (onName.equals("Hole7Top")) {
                                this.mHoleTop[7] = xpp.getText();
                            } else if (onName.equals("Hole8Top")) {
                                this.mHoleTop[8] = xpp.getText();
                            } else if (onName.equals("Hole9Top")) {
                                this.mHoleTop[SOUND_OB] = xpp.getText();
                            }
                        }
                    }
                    for (int i = 1; i <= SOUND_OB; i++) {
                        tmpTotal += this.mPars[i];
                    }
                } catch (Exception e) {
                }
            }
            return tmpTotal;
        }

        public void setCourseOnePar() {
            synchronized (this.mSurfaceHolder) {
                this.COURSE_TOTAL = 0;
                this.mPars[1] = 2;
                this.mPars[2] = 2;
                this.mPars[3] = 3;
                this.mPars[4] = 3;
                this.mPars[5] = 2;
                this.mPars[SOUND_BALLINSAND] = 2;
                this.mPars[7] = 3;
                this.mPars[8] = 3;
                this.mPars[SOUND_OB] = 2;
                for (int i = 1; i <= SOUND_OB; i++) {
                    this.COURSE_TOTAL += this.mPars[i];
                    this.HoleTopX[i] = -1;
                    this.HoleTopY[i] = -1;
                }
            }
        }

        public void setCourseTwoPar() {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void setCourseThreePar() {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void loadCourseOne() {
            synchronized (this.mSurfaceHolder) {
                this.HOLE_COUNT = SOUND_OB;
                if (this.mLoadedGame) {
                    this.START_HOLE = this.mOnHole;
                } else {
                    this.START_HOLE = 1;
                }
                this.COURSE_TOTAL = 0;
                this.mPars[1] = 2;
                this.mPars[2] = 2;
                this.mPars[3] = 3;
                this.mPars[4] = 3;
                this.mPars[5] = 2;
                this.mPars[SOUND_BALLINSAND] = 2;
                this.mPars[7] = 3;
                this.mPars[8] = 3;
                this.mPars[SOUND_OB] = 2;
                for (int i = 1; i <= this.HOLE_COUNT; i++) {
                    this.COURSE_TOTAL += this.mPars[i];
                }
                this.mHoleStartX[1] = 86;
                this.mHoleStartY[1] = 176;
                this.mHoleStartX[2] = 100;
                this.mHoleStartY[2] = 78;
                this.mHoleStartX[3] = 84;
                this.mHoleStartY[3] = 170;
                this.mHoleStartX[4] = 91;
                this.mHoleStartY[4] = 212;
                this.mHoleStartX[5] = 68;
                this.mHoleStartY[5] = 204;
                this.mHoleStartX[SOUND_BALLINSAND] = 48;
                this.mHoleStartY[SOUND_BALLINSAND] = 174;
                this.mHoleStartX[7] = 49;
                this.mHoleStartY[7] = 94;
                this.mHoleStartX[8] = 394;
                this.mHoleStartY[8] = 188;
                this.mHoleStartX[SOUND_OB] = 59;
                this.mHoleStartY[SOUND_OB] = 125;
                this.mHoleXs[1] = 400;
                this.mHoleYs[1] = 88;
                this.mHoleXs[2] = 330;
                this.mHoleYs[2] = 66;
                this.mHoleXs[3] = 366;
                this.mHoleYs[3] = 88;
                this.mHoleXs[4] = 399;
                this.mHoleYs[4] = 199;
                this.mHoleXs[5] = 405;
                this.mHoleYs[5] = 56;
                this.mHoleXs[SOUND_BALLINSAND] = 269;
                this.mHoleYs[SOUND_BALLINSAND] = 35;
                this.mHoleXs[7] = 394;
                this.mHoleYs[7] = 172;
                this.mHoleXs[8] = 78;
                this.mHoleYs[8] = 158;
                this.mHoleXs[SOUND_OB] = 427;
                this.mHoleYs[SOUND_OB] = 124;
                this.HoleTopX[1] = -1;
                this.HoleTopY[1] = -1;
                this.HoleTopX[2] = -1;
                this.HoleTopY[2] = -1;
                this.HoleTopX[3] = -1;
                this.HoleTopY[3] = -1;
                this.HoleTopX[4] = -1;
                this.HoleTopY[4] = -1;
                this.HoleTopX[5] = -1;
                this.HoleTopY[5] = -1;
                this.HoleTopX[SOUND_BALLINSAND] = -1;
                this.HoleTopY[SOUND_BALLINSAND] = -1;
                this.HoleTopX[7] = -1;
                this.HoleTopY[7] = -1;
                this.HoleTopX[8] = 194;
                this.HoleTopY[8] = 184;
                this.HoleTopX[SOUND_OB] = -1;
                this.HoleTopY[SOUND_OB] = -1;
                this.mOnHole = this.START_HOLE;
                if (!this.mLoadedGame) {
                    this.mMyCourseTotal = 0;
                    this.mMyCoursePlusMinus = 0;
                }
                this.mCourseTotalTmp = 0;
                this.mOnCourse = 1;
                startGame();
                setGameMode(3);
            }
        }

        public void loadCourseTwo() {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void loadCourseThree() {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void loadCourseOnePre() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.mOnCourse = 1;
                    loadProgress();
                    this.mLoadedGame = false;
                    if (this.mOnHole <= 0 || this.playSingleHole != -1) {
                        this.mRestartCount = 0;
                        this.mShowingPre = false;
                        this.mShowingMessage = false;
                        loadCourseOne();
                    } else {
                        this.alert = new AlertDialog.Builder(GameView.this.mContext);
                        this.alert.setMessage("Would you like to continue this course from its most recent auto-save point?");
                        this.alert.setCancelable(false);
                        this.alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = true;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.loadCourseOne();
                            }
                        });
                        this.alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = false;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.mRestartCount = 0;
                                GameThread.this.loadCourseOne();
                            }
                        });
                        this.alert.show();
                    }
                }
            }
        }

        public void loadCourseTwoPre() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.mOnCourse = 2;
                    loadProgress();
                    this.mLoadedGame = false;
                    if (this.mOnHole <= 0 || this.playSingleHole != -1) {
                        this.mShowingPre = false;
                        this.mShowingMessage = false;
                        this.mRestartCount = 0;
                        loadCourseTwo();
                    } else {
                        this.alert = new AlertDialog.Builder(GameView.this.mContext);
                        this.alert.setMessage("Would you like to continue this course from its most recent auto-save point?");
                        this.alert.setCancelable(false);
                        this.alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = true;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.loadCourseTwo();
                            }
                        });
                        this.alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = false;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.mRestartCount = 0;
                                GameThread.this.loadCourseTwo();
                            }
                        });
                        this.alert.show();
                    }
                }
            }
        }

        public void loadCourseThreePre() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.mOnCourse = 3;
                    loadProgress();
                    this.mLoadedGame = false;
                    if (this.mOnHole <= 0 || this.playSingleHole != -1) {
                        this.mShowingPre = false;
                        this.mShowingMessage = false;
                        this.mRestartCount = 0;
                        loadCourseThree();
                    } else {
                        this.alert = new AlertDialog.Builder(GameView.this.mContext);
                        this.alert.setMessage("Would you like to continue this course from its most recent auto-save point?");
                        this.alert.setCancelable(false);
                        this.alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = true;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.loadCourseThree();
                            }
                        });
                        this.alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = false;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.mRestartCount = 0;
                                GameThread.this.loadCourseThree();
                            }
                        });
                        this.alert.show();
                    }
                }
            }
        }

        public void loadCourseOneNew() {
            XmlResourceParser xpp;
            synchronized (this.mSurfaceHolder) {
                if (this.mRandomMode) {
                    this.HOLE_COUNT = -1;
                } else {
                    this.HOLE_COUNT = SOUND_OB;
                }
                if (this.mLoadedGame) {
                    this.START_HOLE = this.mOnHole;
                } else {
                    this.START_HOLE = 1;
                }
                if (this.playSingleHole == -1) {
                    this.mOnHole = this.START_HOLE;
                } else {
                    this.mOnHole = this.playSingleHole;
                }
                this.COURSE_TOTAL = 0;
                boolean mOK = false;
                String chash = new String();
                String onName = new String();
                Resources res = GameView.this.mContext.getResources();
                if (!this.mRandomMode || this.playSingleHole != -1) {
                    xpp = res.getXml(this.courseIDs[this.mOnCourse]);
                } else {
                    int mLastOnCourse = this.mOnCourse;
                    int mLastOnHole = this.mOnHole;
                    int ii = 0;
                    while (true) {
                        this.mOnCourse = this.generator.nextInt(13) + 4;
                        this.mOnHole = this.generator.nextInt(SOUND_OB) + 1;
                        if (mLastOnCourse != this.mOnCourse || mLastOnHole != this.mOnHole) {
                            break;
                        }
                        ii++;
                        if (ii > 50) {
                            break;
                        }
                    }
                    xpp = res.getXml(this.courseIDs[this.mOnCourse]);
                }
                try {
                    xpp.next();
                    for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                        if (eventType == 2) {
                            onName = xpp.getName();
                        } else if (eventType == 4 && !onName.equals("CourseName")) {
                            if (onName.equals("Hole1Par")) {
                                this.mPars[1] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole2Par")) {
                                this.mPars[2] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole3Par")) {
                                this.mPars[3] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole4Par")) {
                                this.mPars[4] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole5Par")) {
                                this.mPars[5] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole6Par")) {
                                this.mPars[SOUND_BALLINSAND] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole7Par")) {
                                this.mPars[7] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole8Par")) {
                                this.mPars[8] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole9Par")) {
                                this.mPars[SOUND_OB] = Integer.parseInt(xpp.getText());
                            } else if (onName.equals("Hole1Bottom")) {
                                this.mHoleBottom[1] = xpp.getText();
                            } else if (onName.equals("Hole2Bottom")) {
                                this.mHoleBottom[2] = xpp.getText();
                            } else if (onName.equals("Hole3Bottom")) {
                                this.mHoleBottom[3] = xpp.getText();
                            } else if (onName.equals("Hole4Bottom")) {
                                this.mHoleBottom[4] = xpp.getText();
                            } else if (onName.equals("Hole5Bottom")) {
                                this.mHoleBottom[5] = xpp.getText();
                            } else if (onName.equals("Hole6Bottom")) {
                                this.mHoleBottom[SOUND_BALLINSAND] = xpp.getText();
                            } else if (onName.equals("Hole7Bottom")) {
                                this.mHoleBottom[7] = xpp.getText();
                            } else if (onName.equals("Hole8Bottom")) {
                                this.mHoleBottom[8] = xpp.getText();
                            } else if (onName.equals("Hole9Bottom")) {
                                this.mHoleBottom[SOUND_OB] = xpp.getText();
                            } else if (onName.equals("Hole1Top")) {
                                this.mHoleTop[1] = xpp.getText();
                            } else if (onName.equals("Hole2Top")) {
                                this.mHoleTop[2] = xpp.getText();
                            } else if (onName.equals("Hole3Top")) {
                                this.mHoleTop[3] = xpp.getText();
                            } else if (onName.equals("Hole4Top")) {
                                this.mHoleTop[4] = xpp.getText();
                            } else if (onName.equals("Hole5Top")) {
                                this.mHoleTop[5] = xpp.getText();
                            } else if (onName.equals("Hole6Top")) {
                                this.mHoleTop[SOUND_BALLINSAND] = xpp.getText();
                            } else if (onName.equals("Hole7Top")) {
                                this.mHoleTop[7] = xpp.getText();
                            } else if (onName.equals("Hole8Top")) {
                                this.mHoleTop[8] = xpp.getText();
                            } else if (onName.equals("Hole9Top")) {
                                this.mHoleTop[SOUND_OB] = xpp.getText();
                            }
                        }
                    }
                    for (int i = 1; i <= SOUND_OB; i++) {
                        chash = String.valueOf(chash) + this.mHoleBottom[i] + this.mHoleTop[i];
                    }
                    if (md5(chash).equals(this.courseHash[this.mOnCourse])) {
                        mOK = true;
                    } else {
                        mOK = false;
                    }
                } catch (Exception e) {
                }
                int i2 = 1;
                while (i2 <= SOUND_OB) {
                    try {
                        StringTokenizer stringTokenizer = new StringTokenizer(this.mHoleBottom[i2], ",");
                        int j = 0;
                        while (true) {
                            if (stringTokenizer.hasMoreTokens()) {
                                try {
                                    this.mHoleA[i2][j] = Integer.parseInt(stringTokenizer.nextToken());
                                    if (this.mHoleA[i2][j] == TILE_START) {
                                        this.mHoleStartX[i2] = ((j % 24) * 20) + 10;
                                        this.mHoleStartY[i2] = ((int) (20.0d * Math.floor((double) (j / 24)))) + 10;
                                    } else if (this.mHoleA[i2][j] == TILE_HOLE) {
                                        this.mHoleXs[i2] = ((j % 24) * 20) + 10;
                                        this.mHoleYs[i2] = ((int) (20.0d * Math.floor((double) (j / 24)))) + 10;
                                    }
                                } catch (Exception e2) {
                                    this.mHoleA[i2][j] = -1;
                                }
                                j++;
                                if (j == 360) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        StringTokenizer stringTokenizer2 = new StringTokenizer(this.mHoleTop[i2], ",");
                        while (true) {
                            if (stringTokenizer2.hasMoreTokens()) {
                                try {
                                    this.mHoleA[i2][j] = Integer.parseInt(stringTokenizer2.nextToken());
                                    if (this.mHoleA[i2][j] == TILE_START) {
                                        this.mHoleStartX[i2] = (((j - 360) % 24) * 20) + 10;
                                        this.mHoleStartY[i2] = ((int) (20.0d * Math.floor((double) ((j - 360) / 24)))) + 10;
                                    } else if (this.mHoleA[i2][j] == TILE_HOLE) {
                                        this.mHoleXs[i2] = (((j - 360) % 24) * 20) + 10;
                                        this.mHoleYs[i2] = ((int) (20.0d * Math.floor((double) ((j - 360) / 24)))) + 10;
                                    }
                                } catch (Exception e3) {
                                    this.mHoleA[i2][j] = -1;
                                }
                                j++;
                                if (j == 720) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        i2++;
                    } catch (Exception e4) {
                    }
                }
                if (!this.mLoadedGame) {
                    this.mMyCourseTotal = 0;
                    this.mMyCoursePlusMinus = 0;
                }
                this.mCourseTotalTmp = 0;
                if (mOK) {
                    startGame();
                    setGameMode(3);
                } else if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.alert = new AlertDialog.Builder(GameView.this.mContext);
                    this.alert.setMessage("There was an error loading this course, please contact support at selticesystems@gmail.com");
                    this.alert.setCancelable(false);
                    this.alert.setPositiveButton("Return", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            GameThread.this.mShowingMessage = false;
                            GameThread.this.setGameMode(1);
                        }
                    });
                    this.alert.show();
                } else {
                    setGameMode(1);
                }
            }
        }

        public void loadCoursePre() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    loadProgress();
                    this.mLoadedGame = false;
                    if (this.mOnHole <= 0 || this.playSingleHole != -1) {
                        this.mShowingPre = false;
                        this.mShowingMessage = false;
                        this.mRestartCount = 0;
                        loadCourseOneNew();
                    } else {
                        this.alert = new AlertDialog.Builder(GameView.this.mContext);
                        this.alert.setMessage("Would you like to continue this course from its most recent auto-save point?");
                        this.alert.setCancelable(false);
                        this.alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = true;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.loadCourseOneNew();
                            }
                        });
                        this.alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GameThread.this.mLoadedGame = false;
                                GameThread.this.mShowingMessage = false;
                                GameThread.this.mShowingPre = false;
                                GameThread.this.mRestartCount = 0;
                                GameThread.this.loadCourseOneNew();
                            }
                        });
                        this.alert.show();
                    }
                }
            }
        }

        public void loadCoursePreRandom() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.mLoadedGame = false;
                    this.mShowingPre = false;
                    this.mShowingMessage = false;
                    loadCourseOneNew();
                }
            }
        }

        public void loadCourse() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    this.mOnCourse = -1;
                    this.alert = new AlertDialog.Builder(GameView.this.mContext);
                    this.alert.setTitle("Select a Course");
                    this.alert.setCancelable(false);
                    this.alert.setSingleChoiceItems(this.gameModes, 0, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            GameThread.this.mOnCourse = item + 1;
                        }
                    });
                    this.alert.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            GameThread.this.mShowingMessage = false;
                            if (GameThread.this.mOnCourse <= 0) {
                                GameThread.this.mOnCourse = 1;
                            }
                            GameThread.this.loadCoursePre();
                        }
                    });
                    this.alert.show();
                }
            }
        }

        public Bitmap combineBitmaps(int whichHole) {
            int DIRECTION;
            Bitmap cs2 = Bitmap.createBitmap(480, 320, Bitmap.Config.ARGB_8888);
            Canvas comboImage = new Canvas(cs2);
            comboImage.drawRect(0.0f, 0.0f, 480.0f, 320.0f, this.blackpaint);
            int p = 0;
            for (int m = 0; m < 15; m++) {
                for (int n = 0; n < 24; n++) {
                    comboImage.drawBitmap(this.Tiles[this.mHoleA[whichHole][p]], (float) (n * 20), (float) (m * 20), (Paint) null);
                    if (this.mHoleA[whichHole][p + 360] >= 37 && this.mHoleA[whichHole][p + 360] <= 44) {
                        int TOP_MOST = -1;
                        int BOTTOM_MOST = -1;
                        int LEFT_MOST = -1;
                        int RIGHT_MOST = -1;
                        int DIRECTION2 = 1;
                        if (this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_N || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_N || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_S || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_S) {
                            int r = 0;
                            while (true) {
                                if (r > m) {
                                    break;
                                } else if (this.mHoleA[whichHole][p - (r * 24)] != this.mHoleA[whichHole][p]) {
                                    TOP_MOST = (((m - r) + 1) * 20) + this.ballRad;
                                    break;
                                } else {
                                    r++;
                                }
                            }
                            int r2 = 0;
                            while (true) {
                                if (r2 >= 15 - m) {
                                    break;
                                } else if (this.mHoleA[whichHole][(r2 * 24) + p] != this.mHoleA[whichHole][p]) {
                                    BOTTOM_MOST = (((m + r2) - 1) * 20) - this.ballRad;
                                    break;
                                } else {
                                    r2++;
                                }
                            }
                            if (this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_N || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_N) {
                                DIRECTION = -1;
                            } else {
                                DIRECTION = 1;
                            }
                            LEFT_MOST = n * 20;
                            RIGHT_MOST = n * 20;
                        } else if (this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_W || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_W || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_E || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_E) {
                            int r3 = 0;
                            while (true) {
                                if (r3 > n) {
                                    break;
                                } else if (this.mHoleA[whichHole][p - r3] != this.mHoleA[whichHole][p]) {
                                    LEFT_MOST = (((n - r3) + 1) * 20) + this.ballRad;
                                    break;
                                } else {
                                    r3++;
                                }
                            }
                            int r4 = 0;
                            while (true) {
                                if (r4 >= 24 - n) {
                                    break;
                                } else if (this.mHoleA[whichHole][p + r4] != this.mHoleA[whichHole][p]) {
                                    RIGHT_MOST = (((n + r4) - 1) * 20) - this.ballRad;
                                    break;
                                } else {
                                    r4++;
                                }
                            }
                            if (this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_PLATFORM_W || this.mHoleA[whichHole][p + 360] == this.GROUND_MOVING_WALL_W) {
                                DIRECTION2 = -1;
                            } else {
                                DIRECTION2 = 1;
                            }
                            TOP_MOST = m * 20;
                            BOTTOM_MOST = m * 20;
                        }
                        this.Mover[this.MoverCount] = new Movers(n * 20, m * 20, this.mHoleA[whichHole][p + 360], this.mHoleA[whichHole][p], DIRECTION2, TOP_MOST, BOTTOM_MOST, LEFT_MOST, RIGHT_MOST);
                        this.MoverCount = this.MoverCount + 1;
                    }
                    p++;
                }
            }
            int p2 = 0;
            for (int m2 = 0; m2 < 15; m2++) {
                for (int n2 = 0; n2 < 24; n2++) {
                    if (this.mHoleA[whichHole][p2 + 360] < 37 || this.mHoleA[whichHole][p2 + 360] > 44) {
                        if (this.mHoleA[whichHole][p2 + 360] == SOUND_BALLINSAND) {
                            if (this.generator.nextBoolean()) {
                                comboImage.drawBitmap(this.Tiles[88], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                            } else {
                                comboImage.drawBitmap(this.Tiles[this.mHoleA[whichHole][p2 + 360]], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                            }
                        } else if (this.mHoleA[whichHole][p2 + 360] != 1 && this.mHoleA[whichHole][p2 + 360] != 46) {
                            comboImage.drawBitmap(this.Tiles[this.mHoleA[whichHole][p2 + 360]], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                        } else if (this.generator.nextBoolean()) {
                            comboImage.drawBitmap(this.Tiles[89], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                        } else {
                            comboImage.drawBitmap(this.Tiles[this.mHoleA[whichHole][p2 + 360]], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                        }
                    }
                    p2++;
                }
            }
            comboImage.drawBitmap(this.bmpPause, 438.0f, 0.0f, (Paint) null);
            return cs2;
        }

        public Bitmap combineBitmapsU(int whichHole) {
            Bitmap cs2 = Bitmap.createBitmap(480, 360, Bitmap.Config.ARGB_4444);
            Canvas comboImage = new Canvas(cs2);
            comboImage.drawRect(-200.0f, -200.0f, 680.0f, 550.0f, this.blackpaint);
            int p = 0;
            for (int m = 0; m < 15; m++) {
                for (int n = 0; n < 24; n++) {
                    comboImage.drawBitmap(this.TilesU[this.mHoleA[whichHole][p]], (float) (n * 20), (float) (m * 20), (Paint) null);
                    p++;
                }
            }
            int p2 = 0;
            for (int m2 = 0; m2 < 15; m2++) {
                for (int n2 = 0; n2 < 24; n2++) {
                    if (this.mHoleA[whichHole][p2 + 360] < 37 || this.mHoleA[whichHole][p2 + 360] > 44) {
                        comboImage.drawBitmap(this.TilesU[this.mHoleA[whichHole][p2 + 360]], (float) (n2 * 20), (float) (m2 * 20), (Paint) null);
                    }
                    p2++;
                }
            }
            comboImage.drawBitmap(this.TilesU[0], 0.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[SOUND_BALLIN2], 20.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[SOUND_BALLINSAND], 40.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[RANDOM_COURSE_HIGH], 60.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[30], 80.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[TILE_HOLE], 100.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[4], 120.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[5], 140.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[2], 160.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[3], 180.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[23], 200.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[24], 220.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[21], 240.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[22], 260.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[26], 280.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[28], 300.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[25], 320.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[27], 340.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[1], 360.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[46], 380.0f, 300.0f, (Paint) null);
            comboImage.drawBitmap(this.TilesU[87], 0.0f, 320.0f, (Paint) null);
            this.GROUND_OFF_GRID = cs2.getPixel(10, 310);
            this.GROUND_WATER = cs2.getPixel(30, 310);
            this.GROUND_FAIRWAY = cs2.getPixel(50, 310);
            this.GROUND_SAND = cs2.getPixel(70, 310);
            this.GROUND_FACE_RANDOM = cs2.getPixel(90, 310);
            this.GROUND_HOLE = cs2.getPixel(110, 310);
            this.GROUND_FACE_SW = cs2.getPixel(135, 310);
            this.GROUND_FACE_SE = cs2.getPixel(145, 310);
            this.GROUND_FACE_NW = cs2.getPixel(175, 310);
            this.GROUND_FACE_NE = cs2.getPixel(185, 310);
            this.GROUND_FAIRWAY_WEST = cs2.getPixel(210, 310);
            this.GROUND_FAIRWAY_NORTH = cs2.getPixel(230, 310);
            this.GROUND_FAIRWAY_EAST = cs2.getPixel(250, 310);
            this.GROUND_FAIRWAY_SOUTH = cs2.getPixel(270, 310);
            this.GROUND_FAIRWAY_SW = cs2.getPixel(290, 310);
            this.GROUND_FAIRWAY_NE = cs2.getPixel(310, 310);
            this.GROUND_FAIRWAY_SE = cs2.getPixel(330, 310);
            this.GROUND_FAIRWAY_NW = cs2.getPixel(350, 310);
            this.GROUND_OB_EW = cs2.getPixel(370, 310);
            this.GROUND_OB_NS = cs2.getPixel(390, 310);
            return cs2;
        }

        public void loadHighscoresPre() {
            synchronized (this.mSurfaceHolder) {
                if (!this.mShowingMessage) {
                    this.mShowingMessage = true;
                    CharSequence[] gameModesTmp = new CharSequence[RANDOM_COURSE_HIGH];
                    int j = 1;
                    for (int i = 1; i <= 17; i++) {
                        if (i != 17) {
                            gameModesTmp[j - 1] = this.gameModes[i];
                            this.gameIDsTmp[j - 1] = i;
                            j++;
                        }
                    }
                    this.mOnCourse = -1;
                    this.alert = new AlertDialog.Builder(GameView.this.mContext);
                    this.alert.setTitle("Select a Course");
                    this.alert.setCancelable(false);
                    this.alert.setSingleChoiceItems(gameModesTmp, 0, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            GameThread.this.mOnCourse = GameThread.this.gameIDsTmp[item];
                        }
                    });
                    this.alert.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            GameThread.this.mShowingMessage = false;
                            if (GameThread.this.mOnCourse <= 0) {
                                GameThread.this.mOnCourse = 1;
                            }
                            GameThread.this.loadHighscores();
                            GameThread.this.setGameMode(4);
                        }
                    });
                    this.alert.show();
                }
            }
        }

        /* JADX WARNING: Unknown top exception splitter block from list: {B:318:0x04d3=Splitter:B:318:0x04d3, B:17:0x0034=Splitter:B:17:0x0034, B:331:0x0508=Splitter:B:331:0x0508, B:305:0x049b=Splitter:B:305:0x049b} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void doMakeMove(float r12, float r13) {
            /*
                r11 = this;
                r10 = 1134362624(0x439d0000, float:314.0)
                r8 = 1133117440(0x438a0000, float:276.0)
                r7 = 1131216896(0x436d0000, float:237.0)
                r9 = 1
                android.view.SurfaceHolder r4 = r11.mSurfaceHolder
                monitor-enter(r4)
                r5 = 1
                r11.setRunning(r5)     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mLoadMarket = r5     // Catch:{ all -> 0x0045 }
                r5 = 10
                r11.mDelay = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 3
                if (r5 != r6) goto L_0x020a
                r5 = 0
                r11.safeToRun = r5     // Catch:{ all -> 0x0045 }
                int r2 = (int) r12     // Catch:{ all -> 0x0045 }
                int r3 = (int) r13     // Catch:{ all -> 0x0045 }
                r5 = 440(0x1b8, float:6.17E-43)
                if (r2 < r5) goto L_0x0034
                r5 = 480(0x1e0, float:6.73E-43)
                if (r2 > r5) goto L_0x0034
                r5 = 40
                if (r3 > r5) goto L_0x0034
                if (r3 < 0) goto L_0x0034
                r11.showMessagePause()     // Catch:{ all -> 0x0045 }
            L_0x0030:
                monitor-exit(r4)     // Catch:{ all -> 0x0045 }
                r11.safeToRun = r9
                return
            L_0x0034:
                boolean r5 = r11.mInCup     // Catch:{ all -> 0x0045 }
                if (r5 == 0) goto L_0x00e8
                boolean r5 = r11.mRandomMode     // Catch:{ all -> 0x0045 }
                if (r5 == 0) goto L_0x007f
                int r5 = r11.playSingleHole     // Catch:{ all -> 0x0045 }
                r6 = -1
                if (r5 != r6) goto L_0x0048
                r11.loadCourseOneNew()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0045:
                r5 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0045 }
                throw r5
            L_0x0048:
                r5 = 187(0xbb, float:2.62E-43)
                if (r2 < r5) goto L_0x005e
                r5 = 193(0xc1, float:2.7E-43)
                if (r3 < r5) goto L_0x005e
                r5 = 378(0x17a, float:5.3E-43)
                if (r2 > r5) goto L_0x005e
                r5 = 230(0xe6, float:3.22E-43)
                if (r3 > r5) goto L_0x005e
                r5 = 21
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x005e:
                r5 = 92
                if (r2 < r5) goto L_0x0030
                r5 = 193(0xc1, float:2.7E-43)
                if (r3 < r5) goto L_0x0030
                r5 = 176(0xb0, float:2.47E-43)
                if (r2 > r5) goto L_0x0030
                r5 = 230(0xe6, float:3.22E-43)
                if (r3 > r5) goto L_0x0030
                r5 = 0
                r11.mStrokes = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mStrokesPlus = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mResetCourseStrokes = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mShowingMessage = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.setRestart(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x007f:
                int r5 = r11.mOnHole     // Catch:{ all -> 0x0045 }
                int r6 = r11.HOLE_COUNT     // Catch:{ all -> 0x0045 }
                if (r5 != r6) goto L_0x00a0
                r5 = 187(0xbb, float:2.62E-43)
                if (r2 < r5) goto L_0x0099
                r5 = 193(0xc1, float:2.7E-43)
                if (r3 < r5) goto L_0x0099
                r5 = 378(0x17a, float:5.3E-43)
                if (r2 > r5) goto L_0x0099
                r5 = 230(0xe6, float:3.22E-43)
                if (r3 > r5) goto L_0x0099
                r11.saveHighScore()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0099:
                r5 = 0
                r11.mStrokesPlus = r5     // Catch:{ all -> 0x0045 }
                r11.restartHow()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x00a0:
                r5 = 187(0xbb, float:2.62E-43)
                if (r2 < r5) goto L_0x00cd
                r5 = 193(0xc1, float:2.7E-43)
                if (r3 < r5) goto L_0x00cd
                r5 = 378(0x17a, float:5.3E-43)
                if (r2 > r5) goto L_0x00cd
                r5 = 230(0xe6, float:3.22E-43)
                if (r3 > r5) goto L_0x00cd
                int r5 = r11.mOnHole     // Catch:{ all -> 0x0045 }
                int r5 = r5 + 1
                r11.mOnHole = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mStrokesPlus = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                if (r5 != r9) goto L_0x00c9
                r11.setHoleC123()     // Catch:{ all -> 0x0045 }
            L_0x00c0:
                r5 = 1
                r11.setRestart(r5)     // Catch:{ all -> 0x0045 }
                r11.saveProgress()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x00c9:
                r11.setHole()     // Catch:{ all -> 0x0045 }
                goto L_0x00c0
            L_0x00cd:
                r5 = 92
                if (r2 < r5) goto L_0x0030
                r5 = 193(0xc1, float:2.7E-43)
                if (r3 < r5) goto L_0x0030
                r5 = 176(0xb0, float:2.47E-43)
                if (r2 > r5) goto L_0x0030
                r5 = 230(0xe6, float:3.22E-43)
                if (r3 > r5) goto L_0x0030
                r5 = 0
                r11.mStrokesPlus = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.mResetCourseStrokes = r5     // Catch:{ all -> 0x0045 }
                r11.restartHow()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x00e8:
                int r5 = r11.mAimMethod     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x00f4
                r5 = 1
                r11.mStarted = r5     // Catch:{ all -> 0x0045 }
                r11.setStartXY(r2, r3)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x00f4:
                boolean r5 = r11.mIsSwinging     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x0030
                boolean r5 = r11.mBallMoving     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x0030
                int r5 = r11.ballX     // Catch:{ all -> 0x0045 }
                r11.startX = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.ballY     // Catch:{ all -> 0x0045 }
                r11.startY = r5     // Catch:{ all -> 0x0045 }
                r5 = 265(0x109, float:3.71E-43)
                if (r3 < r5) goto L_0x0132
                r5 = 330(0x14a, float:4.62E-43)
                if (r3 > r5) goto L_0x0132
                r5 = 140(0x8c, float:1.96E-43)
                if (r2 < r5) goto L_0x0132
                r5 = 395(0x18b, float:5.54E-43)
                if (r2 > r5) goto L_0x0132
                r5 = 1
                r11.mSettingPower = r5     // Catch:{ all -> 0x0045 }
                r5 = 220(0xdc, float:3.08E-43)
                if (r2 >= r5) goto L_0x0120
                r5 = 0
                r11.mPowerLevel = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0120:
                r5 = 365(0x16d, float:5.11E-43)
                if (r2 <= r5) goto L_0x012a
                r5 = 150(0x96, float:2.1E-43)
                r11.mPowerLevel = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x012a:
                r5 = 220(0xdc, float:3.08E-43)
                int r5 = r2 - r5
                r11.mPowerLevel = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0132:
                r5 = 265(0x109, float:3.71E-43)
                if (r3 < r5) goto L_0x01f8
                r5 = 330(0x14a, float:4.62E-43)
                if (r3 > r5) goto L_0x01f8
                r5 = 405(0x195, float:5.68E-43)
                if (r2 < r5) goto L_0x01f8
                r5 = 480(0x1e0, float:6.73E-43)
                if (r2 > r5) goto L_0x01f8
                boolean r5 = r11.mAutoAim     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x014e
                boolean r5 = r11.mAutoAim     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x01df
                boolean r5 = r11.mAimSet     // Catch:{ all -> 0x0045 }
                if (r5 == 0) goto L_0x01df
            L_0x014e:
                r5 = 0
                r11.mStarted = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mIsSwinging = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.mBallMoving = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.ballX     // Catch:{ all -> 0x0045 }
                double r5 = (double) r5     // Catch:{ all -> 0x0045 }
                r11.ballRealX = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.ballY     // Catch:{ all -> 0x0045 }
                double r5 = (double) r5     // Catch:{ all -> 0x0045 }
                r11.ballRealY = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.ballX     // Catch:{ all -> 0x0045 }
                r11.mBallOriginalX = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.ballY     // Catch:{ all -> 0x0045 }
                r11.mBallOriginalY = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mPowerLevel     // Catch:{ all -> 0x0045 }
                if (r5 != 0) goto L_0x0170
                r5 = 1
                r11.mPowerLevel = r5     // Catch:{ all -> 0x0045 }
            L_0x0170:
                int r5 = r11.mPowerLevel     // Catch:{ all -> 0x0045 }
                double r5 = (double) r5     // Catch:{ all -> 0x0045 }
                r7 = 4591870180066957722(0x3fb999999999999a, double:0.1)
                double r5 = r5 * r7
                r11.mAPower = r5     // Catch:{ all -> 0x0045 }
                double r5 = r11.mAPower     // Catch:{ all -> 0x0045 }
                r7 = 4624633867356078080(0x402e000000000000, double:15.0)
                int r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
                if (r5 <= 0) goto L_0x0187
                r5 = 4624633867356078080(0x402e000000000000, double:15.0)
                r11.mAPower = r5     // Catch:{ all -> 0x0045 }
            L_0x0187:
                r5 = 4590429028186199163(0x3fb47ae147ae147b, double:0.08)
                r11.mResistanceX = r5     // Catch:{ all -> 0x0045 }
                r5 = 4590429028186199163(0x3fb47ae147ae147b, double:0.08)
                r11.mResistanceY = r5     // Catch:{ all -> 0x0045 }
                double r5 = r11.mAPower     // Catch:{ all -> 0x0045 }
                r11.mVelX = r5     // Catch:{ all -> 0x0045 }
                double r5 = r11.mAPower     // Catch:{ all -> 0x0045 }
                r11.mVelY = r5     // Catch:{ all -> 0x0045 }
                double r5 = r11.theta     // Catch:{ all -> 0x0045 }
                r7 = 4640537203540230144(0x4066800000000000, double:180.0)
                double r5 = r5 / r7
                r7 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
                double r5 = r5 * r7
                double r5 = java.lang.Math.sin(r5)     // Catch:{ all -> 0x0045 }
                double r7 = r11.mAPower     // Catch:{ all -> 0x0045 }
                double r5 = r5 * r7
                r11.dx = r5     // Catch:{ all -> 0x0045 }
                double r5 = r11.theta     // Catch:{ all -> 0x0045 }
                r7 = 4640537203540230144(0x4066800000000000, double:180.0)
                double r5 = r5 / r7
                r7 = 4614256656552045848(0x400921fb54442d18, double:3.141592653589793)
                double r5 = r5 * r7
                double r5 = java.lang.Math.cos(r5)     // Catch:{ all -> 0x0045 }
                double r5 = -r5
                double r7 = r11.mAPower     // Catch:{ all -> 0x0045 }
                double r5 = r5 * r7
                r11.dy = r5     // Catch:{ all -> 0x0045 }
                r5 = 10
                r11.mDelay = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mStrokes     // Catch:{ all -> 0x0045 }
                int r5 = r5 + 1
                r11.mStrokes = r5     // Catch:{ all -> 0x0045 }
                r11.makeScoreStr()     // Catch:{ all -> 0x0045 }
                r5 = 3
                r11.playSound(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x01df:
                com.selticeapps.funfunminigolflite.GameView r5 = com.selticeapps.funfunminigolflite.GameView.this     // Catch:{ all -> 0x0045 }
                android.content.Context r5 = r5.mContext     // Catch:{ all -> 0x0045 }
                java.lang.String r6 = "Touch anywhere on the course to Aim first."
                r7 = 0
                android.widget.Toast r1 = android.widget.Toast.makeText(r5, r6, r7)     // Catch:{ all -> 0x0045 }
                r5 = 49
                r6 = 0
                r7 = 5
                r1.setGravity(r5, r6, r7)     // Catch:{ all -> 0x0045 }
                r1.show()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x01f8:
                r5 = 270(0x10e, float:3.78E-43)
                if (r3 >= r5) goto L_0x0030
                r5 = 1
                r11.mAiming = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.mAimSet = r5     // Catch:{ all -> 0x0045 }
                float r5 = (float) r2     // Catch:{ all -> 0x0045 }
                float r6 = (float) r3     // Catch:{ all -> 0x0045 }
                r7 = 1
                r11.setFXYStandard(r5, r6, r7)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x020a:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 4
                if (r5 != r6) goto L_0x022e
                r5 = 1123811328(0x42fc0000, float:126.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1135869952(0x43b40000, float:360.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1132462080(0x43800000, float:256.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1133936640(0x43968000, float:301.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x022e:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 5
                if (r5 != r6) goto L_0x0283
                r5 = 1115815936(0x42820000, float:65.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0250
                int r5 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x0250
                r5 = 1132462080(0x43800000, float:256.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0250
                r5 = 1133936640(0x43968000, float:301.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0250
                r5 = 1
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0250:
                r5 = 1131872256(0x43770000, float:247.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1137672192(0x43cf8000, float:415.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1132462080(0x43800000, float:256.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1133936640(0x43968000, float:301.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1
                r11.mWentBack = r5     // Catch:{ all -> 0x0045 }
                r5 = 18
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                r11.getNewOnCourse()     // Catch:{ all -> 0x0045 }
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                int r5 = r11.getCourseBest(r5)     // Catch:{ all -> 0x0045 }
                r11.mCourseBest = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0283:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 17
                if (r5 == r6) goto L_0x028f
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 23
                if (r5 != r6) goto L_0x0295
            L_0x028f:
                r5 = 1
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0295:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                if (r5 != r9) goto L_0x0374
                r5 = 1095761920(0x41500000, float:13.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02c2
                r5 = 1122893824(0x42ee0000, float:119.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x02c2
                r5 = 1126629376(0x43270000, float:167.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02c2
                int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x02c2
                r5 = 18
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                int r5 = r11.getCourseBest(r5)     // Catch:{ all -> 0x0045 }
                r11.mCourseBest = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x02c2:
                r5 = 1124073472(0x43000000, float:128.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02df
                r5 = 1131085824(0x436b0000, float:235.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x02df
                r5 = 1126629376(0x43270000, float:167.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02df
                int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x02df
                r5 = 17
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x02df:
                r5 = 1131675648(0x43740000, float:244.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02fc
                r5 = 1135542272(0x43af0000, float:350.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x02fc
                r5 = 1126629376(0x43270000, float:167.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x02fc
                int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x02fc
                r5 = 23
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x02fc:
                r5 = 1135869952(0x43b40000, float:360.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x031b
                r5 = 1139376128(0x43e98000, float:467.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x031b
                r5 = 1126629376(0x43270000, float:167.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x031b
                int r5 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
                if (r5 > 0) goto L_0x031b
                r5 = 1
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                r11.loadHighscoresPre()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x031b:
                r5 = 1133084672(0x43898000, float:275.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x033a
                r5 = 1139376128(0x43e98000, float:467.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x033a
                r5 = 1132265472(0x437d0000, float:253.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x033a
                r5 = 1133576192(0x43910000, float:290.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x033a
                r5 = 1
                r11.mLoadMarket = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x033a:
                r5 = 1099431936(0x41880000, float:17.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0357
                r5 = 1123024896(0x42f00000, float:120.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0357
                r5 = 1132265472(0x437d0000, float:253.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0357
                r5 = 1133576192(0x43910000, float:290.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0357
                r5 = 1
                r11.mLoadMarketRate = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0357:
                r5 = 1123811328(0x42fc0000, float:126.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1132920832(0x43870000, float:270.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1132265472(0x437d0000, float:253.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1133576192(0x43910000, float:290.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1
                r11.mLoadEmail = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0374:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 19
                if (r5 != r6) goto L_0x0406
                r5 = 1098907648(0x41800000, float:16.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x039e
                r5 = 1126105088(0x431f0000, float:159.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x039e
                r5 = 1124270080(0x43030000, float:131.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x039e
                r5 = 1128857600(0x43490000, float:201.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x039e
                r5 = 1
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                r11.loadHighscores()     // Catch:{ all -> 0x0045 }
                r5 = 4
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x039e:
                r5 = 1126825984(0x432a0000, float:170.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x03c3
                r5 = 1134395392(0x439d8000, float:315.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x03c3
                r5 = 1124270080(0x43030000, float:131.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x03c3
                r5 = 1128857600(0x43490000, float:201.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x03c3
                r5 = 2
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                r11.loadHighscores()     // Catch:{ all -> 0x0045 }
                r5 = 4
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x03c3:
                r5 = 1134690304(0x43a20000, float:324.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x03e7
                r5 = 1139474432(0x43eb0000, float:470.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x03e7
                r5 = 1124270080(0x43030000, float:131.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x03e7
                r5 = 1128857600(0x43490000, float:201.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x03e7
                r5 = 3
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                r11.loadHighscores()     // Catch:{ all -> 0x0045 }
                r5 = 4
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x03e7:
                r5 = 1123811328(0x42fc0000, float:126.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1135869952(0x43b40000, float:360.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1130168320(0x435d0000, float:221.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1132691456(0x43838000, float:263.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0406:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 18
                if (r5 != r6) goto L_0x05ca
                r5 = 1137344512(0x43ca8000, float:405.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0442
                r5 = 1112014848(0x42480000, float:50.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0442
                r5 = 1139802112(0x43f00000, float:480.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0442
                r5 = 1123680256(0x42fa0000, float:125.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0442
                int r5 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                int r5 = r5 + 1
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r6 = 17
                if (r5 <= r6) goto L_0x0434
                r5 = 1
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
            L_0x0434:
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                int r5 = r11.getCourseBest(r5)     // Catch:{ all -> 0x0045 }
                r11.mCourseBest = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0442:
                r5 = 0
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0474
                r5 = 1112014848(0x42480000, float:50.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0474
                r5 = 1118044160(0x42a40000, float:82.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0474
                r5 = 1123680256(0x42fa0000, float:125.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0474
                int r5 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                int r5 = r5 - r9
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                int r5 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                if (r5 >= r9) goto L_0x0466
                r5 = 17
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
            L_0x0466:
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                int r5 = r11.getCourseBest(r5)     // Catch:{ all -> 0x0045 }
                r11.mCourseBest = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0474:
                r5 = 1106771968(0x41f80000, float:31.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x04ac
                r5 = 1126563840(0x43260000, float:166.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x04ac
                r5 = 1127546880(0x43350000, float:181.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x04ac
                r5 = 1130561536(0x43630000, float:227.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x04ac
                r5 = 1
                r11.mAimMethod = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.setAutoAim(r5)     // Catch:{ all -> 0x0045 }
                r11.setAim()     // Catch:{ all -> 0x0045 }
                android.graphics.Bitmap r5 = r11.mBackgroundImage     // Catch:{ Exception -> 0x06b1 }
                r5.recycle()     // Catch:{ Exception -> 0x06b1 }
            L_0x049b:
                com.selticeapps.funfunminigolflite.GameView r5 = com.selticeapps.funfunminigolflite.GameView.this     // Catch:{ all -> 0x0045 }
                android.content.res.Resources r5 = r5.getResources()     // Catch:{ all -> 0x0045 }
                r6 = 2130837511(0x7f020007, float:1.7279978E38)
                android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeResource(r5, r6)     // Catch:{ all -> 0x0045 }
                r11.mBackgroundImage = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x04ac:
                r5 = 1127284736(0x43310000, float:177.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x04e4
                r5 = 1134297088(0x439c0000, float:312.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x04e4
                r5 = 1127546880(0x43350000, float:181.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x04e4
                r5 = 1130561536(0x43630000, float:227.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x04e4
                r5 = 1
                r11.mAimMethod = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.setAutoAim(r5)     // Catch:{ all -> 0x0045 }
                r11.setAim()     // Catch:{ all -> 0x0045 }
                android.graphics.Bitmap r5 = r11.mBackgroundImage     // Catch:{ Exception -> 0x06ae }
                r5.recycle()     // Catch:{ Exception -> 0x06ae }
            L_0x04d3:
                com.selticeapps.funfunminigolflite.GameView r5 = com.selticeapps.funfunminigolflite.GameView.this     // Catch:{ all -> 0x0045 }
                android.content.res.Resources r5 = r5.getResources()     // Catch:{ all -> 0x0045 }
                r6 = 2130837512(0x7f020008, float:1.727998E38)
                android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeResource(r5, r6)     // Catch:{ all -> 0x0045 }
                r11.mBackgroundImage = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x04e4:
                r5 = 1134624768(0x43a10000, float:322.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0519
                r5 = 1138720768(0x43df8000, float:447.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0519
                r5 = 1127546880(0x43350000, float:181.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0519
                r5 = 1130561536(0x43630000, float:227.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0519
                r5 = 0
                r11.mAimMethod = r5     // Catch:{ all -> 0x0045 }
                r11.setAim()     // Catch:{ all -> 0x0045 }
                android.graphics.Bitmap r5 = r11.mBackgroundImage     // Catch:{ Exception -> 0x06ab }
                r5.recycle()     // Catch:{ Exception -> 0x06ab }
            L_0x0508:
                com.selticeapps.funfunminigolflite.GameView r5 = com.selticeapps.funfunminigolflite.GameView.this     // Catch:{ all -> 0x0045 }
                android.content.res.Resources r5 = r5.getResources()     // Catch:{ all -> 0x0045 }
                r6 = 2130837513(0x7f020009, float:1.7279982E38)
                android.graphics.Bitmap r5 = android.graphics.BitmapFactory.decodeResource(r5, r6)     // Catch:{ all -> 0x0045 }
                r11.mBackgroundImage = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0519:
                r5 = 1125711872(0x43190000, float:153.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x052e
                r5 = 1134985216(0x43a68000, float:333.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x052e
                int r5 = (r13 > r8 ? 1 : (r13 == r8 ? 0 : -1))
                if (r5 < 0) goto L_0x052e
                int r5 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
                if (r5 <= 0) goto L_0x0547
            L_0x052e:
                r5 = 1119617024(0x42bc0000, float:94.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0595
                r5 = 1137016832(0x43c58000, float:395.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0595
                r5 = 1112014848(0x42480000, float:50.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0595
                r5 = 1123680256(0x42fa0000, float:125.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0595
            L_0x0547:
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                if (r5 == r9) goto L_0x0563
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 10
                if (r5 == r6) goto L_0x0563
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 11
                if (r5 != r6) goto L_0x0590
            L_0x0563:
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 17
                if (r5 != r6) goto L_0x0589
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r11.mOnCourse = r5     // Catch:{ all -> 0x0045 }
                r5 = -1
                r11.HOLE_COUNT = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mShowingPre = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mSetRestartedHole = r5     // Catch:{ all -> 0x0045 }
                r5 = 1
                r11.mRandomMode = r5     // Catch:{ all -> 0x0045 }
                r11.resetRandomPlayed()     // Catch:{ all -> 0x0045 }
                r11.loadCoursePreRandom()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0589:
                r5 = 21
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0590:
                r11.showMessageMustBuy()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0595:
                r5 = 1107296256(0x42000000, float:32.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x05af
                r5 = 1125122048(0x43100000, float:144.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x05af
                int r5 = (r13 > r8 ? 1 : (r13 == r8 ? 0 : -1))
                if (r5 < 0) goto L_0x05af
                int r5 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
                if (r5 > 0) goto L_0x05af
                r5 = 1
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x05af:
                r5 = 1135247360(0x43aa8000, float:341.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                r5 = 1138982912(0x43e38000, float:455.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                int r5 = (r13 > r8 ? 1 : (r13 == r8 ? 0 : -1))
                if (r5 < 0) goto L_0x0030
                int r5 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
                if (r5 > 0) goto L_0x0030
                r5 = 1
                r11.mLoadMarket = r5     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x05ca:
                int r5 = r11.mGameMode     // Catch:{ all -> 0x0045 }
                r6 = 21
                if (r5 != r6) goto L_0x0030
                r5 = 1119617024(0x42bc0000, float:94.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0613
                r5 = 1137016832(0x43c58000, float:395.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0613
                r5 = 1112014848(0x42480000, float:50.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0613
                r5 = 1123680256(0x42fa0000, float:125.0)
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0613
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                if (r5 == r9) goto L_0x0605
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 10
                if (r5 == r6) goto L_0x0605
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 11
                if (r5 != r6) goto L_0x0030
            L_0x0605:
                r5 = 0
                r11.mRandomMode = r5     // Catch:{ all -> 0x0045 }
                r5 = -1
                r11.playSingleHole = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mRestartCount = r5     // Catch:{ all -> 0x0045 }
                r11.loadTheCourse()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0613:
                r5 = 1129119744(0x434d0000, float:205.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x0651
                r5 = 1137246208(0x43c90000, float:402.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x0651
                int r5 = (r13 > r8 ? 1 : (r13 == r8 ? 0 : -1))
                if (r5 < 0) goto L_0x0651
                int r5 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
                if (r5 > 0) goto L_0x0651
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                if (r5 == r9) goto L_0x0643
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 10
                if (r5 == r6) goto L_0x0643
                int[] r5 = r11.gameModesVis     // Catch:{ all -> 0x0045 }
                int r6 = r11.mOnCourse     // Catch:{ all -> 0x0045 }
                r5 = r5[r6]     // Catch:{ all -> 0x0045 }
                r6 = 11
                if (r5 != r6) goto L_0x0030
            L_0x0643:
                r5 = 0
                r11.mRandomMode = r5     // Catch:{ all -> 0x0045 }
                r5 = -1
                r11.playSingleHole = r5     // Catch:{ all -> 0x0045 }
                r5 = 0
                r11.mRestartCount = r5     // Catch:{ all -> 0x0045 }
                r11.loadTheCourse()     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x0651:
                r5 = 1118175232(0x42a60000, float:83.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x066f
                r5 = 1128595456(0x43450000, float:197.0)
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x066f
                int r5 = (r13 > r8 ? 1 : (r13 == r8 ? 0 : -1))
                if (r5 < 0) goto L_0x066f
                int r5 = (r13 > r10 ? 1 : (r13 == r10 ? 0 : -1))
                if (r5 > 0) goto L_0x066f
                r5 = 1
                r11.mWentBack = r5     // Catch:{ all -> 0x0045 }
                r5 = 18
                r11.setGameMode(r5)     // Catch:{ all -> 0x0045 }
                goto L_0x0030
            L_0x066f:
                r0 = 0
            L_0x0670:
                r5 = 9
                if (r0 >= r5) goto L_0x0030
                int[] r5 = r11.DHolePosX     // Catch:{ all -> 0x0045 }
                r5 = r5[r0]     // Catch:{ all -> 0x0045 }
                float r5 = (float) r5     // Catch:{ all -> 0x0045 }
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x06a8
                int[] r5 = r11.DHolePosX     // Catch:{ all -> 0x0045 }
                r5 = r5[r0]     // Catch:{ all -> 0x0045 }
                int r6 = r11.btnHoleWidth     // Catch:{ all -> 0x0045 }
                int r5 = r5 + r6
                float r5 = (float) r5     // Catch:{ all -> 0x0045 }
                int r5 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x06a8
                int[] r5 = r11.DHolePosY     // Catch:{ all -> 0x0045 }
                r5 = r5[r0]     // Catch:{ all -> 0x0045 }
                float r5 = (float) r5     // Catch:{ all -> 0x0045 }
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 < 0) goto L_0x06a8
                int[] r5 = r11.DHolePosY     // Catch:{ all -> 0x0045 }
                r5 = r5[r0]     // Catch:{ all -> 0x0045 }
                int r6 = r11.btnHoleHeight     // Catch:{ all -> 0x0045 }
                int r5 = r5 + r6
                float r5 = (float) r5     // Catch:{ all -> 0x0045 }
                int r5 = (r13 > r5 ? 1 : (r13 == r5 ? 0 : -1))
                if (r5 > 0) goto L_0x06a8
                r5 = 1
                r11.mRandomMode = r5     // Catch:{ all -> 0x0045 }
                int r5 = r0 + 1
                r11.playSingleHole = r5     // Catch:{ all -> 0x0045 }
                r11.loadTheCourse()     // Catch:{ all -> 0x0045 }
            L_0x06a8:
                int r0 = r0 + 1
                goto L_0x0670
            L_0x06ab:
                r5 = move-exception
                goto L_0x0508
            L_0x06ae:
                r5 = move-exception
                goto L_0x04d3
            L_0x06b1:
                r5 = move-exception
                goto L_0x049b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.selticeapps.funfunminigolflite.GameView.GameThread.doMakeMove(float, float):void");
        }

        public void loadTheCourse() {
            synchronized (this.mSurfaceHolder) {
                this.mOnCourse = this.gameModesVis[this.mOnCourse];
                this.HOLE_COUNT = 18;
                this.mShowingPre = true;
                this.mSetRestartedHole = false;
                if (this.mOnCourse == 1) {
                    loadCourseOnePre();
                } else if (this.mOnCourse == 2) {
                    loadCourseTwoPre();
                } else if (this.mOnCourse == 3) {
                    loadCourseThreePre();
                } else {
                    this.HOLE_COUNT = SOUND_OB;
                    loadCoursePre();
                }
            }
        }

        public void setFXYStandard(float x, float y, boolean mOK) {
            synchronized (this.mSurfaceHolder) {
                if (!this.mIsSwinging && !this.mBallMoving) {
                    if (mOK) {
                        this.d = (int) Math.sqrt((double) (((x - ((float) this.ballX)) * (x - ((float) this.ballX))) + ((y - ((float) this.ballY)) * (y - ((float) this.ballY)))));
                        this.theta = (Math.atan2((double) (x - ((float) this.ballX)), (double) (((float) this.ballY) - y)) * 180.0d) / 3.141592653589793d;
                        this.lastAimX = x;
                        this.lastAimY = y;
                    } else {
                        this.d = (int) Math.sqrt((double) (((this.lastAimX - ((float) this.ballX)) * (this.lastAimX - ((float) this.ballX))) + ((this.lastAimY - ((float) this.ballY)) * (this.lastAimY - ((float) this.ballY)))));
                        this.theta = (Math.atan2((double) (this.lastAimX - ((float) this.ballX)), (double) (((float) this.ballY) - this.lastAimY)) * 180.0d) / 3.141592653589793d;
                    }
                    this.x1 = this.ballX - 1;
                    this.x2 = this.ballX;
                    this.x3 = this.ballX + 1;
                    this.y1 = this.ballY;
                    this.y2 = this.ballY;
                    this.y3 = this.ballY;
                    resetArrowMatrix();
                }
            }
        }

        public void resetArrowMatrix() {
            this.matrix.reset();
            this.d = 60;
            this.matrix.setRotate((float) this.theta, (float) this.ballX, (float) this.ballY);
            this.path.reset();
            this.path.moveTo((float) this.x1, (float) this.y1);
            this.path.lineTo((float) (this.x1 - 2), (float) ((this.y2 - this.d) + SOUND_OB));
            this.path.lineTo((float) (this.x1 - SOUND_BALLINSAND), (float) ((this.y2 - this.d) + SOUND_OB));
            this.path.lineTo((float) this.x2, (float) (this.y2 - this.d));
            this.path.lineTo((float) (this.x3 + SOUND_BALLINSAND), (float) ((this.y2 - this.d) + SOUND_OB));
            this.path.lineTo((float) (this.x3 + 2), (float) ((this.y2 - this.d) + SOUND_OB));
            this.path.lineTo((float) this.x3, (float) this.y3);
            this.path.close();
            this.path.transform(this.matrix, null);
        }

        public void pause() {
            synchronized (this.mSurfaceHolder) {
                setPaused(true, true);
            }
        }

        public void saveHighScore() {
            synchronized (this.mSurfaceHolder) {
                checkProfile();
            }
        }

        public void clearHighScores() {
            synchronized (this.mSurfaceHolder) {
                if (this.mGameMode == 18 || this.mGameMode == 21) {
                    new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle("Warning").setMessage("Are you sure you want to clear this best score, flags and trophies?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(GameThread.SAVE_PREFS_NAME, 0).edit();
                            editor.putInt("COURSEBEST_" + GameThread.this.gameModesVis[GameThread.this.mOnCourse], 50000);
                            editor.putInt("COURSEPERFECT_" + GameThread.this.gameModesVis[GameThread.this.mOnCourse], 0);
                            for (int i = 0; i < 18; i++) {
                                if (!GameThread.this.mIsLite) {
                                    editor.putInt("HOLESTARCNT_" + GameThread.this.gameModesVis[GameThread.this.mOnCourse] + "_" + i, 0);
                                }
                                GameThread.this.OHoleStarCnt[GameThread.this.gameModesVis[GameThread.this.mOnCourse]][i] = 0;
                                if (i < GameThread.SOUND_OB) {
                                    GameThread.this.DHoleStarCnt[GameThread.this.gameModesVis[GameThread.this.mOnCourse]][i] = 0;
                                }
                            }
                            GameThread.this.courseStars[GameThread.this.gameModesVis[GameThread.this.mOnCourse]] = 0;
                            GameThread.this.courseFlagTrophy[GameThread.this.gameModesVis[GameThread.this.mOnCourse]] = 0;
                            GameThread.this.courseParTrophy[GameThread.this.gameModesVis[GameThread.this.mOnCourse]] = 0;
                            editor.commit();
                            GameThread.this.mShowingMessage = false;
                            GameThread.this.mCourseBest = GameThread.this.getCourseBest(GameThread.this.gameModesVis[GameThread.this.mOnCourse]);
                        }
                    }).setNeutralButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.mShowingMessage = false;
                        }
                    }).show();
                } else {
                    new AlertDialog.Builder(GameView.this.mContext).setIcon(17301543).setTitle("Warning").setMessage("Are you sure you want to permanently clear these high scores?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = GameView.this.mContext.getSharedPreferences(GameThread.SAVE_PREFS_NAME, 0).edit();
                            editor.putInt("COURSEBEST_" + GameThread.this.gameModesVis[GameThread.this.mOnCourse], 50000);
                            editor.commit();
                            GameThread.this.mShowingMessage = false;
                            GameThread.this.myDbHelper.clearScores(GameThread.this.mOnCourse);
                            GameThread.this.loadHighscores();
                            GameThread.this.setGameMode(4);
                        }
                    }).setNeutralButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            GameThread.this.mShowingMessage = false;
                        }
                    }).show();
                }
            }
        }

        public void setPaused(boolean which, boolean showmsg) {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void moveFXY(float x, float y, boolean mOK) {
            synchronized (this.mSurfaceHolder) {
                if (this.mAimMethod == 0) {
                    if (!this.mIsSwinging && !this.mBallMoving && this.startY < 240) {
                        this.d = (int) Math.sqrt((double) (((x - ((float) this.startX)) * (x - ((float) this.startX))) + ((y - ((float) this.startY)) * (y - ((float) this.startY)))));
                        this.theta = (Math.atan2((double) (x - ((float) this.startX)), (double) (((float) this.startY) - y)) * 180.0d) / 3.141592653589793d;
                    }
                } else if (this.mAiming) {
                    setFXYStandard(x, y, true);
                } else if (y >= 270.0f && y <= 320.0f) {
                    if (this.mSettingPower && x >= 140.0f && x <= 395.0f) {
                        if (x < 220.0f) {
                            this.mPowerLevel = 0;
                        } else if (x > 365.0f) {
                            this.mPowerLevel = 150;
                        } else {
                            this.mPowerLevel = (int) (x - 220.0f);
                        }
                    }
                }
            }
        }

        public void setFXY(float x, float y, boolean mOK) {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void setStartXY(int x, int y) {
            synchronized (this.mSurfaceHolder) {
                if (!this.mIsSwinging) {
                    this.startX = x;
                    this.startY = y;
                }
            }
        }

        public void setEndXY(float x, float y) {
            synchronized (this.mSurfaceHolder) {
                if (this.mAimMethod != 0) {
                    this.mAiming = false;
                    this.mSettingPower = false;
                    this.mSwinging = false;
                } else if (this.mStarted && !this.mBallMoving && !this.mInCup) {
                    this.d = (int) Math.sqrt((double) (((x - ((float) this.startX)) * (x - ((float) this.startX))) + ((y - ((float) this.startY)) * (y - ((float) this.startY)))));
                    this.theta = (Math.atan2((double) (x - ((float) this.startX)), (double) (((float) this.startY) - y)) * 180.0d) / 3.141592653589793d;
                    this.mStarted = false;
                    this.mIsSwinging = false;
                    this.mBallMoving = true;
                    this.ballRealX = (double) this.ballX;
                    this.ballRealY = (double) this.ballY;
                    this.mBallOriginalX = this.ballX;
                    this.mBallOriginalY = this.ballY;
                    this.mAPower = ((double) this.d) * 0.05d;
                    if (this.mAPower > 15.0d) {
                        this.mAPower = 15.0d;
                    }
                    this.mResistanceX = 0.08d;
                    this.mResistanceY = 0.08d;
                    this.mVelX = this.mAPower;
                    this.mVelY = this.mAPower;
                    this.dx = Math.sin((this.theta / 180.0d) * 3.141592653589793d) * this.mAPower;
                    this.dy = (-Math.cos((this.theta / 180.0d) * 3.141592653589793d)) * this.mAPower;
                    this.mDelay = 10;
                    this.mStrokes++;
                    makeScoreStr();
                    playSound(3);
                }
            }
        }

        public void makeScoreStr() {
            synchronized (this.mSurfaceHolder) {
                if (this.mRandomMode) {
                    this.mStrokeStr = "Par: " + this.mSPar + "   Strokes: " + this.mStrokes;
                } else {
                    this.mStrokeStr = "Hole: " + this.mOnHole + "   Par: " + this.mSPar + "   Strokes: " + this.mStrokes;
                }
            }
        }

        private void doDrawNoResize(Canvas canvas) {
            synchronized (this.mSurfaceHolder) {
                if (this.mGameMode == 3) {
                    canvas.drawRect(0.0f, 0.0f, 480.0f, 320.0f, this.blackpaint);
                    canvas.drawBitmap(this.bmpHole, 0.0f, 0.0f, (Paint) null);
                    if (this.mAimMethod == 0) {
                        canvas.drawText(this.mStrokeStr, 240.0f, 305.0f, this.blacktextpaint2);
                    } else {
                        canvas.drawText(this.mStrokeStr, 10.0f, 305.0f, this.blacktextpaint2b2);
                        canvas.drawBitmap(this.bmpSwing1, 220.0f, 282.0f, (Paint) null);
                        canvas.drawRect((float) (this.mPowerLevel + 220), 282.0f, (float) (this.mPowerLevel + 225), 312.0f, this.whitepaint);
                    }
                    if (this.MoverCount > 0) {
                        this.b = 0;
                        while (this.b < this.MoverCount) {
                            canvas.drawBitmap(this.Tiles[this.Mover[this.b].getBlocktype()], (float) this.Mover[this.b].getX(), (float) this.Mover[this.b].getY(), (Paint) null);
                            this.b++;
                        }
                    }
                    if (!this.mBelowTrees && !this.mBelowWater) {
                        if (this.mInCup) {
                            canvas.drawCircle((float) this.mHoleXs[this.mOnHole], (float) this.mHoleYs[this.mOnHole], (float) this.ballRad, this.ballpaint);
                        } else {
                            canvas.drawCircle((float) this.ballX, (float) this.ballY, (float) this.ballRad, this.ballpaint);
                        }
                    }
                    if (this.mOnCourse == 1) {
                        if (this.HoleTopX[this.mOnHole] >= 0) {
                            canvas.drawBitmap(this.bmpHoleTop, (float) this.HoleTopX[this.mOnHole], (float) this.HoleTopY[this.mOnHole], (Paint) null);
                        }
                        canvas.drawBitmap(this.bmpPause, 438.0f, 0.0f, (Paint) null);
                    }
                    if (!this.mInCup || !this.mShowCup) {
                        if (!this.mBallMoving && this.mAimMethod == 1 && (this.mAutoAim || (!this.mAutoAim && this.mAimSet))) {
                            canvas.drawPath(this.path, this.goldpaint);
                        }
                        if (!this.mIsLite) {
                            if (this.mStrokesPlus < this.mPars[this.mOnHole]) {
                                canvas.drawBitmap(this.bmpStars[3], 1.0f, 1.0f, (Paint) null);
                            } else if (this.mStrokesPlus == this.mPars[this.mOnHole]) {
                                canvas.drawBitmap(this.bmpStars[2], 1.0f, 1.0f, (Paint) null);
                            } else if (this.mStrokesPlus == this.mPars[this.mOnHole] + 1) {
                                canvas.drawBitmap(this.bmpStars[1], 1.0f, 1.0f, (Paint) null);
                            } else {
                                canvas.drawBitmap(this.bmpStars[0], 1.0f, 1.0f, (Paint) null);
                            }
                        }
                    } else {
                        canvas.drawCircle((float) this.mHoleXs[this.mOnHole], (float) this.mHoleYs[this.mOnHole], (float) this.ballRad, this.whitepaint);
                        canvas.drawBitmap(this.bmpIncup, 0.0f, 0.0f, (Paint) null);
                        if (!this.mIsLite) {
                            try {
                                canvas.drawBitmap(this.bmpStars[this.starCount], 347.0f, 45.0f, (Paint) null);
                            } catch (Exception e) {
                                Exception exc = e;
                                canvas.drawBitmap(this.bmpStars[0], 347.0f, 45.0f, (Paint) null);
                            }
                        }
                        if (this.mRandomMode) {
                            canvas.drawText(mSNA, (float) this.DRAW_INCUP_TEXT_ONHOLE_X, (float) this.DRAW_INCUP_TEXT_ONHOLE_Y, this.blacktextpaint3);
                            canvas.drawText(this.mSPar, (float) this.DRAW_INCUP_TEXT_PAR_X, (float) this.DRAW_INCUP_TEXT_PAR_Y, this.blacktextpaint3);
                            canvas.drawText(this.mSStrokes, (float) this.DRAW_INCUP_TEXT_STROKES_X, (float) this.DRAW_INCUP_TEXT_STROKES_Y, this.blacktextpaint3);
                            canvas.drawText(mSNA, (float) this.DRAW_INCUP_TEXT_PM_X, (float) this.DRAW_INCUP_TEXT_PM_Y, this.blacktextpaint3);
                            canvas.drawText(mSNA, (float) this.DRAW_INCUP_TEXT_CT_X, (float) this.DRAW_INCUP_TEXT_CT_Y, this.blacktextpaint3);
                        } else {
                            canvas.drawText(this.mSOnHole, (float) this.DRAW_INCUP_TEXT_ONHOLE_X, (float) this.DRAW_INCUP_TEXT_ONHOLE_Y, this.blacktextpaint3);
                            canvas.drawText(this.mSPar, (float) this.DRAW_INCUP_TEXT_PAR_X, (float) this.DRAW_INCUP_TEXT_PAR_Y, this.blacktextpaint3);
                            canvas.drawText(this.mSStrokes, (float) this.DRAW_INCUP_TEXT_STROKES_X, (float) this.DRAW_INCUP_TEXT_STROKES_Y, this.blacktextpaint3);
                            if (this.mMyCoursePlusMinus > 0) {
                                canvas.drawText(String.valueOf(Integer.toString(this.mMyCourseTotal)) + " (+" + Integer.toString(this.mMyCoursePlusMinus) + ")", (float) this.DRAW_INCUP_TEXT_PM_X, (float) this.DRAW_INCUP_TEXT_PM_Y, this.blacktextpaint3);
                            } else if (this.mMyCoursePlusMinus == 0) {
                                canvas.drawText(String.valueOf(Integer.toString(this.mMyCourseTotal)) + " (Even)", (float) this.DRAW_INCUP_TEXT_PM_X, (float) this.DRAW_INCUP_TEXT_PM_Y, this.blacktextpaint3);
                            } else {
                                canvas.drawText(String.valueOf(Integer.toString(this.mMyCourseTotal)) + " (" + Integer.toString(this.mMyCoursePlusMinus) + ")", (float) this.DRAW_INCUP_TEXT_PM_X, (float) this.DRAW_INCUP_TEXT_PM_Y, this.blacktextpaint3);
                            }
                            canvas.drawText(Integer.toString(this.COURSE_TOTAL), (float) this.DRAW_INCUP_TEXT_CT_X, (float) this.DRAW_INCUP_TEXT_CT_Y, this.blacktextpaint3);
                        }
                    }
                } else if (this.mGameMode == 1) {
                    canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                } else if (this.mGameMode == 21) {
                    if (!this.mShowingPre) {
                        canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                        canvas.drawText(this.gameModesS[this.gameModesVis[this.mOnCourse]], 242.0f, 80.0f, this.whitepaintc);
                        if (this.mCourseBest != 50000) {
                            canvas.drawText(this.mBestStr, 242.0f, 100.0f, this.whitepaintd);
                        }
                        if (!this.mIsLite) {
                            for (int j = 0; j < SOUND_OB; j++) {
                                canvas.drawBitmap(this.bmpStars[this.DHoleStarCnt[this.gameModesVis[this.mOnCourse]][j]], (float) this.DHoleStarX[j], (float) this.DHoleStarY[j], (Paint) null);
                            }
                            canvas.drawBitmap(this.bmpFlagTrophy[this.courseFlagTrophy[this.gameModesVis[this.mOnCourse]]], 8.0f, 51.0f, (Paint) null);
                            canvas.drawBitmap(this.bmpParTrophy[this.courseParTrophy[this.gameModesVis[this.mOnCourse]]], 402.0f, 51.0f, (Paint) null);
                        }
                    } else {
                        canvas.drawRect(0.0f, 0.0f, 480.0f, 320.0f, this.blackpaint);
                    }
                } else if (this.mGameMode == 18) {
                    if (!this.mShowingPre) {
                        canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                        canvas.drawText(this.gameModesS[this.gameModesVis[this.mOnCourse]], 242.0f, 80.0f, this.whitepaintc);
                        if (this.mCourseBest != 50000) {
                            canvas.drawText(this.mBestStr, 242.0f, 100.0f, this.whitepaintd);
                        }
                        if (this.mOnCourse < 17 && !this.mIsLite) {
                            canvas.drawBitmap(this.bmpFlagTrophy[this.courseFlagTrophy[this.gameModesVis[this.mOnCourse]] + 2], 153.0f, 125.0f, (Paint) null);
                            canvas.drawBitmap(this.bmpParTrophy[this.courseParTrophy[this.gameModesVis[this.mOnCourse]] + 2], 308.0f, 125.0f, (Paint) null);
                            canvas.drawBitmap(this.bmpStars[this.courseStars[this.gameModesVis[this.mOnCourse]]], 223.0f, 125.0f, (Paint) null);
                        }
                    } else {
                        canvas.drawRect(0.0f, 0.0f, 480.0f, 320.0f, this.blackpaint);
                    }
                } else if (this.mGameMode == 19) {
                    canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                } else if (this.mGameMode == 17 || this.mGameMode == 23) {
                    canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                } else if (this.mGameMode == 4 || this.mGameMode == 5) {
                    canvas.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, (Paint) null);
                    int newcounter = 0;
                    int j2 = 2;
                    for (int counter = 0; counter < 5; counter++) {
                        newcounter += 27;
                        try {
                            this.HIGHSCORE_DATE_X = 220;
                            this.HIGHSCORE_DATE_Y = 65;
                            if (this.hs_shifterstimes[counter] != "") {
                                canvas.drawText(this.hs_shifterstimes[counter], (float) (this.HIGHSCORE_DATE_X - 100), (float) (this.HIGHSCORE_DATE_Y + newcounter), this.whitepaint);
                                canvas.drawText(String.valueOf(this.hs_normaltimes[counter]) + " " + this.hs_normaltimes2[counter], (float) this.HIGHSCORE_DATE_X, (float) (this.HIGHSCORE_DATE_Y + newcounter), this.whitepaint);
                                canvas.drawText(this.hs_players[counter], (float) (this.HIGHSCORE_DATE_X + 85), (float) (this.HIGHSCORE_DATE_Y + newcounter), this.whitepaint);
                            }
                        } catch (Exception e2) {
                        }
                        j2++;
                    }
                }
            }
        }
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.thread = new GameThread(holder, context, new Handler() {
            public void handleMessage(Message m) {
            }
        });
        setFocusable(true);
    }

    public GameThread getThread() {
        return this.thread;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.thread.setSurfaceSize((double) width, (double) height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.thread.start();
        } catch (Exception e) {
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this.thread.setRunning(false);
        while (retry) {
            try {
                this.thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }
}
