package com.selticeapps.funfunminigolflite;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

class Instructions {
    private static final String GAME_ASSET_INSTRUCTIONS = "INSTRUCTIONS";

    interface OnInstructionsOK {
        void onInstructionsOK();
    }

    Instructions() {
    }

    static boolean show(final Context activity) {
        AlertDialog.Builder alertbuilder = new AlertDialog.Builder(activity);
        alertbuilder.setTitle((int) R.string.instructions_title);
        alertbuilder.setCancelable(false);
        alertbuilder.setPositiveButton((int) R.string.instructions_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (activity instanceof OnInstructionsOK) {
                    ((OnInstructionsOK) activity).onInstructionsOK();
                }
            }
        });
        alertbuilder.setMessage(loadInstructions(activity));
        alertbuilder.create().show();
        return false;
    }

    private static CharSequence loadInstructions(Context activity) {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(activity.getAssets().open(GAME_ASSET_INSTRUCTIONS)));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        closeFileStream(in2);
                        return buffer;
                    }
                    buffer.append(line).append(10);
                }
            } catch (IOException e) {
                in = in2;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeFileStream(in);
                throw th;
            }
        } catch (IOException e2) {
            closeFileStream(in);
            return "";
        } catch (Throwable th2) {
            th = th2;
            closeFileStream(in);
            throw th;
        }
    }

    private static void closeFileStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }
}
