package com.selticeapps.funfunminigolflite;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;

class UserAgreement {
    private static final String GAME_ASSET_EULA = "EULA";
    private static final String GAME_PREFERENCES_EULA = "eula400";
    private static final String GAME_PREFERENCE_EULA_ACCEPTED = "eula400.accepted";

    interface OnEulaAgreement {
        void onEulaAgreement();
    }

    UserAgreement() {
    }

    static boolean show(final Activity activity, boolean forceShow) {
        final SharedPreferences preferences = activity.getSharedPreferences(GAME_PREFERENCES_EULA, 0);
        if (!preferences.getBoolean(GAME_PREFERENCE_EULA_ACCEPTED, false) || forceShow) {
            AlertDialog.Builder alertbuilder = new AlertDialog.Builder(activity);
            alertbuilder.setTitle((int) R.string.eula_title);
            alertbuilder.setCancelable(true);
            alertbuilder.setPositiveButton((int) R.string.eula_accept, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    UserAgreement.accept(preferences);
                    Updates.show(activity, false);
                    if (activity instanceof OnEulaAgreement) {
                        ((OnEulaAgreement) activity).onEulaAgreement();
                    }
                }
            });
            alertbuilder.setNegativeButton((int) R.string.eula_refuse, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    UserAgreement.refuse(activity);
                }
            });
            alertbuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    UserAgreement.refuse(activity);
                }
            });
            alertbuilder.setMessage(loadEula(activity));
            alertbuilder.create().show();
            return false;
        }
        Updates.show(activity, false);
        return true;
    }

    private static CharSequence loadEula(Activity activity) {
        BufferedReader in = null;
        try {
            BufferedReader in2 = new BufferedReader(new InputStreamReader(activity.getAssets().open(GAME_ASSET_EULA)));
            try {
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        closeFileStream(in2);
                        return buffer;
                    }
                    buffer.append(line).append(10);
                }
            } catch (IOException e) {
                in = in2;
            } catch (Throwable th) {
                th = th;
                in = in2;
                closeFileStream(in);
                throw th;
            }
        } catch (IOException e2) {
            closeFileStream(in);
            return "";
        } catch (Throwable th2) {
            th = th2;
            closeFileStream(in);
            throw th;
        }
    }

    private static void closeFileStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static void accept(SharedPreferences preferences) {
        preferences.edit().putBoolean(GAME_PREFERENCE_EULA_ACCEPTED, true).commit();
    }

    /* access modifiers changed from: private */
    public static void refuse(Activity activity) {
        activity.finish();
    }
}
