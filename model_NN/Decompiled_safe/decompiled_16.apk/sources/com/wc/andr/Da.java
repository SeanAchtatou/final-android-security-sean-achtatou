package com.wc.andr;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import com.wc.andr.utcl.A;
import com.wc.andr.utcl.C0002a;

public class Da extends Activity {
    public int a = 0;
    private C0002a b;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        this.b = new C0002a(this);
        String stringExtra = getIntent().getStringExtra(A.Y);
        if (stringExtra == null) {
            finish();
        }
        if (A.Z.equals(stringExtra)) {
            this.b.a();
        } else if ("showlist".equals(stringExtra)) {
            this.b.b();
        }
        super.onCreate(bundle);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4 && keyEvent.getRepeatCount() == 0) {
            this.a = 1;
            this.b.c();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.a == 0) {
            finish();
        }
        this.a = 0;
        super.onStop();
    }
}
