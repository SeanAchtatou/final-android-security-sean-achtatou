package com.wc.andr.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.wc.andr.utcl.x;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;

public class c {
    private int a = 0;
    private int b = 0;
    private b c;
    private File d;
    private String e;

    public c(String str, File file) {
        try {
            if (!file.exists()) {
                file.mkdirs();
            }
            this.e = str;
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (httpURLConnection.getResponseCode() == 200) {
                this.b = httpURLConnection.getContentLength();
                httpURLConnection.disconnect();
                if (this.b <= 0) {
                    throw new RuntimeException("Unkown_size");
                }
                this.d = new File(file, b());
                return;
            }
            throw new RuntimeException("server_no_response");
        } catch (Exception e2) {
            throw new RuntimeException("don't_connection");
        }
    }

    public c(String str, File file, byte b2) {
        try {
            if (!file.exists()) {
                file.mkdirs();
            }
            this.e = str;
            this.d = new File(file, b());
        } catch (Exception e2) {
            throw new RuntimeException("don't_connection");
        }
    }

    public static int a(Context context, String str) {
        return context.getSharedPreferences("data", 0).getInt(str, 0);
    }

    public static int a(Context context, String str, Integer num) {
        int a2 = a(context, str) + 1;
        if (a2 > num.intValue()) {
            a2 = 1;
        }
        if (a2 == 10) {
            a2 = 11;
        }
        b(context, str, Integer.valueOf(a2));
        return a2;
    }

    public static void a(Context context, String str, String str2) {
        SharedPreferences.Editor b2 = b(context);
        b2.putString(str, str2);
        b2.commit();
    }

    public static SharedPreferences.Editor b(Context context) {
        return context.getSharedPreferences("data", 0).edit();
    }

    private String b() {
        String substring = this.e.substring(this.e.lastIndexOf(47) + 1);
        return (substring == null || "".equals(substring.trim())) ? UUID.randomUUID() + ".apk" : substring;
    }

    public static String b(Context context, String str) {
        return context.getSharedPreferences("data", 0).getString(str, null);
    }

    public static void b(Context context, String str, Integer num) {
        SharedPreferences.Editor b2 = b(context);
        b2.putInt(str, num.intValue());
        b2.commit();
    }

    public final int a() {
        return this.b;
    }

    public final int a(Context context) {
        try {
            this.c = new b(this, new URL(this.e), this.d);
            if (context != null && !x.a(context)) {
                this.c.a = 1024;
            }
            this.c.setPriority(7);
            this.c.start();
            boolean z = true;
            while (z) {
                Thread.sleep(900);
                z = false;
                if (this.c != null && !this.c.a()) {
                    z = true;
                }
                if (context != null && !x.b(context)) {
                    throw new Exception("file_down_fail");
                } else if (this.c != null && this.c.b() == -1) {
                    throw new Exception("file_down_fail");
                }
            }
            return this.a;
        } catch (Exception e2) {
            System.out.println("---" + "file_down_fail");
            throw new Exception("file_down_fail");
        }
    }

    public final int a(Context context, a aVar) {
        try {
            URL url = new URL(this.e);
            if (this.a < this.b) {
                this.c = new b(this, url, this.d);
                if (context != null && !x.a(context)) {
                    this.c.a = 1024;
                }
                this.c.setPriority(7);
                this.c.start();
            }
            boolean z = true;
            while (z) {
                Thread.sleep(900);
                z = false;
                if (this.c != null && !this.c.a()) {
                    z = true;
                }
                if (context != null && !x.b(context)) {
                    throw new Exception("file_down_fail");
                } else if (this.c != null && this.c.b() == -1) {
                    throw new Exception("file_down_fail");
                } else if (aVar != null) {
                    aVar.a(this.a);
                }
            }
            return this.a;
        } catch (Exception e2) {
            throw new Exception("file_down_fail");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.a += i;
    }
}
