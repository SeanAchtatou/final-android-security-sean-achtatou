package com.wc.andr.a;

import java.io.File;
import java.net.URL;

public final class b extends Thread {
    public int a = 512000;
    private File b;
    private URL c;
    private int d;
    private boolean e = false;
    private c f;

    public b(c cVar, URL url, File file) {
        this.f = cVar;
        this.c = url;
        this.b = file;
    }

    public final boolean a() {
        return this.e;
    }

    public final long b() {
        return (long) this.d;
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.net.HttpURLConnection] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            r6 = -1
            r2 = 0
            java.net.URL r0 = r8.c     // Catch:{ Exception -> 0x00bf, all -> 0x008e }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00bf, all -> 0x008e }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00bf, all -> 0x008e }
            r1 = 5000(0x1388, float:7.006E-42)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "Accept"
            java.lang.String r3 = "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "Accept-Language"
            java.lang.String r3 = "zh-CN"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "Referer"
            java.net.URL r3 = r8.c     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "Charset"
            java.lang.String r3 = "UTF-8"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "User-Agent"
            java.lang.String r3 = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.lang.String r1 = "Connection"
            java.lang.String r3 = "Keep-Alive"
            r0.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x00c3, all -> 0x00ab }
            int r1 = r8.a     // Catch:{ Exception -> 0x00c9, all -> 0x00b1 }
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x00c9, all -> 0x00b1 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c9, all -> 0x00b1 }
            java.io.File r5 = r8.b     // Catch:{ Exception -> 0x00c9, all -> 0x00b1 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x00c9, all -> 0x00b1 }
        L_0x0051:
            r2 = 0
            int r5 = r8.a     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            int r2 = r4.read(r1, r2, r5)     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            if (r2 == r6) goto L_0x007f
            r5 = 0
            r3.write(r1, r5, r2)     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            int r5 = r8.d     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            int r5 = r5 + r2
            r8.d = r5     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            com.wc.andr.a.c r5 = r8.f     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            r5.a(r2)     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            goto L_0x0051
        L_0x0069:
            r1 = move-exception
            r2 = r3
            r3 = r4
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x006f:
            r4 = -1
            r8.d = r4     // Catch:{ all -> 0x00ba }
            r0.printStackTrace()     // Catch:{ all -> 0x00ba }
            r2.close()     // Catch:{ Exception -> 0x009f }
        L_0x0078:
            r3.close()     // Catch:{ Exception -> 0x00a1 }
        L_0x007b:
            r1.disconnect()     // Catch:{ Exception -> 0x00a3 }
        L_0x007e:
            return
        L_0x007f:
            r1 = 1
            r8.e = r1     // Catch:{ Exception -> 0x0069, all -> 0x00b6 }
            r3.close()     // Catch:{ Exception -> 0x009b }
        L_0x0085:
            r4.close()     // Catch:{ Exception -> 0x009d }
        L_0x0088:
            r0.disconnect()     // Catch:{ Exception -> 0x008c }
            goto L_0x007e
        L_0x008c:
            r0 = move-exception
            goto L_0x007e
        L_0x008e:
            r0 = move-exception
            r3 = r2
            r4 = r2
        L_0x0091:
            r3.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x0094:
            r4.close()     // Catch:{ Exception -> 0x00a7 }
        L_0x0097:
            r2.disconnect()     // Catch:{ Exception -> 0x00a9 }
        L_0x009a:
            throw r0
        L_0x009b:
            r1 = move-exception
            goto L_0x0085
        L_0x009d:
            r1 = move-exception
            goto L_0x0088
        L_0x009f:
            r0 = move-exception
            goto L_0x0078
        L_0x00a1:
            r0 = move-exception
            goto L_0x007b
        L_0x00a3:
            r0 = move-exception
            goto L_0x007e
        L_0x00a5:
            r1 = move-exception
            goto L_0x0094
        L_0x00a7:
            r1 = move-exception
            goto L_0x0097
        L_0x00a9:
            r1 = move-exception
            goto L_0x009a
        L_0x00ab:
            r1 = move-exception
            r3 = r2
            r4 = r2
            r2 = r0
            r0 = r1
            goto L_0x0091
        L_0x00b1:
            r1 = move-exception
            r3 = r2
            r2 = r0
            r0 = r1
            goto L_0x0091
        L_0x00b6:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0091
        L_0x00ba:
            r0 = move-exception
            r4 = r3
            r3 = r2
            r2 = r1
            goto L_0x0091
        L_0x00bf:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x006f
        L_0x00c3:
            r1 = move-exception
            r3 = r2
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x006f
        L_0x00c9:
            r1 = move-exception
            r3 = r4
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.a.b.run():void");
    }
}
