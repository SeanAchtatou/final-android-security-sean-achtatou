package com.wc.andr;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import com.wc.andr.utcl.A;
import com.wc.andr.utcl.o;
import com.wc.andr.utcl.x;
import java.io.File;
import org.json.JSONObject;

final class a extends Handler {
    private /* synthetic */ Fs a;

    a(Fs fs) {
        this.a = fs;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                String string = message.getData().getString(A.D);
                String string2 = message.getData().getString(this.a.c);
                String string3 = message.getData().getString(this.a.d);
                int i = message.getData().getInt(A.O);
                SharedPreferences sharedPreferences = Fs.c(this.a).getSharedPreferences("data", 0);
                int i2 = sharedPreferences.getInt(this.a.e + string3, i);
                o unused = this.a.h;
                o.b(Fs.c(this.a), A.s + "..." + ((int) ((((float) i) / ((float) i2)) * 100.0f)) + "%", string, string3);
                if (i == i2) {
                    o unused2 = this.a.h;
                    o.c(Fs.c(this.a), string, this.a.f + string2.substring(string2.lastIndexOf("/") + 1), string3);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.remove(string3 + A.C);
                    edit.putString(A.E + string3, "1");
                    edit.commit();
                    File file = new File(this.a.f + string2.substring(string2.lastIndexOf("/") + 1));
                    if (!file.exists() || !x.b(this.a.getApplicationContext(), file.toString())) {
                        JSONObject jSONObject = new JSONObject();
                        try {
                            jSONObject.put(this.a.b, A.e);
                            jSONObject.put(this.a.a, "setuppck=" + string3 + "&operate=20");
                            new x(this.a.getApplication(), jSONObject.toString()).start();
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    } else {
                        JSONObject jSONObject2 = new JSONObject();
                        try {
                            jSONObject2.put(this.a.b, A.e);
                            jSONObject2.put(this.a.a, "setuppck=" + string3 + "&operate=2");
                            new x(this.a.getApplication(), jSONObject2.toString()).start();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        this.a.startActivity(x.a(file));
                        this.a.stopSelf();
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
