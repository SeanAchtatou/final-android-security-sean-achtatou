package com.wc.andr;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;
import com.wc.andr.a.c;
import com.wc.andr.utcl.A;
import com.wc.andr.utcl.o;
import com.wc.andr.utcl.x;
import java.io.File;

public class Fs extends Service {
    /* access modifiers changed from: private */
    public String a = "url";
    /* access modifiers changed from: private */
    public String b = "para";
    /* access modifiers changed from: private */
    public String c = "mUrl";
    /* access modifiers changed from: private */
    public String d = "setpck";
    /* access modifiers changed from: private */
    public String e = "max_downsize";
    /* access modifiers changed from: private */
    public String f = (A.l + "/");
    private File g = new File(A.l);
    /* access modifiers changed from: private */
    public o h = new o();
    /* access modifiers changed from: private */
    public Handler i = new a(this);

    static /* synthetic */ Context c(Fs fs) {
        return fs;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        String stringExtra = intent.getStringExtra("service_flag");
        if (stringExtra == null) {
            stopSelf();
        } else if (A.U.equals(stringExtra)) {
            String stringExtra2 = intent.getStringExtra(A.V);
            String stringExtra3 = intent.getStringExtra(A.S);
            String stringExtra4 = intent.getStringExtra(A.W);
            String str = this.f + stringExtra2.substring(stringExtra2.lastIndexOf("/") + 1);
            File file = new File(str);
            if (file.exists() && x.b(this, str)) {
                c.a(this, A.E + stringExtra3, "2");
                startActivity(x.a(file));
                stopSelf();
            } else if (!x.b(this)) {
                Toast.makeText(this, A.p, 1).show();
                o oVar = this.h;
                o.d(this, stringExtra4, stringExtra2, stringExtra3);
                stopSelf();
            } else {
                getApplicationContext().getPackageName();
                c.a(this, stringExtra3 + A.C, "1");
                new Thread(new b(this, stringExtra2, this.g, stringExtra3, stringExtra4)).start();
            }
        }
        return i3;
    }
}
