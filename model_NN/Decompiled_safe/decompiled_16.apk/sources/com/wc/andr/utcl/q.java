package com.wc.andr.utcl;

import android.os.Environment;
import java.io.File;
import java.util.Arrays;

public final class q {
    static String a = ".android/reqindex";
    public static String b = ".android/insindex";
    public static String c = ".android/devtime";
    public static String d = ".android/fornum";
    public static String e = ".android/savepicnum";
    public static String f = ".android/savelocknum";
    private static String g = (Environment.getExternalStorageDirectory() + "/");

    static {
        String str = ".android/notififile";
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0020 A[SYNTHETIC, Splitter:B:19:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.io.File r4) {
        /*
            r3 = 0
            r0 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x000f, all -> 0x001c }
            r2.<init>(r4)     // Catch:{ Exception -> 0x000f, all -> 0x001c }
            int r0 = r2.available()     // Catch:{ Exception -> 0x002a }
            r2.close()     // Catch:{ Exception -> 0x0024 }
        L_0x000e:
            return r0
        L_0x000f:
            r1 = move-exception
            r2 = r3
        L_0x0011:
            r1.printStackTrace()     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x000e
            r2.close()     // Catch:{ Exception -> 0x001a }
            goto L_0x000e
        L_0x001a:
            r1 = move-exception
            goto L_0x000e
        L_0x001c:
            r0 = move-exception
            r2 = r3
        L_0x001e:
            if (r2 == 0) goto L_0x0023
            r2.close()     // Catch:{ Exception -> 0x0026 }
        L_0x0023:
            throw r0
        L_0x0024:
            r1 = move-exception
            goto L_0x000e
        L_0x0026:
            r1 = move-exception
            goto L_0x0023
        L_0x0028:
            r0 = move-exception
            goto L_0x001e
        L_0x002a:
            r1 = move-exception
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.q.a(java.io.File):int");
    }

    public static String a() {
        return a(a);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041 A[SYNTHETIC, Splitter:B:13:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[SYNTHETIC, Splitter:B:27:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r5) {
        /*
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.wc.andr.utcl.q.g
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            r2 = 0
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            r1.<init>(r0)     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            boolean r1 = r1.exists()     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            if (r1 == 0) goto L_0x004d
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            r4.<init>(r0)     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x006d, all -> 0x005e }
            java.lang.String r0 = "UTF-8"
            r1.<init>(r4, r0)     // Catch:{ IOException -> 0x006d, all -> 0x005e }
        L_0x0030:
            int r0 = r1.read()     // Catch:{ IOException -> 0x003b }
            if (r0 < 0) goto L_0x004e
            char r0 = (char) r0     // Catch:{ IOException -> 0x003b }
            r3.append(r0)     // Catch:{ IOException -> 0x003b }
            goto L_0x0030
        L_0x003b:
            r0 = move-exception
        L_0x003c:
            r0.printStackTrace()     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0044:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = java.net.URLDecoder.decode(r0)
            return r0
        L_0x004d:
            r1 = r2
        L_0x004e:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ Exception -> 0x0054 }
            goto L_0x0044
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x0059:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r2 == 0) goto L_0x0064
            r2.close()     // Catch:{ Exception -> 0x0065 }
        L_0x0064:
            throw r0
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0064
        L_0x006a:
            r0 = move-exception
            r2 = r1
            goto L_0x005f
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.q.a(java.lang.String):java.lang.String");
    }

    public static void b() {
        c(a);
    }

    public static boolean b(String str) {
        return b(a, str);
    }

    public static boolean b(String str, String str2) {
        String a2 = a(str);
        if (!x.a(a2)) {
            if (Arrays.asList(a2.split(",")).contains(str2)) {
                return false;
            }
            str2 = a2 + "," + str2;
        }
        new q().a(str, str2);
        return true;
    }

    public static void c(String str) {
        new q().a(str, "");
    }

    public static void d(String str) {
        File file = new File(g + str);
        if (file.exists()) {
            file.delete();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004f A[SYNTHETIC, Splitter:B:19:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005b A[SYNTHETIC, Splitter:B:25:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File a(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            r2 = 0
            java.lang.String r1 = java.net.URLEncoder.encode(r7)
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0047 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0047 }
            r3.<init>()     // Catch:{ Exception -> 0x0047 }
            java.lang.String r4 = com.wc.andr.utcl.q.g     // Catch:{ Exception -> 0x0047 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0047 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0047 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0047 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0047 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0047 }
            java.lang.String r4 = r0.getParent()     // Catch:{ Exception -> 0x0047 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0047 }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x0047 }
            if (r4 != 0) goto L_0x002f
            r3.mkdirs()     // Catch:{ Exception -> 0x0047 }
        L_0x002f:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0067 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0067 }
            byte[] r1 = r1.getBytes()     // Catch:{ Exception -> 0x0069, all -> 0x0064 }
            r3.write(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0064 }
            r3.flush()     // Catch:{ Exception -> 0x0069, all -> 0x0064 }
            r3.close()     // Catch:{ Exception -> 0x0042 }
        L_0x0041:
            return r0
        L_0x0042:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0041
        L_0x0047:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x004a:
            r1.printStackTrace()     // Catch:{ all -> 0x0058 }
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ Exception -> 0x0053 }
            goto L_0x0041
        L_0x0053:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0041
        L_0x0058:
            r0 = move-exception
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()     // Catch:{ Exception -> 0x005f }
        L_0x005e:
            throw r0
        L_0x005f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005e
        L_0x0064:
            r0 = move-exception
            r2 = r3
            goto L_0x0059
        L_0x0067:
            r1 = move-exception
            goto L_0x004a
        L_0x0069:
            r1 = move-exception
            r2 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.q.a(java.lang.String, java.lang.String):java.io.File");
    }
}
