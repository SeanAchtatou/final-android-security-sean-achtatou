package com.wc.andr.utcl;

import android.content.Context;
import android.os.Environment;
import com.wc.andr.a.c;
import java.io.File;
import java.net.URLConnection;
import java.util.List;

public final class t {
    public static String a = (Environment.getExternalStorageDirectory() + "/.dsf");
    public static String b = "/.com3jsp";
    public static String c = "/.com3imgs";
    private URLConnection d = null;
    private String e = "www.gyswad.com";
    private String f = "http://www.gyswad.com:90/push";

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036 A[SYNTHETIC, Splitter:B:11:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004f A[SYNTHETIC, Splitter:B:23:0x004f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r6) {
        /*
            r5 = this;
            r2 = 0
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            r4.<init>(r6)     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.net.URLConnection r4 = r4.openConnection()     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            r5.d = r4     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.net.URLConnection r4 = r5.d     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            r4.getContent()     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.net.URLConnection r4 = r5.d     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            java.io.InputStream r4 = r4.getInputStream()     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            r0.<init>(r4)     // Catch:{ Exception -> 0x005b, all -> 0x004c }
            r1.<init>(r0)     // Catch:{ Exception -> 0x005b, all -> 0x004c }
        L_0x0026:
            java.lang.String r0 = r1.readLine()     // Catch:{ Exception -> 0x0030 }
            if (r0 == 0) goto L_0x003e
            r3.append(r0)     // Catch:{ Exception -> 0x0030 }
            goto L_0x0026
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            r0.printStackTrace()     // Catch:{ all -> 0x0058 }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0039:
            java.lang.String r0 = r3.toString()
            return r0
        L_0x003e:
            r1.close()     // Catch:{ Exception -> 0x0042 }
            goto L_0x0039
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0039
        L_0x0047:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0039
        L_0x004c:
            r0 = move-exception
        L_0x004d:
            if (r2 == 0) goto L_0x0052
            r2.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0052:
            throw r0
        L_0x0053:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0052
        L_0x0058:
            r0 = move-exception
            r2 = r1
            goto L_0x004d
        L_0x005b:
            r0 = move-exception
            r1 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.t.a(java.lang.String):java.lang.String");
    }

    private void a(List list, Context context, String str) {
        boolean z;
        File file = new File(a + c);
        if (!file.exists()) {
            file.mkdirs();
        }
        int size = list.size();
        String str2 = null;
        int i = 0;
        boolean z2 = false;
        while (i < size) {
            String str3 = (String) list.get(i);
            String str4 = this.f + "/" + str3;
            try {
                File file2 = new File(a + c + "/" + str3);
                if (x.b(file2.toString()) == null) {
                    if (str == null || !str.equals(str3)) {
                        boolean equals = str3.replaceAll("[\\d.]+", "").equals("imgjpg");
                        if (str == null || !equals || z2) {
                            new u(this, str4, file2, context).start();
                        } else {
                            z = true;
                            i++;
                            z2 = z;
                            str2 = str3;
                        }
                    } else {
                        str3 = str2;
                        z = z2;
                        i++;
                        z2 = z;
                        str2 = str3;
                    }
                }
                str3 = str2;
                z = z2;
            } catch (Exception e2) {
                e2.printStackTrace();
                str3 = str2;
                z = z2;
            }
            i++;
            z2 = z;
            str2 = str3;
        }
        if (str2 != null) {
            try {
                new c(this.f + "/" + str2, new File(a + c + "/" + str2).getParentFile(), (byte) 0).a(context);
            } catch (Exception e3) {
            }
        }
    }

    public final void a(Context context, String str, String str2) {
        a(context, str, str2, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0196  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r9, java.lang.String r10, java.lang.String r11, java.lang.String r12) {
        /*
            r8 = this;
            java.lang.String r2 = r8.a(r10)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.String r0 = r8.e
            boolean r0 = r2.contains(r0)
            if (r0 == 0) goto L_0x0053
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r8.f
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String[] r1 = r2.split(r0)
            r0 = 1
        L_0x002b:
            int r4 = r1.length
            if (r0 >= r4) goto L_0x0053
            r4 = r1[r0]
            java.lang.String r5 = "jpg"
            boolean r4 = r4.contains(r5)
            if (r4 != 0) goto L_0x0042
            r4 = r1[r0]
            java.lang.String r5 = "png"
            boolean r4 = r4.contains(r5)
            if (r4 == 0) goto L_0x0050
        L_0x0042:
            r4 = r1[r0]
            java.lang.String r5 = "\""
            java.lang.String[] r4 = r4.split(r5)
            r5 = 0
            r4 = r4[r5]
            r3.add(r4)
        L_0x0050:
            int r0 = r0 + 1
            goto L_0x002b
        L_0x0053:
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r4 = ".jsp"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            r5.<init>()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = com.wc.andr.utcl.t.b     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0189 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0189 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            r6.<init>()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r7 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r7 = com.wc.andr.utcl.t.b     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r7 = "/"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0189 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0189 }
            boolean r0 = r5.exists()     // Catch:{ Exception -> 0x0189 }
            if (r0 == 0) goto L_0x00ae
            r5.delete()     // Catch:{ Exception -> 0x0189 }
        L_0x00ae:
            boolean r0 = r4.exists()     // Catch:{ Exception -> 0x0189 }
            if (r0 != 0) goto L_0x00f8
            boolean r0 = r4.mkdirs()     // Catch:{ Exception -> 0x0189 }
            if (r0 != 0) goto L_0x00f8
            if (r9 == 0) goto L_0x00f8
            boolean r0 = com.wc.andr.utcl.x.b(r9)     // Catch:{ Exception -> 0x0189 }
            if (r0 == 0) goto L_0x00f8
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0189 }
            r0.<init>()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x0183 }
            r6 = 3
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0183 }
            r6 = {117, 114, 108} // fill-array     // Catch:{ Exception -> 0x0183 }
            r4.<init>(r6)     // Catch:{ Exception -> 0x0183 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0183 }
            r7 = 4
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x0183 }
            r7 = {112, 97, 114, 97} // fill-array     // Catch:{ Exception -> 0x0183 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0183 }
            java.lang.Integer r7 = com.wc.andr.utcl.A.e     // Catch:{ Exception -> 0x0183 }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x0183 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0183 }
            r7 = 28
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x0183 }
            r7 = {115, 101, 116, 117, 112, 112, 99, 107, 61, 115, 101, 116, 117, 112, 112, 99, 107, 38, 111, 112, 101, 114, 97, 116, 101, 61, 50, 49} // fill-array     // Catch:{ Exception -> 0x0183 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0183 }
            r0.put(r4, r6)     // Catch:{ Exception -> 0x0183 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0183 }
            com.wc.andr.utcl.r.a(r9, r0)     // Catch:{ Exception -> 0x0183 }
        L_0x00f8:
            java.lang.String r0 = r8.f     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = "file://"
            r4.<init>(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = com.wc.andr.utcl.t.c     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r0 = r2.replace(r0, r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r2 = "img/"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = "file://"
            r4.<init>(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r0 = r0.replace(r2, r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = "file://"
            r2.<init>(r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = com.wc.andr.utcl.t.c     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = "/soft"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0189 }
            r4.<init>()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = r8.f     // Catch:{ Exception -> 0x0189 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r6 = "/soft"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0189 }
            java.lang.String r0 = r0.replace(r2, r4)     // Catch:{ Exception -> 0x0189 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0189 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0189 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x019d, all -> 0x019a }
            r2.write(r0)     // Catch:{ Exception -> 0x019d, all -> 0x019a }
            r2.flush()     // Catch:{ Exception -> 0x019d, all -> 0x019a }
            r2.close()     // Catch:{ Exception -> 0x019d, all -> 0x019a }
            r2.close()
        L_0x017f:
            r8.a(r3, r9, r12)
            return
        L_0x0183:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0189 }
            goto L_0x00f8
        L_0x0189:
            r0 = move-exception
        L_0x018a:
            r0.printStackTrace()     // Catch:{ all -> 0x0193 }
            if (r1 == 0) goto L_0x017f
            r1.close()
            goto L_0x017f
        L_0x0193:
            r0 = move-exception
        L_0x0194:
            if (r1 == 0) goto L_0x0199
            r1.close()
        L_0x0199:
            throw r0
        L_0x019a:
            r0 = move-exception
            r1 = r2
            goto L_0x0194
        L_0x019d:
            r0 = move-exception
            r1 = r2
            goto L_0x018a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.t.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):void");
    }
}
