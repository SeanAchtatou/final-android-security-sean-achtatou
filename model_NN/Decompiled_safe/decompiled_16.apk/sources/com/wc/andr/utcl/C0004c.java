package com.wc.andr.utcl;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.File;

/* renamed from: com.wc.andr.utcl.c  reason: case insensitive filesystem */
final class C0004c extends WebViewClient {
    private /* synthetic */ C0002a a;

    C0004c(C0002a aVar) {
        this.a = aVar;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!x.b(this.a.h)) {
            webView.goBack();
            Toast.makeText(this.a.h, A.p, 0).show();
        } else if (str.substring(str.lastIndexOf("?") + 1).contains("pagen")) {
            webView.goBack();
            String str2 = this.a.o + "morelist1.jsp";
            if (new File(str2).exists()) {
                this.a.g.loadUrl("file://" + str2);
            } else {
                this.a.g.loadUrl(C0008v.a + "?pagen=1");
            }
        } else {
            String substring = str.substring(str.lastIndexOf("/") + 1);
            if (substring.contains("jsp") && substring.contains("?")) {
                String[] split = substring.substring(substring.lastIndexOf("?")).split("&");
                int i = 0;
                while (true) {
                    if (i >= split.length) {
                        break;
                    }
                    String[] split2 = split[i].split("=");
                    if (split2.length >= 2 && split2[0].equals("pck")) {
                        String str3 = split2[1];
                        x.a(this.a.h, str3.substring(0, str3.lastIndexOf("list")), new byte[]{57});
                        String str4 = this.a.o + split2[1] + ".jsp";
                        if (new File(str4).exists()) {
                            webView.goBack();
                            this.a.g.loadUrl("file://" + str4);
                            break;
                        }
                        new C0006e(this.a, str, str3).start();
                    }
                    i++;
                }
            }
        }
        return false;
    }
}
