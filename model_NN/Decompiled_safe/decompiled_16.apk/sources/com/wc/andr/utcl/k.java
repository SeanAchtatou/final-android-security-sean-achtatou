package com.wc.andr.utcl;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;

final class k implements View.OnTouchListener {
    private /* synthetic */ Bitmap a;
    private /* synthetic */ Bitmap b;

    k(C0002a aVar, Bitmap bitmap, Bitmap bitmap2) {
        this.a = bitmap;
        this.b = bitmap2;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            if (this.a == null) {
                return false;
            }
            view.setBackgroundDrawable(new BitmapDrawable(this.a));
            return false;
        } else if (motionEvent.getAction() != 1 || this.b == null) {
            return false;
        } else {
            view.setBackgroundDrawable(new BitmapDrawable(this.b));
            return false;
        }
    }
}
