package com.wc.andr.utcl;

import android.content.Intent;
import android.webkit.DownloadListener;
import android.widget.Toast;
import com.wc.andr.Da;
import com.wc.andr.a.c;
import java.util.Arrays;

/* renamed from: com.wc.andr.utcl.d  reason: case insensitive filesystem */
final class C0005d implements DownloadListener {
    private /* synthetic */ C0002a a;

    C0005d(C0002a aVar) {
        this.a = aVar;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        if (x.b(this.a.h)) {
            int lastIndexOf = str.lastIndexOf("?");
            if (lastIndexOf >= 0) {
                String substring = str.substring(0, lastIndexOf);
                String sb = new StringBuilder().append(Integer.parseInt(substring.substring(substring.lastIndexOf("/") + 1).replace(".apk", "").replace("soft", "")) - 1).toString();
                String str5 = "";
                String str6 = "";
                String[] split = str.substring(lastIndexOf + 1).split("&");
                for (String split2 : split) {
                    String[] split3 = split2.split("=");
                    if (split3.length >= 2 && split3[0].equals("pck")) {
                        str5 = split3[1];
                        if (x.a(this.a.h, str5) || !Arrays.asList(q.a(q.b).split(",")).contains(sb)) {
                            c.a(this.a.h, "lockids" + str5, sb);
                        } else {
                            x.e(this.a.h, str5);
                            return;
                        }
                    } else if (split3.length > 0 && split3[0].equals(A.D)) {
                        str6 = x.d(split3[1]);
                    }
                }
                if (!"".equals(str5) && !"".equals(str6)) {
                    if (c.b(this.a.h, str5 + A.C) != null) {
                        Toast.makeText(this.a.h, A.r + "...", 0).show();
                        return;
                    }
                    Da da = this.a.h;
                    new B();
                    Intent intent = new Intent(da, B.e());
                    intent.putExtra(A.T, A.U);
                    intent.putExtra(A.S, str5);
                    intent.putExtra(A.V, substring);
                    intent.putExtra(A.W, str6);
                    c.a(this.a.h, str5 + A.F, "2");
                    this.a.h.startService(intent);
                    Toast.makeText(this.a.h, A.t + "……" + A.u, 1).show();
                    x.a(this.a.h, str5, new byte[]{48});
                    return;
                }
                return;
            }
            return;
        }
        Toast.makeText(this.a.h, A.p, 0).show();
    }
}
