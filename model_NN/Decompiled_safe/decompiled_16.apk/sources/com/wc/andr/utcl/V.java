package com.wc.andr.utcl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class V extends LinearLayout {
    public int a;
    private float b;
    private float c;
    private float d;
    private float e;
    private int f;
    private int g;
    private WindowManager h;
    private WindowManager.LayoutParams i;
    private E j;
    private Context k;
    private ImageView l;

    public V(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setBackgroundColor(0);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public V(WindowManager windowManager, WindowManager.LayoutParams layoutParams, Context context, E e2) {
        this(context, null);
        Bitmap bitmap = null;
        this.k = context;
        this.h = windowManager;
        this.i = layoutParams;
        this.j = e2;
        try {
            bitmap = x.c(this.k, "f");
        } catch (Exception e3) {
        }
        LinearLayout linearLayout = new LinearLayout(this.k);
        linearLayout.setOrientation(1);
        this.l = new ImageView(this.k);
        if (this.j.h != null) {
            this.l.setImageBitmap(this.j.h);
        }
        linearLayout.addView(this.l);
        TextView textView = new TextView(this.k);
        textView.setGravity(17);
        textView.setText(this.j.c);
        textView.setBackgroundDrawable(new BitmapDrawable(bitmap));
        textView.setTextColor(Color.rgb((int) MotionEventCompat.ACTION_MASK, 139, 0));
        linearLayout.addView(textView);
        if (this.j.h != null) {
            this.a = bitmap.getHeight() + this.j.h.getHeight() + 8;
        }
        addView(linearLayout);
    }

    private void a() {
        this.i.x = (int) (this.c - this.b);
        this.i.y = (int) (this.e - this.d);
        if (this.h != null && this.i != null) {
            this.h.updateViewLayout(this, this.i);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.c = motionEvent.getRawX();
        this.e = motionEvent.getRawY() - 25.0f;
        switch (motionEvent.getAction()) {
            case 0:
                this.f = (int) motionEvent.getRawX();
                this.g = (int) motionEvent.getRawY();
                this.b = motionEvent.getX();
                this.d = motionEvent.getY();
                return true;
            case 1:
                float abs = Math.abs(this.c - ((float) this.f));
                float abs2 = Math.abs((this.e - ((float) this.g)) + 25.0f);
                if (abs >= 5.0f || abs2 >= 5.0f) {
                    a();
                    return true;
                }
                E e2 = this.j;
                if (((E) A.a.get(e2.f)) != null) {
                    A.a.remove(e2.f);
                }
                Context context = e2.a;
                new B();
                Intent intent = new Intent(context, B.f());
                intent.addFlags(335544320);
                intent.putExtra(A.Y, A.Z);
                intent.putExtra(A.D, e2.b);
                intent.putExtra(A.af, e2.c);
                intent.putExtra(A.ag, e2.d);
                intent.putExtra(A.ae, e2.e);
                intent.putExtra(A.aa, 0);
                intent.putExtra(A.S, e2.f);
                intent.putExtra(A.ab, e2.g);
                intent.putExtra(A.ac, e2.i);
                e2.a.startActivity(intent);
                e2.a();
                return true;
            case 2:
                a();
                return true;
            default:
                return true;
        }
    }
}
