package com.wc.andr.utcl;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.wc.andr.Da;
import com.wc.andr.a.c;
import java.util.Arrays;

final class i implements View.OnClickListener {
    private /* synthetic */ C0002a a;

    i(C0002a aVar) {
        this.a = aVar;
    }

    public final void onClick(View view) {
        if (!x.a(this.a.h, this.a.d)) {
            String[] split = q.a(q.b).split(",");
            String b = c.b(this.a.h, "lockids" + this.a.d);
            if (b != null && Arrays.asList(split).contains(b)) {
                x.e(this.a.h, this.a.d);
                return;
            }
        }
        if (!x.b(this.a.h)) {
            Toast.makeText(this.a.h, A.p, 0).show();
        } else if (c.b(this.a.h, this.a.d + A.C) != null) {
            Toast.makeText(this.a.h, A.r + "...", 0).show();
        } else {
            Da da = this.a.h;
            new B();
            Intent intent = new Intent(da, B.e());
            intent.putExtra(A.T, A.U);
            intent.putExtra(A.S, this.a.d);
            intent.putExtra(A.V, this.a.c);
            intent.putExtra(A.W, this.a.b);
            if (this.a.a != null && !this.a.a.equals(this.a.d)) {
                c.a(this.a.h, this.a.d + A.F, "1");
            }
            this.a.h.startService(intent);
            Toast.makeText(this.a.h, A.t + "……", 0).show();
            x.a(this.a.h, this.a.d, new byte[]{48});
            this.a.h.finish();
        }
    }
}
