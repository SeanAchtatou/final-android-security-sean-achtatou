package com.wc.andr.utcl;

import android.view.MotionEvent;
import android.view.View;

final class g implements View.OnTouchListener {
    private int a = 0;
    private int b = 0;
    private /* synthetic */ C0002a c;

    g(C0002a aVar) {
        this.c = aVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.a = this.c.g.getScrollY();
        }
        if (motionEvent.getAction() == 1) {
            this.b = this.c.g.getScrollY();
            if (this.b <= 50 || this.b - this.a < 0) {
                this.c.f.setVisibility(8);
            } else {
                this.c.f.setVisibility(0);
            }
        }
        return false;
    }
}
