package com.wc.andr.utcl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.wc.andr.a.c;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.wc.andr.utcl.v  reason: case insensitive filesystem */
public final class C0008v implements z {
    public static String a = ("http://gyswad.com:8035/push/html" + "/pushList1.0.jsp");
    Context b;
    private String c = "url";
    private String d = "para";
    private o e = new o();
    private String f = "";

    public C0008v(Context context) {
        this.b = context;
        this.f = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    private void a() {
        new w(this).start();
    }

    public final void a(int i) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            if (x.b(this.b)) {
                String b2 = c.b(this.b, A.G + x.b());
                x.a(this.f, "is_req_sign" + "-----------" + b2);
                if (b2 == null || A.a(this.b).equals(A.k)) {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put(this.d, A.d);
                        jSONObject.put(this.c, "reqNum=" + i);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    new x(this.b, this, jSONObject.toString(), A.b).start();
                    return;
                }
                return;
            }
            x.a(this.f, A.p);
        }
    }

    public final void a(Context context, Integer num, Object obj, Bitmap bitmap) {
        try {
            s sVar = new s(obj.toString());
            String str = "reqnum";
            if (sVar.has(str)) {
                c.a(context, A.G + x.b(), sVar.getString(str));
            }
            if (sVar.has(A.I)) {
                q.b();
                if ("2".equals(sVar.getString(A.I))) {
                    q.c(q.b);
                }
            }
            if (sVar.has(A.J)) {
                String string = sVar.getString(A.J);
                Integer valueOf = string.matches("\\d+") ? Integer.valueOf(Integer.parseInt(string)) : 15;
                String a2 = q.a(q.c);
                String a3 = x.a(new Date().getTime() - ((long) ((((valueOf.intValue() * 1000) * 60) * 60) * 24)));
                if (!a2.matches("\\d+")) {
                    a2 = a3;
                }
                if (Integer.parseInt(a3) >= Integer.parseInt(a2)) {
                    if (!sVar.has("imei")) {
                        q.b();
                        q.c(q.c);
                        String a4 = q.a(q.d);
                        if (x.a(a4)) {
                            new q().a(q.d, "1");
                        } else if (a4.matches("\\d+")) {
                            new q().a(q.d, new StringBuilder().append(Integer.parseInt(a4) + 1).toString());
                        } else {
                            new q().a(q.d, "1");
                        }
                    } else {
                        q.d(q.d);
                    }
                }
            }
            if (sVar.getInt(A.H) != 0) {
                String string2 = sVar.getString(A.D);
                String string3 = sVar.getString("url");
                String string4 = sVar.getString("html");
                String string5 = sVar.getString("info1");
                String string6 = sVar.getString(A.af);
                String string7 = sVar.getString(A.S);
                String string8 = sVar.getString("ids");
                if (string8 != null) {
                    c.a(context, "lockids" + string7, string8);
                }
                if (A.a(context).equals(A.k)) {
                    E e2 = new E();
                    e2.a(context, string2, string6, string3, string4, string7, string5, bitmap);
                    A.a.put(string7, e2);
                    this.e.a(context, string2, string5, string3, 2, string7, bitmap, string6, string4);
                    a();
                    return;
                }
                q.b(string8);
                String a5 = q.a(q.c);
                if (x.a(a5)) {
                    new q().a(q.c, x.b());
                } else if (a5.length() != 8 || !a5.matches("\\d+")) {
                    new q().a(q.c, x.b());
                }
                int i = sVar.has("imei") ? 2 : 1;
                Integer valueOf2 = Integer.valueOf(sVar.getInt("music"));
                if (valueOf2.intValue() != 0) {
                    c.b(context, A.ad, valueOf2);
                }
                if (i == 1 || (A.o.contains(this.f) && this.f.length() > 5)) {
                    E e3 = new E();
                    e3.a(context, string2, string6, string3, string4, string7, string5, bitmap);
                    A.a.put(string7, e3);
                }
                this.e.a(context, string2, string5, string3, i, string7, bitmap, string6, string4);
                x.a(context, string7, new byte[]{49, 49});
                a();
            }
        } catch (JSONException e4) {
            e4.printStackTrace();
            Log.d("D", "json err" + "……");
        } catch (Exception e5) {
            e5.printStackTrace();
        }
    }
}
