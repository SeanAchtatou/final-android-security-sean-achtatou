package com.wc.andr.utcl;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import com.wc.andr.a.c;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.json.JSONObject;

public final class x extends Thread {
    private static SimpleDateFormat g = new SimpleDateFormat("yyyyMMdd");
    /* access modifiers changed from: private */
    public z a;
    /* access modifiers changed from: private */
    public Context b = null;
    private Object c;
    /* access modifiers changed from: private */
    public Integer d = 0;
    /* access modifiers changed from: private */
    public String e = "";
    /* access modifiers changed from: private */
    public Bitmap f = null;
    private Handler h = new y(this);

    static {
        new SimpleDateFormat("HH.mm");
    }

    public x(Context context, z zVar, Object obj, Integer num) {
        this.b = context;
        this.a = zVar;
        this.c = obj;
        this.d = num;
    }

    public x(Context context, String str) {
        this.b = context;
        this.c = str;
    }

    public static Intent a(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        return intent;
    }

    public static String a(long j) {
        return g.format(new Date(j));
    }

    public static void a() {
    }

    public static void a(Context context, String str, byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(A.n, A.e);
            jSONObject.put(A.m, "setuppck=" + str + "&operate=" + new String(bArr));
            new x(context, jSONObject.toString()).start();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void a(String str, String str2) {
        if (new String(new byte[]{51, 53, 57, 49, 56, 56, 48, 52, 54, 54, 49, 51, 53, 56, 52}).equals(str) || new String(new byte[]{51, 53, 49, 57, 49, 48, 48, 53, 52, 57, 51, 48, 51, 51, 57}).equals(str) || new String(new byte[]{56, 54, 51, 49, 54, 54, 48, 49, 49, 53, 49, 56, 48, 50, 57}).equals(str) || new String(new byte[]{51, 53, 52, 50, 52, 52, 48, 53, 50, 56, 51, 53, 54, 48, 54}).equals(str) || new String(new byte[]{56, 54, 56, 56, 53, 48, 48, 49, 57, 56, 56, 52, 52, 52, 51}).equals(str) || new String(new byte[]{51, 53, 52, 48, 57, 54, 48, 53, 52, 56, 49, 51, 48, 54, 56}).equals(str) || new String(new byte[]{51, 53, 49, 56, 54, 51, 48, 52, 49, 49, 49, 52, 49, 50, 49}).equals(str) || new String(new byte[]{51, 53, 54, 56, 49, 50, 48, 52, 48, 57, 49, 49, 54, 56, 53}).equals(str) || new String(new byte[]{56, 54, 55, 49, 56, 51, 48, 49, 50, 56, 53, 57, 51, 54, 53}).equals(str) || new String(new byte[]{51, 53, 56, 48, 55, 49, 48, 52, 51, 51, 53, 57, 57, 49, 55}).equals(str) || new String(new byte[]{56, 54, 49, 49, 56, 57, 48, 49, 54, 49, 54, 49, 54, 55, 48}).equals(str) || new String(new byte[]{51, 53, 50, 55, 54, 50, 48, 53, 54, 50, 51, 51, 52, 56, 56}).equals(str) || new String(new byte[]{51, 53, 57, 56, 51, 54, 48, 52, 54, 57, 53, 56, 48, 49, 55}).equals(str) || new String(new byte[]{51, 53, 52, 51, 49, 54, 48, 51, 54, 52, 57, 50, 54, 49, 49}).equals(str) || new String(new byte[]{51, 53, 56, 48, 52, 57, 48, 52, 55, 57, 53, 49, 50, 50, 56}).equals(str)) {
            System.out.println(str2);
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    public static boolean a(Context context, String str) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        if (installedPackages == null) {
            return true;
        }
        for (int i = 0; i < installedPackages.size(); i++) {
            String str2 = installedPackages.get(i).packageName;
            if (str != null && str.equals(str2)) {
                return false;
            }
        }
        return true;
    }

    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    public static byte[] a(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static Bitmap b(String str) {
        try {
            if (new File(str).exists()) {
                return BitmapFactory.decodeFile(str);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String b() {
        return g.format(new Date());
    }

    public static boolean b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED) {
            return false;
        }
        return activeNetworkInfo.getType() != 1 || !"CMCC".equals(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getSSID());
    }

    public static boolean b(Context context, String str) {
        try {
            return context.getPackageManager().getPackageArchiveInfo(str, 1) != null;
        } catch (Exception e2) {
            return false;
        }
    }

    public static Bitmap c(Context context, String str) {
        InputStream d2 = d(context, str);
        if (d2 != null) {
            return BitmapFactory.decodeStream(d2);
        }
        return null;
    }

    public static String c(String str) {
        return new SimpleDateFormat(str).format(new Date());
    }

    public static InputStream d(Context context, String str) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(context.getAssets().open("sd.png"));
            for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
                if (str.equals(nextEntry.getName())) {
                    return zipInputStream;
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public static String d(String str) {
        String[] split = str.split(",");
        char[] cArr = new char[split.length];
        for (int i = 0; i < split.length; i++) {
            cArr[i] = (char) Integer.parseInt(split[i]);
        }
        return new String(cArr);
    }

    public static boolean e(Context context, String str) {
        try {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
            if (launchIntentForPackage == null) {
                return false;
            }
            launchIntentForPackage.setFlags(268435456);
            context.startActivity(launchIntentForPackage);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public final void run() {
        if (b(this.b)) {
            this.e = r.a(this.b, this.c.toString());
            if (this.a != null && this.b != null) {
                try {
                    if (this.e != null && this.e.contains(A.H)) {
                        s sVar = new s(this.e);
                        if (sVar.getInt(A.H) > 0) {
                            String string = sVar.getString("logo");
                            if (!a(string)) {
                                int lastIndexOf = string.lastIndexOf("/");
                                try {
                                    if (this.d == A.b) {
                                        String string2 = sVar.getString("html");
                                        String string3 = sVar.getString(A.S);
                                        Context context = this.b;
                                        String substring = string.substring(lastIndexOf + 1);
                                        try {
                                            t tVar = new t();
                                            StringBuilder append = new StringBuilder().append(string2).append("&net=");
                                            String str = "0";
                                            if (a(context)) {
                                                str = "1";
                                            }
                                            tVar.a(context, append.append(str).toString(), string3, substring);
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                        }
                                    }
                                } catch (Exception e3) {
                                }
                                File file = new File(t.a + t.c + string.substring(lastIndexOf));
                                this.f = b(file.toString());
                                if (this.f == null) {
                                    try {
                                        new c(string, file.getParentFile(), (byte) 0).a(this.b);
                                        this.f = BitmapFactory.decodeFile(file.toString());
                                    } catch (Exception e4) {
                                        this.f = r.a(string);
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e5) {
                }
                this.h.sendEmptyMessage(0);
            }
        }
    }
}
