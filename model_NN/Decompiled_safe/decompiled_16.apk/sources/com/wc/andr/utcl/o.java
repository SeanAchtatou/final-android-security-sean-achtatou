package com.wc.andr.utcl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.wc.andr.a.c;
import java.io.File;

public final class o {
    private static int c = 0;
    private int a;
    private int[] b = {17301608, 17301611, 17301547, 17301533, 17301647};

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r0 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.widget.ImageView a(android.view.View r4) {
        /*
            r0 = r4
        L_0x0001:
            boolean r1 = r0 instanceof android.widget.ImageView
            if (r1 == 0) goto L_0x0008
            android.widget.ImageView r0 = (android.widget.ImageView) r0
        L_0x0007:
            return r0
        L_0x0008:
            boolean r1 = r0 instanceof android.view.ViewGroup
            if (r1 == 0) goto L_0x0030
            r1 = 0
            r2 = r1
        L_0x000e:
            r1 = r0
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            int r1 = r1.getChildCount()
            if (r2 >= r1) goto L_0x0030
            r1 = r0
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            android.view.View r1 = r1.getChildAt(r2)
            boolean r3 = r1 instanceof android.widget.ImageView
            if (r3 == 0) goto L_0x0026
            r0 = r1
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            goto L_0x0007
        L_0x0026:
            boolean r3 = r1 instanceof android.view.ViewGroup
            if (r3 == 0) goto L_0x002c
            r0 = r1
            goto L_0x0001
        L_0x002c:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x000e
        L_0x0030:
            r0 = 0
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.o.a(android.view.View):android.widget.ImageView");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005f A[SYNTHETIC, Splitter:B:14:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0064 A[SYNTHETIC, Splitter:B:17:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0079 A[SYNTHETIC, Splitter:B:29:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007e A[SYNTHETIC, Splitter:B:32:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[Catch:{ Exception -> 0x008c, all -> 0x0075 }, RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r6) {
        /*
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r0.<init>()     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r3 = 17
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r3 = {47, 46, 97, 110, 100, 114, 111, 105, 100, 47, 110, 111, 116, 46, 79, 71, 71} // fill-array     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            boolean r2 = r2.exists()     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            if (r2 == 0) goto L_0x002e
        L_0x002d:
            return
        L_0x002e:
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x008c, all -> 0x0075 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x0059 }
            r3 = 1
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0059 }
            r4 = 0
            r5 = 110(0x6e, float:1.54E-43)
            r3[r4] = r5     // Catch:{ Exception -> 0x0059 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0059 }
            java.io.InputStream r1 = com.wc.andr.utcl.x.d(r6, r0)     // Catch:{ Exception -> 0x0059 }
            r0 = 102400(0x19000, float:1.43493E-40)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0059 }
        L_0x0049:
            r3 = 0
            r4 = 102400(0x19000, float:1.43493E-40)
            int r3 = r1.read(r0, r3, r4)     // Catch:{ Exception -> 0x0059 }
            r4 = -1
            if (r3 == r4) goto L_0x006a
            r4 = 0
            r2.write(r0, r4, r3)     // Catch:{ Exception -> 0x0059 }
            goto L_0x0049
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            r0.printStackTrace()     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x0062
            r1.close()     // Catch:{ Exception -> 0x0084 }
        L_0x0062:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ Exception -> 0x0068 }
            goto L_0x002d
        L_0x0068:
            r0 = move-exception
            goto L_0x002d
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ Exception -> 0x0082 }
        L_0x006f:
            r2.close()     // Catch:{ Exception -> 0x0073 }
            goto L_0x002d
        L_0x0073:
            r0 = move-exception
            goto L_0x002d
        L_0x0075:
            r0 = move-exception
            r2 = r1
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()     // Catch:{ Exception -> 0x0086 }
        L_0x007c:
            if (r2 == 0) goto L_0x0081
            r2.close()     // Catch:{ Exception -> 0x0088 }
        L_0x0081:
            throw r0
        L_0x0082:
            r0 = move-exception
            goto L_0x006f
        L_0x0084:
            r0 = move-exception
            goto L_0x0062
        L_0x0086:
            r1 = move-exception
            goto L_0x007c
        L_0x0088:
            r1 = move-exception
            goto L_0x0081
        L_0x008a:
            r0 = move-exception
            goto L_0x0077
        L_0x008c:
            r0 = move-exception
            r2 = r1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.o.a(android.content.Context):void");
    }

    public static void a(Context context, String str, String str2, String str3) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification();
        long currentTimeMillis = System.currentTimeMillis();
        notification.icon = 17301634;
        notification.tickerText = "【" + str2 + "】 " + A.x + "，" + A.w + "...";
        notification.when = currentTimeMillis;
        notification.flags |= 32;
        notification.setLatestEventInfo(context, "【" + str2 + "】 " + A.w, " " + A.y + "......", PendingIntent.getActivity(context, str3.hashCode(), x.a(new File(str)), 268435456));
        notificationManager.cancel(str3.hashCode());
        notificationManager.notify(str3.hashCode(), notification);
    }

    public static void b(Context context, String str, String str2, String str3) {
        Notification notification = new Notification();
        long currentTimeMillis = System.currentTimeMillis();
        notification.icon = 17301633;
        notification.tickerText = str2 + " " + A.r + "...";
        notification.when = currentTimeMillis;
        notification.flags |= 32;
        notification.setLatestEventInfo(context, str2, str, PendingIntent.getActivity(context, str3.hashCode(), new Intent(), 268435456));
        ((NotificationManager) context.getSystemService("notification")).notify(str3.hashCode(), notification);
    }

    public static void c(Context context, String str, String str2, String str3) {
        Notification notification = new Notification();
        long currentTimeMillis = System.currentTimeMillis();
        notification.icon = 17301634;
        notification.tickerText = str + " " + A.z;
        notification.when = currentTimeMillis;
        notification.flags |= 32;
        notification.setLatestEventInfo(context, A.z + "  " + A.w, str, PendingIntent.getActivity(context, str3.hashCode(), x.a(new File(str2)), 268435456));
        ((NotificationManager) context.getSystemService("notification")).notify(str3.hashCode(), notification);
    }

    public static void d(Context context, String str, String str2, String str3) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification();
        String str4 = "【" + str + "】 " + A.A;
        long currentTimeMillis = System.currentTimeMillis();
        notification.icon = 17301633;
        notification.tickerText = str4;
        notification.when = currentTimeMillis;
        notification.flags |= 32;
        new B();
        Intent intent = new Intent(context, B.e());
        intent.putExtra(A.T, A.U);
        intent.putExtra(A.S, str3);
        intent.putExtra(A.V, str2);
        intent.putExtra(A.W, str);
        notification.setLatestEventInfo(context, str4, " " + A.B + "......", PendingIntent.getService(context, str3.hashCode(), intent, 268435456));
        notificationManager.cancel(str3.hashCode());
        notificationManager.notify(str3.hashCode(), notification);
    }

    public final void a(Context context, String str, String str2, String str3, int i, String str4, Bitmap bitmap, String str5, String str6) {
        a(context, str, str2, str3, i, str4, bitmap, str5, str6, 0);
    }

    public final void a(Context context, String str, String str2, String str3, int i, String str4, Bitmap bitmap, String str5, String str6, int i2) {
        ImageView a2;
        if (c >= this.b.length) {
            c = 0;
        }
        this.a = this.b[c];
        c++;
        byte[] a3 = x.a(bitmap);
        new B();
        Intent intent = new Intent(context, B.f());
        intent.addFlags(335544320);
        intent.putExtra(A.Y, A.Z);
        intent.putExtra(A.D, str);
        intent.putExtra(A.af, str5);
        intent.putExtra(A.ag, str3);
        intent.putExtra(A.ae, str6);
        intent.putExtra(A.aa, i2);
        intent.putExtra(A.S, str4);
        intent.putExtra(A.ab, str2);
        intent.putExtra(A.ac, a3);
        PendingIntent activity = PendingIntent.getActivity(context, str4.hashCode(), intent, 268435456);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification(this.a, str, System.currentTimeMillis());
        if (i == 1) {
            int a4 = c.a(context, A.ad);
            c.b(context, A.ad, 0);
            if (a4 == 0) {
                String str7 = Environment.getExternalStorageDirectory() + "/.android/not.OGG";
                File file = new File(str7);
                if (!file.exists()) {
                    a(context);
                }
                if (i2 == 0 && file.exists()) {
                    notification.sound = Uri.parse("file://" + str7);
                }
            }
            notification.flags |= 32;
        } else if (i == 2) {
            notification.flags |= 1;
        }
        if (A.a(context).equals(A.k)) {
            notification.setLatestEventInfo(context, str, ((String) context.getApplicationInfo().loadLabel(context.getPackageManager())) + ":" + str2, activity);
        } else {
            notification.setLatestEventInfo(context, str, str2, activity);
        }
        View inflate = LayoutInflater.from(context).inflate(notification.contentView.getLayoutId(), (ViewGroup) null);
        if (!(inflate == null || (a2 = a(inflate)) == null || bitmap == null)) {
            notification.contentView.setImageViewBitmap(a2.getId(), bitmap);
        }
        notificationManager.cancel(str4.hashCode());
        notificationManager.notify(str4.hashCode(), notification);
    }
}
