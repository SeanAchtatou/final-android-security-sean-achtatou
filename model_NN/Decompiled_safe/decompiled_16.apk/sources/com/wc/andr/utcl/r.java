package com.wc.andr.utcl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class r {
    private static String a = ("http://" + a());
    private static String b = "";
    private static String c = "";
    private static String d = "";
    private static String e = "";

    public static Bitmap a(String str) {
        if (x.a(str)) {
            return null;
        }
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.getContent();
            return BitmapFactory.decodeStream(openConnection.getInputStream());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static String a() {
        int[] iArr = {103, 121, 115, 119, 97, 100, 46, 99, 111, 109};
        String str = "";
        for (int i = 0; i < iArr.length; i++) {
            str = str + ((char) iArr[i]);
        }
        return (str + ":8888") + "/push/gy/";
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x02e5 A[SYNTHETIC, Splitter:B:60:0x02e5] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x02fc A[SYNTHETIC, Splitter:B:69:0x02fc] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0301  */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[Catch:{ Exception -> 0x023d }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r11, java.lang.String r12) {
        /*
            r4 = 1
            r5 = 0
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r11.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            java.lang.String r1 = r0.getSubscriberId()
            com.wc.andr.utcl.r.e = r1
            java.lang.String r0 = r0.getDeviceId()
            com.wc.andr.utcl.r.d = r0
            java.lang.String r0 = r11.getPackageName()
            com.wc.andr.utcl.r.c = r0
            java.lang.String r0 = com.wc.andr.utcl.A.b(r11)
            com.wc.andr.utcl.r.b = r0
            java.lang.Integer.valueOf(r5)
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x023d }
            r0.<init>(r12)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {112, 97, 114, 97} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x023d }
            if (r7 == 0) goto L_0x0230
            java.lang.Integer r1 = com.wc.andr.utcl.A.d     // Catch:{ Exception -> 0x023d }
            if (r7 != r1) goto L_0x0138
            r1 = 16
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x023d }
            r1 = {112, 117, 115, 104, 82, 101, 113, 117, 101, 115, 116, 46, 106, 115, 112, 63} // fill-array     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r2.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3.<init>(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r1 = r2.append(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x023d }
            r3 = {117, 114, 108} // fill-array     // Catch:{ Exception -> 0x023d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x023d }
        L_0x006f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r1.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r2 = com.wc.andr.utcl.r.a     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x023d }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = com.wc.andr.utcl.r.d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "--"
            r1.<init>(r2)
            java.lang.String r2 = com.wc.andr.utcl.p.a
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "--"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            com.wc.andr.utcl.x.a(r0, r1)
            r1 = 0
            java.lang.String r0 = ""
            r2 = 0
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x02aa, all -> 0x02f8 }
            r3.<init>()     // Catch:{ Exception -> 0x02aa, all -> 0x02f8 }
            org.apache.http.params.HttpParams r1 = r3.getParams()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r8 = "http.connection.timeout"
            r9 = 8000(0x1f40, float:1.121E-41)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.setParameter(r8, r9)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.params.HttpParams r1 = r3.getParams()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r8 = "http.socket.timeout"
            r9 = 8000(0x1f40, float:1.121E-41)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.setParameter(r8, r9)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r8 = 8
            byte[] r8 = new byte[r8]     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r8 = {104, 101, 97, 100, 95, 107, 101, 121} // fill-array     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r6.<init>(r8)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r8 = com.wc.andr.utcl.r.e     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r8 = b(r8)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.setHeader(r6, r8)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.client.entity.UrlEncodedFormEntity r6 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            com.wc.andr.utcl.p r8 = new com.wc.andr.utcl.p     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r8.<init>()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.util.List r8 = r8.a(r11, r7)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r6.<init>(r8)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.setEntity(r6)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.HttpResponse r1 = r3.execute(r1)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.StatusLine r6 = r1.getStatusLine()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            int r6 = r6.getStatusCode()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r8 = 200(0xc8, float:2.8E-43)
            if (r6 != r8) goto L_0x0253
            org.apache.http.HttpEntity r1 = r1.getEntity()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.io.InputStream r6 = r1.getContent()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
        L_0x0111:
            int r8 = r1.read()     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            r2 = -1
            if (r8 == r2) goto L_0x027e
            r2 = 10
            if (r8 == r2) goto L_0x024d
            r6 = r4
        L_0x011d:
            r2 = 13
            if (r8 == r2) goto L_0x0250
            r2 = r4
        L_0x0122:
            r2 = r2 & r6
            if (r2 == 0) goto L_0x0111
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            r2.<init>()     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            char r2 = (char) r8     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x031b, all -> 0x0310 }
            goto L_0x0111
        L_0x0138:
            java.lang.Integer r1 = com.wc.andr.utcl.A.e     // Catch:{ Exception -> 0x023d }
            if (r7 != r1) goto L_0x017f
            r1 = 19
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x023d }
            r1 = {112, 117, 115, 104, 83, 116, 97, 116, 105, 115, 116, 105, 99, 115, 46, 106, 115, 112, 63} // fill-array     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r2.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3.<init>(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r1 = r2.append(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x023d }
            r3 = {117, 114, 108} // fill-array     // Catch:{ Exception -> 0x023d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 5
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {38, 112, 99, 107, 61} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = com.wc.andr.utcl.r.c     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x023d }
            goto L_0x006f
        L_0x017f:
            java.lang.Integer r1 = com.wc.andr.utcl.A.f     // Catch:{ Exception -> 0x023d }
            if (r7 != r1) goto L_0x01c9
            r0 = 17
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x023d }
            r0 = {112, 117, 115, 104, 86, 105, 98, 114, 97, 110, 99, 121, 46, 106, 115, 112, 63} // fill-array     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r1.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2.<init>(r0)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r1.append(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {112, 99, 107, 61} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = com.wc.andr.utcl.r.c     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 10
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {38, 109, 97, 114, 107, 101, 116, 105, 100, 61} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = com.wc.andr.utcl.r.b     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x023d }
            goto L_0x006f
        L_0x01c9:
            java.lang.Integer r1 = com.wc.andr.utcl.A.g     // Catch:{ Exception -> 0x023d }
            if (r7 != r1) goto L_0x0210
            r1 = 14
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x023d }
            r1 = {110, 111, 116, 105, 102, 105, 82, 101, 113, 46, 106, 115, 112, 63} // fill-array     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r2.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3.<init>(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r1 = r2.append(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x023d }
            r3 = {117, 114, 108} // fill-array     // Catch:{ Exception -> 0x023d }
            r2.<init>(r3)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 5
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {38, 112, 99, 107, 61} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = com.wc.andr.utcl.r.c     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x023d }
            goto L_0x006f
        L_0x0210:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023d }
            r0.<init>()     // Catch:{ Exception -> 0x023d }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r2 = 11
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x023d }
            r2 = {112, 97, 114, 97, 32, -28, -72, -115, -25, -84, -90} // fill-array     // Catch:{ Exception -> 0x023d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.Integer r1 = com.wc.andr.utcl.A.d     // Catch:{ Exception -> 0x023d }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x023d }
        L_0x022f:
            return r0
        L_0x0230:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x023d }
            r1 = 8
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x023d }
            r1 = {112, 97, 114, 97, 32, 101, 114, 114} // fill-array     // Catch:{ Exception -> 0x023d }
            r0.<init>(r1)     // Catch:{ Exception -> 0x023d }
            goto L_0x022f
        L_0x023d:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = new java.lang.String
            r1 = 7
            byte[] r1 = new byte[r1]
            r1 = {117, 114, 108, 32, 101, 114, 114} // fill-array
            r0.<init>(r1)
            goto L_0x022f
        L_0x024d:
            r6 = r5
            goto L_0x011d
        L_0x0250:
            r2 = r5
            goto L_0x0122
        L_0x0253:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r0.<init>()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r4 = 8
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r4 = {104, 116, 116, 112, 67, 111, 100, 101} // fill-array     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r1 = "-"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0317, all -> 0x030e }
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
            goto L_0x022f
        L_0x027e:
            r1.close()     // Catch:{ IOException -> 0x02a5 }
        L_0x0281:
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
            java.lang.Integer r1 = com.wc.andr.utcl.A.d
            if (r7 == r1) goto L_0x0290
            java.lang.Integer r1 = com.wc.andr.utcl.A.g
            if (r7 != r1) goto L_0x022f
        L_0x0290:
            java.lang.String r1 = com.wc.andr.utcl.r.d
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "----"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.wc.andr.utcl.x.a(r1, r2)
            goto L_0x022f
        L_0x02a5:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0281
        L_0x02aa:
            r0 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
        L_0x02ae:
            java.lang.String r3 = com.wc.andr.utcl.r.d     // Catch:{ all -> 0x0313 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0313 }
            r4.<init>()     // Catch:{ all -> 0x0313 }
            java.lang.Class<com.wc.andr.utcl.r> r5 = com.wc.andr.utcl.r.class
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0313 }
            java.lang.String r5 = "-----"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0313 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0313 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0313 }
            java.lang.String r5 = "--"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0313 }
            java.lang.String r5 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0313 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0313 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0313 }
            com.wc.andr.utcl.x.a(r3, r4)     // Catch:{ all -> 0x0313 }
            r0.printStackTrace()     // Catch:{ all -> 0x0313 }
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x02e8
            r1.close()     // Catch:{ IOException -> 0x02f3 }
        L_0x02e8:
            if (r2 == 0) goto L_0x022f
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            goto L_0x022f
        L_0x02f3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02e8
        L_0x02f8:
            r0 = move-exception
            r3 = r1
        L_0x02fa:
            if (r2 == 0) goto L_0x02ff
            r2.close()     // Catch:{ IOException -> 0x0309 }
        L_0x02ff:
            if (r3 == 0) goto L_0x0308
            org.apache.http.conn.ClientConnectionManager r1 = r3.getConnectionManager()
            r1.shutdown()
        L_0x0308:
            throw r0
        L_0x0309:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x02ff
        L_0x030e:
            r0 = move-exception
            goto L_0x02fa
        L_0x0310:
            r0 = move-exception
            r2 = r1
            goto L_0x02fa
        L_0x0313:
            r0 = move-exception
            r3 = r2
            r2 = r1
            goto L_0x02fa
        L_0x0317:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x02ae
        L_0x031b:
            r0 = move-exception
            r2 = r3
            goto L_0x02ae
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.r.a(android.content.Context, java.lang.String):java.lang.String");
    }

    public static String b(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e2) {
            System.exit(-1);
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        byte[] digest = messageDigest.digest();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if (Integer.toHexString(digest[i] & 255).length() == 1) {
                stringBuffer.append("0").append(Integer.toHexString(digest[i] & 255));
            } else {
                stringBuffer.append(Integer.toHexString(digest[i] & 255));
            }
        }
        return stringBuffer.toString();
    }
}
