package com.wc.andr.utcl;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.wc.andr.Da;

final class j implements View.OnClickListener {
    private /* synthetic */ C0002a a;

    j(C0002a aVar) {
        this.a = aVar;
    }

    public final void onClick(View view) {
        if (x.b(this.a.h)) {
            Da da = this.a.h;
            new B();
            Intent intent = new Intent(da, B.f());
            this.a.h.a = 1;
            intent.putExtra(A.Y, "showlist");
            this.a.h.startActivity(intent);
            x.a(this.a.h, this.a.d, new byte[]{49, 48});
            return;
        }
        Toast.makeText(this.a.h, A.p, 0).show();
    }
}
