package com.wc.andr.utcl;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.wc.andr.a.c;
import java.io.File;

final class h extends WebViewClient {
    private /* synthetic */ C0002a a;

    h(C0002a aVar) {
        this.a = aVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.a.e == 0 && c.a(this.a.h, this.a.a + x.b() + A.aa) == 0) {
            x.a(this.a.h, this.a.d, new byte[]{52});
            c.b(this.a.h, this.a.a + x.b() + A.aa, 1);
        }
        super.onPageFinished(webView, str);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.a.e = 1;
        if (!x.b(this.a.h)) {
            webView.canGoBack();
            webView.goBack();
            Toast.makeText(this.a.h, A.p, 0).show();
        } else {
            try {
                String[] split = str.substring(str.lastIndexOf("?") + 1).split("&");
                String str2 = null;
                int i = 0;
                while (true) {
                    if (i < split.length) {
                        String[] split2 = split[i].split("=");
                        if (split2.length != 2) {
                            this.a.h.finish();
                            break;
                        }
                        if (A.D.equals(split2[0])) {
                            this.a.b = x.d(split2[1]);
                        } else if ("setpkg".equals(split2[0])) {
                            this.a.d = split2[1];
                        } else if (A.ag.equals(split2[0])) {
                            this.a.c = split2[1];
                        } else if ("index".equals(split2[0])) {
                            str2 = split2[1];
                        }
                        i++;
                    } else {
                        c.a(this.a.h, "lockids" + this.a.d, str2);
                        x.a(this.a.h, this.a.d, new byte[]{56});
                        if (this.a.d != null) {
                            String str3 = this.a.o + this.a.d + ".jsp";
                            if (new File(str3).exists()) {
                                webView.goBack();
                                this.a.g.loadUrl("file://" + str3);
                            } else {
                                new C0006e(this.a, str, this.a.d).start();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
