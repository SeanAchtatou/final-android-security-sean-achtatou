package com.wc.andr.utcl;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.wc.andr.Da;
import java.io.File;

/* renamed from: com.wc.andr.utcl.a  reason: case insensitive filesystem */
public final class C0002a {
    String a;
    String b;
    String c;
    String d;
    int e;
    ImageView f;
    WebView g = null;
    Da h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private Bitmap n;
    /* access modifiers changed from: private */
    public String o = (t.a + t.b + "/");
    private long p = 0;

    public C0002a() {
    }

    public C0002a(Da da) {
        this.h = da;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x023f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r13 = this;
            r10 = 10
            r9 = -1
            r1 = 0
            r8 = 1
            r6 = 0
            com.wc.andr.Da r0 = r13.h
            android.view.WindowManager r0 = r0.getWindowManager()
            android.view.Display r0 = r0.getDefaultDisplay()
            int r2 = r0.getHeight()
            com.wc.andr.Da r0 = r13.h
            android.content.Intent r3 = r0.getIntent()
            java.lang.String r0 = com.wc.andr.utcl.A.aa
            int r0 = r3.getIntExtra(r0, r6)
            r13.e = r0
            java.lang.String r0 = com.wc.andr.utcl.A.D
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.i = r0
            java.lang.String r0 = com.wc.andr.utcl.A.af
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.j = r0
            java.lang.String r0 = r13.j
            r13.b = r0
            java.lang.String r0 = com.wc.andr.utcl.A.ag
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.k = r0
            java.lang.String r0 = com.wc.andr.utcl.A.ae
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.l = r0
            java.lang.String r0 = r13.l
            java.lang.String r4 = "&"
            boolean r0 = r0.contains(r4)
            if (r0 == 0) goto L_0x0060
            java.lang.String r0 = r13.l
            java.lang.String r4 = r13.l
            java.lang.String r5 = "&"
            int r4 = r4.lastIndexOf(r5)
            java.lang.String r0 = r0.substring(r6, r4)
            r13.l = r0
        L_0x0060:
            java.lang.String r0 = r13.k
            r13.c = r0
            java.lang.String r0 = com.wc.andr.utcl.A.S
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.a = r0
            java.util.HashMap r0 = com.wc.andr.utcl.A.a
            java.lang.String r4 = r13.a
            java.lang.Object r0 = r0.get(r4)
            com.wc.andr.utcl.E r0 = (com.wc.andr.utcl.E) r0
            if (r0 == 0) goto L_0x0082
            java.util.HashMap r4 = com.wc.andr.utcl.A.a
            java.lang.String r5 = r13.a
            r4.remove(r5)
            r0.a()
        L_0x0082:
            java.lang.String r0 = r13.a
            r13.d = r0
            java.lang.String r0 = com.wc.andr.utcl.A.ab
            java.lang.String r0 = r3.getStringExtra(r0)
            r13.m = r0
            java.lang.String r0 = com.wc.andr.utcl.A.ac
            byte[] r0 = r3.getByteArrayExtra(r0)
            if (r0 == 0) goto L_0x00df
            int r3 = r0.length
            if (r3 == 0) goto L_0x00df
            int r3 = r0.length
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r0, r6, r3)
        L_0x009e:
            r13.n = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = r13.o
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r13.a
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = ".jsp"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            java.io.File r3 = new java.io.File
            r3.<init>(r0)
            boolean r4 = r3.exists()
            if (r4 != 0) goto L_0x00e1
            com.wc.andr.Da r4 = r13.h
            boolean r4 = com.wc.andr.utcl.x.b(r4)
            if (r4 != 0) goto L_0x00e1
            com.wc.andr.Da r0 = r13.h
            java.lang.String r1 = com.wc.andr.utcl.A.p
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r8)
            r0.show()
            com.wc.andr.Da r0 = r13.h
            r0.finish()
        L_0x00de:
            return
        L_0x00df:
            r0 = r1
            goto L_0x009e
        L_0x00e1:
            android.widget.RelativeLayout r5 = new android.widget.RelativeLayout
            com.wc.andr.Da r4 = r13.h
            r5.<init>(r4)
            android.view.ViewGroup$LayoutParams r4 = new android.view.ViewGroup$LayoutParams
            r4.<init>(r9, r9)
            r5.setLayoutParams(r4)
            r5.setBackgroundColor(r9)
            int r2 = r2 / 10
            android.webkit.WebView r4 = new android.webkit.WebView
            com.wc.andr.Da r6 = r13.h
            r4.<init>(r6)
            r13.g = r4
            android.webkit.WebView r4 = r13.g
            android.widget.LinearLayout$LayoutParams r6 = new android.widget.LinearLayout$LayoutParams
            int r7 = r2 * 9
            r6.<init>(r9, r7)
            r4.setLayoutParams(r6)
            android.webkit.WebView r4 = r13.g
            android.webkit.WebSettings r4 = r4.getSettings()
            r4.setJavaScriptEnabled(r8)
            boolean r4 = r3.exists()
            if (r4 == 0) goto L_0x028b
            int r3 = com.wc.andr.utcl.q.a(r3)
            r4 = 50
            if (r3 <= r4) goto L_0x028b
            android.webkit.WebView r3 = r13.g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "file://"
            r4.<init>(r6)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            r3.loadUrl(r0)
            android.webkit.WebView r0 = r13.g
            r0.reload()
        L_0x013a:
            android.webkit.WebView r0 = r13.g
            r3 = 32768(0x8000, float:4.5918E-41)
            r0.setScrollBarStyle(r3)
            android.webkit.WebView r0 = r13.g
            r5.addView(r0)
            com.wc.andr.Da r0 = r13.h     // Catch:{ Exception -> 0x02df }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x02df }
            r4 = 1
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x02df }
            r6 = 0
            r7 = 101(0x65, float:1.42E-43)
            r4[r6] = r7     // Catch:{ Exception -> 0x02df }
            r3.<init>(r4)     // Catch:{ Exception -> 0x02df }
            android.graphics.Bitmap r0 = com.wc.andr.utcl.x.c(r0, r3)     // Catch:{ Exception -> 0x02df }
        L_0x015a:
            android.widget.ImageView r3 = new android.widget.ImageView
            com.wc.andr.Da r4 = r13.h
            r3.<init>(r4)
            r13.f = r3
            android.widget.ImageView r3 = r13.f
            r4 = 8
            r3.setVisibility(r4)
            android.widget.ImageView r3 = r13.f
            r3.setImageBitmap(r0)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r3 = -2
            r4 = -2
            r0.<init>(r3, r4)
            r0.addRule(r10)
            r3 = 11
            r0.addRule(r3)
            android.widget.ImageView r3 = r13.f
            r3.setLayoutParams(r0)
            android.widget.ImageView r0 = r13.f
            r5.addView(r0)
            android.widget.ImageView r0 = r13.f
            com.wc.andr.utcl.b r3 = new com.wc.andr.utcl.b
            r3.<init>(r13)
            r0.setOnClickListener(r3)
            android.widget.RelativeLayout r6 = new android.widget.RelativeLayout
            com.wc.andr.Da r0 = r13.h
            r6.<init>(r0)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r0.<init>(r9, r2)
            r3 = 12
            r0.addRule(r3)
            r6.setLayoutParams(r0)
            r0 = 100
            r3 = 229(0xe5, float:3.21E-43)
            r4 = 234(0xea, float:3.28E-43)
            r7 = 227(0xe3, float:3.18E-43)
            int r0 = android.graphics.Color.argb(r0, r3, r4, r7)
            r6.setBackgroundColor(r0)
            android.widget.RelativeLayout$LayoutParams r7 = new android.widget.RelativeLayout$LayoutParams
            int r0 = r2 * 3
            r7.<init>(r0, r2)
            r7.addRule(r10)
            r0 = 9
            r7.addRule(r0)
            android.widget.RelativeLayout$LayoutParams r8 = new android.widget.RelativeLayout$LayoutParams
            int r0 = r2 * 3
            r8.<init>(r0, r2)
            r8.addRule(r10)
            r0 = 11
            r8.addRule(r0)
            com.wc.andr.Da r0 = r13.h     // Catch:{ Exception -> 0x02e3 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x02e3 }
            r3 = 1
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x02e3 }
            r4 = 0
            r9 = 97
            r3[r4] = r9     // Catch:{ Exception -> 0x02e3 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x02e3 }
            android.graphics.Bitmap r3 = com.wc.andr.utcl.x.c(r0, r2)     // Catch:{ Exception -> 0x02e3 }
            com.wc.andr.Da r0 = r13.h     // Catch:{ Exception -> 0x02ed }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x02ed }
            r4 = 1
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x02ed }
            r9 = 0
            r10 = 98
            r4[r9] = r10     // Catch:{ Exception -> 0x02ed }
            r2.<init>(r4)     // Catch:{ Exception -> 0x02ed }
            android.graphics.Bitmap r2 = com.wc.andr.utcl.x.c(r0, r2)     // Catch:{ Exception -> 0x02ed }
            com.wc.andr.Da r0 = r13.h     // Catch:{ Exception -> 0x02f2 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x02f2 }
            r9 = 1
            byte[] r9 = new byte[r9]     // Catch:{ Exception -> 0x02f2 }
            r10 = 0
            r11 = 99
            r9[r10] = r11     // Catch:{ Exception -> 0x02f2 }
            r4.<init>(r9)     // Catch:{ Exception -> 0x02f2 }
            android.graphics.Bitmap r0 = com.wc.andr.utcl.x.c(r0, r4)     // Catch:{ Exception -> 0x02f2 }
            com.wc.andr.Da r4 = r13.h     // Catch:{ Exception -> 0x02f6 }
            java.lang.String r9 = new java.lang.String     // Catch:{ Exception -> 0x02f6 }
            r10 = 1
            byte[] r10 = new byte[r10]     // Catch:{ Exception -> 0x02f6 }
            r11 = 0
            r12 = 100
            r10[r11] = r12     // Catch:{ Exception -> 0x02f6 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x02f6 }
            android.graphics.Bitmap r1 = com.wc.andr.utcl.x.c(r4, r9)     // Catch:{ Exception -> 0x02f6 }
        L_0x021f:
            android.widget.Button r4 = new android.widget.Button
            com.wc.andr.Da r9 = r13.h
            r4.<init>(r9)
            android.widget.Button r9 = new android.widget.Button
            com.wc.andr.Da r10 = r13.h
            r9.<init>(r10)
            r4.setLayoutParams(r8)
            r9.setLayoutParams(r7)
            if (r3 == 0) goto L_0x023d
            android.graphics.drawable.BitmapDrawable r7 = new android.graphics.drawable.BitmapDrawable
            r7.<init>(r3)
            r4.setBackgroundDrawable(r7)
        L_0x023d:
            if (r2 == 0) goto L_0x0247
            android.graphics.drawable.BitmapDrawable r7 = new android.graphics.drawable.BitmapDrawable
            r7.<init>(r2)
            r9.setBackgroundDrawable(r7)
        L_0x0247:
            r6.addView(r4)
            r6.addView(r9)
            r5.addView(r6)
            com.wc.andr.Da r6 = r13.h
            r6.setContentView(r5)
            android.webkit.WebView r5 = r13.g
            com.wc.andr.utcl.g r6 = new com.wc.andr.utcl.g
            r6.<init>(r13)
            r5.setOnTouchListener(r6)
            android.webkit.WebView r5 = r13.g
            com.wc.andr.utcl.h r6 = new com.wc.andr.utcl.h
            r6.<init>(r13)
            r5.setWebViewClient(r6)
            com.wc.andr.utcl.i r5 = new com.wc.andr.utcl.i
            r5.<init>(r13)
            r4.setOnClickListener(r5)
            com.wc.andr.utcl.j r5 = new com.wc.andr.utcl.j
            r5.<init>(r13)
            r9.setOnClickListener(r5)
            com.wc.andr.utcl.k r5 = new com.wc.andr.utcl.k
            r5.<init>(r13, r0, r3)
            r4.setOnTouchListener(r5)
            com.wc.andr.utcl.l r0 = new com.wc.andr.utcl.l
            r0.<init>(r13, r1, r2)
            r9.setOnTouchListener(r0)
            goto L_0x00de
        L_0x028b:
            com.wc.andr.Da r0 = r13.h
            boolean r0 = com.wc.andr.utcl.x.b(r0)
            if (r0 == 0) goto L_0x02a4
            com.wc.andr.Da r0 = r13.h
            boolean r0 = com.wc.andr.utcl.x.a(r0)
            if (r0 == 0) goto L_0x02a4
            android.webkit.WebView r0 = r13.g
            java.lang.String r3 = r13.l
            r0.loadUrl(r3)
            goto L_0x013a
        L_0x02a4:
            com.wc.andr.Da r0 = r13.h
            boolean r0 = com.wc.andr.utcl.x.b(r0)
            if (r0 == 0) goto L_0x02ba
            com.wc.andr.Da r0 = r13.h
            java.lang.String r1 = r13.l
            java.lang.String r2 = r13.a
            com.wc.andr.utcl.f r3 = new com.wc.andr.utcl.f
            r3.<init>(r13, r1, r0, r2)
            r3.start()
        L_0x02ba:
            com.wc.andr.Da r0 = r13.h
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = com.wc.andr.utcl.A.q
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "..."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r8)
            r0.show()
            com.wc.andr.Da r0 = r13.h
            r0.finish()
            goto L_0x00de
        L_0x02df:
            r0 = move-exception
            r0 = r1
            goto L_0x015a
        L_0x02e3:
            r0 = move-exception
            r4 = r0
            r2 = r1
            r3 = r1
            r0 = r1
        L_0x02e8:
            r4.printStackTrace()
            goto L_0x021f
        L_0x02ed:
            r0 = move-exception
            r4 = r0
            r2 = r1
            r0 = r1
            goto L_0x02e8
        L_0x02f2:
            r0 = move-exception
            r4 = r0
            r0 = r1
            goto L_0x02e8
        L_0x02f6:
            r4 = move-exception
            goto L_0x02e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.C0002a.a():void");
    }

    public final void b() {
        String str = this.o + "morelist.jsp";
        File file = new File(str);
        if (file.exists() || x.b(this.h)) {
            RelativeLayout relativeLayout = new RelativeLayout(this.h);
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            relativeLayout.setBackgroundColor(-1);
            this.g = new WebView(this.h);
            this.g.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.g.getSettings().setJavaScriptEnabled(true);
            if (file.exists()) {
                this.g.loadUrl("file://" + str);
            } else {
                this.g.loadUrl(C0008v.a);
            }
            this.g.setScrollBarStyle(32768);
            relativeLayout.addView(this.g);
            Bitmap bitmap = null;
            try {
                bitmap = x.c(this.h, "e");
            } catch (Exception e2) {
            }
            this.f = new ImageView(this.h);
            this.f.setVisibility(8);
            this.f.setImageBitmap(bitmap);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(11);
            this.f.setLayoutParams(layoutParams);
            relativeLayout.addView(this.f);
            this.f.setOnClickListener(new m(this));
            this.h.setContentView(relativeLayout);
            this.g.setOnTouchListener(new n(this));
            this.g.setWebViewClient(new C0004c(this));
            this.g.setDownloadListener(new C0005d(this));
            return;
        }
        Toast.makeText(this.h, A.p, 0).show();
        this.h.finish();
    }

    public final void c() {
        if (this.n == null) {
            if (this.g.canGoBack()) {
                this.g.goBack();
            } else {
                this.h.finish();
            }
        } else if (System.currentTimeMillis() - this.p > 1500) {
            Toast.makeText(this.h, A.v, 0).show();
            this.p = System.currentTimeMillis();
        } else {
            new o().a(this.h, this.i, this.m, this.k, 2, this.a, this.n, this.j, this.l, 1);
            this.h.finish();
        }
    }
}
