package com.wc.andr.utcl;

import org.json.JSONObject;

public final class s extends JSONObject {
    public s() {
    }

    public s(String str) {
        super(str);
    }

    public final Object get(String str) {
        if (has(str)) {
            return super.get(str);
        }
        return null;
    }

    public final int getInt(String str) {
        if (has(str)) {
            return super.getInt(str);
        }
        return 0;
    }

    public final String getString(String str) {
        return has(str) ? super.getString(str) : "";
    }
}
