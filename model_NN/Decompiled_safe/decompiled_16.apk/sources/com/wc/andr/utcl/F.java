package com.wc.andr.utcl;

import android.content.Context;
import android.net.wifi.WifiManager;

public final class F {
    private static WifiManager.WifiLock a;

    public static void a() {
        if (a != null && a.isHeld()) {
            a.release();
        }
    }

    public static void a(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
        if (wifiManager != null) {
            if (a == null) {
                WifiManager.WifiLock createWifiLock = wifiManager.createWifiLock("FSer");
                a = createWifiLock;
                createWifiLock.setReferenceCounted(true);
            }
            if (!a.isHeld()) {
                a.acquire();
            }
        }
    }
}
