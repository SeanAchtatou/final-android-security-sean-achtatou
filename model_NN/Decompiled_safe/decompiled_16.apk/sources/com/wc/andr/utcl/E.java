package com.wc.andr.utcl;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.WindowManager;
import com.wc.andr.a.c;

public final class E {
    Context a;
    String b;
    String c;
    String d;
    String e;
    String f;
    String g;
    Bitmap h;
    byte[] i = new byte[0];
    private WindowManager j;
    private V k;

    public final void a() {
        this.j.removeView(this.k);
    }

    public final void a(Context context, String str, String str2, String str3, String str4, String str5, String str6, Bitmap bitmap) {
        this.a = context;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = bitmap;
        if (bitmap != null) {
            this.i = x.a(bitmap);
        }
        this.j = (WindowManager) context.getApplicationContext().getSystemService("window");
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.format = 1;
        layoutParams.type = 2002;
        layoutParams.flags |= 8;
        layoutParams.gravity = 51;
        this.k = new V(this.j, layoutParams, context, this);
        layoutParams.x = this.j.getDefaultDisplay().getWidth();
        int a2 = c.a(context, x.b() + A.X);
        layoutParams.y = this.k.a * a2;
        layoutParams.width = -2;
        layoutParams.height = -2;
        this.j.addView(this.k, layoutParams);
        c.b(context, x.b() + A.X, Integer.valueOf(a2 + 1));
    }
}
