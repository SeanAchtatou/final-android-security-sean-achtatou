package com.wc.andr;

import android.content.SharedPreferences;
import com.wc.andr.a.c;
import com.wc.andr.utcl.A;
import com.wc.andr.utcl.o;
import com.wc.andr.utcl.x;
import java.io.File;

final class b implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ Fs d;
    private /* synthetic */ File e;

    b(Fs fs, String str, File file, String str2, String str3) {
        this.d = fs;
        this.a = str;
        this.e = file;
        this.b = str2;
        this.c = str3;
    }

    public final void run() {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            c cVar = new c(this.a, this.e);
            "------" + "FileDownloader加载" + "--" + (System.currentTimeMillis() - currentTimeMillis);
            x.a();
            c.b(Fs.c(this.d), this.d.e + this.b, Integer.valueOf(cVar.a()));
            cVar.a(Fs.c(this.d), new c(this));
        } catch (Exception e2) {
            SharedPreferences.Editor b2 = c.b(Fs.c(this.d));
            b2.remove(this.b + A.C);
            b2.commit();
            this.d.i.obtainMessage(-1).sendToTarget();
            o unused = this.d.h;
            o.d(Fs.c(this.d), this.c, this.a, this.b);
            this.d.stopSelf();
        }
    }
}
