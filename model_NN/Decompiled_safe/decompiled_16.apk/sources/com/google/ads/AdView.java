package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

public class AdView extends RelativeLayout implements Ad {
    private d a;

    public AdView(Activity activity, AdSize adSize, String adUnitId) {
        super(activity.getApplicationContext());
        if (a(activity, adSize, (AttributeSet) null)) {
            a(activity, adSize, adUnitId);
        }
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        a(context, attrs);
    }

    public AdView(Context context, AttributeSet attrs, int i) {
        this(context, attrs);
    }

    private void a(Activity activity, AdSize adSize, String str) {
        this.a = new d(activity, this, adSize, str, false);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.i(), (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), activity.getResources().getDisplayMetrics()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x014d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r9, android.util.AttributeSet r10) {
        /*
            r8 = this;
            r2 = 0
            if (r10 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            java.lang.String r0 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adSize"
            java.lang.String r0 = r10.getAttributeValue(r0, r1)
            if (r0 != 0) goto L_0x0016
            java.lang.String r0 = "AdView missing required XML attribute \"adSize\"."
            com.google.ads.AdSize r1 = com.google.ads.AdSize.BANNER
            r8.a(r9, r0, r1, r10)
            goto L_0x0003
        L_0x0016:
            java.lang.String r1 = "BANNER"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0030
            com.google.ads.AdSize r4 = com.google.ads.AdSize.BANNER
        L_0x0020:
            java.lang.String r0 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r1 = "adUnitId"
            java.lang.String r0 = r10.getAttributeValue(r0, r1)
            if (r0 != 0) goto L_0x0070
            java.lang.String r0 = "AdView missing required XML attribute \"adUnitId\"."
            r8.a(r9, r0, r4, r10)
            goto L_0x0003
        L_0x0030:
            java.lang.String r1 = "IAB_MRECT"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x003b
            com.google.ads.AdSize r4 = com.google.ads.AdSize.IAB_MRECT
            goto L_0x0020
        L_0x003b:
            java.lang.String r1 = "IAB_BANNER"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0046
            com.google.ads.AdSize r4 = com.google.ads.AdSize.IAB_BANNER
            goto L_0x0020
        L_0x0046:
            java.lang.String r1 = "IAB_LEADERBOARD"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0051
            com.google.ads.AdSize r4 = com.google.ads.AdSize.IAB_LEADERBOARD
            goto L_0x0020
        L_0x0051:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid \"adSize\" value in XML layout: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.google.ads.AdSize r1 = com.google.ads.AdSize.BANNER
            r8.a(r9, r0, r1, r10)
            goto L_0x0003
        L_0x0070:
            boolean r1 = r8.isInEditMode()
            if (r1 == 0) goto L_0x0080
            java.lang.String r2 = "Ads by Google"
            r3 = -1
            r0 = r8
            r1 = r9
            r5 = r10
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x0003
        L_0x0080:
            java.lang.String r1 = "@string/"
            boolean r1 = r0.startsWith(r1)
            if (r1 == 0) goto L_0x0145
            java.lang.String r1 = "@string/"
            int r1 = r1.length()
            java.lang.String r1 = r0.substring(r1)
            java.lang.String r3 = r9.getPackageName()
            android.util.TypedValue r5 = new android.util.TypedValue
            r5.<init>()
            android.content.res.Resources r6 = r8.getResources()     // Catch:{ NotFoundException -> 0x010a }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NotFoundException -> 0x010a }
            r7.<init>()     // Catch:{ NotFoundException -> 0x010a }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ NotFoundException -> 0x010a }
            java.lang.String r7 = ":string/"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ NotFoundException -> 0x010a }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ NotFoundException -> 0x010a }
            java.lang.String r1 = r1.toString()     // Catch:{ NotFoundException -> 0x010a }
            r3 = 1
            r6.getValue(r1, r5, r3)     // Catch:{ NotFoundException -> 0x010a }
            java.lang.CharSequence r1 = r5.string
            if (r1 == 0) goto L_0x0129
            java.lang.CharSequence r0 = r5.string
            java.lang.String r0 = r0.toString()
            r1 = r0
        L_0x00c5:
            java.lang.String r0 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "loadAdOnCreate"
            boolean r3 = r10.getAttributeBooleanValue(r0, r3, r2)
            boolean r0 = r9 instanceof android.app.Activity
            if (r0 == 0) goto L_0x014d
            r0 = r9
            android.app.Activity r0 = (android.app.Activity) r0
            boolean r5 = r8.a(r9, r4, r10)
            if (r5 == 0) goto L_0x0003
            r8.a(r0, r4, r1)
            if (r3 == 0) goto L_0x0003
            com.google.ads.AdRequest r1 = new com.google.ads.AdRequest
            r1.<init>()
            java.lang.String r0 = "http://schemas.android.com/apk/lib/com.google.ads"
            java.lang.String r3 = "keywords"
            java.lang.String r0 = r10.getAttributeValue(r0, r3)
            if (r0 == 0) goto L_0x0148
            java.lang.String r3 = ","
            java.lang.String[] r3 = r0.split(r3)
            int r4 = r3.length
            r0 = r2
        L_0x00f6:
            if (r0 >= r4) goto L_0x0148
            r2 = r3[r0]
            java.lang.String r2 = r2.trim()
            int r5 = r2.length()
            if (r5 == 0) goto L_0x0107
            r1.addKeyword(r2)
        L_0x0107:
            int r0 = r0 + 1
            goto L_0x00f6
        L_0x010a:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Could not find resource for \"adUnitId\": \""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "\"."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r8.a(r9, r0, r4, r10)
            goto L_0x0003
        L_0x0129:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "\"adUnitId\" was not a string: \""
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r3 = "\"."
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r8.a(r9, r1, r4, r10)
        L_0x0145:
            r1 = r0
            goto L_0x00c5
        L_0x0148:
            r8.loadAd(r1)
            goto L_0x0003
        L_0x014d:
            java.lang.String r0 = "AdView was initialized with a Context that wasn't an Activity."
            com.google.ads.util.a.b(r0)
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdView.a(android.content.Context, android.util.AttributeSet):void");
    }

    private void a(Context context, String str, int i, AdSize adSize, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(-16777216);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private void a(Context context, String str, AdSize adSize, AttributeSet attributeSet) {
        a.b(str);
        a(context, str, -65536, adSize, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, adSize, "");
        } else {
            a.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    private boolean a(Context context, AdSize adSize, AttributeSet attributeSet) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", adSize, attributeSet);
        return false;
    }

    public void destroy() {
        this.a.b();
    }

    public boolean isReady() {
        if (this.a == null) {
            return false;
        }
        return this.a.o();
    }

    public boolean isRefreshing() {
        return this.a.p();
    }

    public void loadAd(AdRequest adRequest) {
        if (isRefreshing()) {
            this.a.c();
        }
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.a(adListener);
    }

    public void stopLoading() {
        this.a.z();
    }
}
