package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.google.ads.util.a;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String ORIENTATION_PARAM = "o";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static d c = null;
    private static AdActivity d = null;
    private static AdActivity e = null;
    private g f;
    private long g;
    private RelativeLayout h;
    private AdActivity i = null;
    private boolean j;
    private VideoView k;

    private void a(g gVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (gVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (gVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            gVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundColor(0);
            imageButton.setOnClickListener(this);
            imageButton.setPadding(0, 0, 0, 0);
            int applyDimension = (int) TypedValue.applyDimension(1, 32.0f, getResources().getDisplayMetrics());
            FrameLayout frameLayout = new FrameLayout(getApplicationContext());
            frameLayout.addView(imageButton, applyDimension, applyDimension);
            this.h.addView(gVar, new ViewGroup.LayoutParams(-1, -1));
            this.h.addView(frameLayout);
            this.h.setKeepScreenOn(true);
            setContentView(this.h);
            if (z) {
                a.a(gVar);
            }
        }
    }

    private void a(String str) {
        a.b(str);
        finish();
    }

    public static boolean isShowing() {
        boolean z;
        synchronized (a) {
            z = d != null;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.google.ads.util.a.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        com.google.ads.util.a.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.a.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void launchAdActivity(defpackage.d r4, defpackage.e r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.e()
            if (r0 != 0) goto L_0x0024
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.a.e(r0)
        L_0x0015:
            return
        L_0x0016:
            d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.a.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        L_0x0024:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            goto L_0x0015
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.a.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.launchAdActivity(d, e):void");
    }

    public g getOpeningAdWebView() {
        if (this.i != null) {
            return this.i.f;
        }
        synchronized (a) {
            if (c == null) {
                a.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            g i2 = c.i();
            if (i2 != this.f) {
                return i2;
            }
            return null;
        }
    }

    public VideoView getVideoView() {
        return this.k;
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        a.d("Video finished playing.");
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        a.a(this.f, "onVideoEvent", "{'event': 'finish'}");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        r9.h = null;
        r9.j = false;
        r9.k = null;
        r0 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (r0 != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0062, code lost:
        r1 = new defpackage.e(r0);
        r0 = r1.b();
        r7 = r1.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0075, code lost:
        if (r0.equals("intent") == false) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0077, code lost:
        r9.f = null;
        r9.g = android.os.SystemClock.elapsedRealtime();
        r9.j = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        if (r7 != null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0089, code lost:
        r0 = r7.get(com.google.ads.AdActivity.URL_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0091, code lost:
        if (r0 != null) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0093, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0099, code lost:
        r1 = r7.get(com.google.ads.AdActivity.INTENT_ACTION_PARAM);
        r2 = r7.get(com.google.ads.AdActivity.TYPE_PARAM);
        r3 = android.net.Uri.parse(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ad, code lost:
        if (r1 != null) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00af, code lost:
        r0 = new android.content.Intent("android.intent.action.VIEW", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b6, code lost:
        r1 = com.google.ads.AdActivity.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b8, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bb, code lost:
        if (com.google.ads.AdActivity.b != null) goto L_0x00c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bd, code lost:
        com.google.ads.AdActivity.b = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c1, code lost:
        if (com.google.ads.AdActivity.c == null) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c3, code lost:
        com.google.ads.AdActivity.c.t();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c8, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        com.google.ads.util.a.a("Launching an intent from AdActivity.");
        startActivity(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d2, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d3, code lost:
        com.google.ads.util.a.a(r0.getMessage(), r0);
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00df, code lost:
        r0 = new android.content.Intent(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e4, code lost:
        if (r2 == null) goto L_0x00ea;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e6, code lost:
        r0.setDataAndType(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ea, code lost:
        r0.setData(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        com.google.ads.util.a.e("currentAdManager is null while trying to call onLeaveApplication().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f7, code lost:
        r9.h = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0108, code lost:
        if (r0.equals("webapp") == false) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x010a, code lost:
        r9.f = new defpackage.g(getApplicationContext(), null);
        r0 = new defpackage.h(r6, defpackage.a.b, true, true);
        r0.b();
        r9.f.setWebViewClient(r0);
        r0 = r7.get(com.google.ads.AdActivity.URL_PARAM);
        r1 = r7.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r2 = r7.get(com.google.ads.AdActivity.HTML_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x013c, code lost:
        if (r0 == null) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x013e, code lost:
        r9.f.loadUrl(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0143, code lost:
        r0 = r7.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0151, code lost:
        if ("p".equals(r0) == false) goto L_0x0171;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0153, code lost:
        r0 = com.google.ads.util.AdUtil.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0157, code lost:
        a(r9.f, false, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x015e, code lost:
        if (r2 == null) goto L_0x016a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0160, code lost:
        r9.f.loadDataWithBaseURL(r1, r2, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x016a, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0177, code lost:
        if ("l".equals(r0) == false) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0179, code lost:
        r0 = com.google.ads.util.AdUtil.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0180, code lost:
        if (r9 != com.google.ads.AdActivity.d) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0182, code lost:
        r0 = r6.m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0187, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x018f, code lost:
        if (r0.equals("interstitial") == false) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0191, code lost:
        r9.f = r6.i();
        a(r9.f, true, r6.m());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a2, code lost:
        a("Unknown AdOpener, <action: " + r0 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r8 = 0
            r3 = 1
            r5 = 0
            super.onCreate(r10)
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            d r0 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0058
            d r6 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x005f }
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0018
            com.google.ads.AdActivity.d = r9     // Catch:{ all -> 0x005f }
            r6.s()     // Catch:{ all -> 0x005f }
        L_0x0018:
            com.google.ads.AdActivity r0 = r9.i     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0024
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x0024
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x005f }
            r9.i = r0     // Catch:{ all -> 0x005f }
        L_0x0024:
            com.google.ads.AdActivity.e = r9     // Catch:{ all -> 0x005f }
            com.google.ads.Ad r0 = r6.f()     // Catch:{ all -> 0x005f }
            boolean r2 = r0 instanceof com.google.ads.AdView     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x0032
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r2 == r9) goto L_0x003c
        L_0x0032:
            boolean r0 = r0 instanceof com.google.ads.InterstitialAd     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x003f
            com.google.ads.AdActivity r0 = r9.i     // Catch:{ all -> 0x005f }
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r0 != r2) goto L_0x003f
        L_0x003c:
            r6.u()     // Catch:{ all -> 0x005f }
        L_0x003f:
            monitor-exit(r1)     // Catch:{ all -> 0x005f }
            r9.h = r5
            r9.j = r8
            r9.k = r5
            android.content.Intent r0 = r9.getIntent()
            java.lang.String r1 = "com.google.ads.AdOpener"
            android.os.Bundle r0 = r0.getBundleExtra(r1)
            if (r0 != 0) goto L_0x0062
            java.lang.String r0 = "Could not get the Bundle used to create AdActivity."
            r9.a(r0)
        L_0x0057:
            return
        L_0x0058:
            java.lang.String r0 = "Could not get currentAdManager."
            r9.a(r0)     // Catch:{ all -> 0x005f }
            monitor-exit(r1)     // Catch:{ all -> 0x005f }
            goto L_0x0057
        L_0x005f:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0062:
            e r1 = new e
            r1.<init>(r0)
            java.lang.String r0 = r1.b()
            java.util.HashMap r7 = r1.c()
            java.lang.String r1 = "intent"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00f7
            r9.f = r5
            long r0 = android.os.SystemClock.elapsedRealtime()
            r9.g = r0
            r9.j = r3
            if (r7 != 0) goto L_0x0089
            java.lang.String r0 = "Could not get the paramMap in launchIntent()"
            r9.a(r0)
            goto L_0x0057
        L_0x0089:
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0099
            java.lang.String r0 = "Could not get the URL parameter in launchIntent()."
            r9.a(r0)
            goto L_0x0057
        L_0x0099:
            java.lang.String r1 = "i"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "m"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            android.net.Uri r3 = android.net.Uri.parse(r0)
            if (r1 != 0) goto L_0x00df
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.<init>(r1, r3)
        L_0x00b6:
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.b     // Catch:{ all -> 0x00f4 }
            if (r2 != 0) goto L_0x00c8
            com.google.ads.AdActivity.b = r9     // Catch:{ all -> 0x00f4 }
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00f4 }
            if (r2 == 0) goto L_0x00ee
            d r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00f4 }
            r2.t()     // Catch:{ all -> 0x00f4 }
        L_0x00c8:
            monitor-exit(r1)     // Catch:{ all -> 0x00f4 }
            java.lang.String r1 = "Launching an intent from AdActivity."
            com.google.ads.util.a.a(r1)     // Catch:{ ActivityNotFoundException -> 0x00d2 }
            r9.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x00d2 }
            goto L_0x0057
        L_0x00d2:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.a.a(r1, r0)
            r9.finish()
            goto L_0x0057
        L_0x00df:
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1)
            if (r2 == 0) goto L_0x00ea
            r0.setDataAndType(r3, r2)
            goto L_0x00b6
        L_0x00ea:
            r0.setData(r3)
            goto L_0x00b6
        L_0x00ee:
            java.lang.String r2 = "currentAdManager is null while trying to call onLeaveApplication()."
            com.google.ads.util.a.e(r2)     // Catch:{ all -> 0x00f4 }
            goto L_0x00c8
        L_0x00f4:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x00f7:
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r2 = r9.getApplicationContext()
            r1.<init>(r2)
            r9.h = r1
            java.lang.String r1 = "webapp"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0189
            g r0 = new g
            android.content.Context r1 = r9.getApplicationContext()
            r0.<init>(r1, r5)
            r9.f = r0
            h r0 = new h
            java.util.Map<java.lang.String, i> r1 = defpackage.a.b
            r0.<init>(r6, r1, r3, r3)
            r0.b()
            g r1 = r9.f
            r1.setWebViewClient(r0)
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "baseurl"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "html"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r0 == 0) goto L_0x015e
            g r1 = r9.f
            r1.loadUrl(r0)
        L_0x0143:
            java.lang.String r0 = "o"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0171
            int r0 = com.google.ads.util.AdUtil.b()
        L_0x0157:
            g r1 = r9.f
            r9.a(r1, r8, r0)
            goto L_0x0057
        L_0x015e:
            if (r2 == 0) goto L_0x016a
            g r0 = r9.f
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x0143
        L_0x016a:
            java.lang.String r0 = "Could not get the URL or HTML parameter to show a web app."
            r9.a(r0)
            goto L_0x0057
        L_0x0171:
            java.lang.String r1 = "l"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x017e
            int r0 = com.google.ads.util.AdUtil.a()
            goto L_0x0157
        L_0x017e:
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.d
            if (r9 != r0) goto L_0x0187
            int r0 = r6.m()
            goto L_0x0157
        L_0x0187:
            r0 = -1
            goto L_0x0157
        L_0x0189:
            java.lang.String r1 = "interstitial"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01a2
            g r0 = r6.i()
            r9.f = r0
            int r0 = r6.m()
            g r1 = r9.f
            r9.a(r1, r3, r0)
            goto L_0x0057
        L_0x01a2:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ">"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.a(r0)
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.h != null) {
            this.h.removeAllViews();
        }
        if (this.f != null) {
            a.b(this.f);
            this.f.a(null);
        }
        if (isFinishing()) {
            if (this.k != null) {
                this.k.stopPlayback();
                this.k = null;
            }
            synchronized (a) {
                if (!(c == null || this.f == null)) {
                    if (this.f == c.i()) {
                        c.a();
                    }
                    this.f.stopLoading();
                    this.f.destroy();
                }
                if (this == d) {
                    if (c != null) {
                        c.r();
                        c = null;
                    } else {
                        a.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    d = null;
                }
            }
            if (this == b) {
                b = null;
            }
            e = this.i;
        }
        a.a("AdActivity is closing.");
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        a.e("Video threw error! <what:" + what + ", extra:" + extra + ">");
        finish();
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        a.d("Video is ready to play.");
        a.a(this.f, "onVideoEvent", "{'event': 'load'}");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (this.j && hasFocus && SystemClock.elapsedRealtime() - this.g > 250) {
            a.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public void showVideo(VideoView videoView) {
        this.k = videoView;
        if (this.f == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.f.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setOnErrorListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.h.addView(linearLayout, 0, layoutParams);
    }
}
