package com.zylist;

import android.app.Activity;
import android.content.Intent;

public class Test {
    public void start(Activity a, String ms) {
        Class c = null;
        try {
            c = Class.forName(ms);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        a.startActivity(new Intent(a, c));
    }
}
