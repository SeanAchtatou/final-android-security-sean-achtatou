package com.zylist;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import klkl.flkd.jkou.ChJk;

public class ListActivity extends Activity {
    ApplicationInfo appInfo;
    public String msg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChJk.getInstance(this);
        try {
            this.appInfo = getPackageManager().getApplicationInfo(getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.msg = this.appInfo.metaData.getString("StartActivity");
        new Test().start(this, this.msg);
        finish();
    }
}
