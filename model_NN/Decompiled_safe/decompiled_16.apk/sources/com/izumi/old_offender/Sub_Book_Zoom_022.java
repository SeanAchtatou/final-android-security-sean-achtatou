package com.izumi.old_offender;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.izumi.old_offenderzykj.R;

public class Sub_Book_Zoom_022 extends Activity {
    final int ACTIVITY_NUM = 22;
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_DRAWABLE_SUUJI = this.FAC.get_R_Drawable_Suuji();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    int load_sound3;
    /* access modifiers changed from: private */
    public ImageView m_suuji_zone_001;
    /* access modifiers changed from: private */
    public ImageView m_suuji_zone_002;
    /* access modifiers changed from: private */
    public ImageView m_suuji_zone_003;
    /* access modifiers changed from: private */
    public ImageView m_suuji_zone_004;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_book_zoom);
        } else {
            setContentView((int) R.layout.w_layout_sub_book_zoom);
        }
        Intent GET_INTENT = getIntent();
        int INTENT_FROM_ITEM_LIST = GET_INTENT.getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[13], 1);
        this.load_sound3 = this.SPool.load(this, this.R_RAW_SOUND[7], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        final SharedPreferences.Editor ED_HAVE_ITEM = SP_HAVE_ITEM.edit();
        SharedPreferences SP_CLICK_ZONE = getSharedPreferences("click_zone", 0);
        final SharedPreferences SP_ANGO_001 = getSharedPreferences("ango_list001", 0);
        final SharedPreferences.Editor ED_ANGO_001 = SP_ANGO_001.edit();
        final int WHERE_FROM = GET_INTENT.getIntExtra("where_from", 1);
        this.m_suuji_zone_001 = (ImageView) findViewById(R.id.suuji_zone_001);
        this.m_suuji_zone_002 = (ImageView) findViewById(R.id.suuji_zone_002);
        this.m_suuji_zone_003 = (ImageView) findViewById(R.id.suuji_zone_003);
        this.m_suuji_zone_004 = (ImageView) findViewById(R.id.suuji_zone_004);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        ((ImageView) findViewById(R.id.under_b)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(Sub_Book_Zoom_022.this.getApplicationContext(), Item_select.class);
                intent.putExtra("where_from", WHERE_FROM);
                intent.putExtra("item_number", 7);
                Sub_Book_Zoom_022.this.startActivity(intent);
                Sub_Book_Zoom_022.this.finish();
                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
            }
        });
        if (SP_CLICK_ZONE.getInt("zone005", -100) == -100) {
            if (SP_ANGO_001.getInt("ango000", -100) == -100) {
                ED_ANGO_001.putInt("ango001", 0);
                ED_ANGO_001.putInt("ango002", 0);
                ED_ANGO_001.putInt("ango003", 0);
                ED_ANGO_001.putInt("ango004", 0);
                ED_ANGO_001.commit();
            }
            this.m_suuji_zone_001.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                    int ango_sp_001 = SP_ANGO_001.getInt("ango001", 0);
                    if (ango_sp_001 < 9) {
                        ED_ANGO_001.putInt("ango001", ango_sp_001 + 1);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_001.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[ango_sp_001 + 1]);
                    } else {
                        ED_ANGO_001.putInt("ango001", 0);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_001.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[0]);
                    }
                    if (SP_ANGO_001.getInt("ango001", 0) == 1 && SP_ANGO_001.getInt("ango002", 0) == 2 && SP_ANGO_001.getInt("ango003", 0) == 2 && SP_ANGO_001.getInt("ango004", 0) == 9) {
                        Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound3, 100.0f, 100.0f, 1, 0, 1.0f);
                        AlertDialog.Builder ad = new AlertDialog.Builder(Sub_Book_Zoom_022.this);
                        ad.setTitle((int) R.string.item_name_009);
                        ad.setMessage((int) R.string.book_open);
                        ad.setCancelable(false);
                        ad.setIcon(Sub_Book_Zoom_022.this.R_DRAWABLE_ITEM_IMAGE_S[9]);
                        final SharedPreferences.Editor editor = ED_ITEMS_LIST;
                        final SharedPreferences.Editor editor2 = ED_HAVE_ITEM;
                        final int i = WHERE_FROM;
                        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("item007", -200);
                                editor.putInt("item009", 9);
                                editor.commit();
                                editor2.putInt("now_have_item", -100);
                                editor2.putInt("what_item", -100);
                                editor2.commit();
                                Intent intent = new Intent(Sub_Book_Zoom_022.this, Items_list.class);
                                intent.putExtra("where_from", i);
                                intent.putExtra("select", 1);
                                Sub_Book_Zoom_022.this.startActivity(intent);
                                Sub_Book_Zoom_022.this.finish();
                                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
                            }
                        });
                        ad.create();
                        ad.show();
                    }
                }
            });
            this.m_suuji_zone_002.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                    int ango_sp_002 = SP_ANGO_001.getInt("ango002", 0);
                    if (ango_sp_002 < 9) {
                        ED_ANGO_001.putInt("ango002", ango_sp_002 + 1);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_002.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[ango_sp_002 + 1]);
                    } else {
                        ED_ANGO_001.putInt("ango002", 0);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_002.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[0]);
                    }
                    if (SP_ANGO_001.getInt("ango001", 0) == 1 && SP_ANGO_001.getInt("ango002", 0) == 2 && SP_ANGO_001.getInt("ango003", 0) == 2 && SP_ANGO_001.getInt("ango004", 0) == 9) {
                        Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound3, 100.0f, 100.0f, 1, 0, 1.0f);
                        AlertDialog.Builder ad = new AlertDialog.Builder(Sub_Book_Zoom_022.this);
                        ad.setTitle((int) R.string.item_name_009);
                        ad.setMessage((int) R.string.book_open);
                        ad.setCancelable(false);
                        ad.setIcon(Sub_Book_Zoom_022.this.R_DRAWABLE_ITEM_IMAGE_S[9]);
                        final SharedPreferences.Editor editor = ED_ITEMS_LIST;
                        final SharedPreferences.Editor editor2 = ED_HAVE_ITEM;
                        final int i = WHERE_FROM;
                        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("item007", -200);
                                editor.putInt("item009", 9);
                                editor.commit();
                                editor2.putInt("now_have_item", -100);
                                editor2.putInt("what_item", -100);
                                editor2.commit();
                                Intent intent = new Intent(Sub_Book_Zoom_022.this, Items_list.class);
                                intent.putExtra("where_from", i);
                                intent.putExtra("select", 1);
                                Sub_Book_Zoom_022.this.startActivity(intent);
                                Sub_Book_Zoom_022.this.finish();
                                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
                            }
                        });
                        ad.create();
                        ad.show();
                    }
                }
            });
            this.m_suuji_zone_003.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                    int ango_sp_003 = SP_ANGO_001.getInt("ango003", 0);
                    if (ango_sp_003 < 9) {
                        ED_ANGO_001.putInt("ango003", ango_sp_003 + 1);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_003.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[ango_sp_003 + 1]);
                    } else {
                        ED_ANGO_001.putInt("ango003", 0);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_003.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[0]);
                    }
                    if (SP_ANGO_001.getInt("ango001", 0) == 1 && SP_ANGO_001.getInt("ango002", 0) == 2 && SP_ANGO_001.getInt("ango003", 0) == 2 && SP_ANGO_001.getInt("ango004", 0) == 9) {
                        Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound3, 100.0f, 100.0f, 1, 0, 1.0f);
                        AlertDialog.Builder ad = new AlertDialog.Builder(Sub_Book_Zoom_022.this);
                        ad.setTitle((int) R.string.item_name_009);
                        ad.setMessage((int) R.string.book_open);
                        ad.setCancelable(false);
                        ad.setIcon(Sub_Book_Zoom_022.this.R_DRAWABLE_ITEM_IMAGE_S[9]);
                        final SharedPreferences.Editor editor = ED_ITEMS_LIST;
                        final SharedPreferences.Editor editor2 = ED_HAVE_ITEM;
                        final int i = WHERE_FROM;
                        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("item007", -200);
                                editor.putInt("item009", 9);
                                editor.commit();
                                editor2.putInt("now_have_item", -100);
                                editor2.putInt("what_item", -100);
                                editor2.commit();
                                Intent intent = new Intent(Sub_Book_Zoom_022.this, Items_list.class);
                                intent.putExtra("where_from", i);
                                intent.putExtra("select", 1);
                                Sub_Book_Zoom_022.this.startActivity(intent);
                                Sub_Book_Zoom_022.this.finish();
                                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
                            }
                        });
                        ad.create();
                        ad.show();
                    }
                }
            });
            this.m_suuji_zone_004.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                    int ango_sp_004 = SP_ANGO_001.getInt("ango004", 0);
                    if (ango_sp_004 < 9) {
                        ED_ANGO_001.putInt("ango004", ango_sp_004 + 1);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_004.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[ango_sp_004 + 1]);
                    } else {
                        ED_ANGO_001.putInt("ango004", 0);
                        ED_ANGO_001.commit();
                        Sub_Book_Zoom_022.this.m_suuji_zone_004.setImageResource(Sub_Book_Zoom_022.this.R_DRAWABLE_SUUJI[0]);
                    }
                    if (SP_ANGO_001.getInt("ango001", 0) == 1 && SP_ANGO_001.getInt("ango002", 0) == 2 && SP_ANGO_001.getInt("ango003", 0) == 2 && SP_ANGO_001.getInt("ango004", 0) == 9) {
                        Sub_Book_Zoom_022.this.SPool.play(Sub_Book_Zoom_022.this.load_sound3, 100.0f, 100.0f, 1, 0, 1.0f);
                        AlertDialog.Builder ad = new AlertDialog.Builder(Sub_Book_Zoom_022.this);
                        ad.setTitle((int) R.string.item_name_009);
                        ad.setMessage((int) R.string.book_open);
                        ad.setCancelable(false);
                        ad.setIcon(Sub_Book_Zoom_022.this.R_DRAWABLE_ITEM_IMAGE_S[9]);
                        final SharedPreferences.Editor editor = ED_ITEMS_LIST;
                        final SharedPreferences.Editor editor2 = ED_HAVE_ITEM;
                        final int i = WHERE_FROM;
                        ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putInt("item007", -200);
                                editor.putInt("item009", 9);
                                editor.commit();
                                editor2.putInt("now_have_item", -100);
                                editor2.putInt("what_item", -100);
                                editor2.commit();
                                Intent intent = new Intent(Sub_Book_Zoom_022.this, Items_list.class);
                                intent.putExtra("where_from", i);
                                intent.putExtra("select", 1);
                                Sub_Book_Zoom_022.this.startActivity(intent);
                                Sub_Book_Zoom_022.this.finish();
                                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
                            }
                        });
                        ad.create();
                        ad.show();
                    }
                }
            });
        }
        ImageView button_i = (ImageView) findViewById(R.id.item);
        final ImageView imageView = button_i;
        button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_i;
        button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Book_Zoom_022.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", WHERE_FROM);
                Sub_Book_Zoom_022.this.startActivity(intent);
                Sub_Book_Zoom_022.this.finish();
                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_e = (ImageView) findViewById(R.id.end);
        final ImageView imageView3 = button_e;
        button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView3.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        imageView3.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView4 = button_e;
        button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView4.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Book_Zoom_022.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 22);
                Sub_Book_Zoom_022.this.startActivity(intent);
                Sub_Book_Zoom_022.this.finish();
                Sub_Book_Zoom_022.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
