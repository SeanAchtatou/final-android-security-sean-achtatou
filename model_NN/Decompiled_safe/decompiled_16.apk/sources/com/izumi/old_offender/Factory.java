package com.izumi.old_offender;

import com.izumi.old_offenderzykj.R;

public final class Factory {
    private static final Factory INSTANCE = new Factory();
    private static final int[] R_DRAWABLE_ITEM_IMAGE_S = {R.drawable.item_small_000, R.drawable.item_small_001, R.drawable.item_small_002, R.drawable.item_small_003, R.drawable.item_small_004, R.drawable.item_small_005, R.drawable.item_small_006, R.drawable.item_small_007, R.drawable.item_small_008, R.drawable.item_small_009, R.drawable.item_small_010, R.drawable.item_small_011, R.drawable.item_small_012, R.drawable.item_small_013, R.drawable.item_small_014, R.drawable.item_small_015, R.drawable.item_small_016, R.drawable.item_small_017, R.drawable.item_small_018};
    private final int ACTIVITIES = 33;
    private final int BACKGROUNDS = 31;
    private final Class<?>[] CLASS_NAME = {_Dammy.class, Main_A_001.class, Main_B_002.class, Main_C_003.class, Main_D_004.class, Sub_Desk_005.class, Sub_Z_006.class, Sub_Bed_007.class, Sub_Toilet_008.class, Sub_Mado_009.class, Sub_Senmen_010.class, Sub_Toilet_waki_011.class, Sub_Senmen_waki_012.class, Sub_Desk_waki_013.class, Sub_Bed_waki_014.class, Sub_Desk_Open_015.class, Sub_Isu_016.class, Sub_Isu_Chalk_017.class, Sub_Mado_Soto_018.class, Sub_Deguchi_Soto_019.class, Sub_Mado_Tetsubo_Off_020.class, Sub_Deguchi_KeyHole_021.class, Sub_Book_Zoom_022.class, Sub_Mado_Ana_023.class, Finish_True_End_024.class, Finish_Bad_End_025.class, Sub_KeyUp_026.class, Sub_KabeUp_027.class, Sub_Isu_Chalk_Up_028.class, Help_029.class, Sub_Mado_Up_030.class, Game_Finish_031.class, Sub_Deguchi_KeyHole_Open_032.class};
    private final int CLICKS = 25;
    private final int ITEMS = 19;
    private final int[] R_DRAWABLE_ALFABET = {R.drawable.alfabet_z, R.drawable.alfabet_a, R.drawable.alfabet_b, R.drawable.alfabet_c, R.drawable.alfabet_d, R.drawable.alfabet_e, R.drawable.alfabet_f, R.drawable.alfabet_g, R.drawable.alfabet_h, R.drawable.alfabet_i, R.drawable.alfabet_j, R.drawable.alfabet_k, R.drawable.alfabet_l, R.drawable.alfabet_m, R.drawable.alfabet_n, R.drawable.alfabet_o, R.drawable.alfabet_p, R.drawable.alfabet_q, R.drawable.alfabet_r, R.drawable.alfabet_s, R.drawable.alfabet_t, R.drawable.alfabet_u, R.drawable.alfabet_v, R.drawable.alfabet_w, R.drawable.alfabet_x, R.drawable.alfabet_y, R.drawable.alfabet_z};
    private final int[] R_DRAWABLE_BACKGROUND = {R.drawable.background_000, R.drawable.background_000, R.drawable.background_005, R.drawable.background_010, R.drawable.background_015, R.drawable.deskback_005, R.drawable.zback_005, R.drawable.bedback_005, R.drawable.toiletback_005, R.drawable.madoback_005, R.drawable.senmenback_005, R.drawable.toilet_waki, R.drawable.senmen_waki, R.drawable.desk_waki, R.drawable.bed_waki, R.drawable.desk_open, R.drawable.isu_ura, R.drawable.isu_ura_chalk, R.drawable.mado_soto, R.drawable.deguchi_soto, R.drawable.tetsubo_off, R.drawable.keyhole, R.drawable.background_000, R.drawable.ana, R.drawable.finish_true_end, R.drawable.finish_bad_end, R.drawable.key_up, R.drawable.kabe_up, R.drawable.isu_ura_up, R.drawable.book_gattai_zoom, R.drawable.background_000};
    private final int[] R_DRAWABLE_ITEM_IMAGE_L = {R.drawable.item_large_000, R.drawable.item_large_001, R.drawable.item_large_002, R.drawable.item_large_003, R.drawable.item_large_004, R.drawable.item_large_005, R.drawable.item_large_006, R.drawable.item_large_007, R.drawable.item_large_008, R.drawable.item_large_009, R.drawable.item_large_010, R.drawable.item_large_011, R.drawable.item_large_012, R.drawable.item_large_013, R.drawable.item_large_014, R.drawable.item_large_015, R.drawable.item_large_016, R.drawable.item_large_017, R.drawable.item_large_018};
    private final int[] R_DRAWABLE_SUUJI = {R.drawable.suuji_0, R.drawable.suuji_1, R.drawable.suuji_2, R.drawable.suuji_3, R.drawable.suuji_4, R.drawable.suuji_5, R.drawable.suuji_6, R.drawable.suuji_7, R.drawable.suuji_8, R.drawable.suuji_9};
    private final int[] R_ID_ALFABET = {R.id.alfabet_ango_000, R.id.alfabet_ango_001, R.id.alfabet_ango_002, R.id.alfabet_ango_003, R.id.alfabet_ango_004, R.id.alfabet_ango_005, R.id.alfabet_ango_006, R.id.alfabet_ango_007, R.id.alfabet_ango_008, R.id.alfabet_ango_009, R.id.alfabet_ango_010, R.id.alfabet_ango_011, R.id.alfabet_ango_012, R.id.alfabet_ango_013, R.id.alfabet_ango_014, R.id.alfabet_ango_015, R.id.alfabet_ango_016, R.id.alfabet_ango_017, R.id.alfabet_ango_018, R.id.alfabet_ango_019, R.id.alfabet_ango_020, R.id.alfabet_ango_021, R.id.alfabet_ango_022, R.id.alfabet_ango_023, R.id.alfabet_ango_024, R.id.alfabet_ango_025, R.id.alfabet_ango_026};
    private final int[] R_ID_ITEM_IMAGE_L = {R.id.item_large_000, R.id.item_large_001, R.id.item_large_002, R.id.item_large_003, R.id.item_large_004, R.id.item_large_005, R.id.item_large_006, R.id.item_large_007, R.id.item_large_008, R.id.item_large_009, R.id.item_large_010, R.id.item_large_011, R.id.item_large_012, R.id.item_large_013, R.id.item_large_014, R.id.item_large_015, R.id.item_large_016, R.id.item_large_017, R.id.item_large_018};
    private final int[] R_ID_SUUJI = {R.id.suuji_zone_001, R.id.suuji_zone_002, R.id.suuji_zone_003, R.id.suuji_zone_004};
    private final int[] R_ITEM_GET_COMMENT = {R.string.item_get_comment_000, R.string.item_get_comment_001, R.string.item_get_comment_002, R.string.item_get_comment_003, R.string.item_get_comment_004, R.string.item_get_comment_005, R.string.item_get_comment_006, R.string.item_get_comment_007, R.string.item_get_comment_008, R.string.item_get_comment_009, R.string.item_get_comment_010, R.string.item_get_comment_011, R.string.item_get_comment_012, R.string.item_get_comment_013, R.string.item_get_comment_014, R.string.item_get_comment_015, R.string.item_get_comment_016, R.string.item_get_comment_017, R.string.item_get_comment_018};
    private final int[] R_ITEM_NAME = {R.string.item_name_000, R.string.item_name_001, R.string.item_name_002, R.string.item_name_003, R.string.item_name_004, R.string.item_name_005, R.string.item_name_006, R.string.item_name_007, R.string.item_name_008, R.string.item_name_009, R.string.item_name_010, R.string.item_name_011, R.string.item_name_012, R.string.item_name_013, R.string.item_name_014, R.string.item_name_015, R.string.item_name_016, R.string.item_name_017, R.string.item_name_018};
    private final int[] R_RAW_SOUND = {R.raw.tm2_put003, R.raw.item03, R.raw.beep01, R.raw.chari00, R.raw.cursor17, R.raw.kachi12, R.raw.kachi13, R.raw.kachi23, R.raw.kachi12, R.raw.sha05, R.raw.open16, R.raw.open56, R.raw.open59, R.raw.pi42, R.raw.type00, R.raw.insect01, R.raw.sha00, R.raw.pi38, R.raw.tm2_footstep000, R.raw.brushing, R.raw.pen03, R.raw.metal47, R.raw.opening_door_1, R.raw.page_flip01};
    private final int[] R_SELECT_ITEM_TEXT = {R.string.select_item_text_000, R.string.select_item_text_001, R.string.select_item_text_002, R.string.select_item_text_003, R.string.select_item_text_004, R.string.select_item_text_005, R.string.select_item_text_006, R.string.select_item_text_007, R.string.select_item_text_008, R.string.select_item_text_009, R.string.select_item_text_010, R.string.select_item_text_011, R.string.select_item_text_012, R.string.select_item_text_013, R.string.select_item_text_014, R.string.select_item_text_015, R.string.select_item_text_016, R.string.select_item_text_017, R.string.select_item_text_018};
    private final int SOUNDS = 24;

    private Factory() {
    }

    public static Factory get_Instance() {
        return INSTANCE;
    }

    public int get_Items() {
        return 19;
    }

    public int get_Clicks() {
        return 25;
    }

    public int get_Backgrounds() {
        return 31;
    }

    public int get_Sounds() {
        return 24;
    }

    public int[] get_R_Drawable_Item_Image_S() {
        int[] drw = new int[19];
        for (int i = 0; i < 19; i++) {
            drw[i] = R_DRAWABLE_ITEM_IMAGE_S[i];
        }
        return drw;
    }

    public int[] get_R_Drawable_Item_Image_L() {
        int[] drw = new int[19];
        for (int i = 0; i < 19; i++) {
            drw[i] = this.R_DRAWABLE_ITEM_IMAGE_L[i];
        }
        return drw;
    }

    public int[] get_R_Drawable_Background() {
        int[] dbg = new int[31];
        for (int i = 0; i < 31; i++) {
            dbg[i] = this.R_DRAWABLE_BACKGROUND[i];
        }
        return dbg;
    }

    public int[] get_R_Drawable_Alfabet() {
        int[] afb = new int[27];
        for (int i = 0; i < 27; i++) {
            afb[i] = this.R_DRAWABLE_ALFABET[i];
        }
        return afb;
    }

    public int[] get_R_Drawable_Suuji() {
        int[] sj = new int[10];
        for (int i = 0; i < 10; i++) {
            sj[i] = this.R_DRAWABLE_SUUJI[i];
        }
        return sj;
    }

    public int[] get_R_Id_Item_Image_L() {
        int[] id = new int[19];
        for (int i = 0; i < 19; i++) {
            id[i] = this.R_ID_ITEM_IMAGE_L[i];
        }
        return id;
    }

    public int[] get_R_Id_Alfabet() {
        int[] id = new int[27];
        for (int i = 0; i < 27; i++) {
            id[i] = this.R_ID_ALFABET[i];
        }
        return id;
    }

    public int[] get_R_Id_Suuji() {
        int[] sj = new int[4];
        for (int i = 0; i < 4; i++) {
            sj[i] = this.R_ID_SUUJI[i];
        }
        return sj;
    }

    public int[] get_R_Raw_Sound() {
        int[] rs = new int[24];
        for (int i = 0; i < 24; i++) {
            rs[i] = this.R_RAW_SOUND[i];
        }
        return rs;
    }

    public int[] get_R_Item_Name() {
        int[] gc = new int[19];
        for (int i = 0; i < 19; i++) {
            gc[i] = this.R_ITEM_NAME[i];
        }
        return gc;
    }

    public int[] get_R_Item_Get_Comment() {
        int[] gc = new int[19];
        for (int i = 0; i < 19; i++) {
            gc[i] = this.R_ITEM_GET_COMMENT[i];
        }
        return gc;
    }

    public int[] get_R_Select_Item_Text() {
        int[] stz = new int[19];
        for (int i = 0; i < 19; i++) {
            stz[i] = this.R_SELECT_ITEM_TEXT[i];
        }
        return stz;
    }

    public Class<?>[] get_Class_Name() {
        Class[] cn = new Class[33];
        for (int i = 0; i < 33; i++) {
            cn[i] = this.CLASS_NAME[i];
        }
        return cn;
    }
}
