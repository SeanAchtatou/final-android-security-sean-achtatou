package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Sub_Deguchi_KeyHole_Open_032 extends Activity {
    final int ACTIVITY_NUM = 32;
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ALFABET = this.FAC.get_R_Drawable_Alfabet();
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_ID_ALFABET = this.FAC.get_R_Id_Alfabet();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    int load_sound3;
    int load_sound4;
    int load_sound5;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_deguchi_keyhole_open);
        } else {
            setContentView((int) R.layout.w_layout_sub_deguchi_keyhole_open);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[2], 1);
        this.load_sound3 = this.SPool.load(this, this.R_RAW_SOUND[4], 1);
        this.load_sound4 = this.SPool.load(this, this.R_RAW_SOUND[11], 1);
        this.load_sound5 = this.SPool.load(this, this.R_RAW_SOUND[14], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        SharedPreferences SP_CLICK_ZONE = getSharedPreferences("click_zone", 0);
        final SharedPreferences.Editor ED_CLICK_ZONE = SP_CLICK_ZONE.edit();
        final SharedPreferences SP_ANGO_002 = getSharedPreferences("ango_list002", 0);
        final SharedPreferences.Editor ED_ANGO_002 = SP_ANGO_002.edit();
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            SharedPreferences sharedPreferences = SP_ITEMS_LIST;
            if (what_item == sharedPreferences.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        ((ImageView) findViewById(R.id.under_b)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Deguchi_KeyHole_Open_032.this.startActivity(new Intent(Sub_Deguchi_KeyHole_Open_032.this.getApplicationContext(), Sub_KeyUp_026.class));
                Sub_Deguchi_KeyHole_Open_032.this.finish();
                Sub_Deguchi_KeyHole_Open_032.this.overridePendingTransition(0, 0);
            }
        });
        if (SP_CLICK_ZONE.getInt("zone004", -100) == -100) {
            if (SP_ANGO_002.getInt("ango000", -100) == -100) {
                for (int i = 0; i < 27; i++) {
                    ED_ANGO_002.putInt("ango" + String.format("%1$03d", Integer.valueOf(i)), -100);
                }
                ED_ANGO_002.putInt("ango_view001", -100);
                ED_ANGO_002.putInt("ango_view002", -100);
                ED_ANGO_002.putInt("ango_view003", -100);
                ED_ANGO_002.putInt("ango_view004", -100);
                ED_ANGO_002.commit();
            }
            for (int i2 = 0; i2 < 27; i2++) {
                ((ImageView) findViewById(this.R_ID_ALFABET[i2])).setOnClickListener(new View.OnClickListener() {
                    int k;

                    public View.OnClickListener i_to_j(int j) {
                        this.k = j;
                        return this;
                    }

                    public void onClick(View view) {
                        Sub_Deguchi_KeyHole_Open_032.this.SPool.play(Sub_Deguchi_KeyHole_Open_032.this.load_sound5, 100.0f, 100.0f, 1, 0, 1.0f);
                        if (SP_ANGO_002.getInt("ango_view004", -100) == -100 && SP_ANGO_002.getInt("ango_view003", -100) != -100 && SP_ANGO_002.getInt("ango_view002", -100) != -100 && SP_ANGO_002.getInt("ango_view001", -100) != -100) {
                            ED_ANGO_002.putInt("ango" + String.format("%1$03d", Integer.valueOf(this.k)), this.k);
                            ED_ANGO_002.putInt("ango_view004", this.k);
                            ED_ANGO_002.commit();
                            ImageView alfabet_view_004 = (ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_004);
                            alfabet_view_004.setImageResource(Sub_Deguchi_KeyHole_Open_032.this.R_DRAWABLE_ALFABET[this.k]);
                            alfabet_view_004.setVisibility(0);
                            if (SP_ANGO_002.getInt("ango023", -100) == 23 && SP_ANGO_002.getInt("ango001", -100) == 1 && SP_ANGO_002.getInt("ango009", -100) == 9 && SP_ANGO_002.getInt("ango020", -100) == 20 && SP_ANGO_002.getInt("ango_view001", -100) == 20 && SP_ANGO_002.getInt("ango_view002", -100) == 9 && SP_ANGO_002.getInt("ango_view003", -100) == 1 && SP_ANGO_002.getInt("ango_view004", -100) == 23) {
                                Sub_Deguchi_KeyHole_Open_032.this.SPool.play(Sub_Deguchi_KeyHole_Open_032.this.load_sound3, 100.0f, 100.0f, 1, 0, 1.0f);
                                ED_CLICK_ZONE.putInt("zone004", 4);
                                ED_CLICK_ZONE.commit();
                                TextView text_zone = (TextView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.talk);
                                text_zone.setText((int) R.string.talk05);
                                text_zone.setVisibility(0);
                                Sub_Deguchi_KeyHole_Open_032.this.SPool.play(Sub_Deguchi_KeyHole_Open_032.this.load_sound4, 100.0f, 100.0f, 1, 0, 1.0f);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.under_b)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.item)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.end)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_001)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_002)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_003)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_004)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_005)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_006)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_007)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_008)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_009)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_010)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_011)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_012)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_013)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_014)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_015)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_016)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_017)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_018)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_019)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_020)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_021)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_022)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_023)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_024)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_025)).setVisibility(4);
                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_026)).setVisibility(4);
                                Sub_Deguchi_KeyHole_Open_032.this.HANDLER = new Handler();
                                new Thread(new Runnable() {
                                    public void run() {
                                        try {
                                            Thread.sleep(1500);
                                            Sub_Deguchi_KeyHole_Open_032.this.HANDLER.post(new Runnable() {
                                                public void run() {
                                                    Sub_Deguchi_KeyHole_Open_032.this.startActivity(new Intent(Sub_Deguchi_KeyHole_Open_032.this.getApplicationContext(), Sub_KeyUp_026.class));
                                                    Sub_Deguchi_KeyHole_Open_032.this.finish();
                                                    Sub_Deguchi_KeyHole_Open_032.this.overridePendingTransition(0, 0);
                                                }
                                            });
                                        } catch (InterruptedException e) {
                                        }
                                    }
                                }).start();
                                return;
                            }
                            Sub_Deguchi_KeyHole_Open_032.this.SPool.play(Sub_Deguchi_KeyHole_Open_032.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.under_b)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.item)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.end)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_001)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_002)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_003)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_004)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_005)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_006)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_007)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_008)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_009)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_010)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_011)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_012)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_013)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_014)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_015)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_016)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_017)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_018)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_019)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_020)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_021)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_022)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_023)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_024)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_025)).setVisibility(4);
                            ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_026)).setVisibility(4);
                            Sub_Deguchi_KeyHole_Open_032.this.HANDLER = new Handler();
                            final SharedPreferences.Editor editor = ED_ANGO_002;
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Thread.sleep(600);
                                        Handler access$2 = Sub_Deguchi_KeyHole_Open_032.this.HANDLER;
                                        final SharedPreferences.Editor editor = editor;
                                        access$2.post(new Runnable() {
                                            public void run() {
                                                Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_001).setVisibility(4);
                                                Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_002).setVisibility(4);
                                                Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_003).setVisibility(4);
                                                Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_004).setVisibility(4);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.under_b)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.item)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.end)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_001)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_002)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_003)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_004)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_005)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_006)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_007)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_008)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_009)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_010)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_011)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_012)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_013)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_014)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_015)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_016)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_017)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_018)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_019)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_020)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_021)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_022)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_023)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_024)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_025)).setVisibility(0);
                                                ((ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_ango_026)).setVisibility(0);
                                                for (int i = 0; i < 27; i++) {
                                                    editor.putInt("ango" + String.format("%1$03d", Integer.valueOf(i)), -100);
                                                }
                                                editor.putInt("ango_view001", -100);
                                                editor.putInt("ango_view002", -100);
                                                editor.putInt("ango_view003", -100);
                                                editor.putInt("ango_view004", -100);
                                                editor.commit();
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                    }
                                }
                            }).start();
                        } else if (SP_ANGO_002.getInt("ango_view003", -100) == -100 && SP_ANGO_002.getInt("ango_view002", -100) != -100 && SP_ANGO_002.getInt("ango_view001", -100) != -100) {
                            Log.d("Room001", "3tsu_me");
                            ED_ANGO_002.putInt("ango" + String.format("%1$03d", Integer.valueOf(this.k)), this.k);
                            ED_ANGO_002.putInt("ango_view003", this.k);
                            ED_ANGO_002.commit();
                            ImageView alfabet_view_003 = (ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_003);
                            alfabet_view_003.setImageResource(Sub_Deguchi_KeyHole_Open_032.this.R_DRAWABLE_ALFABET[this.k]);
                            alfabet_view_003.setVisibility(0);
                        } else if (SP_ANGO_002.getInt("ango_view002", -100) != -100 || SP_ANGO_002.getInt("ango_view001", -100) == -100) {
                            Log.d("Room001", "1tsu_me");
                            ED_ANGO_002.putInt("ango" + String.format("%1$03d", Integer.valueOf(this.k)), this.k);
                            ED_ANGO_002.putInt("ango_view001", this.k);
                            ED_ANGO_002.commit();
                            ImageView alfabet_view_001 = (ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_001);
                            alfabet_view_001.setImageResource(Sub_Deguchi_KeyHole_Open_032.this.R_DRAWABLE_ALFABET[this.k]);
                            alfabet_view_001.setVisibility(0);
                        } else {
                            Log.d("Room001", "2tsu_me");
                            ED_ANGO_002.putInt("ango" + String.format("%1$03d", Integer.valueOf(this.k)), this.k);
                            ED_ANGO_002.putInt("ango_view002", this.k);
                            ED_ANGO_002.commit();
                            ImageView alfabet_view_002 = (ImageView) Sub_Deguchi_KeyHole_Open_032.this.findViewById(R.id.alfabet_view_002);
                            alfabet_view_002.setImageResource(Sub_Deguchi_KeyHole_Open_032.this.R_DRAWABLE_ALFABET[this.k]);
                            alfabet_view_002.setVisibility(0);
                        }
                    }
                }.i_to_j(i2));
            }
        }
        ImageView button_i = (ImageView) findViewById(R.id.item);
        final ImageView imageView = button_i;
        button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_i;
        button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Deguchi_KeyHole_Open_032.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 21);
                Sub_Deguchi_KeyHole_Open_032.this.startActivity(intent);
                Sub_Deguchi_KeyHole_Open_032.this.finish();
                Sub_Deguchi_KeyHole_Open_032.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_e = (ImageView) findViewById(R.id.end);
        final ImageView imageView3 = button_e;
        button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView3.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        imageView3.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView4 = button_e;
        button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView4.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Deguchi_KeyHole_Open_032.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 32);
                Sub_Deguchi_KeyHole_Open_032.this.startActivity(intent);
                Sub_Deguchi_KeyHole_Open_032.this.finish();
                Sub_Deguchi_KeyHole_Open_032.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.SPool.play(this.load_sound4, 100.0f, 100.0f, 1, 0, 1.0f);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
