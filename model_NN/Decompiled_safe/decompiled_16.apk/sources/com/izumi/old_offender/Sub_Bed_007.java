package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Sub_Bed_007 extends Activity {
    final int ACTIVITY_NUM = 7;
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    Resources e_resources;
    int load_sound;
    int load_sound2;
    /* access modifiers changed from: private */
    public final int m_BITMAP_SIZE_000 = this.m_bitmap_000.length;
    /* access modifiers changed from: private */
    public Handler m_anim_handler_000;
    /* access modifiers changed from: private */
    public boolean m_anim_isRepeat = true;
    /* access modifiers changed from: private */
    public Message m_anim_message;
    /* access modifiers changed from: private */
    public int m_anim_page = 0;
    /* access modifiers changed from: private */
    public Bitmap[] m_bitmap_000 = new Bitmap[5];
    ImageView m_button_e;
    ImageView m_button_i;
    /* access modifiers changed from: private */
    public ImageView m_change_anim_zone;
    /* access modifiers changed from: private */
    public ImageView m_item_small_001;
    /* access modifiers changed from: private */
    public ImageView m_under_b;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        final SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        this.e_resources = getResources();
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_bed_pipe);
        } else {
            setContentView((int) R.layout.w_layout_sub_bed_pipe);
        }
        int item001_sp = SP_ITEMS_LIST.getInt("item001", -100);
        if (item001_sp != -100) {
            ((ImageView) findViewById(R.id.bedback_pipe_005)).setImageResource(R.drawable.bedback_005);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[5], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        this.m_change_anim_zone = (ImageView) findViewById(R.id.change_anim_zone);
        this.m_under_b = (ImageView) findViewById(R.id.under_b);
        this.m_button_i = (ImageView) findViewById(R.id.item);
        this.m_button_e = (ImageView) findViewById(R.id.end);
        this.m_item_small_001 = (ImageView) findViewById(R.id.item_small_001);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        this.m_anim_handler_000 = new Handler() {
            public void handleMessage(Message _msg) {
                Sub_Bed_007.this.m_change_anim_zone.setVisibility(0);
                if (Sub_Bed_007.this.m_anim_isRepeat) {
                    if (Sub_Bed_007.this.m_anim_page < Sub_Bed_007.this.m_BITMAP_SIZE_000 - 1) {
                        Sub_Bed_007.this.m_change_anim_zone.setImageBitmap(Sub_Bed_007.this.m_bitmap_000[Sub_Bed_007.this.m_anim_page]);
                        Sub_Bed_007.this.m_anim_handler_000.sendMessageDelayed(obtainMessage(), 100);
                    }
                    if (Sub_Bed_007.this.m_anim_page == Sub_Bed_007.this.m_BITMAP_SIZE_000 - 1) {
                        Sub_Bed_007.this.m_change_anim_zone.setImageBitmap(Sub_Bed_007.this.m_bitmap_000[Sub_Bed_007.this.m_anim_page]);
                        Sub_Bed_007.this.m_anim_handler_000.sendMessageDelayed(obtainMessage(), 15);
                    } else if (Sub_Bed_007.this.m_anim_page == Sub_Bed_007.this.m_BITMAP_SIZE_000) {
                        Sub_Bed_007.this.startActivity(new Intent(Sub_Bed_007.this.getApplicationContext(), Main_B_002.class));
                        Sub_Bed_007.this.finish();
                        Sub_Bed_007.this.overridePendingTransition(0, 0);
                        Sub_Bed_007.this.m_anim_isRepeat = false;
                    }
                    Sub_Bed_007 sub_Bed_007 = Sub_Bed_007.this;
                    sub_Bed_007.m_anim_page = sub_Bed_007.m_anim_page + 1;
                }
            }
        };
        this.m_under_b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Bed_007.this.m_under_b.setVisibility(4);
                Sub_Bed_007.this.m_item_small_001.setVisibility(4);
                Sub_Bed_007.this.m_button_i.setVisibility(4);
                Sub_Bed_007.this.m_button_e.setVisibility(4);
                Sub_Bed_007.this.m_bitmap_000[0] = BitmapFactory.decodeResource(Sub_Bed_007.this.e_resources, R.drawable.bedanim_004);
                Sub_Bed_007.this.m_bitmap_000[1] = BitmapFactory.decodeResource(Sub_Bed_007.this.e_resources, R.drawable.bedanim_003);
                Sub_Bed_007.this.m_bitmap_000[2] = BitmapFactory.decodeResource(Sub_Bed_007.this.e_resources, R.drawable.bedanim_002);
                Sub_Bed_007.this.m_bitmap_000[3] = BitmapFactory.decodeResource(Sub_Bed_007.this.e_resources, R.drawable.bedanim_001);
                Sub_Bed_007.this.m_bitmap_000[4] = BitmapFactory.decodeResource(Sub_Bed_007.this.e_resources, R.drawable.backanim_005);
                Sub_Bed_007.this.m_anim_message = Message.obtain();
                Sub_Bed_007.this.m_anim_handler_000.sendMessage(Sub_Bed_007.this.m_anim_message);
            }
        });
        if (item001_sp == -100) {
            this.m_item_small_001.setVisibility(0);
            this.m_item_small_001.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    int item003_sp = SP_ITEMS_LIST.getInt("item003", -100);
                    int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                    if (item003_sp == 3 && what_item == 3) {
                        Sub_Bed_007.this.m_under_b.setVisibility(4);
                        Sub_Bed_007.this.m_item_small_001.setVisibility(4);
                        Sub_Bed_007.this.m_button_i.setVisibility(4);
                        Sub_Bed_007.this.m_button_e.setVisibility(4);
                        TextView text_zone = (TextView) Sub_Bed_007.this.findViewById(R.id.talk);
                        text_zone.setText((int) R.string.talk03);
                        text_zone.setVisibility(0);
                        Sub_Bed_007.this.SPool.play(Sub_Bed_007.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                        ED_ITEMS_LIST.putInt("item001", 1);
                        ED_ITEMS_LIST.commit();
                        Sub_Bed_007.this.HANDLER = new Handler();
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    Thread.sleep(1010);
                                    Sub_Bed_007.this.HANDLER.post(new Runnable() {
                                        public void run() {
                                            Intent intent = new Intent(Sub_Bed_007.this.getApplicationContext(), Item_get.class);
                                            intent.putExtra("item_number", 1);
                                            intent.putExtra("where_from", 7);
                                            Sub_Bed_007.this.startActivity(intent);
                                            Sub_Bed_007.this.finish();
                                            Sub_Bed_007.this.overridePendingTransition(0, 0);
                                        }
                                    });
                                } catch (InterruptedException e) {
                                }
                            }
                        }).start();
                        return;
                    }
                    TextView text_zone2 = (TextView) Sub_Bed_007.this.findViewById(R.id.talk);
                    text_zone2.setText((int) R.string.talk01);
                    text_zone2.setVisibility(0);
                    ((Vibrator) Sub_Bed_007.this.getSystemService("vibrator")).vibrate(200);
                }
            });
        } else if (item001_sp == 1 || item001_sp == -200) {
            this.m_item_small_001.setVisibility(4);
        }
        this.m_button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Bed_007.this.m_button_i.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        Sub_Bed_007.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Bed_007.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Bed_007.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 7);
                Sub_Bed_007.this.startActivity(intent);
                Sub_Bed_007.this.finish();
                Sub_Bed_007.this.overridePendingTransition(0, 0);
            }
        });
        this.m_button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Bed_007.this.m_button_e.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        Sub_Bed_007.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Bed_007.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Bed_007.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 7);
                Sub_Bed_007.this.startActivity(intent);
                Sub_Bed_007.this.finish();
                Sub_Bed_007.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
