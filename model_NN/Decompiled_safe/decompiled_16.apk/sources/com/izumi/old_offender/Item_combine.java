package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Item_combine extends Activity {
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    final int ITEMS = this.FAC.get_Items();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_ID_ITEM_IMAGE_L = this.FAC.get_R_Id_Item_Image_L();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    int load_sound3;
    int load_sound4;
    int load_sound5;
    int load_sound6;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[7], 1);
        this.load_sound4 = this.SPool.load(this, this.R_RAW_SOUND[17], 1);
        this.load_sound5 = this.SPool.load(this, this.R_RAW_SOUND[19], 1);
        this.load_sound6 = this.SPool.load(this, this.R_RAW_SOUND[9], 1);
        do {
        } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
        this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_item_combine);
        } else {
            setContentView((int) R.layout.w_layout_item_combine);
        }
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        final SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        final SharedPreferences.Editor ED_HAVE_ITEM = SP_HAVE_ITEM.edit();
        final int WHERE_FROM = getIntent().getIntExtra("where_from", 1);
        ((ImageView) findViewById(R.id.combine_item_000)).setVisibility(4);
        ((ImageView) findViewById(R.id.combine_item_001)).setVisibility(4);
        ED_HAVE_ITEM.putInt("have_combine_item", -100);
        ED_HAVE_ITEM.putInt("combine_item_000", -100);
        ED_HAVE_ITEM.putInt("combine_item_001", -100);
        ED_HAVE_ITEM.commit();
        ImageView[] item_image_large_obj = new ImageView[this.ITEMS];
        int[] items_list_sp = new int[this.ITEMS];
        for (int i = 0; i < this.ITEMS; i++) {
            items_list_sp[i] = SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(i)), -100);
            if (!(items_list_sp[i] == -100 || items_list_sp[i] == -200)) {
                item_image_large_obj[i] = (ImageView) findViewById(this.R_ID_ITEM_IMAGE_L[i]);
                item_image_large_obj[i].setVisibility(0);
                item_image_large_obj[i].setOnClickListener(new View.OnClickListener() {
                    int k;

                    public View.OnClickListener i_to_j(int j) {
                        this.k = j;
                        return this;
                    }

                    public void onClick(View view) {
                        Item_combine.this.SPool.play(Item_combine.this.load_sound4, 1.0f, 1.0f, 0, 0, 1.0f);
                        ImageView combine_item_img_000 = (ImageView) Item_combine.this.findViewById(R.id.combine_item_000);
                        ImageView combine_item_img_001 = (ImageView) Item_combine.this.findViewById(R.id.combine_item_001);
                        SP_HAVE_ITEM.getInt("have_combine_item", -100);
                        SP_HAVE_ITEM.getInt("combine_item_000", -100);
                        SP_HAVE_ITEM.getInt("combine_item_001", -100);
                        if (SP_HAVE_ITEM.getInt("have_combine_item", -100) != 1 || SP_HAVE_ITEM.getInt("combine_item_000", -100) == -100) {
                            combine_item_img_000.setImageResource(Item_combine.this.R_DRAWABLE_ITEM_IMAGE_S[this.k]);
                            combine_item_img_000.setVisibility(0);
                            combine_item_img_001.setVisibility(4);
                            ED_HAVE_ITEM.putInt("have_combine_item", 1);
                            ED_HAVE_ITEM.putInt("combine_item_000", this.k);
                            ED_HAVE_ITEM.commit();
                            ((TextView) Item_combine.this.findViewById(R.id.talk)).setVisibility(4);
                            return;
                        }
                        combine_item_img_001.setImageResource(Item_combine.this.R_DRAWABLE_ITEM_IMAGE_S[this.k]);
                        combine_item_img_001.setVisibility(0);
                        ED_HAVE_ITEM.putInt("combine_item_001", this.k);
                        ED_HAVE_ITEM.commit();
                        if ((SP_HAVE_ITEM.getInt("combine_item_000", -100) == 1 && SP_HAVE_ITEM.getInt("combine_item_001", -100) == 2) || (SP_HAVE_ITEM.getInt("combine_item_001", -100) == 1 && SP_HAVE_ITEM.getInt("combine_item_000", -100) == 2)) {
                            Item_combine.this.SPool.play(Item_combine.this.load_sound6, 1.0f, 1.0f, 0, 0, 1.0f);
                            TextView text_zone = (TextView) Item_combine.this.findViewById(R.id.talk);
                            text_zone.setText((int) R.string.talk29);
                            text_zone.setVisibility(0);
                            ((ImageView) Item_combine.this.findViewById(R.id.combine_item_clear)).setVisibility(4);
                            ((ImageView) Item_combine.this.findViewById(R.id.to_main_back)).setVisibility(4);
                            int[] items_list_sp = new int[Item_combine.this.ITEMS];
                            ImageView[] item_image_large_obj = new ImageView[Item_combine.this.ITEMS];
                            for (int i = 0; i < Item_combine.this.ITEMS; i++) {
                                items_list_sp[i] = SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(i)), -100);
                                if (!(items_list_sp[i] == -100 || items_list_sp[i] == -200)) {
                                    item_image_large_obj[i] = (ImageView) Item_combine.this.findViewById(Item_combine.this.R_ID_ITEM_IMAGE_L[i]);
                                    item_image_large_obj[i].setVisibility(4);
                                }
                            }
                            combine_item_img_001.setVisibility(0);
                            ED_ITEMS_LIST.putInt("item001", -200);
                            ED_ITEMS_LIST.putInt("item002", -200);
                            ED_ITEMS_LIST.putInt("item008", 8);
                            ED_ITEMS_LIST.commit();
                            Item_combine.this.HANDLER = new Handler();
                            final int i2 = WHERE_FROM;
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Thread.sleep(1000);
                                        Handler access$2 = Item_combine.this.HANDLER;
                                        final int i = i2;
                                        access$2.post(new Runnable() {
                                            public void run() {
                                                Intent intent = new Intent(Item_combine.this.getApplicationContext(), Item_get.class);
                                                intent.putExtra("item_number", 8);
                                                intent.putExtra("from_combine", 100);
                                                intent.putExtra("where_from", i);
                                                Item_combine.this.startActivity(intent);
                                                Item_combine.this.finish();
                                                Item_combine.this.overridePendingTransition(0, 0);
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                    }
                                }
                            }).start();
                        } else {
                            combine_item_img_001.setVisibility(0);
                        }
                        if ((SP_HAVE_ITEM.getInt("combine_item_000", -100) == 4 && SP_HAVE_ITEM.getInt("combine_item_001", -100) == 11) || (SP_HAVE_ITEM.getInt("combine_item_001", -100) == 4 && SP_HAVE_ITEM.getInt("combine_item_000", -100) == 11)) {
                            Item_combine.this.SPool.play(Item_combine.this.load_sound5, 1.0f, 1.0f, 0, 0, 1.0f);
                            TextView text_zone2 = (TextView) Item_combine.this.findViewById(R.id.talk);
                            text_zone2.setText((int) R.string.talk21);
                            text_zone2.setVisibility(0);
                            ((ImageView) Item_combine.this.findViewById(R.id.combine_item_clear)).setVisibility(4);
                            ((ImageView) Item_combine.this.findViewById(R.id.to_main_back)).setVisibility(4);
                            int[] items_list_sp2 = new int[Item_combine.this.ITEMS];
                            ImageView[] item_image_large_obj2 = new ImageView[Item_combine.this.ITEMS];
                            for (int i3 = 0; i3 < Item_combine.this.ITEMS; i3++) {
                                items_list_sp2[i3] = SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(i3)), -100);
                                if (!(items_list_sp2[i3] == -100 || items_list_sp2[i3] == -200)) {
                                    item_image_large_obj2[i3] = (ImageView) Item_combine.this.findViewById(Item_combine.this.R_ID_ITEM_IMAGE_L[i3]);
                                    item_image_large_obj2[i3].setVisibility(4);
                                }
                            }
                            combine_item_img_001.setVisibility(0);
                            ED_ITEMS_LIST.putInt("item011", -200);
                            ED_ITEMS_LIST.putInt("item012", 12);
                            ED_ITEMS_LIST.commit();
                            Item_combine.this.HANDLER = new Handler();
                            final int i4 = WHERE_FROM;
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Thread.sleep(2000);
                                        Handler access$2 = Item_combine.this.HANDLER;
                                        final int i = i4;
                                        access$2.post(new Runnable() {
                                            public void run() {
                                                Intent intent = new Intent(Item_combine.this.getApplicationContext(), Item_get.class);
                                                intent.putExtra("item_number", 12);
                                                intent.putExtra("where_from", i);
                                                Item_combine.this.startActivity(intent);
                                                Item_combine.this.finish();
                                                Item_combine.this.overridePendingTransition(0, 0);
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                    }
                                }
                            }).start();
                        } else {
                            combine_item_img_001.setVisibility(0);
                        }
                        if ((SP_HAVE_ITEM.getInt("combine_item_000", -100) == 1 && SP_HAVE_ITEM.getInt("combine_item_001", -100) == 13) || (SP_HAVE_ITEM.getInt("combine_item_001", -100) == 1 && SP_HAVE_ITEM.getInt("combine_item_000", -100) == 13)) {
                            Item_combine.this.SPool.play(Item_combine.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                            TextView text_zone3 = (TextView) Item_combine.this.findViewById(R.id.talk);
                            text_zone3.setText((int) R.string.talk28);
                            text_zone3.setVisibility(0);
                            ((ImageView) Item_combine.this.findViewById(R.id.combine_item_clear)).setVisibility(4);
                            ((ImageView) Item_combine.this.findViewById(R.id.to_main_back)).setVisibility(4);
                            combine_item_img_001.setVisibility(0);
                            ED_ITEMS_LIST.putInt("item001", -200);
                            ED_ITEMS_LIST.putInt("item013", -200);
                            ED_ITEMS_LIST.putInt("item014", 14);
                            ED_ITEMS_LIST.commit();
                            Item_combine.this.HANDLER = new Handler();
                            final int i5 = WHERE_FROM;
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Thread.sleep(800);
                                        Handler access$2 = Item_combine.this.HANDLER;
                                        final int i = i5;
                                        access$2.post(new Runnable() {
                                            public void run() {
                                                Intent intent = new Intent(Item_combine.this.getApplicationContext(), Item_get.class);
                                                intent.putExtra("item_number", 14);
                                                intent.putExtra("where_from", i);
                                                Item_combine.this.startActivity(intent);
                                                Item_combine.this.finish();
                                                Item_combine.this.overridePendingTransition(0, 0);
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                    }
                                }
                            }).start();
                        } else {
                            combine_item_img_001.setVisibility(0);
                        }
                        if (SP_HAVE_ITEM.getInt("combine_item_000", -100) != 1 || SP_HAVE_ITEM.getInt("combine_item_001", -100) != 2) {
                            if (SP_HAVE_ITEM.getInt("combine_item_001", -100) != 1 || SP_HAVE_ITEM.getInt("combine_item_000", -100) != 2) {
                                if (SP_HAVE_ITEM.getInt("combine_item_000", -100) != 4 || SP_HAVE_ITEM.getInt("combine_item_001", -100) != 11) {
                                    if (SP_HAVE_ITEM.getInt("combine_item_001", -100) != 4 || SP_HAVE_ITEM.getInt("combine_item_000", -100) != 11) {
                                        if (SP_HAVE_ITEM.getInt("combine_item_000", -100) != 1 || SP_HAVE_ITEM.getInt("combine_item_001", -100) != 13) {
                                            if (SP_HAVE_ITEM.getInt("combine_item_001", -100) != 1 || SP_HAVE_ITEM.getInt("combine_item_000", -100) != 13) {
                                                TextView text_zone4 = (TextView) Item_combine.this.findViewById(R.id.talk);
                                                text_zone4.setText((int) R.string.no_combine);
                                                text_zone4.setVisibility(0);
                                                ED_HAVE_ITEM.putInt("have_combine_item", -100);
                                                ED_HAVE_ITEM.putInt("combine_item_000", -100);
                                                ED_HAVE_ITEM.putInt("combine_item_001", -100);
                                                ED_HAVE_ITEM.commit();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }.i_to_j(i));
            }
        }
        final ImageView button_clear = (ImageView) findViewById(R.id.combine_item_clear);
        button_clear.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_clear.setImageResource(R.drawable.bt_clear_001_15052);
                        return false;
                    case 1:
                        button_clear.setImageResource(R.drawable.bt_clear_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Item_combine.this.SPool.play(Item_combine.this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
                button_clear.setImageResource(R.drawable.bt_clear_000_15052);
                ((ImageView) Item_combine.this.findViewById(R.id.combine_item_000)).setVisibility(4);
                ((ImageView) Item_combine.this.findViewById(R.id.combine_item_001)).setVisibility(4);
                ED_HAVE_ITEM.putInt("have_combine_item", -100);
                ED_HAVE_ITEM.putInt("combine_item_000", -100);
                ED_HAVE_ITEM.putInt("combine_item_001", -100);
                ED_HAVE_ITEM.commit();
                ((TextView) Item_combine.this.findViewById(R.id.talk)).setVisibility(4);
            }
        });
        final ImageView button_back = (ImageView) findViewById(R.id.to_main_back);
        button_back.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_back.setImageResource(R.drawable.bt_back_001_15052);
                        return false;
                    case 1:
                        button_back.setImageResource(R.drawable.bt_back_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                button_back.setImageResource(R.drawable.bt_back_000_15052);
                ED_HAVE_ITEM.putInt("have_combine_item", -100);
                ED_HAVE_ITEM.putInt("combine_item_000", -100);
                ED_HAVE_ITEM.putInt("combine_item_001", -100);
                ED_HAVE_ITEM.commit();
                Intent intent = new Intent(Item_combine.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", WHERE_FROM);
                Item_combine.this.startActivity(intent);
                Item_combine.this.finish();
                Item_combine.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
