package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Sub_Deguchi_Soto_019 extends Activity {
    final int ACTIVITY_NUM = 19;
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    /* access modifiers changed from: private */
    public ImageView m_button_e;
    /* access modifiers changed from: private */
    public ImageView m_button_i;
    private ImageView m_button_u;
    /* access modifiers changed from: private */
    public ImageView m_item_image_015;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_deguchi_soto);
        } else {
            setContentView((int) R.layout.w_layout_sub_deguchi_soto);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[3], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        final SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        this.m_button_u = (ImageView) findViewById(R.id.under_b);
        this.m_item_image_015 = (ImageView) findViewById(R.id.item_small_015);
        this.m_button_i = (ImageView) findViewById(R.id.item);
        this.m_button_e = (ImageView) findViewById(R.id.end);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        this.m_button_u.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Deguchi_Soto_019.this.startActivity(new Intent(Sub_Deguchi_Soto_019.this.getApplicationContext(), Sub_KeyUp_026.class));
                Sub_Deguchi_Soto_019.this.finish();
                Sub_Deguchi_Soto_019.this.overridePendingTransition(0, 0);
            }
        });
        if (SP_ITEMS_LIST.getInt("item015", -100) == -100) {
            this.m_item_image_015.setVisibility(0);
            this.m_item_image_015.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    int item014_sp = SP_ITEMS_LIST.getInt("item014", -100);
                    int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                    if (item014_sp == 14 && what_item == 14) {
                        ED_ITEMS_LIST.putInt("item015", 15);
                        ED_ITEMS_LIST.commit();
                        Sub_Deguchi_Soto_019.this.SPool.play(Sub_Deguchi_Soto_019.this.load_sound2, 100.0f, 100.0f, 1, 0, 1.0f);
                        TextView text_zone = (TextView) Sub_Deguchi_Soto_019.this.findViewById(R.id.talk);
                        text_zone.setText((int) R.string.talk18);
                        text_zone.setVisibility(0);
                        ((ImageView) Sub_Deguchi_Soto_019.this.findViewById(R.id.under_b)).setVisibility(4);
                        ((ImageView) Sub_Deguchi_Soto_019.this.findViewById(R.id.item)).setVisibility(4);
                        ((ImageView) Sub_Deguchi_Soto_019.this.findViewById(R.id.end)).setVisibility(4);
                        Sub_Deguchi_Soto_019.this.m_item_image_015.setVisibility(4);
                        Sub_Deguchi_Soto_019.this.HANDLER = new Handler();
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    Thread.sleep(500);
                                    Sub_Deguchi_Soto_019.this.HANDLER.post(new Runnable() {
                                        public void run() {
                                            Intent intent = new Intent(Sub_Deguchi_Soto_019.this.getApplicationContext(), Item_get.class);
                                            intent.putExtra("item_number", 15);
                                            intent.putExtra("where_from", 19);
                                            Sub_Deguchi_Soto_019.this.startActivity(intent);
                                            Sub_Deguchi_Soto_019.this.finish();
                                            Sub_Deguchi_Soto_019.this.overridePendingTransition(0, 0);
                                        }
                                    });
                                } catch (InterruptedException e) {
                                }
                            }
                        }).start();
                        return;
                    }
                    TextView text_zone2 = (TextView) Sub_Deguchi_Soto_019.this.findViewById(R.id.talk);
                    text_zone2.setText((int) R.string.talk17);
                    text_zone2.setVisibility(0);
                }
            });
        } else {
            this.m_item_image_015.setVisibility(4);
        }
        this.m_button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Deguchi_Soto_019.this.m_button_i.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        Sub_Deguchi_Soto_019.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Deguchi_Soto_019.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Deguchi_Soto_019.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 19);
                Sub_Deguchi_Soto_019.this.startActivity(intent);
                Sub_Deguchi_Soto_019.this.finish();
                Sub_Deguchi_Soto_019.this.overridePendingTransition(0, 0);
            }
        });
        this.m_button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Deguchi_Soto_019.this.m_button_e.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        Sub_Deguchi_Soto_019.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Deguchi_Soto_019.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Deguchi_Soto_019.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 19);
                Sub_Deguchi_Soto_019.this.startActivity(intent);
                Sub_Deguchi_Soto_019.this.finish();
                Sub_Deguchi_Soto_019.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
