package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.izumi.old_offenderzykj.R;

public class Sub_Mado_Soto_018 extends Activity {
    final int ACTIVITY_NUM = 18;
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    int load_sound;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_mado_soto);
        } else {
            setContentView((int) R.layout.w_layout_sub_mado_soto);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        ((ImageView) findViewById(R.id.under_b)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Soto_018.this.startActivity(new Intent(Sub_Mado_Soto_018.this.getApplicationContext(), Sub_Mado_Up_030.class));
                Sub_Mado_Soto_018.this.finish();
                Sub_Mado_Soto_018.this.overridePendingTransition(0, 0);
            }
        });
        int item005_sp = SP_ITEMS_LIST.getInt("item005", -100);
        ImageView item_image_005 = (ImageView) findViewById(R.id.item_small_005);
        if (item005_sp == -100) {
            item_image_005.setVisibility(0);
            item_image_005.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ED_ITEMS_LIST.putInt("item005", 5);
                    ED_ITEMS_LIST.commit();
                    Intent intent = new Intent(Sub_Mado_Soto_018.this.getApplicationContext(), Item_get.class);
                    intent.putExtra("item_number", 5);
                    intent.putExtra("where_from", 18);
                    Sub_Mado_Soto_018.this.startActivity(intent);
                    Sub_Mado_Soto_018.this.finish();
                    Sub_Mado_Soto_018.this.overridePendingTransition(0, 0);
                }
            });
        } else {
            item_image_005.setVisibility(4);
        }
        int item013_sp = SP_ITEMS_LIST.getInt("item013", -100);
        ImageView item_image_013 = (ImageView) findViewById(R.id.item_small_013);
        if (item013_sp == -100) {
            item_image_013.setVisibility(0);
            item_image_013.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ED_ITEMS_LIST.putInt("item013", 13);
                    ED_ITEMS_LIST.commit();
                    Intent intent = new Intent(Sub_Mado_Soto_018.this.getApplicationContext(), Item_get.class);
                    intent.putExtra("item_number", 13);
                    intent.putExtra("where_from", 18);
                    Sub_Mado_Soto_018.this.startActivity(intent);
                    Sub_Mado_Soto_018.this.finish();
                    Sub_Mado_Soto_018.this.overridePendingTransition(0, 0);
                }
            });
        } else {
            item_image_013.setVisibility(4);
        }
        ImageView button_i = (ImageView) findViewById(R.id.item);
        final ImageView imageView = button_i;
        button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_i;
        button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Mado_Soto_018.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 18);
                Sub_Mado_Soto_018.this.startActivity(intent);
                Sub_Mado_Soto_018.this.finish();
                Sub_Mado_Soto_018.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_e = (ImageView) findViewById(R.id.end);
        final ImageView imageView3 = button_e;
        button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView3.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        imageView3.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView4 = button_e;
        button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView4.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Mado_Soto_018.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 18);
                Sub_Mado_Soto_018.this.startActivity(intent);
                Sub_Mado_Soto_018.this.finish();
                Sub_Mado_Soto_018.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
