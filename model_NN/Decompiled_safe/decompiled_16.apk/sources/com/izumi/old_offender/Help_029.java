package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.izumi.old_offenderzykj.R;
import java.util.Locale;
import mediba.ad.sdk.android.MasAdView;

public class Help_029 extends Activity {
    final int ACTIVITY_NUM = 29;
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    int load_sound;
    private MasAdView mad = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        do {
        } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
        this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_help);
        } else {
            setContentView((int) R.layout.w_layout_help);
        }
        if (Locale.JAPAN.equals(Locale.getDefault())) {
            this.mad = new MasAdView(this);
            this.mad.setRefreshAnimation(0);
            this.mad.setRequestInterval(30);
            this.mad.setEnabled(true);
            ((RelativeLayout) findViewById(R.id.layout_kokoku_000)).addView(this.mad);
        } else {
            AdView adView = new AdView(this, AdSize.BANNER, "a14d7ba593151f3");
            ((RelativeLayout) findViewById(R.id.layout_kokoku_001)).addView(adView);
            adView.loadAd(new AdRequest());
        }
        ((TextView) findViewById(R.id.help_text)).setText((int) R.string.help_text);
        final ImageView button_back = (ImageView) findViewById(R.id.back);
        button_back.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_back.setImageResource(R.drawable.bt_back_001_15052);
                        return false;
                    case 1:
                        button_back.setImageResource(R.drawable.bt_back_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                button_back.setImageResource(R.drawable.bt_back_000_15052);
                Intent intent = new Intent(Help_029.this.getApplicationContext(), Opening.class);
                intent.putExtra("from_item_list", 1);
                Help_029.this.startActivity(intent);
                Help_029.this.finish();
                Help_029.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mad != null) {
            this.mad.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
