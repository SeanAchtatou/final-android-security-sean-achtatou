package com.izumi.old_offender;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import com.izumi.old_offenderzykj.R;

public class Game_Finish_031 extends Activity {
    final Class<?>[] CLASS_NAME = this.FAC.get_Class_Name();
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    private SoundPool SPool;
    int load_sound;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.SPool = new SoundPool(this.R_RAW_SOUND.length, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        do {
        } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
        this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_game_finish);
        } else {
            setContentView((int) R.layout.w_layout_game_finish);
        }
        final int INTENT_WHERE_FROM = getIntent().getIntExtra("where_from", 1);
        AlertDialog.Builder adb2 = new AlertDialog.Builder(this);
        adb2.setTitle("EXIT");
        adb2.setMessage((int) R.string.end_finish);
        adb2.setCancelable(false);
        adb2.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Game_Finish_031.this.finish();
                Game_Finish_031.this.overridePendingTransition(0, 0);
            }
        });
        adb2.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Game_Finish_031.this.startActivity(new Intent(Game_Finish_031.this.getApplicationContext(), Game_Finish_031.this.CLASS_NAME[INTENT_WHERE_FROM]));
                Game_Finish_031.this.finish();
                Game_Finish_031.this.overridePendingTransition(0, 0);
            }
        });
        adb2.show();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
