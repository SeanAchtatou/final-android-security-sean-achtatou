package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.izumi.old_offenderzykj.R;

public class Items_list extends Activity {
    final Factory FAC = Factory.get_Instance();
    final int ITEMS = this.FAC.get_Items();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_ID_ITEM_IMAGE_L = this.FAC.get_R_Id_Item_Image_L();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    int load_sound;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_items_list);
        } else {
            setContentView((int) R.layout.w_layout_items_list);
        }
        SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        Intent GET_INTENT = getIntent();
        final int WHERE_FROM = GET_INTENT.getIntExtra("where_from", 1);
        final Class[] CLASS_NAME = this.FAC.get_Class_Name();
        int SELECT = GET_INTENT.getIntExtra("select", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        if (SELECT != 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        ImageView[] item_image_large_obj = new ImageView[this.ITEMS];
        int[] items_list_sp = new int[this.ITEMS];
        for (int i = 0; i < this.ITEMS; i++) {
            items_list_sp[i] = SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(i)), -100);
            if (!(items_list_sp[i] == -100 || items_list_sp[i] == -200)) {
                item_image_large_obj[i] = (ImageView) findViewById(this.R_ID_ITEM_IMAGE_L[i]);
                item_image_large_obj[i].setVisibility(0);
                item_image_large_obj[i].setOnClickListener(new View.OnClickListener() {
                    int k;

                    public View.OnClickListener i_to_j(int j) {
                        this.k = j;
                        return this;
                    }

                    public void onClick(View view) {
                        Intent intent = new Intent(Items_list.this.getApplicationContext(), Item_select.class);
                        intent.putExtra("where_from", WHERE_FROM);
                        intent.putExtra("item_number", this.k);
                        Items_list.this.startActivity(intent);
                        Items_list.this.finish();
                        Items_list.this.overridePendingTransition(0, 0);
                    }
                }.i_to_j(i));
            }
        }
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        ImageView button_combine = (ImageView) findViewById(R.id.to_combine);
        final ImageView imageView = button_combine;
        button_combine.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_combine_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_combine_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_combine;
        button_combine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_combine_000_15052);
                Intent intent = new Intent(Items_list.this.getApplicationContext(), Item_combine.class);
                intent.putExtra("where_from", WHERE_FROM);
                Items_list.this.startActivity(intent);
                Items_list.this.finish();
                Items_list.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_back = (ImageView) findViewById(R.id.to_main_back);
        final ImageView imageView3 = button_back;
        button_back.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView3.setImageResource(R.drawable.bt_back_001_15052);
                        return false;
                    case 1:
                        imageView3.setImageResource(R.drawable.bt_back_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView4 = button_back;
        button_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView4.setImageResource(R.drawable.bt_back_000_15052);
                Intent intent = new Intent(Items_list.this.getApplicationContext(), CLASS_NAME[WHERE_FROM]);
                intent.putExtra("from_item_list", 1);
                Items_list.this.startActivity(intent);
                Items_list.this.finish();
                Items_list.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
