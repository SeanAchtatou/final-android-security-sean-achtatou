package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.izumi.old_offenderzykj.R;

public class Finish_True_End_024 extends Activity {
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer = null;
    private MediaPlayer mediaPlayer2 = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.opdext);
        this.mediaPlayer.setLooping(true);
        this.mediaPlayer2 = MediaPlayer.create(this, (int) R.raw.opening_loop);
        this.mediaPlayer2.setLooping(true);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_finish_true_end);
        } else {
            setContentView((int) R.layout.w_layout_finish_true_end);
        }
        ((ImageView) findViewById(R.id.finish_true_end)).setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        SharedPreferences.Editor ED_ITEMS_LIST = getSharedPreferences("items_list", 0).edit();
        SharedPreferences.Editor ED_HAVE_ITEM = getSharedPreferences("have_item", 0).edit();
        SharedPreferences.Editor ED_CLICK_ZONE = getSharedPreferences("click_zone", 0).edit();
        ED_ITEMS_LIST.clear();
        ED_ITEMS_LIST.commit();
        ED_HAVE_ITEM.clear();
        ED_HAVE_ITEM.commit();
        ED_CLICK_ZONE.clear();
        ED_CLICK_ZONE.commit();
        this.HANDLER = new Handler();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(4000);
                    Finish_True_End_024.this.HANDLER.post(new Runnable() {
                        public void run() {
                            if (Finish_True_End_024.this.mediaPlayer != null) {
                                Finish_True_End_024.this.mediaPlayer.start();
                            }
                            final ImageView button_main = (ImageView) Finish_True_End_024.this.findViewById(R.id.go_to_main);
                            button_main.setOnTouchListener(new View.OnTouchListener() {
                                public boolean onTouch(View view, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case 0:
                                            button_main.setImageResource(R.drawable.bt_main_001_15052);
                                            return false;
                                        case 1:
                                            button_main.setImageResource(R.drawable.bt_main_000_15052);
                                            return false;
                                        default:
                                            return false;
                                    }
                                }
                            });
                            button_main.setVisibility(0);
                            button_main.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    button_main.setImageResource(R.drawable.bt_main_000_15052);
                                    Intent intent = new Intent(Finish_True_End_024.this.getApplicationContext(), Opening.class);
                                    intent.putExtra("from_item_list", 1);
                                    Finish_True_End_024.this.startActivity(intent);
                                    Finish_True_End_024.this.finish();
                                    Finish_True_End_024.this.overridePendingTransition(0, 0);
                                }
                            });
                            final ImageView button_goal = (ImageView) Finish_True_End_024.this.findViewById(R.id.go_to_web);
                            button_goal.setOnTouchListener(new View.OnTouchListener() {
                                public boolean onTouch(View view, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case 0:
                                            button_goal.setImageResource(R.drawable.bt_goal_001_15052);
                                            return false;
                                        case 1:
                                            button_goal.setImageResource(R.drawable.bt_goal_000_15052);
                                            return false;
                                        default:
                                            return false;
                                    }
                                }
                            });
                            button_goal.setVisibility(0);
                            button_goal.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    button_goal.setImageResource(R.drawable.bt_goal_000_15052);
                                    Finish_True_End_024.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Finish_True_End_024.this.getString(R.string.url_true))));
                                    Finish_True_End_024.this.finish();
                                    Finish_True_End_024.this.overridePendingTransition(0, 0);
                                }
                            });
                            final ImageView button_end = (ImageView) Finish_True_End_024.this.findViewById(R.id.go_to_end);
                            button_end.setOnTouchListener(new View.OnTouchListener() {
                                public boolean onTouch(View view, MotionEvent event) {
                                    switch (event.getAction()) {
                                        case 0:
                                            button_end.setImageResource(R.drawable.bt_end_001_15052);
                                            return false;
                                        case 1:
                                            button_end.setImageResource(R.drawable.bt_end_000_15052);
                                            return false;
                                        default:
                                            return false;
                                    }
                                }
                            });
                            button_end.setVisibility(0);
                            button_end.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    button_end.setImageResource(R.drawable.bt_end_000_15052);
                                    Finish_True_End_024.this.finish();
                                    Finish_True_End_024.this.overridePendingTransition(0, 0);
                                }
                            });
                        }
                    });
                } catch (InterruptedException e) {
                }
            }
        }).start();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayer2 != null) {
            this.mediaPlayer2.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer = null;
        }
        if (this.mediaPlayer2 != null) {
            if (this.mediaPlayer2.isPlaying()) {
                this.mediaPlayer2.stop();
            }
            this.mediaPlayer2 = null;
        }
        SharedPreferences.Editor ED_TETSUBO = getSharedPreferences("tetsubo", 0).edit();
        ED_TETSUBO.putInt("tetsubo001", 0);
        ED_TETSUBO.putInt("tetsubo002", 0);
        ED_TETSUBO.putInt("tetsubo003", 0);
        ED_TETSUBO.putInt("tetsubo004", 0);
        ED_TETSUBO.commit();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
