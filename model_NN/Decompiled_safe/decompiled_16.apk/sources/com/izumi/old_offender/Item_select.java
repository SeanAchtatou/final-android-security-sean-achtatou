package com.izumi.old_offender;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Item_select extends Activity {
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_L = this.FAC.get_R_Drawable_Item_Image_L();
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    int load_sound3;
    int load_sound4;
    int load_sound5;
    int load_sound6;
    int page = 1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[16], 1);
        this.load_sound3 = this.SPool.load(this, this.R_RAW_SOUND[7], 1);
        this.load_sound4 = this.SPool.load(this, this.R_RAW_SOUND[23], 1);
        this.load_sound5 = this.SPool.load(this, this.R_RAW_SOUND[19], 1);
        this.load_sound6 = this.SPool.load(this, this.R_RAW_SOUND[9], 1);
        this.HANDLER = new Handler();
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_item_select);
        } else {
            setContentView((int) R.layout.w_layout_item_select);
        }
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        final SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        final SharedPreferences.Editor ED_HAVE_ITEM = SP_HAVE_ITEM.edit();
        Intent GET_INTENT = getIntent();
        final int WHERE_FROM = GET_INTENT.getIntExtra("where_from", 1);
        final int ITEM_NUMBER = GET_INTENT.getIntExtra("item_number", 0);
        int[] R_SELECT_ITEM_TEXT = this.FAC.get_R_Select_Item_Text();
        final SharedPreferences SP_CLICK_ZONE = getSharedPreferences("click_zone", 0);
        final SharedPreferences.Editor ED_CLICK_ZONE = SP_CLICK_ZONE.edit();
        ((ImageView) findViewById(R.id.item_select_image_zone)).setImageResource(this.R_DRAWABLE_ITEM_IMAGE_L[ITEM_NUMBER]);
        TextView text_zone = (TextView) findViewById(R.id.talk);
        text_zone.setText(R_SELECT_ITEM_TEXT[ITEM_NUMBER]);
        text_zone.setVisibility(0);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        final ImageView button_select = (ImageView) findViewById(R.id.select);
        button_select.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_select.setImageResource(R.drawable.bt_select_001_15052);
                        return false;
                    case 1:
                        button_select.setImageResource(R.drawable.bt_select_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                button_select.setImageResource(R.drawable.bt_select_000_15052);
                Item_select.this.SPool.play(Item_select.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                ((ImageView) Item_select.this.findViewById(R.id.select)).setVisibility(4);
                ((ImageView) Item_select.this.findViewById(R.id.back)).setVisibility(4);
                ((ImageView) Item_select.this.findViewById(R.id.separate)).setVisibility(4);
                ED_HAVE_ITEM.putInt("now_have_item", 1);
                ED_HAVE_ITEM.putInt("what_item", ITEM_NUMBER);
                ED_HAVE_ITEM.commit();
                int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
                ImageView now_soubi_item = (ImageView) Item_select.this.findViewById(R.id.now_soubi_item);
                if (now_have_item == -100) {
                    now_soubi_item.setImageResource(Item_select.this.R_DRAWABLE_ITEM_IMAGE_S[0]);
                    now_soubi_item.setVisibility(4);
                } else {
                    int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                    if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                        now_soubi_item.setImageResource(Item_select.this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
                    }
                }
                final int i = WHERE_FROM;
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            Thread.sleep(200);
                            Handler access$1 = Item_select.this.HANDLER;
                            final int i = i;
                            access$1.post(new Runnable() {
                                public void run() {
                                    Intent intent = new Intent(Item_select.this, Items_list.class);
                                    intent.putExtra("where_from", i);
                                    intent.putExtra("select", 1);
                                    Item_select.this.startActivity(intent);
                                    Item_select.this.finish();
                                    Item_select.this.overridePendingTransition(0, 0);
                                }
                            });
                        } catch (InterruptedException e) {
                        }
                    }
                }).start();
            }
        });
        ImageView button_back = (ImageView) findViewById(R.id.back);
        final ImageView imageView = button_back;
        button_back.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_back_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_back_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_back;
        button_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_back_000_15052);
                Intent intent = new Intent(Item_select.this, Items_list.class);
                intent.putExtra("where_from", WHERE_FROM);
                Item_select.this.startActivity(intent);
                Item_select.this.finish();
                Item_select.this.overridePendingTransition(0, 0);
            }
        });
        if (ITEM_NUMBER == 11) {
            ImageView clickzone_050 = (ImageView) findViewById(R.id.clickzone_050);
            clickzone_050.setVisibility(0);
            clickzone_050.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (SP_HAVE_ITEM.getInt("what_item", -100) == 4) {
                        Item_select.this.SPool.play(Item_select.this.load_sound5, 1.0f, 1.0f, 0, 0, 1.0f);
                        TextView text_zone = (TextView) Item_select.this.findViewById(R.id.talk);
                        text_zone.setText((int) R.string.talk21);
                        text_zone.setVisibility(0);
                        ((ImageView) Item_select.this.findViewById(R.id.select)).setVisibility(4);
                        ((ImageView) Item_select.this.findViewById(R.id.back)).setVisibility(4);
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_050)).setVisibility(4);
                        ED_ITEMS_LIST.putInt("item011", -200);
                        ED_ITEMS_LIST.putInt("item012", 12);
                        ED_ITEMS_LIST.commit();
                        final int i = WHERE_FROM;
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    Thread.sleep(2000);
                                    Handler access$1 = Item_select.this.HANDLER;
                                    final int i = i;
                                    access$1.post(new Runnable() {
                                        public void run() {
                                            Intent intent = new Intent(Item_select.this.getApplicationContext(), Item_get.class);
                                            intent.putExtra("item_number", 12);
                                            intent.putExtra("where_from", i);
                                            Item_select.this.startActivity(intent);
                                            Item_select.this.finish();
                                            Item_select.this.overridePendingTransition(0, 0);
                                        }
                                    });
                                } catch (InterruptedException e) {
                                }
                            }
                        }).start();
                    }
                }
            });
        }
        if (ITEM_NUMBER == 8) {
            final ImageView button_separate = (ImageView) findViewById(R.id.separate);
            button_separate.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent event) {
                    switch (event.getAction()) {
                        case 0:
                            button_separate.setImageResource(R.drawable.bt_separate_001_15052);
                            return false;
                        case 1:
                            button_separate.setImageResource(R.drawable.bt_separate_000_15052);
                            return false;
                        default:
                            return false;
                    }
                }
            });
            button_separate.setVisibility(0);
            final SharedPreferences.Editor editor = ED_HAVE_ITEM;
            final int i = WHERE_FROM;
            button_separate.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Item_select.this.SPool.play(Item_select.this.load_sound6, 1.0f, 1.0f, 1, 0, 1.0f);
                    button_separate.setImageResource(R.drawable.bt_separate_000_15052);
                    ED_ITEMS_LIST.putInt("item001", 1);
                    ED_ITEMS_LIST.putInt("item002", 2);
                    ED_ITEMS_LIST.putInt("item008", -100);
                    ED_ITEMS_LIST.commit();
                    editor.putInt("now_have_item", -100);
                    editor.putInt("what_item", -100);
                    editor.commit();
                    AlertDialog.Builder cancelable = new AlertDialog.Builder(Item_select.this).setTitle("SEPARATE").setMessage((int) R.string.item_008_restore_item_001_item_002).setCancelable(false);
                    final int i = i;
                    cancelable.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Item_select.this, Items_list.class);
                            intent.putExtra("where_from", i);
                            intent.putExtra("select", 1);
                            Item_select.this.startActivity(intent);
                            Item_select.this.finish();
                            Item_select.this.overridePendingTransition(0, 0);
                        }
                    }).show();
                }
            });
        }
        if (ITEM_NUMBER == 7) {
            ImageView click_zone_012 = (ImageView) findViewById(R.id.clickzone_012);
            click_zone_012.setVisibility(0);
            click_zone_012.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(Item_select.this, Sub_Book_Zoom_022.class);
                    intent.putExtra("where_from", WHERE_FROM);
                    Item_select.this.startActivity(intent);
                    Item_select.this.finish();
                    Item_select.this.overridePendingTransition(0, 0);
                }
            });
        }
        if (ITEM_NUMBER == 9) {
            ImageView next_page = (ImageView) findViewById(R.id.next_page);
            next_page.setVisibility(0);
            final SharedPreferences sharedPreferences = SP_ITEMS_LIST;
            final SharedPreferences sharedPreferences2 = SP_HAVE_ITEM;
            final SharedPreferences.Editor editor2 = ED_ITEMS_LIST;
            final SharedPreferences.Editor editor3 = ED_HAVE_ITEM;
            final int i2 = WHERE_FROM;
            next_page.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Item_select.this.SPool.play(Item_select.this.load_sound4, 1.0f, 1.0f, 1, 0, 1.2f);
                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.item_large_009);
                    Item_select.this.findViewById(R.id.next_page).setVisibility(0);
                    Item_select.this.page++;
                    if (Item_select.this.page == 1) {
                        return;
                    }
                    if (Item_select.this.page == 2) {
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_1page);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page002);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(0);
                        ImageView click_zone_028 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                        click_zone_028.setVisibility(0);
                        click_zone_028.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_1page_kakudai);
                            }
                        });
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                    } else if (Item_select.this.page == 3) {
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_2page);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page003);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(0);
                        ImageView click_zone_0282 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                        click_zone_0282.setVisibility(0);
                        click_zone_0282.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_2page_kakudai);
                            }
                        });
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                    } else if (Item_select.this.page == 4) {
                        if (SP_CLICK_ZONE.getInt("zone007", -100) == 7) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_gattai);
                            ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai);
                            ImageView click_zone_0283 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0283.setVisibility(0);
                            click_zone_0283.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                }
                            });
                        } else if (SP_CLICK_ZONE.getInt("zone007", -100) == 15) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi);
                            ImageView click_zone_0284 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0284.setVisibility(0);
                            final SharedPreferences sharedPreferences = sharedPreferences;
                            final SharedPreferences sharedPreferences2 = sharedPreferences2;
                            final SharedPreferences.Editor editor = editor2;
                            final SharedPreferences.Editor editor2 = editor3;
                            final SharedPreferences.Editor editor3 = ED_CLICK_ZONE;
                            click_zone_0284.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    int item015_sp = sharedPreferences.getInt("item015", -100);
                                    int item016_sp = sharedPreferences.getInt("item016", -100);
                                    if (item015_sp == -200 && item016_sp != -200) {
                                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi_kakudai);
                                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                        ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                        click_zone_029.setVisibility(0);
                                        final SharedPreferences sharedPreferences = sharedPreferences;
                                        final SharedPreferences sharedPreferences2 = sharedPreferences2;
                                        final SharedPreferences.Editor editor = editor;
                                        final SharedPreferences.Editor editor2 = editor2;
                                        final SharedPreferences.Editor editor3 = editor3;
                                        click_zone_029.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                int item016_sp = sharedPreferences.getInt("item016", -100);
                                                int what_item = sharedPreferences2.getInt("what_item", -100);
                                                if (item016_sp == 16 && what_item == 16) {
                                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                                    ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_hamatta);
                                                    Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                    editor.putInt("item016", -200);
                                                    editor.commit();
                                                    editor2.putInt("now_have_item", -100);
                                                    editor2.putInt("what_item", -100);
                                                    editor2.commit();
                                                    editor3.putInt("zone007", 7);
                                                    editor3.commit();
                                                } else if (item016_sp != -200 && item016_sp != 16) {
                                                    AlertDialog.Builder cancelable = new AlertDialog.Builder(Item_select.this).setMessage((int) R.string.key_hazusu).setCancelable(false);
                                                    final SharedPreferences.Editor editor = editor;
                                                    final SharedPreferences.Editor editor2 = editor3;
                                                    cancelable.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            editor.putInt("item015", 15);
                                                            editor.commit();
                                                            editor2.putInt("zone007", -100);
                                                            editor2.commit();
                                                            Intent intent = new Intent(Item_select.this, Items_list.class);
                                                            intent.putExtra("select", 1);
                                                            Item_select.this.startActivity(intent);
                                                            Item_select.this.finish();
                                                            Item_select.this.overridePendingTransition(0, 0);
                                                        }
                                                    }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent intent = new Intent(Item_select.this, Items_list.class);
                                                            intent.putExtra("select", 1);
                                                            Item_select.this.startActivity(intent);
                                                            Item_select.this.finish();
                                                            Item_select.this.overridePendingTransition(0, 0);
                                                        }
                                                    }).show();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        } else if (SP_CLICK_ZONE.getInt("zone007", -100) == 16) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen);
                            ImageView click_zone_0285 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0285.setVisibility(0);
                            final SharedPreferences sharedPreferences3 = sharedPreferences;
                            final SharedPreferences sharedPreferences4 = sharedPreferences2;
                            final SharedPreferences.Editor editor4 = editor2;
                            final SharedPreferences.Editor editor5 = editor3;
                            final SharedPreferences.Editor editor6 = ED_CLICK_ZONE;
                            click_zone_0285.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    int item015_sp = sharedPreferences3.getInt("item015", -100);
                                    if (sharedPreferences3.getInt("item016", -100) == -200 && item015_sp != -200) {
                                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen_kakudai);
                                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                        ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                        click_zone_029.setVisibility(0);
                                        final SharedPreferences sharedPreferences = sharedPreferences3;
                                        final SharedPreferences sharedPreferences2 = sharedPreferences4;
                                        final SharedPreferences.Editor editor = editor4;
                                        final SharedPreferences.Editor editor2 = editor5;
                                        final SharedPreferences.Editor editor3 = editor6;
                                        click_zone_029.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                int item015_sp = sharedPreferences.getInt("item015", -100);
                                                int what_item = sharedPreferences2.getInt("what_item", -100);
                                                if (item015_sp == 15 && what_item == 15) {
                                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                                    Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                    ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_hamatta);
                                                    editor.putInt("item015", -200);
                                                    editor.commit();
                                                    editor2.putInt("now_have_item", -100);
                                                    editor2.putInt("what_item", -100);
                                                    editor2.commit();
                                                    editor3.putInt("zone007", 7);
                                                    editor3.commit();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        } else {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_seikai);
                            ImageView click_zone_0286 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0286.setVisibility(0);
                            final SharedPreferences sharedPreferences5 = sharedPreferences;
                            final SharedPreferences sharedPreferences6 = sharedPreferences2;
                            final SharedPreferences.Editor editor7 = editor2;
                            final SharedPreferences.Editor editor8 = editor3;
                            final SharedPreferences.Editor editor9 = ED_CLICK_ZONE;
                            click_zone_0286.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_seikai_kakudai);
                                    ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                    ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                    click_zone_029.setVisibility(0);
                                    final SharedPreferences sharedPreferences = sharedPreferences5;
                                    final SharedPreferences sharedPreferences2 = sharedPreferences6;
                                    final SharedPreferences.Editor editor = editor7;
                                    final SharedPreferences.Editor editor2 = editor8;
                                    final SharedPreferences.Editor editor3 = editor9;
                                    click_zone_029.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View view) {
                                            int item015_sp = sharedPreferences.getInt("item015", -100);
                                            int item016_sp = sharedPreferences.getInt("item016", -100);
                                            int what_item = sharedPreferences2.getInt("what_item", -100);
                                            if (item015_sp == 15 && what_item == 15) {
                                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi_kakudai);
                                                ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai_key);
                                                Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                editor.putInt("item015", -200);
                                                editor.commit();
                                                editor2.putInt("now_have_item", -100);
                                                editor2.putInt("what_item", -100);
                                                editor2.commit();
                                                editor3.putInt("zone007", 15);
                                                editor3.commit();
                                            } else if (item016_sp == 16 && what_item == 16) {
                                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen_kakudai);
                                                ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai_key);
                                                Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                editor.putInt("item016", -200);
                                                editor.commit();
                                                editor2.putInt("now_have_item", -100);
                                                editor2.putInt("what_item", -100);
                                                editor2.commit();
                                                editor3.putInt("zone007", 16);
                                                editor3.commit();
                                            }
                                        }
                                    });
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        }
                    } else if (Item_select.this.page == 5) {
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_millor);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page005);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(0);
                        Item_select.this.findViewById(R.id.next_page).setVisibility(4);
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                        int item011_sp = sharedPreferences.getInt("item011", -100);
                        ImageView item_image_011 = (ImageView) Item_select.this.findViewById(R.id.item_small_011);
                        if (item011_sp == -100) {
                            item_image_011.setVisibility(0);
                            final SharedPreferences.Editor editor10 = editor2;
                            final int i = i2;
                            item_image_011.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    editor10.putInt("item011", 11);
                                    editor10.commit();
                                    Intent intent = new Intent(Item_select.this.getApplicationContext(), Item_get.class);
                                    intent.putExtra("item_number", 11);
                                    intent.putExtra("where_from", i);
                                    Item_select.this.startActivity(intent);
                                    Item_select.this.finish();
                                    Item_select.this.overridePendingTransition(0, 0);
                                }
                            });
                            return;
                        }
                        item_image_011.setVisibility(4);
                    }
                }
            });
            final SharedPreferences sharedPreferences3 = SP_ITEMS_LIST;
            final SharedPreferences sharedPreferences4 = SP_HAVE_ITEM;
            final SharedPreferences.Editor editor4 = ED_ITEMS_LIST;
            final SharedPreferences.Editor editor5 = ED_HAVE_ITEM;
            ((ImageView) findViewById(R.id.back_page)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Item_select.this.SPool.play(Item_select.this.load_sound4, 1.0f, 1.0f, 1, 0, 1.2f);
                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.item_large_009);
                    Item_select.this.findViewById(R.id.next_page).setVisibility(0);
                    Item_select item_select = Item_select.this;
                    item_select.page--;
                    if (Item_select.this.page == 1) {
                        Item_select.this.page = 1;
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.item_large_009);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page001);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(4);
                        ImageView click_zone_028 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                        click_zone_028.setVisibility(0);
                        click_zone_028.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.item_large_009);
                            }
                        });
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                    } else if (Item_select.this.page == 2) {
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_1page);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page002);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(0);
                        ImageView click_zone_0282 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                        click_zone_0282.setVisibility(0);
                        click_zone_0282.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_1page_kakudai);
                            }
                        });
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                    } else if (Item_select.this.page == 3) {
                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_2page);
                        ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.page003);
                        Item_select.this.findViewById(R.id.back_page).setVisibility(0);
                        ImageView click_zone_0283 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                        click_zone_0283.setVisibility(0);
                        click_zone_0283.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_2page_kakudai);
                            }
                        });
                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_029)).setVisibility(4);
                    } else if (Item_select.this.page != 4) {
                    } else {
                        if (SP_CLICK_ZONE.getInt("zone007", -100) == 7) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_gattai);
                            ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai);
                            ImageView click_zone_0284 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0284.setVisibility(0);
                            click_zone_0284.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                }
                            });
                        } else if (SP_CLICK_ZONE.getInt("zone007", -100) == 15) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi);
                            ImageView click_zone_0285 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0285.setVisibility(0);
                            final SharedPreferences sharedPreferences = sharedPreferences3;
                            final SharedPreferences sharedPreferences2 = sharedPreferences4;
                            final SharedPreferences.Editor editor = editor4;
                            final SharedPreferences.Editor editor2 = editor5;
                            final SharedPreferences.Editor editor3 = ED_CLICK_ZONE;
                            click_zone_0285.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    int item015_sp = sharedPreferences.getInt("item015", -100);
                                    int item016_sp = sharedPreferences.getInt("item016", -100);
                                    if (item015_sp == -200 && item016_sp != -200) {
                                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi_kakudai);
                                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                        ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                        click_zone_029.setVisibility(0);
                                        final SharedPreferences sharedPreferences = sharedPreferences;
                                        final SharedPreferences sharedPreferences2 = sharedPreferences2;
                                        final SharedPreferences.Editor editor = editor;
                                        final SharedPreferences.Editor editor2 = editor2;
                                        final SharedPreferences.Editor editor3 = editor3;
                                        click_zone_029.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                int item016_sp = sharedPreferences.getInt("item016", -100);
                                                int what_item = sharedPreferences2.getInt("what_item", -100);
                                                if (item016_sp == 16 && what_item == 16) {
                                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                                    ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_hamatta);
                                                    Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                    editor.putInt("item016", -200);
                                                    editor.commit();
                                                    editor2.putInt("now_have_item", -100);
                                                    editor2.putInt("what_item", -100);
                                                    editor2.commit();
                                                    editor3.putInt("zone007", 7);
                                                    editor3.commit();
                                                } else if (item016_sp != -200 && item016_sp != 16) {
                                                    AlertDialog.Builder cancelable = new AlertDialog.Builder(Item_select.this).setMessage((int) R.string.key_hazusu).setCancelable(false);
                                                    final SharedPreferences.Editor editor = editor;
                                                    final SharedPreferences.Editor editor2 = editor3;
                                                    cancelable.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            editor.putInt("item015", 15);
                                                            editor.commit();
                                                            editor2.putInt("zone007", -100);
                                                            editor2.commit();
                                                            Intent intent = new Intent(Item_select.this, Items_list.class);
                                                            intent.putExtra("select", 1);
                                                            Item_select.this.startActivity(intent);
                                                            Item_select.this.finish();
                                                            Item_select.this.overridePendingTransition(0, 0);
                                                        }
                                                    }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Intent intent = new Intent(Item_select.this, Items_list.class);
                                                            intent.putExtra("select", 1);
                                                            Item_select.this.startActivity(intent);
                                                            Item_select.this.finish();
                                                            Item_select.this.overridePendingTransition(0, 0);
                                                        }
                                                    }).show();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        } else if (SP_CLICK_ZONE.getInt("zone007", -100) == 16) {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen);
                            ImageView click_zone_0286 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0286.setVisibility(0);
                            final SharedPreferences sharedPreferences3 = sharedPreferences3;
                            final SharedPreferences sharedPreferences4 = sharedPreferences4;
                            final SharedPreferences.Editor editor4 = editor4;
                            final SharedPreferences.Editor editor5 = editor5;
                            final SharedPreferences.Editor editor6 = ED_CLICK_ZONE;
                            click_zone_0286.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    int item015_sp = sharedPreferences3.getInt("item015", -100);
                                    if (sharedPreferences3.getInt("item016", -100) == -200 && item015_sp != -200) {
                                        ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen_kakudai);
                                        ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                        ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                        click_zone_029.setVisibility(0);
                                        final SharedPreferences sharedPreferences = sharedPreferences3;
                                        final SharedPreferences sharedPreferences2 = sharedPreferences4;
                                        final SharedPreferences.Editor editor = editor4;
                                        final SharedPreferences.Editor editor2 = editor5;
                                        final SharedPreferences.Editor editor3 = editor6;
                                        click_zone_029.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                int item015_sp = sharedPreferences.getInt("item015", -100);
                                                int what_item = sharedPreferences2.getInt("what_item", -100);
                                                if (item015_sp == 15 && what_item == 15) {
                                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_gattai_zoom);
                                                    ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_hamatta);
                                                    Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                    editor.putInt("item015", -200);
                                                    editor.commit();
                                                    editor2.putInt("now_have_item", -100);
                                                    editor2.putInt("what_item", -100);
                                                    editor2.commit();
                                                    editor3.putInt("zone007", 7);
                                                    editor3.commit();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        } else {
                            ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_seikai);
                            ImageView click_zone_0287 = (ImageView) Item_select.this.findViewById(R.id.clickzone_028);
                            click_zone_0287.setVisibility(0);
                            final SharedPreferences sharedPreferences5 = sharedPreferences3;
                            final SharedPreferences sharedPreferences6 = sharedPreferences4;
                            final SharedPreferences.Editor editor7 = editor4;
                            final SharedPreferences.Editor editor8 = editor5;
                            final SharedPreferences.Editor editor9 = ED_CLICK_ZONE;
                            click_zone_0287.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_seikai_kakudai);
                                    ((ImageView) Item_select.this.findViewById(R.id.clickzone_028)).setVisibility(4);
                                    ImageView click_zone_029 = (ImageView) Item_select.this.findViewById(R.id.clickzone_029);
                                    click_zone_029.setVisibility(0);
                                    final SharedPreferences sharedPreferences = sharedPreferences5;
                                    final SharedPreferences sharedPreferences2 = sharedPreferences6;
                                    final SharedPreferences.Editor editor = editor7;
                                    final SharedPreferences.Editor editor2 = editor8;
                                    final SharedPreferences.Editor editor3 = editor9;
                                    click_zone_029.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View view) {
                                            int item015_sp = sharedPreferences.getInt("item015", -100);
                                            int item016_sp = sharedPreferences.getInt("item016", -100);
                                            int what_item = sharedPreferences2.getInt("what_item", -100);
                                            if (item015_sp == 15 && what_item == 15) {
                                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_kagi_kakudai);
                                                ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai_key);
                                                Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                editor.putInt("item015", -200);
                                                editor.commit();
                                                editor2.putInt("now_have_item", -100);
                                                editor2.putInt("what_item", -100);
                                                editor2.commit();
                                                editor3.putInt("zone007", 15);
                                                editor3.commit();
                                            } else if (item016_sp == 16 && what_item == 16) {
                                                ((ImageView) Item_select.this.findViewById(R.id.item_select_image_zone)).setImageResource(R.drawable.book_open_teppen_kakudai);
                                                ((TextView) Item_select.this.findViewById(R.id.talk)).setText((int) R.string.key_gattai_key);
                                                Item_select.this.SPool.play(Item_select.this.load_sound, 1.0f, 1.0f, 0, 0, 1.0f);
                                                editor.putInt("item016", -200);
                                                editor.commit();
                                                editor2.putInt("now_have_item", -100);
                                                editor2.putInt("what_item", -100);
                                                editor2.commit();
                                                editor3.putInt("zone007", 16);
                                                editor3.commit();
                                            }
                                        }
                                    });
                                }
                            });
                            ((ImageView) Item_select.this.findViewById(R.id.item_small_011)).setVisibility(4);
                        }
                    }
                }
            });
        }
        if (ITEM_NUMBER == 14) {
            final ImageView button_separate2 = (ImageView) findViewById(R.id.separate);
            button_separate2.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent event) {
                    switch (event.getAction()) {
                        case 0:
                            button_separate2.setImageResource(R.drawable.bt_separate_001_15052);
                            return false;
                        case 1:
                            button_separate2.setImageResource(R.drawable.bt_separate_000_15052);
                            return false;
                        default:
                            return false;
                    }
                }
            });
            button_separate2.setVisibility(0);
            final SharedPreferences.Editor editor6 = ED_HAVE_ITEM;
            final int i3 = WHERE_FROM;
            button_separate2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Item_select.this.SPool.play(Item_select.this.load_sound3, 1.0f, 1.0f, 1, 0, 1.0f);
                    button_separate2.setImageResource(R.drawable.bt_separate_000_15052);
                    ED_ITEMS_LIST.putInt("item001", 1);
                    ED_ITEMS_LIST.putInt("item013", 13);
                    ED_ITEMS_LIST.putInt("item014", -100);
                    ED_ITEMS_LIST.commit();
                    editor6.putInt("now_have_item", -100);
                    editor6.putInt("what_item", -100);
                    editor6.commit();
                    AlertDialog.Builder cancelable = new AlertDialog.Builder(Item_select.this).setTitle("SEPARATE").setMessage((int) R.string.item_014_restore_item_001_item_013).setCancelable(false);
                    final int i = i3;
                    cancelable.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Item_select.this, Items_list.class);
                            intent.putExtra("where_from", i);
                            intent.putExtra("select", 1);
                            Item_select.this.startActivity(intent);
                            Item_select.this.finish();
                            Item_select.this.overridePendingTransition(0, 0);
                        }
                    }).show();
                }
            });
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
