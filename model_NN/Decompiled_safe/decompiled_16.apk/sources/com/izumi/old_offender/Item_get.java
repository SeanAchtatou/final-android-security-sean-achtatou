package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Item_get extends Activity {
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_BACKGROUND = this.FAC.get_R_Drawable_Background();
    final int[] R_DRAWABLE_ITEM_IMAGE_L = this.FAC.get_R_Drawable_Item_Image_L();
    final int[] R_ITEM_NAME = this.FAC.get_R_Item_Name();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    int load_sound;
    private MediaPlayer mediaPlayer = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.item03);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[1], 1);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_item_get);
        } else {
            setContentView((int) R.layout.w_layout_item_get);
        }
        Intent GET_INTENT = getIntent();
        final int WHERE_FROM = GET_INTENT.getIntExtra("where_from", 1);
        int ITEM_NUMBER = GET_INTENT.getIntExtra("item_number", 0);
        int FROM_COMBINE = GET_INTENT.getIntExtra("from_combine", 0);
        final Class[] CLASS_NAME = this.FAC.get_Class_Name();
        ((TextView) findViewById(R.id.item_name)).setText(this.R_ITEM_NAME[ITEM_NUMBER]);
        if (FROM_COMBINE == 100) {
            ((ImageView) findViewById(R.id.item_get_back)).setImageResource(R.drawable.game_finish_back_000);
        } else {
            ImageView item_get_back = (ImageView) findViewById(R.id.item_get_back);
            item_get_back.setImageResource(this.R_DRAWABLE_BACKGROUND[WHERE_FROM]);
            item_get_back.setAlpha(100);
        }
        ImageView item_image_zone = (ImageView) findViewById(R.id.item_get_image_zone);
        item_image_zone.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_L[ITEM_NUMBER]);
        item_image_zone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Item_get.this.startActivity(new Intent(Item_get.this, CLASS_NAME[WHERE_FROM]));
                Item_get.this.finish();
                Item_get.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mediaPlayer != null) {
            this.mediaPlayer.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
