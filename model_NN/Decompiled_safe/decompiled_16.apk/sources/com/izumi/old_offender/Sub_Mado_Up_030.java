package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Sub_Mado_Up_030 extends Activity {
    final int ACTIVITY_NUM = 30;
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    int load_sound2;
    /* access modifiers changed from: private */
    public ImageView m_button_e;
    /* access modifiers changed from: private */
    public ImageView m_button_i;
    /* access modifiers changed from: private */
    public ImageView m_button_u;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_015;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_017;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_018;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_019;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_020;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_095;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_096;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_mado_up);
        } else {
            setContentView((int) R.layout.w_layout_sub_mado_up);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[6], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        final SharedPreferences SP_CLICK_ZONE = getSharedPreferences("click_zone", 0);
        final SharedPreferences.Editor ED_CLICK_ZONE = SP_CLICK_ZONE.edit();
        final SharedPreferences SP_TETSUBO = getSharedPreferences("tetsubo", 0);
        final SharedPreferences.Editor ED_TETSUBO = SP_TETSUBO.edit();
        this.m_click_zone_096 = (ImageView) findViewById(R.id.clickzone_096);
        this.m_click_zone_095 = (ImageView) findViewById(R.id.clickzone_095);
        this.m_click_zone_020 = (ImageView) findViewById(R.id.clickzone_020);
        this.m_click_zone_019 = (ImageView) findViewById(R.id.clickzone_019);
        this.m_click_zone_017 = (ImageView) findViewById(R.id.clickzone_017);
        this.m_click_zone_018 = (ImageView) findViewById(R.id.clickzone_018);
        this.m_click_zone_015 = (ImageView) findViewById(R.id.clickzone_015);
        this.m_button_i = (ImageView) findViewById(R.id.item);
        this.m_button_e = (ImageView) findViewById(R.id.end);
        this.m_button_u = (ImageView) findViewById(R.id.under_b);
        if (SP_CLICK_ZONE.getInt("zone002", -100) == 2) {
            startActivity(new Intent(this, Sub_Mado_Tetsubo_Off_020.class));
            finish();
            overridePendingTransition(0, 0);
        }
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        this.m_button_u.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_009.class));
                Sub_Mado_Up_030.this.finish();
                Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
            }
        });
        this.m_click_zone_015.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int item012_sp = SP_ITEMS_LIST.getInt("item012", -100);
                int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                if (SP_CLICK_ZONE.getInt("mirror03", -100) == 10 || (what_item == 12 && item012_sp == 12)) {
                    TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                    text_zone.setText((int) R.string.talk12);
                    text_zone.setVisibility(0);
                    ED_CLICK_ZONE.putInt("mirror03", 10);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.m_button_u.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_i.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_e.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_015.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_017.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_018.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_019.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_020.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_095.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_096.setVisibility(4);
                    Sub_Mado_Up_030.this.HANDLER = new Handler();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(1000);
                                Sub_Mado_Up_030.this.HANDLER.post(new Runnable() {
                                    public void run() {
                                        Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Soto_018.class));
                                        Sub_Mado_Up_030.this.finish();
                                        Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                                    }
                                });
                            } catch (InterruptedException e) {
                            }
                        }
                    }).start();
                    return;
                }
                TextView text_zone2 = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone2.setText((int) R.string.talk11);
                text_zone2.setVisibility(0);
            }
        });
        this.m_click_zone_095.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int item012_sp = SP_ITEMS_LIST.getInt("item012", -100);
                int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                if (SP_CLICK_ZONE.getInt("mirror03", -100) == 10 || (what_item == 12 && item012_sp == 12)) {
                    TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                    text_zone.setText((int) R.string.talk12);
                    text_zone.setVisibility(0);
                    ED_CLICK_ZONE.putInt("mirror03", 10);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.m_button_u.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_i.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_e.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_015.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_017.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_018.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_019.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_020.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_095.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_096.setVisibility(4);
                    Sub_Mado_Up_030.this.HANDLER = new Handler();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(1000);
                                Sub_Mado_Up_030.this.HANDLER.post(new Runnable() {
                                    public void run() {
                                        Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Soto_018.class));
                                        Sub_Mado_Up_030.this.finish();
                                        Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                                    }
                                });
                            } catch (InterruptedException e) {
                            }
                        }
                    }).start();
                    return;
                }
                TextView text_zone2 = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone2.setText((int) R.string.talk11);
                text_zone2.setVisibility(0);
            }
        });
        this.m_click_zone_096.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int item012_sp = SP_ITEMS_LIST.getInt("item012", -100);
                int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
                if (SP_CLICK_ZONE.getInt("mirror03", -100) == 10 || (what_item == 12 && item012_sp == 12)) {
                    TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                    text_zone.setText((int) R.string.talk12);
                    text_zone.setVisibility(0);
                    ED_CLICK_ZONE.putInt("mirror03", 10);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.m_button_u.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_i.setVisibility(4);
                    Sub_Mado_Up_030.this.m_button_e.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_015.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_017.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_018.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_019.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_020.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_095.setVisibility(4);
                    Sub_Mado_Up_030.this.m_click_zone_096.setVisibility(4);
                    Sub_Mado_Up_030.this.HANDLER = new Handler();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(1000);
                                Sub_Mado_Up_030.this.HANDLER.post(new Runnable() {
                                    public void run() {
                                        Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Soto_018.class));
                                        Sub_Mado_Up_030.this.finish();
                                        Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                                    }
                                });
                            } catch (InterruptedException e) {
                            }
                        }
                    }).start();
                    return;
                }
                TextView text_zone2 = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone2.setText((int) R.string.talk11);
                text_zone2.setVisibility(0);
            }
        });
        this.m_click_zone_017.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.SPool.play(Sub_Mado_Up_030.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone.setText((int) R.string.talk10);
                text_zone.setVisibility(0);
                int tetsubo_001_sp = SP_TETSUBO.getInt("tetsubo001", 0);
                int tetsubo_002_sp = SP_TETSUBO.getInt("tetsubo002", 0);
                int tetsubo_003_sp = SP_TETSUBO.getInt("tetsubo003", 0);
                int tetsubo_004_sp = SP_TETSUBO.getInt("tetsubo004", 0);
                int tetsubo_001_sp2 = tetsubo_001_sp + 1;
                ED_TETSUBO.putInt("tetsubo001", tetsubo_001_sp2);
                ED_TETSUBO.commit();
                ((Vibrator) Sub_Mado_Up_030.this.getSystemService("vibrator")).vibrate(200);
                if (tetsubo_001_sp2 == 3 && tetsubo_002_sp == 2 && tetsubo_003_sp == 4 && tetsubo_004_sp == 1) {
                    ED_CLICK_ZONE.putInt("zone002", 2);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Tetsubo_Off_020.class));
                    Sub_Mado_Up_030.this.finish();
                    Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                }
            }
        });
        this.m_click_zone_018.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.SPool.play(Sub_Mado_Up_030.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone.setText((int) R.string.talk10);
                text_zone.setVisibility(0);
                int tetsubo_001_sp = SP_TETSUBO.getInt("tetsubo001", 0);
                int tetsubo_002_sp = SP_TETSUBO.getInt("tetsubo002", 0);
                int tetsubo_003_sp = SP_TETSUBO.getInt("tetsubo003", 0);
                int tetsubo_004_sp = SP_TETSUBO.getInt("tetsubo004", 0);
                int tetsubo_002_sp2 = tetsubo_002_sp + 1;
                ED_TETSUBO.putInt("tetsubo002", tetsubo_002_sp2);
                ED_TETSUBO.commit();
                ((Vibrator) Sub_Mado_Up_030.this.getSystemService("vibrator")).vibrate(200);
                if (tetsubo_001_sp == 3 && tetsubo_002_sp2 == 2 && tetsubo_003_sp == 4 && tetsubo_004_sp == 1) {
                    ED_CLICK_ZONE.putInt("zone002", 2);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Tetsubo_Off_020.class));
                    Sub_Mado_Up_030.this.finish();
                    Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                }
            }
        });
        this.m_click_zone_019.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.SPool.play(Sub_Mado_Up_030.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone.setText((int) R.string.talk10);
                text_zone.setVisibility(0);
                int tetsubo_001_sp = SP_TETSUBO.getInt("tetsubo001", 0);
                int tetsubo_002_sp = SP_TETSUBO.getInt("tetsubo002", 0);
                int tetsubo_003_sp = SP_TETSUBO.getInt("tetsubo003", 0);
                int tetsubo_004_sp = SP_TETSUBO.getInt("tetsubo004", 0);
                int tetsubo_003_sp2 = tetsubo_003_sp + 1;
                ED_TETSUBO.putInt("tetsubo003", tetsubo_003_sp2);
                ED_TETSUBO.commit();
                ((Vibrator) Sub_Mado_Up_030.this.getSystemService("vibrator")).vibrate(200);
                if (tetsubo_001_sp == 3 && tetsubo_002_sp == 2 && tetsubo_003_sp2 == 4 && tetsubo_004_sp == 1) {
                    ED_CLICK_ZONE.putInt("zone002", 2);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Tetsubo_Off_020.class));
                    Sub_Mado_Up_030.this.finish();
                    Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                }
            }
        });
        this.m_click_zone_020.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.SPool.play(Sub_Mado_Up_030.this.load_sound2, 1.0f, 1.0f, 0, 0, 1.0f);
                TextView text_zone = (TextView) Sub_Mado_Up_030.this.findViewById(R.id.talk);
                text_zone.setText((int) R.string.talk10);
                text_zone.setVisibility(0);
                int tetsubo_001_sp = SP_TETSUBO.getInt("tetsubo001", 0);
                int tetsubo_002_sp = SP_TETSUBO.getInt("tetsubo002", 0);
                int tetsubo_003_sp = SP_TETSUBO.getInt("tetsubo003", 0);
                int tetsubo_004_sp = SP_TETSUBO.getInt("tetsubo004", 0) + 1;
                ED_TETSUBO.putInt("tetsubo004", tetsubo_004_sp);
                ED_TETSUBO.commit();
                ((Vibrator) Sub_Mado_Up_030.this.getSystemService("vibrator")).vibrate(200);
                if (tetsubo_001_sp == 3 && tetsubo_002_sp == 2 && tetsubo_003_sp == 4 && tetsubo_004_sp == 1) {
                    ED_CLICK_ZONE.putInt("zone002", 2);
                    ED_CLICK_ZONE.commit();
                    Sub_Mado_Up_030.this.startActivity(new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Sub_Mado_Tetsubo_Off_020.class));
                    Sub_Mado_Up_030.this.finish();
                    Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
                }
            }
        });
        this.m_button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Mado_Up_030.this.m_button_i.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        Sub_Mado_Up_030.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.m_button_i.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 30);
                Sub_Mado_Up_030.this.startActivity(intent);
                Sub_Mado_Up_030.this.finish();
                Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
            }
        });
        this.m_button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        Sub_Mado_Up_030.this.m_button_e.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        Sub_Mado_Up_030.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.m_button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Up_030.this.m_button_e.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Sub_Mado_Up_030.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 30);
                Sub_Mado_Up_030.this.startActivity(intent);
                Sub_Mado_Up_030.this.finish();
                Sub_Mado_Up_030.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
        SharedPreferences.Editor ED_TETSUBO = getSharedPreferences("tetsubo", 0).edit();
        ED_TETSUBO.putInt("tetsubo001", 0);
        ED_TETSUBO.putInt("tetsubo002", 0);
        ED_TETSUBO.putInt("tetsubo003", 0);
        ED_TETSUBO.putInt("tetsubo004", 0);
        ED_TETSUBO.commit();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.HANDLER != null) {
            this.HANDLER = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
