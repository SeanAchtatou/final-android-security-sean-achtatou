package com.izumi.old_offender;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.izumi.old_offenderzykj.R;
import java.util.Locale;
import mediba.ad.sdk.android.MasAdView;

public class Opening extends Activity {
    final Factory FAC = Factory.get_Instance();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int load_sound;
    private int m_density_dpi;
    private Display m_display;
    private int m_display_height;
    private DisplayMetrics m_display_metrics;
    private int m_display_width;
    private float m_scaled_density;
    private MasAdView mad = null;
    private MediaPlayer mediaPlayer = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences.Editor ED_KAIZOUDO_LIST = getSharedPreferences("kaizoudo", 0).edit();
        this.m_display_metrics = new DisplayMetrics();
        this.m_display = getWindowManager().getDefaultDisplay();
        this.m_display.getMetrics(this.m_display_metrics);
        this.m_scaled_density = this.m_display_metrics.scaledDensity;
        this.m_density_dpi = this.m_display_metrics.densityDpi;
        this.m_display_width = this.m_display.getWidth();
        this.m_display_height = this.m_display.getHeight();
        if (((double) this.m_scaled_density) == 1.5d && this.m_density_dpi == 240 && this.m_display_width >= 960 && this.m_display_height == 540) {
            setContentView((int) R.layout.hq_layout_opening);
            ED_KAIZOUDO_LIST.putInt("kaizoudo", 1);
            ED_KAIZOUDO_LIST.commit();
        } else if (this.m_scaled_density == 2.0f && this.m_density_dpi == 320 && this.m_display_width >= 1280 && this.m_display_height == 720) {
            setContentView((int) R.layout.hq_layout_opening);
            ED_KAIZOUDO_LIST.putInt("kaizoudo", 1);
            ED_KAIZOUDO_LIST.commit();
        } else if (((double) this.m_scaled_density) == 1.5d && this.m_density_dpi == 240 && this.m_display_width >= 800 && this.m_display_height == 480) {
            setContentView((int) R.layout.w_layout_opening);
            ED_KAIZOUDO_LIST.putInt("kaizoudo", 2);
            ED_KAIZOUDO_LIST.commit();
        } else {
            setContentView((int) R.layout.w_layout_opening);
            ED_KAIZOUDO_LIST.putInt("kaizoudo", 2);
            ED_KAIZOUDO_LIST.commit();
        }
        if (Locale.JAPAN.equals(Locale.getDefault())) {
            this.mad = new MasAdView(this);
            this.mad.setRefreshAnimation(0);
            this.mad.setRequestInterval(30);
            this.mad.setEnabled(true);
            ((RelativeLayout) findViewById(R.id.layout_kokoku_000)).addView(this.mad);
        } else {
            AdView adView = new AdView(this, AdSize.BANNER, "a14d7ba593151f3");
            ((RelativeLayout) findViewById(R.id.layout_kokoku_001)).addView(adView);
            adView.loadAd(new AdRequest());
        }
        setVolumeControlStream(3);
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.opening_loop);
        if (this.mediaPlayer != null) {
            this.mediaPlayer.setLooping(true);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        final SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        final SharedPreferences.Editor ED_ITEMS_LIST = SP_ITEMS_LIST.edit();
        final SharedPreferences.Editor ED_HAVE_ITEM = getSharedPreferences("have_item", 0).edit();
        final SharedPreferences SP_CLICK_ZONE = getSharedPreferences("click_zone", 0);
        final SharedPreferences.Editor ED_CLICK_ZONE = SP_CLICK_ZONE.edit();
        final ImageView button_start = (ImageView) findViewById(R.id.opening_start);
        button_start.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_start.setImageResource(R.drawable.bt_start_001_15052);
                        return false;
                    case 1:
                        button_start.setImageResource(R.drawable.bt_start_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Opening.this.SPool.play(Opening.this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
                button_start.setImageResource(R.drawable.bt_start_000_15052);
                if (SP_ITEMS_LIST.getInt("item000", -10000) != -10000) {
                    AlertDialog.Builder cancelable = new AlertDialog.Builder(Opening.this).setTitle("START").setMessage((int) R.string.opening_save_delete).setCancelable(false);
                    final SharedPreferences.Editor editor = ED_ITEMS_LIST;
                    final SharedPreferences.Editor editor2 = ED_HAVE_ITEM;
                    final SharedPreferences.Editor editor3 = ED_CLICK_ZONE;
                    cancelable.setPositiveButton("START", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            editor.clear();
                            editor.commit();
                            editor2.clear();
                            editor2.commit();
                            editor3.clear();
                            editor3.commit();
                            Opening.this.startActivity(new Intent(Opening.this, Main_A_001.class));
                            Opening.this.finish();
                            Opening.this.overridePendingTransition(0, 0);
                        }
                    }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).show();
                    return;
                }
                ED_ITEMS_LIST.clear();
                ED_ITEMS_LIST.commit();
                ED_HAVE_ITEM.clear();
                ED_HAVE_ITEM.commit();
                ED_CLICK_ZONE.clear();
                ED_CLICK_ZONE.commit();
                Opening.this.startActivity(new Intent(Opening.this, Main_A_001.class));
                Opening.this.finish();
                Opening.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_continue = (ImageView) findViewById(R.id.opening_continue);
        final ImageView imageView = button_continue;
        button_continue.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_continue_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_continue_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_continue;
        button_continue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_continue_000_15052);
                if (SP_CLICK_ZONE.getInt("zone002", -100) == 2) {
                    Intent intent = new Intent(Opening.this.getApplicationContext(), Sub_Mado_Tetsubo_Off_020.class);
                    intent.putExtra("from_item_list", 1);
                    Opening.this.startActivity(intent);
                    Opening.this.finish();
                    Opening.this.overridePendingTransition(0, 0);
                    return;
                }
                Intent intent2 = new Intent(Opening.this.getApplicationContext(), Main_A_001.class);
                intent2.putExtra("from_item_list", 1);
                Opening.this.startActivity(intent2);
                Opening.this.finish();
                Opening.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_help = (ImageView) findViewById(R.id.opening_help);
        final ImageView imageView3 = button_help;
        button_help.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView3.setImageResource(R.drawable.bt_help_001_15052);
                        return false;
                    case 1:
                        imageView3.setImageResource(R.drawable.bt_help_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView4 = button_help;
        button_help.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView4.setImageResource(R.drawable.bt_help_000_15052);
                Opening.this.startActivity(new Intent(Opening.this.getApplicationContext(), Help_029.class));
                Opening.this.finish();
                Opening.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_bbs = (ImageView) findViewById(R.id.go_to_web);
        final ImageView imageView5 = button_bbs;
        button_bbs.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView5.setImageResource(R.drawable.bt_bbs_001_15052);
                        return false;
                    case 1:
                        imageView5.setImageResource(R.drawable.bt_bbs_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView6 = button_bbs;
        button_bbs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Opening.this.SPool.play(Opening.this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
                imageView6.setImageResource(R.drawable.bt_bbs_000_15052);
                Opening.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Opening.this.getString(R.string.url_old_offender))));
                Opening.this.overridePendingTransition(0, 0);
            }
        });
        ImageView button_website = (ImageView) findViewById(R.id.go_to_website);
        final ImageView imageView7 = button_website;
        button_website.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView7.setImageResource(R.drawable.btn_website_p_8686);
                        return false;
                    case 1:
                        imageView7.setImageResource(R.drawable.btn_website_n_8686);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView8 = button_website;
        button_website.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView8.setImageResource(R.drawable.btn_website_n_8686);
                Opening.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Opening.this.getString(R.string.url_ia))));
                Opening.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            finish();
            overridePendingTransition(0, 0);
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayer != null) {
            this.mediaPlayer.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mad != null) {
            this.mad.setEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer = null;
        }
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mad != null) {
            this.mad.setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
