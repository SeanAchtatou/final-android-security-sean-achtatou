package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Sub_Mado_Tetsubo_Off_020 extends Activity {
    final int ACTIVITY_NUM = 20;
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    int load_sound;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_sub_mado_tetsubo_off);
        } else {
            setContentView((int) R.layout.w_layout_sub_mado_tetsubo_off);
        }
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[21], 1);
        do {
        } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
        this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        TextView text_zone = (TextView) findViewById(R.id.talk);
        text_zone.setText((int) R.string.talk19);
        text_zone.setVisibility(0);
        ((ImageView) findViewById(R.id.clickzone_021)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Sub_Mado_Tetsubo_Off_020.this.startActivity(new Intent(Sub_Mado_Tetsubo_Off_020.this.getApplicationContext(), Finish_True_End_024.class));
                Sub_Mado_Tetsubo_Off_020.this.finish();
                Sub_Mado_Tetsubo_Off_020.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
        SharedPreferences.Editor ED_TETSUBO = getSharedPreferences("tetsubo", 0).edit();
        ED_TETSUBO.putInt("tetsubo001", 0);
        ED_TETSUBO.putInt("tetsubo002", 0);
        ED_TETSUBO.putInt("tetsubo003", 0);
        ED_TETSUBO.putInt("tetsubo004", 0);
        ED_TETSUBO.commit();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
