package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.izumi.old_offenderzykj.R;

public class Finish_Bad_End_025 extends Activity {
    final Factory FAC = Factory.get_Instance();
    /* access modifiers changed from: private */
    public Handler HANDLER;
    private int KAIZOUDO;
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    /* access modifiers changed from: private */
    public SoundPool SPool;
    int a = 0;
    int load_sound;
    int load_sound2;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer = null;
    private Runnable test;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.policesiren3);
        this.mediaPlayer.setLooping(true);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[22], 1);
        this.load_sound2 = this.SPool.load(this, this.R_RAW_SOUND[18], 1);
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_finish_bad_end);
        } else {
            setContentView((int) R.layout.w_layout_finish_bad_end);
        }
        ((ImageView) findViewById(R.id.finish_bad_end)).setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
        if (this.SPool != null) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.HANDLER = new Handler();
        this.test = new Runnable() {
            public void run() {
                Finish_Bad_End_025.this.a++;
                if (Finish_Bad_End_025.this.a == 1) {
                    Finish_Bad_End_025.this.SPool.play(Finish_Bad_End_025.this.load_sound2, 1.0f, 1.0f, 0, 3, 2.0f);
                    Finish_Bad_End_025.this.HANDLER.postDelayed(this, 3800);
                } else if (Finish_Bad_End_025.this.a == 2) {
                    if (Finish_Bad_End_025.this.mediaPlayer != null) {
                        Finish_Bad_End_025.this.mediaPlayer.start();
                    }
                    final ImageView button_main = (ImageView) Finish_Bad_End_025.this.findViewById(R.id.go_to_main);
                    button_main.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View view, MotionEvent event) {
                            switch (event.getAction()) {
                                case 0:
                                    button_main.setImageResource(R.drawable.bt_main_001_15052);
                                    return false;
                                case 1:
                                    button_main.setImageResource(R.drawable.bt_main_000_15052);
                                    return false;
                                default:
                                    return false;
                            }
                        }
                    });
                    button_main.setVisibility(0);
                    button_main.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            button_main.setImageResource(R.drawable.bt_main_000_15052);
                            Intent intent = new Intent(Finish_Bad_End_025.this.getApplicationContext(), Opening.class);
                            intent.putExtra("from_item_list", 1);
                            Finish_Bad_End_025.this.startActivity(intent);
                            Finish_Bad_End_025.this.finish();
                            Finish_Bad_End_025.this.overridePendingTransition(0, 0);
                        }
                    });
                    final ImageView button_goal = (ImageView) Finish_Bad_End_025.this.findViewById(R.id.go_to_web);
                    button_goal.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View view, MotionEvent event) {
                            switch (event.getAction()) {
                                case 0:
                                    button_goal.setImageResource(R.drawable.bt_goal_001_15052);
                                    return false;
                                case 1:
                                    button_goal.setImageResource(R.drawable.bt_goal_000_15052);
                                    return false;
                                default:
                                    return false;
                            }
                        }
                    });
                    button_goal.setVisibility(0);
                    button_goal.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            button_goal.setImageResource(R.drawable.bt_goal_000_15052);
                            Finish_Bad_End_025.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Finish_Bad_End_025.this.getString(R.string.url_bad))));
                            Finish_Bad_End_025.this.finish();
                            Finish_Bad_End_025.this.overridePendingTransition(0, 0);
                        }
                    });
                    final ImageView button_end = (ImageView) Finish_Bad_End_025.this.findViewById(R.id.go_to_end);
                    button_end.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View view, MotionEvent event) {
                            switch (event.getAction()) {
                                case 0:
                                    button_end.setImageResource(R.drawable.bt_end_001_15052);
                                    return false;
                                case 1:
                                    button_end.setImageResource(R.drawable.bt_end_000_15052);
                                    return false;
                                default:
                                    return false;
                            }
                        }
                    });
                    button_end.setVisibility(0);
                    button_end.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            button_end.setImageResource(R.drawable.bt_end_000_15052);
                            Finish_Bad_End_025.this.finish();
                            Finish_Bad_End_025.this.overridePendingTransition(0, 0);
                        }
                    });
                } else {
                    Finish_Bad_End_025.this.HANDLER.postDelayed(this, 1600);
                }
            }
        };
        this.HANDLER.postDelayed(this.test, 1600);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
        SharedPreferences.Editor ED_TETSUBO = getSharedPreferences("tetsubo", 0).edit();
        ED_TETSUBO.putInt("tetsubo001", 0);
        ED_TETSUBO.putInt("tetsubo002", 0);
        ED_TETSUBO.putInt("tetsubo003", 0);
        ED_TETSUBO.putInt("tetsubo004", 0);
        ED_TETSUBO.commit();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.HANDLER.removeCallbacks(this.test);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
