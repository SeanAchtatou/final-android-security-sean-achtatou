package com.izumi.old_offender;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.izumi.old_offenderzykj.R;

public class Main_B_002 extends Activity {
    final int ACTIVITY_NUM = 2;
    final Factory FAC = Factory.get_Instance();
    private int KAIZOUDO;
    final int[] R_DRAWABLE_ITEM_IMAGE_S = this.FAC.get_R_Drawable_Item_Image_S();
    final int[] R_RAW_SOUND = this.FAC.get_R_Raw_Sound();
    final int SOUNDS = this.FAC.get_Sounds();
    private SoundPool SPool;
    Resources e_resources;
    int load_sound;
    /* access modifiers changed from: private */
    public final int m_BITMAP_SIZE_000 = this.m_bitmap_000.length;
    /* access modifiers changed from: private */
    public Handler m_anim_handler_000;
    /* access modifiers changed from: private */
    public Handler m_anim_handler_001;
    /* access modifiers changed from: private */
    public Handler m_anim_handler_002;
    /* access modifiers changed from: private */
    public boolean m_anim_isRepeat = true;
    /* access modifiers changed from: private */
    public Message m_anim_message;
    /* access modifiers changed from: private */
    public int m_anim_page = 0;
    /* access modifiers changed from: private */
    public Bitmap[] m_bitmap_000 = new Bitmap[5];
    /* access modifiers changed from: private */
    public ImageView m_button_left;
    /* access modifiers changed from: private */
    public ImageView m_button_right;
    /* access modifiers changed from: private */
    public ImageView m_change_anim_zone;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_003;
    /* access modifiers changed from: private */
    public ImageView m_click_zone_051;
    /* access modifiers changed from: private */
    public int m_item001_sp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.e_resources = getResources();
        this.KAIZOUDO = getSharedPreferences("kaizoudo", 0).getInt("kaizoudo", 0);
        if (this.KAIZOUDO == 1) {
            setContentView((int) R.layout.hq_layout_b);
        } else {
            setContentView((int) R.layout.w_layout_b);
        }
        int INTENT_FROM_ITEM_LIST = getIntent().getIntExtra("from_item_list", -100);
        this.SPool = new SoundPool(this.SOUNDS, 3, 0);
        this.load_sound = this.SPool.load(this, this.R_RAW_SOUND[0], 1);
        if (INTENT_FROM_ITEM_LIST == 1) {
            do {
            } while (this.SPool.play(this.load_sound, 0.0f, 0.0f, 1, 0, 1.0f) == 0);
            this.SPool.play(this.load_sound, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        SharedPreferences SP_ITEMS_LIST = getSharedPreferences("items_list", 0);
        SharedPreferences SP_HAVE_ITEM = getSharedPreferences("have_item", 0);
        this.m_change_anim_zone = (ImageView) findViewById(R.id.change_anim_zone);
        this.m_button_right = (ImageView) findViewById(R.id.right_b);
        this.m_button_left = (ImageView) findViewById(R.id.left_b);
        this.m_click_zone_051 = (ImageView) findViewById(R.id.clickzone_051);
        this.m_click_zone_003 = (ImageView) findViewById(R.id.clickzone_003);
        int now_have_item = SP_HAVE_ITEM.getInt("now_have_item", -1000);
        ImageView now_soubi_item = (ImageView) findViewById(R.id.now_soubi_item);
        if (now_have_item == -100) {
            now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[0]);
            now_soubi_item.setVisibility(4);
        } else {
            int what_item = SP_HAVE_ITEM.getInt("what_item", -100);
            if (what_item == SP_ITEMS_LIST.getInt("item" + String.format("%1$03d", Integer.valueOf(what_item)), -1000)) {
                now_soubi_item.setImageResource(this.R_DRAWABLE_ITEM_IMAGE_S[what_item]);
            }
        }
        this.m_item001_sp = SP_ITEMS_LIST.getInt("item001", -100);
        ImageView button_i = (ImageView) findViewById(R.id.item);
        final ImageView imageView = button_i;
        button_i.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        imageView.setImageResource(R.drawable.bt_item_001_15052);
                        return false;
                    case 1:
                        imageView.setImageResource(R.drawable.bt_item_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        final ImageView imageView2 = button_i;
        button_i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                imageView2.setImageResource(R.drawable.bt_item_000_15052);
                Intent intent = new Intent(Main_B_002.this.getApplicationContext(), Items_list.class);
                intent.putExtra("where_from", 2);
                Main_B_002.this.startActivity(intent);
                Main_B_002.this.finish();
                Main_B_002.this.overridePendingTransition(0, 0);
            }
        });
        final ImageView button_e = (ImageView) findViewById(R.id.end);
        button_e.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        button_e.setImageResource(R.drawable.bt_end_001_15052);
                        return false;
                    case 1:
                        button_e.setImageResource(R.drawable.bt_end_000_15052);
                        return false;
                    default:
                        return false;
                }
            }
        });
        button_e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                button_e.setImageResource(R.drawable.bt_end_000_15052);
                Intent intent = new Intent(Main_B_002.this.getApplicationContext(), Game_Finish_031.class);
                intent.putExtra("where_from", 2);
                Main_B_002.this.startActivity(intent);
                Main_B_002.this.finish();
                Main_B_002.this.overridePendingTransition(0, 0);
            }
        });
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.m_anim_handler_000 = new Handler() {
            public void handleMessage(Message _msg) {
                Main_B_002.this.m_change_anim_zone.setVisibility(0);
                if (Main_B_002.this.m_anim_isRepeat) {
                    if (Main_B_002.this.m_anim_page < Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_000.sendMessageDelayed(obtainMessage(), 100);
                    }
                    if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_000.sendMessageDelayed(obtainMessage(), 15);
                    } else if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000) {
                        Main_B_002.this.startActivity(new Intent(Main_B_002.this.getApplicationContext(), Main_C_003.class));
                        Main_B_002.this.finish();
                        Main_B_002.this.overridePendingTransition(0, 0);
                        Main_B_002.this.m_anim_isRepeat = false;
                    }
                    Main_B_002 main_B_002 = Main_B_002.this;
                    main_B_002.m_anim_page = main_B_002.m_anim_page + 1;
                }
            }
        };
        this.m_button_right.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Main_B_002.this.m_button_right.setVisibility(4);
                Main_B_002.this.m_button_left.setVisibility(4);
                Main_B_002.this.m_click_zone_003.setVisibility(4);
                Main_B_002.this.m_click_zone_051.setVisibility(4);
                Main_B_002.this.m_bitmap_000[0] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_006);
                Main_B_002.this.m_bitmap_000[1] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_007);
                Main_B_002.this.m_bitmap_000[2] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_008);
                Main_B_002.this.m_bitmap_000[3] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_009);
                Main_B_002.this.m_bitmap_000[4] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_010);
                Main_B_002.this.m_anim_message = Message.obtain();
                Main_B_002.this.m_anim_handler_000.sendMessage(Main_B_002.this.m_anim_message);
            }
        });
        this.m_anim_handler_001 = new Handler() {
            public void handleMessage(Message _msg) {
                Main_B_002.this.m_change_anim_zone.setVisibility(0);
                if (Main_B_002.this.m_anim_isRepeat) {
                    if (Main_B_002.this.m_anim_page < Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_001.sendMessageDelayed(obtainMessage(), 100);
                    }
                    if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_001.sendMessageDelayed(obtainMessage(), 15);
                    } else if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000) {
                        Main_B_002.this.startActivity(new Intent(Main_B_002.this.getApplicationContext(), Main_A_001.class));
                        Main_B_002.this.finish();
                        Main_B_002.this.overridePendingTransition(0, 0);
                        Main_B_002.this.m_anim_isRepeat = false;
                    }
                    Main_B_002 main_B_002 = Main_B_002.this;
                    main_B_002.m_anim_page = main_B_002.m_anim_page + 1;
                }
            }
        };
        this.m_button_left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Main_B_002.this.m_button_right.setVisibility(4);
                Main_B_002.this.m_button_left.setVisibility(4);
                Main_B_002.this.m_click_zone_003.setVisibility(4);
                Main_B_002.this.m_click_zone_051.setVisibility(4);
                Main_B_002.this.m_bitmap_000[0] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_004);
                Main_B_002.this.m_bitmap_000[1] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_003);
                Main_B_002.this.m_bitmap_000[2] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_002);
                Main_B_002.this.m_bitmap_000[3] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_001);
                Main_B_002.this.m_bitmap_000[4] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.backanim_000);
                Main_B_002.this.m_anim_message = Message.obtain();
                Main_B_002.this.m_anim_handler_001.sendMessage(Main_B_002.this.m_anim_message);
            }
        });
        this.m_click_zone_051.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                TextView text_zone = (TextView) Main_B_002.this.findViewById(R.id.talk);
                text_zone.setText((int) R.string.talk24);
                text_zone.setVisibility(0);
            }
        });
        this.m_anim_handler_002 = new Handler() {
            public void handleMessage(Message _msg) {
                Main_B_002.this.m_change_anim_zone.setVisibility(0);
                if (Main_B_002.this.m_anim_isRepeat) {
                    if (Main_B_002.this.m_anim_page < Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_002.sendMessageDelayed(obtainMessage(), 100);
                    }
                    if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000 - 1) {
                        Main_B_002.this.m_change_anim_zone.setImageBitmap(Main_B_002.this.m_bitmap_000[Main_B_002.this.m_anim_page]);
                        Main_B_002.this.m_anim_handler_002.sendMessageDelayed(obtainMessage(), 15);
                    } else if (Main_B_002.this.m_anim_page == Main_B_002.this.m_BITMAP_SIZE_000) {
                        Main_B_002.this.startActivity(new Intent(Main_B_002.this.getApplicationContext(), Sub_Bed_007.class));
                        Main_B_002.this.finish();
                        Main_B_002.this.overridePendingTransition(0, 0);
                        Main_B_002.this.m_anim_isRepeat = false;
                    }
                    Main_B_002 main_B_002 = Main_B_002.this;
                    main_B_002.m_anim_page = main_B_002.m_anim_page + 1;
                }
            }
        };
        this.m_click_zone_003.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Main_B_002.this.m_button_right.setVisibility(4);
                Main_B_002.this.m_button_left.setVisibility(4);
                Main_B_002.this.m_click_zone_003.setVisibility(4);
                Main_B_002.this.m_click_zone_051.setVisibility(4);
                Main_B_002.this.m_bitmap_000[0] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_001);
                Main_B_002.this.m_bitmap_000[1] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_002);
                Main_B_002.this.m_bitmap_000[2] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_003);
                Main_B_002.this.m_bitmap_000[3] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_004);
                if (Main_B_002.this.m_item001_sp == -100) {
                    Main_B_002.this.m_bitmap_000[4] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_pipe_005);
                } else if (Main_B_002.this.m_item001_sp == 1 || Main_B_002.this.m_item001_sp == -200) {
                    Main_B_002.this.m_bitmap_000[4] = BitmapFactory.decodeResource(Main_B_002.this.e_resources, R.drawable.bedanim_005);
                }
                Main_B_002.this.m_anim_message = Message.obtain();
                Main_B_002.this.m_anim_handler_002.sendMessage(Main_B_002.this.m_anim_message);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.SPool.release();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }
}
