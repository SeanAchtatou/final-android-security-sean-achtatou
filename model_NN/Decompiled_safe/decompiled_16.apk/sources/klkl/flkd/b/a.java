package klkl.flkd.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.DisplayMetrics;
import java.io.File;
import java.lang.reflect.Field;
import klkl.flkd.tools.n;

public final class a {
    private Context a;
    private Drawable b;
    private String c;

    public a(Context context) {
        this.a = context;
    }

    private void a(File file) {
        if (!file.isFile()) {
            File[] listFiles = file.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File a2 : listFiles) {
                    a(a2);
                }
            }
        } else if (file.getName().toLowerCase().endsWith(".apk")) {
            String absolutePath = file.getAbsolutePath();
            PackageInfo packageArchiveInfo = this.a.getPackageManager().getPackageArchiveInfo(absolutePath, 1);
            try {
                Class<?> cls = Class.forName("android.content.pm.PackageParser");
                Object newInstance = cls.getConstructor(String.class).newInstance(file.getAbsolutePath());
                DisplayMetrics displayMetrics = new DisplayMetrics();
                displayMetrics.setToDefaults();
                Object invoke = cls.getDeclaredMethod("parsePackage", File.class, String.class, DisplayMetrics.class, Integer.TYPE).invoke(newInstance, file, file.getAbsolutePath(), displayMetrics, 0);
                if (invoke != null) {
                    Field declaredField = invoke.getClass().getDeclaredField("applicationInfo");
                    if (declaredField.get(invoke) != null) {
                        ApplicationInfo applicationInfo = (ApplicationInfo) declaredField.get(invoke);
                        Class<?> cls2 = Class.forName("android.content.res.AssetManager");
                        Object newInstance2 = cls2.newInstance();
                        cls2.getDeclaredMethod("addAssetPath", String.class).invoke(newInstance2, file.getAbsolutePath());
                        Resources resources = this.a.getResources();
                        Resources newInstance3 = Resources.class.getConstructor(newInstance2.getClass(), resources.getDisplayMetrics().getClass(), resources.getConfiguration().getClass()).newInstance(newInstance2, resources.getDisplayMetrics(), resources.getConfiguration());
                        PackageManager packageManager = this.a.getPackageManager();
                        if (applicationInfo != null) {
                            if (applicationInfo.icon != 0) {
                                this.b = newInstance3.getDrawable(applicationInfo.icon);
                            }
                            if (applicationInfo.labelRes != 0) {
                                this.c = (String) newInstance3.getText(applicationInfo.labelRes);
                            } else {
                                String charSequence = applicationInfo.loadLabel(packageManager).toString();
                                if (charSequence == null || "".equals(charSequence)) {
                                    String name = file.getName();
                                    this.c = name.substring(0, name.lastIndexOf("."));
                                } else {
                                    this.c = charSequence;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (this.a != null && this.c != null && absolutePath != null && packageArchiveInfo != null && packageArchiveInfo.packageName != null) {
                n.a(this.a, this.c, absolutePath, packageArchiveInfo.packageName, this.b);
                SharedPreferences.Editor edit = this.a.getSharedPreferences("data", 0).edit();
                edit.putString(String.valueOf(packageArchiveInfo.packageName) + "_filename", absolutePath);
                edit.commit();
            }
        }
    }

    public final void a() {
        a(new File(Environment.getExternalStorageDirectory() + "/panda/"));
    }
}
