package klkl.flkd.slide;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class SvfDefine extends ViewFlipper {
    public SvfDefine(Context context) {
        super(context);
    }

    public SvfDefine(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }
}
