package klkl.flkd.slide;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

final class c implements View.OnTouchListener {
    private float a = 0.0f;
    private float b = 0.0f;
    private /* synthetic */ a c;

    c(a aVar) {
        this.c = aVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a = motionEvent.getX();
                return true;
            case 1:
                this.b = motionEvent.getX();
                if (this.a >= this.b) {
                    if (this.c.f.getCurrentView() == this.c.h) {
                        this.c.f.stopFlipping();
                        a.a();
                        return true;
                    }
                    SvfDefine c2 = this.c.f;
                    a aVar = this.c;
                    c2.setInAnimation(a.b());
                    SvfDefine c3 = this.c.f;
                    a aVar2 = this.c;
                    c3.setOutAnimation(a.c());
                    this.c.f.showNext();
                    return true;
                } else if (this.a >= this.b) {
                    return true;
                } else {
                    if (this.c.f.getCurrentView() == this.c.g) {
                        Toast makeText = Toast.makeText(this.c.c, "已经是第一张", 0);
                        makeText.setGravity(80, 0, 0);
                        makeText.show();
                        return true;
                    }
                    SvfDefine c4 = this.c.f;
                    a aVar3 = this.c;
                    c4.setInAnimation(a.d());
                    SvfDefine c5 = this.c.f;
                    a aVar4 = this.c;
                    c5.setOutAnimation(a.e());
                    this.c.f.showPrevious();
                    return true;
                }
            case 2:
            default:
                return true;
        }
    }
}
