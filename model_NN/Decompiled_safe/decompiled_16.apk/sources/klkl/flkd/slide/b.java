package klkl.flkd.slide;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import klkl.flkd.tools.r;

final class b implements SensorEventListener {
    private /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (this.a.l.orientation == 2) {
            this.a.d.width = (r.ae * 1) / 3;
            this.a.d.height = (r.af * 6) / 7;
            return;
        }
        this.a.d.width = (r.ae * 4) / 5;
        this.a.d.height = (r.af * 4) / 5;
    }
}
