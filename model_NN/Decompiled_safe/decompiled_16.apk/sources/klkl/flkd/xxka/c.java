package klkl.flkd.xxka;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabWidget;

public final class c extends RelativeLayout {
    public static ViewPager b;
    public LinearLayout a;
    private Activity c;
    private int d = 11;
    private int e = 16908307;
    private int f = 16908305;
    private FrameLayout g;
    private int h = 562787;

    public c(Activity activity) {
        super(activity);
        this.c = activity;
        this.a = new LinearLayout(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10, -1);
        this.a.setLayoutParams(layoutParams);
        this.a.setId(this.d);
        addView(this.a);
        this.g = new FrameLayout(this.c);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(3, this.d);
        layoutParams2.addRule(2, this.e);
        this.g.setLayoutParams(layoutParams2);
        this.g.setId(this.f);
        this.g.setBackgroundColor(-1);
        this.g.setVisibility(8);
        addView(this.g);
        ViewPager viewPager = new ViewPager(this.c);
        b = viewPager;
        viewPager.setBackgroundColor(-1);
        b.setId(this.h);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(3, this.d);
        layoutParams3.addRule(2, this.e);
        addView(b, layoutParams3);
        TabWidget tabWidget = new TabWidget(this.c);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(12, -1);
        tabWidget.setLayoutParams(layoutParams4);
        tabWidget.setId(this.e);
        addView(tabWidget);
    }
}
