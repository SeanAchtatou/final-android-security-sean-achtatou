package klkl.flkd.xxka;

import android.app.LocalActivityManager;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import klkl.flkd.tools.Rubbish;
import klkl.flkd.tools.j;
import klkl.flkd.tools.r;

public abstract class LtActy extends TabActivity implements ViewPager.OnPageChangeListener, TabHost.OnTabChangeListener {
    public static List a;
    public static List b;
    /* access modifiers changed from: private */
    public TabHost c;
    private c d;
    private int e = 16908306;
    private LocalActivityManager f = null;
    private j g;
    private List h;
    /* access modifiers changed from: private */
    public int i = 0;
    private boolean j = true;
    private Handler k = new b(this);

    private View a(String str, Intent intent) {
        return this.f.startActivity(str, intent).getDecorView();
    }

    private void d() {
        ((ViewGroup) this.h.get(1)).addView(a("1", b(1)));
        this.g.notifyDataSetChanged();
    }

    private void e() {
        a = new ArrayList();
        b = new ArrayList();
        for (int i2 = 0; i2 < c(); i2++) {
            a.add(new TextView(this));
            b.add(new ImageView(this));
        }
    }

    private void f() {
        int c2 = c();
        int i2 = r.ae / c2;
        for (int i3 = 0; i3 < c2; i3++) {
            LinearLayout linearLayout = new LinearLayout(this);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(i2, -2);
            ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(i2, 5);
            ViewGroup.LayoutParams layoutParams3 = r.ag >= 320 ? new ViewGroup.LayoutParams(i2, 85) : (r.ag < 240 || r.ag >= 320) ? (240 <= r.ag || r.ag < 180) ? (r.ag >= 180 || r.ag <= 120) ? r.ag <= 120 ? new ViewGroup.LayoutParams(i2, 27) : null : new ViewGroup.LayoutParams(i2, 36) : new ViewGroup.LayoutParams(i2, 48) : new ViewGroup.LayoutParams(i2, 64);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(1);
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, 5));
            relativeLayout.setGravity(48);
            ImageView imageView = (ImageView) b.get(i3);
            imageView.setLayoutParams(layoutParams2);
            relativeLayout.addView(imageView);
            linearLayout.addView(relativeLayout);
            TextView textView = (TextView) a.get(i3);
            textView.setLayoutParams(layoutParams3);
            textView.setGravity(17);
            linearLayout.setGravity(17);
            linearLayout.addView(textView);
            a(textView, imageView, i3);
            TabHost.TabSpec newTabSpec = this.c.newTabSpec(a(i3));
            newTabSpec.setIndicator(linearLayout);
            newTabSpec.setContent(new Intent(this, Rubbish.class));
            if (this.h.size() == 0) {
                this.h.add(a(String.valueOf(i3), b(i3)));
            } else {
                LinearLayout linearLayout2 = new LinearLayout(this);
                linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                linearLayout2.setGravity(17);
                this.h.add(linearLayout2);
            }
            this.c.addTab(newTabSpec);
        }
        c.b.setAdapter(this.g);
        c.b.setCurrentItem(0);
    }

    /* access modifiers changed from: protected */
    public abstract String a(int i2);

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public abstract void a(TextView textView, ImageView imageView, int i2);

    /* access modifiers changed from: protected */
    public abstract Intent b(int i2);

    /* access modifiers changed from: protected */
    public View b() {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public final void c(int i2) {
        this.c.setCurrentTab(0);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        requestWindowFeature(1);
        TabHost tabHost = new TabHost(this);
        tabHost.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        tabHost.setId(this.e);
        this.d = new c(this);
        tabHost.addView(this.d);
        setContentView(tabHost);
        this.c = getTabHost();
        getTabWidget();
        this.f = new LocalActivityManager(this, true);
        this.f.dispatchCreate(bundle);
        this.h = new ArrayList();
        this.g = new j(this.h);
        c.b.setOnPageChangeListener(this);
        this.c.setOnTabChangedListener(this);
        a();
        this.d.a.addView(b());
        e();
        f();
    }

    public void onPageScrollStateChanged(int i2) {
    }

    public void onPageScrolled(int i2, float f2, int i3) {
    }

    public void onPageSelected(int i2) {
        boolean z = false;
        this.i = i2;
        this.k.sendEmptyMessageDelayed(0, 300);
        if (this.j && i2 == 1) {
            if (!this.j) {
                z = true;
            }
            this.j = z;
            d();
        }
    }

    public void onTabChanged(String str) {
        if (this.c.getCurrentTab() == 0) {
            ((TextView) a.get(0)).setTextColor(-16776961);
            ((TextView) a.get(1)).setTextColor(-7829368);
            ImageView imageView = (ImageView) b.get(1);
            imageView.clearAnimation();
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) ((-r.ae) / c()), 0.0f, 0.0f);
            imageView.setAnimation(translateAnimation);
            translateAnimation.setDuration(500);
            translateAnimation.setFillAfter(true);
            translateAnimation.startNow();
            ImageView imageView2 = (ImageView) b.get(0);
            imageView2.clearAnimation();
            TranslateAnimation translateAnimation2 = new TranslateAnimation((float) (r.ae / c()), 0.0f, 0.0f, 0.0f);
            imageView2.setAnimation(translateAnimation2);
            translateAnimation2.setDuration(500);
            translateAnimation2.setFillAfter(true);
            translateAnimation2.startNow();
            c.b.setCurrentItem(0);
        } else if (this.c.getCurrentTab() == 1) {
            ((TextView) a.get(1)).setTextColor(-16776961);
            ((TextView) a.get(0)).setTextColor(-7829368);
            ((ImageView) b.get(1)).setVisibility(0);
            ImageView imageView3 = (ImageView) b.get(0);
            imageView3.clearAnimation();
            TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, (float) (r.ae / c()), 0.0f, 0.0f);
            imageView3.setAnimation(translateAnimation3);
            translateAnimation3.setDuration(500);
            translateAnimation3.setFillAfter(true);
            translateAnimation3.startNow();
            ImageView imageView4 = (ImageView) b.get(1);
            imageView4.clearAnimation();
            TranslateAnimation translateAnimation4 = new TranslateAnimation((float) ((-r.ae) / c()), 0.0f, 0.0f, 0.0f);
            imageView4.setAnimation(translateAnimation4);
            translateAnimation4.setDuration(500);
            translateAnimation4.setFillAfter(true);
            translateAnimation4.startNow();
            c.b.setCurrentItem(1);
            if (this.j) {
                this.j = !this.j;
                d();
            }
        }
    }
}
