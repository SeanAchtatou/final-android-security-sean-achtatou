package klkl.flkd.jkou;

import android.content.Context;
import android.util.Log;
import klkl.flkd.d.a;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import klkl.flkd.tools.r;
import org.json.JSONException;
import org.json.JSONObject;

final class e implements q {
    private /* synthetic */ ChJk a;

    public e(ChJk chJk, Context context) {
        this.a = chJk;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        r.ai = false;
        new o(context, this, jSONObject.toString()).start();
    }

    public final void a(Context context, Object obj) {
        try {
            JSONObject jSONObject = new JSONObject(obj.toString());
            Log.e("flag", "flag = " + jSONObject.getString("flag"));
            if ("1".equals(jSONObject.getString("flag"))) {
                this.a.floatWindow = a.a(ChJk.myContext);
                this.a.install();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
