package klkl.flkd.c;

import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.Map;
import klkl.flkd.tools.r;

public final class b extends Thread {
    private Context a;
    private String b;
    private String c;
    private Map d;
    private String e;

    public b(Context context, String str, String str2, Map map, String str3) {
        this.a = context;
        this.b = str;
        this.c = str2;
        this.d = map;
        this.e = str3;
    }

    private static boolean a(String str, Map map, a[] aVarArr) {
        byte[] bArr = null;
        int length = aVarArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            a aVar = aVarArr[i];
            StringBuilder sb = new StringBuilder();
            sb.append("--");
            sb.append("---------------------------7da2137580612");
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data;name=\"" + aVar.d() + "\";filename=\"" + aVar.c() + "\"\r\n");
            sb.append("Content-Type: " + aVar.e() + "\r\n\r\n");
            sb.append("\r\n");
            int length2 = i2 + sb.length();
            i++;
            i2 = aVar.b() != null ? (int) (((long) length2) + aVar.a().length()) : bArr.length + length2;
        }
        StringBuilder sb2 = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            sb2.append("--");
            sb2.append("---------------------------7da2137580612");
            sb2.append("\r\n");
            sb2.append("Content-Disposition: form-data; name=\"" + ((String) entry.getKey()) + "\"\r\n\r\n");
            sb2.append((String) entry.getValue());
            sb2.append("\r\n");
        }
        int length3 = "-----------------------------7da2137580612--\r\n".getBytes().length + sb2.toString().getBytes().length + i2;
        URL url = new URL(str);
        int port = url.getPort() == -1 ? 80 : url.getPort();
        Socket socket = new Socket(InetAddress.getByName(url.getHost()), port);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(("POST " + url.getPath() + " HTTP/1.1\r\n").getBytes());
        outputStream.write("Accept: image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*\r\n".getBytes());
        outputStream.write("Accept-Language: zh-CN\r\n".getBytes());
        outputStream.write("Content-Type: multipart/form-data; boundary=---------------------------7da2137580612\r\n".getBytes());
        outputStream.write(("Content-Length: " + length3 + "\r\n").getBytes());
        outputStream.write("Connection: Keep-Alive\r\n".getBytes());
        outputStream.write(("Host: " + url.getHost() + ":" + port + "\r\n").getBytes());
        outputStream.write("\r\n".getBytes());
        outputStream.write(sb2.toString().getBytes());
        for (a aVar2 : aVarArr) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("--");
            sb3.append("---------------------------7da2137580612");
            sb3.append("\r\n");
            sb3.append("Content-Disposition: form-data;name=\"" + aVar2.d() + "\";filename=\"" + aVar2.c() + "\"\r\n");
            sb3.append("Content-Type: " + aVar2.e() + "\r\n\r\n");
            outputStream.write(sb3.toString().getBytes());
            if (aVar2.b() != null) {
                byte[] bArr2 = new byte[1024];
                while (true) {
                    int read = aVar2.b().read(bArr2, 0, 1024);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(bArr2, 0, read);
                }
                aVar2.b().close();
            } else {
                outputStream.write(bArr, 0, bArr.length);
            }
            outputStream.write("\r\n".getBytes());
        }
        outputStream.write("-----------------------------7da2137580612--\r\n".getBytes());
        outputStream.flush();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        int indexOf = bufferedReader.readLine().indexOf("200");
        if (indexOf == -1) {
            return false;
        }
        Log.e("result", "result = " + indexOf);
        outputStream.flush();
        outputStream.close();
        bufferedReader.close();
        socket.close();
        return true;
    }

    public final void run() {
        boolean z;
        Looper.prepare();
        String str = this.b;
        if (str == null) {
            try {
                z = a(this.c, this.d, new a[0]);
            } catch (Exception e2) {
                e2.printStackTrace();
                z = false;
            }
        } else {
            File file = new File(str);
            try {
                z = a(this.c, this.d, new a[]{new a(file.getName(), file, "image", "application/octet-stream")});
            } catch (Exception e3) {
                e3.printStackTrace();
                z = false;
            }
        }
        if (z) {
            Intent intent = null;
            if (this.e.equals("square")) {
                Intent intent2 = new Intent("loadSquare");
                intent2.putExtra("who", "");
                intent2.putExtra("userNmae", r.z);
                intent2.putExtra("noteInfo", (String) this.d.get("sdetail"));
                intent2.putExtra("noteInfoID", (String) this.d.get("uuid"));
                intent2.putExtra("date", "最近");
                intent2.putExtra("time", "");
                intent2.putExtra("rank", r.B);
                intent2.putExtra("imgUrl", "sd" + str);
                intent = intent2;
            } else if (this.e.equals("tribute")) {
                Intent intent3 = new Intent("loadTribute");
                intent3.putExtra("who", "");
                intent3.putExtra("userNmae", r.z);
                intent3.putExtra("noteInfo", (String) this.d.get("appnoteInfo"));
                intent3.putExtra("noteInfoID", (String) this.d.get("uuid"));
                intent3.putExtra("date", "最近");
                intent3.putExtra("time", "");
                intent3.putExtra("rank", r.B);
                intent3.putExtra("imgUrl", "sd" + str);
                intent = intent3;
            }
            this.a.sendBroadcast(intent);
        }
        Looper.loop();
    }
}
