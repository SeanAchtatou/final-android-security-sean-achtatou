package klkl.flkd.d;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.view.MotionEvent;
import android.view.View;
import klkl.flkd.xxka.XxKNrTj;

final class c implements View.OnTouchListener {
    private float a = 0.0f;
    private float b = 0.0f;
    private PackageInfo c;
    private String d;
    private SharedPreferences e;
    private /* synthetic */ a f;

    c(a aVar) {
        this.f = aVar;
        this.c = aVar.d.getPackageManager().getPackageInfo(aVar.d.getPackageName(), 0);
        this.d = this.c.packageName;
        this.e = aVar.d.getSharedPreferences(this.d, 0);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        this.f.v = motionEvent.getRawX();
        a aVar = this.f;
        motionEvent.getRawY();
        switch (motionEvent.getAction()) {
            case 0:
                this.a = motionEvent.getY();
                this.f.h.setImageBitmap(this.f.z);
                this.f.w = this.f.v;
                this.f.t = motionEvent.getX();
                this.f.u = motionEvent.getY();
                break;
            case 1:
                this.b = motionEvent.getY();
                this.f.h.setImageBitmap(this.f.y);
                this.f.x = this.f.v;
                this.f.a();
                if (Math.abs(this.f.w - this.f.x) <= 20.0f && this.a < this.b) {
                    this.f.f.x = (int) (this.f.w - this.f.t);
                    this.f.f.y = 0;
                    this.f.e.updateViewLayout(a.a, this.f.f);
                    if (XxKNrTj.c == null) {
                        if (!this.e.getString("drop", "no").equals("drop")) {
                            SharedPreferences.Editor edit = this.e.edit();
                            edit.putString("installFlag", this.e.getString("installFlag", "no"));
                            edit.putString("click", "click");
                            edit.putString("drop", "drop");
                            edit.commit();
                        }
                        Intent intent = new Intent(this.f.d, XxKNrTj.class);
                        intent.addFlags(67108864);
                        this.f.d.startActivity(intent);
                    } else {
                        XxKNrTj.c.finish();
                    }
                } else if (!this.f.b && this.a >= this.b) {
                    if (XxKNrTj.c == null) {
                        if (!this.e.getString("click", "no").equals("click")) {
                            SharedPreferences.Editor edit2 = this.e.edit();
                            edit2.putString("installFlag", this.e.getString("installFlag", "no"));
                            edit2.putString("drop", "drop");
                            edit2.putString("click", "click");
                            edit2.commit();
                        }
                        Intent intent2 = new Intent(this.f.d, XxKNrTj.class);
                        intent2.addFlags(67108864);
                        this.f.d.startActivity(intent2);
                    } else {
                        XxKNrTj.c.finish();
                    }
                }
                this.f.b = false;
                a aVar2 = this.f;
                this.f.u = 0.0f;
                aVar2.t = 0.0f;
                if (this.f.u == 0.0f) {
                    this.f.g.topMargin = this.f.E;
                    break;
                }
                break;
            case 2:
                this.f.a();
                this.b = motionEvent.getY();
                if (this.a < this.b) {
                    this.f.g.topMargin = this.f.D;
                    this.f.b = true;
                    break;
                }
                break;
        }
        return false;
    }
}
