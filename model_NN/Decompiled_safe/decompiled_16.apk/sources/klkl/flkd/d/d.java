package klkl.flkd.d;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import klkl.flkd.a.a;
import klkl.flkd.tribute.Ft;

final class d implements View.OnLongClickListener {
    private /* synthetic */ a a;

    d(a aVar) {
        this.a = aVar;
    }

    public final boolean onLongClick(View view) {
        Activity f = this.a.d;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display defaultDisplay = ((WindowManager) f.getSystemService("window")).getDefaultDisplay();
        defaultDisplay.getMetrics(displayMetrics);
        int i = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        int pixelFormat = defaultDisplay.getPixelFormat();
        PixelFormat pixelFormat2 = new PixelFormat();
        PixelFormat.getPixelFormatInfo(pixelFormat, pixelFormat2);
        int i3 = pixelFormat2.bytesPerPixel;
        try {
            byte[] bArr = new byte[(i * i2 * i3)];
            int[] iArr = new int[(i * i2)];
            try {
                if (new DataInputStream(new FileInputStream(new File(new File("/dev/graphics/"), "fb0"))).read(bArr, 0, i3 * i * i2) != -1) {
                    for (int i4 = 0; i4 < bArr.length; i4++) {
                        if (i4 % 4 == 0) {
                            iArr[i4 / 4] = (bArr[i4] & 255) + ((bArr[i4 + 1] & 255) << 8) + ((bArr[i4 + 2] & 255) << 16) + ((bArr[i4 + 3] & 255) << 24);
                        }
                    }
                    Bitmap createBitmap = Bitmap.createBitmap(iArr, i2, i, Bitmap.Config.ARGB_8888);
                    ((Vibrator) f.getSystemService("vibrator")).vibrate(1000);
                    a.a("/mnt/sdcard/MyScreen/", "cut_img.jpg", createBitmap);
                    Intent intent = new Intent(f, Ft.class);
                    intent.putExtra("loadFlag", "loadTribute");
                    intent.putExtra("postOrReply", "post");
                    intent.putExtra("isScreenShotPost", "isScreenShotPost");
                    f.startActivity(intent);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(f, "未取得Root权限，请确定手机有至高的权利！", 0).show();
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            Toast.makeText(f, "未取得Root权限，请确定手机有至高的权利！", 0).show();
        }
        return false;
    }
}
