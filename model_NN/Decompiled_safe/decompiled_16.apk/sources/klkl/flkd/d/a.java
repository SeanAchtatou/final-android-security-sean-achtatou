package klkl.flkd.d;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.WindowManager;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.io.File;
import klkl.flkd.tools.i;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public final class a {
    public static RelativeLayout a;
    private static a c;
    private Bitmap A;
    private int B;
    private int C;
    /* access modifiers changed from: private */
    public int D;
    /* access modifiers changed from: private */
    public int E;
    boolean b = false;
    /* access modifiers changed from: private */
    public Activity d = null;
    /* access modifiers changed from: private */
    public WindowManager e = null;
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams f = null;
    /* access modifiers changed from: private */
    public RelativeLayout.LayoutParams g;
    /* access modifiers changed from: private */
    public ImageView h = null;
    private ImageView i = null;
    private LinearLayout j;
    private LinearLayout k;
    private Sensor l;
    private SensorEventListener m;
    private SensorManager n;
    /* access modifiers changed from: private */
    public Configuration o;
    /* access modifiers changed from: private */
    public float p = 0.0f;
    /* access modifiers changed from: private */
    public float q = 0.0f;
    /* access modifiers changed from: private */
    public float r;
    /* access modifiers changed from: private */
    public float s;
    /* access modifiers changed from: private */
    public float t;
    /* access modifiers changed from: private */
    public float u;
    /* access modifiers changed from: private */
    public float v;
    /* access modifiers changed from: private */
    public float w;
    /* access modifiers changed from: private */
    public float x;
    /* access modifiers changed from: private */
    public Bitmap y;
    /* access modifiers changed from: private */
    public Bitmap z;

    private a(Activity activity) {
        this.d = activity;
        this.y = o.a("bitmap/panda1.png", this.d);
        this.z = o.a("bitmap/touchpanda1.png", this.d);
        this.A = o.a("bitmap/line1.png", this.d);
        if (r.ag >= 320) {
            this.E = 10;
            this.D = 20;
            this.B = r.ae / 6;
            this.C = (r.ae / 6) + 10;
        } else if (r.ag >= 240 && r.ag < 320) {
            this.E = 8;
            this.D = 18;
            new i(this.d);
            this.y = i.a(this.y, (this.y.getWidth() * 3) / 4, (this.y.getHeight() * 3) / 4);
            new i(this.d);
            this.z = i.a(this.z, (this.z.getWidth() * 3) / 4, (this.z.getHeight() * 3) / 4);
            new i(this.d);
            this.A = i.a(this.A, (this.A.getWidth() * 3) / 4, (this.A.getHeight() * 3) / 4);
            this.B = r.ae / 5;
            this.C = (r.ae / 5) + 10;
        } else if (240 > r.ag && r.ag >= 180) {
            this.E = 6;
            this.D = 14;
            new i(this.d);
            this.y = i.a(this.y, (this.y.getWidth() * 3) / 4, (this.y.getHeight() * 3) / 4);
            new i(this.d);
            this.z = i.a(this.z, (this.z.getWidth() * 3) / 4, (this.z.getHeight() * 3) / 4);
            new i(this.d);
            this.A = i.a(this.A, (this.A.getWidth() * 3) / 4, (this.A.getHeight() * 3) / 4);
            this.B = r.ae / 5;
            this.C = (r.ae / 5) + 10;
        } else if (r.ag < 180 && r.ag > 120) {
            this.E = 5;
            this.D = 12;
            new i(this.d);
            this.y = i.a(this.y, (this.y.getWidth() * 9) / 16, (this.y.getHeight() * 9) / 16);
            new i(this.d);
            this.z = i.a(this.z, (this.z.getWidth() * 9) / 16, (this.z.getHeight() * 9) / 16);
            new i(this.d);
            this.A = i.a(this.A, (this.A.getWidth() * 9) / 16, (this.A.getHeight() * 9) / 16);
            this.B = r.ae / 5;
            this.C = r.ae / 5;
        } else if (r.ag <= 120) {
            this.E = 4;
            this.D = 8;
            new i(this.d);
            this.y = i.a(this.y, (this.y.getWidth() * 27) / 64, (this.y.getHeight() * 27) / 64);
            new i(this.d);
            this.z = i.a(this.z, (this.z.getWidth() * 27) / 64, (this.z.getHeight() * 27) / 64);
            new i(this.d);
            this.A = i.a(this.A, (this.A.getWidth() * 27) / 64, (this.A.getHeight() * 27) / 64);
            this.B = r.ae / 6;
            this.C = r.ae / 6;
        }
        this.e = (WindowManager) this.d.getApplicationContext().getSystemService("window");
        this.f = new WindowManager.LayoutParams();
        this.f.type = 2002;
        this.f.format = 1;
        this.f.flags = 40;
        this.f.gravity = 51;
        this.f.x = 0;
        this.f.y = 0;
        this.f.width = this.B;
        this.f.height = this.C;
        a = new RelativeLayout(this.d);
        this.j = new LinearLayout(this.d);
        this.k = new LinearLayout(this.d);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.leftMargin = 50;
        this.g = new RelativeLayout.LayoutParams(-1, -1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        this.g.setMargins(0, this.E, 0, 10);
        this.h = new ImageView(this.d);
        this.h.setImageBitmap(this.y);
        this.j.addView(this.h, layoutParams);
        this.i = new ImageView(this.d);
        this.i.setImageBitmap(this.A);
        this.k.addView(this.i, layoutParams2);
        a.addView(this.k, layoutParams3);
        a.addView(this.j, this.g);
        this.e.addView(a, this.f);
        c();
        this.o = this.d.getResources().getConfiguration();
        this.n = (SensorManager) this.d.getSystemService("sensor");
        this.l = this.n.getDefaultSensor(1);
        this.m = new b(this);
        this.n.registerListener(this.m, this.l, 0);
    }

    public static a a(Activity activity) {
        if (c == null) {
            c = new a(activity);
        }
        return c;
    }

    static /* synthetic */ void a(a aVar, float f2, float f3) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        aVar.h = new ImageView(aVar.j.getContext());
        aVar.h.setImageBitmap(aVar.y);
        aVar.h.setScaleType(ImageView.ScaleType.CENTER);
        aVar.j.removeAllViews();
        aVar.j.addView(aVar.h, layoutParams);
        RotateAnimation rotateAnimation = new RotateAnimation(f2, f3, 1, 0.5f, 1, 0.0f);
        aVar.h.setAnimation(rotateAnimation);
        rotateAnimation.startNow();
        rotateAnimation.setRepeatCount(0);
        rotateAnimation.setFillAfter(true);
    }

    private void c() {
        try {
            a.setOnTouchListener(new c(this));
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        a.setOnLongClickListener(new d(this));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.f.x = (int) (this.v - this.t);
        this.f.y = 0;
        this.e.updateViewLayout(a, this.f);
    }

    public final void b() {
        File file = new File("/data/data/" + this.d.getPackageName().toString() + "/shared_prefs", "AppData.xml");
        if (file.exists()) {
            file.delete();
        }
        File file2 = new File("/data/data/" + this.d.getPackageName().toString() + "/shared_prefs", "GameData.xml");
        if (file2.exists()) {
            file2.delete();
        }
        this.n.unregisterListener(this.m);
        a.removeAllViewsInLayout();
        this.e.removeView(a);
        this.e = null;
        c = null;
    }
}
