package klkl.flkd.tribute;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import klkl.flkd.tools.r;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: klkl.flkd.tribute.o  reason: case insensitive filesystem */
final class C0063o implements q {
    private Activity a;
    private /* synthetic */ C0059k b;

    public C0063o(C0059k kVar, Activity activity) {
        this.b = kVar;
        this.a = activity;
        if (o.b((Context) activity)) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (kVar.r.equals("loadTribute") || kVar.r.equals("allnotes")) {
                    jSONObject.put("para", r.l);
                    jSONObject.put("url", "questionID=" + kVar.j.get("noteID") + "&pagenumber=" + kVar.o + "&pagesize=10&" + "registerAccount=" + r.z + "&sayRegisterAccount=" + kVar.j.get("name"));
                    r.ai = false;
                    new o(this.a.getApplication(), this, jSONObject.toString(), "discuss").start();
                }
                if (kVar.r.equals("loadSquare")) {
                    jSONObject.put("para", r.t);
                    jSONObject.put("url", "sid=" + kVar.j.get("noteID") + "&pagenumber=" + kVar.o + "&pagesize=10&" + "sayRegisterAccount=" + kVar.j.get("name") + "&registerAccount=" + r.z);
                }
                r.ai = false;
                new o(this.a.getApplication(), this, jSONObject.toString(), "discuss").start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this.a, "网络不给力哦……请您连接网络", 1).show();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0088 A[LOOP:0: B:10:0x004d->B:17:0x0088, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r7, java.lang.Object r8) {
        /*
            r6 = this;
            r5 = 1
            r2 = 0
            java.lang.String r3 = r8.toString()
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x003d }
            r0.<init>(r3)     // Catch:{ JSONException -> 0x003d }
            int r1 = r0.length()     // Catch:{ JSONException -> 0x003d }
            int r1 = r1 + -1
            org.json.JSONObject r0 = r0.getJSONObject(r1)     // Catch:{ JSONException -> 0x003d }
            java.lang.String r1 = "flag"
            int r0 = r0.getInt(r1)     // Catch:{ JSONException -> 0x003d }
            if (r0 != 0) goto L_0x0043
            klkl.flkd.tribute.k r1 = r6.b     // Catch:{ JSONException -> 0x00d7 }
            klkl.flkd.tools.f r1 = r1.p     // Catch:{ JSONException -> 0x00d7 }
            r4 = 8
            r1.setVisibility(r4)     // Catch:{ JSONException -> 0x00d7 }
            klkl.flkd.tribute.k r1 = r6.b     // Catch:{ JSONException -> 0x00d7 }
            android.widget.ListView r1 = r1.g     // Catch:{ JSONException -> 0x00d7 }
            r4 = 0
            r1.setVisibility(r4)     // Catch:{ JSONException -> 0x00d7 }
            java.lang.String r1 = "加载失败！"
            r4 = 0
            android.widget.Toast r1 = android.widget.Toast.makeText(r7, r1, r4)     // Catch:{ JSONException -> 0x00d7 }
            r1.show()     // Catch:{ JSONException -> 0x00d7 }
        L_0x003c:
            return
        L_0x003d:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0040:
            r1.printStackTrace()
        L_0x0043:
            r1 = r0
            klkl.flkd.tribute.k r0 = r6.b
            java.util.List r3 = klkl.flkd.tools.o.a(r3)
            r0.l = r3
        L_0x004d:
            klkl.flkd.tribute.k r0 = r6.b
            java.util.List r0 = r0.l
            int r0 = r0.size()
            if (r2 < r0) goto L_0x0088
            klkl.flkd.tribute.k r0 = r6.b
            klkl.flkd.tribute.k r2 = r6.b
            java.util.List r2 = r2.k
            int r2 = r2.size()
            r0.n = r2
            r0 = 2
            if (r1 == r0) goto L_0x00c0
            klkl.flkd.tribute.k r0 = r6.b
            int r0 = r0.o
            if (r0 != r5) goto L_0x00a1
            klkl.flkd.tribute.k r0 = r6.b
            int r1 = r0.o
            int r1 = r1 + 1
            r0.o = r1
            klkl.flkd.tribute.k r0 = r6.b
            android.os.Handler r0 = r0.u
            r0.sendEmptyMessage(r5)
            goto L_0x003c
        L_0x0088:
            klkl.flkd.tribute.k r0 = r6.b
            java.util.List r3 = r0.k
            klkl.flkd.tribute.k r0 = r6.b
            java.util.List r0 = r0.l
            java.lang.Object r0 = r0.get(r2)
            java.util.Map r0 = (java.util.Map) r0
            r3.add(r0)
            int r0 = r2 + 1
            r2 = r0
            goto L_0x004d
        L_0x00a1:
            klkl.flkd.tribute.k r0 = r6.b
            int r0 = r0.o
            if (r0 == r5) goto L_0x003c
            klkl.flkd.tribute.k r0 = r6.b
            int r1 = r0.o
            int r1 = r1 + 1
            r0.o = r1
            klkl.flkd.tribute.k r0 = r6.b
            android.os.Handler r0 = r0.u
            r1 = 3
            r0.sendEmptyMessage(r1)
            goto L_0x003c
        L_0x00c0:
            klkl.flkd.tribute.k r0 = r6.b
            int r1 = r0.o
            int r1 = r1 + 1
            r0.o = r1
            klkl.flkd.tribute.k r0 = r6.b
            android.os.Handler r0 = r0.u
            r1 = 4
            r0.sendEmptyMessage(r1)
            goto L_0x003c
        L_0x00d7:
            r1 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: klkl.flkd.tribute.C0063o.a(android.content.Context, java.lang.Object):void");
    }
}
