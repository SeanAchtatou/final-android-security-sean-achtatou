package klkl.flkd.tribute;

import android.graphics.Bitmap;
import android.widget.ImageView;
import klkl.flkd.a.d;
import klkl.flkd.tools.i;

/* renamed from: klkl.flkd.tribute.af  reason: case insensitive filesystem */
final class C0048af implements d {
    private /* synthetic */ C0046ad a;

    C0048af(C0046ad adVar) {
        this.a = adVar;
    }

    public final void a(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(i.a(bitmap, this.a.f));
    }
}
