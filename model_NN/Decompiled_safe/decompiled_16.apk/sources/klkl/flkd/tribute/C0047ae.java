package klkl.flkd.tribute;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import java.util.Map;

/* renamed from: klkl.flkd.tribute.ae  reason: case insensitive filesystem */
final class C0047ae extends Handler {
    private /* synthetic */ C0046ad a;

    C0047ae(C0046ad adVar) {
        this.a = adVar;
    }

    public final void handleMessage(Message message) {
        Intent intent = new Intent("non_support");
        intent.putExtra("position", message.arg1);
        if (message.what == 1365) {
            intent.putExtra("support", ((Integer) ((Map) this.a.b.get(message.arg1)).get("support")).intValue() + 1);
            intent.putExtra("nonsupport", (Integer) ((Map) this.a.b.get(message.arg1)).get("nonsupport"));
        } else if (message.what == 1366) {
            intent.putExtra("nonsupport", ((Integer) ((Map) this.a.b.get(message.arg1)).get("nonsupport")).intValue() + 1);
            intent.putExtra("support", (Integer) ((Map) this.a.b.get(message.arg1)).get("support"));
        }
        this.a.a.sendBroadcast(intent);
    }
}
