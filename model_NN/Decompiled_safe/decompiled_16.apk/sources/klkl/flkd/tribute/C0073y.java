package klkl.flkd.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: klkl.flkd.tribute.y  reason: case insensitive filesystem */
final class C0073y extends BroadcastReceiver {
    private /* synthetic */ Ft a;

    private C0073y(Ft ft) {
        this.a = ft;
    }

    /* synthetic */ C0073y(Ft ft, byte b) {
        this(ft);
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("loadSquare") || intent.getAction().equals("loadTribute")) {
            this.a.finish();
        }
    }
}
