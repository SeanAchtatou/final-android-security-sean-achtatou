package klkl.flkd.tribute;

import android.view.View;
import android.widget.Toast;

final class aE implements View.OnClickListener {
    private /* synthetic */ aA a;

    private aE(aA aAVar) {
        this.a = aAVar;
    }

    /* synthetic */ aE(aA aAVar, byte b) {
        this(aAVar);
    }

    public final void onClick(View view) {
        switch (view.getId()) {
            case 33333:
                String editable = this.a.c.getText().toString();
                String editable2 = this.a.d.getText().toString();
                if (editable.equals("")) {
                    Toast.makeText(this.a.a, "账号不能为空，请修改！", 1).show();
                    return;
                } else if (this.a.d.getText().toString().equals("")) {
                    Toast.makeText(this.a.a, "密码不能为空", 1).show();
                    return;
                } else if (editable2.length() < 6) {
                    Toast.makeText(this.a.a, "密码过短，请修改！", 0).show();
                    return;
                } else if (editable2.length() > 18) {
                    Toast.makeText(this.a.a, "密码过长，请修改！", 0).show();
                    return;
                } else if (this.a.d.getText().toString().equals(this.a.e.getText().toString())) {
                    new aD(this.a, this.a.a, editable, this.a.d.getText().toString());
                    return;
                } else {
                    Toast.makeText(this.a.a, "前后两次密码不同！", 1).show();
                    return;
                }
            case 44444:
                aA.f(this.a);
                return;
            default:
                return;
        }
    }
}
