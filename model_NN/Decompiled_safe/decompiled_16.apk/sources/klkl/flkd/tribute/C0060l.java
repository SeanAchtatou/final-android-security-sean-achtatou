package klkl.flkd.tribute;

import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;

/* renamed from: klkl.flkd.tribute.l  reason: case insensitive filesystem */
final class C0060l extends Handler {
    private /* synthetic */ C0059k a;

    C0060l(C0059k kVar) {
        this.a = kVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: klkl.flkd.tribute.k.a(klkl.flkd.tribute.k, boolean):void
     arg types: [klkl.flkd.tribute.k, int]
     candidates:
      klkl.flkd.tribute.k.a(klkl.flkd.tribute.k, int):void
      klkl.flkd.tribute.k.a(klkl.flkd.tribute.k, java.util.List):void
      klkl.flkd.tribute.k.a(klkl.flkd.tribute.k, klkl.flkd.tribute.i):void
      klkl.flkd.tribute.k.a(klkl.flkd.tribute.k, boolean):void */
    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                this.a.q = false;
                if (this.a.o == 2) {
                    this.a.p.setVisibility(8);
                    this.a.g.setVisibility(0);
                }
                this.a.b = new C0057i(this.a.c, this.a.k);
                this.a.g.setAdapter((ListAdapter) this.a.b);
                this.a.b.notifyDataSetChanged();
                return;
            case 2:
                this.a.h.setVisibility(0);
                return;
            case 3:
                this.a.q = false;
                this.a.b.notifyDataSetChanged();
                return;
            case 4:
                this.a.q = false;
                if (this.a.b == null) {
                    this.a.b = new C0057i(this.a.c, this.a.k);
                    this.a.g.setAdapter((ListAdapter) this.a.b);
                }
                if (this.a.o == 2) {
                    this.a.p.setVisibility(8);
                    this.a.g.setVisibility(0);
                }
                this.a.b.notifyDataSetChanged();
                this.a.g.removeFooterView(this.a.h);
                return;
            default:
                return;
        }
    }
}
