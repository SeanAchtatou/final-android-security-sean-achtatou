package klkl.flkd.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;
import klkl.flkd.tools.o;

final class aB implements DialogInterface.OnKeyListener {
    private /* synthetic */ aA a;

    aB(aA aAVar) {
        this.a = aAVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4 || this.a.b == null) {
            return false;
        }
        this.a.b.setClickable(true);
        this.a.b.setBackgroundDrawable(o.a(this.a.a, "discuss/register01.png"));
        return false;
    }
}
