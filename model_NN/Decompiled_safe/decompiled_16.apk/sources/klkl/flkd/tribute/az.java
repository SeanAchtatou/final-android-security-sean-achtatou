package klkl.flkd.tribute;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import klkl.flkd.tools.r;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class az implements q {
    private Activity a;
    private /* synthetic */ YyLt b;

    public az(YyLt yyLt, Activity activity) {
        this.b = yyLt;
        this.a = activity;
        if (o.b((Context) activity)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("para", r.v);
                jSONObject.put("url", "registerAccount=" + r.z + "&type=" + yyLt.y);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            r.ai = false;
            new o(yyLt.getApplication(), this, jSONObject.toString(), "discuss").start();
            return;
        }
        Toast.makeText(this.a, "网络不给力哦……请您连接网络", 0).show();
    }

    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        o.a(r.M, "result = " + obj2);
        String str = null;
        try {
            str = new JSONArray(obj2).getJSONObject(0).getString("checkReplayMsgFlag");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        o.a(r.M, "isExist = " + str);
        if (str != null && str.equals("YES")) {
            YyLt.A(this.b);
        }
    }
}
