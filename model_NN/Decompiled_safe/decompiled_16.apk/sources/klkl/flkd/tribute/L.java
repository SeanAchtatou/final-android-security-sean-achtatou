package klkl.flkd.tribute;

import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import klkl.flkd.tools.o;

final class L extends Thread {
    private /* synthetic */ F a;

    L(F f) {
        this.a = f;
    }

    public final void run() {
        for (int i = 0; i < this.a.W.size(); i++) {
            ((ImageView) this.a.W.get(i)).setBackgroundDrawable(null);
        }
        for (int i2 = 0; i2 < this.a.V.size(); i2++) {
            o.a(((BitmapDrawable) this.a.V.get(i2)).getBitmap());
        }
        this.a.z.setBackgroundDrawable(null);
        this.a.A.setBackgroundDrawable(null);
        this.a.B.setBackgroundDrawable(null);
        this.a.C.setBackgroundDrawable(null);
        this.a.D.setBackgroundDrawable(null);
        this.a.E.setBackgroundDrawable(null);
        this.a.F.setBackgroundDrawable(null);
        for (int i3 = 0; i3 < this.a.aq.size(); i3++) {
            o.a(((BitmapDrawable) this.a.aq.get(i3)).getBitmap());
        }
    }
}
