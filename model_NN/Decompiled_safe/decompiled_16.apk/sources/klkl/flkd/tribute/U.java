package klkl.flkd.tribute;

import android.app.Activity;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import java.util.Map;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public final class U implements View.OnTouchListener {
    private Activity a;
    private Map b;
    private String c = null;

    public U(Activity activity) {
        this.a = activity;
    }

    public U(Activity activity, String str) {
        this.a = activity;
        this.c = str;
    }

    public U(Activity activity, String str, byte b2) {
        this.a = activity;
        this.c = str;
    }

    public U(Activity activity, Map map, String str) {
        this.a = activity;
        this.b = map;
        this.c = str;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case 5555:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() != 1) {
                        view.setBackgroundDrawable(o.a(this.a, "button/follownote01.png"));
                        break;
                    } else {
                        view.setBackgroundDrawable(o.a(this.a, "button/follownote01.png"));
                        if (this.c.equals("loadSquare") && !r.H) {
                            Toast.makeText(this.a, "请先登录，谢谢合作！", 0).show();
                            break;
                        } else {
                            Intent intent = new Intent(this.a, Ft.class);
                            intent.putExtra("loadFlag", this.c);
                            intent.putExtra("postOrReply", "reply");
                            intent.putExtra("repliedWho", this.b.get("name").toString());
                            intent.putExtra("repliedNotesId", this.b.get("noteID").toString());
                            this.a.startActivity(intent);
                            break;
                        }
                    }
                } else {
                    view.setClickable(false);
                    view.setBackgroundDrawable(o.a(this.a, "button/follownote02.png"));
                    break;
                }
            case 6666:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() != 1) {
                        view.setBackgroundDrawable(o.a(this.a, "discuss/follow_note_btn_back.png"));
                        break;
                    } else {
                        view.setBackgroundDrawable(o.a(this.a, "discuss/follow_note_btn_back.png"));
                        new C0059k(this.a, this.b, this.c);
                        break;
                    }
                } else {
                    view.setBackgroundDrawable(o.a(this.a, "discuss/follow_note_btn_clk_back.png"));
                    break;
                }
            case 7777:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() != 1) {
                        view.setBackgroundDrawable(o.a(this.a, "discuss/tribute_post.png"));
                        break;
                    } else {
                        view.setBackgroundDrawable(o.a(this.a, "discuss/tribute_post.png"));
                        if (this.c.equals("loadSquare") && !r.H) {
                            Toast.makeText(this.a, "登录之后才可发帖，谢谢合作！", 0).show();
                            break;
                        } else {
                            Intent intent2 = new Intent(this.a, Ft.class);
                            intent2.putExtra("loadFlag", this.c);
                            intent2.putExtra("postOrReply", "post");
                            intent2.putExtra("isScreenShotPost", "");
                            this.a.startActivity(intent2);
                            break;
                        }
                    }
                } else {
                    view.setBackgroundDrawable(o.a(this.a, "discuss/tribute_post_clk.png"));
                    view.setClickable(false);
                    break;
                }
                break;
            case 8888:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() != 1) {
                        view.setBackgroundDrawable(o.a(this.a, "discuss/tribute_login.png"));
                        break;
                    } else {
                        new C0042a(this.a, view);
                        break;
                    }
                } else {
                    view.setBackgroundDrawable(o.a(this.a, "discuss/tribute_login_clk.png"));
                    break;
                }
            case 11112:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() == 1) {
                        new aA(this.a, view);
                        break;
                    }
                } else {
                    view.setClickable(false);
                    break;
                }
                break;
            case 6666214:
                if (motionEvent.getAction() != 0) {
                    if (motionEvent.getAction() == 1) {
                        Intent intent3 = new Intent(this.a, Ft.class);
                        intent3.putExtra("loadFlag", this.c);
                        intent3.putExtra("postOrReply", "post");
                        intent3.putExtra("isScreenShotPost", "");
                        this.a.startActivity(intent3);
                    }
                    view.setBackgroundDrawable(o.a(this.a, "button/sofa01.png"));
                    break;
                } else {
                    view.setClickable(false);
                    view.setBackgroundDrawable(o.a(this.a, "button/sofa02.png"));
                    break;
                }
        }
        return true;
    }
}
