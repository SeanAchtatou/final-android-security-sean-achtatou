package klkl.flkd.tribute;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public final class Q extends LinearLayout {
    private Activity a;
    private int b;
    private int c;
    private TextView d;
    private int e;

    public Q(Activity activity, Drawable drawable) {
        super(activity);
        this.a = activity;
        if (r.ag >= 320) {
            this.b = 12;
            this.c = 50;
            this.e = 70;
            setMinimumWidth(100);
            o.a(this.a, "port/portback.png");
        } else if (r.ag >= 240 && r.ag < 320) {
            this.b = 12;
            this.c = 45;
            setMinimumWidth(90);
            this.e = 60;
            o.a(this.a, "port/portback2.png");
        } else if (240 > r.ag && r.ag >= 180) {
            this.b = 14;
            this.c = 30;
            setMinimumWidth(70);
            this.e = 35;
            o.a(this.a, "port/portback3.png");
        } else if (r.ag < 180 && r.ag > 120) {
            this.b = 14;
            this.c = 25;
            setMinimumWidth(50);
            this.e = 25;
            o.a(this.a, "port/portback4.png");
        } else if (r.ag <= 120) {
            this.b = 12;
            this.c = 20;
            setMinimumWidth(40);
            this.e = 20;
            o.a(this.a, "port/portback4.png");
        }
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-2, this.e));
        setClickable(true);
        ImageView imageView = new ImageView(this.a);
        imageView.setBackgroundDrawable(drawable);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.c, this.c);
        layoutParams.leftMargin = 5;
        layoutParams.gravity = 16;
        addView(imageView, layoutParams);
        this.d = new TextView(this.a);
        this.d.setTextSize((float) this.b);
        this.d.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        this.d.setGravity(5);
        layoutParams2.leftMargin = 5;
        addView(this.d, layoutParams2);
    }

    public final void a(String str) {
        this.d.setText(str);
    }
}
