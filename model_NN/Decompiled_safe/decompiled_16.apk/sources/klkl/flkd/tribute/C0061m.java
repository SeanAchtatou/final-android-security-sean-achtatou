package klkl.flkd.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: klkl.flkd.tribute.m  reason: case insensitive filesystem */
final class C0061m implements DialogInterface.OnKeyListener {
    private /* synthetic */ C0059k a;

    C0061m(C0059k kVar) {
        this.a = kVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        this.a.a();
        return true;
    }
}
