package klkl.flkd.tribute;

import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;

final class A extends Handler {
    private /* synthetic */ C0074z a;

    A(C0074z zVar) {
        this.a = zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: klkl.flkd.tribute.z.a(klkl.flkd.tribute.z, boolean):void
     arg types: [klkl.flkd.tribute.z, int]
     candidates:
      klkl.flkd.tribute.z.a(klkl.flkd.tribute.z, int):void
      klkl.flkd.tribute.z.a(klkl.flkd.tribute.z, java.util.List):void
      klkl.flkd.tribute.z.a(klkl.flkd.tribute.z, klkl.flkd.tribute.ad):void
      klkl.flkd.tribute.z.a(klkl.flkd.tribute.z, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: klkl.flkd.tribute.z.b(klkl.flkd.tribute.z, boolean):void
     arg types: [klkl.flkd.tribute.z, int]
     candidates:
      klkl.flkd.tribute.z.b(klkl.flkd.tribute.z, int):void
      klkl.flkd.tribute.z.b(klkl.flkd.tribute.z, boolean):void */
    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                if (this.a.p) {
                    this.a.p = false;
                    this.a.d.addFooterView(this.a.f);
                }
                this.a.o = false;
                if (this.a.l == 2) {
                    this.a.m.setVisibility(8);
                    this.a.d.setVisibility(0);
                }
                this.a.e = new C0046ad(this.a.a, this.a.g, this.a.q);
                this.a.d.setAdapter((ListAdapter) this.a.e);
                this.a.e.notifyDataSetChanged();
                if (this.a.n) {
                    this.a.n = false;
                    return;
                }
                return;
            case 2:
                this.a.m.setVisibility(0);
                this.a.d.setVisibility(8);
                return;
            case 3:
                if (this.a.p) {
                    this.a.p = false;
                    this.a.d.addFooterView(this.a.f);
                }
                this.a.o = false;
                this.a.e.notifyDataSetChanged();
                return;
            case 4:
                if (this.a.p) {
                    this.a.p = false;
                }
                this.a.o = false;
                if (this.a.l == 2) {
                    this.a.m.setVisibility(8);
                    this.a.d.setVisibility(0);
                }
                if (this.a.e == null) {
                    this.a.e = new C0046ad(this.a.a, this.a.g, this.a.q);
                    this.a.d.setAdapter((ListAdapter) this.a.e);
                }
                if (this.a.n) {
                    this.a.n = false;
                }
                this.a.e.notifyDataSetChanged();
                this.a.d.removeFooterView(this.a.f);
                this.a.p = true;
                return;
            case 5:
                if (this.a.n) {
                    this.a.n = false;
                }
                this.a.m.setVisibility(8);
                this.a.d.setVisibility(8);
                if (this.a.q.equals("noLogin")) {
                    this.a.s.setVisibility(0);
                    return;
                } else {
                    this.a.r.setVisibility(0);
                    return;
                }
            default:
                return;
        }
    }
}
