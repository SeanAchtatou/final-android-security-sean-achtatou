package klkl.flkd.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import klkl.flkd.tools.o;

/* renamed from: klkl.flkd.tribute.ac  reason: case insensitive filesystem */
final class C0045ac extends BroadcastReceiver {
    private /* synthetic */ X a;

    private C0045ac(X x) {
        this.a = x;
    }

    /* synthetic */ C0045ac(X x, byte b) {
        this(x);
    }

    public final void onReceive(Context context, Intent intent) {
        this.a.a.setImageDrawable(o.a(o.b(this.a.e, "icon.dat", "icon/" + intent.getStringExtra("icon")), 15));
    }
}
