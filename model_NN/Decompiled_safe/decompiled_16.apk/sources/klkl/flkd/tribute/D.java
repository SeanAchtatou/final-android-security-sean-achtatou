package klkl.flkd.tribute;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import klkl.flkd.tools.r;
import org.json.JSONException;
import org.json.JSONObject;

final class D implements q {
    private Activity a;
    private /* synthetic */ C0074z b;

    public D(C0074z zVar, Activity activity) {
        this.b = zVar;
        this.a = activity;
        if (o.b((Context) activity)) {
            try {
                if (zVar.q.equals("loadTribute")) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("para", r.p);
                    jSONObject.put("url", "registerAccount=" + r.z + "&pagenumber=" + zVar.l + "&pagesize=6");
                    new o(this.a, this, jSONObject.toString(), "discuss").start();
                } else if (zVar.q.equals("loadSquare")) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("para", r.r);
                    jSONObject2.put("url", "registerAccount=" + r.z + "&pagenumber=" + zVar.l + "&pagesize=6");
                    new o(this.a, this, jSONObject2.toString(), "discuss").start();
                } else if (zVar.q.equals("allnotes")) {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("para", r.q);
                    jSONObject3.put("url", "registerAccount=" + r.z + "&pagenumber=" + zVar.l + "&pagesize=6");
                    new o(this.a, this, jSONObject3.toString(), "discuss").start();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            r.ai = false;
            return;
        }
        Toast.makeText(this.a, "网络不给力哦……请您连接网络", 0).show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0093 A[LOOP:0: B:10:0x0058->B:17:0x0093, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r8, java.lang.Object r9) {
        /*
            r7 = this;
            r6 = 1
            r2 = 0
            java.lang.String r3 = r9.toString()
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ JSONException -> 0x003d }
            r0.<init>(r3)     // Catch:{ JSONException -> 0x003d }
            int r1 = r0.length()     // Catch:{ JSONException -> 0x003d }
            int r1 = r1 + -1
            org.json.JSONObject r0 = r0.getJSONObject(r1)     // Catch:{ JSONException -> 0x003d }
            java.lang.String r1 = "flag"
            int r0 = r0.getInt(r1)     // Catch:{ JSONException -> 0x003d }
            if (r0 != 0) goto L_0x0043
            klkl.flkd.tribute.z r1 = r7.b     // Catch:{ JSONException -> 0x00f6 }
            klkl.flkd.tools.f r1 = r1.m     // Catch:{ JSONException -> 0x00f6 }
            r4 = 8
            r1.setVisibility(r4)     // Catch:{ JSONException -> 0x00f6 }
            klkl.flkd.tribute.z r1 = r7.b     // Catch:{ JSONException -> 0x00f6 }
            android.widget.ListView r1 = r1.d     // Catch:{ JSONException -> 0x00f6 }
            r4 = 0
            r1.setVisibility(r4)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "加载失败！"
            r4 = 0
            android.widget.Toast r1 = android.widget.Toast.makeText(r8, r1, r4)     // Catch:{ JSONException -> 0x00f6 }
            r1.show()     // Catch:{ JSONException -> 0x00f6 }
        L_0x003c:
            return
        L_0x003d:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0040:
            r1.printStackTrace()
        L_0x0043:
            r1 = r0
            klkl.flkd.tribute.z r0 = r7.b
            klkl.flkd.tribute.z r4 = r7.b
            java.lang.String r4 = r4.q
            klkl.flkd.tribute.z r5 = r7.b
            android.app.Activity unused = r5.a
            java.util.List r3 = klkl.flkd.tools.o.b(r3, r4)
            r0.h = r3
        L_0x0058:
            klkl.flkd.tribute.z r0 = r7.b
            java.util.List r0 = r0.h
            int r0 = r0.size()
            if (r2 < r0) goto L_0x0093
            klkl.flkd.tribute.z r0 = r7.b
            klkl.flkd.tribute.z r2 = r7.b
            java.util.List r2 = r2.g
            int r2 = r2.size()
            r0.k = r2
            r0 = 2
            if (r1 == r0) goto L_0x00cb
            klkl.flkd.tribute.z r0 = r7.b
            int r0 = r0.l
            if (r0 != r6) goto L_0x00ac
            klkl.flkd.tribute.z r0 = r7.b
            int r1 = r0.l
            int r1 = r1 + 1
            r0.l = r1
            klkl.flkd.tribute.z r0 = r7.b
            android.os.Handler r0 = r0.t
            r0.sendEmptyMessage(r6)
            goto L_0x003c
        L_0x0093:
            klkl.flkd.tribute.z r0 = r7.b
            java.util.List r3 = r0.g
            klkl.flkd.tribute.z r0 = r7.b
            java.util.List r0 = r0.h
            java.lang.Object r0 = r0.get(r2)
            java.util.Map r0 = (java.util.Map) r0
            r3.add(r0)
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0058
        L_0x00ac:
            klkl.flkd.tribute.z r0 = r7.b
            int r0 = r0.l
            if (r0 == r6) goto L_0x003c
            klkl.flkd.tribute.z r0 = r7.b
            int r1 = r0.l
            int r1 = r1 + 1
            r0.l = r1
            klkl.flkd.tribute.z r0 = r7.b
            android.os.Handler r0 = r0.t
            r1 = 3
            r0.sendEmptyMessage(r1)
            goto L_0x003c
        L_0x00cb:
            klkl.flkd.tribute.z r0 = r7.b
            int r0 = r0.k
            if (r0 != 0) goto L_0x00df
            klkl.flkd.tribute.z r0 = r7.b
            android.os.Handler r0 = r0.t
            r1 = 5
            r0.sendEmptyMessage(r1)
            goto L_0x003c
        L_0x00df:
            klkl.flkd.tribute.z r0 = r7.b
            int r1 = r0.l
            int r1 = r1 + 1
            r0.l = r1
            klkl.flkd.tribute.z r0 = r7.b
            android.os.Handler r0 = r0.t
            r1 = 4
            r0.sendEmptyMessage(r1)
            goto L_0x003c
        L_0x00f6:
            r1 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: klkl.flkd.tribute.D.a(android.content.Context, java.lang.Object):void");
    }
}
