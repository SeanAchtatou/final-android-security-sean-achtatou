package klkl.flkd.tribute;

import android.view.MotionEvent;
import android.view.View;
import klkl.flkd.tools.o;

final class Y implements View.OnTouchListener {
    private /* synthetic */ X a;

    Y(X x) {
        this.a = x;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.a.d.setBackgroundDrawable(o.a(this.a.e, "discuss/tribute_home_clk.png"));
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            this.a.d.setBackgroundDrawable(o.a(this.a.e, "discuss/tribute_home.png"));
            new F(this.a.e, this.a.d);
            return false;
        }
    }
}
