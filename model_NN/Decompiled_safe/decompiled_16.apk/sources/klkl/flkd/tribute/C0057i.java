package klkl.flkd.tribute;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import java.util.Map;
import klkl.flkd.tools.o;

/* renamed from: klkl.flkd.tribute.i  reason: case insensitive filesystem */
public final class C0057i extends BaseAdapter {
    private List a = null;
    private Activity b;

    public C0057i(Activity activity, List list) {
        this.b = activity;
        this.a = list;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        C0058j jVar;
        if (view == null) {
            C0058j jVar2 = new C0058j(this, (byte) 0);
            C0056h hVar = new C0056h(this.b);
            jVar2.a = (TextView) hVar.getChildAt(0);
            jVar2.b = (TextView) hVar.getChildAt(1);
            hVar.setTag(jVar2);
            view = hVar;
            jVar = jVar2;
        } else {
            jVar = (C0058j) view.getTag();
        }
        jVar.a.setText("   " + ((Map) this.a.get(i)).get("nameInfo").toString() + ((Map) this.a.get(i)).get("userNum").toString() + ": ");
        jVar.b.setText("\t " + o.b(((Map) this.a.get(i)).get("noteInfo").toString()));
        return view;
    }
}
