package klkl.flkd.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public final class F extends Dialog {
    /* access modifiers changed from: private */
    public Button A;
    /* access modifiers changed from: private */
    public Button B;
    /* access modifiers changed from: private */
    public Button C;
    /* access modifiers changed from: private */
    public Button D;
    /* access modifiers changed from: private */
    public Button E;
    /* access modifiers changed from: private */
    public Button F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private EditText O;
    private EditText P;
    private EditText Q;
    /* access modifiers changed from: private */
    public int R = 0;
    /* access modifiers changed from: private */
    public String S = "1";
    /* access modifiers changed from: private */
    public String T = "";
    /* access modifiers changed from: private */
    public String U = "";
    /* access modifiers changed from: private */
    public List V = new ArrayList();
    /* access modifiers changed from: private */
    public List W = new ArrayList();
    private GridView X;
    /* access modifiers changed from: private */
    public int Y;
    /* access modifiers changed from: private */
    public BitmapDrawable Z;
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public BitmapDrawable aa;
    /* access modifiers changed from: private */
    public BitmapDrawable ab;
    /* access modifiers changed from: private */
    public BitmapDrawable ac;
    /* access modifiers changed from: private */
    public BitmapDrawable ad;
    private BitmapDrawable ae;
    /* access modifiers changed from: private */
    public BitmapDrawable af;
    private BitmapDrawable ag;
    /* access modifiers changed from: private */
    public BitmapDrawable ah;
    /* access modifiers changed from: private */
    public BitmapDrawable ai;
    /* access modifiers changed from: private */
    public BitmapDrawable aj;
    /* access modifiers changed from: private */
    public BitmapDrawable ak;
    /* access modifiers changed from: private */
    public BitmapDrawable al;
    /* access modifiers changed from: private */
    public BitmapDrawable am;
    /* access modifiers changed from: private */
    public BitmapDrawable an;
    /* access modifiers changed from: private */
    public BitmapDrawable ao;
    /* access modifiers changed from: private */
    public BitmapDrawable ap;
    /* access modifiers changed from: private */
    public List aq = new ArrayList();
    private DialogInterface.OnKeyListener ar = new G(this);
    private View.OnTouchListener as = new H(this);
    private final TextWatcher at = new I(this);
    private RadioGroup.OnCheckedChangeListener au = new J(this);
    private AdapterView.OnItemClickListener av = new K(this);
    /* access modifiers changed from: private */
    public F b;
    private LinearLayout c;
    private LinearLayout d;
    private LinearLayout e;
    private RelativeLayout f;
    /* access modifiers changed from: private */
    public View g;
    private int h = 561;
    private int i = 2434;
    private int j = 34;
    private int k = 35;
    private int l = 36;
    private int m = 69905;
    private int n = 291;
    /* access modifiers changed from: private */
    public int o = 292;
    /* access modifiers changed from: private */
    public int p = 293;
    private int q = 0;
    /* access modifiers changed from: private */
    public int r = 1;
    /* access modifiers changed from: private */
    public int s = 2;
    /* access modifiers changed from: private */
    public int t = 4;
    /* access modifiers changed from: private */
    public int u = 3;
    /* access modifiers changed from: private */
    public int v = 5;
    /* access modifiers changed from: private */
    public int w = 6;
    /* access modifiers changed from: private */
    public ImageView x;
    private TextView y;
    /* access modifiers changed from: private */
    public Button z;

    public F(Activity activity, View view) {
        super(activity, 16973829);
        setOwnerActivity(activity);
        this.a = activity;
        this.g = view;
        this.b = this;
        this.Z = o.a(this.a, "discuss/set_icon.png");
        this.aq.add(this.Z);
        this.aa = o.a(this.a, "discuss/cancle_set_icon.png");
        this.aq.add(this.aa);
        this.ab = o.a(this.a, "discuss/edit.png");
        this.aq.add(this.ab);
        this.ac = o.a(this.a, "discuss/save.png");
        this.aq.add(this.ac);
        this.ad = o.a(this.a, "discuss/close.png");
        this.aq.add(this.ad);
        this.ae = o.a(this.a, "discuss/layout3Back.png");
        this.aq.add(this.ae);
        this.af = o.a(this.a, "discuss/check.png");
        this.aq.add(this.Z);
        this.ag = o.a(this.a, "bitmap/followNotepic.png");
        this.aq.add(this.af);
        this.ah = o.a(this.a, "discuss/show_info_exit.png");
        this.aq.add(this.ag);
        this.ai = o.a(this.a, "discuss/tribute_home.png");
        this.aq.add(this.ai);
        this.aj = o.a(this.a, "discuss/set_icon_clk.png");
        this.aq.add(this.aj);
        this.ak = o.a(this.a, "discuss/edit_clk.png");
        this.aq.add(this.ak);
        this.al = o.a(this.a, "discuss/save_clk.png");
        this.aq.add(this.al);
        this.am = o.a(this.a, "discuss/show_info_exit_clk.png");
        this.aq.add(this.am);
        this.an = o.a(this.a, "discuss/close_clk.png");
        this.aq.add(this.an);
        this.ao = o.a(this.a, "discuss/check_clk.png");
        this.aq.add(this.ao);
        this.ap = o.a(this.a, "discuss/cancle_set_icon_clk.png");
        this.aq.add(this.ap);
        this.c = new LinearLayout(this.a);
        this.c.setBackgroundDrawable(o.a(this.a, "tabhost/back.png"));
        this.c.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.c.setOrientation(1);
        this.J = r.ae;
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        this.x = new ImageView(this.a);
        if (r.ag >= 320) {
            this.x.setLayoutParams(new ViewGroup.LayoutParams(128, 128));
            this.x.setPadding(8, 8, 8, 8);
            this.H = 120;
            this.G = 60;
            this.I = 40;
            this.Y = 150;
            this.K = (r.af * 3) / 4;
            this.L = (r.ae * 11) / 16;
            this.M = 22;
            this.N = 20;
        } else if (r.ag >= 240 && r.ag < 320) {
            this.x.setLayoutParams(new ViewGroup.LayoutParams(96, 96));
            this.x.setPadding(7, 7, 7, 7);
            this.H = 90;
            this.G = 45;
            this.I = 28;
            this.K = (r.af * 7) / 10;
            this.Y = 110;
            this.L = ((r.ae * 5) / 8) + 10;
            this.M = 20;
            this.N = 18;
        } else if (240 > r.ag && r.ag >= 180) {
            this.x.setLayoutParams(new ViewGroup.LayoutParams(64, 64));
            this.x.setPadding(6, 6, 6, 6);
            this.H = 60;
            this.G = 30;
            this.I = 30;
            this.K = 650;
            this.Y = 140;
            this.L = (r.ae * 11) / 16;
            this.M = 22;
            this.N = 20;
        } else if (r.ag < 180 && r.ag > 120) {
            this.x.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
            this.x.setPadding(3, 3, 3, 3);
            this.H = 56;
            this.G = 28;
            this.I = 16;
            this.K = (r.af * 7) / 10;
            this.Y = 65;
            this.L = (r.ae * 13) / 20;
            this.M = 22;
            this.N = 20;
        } else if (r.ag <= 120) {
            this.x.setLayoutParams(new ViewGroup.LayoutParams(45, 45));
            this.x.setPadding(4, 4, 2, 2);
            this.H = 40;
            this.G = 20;
            this.I = 12;
            this.K = (r.af * 7) / 11;
            this.Y = 50;
            this.L = (r.ae * 11) / 16;
            this.M = 22;
            this.N = 20;
        }
        this.x.setId(this.h);
        relativeLayout.addView(this.x);
        LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(new ViewGroup.LayoutParams(-1, -2));
        layoutParams.addRule(1, this.h);
        layoutParams.addRule(10);
        layoutParams.setMargins(25, 0, 0, 0);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(this.a);
        textView.setId(this.i);
        textView.setGravity(16);
        textView.setTextSize(18.0f);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView.setTextColor(-16777216);
        textView.setText("昵称：" + r.z);
        linearLayout.addView(textView);
        TextView textView2 = new TextView(this.a);
        textView2.setGravity(16);
        textView2.setTextSize(18.0f);
        textView2.setSingleLine(true);
        textView2.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView2.setTextColor(-16777216);
        textView2.setText("等级：" + r.B);
        linearLayout.addView(textView2);
        relativeLayout.addView(linearLayout);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.a);
        this.z = new Button(this.a);
        this.A = new Button(this.a);
        this.z.setVisibility(8);
        this.A.setVisibility(8);
        this.z.setId(0);
        this.A.setId(this.w);
        this.z.setOnTouchListener(this.as);
        this.A.setOnTouchListener(this.as);
        this.z.setBackgroundDrawable(this.Z);
        this.A.setBackgroundDrawable(this.aa);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.H, this.G);
        layoutParams2.addRule(9);
        layoutParams2.leftMargin = 5;
        relativeLayout2.addView(this.z, layoutParams2);
        relativeLayout2.addView(this.A, layoutParams2);
        this.B = new Button(this.a);
        this.C = new Button(this.a);
        this.C.setVisibility(8);
        this.D = new Button(this.a);
        this.D.setVisibility(8);
        this.B.setId(this.r);
        this.C.setId(this.s);
        this.D.setId(this.t);
        this.B.setOnTouchListener(this.as);
        this.C.setOnTouchListener(this.as);
        this.D.setOnTouchListener(this.as);
        this.B.setBackgroundDrawable(this.ab);
        this.C.setBackgroundDrawable(this.ac);
        this.D.setBackgroundDrawable(this.ad);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.H, this.G);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(this.H, this.G);
        layoutParams4.addRule(0, this.t);
        layoutParams4.rightMargin = 5;
        layoutParams3.addRule(11);
        layoutParams3.rightMargin = 5;
        relativeLayout2.addView(this.B, layoutParams3);
        relativeLayout2.addView(this.D, layoutParams3);
        relativeLayout2.addView(this.C, layoutParams4);
        this.f = new RelativeLayout(this.a);
        this.f.setGravity(1);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(this.J, this.K);
        layoutParams5.topMargin = relativeLayout.getHeight() + relativeLayout2.getHeight() + 10;
        this.f.setPadding(0, this.I, 0, 0);
        this.f.setBackgroundDrawable(this.ae);
        this.y = new TextView(this.a);
        this.y.setTextColor(-16777216);
        this.y.setId(this.j);
        this.y.setTextSize((float) this.N);
        this.y.setPadding(this.I, 0, 0, 0);
        this.f.addView(this.y, new ViewGroup.LayoutParams(-1, -2));
        TextView textView3 = new TextView(this.a);
        textView3.setId(this.k);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams6.addRule(3, this.j);
        textView3.setTextColor(-16777216);
        textView3.setTextSize((float) this.N);
        textView3.setText("积分：" + r.C);
        textView3.setPadding(this.I, 0, 0, 0);
        this.f.addView(textView3, layoutParams6);
        RelativeLayout relativeLayout3 = new RelativeLayout(this.a);
        relativeLayout3.setId(this.m);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams7.addRule(3, this.k);
        TextView textView4 = new TextView(this.a);
        textView4.setId(this.l);
        textView4.setTextColor(-16777216);
        textView4.setTextSize((float) this.N);
        textView4.setPadding(this.I, 0, 0, 0);
        textView4.setText("发帖数量：" + r.D);
        this.F = new Button(this.a);
        this.F.setId(this.v);
        this.F.setOnTouchListener(this.as);
        this.F.setBackgroundDrawable(this.af);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(this.H, this.G);
        layoutParams8.addRule(11);
        layoutParams8.rightMargin = 5;
        relativeLayout3.addView(textView4);
        relativeLayout3.addView(this.F, layoutParams8);
        this.f.addView(relativeLayout3, layoutParams7);
        TextView textView5 = new TextView(this.a);
        RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams9.addRule(3, this.m);
        textView5.setTextColor(-16777216);
        textView5.setTextSize((float) this.N);
        textView5.setPadding(this.I, 0, 0, 0);
        textView5.setText("跟帖数量：" + r.E);
        this.f.addView(textView5, layoutParams9);
        this.d = new LinearLayout(this.a);
        this.d.setVisibility(8);
        this.d.setOrientation(1);
        this.d.setGravity(1);
        this.d.setPadding(0, this.I, 0, 0);
        this.d.setBackgroundDrawable(this.ae);
        LinearLayout linearLayout2 = new LinearLayout(this.a);
        linearLayout2.setOrientation(0);
        linearLayout2.setId(this.n);
        linearLayout2.setPadding(this.I, 0, 0, 0);
        TextView textView6 = new TextView(this.a);
        textView6.setText("性别:");
        textView6.setTextColor(-16777216);
        textView6.setTextSize((float) this.M);
        linearLayout2.addView(textView6, new ViewGroup.LayoutParams(-2, -2));
        RadioGroup radioGroup = new RadioGroup(this.a);
        radioGroup.setOnCheckedChangeListener(this.au);
        radioGroup.setOrientation(0);
        RadioButton radioButton = new RadioButton(this.a);
        radioButton.setId(this.o);
        radioButton.setTextColor(-16777216);
        radioButton.setText("男");
        radioGroup.addView(radioButton, new ViewGroup.LayoutParams(-2, -1));
        RadioButton radioButton2 = new RadioButton(this.a);
        radioButton2.setId(this.p);
        radioButton2.setText("女");
        radioButton2.setTextColor(-16777216);
        if (r.F == 0) {
            radioButton.setChecked(true);
        } else {
            radioButton2.setChecked(true);
        }
        radioGroup.addView(radioButton2, new ViewGroup.LayoutParams(-2, -1));
        linearLayout2.addView(radioGroup, new ViewGroup.LayoutParams(-2, -1));
        LinearLayout linearLayout3 = new LinearLayout(this.a);
        linearLayout3.setOrientation(1);
        linearLayout3.setPadding(this.I, 0, 0, 0);
        linearLayout3.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        TextView textView7 = new TextView(this.a);
        textView7.setPadding(0, this.I / 2, 0, 0);
        textView7.setText("密码修改:(可以不做更改)");
        textView7.setTextColor(-16777216);
        textView7.setTextSize((float) this.M);
        linearLayout3.addView(textView7);
        PasswordTransformationMethod instance = PasswordTransformationMethod.getInstance();
        ViewGroup.LayoutParams layoutParams10 = new ViewGroup.LayoutParams(-1, -2);
        LinearLayout linearLayout4 = new LinearLayout(this.c.getContext());
        linearLayout4.setLayoutParams(layoutParams10);
        linearLayout4.setOrientation(0);
        TextView textView8 = new TextView(linearLayout4.getContext());
        textView8.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView8.setText("    原密码:");
        textView8.setTextColor(-16777216);
        textView8.setTextSize((float) this.N);
        this.O = new EditText(linearLayout4.getContext());
        this.O.setBackgroundDrawable(this.ag);
        this.O.setHint("请输入原密码");
        this.O.setTextColor(-16777216);
        this.O.requestFocus();
        this.O.setId(1234101);
        this.O.setOnFocusChangeListener(new O(this, (byte) 0));
        this.O.setSingleLine(true);
        this.O.setTransformationMethod(instance);
        this.O.setLayoutParams(new ViewGroup.LayoutParams(this.L, -2));
        linearLayout4.addView(textView8);
        linearLayout4.addView(this.O);
        linearLayout3.addView(linearLayout4);
        LinearLayout linearLayout5 = new LinearLayout(this.c.getContext());
        linearLayout5.setLayoutParams(layoutParams10);
        linearLayout5.setOrientation(0);
        TextView textView9 = new TextView(this.a);
        textView9.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView9.setText("    新密码:");
        textView9.setTextColor(-16777216);
        textView9.setTextSize((float) this.N);
        this.P = new EditText(this.a);
        this.P.setBackgroundDrawable(this.ag);
        this.P.setSingleLine(true);
        this.P.setHint("请输入6-15个字符密码");
        this.P.setTextColor(-16777216);
        this.P.setTransformationMethod(instance);
        this.P.setId(13245101);
        this.P.addTextChangedListener(this.at);
        this.P.setOnFocusChangeListener(new O(this, (byte) 0));
        this.P.setLayoutParams(new ViewGroup.LayoutParams(this.L, -2));
        linearLayout5.addView(textView9);
        linearLayout5.addView(this.P);
        linearLayout3.addView(linearLayout5);
        LinearLayout linearLayout6 = new LinearLayout(this.a);
        linearLayout6.setLayoutParams(layoutParams10);
        linearLayout6.setOrientation(0);
        TextView textView10 = new TextView(linearLayout6.getContext());
        textView10.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView10.setText("确认密码:");
        textView10.setTextColor(-16777216);
        textView10.setTextSize((float) this.N);
        this.Q = new EditText(this.a);
        this.Q.setBackgroundDrawable(this.ag);
        this.Q.setSingleLine(true);
        this.Q.setHint("请确认密码");
        this.Q.setTextColor(-16777216);
        this.Q.setTransformationMethod(instance);
        this.Q.addTextChangedListener(this.at);
        this.Q.setId(12346101);
        this.Q.setLayoutParams(new ViewGroup.LayoutParams(this.L, -2));
        linearLayout6.addView(textView10);
        linearLayout6.addView(this.Q);
        linearLayout3.addView(linearLayout6);
        this.d.addView(linearLayout2, new ViewGroup.LayoutParams(-1, -2));
        this.d.addView(linearLayout3, new ViewGroup.LayoutParams(-1, -2));
        this.e = new LinearLayout(this.a);
        this.e.setVisibility(8);
        this.e.setOrientation(1);
        this.e.setGravity(1);
        this.e.setPadding(0, this.I, 0, 0);
        this.e.setBackgroundDrawable(this.ae);
        this.X = new GridView(this.a);
        this.X.setGravity(1);
        this.X.setNumColumns(4);
        this.X.setVerticalSpacing(5);
        this.X.setHorizontalSpacing(3);
        this.e.addView(this.X);
        RelativeLayout relativeLayout4 = new RelativeLayout(this.a);
        this.E = new Button(this.a);
        this.E.setId(this.u);
        this.E.setOnTouchListener(this.as);
        this.E.setBackgroundDrawable(this.ah);
        RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(this.H, this.G);
        layoutParams11.addRule(12);
        layoutParams11.addRule(11);
        layoutParams11.bottomMargin = 10;
        layoutParams11.rightMargin = 10;
        this.E.setLayoutParams(layoutParams11);
        relativeLayout4.addView(this.E);
        this.c.addView(relativeLayout, new LinearLayout.LayoutParams(-1, -2));
        this.c.addView(relativeLayout2, new LinearLayout.LayoutParams(-1, -2));
        this.c.addView(this.f, layoutParams5);
        this.c.addView(this.d, layoutParams5);
        this.c.addView(this.e, layoutParams5);
        this.c.addView(relativeLayout4, new LinearLayout.LayoutParams(-1, -2));
        b();
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(this.ar);
        show();
        setContentView(this.c);
        this.S = r.G;
        this.R = r.F;
    }

    static /* synthetic */ void F(F f2) {
        f2.d.setVisibility(0);
        f2.e.setVisibility(8);
        f2.z.setVisibility(0);
        f2.A.setVisibility(8);
    }

    static /* synthetic */ void Q(F f2) {
        f2.b();
        f2.a();
    }

    /* access modifiers changed from: private */
    public void a() {
        this.B.setVisibility(0);
        this.f.setVisibility(0);
        this.d.setVisibility(8);
        this.e.setVisibility(8);
        this.z.setVisibility(8);
        this.A.setVisibility(8);
        this.C.setVisibility(8);
        this.D.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.x.setImageDrawable(o.a(o.b(this.a, "icon.dat", "icon/icon" + r.G + ".jpg"), 15));
        if (r.F == 0) {
            this.y.setText("性别：男");
        } else {
            this.y.setText("性别：女");
        }
    }

    static /* synthetic */ int d(F f2) {
        return 0;
    }

    static /* synthetic */ void s(F f2) {
        f2.d.setVisibility(8);
        f2.e.setVisibility(0);
        f2.z.setVisibility(8);
        f2.A.setVisibility(0);
        f2.X.setAdapter((ListAdapter) new M(f2));
        f2.X.setOnItemClickListener(f2.av);
    }

    static /* synthetic */ void u(F f2) {
        f2.B.setVisibility(8);
        f2.f.setVisibility(8);
        f2.d.setVisibility(0);
        f2.z.setVisibility(0);
        f2.C.setVisibility(0);
        f2.D.setVisibility(0);
    }

    static /* synthetic */ void w(F f2) {
        if (f2.U.equals("")) {
            new P(f2);
        } else if (f2.U.length() < 6) {
            Toast.makeText(f2.a, "密码过短，请修改！", 0).show();
        } else if (f2.U.length() > 18) {
            Toast.makeText(f2.a, "密码过长，请修改！", 0).show();
        } else if (f2.U.equals(f2.Q.getText().toString())) {
            new P(f2);
        } else {
            Toast.makeText(f2.a, "前后两次密码不同！", 1).show();
        }
    }
}
