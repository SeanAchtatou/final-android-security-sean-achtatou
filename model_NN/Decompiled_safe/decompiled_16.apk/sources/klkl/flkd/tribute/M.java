package klkl.flkd.tribute;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import klkl.flkd.tools.o;

final class M extends BaseAdapter {
    private /* synthetic */ F a;

    public M(F f) {
        this.a = f;
        if (f.V.size() == 0) {
            a();
        }
    }

    private void a() {
        for (int i = 1; i < 21; i++) {
            this.a.V.add(o.a(o.b(this.a.a, "icon.dat", "icon/icon" + i + ".jpg"), 15));
        }
    }

    public final int getCount() {
        return this.a.V.size();
    }

    public final Object getItem(int i) {
        return this.a.V.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        N n;
        ImageView imageView;
        if (view == null) {
            N n2 = new N(this);
            imageView = new ImageView(this.a.a);
            this.a.W.add(imageView);
            n2.a = imageView;
            n2.a.setLayoutParams(new AbsListView.LayoutParams(this.a.Y, this.a.Y));
            imageView.setTag(n2);
            n = n2;
        } else {
            n = (N) view.getTag();
            imageView = view;
        }
        n.a.setBackgroundDrawable((Drawable) this.a.V.get(i));
        return imageView;
    }
}
