package klkl.flkd.tribute;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

final class O implements View.OnFocusChangeListener {
    private /* synthetic */ F a;

    private O(F f) {
        this.a = f;
    }

    /* synthetic */ O(F f, byte b) {
        this(f);
    }

    public final void onFocusChange(View view, boolean z) {
        switch (view.getId()) {
            case 1234101:
                if (!z) {
                    this.a.T = ((EditText) view).getText().toString();
                    return;
                }
                return;
            case 13245101:
                if (!z) {
                    this.a.U = ((EditText) view).getText().toString();
                    if (this.a.U == null) {
                        return;
                    }
                    if (this.a.U.length() < 6) {
                        Toast.makeText(this.a.a, "密码过短，请修改！", 0).show();
                        return;
                    } else if (this.a.U.length() > 18) {
                        Toast.makeText(this.a.a, "密码过长，请修改！", 0).show();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
