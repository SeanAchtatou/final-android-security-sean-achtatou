package klkl.flkd.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;

final class G implements DialogInterface.OnKeyListener {
    private /* synthetic */ F a;

    G(F f) {
        this.a = f;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        this.a.g.setBackgroundDrawable(this.a.ai);
        dialogInterface.cancel();
        new L(this.a).start();
        return false;
    }
}
