package klkl.flkd.tribute;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: klkl.flkd.tribute.p  reason: case insensitive filesystem */
final class C0064p extends Handler {
    private /* synthetic */ C0059k a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0064p(C0059k kVar, Looper looper) {
        super(looper);
        this.a = kVar;
    }

    public final void handleMessage(Message message) {
        synchronized (this) {
            switch (message.what) {
                case 0:
                    if (this.a.o == 1) {
                        this.a.p.setVisibility(0);
                        this.a.g.setVisibility(8);
                    }
                    new C0063o(this.a, this.a.c);
                    break;
            }
        }
    }
}
