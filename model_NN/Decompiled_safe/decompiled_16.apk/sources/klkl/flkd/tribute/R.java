package klkl.flkd.tribute;

import android.app.Activity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public final class R extends RelativeLayout {
    private static RelativeLayout.LayoutParams j;
    /* access modifiers changed from: private */
    public Activity a = null;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private ImageView f;
    private Button g;
    private T h;
    private Q i;

    public R(Activity activity) {
        super(activity);
        this.a = activity;
        if (r.ag >= 180) {
            setPadding(0, 20, 0, 20);
        } else {
            setPadding(0, 12, 0, 12);
        }
        this.b = new TextView(this.a);
        this.b.setTextColor(-7829368);
        this.b.setId(1111);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        this.b.setLayoutParams(layoutParams);
        this.b.setTextSize(20.0f);
        addView(this.b);
        this.e = new TextView(this.a);
        this.e.setId(4642);
        this.e.setTextColor(-7829368);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(1, 1111);
        this.e.setTextSize(15.0f);
        this.e.setLayoutParams(layoutParams2);
        addView(this.e);
        this.d = new TextView(this.a);
        this.d.setTextColor(-7829368);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        layoutParams3.setMargins(0, 0, 15, 0);
        this.d.setLayoutParams(layoutParams3);
        this.d.setTextSize(13.0f);
        addView(this.d);
        this.c = new TextView(this.a);
        this.c.setTextColor(-16777216);
        this.c.setTextSize(17.0f);
        this.c.setTextScaleX(1.0f);
        this.c.setId(2222);
        if (r.ag >= 180) {
            this.c.setPadding(20, 10, 15, 10);
        } else {
            this.c.setPadding(10, 5, 8, 5);
        }
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(3, 1111);
        this.c.setLayoutParams(layoutParams4);
        addView(this.c);
        j = new RelativeLayout.LayoutParams(-2, -2);
        this.f = new ImageView(this.a);
        this.f.setVisibility(8);
        this.f.setId(69632);
        if (r.ag >= 180) {
            j.leftMargin = 20;
        } else {
            j.leftMargin = 10;
        }
        j.addRule(3, 2222);
        j.addRule(9);
        this.f.setLayoutParams(j);
        addView(this.f);
        this.g = new Button(this.a);
        this.g.setBackgroundDrawable(o.a(this.a, "discuss/follow_note_btn_back.png"));
        RelativeLayout.LayoutParams layoutParams5 = null;
        if (r.ag >= 320) {
            layoutParams5 = new RelativeLayout.LayoutParams(-2, r.af / 20);
            layoutParams5.bottomMargin = 10;
            layoutParams5.rightMargin = 10;
            this.g.setTextSize(10.0f);
        } else if (r.ag >= 240 && r.ag < 320) {
            layoutParams5 = new RelativeLayout.LayoutParams(-2, (r.af / 17) + 2);
            layoutParams5.bottomMargin = 5;
            layoutParams5.rightMargin = 8;
            this.g.setTextSize(11.0f);
        } else if (240 > r.ag && r.ag >= 180) {
            layoutParams5 = new RelativeLayout.LayoutParams(r.ae / 5, r.af / 15);
            layoutParams5.bottomMargin = 5;
            layoutParams5.rightMargin = 7;
            this.g.setTextSize(13.0f);
        } else if (r.ag < 180 && r.ag > 120) {
            layoutParams5 = new RelativeLayout.LayoutParams(r.ae / 5, r.af / 15);
            layoutParams5.bottomMargin = 5;
            layoutParams5.rightMargin = 5;
            this.g.setTextSize(13.0f);
        } else if (r.ag <= 120) {
            layoutParams5 = new RelativeLayout.LayoutParams(r.ae / 5, r.af / 15);
            layoutParams5.bottomMargin = 5;
            layoutParams5.rightMargin = 5;
            this.g.setTextSize(12.0f);
        }
        this.g.setTextColor(-16777216);
        layoutParams5.addRule(3, 69632);
        layoutParams5.addRule(11);
        layoutParams5.addRule(12);
        this.g.setLayoutParams(layoutParams5);
        this.g.setId(6666);
        this.g.setFocusable(false);
        this.g.setOnTouchListener(new S(this));
        addView(this.g);
        this.h = new T(this.a, o.a(this.a, "discuss/support.png"));
        this.h.setId(17895714);
        this.h.setVisibility(8);
        this.i = new Q(this.a, o.a(this.a, "discuss/nonsupport.png"));
        this.i.setId(17895715);
        this.i.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.addRule(9);
        layoutParams6.addRule(12);
        layoutParams6.leftMargin = 8;
        layoutParams7.addRule(1, 17895714);
        layoutParams7.addRule(12);
        layoutParams7.leftMargin = 8;
        this.h.setLayoutParams(layoutParams6);
        this.i.setLayoutParams(layoutParams7);
        addView(this.h);
        addView(this.i);
    }
}
