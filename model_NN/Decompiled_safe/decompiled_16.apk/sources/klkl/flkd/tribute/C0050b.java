package klkl.flkd.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;
import klkl.flkd.tools.o;

/* renamed from: klkl.flkd.tribute.b  reason: case insensitive filesystem */
final class C0050b implements DialogInterface.OnKeyListener {
    private /* synthetic */ C0042a a;

    C0050b(C0042a aVar) {
        this.a = aVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        this.a.c.setClickable(true);
        this.a.c.setBackgroundDrawable(o.a(this.a.b, "discuss/tribute_login.png"));
        return false;
    }
}
