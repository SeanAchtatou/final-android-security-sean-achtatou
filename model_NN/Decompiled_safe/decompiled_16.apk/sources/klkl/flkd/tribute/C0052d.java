package klkl.flkd.tribute;

import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;

/* renamed from: klkl.flkd.tribute.d  reason: case insensitive filesystem */
final class C0052d implements View.OnTouchListener {
    private /* synthetic */ C0042a a;

    C0052d(C0042a aVar) {
        this.a = aVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case 11110:
                if (motionEvent.getAction() == 0) {
                    this.a.h.setBackgroundDrawable(new BitmapDrawable(this.a.l));
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    this.a.h.setBackgroundDrawable(new BitmapDrawable(this.a.k));
                    return false;
                }
            case 11111:
                if (motionEvent.getAction() == 0) {
                    this.a.i.setBackgroundDrawable(new BitmapDrawable(this.a.l));
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    this.a.i.setBackgroundDrawable(new BitmapDrawable(this.a.k));
                    return false;
                }
            default:
                return false;
        }
    }
}
