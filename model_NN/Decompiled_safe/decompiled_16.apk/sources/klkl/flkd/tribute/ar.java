package klkl.flkd.tribute;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import klkl.flkd.tools.r;

final class ar extends Handler {
    private /* synthetic */ YyLt a;

    ar(YyLt yyLt) {
        this.a = yyLt;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 6:
                if (r.H) {
                    new az(this.a, this.a);
                    return;
                }
                return;
            case MotionEventCompat.ACTION_HOVER_MOVE:
                if (this.a.z % 2 == 0) {
                    this.a.f.c.setBackgroundDrawable(this.a.I);
                } else {
                    this.a.f.c.setBackgroundDrawable(this.a.D);
                }
                YyLt yyLt = this.a;
                yyLt.z = yyLt.z + 1;
                return;
            default:
                return;
        }
    }
}
