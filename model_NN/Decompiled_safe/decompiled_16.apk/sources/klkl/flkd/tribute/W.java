package klkl.flkd.tribute;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import klkl.flkd.tools.o;
import klkl.flkd.xxka.XxKZs;

final class W implements View.OnTouchListener {
    private /* synthetic */ V a;

    W(V v) {
        this.a = v;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.b.setBackgroundDrawable(o.a(this.a.d, "discuss/tribute_more_clk.png"));
                return false;
            case 1:
                this.a.b.setBackgroundDrawable(o.a(this.a.d, "discuss/tribute_more.png"));
                Intent intent = new Intent(this.a.d, XxKZs.class);
                intent.addFlags(67108864);
                this.a.d.startActivity(intent);
                return false;
            default:
                return false;
        }
    }
}
