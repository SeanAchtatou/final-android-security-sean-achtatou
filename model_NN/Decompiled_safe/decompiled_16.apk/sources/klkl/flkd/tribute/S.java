package klkl.flkd.tribute;

import android.view.MotionEvent;
import android.view.View;
import klkl.flkd.tools.o;

final class S implements View.OnTouchListener {
    private /* synthetic */ R a;

    S(R r) {
        this.a = r;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(o.a(this.a.a, "discuss/follow_note_btn_clk_back.png"));
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            view.setBackgroundDrawable(o.a(this.a.a, "discuss/follow_note_btn_back.png"));
            return false;
        }
    }
}
