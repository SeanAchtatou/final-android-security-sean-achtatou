package klkl.flkd.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;
import klkl.flkd.tools.o;

final class B implements DialogInterface.OnKeyListener {
    private /* synthetic */ C0074z a;

    B(C0074z zVar) {
        this.a = zVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        if (!this.a.q.equals("allnotes")) {
            this.a.c.setBackgroundDrawable(o.a(this.a.a, "discuss/tribute_message.png"));
        }
        dialogInterface.cancel();
        return true;
    }
}
