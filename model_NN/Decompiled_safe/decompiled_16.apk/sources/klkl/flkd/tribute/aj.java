package klkl.flkd.tribute;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import java.util.Map;
import klkl.flkd.tools.r;

final class aj implements View.OnTouchListener {
    private String a;
    private /* synthetic */ C0046ad b;

    public aj(C0046ad adVar, String str) {
        this.b = adVar;
        this.a = str;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        int parseInt = Integer.parseInt(view.getTag().toString());
        if (!r.H) {
            Toast.makeText(this.b.a, "请先登录，谢谢合作！", 0).show();
            return true;
        } else if (((Map) this.b.b.get(parseInt)).get("ifport").toString().equals("1")) {
            Toast.makeText(this.b.a, "你已经对此帖子评价过，请勿重复评价，谢谢合作！", 0).show();
            return true;
        } else {
            if (motionEvent.getAction() == 0) {
                view.setBackgroundColor(-256);
            } else if (motionEvent.getAction() == 1) {
                view.setBackgroundColor(-1);
                new ai(this.b, this.a, parseInt);
            }
            return false;
        }
    }
}
