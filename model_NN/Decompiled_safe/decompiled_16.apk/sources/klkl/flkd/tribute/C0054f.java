package klkl.flkd.tribute;

import android.content.Context;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import klkl.flkd.tools.r;
import org.json.JSONObject;

/* renamed from: klkl.flkd.tribute.f  reason: case insensitive filesystem */
public final class C0054f implements q {
    private int a;

    public C0054f(Context context, int i) {
        this.a = i;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.h);
            jSONObject.put("url", "registerAccount=" + r.z + "&registerPassword=" + r.A);
        } catch (Exception e) {
            e.printStackTrace();
        }
        r.ai = false;
        new o(context, this, jSONObject.toString(), "discuss").start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r6, java.lang.Object r7) {
        /*
            r5 = this;
            r1 = 0
            r3 = 1
            java.lang.String r0 = r7.toString()
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0060 }
            r2.<init>(r0)     // Catch:{ JSONException -> 0x0060 }
            r0 = 0
            org.json.JSONObject r2 = r2.getJSONObject(r0)     // Catch:{ JSONException -> 0x0060 }
            java.lang.String r0 = "loginFlag"
            int r0 = r2.getInt(r0)     // Catch:{ JSONException -> 0x0060 }
            java.lang.String r1 = "userRank"
            java.lang.String r1 = r2.getString(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.B = r1     // Catch:{ JSONException -> 0x009d }
            java.lang.String r1 = "userScore"
            java.lang.String r1 = r2.getString(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.C = r1     // Catch:{ JSONException -> 0x009d }
            java.lang.String r1 = "userPostCounts"
            int r1 = r2.getInt(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.D = r1     // Catch:{ JSONException -> 0x009d }
            java.lang.String r1 = "userFollowCounts"
            int r1 = r2.getInt(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.E = r1     // Catch:{ JSONException -> 0x009d }
            java.lang.String r1 = "userSex"
            int r1 = r2.getInt(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.F = r1     // Catch:{ JSONException -> 0x009d }
            java.lang.String r1 = "userIconId"
            java.lang.String r1 = r2.getString(r1)     // Catch:{ JSONException -> 0x009d }
            klkl.flkd.tools.r.G = r1     // Catch:{ JSONException -> 0x009d }
        L_0x0046:
            int r1 = r5.a
            if (r1 != 0) goto L_0x0081
            if (r0 != r3) goto L_0x0068
            java.lang.String r0 = "登录成功!"
            android.widget.Toast r0 = android.widget.Toast.makeText(r6, r0, r3)
            r0.show()
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "loginsucess"
            r0.<init>(r1)
            r6.sendBroadcast(r0)
        L_0x005f:
            return
        L_0x0060:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0064:
            r1.printStackTrace()
            goto L_0x0046
        L_0x0068:
            if (r0 != 0) goto L_0x0074
            java.lang.String r0 = "用户名不存在!"
            android.widget.Toast r0 = android.widget.Toast.makeText(r6, r0, r3)
            r0.show()
            goto L_0x005f
        L_0x0074:
            r1 = 2
            if (r0 != r1) goto L_0x005f
            java.lang.String r0 = "密码错误，请重新输入！"
            android.widget.Toast r0 = android.widget.Toast.makeText(r6, r0, r3)
            r0.show()
            goto L_0x005f
        L_0x0081:
            int r1 = r5.a
            if (r1 != r3) goto L_0x005f
            if (r0 != r3) goto L_0x0092
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "loginsucess"
            r0.<init>(r1)
            r6.sendBroadcast(r0)
            goto L_0x005f
        L_0x0092:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "loginfalse"
            r0.<init>(r1)
            r6.sendBroadcast(r0)
            goto L_0x005f
        L_0x009d:
            r1 = move-exception
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: klkl.flkd.tribute.C0054f.a(android.content.Context, java.lang.Object):void");
    }
}
