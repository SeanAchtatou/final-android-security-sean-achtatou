package klkl.flkd.tribute;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

final class I implements TextWatcher {
    private /* synthetic */ F a;

    I(F f) {
        this.a = f;
    }

    public final void afterTextChanged(Editable editable) {
        if (editable.length() > 0) {
            int length = editable.length() - 1;
            char charAt = editable.charAt(length);
            if (this.a.T.equals("")) {
                Toast.makeText(this.a.a, "原密码不能为空，请输入！", 0).show();
                editable.delete(length, length + 1);
            } else if (charAt >= 'a' && charAt <= 'z') {
            } else {
                if ((charAt < 'A' || charAt > 'Z') && charAt != '_' && charAt != '@') {
                    if (charAt < '0' || charAt > '9') {
                        Toast.makeText(this.a.a, "只能输入字母、数字以及'_'和'@'等字符！", 0).show();
                        editable.delete(length, length + 1);
                    }
                }
            }
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
