package klkl.flkd.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import klkl.flkd.tools.b;
import klkl.flkd.tools.c;
import klkl.flkd.tools.f;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

/* renamed from: klkl.flkd.tribute.z  reason: case insensitive filesystem */
public final class C0074z extends Dialog implements AbsListView.OnScrollListener {
    /* access modifiers changed from: private */
    public Activity a;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public View c;
    /* access modifiers changed from: private */
    public ListView d = null;
    /* access modifiers changed from: private */
    public C0046ad e;
    /* access modifiers changed from: private */
    public b f;
    /* access modifiers changed from: private */
    public List g = new ArrayList();
    /* access modifiers changed from: private */
    public List h = new ArrayList();
    private E i;
    private int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l = 1;
    /* access modifiers changed from: private */
    public f m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public TextView r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public Handler t = new A(this);
    private DialogInterface.OnKeyListener u = new B(this);

    public C0074z(Activity activity, View view, String str) {
        super(activity, 16973829);
        setOwnerActivity(activity);
        this.a = activity;
        this.c = view;
        this.q = str;
        HandlerThread handlerThread = new HandlerThread("discuss");
        handlerThread.start();
        this.i = new E(this, handlerThread.getLooper());
        this.b = new LinearLayout(this.a);
        this.b.setBackgroundColor(-1);
        this.b.setBackgroundDrawable(o.a(this.a, "tabhost/back.png"));
        this.b.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.b.setOrientation(1);
        this.f = new b(this.a);
        this.m = new f(this.a);
        this.m.setGravity(17);
        c cVar = new c(this.a);
        cVar.a("我的消息");
        this.d = new ListView(this.a);
        this.d.setDivider(o.a(this.a, "bitmap/dividerH.png"));
        this.d.setOnItemClickListener(new C(this));
        this.d.setBackgroundDrawable(o.a(this.a, "tabhost/back.png"));
        this.d.setCacheColorHint(-1);
        this.d.addFooterView(this.f);
        this.d.setOnScrollListener(this);
        this.r = new TextView(this.a);
        this.r.setText(r.at);
        this.r.setTextColor(-16777216);
        this.r.setGravity(17);
        this.r.setVisibility(8);
        this.s = new TextView(this.a);
        this.s.setTextColor(-16777216);
        this.s.setText(r.au);
        this.s.setGravity(17);
        this.s.setVisibility(8);
        this.b.addView(cVar);
        this.b.addView(this.d, new LinearLayout.LayoutParams(-1, -1));
        this.b.addView(this.m, new LinearLayout.LayoutParams(-1, -1));
        this.b.addView(this.r, new ViewGroup.LayoutParams(-1, -1));
        this.b.addView(this.s, new ViewGroup.LayoutParams(-1, -1));
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(this.u);
        show();
        setContentView(this.b);
        if (this.q.equals("noLogin")) {
            this.m.setVisibility(8);
            this.d.setVisibility(8);
            this.s.setVisibility(0);
            return;
        }
        this.i.sendEmptyMessage(0);
    }

    public final void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.j = (i2 + i3) - 1;
    }

    public final void onScrollStateChanged(AbsListView absListView, int i2) {
        if (this.j == this.k && i2 == 0 && !this.o) {
            this.o = true;
            this.i.sendEmptyMessage(0);
        }
    }
}
