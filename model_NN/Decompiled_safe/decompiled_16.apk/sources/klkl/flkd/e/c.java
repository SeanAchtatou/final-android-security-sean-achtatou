package klkl.flkd.e;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import klkl.flkd.tools.o;

public final class c {
    private Context a;
    private String b;
    private int c = 0;
    private int d = 0;
    private b e;
    private File f;
    private String g;

    public c(Context context, String str, File file, String str2) {
        this.a = context;
        this.b = str2;
        try {
            if (!file.exists()) {
                file.mkdirs();
            }
            this.g = str;
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (httpURLConnection.getResponseCode() == 200) {
                this.d = httpURLConnection.getContentLength();
                httpURLConnection.disconnect();
                if (this.d <= 0) {
                    throw new RuntimeException("Unkown file size ");
                }
                String substring = this.g.substring(this.g.lastIndexOf(47) + 1);
                substring = (substring == null || "".equals(substring.trim())) ? UUID.randomUUID() + ".apk" : substring;
                SharedPreferences.Editor edit = this.a.getSharedPreferences("data", 0).edit();
                edit.putString(String.valueOf(this.b) + "_filename", Environment.getExternalStorageDirectory() + "/panda/" + substring);
                edit.commit();
                this.f = new File(file, substring);
                return;
            }
            throw new RuntimeException("server no response ");
        } catch (Exception e2) {
            throw new RuntimeException("don't connection this url");
        }
    }

    public final int a() {
        return this.d;
    }

    public final int a(a aVar) {
        try {
            URL url = new URL(this.g);
            if (this.c < this.d) {
                this.e = new b(this, url, this.f);
                if (this.a != null && o.a(this.a)) {
                    this.e.a = 102400;
                }
                this.e.setPriority(7);
                this.e.start();
            }
            boolean z = true;
            while (z) {
                Thread.sleep(700);
                z = false;
                if (this.e != null && !this.e.a()) {
                    z = true;
                }
                if (this.e != null && this.e.b() == -1) {
                    throw new Exception("file download fail");
                } else if (aVar != null) {
                    aVar.a(this.c);
                }
            }
            return this.c;
        } catch (Exception e2) {
            throw new Exception("file download fail");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.c += i;
    }
}
