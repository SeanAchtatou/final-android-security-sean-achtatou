package klkl.flkd.lbiao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageButton;
import java.util.Map;

final class n extends BroadcastReceiver {
    private /* synthetic */ k a;

    private n(k kVar) {
        this.a = kVar;
    }

    /* synthetic */ n(k kVar, byte b) {
        this(kVar);
    }

    public final void onReceive(Context context, Intent intent) {
        int i = 0;
        Intent intent2 = new Intent();
        if (intent.getAction().equals("downloadinstallfromscreen")) {
            int i2 = 0;
            while (true) {
                if (i2 >= this.a.l.size()) {
                    break;
                } else if (((Map) this.a.l.get(i2)).get("packagename").equals(intent.getStringExtra("packagename"))) {
                    k kVar = this.a;
                    kVar.n = kVar.n + 1;
                    ((ImageButton) ((Map) this.a.l.get(i2)).get("btn")).setBackgroundDrawable(this.a.i);
                    ((Map) this.a.l.get(i2)).put("back", ((ImageButton) ((Map) this.a.l.get(i2)).get("btn")).getBackground());
                    intent2.setAction("installfrapadapter");
                    break;
                } else {
                    i2++;
                }
            }
        } else if (intent.getAction().equals("installupgratefinish")) {
            if (this.a.m != 0) {
                k kVar2 = this.a;
                kVar2.m = kVar2.m - 1;
                int i3 = 0;
                while (true) {
                    if (i3 >= this.a.k.size()) {
                        break;
                    } else if (!((Map) this.a.k.get(i3)).get("packagename").equals(intent.getStringExtra("packagename"))) {
                        i3++;
                    } else if (((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).getBackground().equals(this.a.e)) {
                        ((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).setBackgroundDrawable(this.a.f);
                        ((Map) this.a.k.get(i3)).put("back", ((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).getBackground());
                    } else if (((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).getBackground().equals(this.a.i)) {
                        ((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).setBackgroundDrawable(this.a.j);
                        ((Map) this.a.k.get(i3)).put("back", ((ImageButton) ((Map) this.a.k.get(i3)).get("btn")).getBackground());
                    }
                }
            }
            if (this.a.n != 0) {
                k kVar3 = this.a;
                kVar3.n = kVar3.n - 1;
                while (true) {
                    if (i >= this.a.l.size()) {
                        break;
                    } else if (((Map) this.a.l.get(i)).get("packagename").equals(intent.getStringExtra("packagename"))) {
                        ((ImageButton) ((Map) this.a.l.get(i)).get("btn")).setBackgroundDrawable(this.a.j);
                        ((Map) this.a.l.get(i)).put("back", ((ImageButton) ((Map) this.a.l.get(i)).get("btn")).getBackground());
                        intent2.setAction("installokfrapadapter");
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        this.a.a.sendBroadcast(intent2);
    }
}
