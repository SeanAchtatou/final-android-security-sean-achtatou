package klkl.flkd.lbiao;

import android.graphics.drawable.Drawable;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public final class o {
    /* access modifiers changed from: private */
    public HashMap a = new HashMap();

    public static Drawable a(String str) {
        InputStream inputStream;
        try {
            inputStream = (InputStream) new URL(str).getContent();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            inputStream = null;
        } catch (IOException e2) {
            e2.printStackTrace();
            inputStream = null;
        }
        return Drawable.createFromStream(inputStream, "src");
    }

    public final Drawable a(String str, r rVar) {
        Drawable drawable;
        if (this.a.containsKey(str) && (drawable = (Drawable) ((SoftReference) this.a.get(str)).get()) != null) {
            return drawable;
        }
        new q(this, str, new p(this, rVar, str)).start();
        return null;
    }
}
