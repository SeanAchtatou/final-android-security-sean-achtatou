package klkl.flkd.lbiao;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import java.util.Map;
import klkl.flkd.tools.o;

final class g implements AdapterView.OnItemClickListener {
    private /* synthetic */ YyLb a;

    private g(YyLb yyLb) {
        this.a = yyLb;
    }

    /* synthetic */ g(YyLb yyLb, byte b) {
        this(yyLb);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: klkl.flkd.tools.o.a(android.content.Context, java.lang.String):void
     arg types: [klkl.flkd.lbiao.YyLb, java.lang.String]
     candidates:
      klkl.flkd.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      klkl.flkd.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
      klkl.flkd.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      klkl.flkd.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      klkl.flkd.tools.o.a(java.lang.String, java.lang.String):void
      klkl.flkd.tools.o.a(android.content.Context, java.lang.String):void */
    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        if (this.a.k.equals("loadApp")) {
            this.a.b = (String) ((Map) this.a.l.get(i)).get("title");
            this.a.c = (String) ((Map) this.a.l.get(i)).get("uil");
            this.a.d = (String) ((Map) this.a.l.get(i)).get("apk");
            this.a.e = (String) ((Map) this.a.l.get(i)).get("setpkg");
            this.a.f = ((Integer) ((Map) this.a.l.get(i)).get("versionCode")).intValue();
        } else if (this.a.k.equals("loadGame")) {
            this.a.b = (String) ((Map) this.a.m.get(i)).get("title");
            this.a.c = (String) ((Map) this.a.m.get(i)).get("uil");
            this.a.d = (String) ((Map) this.a.m.get(i)).get("apk");
            this.a.e = (String) ((Map) this.a.m.get(i)).get("setpkg");
            this.a.f = ((Integer) ((Map) this.a.m.get(i)).get("versionCode")).intValue();
        }
        if (!o.a(this.a, this.a.e, this.a.f, "open")) {
            o.a((Context) this.a, this.a.e);
            return;
        }
        Intent intent = new Intent(this.a, YyXxXs.class);
        intent.putExtra("activity_flag", "show");
        intent.putExtra("title", this.a.b);
        intent.putExtra("uil", this.a.c);
        intent.putExtra("http", this.a.d);
        intent.putExtra("packagename", this.a.e);
        intent.putExtra("position", i);
        intent.putExtra("versionCode", this.a.f);
        this.a.startActivity(intent);
    }
}
