package klkl.flkd.lbiao;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import klkl.flkd.a.a;
import klkl.flkd.tools.b;
import klkl.flkd.tools.f;
import klkl.flkd.tools.m;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public class YyLb extends Activity implements AbsListView.OnScrollListener {
    private LinearLayout a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public LinearLayout g = null;
    /* access modifiers changed from: private */
    public ListView h = null;
    /* access modifiers changed from: private */
    public k i;
    /* access modifiers changed from: private */
    public k j;
    /* access modifiers changed from: private */
    public String k = null;
    /* access modifiers changed from: private */
    public ArrayList l = new ArrayList();
    /* access modifiers changed from: private */
    public ArrayList m = new ArrayList();
    private i n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public int q = 1;
    /* access modifiers changed from: private */
    public List r = new ArrayList();
    /* access modifiers changed from: private */
    public f s;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public m u;
    /* access modifiers changed from: private */
    public Handler v = new f(this);
    private j w;

    static /* synthetic */ void a(YyLb yyLb, Map map) {
        if (map != null) {
            r.O = (String[]) map.get("title");
            r.P = (String[]) map.get("logo");
            r.R = (String[]) map.get("uil");
            r.S = (String[]) map.get("apk");
            r.T = (String[]) map.get("setpkg");
            r.Q = (String[]) map.get("size");
            r.U = (Integer[]) map.get("versionCode");
            r.V = (String[]) map.get("versionName");
            r.ab = (String[]) map.get("install");
            r.W = (String[]) map.get("appKeyWord");
            for (int i2 = 0; i2 < r.O.length; i2++) {
                HashMap hashMap = new HashMap();
                hashMap.put("logo", r.P[i2]);
                hashMap.put("title", r.O[i2]);
                hashMap.put("uil", r.R[i2]);
                hashMap.put("apk", r.S[i2]);
                hashMap.put("setpkg", r.T[i2]);
                hashMap.put("size", r.Q[i2]);
                hashMap.put("versionCode", r.U[i2]);
                hashMap.put("versionName", r.V[i2]);
                hashMap.put("install", r.ab[i2]);
                hashMap.put("appKeyWord", r.W[i2]);
                yyLb.r.add(hashMap);
            }
            yyLb.p = yyLb.r.size();
        }
    }

    static /* synthetic */ void e(YyLb yyLb) {
        yyLb.u.setVisibility(0);
        yyLb.n.sendEmptyMessageDelayed(6, 3000);
    }

    static /* synthetic */ void r(YyLb yyLb) {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, -100.0f);
        translateAnimation.setDuration(1000);
        translateAnimation.setFillAfter(true);
        yyLb.u.setAnimation(translateAnimation);
        translateAnimation.startNow();
        yyLb.v.sendEmptyMessageDelayed(5, 1000);
    }

    static /* synthetic */ void t(YyLb yyLb) {
        yyLb.unregisterReceiver(yyLb.w);
        yyLb.finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: klkl.flkd.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [klkl.flkd.lbiao.YyLb, java.lang.String]
     candidates:
      klkl.flkd.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      klkl.flkd.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      klkl.flkd.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      klkl.flkd.tools.o.a(android.content.Context, java.lang.String):void
      klkl.flkd.tools.o.a(java.lang.String, java.lang.String):void
      klkl.flkd.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        HandlerThread handlerThread = new HandlerThread("applist");
        handlerThread.start();
        this.n = new i(this, handlerThread.getLooper());
        this.a = new LinearLayout(this);
        this.a.setOrientation(1);
        this.a.setBackgroundDrawable(o.a((Activity) this, "tabhost/back.png"));
        this.s = new f(this);
        this.s.setGravity(17);
        this.u = new m(this);
        this.u.setText("下载APP迅速升级论坛等级");
        this.u.setVisibility(8);
        this.a.addView(this.u);
        this.g = new b(this);
        this.h = new ListView(this);
        this.h.setFastScrollEnabled(true);
        a.a(this.h, this);
        this.h.setDivider(o.a((Activity) this, "bitmap/dividerH.png"));
        this.h.setBackgroundDrawable(o.a((Activity) this, "tabhost/back.png"));
        this.h.setCacheColorHint(-1);
        this.h.addFooterView(this.g);
        this.k = getIntent().getStringExtra("load");
        this.h.setOnScrollListener(this);
        this.h.setOnItemClickListener(new g(this, (byte) 0));
        this.a.addView(this.s);
        this.a.addView(this.h);
        setContentView(this.a);
        this.n.sendEmptyMessage(0);
        this.w = new j(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("installfrapadapter");
        intentFilter.addAction("installokfrapadapter");
        intentFilter.addAction("AppListActivityDestroy");
        registerReceiver(this.w, intentFilter);
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.o = (i2 + i3) - 1;
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        if (this.o == this.p && i2 == 0 && this.t) {
            this.t = false;
            this.n.sendEmptyMessage(0);
        }
    }
}
