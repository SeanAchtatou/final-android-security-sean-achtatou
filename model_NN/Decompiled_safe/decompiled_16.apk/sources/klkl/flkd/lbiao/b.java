package klkl.flkd.lbiao;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import klkl.flkd.tools.n;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;
import org.json.JSONObject;

final class b extends Handler {
    private /* synthetic */ YyFwQq a;

    b(YyFwQq yyFwQq) {
        this.a = yyFwQq;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.a.d.setProgress(message.getData().getInt("size"));
                String string = message.getData().getString("title");
                message.getData().getString("mUrl");
                String string2 = message.getData().getString("setpck");
                n unused = this.a.c;
                YyFwQq yyFwQq = this.a;
                Notification notification = new Notification();
                long currentTimeMillis = System.currentTimeMillis();
                notification.icon = 17301633;
                notification.tickerText = String.valueOf(string) + " 下载中...";
                notification.when = currentTimeMillis;
                notification.flags |= 32;
                notification.setLatestEventInfo(yyFwQq, string, "下载进度" + ((int) ((((float) this.a.d.getProgress()) / ((float) this.a.d.getMax())) * 100.0f)) + "%", PendingIntent.getActivity(yyFwQq, string2.hashCode(), new Intent(), 268435456));
                ((NotificationManager) yyFwQq.getSystemService("notification")).notify(string2.hashCode(), notification);
                if (this.a.d.getProgress() == this.a.d.getMax()) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    String string3 = this.a.getSharedPreferences("data", 0).getString(String.valueOf(string2) + "_filename", "");
                    intent.setDataAndType(Uri.fromFile(new File(string3)), "application/vnd.android.package-archive");
                    this.a.startActivity(intent);
                    n unused2 = this.a.c;
                    YyFwQq yyFwQq2 = this.a;
                    Notification notification2 = new Notification();
                    long currentTimeMillis2 = System.currentTimeMillis();
                    notification2.icon = 17301634;
                    notification2.tickerText = "软件下载成功";
                    notification2.when = currentTimeMillis2;
                    notification2.flags |= 32;
                    Intent intent2 = new Intent(yyFwQq2, YyXxXs.class);
                    intent2.putExtra("activity_flag", "install");
                    intent2.putExtra("Pash", string3);
                    intent2.putExtra("packagename", string2);
                    intent2.putExtra("Title", string);
                    intent2.putExtra("Biaoshi", "1");
                    notification2.setLatestEventInfo(yyFwQq2, "下载成功  点击安装", string, PendingIntent.getActivity(yyFwQq2, string2.hashCode(), intent2, 268435456));
                    ((NotificationManager) yyFwQq2.getSystemService("notification")).notify(string2.hashCode(), notification2);
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("para", r.c);
                        jSONObject.put("url", "warepck=" + string2 + "&operate=3");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    r.ai = false;
                    new o(this.a.getApplication(), jSONObject.toString()).start();
                    SharedPreferences.Editor edit = this.a.getSharedPreferences("data", 0).edit();
                    edit.putString("list" + string2, "1");
                    edit.commit();
                    this.a.stopSelf();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
