package klkl.flkd.lbiao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class j extends BroadcastReceiver {
    private /* synthetic */ YyLb a;

    private j(YyLb yyLb) {
        this.a = yyLb;
    }

    /* synthetic */ j(YyLb yyLb, byte b) {
        this(yyLb);
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("AppListActivityDestroy")) {
            YyLb.t(this.a);
            return;
        }
        if (this.a.i != null) {
            this.a.i.notifyDataSetChanged();
        }
        if (this.a.j != null) {
            this.a.j.notifyDataSetChanged();
        }
    }
}
