package klkl.flkd.lbiao;

import android.view.MotionEvent;
import android.view.View;

final class l implements View.OnTouchListener {
    private /* synthetic */ k a;

    l(k kVar) {
        this.a = kVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (view.getBackground().equals(this.a.c)) {
                    view.setBackgroundDrawable(this.a.d);
                    return true;
                } else if (!view.getBackground().equals(this.a.g)) {
                    return true;
                } else {
                    view.setBackgroundDrawable(this.a.h);
                    return true;
                }
            case 1:
                if (view.getBackground().equals(this.a.d)) {
                    view.setBackgroundDrawable(this.a.e);
                    k.a(this.a, view, "no");
                    return true;
                } else if (!view.getBackground().equals(this.a.h)) {
                    return true;
                } else {
                    view.setBackgroundDrawable(this.a.i);
                    k.a(this.a, view, "yes");
                    return true;
                }
            default:
                return true;
        }
    }
}
