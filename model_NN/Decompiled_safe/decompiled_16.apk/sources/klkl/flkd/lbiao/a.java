package klkl.flkd.lbiao;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import klkl.flkd.tools.r;

public final class a extends RelativeLayout {
    private Activity a;
    private int b = 1;
    private int c = 11;

    public a(Activity activity) {
        super(activity);
        this.a = activity;
        ImageView imageView = new ImageView(this.a);
        if (r.ag >= 320) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(128, 128));
            imageView.setPadding(15, 15, 0, 0);
        } else if (r.ag >= 240 && r.ag < 320) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(96, 96));
            imageView.setPadding(25, 10, 0, 0);
        } else if (240 > r.ag && r.ag >= 180) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(64, 64));
            imageView.setPadding(20, 5, 0, 0);
        } else if (r.ag < 180 && r.ag > 120) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(55, 55));
            imageView.setPadding(10, 10, 0, 0);
        } else if (r.ag <= 120) {
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(45, 45));
            imageView.setPadding(8, 8, 0, 0);
        }
        imageView.setId(this.b);
        addView(imageView);
        LinearLayout linearLayout = new LinearLayout(this.a);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(1, this.b);
        layoutParams.addRule(0, this.c);
        layoutParams.addRule(15);
        layoutParams.setMargins(18, 0, 0, 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        TextView textView = new TextView(linearLayout.getContext());
        textView.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        textView.setTextSize(18.0f);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView.setTextColor(-16777216);
        linearLayout.addView(textView);
        TextView textView2 = new TextView(linearLayout.getContext());
        textView2.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        textView2.setTextSize(13.0f);
        textView2.setSingleLine(true);
        textView2.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView2.setTextColor(-7829368);
        linearLayout.addView(textView2);
        TextView textView3 = new TextView(this.a);
        textView3.setSingleLine(true);
        textView3.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView3.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        textView3.setTextSize(13.0f);
        textView3.setTextColor(-7829368);
        linearLayout.addView(textView3);
        addView(linearLayout);
        ImageButton imageButton = new ImageButton(this.a);
        RelativeLayout.LayoutParams layoutParams2 = null;
        if (r.ag >= 320) {
            setMinimumHeight(150);
            layoutParams2 = new RelativeLayout.LayoutParams(140, 60);
            layoutParams2.setMargins(0, 0, 15, 0);
        } else if (r.ag >= 240 && r.ag < 320) {
            setMinimumHeight(113);
            layoutParams2 = new RelativeLayout.LayoutParams(105, 45);
            layoutParams2.setMargins(0, 0, 12, 0);
        } else if (240 > r.ag && r.ag >= 180) {
            setMinimumHeight(85);
            layoutParams2 = new RelativeLayout.LayoutParams(79, 34);
            layoutParams2.setMargins(0, 0, 10, 0);
        } else if (r.ag < 180 && r.ag > 120) {
            setMinimumHeight(64);
            layoutParams2 = new RelativeLayout.LayoutParams(60, 26);
            layoutParams2.setMargins(0, 0, 10, 0);
        } else if (r.ag <= 120) {
            setMinimumHeight(48);
            layoutParams2 = new RelativeLayout.LayoutParams(45, 20);
            layoutParams2.setMargins(0, 0, 8, 0);
        }
        layoutParams2.addRule(11);
        layoutParams2.addRule(13);
        imageButton.setLayoutParams(layoutParams2);
        imageButton.setId(this.c);
        imageButton.setFocusable(false);
        addView(imageButton);
    }
}
