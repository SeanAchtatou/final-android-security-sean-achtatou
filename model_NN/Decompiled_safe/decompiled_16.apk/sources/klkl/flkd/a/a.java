package klkl.flkd.a;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import klkl.flkd.tools.o;
import klkl.flkd.tools.r;

public class a {
    /* access modifiers changed from: private */
    public HashMap a;

    public a() {
        this.a = null;
        this.a = new HashMap();
    }

    public static int a(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(Integer.MAX_VALUE);
        if (!runningTasks.isEmpty()) {
            ComponentName componentName = runningTasks.get(0).topActivity;
            if (context.getPackageName().equals(componentName.getPackageName())) {
                return 0;
            }
            if ("com.android.packageinstaller".equals(componentName.getPackageName())) {
                return 1;
            }
            for (int i = 1; i < runningTasks.size(); i++) {
                if (context.getPackageName().equals(runningTasks.get(i).topActivity.getPackageName())) {
                    return 1;
                }
            }
            return !r.J ? 2 : 1;
        }
        Log.e("panda", "___222222");
        return 2;
    }

    public static Bitmap a(Bitmap bitmap, int i) {
        int i2 = 100;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        while (byteArrayOutputStream.toByteArray().length / 1024 > i) {
            byteArrayOutputStream.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, i2, byteArrayOutputStream);
            i2 -= 10;
        }
        return BitmapFactory.decodeStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), null, null);
    }

    public static Bitmap a(String str) {
        try {
            InputStream inputStream = ((HttpURLConnection) new URL(str).openConnection()).getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = 3;
            return BitmapFactory.decodeStream(inputStream, null, options);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void a(ListView listView, Activity activity) {
        Field declaredField;
        try {
            Field declaredField2 = AbsListView.class.getDeclaredField("mFastScroller");
            if (declaredField2 != null) {
                declaredField2.setAccessible(true);
                Object obj = declaredField2.get(listView);
                if (obj != null && (declaredField = declaredField2.getType().getDeclaredField("mThumbDrawable")) != null) {
                    declaredField.setAccessible(true);
                    if (((Drawable) declaredField.get(obj)) != null) {
                        declaredField.set(obj, o.a(activity, "bitmap/listViewFastScorllImg.png"));
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void a(File file) {
        if (!file.exists()) {
            return;
        }
        if (file.isFile()) {
            file.delete();
        } else if (file.isDirectory()) {
            File[] listFiles = file.listFiles();
            if (listFiles == null || listFiles.length == 0) {
                file.delete();
                return;
            }
            for (File a2 : listFiles) {
                a(a2);
            }
            file.delete();
        }
    }

    public static void a(String str, String str2, Bitmap bitmap) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(String.valueOf(str) + str2);
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, new BufferedOutputStream(fileOutputStream));
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap b(java.lang.String r5) {
        /*
            r1 = 0
            if (r5 != 0) goto L_0x0005
            r0 = r1
        L_0x0004:
            return r0
        L_0x0005:
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0033, all -> 0x003f }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            r2 = 5000(0x1388, float:7.006E-42)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            r0.connect()     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            r3.close()     // Catch:{ Exception -> 0x004e, all -> 0x0046 }
            if (r0 == 0) goto L_0x0031
            r0.disconnect()
        L_0x0031:
            r0 = r2
            goto L_0x0004
        L_0x0033:
            r0 = move-exception
            r2 = r1
        L_0x0035:
            r0.printStackTrace()     // Catch:{ all -> 0x004b }
            if (r2 == 0) goto L_0x003d
            r2.disconnect()
        L_0x003d:
            r0 = r1
            goto L_0x0004
        L_0x003f:
            r0 = move-exception
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.disconnect()
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            r1 = r2
            goto L_0x0040
        L_0x004e:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: klkl.flkd.a.a.b(java.lang.String):android.graphics.Bitmap");
    }

    public static boolean c(String str) {
        try {
            byte[] bArr = new byte[2];
            String str2 = "";
            if (new FileInputStream(str).read(bArr) == -1) {
                return false;
            }
            for (int i = 0; i < bArr.length; i++) {
                str2 = String.valueOf(str2) + Integer.toString(bArr[i] & 255);
            }
            switch (Integer.parseInt(str2)) {
                case 6677:
                    return true;
                case 7173:
                case 7784:
                case 7790:
                case 8075:
                case 8297:
                    return false;
                case 13780:
                    return true;
                case 255216:
                    return true;
                default:
                    "unknown type: " + str2;
                    return false;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public Bitmap a(ImageView imageView, String str, d dVar) {
        if (this.a.containsKey(str)) {
            Bitmap bitmap = (Bitmap) ((SoftReference) this.a.get(str)).get();
            if (bitmap != null) {
                return bitmap;
            }
        } else {
            String substring = str.substring(str.lastIndexOf("/") + 1);
            File[] listFiles = new File(r.ar).listFiles();
            int i = 0;
            if (listFiles != null) {
                while (i < listFiles.length && !substring.equals(listFiles[i].getName())) {
                    i++;
                }
                if (i < listFiles.length) {
                    return BitmapFactory.decodeFile(String.valueOf(r.ar) + substring);
                }
            }
        }
        new c(this, str, new b(this, dVar, imageView)).start();
        return null;
    }
}
