package klkl.flkd.a;

import android.graphics.Bitmap;
import android.os.Handler;
import java.lang.ref.SoftReference;
import klkl.flkd.tools.r;

final class c extends Thread {
    private /* synthetic */ a a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Handler c;

    c(a aVar, String str, Handler handler) {
        this.a = aVar;
        this.b = str;
        this.c = handler;
    }

    public final void run() {
        Bitmap a2 = a.a(this.b);
        this.a.a.put(this.b, new SoftReference(a2));
        this.c.sendMessage(this.c.obtainMessage(0, a2));
        a.a(r.ar, this.b.substring(this.b.lastIndexOf("/") + 1), a2);
    }
}
