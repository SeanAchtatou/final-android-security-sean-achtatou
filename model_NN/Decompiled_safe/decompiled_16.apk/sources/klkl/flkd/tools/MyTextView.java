package klkl.flkd.tools;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView implements Runnable {
    private int a;
    private String b;
    private int c;
    private boolean d = false;

    public MyTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public MyTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        setFocusableInTouchMode(true);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        setMarqueeRepeatLimit(-1);
        setHorizontallyScrolling(true);
        removeCallbacks(this);
        post(this);
    }

    public boolean isFocused() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.d) {
            TextPaint paint = getPaint();
            this.b = getText().toString();
            this.c = (int) paint.measureText(this.b);
            this.d = true;
        }
    }

    public void run() {
        this.a++;
        scrollTo(this.a, 0);
        if (getScrollX() == this.c) {
            scrollTo(this.c, 0);
            this.a = -this.c;
        }
        postDelayed(this, 5);
    }
}
