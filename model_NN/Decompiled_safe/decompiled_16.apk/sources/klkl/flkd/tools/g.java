package klkl.flkd.tools;

import android.content.Context;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.EditText;

public final class g extends EditText {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public int b = 0;
    private String c;
    private final TextWatcher d = new h(this);

    public g(Context context, String str) {
        super(context);
        this.a = context;
        if (str.equals("loadSquare")) {
            this.b = 500;
            this.c = "说点什么吧，让游友认识一下，最多只能输入500个汉字！提示:你现在发表的是说说、笑话、段子等,发表内容不符管理员有权删除,谢谢合作！";
            setHeight((r.af * 5) / 8);
            setWidth((r.ae * 9) / 10);
        } else {
            this.c = "说点什么吧，让游友认识一下，最多只能输入120个汉字！提示:你现在发表的是游戏、应用评论，发表内容不符管理员有权删除,，谢谢合作！";
            this.b = 120;
            setHeight((r.af * 5) / 8);
            setWidth((r.ae * 9) / 10);
        }
        setFocusable(true);
        setSingleLine(false);
        setHint(this.c);
        setTextColor(-16777216);
        setGravity(48);
        setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        addTextChangedListener(this.d);
    }
}
