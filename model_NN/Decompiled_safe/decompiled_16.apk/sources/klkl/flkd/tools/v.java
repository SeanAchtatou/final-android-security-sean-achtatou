package klkl.flkd.tools;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import klkl.flkd.lbiao.a;

final class v implements View.OnTouchListener {
    private final /* synthetic */ Drawable a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ Drawable c;
    private final /* synthetic */ a d;

    v(Drawable drawable, Activity activity, Drawable drawable2, a aVar) {
        this.a = drawable;
        this.b = activity;
        this.c = drawable2;
        this.d = aVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.a);
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            t.b(this.b);
            view.setBackgroundDrawable(this.c);
            Message message = new Message();
            message.obj = this.d;
            t.a.sendMessageDelayed(message, 2500);
            return false;
        }
    }
}
