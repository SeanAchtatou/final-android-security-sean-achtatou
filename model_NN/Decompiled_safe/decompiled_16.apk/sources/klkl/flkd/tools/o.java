package klkl.flkd.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import klkl.flkd.a.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class o extends Thread {
    private static List h;
    /* access modifiers changed from: private */
    public q a;
    /* access modifiers changed from: private */
    public Context b = null;
    private Object c;
    private String d = "nodiscuss";
    /* access modifiers changed from: private */
    public Integer e = 0;
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public Bitmap g = null;
    private Handler i = new p(this);

    public o(Context context, String str) {
        this.b = context;
        this.c = str;
    }

    public o(Context context, String str, String str2) {
        this.b = context;
        this.c = str;
        this.d = str2;
    }

    public o(Context context, q qVar, Object obj) {
        this.b = context;
        this.a = qVar;
        this.c = obj;
    }

    public o(Context context, q qVar, Object obj, String str) {
        this.b = context;
        this.a = qVar;
        this.c = obj;
        this.d = str;
    }

    public static Intent a(File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        return intent;
    }

    public static Bitmap a(String str, Context context) {
        IOException e2;
        Bitmap bitmap;
        try {
            InputStream open = context.getResources().getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            try {
                open.close();
            } catch (IOException e3) {
                e2 = e3;
                e2.printStackTrace();
                return bitmap;
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            bitmap = null;
            e2 = iOException;
        }
        return bitmap;
    }

    public static BitmapDrawable a(Activity activity, String str) {
        try {
            return new BitmapDrawable(BitmapFactory.decodeStream(activity.getAssets().open(str)));
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static BitmapDrawable a(Bitmap bitmap, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, 15.0f, 15.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return new BitmapDrawable(createBitmap);
    }

    public static List a(String str) {
        JSONObject jSONObject;
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(str);
            r.aa = Integer.valueOf(jSONArray.length() - 1);
            int i2 = 0;
            while (i2 < r.aa.intValue() && (jSONObject = jSONArray.getJSONObject(i2)) != null) {
                HashMap hashMap = new HashMap();
                hashMap.put("nameInfo", jSONObject.getString("viewName"));
                hashMap.put("noteInfo", jSONObject.getString("replayDetail"));
                hashMap.put("userNum", jSONObject.getString("userNum"));
                arrayList.add(hashMap);
                i2++;
            }
            return arrayList;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0103, code lost:
        if (r1.versionCode >= r15.getInt("versionCode")) goto L_0x0111;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0105, code lost:
        r13[r4] = "updateyes";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r13[r4] = "installyes";
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Map a(android.content.SharedPreferences r18, int r19) {
        /*
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            if (r18 == 0) goto L_0x011e
            r0 = r19
            java.lang.String[] r5 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r6 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r7 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r8 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r9 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r10 = new java.lang.String[r0]
            r0 = r19
            java.lang.Integer[] r11 = new java.lang.Integer[r0]
            r0 = r19
            java.lang.String[] r12 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r13 = new java.lang.String[r0]
            r0 = r19
            java.lang.String[] r14 = new java.lang.String[r0]
            r1 = 0
            r4 = r1
        L_0x0031:
            r0 = r19
            if (r4 < r0) goto L_0x0069
        L_0x0035:
            java.lang.String r1 = "title"
            r2.put(r1, r5)
            java.lang.String r1 = "logo"
            r2.put(r1, r6)
            java.lang.String r1 = "uil"
            r2.put(r1, r7)
            java.lang.String r1 = "apk"
            r2.put(r1, r8)
            java.lang.String r1 = "setpkg"
            r2.put(r1, r9)
            java.lang.String r1 = "size"
            r2.put(r1, r10)
            java.lang.String r1 = "versionCode"
            r2.put(r1, r11)
            java.lang.String r1 = "versionName"
            r2.put(r1, r12)
            java.lang.String r1 = "install"
            r2.put(r1, r13)
            java.lang.String r1 = "appKeyWord"
            r2.put(r1, r14)
            r1 = r2
        L_0x0068:
            return r1
        L_0x0069:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "json"
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r3 = 0
            r0 = r18
            java.lang.String r1 = r0.getString(r1, r3)
            if (r1 == 0) goto L_0x0035
            org.json.JSONObject r15 = new org.json.JSONObject     // Catch:{ JSONException -> 0x010a }
            r15.<init>(r1)     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "name"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r5[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "icon"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r6[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "html"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r7[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "apk"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r8[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "pck"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r9[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "sizes"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r10[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "versionCode"
            int r1 = r15.getInt(r1)     // Catch:{ JSONException -> 0x010a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ JSONException -> 0x010a }
            r11[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "versionName"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r12[r4] = r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r1 = "mainwords"
            java.lang.String r1 = r15.getString(r1)     // Catch:{ JSONException -> 0x010a }
            r14[r4] = r1     // Catch:{ JSONException -> 0x010a }
            r1 = 0
            r3 = r1
        L_0x00d4:
            java.util.List r1 = klkl.flkd.tools.o.h     // Catch:{ JSONException -> 0x010a }
            int r1 = r1.size()     // Catch:{ JSONException -> 0x010a }
            if (r3 < r1) goto L_0x00e1
        L_0x00dc:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x0031
        L_0x00e1:
            java.util.List r1 = klkl.flkd.tools.o.h     // Catch:{ JSONException -> 0x010a }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ JSONException -> 0x010a }
            android.content.pm.PackageInfo r1 = (android.content.pm.PackageInfo) r1     // Catch:{ JSONException -> 0x010a }
            java.lang.String r0 = r1.packageName     // Catch:{ JSONException -> 0x010a }
            r16 = r0
            java.lang.String r17 = "pck"
            r0 = r17
            java.lang.String r17 = r15.getString(r0)     // Catch:{ JSONException -> 0x010a }
            boolean r16 = r16.equals(r17)     // Catch:{ JSONException -> 0x010a }
            if (r16 == 0) goto L_0x0116
            int r1 = r1.versionCode     // Catch:{ JSONException -> 0x010a }
            java.lang.String r3 = "versionCode"
            int r3 = r15.getInt(r3)     // Catch:{ JSONException -> 0x010a }
            if (r1 >= r3) goto L_0x0111
            java.lang.String r1 = "updateyes"
            r13[r4] = r1     // Catch:{ JSONException -> 0x010a }
            goto L_0x00dc
        L_0x010a:
            r1 = move-exception
            r1.printStackTrace()
            r1 = 0
            goto L_0x0068
        L_0x0111:
            java.lang.String r1 = "installyes"
            r13[r4] = r1     // Catch:{ JSONException -> 0x010a }
            goto L_0x00dc
        L_0x0116:
            java.lang.String r1 = "installno"
            r13[r4] = r1     // Catch:{ JSONException -> 0x010a }
            int r1 = r3 + 1
            r3 = r1
            goto L_0x00d4
        L_0x011e:
            r1 = r2
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: klkl.flkd.tools.o.a(android.content.SharedPreferences, int):java.util.Map");
    }

    public static void a() {
    }

    public static void a(Activity activity) {
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (activity.getResources().getConfiguration().orientation == 2) {
            r.ac = defaultDisplay.getHeight();
            r.ad = defaultDisplay.getWidth();
            r.ae = displayMetrics.heightPixels;
            r.af = displayMetrics.widthPixels;
            return;
        }
        r.ac = defaultDisplay.getWidth();
        r.ad = defaultDisplay.getHeight();
        r.ae = displayMetrics.widthPixels;
        r.af = displayMetrics.heightPixels;
    }

    public static void a(Context context, String str) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
        launchIntentForPackage.setFlags(268435456);
        context.startActivity(launchIntentForPackage);
    }

    public static void a(Context context, String str, String str2) {
        SharedPreferences.Editor editor = null;
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (str2.equals("loadApp")) {
                r.X = Integer.valueOf(jSONArray.length() - 1);
                editor = context.getSharedPreferences("AppData", 0).edit();
            } else if (str2.equals("loadGame")) {
                r.Y = Integer.valueOf(jSONArray.length() - 1);
                editor = context.getSharedPreferences("GameData", 0).edit();
            }
            editor.putString("json_full", str);
            for (int i2 = 0; i2 < jSONArray.length() - 1; i2++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                if (jSONObject.toString() == null) {
                    break;
                }
                editor.putString("json" + i2, jSONObject.toString());
            }
            editor.commit();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public static void a(Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
            System.gc();
        }
    }

    public static void a(String str, String str2) {
        if ("354244052835606".equals(str) || "868850019884443".equals(str) || "354096054813068".equals(str) || "351863041114121".equals(str) || "356812040911685".equals(str) || "867183012859365".equals(str) || "358071043359917".equals(str) || "861189016161670".equals(str) || "352762056233488".equals(str) || "359836046958017".equals(str) || "354316036492611".equals(str) || "867064016763631".equals(str)) {
            System.out.println(str2);
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    public static boolean a(Context context, String str, int i2, String str2) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        if (installedPackages == null) {
            return true;
        }
        for (int i3 = 0; i3 < installedPackages.size(); i3++) {
            String str3 = installedPackages.get(i3).packageName;
            int i4 = installedPackages.get(i3).versionCode;
            if (str != null && str.equals(str3)) {
                if (str2.equals("open")) {
                    return false;
                }
                if (str2.equals("install")) {
                    return i4 < i2;
                }
            }
        }
        return true;
    }

    public static Bitmap b(Context context, String str, String str2) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(context.getAssets().open(str));
            for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
                if (str2.equals(nextEntry.getName())) {
                    return BitmapFactory.decodeStream(zipInputStream);
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public static String b() {
        return UUID.randomUUID().toString();
    }

    public static String b(String str) {
        char[] charArray = str.toCharArray();
        for (int i2 = 0; i2 < charArray.length; i2++) {
            if (charArray[i2] == 12288) {
                charArray[i2] = ' ';
            } else if (charArray[i2] > 65280 && charArray[i2] < 65375) {
                charArray[i2] = (char) (charArray[i2] - 65248);
            }
        }
        return new String(charArray);
    }

    public static List b(String str, String str2) {
        JSONObject jSONObject;
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(str);
            r.Z = Integer.valueOf(jSONArray.length() - 1);
            int i2 = 0;
            while (i2 < r.Z.intValue() && (jSONObject = jSONArray.getJSONObject(i2)) != null) {
                HashMap hashMap = new HashMap();
                hashMap.put("uesrID", jSONObject.getString("questionerid"));
                hashMap.put("who", jSONObject.getString("usernum"));
                hashMap.put("name", String.valueOf(jSONObject.getString("viewName")) + jSONObject.getString("usernum"));
                hashMap.put("note", jSONObject.getString("questionDetail"));
                hashMap.put("count", jSONObject.getString("anserCount"));
                hashMap.put("noteID", jSONObject.getString("questionID"));
                hashMap.put("date", jSONObject.getString("questDate"));
                hashMap.put("time", jSONObject.getString("questTime"));
                hashMap.put("rank", jSONObject.getString("rankName"));
                hashMap.put("imgUrl", jSONObject.getString("imgUrl"));
                hashMap.put("smalImgUrl", jSONObject.getString("smalImgUrl"));
                hashMap.put("loadBitmap", "");
                if (str2.equals("loadSquare")) {
                    hashMap.put("support", Integer.valueOf(jSONObject.getInt("sayGoodCount")));
                    hashMap.put("nonsupport", Integer.valueOf(jSONObject.getInt("sayBadCount")));
                    hashMap.put("ifport", jSONObject.getString("isSayGoodBadFlag"));
                }
                arrayList.add(hashMap);
                i2++;
            }
            return arrayList;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Map b(Context context, String str) {
        h = c(context);
        if (str.equals("loadApp")) {
            return a(context.getSharedPreferences("AppData", 0), r.X.intValue());
        }
        if (str.equals("loadGame")) {
            return a(context.getSharedPreferences("GameData", 0), r.Y.intValue());
        }
        return null;
    }

    public static void b(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) activity.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        r.ag = displayMetrics.densityDpi;
    }

    public static boolean b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getState() != NetworkInfo.State.CONNECTED) {
            return false;
        }
        return activeNetworkInfo.getType() != 1 || !"CMCC".equals(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getSSID());
    }

    private static List c(Context context) {
        int i2 = 0;
        ArrayList arrayList = new ArrayList();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        while (true) {
            int i3 = i2;
            if (i3 >= installedPackages.size()) {
                return arrayList;
            }
            PackageInfo packageInfo = installedPackages.get(i3);
            int i4 = packageInfo.applicationInfo.flags;
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if ((i4 & 1) <= 0) {
                arrayList.add(packageInfo);
            }
            i2 = i3 + 1;
        }
    }

    public static boolean c() {
        try {
            Runtime.getRuntime().exec(new String[]{"/system/bin/su", "-c", "chmod 777 /dev/graphics/fb0"});
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean c(Context context, String str) {
        h = c(context);
        for (int i2 = 0; i2 < h.size(); i2++) {
            if (((PackageInfo) h.get(i2)).packageName.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public final void run() {
        if (this.d.equals("discuss")) {
            new s(this.d);
            this.f = s.a(this.b, this.c.toString());
            this.d = "nodiscuss";
        } else {
            this.f = s.a(this.b, this.c.toString());
        }
        if (this.a != null && this.b != null) {
            try {
                if (this.f != null && this.f.contains("flag")) {
                    JSONObject jSONObject = new JSONObject(this.f);
                    if (jSONObject.getInt("flag") != 0) {
                        String string = jSONObject.getString("logo");
                        Log.d("D", string);
                        if (string != null) {
                            this.g = a.b(string);
                        }
                    }
                }
            } catch (Exception e2) {
            }
            this.i.sendEmptyMessage(0);
        }
    }
}
