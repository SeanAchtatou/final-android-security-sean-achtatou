package klkl.flkd.tools;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import klkl.flkd.lbiao.YyFwQq;
import klkl.flkd.lbiao.a;

public final class t {
    /* access modifiers changed from: private */
    public static Handler a = new u();

    public static a a(Activity activity) {
        a aVar = new a(activity);
        aVar.setBackgroundDrawable(o.a(activity, "bitmap/gray_back.png"));
        BitmapDrawable a2 = o.a(activity, "button/install.png");
        BitmapDrawable a3 = o.a(activity, "button/installclk.png");
        ((ImageView) aVar.getChildAt(0)).setImageBitmap(o.a("port/logo.png", activity));
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("下载  游问友答客户端");
        spannableStringBuilder.setSpan(new ForegroundColorSpan(-65536), 3, 11, 33);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(30), 0, 2, 33);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(30), 8, 11, 33);
        ((TextView) ((ViewGroup) aVar.getChildAt(1)).getChildAt(0)).setText(spannableStringBuilder);
        ((TextView) ((ViewGroup) aVar.getChildAt(1)).getChildAt(1)).setVisibility(8);
        ((TextView) ((ViewGroup) aVar.getChildAt(1)).getChildAt(2)).setText(" 及时查看游友最新聊天信息");
        ImageButton imageButton = (ImageButton) aVar.getChildAt(2);
        imageButton.setBackgroundDrawable(a3);
        imageButton.setOnTouchListener(new v(a2, activity, a3, aVar));
        return aVar;
    }

    static /* synthetic */ void b(Activity activity) {
        Intent intent = new Intent(activity, YyFwQq.class);
        intent.putExtra("service_flag", "down");
        intent.putExtra("upgrateFlag", "yes");
        intent.putExtra("packagename", "com.panda.client");
        intent.putExtra("Duil", "http://www.ywyd.net:8080/APP/MyPandaClient/YWYD_CLIENT.apk");
        intent.putExtra("Title", "游问友答");
        activity.startService(intent);
    }
}
