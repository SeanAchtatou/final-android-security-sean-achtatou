package klkl.flkd.tools;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.TextView;

public final class m extends TextView {
    private Activity a;

    public m(Activity activity) {
        super(activity);
        this.a = activity;
        setBackgroundDrawable(o.a(this.a, "bitmap/gray_back.png"));
        setGravity(17);
        setTextSize(20.0f);
        if (r.ag >= 320) {
            setLayoutParams(new ViewGroup.LayoutParams(-1, 80));
        } else if (r.ag >= 240 && r.ag < 320) {
            setLayoutParams(new ViewGroup.LayoutParams(-1, 60));
        } else if (240 > r.ag && r.ag >= 180) {
            setLayoutParams(new ViewGroup.LayoutParams(-1, 50));
        } else if (r.ag < 180 && r.ag > 120) {
            setLayoutParams(new ViewGroup.LayoutParams(-1, 40));
        } else if (r.ag <= 120) {
            setLayoutParams(new ViewGroup.LayoutParams(-1, 28));
        }
    }
}
