package klkl.flkd.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

final class p extends Handler {
    private /* synthetic */ o a;

    p(o oVar) {
        this.a = oVar;
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        if (!Thread.currentThread().isInterrupted()) {
            switch (message.what) {
                case 0:
                    q a2 = this.a.a;
                    Context b = this.a.b;
                    Integer unused = this.a.e;
                    String d = this.a.f;
                    Bitmap unused2 = this.a.g;
                    a2.a(b, d);
                    return;
                default:
                    return;
            }
        }
    }
}
