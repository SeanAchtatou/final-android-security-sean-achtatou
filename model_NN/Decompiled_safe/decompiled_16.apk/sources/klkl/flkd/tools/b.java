package klkl.flkd.tools;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public final class b extends LinearLayout {
    private Activity a;
    private ProgressBar b;
    private LinearLayout.LayoutParams c = new LinearLayout.LayoutParams(-2, -2);
    private LinearLayout.LayoutParams d = new LinearLayout.LayoutParams(-2, -2);

    public b(Activity activity) {
        super(activity);
        this.a = activity;
        setMinimumHeight(50);
        setGravity(17);
        setOrientation(0);
        this.b = new ProgressBar(this.a);
        this.b.setPadding(0, 0, 15, 0);
        this.d.gravity = 17;
        addView(this.b, this.d);
        TextView textView = new TextView(this.a);
        this.c.gravity = 17;
        textView.setText("加载中...");
        addView(textView, this.c);
    }
}
