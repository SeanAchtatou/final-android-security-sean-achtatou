package klkl.flkd.tools;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import klkl.flkd.tribute.U;

public final class d extends LinearLayout {
    private static Button b;
    private Activity a;
    private LinearLayout.LayoutParams c = new LinearLayout.LayoutParams(-2, -2);
    private LinearLayout.LayoutParams d = null;

    public d(Activity activity) {
        super(activity);
        this.a = activity;
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        setGravity(17);
        b = new Button(this.a);
        if (r.ag >= 320) {
            this.d = new LinearLayout.LayoutParams(200, 60);
        } else if (r.ag >= 240 && r.ag < 320) {
            this.d = new LinearLayout.LayoutParams(180, 54);
        } else if (240 > r.ag && r.ag >= 180) {
            this.d = new LinearLayout.LayoutParams(160, 48);
        } else if (r.ag < 180 && r.ag > 120) {
            this.d = new LinearLayout.LayoutParams(140, 42);
        } else if (r.ag <= 120) {
            this.d = new LinearLayout.LayoutParams(120, 36);
        }
        b.setLayoutParams(this.d);
        b.setId(6666214);
        b.setBackgroundDrawable(o.a(this.a, "button/sofa01.png"));
        Button button = b;
        Activity activity2 = this.a;
        String str = r.K;
        button.setOnTouchListener(new U(activity2, "loadTribute"));
        addView(b, this.d);
        TextView textView = new TextView(this.a);
        textView.setGravity(17);
        this.c.gravity = 17;
        textView.setText("暂无评论，快来抢占沙发");
        textView.setTextSize(25.0f);
        addView(textView, this.c);
    }
}
