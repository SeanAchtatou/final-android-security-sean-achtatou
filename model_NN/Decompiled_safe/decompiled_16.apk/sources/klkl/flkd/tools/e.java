package klkl.flkd.tools;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class e extends LinearLayout {
    public static Button a;
    private Activity b;
    private LinearLayout.LayoutParams c = new LinearLayout.LayoutParams(-2, -2);
    private LinearLayout.LayoutParams d = null;

    public e(Activity activity) {
        super(activity);
        this.b = activity;
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        setGravity(17);
        a = new Button(this.b);
        if (r.ag >= 320) {
            this.d = new LinearLayout.LayoutParams(200, 60);
        } else if (r.ag >= 240 && r.ag < 320) {
            this.d = new LinearLayout.LayoutParams(180, 54);
        } else if (240 > r.ag && r.ag >= 180) {
            this.d = new LinearLayout.LayoutParams(160, 48);
        } else if (r.ag < 180 && r.ag > 120) {
            this.d = new LinearLayout.LayoutParams(140, 42);
        } else if (r.ag <= 120) {
            this.d = new LinearLayout.LayoutParams(120, 36);
        }
        a.setLayoutParams(this.d);
        a.setId(6666214);
        a.setBackgroundDrawable(o.a(this.b, "button/again01.png"));
        addView(a, this.d);
        TextView textView = new TextView(this.b);
        textView.setGravity(17);
        this.c.gravity = 17;
        textView.setText("加载失败，请检查网络");
        textView.setTextSize(25.0f);
        addView(textView, this.c);
    }
}
