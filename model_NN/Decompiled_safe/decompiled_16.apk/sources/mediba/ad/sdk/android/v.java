package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class v implements Runnable {
    private WeakReference a;
    private WeakReference b;
    private int c;
    private boolean d = true;
    private /* synthetic */ MasAdView e;

    public v(MasAdView masAdView, MasAdView masAdView2, AdContainer adContainer, int i, boolean z) {
        this.e = masAdView;
        this.a = new WeakReference(masAdView2);
        this.b = new WeakReference(adContainer);
        this.c = i;
    }

    public final void run() {
        try {
            MasAdView masAdView = (MasAdView) this.a.get();
            AdContainer i = this.e.f;
            if (i != null) {
                masAdView.setClickable(false);
                if (MasAdView.n == 2) {
                    i.fadeOut(300);
                } else if (MasAdView.n == 3) {
                    i.slideDownOut(300);
                } else if (MasAdView.n == 4) {
                    i.slideUpOut(300);
                }
            }
            AdContainer adContainer = (AdContainer) this.b.get();
            if (masAdView != null && adContainer != null) {
                if (masAdView.f == null || masAdView.f.getParent() == null) {
                    masAdView.isRefreshed = false;
                } else {
                    masAdView.isRefreshed = true;
                }
                masAdView.removeAllViews();
                masAdView.addView(adContainer);
                if (this.c == 0) {
                    if (this.d) {
                        MasAdView.a(masAdView, adContainer);
                    } else {
                        MasAdView.a(adContainer);
                    }
                }
                if (MasAdView.n == 2) {
                    adContainer.fadeIn(300);
                } else if (MasAdView.n == 3) {
                    adContainer.slideDownIn(300);
                } else if (MasAdView.n == 4) {
                    adContainer.slideUpIn(300);
                }
                masAdView.setClickable(true);
                MasAdView.b(masAdView, adContainer);
                MasAdView.a(masAdView, adContainer.getAdProxyInstance());
            }
        } catch (Exception e2) {
            Log.e("MasAdView", "Unhandled exception placing AdContainer into AdView.", e2);
            MasAdView masAdView2 = (MasAdView) this.a.get();
            if (masAdView2 != null) {
                MasAdView.a(masAdView2);
            }
        }
    }
}
