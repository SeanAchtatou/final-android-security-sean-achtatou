package mediba.ad.sdk.android;

import android.os.Handler;
import android.view.View;
import android.view.animation.ScaleAnimation;

final class f implements View.OnClickListener {
    /* access modifiers changed from: private */
    public /* synthetic */ AdContainer a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    f(AdContainer adContainer, String str, String str2) {
        this.a = adContainer;
        this.b = str;
        this.c = str2;
    }

    public final void onClick(View view) {
        this.a.g.setClickable(false);
        if (AdContainer.n.getVisibility() != 0) {
            Handler handler = new Handler();
            handler.post(new g(this));
            handler.postDelayed(new h(this, this.b, this.c), 500);
            handler.postDelayed(new i(this), 1000);
            return;
        }
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.2f, 0.0f, 1.2f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        AdContainer.n.startAnimation(scaleAnimation);
        AdContainer.n.setVisibility(4);
        Handler handler2 = new Handler();
        handler2.postDelayed(new j(this), 600);
        handler2.postDelayed(new k(this, this.b, this.c), 1000);
        handler2.postDelayed(new l(this), 2000);
    }
}
