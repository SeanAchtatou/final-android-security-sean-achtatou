package mediba.ad.sdk.android;

import java.util.Map;

public final class AdProxyConnectorFactory {
    public static AdProxyConnector a(String str, String str2, String str3) {
        return a(str, str2, str3, null);
    }

    public static AdProxyConnector a(String str, String str2, String str3, AdProxyConnectorListener adProxyConnectorListener) {
        return a(str, str2, str3, adProxyConnectorListener, 5000, null, null);
    }

    public static AdProxyConnector a(String str, String str2, String str3, AdProxyConnectorListener adProxyConnectorListener, int i) {
        AdProxyConnector a = a(str, null, str3, adProxyConnectorListener, 5000, null, null);
        if (a != null) {
            a.setMaxRetry(1);
        }
        return a;
    }

    public static AdProxyConnector a(String str, String str2, String str3, AdProxyConnectorListener adProxyConnectorListener, int i, Map map, String str4) {
        return new AdProxyURLConnector(str, str2, str3, adProxyConnectorListener, i, null, str4);
    }
}
