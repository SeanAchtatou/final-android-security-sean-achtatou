package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class m implements Runnable {
    private WeakReference a;

    public m(AdContainer adContainer) {
        this.a = new WeakReference(adContainer);
    }

    public final void run() {
        Log.d("AdContainer", "Adcontainer.c");
        try {
            AdContainer adContainer = (AdContainer) this.a.get();
            if (adContainer != null) {
                adContainer.swapProgressBar();
            }
        } catch (Exception e) {
        }
    }
}
