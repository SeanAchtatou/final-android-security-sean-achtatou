package mediba.ad.sdk.android;

import android.os.Build;
import android.util.Log;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class AdProxyConnector implements Runnable {
    private static Executor a = null;
    private static String b;
    protected AdProxyConnectorListener adproxyConnectorListener;
    protected byte[] buffer;
    protected Exception c = null;
    protected Map d;
    private String e;
    protected String g;
    protected boolean k;
    protected int max_retry;
    protected String request_header;
    protected String request_query;
    protected int retry_count;
    protected int timeout;
    protected URL url;

    protected AdProxyConnector(String str, String str2, AdProxyConnectorListener adProxyConnectorListener, int i, Map map, String str3) {
        this.e = str;
        this.g = str2;
        this.adproxyConnectorListener = adProxyConnectorListener;
        this.timeout = i;
        this.d = map;
        this.k = true;
        this.retry_count = 0;
        this.max_retry = 3;
        if (str3 != null) {
            this.request_query = str3;
            this.request_header = "application/x-www-form-urlencoded";
            return;
        }
        this.request_query = null;
        this.request_header = null;
    }

    public static String user_agent() {
        if (b == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            String language = Locale.getDefault().getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                if (Locale.getDefault().getCountry() != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(Locale.getDefault().getCountry().toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            b = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1", stringBuffer);
            Log.d("AdProxyConnector", "user-agent :  " + b);
        }
        return b;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.request_header = str;
    }

    public abstract boolean connect();

    public byte[] d() {
        return this.buffer;
    }

    public abstract void disconnect();

    public final String e() {
        return this.e;
    }

    public final URL f() {
        return this.url;
    }

    public final void g() {
        if (a == null) {
            a = Executors.newCachedThreadPool();
        }
        a.execute(this);
    }

    public final void setMaxRetry(int i) {
        this.max_retry = i;
    }
}
