package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class r implements Runnable {
    private AdContainer a;
    private WeakReference b;

    public r(AdContainer adContainer, AdProxy adProxy) {
        this.a = adContainer;
        this.b = new WeakReference(adProxy);
    }

    public final void run() {
        try {
            if (this.a != null) {
                this.a.setPadding(0, 0, 0, 0);
                this.a.invalidate();
                this.a.requestLayout();
            } else {
                Log.e("AdProxy", "コンテナデータがありません");
            }
            AdProxy adProxy = (AdProxy) this.b.get();
            if (adProxy != null) {
                AdProxy.a(adProxy);
            }
        } catch (Exception e) {
            Log.e("AdProxy", "exception caught in AdProxy Viewadd, " + e.getMessage());
        }
    }
}
