package mediba.ad.sdk.android;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONException;

public class AdRequestThread extends Thread {
    private WeakReference a;

    public AdRequestThread(MasAdView masAdView) {
        this.a = new WeakReference(masAdView);
    }

    public void run() {
        Log.d("AdRequestThread", "Starting AdRequestThread");
        MasAdView masAdView = (MasAdView) this.a.get();
        Context context = masAdView.getContext();
        AdContainer adContainer = new AdContainer(null, context, masAdView);
        int measuredWidth = (int) (((float) masAdView.getMeasuredWidth()) / AdContainer.b());
        if (masAdView.getMeasuredWidth() <= 0 || ((float) measuredWidth) > 310.0f) {
            try {
                if (s.a(MasAdView.d(masAdView), context, masAdView.getPrimaryTextColor(), masAdView.getSecondaryTextColor(), masAdView.getBackgroundColor(), adContainer, measuredWidth) == null) {
                    MasAdView.a(masAdView);
                }
            } catch (JSONException e) {
                Log.e("AdRequestThread", "JSONException caught in AdRequestThread.run " + e.getMessage());
            } catch (Exception e2) {
                Log.e("AdRequestThread", "Unhandled exception caught in AdRequestThread.run " + e2.getMessage());
            }
        } else {
            Log.w("MasAdView", "表示領域の横幅が狭すぎます。");
        }
        MasAdView.e(masAdView);
        MasAdView.f(masAdView);
    }
}
