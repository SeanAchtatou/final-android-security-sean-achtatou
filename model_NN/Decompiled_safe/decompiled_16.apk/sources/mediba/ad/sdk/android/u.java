package mediba.ad.sdk.android;

import android.util.Log;
import java.lang.ref.WeakReference;

final class u implements Runnable {
    private WeakReference a;

    public u(MasAdView masAdView) {
        this.a = new WeakReference(masAdView);
    }

    public final void run() {
        Log.w("MasAdView", "AdFailed");
        MasAdView masAdView = (MasAdView) this.a.get();
        if (masAdView != null) {
            if (MasAdView.h(masAdView) == null || MasAdView.h(masAdView).getParent() == null) {
                masAdView.isRefreshed = false;
            } else {
                masAdView.isRefreshed = true;
            }
            try {
                MasAdView.a(false);
                AdUtil.onFailedToReceiveAd(masAdView);
            } catch (Exception e) {
                Log.w("MasAdView", "Unhandled exception raised in your AdListener.onFailedToReceiveAd." + e);
            }
        }
    }
}
