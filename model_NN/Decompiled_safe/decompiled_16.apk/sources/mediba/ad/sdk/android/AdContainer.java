package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import mediba.ad.sdk.android.AdProxy;

public class AdContainer extends RelativeLayout implements AdProxy.b {
    public static final String IMAGE_PATH = "res/grab.jpg";
    private static float b = -1.0f;
    public static Intent intent;
    /* access modifiers changed from: private */
    public static ImageView n;
    /* access modifiers changed from: private */
    public static ProgressBar p;
    /* access modifiers changed from: private */
    public static Context r;
    private final MasAdView a;
    private View c;
    /* access modifiers changed from: private */
    public ProgressBar d;
    private boolean e;
    private Long f = -1L;
    /* access modifiers changed from: private */
    public RelativeLayout g;
    private TextView h;
    private TextView i;
    private TextView j;
    private ImageView k;
    private ImageView l;
    private ImageView m;
    protected AdProxy mAdproxy;
    private LinearLayout o;
    /* access modifiers changed from: private */
    public ImageView q;

    static {
        Log.d("AdContainer", "Activate AdContainer");
    }

    public AdContainer(AdProxy adProxy, Context context, MasAdView masAdView) {
        super(context);
        Bitmap imageBitmap;
        Bitmap imageBitmap2;
        r = context;
        setFocusable(false);
        setFocusableInTouchMode(false);
        Log.d("AdContainer", "AdContainer Construct");
        this.f = -1L;
        this.a = masAdView;
        setId(1);
        b = getResources().getDisplayMetrics().density;
        setBackgroundDrawable(context.getResources().getDrawable(17301602));
        int i2 = (int) (48.0f * b);
        int i3 = (int) (5.0f * b);
        int i4 = (int) (8.0f * b);
        int screenWidth = MasAdManager.getScreenWidth(context);
        MasAdManager.getScreenHeight(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.q = new ImageView(context);
        this.q.setScaleType(ImageView.ScaleType.FIT_CENTER);
        this.g = new RelativeLayout(context);
        this.g.setBackgroundColor(this.a.getBackgroundColor());
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i2, i2);
        this.k = new ImageView(context);
        this.k.setScaleType(ImageView.ScaleType.FIT_XY);
        this.k.setPadding(i3, i3, i3, i3);
        this.k.setId(10);
        this.g.addView(this.k, layoutParams2);
        if (this.a.getBackgroundColor() == 2) {
            imageBitmap = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_left.png"));
            imageBitmap2 = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_right.png"));
        } else {
            imageBitmap = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_left.png"));
            imageBitmap2 = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_right.png"));
        }
        this.o = new LinearLayout(context);
        this.o.setOrientation(0);
        this.l = new ImageView(context);
        this.l.setImageBitmap(imageBitmap);
        this.l.setScaleType(ImageView.ScaleType.FIT_XY);
        this.l.setLayoutParams(new Gallery.LayoutParams(i2, i2));
        this.m = new ImageView(context);
        this.m.setImageBitmap(imageBitmap2);
        this.m.setScaleType(ImageView.ScaleType.FIT_XY);
        this.m.setLayoutParams(new Gallery.LayoutParams(screenWidth, -1));
        this.o.addView(this.l);
        this.o.addView(this.m);
        this.g.addView(this.o);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i2, i2);
        layoutParams3.addRule(11);
        ProgressBar progressBar = new ProgressBar(context);
        p = progressBar;
        progressBar.setPadding(i4, i4, i4, i4);
        p.setMax(2);
        p.incrementProgressBy(1);
        p.setVisibility(4);
        this.g.addView(p, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(i2, i2);
        layoutParams4.addRule(11);
        ImageView imageView = new ImageView(context);
        n = imageView;
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        n.setPadding(i3, i3, i3, i3);
        n.setId(13);
        this.g.addView(n, layoutParams4);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams5.setMargins(0, (int) (5.0f * b), 0, -((int) (3.0f * b)));
        layoutParams5.addRule(1, 10);
        layoutParams6.addRule(1, 10);
        layoutParams6.addRule(3, 11);
        layoutParams6.setMargins(0, 0, 0, -((int) (5.0f * b)));
        this.h = new TextView(context);
        this.h.setId(11);
        this.h.setTextColor(this.a.getPrimaryTextColor());
        this.i = new TextView(context);
        this.i.setId(12);
        this.i.setTextColor(this.a.getSecondaryTextColor());
        this.g.addView(this.h, layoutParams5);
        this.g.addView(this.i, layoutParams6);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams7.addRule(3, 12);
        layoutParams7.addRule(11);
        this.j = new TextView(context);
        this.j.setTextSize(10.0f);
        this.j.setText("Ads by mediba");
        if (this.a.getBackgroundColor() == 1) {
            this.j.setTextColor(Color.rgb(153, 153, 153));
        } else if (this.a.getBackgroundColor() == 2) {
            this.j.setTextColor(Color.rgb(102, 102, 102));
        }
        this.g.addView(this.j, layoutParams7);
        addView(this.g);
        addView(this.q, layoutParams);
        a((AdProxy) null);
    }

    private Drawable a(String str) {
        try {
            return Drawable.createFromStream((InputStream) fetch(str), "src");
        } catch (MalformedURLException e2) {
            Log.e("AdContainer", "MalformedURLException caught in AdContainer.ImageOperations(), " + e2.getMessage());
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            Log.e("AdContainer", "IOException caught in AdContainer.ImageOperations(), " + e3.getMessage());
            e3.printStackTrace();
            return null;
        } catch (Exception e4) {
            e4.printStackTrace();
            Log.e("AdContainer", "exception caught in AdContainer.ImageOperations(), " + e4.getMessage());
            return null;
        }
    }

    static float b() {
        return b;
    }

    public static Context getAdContext() {
        Log.d("AdContainer", "context:" + r.toString());
        return r;
    }

    protected static void setActionIconVisibility(int i2) {
        if (i2 == 0) {
            n.setVisibility(0);
        } else if (i2 == 4) {
            n.setVisibility(4);
        }
    }

    protected static void setProgressbarVisibility(int i2) {
        if (i2 == 0) {
            p.setVisibility(0);
        } else if (i2 == 4) {
            p.setVisibility(4);
        }
    }

    public void a() {
        post(new m(this));
    }

    /* access modifiers changed from: package-private */
    public final void a(AdProxy adProxy) {
        this.mAdproxy = adProxy;
        if (adProxy == null) {
            setClickable(false);
            return;
        }
        adProxy.setAdContainer(this);
        setFocusable(true);
        setClickable(true);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                setPressed(true);
                break;
            case 1:
                setPressed(false);
                break;
        }
        return super.onTrackballEvent(motionEvent);
    }

    public void fadeIn(int i2) {
        setVisibility(0);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    public void fadeOut(int i2) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    public Object fetch(String str) {
        Object content = new URL(str).getContent();
        Log.d("AdContainer", "fetching:" + str);
        return content;
    }

    public final long g() {
        long uptimeMillis = SystemClock.uptimeMillis() - this.f.longValue();
        if (this.f.longValue() < 0 || uptimeMillis < 0 || uptimeMillis > 10000000) {
            return 0;
        }
        return uptimeMillis;
    }

    /* access modifiers changed from: protected */
    public final AdProxy getAdProxyInstance() {
        return this.mAdproxy;
    }

    public Bitmap getImageBitmap(String str) {
        Exception e2;
        Bitmap bitmap;
        IOException e3;
        MalformedURLException e4;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(((HttpURLConnection) new URL(str).openConnection()).getInputStream(), 1048576);
            bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
            try {
                bufferedInputStream.close();
            } catch (MalformedURLException e5) {
                e4 = e5;
                Log.e("AdContainer", "MalformedURLException caught in AdContainer.getImageBitmap(), " + e4.getMessage());
                e4.printStackTrace();
                return bitmap;
            } catch (IOException e6) {
                e3 = e6;
                Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e3.getMessage());
                e3.printStackTrace();
                return bitmap;
            } catch (Exception e7) {
                e2 = e7;
                Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
                e2.printStackTrace();
                return bitmap;
            }
        } catch (MalformedURLException e8) {
            MalformedURLException malformedURLException = e8;
            bitmap = null;
            e4 = malformedURLException;
        } catch (IOException e9) {
            IOException iOException = e9;
            bitmap = null;
            e3 = iOException;
            Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e3.getMessage());
            e3.printStackTrace();
            return bitmap;
        } catch (Exception e10) {
            Exception exc = e10;
            bitmap = null;
            e2 = exc;
            Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
            e2.printStackTrace();
            return bitmap;
        }
        return bitmap;
    }

    public Bitmap getImageBitmap(URL url) {
        Exception e2;
        Bitmap bitmap;
        IOException e3;
        MalformedURLException e4;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
            bitmap = BitmapFactory.decodeStream(bufferedInputStream, null, new BitmapFactory.Options());
            try {
                bufferedInputStream.close();
            } catch (MalformedURLException e5) {
                e4 = e5;
                Log.e("AdContainer", "MalformedURLException caught in AdContainer.getImageBitmap(), " + e4.getMessage());
                e4.printStackTrace();
                return bitmap;
            } catch (IOException e6) {
                e3 = e6;
                Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e3.getMessage());
                e3.printStackTrace();
                return bitmap;
            } catch (Exception e7) {
                e2 = e7;
                Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
                e2.printStackTrace();
                return bitmap;
            }
        } catch (MalformedURLException e8) {
            MalformedURLException malformedURLException = e8;
            bitmap = null;
            e4 = malformedURLException;
        } catch (IOException e9) {
            IOException iOException = e9;
            bitmap = null;
            e3 = iOException;
            Log.e("AdContainer", "IOException caught in AdContainer.getImageBitmap(), " + e3.getMessage());
            e3.printStackTrace();
            return bitmap;
        } catch (Exception e10) {
            Exception exc = e10;
            bitmap = null;
            e2 = exc;
            Log.e("AdContainer", "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
            e2.printStackTrace();
            return bitmap;
        }
        return bitmap;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.f.longValue() != -1) {
            this.f = Long.valueOf(SystemClock.uptimeMillis());
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        setPressed(false);
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        setPressed(true);
        return super.onKeyUp(i2, keyEvent);
    }

    public void setIconLink(String str, String str2) {
        this.g.setOnTouchListener(new e(this));
        this.g.setOnClickListener(new f(this, str2, str));
    }

    public void setIconView(String str, String str2, String str3, String str4) {
        this.h.setTextSize(12.0f);
        this.i.setTextSize(12.0f);
        this.h.setText(str);
        this.i.setText(str2);
        getContext();
        Drawable a2 = a(str3);
        getContext();
        Drawable a3 = a(str4);
        if (a2 != null && a3 != null) {
            this.k.setImageDrawable(a2);
            n.setImageDrawable(a3);
            this.j.setPadding(0, 0, (int) (48.0f * b), 0);
        } else if (a2 != null && a3 == null) {
            this.k.setImageDrawable(a2);
            n.setVisibility(4);
            int i2 = (int) (b * 14.0f);
            p.setPadding(i2, i2, i2, i2);
            this.j.setPadding(0, 0, (int) (b * 8.0f), 0);
        } else if (a2 == null && a3 != null) {
            this.k.setImageDrawable(a3);
            n.setVisibility(4);
            int i3 = (int) (b * 14.0f);
            p.setPadding(i3, i3, i3, i3);
            this.j.setPadding(0, 0, (int) (b * 8.0f), 0);
        }
        this.q.setVisibility(4);
        this.g.setVisibility(0);
        invalidate();
    }

    public void setImageLink(String str, String str2) {
        this.q.setOnTouchListener(new a(this));
        this.q.setOnClickListener(new b(this, str2, str));
    }

    public void setImageView(String str) {
        ImageView imageView = this.q;
        getContext();
        imageView.setImageDrawable(a(str));
        this.g.setVisibility(4);
        this.q.setVisibility(0);
        invalidate();
    }

    public final void setPressed(boolean z) {
        if (isPressed() != z) {
            if (AdProxy.intent != null) {
                getContext().startActivity(AdProxy.intent);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    public void setView(View view, RelativeLayout.LayoutParams layoutParams) {
        Log.d("AdContainer", "setView");
        if (view == null || view == this.c) {
            Log.d("AdContainer", "return");
            return;
        }
        this.c = view;
        this.d = new ProgressBar(getContext());
        this.d.setIndeterminate(true);
        this.d.setId(2);
        if (layoutParams != null) {
            this.d.setLayoutParams(layoutParams);
        }
        this.d.setVisibility(4);
        Log.d("AdContainer", "Loading");
        post(new n(this));
    }

    public void slideDownIn(int i2) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        translateAnimation.setDuration((long) i2);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public void slideDownOut(int i2) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        translateAnimation.setDuration((long) i2);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public void slideUpIn(int i2) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration((long) i2);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    public void slideUpOut(int i2) {
        AnimationSet animationSet = new AnimationSet(true);
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        translateAnimation.setDuration((long) i2);
        translateAnimation.startNow();
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(translateAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) i2);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        animationSet.addAnimation(alphaAnimation);
        startAnimation(animationSet);
    }

    /* access modifiers changed from: protected */
    public final void swapProgressBar() {
        Log.d("AdContainer", "SwapProgress");
        this.e = false;
        if (this.d != null) {
            this.d.setVisibility(4);
        }
        if (this.c != null) {
            this.c.setVisibility(0);
        }
    }
}
