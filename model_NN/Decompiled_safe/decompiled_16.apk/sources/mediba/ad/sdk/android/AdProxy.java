package mediba.ad.sdk.android;

import android.content.Intent;
import android.graphics.Rect;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.Vector;

public class AdProxy implements View.OnClickListener, AdProxyConnectorListener {
    private static String b;
    private static String c;
    private static String d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    public static Intent intent;
    protected View a;
    private int i = -1;
    private int j = 48;
    private a k = null;
    private Hashtable l = new Hashtable();
    private double m;
    protected AdContainer mAdcontainer;
    private double n;

    public class a {
        private WeakReference a;

        public a(MasAdView masAdView) {
            this.a = new WeakReference(masAdView);
        }

        public void a() {
            MasAdView masAdView = (MasAdView) this.a.get();
            if (masAdView != null) {
                MasAdView.a(masAdView);
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(AdProxy adProxy) {
            MasAdView masAdView = (MasAdView) this.a.get();
            if (masAdView != null) {
                synchronized (masAdView) {
                    try {
                        if (MasAdView.h(masAdView) == null || !adProxy.equals(MasAdView.h(masAdView).getAdProxyInstance())) {
                            Log.i("AdProxy", "Ad Response (" + (SystemClock.uptimeMillis() - MasAdView.g(masAdView)) + " ms)");
                            masAdView.getContext();
                            masAdView.a(adProxy, adProxy.getAdContainer());
                        } else {
                            Log.d("AdProxy", "Same AD SKIP");
                        }
                    } catch (Exception e) {
                        Log.e("AdProxy", "Handler Illeguler in AdProxy.a(AdProxy adproxy) " + e.getMessage());
                    }
                }
                return;
            }
            return;
        }
    }

    public interface b {
        void a();
    }

    protected AdProxy() {
        new Vector();
        this.m = -1.0d;
        this.n = -1.0d;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static mediba.ad.sdk.android.AdProxy a(mediba.ad.sdk.android.AdProxy.a r5, java.lang.String r6, java.lang.String[] r7, int r8, int r9, int r10, mediba.ad.sdk.android.AdContainer r11) {
        /*
            r0 = 0
            mediba.ad.sdk.android.AdProxy r1 = new mediba.ad.sdk.android.AdProxy
            r1.<init>()
            r1.k = r5
            r1.mAdcontainer = r11
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>(r6)
            int r3 = r2.length()
            r4 = 6
            if (r3 == r4) goto L_0x0025
            int r3 = r2.length()
            r4 = 3
            if (r3 == r4) goto L_0x0025
            java.lang.String r1 = "AdProxy"
            java.lang.String r2 = "不正なADを所得しました。"
            android.util.Log.w(r1, r2)
        L_0x0024:
            return r0
        L_0x0025:
            java.lang.String r3 = "type"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.b = r3
            float r3 = mediba.ad.sdk.android.AdContainer.b()
            double r3 = (double) r3
            r1.n = r3
            mediba.ad.sdk.android.AdProxy.c = r0
            mediba.ad.sdk.android.AdProxy.d = r0
            mediba.ad.sdk.android.AdProxy.e = r0
            mediba.ad.sdk.android.AdProxy.f = r0
            mediba.ad.sdk.android.AdProxy.g = r0
            mediba.ad.sdk.android.AdProxy.h = r0
            java.lang.String r3 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r4 = "1"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0058
            java.lang.String r3 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r4 = "3"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0078
        L_0x0058:
            java.lang.String r3 = "imageurl"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.f = r3
            java.lang.String r3 = "clickurl"
            java.lang.String r2 = r2.getString(r3)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.c = r2
        L_0x0070:
            r1.c()
            r2 = 1
        L_0x0074:
            if (r2 == 0) goto L_0x0024
            r0 = r1
            goto L_0x0024
        L_0x0078:
            java.lang.String r3 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r4 = "2"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x008c
            java.lang.String r3 = mediba.ad.sdk.android.AdProxy.b
            java.lang.String r4 = "4"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00c9
        L_0x008c:
            java.lang.String r3 = "icon1url"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.g = r3
            java.lang.String r3 = "icon2url"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.h = r3
            java.lang.String r3 = "text1"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.d = r3
            java.lang.String r3 = "text2"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r3 = r3.toString()
            mediba.ad.sdk.android.AdProxy.e = r3
            java.lang.String r3 = "clickurl"
            java.lang.String r2 = r2.getString(r3)
            java.lang.String r2 = r2.toString()
            mediba.ad.sdk.android.AdProxy.c = r2
            goto L_0x0070
        L_0x00c9:
            java.lang.String r2 = "AdProxy"
            java.lang.String r3 = "不正なADを所得しました。"
            android.util.Log.w(r2, r3)
            r2 = 0
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.AdProxy.a(mediba.ad.sdk.android.AdProxy$a, java.lang.String, java.lang.String[], int, int, int, mediba.ad.sdk.android.AdContainer):mediba.ad.sdk.android.AdProxy");
    }

    static void a(AdProxy adProxy) {
        if (adProxy.k != null) {
            adProxy.k.a(adProxy);
        }
    }

    private void c() {
        try {
            r rVar = new r(this.mAdcontainer, this);
            d();
            MasAdView.a.post(rVar);
            if (this.l != null) {
                this.l.clear();
                this.l = null;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.constructView ");
        }
    }

    private boolean d() {
        new Rect(50, 50, 50, 50);
        try {
            this.mAdcontainer.getContext();
            if (b.equals("1")) {
                this.mAdcontainer.setImageView(f);
                this.mAdcontainer.setImageLink(c, "nomal");
                return true;
            } else if (b.equals("2")) {
                this.mAdcontainer.setIconView(d, e, g, h);
                this.mAdcontainer.setIconLink(c, "nomal");
                return true;
            } else if (b.equals("3")) {
                this.mAdcontainer.setImageView(f);
                this.mAdcontainer.setImageLink(c, "overlay");
                return true;
            } else if (!b.equals("4")) {
                return true;
            } else {
                this.mAdcontainer.setIconView(d, e, g, h);
                this.mAdcontainer.setIconLink(c, "overlay");
                return true;
            }
        } catch (Exception e2) {
            Log.e("AdProxy", "Handler Illeguler in AdProxy.createInnerView " + e2.getMessage());
            return true;
        }
    }

    private void f() {
        if (this.l != null) {
            this.l.clear();
            this.l = null;
        }
        if (this.k != null) {
            this.k.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final double a() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final int a(int i2) {
        double d2 = (double) i2;
        if (this.n > 0.0d) {
            d2 *= this.n;
        }
        return (int) d2;
    }

    public void a(AdProxyConnector adProxyConnector) {
        String e2 = adProxyConnector.e();
        byte[] d2 = adProxyConnector.d();
        if (d2 != null) {
            this.l.put(e2, d2);
            c();
            return;
        }
        Log.d("AdProxy", "Failed reading asset(" + e2 + ") for ad");
        f();
    }

    public void a(AdProxyConnector adProxyConnector, Exception exc) {
        f();
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return false;
    }

    public final int e() {
        return this.i;
    }

    public final AdContainer getAdContainer() {
        return this.mAdcontainer;
    }

    public final int getContainerHeight() {
        return this.j;
    }

    public final void onClick(View view) {
        Log.d("AdProxy", "View Clicked");
    }

    public final void setAdContainer(AdContainer adContainer) {
        this.mAdcontainer = adContainer;
    }
}
