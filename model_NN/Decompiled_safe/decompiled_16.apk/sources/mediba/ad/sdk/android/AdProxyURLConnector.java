package mediba.ad.sdk.android;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class AdProxyURLConnector extends AdProxyConnector {
    private URL a;
    private HttpURLConnection b;

    public AdProxyURLConnector(String str, String str2, String str3, AdProxyConnectorListener adProxyConnectorListener, int i, Map map, String str4) {
        super(str2, str3, adProxyConnectorListener, i, map, str4);
        try {
            Log.d("AdProxyURLConnector", str);
            this.a = new URL(String.valueOf(str) + "?" + str4);
            this.url = this.a;
        } catch (Exception e) {
            this.a = null;
            this.c = e;
            Log.e("AdProxyURLConnector", "exception caught in AdProxyURLConnector " + e.getMessage());
        }
        this.b = null;
        this.retry_count = 0;
    }

    private void a() {
        if (this.b != null) {
            this.b.disconnect();
            this.b = null;
        }
    }

    /* JADX INFO: finally extract failed */
    public final boolean connect() {
        boolean z;
        boolean z2;
        String str;
        if (this.a == null) {
            if (this.adproxyConnectorListener != null) {
                this.adproxyConnectorListener.a(this, new Exception("url was null"));
            }
            z = false;
        } else {
            Log.d("AdProxyURLConnector", "URLConn");
            HttpURLConnection.setFollowRedirects(true);
            z = false;
            while (this.retry_count < this.max_retry && !z) {
                Log.v("AdProxyURLConnector", "attempt " + this.retry_count + " to connect to url " + this.a);
                try {
                    a();
                    this.b = (HttpURLConnection) this.a.openConnection();
                    if (this.b != null) {
                        this.b.setRequestProperty("User-Agent", user_agent());
                        this.b.setConnectTimeout(this.timeout);
                        this.b.setReadTimeout(this.timeout);
                        if (this.d != null) {
                            for (String str2 : this.d.keySet()) {
                                if (!(str2 == null || (str = (String) this.d.get(str2)) == null)) {
                                    this.b.addRequestProperty(str2, str);
                                }
                            }
                        }
                        this.b.connect();
                        int responseCode = this.b.getResponseCode();
                        if (responseCode >= 200 && responseCode < 300) {
                            this.url = this.b.getURL();
                            if (this.k) {
                                BufferedInputStream bufferedInputStream = new BufferedInputStream(this.b.getInputStream(), 4096);
                                byte[] bArr = new byte[4096];
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(4096);
                                while (true) {
                                    int read = bufferedInputStream.read(bArr);
                                    if (read == -1) {
                                        break;
                                    }
                                    byteArrayOutputStream.write(bArr, 0, read);
                                }
                                this.buffer = byteArrayOutputStream.toByteArray();
                            }
                            if (this.adproxyConnectorListener != null) {
                                this.adproxyConnectorListener.a(this);
                            }
                            z2 = true;
                            a();
                            a();
                            this.retry_count++;
                            z = z2;
                        }
                    }
                    z2 = z;
                    a();
                    a();
                } catch (Exception e) {
                    Log.w("AdProxyURLConnector", "could not open connection to url " + this.a, e);
                    this.c = e;
                    a();
                    z2 = false;
                } catch (Throwable th) {
                    a();
                    throw th;
                }
                this.retry_count++;
                z = z2;
            }
        }
        if (this.adproxyConnectorListener != null && z) {
            this.adproxyConnectorListener.a(this, this.c);
        }
        return z;
    }

    public final void disconnect() {
        a();
        this.adproxyConnectorListener = null;
    }

    public final void run() {
        try {
            connect();
        } catch (Exception e) {
            if (Log.isLoggable("AdProxyURLConnector", 6)) {
                Log.e("AdProxyURLConnector", "exception caught in AdProxyURLConnector.run(), " + e.getMessage());
            }
        }
    }
}
