package mediba.ad.sdk.android;

import android.view.MotionEvent;
import android.view.View;

final class e implements View.OnTouchListener {
    private /* synthetic */ AdContainer a;

    e(AdContainer adContainer) {
        this.a = adContainer;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.a.setPressed(true);
                break;
            case 1:
                if (this.a.g.isPressed()) {
                    this.a.g.performClick();
                }
                this.a.setPressed(false);
                break;
        }
        return false;
    }
}
