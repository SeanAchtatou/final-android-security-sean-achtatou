package fjl.oofdskl.slide;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

public final class a {
    public static WindowManager a = null;
    public static FrameLayout b;
    private static a e;
    private static SensorEventListener i;
    private static SensorManager j;
    /* access modifiers changed from: private */
    public Activity c;
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams d = null;
    /* access modifiers changed from: private */
    public SvfDefine f;
    /* access modifiers changed from: private */
    public ImageView g;
    /* access modifiers changed from: private */
    public ImageView h;
    private Sensor k;
    /* access modifiers changed from: private */
    public Configuration l;

    private a(Activity activity) {
        this.c = activity;
        a = (WindowManager) this.c.getApplicationContext().getSystemService("window");
        this.d = new WindowManager.LayoutParams();
        this.d.type = 2002;
        this.d.format = 1;
        this.d.flags = 40;
        this.d.gravity = 17;
        this.d.x = 0;
        this.d.y = 0;
        this.l = this.c.getResources().getConfiguration();
        j = (SensorManager) this.c.getSystemService("sensor");
        this.k = j.getDefaultSensor(1);
        i = new b(this);
        j.registerListener(i, this.k, 0);
        b = new FrameLayout(this.c);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams((r.ae * 4) / 5, r.af);
        this.f = new SvfDefine(b.getContext());
        b.addView(this.f, layoutParams);
        a.addView(b, this.d);
        this.g = new ImageView(this.c);
        this.g.setImageBitmap(o.a("bitmap/1.png", this.c));
        a(this.g);
        this.f.addView(this.g);
        this.f.addView(a(o.a("bitmap/2.png", this.c)));
        this.f.addView(a(o.a("bitmap/3.png", this.c)));
        this.h = new ImageView(this.c);
        this.h.setImageBitmap(o.a("bitmap/4.png", this.c));
        a(this.h);
        this.f.addView(this.h);
    }

    private ImageView a(Bitmap bitmap) {
        ImageView imageView = new ImageView(b.getContext());
        imageView.setImageBitmap(bitmap);
        a(imageView);
        return imageView;
    }

    public static a a(Activity activity) {
        if (e == null) {
            e = new a(activity);
        }
        return e;
    }

    public static void a() {
        j.unregisterListener(i);
        a.removeView(b);
        a = null;
        e = null;
    }

    private void a(ImageView imageView) {
        imageView.setOnTouchListener(new c(this));
    }

    protected static Animation b() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    protected static Animation c() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    protected static Animation d() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }

    protected static Animation e() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        return translateAnimation;
    }
}
