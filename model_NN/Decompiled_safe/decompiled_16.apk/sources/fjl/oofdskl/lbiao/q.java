package fjl.oofdskl.lbiao;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import java.lang.ref.SoftReference;

final class q extends Thread {
    private /* synthetic */ o a;
    private final /* synthetic */ String b;
    private final /* synthetic */ Handler c;

    q(o oVar, String str, Handler handler) {
        this.a = oVar;
        this.b = str;
        this.c = handler;
    }

    public final void run() {
        Drawable a2 = o.a(this.b);
        this.a.a.put(this.b, new SoftReference(a2));
        this.c.sendMessage(this.c.obtainMessage(0, a2));
    }
}
