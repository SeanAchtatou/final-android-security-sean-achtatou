package fjl.oofdskl.lbiao;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class h implements q {
    private Activity a;
    private /* synthetic */ YyLb b;

    public h(YyLb yyLb, Activity activity) {
        this.b = yyLb;
        this.a = activity;
        if (o.b((Context) activity)) {
            try {
                if (yyLb.k.equals("loadApp")) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("para", r.b);
                    if (yyLb.q == 1) {
                        jSONObject.put("url", "flush=0&category=0&pagenumber=" + yyLb.q + "&pagesize=8");
                    } else {
                        jSONObject.put("url", "flush=1&category=0&pagenumber=" + yyLb.q + "&pagesize=8");
                    }
                    r.ai = false;
                    new o(yyLb.getApplication(), this, jSONObject.toString()).start();
                } else if (yyLb.k.equals("loadGame")) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("para", r.b);
                    jSONObject2.put("url", "flush=1&category=1&pagenumber=" + yyLb.q + "&pagesize=8");
                    r.ai = false;
                    new o(yyLb.getApplication(), this, jSONObject2.toString()).start();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this.a, "网络不给力哦……请您连接网络", 1).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, boolean):void
     arg types: [fjl.oofdskl.lbiao.YyLb, int]
     candidates:
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, int):void
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, fjl.oofdskl.lbiao.k):void
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, java.lang.String):void
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, java.util.ArrayList):void
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, java.util.Map):void
      fjl.oofdskl.lbiao.YyLb.a(fjl.oofdskl.lbiao.YyLb, boolean):void */
    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        try {
            JSONArray jSONArray = new JSONArray(obj2);
            int i = jSONArray.getJSONObject(jSONArray.length() - 1).getInt("flag");
            if (i == 0) {
                this.b.s.setVisibility(8);
                this.b.h.setVisibility(0);
                Toast.makeText(context, "加载失败！", 0).show();
                return;
            }
            o.a(this.b, obj2, this.b.k);
            YyLb.a(this.b, o.b(this.b, this.b.k));
            this.b.t = true;
            if (this.b.k.equals("loadApp")) {
                this.b.l = (ArrayList) this.b.r;
            } else if (this.b.k.equals("loadGame")) {
                this.b.m = (ArrayList) this.b.r;
            }
            if (i == 2) {
                YyLb yyLb = this.b;
                yyLb.q = yyLb.q + 1;
                this.b.v.sendEmptyMessage(4);
            } else if (this.b.q == 1) {
                YyLb yyLb2 = this.b;
                yyLb2.q = yyLb2.q + 1;
                this.b.v.sendEmptyMessage(1);
            } else if (this.b.q != 1) {
                YyLb yyLb3 = this.b;
                yyLb3.q = yyLb3.q + 1;
                this.b.v.sendEmptyMessage(3);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            o.a(r.M, "result = " + obj2.toString());
        }
    }
}
