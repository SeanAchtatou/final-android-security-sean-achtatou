package fjl.oofdskl.lbiao;

import android.os.Message;
import fjl.oofdskl.e.a;

final class d implements a {
    private /* synthetic */ c a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    d(c cVar, String str, String str2, String str3) {
        this.a = cVar;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final void a(int i) {
        Message message = new Message();
        message.what = 1;
        message.getData().putInt("size", i);
        message.getData().putString("title", this.b);
        message.getData().putString("mUrl", this.c);
        message.getData().putString("setpck", this.d);
        this.a.a.h.sendMessage(message);
    }
}
