package fjl.oofdskl.lbiao;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import fjl.oofdskl.tools.o;

final class w implements View.OnTouchListener {
    private /* synthetic */ YyXxXs a;

    w(YyXxXs yyXxXs) {
        this.a = yyXxXs;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.lbiao.YyXxXs, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.a.n.setBackgroundDrawable(o.a((Activity) this.a, "button/down02.png"));
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            this.a.n.setBackgroundDrawable(o.a((Activity) this.a, "button/down01.png"));
            return false;
        }
    }
}
