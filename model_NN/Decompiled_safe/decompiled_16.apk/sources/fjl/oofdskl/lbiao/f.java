package fjl.oofdskl.lbiao;

import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;

final class f extends Handler {
    private /* synthetic */ YyLb a;

    f(YyLb yyLb) {
        this.a = yyLb;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.a.g.setVisibility(8);
                if (this.a.q == 2) {
                    this.a.s.setVisibility(8);
                    this.a.h.setVisibility(0);
                    YyLb.e(this.a);
                }
                if (this.a.k.equals("loadApp")) {
                    this.a.i = new k(this.a, this.a.l, this.a.h);
                    this.a.h.setAdapter((ListAdapter) this.a.i);
                    this.a.i.notifyDataSetChanged();
                    return;
                } else if (this.a.k.equals("loadGame")) {
                    this.a.j = new k(this.a, this.a.m, this.a.h);
                    this.a.h.setAdapter((ListAdapter) this.a.j);
                    this.a.j.notifyDataSetChanged();
                    return;
                } else {
                    return;
                }
            case 2:
                this.a.g.setVisibility(0);
                return;
            case 3:
                this.a.g.setVisibility(8);
                if (this.a.k.equals("loadApp")) {
                    this.a.i.notifyDataSetChanged();
                    return;
                } else if (this.a.k.equals("loadGame")) {
                    this.a.j.notifyDataSetChanged();
                    return;
                } else {
                    return;
                }
            case 4:
                if (this.a.k.equals("loadApp")) {
                    if (this.a.i == null) {
                        this.a.i = new k(this.a, this.a.l, this.a.h);
                        this.a.h.setAdapter((ListAdapter) this.a.i);
                    }
                    if (this.a.q == 2) {
                        this.a.s.setVisibility(8);
                        this.a.h.setVisibility(0);
                        YyLb.e(this.a);
                    }
                    this.a.i.notifyDataSetChanged();
                } else if (this.a.k.equals("loadGame")) {
                    if (this.a.j == null) {
                        this.a.j = new k(this.a, this.a.m, this.a.h);
                        this.a.h.setAdapter((ListAdapter) this.a.j);
                    }
                    if (this.a.q == 2) {
                        this.a.s.setVisibility(8);
                        this.a.h.setVisibility(0);
                        YyLb.e(this.a);
                    }
                    this.a.j.notifyDataSetChanged();
                }
                this.a.h.removeFooterView(this.a.g);
                return;
            case 5:
                this.a.u.setVisibility(8);
                return;
            default:
                return;
        }
    }
}
