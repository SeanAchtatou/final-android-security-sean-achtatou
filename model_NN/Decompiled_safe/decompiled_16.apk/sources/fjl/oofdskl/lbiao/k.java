package fjl.oofdskl.lbiao;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public final class k extends BaseAdapter {
    /* access modifiers changed from: private */
    public Activity a;
    private List b = null;
    /* access modifiers changed from: private */
    public Drawable c;
    /* access modifiers changed from: private */
    public Drawable d;
    /* access modifiers changed from: private */
    public Drawable e;
    /* access modifiers changed from: private */
    public Drawable f;
    /* access modifiers changed from: private */
    public Drawable g;
    /* access modifiers changed from: private */
    public Drawable h;
    /* access modifiers changed from: private */
    public Drawable i;
    /* access modifiers changed from: private */
    public Drawable j;
    /* access modifiers changed from: private */
    public List k;
    /* access modifiers changed from: private */
    public List l;
    /* access modifiers changed from: private */
    public int m = 0;
    /* access modifiers changed from: private */
    public int n = 0;
    private o o;
    /* access modifiers changed from: private */
    public ListView p;
    private n q;

    public k(Activity activity, List list, ListView listView) {
        this.a = activity;
        this.b = list;
        this.k = new ArrayList();
        this.l = new ArrayList();
        this.p = listView;
        this.o = new o();
        this.c = o.a(this.a, "button/upgrate.png");
        this.d = o.a(this.a, "button/upgrateclk.png");
        this.e = o.a(this.a, "button/upgrate1.png");
        this.f = o.a(this.a, "button/updateok.png");
        this.g = o.a(this.a, "button/install.png");
        this.h = o.a(this.a, "button/installclk.png");
        this.i = o.a(this.a, "button/install0.png");
        this.j = o.a(this.a, "button/installok.png");
        this.q = new n(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("downloadinstallfromscreen");
        intentFilter.addAction("installupgratefinish");
        this.a.registerReceiver(this.q, intentFilter);
    }

    static /* synthetic */ void a(k kVar, View view, String str) {
        int parseInt = Integer.parseInt(view.getTag().toString());
        if (!o.b((Context) kVar.a)) {
            Toast.makeText(kVar.a, "网络不给力哦……请您连接网络", 1).show();
        } else {
            Intent intent = new Intent(kVar.a, YyFwQq.class);
            intent.putExtra("service_flag", "down");
            intent.putExtra("upgrateFlag", "yes");
            intent.putExtra("packagename", (String) ((Map) kVar.b.get(parseInt)).get("setpkg"));
            intent.putExtra("Duil", (String) ((Map) kVar.b.get(parseInt)).get("apk"));
            intent.putExtra("Title", (String) ((Map) kVar.b.get(parseInt)).get("title"));
            kVar.a.startService(intent);
        }
        if (str.equals("yes")) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("para", r.c);
                jSONObject.put("url", "warepck=" + ((String) ((Map) kVar.b.get(parseInt)).get("setpkg")) + "&operate=2");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            r.ai = false;
            new o(kVar.a, jSONObject.toString()).start();
        }
        kVar.m++;
        HashMap hashMap = new HashMap();
        hashMap.put("position", Integer.valueOf(parseInt));
        hashMap.put("back", view.getBackground());
        hashMap.put("packagename", (String) ((Map) kVar.b.get(parseInt)).get("setpkg"));
        hashMap.put("btn", view);
        kVar.k.add(hashMap);
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i2) {
        return this.b.get(i2);
    }

    public final long getItemId(int i2) {
        return (long) i2;
    }

    public final View getView(int i2, View view, ViewGroup viewGroup) {
        m mVar;
        ViewGroup viewGroup2;
        int i3 = 0;
        String str = (String) ((Map) this.b.get(i2)).get("logo");
        if (view == null) {
            m mVar2 = new m(this);
            viewGroup2 = new a(this.a);
            mVar2.a = (ImageView) viewGroup2.getChildAt(0);
            mVar2.b = (TextView) ((ViewGroup) viewGroup2.getChildAt(1)).getChildAt(0);
            mVar2.c = (TextView) ((ViewGroup) viewGroup2.getChildAt(1)).getChildAt(1);
            mVar2.d = (TextView) ((ViewGroup) viewGroup2.getChildAt(1)).getChildAt(2);
            mVar2.e = (ImageButton) viewGroup2.getChildAt(2);
            viewGroup2.setTag(mVar2);
            mVar = mVar2;
        } else {
            mVar = (m) view.getTag();
            viewGroup2 = view;
        }
        mVar.a.setTag(str);
        Drawable a2 = this.o.a(str, new r(this));
        if (a2 == null) {
            mVar.a.setImageBitmap(o.a("bitmap/loading.png", this.a));
        } else {
            mVar.a.setImageDrawable(a2);
        }
        mVar.b.setText((String) ((Map) this.b.get(i2)).get("title"));
        mVar.c.setText("大小:" + ((String) ((Map) this.b.get(i2)).get("size")));
        mVar.d.setText((String) ((Map) this.b.get(i2)).get("appKeyWord"));
        mVar.e.setTag(Integer.valueOf(i2));
        if (((Map) this.b.get(i2)).get("install").toString().equals("updateyes")) {
            mVar.e.setBackgroundDrawable(this.c);
        } else if (((Map) this.b.get(i2)).get("install").toString().equals("installyes")) {
            mVar.e.setBackgroundDrawable(this.j);
        } else if (((Map) this.b.get(i2)).get("install").toString().equals("installno")) {
            mVar.e.setBackgroundDrawable(this.g);
            HashMap hashMap = new HashMap();
            hashMap.put("position", Integer.valueOf(i2));
            hashMap.put("btn", mVar.e);
            hashMap.put("back", mVar.e.getBackground());
            hashMap.put("packagename", (String) ((Map) this.b.get(i2)).get("setpkg"));
            this.l.add(hashMap);
        }
        if (this.l.size() != 0) {
            int i4 = 0;
            while (true) {
                if (i4 < this.l.size()) {
                    if (this.l.get(i4) != null && Integer.parseInt(((Map) this.l.get(i4)).get("position").toString()) == i2) {
                        mVar.e.setBackgroundDrawable((Drawable) ((Map) this.l.get(i4)).get("back"));
                        break;
                    }
                    i4++;
                } else {
                    break;
                }
            }
        }
        if (this.k.size() != 0) {
            while (true) {
                if (i3 < this.k.size()) {
                    if (this.k.get(i3) != null && Integer.parseInt(((Map) this.k.get(i3)).get("position").toString()) == i2) {
                        mVar.e.setBackgroundDrawable((Drawable) ((Map) this.k.get(i3)).get("back"));
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
        }
        mVar.e.setOnTouchListener(new l(this));
        return viewGroup2;
    }
}
