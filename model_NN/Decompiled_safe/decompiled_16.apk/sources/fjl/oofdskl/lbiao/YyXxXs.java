package fjl.oofdskl.lbiao;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.io.File;

public class YyXxXs extends Activity {
    public Handler a = new s(this);
    private NotificationManager b;
    private String c;
    private Intent d;
    private String e;
    private LinearLayout f;
    /* access modifiers changed from: private */
    public String g;
    private String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    private int k;
    /* access modifiers changed from: private */
    public int l;
    private WebView m;
    /* access modifiers changed from: private */
    public Button n;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.lbiao.YyXxXs, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i2;
        ViewGroup.LayoutParams layoutParams;
        int i3;
        super.onCreate(bundle);
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        this.d = getIntent();
        this.e = this.d.getStringExtra("activity_flag");
        if (this.e == null) {
            finish();
        }
        if (this.e.equals("show")) {
            this.g = this.d.getStringExtra("title");
            this.h = this.d.getStringExtra("uil");
            this.i = this.d.getStringExtra("http");
            this.j = this.d.getStringExtra("packagename");
            this.k = this.d.getIntExtra("versionCode", -1);
            this.l = this.d.getIntExtra("position", -1);
            if (r.ag >= 320) {
                layoutParams = new ViewGroup.LayoutParams(-1, (r.af * 5) / 6);
                i2 = r.ae / 2;
                i3 = 100;
            } else if (r.ag >= 240 && r.ag < 320) {
                layoutParams = new ViewGroup.LayoutParams(-1, (r.af * 4) / 5);
                i2 = r.ae / 2;
                i3 = 75;
            } else if (240 > r.ag && r.ag >= 180) {
                layoutParams = new ViewGroup.LayoutParams(-1, (r.af * 4) / 5);
                i2 = r.ae / 2;
                i3 = 60;
            } else if (r.ag < 180 && r.ag > 120) {
                layoutParams = new ViewGroup.LayoutParams(-1, (r.af * 31) / 40);
                i2 = r.ae / 2;
                i3 = 50;
            } else if (r.ag <= 120) {
                layoutParams = new ViewGroup.LayoutParams(-1, (r.af * 2) / 3);
                i2 = r.ae / 2;
                i3 = 40;
            } else {
                i2 = 0;
                layoutParams = null;
                i3 = 0;
            }
            this.f = new LinearLayout(this);
            this.f.setBackgroundColor(-1);
            this.f.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.f.setOrientation(1);
            LinearLayout linearLayout = new LinearLayout(this);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            linearLayout.setOrientation(1);
            linearLayout.setGravity(1);
            layoutParams2.weight = 1.0f;
            linearLayout.setLayoutParams(layoutParams2);
            this.m = new WebView(this);
            this.m.setLayoutParams(layoutParams);
            this.m.loadUrl(this.h);
            this.m.getSettings().setJavaScriptEnabled(true);
            linearLayout.addView(this.m);
            this.f.addView(linearLayout);
            LinearLayout linearLayout2 = new LinearLayout(this);
            linearLayout2.setBackgroundColor(Color.argb(40, 100, 100, 100));
            linearLayout2.setGravity(17);
            linearLayout2.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            linearLayout2.setOrientation(0);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(i2, i3);
            layoutParams3.bottomMargin = 6;
            layoutParams3.gravity = 17;
            this.n = new Button(this);
            this.n.setLayoutParams(layoutParams3);
            this.n.setBackgroundDrawable(o.a((Activity) this, "button/down01.png"));
            linearLayout2.addView(this.n);
            this.f.addView(linearLayout2);
            setTitle("   信息加载中……");
            setContentView(this.f);
            this.m.setWebViewClient(new t(this));
            this.m.setDownloadListener(new u(this));
            this.n.setOnClickListener(new v(this));
            this.n.setOnTouchListener(new w(this));
        } else if (this.e.equals("install")) {
            requestWindowFeature(1);
            String stringExtra = this.d.getStringExtra("Biaoshi");
            String stringExtra2 = this.d.getStringExtra("Title");
            if (stringExtra == null) {
                finish();
                return;
            }
            this.c = this.d.getStringExtra("Pash");
            SharedPreferences.Editor edit = getSharedPreferences("data", 0).edit();
            edit.commit();
            if (new Integer(stringExtra).intValue() == 1) {
                this.j = this.d.getStringExtra("packagename");
                if (o.a(this, this.j, this.k, "install")) {
                    edit.putInt("biaoshi", 1);
                    edit.putString("settitle", stringExtra2);
                    edit.commit();
                    this.d = new Intent("android.intent.action.VIEW");
                    this.d.addFlags(268435456);
                    this.d.setDataAndType(Uri.fromFile(new File(this.c)), "application/vnd.android.package-archive");
                    startActivity(this.d);
                } else {
                    Toast.makeText(this, "测试ID随机生成推广包，目前检测你的手机已经安装此软件！", 1).show();
                    this.b = (NotificationManager) getSystemService("notification");
                    this.b.cancel(this.j.hashCode());
                }
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.m != null) {
            this.m.removeAllViews();
            this.m.destroy();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (getRequestedOrientation() != 1) {
            setRequestedOrientation(1);
        }
        super.onResume();
    }
}
