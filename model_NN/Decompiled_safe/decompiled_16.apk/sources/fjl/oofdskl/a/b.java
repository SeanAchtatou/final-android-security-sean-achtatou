package fjl.oofdskl.a;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

final class b extends Handler {
    private final /* synthetic */ d a;
    private final /* synthetic */ ImageView b;

    b(a aVar, d dVar, ImageView imageView) {
        this.a = dVar;
        this.b = imageView;
    }

    public final void handleMessage(Message message) {
        this.a.a(this.b, (Bitmap) message.obj);
    }
}
