package fjl.oofdskl.d;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

final class b implements SensorEventListener {
    private /* synthetic */ a a;

    b(a aVar) {
        this.a = aVar;
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        this.a.r = sensorEvent.values[0];
        this.a.s = sensorEvent.values[1];
        if (this.a.o.orientation == 2) {
            if (((double) this.a.s) > 0.3d) {
                if (this.a.q < 36.0f) {
                    a aVar = this.a;
                    aVar.q = aVar.q % 90.0f;
                    this.a.p = this.a.q % 90.0f;
                    this.a.q = this.a.q + 1.0f;
                    a.a(this.a, this.a.p, this.a.q);
                }
            } else if (((double) this.a.s) < -0.3d) {
                if (this.a.q > -36.0f) {
                    a aVar2 = this.a;
                    aVar2.q = aVar2.q % 90.0f;
                    this.a.p = this.a.q % 90.0f;
                    this.a.q = this.a.q - 1.0f;
                    a.a(this.a, this.a.p, this.a.q);
                }
            } else if (this.a.q > 0.0f) {
                this.a.p = this.a.q;
                this.a.q = this.a.q - 1.0f;
                a.a(this.a, this.a.p, this.a.q);
            } else if (this.a.q < 0.0f) {
                this.a.p = this.a.q;
                this.a.q = this.a.q + 1.0f;
                a.a(this.a, this.a.p, this.a.q);
            }
        } else if (((double) this.a.r) > 0.3d) {
            if (this.a.q < 36.0f) {
                a aVar3 = this.a;
                aVar3.q = aVar3.q % 90.0f;
                this.a.p = this.a.q;
                this.a.q = this.a.q + 1.0f;
                a.a(this.a, this.a.p, this.a.q);
            }
        } else if (((double) this.a.r) < -0.3d) {
            if (this.a.q > -36.0f) {
                a aVar4 = this.a;
                aVar4.q = aVar4.q % 90.0f;
                this.a.p = this.a.q;
                this.a.q = this.a.q - 1.0f;
                a.a(this.a, this.a.p, this.a.q);
            }
        } else if (this.a.q > 0.0f) {
            this.a.p = this.a.q;
            this.a.q = this.a.q - 1.0f;
            a.a(this.a, this.a.p, this.a.q);
        } else if (this.a.q < 0.0f) {
            this.a.p = this.a.q;
            this.a.q = this.a.q + 1.0f;
            a.a(this.a, this.a.p, this.a.q);
        }
    }
}
