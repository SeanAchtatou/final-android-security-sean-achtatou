package fjl.oofdskl.d;

import android.app.Activity;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.MyTextView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

public final class e {
    public static LinearLayout a;
    public static WindowManager b = null;
    public static boolean c = false;
    private static e g;
    private Activity d;
    private TextView e;
    private WindowManager.LayoutParams f = null;

    private e(Activity activity) {
        this.d = activity;
        b = (WindowManager) this.d.getApplicationContext().getSystemService("window");
        this.f = new WindowManager.LayoutParams();
        this.f.type = 2002;
        this.f.format = 1;
        this.f.flags = 40;
        this.f.gravity = 49;
        this.f.x = 0;
        this.f.y = 0;
        LinearLayout linearLayout = new LinearLayout(this.d);
        a = linearLayout;
        linearLayout.setBackgroundDrawable(o.a(this.d, "bitmap/textback.png"));
        WindowManager.LayoutParams layoutParams = null;
        if (r.ag >= 320) {
            layoutParams = new WindowManager.LayoutParams(-1, 90);
        } else if (r.ag >= 240 && r.ag < 320) {
            layoutParams = new WindowManager.LayoutParams(-1, 68);
        } else if (240 > r.ag && r.ag >= 180) {
            layoutParams = new WindowManager.LayoutParams(-1, 51);
        } else if (r.ag < 180 && r.ag > 120) {
            layoutParams = new WindowManager.LayoutParams(-1, 38);
        } else if (r.ag <= 120) {
            layoutParams = new WindowManager.LayoutParams(-1, 28);
        }
        a.setGravity(17);
        a.setLayoutParams(layoutParams);
        this.e = new MyTextView(this.d);
        this.e.setLayoutParams(new WindowManager.LayoutParams(-1, -2));
        this.e.setTextColor(-16777216);
        this.e.setTextSize(20.0f);
        a.addView(this.e);
    }

    public static e a(Activity activity) {
        if (g == null) {
            g = new e(activity);
        }
        return g;
    }

    public static void b() {
        if (c) {
            c = !c;
            b.removeView(a);
        }
        b = null;
        g = null;
    }

    public final void a() {
        boolean z = false;
        if (c) {
            b.removeView(a);
            this.f.width = 0;
            this.f.height = 0;
            if (!c) {
                z = true;
            }
            c = z;
        }
    }

    public final void a(String str) {
        if (!c) {
            this.e.setText(str);
            this.f.width = (r.ae * 2) / 3;
            this.f.height = r.ae / 11;
            b.addView(a, this.f);
            c = !c;
        }
    }
}
