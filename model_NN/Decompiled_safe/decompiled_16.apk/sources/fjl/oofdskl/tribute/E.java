package fjl.oofdskl.tribute;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class E extends Handler {
    private /* synthetic */ C0041z a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public E(C0041z zVar, Looper looper) {
        super(looper);
        this.a = zVar;
    }

    public final void handleMessage(Message message) {
        synchronized (this) {
            switch (message.what) {
                case 0:
                    if (this.a.l == 1) {
                        this.a.t.sendEmptyMessageDelayed(2, 0);
                    }
                    new D(this.a, this.a.a);
                    break;
            }
        }
    }
}
