package fjl.oofdskl.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ListAdapter;
import fjl.oofdskl.tools.r;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

final class ay extends BroadcastReceiver {
    private /* synthetic */ YyLt a;

    private ay(YyLt yyLt) {
        this.a = yyLt;
    }

    /* synthetic */ ay(YyLt yyLt, byte b) {
        this(yyLt);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, boolean):void
     arg types: [fjl.oofdskl.tribute.YyLt, int]
     candidates:
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, int):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, fjl.oofdskl.tribute.ad):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, java.util.List):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, boolean):void */
    public final void onReceive(Context context, Intent intent) {
        int i = 0;
        if (intent.getAction().equals("Registersucess")) {
            r.B = "列兵";
            r.C = "0.0";
            r.G = "1";
            this.a.f.a(r.z, r.B);
        } else if (intent.getAction().equals("loginsucess")) {
            this.a.f.a(r.z, r.B);
        } else if (intent.getAction().equals("loginfalse")) {
            YyLt.v(this.a);
        } else if (intent.getAction().equals("loginexit")) {
            this.a.f.a();
        } else if (intent.getAction().equals("loadTribute")) {
            ArrayList arrayList = new ArrayList();
            HashMap hashMap = new HashMap();
            hashMap.put("who", intent.getStringExtra("who"));
            hashMap.put("name", intent.getStringExtra("userNmae"));
            hashMap.put("note", intent.getStringExtra("noteInfo"));
            hashMap.put("count", "0");
            hashMap.put("noteID", intent.getStringExtra("noteInfoID"));
            hashMap.put("date", intent.getStringExtra("date"));
            hashMap.put("time", intent.getStringExtra("time"));
            hashMap.put("rank", intent.getStringExtra("rank"));
            hashMap.put("ifport", "0");
            hashMap.put("loadBitmap", "");
            hashMap.put("imgUrl", intent.getStringExtra("imgUrl"));
            arrayList.add(hashMap);
            for (int i2 = 0; i2 < this.a.k.size(); i2++) {
                arrayList.add((Map) this.a.k.get(i2));
            }
            this.a.k.clear();
            this.a.k = arrayList;
            this.a.q = this.a.k.size();
            this.a.d = new C0013ad(this.a, this.a.k, this.a.y);
            this.a.c.setAdapter((ListAdapter) this.a.d);
            this.a.d.notifyDataSetChanged();
            if (this.a.w) {
                this.a.w = false;
                this.a.c.removeFooterView(this.a.j);
                this.a.h.setVisibility(8);
                this.a.c.setVisibility(0);
            }
        } else if (intent.getAction().equals("loadSquare")) {
            Log.e("port", "squareListItems = " + this.a.m.size());
            ArrayList arrayList2 = new ArrayList();
            HashMap hashMap2 = new HashMap();
            hashMap2.put("who", intent.getStringExtra("who"));
            hashMap2.put("name", intent.getStringExtra("userNmae"));
            hashMap2.put("note", intent.getStringExtra("noteInfo"));
            hashMap2.put("count", "0");
            hashMap2.put("noteID", intent.getStringExtra("noteInfoID"));
            hashMap2.put("date", intent.getStringExtra("date"));
            hashMap2.put("time", intent.getStringExtra("time"));
            hashMap2.put("rank", intent.getStringExtra("rank"));
            hashMap2.put("imgUrl", intent.getStringExtra("imgUrl"));
            hashMap2.put("smalImgUrl", intent.getStringExtra("imgUrl"));
            hashMap2.put("loadBitmap", "");
            hashMap2.put("support", 0);
            hashMap2.put("nonsupport", 0);
            hashMap2.put("ifport", "0");
            arrayList2.add(hashMap2);
            for (int i3 = 0; i3 < this.a.m.size(); i3++) {
                arrayList2.add((Map) this.a.m.get(i3));
            }
            this.a.m.clear();
            this.a.m = arrayList2;
            this.a.q = this.a.m.size();
            this.a.e = new C0013ad(this.a, this.a.m, this.a.y);
            this.a.c.setAdapter((ListAdapter) this.a.e);
            this.a.e.notifyDataSetChanged();
            if (this.a.w) {
                this.a.w = false;
                this.a.c.removeFooterView(this.a.j);
                this.a.h.setVisibility(8);
                this.a.c.setVisibility(0);
            }
        } else if (intent.getAction().equals("non_support")) {
            while (true) {
                if (i >= this.a.m.size()) {
                    break;
                } else if (intent.getIntExtra("position", -1) == i) {
                    ((Map) this.a.m.get(i)).put("support", Integer.valueOf(intent.getIntExtra("support", -1)));
                    ((Map) this.a.m.get(i)).put("nonsupport", Integer.valueOf(intent.getIntExtra("nonsupport", -1)));
                    break;
                } else {
                    i++;
                }
            }
            this.a.e.notifyDataSetChanged();
        } else if (intent.getAction().equals("AppDiscussDestroy")) {
            YyLt.B(this.a);
        }
    }
}
