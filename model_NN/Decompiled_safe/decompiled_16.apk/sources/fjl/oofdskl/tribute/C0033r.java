package fjl.oofdskl.tribute;

import android.app.Activity;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.r  reason: case insensitive filesystem */
public final class C0033r extends LinearLayout {
    private Activity a = null;
    private String b;
    private String c;

    public C0033r(Activity activity, Map map) {
        super(activity);
        this.a = activity;
        this.b = (String) map.get("note");
        this.c = (String) map.get("name");
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        TextView textView = new TextView(this.a);
        textView.setTextColor(-16777216);
        textView.setBackgroundColor(Color.argb(200, 250, 6, 19));
        textView.setTextSize(25.0f);
        textView.setGravity(17);
        textView.setText(String.valueOf(this.c) + "说");
        textView.setTextColor(-1);
        addView(textView);
        TextView textView2 = new TextView(this.a);
        ScrollView scrollView = new ScrollView(this.a);
        LinearLayout.LayoutParams layoutParams = null;
        scrollView.setBackgroundDrawable(o.a(this.a, "bitmap/gray_back.png"));
        textView2.setGravity(3);
        textView2.setTextSize(20.0f);
        textView2.setText("\t" + o.b(this.b));
        if (r.ag >= 320) {
            layoutParams = new LinearLayout.LayoutParams(-1, 240);
            layoutParams.topMargin = 10;
            layoutParams.bottomMargin = 10;
            textView2.setPadding(25, 0, 5, 3);
        } else if (r.ag >= 240 && r.ag < 320) {
            layoutParams = new LinearLayout.LayoutParams(-1, 220);
            layoutParams.topMargin = 7;
            layoutParams.bottomMargin = 7;
            textView2.setPadding(17, 0, 5, 3);
        } else if (240 > r.ag && r.ag >= 180) {
            layoutParams = new LinearLayout.LayoutParams(-1, 180);
            layoutParams.topMargin = 6;
            layoutParams.bottomMargin = 6;
            textView2.setPadding(14, 0, 4, 3);
        } else if (r.ag < 180 && r.ag > 120) {
            layoutParams = new LinearLayout.LayoutParams(-1, 100);
            layoutParams.topMargin = 5;
            layoutParams.bottomMargin = 5;
            textView2.setPadding(12, 0, 4, 3);
        } else if (r.ag <= 120) {
            layoutParams = new LinearLayout.LayoutParams(-1, 80);
            layoutParams.topMargin = 4;
            layoutParams.bottomMargin = 4;
            textView2.setPadding(10, 0, 3, 3);
        }
        layoutParams.gravity = 1;
        scrollView.addView(textView2, new LinearLayout.LayoutParams(-1, -1));
        scrollView.setLayoutParams(layoutParams);
        addView(scrollView);
    }
}
