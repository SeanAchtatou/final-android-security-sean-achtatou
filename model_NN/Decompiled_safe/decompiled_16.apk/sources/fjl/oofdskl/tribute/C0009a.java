package fjl.oofdskl.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.i;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

/* renamed from: fjl.oofdskl.tribute.a  reason: case insensitive filesystem */
public final class C0009a extends Dialog {
    private static Dialog e;
    private C0020e a;
    /* access modifiers changed from: private */
    public Activity b;
    /* access modifiers changed from: private */
    public View c;
    private LinearLayout d;
    /* access modifiers changed from: private */
    public EditText f;
    /* access modifiers changed from: private */
    public EditText g;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    private Bitmap j;
    /* access modifiers changed from: private */
    public Bitmap k;
    /* access modifiers changed from: private */
    public Bitmap l;
    private Bitmap m;
    private ViewGroup.LayoutParams n;
    private int o;
    private Bitmap p;
    private ImageView q;
    private ImageView r;
    private DialogInterface.OnKeyListener s = new C0017b(this);

    public C0009a(Activity activity, View view) {
        super(activity);
        this.b = activity;
        this.c = view;
        view.setClickable(false);
        this.j = o.a("bitmap/login1.png", this.b);
        this.m = o.a("bitmap/regesterlogo1.png", this.b);
        this.k = o.a("bitmap/loginbtn2-1.png", this.b);
        this.l = o.a("bitmap/loginbtn1-1.png", this.b);
        if (r.ag >= 320) {
            this.n = new ViewGroup.LayoutParams((r.ae * 5) / 16, 100);
            new i(this.b);
            this.m = i.a(this.m, (this.m.getWidth() * 3) / 2, (this.m.getHeight() * 3) / 2);
            new i(this.b);
            this.p = i.a(o.a("bitmap/logindivider.png", this.b), r.ae, 5);
            this.o = 100;
        } else if (r.ag >= 240 && r.ag < 320) {
            this.n = new ViewGroup.LayoutParams((r.ae * 5) / 16, 80);
            new i(this.b);
            this.j = i.a(this.j, (this.j.getWidth() * 9) / 8, (this.j.getHeight() * 9) / 8);
            new i(this.b);
            this.k = i.a(this.k, (this.k.getWidth() * 3) / 4, (this.k.getHeight() * 3) / 4);
            new i(this.b);
            this.l = i.a(this.l, (this.l.getWidth() * 3) / 4, (this.l.getHeight() * 3) / 4);
            new i(this.b);
            this.m = i.a(this.m, (this.m.getWidth() * 9) / 8, (this.m.getHeight() * 9) / 8);
            new i(this.b);
            this.p = i.a(o.a("bitmap/logindivider.png", this.b), r.ae, 4);
            this.o = 80;
        } else if (240 > r.ag && r.ag >= 180) {
            this.n = new ViewGroup.LayoutParams((r.ae * 5) / 16, 60);
            new i(this.b);
            this.j = i.a(this.j, (this.j.getWidth() * 27) / 32, (this.j.getHeight() * 27) / 32);
            new i(this.b);
            this.k = i.a(this.k, (this.k.getWidth() * 9) / 16, (this.k.getHeight() * 9) / 16);
            new i(this.b);
            this.l = i.a(this.l, (this.l.getWidth() * 9) / 16, (this.l.getHeight() * 9) / 16);
            new i(this.b);
            this.m = i.a(this.m, (this.m.getWidth() * 27) / 32, (this.m.getHeight() * 27) / 32);
            new i(this.b);
            this.p = i.a(o.a("bitmap/logindivider.png", this.b), r.ae, 3);
            this.o = 65;
        } else if (r.ag < 180 && r.ag > 120) {
            this.n = new ViewGroup.LayoutParams((r.ae * 5) / 16, 50);
            new i(this.b);
            this.j = i.a(this.j, (this.j.getWidth() * 81) / 128, (this.j.getHeight() * 81) / 128);
            new i(this.b);
            this.k = i.a(this.k, (this.k.getWidth() * 27) / 64, (this.k.getHeight() * 27) / 64);
            new i(this.b);
            this.l = i.a(this.l, (this.l.getWidth() * 27) / 64, (this.l.getHeight() * 27) / 64);
            new i(this.b);
            this.m = i.a(this.m, (this.m.getWidth() * 81) / 128, (this.m.getHeight() * 81) / 128);
            new i(this.b);
            this.p = i.a(o.a("bitmap/logindivider.png", this.b), r.ae, 2);
            this.o = 50;
        } else if (r.ag <= 120) {
            this.n = new ViewGroup.LayoutParams((r.ae * 5) / 16, 40);
            new i(this.b);
            this.j = i.a(this.j, (this.j.getWidth() * 243) / 512, (this.j.getHeight() * 243) / 512);
            new i(this.b);
            this.k = i.a(this.k, (this.k.getWidth() * 81) / 256, (this.k.getHeight() * 81) / 256);
            new i(this.b);
            this.l = i.a(this.l, (this.l.getWidth() * 81) / 256, (this.l.getHeight() * 81) / 256);
            new i(this.b);
            this.m = i.a(this.m, (this.m.getWidth() * 143) / 512, (this.m.getHeight() * 243) / 512);
            new i(this.b);
            this.p = i.a(o.a("bitmap/logindivider.png", this.b), r.ae, 1);
            this.o = 40;
        }
        this.d = new LinearLayout(this.b);
        this.d.setOrientation(1);
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
        LinearLayout linearLayout = new LinearLayout(this.b);
        linearLayout.setPadding(0, 3, 0, 0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        TextView textView = new TextView(this.b);
        textView.setText("     游友登录");
        textView.setTextColor(Color.argb(150, 22, 70, 150));
        textView.setTextSize(25.0f);
        linearLayout.addView(textView, new ViewGroup.LayoutParams(-2, -2));
        linearLayout.addView(new View(this.b), this.n);
        this.q = new ImageView(this.b);
        this.q.setId(11112);
        this.q.setImageBitmap(this.m);
        this.q.setOnTouchListener(new U(this.b));
        linearLayout.addView(this.q, new ViewGroup.LayoutParams(-2, -2));
        LinearLayout linearLayout2 = new LinearLayout(this.b);
        linearLayout2.setLayoutParams(layoutParams);
        this.r = new ImageView(this.b);
        this.r.setImageBitmap(this.p);
        this.r.setAdjustViewBounds(true);
        this.r.setMaxWidth(r.ae);
        linearLayout2.addView(this.r, new ViewGroup.LayoutParams(-1, -2));
        linearLayout2.addView(new View(this.b), new ViewGroup.LayoutParams(-1, 10));
        LinearLayout linearLayout3 = new LinearLayout(this.b);
        linearLayout3.setLayoutParams(layoutParams);
        linearLayout3.setOrientation(0);
        TextView textView2 = new TextView(this.b);
        textView2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView2.setText("   账 号 ");
        textView2.setTextSize(25.0f);
        this.f = new EditText(this.b);
        this.f.setBackgroundDrawable(o.a(this.b, "bitmap/followNotepic.png"));
        this.f.setTextColor(-16777216);
        this.f.setHint("请输入账号");
        this.f.setSingleLine(true);
        this.f.setLayoutParams(new ViewGroup.LayoutParams((r.ae * 5) / 8, -2));
        linearLayout3.addView(textView2);
        linearLayout3.addView(this.f);
        LinearLayout linearLayout4 = new LinearLayout(this.b);
        linearLayout4.setPadding(0, 2, 0, 0);
        linearLayout4.setLayoutParams(layoutParams);
        linearLayout4.setOrientation(0);
        TextView textView3 = new TextView(this.b);
        textView3.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView3.setText("   密 码 ");
        textView3.setTextSize(25.0f);
        this.g = new EditText(this.b);
        this.g.setTextColor(-16777216);
        this.g.setBackgroundDrawable(o.a(this.b, "bitmap/followNotepic.png"));
        this.g.setHint("请输入密码");
        this.g.setSingleLine(true);
        this.g.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.g.setLayoutParams(new ViewGroup.LayoutParams((r.ae * 5) / 8, -2));
        linearLayout4.addView(textView3);
        linearLayout4.addView(this.g);
        LinearLayout linearLayout5 = new LinearLayout(this.b);
        linearLayout5.setPadding(0, 3, 0, 0);
        linearLayout5.setLayoutParams(layoutParams);
        linearLayout5.setOrientation(0);
        this.h = new Button(this.b);
        this.h.setId(11110);
        this.h.setBackgroundDrawable(new BitmapDrawable(this.k));
        this.h.setLayoutParams(new LinearLayout.LayoutParams((r.ae * 3) / 8, -2));
        this.h.setText("确认");
        this.h.setOnClickListener(new C0018c(this));
        this.h.setOnTouchListener(new C0019d(this));
        this.i = new Button(this.b);
        this.i.setLayoutParams(new LinearLayout.LayoutParams((r.ae * 3) / 8, -2));
        this.i.setText("取消");
        this.i.setBackgroundDrawable(new BitmapDrawable(this.k));
        this.i.setId(11111);
        this.i.setOnClickListener(new C0018c(this));
        this.i.setOnTouchListener(new C0019d(this));
        View view2 = new View(this.b);
        View view3 = new View(this.b);
        View view4 = new View(this.b);
        linearLayout5.addView(view2, new ViewGroup.LayoutParams((r.ae * 4) / 35, this.o));
        linearLayout5.addView(this.h);
        linearLayout5.addView(view3, new ViewGroup.LayoutParams((r.ae * 1) / 35, this.o));
        linearLayout5.addView(this.i);
        linearLayout5.addView(view4, new ViewGroup.LayoutParams((r.ae * 1) / 35, this.o));
        this.d.addView(linearLayout);
        this.d.addView(linearLayout2);
        this.d.addView(linearLayout3);
        this.d.addView(linearLayout4);
        this.d.addView(linearLayout5);
        e = new Dialog(this.b);
        this.a = new C0020e(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("Registersucess");
        intentFilter.addAction("loginsucess");
        this.b.registerReceiver(this.a, intentFilter);
        e.setOnKeyListener(this.s);
        e.setCanceledOnTouchOutside(false);
        e.requestWindowFeature(1);
        e.show();
        e.setContentView(this.d);
        e.getWindow().setBackgroundDrawable(new BitmapDrawable(this.j));
    }

    static /* synthetic */ void e(C0009a aVar) {
        aVar.c.setClickable(true);
        aVar.c.setBackgroundDrawable(o.a(aVar.b, "discuss/tribute_login.png"));
        aVar.b.unregisterReceiver(aVar.a);
        e.cancel();
        e.getWindow().setBackgroundDrawable(null);
        o.a(aVar.j);
        aVar.h.setBackgroundDrawable(null);
        aVar.i.setBackgroundDrawable(null);
        o.a(aVar.k);
        o.a(aVar.l);
        aVar.q.setImageBitmap(null);
        o.a(aVar.m);
        aVar.r.setImageBitmap(null);
        o.a(aVar.p);
    }
}
