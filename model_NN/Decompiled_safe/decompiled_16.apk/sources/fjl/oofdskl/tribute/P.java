package fjl.oofdskl.tribute;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import org.json.JSONException;
import org.json.JSONObject;

final class P implements q {
    private /* synthetic */ F a;

    public P(F f) {
        this.a = f;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.o);
            jSONObject.put("url", "registerAccount=" + r.z + "&icon=" + f.S + "&sex=" + f.R + "&oldCode=" + f.T + "&newCode=" + f.U);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        r.ai = false;
        new o(f.a, this, jSONObject.toString(), "discuss").start();
    }

    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        if (obj2.equals("0")) {
            r.G = this.a.S;
            r.F = this.a.R;
            Intent intent = new Intent("iconIncoChanged");
            intent.putExtra("icon", "icon" + r.G + ".jpg");
            this.a.a.sendBroadcast(intent);
            F.Q(this.a);
        } else if (obj2.equals("1")) {
            Toast.makeText(this.a.a, "信息修改失败！", 0).show();
        }
    }
}
