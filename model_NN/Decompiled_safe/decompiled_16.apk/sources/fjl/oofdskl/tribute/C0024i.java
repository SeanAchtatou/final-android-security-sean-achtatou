package fjl.oofdskl.tribute;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import fjl.oofdskl.tools.o;
import java.util.List;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.i  reason: case insensitive filesystem */
public final class C0024i extends BaseAdapter {
    private List a = null;
    private Activity b;

    public C0024i(Activity activity, List list) {
        this.b = activity;
        this.a = list;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        C0025j jVar;
        if (view == null) {
            C0025j jVar2 = new C0025j(this, (byte) 0);
            C0023h hVar = new C0023h(this.b);
            jVar2.a = (TextView) hVar.getChildAt(0);
            jVar2.b = (TextView) hVar.getChildAt(1);
            hVar.setTag(jVar2);
            view = hVar;
            jVar = jVar2;
        } else {
            jVar = (C0025j) view.getTag();
        }
        jVar.a.setText("   " + ((Map) this.a.get(i)).get("nameInfo").toString() + ((Map) this.a.get(i)).get("userNum").toString() + ": ");
        jVar.b.setText("\t " + o.b(((Map) this.a.get(i)).get("noteInfo").toString()));
        return view;
    }
}
