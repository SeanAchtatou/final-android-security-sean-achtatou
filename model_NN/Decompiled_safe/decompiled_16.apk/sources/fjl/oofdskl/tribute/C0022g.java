package fjl.oofdskl.tribute;

import android.app.Activity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.g  reason: case insensitive filesystem */
public final class C0022g extends LinearLayout {
    public Button a = new Button(this.c);
    public Button b = new Button(this.c);
    private Activity c;
    private Button d = new Button(this.c);
    private Button e = new Button(this.c);
    private TextView f = new TextView(this.c);

    public C0022g(Activity activity, Map map) {
        super(activity);
        this.c = activity;
        setOrientation(0);
        setBackgroundDrawable(o.a(activity, "tabhost/titleback.png"));
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.f.setGravity(17);
        this.f.setText("游问友答");
        this.b.setId(5555);
        this.a.setBackgroundDrawable(o.a(this.c, "button/back01.png"));
        this.b.setBackgroundDrawable(o.a(this.c, "button/follownote01.png"));
        this.e.setBackgroundDrawable(o.a(this.c, "button/left.png"));
        this.d.setBackgroundDrawable(o.a(this.c, "button/right.png"));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(r.ae / 6, r.af / 18, 1.0f);
        layoutParams.gravity = 17;
        layoutParams.leftMargin = r.ae / 25;
        layoutParams.rightMargin = r.ae / 25;
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(r.ae / 7, r.af / 15, 1.0f);
        layoutParams2.gravity = 17;
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(r.ae / 5, r.af / 15, 8.0f);
        layoutParams3.gravity = 16;
        addView(this.a, layoutParams);
        addView(this.e, layoutParams2);
        addView(this.f, layoutParams3);
        addView(this.d, layoutParams2);
        addView(this.b, layoutParams);
    }
}
