package fjl.oofdskl.tribute;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

public final class X extends RelativeLayout {
    public ImageView a;
    public TextView b;
    public Button c;
    public Button d;
    /* access modifiers changed from: private */
    public Activity e;
    private int f = 35;
    private int g = 8888;
    private int h = 152;
    private Button i;
    private C0012ac j;

    public X(Activity activity) {
        super(activity);
        this.e = activity;
        setBackgroundColor(-1);
        this.a = new ImageView(this.e);
        this.a.setImageDrawable(o.a(o.b(this.e, "icon.dat", "icon/icon1.jpg"), 15));
        if (r.ag >= 320) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(140, 140));
            this.a.setPadding(5, 5, 5, 5);
        } else if (r.ag >= 240 && r.ag < 320) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(128, 128));
            this.a.setPadding(8, 8, 8, 8);
        } else if (240 > r.ag && r.ag >= 180) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(64, 64));
            this.a.setPadding(6, 6, 6, 6);
        } else if (r.ag < 180 && r.ag > 120) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(55, 55));
            this.a.setPadding(3, 3, 3, 3);
        } else if (r.ag <= 120) {
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(45, 45));
            this.a.setPadding(4, 4, 4, 4);
        }
        this.a.setId(this.f);
        addView(this.a);
        LinearLayout linearLayout = new LinearLayout(this.e);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) new RelativeLayout.LayoutParams(-2, -2));
        layoutParams.addRule(1, this.f);
        layoutParams.addRule(10);
        linearLayout.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = null;
        if (r.ag >= 320) {
            layoutParams.setMargins(25, 0, 0, 0);
            layoutParams2 = new RelativeLayout.LayoutParams(-1, 60);
        } else if (r.ag >= 240 && r.ag < 320) {
            layoutParams.setMargins(20, 0, 0, 0);
            layoutParams2 = new RelativeLayout.LayoutParams(-1, 50);
        } else if (240 > r.ag && r.ag >= 180) {
            layoutParams2 = new RelativeLayout.LayoutParams(-1, 40);
        } else if (r.ag < 180 && r.ag > 120) {
            layoutParams2 = new RelativeLayout.LayoutParams(-1, 30);
        } else if (r.ag <= 120) {
            layoutParams2 = new RelativeLayout.LayoutParams(-1, 25);
        }
        LinearLayout linearLayout2 = new LinearLayout(this.e);
        linearLayout2.setOrientation(0);
        linearLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout2.setGravity(48);
        this.b = new TextView(this.e);
        this.b.setId(this.h);
        this.b.setLayoutParams(layoutParams2);
        this.b.setGravity(16);
        this.b.setTextSize(18.0f);
        this.b.setSingleLine(true);
        this.b.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        this.b.setTextColor(-16777216);
        this.b.setText("昵称：游友☞登录有惊喜");
        linearLayout2.addView(this.b);
        linearLayout.addView(linearLayout2);
        addView(linearLayout);
        linearLayout.setOnTouchListener(new Z(this));
        RelativeLayout relativeLayout = new RelativeLayout(this.e);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams((ViewGroup.MarginLayoutParams) new RelativeLayout.LayoutParams(-1, -2));
        layoutParams3.addRule(1, this.f);
        layoutParams3.addRule(12);
        relativeLayout.setLayoutParams(layoutParams3);
        this.i = new Button(this.e);
        this.i.setId(this.g);
        this.i.setOnTouchListener(new U(this.e));
        this.i.setBackgroundDrawable(o.a(this.e, "discuss/tribute_login.png"));
        this.d = new Button(this.e);
        this.d.setVisibility(8);
        this.d.setBackgroundDrawable(o.a(this.e, "discuss/tribute_home.png"));
        this.d.setOnTouchListener(new Y(this));
        this.c = new Button(this.e);
        this.c.setBackgroundDrawable(o.a(this.e, "discuss/tribute_message.png"));
        RelativeLayout.LayoutParams layoutParams4 = null;
        RelativeLayout.LayoutParams layoutParams5 = null;
        if (r.ag >= 320) {
            layoutParams4 = new RelativeLayout.LayoutParams(200, 60);
            layoutParams5 = new RelativeLayout.LayoutParams(200, 60);
            layoutParams3.setMargins(25, 0, 0, 10);
            layoutParams5.setMargins(0, 0, 20, 0);
        } else if (r.ag >= 240 && r.ag < 320) {
            layoutParams4 = new RelativeLayout.LayoutParams(120, 40);
            layoutParams5 = new RelativeLayout.LayoutParams(120, 40);
            layoutParams3.setMargins(13, 0, 0, 10);
            layoutParams5.setMargins(0, 0, 20, 0);
        } else if (240 > r.ag && r.ag >= 180) {
            layoutParams4 = new RelativeLayout.LayoutParams(90, 40);
            layoutParams5 = new RelativeLayout.LayoutParams(90, 40);
            layoutParams3.setMargins(10, 0, 0, 10);
            layoutParams5.setMargins(0, 0, 17, 0);
        } else if (r.ag < 180 && r.ag > 120) {
            layoutParams4 = new RelativeLayout.LayoutParams(75, 25);
            layoutParams5 = new RelativeLayout.LayoutParams(75, 25);
            layoutParams3.setMargins(10, 0, 0, 10);
            layoutParams5.setMargins(0, 0, 15, 0);
        } else if (r.ag <= 120) {
            layoutParams4 = new RelativeLayout.LayoutParams(40, 15);
            layoutParams5 = new RelativeLayout.LayoutParams(40, 15);
            layoutParams3.setMargins(10, 0, 0, 10);
            layoutParams5.setMargins(0, 0, 5, 0);
        }
        layoutParams4.addRule(1, this.f);
        layoutParams4.addRule(12);
        layoutParams5.addRule(11);
        layoutParams5.addRule(12);
        this.i.setLayoutParams(layoutParams4);
        this.d.setLayoutParams(layoutParams4);
        this.c.setLayoutParams(layoutParams5);
        relativeLayout.addView(this.i);
        relativeLayout.addView(this.d);
        relativeLayout.addView(this.c);
        addView(relativeLayout);
        this.j = new C0012ac(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("iconIncoChanged");
        this.e.registerReceiver(this.j, intentFilter);
    }

    private void a(String str, String str2, int i2) {
        SharedPreferences.Editor edit = this.e.getSharedPreferences("isLogin", 0).edit();
        edit.putString("userName", str);
        edit.putString("password", str2);
        edit.putInt("isLogin", i2);
        edit.commit();
    }

    static /* synthetic */ void b(X x) {
        AlertDialog.Builder builder = new AlertDialog.Builder(x.e);
        builder.setMessage("确认要注销账户吗？");
        builder.setTitle("提示");
        builder.setPositiveButton("确认", new C0010aa(x));
        builder.setNegativeButton("取消", new C0011ab(x));
        builder.create().show();
    }

    public final void a() {
        r.H = false;
        this.b.setText("昵称：游友☞登录有惊喜");
        this.i.setVisibility(0);
        this.d.setVisibility(8);
        r.z = "";
        r.A = null;
        a(null, null, 0);
    }

    public final void a(String str, String str2) {
        r.H = true;
        this.b.setText("昵称：" + str + "☞" + str2);
        this.i.setVisibility(8);
        this.d.setVisibility(0);
        this.a.setImageDrawable(o.a(o.b(this.e, "icon.dat", "icon/icon" + r.G + ".jpg"), 15));
        a(r.z, r.A, 1);
    }

    public final boolean b() {
        SharedPreferences sharedPreferences = this.e.getSharedPreferences("isLogin", 0);
        if (sharedPreferences.getString("userName", "") == null || sharedPreferences.getInt("isLogin", -1) != 1) {
            return false;
        }
        r.z = sharedPreferences.getString("userName", "");
        r.A = sharedPreferences.getString("password", "");
        return true;
    }
}
