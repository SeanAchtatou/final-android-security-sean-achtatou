package fjl.oofdskl.tribute;

import android.content.Context;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import java.net.URLEncoder;
import org.json.JSONObject;

/* renamed from: fjl.oofdskl.tribute.v  reason: case insensitive filesystem */
final class C0037v implements q {
    final /* synthetic */ Ft a;

    public C0037v(Ft ft, Context context) {
        this.a = ft;
        JSONObject jSONObject = new JSONObject();
        try {
            if (ft.q.equals("loadTribute")) {
                if (ft.p) {
                    jSONObject.put("para", r.i);
                    jSONObject.put("url", "registerAccount=" + r.z + "&appackageName=" + r.K + "&appnoteInfo=" + URLEncoder.encode(ft.b.getText().toString()));
                } else {
                    jSONObject.put("para", r.j);
                    jSONObject.put("url", "registerAccount=" + r.z + "&questionID=" + ft.t + "&appnoteInfo=" + URLEncoder.encode(ft.b.getText().toString()));
                }
                r.ai = false;
                new o(context, this, jSONObject.toString(), "discuss").start();
            }
            if (ft.q.equals("allnotes")) {
                jSONObject.put("para", r.j);
                jSONObject.put("url", "registerAccount=" + r.z + "&questionID=" + ft.t + "&appnoteInfo=" + URLEncoder.encode(ft.b.getText().toString()));
            } else if (ft.q.equals("loadSquare") && !ft.p) {
                jSONObject.put("para", r.u);
                jSONObject.put("url", "registerAccount=" + r.z + "&sid=" + ft.t + "&sayRegisterAccount=" + ft.s + "&appnoteInfo=" + URLEncoder.encode(ft.b.getText().toString()));
            }
            r.ai = false;
            new o(context, this, jSONObject.toString(), "discuss").start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, java.lang.Boolean):void
     arg types: [fjl.oofdskl.tribute.Ft, int]
     candidates:
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, boolean):void
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, java.lang.Boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r9, java.lang.Object r10) {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r0 = r10.toString()
            r1 = 0
            java.lang.String r2 = "false"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x001a
            fjl.oofdskl.tribute.Ft r0 = r8.a
            java.lang.String r1 = "发帖失败！"
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r6)
            r0.show()
        L_0x0019:
            return
        L_0x001a:
            fjl.oofdskl.tribute.Ft r2 = r8.a
            boolean r2 = r2.p
            if (r2 == 0) goto L_0x0127
            android.content.Intent r2 = new android.content.Intent
            fjl.oofdskl.tribute.Ft r3 = r8.a
            java.lang.String r3 = r3.q
            r2.<init>(r3)
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x011f }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x011f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x011f }
            java.lang.String r4 = "userID"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x011f }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ JSONException -> 0x011f }
            r0.<init>(r4)     // Catch:{ JSONException -> 0x011f }
            java.lang.String r4 = "userNum"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x011f }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ JSONException -> 0x011f }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x011f }
            java.lang.String r1 = "who"
            java.lang.String r4 = "userNum"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "userNmae"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r5 = "userID"
            java.lang.String r5 = r3.getString(r5)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ JSONException -> 0x0197 }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r5 = "userNum"
            java.lang.String r5 = r3.getString(r5)     // Catch:{ JSONException -> 0x0197 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r4 = r4.toString()     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "noteInfo"
            fjl.oofdskl.tribute.Ft r4 = r8.a     // Catch:{ JSONException -> 0x0197 }
            android.widget.EditText r4 = r4.b     // Catch:{ JSONException -> 0x0197 }
            android.text.Editable r4 = r4.getText()     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r4 = r4.toString()     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "noteInfoID"
            java.lang.String r4 = "noteid"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "date"
            java.lang.String r4 = "noteDate"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "time"
            java.lang.String r4 = "noteTime"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r4)     // Catch:{ JSONException -> 0x0197 }
            java.lang.String r1 = "rank"
            java.lang.String r4 = "rankName"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ JSONException -> 0x0197 }
            r2.putExtra(r1, r3)     // Catch:{ JSONException -> 0x0197 }
            fjl.oofdskl.tribute.Ft r1 = r8.a     // Catch:{ JSONException -> 0x0197 }
            r1.sendBroadcast(r2)     // Catch:{ JSONException -> 0x0197 }
        L_0x00c0:
            fjl.oofdskl.tribute.Ft r1 = r8.a
            r1.finish()
            fjl.oofdskl.tribute.Ft r1 = r8.a
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)
            r1.c = r2
            java.lang.String r1 = fjl.oofdskl.tools.r.z
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0019
            android.app.AlertDialog$Builder r1 = new android.app.AlertDialog$Builder
            fjl.oofdskl.tribute.Ft r2 = r8.a
            r1.<init>(r2)
            r2 = 17301659(0x108009b, float:2.497969E-38)
            r1.setIcon(r2)
            java.lang.String r2 = "温馨提示"
            r1.setTitle(r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "你好，"
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "。注册用户可以享有更多权限，发表说说，查看别人给自己的回复等。你是否现在注册？"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.setMessage(r0)
            java.lang.String r0 = "是"
            fjl.oofdskl.tribute.w r2 = new fjl.oofdskl.tribute.w
            r2.<init>(r8)
            r1.setPositiveButton(r0, r2)
            java.lang.String r0 = "否"
            fjl.oofdskl.tribute.x r2 = new fjl.oofdskl.tribute.x
            r2.<init>(r8)
            r1.setNegativeButton(r0, r2)
            android.app.AlertDialog r0 = r1.create()
            r0.show()
            goto L_0x0019
        L_0x011f:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0123:
            r1.printStackTrace()
            goto L_0x00c0
        L_0x0127:
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "postReplyNoteSucess"
            r2.<init>(r3)
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0192 }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x0192 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0192 }
            java.lang.String r4 = "userID"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0192 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ JSONException -> 0x0192 }
            r0.<init>(r4)     // Catch:{ JSONException -> 0x0192 }
            java.lang.String r4 = "userNum"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ JSONException -> 0x0192 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ JSONException -> 0x0192 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0192 }
            java.lang.String r1 = "userNmae"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x018c }
            java.lang.String r5 = "userID"
            java.lang.String r5 = r3.getString(r5)     // Catch:{ JSONException -> 0x018c }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ JSONException -> 0x018c }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x018c }
            java.lang.String r5 = "userNum"
            java.lang.String r3 = r3.getString(r5)     // Catch:{ JSONException -> 0x018c }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ JSONException -> 0x018c }
            java.lang.String r3 = r3.toString()     // Catch:{ JSONException -> 0x018c }
            r2.putExtra(r1, r3)     // Catch:{ JSONException -> 0x018c }
            java.lang.String r1 = "noteInfo"
            fjl.oofdskl.tribute.Ft r3 = r8.a     // Catch:{ JSONException -> 0x018c }
            android.widget.EditText r3 = r3.b     // Catch:{ JSONException -> 0x018c }
            android.text.Editable r3 = r3.getText()     // Catch:{ JSONException -> 0x018c }
            java.lang.String r3 = r3.toString()     // Catch:{ JSONException -> 0x018c }
            r2.putExtra(r1, r3)     // Catch:{ JSONException -> 0x018c }
            fjl.oofdskl.tribute.Ft r1 = r8.a     // Catch:{ JSONException -> 0x018c }
            r1.sendBroadcast(r2)     // Catch:{ JSONException -> 0x018c }
            goto L_0x00c0
        L_0x018c:
            r1 = move-exception
        L_0x018d:
            r1.printStackTrace()
            goto L_0x00c0
        L_0x0192:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x018d
        L_0x0197:
            r1 = move-exception
            goto L_0x0123
        */
        throw new UnsupportedOperationException("Method not decompiled: fjl.oofdskl.tribute.C0037v.a(android.content.Context, java.lang.Object):void");
    }
}
