package fjl.oofdskl.tribute;

import android.view.MotionEvent;
import android.view.View;
import fjl.oofdskl.tools.r;

final class as implements View.OnTouchListener {
    private /* synthetic */ YyLt a;

    as(YyLt yyLt) {
        this.a = yyLt;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            if (YyLt.a != null) {
                YyLt.a.cancel();
                this.a.A = false;
                YyLt.a = null;
            }
            this.a.f.c.setBackgroundDrawable(this.a.C);
        } else if (motionEvent.getAction() == 1) {
            this.a.f.c.setBackgroundDrawable(this.a.D);
            if (r.H) {
                new C0041z(this.a, this.a.f.c, this.a.y);
            } else {
                new C0041z(this.a, this.a.f.c, "noLogin");
            }
        }
        return false;
    }
}
