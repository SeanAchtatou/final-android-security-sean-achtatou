package fjl.oofdskl.tribute;

import android.app.Activity;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import fjl.oofdskl.a.a;
import fjl.oofdskl.tools.b;
import fjl.oofdskl.tools.d;
import fjl.oofdskl.tools.e;
import fjl.oofdskl.tools.f;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class YyLt extends Activity implements AbsListView.OnScrollListener {
    public static Timer a = null;
    /* access modifiers changed from: private */
    public boolean A = false;
    private int B;
    /* access modifiers changed from: private */
    public BitmapDrawable C;
    /* access modifiers changed from: private */
    public BitmapDrawable D;
    /* access modifiers changed from: private */
    public BitmapDrawable E;
    /* access modifiers changed from: private */
    public BitmapDrawable F;
    /* access modifiers changed from: private */
    public BitmapDrawable G;
    /* access modifiers changed from: private */
    public BitmapDrawable H;
    /* access modifiers changed from: private */
    public BitmapDrawable I;
    /* access modifiers changed from: private */
    public Handler J = new aq(this);
    /* access modifiers changed from: private */
    public Handler K = new ar(this);
    private ay L;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public ListView c = null;
    /* access modifiers changed from: private */
    public C0013ad d;
    /* access modifiers changed from: private */
    public C0013ad e;
    /* access modifiers changed from: private */
    public X f;
    /* access modifiers changed from: private */
    public V g;
    /* access modifiers changed from: private */
    public d h;
    /* access modifiers changed from: private */
    public e i;
    /* access modifiers changed from: private */
    public b j;
    /* access modifiers changed from: private */
    public List k = new ArrayList();
    /* access modifiers changed from: private */
    public List l = new ArrayList();
    /* access modifiers changed from: private */
    public List m = new ArrayList();
    /* access modifiers changed from: private */
    public List n = new ArrayList();
    private ax o;
    private int p;
    /* access modifiers changed from: private */
    public int q;
    /* access modifiers changed from: private */
    public int r = 1;
    /* access modifiers changed from: private */
    public f s;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public boolean w = true;
    /* access modifiers changed from: private */
    public boolean x = false;
    /* access modifiers changed from: private */
    public String y = null;
    /* access modifiers changed from: private */
    public int z = 0;

    static /* synthetic */ void B(YyLt yyLt) {
        yyLt.unregisterReceiver(yyLt.L);
        yyLt.f.c.setBackgroundDrawable(null);
        yyLt.g.c.setBackgroundDrawable(null);
        e.a.setBackgroundDrawable(null);
        o.a(yyLt.C.getBitmap());
        o.a(yyLt.D.getBitmap());
        o.a(yyLt.E.getBitmap());
        o.a(yyLt.F.getBitmap());
        o.a(yyLt.G.getBitmap());
        o.a(yyLt.H.getBitmap());
        o.a(yyLt.I.getBitmap());
        yyLt.finish();
    }

    static /* synthetic */ void E(YyLt yyLt) {
        if (yyLt.B != 2 && !yyLt.u) {
            yyLt.r = 1;
            yyLt.t = true;
            if (yyLt.y.equals("loadTribute")) {
                yyLt.d = null;
                yyLt.k.clear();
            } else if (yyLt.y.equals("loadSquare")) {
                yyLt.e = null;
                yyLt.m.clear();
            }
            yyLt.o.sendEmptyMessage(0);
        }
    }

    static /* synthetic */ void v(YyLt yyLt) {
        if (a == null) {
            a = new Timer();
        }
        if (!yyLt.A) {
            yyLt.A = true;
            a.schedule(new av(yyLt), 1000, 500);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.tribute.YyLt, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.C = o.a((Activity) this, "discuss/tribute_message_clk.png");
        this.D = o.a((Activity) this, "discuss/tribute_message.png");
        this.E = o.a((Activity) this, "discuss/tribute_refresh_clk.png");
        this.F = o.a((Activity) this, "button/again02.png");
        this.G = o.a((Activity) this, "button/again01.png");
        this.H = o.a((Activity) this, "discuss/tribute_refresh.png");
        this.I = o.a((Activity) this, "discuss/tribute_message_bicker.png");
        this.y = getIntent().getStringExtra("load");
        HandlerThread handlerThread = new HandlerThread("discuss");
        handlerThread.start();
        this.o = new ax(this, handlerThread.getLooper());
        this.b = new LinearLayout(this);
        this.b.setBackgroundColor(-1);
        this.b.setBackgroundDrawable(o.a((Activity) this, "tabhost/back.png"));
        this.b.setOrientation(1);
        this.f = new X(this);
        if (r.H) {
            this.f.a(r.z, r.B);
        }
        this.g = new V(this);
        this.f.c.setOnTouchListener(new as(this));
        this.g.c.setOnClickListener(new at(this));
        this.g.a.setOnTouchListener(new U(this, this.y, (byte) 0));
        this.s = new f(this);
        this.s.setGravity(17);
        this.h = new d(this);
        this.h.setVisibility(8);
        this.i = new e(this);
        this.i.setVisibility(8);
        e.a.setOnTouchListener(new au(this));
        this.j = new b(this);
        this.c = new ListView(this);
        this.c.setDivider(o.a((Activity) this, "bitmap/dividerH.png"));
        this.c.setDividerHeight(2);
        this.c.setClickable(false);
        this.c.setClipChildren(false);
        this.c.setPressed(false);
        this.c.setFastScrollEnabled(true);
        a.a(this.c, this);
        this.c.setBackgroundDrawable(o.a((Activity) this, "tabhost/back.png"));
        this.c.setCacheColorHint(-1);
        this.c.addFooterView(this.j);
        this.c.setOnScrollListener(this);
        this.b.addView(this.f, new LinearLayout.LayoutParams(-1, (r.af / 8) + 10, 1.0f));
        this.b.addView(this.g, new LinearLayout.LayoutParams(-1, (r.af / 16) + 10, 1.0f));
        this.b.addView(this.c, new LinearLayout.LayoutParams(-1, (r.af * 41) / 50, 10.0f));
        this.b.addView(this.s, new LinearLayout.LayoutParams(-1, (r.af * 41) / 50, 10.0f));
        this.b.addView(this.i, new LinearLayout.LayoutParams(-1, (r.af * 41) / 50, 10.0f));
        this.b.addView(this.h, new LinearLayout.LayoutParams(-1, (r.af * 41) / 50, 10.0f));
        setContentView(this.b);
        this.o.sendEmptyMessage(0);
        this.L = new ay(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("Registersucess");
        if (this.y.equals("loadTribute")) {
            intentFilter.addAction("loadTribute");
        } else if (this.y.equals("loadSquare")) {
            intentFilter.addAction("loadSquare");
            intentFilter.addAction("non_support");
        }
        intentFilter.addAction("loginsucess");
        intentFilter.addAction("loginfalse");
        intentFilter.addAction("loginexit");
        intentFilter.addAction("AppDiscussDestroy");
        registerReceiver(this.L, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (getRequestedOrientation() != 1) {
            setRequestedOrientation(1);
        }
        super.onResume();
    }

    public void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.j.setVisibility(0);
        this.p = (i2 + i3) - 1;
    }

    public void onScrollStateChanged(AbsListView absListView, int i2) {
        this.B = i2;
        if (this.p == this.q && i2 == 0 && !this.u) {
            this.u = true;
            this.o.sendEmptyMessage(0);
        }
    }
}
