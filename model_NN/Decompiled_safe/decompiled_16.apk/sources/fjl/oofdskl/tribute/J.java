package fjl.oofdskl.tribute;

import android.widget.RadioGroup;

final class J implements RadioGroup.OnCheckedChangeListener {
    private /* synthetic */ F a;

    J(F f) {
        this.a = f;
    }

    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == this.a.o) {
            this.a.R = 0;
        } else if (i == this.a.p) {
            this.a.R = 1;
        }
    }
}
