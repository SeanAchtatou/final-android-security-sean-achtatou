package fjl.oofdskl.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import fjl.oofdskl.tools.k;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

public final class ak extends Dialog {
    final Handler a = new al(this);
    /* access modifiers changed from: private */
    public Activity b;
    private FrameLayout c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public k e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Bitmap g;
    /* access modifiers changed from: private */
    public BitmapDrawable h;
    /* access modifiers changed from: private */
    public BitmapDrawable i;
    private LinearLayout j;
    private int k = 0;
    private int l = 0;
    private DialogInterface.OnKeyListener m = new am(this);

    public ak(Activity activity, String str) {
        super(activity, 16973829);
        setOwnerActivity(activity);
        this.b = activity;
        this.d = str;
        this.h = o.a(this.b, "discuss/img_load.png");
        this.i = o.a(this.b, "discuss/img_load_ok.png");
        this.c = new FrameLayout(this.b);
        this.c.setBackgroundColor(-1);
        this.c.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.e = new k(this.b, r.ac, r.ad);
        this.e.setId(39321);
        this.e.setOnTouchListener(new ap(this, (byte) 0));
        this.e.setImageBitmap(o.a("bitmap/squareBitmapBig.png", this.b));
        layoutParams.gravity = 17;
        if (new StringBuilder().append(this.d.charAt(0)).append(this.d.charAt(1)).toString().equals("sd")) {
            new an(this).start();
        } else {
            new ao(this).start();
        }
        this.j = new LinearLayout(this.b);
        this.j.setGravity(81);
        ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(-1, -1);
        this.j.setPadding(0, 0, 0, 7);
        this.j.setLayoutParams(layoutParams2);
        this.j.setOrientation(0);
        if (r.ag >= 320) {
            this.k = 168;
            this.l = 64;
        } else if (r.ag >= 240 && r.ag < 320) {
            this.k = 151;
            this.l = 58;
        } else if (240 > r.ag && r.ag >= 180) {
            this.k = 117;
            this.l = 45;
        } else if (r.ag < 180 && r.ag > 120) {
            this.k = 84;
            this.l = 32;
        } else if (r.ag <= 120) {
            this.k = 67;
            this.l = 26;
        }
        this.f = new Button(this.b);
        this.f.setId(34952);
        this.f.setOnTouchListener(new ap(this, (byte) 0));
        this.f.setBackgroundDrawable(this.h);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.k, this.l);
        layoutParams3.addRule(12);
        this.j.addView(this.f, layoutParams3);
        this.c.addView(this.e, layoutParams);
        this.c.addView(this.j);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(this.m);
        show();
        setContentView(this.c);
    }

    static /* synthetic */ void a(ak akVar, int i2) {
        TranslateAnimation translateAnimation = null;
        if (i2 == 0) {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (akVar.l + 10));
        } else if (i2 == 1) {
            translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (akVar.l + 10), 0.0f);
        }
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        akVar.j.setAnimation(translateAnimation);
        translateAnimation.startNow();
    }
}
