package fjl.oofdskl.tribute;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

final class aC implements TextWatcher {
    private /* synthetic */ aA a;

    aC(aA aAVar) {
        this.a = aAVar;
    }

    public final void afterTextChanged(Editable editable) {
        if (editable.length() > 0) {
            int length = editable.length() - 1;
            char charAt = editable.charAt(length);
            if (charAt >= 'a' && charAt <= 'z') {
                return;
            }
            if ((charAt < 'A' || charAt > 'Z') && charAt != '_' && charAt != '@') {
                if (charAt < '0' || charAt > '9') {
                    Toast.makeText(this.a.a, "只能输入字母、数字以及'_'和'@'等字符！", 0).show();
                    editable.delete(length, length + 1);
                }
            }
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
