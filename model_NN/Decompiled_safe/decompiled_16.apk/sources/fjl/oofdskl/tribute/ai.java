package fjl.oofdskl.tribute;

import android.content.Context;
import android.os.Message;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import java.util.Map;
import org.json.JSONObject;

final class ai implements q {
    private String a;
    private int b;
    private /* synthetic */ C0013ad c;

    public ai(C0013ad adVar, String str, int i) {
        this.c = adVar;
        this.a = str;
        this.b = i;
        if (o.b((Context) adVar.a)) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (str.equals("support")) {
                    jSONObject.put("para", r.w);
                    jSONObject.put("url", "sid=" + ((Map) adVar.b.get(i)).get("noteID") + "&registerAccount=" + r.z + "&sayRegisterAccount=" + ((Map) adVar.b.get(i)).get("name"));
                } else if (str.equals("nonsupport")) {
                    jSONObject.put("para", r.x);
                    jSONObject.put("url", "sid=" + ((Map) adVar.b.get(i)).get("noteID") + "&registerAccount=" + r.z + "&sayRegisterAccount=" + ((Map) adVar.b.get(i)).get("name"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            r.ai = false;
            new o(adVar.a, this, jSONObject.toString(), "discuss").start();
            return;
        }
        Toast.makeText(adVar.a, "网络不给力哦……请您连接网络", 0).show();
    }

    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        if (obj2.equals("yes")) {
            Message message = new Message();
            if (this.a.equals("support")) {
                message.what = 1365;
                message.arg1 = this.b;
                this.c.g.sendMessage(message);
            } else if (this.a.equals("nonsupport")) {
                message.what = 1366;
                message.arg1 = this.b;
                this.c.g.sendMessage(message);
            }
            Toast.makeText(this.c.a, "你已经对此帖评价成功！", 0).show();
        } else if (obj2.equals("error")) {
            Toast.makeText(this.c.a, "不好意思，你的评价失败，请重新操作！", 0).show();
        } else if (obj2.equals("no")) {
            Toast.makeText(this.c.a, "你已经对此帖子评价过，请勿重复评价，谢谢合作！", 0).show();
        }
    }
}
