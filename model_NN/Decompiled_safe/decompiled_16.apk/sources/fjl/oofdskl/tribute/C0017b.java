package fjl.oofdskl.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;
import fjl.oofdskl.tools.o;

/* renamed from: fjl.oofdskl.tribute.b  reason: case insensitive filesystem */
final class C0017b implements DialogInterface.OnKeyListener {
    private /* synthetic */ C0009a a;

    C0017b(C0009a aVar) {
        this.a = aVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        this.a.c.setClickable(true);
        this.a.c.setBackgroundDrawable(o.a(this.a.b, "discuss/tribute_login.png"));
        return false;
    }
}
