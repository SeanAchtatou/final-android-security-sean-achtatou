package fjl.oofdskl.tribute;

import android.view.MotionEvent;
import android.view.View;

final class H implements View.OnTouchListener {
    private /* synthetic */ F a;

    H(F f) {
        this.a = f;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (view.getId() == F.d(this.a)) {
                    view.setBackgroundDrawable(this.a.aj);
                    return false;
                } else if (view.getId() == this.a.r) {
                    view.setBackgroundDrawable(this.a.ak);
                    return false;
                } else if (view.getId() == this.a.s) {
                    view.setBackgroundDrawable(this.a.al);
                    return false;
                } else if (view.getId() == this.a.u) {
                    view.setBackgroundDrawable(this.a.am);
                    return false;
                } else if (view.getId() == this.a.t) {
                    view.setBackgroundDrawable(this.a.an);
                    return false;
                } else if (view.getId() == this.a.v) {
                    view.setBackgroundDrawable(this.a.ao);
                    return false;
                } else if (view.getId() != this.a.w) {
                    return false;
                } else {
                    view.setBackgroundDrawable(this.a.ap);
                    return false;
                }
            case 1:
                if (view.getId() == F.d(this.a)) {
                    view.setBackgroundDrawable(this.a.Z);
                    F.s(this.a);
                    return false;
                } else if (view.getId() == this.a.r) {
                    view.setBackgroundDrawable(this.a.ab);
                    F.u(this.a);
                    return false;
                } else if (view.getId() == this.a.s) {
                    view.setBackgroundDrawable(this.a.ac);
                    F.w(this.a);
                    return false;
                } else if (view.getId() == this.a.u) {
                    view.setBackgroundDrawable(this.a.ah);
                    this.a.b.cancel();
                    new L(this.a).start();
                    return false;
                } else if (view.getId() == this.a.t) {
                    view.setBackgroundDrawable(this.a.ad);
                    this.a.b();
                    this.a.a();
                    return false;
                } else if (view.getId() == this.a.v) {
                    view.setBackgroundDrawable(this.a.af);
                    new C0041z(this.a.a, view, "allnotes");
                    return false;
                } else if (view.getId() != this.a.w) {
                    return false;
                } else {
                    view.setBackgroundDrawable(this.a.aa);
                    F.F(this.a);
                    return false;
                }
            default:
                return false;
        }
    }
}
