package fjl.oofdskl.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: fjl.oofdskl.tribute.m  reason: case insensitive filesystem */
final class C0028m implements DialogInterface.OnKeyListener {
    private /* synthetic */ C0026k a;

    C0028m(C0026k kVar) {
        this.a = kVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        this.a.a();
        return true;
    }
}
