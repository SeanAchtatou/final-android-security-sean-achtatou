package fjl.oofdskl.tribute;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.AdapterView;

final class K implements AdapterView.OnItemClickListener {
    private /* synthetic */ F a;

    K(F f) {
        this.a = f;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.a.x.setImageDrawable((Drawable) this.a.V.get(i));
        this.a.S = new StringBuilder().append(i + 1).toString();
        F.F(this.a);
    }
}
