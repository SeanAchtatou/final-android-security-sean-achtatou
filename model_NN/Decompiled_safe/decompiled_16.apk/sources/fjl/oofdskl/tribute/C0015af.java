package fjl.oofdskl.tribute;

import android.graphics.Bitmap;
import android.widget.ImageView;
import fjl.oofdskl.a.d;
import fjl.oofdskl.tools.i;

/* renamed from: fjl.oofdskl.tribute.af  reason: case insensitive filesystem */
final class C0015af implements d {
    private /* synthetic */ C0013ad a;

    C0015af(C0013ad adVar) {
        this.a = adVar;
    }

    public final void a(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(i.a(bitmap, this.a.f));
    }
}
