package fjl.oofdskl.tribute;

import android.view.View;
import android.widget.AdapterView;
import java.util.Map;

final class C implements AdapterView.OnItemClickListener {
    private /* synthetic */ C0041z a;

    C(C0041z zVar) {
        this.a = zVar;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        new C0026k(this.a.a, (Map) this.a.g.get(i), this.a.q);
    }
}
