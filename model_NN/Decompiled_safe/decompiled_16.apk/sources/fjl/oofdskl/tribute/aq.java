package fjl.oofdskl.tribute;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.MotionEventCompat;
import android.widget.ListAdapter;
import android.widget.Toast;

final class aq extends Handler {
    private /* synthetic */ YyLt a;

    aq(YyLt yyLt) {
        this.a = yyLt;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, boolean):void
     arg types: [fjl.oofdskl.tribute.YyLt, int]
     candidates:
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, int):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, fjl.oofdskl.tribute.ad):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, java.util.List):void
      fjl.oofdskl.tribute.YyLt.a(fjl.oofdskl.tribute.YyLt, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.YyLt.b(fjl.oofdskl.tribute.YyLt, boolean):void
     arg types: [fjl.oofdskl.tribute.YyLt, int]
     candidates:
      fjl.oofdskl.tribute.YyLt.b(fjl.oofdskl.tribute.YyLt, int):void
      fjl.oofdskl.tribute.YyLt.b(fjl.oofdskl.tribute.YyLt, fjl.oofdskl.tribute.ad):void
      fjl.oofdskl.tribute.YyLt.b(fjl.oofdskl.tribute.YyLt, java.util.List):void
      fjl.oofdskl.tribute.YyLt.b(fjl.oofdskl.tribute.YyLt, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.YyLt.c(fjl.oofdskl.tribute.YyLt, boolean):void
     arg types: [fjl.oofdskl.tribute.YyLt, int]
     candidates:
      fjl.oofdskl.tribute.YyLt.c(fjl.oofdskl.tribute.YyLt, int):void
      fjl.oofdskl.tribute.YyLt.c(fjl.oofdskl.tribute.YyLt, java.util.List):void
      fjl.oofdskl.tribute.YyLt.c(fjl.oofdskl.tribute.YyLt, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.YyLt.d(fjl.oofdskl.tribute.YyLt, boolean):void
     arg types: [fjl.oofdskl.tribute.YyLt, int]
     candidates:
      fjl.oofdskl.tribute.YyLt.d(fjl.oofdskl.tribute.YyLt, java.util.List):void
      fjl.oofdskl.tribute.YyLt.d(fjl.oofdskl.tribute.YyLt, boolean):void */
    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                this.a.w = false;
                if (this.a.v) {
                    this.a.v = false;
                    this.a.c.addFooterView(this.a.j);
                }
                this.a.u = false;
                if (this.a.r == 2) {
                    this.a.s.setVisibility(8);
                    this.a.c.setVisibility(0);
                }
                if (this.a.y.equals("loadTribute")) {
                    this.a.d = new C0013ad(this.a, this.a.k, this.a.y);
                    this.a.c.setAdapter((ListAdapter) this.a.d);
                    this.a.d.notifyDataSetChanged();
                } else if (this.a.y.equals("loadSquare")) {
                    this.a.e = new C0013ad(this.a, this.a.m, this.a.y);
                    this.a.c.setAdapter((ListAdapter) this.a.e);
                    this.a.e.notifyDataSetChanged();
                }
                if (this.a.t) {
                    this.a.t = false;
                    this.a.g.c.setBackgroundDrawable(this.a.H);
                    return;
                }
                return;
            case 2:
                this.a.s.setVisibility(0);
                this.a.c.setVisibility(8);
                if (this.a.h.getVisibility() == 0) {
                    this.a.h.setVisibility(8);
                }
                if (this.a.i.getVisibility() == 0) {
                    this.a.i.setVisibility(8);
                    return;
                }
                return;
            case 3:
                if (this.a.v) {
                    this.a.v = false;
                    this.a.c.addFooterView(this.a.j);
                }
                this.a.u = false;
                if (this.a.y.equals("loadTribute")) {
                    this.a.d.notifyDataSetChanged();
                    return;
                } else if (this.a.y.equals("loadSquare")) {
                    this.a.e.notifyDataSetChanged();
                    return;
                } else {
                    return;
                }
            case 4:
                if (this.a.v) {
                    this.a.v = false;
                }
                this.a.u = false;
                if (this.a.r == 2) {
                    this.a.s.setVisibility(8);
                    this.a.c.setVisibility(0);
                }
                if (this.a.y.equals("loadTribute")) {
                    if (this.a.d == null) {
                        this.a.w = false;
                        this.a.d = new C0013ad(this.a, this.a.k, this.a.y);
                        this.a.c.setAdapter((ListAdapter) this.a.d);
                        this.a.d.notifyDataSetChanged();
                    }
                } else if (this.a.y.equals("loadSquare") && this.a.e == null) {
                    this.a.w = false;
                    this.a.e = new C0013ad(this.a, this.a.m, this.a.y);
                    this.a.c.setAdapter((ListAdapter) this.a.e);
                    this.a.e.notifyDataSetChanged();
                }
                if (this.a.t) {
                    this.a.t = false;
                    this.a.g.c.setBackgroundDrawable(this.a.H);
                }
                this.a.c.removeFooterView(this.a.j);
                this.a.v = true;
                return;
            case 5:
                if (this.a.t) {
                    this.a.t = false;
                    this.a.g.c.setBackgroundDrawable(this.a.H);
                }
                this.a.s.setVisibility(8);
                this.a.c.setVisibility(8);
                this.a.h.setVisibility(0);
                return;
            case 6:
            case MotionEventCompat.ACTION_HOVER_MOVE:
            default:
                return;
            case 8:
                if (this.a.t) {
                    this.a.t = false;
                    this.a.g.c.setBackgroundDrawable(this.a.H);
                }
                if (this.a.r == 1) {
                    this.a.s.setVisibility(8);
                    this.a.j.setVisibility(8);
                    this.a.c.setVisibility(8);
                    this.a.i.setVisibility(0);
                    return;
                }
                Toast.makeText(this.a, "加载失败,请检查网络设置！", 0).show();
                this.a.j.setVisibility(8);
                return;
        }
    }
}
