package fjl.oofdskl.tribute;

import android.view.MotionEvent;
import android.view.View;
import fjl.oofdskl.tools.r;

final class Z implements View.OnTouchListener {
    private /* synthetic */ X a;

    Z(X x) {
        this.a = x;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (r.H) {
            if (motionEvent.getAction() == 0) {
                this.a.b.setTextColor(-16776961);
            } else if (motionEvent.getAction() == 1) {
                this.a.b.setTextColor(-16777216);
                X.b(this.a);
            }
        }
        return true;
    }
}
