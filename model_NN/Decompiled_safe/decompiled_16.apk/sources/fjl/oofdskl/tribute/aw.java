package fjl.oofdskl.tribute;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class aw implements q {
    private Activity a;
    private /* synthetic */ YyLt b;

    public aw(YyLt yyLt, Activity activity) {
        this.b = yyLt;
        this.a = activity;
        if (o.b((Context) activity)) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (yyLt.y.equals("loadTribute")) {
                    jSONObject.put("para", r.k);
                    jSONObject.put("url", "registerAccount=" + r.z + "&appackageName=" + r.K + "&pagenumber=" + yyLt.r + "&pagesize=6");
                } else if (yyLt.y.equals("loadSquare")) {
                    jSONObject.put("para", r.s);
                    jSONObject.put("url", "registerAccount=" + r.z + "&pagenumber=" + yyLt.r + "&pagesize=6");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            r.ai = false;
            new o(yyLt.getApplication(), this, jSONObject.toString(), "discuss").start();
            return;
        }
        Toast.makeText(this.a, "网络不给力哦……请您连接网络", 0).show();
    }

    public final void a(Context context, Object obj) {
        int i = 0;
        if (!this.b.x && !r.H && !r.I) {
            if (this.b.f.b()) {
                new C0021f(context, 1);
                this.b.x = true;
                r.I = true;
            } else {
                YyLt.v(this.b);
            }
        }
        this.b.K.sendEmptyMessageDelayed(6, 2000);
        String obj2 = obj.toString();
        o.a(r.M, "result = " + obj2);
        try {
            JSONArray jSONArray = new JSONArray(obj2);
            int i2 = jSONArray.getJSONObject(jSONArray.length() - 1).getInt("flag");
            if (i2 == 0) {
                this.b.s.setVisibility(8);
                this.b.c.setVisibility(0);
                Toast.makeText(context, "加载失败！", 0).show();
                return;
            }
            if (this.b.y.equals("loadTribute")) {
                YyLt yyLt = this.b;
                String h = this.b.y;
                YyLt yyLt2 = this.b;
                yyLt.l = o.b(obj2, h);
                if (this.b.l != null) {
                    while (true) {
                        int i3 = i;
                        if (i3 >= this.b.l.size()) {
                            break;
                        }
                        this.b.k.add((Map) this.b.l.get(i3));
                        i = i3 + 1;
                    }
                    this.b.q = this.b.k.size();
                } else {
                    return;
                }
            } else if (this.b.y.equals("loadSquare")) {
                YyLt yyLt3 = this.b;
                String h2 = this.b.y;
                YyLt yyLt4 = this.b;
                yyLt3.n = o.b(obj2, h2);
                if (this.b.n != null) {
                    while (true) {
                        int i4 = i;
                        if (i4 >= this.b.n.size()) {
                            break;
                        }
                        this.b.m.add((Map) this.b.n.get(i4));
                        i = i4 + 1;
                    }
                    this.b.q = this.b.m.size();
                } else {
                    return;
                }
            }
            if (i2 != 2) {
                if (this.b.r == 1) {
                    YyLt yyLt5 = this.b;
                    yyLt5.r = yyLt5.r + 1;
                    this.b.J.sendEmptyMessage(1);
                } else if (this.b.r != 1) {
                    YyLt yyLt6 = this.b;
                    yyLt6.r = yyLt6.r + 1;
                    this.b.J.sendEmptyMessage(3);
                }
            } else if (this.b.q == 0) {
                this.b.J.sendEmptyMessage(5);
            } else {
                YyLt yyLt7 = this.b;
                yyLt7.r = yyLt7.r + 1;
                this.b.J.sendEmptyMessage(4);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            this.b.J.sendEmptyMessage(8);
        }
    }
}
