package fjl.oofdskl.tribute;

import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;

final class aG implements View.OnTouchListener {
    private /* synthetic */ aA a;

    private aG(aA aAVar) {
        this.a = aAVar;
    }

    /* synthetic */ aG(aA aAVar, byte b) {
        this(aAVar);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case 33333:
                if (motionEvent.getAction() == 0) {
                    this.a.i.setBackgroundDrawable(new BitmapDrawable(this.a.l));
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    this.a.i.setBackgroundDrawable(new BitmapDrawable(this.a.k));
                    return false;
                }
            case 44444:
                if (motionEvent.getAction() == 0) {
                    this.a.h.setBackgroundDrawable(new BitmapDrawable(this.a.l));
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    this.a.h.setBackgroundDrawable(new BitmapDrawable(this.a.k));
                    return false;
                }
            default:
                return false;
        }
    }
}
