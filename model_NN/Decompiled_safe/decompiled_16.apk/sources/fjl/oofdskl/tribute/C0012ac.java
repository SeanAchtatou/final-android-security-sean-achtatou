package fjl.oofdskl.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import fjl.oofdskl.tools.o;

/* renamed from: fjl.oofdskl.tribute.ac  reason: case insensitive filesystem */
final class C0012ac extends BroadcastReceiver {
    private /* synthetic */ X a;

    private C0012ac(X x) {
        this.a = x;
    }

    /* synthetic */ C0012ac(X x, byte b) {
        this(x);
    }

    public final void onReceive(Context context, Intent intent) {
        this.a.a.setImageDrawable(o.a(o.b(this.a.e, "icon.dat", "icon/" + intent.getStringExtra("icon")), 15));
    }
}
