package fjl.oofdskl.tribute;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import fjl.oofdskl.a.a;
import fjl.oofdskl.tools.g;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Locale;

public class Ft extends Activity {
    private LinearLayout a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public Boolean c = false;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public ImageButton f;
    /* access modifiers changed from: private */
    public ImageButton g;
    /* access modifiers changed from: private */
    public BitmapDrawable h;
    /* access modifiers changed from: private */
    public BitmapDrawable i;
    /* access modifiers changed from: private */
    public BitmapDrawable j;
    /* access modifiers changed from: private */
    public BitmapDrawable k;
    /* access modifiers changed from: private */
    public BitmapDrawable l;
    /* access modifiers changed from: private */
    public BitmapDrawable m;
    /* access modifiers changed from: private */
    public BitmapDrawable n;
    /* access modifiers changed from: private */
    public BitmapDrawable o;
    /* access modifiers changed from: private */
    public boolean p = true;
    /* access modifiers changed from: private */
    public String q = null;
    private String r = null;
    /* access modifiers changed from: private */
    public String s = null;
    /* access modifiers changed from: private */
    public String t = null;
    /* access modifiers changed from: private */
    public String u = null;
    /* access modifiers changed from: private */
    public Bitmap v = null;
    private C0040y w;
    /* access modifiers changed from: private */
    public boolean x = false;
    private String y = "";

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        boolean z;
        if (this.x) {
            if (i3 == -1) {
                if (!Environment.getExternalStorageState().equals("mounted")) {
                    o.a(r.M, "SD card is not avaiable/writeable right now.");
                    return;
                }
                StringBuilder sb = new StringBuilder();
                new DateFormat();
                String sb2 = sb.append((Object) DateFormat.format("yyyyMMdd_hhmmss", Calendar.getInstance(Locale.CHINA))).append(".jpg").toString();
                this.u = String.valueOf(r.aq) + sb2;
                this.v = (Bitmap) intent.getExtras().get("data");
                new C0034s(this, sb2).start();
                this.g.setBackgroundDrawable(new BitmapDrawable(this.v));
            }
        } else if (i3 == -1) {
            Uri data = intent.getData();
            Cursor managedQuery = managedQuery(data, new String[]{"_data"}, null, null, null);
            if (managedQuery == null) {
                Toast.makeText(this, "不好意思咯，获取图片路径失败，请重新选择！", 0).show();
                z = false;
            } else {
                int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
                managedQuery.moveToFirst();
                this.u = managedQuery.getString(columnIndexOrThrow);
                if (!a.c(this.u)) {
                    Toast.makeText(this, "请上传图片！", 0).show();
                    z = false;
                } else {
                    z = true;
                }
            }
            if (z && this.u != null) {
                try {
                    this.v = BitmapFactory.decodeStream(getContentResolver().openInputStream(data));
                    new C0035t(this).start();
                    ImageButton imageButton = this.f;
                    String str = this.u;
                    int i4 = r.ac / 3;
                    int i5 = r.ad / 4;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(str, options);
                    options.inJustDecodeBounds = false;
                    int i6 = options.outWidth;
                    int i7 = options.outHeight;
                    options.inSampleSize = (i6 <= i7 || i6 <= r.ae / 3) ? (i6 >= i7 || i7 <= r.af / 3) ? 1 : options.outHeight / i5 : options.outWidth / i4;
                    imageButton.setBackgroundDrawable(new BitmapDrawable(BitmapFactory.decodeFile(str, options)));
                } catch (FileNotFoundException e2) {
                    Log.e("Exception", e2.getMessage(), e2);
                }
            } else {
                return;
            }
        }
        super.onActivityResult(i2, i3, intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.tribute.Ft, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        Intent intent = getIntent();
        this.q = intent.getStringExtra("loadFlag");
        this.r = intent.getStringExtra("postOrReply");
        this.y = intent.getStringExtra("isScreenShotPost");
        if (this.r.equals("post")) {
            this.p = true;
        } else if (this.r.equals("reply")) {
            this.p = false;
            this.s = intent.getStringExtra("repliedWho");
            this.t = intent.getStringExtra("repliedNotesId");
        }
        this.h = o.a((Activity) this, "discuss/discuss_sure.png");
        this.i = o.a((Activity) this, "discuss/discuss_sure_clk.png");
        this.j = o.a((Activity) this, "discuss/top_back_0.png");
        this.k = o.a((Activity) this, "discuss/top_back_0_clk.png");
        this.l = o.a((Activity) this, "port/addimg.png");
        this.m = o.a((Activity) this, "port/addimg_clk.png");
        this.n = o.a((Activity) this, "port/cameraimg.png");
        this.o = o.a((Activity) this, "port/cameraimg_clk.png");
        this.a = new LinearLayout(this);
        this.a.setGravity(17);
        this.a.setGravity(48);
        this.a.setBackgroundColor(-1);
        this.a.setOrientation(1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        LinearLayout linearLayout = new LinearLayout(this.a.getContext());
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setPadding(0, 5, 0, 0);
        this.b = new g(this, this.q);
        linearLayout.addView(this.b);
        LinearLayout linearLayout2 = new LinearLayout(this.a.getContext());
        linearLayout2.setMinimumHeight(r.af / 14);
        linearLayout2.setBackgroundColor(Color.argb(150, 22, 70, 150));
        linearLayout2.setLayoutParams(layoutParams);
        linearLayout2.setOrientation(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(r.ae / 6, r.af / 18);
        layoutParams2.gravity = 17;
        layoutParams2.leftMargin = r.ae / 50;
        layoutParams2.rightMargin = r.ae / 50;
        this.d = new Button(linearLayout2.getContext());
        this.d.setId(55555);
        this.d.setLayoutParams(layoutParams2);
        this.d.setBackgroundDrawable(this.h);
        this.d.setOnTouchListener(new C0036u(this));
        this.e = new Button(linearLayout2.getContext());
        this.e.setLayoutParams(layoutParams2);
        this.e.setBackgroundDrawable(this.j);
        this.e.setId(55556);
        this.e.setOnTouchListener(new C0036u(this));
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams((r.ae * 44) / 75, -2);
        layoutParams3.gravity = 17;
        TextView textView = new TextView(this);
        textView.setTextSize(20.0f);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        textView.setGravity(17);
        textView.setLayoutParams(layoutParams3);
        if (this.q.equals("loadTribute")) {
            if (this.p) {
                textView.setText("撰写评论：" + r.L);
            } else {
                textView.setText("回复：" + this.s);
            }
        } else if (this.q.equals("loadSquare")) {
            if (this.p) {
                textView.setText("发表说说");
            } else {
                textView.setText("回复：" + this.s);
            }
        }
        linearLayout2.addView(this.e);
        linearLayout2.addView(textView);
        linearLayout2.addView(this.d);
        this.a.addView(linearLayout2);
        this.a.addView(linearLayout);
        if (this.q.equals("loadSquare") && this.p) {
            LinearLayout linearLayout3 = new LinearLayout(this);
            linearLayout3.setLayoutParams(layoutParams);
            linearLayout3.setOrientation(0);
            this.f = new ImageButton(this);
            this.f.setBackgroundDrawable(this.l);
            ViewGroup.LayoutParams layoutParams4 = new ViewGroup.LayoutParams(-2, -2);
            this.f.setId(2197);
            this.f.setOnTouchListener(new C0036u(this));
            RelativeLayout relativeLayout = new RelativeLayout(this);
            relativeLayout.setLayoutParams(layoutParams4);
            relativeLayout.setMinimumHeight(r.af / 4);
            relativeLayout.setMinimumWidth((r.af * 3) / 16);
            relativeLayout.addView(this.f, new ViewGroup.LayoutParams(r.ae / 3, -1));
            linearLayout3.addView(relativeLayout);
            this.g = new ImageButton(this);
            this.g.setBackgroundDrawable(this.n);
            this.g.setId(562790);
            this.g.setOnTouchListener(new C0036u(this));
            RelativeLayout relativeLayout2 = new RelativeLayout(this);
            relativeLayout2.setGravity(5);
            relativeLayout2.setLayoutParams(layoutParams4);
            relativeLayout2.setMinimumHeight(r.af / 4);
            relativeLayout2.setMinimumWidth((r.af * 3) / 16);
            relativeLayout2.addView(this.g, new ViewGroup.LayoutParams(r.ae / 3, -1));
            linearLayout3.addView(relativeLayout2);
            this.a.addView(linearLayout3);
        } else if (this.q.equals("loadTribute") && this.p && this.y.equals("isScreenShotPost")) {
            LinearLayout linearLayout4 = new LinearLayout(this);
            linearLayout4.setLayoutParams(layoutParams);
            linearLayout4.setOrientation(0);
            this.f = new ImageButton(this);
            this.u = "/mnt/sdcard/MyScreen/cut_img.jpg";
            ImageButton imageButton = this.f;
            Bitmap decodeFile = BitmapFactory.decodeFile(new File(this.u).getAbsolutePath());
            imageButton.setBackgroundDrawable(decodeFile != null ? new BitmapDrawable(decodeFile) : null);
            ViewGroup.LayoutParams layoutParams5 = new ViewGroup.LayoutParams(-2, -2);
            this.f.setId(2197);
            RelativeLayout relativeLayout3 = new RelativeLayout(this);
            relativeLayout3.setLayoutParams(layoutParams5);
            relativeLayout3.setMinimumHeight(r.af / 4);
            relativeLayout3.setMinimumWidth((r.af * 3) / 16);
            relativeLayout3.addView(this.f, new ViewGroup.LayoutParams(r.ae / 3, -1));
            relativeLayout3.setGravity(1);
            linearLayout4.addView(relativeLayout3);
            this.a.addView(linearLayout4);
        }
        setContentView(this.a);
        r.J = true;
        this.w = new C0040y(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("loadSquare");
        intentFilter.addAction("loadTribute");
        registerReceiver(this.w, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.w);
        if (this.u != null) {
            a.a(new File(r.aq));
        }
        r.J = false;
        this.x = false;
        this.u = null;
        if (this.f != null) {
            this.f.setImageBitmap(null);
            this.f.setBackgroundDrawable(null);
        }
        if (this.g != null) {
            this.g.setImageBitmap(null);
            this.g.setBackgroundDrawable(null);
        }
        if (this.v != null) {
            o.a(this.v);
        }
        o.a(this.h.getBitmap());
        o.a(this.i.getBitmap());
        o.a(this.j.getBitmap());
        o.a(this.k.getBitmap());
        o.a(this.l.getBitmap());
        o.a(this.m.getBitmap());
        o.a(this.n.getBitmap());
        o.a(this.o.getBitmap());
        super.onDestroy();
    }
}
