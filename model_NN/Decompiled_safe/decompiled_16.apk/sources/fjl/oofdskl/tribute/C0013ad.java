package fjl.oofdskl.tribute;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import fjl.oofdskl.a.a;
import fjl.oofdskl.tools.i;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.List;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.ad  reason: case insensitive filesystem */
public final class C0013ad extends BaseAdapter {
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public List b = null;
    private String c;
    private a d;
    private Bitmap e;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public Handler g = new C0014ae(this);

    public C0013ad(Activity activity, List list, String str) {
        this.a = activity;
        this.b = list;
        this.c = str;
        this.d = new a();
        this.e = o.a("bitmap/squareBitmap.png", this.a);
        if (r.ag >= 320) {
            this.f = 300;
            o.a(this.a, "port/portback.png");
            o.a(this.a, "port/portback_clk.png");
        } else if (r.ag >= 240 && r.ag < 320) {
            this.f = 280;
            o.a(this.a, "port/portback2.png");
            o.a(this.a, "port/portback_clk2.png");
        } else if (240 > r.ag && r.ag >= 180) {
            this.f = 200;
            o.a(this.a, "port/portback3.png");
            o.a(this.a, "port/portback_clk3.png");
        } else if (r.ag < 180 && r.ag > 120) {
            this.f = 160;
            o.a(this.a, "port/portback4.png");
            o.a(this.a, "port/portback_clk4.png");
        } else if (r.ag <= 120) {
            this.f = 130;
            o.a(this.a, "port/portback4.png");
            o.a(this.a, "port/portback_clk4.png");
        }
    }

    public final int getCount() {
        return this.b.size();
    }

    public final Object getItem(int i) {
        return this.b.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ah ahVar;
        if (view == null) {
            ah ahVar2 = new ah(this, (byte) 0);
            R r = new R(this.a);
            ahVar2.a = (TextView) r.getChildAt(0);
            ahVar2.b = (TextView) r.getChildAt(1);
            ahVar2.c = (TextView) r.getChildAt(2);
            ahVar2.d = (TextView) r.getChildAt(3);
            ahVar2.e = (ImageView) r.getChildAt(4);
            ahVar2.f = (Button) r.getChildAt(5);
            ahVar2.g = (T) r.getChildAt(6);
            ahVar2.h = (Q) r.getChildAt(7);
            r.setTag(ahVar2);
            view = r;
            ahVar = ahVar2;
        } else {
            ahVar = (ah) view.getTag();
        }
        ahVar.e.setTag(Integer.valueOf(i));
        if (!((Map) this.b.get(i)).get("loadBitmap").equals("")) {
            ahVar.e.setVisibility(0);
            ahVar.e.setImageBitmap(i.a((Bitmap) ((Map) this.b.get(i)).get("loadBitmap"), this.f));
            ahVar.e.setOnClickListener(new C0016ag(this));
        } else {
            String str = (String) ((Map) this.b.get(i)).get("imgUrl");
            if (!str.equals("")) {
                ahVar.e.setVisibility(0);
                ahVar.e.setOnClickListener(new C0016ag(this));
                if (new StringBuilder().append(str.charAt(0)).append(str.charAt(1)).toString().equals("sd")) {
                    String substring = str.substring(2);
                    if (substring.length() == 4) {
                        ((Map) this.b.get(i)).put("loadBitmap", "");
                    } else {
                        new BitmapFactory.Options().inSampleSize = 2;
                        Bitmap decodeFile = BitmapFactory.decodeFile(substring);
                        ahVar.e.setImageBitmap(i.a(decodeFile, this.f));
                        ((Map) this.b.get(i)).put("loadBitmap", decodeFile);
                    }
                } else {
                    Bitmap a2 = this.d.a(ahVar.e, str, new C0015af(this));
                    if (a2 == null) {
                        ahVar.e.setImageBitmap(i.a(this.e, this.f));
                        ((Map) this.b.get(i)).put("loadBitmap", "");
                    } else {
                        ahVar.e.setImageBitmap(i.a(a2, this.f));
                        ((Map) this.b.get(i)).put("loadBitmap", a2);
                    }
                }
            }
            ahVar.e.setVisibility(8);
        }
        if (this.c.equals("loadSquare")) {
            ahVar.g.setVisibility(0);
            ahVar.h.setVisibility(0);
            ahVar.g.setTag(Integer.valueOf(i));
            ahVar.h.setTag(Integer.valueOf(i));
            ahVar.g.setOnTouchListener(new aj(this, "support"));
            ahVar.h.setOnTouchListener(new aj(this, "nonsupport"));
            ahVar.g.a("+" + ((Integer) ((Map) this.b.get(i)).get("support")));
            ahVar.h.a("-" + ((Integer) ((Map) this.b.get(i)).get("nonsupport")));
        }
        ahVar.a.setText("  " + ((String) ((Map) this.b.get(i)).get("name")));
        if (!((Map) this.b.get(i)).get("who").equals("")) {
            ahVar.b.setText("");
        } else if (((Map) this.b.get(i)).get("rank").equals("")) {
            ahVar.b.setText("☞列兵");
        } else {
            ahVar.b.setText("☞" + ((Map) this.b.get(i)).get("rank"));
        }
        ahVar.c.setText(((Map) this.b.get(i)).get("date") + " " + ((Map) this.b.get(i)).get("time"));
        ahVar.d.setText(o.b((String) ((Map) this.b.get(i)).get("note")));
        ahVar.f.setText("回帖" + ((Map) this.b.get(i)).get("count"));
        ahVar.f.setTag(Integer.valueOf(i));
        ahVar.f.setOnTouchListener(new U(this.a, (Map) this.b.get(i), this.c));
        return view;
    }
}
