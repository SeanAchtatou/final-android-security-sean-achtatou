package fjl.oofdskl.tribute;

import android.os.Handler;
import android.os.Message;
import android.widget.ListAdapter;

/* renamed from: fjl.oofdskl.tribute.l  reason: case insensitive filesystem */
final class C0027l extends Handler {
    private /* synthetic */ C0026k a;

    C0027l(C0026k kVar) {
        this.a = kVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.k.a(fjl.oofdskl.tribute.k, boolean):void
     arg types: [fjl.oofdskl.tribute.k, int]
     candidates:
      fjl.oofdskl.tribute.k.a(fjl.oofdskl.tribute.k, int):void
      fjl.oofdskl.tribute.k.a(fjl.oofdskl.tribute.k, fjl.oofdskl.tribute.i):void
      fjl.oofdskl.tribute.k.a(fjl.oofdskl.tribute.k, java.util.List):void
      fjl.oofdskl.tribute.k.a(fjl.oofdskl.tribute.k, boolean):void */
    public final void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 1:
                this.a.q = false;
                if (this.a.o == 2) {
                    this.a.p.setVisibility(8);
                    this.a.g.setVisibility(0);
                }
                this.a.b = new C0024i(this.a.c, this.a.k);
                this.a.g.setAdapter((ListAdapter) this.a.b);
                this.a.b.notifyDataSetChanged();
                return;
            case 2:
                this.a.h.setVisibility(0);
                return;
            case 3:
                this.a.q = false;
                this.a.b.notifyDataSetChanged();
                return;
            case 4:
                this.a.q = false;
                if (this.a.b == null) {
                    this.a.b = new C0024i(this.a.c, this.a.k);
                    this.a.g.setAdapter((ListAdapter) this.a.b);
                }
                if (this.a.o == 2) {
                    this.a.p.setVisibility(8);
                    this.a.g.setVisibility(0);
                }
                this.a.b.notifyDataSetChanged();
                this.a.g.removeFooterView(this.a.h);
                return;
            default:
                return;
        }
    }
}
