package fjl.oofdskl.tribute;

import android.view.MotionEvent;
import android.view.View;
import fjl.oofdskl.tools.e;

final class au implements View.OnTouchListener {
    private /* synthetic */ YyLt a;

    au(YyLt yyLt) {
        this.a = yyLt;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            e.a.setBackgroundDrawable(this.a.F);
            return false;
        }
        YyLt.E(this.a);
        e.a.setBackgroundDrawable(this.a.G);
        return false;
    }
}
