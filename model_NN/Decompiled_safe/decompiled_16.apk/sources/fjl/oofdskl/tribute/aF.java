package fjl.oofdskl.tribute;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

final class aF implements View.OnFocusChangeListener {
    private /* synthetic */ aA a;

    private aF(aA aAVar) {
        this.a = aAVar;
    }

    /* synthetic */ aF(aA aAVar, byte b) {
        this(aAVar);
    }

    public final void onFocusChange(View view, boolean z) {
        String editable;
        switch (view.getId()) {
            case 1234:
                if (!z) {
                    String editable2 = ((EditText) view).getText().toString();
                    if (editable2 == null) {
                        this.a.i.setClickable(false);
                        Toast.makeText(this.a.a, "账号不能为空，请修改！", 0).show();
                        return;
                    }
                    this.a.i.setClickable(true);
                    new aD(this.a, this.a.a, editable2);
                    return;
                }
                return;
            case 13245:
                if (!z && (editable = ((EditText) view).getText().toString()) != null) {
                    if (editable.length() < 6) {
                        Toast.makeText(this.a.a, "密码过短，请修改！", 0).show();
                        return;
                    } else if (editable.length() > 18) {
                        Toast.makeText(this.a.a, "密码过长，请修改！", 0).show();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
