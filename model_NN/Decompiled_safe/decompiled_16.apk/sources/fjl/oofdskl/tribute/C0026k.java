package fjl.oofdskl.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import fjl.oofdskl.tools.b;
import fjl.oofdskl.tools.f;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.k  reason: case insensitive filesystem */
public final class C0026k extends Dialog implements AbsListView.OnScrollListener {
    private static C0026k a;
    /* access modifiers changed from: private */
    public C0024i b;
    /* access modifiers changed from: private */
    public Activity c;
    private RelativeLayout d;
    /* access modifiers changed from: private */
    public C0022g e;
    private C0033r f;
    /* access modifiers changed from: private */
    public ListView g = null;
    /* access modifiers changed from: private */
    public LinearLayout h;
    private C0031p i;
    /* access modifiers changed from: private */
    public Map j;
    /* access modifiers changed from: private */
    public List k = new ArrayList();
    /* access modifiers changed from: private */
    public List l = new ArrayList();
    private int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public int o = 1;
    /* access modifiers changed from: private */
    public f p;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public String r;
    private C0032q s;
    private boolean t = false;
    /* access modifiers changed from: private */
    public Handler u = new C0027l(this);
    private DialogInterface.OnKeyListener v = new C0028m(this);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected C0026k(Activity activity, Map map, String str) {
        super(activity, 16973829);
        boolean z = true;
        setOwnerActivity(activity);
        this.c = activity;
        this.j = map;
        this.r = str;
        a = this;
        HandlerThread handlerThread = new HandlerThread("follownote");
        handlerThread.start();
        this.i = new C0031p(this, handlerThread.getLooper());
        this.d = new RelativeLayout(this.c);
        this.d.setBackgroundColor(-1);
        this.d.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(3, 4369);
        layoutParams.addRule(2, 209715);
        this.h = new b(this.c);
        this.p = new f(this.c);
        this.p.setGravity(17);
        this.p.setLayoutParams(layoutParams);
        this.f = new C0033r(this.c, this.j);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(10);
        this.f.setId(4369);
        this.f.setLayoutParams(layoutParams2);
        this.g = new ListView(this.c);
        this.g.setLayoutParams(layoutParams);
        this.g.setScrollingCacheEnabled(false);
        this.g.setDivider(o.a(this.c, "bitmap/dividerH.png"));
        this.g.setBackgroundColor(-1);
        this.g.addFooterView(this.h);
        this.g.setOnScrollListener(this);
        this.e = new C0022g(this.c, this.j);
        this.e.setId(209715);
        this.e.setGravity(80);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, (r.af * 2) / 25);
        layoutParams3.addRule(12);
        this.e.setLayoutParams(layoutParams3);
        this.e.b.setOnTouchListener(new U(this.c, this.j, this.r));
        this.d.addView(this.f);
        this.d.addView(this.g);
        this.d.addView(this.p);
        this.d.addView(this.e);
        this.e.a.setOnClickListener(new C0029n(this));
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setCanceledOnTouchOutside(false);
        setOnKeyListener(this.v);
        show();
        setContentView(this.d);
        this.s = new C0032q(this, (byte) 0);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("postReplyNoteSucess");
        this.c.registerReceiver(this.s, intentFilter);
        this.t = this.t ? false : z;
        this.i.sendEmptyMessage(0);
    }

    public final void a() {
        if (this.t) {
            this.t = !this.t;
            this.c.unregisterReceiver(this.s);
        }
        a.cancel();
    }

    public final void onScroll(AbsListView absListView, int i2, int i3, int i4) {
        this.m = (i2 + i3) - 1;
    }

    public final void onScrollStateChanged(AbsListView absListView, int i2) {
        if (this.m == this.n && i2 == 0 && !this.q) {
            this.q = true;
            this.i.sendEmptyMessage(0);
        }
    }
}
