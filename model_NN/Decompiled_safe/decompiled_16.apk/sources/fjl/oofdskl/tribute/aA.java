package fjl.oofdskl.tribute;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.i;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;

public final class aA extends Dialog {
    private static Dialog g;
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public View b = null;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private LinearLayout f;
    /* access modifiers changed from: private */
    public Button h;
    /* access modifiers changed from: private */
    public Button i;
    private Bitmap j;
    /* access modifiers changed from: private */
    public Bitmap k;
    /* access modifiers changed from: private */
    public Bitmap l;
    private Bitmap m;
    private int n;
    private ImageView o;
    private DialogInterface.OnKeyListener p = new aB(this);
    private final TextWatcher q = new aC(this);

    public aA(Activity activity) {
        super(activity);
        this.a = activity;
        a();
        c();
        b();
    }

    public aA(Activity activity, View view) {
        super(activity);
        this.a = activity;
        this.b = view;
        view.setClickable(false);
        a();
        c();
        b();
    }

    private void a() {
        this.j = o.a("bitmap/regester1.png", this.a);
        this.k = o.a("bitmap/regesterbtn2-1.png", this.a);
        this.l = o.a("bitmap/regesterbtn1-1.png", this.a);
        if (r.ag >= 320) {
            this.n = 100;
            new i(this.a);
            this.m = i.a(o.a("bitmap/regesterdiver.png", this.a), r.ae, 5);
        } else if (r.ag >= 240 && r.ag < 320) {
            new i(this.a);
            this.j = i.a(this.j, (this.j.getWidth() * 3) / 4, (this.j.getHeight() * 3) / 4);
            new i(this.a);
            this.k = i.a(this.k, (this.k.getWidth() * 3) / 4, (this.k.getHeight() * 3) / 4);
            new i(this.a);
            this.l = i.a(this.l, (this.l.getWidth() * 3) / 4, (this.l.getHeight() * 3) / 4);
            new i(this.a);
            this.m = i.a(o.a("bitmap/regesterdiver.png", this.a), r.ae, 4);
            this.n = 80;
        } else if (240 > r.ag && r.ag >= 180) {
            new i(this.a);
            this.j = i.a(this.j, (this.j.getWidth() * 9) / 16, (this.j.getHeight() * 9) / 16);
            new i(this.a);
            this.k = i.a(this.k, (this.k.getWidth() * 9) / 16, (this.k.getHeight() * 9) / 16);
            new i(this.a);
            this.l = i.a(this.l, (this.l.getWidth() * 9) / 16, (this.l.getHeight() * 9) / 16);
            new i(this.a);
            this.m = i.a(o.a("bitmap/regesterdiver.png", this.a), r.ae, 3);
            this.n = 65;
        } else if (r.ag < 180 && r.ag > 120) {
            new i(this.a);
            this.j = i.a(this.j, (this.j.getWidth() * 27) / 64, (this.j.getHeight() * 27) / 64);
            new i(this.a);
            this.k = i.a(this.k, (this.k.getWidth() * 27) / 64, (this.k.getHeight() * 27) / 64);
            new i(this.a);
            this.l = i.a(this.l, (this.l.getWidth() * 27) / 64, (this.l.getHeight() * 27) / 64);
            new i(this.a);
            this.m = i.a(o.a("bitmap/regesterdiver.png", this.a), r.ae, 2);
            this.n = 50;
        } else if (r.ag <= 120) {
            new i(this.a);
            this.j = i.a(this.j, (this.j.getWidth() * 81) / 256, (this.j.getHeight() * 81) / 256);
            new i(this.a);
            this.k = i.a(this.k, (this.k.getWidth() * 81) / 256, (this.k.getHeight() * 81) / 256);
            new i(this.a);
            this.l = i.a(this.l, (this.l.getWidth() * 81) / 256, (this.l.getHeight() * 81) / 256);
            new i(this.a);
            this.m = i.a(o.a("bitmap/regesterdiver.png", this.a), r.ae, 1);
            this.n = 40;
        }
    }

    private void b() {
        Dialog dialog = new Dialog(this.a);
        g = dialog;
        dialog.setOnKeyListener(this.p);
        g.setCanceledOnTouchOutside(false);
        g.requestWindowFeature(1);
        g.show();
        g.setContentView(this.f);
        g.getWindow().setBackgroundDrawable(new BitmapDrawable(this.j));
    }

    private void c() {
        this.f = new LinearLayout(this.a);
        this.f.setOrientation(1);
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, (r.af * 3) / 4));
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
        LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        TextView textView = new TextView(this.a);
        textView.setText("     游问友答注册");
        textView.setTextColor(Color.argb(150, 22, 70, 150));
        textView.setTextSize(25.0f);
        linearLayout.addView(textView, new ViewGroup.LayoutParams(-2, -2));
        LinearLayout linearLayout2 = new LinearLayout(this.a);
        linearLayout2.setLayoutParams(layoutParams);
        this.o = new ImageView(this.a);
        this.o.setImageBitmap(this.m);
        this.o.setAdjustViewBounds(true);
        this.o.setMaxWidth(r.ae);
        linearLayout2.addView(this.o, new ViewGroup.LayoutParams(-1, -2));
        LinearLayout linearLayout3 = new LinearLayout(this.f.getContext());
        linearLayout3.setPadding(0, 2, 0, 0);
        linearLayout3.setLayoutParams(layoutParams);
        linearLayout3.setOrientation(0);
        TextView textView2 = new TextView(linearLayout3.getContext());
        textView2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView2.setText("  账 号");
        textView2.setTextSize(25.0f);
        this.c = new EditText(linearLayout3.getContext());
        this.c.setBackgroundDrawable(o.a(this.a, "bitmap/followNotepic.png"));
        this.c.setHint("请输入6-15个字符账号");
        this.c.setTextColor(-16777216);
        this.c.requestFocus();
        this.c.setId(1234);
        this.c.addTextChangedListener(this.q);
        this.c.setOnFocusChangeListener(new aF(this, (byte) 0));
        this.c.setSingleLine(true);
        this.c.setLayoutParams(new ViewGroup.LayoutParams((r.ae * 11) / 16, -2));
        linearLayout3.addView(textView2);
        linearLayout3.addView(this.c);
        PasswordTransformationMethod instance = PasswordTransformationMethod.getInstance();
        LinearLayout linearLayout4 = new LinearLayout(this.f.getContext());
        linearLayout4.setPadding(0, 2, 0, 0);
        linearLayout4.setLayoutParams(layoutParams);
        linearLayout4.setOrientation(0);
        TextView textView3 = new TextView(linearLayout4.getContext());
        textView3.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView3.setText("  密 码");
        textView3.setTextSize(25.0f);
        this.d = new EditText(linearLayout4.getContext());
        this.d.setBackgroundDrawable(o.a(this.a, "bitmap/followNotepic.png"));
        this.d.setSingleLine(true);
        this.d.setHint("请输入6-15个字符密码");
        this.d.setTextColor(-16777216);
        this.d.setTransformationMethod(instance);
        this.d.setId(13245);
        this.d.addTextChangedListener(this.q);
        this.d.setOnFocusChangeListener(new aF(this, (byte) 0));
        this.d.setLayoutParams(new ViewGroup.LayoutParams((r.ae * 11) / 16, -2));
        linearLayout4.addView(textView3);
        linearLayout4.addView(this.d);
        LinearLayout linearLayout5 = new LinearLayout(this.f.getContext());
        linearLayout5.setPadding(0, 2, 0, 0);
        linearLayout5.setLayoutParams(layoutParams);
        linearLayout5.setOrientation(0);
        TextView textView4 = new TextView(linearLayout5.getContext());
        textView4.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        textView4.setText("  密 码");
        textView4.setTextSize(25.0f);
        this.e = new EditText(linearLayout5.getContext());
        this.e.setBackgroundDrawable(o.a(this.a, "bitmap/followNotepic.png"));
        this.e.setSingleLine(true);
        this.e.setHint("请确认密码");
        this.e.setTextColor(-16777216);
        this.e.setTransformationMethod(instance);
        this.e.addTextChangedListener(this.q);
        this.e.setId(12346);
        this.e.setLayoutParams(new ViewGroup.LayoutParams((r.ae * 11) / 16, -2));
        linearLayout5.addView(textView4);
        linearLayout5.addView(this.e);
        LinearLayout linearLayout6 = new LinearLayout(linearLayout4.getContext());
        linearLayout6.setPadding(0, 3, 0, 0);
        linearLayout6.setLayoutParams(layoutParams);
        linearLayout6.setOrientation(0);
        this.i = new Button(linearLayout6.getContext());
        this.i.setLayoutParams(new LinearLayout.LayoutParams((r.ae * 5) / 16, -2));
        this.i.setText("提交");
        this.i.setId(33333);
        this.i.setBackgroundDrawable(new BitmapDrawable(this.k));
        this.i.setOnClickListener(new aE(this, (byte) 0));
        this.i.setOnTouchListener(new aG(this, (byte) 0));
        this.h = new Button(linearLayout6.getContext());
        this.h.setLayoutParams(new LinearLayout.LayoutParams((r.ae * 5) / 16, -2));
        this.h.setText("取消");
        this.h.setId(44444);
        this.h.setBackgroundDrawable(new BitmapDrawable(this.k));
        this.h.setOnClickListener(new aE(this, (byte) 0));
        this.h.setOnTouchListener(new aG(this, (byte) 0));
        View view = new View(this.a);
        View view2 = new View(this.a);
        View view3 = new View(this.a);
        linearLayout6.addView(view, new ViewGroup.LayoutParams((r.ae * 6) / 35, this.n));
        linearLayout6.addView(this.i);
        linearLayout6.addView(view2, new ViewGroup.LayoutParams((r.ae * 3) / 35, this.n));
        linearLayout6.addView(this.h);
        linearLayout6.addView(view3, new ViewGroup.LayoutParams((r.ae * 4) / 35, this.n));
        this.f.addView(linearLayout);
        this.f.addView(linearLayout2);
        this.f.addView(linearLayout3);
        this.f.addView(linearLayout4);
        this.f.addView(linearLayout5);
        this.f.addView(linearLayout6);
    }

    static /* synthetic */ void f(aA aAVar) {
        if (aAVar.b != null) {
            aAVar.b.setClickable(true);
            if (aAVar.b.getId() == 9999) {
                aAVar.b.setBackgroundDrawable(o.a(aAVar.a, "discuss/register01.png"));
            }
        }
        g.cancel();
        g.getWindow().setBackgroundDrawable(null);
        aAVar.i.setBackgroundDrawable(null);
        aAVar.h.setBackgroundDrawable(null);
        aAVar.o.setImageBitmap(null);
        o.a(aAVar.j);
        o.a(aAVar.k);
        o.a(aAVar.l);
        o.a(aAVar.m);
    }
}
