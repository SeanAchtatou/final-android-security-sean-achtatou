package fjl.oofdskl.tribute;

import android.graphics.BitmapFactory;
import fjl.oofdskl.a.a;
import fjl.oofdskl.tools.r;

/* renamed from: fjl.oofdskl.tribute.t  reason: case insensitive filesystem */
final class C0035t extends Thread {
    private /* synthetic */ Ft a;

    C0035t(Ft ft) {
        this.a = ft;
    }

    public final void run() {
        int i = 1;
        String str = r.aq;
        String substring = this.a.u.substring(this.a.u.lastIndexOf("/") + 1);
        String n = this.a.u;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(n, options);
        options.inJustDecodeBounds = false;
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        float f = (float) r.af;
        float f2 = (float) r.ae;
        int i4 = (i2 <= i3 || ((float) i2) <= f2) ? (i2 >= i3 || ((float) i3) <= f) ? 1 : (int) (((float) options.outHeight) / f) : (int) (((float) options.outWidth) / f2);
        if (i4 > 0) {
            i = i4;
        }
        options.inSampleSize = i;
        a.a(str, substring, a.a(BitmapFactory.decodeFile(n, options), 100));
    }
}
