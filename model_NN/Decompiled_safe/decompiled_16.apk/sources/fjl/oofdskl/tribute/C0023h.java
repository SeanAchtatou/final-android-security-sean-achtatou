package fjl.oofdskl.tribute;

import android.app.Activity;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.TextView;
import fjl.oofdskl.tools.o;

/* renamed from: fjl.oofdskl.tribute.h  reason: case insensitive filesystem */
public final class C0023h extends LinearLayout {
    private Activity a;

    public C0023h(Activity activity) {
        super(activity);
        this.a = activity;
        setMinimumHeight(60);
        setOrientation(1);
        setBackgroundDrawable(o.a(this.a, "port/itemback.png"));
        TextView textView = new TextView(this.a);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        textView.setTextSize(20.0f);
        textView.setTextColor(Color.argb(150, 22, 70, 150));
        addView(textView);
        TextView textView2 = new TextView(this.a);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView2.setTextSize(20.0f);
        addView(textView2);
    }
}
