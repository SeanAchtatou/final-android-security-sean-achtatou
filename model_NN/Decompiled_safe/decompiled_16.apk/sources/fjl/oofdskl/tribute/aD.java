package fjl.oofdskl.tribute;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import java.net.URLEncoder;
import org.json.JSONObject;

final class aD implements q {
    private /* synthetic */ aA a;

    public aD(aA aAVar, Context context, String str) {
        this.a = aAVar;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.f);
            jSONObject.put("url", "registerAccount=" + URLEncoder.encode(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
        r.ai = false;
        new o(context, this, jSONObject.toString(), "discuss").start();
    }

    public aD(aA aAVar, Context context, String str, String str2) {
        this.a = aAVar;
        r.z = str;
        r.A = str2;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.g);
            jSONObject.put("url", "registerAccount=" + URLEncoder.encode(str) + "&registerPassword=" + str2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        r.ai = false;
        new o(context, this, jSONObject.toString(), "discuss").start();
    }

    public final void a(Context context, Object obj) {
        String obj2 = obj.toString();
        if (obj2.equals("1")) {
            Toast.makeText(this.a.a, "此账号已存在！", 0).show();
        } else if (obj2.equals("0")) {
            Toast.makeText(this.a.a, "此账号可用！", 0).show();
        } else if (obj2.equals("Perfect!注册成功!")) {
            Intent intent = new Intent("Registersucess");
            intent.putExtra("name", r.z);
            intent.putExtra("password", r.A);
            this.a.a.sendBroadcast(intent);
            aA.f(this.a);
        }
    }
}
