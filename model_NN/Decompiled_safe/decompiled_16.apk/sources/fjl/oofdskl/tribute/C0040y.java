package fjl.oofdskl.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* renamed from: fjl.oofdskl.tribute.y  reason: case insensitive filesystem */
final class C0040y extends BroadcastReceiver {
    private /* synthetic */ Ft a;

    private C0040y(Ft ft) {
        this.a = ft;
    }

    /* synthetic */ C0040y(Ft ft, byte b) {
        this(ft);
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("loadSquare") || intent.getAction().equals("loadTribute")) {
            this.a.finish();
        }
    }
}
