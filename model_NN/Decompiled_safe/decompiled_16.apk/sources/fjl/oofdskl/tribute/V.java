package fjl.oofdskl.tribute;

import android.app.Activity;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import fjl.oofdskl.tools.o;

public final class V extends LinearLayout {
    public ImageButton a = new ImageButton(this.d);
    public ImageButton b;
    public ImageButton c;
    /* access modifiers changed from: private */
    public Activity d;

    public V(Activity activity) {
        super(activity);
        this.d = activity;
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.a.setId(7777);
        this.a.setBackgroundDrawable(o.a(this.d, "discuss/tribute_post.png"));
        addView(this.a, new LinearLayout.LayoutParams(-2, -1, 1.0f));
        this.b = new ImageButton(this.d);
        this.b.setBackgroundDrawable(o.a(this.d, "discuss/tribute_more.png"));
        this.b.setOnTouchListener(new W(this));
        addView(this.b, new LinearLayout.LayoutParams(-2, -1, 4.0f));
        this.c = new ImageButton(this.d);
        this.c.setBackgroundDrawable(o.a(this.d, "discuss/tribute_refresh.png"));
        addView(this.c, new LinearLayout.LayoutParams(-2, -1, 1.0f));
    }
}
