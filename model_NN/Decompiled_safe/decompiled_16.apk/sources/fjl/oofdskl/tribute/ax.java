package fjl.oofdskl.tribute;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class ax extends Handler {
    private /* synthetic */ YyLt a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ax(YyLt yyLt, Looper looper) {
        super(looper);
        this.a = yyLt;
    }

    public final void handleMessage(Message message) {
        synchronized (this) {
            switch (message.what) {
                case 0:
                    if (this.a.r == 1) {
                        this.a.J.sendEmptyMessageDelayed(2, 0);
                    }
                    new aw(this.a, this.a);
                    break;
            }
        }
    }
}
