package fjl.oofdskl.tribute;

import android.content.DialogInterface;
import android.view.KeyEvent;
import fjl.oofdskl.tools.o;

final class am implements DialogInterface.OnKeyListener {
    private /* synthetic */ ak a;

    am(ak akVar) {
        this.a = akVar;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        dialogInterface.cancel();
        this.a.f.setBackgroundDrawable(null);
        this.a.e.a();
        if (this.a.h.getBitmap() != null) {
            o.a(this.a.h.getBitmap());
        }
        if (this.a.i.getBitmap() == null) {
            return false;
        }
        o.a(this.a.i.getBitmap());
        return false;
    }
}
