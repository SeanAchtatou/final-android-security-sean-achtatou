package fjl.oofdskl.tribute;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ListAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: fjl.oofdskl.tribute.q  reason: case insensitive filesystem */
final class C0032q extends BroadcastReceiver {
    private /* synthetic */ C0026k a;

    private C0032q(C0026k kVar) {
        this.a = kVar;
    }

    /* synthetic */ C0032q(C0026k kVar, byte b) {
        this(kVar);
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("postReplyNoteSucess")) {
            ArrayList arrayList = new ArrayList();
            HashMap hashMap = new HashMap();
            hashMap.put("nameInfo", intent.getStringExtra("userNmae"));
            hashMap.put("noteInfo", intent.getStringExtra("noteInfo"));
            hashMap.put("userNum", "");
            arrayList.add(hashMap);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.a.k.size()) {
                    this.a.k.clear();
                    this.a.k = arrayList;
                    this.a.n = this.a.k.size();
                    this.a.b = new C0024i(this.a.c, this.a.k);
                    this.a.g.setAdapter((ListAdapter) this.a.b);
                    this.a.b.notifyDataSetChanged();
                    return;
                }
                arrayList.add((Map) this.a.k.get(i2));
                i = i2 + 1;
            }
        }
    }
}
