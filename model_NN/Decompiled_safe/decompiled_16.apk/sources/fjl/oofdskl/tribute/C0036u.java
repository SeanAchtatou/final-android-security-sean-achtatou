package fjl.oofdskl.tribute;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import fjl.oofdskl.c.b;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.util.HashMap;

/* renamed from: fjl.oofdskl.tribute.u  reason: case insensitive filesystem */
final class C0036u implements View.OnTouchListener {
    private /* synthetic */ Ft a;

    C0036u(Ft ft) {
        this.a = ft;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, java.lang.Boolean):void
     arg types: [fjl.oofdskl.tribute.Ft, int]
     candidates:
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, boolean):void
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, java.lang.Boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, boolean):void
     arg types: [fjl.oofdskl.tribute.Ft, int]
     candidates:
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, java.lang.Boolean):void
      fjl.oofdskl.tribute.Ft.a(fjl.oofdskl.tribute.Ft, boolean):void */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (view.getId() != 55555) {
                    if (view.getId() != 55556) {
                        if (view.getId() != 2197) {
                            if (view.getId() == 562790) {
                                this.a.g.setBackgroundDrawable(this.a.o);
                                break;
                            }
                        } else {
                            this.a.f.setBackgroundDrawable(this.a.m);
                            break;
                        }
                    } else {
                        this.a.e.setBackgroundDrawable(this.a.k);
                        break;
                    }
                } else {
                    this.a.d.setBackgroundDrawable(this.a.i);
                    break;
                }
                break;
            case 1:
                if (view.getId() != 55555) {
                    if (view.getId() != 55556) {
                        if (view.getId() != 2197) {
                            if (view.getId() == 562790) {
                                Toast.makeText(this.a, "你选择了拍照上传,更换上传方式，此次操作将失效！", 0).show();
                                this.a.g.setBackgroundDrawable(this.a.n);
                                this.a.x = true;
                                this.a.startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 1);
                                break;
                            }
                        } else {
                            Toast.makeText(this.a, "你选择了本地上传,更换上传方式，此次操作将失效！", 0).show();
                            this.a.f.setBackgroundDrawable(this.a.l);
                            this.a.x = false;
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction("android.intent.action.GET_CONTENT");
                            this.a.startActivityForResult(intent, 0);
                            break;
                        }
                    } else {
                        this.a.e.setBackgroundDrawable(this.a.j);
                        this.a.finish();
                        break;
                    }
                } else {
                    this.a.d.setBackgroundDrawable(this.a.h);
                    if (this.a.b.getText().toString().length() != 0) {
                        if (this.a.b.getText().toString().trim().length() != 0) {
                            if (!this.a.c.booleanValue()) {
                                this.a.c = (Boolean) true;
                                if (!this.a.q.equals("loadSquare") || !this.a.p) {
                                    if (this.a.q.equals("loadTribute") && this.a.p) {
                                        HashMap hashMap = new HashMap();
                                        hashMap.put("registerAccount", r.z);
                                        hashMap.put("appnoteInfo", this.a.b.getText().toString());
                                        hashMap.put("uuid", o.b());
                                        hashMap.put("appackageName", r.K);
                                        hashMap.put("imei", r.M);
                                        hashMap.put("imsi", r.N);
                                        (this.a.u == null ? new b(this.a, null, r.ap, hashMap, "tribute") : new b(this.a, this.a.u, r.ap, hashMap, "tribute")).start();
                                        break;
                                    } else {
                                        new C0037v(this.a, this.a);
                                        break;
                                    }
                                } else {
                                    HashMap hashMap2 = new HashMap();
                                    hashMap2.put("registerAccount", r.z);
                                    hashMap2.put("sdetail", this.a.b.getText().toString());
                                    hashMap2.put("uuid", o.b());
                                    (this.a.u == null ? new b(this.a, null, r.ao, hashMap2, "square") : new b(this.a, String.valueOf(r.aq) + this.a.u.substring(this.a.u.lastIndexOf("/") + 1), r.ao, hashMap2, "square")).start();
                                    break;
                                }
                            }
                        } else {
                            Toast.makeText(this.a, "信息不能全为空格……", 0).show();
                            break;
                        }
                    } else {
                        Toast.makeText(this.a, "信息不能为空……", 0).show();
                        break;
                    }
                }
                break;
        }
        return false;
    }
}
