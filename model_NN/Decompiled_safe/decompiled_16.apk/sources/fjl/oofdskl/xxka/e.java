package fjl.oofdskl.xxka;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;

public final class e {
    private String a;
    private Intent b;
    private BitmapDrawable c;

    public e(String str, BitmapDrawable bitmapDrawable, Intent intent) {
        this.a = str;
        this.b = intent;
        this.c = bitmapDrawable;
    }

    public final String a() {
        return this.a;
    }

    public final Intent b() {
        return this.b;
    }

    public final BitmapDrawable c() {
        return this.c;
    }
}
