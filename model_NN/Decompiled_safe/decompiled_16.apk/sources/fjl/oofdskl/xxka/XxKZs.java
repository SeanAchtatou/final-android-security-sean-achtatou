package fjl.oofdskl.xxka;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import fjl.oofdskl.lbiao.YyLb;
import fjl.oofdskl.tools.c;
import fjl.oofdskl.tools.o;
import java.util.ArrayList;
import java.util.List;

public class XxKZs extends FuActivity {
    private List c;
    private TabWidget d;
    private TabHost e;
    private BitmapDrawable f;
    private BitmapDrawable g;

    /* access modifiers changed from: protected */
    public final String a(int i) {
        return ((e) this.c.get(i)).a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.xxka.XxKZs, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public final void a() {
        this.f = o.a((Activity) this, "tabhost/cursor1.png");
        this.g = o.a((Activity) this, "tabhost/cursor1.png");
        Intent intent = new Intent(this, YyLb.class);
        intent.putExtra("load", "loadApp");
        Intent intent2 = new Intent(this, YyLb.class);
        intent2.putExtra("load", "loadGame");
        e eVar = new e("应用", this.f, intent);
        e eVar2 = new e("游戏", this.g, intent2);
        this.c = new ArrayList();
        this.c.add(eVar);
        this.c.add(eVar2);
        this.d = getTabWidget();
        this.e = getTabHost();
    }

    /* access modifiers changed from: protected */
    public final void a(TextView textView, ImageView imageView, int i) {
        textView.setText(((e) this.c.get(i)).a());
        textView.setTextColor(-16776961);
        textView.setTextSize(25.0f);
        imageView.setBackgroundDrawable(((e) this.c.get(i)).c());
    }

    /* access modifiers changed from: protected */
    public final Intent b(int i) {
        return ((e) this.c.get(i)).b();
    }

    /* access modifiers changed from: protected */
    public final View b() {
        c cVar = new c(this);
        cVar.a("APP精品推荐");
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.c.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.xxka.XxKZs, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i = 0;
        super.onCreate(bundle);
        c(0);
        while (true) {
            int i2 = i;
            if (i2 < this.d.getChildCount()) {
                View childAt = this.d.getChildAt(i2);
                if (i2 == this.e.getCurrentTab()) {
                    ((TextView) a.get(i2)).setTextColor(-16776961);
                    childAt.setBackgroundDrawable(o.a((Activity) this, "tabhost/titleback.png"));
                } else {
                    ((TextView) a.get(i2)).setTextColor(-7829368);
                    ((ImageView) b.get(i2)).setVisibility(8);
                    childAt.setBackgroundDrawable(o.a((Activity) this, "tabhost/titleback.png"));
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        o.a(this.f.getBitmap());
        o.a(this.g.getBitmap());
        sendBroadcast(new Intent("AppListActivityDestroy"));
    }
}
