package fjl.oofdskl.xxka;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tribute.YyLt;
import java.util.ArrayList;
import java.util.List;

public class XxKNrTj extends LtActy {
    public static XxKNrTj c;
    private List d;
    private TabWidget e;
    private TabHost f;
    private BitmapDrawable g;
    private BitmapDrawable h;

    /* access modifiers changed from: protected */
    public final String a(int i) {
        return ((e) this.d.get(i)).a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.xxka.XxKNrTj, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public final void a() {
        this.g = o.a((Activity) this, "tabhost/cursor.png");
        this.h = o.a((Activity) this, "tabhost/cursor.png");
        Intent intent = new Intent(this, YyLt.class);
        intent.addFlags(67108864);
        intent.putExtra("load", "loadTribute");
        Intent intent2 = new Intent(this, YyLt.class);
        intent2.addFlags(67108864);
        intent2.putExtra("load", "loadSquare");
        e eVar = new e("游戏论坛", this.g, intent);
        e eVar2 = new e("说说天地", this.h, intent2);
        this.d = new ArrayList();
        this.d.add(eVar);
        this.d.add(eVar2);
        this.e = getTabWidget();
        this.f = getTabHost();
    }

    /* access modifiers changed from: protected */
    public final void a(TextView textView, ImageView imageView, int i) {
        textView.setText(((e) this.d.get(i)).a());
        textView.setTextColor(-16777216);
        textView.setTextSize(25.0f);
        imageView.setBackgroundDrawable(((e) this.d.get(i)).c());
    }

    /* access modifiers changed from: protected */
    public final Intent b(int i) {
        return ((e) this.d.get(i)).b();
    }

    /* access modifiers changed from: protected */
    public final View b() {
        return new RelativeLayout(this);
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return this.d.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable
     arg types: [fjl.oofdskl.xxka.XxKNrTj, java.lang.String]
     candidates:
      fjl.oofdskl.tools.o.a(java.lang.String, android.content.Context):android.graphics.Bitmap
      fjl.oofdskl.tools.o.a(android.graphics.Bitmap, int):android.graphics.drawable.BitmapDrawable
      fjl.oofdskl.tools.o.a(android.content.SharedPreferences, int):java.util.Map
      fjl.oofdskl.tools.o.a(android.content.Context, java.lang.String):void
      fjl.oofdskl.tools.o.a(java.lang.String, java.lang.String):void
      fjl.oofdskl.tools.o.a(android.app.Activity, java.lang.String):android.graphics.drawable.BitmapDrawable */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i = 0;
        super.onCreate(bundle);
        c = this;
        c(0);
        while (true) {
            int i2 = i;
            if (i2 < this.e.getChildCount()) {
                View childAt = this.e.getChildAt(i2);
                if (i2 == this.f.getCurrentTab()) {
                    ((TextView) a.get(i2)).setTextColor(-16777216);
                    childAt.setBackgroundDrawable(o.a((Activity) this, "tabhost/titleback.png"));
                } else {
                    ((TextView) a.get(i2)).setTextColor(-7829368);
                    ((ImageView) b.get(i2)).setVisibility(8);
                    childAt.setBackgroundDrawable(o.a((Activity) this, "tabhost/titleback.png"));
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        c = null;
        o.a(this.g.getBitmap());
        o.a(this.h.getBitmap());
        sendBroadcast(new Intent("AppDiscussDestroy"));
    }
}
