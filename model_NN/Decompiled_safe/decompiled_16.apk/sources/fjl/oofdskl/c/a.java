package fjl.oofdskl.c;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class a {
    private InputStream a;
    private File b;
    private String c;
    private String d;
    private String e = "application/octet-stream";

    public a(String str, File file, String str2, String str3) {
        this.c = str;
        this.d = str2;
        this.b = file;
        try {
            this.a = new FileInputStream(file);
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        if (str3 != null) {
            this.e = str3;
        }
    }

    public final File a() {
        return this.b;
    }

    public final InputStream b() {
        return this.a;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }
}
