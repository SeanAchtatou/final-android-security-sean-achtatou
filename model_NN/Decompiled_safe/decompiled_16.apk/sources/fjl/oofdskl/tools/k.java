package fjl.oofdskl.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public final class k extends ImageView {
    private int a = 0;
    private float b;
    private float c;
    private float d = 0.06f;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private TranslateAnimation k;
    private int l;
    private int m;
    private Bitmap n;
    private float o = 2.0f;
    private float p = 1.0f;
    private int q = 0;
    private float r = 0.5f;
    private float s = 0.5f;
    private int t = 3;
    private boolean u = false;
    private float[] v;

    public k(Context context, int i2, int i3) {
        super(context);
        setPadding(0, 0, 0, 0);
        this.e = i2;
        this.f = i3;
    }

    private static float a(MotionEvent motionEvent) {
        float x = motionEvent.getX(0) - motionEvent.getX(1);
        float y = motionEvent.getY(0) - motionEvent.getY(1);
        return FloatMath.sqrt((x * x) + (y * y));
    }

    private void a(int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        if (i2 == 3) {
            i5 = getLeft() - ((int) (this.v[0] * ((float) getWidth())));
            i4 = getTop() - ((int) (this.v[1] * ((float) getHeight())));
            i3 = getRight() + ((int) ((this.d - this.v[0]) * ((float) getWidth())));
            i6 = getBottom() + ((int) ((this.d - this.v[1]) * ((float) getHeight())));
        } else if (i2 == 4) {
            i5 = getLeft() + ((int) (this.v[0] * ((float) getWidth())));
            i4 = ((int) (this.v[1] * ((float) getHeight()))) + getTop();
            i3 = getRight() - ((int) ((this.d - this.v[0]) * ((float) getWidth())));
            i6 = getBottom() - ((int) ((this.d - this.v[1]) * ((float) getHeight())));
        } else {
            i3 = 0;
            i4 = 0;
            i5 = 0;
        }
        setFrame(i5, i4, i3, i6);
    }

    /* access modifiers changed from: private */
    public void c() {
        int i2 = 0;
        int height = getHeight() < this.f ? ((this.f - getHeight()) / 2) - getTop() : 0;
        if (getWidth() < this.e) {
            i2 = ((this.e - getWidth()) / 2) - getLeft();
        }
        if (getHeight() >= this.f) {
            if (getTop() > 0) {
                height = -getTop();
            }
            if (getBottom() < this.f) {
                height = this.f - getBottom();
            }
        }
        if (getWidth() >= this.e) {
            if (getLeft() > 0) {
                i2 = -getLeft();
            }
            if (getRight() < this.e) {
                i2 = this.e - getRight();
            }
        }
        layout(getLeft() + i2, getTop() + height, getLeft() + i2 + getWidth(), getTop() + height + getHeight());
        if ((this.t & 2) != 0) {
            this.k = new TranslateAnimation((float) (-i2), 0.0f, (float) (-height), 0.0f);
            this.k.setInterpolator(new AccelerateInterpolator());
            this.k.setDuration(300);
            startAnimation(this.k);
        }
    }

    public final void a() {
        setImageBitmap(null);
        if (this.n != null && !this.n.isRecycled()) {
            this.n.recycle();
        }
    }

    public final void b() {
        float min = Math.min(((float) getWidth()) / ((float) this.l), ((float) getHeight()) / ((float) this.m));
        int top = getTop();
        int left = getLeft();
        layout(left, top, ((int) (((float) this.l) * min)) + 1 + left, ((int) (min * ((float) this.m))) + 1 + top);
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.q == 0) {
            this.q = i4 - i2;
            b();
            this.t = 0;
            c();
            this.t = 3;
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        float f2;
        float f3;
        float f4;
        float f5;
        boolean z = false;
        switch (motionEvent.getAction() & MotionEventCompat.ACTION_MASK) {
            case 0:
                if (!this.u) {
                    z = true;
                }
                this.u = z;
                this.a = 1;
                this.i = (int) motionEvent.getRawX();
                this.j = (int) motionEvent.getRawY();
                this.g = (int) motionEvent.getX();
                this.h = this.j - getTop();
                if (motionEvent.getPointerCount() == 2) {
                    this.b = a(motionEvent);
                    break;
                }
                break;
            case 1:
                this.u = !this.u;
                this.a = 0;
                b();
                int width = getWidth();
                int height = getHeight();
                if (this.v != null) {
                    if (((float) getWidth()) > ((float) this.q) * this.o) {
                        while (((float) getWidth()) > ((float) this.q) * this.o) {
                            setFrame(getLeft() + ((int) (this.v[0] * ((float) getWidth()))), getTop() + ((int) (this.v[1] * ((float) getHeight()))), getRight() - ((int) ((this.d - this.v[0]) * ((float) getWidth()))), getBottom() - ((int) ((this.d - this.v[1]) * ((float) getHeight()))));
                        }
                        f3 = ((float) width) / ((float) getWidth());
                        f2 = ((float) height) / ((float) getHeight());
                    } else {
                        f2 = 1.0f;
                        f3 = 1.0f;
                    }
                    if (((float) getWidth()) < ((float) this.q) * this.p) {
                        while (((float) getWidth()) < ((float) this.q) * this.p) {
                            setFrame(getLeft() - ((int) (this.v[0] * ((float) getWidth()))), getTop() - ((int) (this.v[1] * ((float) getHeight()))), getRight() + ((int) ((this.d - this.v[0]) * ((float) getWidth()))), getBottom() + ((int) ((this.d - this.v[1]) * ((float) getHeight()))));
                        }
                        f4 = ((float) width) / ((float) getWidth());
                        f5 = ((float) height) / ((float) getHeight());
                    } else {
                        float f6 = f2;
                        f4 = f3;
                        f5 = f6;
                    }
                    if (!(f4 == 1.0f && f5 == 1.0f)) {
                        if ((this.t & 1) == 0) {
                            b();
                            c();
                            z = true;
                        } else {
                            ScaleAnimation scaleAnimation = new ScaleAnimation(f4, 1.0f, f5, 1.0f, 1, this.r, 1, this.s);
                            scaleAnimation.setDuration(300);
                            scaleAnimation.setInterpolator(new AccelerateInterpolator());
                            scaleAnimation.setAnimationListener(new l(this));
                            startAnimation(scaleAnimation);
                            z = true;
                        }
                    }
                }
                if (!z) {
                    c();
                    break;
                }
                break;
            case 2:
                if (this.a != 1) {
                    if (this.a == 2 && a(motionEvent) > 10.0f) {
                        this.c = a(motionEvent);
                        float f7 = this.c - this.b;
                        if (f7 != 0.0f && Math.abs(f7) > 5.0f && getWidth() > 200) {
                            if (f7 > 0.0f) {
                                float f8 = this.d;
                                a(3);
                            } else {
                                float f9 = this.d;
                                a(4);
                            }
                            this.b = this.c;
                            break;
                        }
                    }
                } else {
                    layout(this.i - this.g, this.j - this.h, (this.i + getWidth()) - this.g, (this.j - this.h) + getHeight());
                    this.i = (int) motionEvent.getRawX();
                    this.j = (int) motionEvent.getRawY();
                    break;
                }
            case 5:
                this.u = !this.u;
                float x = motionEvent.getX(0);
                float y = motionEvent.getY(0);
                float x2 = motionEvent.getX(1);
                float y2 = motionEvent.getY(1);
                float[] fArr = {Math.abs((x2 - x) / 2.0f), Math.abs((y2 - y) / 2.0f)};
                fArr[0] = Math.max(x, x2) - fArr[0];
                fArr[1] = Math.max(y, y2) - fArr[1];
                this.v = fArr;
                this.r = this.v[0] / ((float) getWidth());
                this.s = this.v[1] / ((float) getHeight());
                this.v[0] = (this.v[0] / ((float) getWidth())) * this.d;
                this.v[1] = (this.v[1] / ((float) getHeight())) * this.d;
                if (a(motionEvent) > 10.0f) {
                    this.a = 2;
                    this.b = a(motionEvent);
                    break;
                }
                break;
            case 6:
                this.u = !this.u;
                this.a = 0;
                break;
        }
        return true;
    }

    public final void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        this.q = 0;
        if (bitmap != null) {
            this.l = bitmap.getWidth();
            this.m = bitmap.getHeight();
        }
        if (this.n != null && !this.n.isRecycled()) {
            this.n.recycle();
        }
        this.n = bitmap;
    }
}
