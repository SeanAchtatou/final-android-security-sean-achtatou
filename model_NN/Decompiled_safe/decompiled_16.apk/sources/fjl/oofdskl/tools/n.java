package fjl.oofdskl.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import fjl.oofdskl.lbiao.YyLb;
import java.io.File;

public final class n {
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        r0 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.widget.ImageView a(android.view.View r4) {
        /*
            r0 = r4
        L_0x0001:
            boolean r1 = r0 instanceof android.widget.ImageView
            if (r1 == 0) goto L_0x0008
            android.widget.ImageView r0 = (android.widget.ImageView) r0
        L_0x0007:
            return r0
        L_0x0008:
            boolean r1 = r0 instanceof android.view.ViewGroup
            if (r1 == 0) goto L_0x0017
            r1 = 0
            r2 = r1
        L_0x000e:
            r1 = r0
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            int r1 = r1.getChildCount()
            if (r2 < r1) goto L_0x0019
        L_0x0017:
            r0 = 0
            goto L_0x0007
        L_0x0019:
            r1 = r0
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            android.view.View r1 = r1.getChildAt(r2)
            boolean r3 = r1 instanceof android.widget.ImageView
            if (r3 == 0) goto L_0x0028
            r0 = r1
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            goto L_0x0007
        L_0x0028:
            boolean r3 = r1 instanceof android.view.ViewGroup
            if (r3 == 0) goto L_0x002e
            r0 = r1
            goto L_0x0001
        L_0x002e:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: fjl.oofdskl.tools.n.a(android.view.View):android.widget.ImageView");
    }

    public static void a(Context context, String str, String str2, String str3) {
        Notification notification = new Notification();
        long currentTimeMillis = System.currentTimeMillis();
        notification.icon = 17301633;
        notification.tickerText = "软件下载失败";
        notification.when = currentTimeMillis;
        notification.flags |= 16;
        Intent intent = new Intent(context, YyLb.class);
        intent.putExtra("service_flag", "down");
        intent.putExtra("packagename", str3);
        intent.putExtra("Duil", str2);
        intent.putExtra("Title", str);
        notification.setLatestEventInfo(context, "下载失败   点击重新下载", str, PendingIntent.getService(context, str3.hashCode(), intent, 268435456));
        ((NotificationManager) context.getSystemService("notification")).notify(str3.hashCode(), notification);
    }

    public static void a(Context context, String str, String str2, String str3, Drawable drawable) {
        ImageView a;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification(17301634, "检测到你有未安装的apk包，请查看！", System.currentTimeMillis());
        notification.flags = 16;
        notification.setLatestEventInfo(context, "你有未安装的应用，请点击安装！", str, PendingIntent.getActivity(context, str3.hashCode(), o.a(new File(str2)), 268435456));
        View inflate = LayoutInflater.from(context).inflate(notification.contentView.getLayoutId(), (ViewGroup) null);
        if (!(inflate == null || (a = a(inflate)) == null || drawable == null)) {
            notification.contentView.setImageViewBitmap(a.getId(), ((BitmapDrawable) drawable).getBitmap());
        }
        notificationManager.notify(str3.hashCode(), notification);
    }
}
