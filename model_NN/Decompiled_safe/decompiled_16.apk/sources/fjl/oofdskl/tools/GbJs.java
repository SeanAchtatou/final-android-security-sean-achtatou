package fjl.oofdskl.tools;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import fjl.oofdskl.a.a;
import fjl.oofdskl.d.e;
import fjl.oofdskl.jkou.ChJk;
import fjl.oofdskl.lbiao.YyFwQq;
import java.io.File;
import org.json.JSONObject;

public class GbJs extends BroadcastReceiver {
    private String a;
    private n b = new n();

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("data", 0);
            String dataString = intent.getDataString();
            this.a = dataString.substring(dataString.indexOf(":") + 1);
            String string = sharedPreferences.getString("list" + this.a, "0");
            Intent intent2 = new Intent();
            intent2.setAction("installupgratefinish");
            intent2.putExtra("packagename", this.a);
            context.sendBroadcast(intent2);
            if (string.equals("1")) {
                String string2 = sharedPreferences.getString("settitle", "");
                n nVar = this.b;
                String str = this.a;
                Notification notification = new Notification();
                long currentTimeMillis = System.currentTimeMillis();
                notification.icon = 17301634;
                notification.tickerText = "软件安装成功";
                notification.when = currentTimeMillis;
                notification.flags |= 16;
                Intent intent3 = new Intent(context, YyFwQq.class);
                intent3.putExtra("service_flag", "open");
                intent3.putExtra("packagename", str);
                intent3.putExtra("Title", string2);
                notification.setLatestEventInfo(context, "软件安装成功点击打开查看", string2, PendingIntent.getService(context, str.hashCode(), intent3, 268435456));
                ((NotificationManager) context.getSystemService("notification")).notify(str.hashCode(), notification);
                if (o.b(context)) {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("para", r.c);
                        jSONObject.put("url", "warepck=" + this.a + "&operate=4");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    r.ai = false;
                    new o(context, jSONObject.toString()).start();
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString(this.a, r.aj);
                    edit.commit();
                    File file = new File(sharedPreferences.getString(String.valueOf(this.a) + "_filename", ""));
                    if (file.isFile()) {
                        file.delete();
                    }
                }
                if (r.H && o.b(context)) {
                    JSONObject jSONObject2 = new JSONObject();
                    try {
                        jSONObject2.put("para", r.y);
                        jSONObject2.put("url", "packagename=" + this.a + "&registerAccount=" + r.z);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    r.ai = false;
                    new o(context, jSONObject2.toString(), "discuss").start();
                }
            }
            if (sharedPreferences.getInt("biaoshi", 5) == 1) {
                SharedPreferences.Editor edit2 = context.getSharedPreferences("data", 0).edit();
                edit2.putInt("biaoshi", 2);
                edit2.commit();
            }
        } else if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            SharedPreferences sharedPreferences2 = context.getSharedPreferences("data", 0);
            String dataString2 = intent.getDataString();
            this.a = dataString2.substring(dataString2.indexOf(":") + 1);
            r.aj = sharedPreferences2.getString(this.a, "noAppId");
            r.an = true;
            if (o.b(context)) {
                JSONObject jSONObject3 = new JSONObject();
                try {
                    jSONObject3.put("para", r.c);
                    jSONObject3.put("url", "warepck=" + this.a + "&operate=5");
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                r.ai = false;
                new o(context, jSONObject3.toString()).start();
            }
        } else if (intent.getAction().equals("judge")) {
            int a2 = a.a(context);
            if (a2 == 0) {
                if (!(fjl.oofdskl.d.a.a == null || fjl.oofdskl.d.a.a.getVisibility() == 0)) {
                    fjl.oofdskl.d.a.a.setVisibility(0);
                }
                if (!(fjl.oofdskl.slide.a.b == null || fjl.oofdskl.slide.a.b.getVisibility() == 0)) {
                    fjl.oofdskl.slide.a.b.setVisibility(4);
                }
                if (e.c) {
                    e.a.setVisibility(0);
                }
            } else if (a2 == 1) {
                if (fjl.oofdskl.d.a.a != null && fjl.oofdskl.d.a.a.getVisibility() == 0) {
                    fjl.oofdskl.d.a.a.setVisibility(4);
                }
                if (fjl.oofdskl.slide.a.b != null && fjl.oofdskl.slide.a.b.getVisibility() == 0) {
                    fjl.oofdskl.slide.a.b.setVisibility(4);
                }
                if (e.c) {
                    e.a.setVisibility(4);
                }
            } else if (a2 == 2) {
                if (fjl.oofdskl.d.a.a != null) {
                    ChJk.singleWindows.floatWindow.b();
                    ChJk.singleWindows = null;
                    if (YyFwQq.a != null) {
                        YyFwQq.a.cancel();
                    }
                    if (ChJk.jumpTimer != null) {
                        ChJk.jumpTimer.cancel();
                    }
                }
                if (fjl.oofdskl.slide.a.a != null) {
                    fjl.oofdskl.slide.a.a();
                }
                if (e.b != null) {
                    e.b();
                }
                a.a(new File(r.ar));
            }
        }
    }
}
