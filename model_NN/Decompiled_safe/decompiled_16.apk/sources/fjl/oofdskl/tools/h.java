package fjl.oofdskl.tools;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

final class h implements TextWatcher {
    private /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    public final void afterTextChanged(Editable editable) {
        int length;
        if (editable.length() > 0 && (length = editable.length()) > this.a.b) {
            Toast.makeText(this.a.a, "亲，您的输入已经达到最高字数上限咯……", 0).show();
            editable.delete(length, length + 1);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
