package fjl.oofdskl.tools;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

public final class j extends PagerAdapter {
    private List a;

    public j(List list) {
        this.a = list;
    }

    public final void destroyItem(View view, int i, Object obj) {
        ((ViewPager) view).removeView((View) this.a.get(i));
    }

    public final void finishUpdate(ViewGroup viewGroup) {
        super.finishUpdate(viewGroup);
    }

    public final int getCount() {
        return this.a.size();
    }

    public final Object instantiateItem(View view, int i) {
        ((ViewPager) view).addView((View) this.a.get(i), i);
        return this.a.get(i);
    }

    public final boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public final void setPrimaryItem(View view, int i, Object obj) {
    }

    public final void startUpdate(ViewGroup viewGroup) {
        super.startUpdate(viewGroup);
    }
}
