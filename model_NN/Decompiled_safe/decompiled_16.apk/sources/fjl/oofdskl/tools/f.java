package fjl.oofdskl.tools;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public final class f extends LinearLayout {
    private Activity a;
    private LinearLayout b;
    private ProgressBar c;
    private LinearLayout.LayoutParams d = new LinearLayout.LayoutParams(-2, -2);
    private LinearLayout.LayoutParams e = new LinearLayout.LayoutParams(-2, -2);

    public f(Activity activity) {
        super(activity);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.a = activity;
        this.b = new LinearLayout(this.a);
        this.b.setOrientation(0);
        this.c = new ProgressBar(this.a);
        this.e.gravity = 17;
        this.b.addView(this.c, this.e);
        TextView textView = new TextView(this.a);
        this.d.gravity = 17;
        textView.setText("加载中...");
        this.b.addView(textView, this.d);
        addView(this.b);
    }
}
