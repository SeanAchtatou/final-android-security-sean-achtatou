package fjl.oofdskl.tools;

import android.content.Context;
import android.graphics.Color;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class c extends RelativeLayout {
    private Context a;
    private RelativeLayout.LayoutParams b = null;
    private TextView c;

    public c(Context context) {
        super(context);
        this.a = context;
        this.c = new TextView(this.a);
        if (r.ag >= 320) {
            this.b = new RelativeLayout.LayoutParams(-1, 100);
            this.c.setTextSize(27.0f);
        } else if (r.ag >= 240 && r.ag < 320) {
            this.b = new RelativeLayout.LayoutParams(-1, 75);
            this.c.setTextSize(27.0f);
        } else if (240 > r.ag && r.ag >= 180) {
            this.b = new RelativeLayout.LayoutParams(-1, 57);
            this.c.setTextSize(27.0f);
        } else if (r.ag < 180 && r.ag > 120) {
            this.b = new RelativeLayout.LayoutParams(-1, 48);
            this.c.setTextSize(23.0f);
        } else if (r.ag <= 120) {
            this.b = new RelativeLayout.LayoutParams(-1, 33);
            this.c.setTextSize(27.0f);
        }
        setLayoutParams(this.b);
        setPadding(10, 8, 0, 0);
        setBackgroundColor(Color.argb(200, 250, 6, 19));
        this.c.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.c.setGravity(17);
        this.c.setTextColor(-1);
        addView(this.c);
    }

    public final void a(String str) {
        this.c.setText(str);
    }
}
