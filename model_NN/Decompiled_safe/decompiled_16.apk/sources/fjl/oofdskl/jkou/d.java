package fjl.oofdskl.jkou;

import android.content.Context;
import android.os.Message;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.q;
import fjl.oofdskl.tools.r;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class d implements q {
    private /* synthetic */ ChJk a;

    public d(ChJk chJk, Context context) {
        this.a = chJk;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("para", r.n);
            if (chJk.count == 5) {
                jSONObject.put("url", "appackageName=" + r.K + "&flag=1");
            } else {
                jSONObject.put("url", "appackageName=" + r.K + "&flag=0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        new o(context, this, jSONObject.toString(), "discuss").start();
    }

    public final void a(Context context, Object obj) {
        try {
            JSONObject jSONObject = new JSONArray(obj.toString()).getJSONObject(0);
            int i = jSONObject.getInt("totalCount");
            int i2 = jSONObject.getInt("thisAppCount");
            this.a.pushMessage = jSONObject.getString("printmsg");
            r.ah = jSONObject.getInt("period") * 60 * 1000;
            if (this.a.count == 5) {
                this.a.count = 0;
                Message message = new Message();
                message.what = 18;
                message.arg1 = i;
                message.arg2 = i2;
                this.a.showTextHandler.sendMessage(message);
                this.a.myHandler.sendEmptyMessage(18);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
