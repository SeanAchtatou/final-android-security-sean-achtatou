package com.admob.android.ads;

import android.app.Activity;
import android.view.KeyEvent;
import android.webkit.WebView;
import com.admob.android.ads.view.AdMobWebView;
import java.lang.ref.WeakReference;

/* compiled from: AdMobCanvasView */
public final class y extends AdMobWebView {
    String a;
    boolean b = true;
    private q e;

    public y(Activity activity, String str, q qVar) {
        super(activity, false, new WeakReference(activity));
        this.a = str;
        this.e = qVar;
    }

    /* access modifiers changed from: protected */
    public final ad a(WeakReference<Activity> weakReference) {
        return new a(this, weakReference);
    }

    public final void a(String str) {
        this.c = str + "#sdk";
    }

    public final void a() {
        if (this.e != null) {
            this.e.a();
        }
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        a();
        return true;
    }

    /* compiled from: AdMobCanvasView */
    public class a extends ad {
        public a(y yVar, WeakReference<Activity> weakReference) {
            super(yVar, weakReference);
        }

        public final void onPageFinished(WebView webView, String str) {
            if ("http://mm.admob.com/static/android/canvas.html".equals(str) && y.this.b) {
                y.this.b = false;
                y.this.loadUrl("javascript:cb('" + y.this.c + "','" + y.this.a + "')");
            }
        }
    }
}
