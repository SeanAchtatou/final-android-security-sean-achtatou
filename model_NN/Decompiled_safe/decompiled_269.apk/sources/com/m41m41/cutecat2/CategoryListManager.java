package com.m41m41.cutecat2;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class CategoryListManager {
    private static ByteArrayBuffer baf;
    private static boolean cancel = false;
    private static final Map<String, ArrayList<String>> categoryToKeywords = new HashMap();
    private static String last_adress = "";
    private String STR_google_gwt = ("http://www.google.cn/gwt/x?u=" + URLEncoder.encode("http://images.google.com.hk/imgcat?hl=zh-CN&catid="));
    InputStream bis;
    URLConnection uconnection;
    URL uri;

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(last_adress)) {
            return baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        baf = new ByteArrayBuffer(size);
        baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                last_adress = address;
                return baf;
            } else if (cancel) {
                this.bis.close();
                baf.clear();
                return baf;
            } else {
                baf.append((byte) current);
            }
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }

    public void Cancel() {
        cancel = true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public String[] getHotStrings(String index_id, String group_id) {
        cancel = false;
        ArrayList<String> list = categoryToKeywords.get(index_id);
        if (list != null && list.size() != 0) {
            return (String[]) list.toArray(new String[list.size()]);
        }
        try {
            String string = decodeURL(read(String.valueOf(this.STR_google_gwt) + group_id, 10000));
            int start = string.indexOf("</b><a");
            int end = string.lastIndexOf("</b><a");
            if (!(start == -1 || end == -1)) {
                getContentKeywords(string.substring(start));
            }
            ArrayList<String> list2 = categoryToKeywords.get(index_id);
            if (list2 != null) {
                return (String[]) list2.toArray(new String[list2.size()]);
            }
            return new String[0];
        } catch (IOException e) {
            return new String[]{e.toString()};
        }
    }

    /* access modifiers changed from: protected */
    public void getContentKeywords(String str) {
        int pos = str.indexOf("%26catid%3D");
        while (pos != -1) {
            String catid = str.substring("%26catid%3D".length() + pos, "%26catid%3D".length() + pos + 3);
            int pos2 = str.indexOf("wsc=tc'><b>", pos);
            if (pos2 != -1) {
                ArrayList<String> list = new ArrayList<>();
                int start = str.indexOf("wsc=tc'>", pos2 + 50);
                int end = str.indexOf("</a>", start);
                while (true) {
                    if (start != -1 && end != -1) {
                        String strKeyword = str.substring("wsc=tc'>".length() + start, end);
                        if (strKeyword.startsWith("<b>")) {
                            break;
                        }
                        list.add(strKeyword);
                        start = str.indexOf("wsc=tc'>", end);
                        if (start == -1) {
                            end = -1;
                            break;
                        }
                        end = str.indexOf("</a>", start);
                    } else {
                        break;
                    }
                }
                categoryToKeywords.put(catid, list);
                if (end != -1) {
                    pos = str.indexOf("%26catid%3D", end);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }
}
