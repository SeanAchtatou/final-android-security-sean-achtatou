package com.m41m41.cutecat2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class LoadHotSearchListTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
    public static final String app_url = "market://search?q=pname:com.m41m41.cutecat2";
    public static final String english_name = "CuteCat";
    private static final String refresh_err = "Refresh error,please retry";
    private final String RAN = "ran";
    private final String SEQ = "seq";
    private final String URL = "url";
    /* access modifiers changed from: private */
    public final activity activity;
    private ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public String[] hotStrings = new String[(this.metaData.length / 3)];
    private String last_adress = "";
    private final String[] metaData = {"url", "Cute Cat Pictures Daily(lite)", "http://www.google.com/gwt/x?u=http://www.cutecats.com/", "url", "Cute Cat Pictures Daily", "http://www.cutecats.com/", "url", "Daily Kitten", "http://www.google.cn/gwt/x?u=http://www.dailykitten.com/", "url", "Flickr Cute Cat", "http://www.google.com/gwt/x?u=http://www.flickr.com/groups/cutecat/", "ran", "funny kitten", "funny kitten", "ran", "funny cat", "funny cat", "ran", "cute kitten", "cute kitten", "ran", "cat girl", "cat girl", "ran", "cute cat", "cute cat", "ran", "baby kitten", "baby kitten", "ran", "hello kitty", "hello kitty", "ran", "pitiful kitten", "pitiful kitten", "ran", "fat kitten", "fat kitten", "ran", "persian cat", "persian cat", "ran", "cat kitten", "cat kitten", "ran", "cat tattoo", "cat tattoo", "ran", "puppies kitten", "puppies kitten", "ran", "dog and cat", "dog and cat", "ran", "black kitten", "black kitten", "ran", "tabby cat", "tabby cat", "ran", "white kittes", "white kittes", "ran", "orange kitten", "orange kitten", "ran", "cheshire cat", "cheshire cat", "ran", "persian kitten", "persian kitten", "ran", "siamese kitten", "siamese kitten", "ran", "tabby kitten", "tabby kitten", "ran", "bengal kitten", "bengal kitten", "ran", "calico kitten", "calico kitten", "ran", "maine coon kitten", "maine coon kitten", "ran", "garfield cat", "garfield cat", "ran", "ragdoll kitten", "ragdoll kitten", "ran", "tiger kitten", "tiger kitten", "ran", "cat cartoon", "cat cartoon", "ran", "halloween cat", "halloween cat", "ran", "cheshire cat", "cheshire cat", "ran", "lol cat", "lol cat", "ran", "cat face", "cat face", "ran", "cat eyes", "cat eyes", "ran", "crazy cat", "crazy cat"};
    /* access modifiers changed from: private */
    public String[] searchOption = new String[(this.metaData.length / 3)];
    /* access modifiers changed from: private */
    public String[] stringToLink = new String[(this.metaData.length / 3)];
    URLConnection uconnection;
    URL uri;

    public String[] getHotStrings() {
        return this.hotStrings;
    }

    LoadHotSearchListTask(activity activity2) {
        this.activity = activity2;
        for (int i = 0; i < this.metaData.length; i += 3) {
            this.searchOption[i / 3] = this.metaData[i];
            this.hotStrings[i / 3] = this.metaData[i + 1];
            this.stringToLink[i / 3] = this.metaData[i + 2];
        }
    }

    /* access modifiers changed from: protected */
    public CursorJoiner.Result doInBackground(Activity... arg0) {
        this.activity.list_hot_search = getHotStrings();
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        this.activity.getClass();
        intent.putExtra("KEY_NET_status", "sucess");
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CursorJoiner.Result result) {
        activity activity2 = this.activity;
        this.activity.getClass();
        activity2.removeDialog(5);
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        String stringExtra = intent.getStringExtra("KEY_NET_status");
        this.activity.getClass();
        if (stringExtra.compareTo("fail") == 0) {
            Toast.makeText(this.activity, refresh_err, 1).show();
        } else if (!(this.activity.list_hot_search == null || this.activity.list_hot_search.length == 0)) {
            ListView list1 = (ListView) this.activity.findViewById(R.id.ListView01);
            list1.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.activity.list_hot_search));
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    if (LoadHotSearchListTask.this.searchOption[position].compareTo("url") == 0) {
                        Intent i = new Intent(LoadHotSearchListTask.this.activity, BrowserActivity.class);
                        i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, LoadHotSearchListTask.this.hotStrings[position]);
                        i.setData(Uri.parse(LoadHotSearchListTask.this.stringToLink[position]));
                        LoadHotSearchListTask.this.activity.startActivity(i);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("ran") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], false);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("seq") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], true);
                    }
                }
            });
        }
        super.onPostExecute((Object) result);
    }

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(this.last_adress)) {
            return this.baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        this.baf = new ByteArrayBuffer(size);
        this.baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                this.last_adress = address;
                return this.baf;
            }
            this.baf.append((byte) current);
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }
}
