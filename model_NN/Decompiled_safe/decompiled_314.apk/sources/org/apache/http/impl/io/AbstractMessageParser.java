package org.apache.http.impl.io;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.ParseException;
import org.apache.http.ProtocolException;
import org.apache.http.io.HttpMessageParser;
import org.apache.http.io.SessionInputBuffer;
import org.apache.http.message.BasicLineParser;
import org.apache.http.message.LineParser;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.CharArrayBuffer;

public abstract class AbstractMessageParser implements HttpMessageParser {
    protected final LineParser lineParser;
    private final int maxHeaderCount;
    private final int maxLineLen;
    private final SessionInputBuffer sessionBuffer;

    /* access modifiers changed from: protected */
    public abstract HttpMessage parseHead(SessionInputBuffer sessionInputBuffer) throws IOException, HttpException, ParseException;

    public AbstractMessageParser(SessionInputBuffer buffer, LineParser parser, HttpParams params) {
        if (buffer == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (params == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.sessionBuffer = buffer;
            this.maxHeaderCount = params.getIntParameter(CoreConnectionPNames.MAX_HEADER_COUNT, -1);
            this.maxLineLen = params.getIntParameter(CoreConnectionPNames.MAX_LINE_LENGTH, -1);
            this.lineParser = parser != null ? parser : BasicLineParser.DEFAULT;
        }
    }

    /* JADX INFO: Multiple debug info for r0v2 int: [D('current' org.apache.http.util.CharArrayBuffer), D('l' int)] */
    /* JADX INFO: Multiple debug info for r7v1 org.apache.http.Header[]: [D('headers' org.apache.http.Header[]), D('maxHeaderCount' int)] */
    /* JADX INFO: Multiple debug info for r8v1 int: [D('maxLineLen' int), D('i' int)] */
    public static Header[] parseHeaders(SessionInputBuffer inbuffer, int maxHeaderCount2, int maxLineLen2, LineParser parser) throws HttpException, IOException {
        CharArrayBuffer current;
        CharArrayBuffer current2;
        int i;
        char ch;
        if (inbuffer == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        if (parser == null) {
            parser = BasicLineParser.DEFAULT;
        }
        ArrayList headerLines = new ArrayList();
        CharArrayBuffer current3 = null;
        CharArrayBuffer previous = null;
        while (true) {
            if (current3 == null) {
                current = new CharArrayBuffer(64);
            } else {
                current3.clear();
                current = current3;
            }
            if (inbuffer.readLine(current) == -1 || current.length() < 1) {
                Header[] headers = new Header[headerLines.size()];
                int i2 = 0;
            } else {
                if ((current.charAt(0) == ' ' || current.charAt(0) == 9) && previous != null) {
                    int i3 = 0;
                    while (true) {
                        i = i3;
                        if (i < current.length() && ((ch = current.charAt(i)) == ' ' || ch == 9)) {
                            i3 = i + 1;
                        } else if (maxLineLen2 > 0 || ((previous.length() + 1) + current.length()) - i <= maxLineLen2) {
                            previous.append(' ');
                            previous.append(current, i, current.length() - i);
                            current3 = current;
                            current2 = previous;
                        } else {
                            throw new IOException("Maximum line length limit exceeded");
                        }
                    }
                    if (maxLineLen2 > 0) {
                    }
                    previous.append(' ');
                    previous.append(current, i, current.length() - i);
                    current3 = current;
                    current2 = previous;
                } else {
                    headerLines.add(current);
                    current2 = current;
                    current3 = null;
                }
                if (maxHeaderCount2 <= 0 || headerLines.size() < maxHeaderCount2) {
                    previous = current2;
                } else {
                    throw new IOException("Maximum header count exceeded");
                }
            }
        }
        Header[] headers2 = new Header[headerLines.size()];
        int i22 = 0;
        while (true) {
            int i4 = i22;
            if (i4 >= headerLines.size()) {
                return headers2;
            }
            try {
                headers2[i4] = parser.parseHeader((CharArrayBuffer) headerLines.get(i4));
                i22 = i4 + 1;
            } catch (ParseException ex) {
                throw new ProtocolException(ex.getMessage());
            }
        }
    }

    public HttpMessage parse() throws IOException, HttpException {
        try {
            HttpMessage message = parseHead(this.sessionBuffer);
            message.setHeaders(parseHeaders(this.sessionBuffer, this.maxHeaderCount, this.maxLineLen, this.lineParser));
            return message;
        } catch (ParseException e) {
            ParseException px = e;
            throw new ProtocolException(px.getMessage(), px);
        }
    }
}
