package org.apache.http.client.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.apache.http.annotation.Immutable;

@Immutable
public class JdkIdn implements Idn {
    private final Method toUnicode;

    public JdkIdn() throws ClassNotFoundException {
        try {
            this.toUnicode = Class.forName("java.net.IDN").getMethod("toUnicode", String.class);
        } catch (SecurityException e) {
            SecurityException e2 = e;
            throw new IllegalStateException(e2.getMessage(), e2);
        } catch (NoSuchMethodException e3) {
            NoSuchMethodException e4 = e3;
            throw new IllegalStateException(e4.getMessage(), e4);
        }
    }

    public String toUnicode(String punycode) {
        try {
            return (String) this.toUnicode.invoke(null, punycode);
        } catch (IllegalAccessException e) {
            IllegalAccessException e2 = e;
            throw new IllegalStateException(e2.getMessage(), e2);
        } catch (InvocationTargetException e3) {
            Throwable t = e3.getCause();
            throw new RuntimeException(t.getMessage(), t);
        }
    }
}
