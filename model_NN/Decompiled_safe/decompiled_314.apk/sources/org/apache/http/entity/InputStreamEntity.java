package org.apache.http.entity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class InputStreamEntity extends AbstractHttpEntity {
    private static final int BUFFER_SIZE = 2048;
    private boolean consumed = false;
    private final InputStream content;
    private final long length;

    public InputStreamEntity(InputStream instream, long length2) {
        if (instream == null) {
            throw new IllegalArgumentException("Source input stream may not be null");
        }
        this.content = instream;
        this.length = length2;
    }

    public boolean isRepeatable() {
        return false;
    }

    public long getContentLength() {
        return this.length;
    }

    public InputStream getContent() throws IOException {
        return this.content;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public void writeTo(OutputStream outstream) throws IOException {
        int l;
        if (outstream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream instream = this.content;
        byte[] buffer = new byte[BUFFER_SIZE];
        if (this.length < 0) {
            while (true) {
                int l2 = instream.read(buffer);
                if (l2 == -1) {
                    break;
                }
                outstream.write(buffer, 0, l2);
            }
        } else {
            long remaining = this.length;
            while (remaining > 0 && (l = instream.read(buffer, 0, (int) Math.min(2048L, remaining))) != -1) {
                outstream.write(buffer, 0, l);
                remaining -= (long) l;
            }
        }
        this.consumed = true;
    }

    public boolean isStreaming() {
        return !this.consumed;
    }

    public void consumeContent() throws IOException {
        this.consumed = true;
        this.content.close();
    }
}
