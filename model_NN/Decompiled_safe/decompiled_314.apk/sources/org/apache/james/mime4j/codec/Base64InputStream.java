package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Base64InputStream extends InputStream {
    static final /* synthetic */ boolean $assertionsDisabled = (!Base64InputStream.class.desiredAssertionStatus());
    private static final int[] BASE64_DECODE = new int[256];
    private static final byte BASE64_PAD = 61;
    private static final int ENCODED_BUFFER_SIZE = 1536;
    private static final int EOF = -1;
    private static Log log = LogFactory.getLog(Base64InputStream.class);
    private boolean closed;
    private final byte[] encoded;
    private boolean eof;
    private final InputStream in;
    private int position;
    private final ByteQueue q;
    private final byte[] singleByte;
    private int size;
    private boolean strict;

    static {
        for (int i = 0; i < 256; i++) {
            BASE64_DECODE[i] = -1;
        }
        for (int i2 = 0; i2 < Base64OutputStream.BASE64_TABLE.length; i2++) {
            BASE64_DECODE[Base64OutputStream.BASE64_TABLE[i2] & 255] = i2;
        }
    }

    public Base64InputStream(InputStream in2) {
        this(in2, false);
    }

    public Base64InputStream(InputStream in2, boolean strict2) {
        this.singleByte = new byte[1];
        this.closed = false;
        this.encoded = new byte[ENCODED_BUFFER_SIZE];
        this.position = 0;
        this.size = 0;
        this.q = new ByteQueue();
        if (in2 == null) {
            throw new IllegalArgumentException();
        }
        this.in = in2;
        this.strict = strict2;
    }

    public int read() throws IOException {
        int bytes;
        if (this.closed) {
            throw new IOException("Base64InputStream has been closed");
        }
        do {
            bytes = read0(this.singleByte, 0, 1);
            if (bytes == -1) {
                return -1;
            }
        } while (bytes != 1);
        return this.singleByte[0] & 255;
    }

    public int read(byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("Base64InputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (buffer.length == 0) {
            return 0;
        } else {
            return read0(buffer, 0, buffer.length);
        }
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (this.closed) {
            throw new IOException("Base64InputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        } else if (length == 0) {
            return 0;
        } else {
            return read0(buffer, offset, offset + length);
        }
    }

    public void close() throws IOException {
        if (!this.closed) {
            this.closed = true;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 185 */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0096, code lost:
        r11 = org.apache.james.mime4j.codec.Base64InputStream.BASE64_DECODE[r16];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009a, code lost:
        if (r11 >= 0) goto L_0x00da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a6, code lost:
        if (r0.position >= r0.size) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ab, code lost:
        if (r6 >= r20) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ad, code lost:
        r2 = r0.encoded;
        r5 = r0.position;
        r1.position = r5 + 1;
        r16 = r2[r5] & 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ca, code lost:
        if (r16 != 61) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00cc, code lost:
        r2 = decodePad(r3, r4, r18, r6, r20) - r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00da, code lost:
        r3 = (r3 << 6) | r11;
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e1, code lost:
        if (r4 != 4) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e3, code lost:
        r4 = 0;
        r8 = (byte) (r3 >>> 16);
        r9 = (byte) (r3 >>> 8);
        r10 = (byte) r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ee, code lost:
        if (r6 >= (r20 - 2)) goto L_0x00fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00f0, code lost:
        r12 = r6 + 1;
        r18[r6] = r8;
        r6 = r12 + 1;
        r18[r12] = r9;
        r18[r6] = r10;
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0101, code lost:
        if (r6 >= (r20 - 1)) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0103, code lost:
        r12 = r6 + 1;
        r18[r6] = r8;
        r6 = r12 + 1;
        r18[r12] = r9;
        r0.q.enqueue(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0115, code lost:
        if (org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled != false) goto L_0x0156;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x011a, code lost:
        if (r6 == r20) goto L_0x0156;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0121, code lost:
        throw new java.lang.AssertionError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0125, code lost:
        if (r6 >= r20) goto L_0x013d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0127, code lost:
        r18[r6] = r8;
        r0.q.enqueue(r9);
        r0.q.enqueue(r10);
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x013d, code lost:
        r0.q.enqueue(r8);
        r0.q.enqueue(r9);
        r0.q.enqueue(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0156, code lost:
        r2 = r20 - r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x003b, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int read0(byte[] r18, int r19, int r20) throws java.io.IOException {
        /*
            r17 = this;
            r6 = r19
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            int r14 = r2.count()
            r15 = r14
            r12 = r6
        L_0x000d:
            int r14 = r15 + -1
            if (r15 <= 0) goto L_0x0026
            r0 = r12
            r1 = r20
            if (r0 >= r1) goto L_0x0026
            int r6 = r12 + 1
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            byte r2 = r2.dequeue()
            r18[r12] = r2
            r15 = r14
            r12 = r6
            goto L_0x000d
        L_0x0026:
            r0 = r17
            boolean r0 = r0.eof
            r2 = r0
            if (r2 == 0) goto L_0x0038
            r0 = r12
            r1 = r19
            if (r0 != r1) goto L_0x0035
            r2 = -1
        L_0x0033:
            r6 = r12
        L_0x0034:
            return r2
        L_0x0035:
            int r2 = r12 - r19
            goto L_0x0033
        L_0x0038:
            r3 = 0
            r4 = 0
            r6 = r12
        L_0x003b:
            r0 = r6
            r1 = r20
            if (r0 >= r1) goto L_0x015a
        L_0x0040:
            r0 = r17
            int r0 = r0.position
            r2 = r0
            r0 = r17
            int r0 = r0.size
            r5 = r0
            if (r2 != r5) goto L_0x009c
            r0 = r17
            java.io.InputStream r0 = r0.in
            r2 = r0
            r0 = r17
            byte[] r0 = r0.encoded
            r5 = r0
            r7 = 0
            r0 = r17
            byte[] r0 = r0.encoded
            r8 = r0
            int r8 = r8.length
            int r13 = r2.read(r5, r7, r8)
            r2 = -1
            if (r13 != r2) goto L_0x007c
            r2 = 1
            r0 = r2
            r1 = r17
            r1.eof = r0
            if (r4 == 0) goto L_0x0072
            r0 = r17
            r1 = r4
            r0.handleUnexpectedEof(r1)
        L_0x0072:
            r0 = r6
            r1 = r19
            if (r0 != r1) goto L_0x0079
            r2 = -1
            goto L_0x0034
        L_0x0079:
            int r2 = r6 - r19
            goto L_0x0034
        L_0x007c:
            if (r13 <= 0) goto L_0x008a
            r2 = 0
            r0 = r2
            r1 = r17
            r1.position = r0
            r0 = r13
            r1 = r17
            r1.size = r0
            goto L_0x0040
        L_0x008a:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0040
            if (r13 == 0) goto L_0x0040
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0096:
            int[] r2 = org.apache.james.mime4j.codec.Base64InputStream.BASE64_DECODE
            r11 = r2[r16]
            if (r11 >= 0) goto L_0x00da
        L_0x009c:
            r0 = r17
            int r0 = r0.position
            r2 = r0
            r0 = r17
            int r0 = r0.size
            r5 = r0
            if (r2 >= r5) goto L_0x003b
            r0 = r6
            r1 = r20
            if (r0 >= r1) goto L_0x003b
            r0 = r17
            byte[] r0 = r0.encoded
            r2 = r0
            r0 = r17
            int r0 = r0.position
            r5 = r0
            int r7 = r5 + 1
            r0 = r7
            r1 = r17
            r1.position = r0
            byte r2 = r2[r5]
            r0 = r2
            r0 = r0 & 255(0xff, float:3.57E-43)
            r16 = r0
            r2 = 61
            r0 = r16
            r1 = r2
            if (r0 != r1) goto L_0x0096
            r2 = r17
            r5 = r18
            r7 = r20
            int r6 = r2.decodePad(r3, r4, r5, r6, r7)
            int r2 = r6 - r19
            goto L_0x0034
        L_0x00da:
            int r2 = r3 << 6
            r3 = r2 | r11
            int r4 = r4 + 1
            r2 = 4
            if (r4 != r2) goto L_0x009c
            r4 = 0
            int r2 = r3 >>> 16
            byte r8 = (byte) r2
            int r2 = r3 >>> 8
            byte r9 = (byte) r2
            byte r10 = (byte) r3
            r2 = 2
            int r2 = r20 - r2
            if (r6 >= r2) goto L_0x00fe
            int r12 = r6 + 1
            r18[r6] = r8
            int r6 = r12 + 1
            r18[r12] = r9
            int r12 = r6 + 1
            r18[r6] = r10
            r6 = r12
            goto L_0x009c
        L_0x00fe:
            r2 = 1
            int r2 = r20 - r2
            if (r6 >= r2) goto L_0x0122
            int r12 = r6 + 1
            r18[r6] = r8
            int r6 = r12 + 1
            r18[r12] = r9
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r10)
        L_0x0113:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0156
            r0 = r6
            r1 = r20
            if (r0 == r1) goto L_0x0156
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0122:
            r0 = r6
            r1 = r20
            if (r0 >= r1) goto L_0x013d
            int r12 = r6 + 1
            r18[r6] = r8
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r9)
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r10)
            r6 = r12
            goto L_0x0113
        L_0x013d:
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r8)
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r9)
            r0 = r17
            org.apache.james.mime4j.codec.ByteQueue r0 = r0.q
            r2 = r0
            r2.enqueue(r10)
            goto L_0x0113
        L_0x0156:
            int r2 = r20 - r19
            goto L_0x0034
        L_0x015a:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0166
            if (r4 == 0) goto L_0x0166
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0166:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0175
            r0 = r6
            r1 = r20
            if (r0 == r1) goto L_0x0175
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0175:
            int r2 = r20 - r19
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.codec.Base64InputStream.read0(byte[], int, int):int");
    }

    private int decodePad(int data, int sextets, byte[] buffer, int index, int end) throws IOException {
        this.eof = true;
        if (sextets == 2) {
            byte b = (byte) (data >>> 4);
            if (index < end) {
                buffer[index] = b;
                return index + 1;
            }
            this.q.enqueue(b);
            return index;
        } else if (sextets == 3) {
            byte b1 = (byte) (data >>> 10);
            byte b2 = (byte) ((data >>> 2) & 255);
            if (index < end - 1) {
                int index2 = index + 1;
                buffer[index] = b1;
                int index3 = index2 + 1;
                buffer[index2] = b2;
                return index3;
            } else if (index < end) {
                buffer[index] = b1;
                this.q.enqueue(b2);
                return index + 1;
            } else {
                this.q.enqueue(b1);
                this.q.enqueue(b2);
                return index;
            }
        } else {
            handleUnexpecedPad(sextets);
            return index;
        }
    }

    private void handleUnexpectedEof(int sextets) throws IOException {
        if (this.strict) {
            throw new IOException("unexpected end of file");
        }
        log.warn("unexpected end of file; dropping " + sextets + " sextet(s)");
    }

    private void handleUnexpecedPad(int sextets) throws IOException {
        if (this.strict) {
            throw new IOException("unexpected padding character");
        }
        log.warn("unexpected padding character; dropping " + sextets + " sextet(s)");
    }
}
