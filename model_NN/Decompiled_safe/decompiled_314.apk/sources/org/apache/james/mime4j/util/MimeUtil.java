package org.apache.james.mime4j.util;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.ContentTypeField;

public final class MimeUtil {
    public static final String ENC_7BIT = "7bit";
    public static final String ENC_8BIT = "8bit";
    public static final String ENC_BASE64 = "base64";
    public static final String ENC_BINARY = "binary";
    public static final String ENC_QUOTED_PRINTABLE = "quoted-printable";
    public static final String MIME_HEADER_CONTENT_DESCRIPTION = "content-description";
    public static final String MIME_HEADER_CONTENT_DISPOSITION = "content-disposition";
    public static final String MIME_HEADER_CONTENT_ID = "content-id";
    public static final String MIME_HEADER_LANGAUGE = "content-language";
    public static final String MIME_HEADER_LOCATION = "content-location";
    public static final String MIME_HEADER_MD5 = "content-md5";
    public static final String MIME_HEADER_MIME_VERSION = "mime-version";
    public static final String PARAM_CREATION_DATE = "creation-date";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_MODIFICATION_DATE = "modification-date";
    public static final String PARAM_READ_DATE = "read-date";
    public static final String PARAM_SIZE = "size";
    private static final ThreadLocal<DateFormat> RFC822_DATE_FORMAT = new ThreadLocal<DateFormat>() {
        /* access modifiers changed from: protected */
        public DateFormat initialValue() {
            return new Rfc822DateFormat();
        }
    };
    private static int counter = 0;
    private static final Log log = LogFactory.getLog(MimeUtil.class);
    private static final Random random = new Random();

    private MimeUtil() {
    }

    public static boolean isSameMimeType(String pType1, String pType2) {
        return (pType1 == null || pType2 == null || !pType1.equalsIgnoreCase(pType2)) ? false : true;
    }

    public static boolean isMessage(String pMimeType) {
        return pMimeType != null && pMimeType.equalsIgnoreCase(ContentTypeField.TYPE_MESSAGE_RFC822);
    }

    public static boolean isMultipart(String pMimeType) {
        return pMimeType != null && pMimeType.toLowerCase().startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX);
    }

    public static boolean isBase64Encoding(String pTransferEncoding) {
        return ENC_BASE64.equalsIgnoreCase(pTransferEncoding);
    }

    public static boolean isQuotedPrintableEncoded(String pTransferEncoding) {
        return ENC_QUOTED_PRINTABLE.equalsIgnoreCase(pTransferEncoding);
    }

    /* JADX INFO: Multiple debug info for r10v3 java.lang.String: [D('pValue' java.lang.String), D('rest' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v6 char[]: [D('rest' java.lang.String), D('chars' char[])] */
    /* JADX INFO: Multiple debug info for r10v15 char[]: [D('ERROR' byte), D('arr$' char[])] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> getHeaderParams(java.lang.String r10) {
        /*
            java.lang.String r10 = r10.trim()
            java.lang.String r10 = unfold(r10)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r0 = ";"
            int r0 = r10.indexOf(r0)
            r1 = -1
            if (r0 != r1) goto L_0x004f
            r10 = r10
            r0 = 0
        L_0x0018:
            java.lang.String r1 = ""
            r7.put(r1, r10)
            if (r0 == 0) goto L_0x0141
            char[] r0 = r0.toCharArray()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r10 = 64
            r5.<init>(r10)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r10 = 64
            r6.<init>(r10)
            r10 = 0
            r10 = 1
            r10 = 2
            r10 = 3
            r10 = 4
            r10 = 5
            r10 = 99
            r2 = 0
            r1 = 0
            r10 = r0
            int r4 = r10.length
            r0 = 0
            r3 = r0
            r8 = r2
        L_0x0040:
            if (r3 >= r4) goto L_0x0127
            char r0 = r10[r3]
            switch(r8) {
                case 0: goto L_0x0070;
                case 1: goto L_0x0144;
                case 2: goto L_0x00a1;
                case 3: goto L_0x00ae;
                case 4: goto L_0x00e6;
                case 5: goto L_0x0142;
                case 99: goto L_0x0068;
                default: goto L_0x0047;
            }
        L_0x0047:
            r0 = r1
            r2 = r8
        L_0x0049:
            int r1 = r3 + 1
            r3 = r1
            r8 = r2
            r1 = r0
            goto L_0x0040
        L_0x004f:
            r0 = 0
            java.lang.String r1 = ";"
            int r1 = r10.indexOf(r1)
            java.lang.String r0 = r10.substring(r0, r1)
            int r1 = r0.length()
            int r1 = r1 + 1
            java.lang.String r10 = r10.substring(r1)
            r9 = r10
            r10 = r0
            r0 = r9
            goto L_0x0018
        L_0x0068:
            r2 = 59
            if (r0 != r2) goto L_0x0047
            r0 = 0
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x0070:
            r2 = 61
            if (r0 != r2) goto L_0x0080
            org.apache.commons.logging.Log r0 = org.apache.james.mime4j.util.MimeUtil.log
            java.lang.String r2 = "Expected header param name, got '='"
            r0.error(r2)
            r0 = 99
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x0080:
            r2 = 0
            r5.setLength(r2)
            r2 = 0
            r6.setLength(r2)
            r2 = 1
        L_0x0089:
            r8 = 61
            if (r0 != r8) goto L_0x009c
            int r0 = r5.length()
            if (r0 != 0) goto L_0x0098
            r0 = 99
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x0098:
            r0 = 2
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x009c:
            r5.append(r0)
            r0 = r1
            goto L_0x0049
        L_0x00a1:
            r2 = 0
            switch(r0) {
                case 9: goto L_0x00a7;
                case 32: goto L_0x00a7;
                case 34: goto L_0x00ac;
                default: goto L_0x00a5;
            }
        L_0x00a5:
            r8 = 3
            r2 = 1
        L_0x00a7:
            if (r2 != 0) goto L_0x00ae
            r0 = r1
            r2 = r8
            goto L_0x0049
        L_0x00ac:
            r8 = 4
            goto L_0x00a7
        L_0x00ae:
            r2 = 0
            switch(r0) {
                case 9: goto L_0x00ba;
                case 32: goto L_0x00ba;
                case 59: goto L_0x00ba;
                default: goto L_0x00b2;
            }
        L_0x00b2:
            r6.append(r0)
        L_0x00b5:
            if (r2 != 0) goto L_0x00d4
            r0 = r1
            r2 = r8
            goto L_0x0049
        L_0x00ba:
            java.lang.String r2 = r5.toString()
            java.lang.String r2 = r2.trim()
            java.lang.String r2 = r2.toLowerCase()
            java.lang.String r8 = r6.toString()
            java.lang.String r8 = r8.trim()
            r7.put(r2, r8)
            r8 = 5
            r2 = 1
            goto L_0x00b5
        L_0x00d4:
            r2 = r8
        L_0x00d5:
            switch(r0) {
                case 9: goto L_0x00e3;
                case 32: goto L_0x00e3;
                case 59: goto L_0x00de;
                default: goto L_0x00d8;
            }
        L_0x00d8:
            r0 = 99
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x00de:
            r0 = 0
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x00e3:
            r0 = r1
            goto L_0x0049
        L_0x00e6:
            switch(r0) {
                case 34: goto L_0x00f6;
                case 92: goto L_0x0118;
                default: goto L_0x00e9;
            }
        L_0x00e9:
            if (r1 == 0) goto L_0x00f0
            r1 = 92
            r6.append(r1)
        L_0x00f0:
            r1 = 0
            r6.append(r0)
            goto L_0x0047
        L_0x00f6:
            if (r1 != 0) goto L_0x0110
            java.lang.String r0 = r5.toString()
            java.lang.String r0 = r0.trim()
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r2 = r6.toString()
            r7.put(r0, r2)
            r0 = 5
            r2 = r0
            r0 = r1
            goto L_0x0049
        L_0x0110:
            r1 = 0
            r6.append(r0)
            r0 = r1
            r2 = r8
            goto L_0x0049
        L_0x0118:
            if (r1 == 0) goto L_0x011f
            r0 = 92
            r6.append(r0)
        L_0x011f:
            if (r1 != 0) goto L_0x0125
            r0 = 1
        L_0x0122:
            r2 = r8
            goto L_0x0049
        L_0x0125:
            r0 = 0
            goto L_0x0122
        L_0x0127:
            r10 = 3
            if (r8 != r10) goto L_0x0141
            java.lang.String r10 = r5.toString()
            java.lang.String r10 = r10.trim()
            java.lang.String r10 = r10.toLowerCase()
            java.lang.String r0 = r6.toString()
            java.lang.String r0 = r0.trim()
            r7.put(r10, r0)
        L_0x0141:
            return r7
        L_0x0142:
            r2 = r8
            goto L_0x00d5
        L_0x0144:
            r2 = r8
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.util.MimeUtil.getHeaderParams(java.lang.String):java.util.Map");
    }

    public static String createUniqueBoundary() {
        return "-=Part." + Integer.toHexString(nextCounterValue()) + '.' + Long.toHexString(random.nextLong()) + '.' + Long.toHexString(System.currentTimeMillis()) + '.' + Long.toHexString(random.nextLong()) + "=-";
    }

    public static String createUniqueMessageId(String hostName) {
        StringBuilder sb = new StringBuilder("<Mime4j.");
        sb.append(Integer.toHexString(nextCounterValue()));
        sb.append('.');
        sb.append(Long.toHexString(random.nextLong()));
        sb.append('.');
        sb.append(Long.toHexString(System.currentTimeMillis()));
        if (hostName != null) {
            sb.append('@');
            sb.append(hostName);
        }
        sb.append('>');
        return sb.toString();
    }

    public static String formatDate(Date date, TimeZone zone) {
        DateFormat df = RFC822_DATE_FORMAT.get();
        if (zone == null) {
            df.setTimeZone(TimeZone.getDefault());
        } else {
            df.setTimeZone(zone);
        }
        return df.format(date);
    }

    public static String fold(String s, int usedCharacters) {
        int length = s.length();
        if (usedCharacters + length <= 76) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        int lastLineBreak = -usedCharacters;
        int wspIdx = indexOfWsp(s, 0);
        while (wspIdx != length) {
            int nextWspIdx = indexOfWsp(s, wspIdx + 1);
            if (nextWspIdx - lastLineBreak > 76) {
                sb.append(s.substring(Math.max(0, lastLineBreak), wspIdx));
                sb.append("\r\n");
                lastLineBreak = wspIdx;
            }
            wspIdx = nextWspIdx;
        }
        sb.append(s.substring(Math.max(0, lastLineBreak)));
        return sb.toString();
    }

    public static String unfold(String s) {
        int length = s.length();
        for (int idx = 0; idx < length; idx++) {
            char c = s.charAt(idx);
            if (c == 13 || c == 10) {
                return unfold0(s, idx);
            }
        }
        return s;
    }

    private static String unfold0(String s, int crlfIdx) {
        int length = s.length();
        StringBuilder sb = new StringBuilder(length);
        if (crlfIdx > 0) {
            sb.append(s.substring(0, crlfIdx));
        }
        for (int idx = crlfIdx + 1; idx < length; idx++) {
            char c = s.charAt(idx);
            if (!(c == 13 || c == 10)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static int indexOfWsp(String s, int fromIndex) {
        int len = s.length();
        for (int index = fromIndex; index < len; index++) {
            char c = s.charAt(index);
            if (c == ' ' || c == 9) {
                return index;
            }
        }
        return len;
    }

    private static synchronized int nextCounterValue() {
        int i;
        synchronized (MimeUtil.class) {
            i = counter;
            counter = i + 1;
        }
        return i;
    }

    private static final class Rfc822DateFormat extends SimpleDateFormat {
        private static final long serialVersionUID = 1;

        public Rfc822DateFormat() {
            super("EEE, d MMM yyyy HH:mm:ss ", Locale.US);
        }

        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
            StringBuffer sb = super.format(date, toAppendTo, pos);
            int minutes = ((this.calendar.get(15) + this.calendar.get(16)) / DateUtils.MILLIS_IN_SECOND) / 60;
            if (minutes < 0) {
                sb.append('-');
                minutes = -minutes;
            } else {
                sb.append('+');
            }
            sb.append(String.format("%02d%02d", Integer.valueOf(minutes / 60), Integer.valueOf(minutes % 60)));
            return sb;
        }
    }
}
