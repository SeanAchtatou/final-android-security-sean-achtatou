package org.apache.commons.logging;

import java.lang.reflect.Constructor;
import java.util.Hashtable;
import org.apache.commons.logging.impl.NoOpLog;

public class LogSource {
    protected static boolean jdk14IsAvailable;
    protected static boolean log4jIsAvailable;
    protected static Constructor logImplctor = null;
    protected static Hashtable logs = new Hashtable();

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036 A[Catch:{ Throwable -> 0x0081 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e A[SYNTHETIC, Splitter:B:18:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005d A[SYNTHETIC, Splitter:B:35:0x005d] */
    static {
        /*
            r3 = 0
            java.util.Hashtable r2 = new java.util.Hashtable
            r2.<init>()
            org.apache.commons.logging.LogSource.logs = r2
            org.apache.commons.logging.LogSource.log4jIsAvailable = r3
            org.apache.commons.logging.LogSource.jdk14IsAvailable = r3
            r2 = 0
            org.apache.commons.logging.LogSource.logImplctor = r2
            java.lang.String r2 = "org.apache.log4j.Logger"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x0046 }
            if (r2 == 0) goto L_0x0042
            r2 = 1
            org.apache.commons.logging.LogSource.log4jIsAvailable = r2     // Catch:{ Throwable -> 0x0046 }
        L_0x001a:
            java.lang.String r2 = "java.util.logging.Logger"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x004f }
            if (r2 == 0) goto L_0x004b
            java.lang.String r2 = "org.apache.commons.logging.impl.Jdk14Logger"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x004f }
            if (r2 == 0) goto L_0x004b
            r2 = 1
            org.apache.commons.logging.LogSource.jdk14IsAvailable = r2     // Catch:{ Throwable -> 0x004f }
        L_0x002d:
            r0 = 0
            java.lang.String r2 = "org.apache.commons.logging.log"
            java.lang.String r0 = java.lang.System.getProperty(r2)     // Catch:{ Throwable -> 0x0081 }
            if (r0 != 0) goto L_0x003c
            java.lang.String r2 = "org.apache.commons.logging.Log"
            java.lang.String r0 = java.lang.System.getProperty(r2)     // Catch:{ Throwable -> 0x0081 }
        L_0x003c:
            if (r0 == 0) goto L_0x005d
            setLogImplementation(r0)     // Catch:{ Throwable -> 0x0054 }
        L_0x0041:
            return
        L_0x0042:
            r2 = 0
            org.apache.commons.logging.LogSource.log4jIsAvailable = r2     // Catch:{ Throwable -> 0x0046 }
            goto L_0x001a
        L_0x0046:
            r2 = move-exception
            r1 = r2
            org.apache.commons.logging.LogSource.log4jIsAvailable = r3
            goto L_0x001a
        L_0x004b:
            r2 = 0
            org.apache.commons.logging.LogSource.jdk14IsAvailable = r2     // Catch:{ Throwable -> 0x004f }
            goto L_0x002d
        L_0x004f:
            r2 = move-exception
            r1 = r2
            org.apache.commons.logging.LogSource.jdk14IsAvailable = r3
            goto L_0x002d
        L_0x0054:
            r1 = move-exception
            java.lang.String r2 = "org.apache.commons.logging.impl.NoOpLog"
            setLogImplementation(r2)     // Catch:{ Throwable -> 0x005b }
            goto L_0x0041
        L_0x005b:
            r2 = move-exception
            goto L_0x0041
        L_0x005d:
            boolean r2 = org.apache.commons.logging.LogSource.log4jIsAvailable     // Catch:{ Throwable -> 0x0067 }
            if (r2 == 0) goto L_0x0071
            java.lang.String r2 = "org.apache.commons.logging.impl.Log4JLogger"
            setLogImplementation(r2)     // Catch:{ Throwable -> 0x0067 }
            goto L_0x0041
        L_0x0067:
            r2 = move-exception
            r1 = r2
            java.lang.String r2 = "org.apache.commons.logging.impl.NoOpLog"
            setLogImplementation(r2)     // Catch:{ Throwable -> 0x006f }
            goto L_0x0041
        L_0x006f:
            r2 = move-exception
            goto L_0x0041
        L_0x0071:
            boolean r2 = org.apache.commons.logging.LogSource.jdk14IsAvailable     // Catch:{ Throwable -> 0x0067 }
            if (r2 == 0) goto L_0x007b
            java.lang.String r2 = "org.apache.commons.logging.impl.Jdk14Logger"
            setLogImplementation(r2)     // Catch:{ Throwable -> 0x0067 }
            goto L_0x0041
        L_0x007b:
            java.lang.String r2 = "org.apache.commons.logging.impl.NoOpLog"
            setLogImplementation(r2)     // Catch:{ Throwable -> 0x0067 }
            goto L_0x0041
        L_0x0081:
            r2 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.logging.LogSource.<clinit>():void");
    }

    private LogSource() {
    }

    public static void setLogImplementation(String classname) throws LinkageError, ExceptionInInitializerError, NoSuchMethodException, SecurityException, ClassNotFoundException {
        try {
            logImplctor = Class.forName(classname).getConstructor("".getClass());
        } catch (Throwable th) {
            logImplctor = null;
        }
    }

    public static void setLogImplementation(Class logclass) throws LinkageError, ExceptionInInitializerError, NoSuchMethodException, SecurityException {
        logImplctor = logclass.getConstructor("".getClass());
    }

    public static Log getInstance(String name) {
        Log log = (Log) logs.get(name);
        if (log != null) {
            return log;
        }
        Log log2 = makeNewLogInstance(name);
        logs.put(name, log2);
        return log2;
    }

    public static Log getInstance(Class clazz) {
        return getInstance(clazz.getName());
    }

    public static Log makeNewLogInstance(String name) {
        Log log;
        try {
            log = (Log) logImplctor.newInstance(name);
        } catch (Throwable th) {
            log = null;
        }
        if (log == null) {
            return new NoOpLog(name);
        }
        return log;
    }

    public static String[] getLogNames() {
        return (String[]) logs.keySet().toArray(new String[logs.size()]);
    }
}
