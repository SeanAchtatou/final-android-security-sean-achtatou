package org.apache.commons.lang.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.http.HttpStatus;

public class NumberUtils {
    public static final Byte BYTE_MINUS_ONE = new Byte((byte) -1);
    public static final Byte BYTE_ONE = new Byte((byte) 1);
    public static final Byte BYTE_ZERO = new Byte((byte) 0);
    public static final Double DOUBLE_MINUS_ONE = new Double(-1.0d);
    public static final Double DOUBLE_ONE = new Double(1.0d);
    public static final Double DOUBLE_ZERO = new Double(0.0d);
    public static final Float FLOAT_MINUS_ONE = new Float(-1.0f);
    public static final Float FLOAT_ONE = new Float(1.0f);
    public static final Float FLOAT_ZERO = new Float((float) SystemUtils.JAVA_VERSION_FLOAT);
    public static final Integer INTEGER_MINUS_ONE = new Integer(-1);
    public static final Integer INTEGER_ONE = new Integer(1);
    public static final Integer INTEGER_ZERO = new Integer(0);
    public static final Long LONG_MINUS_ONE = new Long(-1);
    public static final Long LONG_ONE = new Long(1);
    public static final Long LONG_ZERO = new Long(0);
    public static final Short SHORT_MINUS_ONE = new Short((short) -1);
    public static final Short SHORT_ONE = new Short((short) 1);
    public static final Short SHORT_ZERO = new Short((short) 0);

    public static int stringToInt(String str) {
        return toInt(str);
    }

    public static int toInt(String str) {
        return toInt(str, 0);
    }

    public static int stringToInt(String str, int defaultValue) {
        return toInt(str, defaultValue);
    }

    public static int toInt(String str, int defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static long toLong(String str) {
        return toLong(str, 0);
    }

    public static long toLong(String str, long defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static float toFloat(String str) {
        return toFloat(str, SystemUtils.JAVA_VERSION_FLOAT);
    }

    public static float toFloat(String str, float defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Float.parseFloat(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static double toDouble(String str) {
        return toDouble(str, 0.0d);
    }

    public static double toDouble(String str, double defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static byte toByte(String str) {
        return toByte(str, (byte) 0);
    }

    public static byte toByte(String str, byte defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Byte.parseByte(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static short toShort(String str) {
        return toShort(str, 0);
    }

    public static short toShort(String str, short defaultValue) {
        if (str == null) {
            return defaultValue;
        }
        try {
            return Short.parseShort(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    /* JADX INFO: Multiple debug info for r1v15 java.lang.Float: [D('dec' java.lang.String), D('f' java.lang.Float)] */
    /* JADX INFO: Multiple debug info for r1v21 java.lang.String: [D('mant' java.lang.String), D('decPos' int)] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static Number createNumber(String str) throws NumberFormatException {
        String mant;
        String mant2;
        String mant3;
        String exp;
        String exp2;
        String dec;
        if (str == null) {
            return null;
        }
        if (StringUtils.isBlank(str)) {
            throw new NumberFormatException("A blank string is not a valid number");
        } else if (str.startsWith("--")) {
            return null;
        } else {
            if (str.startsWith("0x") || str.startsWith("-0x")) {
                return createInteger(str);
            }
            char lastChar = str.charAt(str.length() - 1);
            int decPos = str.indexOf(46);
            int expPos = str.indexOf((int) HttpStatus.SC_SWITCHING_PROTOCOLS) + str.indexOf(69) + 1;
            if (decPos > -1) {
                if (expPos <= -1) {
                    dec = str.substring(decPos + 1);
                } else if (expPos < decPos || expPos > str.length()) {
                    throw new NumberFormatException(new StringBuffer().append(str).append(" is not a valid number.").toString());
                } else {
                    dec = str.substring(decPos + 1, expPos);
                }
                mant2 = str.substring(0, decPos);
                mant3 = dec;
            } else {
                if (expPos <= -1) {
                    mant = str;
                } else if (expPos > str.length()) {
                    throw new NumberFormatException(new StringBuffer().append(str).append(" is not a valid number.").toString());
                } else {
                    mant = str.substring(0, expPos);
                }
                mant2 = mant;
                mant3 = null;
            }
            if (Character.isDigit(lastChar) || lastChar == '.') {
                if (expPos <= -1 || expPos >= str.length() - 1) {
                    exp = null;
                } else {
                    exp = str.substring(expPos + 1, str.length());
                }
                if (mant3 == null && exp == null) {
                    try {
                        return createInteger(str);
                    } catch (NumberFormatException e) {
                        try {
                            return createLong(str);
                        } catch (NumberFormatException e2) {
                            return createBigInteger(str);
                        }
                    }
                } else {
                    boolean allZeros = isAllZeros(mant2) && isAllZeros(exp);
                    try {
                        Float f = createFloat(str);
                        if (!f.isInfinite() && (f.floatValue() != SystemUtils.JAVA_VERSION_FLOAT || allZeros)) {
                            return f;
                        }
                    } catch (NumberFormatException e3) {
                    }
                    try {
                        Double d = createDouble(str);
                        if (!d.isInfinite() && (d.doubleValue() != 0.0d || allZeros)) {
                            return d;
                        }
                    } catch (NumberFormatException e4) {
                    }
                    return createBigDecimal(str);
                }
            } else {
                if (expPos <= -1 || expPos >= str.length() - 1) {
                    exp2 = null;
                } else {
                    exp2 = str.substring(expPos + 1, str.length() - 1);
                }
                String numeric = str.substring(0, str.length() - 1);
                boolean allZeros2 = isAllZeros(mant2) && isAllZeros(exp2);
                switch (lastChar) {
                    case 'D':
                    case 'd':
                        break;
                    case 'F':
                    case HttpStatus.SC_PROCESSING /*102*/:
                        try {
                            Float f2 = createFloat(numeric);
                            if (!f2.isInfinite() && (f2.floatValue() != SystemUtils.JAVA_VERSION_FLOAT || allZeros2)) {
                                return f2;
                            }
                        } catch (NumberFormatException e5) {
                            break;
                        }
                    case 'L':
                    case 'l':
                        if (mant3 == null && exp2 == null && ((numeric.charAt(0) == '-' && isDigits(numeric.substring(1))) || isDigits(numeric))) {
                            try {
                                return createLong(numeric);
                            } catch (NumberFormatException e6) {
                                return createBigInteger(numeric);
                            }
                        } else {
                            throw new NumberFormatException(new StringBuffer().append(str).append(" is not a valid number.").toString());
                        }
                    default:
                        throw new NumberFormatException(new StringBuffer().append(str).append(" is not a valid number.").toString());
                }
                try {
                    Double d2 = createDouble(numeric);
                    if (!d2.isInfinite() && (((double) d2.floatValue()) != 0.0d || allZeros2)) {
                        return d2;
                    }
                } catch (NumberFormatException e7) {
                }
                try {
                    return createBigDecimal(numeric);
                } catch (NumberFormatException e8) {
                }
            }
        }
    }

    private static boolean isAllZeros(String str) {
        if (str == null) {
            return true;
        }
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) != '0') {
                return false;
            }
        }
        return str.length() > 0;
    }

    public static Float createFloat(String str) {
        if (str == null) {
            return null;
        }
        return Float.valueOf(str);
    }

    public static Double createDouble(String str) {
        if (str == null) {
            return null;
        }
        return Double.valueOf(str);
    }

    public static Integer createInteger(String str) {
        if (str == null) {
            return null;
        }
        return Integer.decode(str);
    }

    public static Long createLong(String str) {
        if (str == null) {
            return null;
        }
        return Long.valueOf(str);
    }

    public static BigInteger createBigInteger(String str) {
        if (str == null) {
            return null;
        }
        return new BigInteger(str);
    }

    public static BigDecimal createBigDecimal(String str) {
        if (str == null) {
            return null;
        }
        if (!StringUtils.isBlank(str)) {
            return new BigDecimal(str);
        }
        throw new NumberFormatException("A blank string is not a valid number");
    }

    public static long min(long[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            long min = array[0];
            for (int i = 1; i < array.length; i++) {
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }
    }

    public static int min(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            int min = array[0];
            for (int j = 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                }
            }
            return min;
        }
    }

    public static short min(short[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            short min = array[0];
            for (int i = 1; i < array.length; i++) {
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }
    }

    public static byte min(byte[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            byte min = array[0];
            for (int i = 1; i < array.length; i++) {
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }
    }

    public static double min(double[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            double min = array[0];
            for (int i = 1; i < array.length; i++) {
                if (Double.isNaN(array[i])) {
                    return Double.NaN;
                }
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }
    }

    public static float min(float[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            float min = array[0];
            for (int i = 1; i < array.length; i++) {
                if (Float.isNaN(array[i])) {
                    return Float.NaN;
                }
                if (array[i] < min) {
                    min = array[i];
                }
            }
            return min;
        }
    }

    public static long max(long[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            long max = array[0];
            for (int j = 1; j < array.length; j++) {
                if (array[j] > max) {
                    max = array[j];
                }
            }
            return max;
        }
    }

    public static int max(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            int max = array[0];
            for (int j = 1; j < array.length; j++) {
                if (array[j] > max) {
                    max = array[j];
                }
            }
            return max;
        }
    }

    public static short max(short[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            short max = array[0];
            for (int i = 1; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            return max;
        }
    }

    public static byte max(byte[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            byte max = array[0];
            for (int i = 1; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            return max;
        }
    }

    public static double max(double[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            double max = array[0];
            for (int j = 1; j < array.length; j++) {
                if (Double.isNaN(array[j])) {
                    return Double.NaN;
                }
                if (array[j] > max) {
                    max = array[j];
                }
            }
            return max;
        }
    }

    public static float max(float[] array) {
        if (array == null) {
            throw new IllegalArgumentException("The Array must not be null");
        } else if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty.");
        } else {
            float max = array[0];
            for (int j = 1; j < array.length; j++) {
                if (Float.isNaN(array[j])) {
                    return Float.NaN;
                }
                if (array[j] > max) {
                    max = array[j];
                }
            }
            return max;
        }
    }

    public static long min(long a, long b, long c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static int min(int a, int b, int c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static short min(short a, short b, short c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static byte min(byte a, byte b, byte c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static double min(double a, double b, double c) {
        return Math.min(Math.min(a, b), c);
    }

    public static float min(float a, float b, float c) {
        return Math.min(Math.min(a, b), c);
    }

    public static long max(long a, long b, long c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static int max(int a, int b, int c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static short max(short a, short b, short c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static byte max(byte a, byte b, byte c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static double max(double a, double b, double c) {
        return Math.max(Math.max(a, b), c);
    }

    public static float max(float a, float b, float c) {
        return Math.max(Math.max(a, b), c);
    }

    public static int compare(double lhs, double rhs) {
        if (lhs < rhs) {
            return -1;
        }
        if (lhs > rhs) {
            return 1;
        }
        long lhsBits = Double.doubleToLongBits(lhs);
        long rhsBits = Double.doubleToLongBits(rhs);
        if (lhsBits == rhsBits) {
            return 0;
        }
        if (lhsBits < rhsBits) {
            return -1;
        }
        return 1;
    }

    public static int compare(float lhs, float rhs) {
        if (lhs < rhs) {
            return -1;
        }
        if (lhs > rhs) {
            return 1;
        }
        int lhsBits = Float.floatToIntBits(lhs);
        int rhsBits = Float.floatToIntBits(rhs);
        if (lhsBits == rhsBits) {
            return 0;
        }
        if (lhsBits < rhsBits) {
            return -1;
        }
        return 1;
    }

    public static boolean isDigits(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x010b, code lost:
        if (r2 == false) goto L_0x0112;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x010d, code lost:
        if (r4 != false) goto L_0x0112;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0118, code lost:
        if (r0 != false) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x011a, code lost:
        if (r2 == false) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00b6, code lost:
        if (r5 >= r1.length) goto L_0x0118;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00ba, code lost:
        if (r1[r5] < '0') goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00be, code lost:
        if (r1[r5] > '9') goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00c7, code lost:
        if (r1[r5] == 'e') goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00cd, code lost:
        if (r1[r5] != 'E') goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x00d6, code lost:
        if (r1[r5] != '.') goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x00d8, code lost:
        if (r3 != false) goto L_0x00dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x00da, code lost:
        if (r4 == false) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00e2, code lost:
        if (r0 != false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x00e8, code lost:
        if (r1[r5] == 'd') goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x00ee, code lost:
        if (r1[r5] == 'D') goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x00f4, code lost:
        if (r1[r5] == 'f') goto L_0x00fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x00fa, code lost:
        if (r1[r5] != 'F') goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0103, code lost:
        if (r1[r5] == 'l') goto L_0x010b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0109, code lost:
        if (r1[r5] != 'L') goto L_0x0115;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isNumber(java.lang.String r15) {
        /*
            r14 = 45
            r13 = 57
            r12 = 48
            r11 = 1
            r10 = 0
            boolean r8 = org.apache.commons.lang.StringUtils.isEmpty(r15)
            if (r8 == 0) goto L_0x0010
            r8 = r10
        L_0x000f:
            return r8
        L_0x0010:
            char[] r1 = r15.toCharArray()
            int r7 = r1.length
            r4 = 0
            r3 = 0
            r0 = 0
            r2 = 0
            char r8 = r1[r10]
            if (r8 != r14) goto L_0x0034
            r6 = r11
        L_0x001e:
            int r8 = r6 + 1
            if (r7 <= r8) goto L_0x005f
            char r8 = r1[r6]
            if (r8 != r12) goto L_0x005f
            int r8 = r6 + 1
            char r8 = r1[r8]
            r9 = 120(0x78, float:1.68E-43)
            if (r8 != r9) goto L_0x005f
            int r5 = r6 + 2
            if (r5 != r7) goto L_0x0038
            r8 = r10
            goto L_0x000f
        L_0x0034:
            r6 = r10
            goto L_0x001e
        L_0x0036:
            int r5 = r5 + 1
        L_0x0038:
            int r8 = r1.length
            if (r5 >= r8) goto L_0x005d
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x0043
            char r8 = r1[r5]
            if (r8 <= r13) goto L_0x0036
        L_0x0043:
            char r8 = r1[r5]
            r9 = 97
            if (r8 < r9) goto L_0x004f
            char r8 = r1[r5]
            r9 = 102(0x66, float:1.43E-43)
            if (r8 <= r9) goto L_0x0036
        L_0x004f:
            char r8 = r1[r5]
            r9 = 65
            if (r8 < r9) goto L_0x005b
            char r8 = r1[r5]
            r9 = 70
            if (r8 <= r9) goto L_0x0036
        L_0x005b:
            r8 = r10
            goto L_0x000f
        L_0x005d:
            r8 = r11
            goto L_0x000f
        L_0x005f:
            int r7 = r7 + -1
            r5 = r6
        L_0x0062:
            if (r5 < r7) goto L_0x006c
            int r8 = r7 + 1
            if (r5 >= r8) goto L_0x00b5
            if (r0 == 0) goto L_0x00b5
            if (r2 != 0) goto L_0x00b5
        L_0x006c:
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x0079
            char r8 = r1[r5]
            if (r8 > r13) goto L_0x0079
            r2 = 1
            r0 = 0
        L_0x0076:
            int r5 = r5 + 1
            goto L_0x0062
        L_0x0079:
            char r8 = r1[r5]
            r9 = 46
            if (r8 != r9) goto L_0x0087
            if (r3 != 0) goto L_0x0083
            if (r4 == 0) goto L_0x0085
        L_0x0083:
            r8 = r10
            goto L_0x000f
        L_0x0085:
            r3 = 1
            goto L_0x0076
        L_0x0087:
            char r8 = r1[r5]
            r9 = 101(0x65, float:1.42E-43)
            if (r8 == r9) goto L_0x0093
            char r8 = r1[r5]
            r9 = 69
            if (r8 != r9) goto L_0x00a0
        L_0x0093:
            if (r4 == 0) goto L_0x0098
            r8 = r10
            goto L_0x000f
        L_0x0098:
            if (r2 != 0) goto L_0x009d
            r8 = r10
            goto L_0x000f
        L_0x009d:
            r4 = 1
            r0 = 1
            goto L_0x0076
        L_0x00a0:
            char r8 = r1[r5]
            r9 = 43
            if (r8 == r9) goto L_0x00aa
            char r8 = r1[r5]
            if (r8 != r14) goto L_0x00b2
        L_0x00aa:
            if (r0 != 0) goto L_0x00af
            r8 = r10
            goto L_0x000f
        L_0x00af:
            r0 = 0
            r2 = 0
            goto L_0x0076
        L_0x00b2:
            r8 = r10
            goto L_0x000f
        L_0x00b5:
            int r8 = r1.length
            if (r5 >= r8) goto L_0x0118
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x00c3
            char r8 = r1[r5]
            if (r8 > r13) goto L_0x00c3
            r8 = r11
            goto L_0x000f
        L_0x00c3:
            char r8 = r1[r5]
            r9 = 101(0x65, float:1.42E-43)
            if (r8 == r9) goto L_0x00cf
            char r8 = r1[r5]
            r9 = 69
            if (r8 != r9) goto L_0x00d2
        L_0x00cf:
            r8 = r10
            goto L_0x000f
        L_0x00d2:
            char r8 = r1[r5]
            r9 = 46
            if (r8 != r9) goto L_0x00e2
            if (r3 != 0) goto L_0x00dc
            if (r4 == 0) goto L_0x00df
        L_0x00dc:
            r8 = r10
            goto L_0x000f
        L_0x00df:
            r8 = r2
            goto L_0x000f
        L_0x00e2:
            if (r0 != 0) goto L_0x00ff
            char r8 = r1[r5]
            r9 = 100
            if (r8 == r9) goto L_0x00fc
            char r8 = r1[r5]
            r9 = 68
            if (r8 == r9) goto L_0x00fc
            char r8 = r1[r5]
            r9 = 102(0x66, float:1.43E-43)
            if (r8 == r9) goto L_0x00fc
            char r8 = r1[r5]
            r9 = 70
            if (r8 != r9) goto L_0x00ff
        L_0x00fc:
            r8 = r2
            goto L_0x000f
        L_0x00ff:
            char r8 = r1[r5]
            r9 = 108(0x6c, float:1.51E-43)
            if (r8 == r9) goto L_0x010b
            char r8 = r1[r5]
            r9 = 76
            if (r8 != r9) goto L_0x0115
        L_0x010b:
            if (r2 == 0) goto L_0x0112
            if (r4 != 0) goto L_0x0112
            r8 = r11
            goto L_0x000f
        L_0x0112:
            r8 = r10
            goto L_0x000f
        L_0x0115:
            r8 = r10
            goto L_0x000f
        L_0x0118:
            if (r0 != 0) goto L_0x011f
            if (r2 == 0) goto L_0x011f
            r8 = r11
            goto L_0x000f
        L_0x011f:
            r8 = r10
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.math.NumberUtils.isNumber(java.lang.String):boolean");
    }
}
