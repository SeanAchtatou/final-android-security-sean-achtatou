package org.apache.commons.lang;

import java.io.PrintStream;
import java.io.PrintWriter;
import org.apache.commons.lang.exception.Nestable;
import org.apache.commons.lang.exception.NestableDelegate;

public class NotImplementedException extends UnsupportedOperationException implements Nestable {
    private static final String DEFAULT_MESSAGE = "Code is not implemented";
    private static final long serialVersionUID = -6894122266938754088L;
    private Throwable cause;
    private NestableDelegate delegate;

    public NotImplementedException() {
        super(DEFAULT_MESSAGE);
        this.delegate = new NestableDelegate(this);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NotImplementedException(java.lang.String r2) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x000f
            java.lang.String r0 = "Code is not implemented"
        L_0x0004:
            r1.<init>(r0)
            org.apache.commons.lang.exception.NestableDelegate r0 = new org.apache.commons.lang.exception.NestableDelegate
            r0.<init>(r1)
            r1.delegate = r0
            return
        L_0x000f:
            r0 = r2
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.NotImplementedException.<init>(java.lang.String):void");
    }

    public NotImplementedException(Throwable cause2) {
        super(DEFAULT_MESSAGE);
        this.delegate = new NestableDelegate(this);
        this.cause = cause2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NotImplementedException(java.lang.String r2, java.lang.Throwable r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0011
            java.lang.String r0 = "Code is not implemented"
        L_0x0004:
            r1.<init>(r0)
            org.apache.commons.lang.exception.NestableDelegate r0 = new org.apache.commons.lang.exception.NestableDelegate
            r0.<init>(r1)
            r1.delegate = r0
            r1.cause = r3
            return
        L_0x0011:
            r0 = r2
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.NotImplementedException.<init>(java.lang.String, java.lang.Throwable):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NotImplementedException(java.lang.Class r3) {
        /*
            r2 = this;
            if (r3 != 0) goto L_0x000f
            java.lang.String r0 = "Code is not implemented"
        L_0x0004:
            r2.<init>(r0)
            org.apache.commons.lang.exception.NestableDelegate r0 = new org.apache.commons.lang.exception.NestableDelegate
            r0.<init>(r2)
            r2.delegate = r0
            return
        L_0x000f:
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Code is not implemented in "
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.StringBuffer r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.NotImplementedException.<init>(java.lang.Class):void");
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        }
        if (this.cause != null) {
            return this.cause.toString();
        }
        return null;
    }

    public String getMessage(int index) {
        if (index == 0) {
            return super.getMessage();
        }
        return this.delegate.getMessage(index);
    }

    public String[] getMessages() {
        return this.delegate.getMessages();
    }

    public Throwable getThrowable(int index) {
        return this.delegate.getThrowable(index);
    }

    public int getThrowableCount() {
        return this.delegate.getThrowableCount();
    }

    public Throwable[] getThrowables() {
        return this.delegate.getThrowables();
    }

    public int indexOfThrowable(Class type) {
        return this.delegate.indexOfThrowable(type, 0);
    }

    public int indexOfThrowable(Class type, int fromIndex) {
        return this.delegate.indexOfThrowable(type, fromIndex);
    }

    public void printStackTrace() {
        this.delegate.printStackTrace();
    }

    public void printStackTrace(PrintStream out) {
        this.delegate.printStackTrace(out);
    }

    public void printStackTrace(PrintWriter out) {
        this.delegate.printStackTrace(out);
    }

    public final void printPartialStackTrace(PrintWriter out) {
        super.printStackTrace(out);
    }
}
