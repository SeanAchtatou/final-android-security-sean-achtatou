package org.apache.commons.lang.mutable;

public class MutableByte extends Number implements Comparable, Mutable {
    private static final long serialVersionUID = -1585823265;
    private byte value;

    public MutableByte() {
    }

    public MutableByte(byte value2) {
        this.value = value2;
    }

    public MutableByte(Number value2) {
        this.value = value2.byteValue();
    }

    public MutableByte(String value2) throws NumberFormatException {
        this.value = Byte.parseByte(value2);
    }

    public Object getValue() {
        return new Byte(this.value);
    }

    public void setValue(byte value2) {
        this.value = value2;
    }

    public void setValue(Object value2) {
        setValue(((Number) value2).byteValue());
    }

    public void increment() {
        this.value = (byte) (this.value + 1);
    }

    public void decrement() {
        this.value = (byte) (this.value - 1);
    }

    public void add(byte operand) {
        this.value = (byte) (this.value + operand);
    }

    public void add(Number operand) {
        this.value = (byte) (this.value + operand.byteValue());
    }

    public void subtract(byte operand) {
        this.value = (byte) (this.value - operand);
    }

    public void subtract(Number operand) {
        this.value = (byte) (this.value - operand.byteValue());
    }

    public byte byteValue() {
        return this.value;
    }

    public int intValue() {
        return this.value;
    }

    public long longValue() {
        return (long) this.value;
    }

    public float floatValue() {
        return (float) this.value;
    }

    public double doubleValue() {
        return (double) this.value;
    }

    public Byte toByte() {
        return new Byte(byteValue());
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MutableByte)) {
            return false;
        }
        if (this.value == ((MutableByte) obj).byteValue()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.value;
    }

    public int compareTo(Object obj) {
        byte anotherVal = ((MutableByte) obj).value;
        if (this.value < anotherVal) {
            return -1;
        }
        return this.value == anotherVal ? 0 : 1;
    }

    public String toString() {
        return String.valueOf((int) this.value);
    }
}
