package org.apache.commons.lang.time;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.apache.commons.lang.Validate;

public class FastDateFormat extends Format {
    public static final int FULL = 0;
    public static final int LONG = 1;
    public static final int MEDIUM = 2;
    public static final int SHORT = 3;
    private static final Map cDateInstanceCache = new HashMap(7);
    private static final Map cDateTimeInstanceCache = new HashMap(7);
    private static String cDefaultPattern = null;
    private static final Map cInstanceCache = new HashMap(7);
    private static final Map cTimeInstanceCache = new HashMap(7);
    private static final Map cTimeZoneDisplayCache = new HashMap(7);
    private static final long serialVersionUID = 1;
    private final Locale mLocale;
    private final boolean mLocaleForced;
    private transient int mMaxLengthEstimate;
    private final String mPattern;
    private transient Rule[] mRules;
    private final TimeZone mTimeZone;
    private final boolean mTimeZoneForced;

    private interface NumberRule extends Rule {
        void appendTo(StringBuffer stringBuffer, int i);
    }

    private interface Rule {
        void appendTo(StringBuffer stringBuffer, Calendar calendar);

        int estimateLength();
    }

    public static FastDateFormat getInstance() {
        return getInstance(getDefaultPattern(), null, null);
    }

    public static FastDateFormat getInstance(String pattern) {
        return getInstance(pattern, null, null);
    }

    public static FastDateFormat getInstance(String pattern, TimeZone timeZone) {
        return getInstance(pattern, timeZone, null);
    }

    public static FastDateFormat getInstance(String pattern, Locale locale) {
        return getInstance(pattern, null, locale);
    }

    public static synchronized FastDateFormat getInstance(String pattern, TimeZone timeZone, Locale locale) {
        FastDateFormat format;
        synchronized (FastDateFormat.class) {
            FastDateFormat emptyFormat = new FastDateFormat(pattern, timeZone, locale);
            format = (FastDateFormat) cInstanceCache.get(emptyFormat);
            if (format == null) {
                format = emptyFormat;
                format.init();
                cInstanceCache.put(format, format);
            }
        }
        return format;
    }

    public static FastDateFormat getDateInstance(int style) {
        return getDateInstance(style, null, null);
    }

    public static FastDateFormat getDateInstance(int style, Locale locale) {
        return getDateInstance(style, null, locale);
    }

    public static FastDateFormat getDateInstance(int style, TimeZone timeZone) {
        return getDateInstance(style, timeZone, null);
    }

    public static synchronized FastDateFormat getDateInstance(int style, TimeZone timeZone, Locale locale) {
        Object obj;
        FastDateFormat format;
        synchronized (FastDateFormat.class) {
            Integer key = new Integer(style);
            if (timeZone != null) {
                obj = new Pair(key, timeZone);
            } else {
                obj = key;
            }
            if (locale == null) {
                locale = Locale.getDefault();
            }
            Pair key2 = new Pair(obj, locale);
            format = (FastDateFormat) cDateInstanceCache.get(key2);
            if (format == null) {
                try {
                    format = getInstance(((SimpleDateFormat) DateFormat.getDateInstance(style, locale)).toPattern(), timeZone, locale);
                    cDateInstanceCache.put(key2, format);
                } catch (ClassCastException e) {
                    throw new IllegalArgumentException(new StringBuffer().append("No date pattern for locale: ").append(locale).toString());
                }
            }
        }
        return format;
    }

    public static FastDateFormat getTimeInstance(int style) {
        return getTimeInstance(style, null, null);
    }

    public static FastDateFormat getTimeInstance(int style, Locale locale) {
        return getTimeInstance(style, null, locale);
    }

    public static FastDateFormat getTimeInstance(int style, TimeZone timeZone) {
        return getTimeInstance(style, timeZone, null);
    }

    public static synchronized FastDateFormat getTimeInstance(int style, TimeZone timeZone, Locale locale) {
        Object obj;
        Pair pair;
        FastDateFormat format;
        synchronized (FastDateFormat.class) {
            Integer key = new Integer(style);
            if (timeZone != null) {
                obj = new Pair(key, timeZone);
            } else {
                obj = key;
            }
            if (locale != null) {
                pair = new Pair(obj, locale);
            } else {
                pair = obj;
            }
            format = (FastDateFormat) cTimeInstanceCache.get(pair);
            if (format == null) {
                if (locale == null) {
                    locale = Locale.getDefault();
                }
                try {
                    format = getInstance(((SimpleDateFormat) DateFormat.getTimeInstance(style, locale)).toPattern(), timeZone, locale);
                    cTimeInstanceCache.put(pair, format);
                } catch (ClassCastException e) {
                    throw new IllegalArgumentException(new StringBuffer().append("No date pattern for locale: ").append(locale).toString());
                }
            }
        }
        return format;
    }

    public static FastDateFormat getDateTimeInstance(int dateStyle, int timeStyle) {
        return getDateTimeInstance(dateStyle, timeStyle, null, null);
    }

    public static FastDateFormat getDateTimeInstance(int dateStyle, int timeStyle, Locale locale) {
        return getDateTimeInstance(dateStyle, timeStyle, null, locale);
    }

    public static FastDateFormat getDateTimeInstance(int dateStyle, int timeStyle, TimeZone timeZone) {
        return getDateTimeInstance(dateStyle, timeStyle, timeZone, null);
    }

    public static synchronized FastDateFormat getDateTimeInstance(int dateStyle, int timeStyle, TimeZone timeZone, Locale locale) {
        Pair key;
        FastDateFormat format;
        synchronized (FastDateFormat.class) {
            Pair key2 = new Pair(new Integer(dateStyle), new Integer(timeStyle));
            if (timeZone != null) {
                key = new Pair(key2, timeZone);
            } else {
                key = key2;
            }
            if (locale == null) {
                locale = Locale.getDefault();
            }
            Pair key3 = new Pair(key, locale);
            format = (FastDateFormat) cDateTimeInstanceCache.get(key3);
            if (format == null) {
                try {
                    format = getInstance(((SimpleDateFormat) DateFormat.getDateTimeInstance(dateStyle, timeStyle, locale)).toPattern(), timeZone, locale);
                    cDateTimeInstanceCache.put(key3, format);
                } catch (ClassCastException e) {
                    throw new IllegalArgumentException(new StringBuffer().append("No date time pattern for locale: ").append(locale).toString());
                }
            }
        }
        return format;
    }

    static synchronized String getTimeZoneDisplay(TimeZone tz, boolean daylight, int style, Locale locale) {
        String value;
        synchronized (FastDateFormat.class) {
            TimeZoneDisplayKey key = new TimeZoneDisplayKey(tz, daylight, style, locale);
            value = (String) cTimeZoneDisplayCache.get(key);
            if (value == null) {
                value = tz.getDisplayName(daylight, style, locale);
                cTimeZoneDisplayCache.put(key, value);
            }
        }
        return value;
    }

    private static synchronized String getDefaultPattern() {
        String str;
        synchronized (FastDateFormat.class) {
            if (cDefaultPattern == null) {
                cDefaultPattern = new SimpleDateFormat().toPattern();
            }
            str = cDefaultPattern;
        }
        return str;
    }

    protected FastDateFormat(String pattern, TimeZone timeZone, Locale locale) {
        boolean z;
        if (pattern == null) {
            throw new IllegalArgumentException("The pattern must not be null");
        }
        this.mPattern = pattern;
        this.mTimeZoneForced = timeZone != null;
        this.mTimeZone = timeZone == null ? TimeZone.getDefault() : timeZone;
        if (locale != null) {
            z = true;
        } else {
            z = false;
        }
        this.mLocaleForced = z;
        this.mLocale = locale == null ? Locale.getDefault() : locale;
    }

    /* access modifiers changed from: protected */
    public void init() {
        List rulesList = parsePattern();
        this.mRules = (Rule[]) rulesList.toArray(new Rule[rulesList.size()]);
        int len = 0;
        int i = this.mRules.length;
        while (true) {
            i--;
            if (i >= 0) {
                len += this.mRules[i].estimateLength();
            } else {
                this.mMaxLengthEstimate = len;
                return;
            }
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:84:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:83:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:82:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:81:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:80:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:79:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:78:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:77:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:76:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:75:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:74:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:73:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:72:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:71:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:70:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:69:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:68:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:67:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:66:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:65:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:64:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:63:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:62:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:61:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:60:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:59:0x0093 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:58:0x0093 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v26, resolved type: java.lang.Object} */
    /* JADX WARN: Type inference failed for: r0v14, types: [org.apache.commons.lang.time.FastDateFormat$StringLiteral] */
    /* JADX WARN: Type inference failed for: r0v16, types: [org.apache.commons.lang.time.FastDateFormat$CharacterLiteral] */
    /* JADX WARN: Type inference failed for: r12v2, types: [org.apache.commons.lang.time.FastDateFormat$TimeZoneNumberRule] */
    /* JADX WARN: Type inference failed for: r12v3, types: [org.apache.commons.lang.time.FastDateFormat$TimeZoneNumberRule] */
    /* JADX WARN: Type inference failed for: r0v25, types: [org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule] */
    /* JADX WARN: Type inference failed for: r0v32, types: [org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule] */
    /* JADX WARN: Type inference failed for: r12v6, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r0v35, types: [org.apache.commons.lang.time.FastDateFormat$TwentyFourHourField] */
    /* JADX WARN: Type inference failed for: r0v36, types: [org.apache.commons.lang.time.FastDateFormat$TextField] */
    /* JADX WARN: Type inference failed for: r12v9, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v10, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v11, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v12, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r0v42, types: [org.apache.commons.lang.time.FastDateFormat$TextField] */
    /* JADX WARN: Type inference failed for: r12v14, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v15, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v16, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v17, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r0v48, types: [org.apache.commons.lang.time.FastDateFormat$TwelveHourField] */
    /* JADX WARN: Type inference failed for: r12v19, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r12v20, types: [org.apache.commons.lang.time.FastDateFormat$UnpaddedMonthField] */
    /* JADX WARN: Type inference failed for: r12v21, types: [org.apache.commons.lang.time.FastDateFormat$TwoDigitMonthField] */
    /* JADX WARN: Type inference failed for: r0v53, types: [org.apache.commons.lang.time.FastDateFormat$TextField] */
    /* JADX WARN: Type inference failed for: r0v54, types: [org.apache.commons.lang.time.FastDateFormat$TextField] */
    /* JADX WARN: Type inference failed for: r12v24, types: [org.apache.commons.lang.time.FastDateFormat$TwoDigitYearField] */
    /* JADX WARN: Type inference failed for: r12v25, types: [org.apache.commons.lang.time.FastDateFormat$NumberRule] */
    /* JADX WARN: Type inference failed for: r0v57, types: [org.apache.commons.lang.time.FastDateFormat$TextField] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List parsePattern() {
        /*
            r25 = this;
            java.text.DateFormatSymbols r17 = new java.text.DateFormatSymbols
            r0 = r25
            java.util.Locale r0 = r0.mLocale
            r21 = r0
            r0 = r17
            r1 = r21
            r0.<init>(r1)
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            java.lang.String[] r6 = r17.getEras()
            java.lang.String[] r11 = r17.getMonths()
            java.lang.String[] r14 = r17.getShortMonths()
            java.lang.String[] r20 = r17.getWeekdays()
            java.lang.String[] r15 = r17.getShortWeekdays()
            java.lang.String[] r5 = r17.getAmPmStrings()
            r0 = r25
            java.lang.String r0 = r0.mPattern
            r21 = r0
            int r10 = r21.length()
            r21 = 1
            r0 = r21
            int[] r0 = new int[r0]
            r9 = r0
            r8 = 0
        L_0x003e:
            if (r8 >= r10) goto L_0x005d
            r21 = 0
            r9[r21] = r8
            r0 = r25
            java.lang.String r0 = r0.mPattern
            r21 = r0
            r0 = r25
            r1 = r21
            r2 = r9
            java.lang.String r18 = r0.parseToken(r1, r2)
            r21 = 0
            r8 = r9[r21]
            int r19 = r18.length()
            if (r19 != 0) goto L_0x005e
        L_0x005d:
            return r13
        L_0x005e:
            r21 = 0
            r0 = r18
            r1 = r21
            char r7 = r0.charAt(r1)
            switch(r7) {
                case 39: goto L_0x0225;
                case 68: goto L_0x015c;
                case 69: goto L_0x0141;
                case 70: goto L_0x016a;
                case 71: goto L_0x0088;
                case 72: goto L_0x0109;
                case 75: goto L_0x01b7;
                case 77: goto L_0x00b1;
                case 83: goto L_0x0133;
                case 87: goto L_0x0186;
                case 90: goto L_0x0215;
                case 97: goto L_0x0194;
                case 100: goto L_0x00e7;
                case 104: goto L_0x00f4;
                case 107: goto L_0x01a1;
                case 109: goto L_0x0117;
                case 115: goto L_0x0125;
                case 119: goto L_0x0178;
                case 121: goto L_0x0099;
                case 122: goto L_0x01c5;
                default: goto L_0x006b;
            }
        L_0x006b:
            java.lang.IllegalArgumentException r21 = new java.lang.IllegalArgumentException
            java.lang.StringBuffer r22 = new java.lang.StringBuffer
            r22.<init>()
            java.lang.String r23 = "Illegal pattern component: "
            java.lang.StringBuffer r22 = r22.append(r23)
            r0 = r22
            r1 = r18
            java.lang.StringBuffer r22 = r0.append(r1)
            java.lang.String r22 = r22.toString()
            r21.<init>(r22)
            throw r21
        L_0x0088:
            org.apache.commons.lang.time.FastDateFormat$TextField r12 = new org.apache.commons.lang.time.FastDateFormat$TextField
            r21 = 0
            r0 = r12
            r1 = r21
            r2 = r6
            r0.<init>(r1, r2)
        L_0x0093:
            r13.add(r12)
            int r8 = r8 + 1
            goto L_0x003e
        L_0x0099:
            r21 = 4
            r0 = r19
            r1 = r21
            if (r0 < r1) goto L_0x00ae
            r21 = 1
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x00ae:
            org.apache.commons.lang.time.FastDateFormat$TwoDigitYearField r12 = org.apache.commons.lang.time.FastDateFormat.TwoDigitYearField.INSTANCE
            goto L_0x0093
        L_0x00b1:
            r21 = 4
            r0 = r19
            r1 = r21
            if (r0 < r1) goto L_0x00c5
            org.apache.commons.lang.time.FastDateFormat$TextField r12 = new org.apache.commons.lang.time.FastDateFormat$TextField
            r21 = 2
            r0 = r12
            r1 = r21
            r2 = r11
            r0.<init>(r1, r2)
            goto L_0x0093
        L_0x00c5:
            r21 = 3
            r0 = r19
            r1 = r21
            if (r0 != r1) goto L_0x00d9
            org.apache.commons.lang.time.FastDateFormat$TextField r12 = new org.apache.commons.lang.time.FastDateFormat$TextField
            r21 = 2
            r0 = r12
            r1 = r21
            r2 = r14
            r0.<init>(r1, r2)
            goto L_0x0093
        L_0x00d9:
            r21 = 2
            r0 = r19
            r1 = r21
            if (r0 != r1) goto L_0x00e4
            org.apache.commons.lang.time.FastDateFormat$TwoDigitMonthField r12 = org.apache.commons.lang.time.FastDateFormat.TwoDigitMonthField.INSTANCE
            goto L_0x0093
        L_0x00e4:
            org.apache.commons.lang.time.FastDateFormat$UnpaddedMonthField r12 = org.apache.commons.lang.time.FastDateFormat.UnpaddedMonthField.INSTANCE
            goto L_0x0093
        L_0x00e7:
            r21 = 5
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x00f4:
            org.apache.commons.lang.time.FastDateFormat$TwelveHourField r12 = new org.apache.commons.lang.time.FastDateFormat$TwelveHourField
            r21 = 10
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r21 = r0.selectNumberRule(r1, r2)
            r0 = r12
            r1 = r21
            r0.<init>(r1)
            goto L_0x0093
        L_0x0109:
            r21 = 11
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0117:
            r21 = 12
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0125:
            r21 = 13
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0133:
            r21 = 14
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0141:
            org.apache.commons.lang.time.FastDateFormat$TextField r12 = new org.apache.commons.lang.time.FastDateFormat$TextField
            r21 = 7
            r22 = 4
            r0 = r19
            r1 = r22
            if (r0 >= r1) goto L_0x0159
            r22 = r15
        L_0x014f:
            r0 = r12
            r1 = r21
            r2 = r22
            r0.<init>(r1, r2)
            goto L_0x0093
        L_0x0159:
            r22 = r20
            goto L_0x014f
        L_0x015c:
            r21 = 6
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x016a:
            r21 = 8
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0178:
            r21 = 3
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0186:
            r21 = 4
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x0194:
            org.apache.commons.lang.time.FastDateFormat$TextField r12 = new org.apache.commons.lang.time.FastDateFormat$TextField
            r21 = 9
            r0 = r12
            r1 = r21
            r2 = r5
            r0.<init>(r1, r2)
            goto L_0x0093
        L_0x01a1:
            org.apache.commons.lang.time.FastDateFormat$TwentyFourHourField r12 = new org.apache.commons.lang.time.FastDateFormat$TwentyFourHourField
            r21 = 11
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r21 = r0.selectNumberRule(r1, r2)
            r0 = r12
            r1 = r21
            r0.<init>(r1)
            goto L_0x0093
        L_0x01b7:
            r21 = 10
            r0 = r25
            r1 = r21
            r2 = r19
            org.apache.commons.lang.time.FastDateFormat$NumberRule r12 = r0.selectNumberRule(r1, r2)
            goto L_0x0093
        L_0x01c5:
            r21 = 4
            r0 = r19
            r1 = r21
            if (r0 < r1) goto L_0x01f1
            org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule r12 = new org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule
            r0 = r25
            java.util.TimeZone r0 = r0.mTimeZone
            r21 = r0
            r0 = r25
            boolean r0 = r0.mTimeZoneForced
            r22 = r0
            r0 = r25
            java.util.Locale r0 = r0.mLocale
            r23 = r0
            r24 = 1
            r0 = r12
            r1 = r21
            r2 = r22
            r3 = r23
            r4 = r24
            r0.<init>(r1, r2, r3, r4)
            goto L_0x0093
        L_0x01f1:
            org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule r12 = new org.apache.commons.lang.time.FastDateFormat$TimeZoneNameRule
            r0 = r25
            java.util.TimeZone r0 = r0.mTimeZone
            r21 = r0
            r0 = r25
            boolean r0 = r0.mTimeZoneForced
            r22 = r0
            r0 = r25
            java.util.Locale r0 = r0.mLocale
            r23 = r0
            r24 = 0
            r0 = r12
            r1 = r21
            r2 = r22
            r3 = r23
            r4 = r24
            r0.<init>(r1, r2, r3, r4)
            goto L_0x0093
        L_0x0215:
            r21 = 1
            r0 = r19
            r1 = r21
            if (r0 != r1) goto L_0x0221
            org.apache.commons.lang.time.FastDateFormat$TimeZoneNumberRule r12 = org.apache.commons.lang.time.FastDateFormat.TimeZoneNumberRule.INSTANCE_NO_COLON
            goto L_0x0093
        L_0x0221:
            org.apache.commons.lang.time.FastDateFormat$TimeZoneNumberRule r12 = org.apache.commons.lang.time.FastDateFormat.TimeZoneNumberRule.INSTANCE_COLON
            goto L_0x0093
        L_0x0225:
            r21 = 1
            r0 = r18
            r1 = r21
            java.lang.String r16 = r0.substring(r1)
            int r21 = r16.length()
            r22 = 1
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x024f
            org.apache.commons.lang.time.FastDateFormat$CharacterLiteral r12 = new org.apache.commons.lang.time.FastDateFormat$CharacterLiteral
            r21 = 0
            r0 = r16
            r1 = r21
            char r21 = r0.charAt(r1)
            r0 = r12
            r1 = r21
            r0.<init>(r1)
            goto L_0x0093
        L_0x024f:
            org.apache.commons.lang.time.FastDateFormat$StringLiteral r12 = new org.apache.commons.lang.time.FastDateFormat$StringLiteral
            r0 = r12
            r1 = r16
            r0.<init>(r1)
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.time.FastDateFormat.parsePattern():java.util.List");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006a, code lost:
        r2 = r2 - 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String parseToken(java.lang.String r13, int[] r14) {
        /*
            r12 = this;
            r11 = 97
            r10 = 90
            r9 = 65
            r8 = 39
            r7 = 0
            org.apache.commons.lang.text.StrBuilder r0 = new org.apache.commons.lang.text.StrBuilder
            r0.<init>()
            r2 = r14[r7]
            int r4 = r13.length()
            char r1 = r13.charAt(r2)
            if (r1 < r9) goto L_0x001c
            if (r1 <= r10) goto L_0x0022
        L_0x001c:
            if (r1 < r11) goto L_0x0037
            r6 = 122(0x7a, float:1.71E-43)
            if (r1 > r6) goto L_0x0037
        L_0x0022:
            r0.append(r1)
        L_0x0025:
            int r6 = r2 + 1
            if (r6 >= r4) goto L_0x006c
            int r6 = r2 + 1
            char r5 = r13.charAt(r6)
            if (r5 != r1) goto L_0x006c
            r0.append(r1)
            int r2 = r2 + 1
            goto L_0x0025
        L_0x0037:
            r0.append(r8)
            r3 = 0
        L_0x003b:
            if (r2 >= r4) goto L_0x006c
            char r1 = r13.charAt(r2)
            if (r1 != r8) goto L_0x005e
            int r6 = r2 + 1
            if (r6 >= r4) goto L_0x0057
            int r6 = r2 + 1
            char r6 = r13.charAt(r6)
            if (r6 != r8) goto L_0x0057
            int r2 = r2 + 1
            r0.append(r1)
        L_0x0054:
            int r2 = r2 + 1
            goto L_0x003b
        L_0x0057:
            if (r3 != 0) goto L_0x005c
            r6 = 1
            r3 = r6
        L_0x005b:
            goto L_0x0054
        L_0x005c:
            r3 = r7
            goto L_0x005b
        L_0x005e:
            if (r3 != 0) goto L_0x0073
            if (r1 < r9) goto L_0x0064
            if (r1 <= r10) goto L_0x006a
        L_0x0064:
            if (r1 < r11) goto L_0x0073
            r6 = 122(0x7a, float:1.71E-43)
            if (r1 > r6) goto L_0x0073
        L_0x006a:
            int r2 = r2 + -1
        L_0x006c:
            r14[r7] = r2
            java.lang.String r6 = r0.toString()
            return r6
        L_0x0073:
            r0.append(r1)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.time.FastDateFormat.parseToken(java.lang.String, int[]):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public NumberRule selectNumberRule(int field, int padding) {
        switch (padding) {
            case 1:
                return new UnpaddedNumberField(field);
            case 2:
                return new TwoDigitNumberField(field);
            default:
                return new PaddedNumberField(field, padding);
        }
    }

    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        if (obj instanceof Date) {
            return format((Date) obj, toAppendTo);
        }
        if (obj instanceof Calendar) {
            return format((Calendar) obj, toAppendTo);
        }
        if (obj instanceof Long) {
            return format(((Long) obj).longValue(), toAppendTo);
        }
        throw new IllegalArgumentException(new StringBuffer().append("Unknown class: ").append(obj == null ? "<null>" : obj.getClass().getName()).toString());
    }

    public String format(long millis) {
        return format(new Date(millis));
    }

    public String format(Date date) {
        Calendar c = new GregorianCalendar(this.mTimeZone, this.mLocale);
        c.setTime(date);
        return applyRules(c, new StringBuffer(this.mMaxLengthEstimate)).toString();
    }

    public String format(Calendar calendar) {
        return format(calendar, new StringBuffer(this.mMaxLengthEstimate)).toString();
    }

    public StringBuffer format(long millis, StringBuffer buf) {
        return format(new Date(millis), buf);
    }

    public StringBuffer format(Date date, StringBuffer buf) {
        Calendar c = new GregorianCalendar(this.mTimeZone);
        c.setTime(date);
        return applyRules(c, buf);
    }

    public StringBuffer format(Calendar calendar, StringBuffer buf) {
        if (this.mTimeZoneForced) {
            calendar.getTime();
            calendar = (Calendar) calendar.clone();
            calendar.setTimeZone(this.mTimeZone);
        }
        return applyRules(calendar, buf);
    }

    /* access modifiers changed from: protected */
    public StringBuffer applyRules(Calendar calendar, StringBuffer buf) {
        Rule[] rules = this.mRules;
        int len = this.mRules.length;
        for (int i = 0; i < len; i++) {
            rules[i].appendTo(buf, calendar);
        }
        return buf;
    }

    public Object parseObject(String source, ParsePosition pos) {
        pos.setIndex(0);
        pos.setErrorIndex(0);
        return null;
    }

    public String getPattern() {
        return this.mPattern;
    }

    public TimeZone getTimeZone() {
        return this.mTimeZone;
    }

    public boolean getTimeZoneOverridesCalendar() {
        return this.mTimeZoneForced;
    }

    public Locale getLocale() {
        return this.mLocale;
    }

    public int getMaxLengthEstimate() {
        return this.mMaxLengthEstimate;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FastDateFormat)) {
            return false;
        }
        FastDateFormat other = (FastDateFormat) obj;
        return (this.mPattern == other.mPattern || this.mPattern.equals(other.mPattern)) && (this.mTimeZone == other.mTimeZone || this.mTimeZone.equals(other.mTimeZone)) && ((this.mLocale == other.mLocale || this.mLocale.equals(other.mLocale)) && this.mTimeZoneForced == other.mTimeZoneForced && this.mLocaleForced == other.mLocaleForced);
    }

    public int hashCode() {
        int i;
        int i2;
        int total = 0 + this.mPattern.hashCode() + this.mTimeZone.hashCode();
        if (this.mTimeZoneForced) {
            i = 1;
        } else {
            i = 0;
        }
        int total2 = total + i + this.mLocale.hashCode();
        if (this.mLocaleForced) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        return total2 + i2;
    }

    public String toString() {
        return new StringBuffer().append("FastDateFormat[").append(this.mPattern).append("]").toString();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        init();
    }

    private static class CharacterLiteral implements Rule {
        private final char mValue;

        CharacterLiteral(char value) {
            this.mValue = value;
        }

        public int estimateLength() {
            return 1;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            buffer.append(this.mValue);
        }
    }

    private static class StringLiteral implements Rule {
        private final String mValue;

        StringLiteral(String value) {
            this.mValue = value;
        }

        public int estimateLength() {
            return this.mValue.length();
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            buffer.append(this.mValue);
        }
    }

    private static class TextField implements Rule {
        private final int mField;
        private final String[] mValues;

        TextField(int field, String[] values) {
            this.mField = field;
            this.mValues = values;
        }

        public int estimateLength() {
            int max = 0;
            int i = this.mValues.length;
            while (true) {
                i--;
                if (i < 0) {
                    return max;
                }
                int len = this.mValues[i].length();
                if (len > max) {
                    max = len;
                }
            }
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            buffer.append(this.mValues[calendar.get(this.mField)]);
        }
    }

    private static class UnpaddedNumberField implements NumberRule {
        private final int mField;

        UnpaddedNumberField(int field) {
            this.mField = field;
        }

        public int estimateLength() {
            return 4;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(this.mField));
        }

        public final void appendTo(StringBuffer buffer, int value) {
            if (value < 10) {
                buffer.append((char) (value + 48));
            } else if (value < 100) {
                buffer.append((char) ((value / 10) + 48));
                buffer.append((char) ((value % 10) + 48));
            } else {
                buffer.append(Integer.toString(value));
            }
        }
    }

    private static class UnpaddedMonthField implements NumberRule {
        static final UnpaddedMonthField INSTANCE = new UnpaddedMonthField();

        UnpaddedMonthField() {
        }

        public int estimateLength() {
            return 2;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(2) + 1);
        }

        public final void appendTo(StringBuffer buffer, int value) {
            if (value < 10) {
                buffer.append((char) (value + 48));
                return;
            }
            buffer.append((char) ((value / 10) + 48));
            buffer.append((char) ((value % 10) + 48));
        }
    }

    private static class PaddedNumberField implements NumberRule {
        private final int mField;
        private final int mSize;

        PaddedNumberField(int field, int size) {
            if (size < 3) {
                throw new IllegalArgumentException();
            }
            this.mField = field;
            this.mSize = size;
        }

        public int estimateLength() {
            return 4;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(this.mField));
        }

        public final void appendTo(StringBuffer buffer, int value) {
            int digits;
            if (value < 100) {
                int i = this.mSize;
                while (true) {
                    i--;
                    if (i >= 2) {
                        buffer.append('0');
                    } else {
                        buffer.append((char) ((value / 10) + 48));
                        buffer.append((char) ((value % 10) + 48));
                        return;
                    }
                }
            } else {
                if (value < 1000) {
                    digits = 3;
                } else {
                    Validate.isTrue(value > -1, "Negative values should not be possible", (long) value);
                    digits = Integer.toString(value).length();
                }
                int i2 = this.mSize;
                while (true) {
                    i2--;
                    if (i2 >= digits) {
                        buffer.append('0');
                    } else {
                        buffer.append(Integer.toString(value));
                        return;
                    }
                }
            }
        }
    }

    private static class TwoDigitNumberField implements NumberRule {
        private final int mField;

        TwoDigitNumberField(int field) {
            this.mField = field;
        }

        public int estimateLength() {
            return 2;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(this.mField));
        }

        public final void appendTo(StringBuffer buffer, int value) {
            if (value < 100) {
                buffer.append((char) ((value / 10) + 48));
                buffer.append((char) ((value % 10) + 48));
                return;
            }
            buffer.append(Integer.toString(value));
        }
    }

    private static class TwoDigitYearField implements NumberRule {
        static final TwoDigitYearField INSTANCE = new TwoDigitYearField();

        TwoDigitYearField() {
        }

        public int estimateLength() {
            return 2;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(1) % 100);
        }

        public final void appendTo(StringBuffer buffer, int value) {
            buffer.append((char) ((value / 10) + 48));
            buffer.append((char) ((value % 10) + 48));
        }
    }

    private static class TwoDigitMonthField implements NumberRule {
        static final TwoDigitMonthField INSTANCE = new TwoDigitMonthField();

        TwoDigitMonthField() {
        }

        public int estimateLength() {
            return 2;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            appendTo(buffer, calendar.get(2) + 1);
        }

        public final void appendTo(StringBuffer buffer, int value) {
            buffer.append((char) ((value / 10) + 48));
            buffer.append((char) ((value % 10) + 48));
        }
    }

    private static class TwelveHourField implements NumberRule {
        private final NumberRule mRule;

        TwelveHourField(NumberRule rule) {
            this.mRule = rule;
        }

        public int estimateLength() {
            return this.mRule.estimateLength();
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            int value = calendar.get(10);
            if (value == 0) {
                value = calendar.getLeastMaximum(10) + 1;
            }
            this.mRule.appendTo(buffer, value);
        }

        public void appendTo(StringBuffer buffer, int value) {
            this.mRule.appendTo(buffer, value);
        }
    }

    private static class TwentyFourHourField implements NumberRule {
        private final NumberRule mRule;

        TwentyFourHourField(NumberRule rule) {
            this.mRule = rule;
        }

        public int estimateLength() {
            return this.mRule.estimateLength();
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            int value = calendar.get(11);
            if (value == 0) {
                value = calendar.getMaximum(11) + 1;
            }
            this.mRule.appendTo(buffer, value);
        }

        public void appendTo(StringBuffer buffer, int value) {
            this.mRule.appendTo(buffer, value);
        }
    }

    private static class TimeZoneNameRule implements Rule {
        private final String mDaylight;
        private final Locale mLocale;
        private final String mStandard;
        private final int mStyle;
        private final TimeZone mTimeZone;
        private final boolean mTimeZoneForced;

        TimeZoneNameRule(TimeZone timeZone, boolean timeZoneForced, Locale locale, int style) {
            this.mTimeZone = timeZone;
            this.mTimeZoneForced = timeZoneForced;
            this.mLocale = locale;
            this.mStyle = style;
            if (timeZoneForced) {
                this.mStandard = FastDateFormat.getTimeZoneDisplay(timeZone, false, style, locale);
                this.mDaylight = FastDateFormat.getTimeZoneDisplay(timeZone, true, style, locale);
                return;
            }
            this.mStandard = null;
            this.mDaylight = null;
        }

        public int estimateLength() {
            if (this.mTimeZoneForced) {
                return Math.max(this.mStandard.length(), this.mDaylight.length());
            }
            if (this.mStyle == 0) {
                return 4;
            }
            return 40;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            if (!this.mTimeZoneForced) {
                TimeZone timeZone = calendar.getTimeZone();
                if (!timeZone.useDaylightTime() || calendar.get(16) == 0) {
                    buffer.append(FastDateFormat.getTimeZoneDisplay(timeZone, false, this.mStyle, this.mLocale));
                } else {
                    buffer.append(FastDateFormat.getTimeZoneDisplay(timeZone, true, this.mStyle, this.mLocale));
                }
            } else if (!this.mTimeZone.useDaylightTime() || calendar.get(16) == 0) {
                buffer.append(this.mStandard);
            } else {
                buffer.append(this.mDaylight);
            }
        }
    }

    private static class TimeZoneNumberRule implements Rule {
        static final TimeZoneNumberRule INSTANCE_COLON = new TimeZoneNumberRule(true);
        static final TimeZoneNumberRule INSTANCE_NO_COLON = new TimeZoneNumberRule(false);
        final boolean mColon;

        TimeZoneNumberRule(boolean colon) {
            this.mColon = colon;
        }

        public int estimateLength() {
            return 5;
        }

        public void appendTo(StringBuffer buffer, Calendar calendar) {
            int offset = calendar.get(15) + calendar.get(16);
            if (offset < 0) {
                buffer.append('-');
                offset = -offset;
            } else {
                buffer.append('+');
            }
            int hours = offset / DateUtils.MILLIS_IN_HOUR;
            buffer.append((char) ((hours / 10) + 48));
            buffer.append((char) ((hours % 10) + 48));
            if (this.mColon) {
                buffer.append(':');
            }
            int minutes = (offset / DateUtils.MILLIS_IN_MINUTE) - (hours * 60);
            buffer.append((char) ((minutes / 10) + 48));
            buffer.append((char) ((minutes % 10) + 48));
        }
    }

    private static class TimeZoneDisplayKey {
        private final Locale mLocale;
        private final int mStyle;
        private final TimeZone mTimeZone;

        TimeZoneDisplayKey(TimeZone timeZone, boolean daylight, int style, Locale locale) {
            this.mTimeZone = timeZone;
            this.mStyle = daylight ? style | Integer.MIN_VALUE : style;
            this.mLocale = locale;
        }

        public int hashCode() {
            return (this.mStyle * 31) + this.mLocale.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TimeZoneDisplayKey)) {
                return false;
            }
            TimeZoneDisplayKey other = (TimeZoneDisplayKey) obj;
            return this.mTimeZone.equals(other.mTimeZone) && this.mStyle == other.mStyle && this.mLocale.equals(other.mLocale);
        }
    }

    private static class Pair {
        private final Object mObj1;
        private final Object mObj2;

        public Pair(Object obj1, Object obj2) {
            this.mObj1 = obj1;
            this.mObj2 = obj2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Pair)) {
                return false;
            }
            Pair key = (Pair) obj;
            if (this.mObj1 != null ? this.mObj1.equals(key.mObj1) : key.mObj1 == null) {
                if (this.mObj2 != null ? this.mObj2.equals(key.mObj2) : key.mObj2 == null) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return (this.mObj1 == null ? 0 : this.mObj1.hashCode()) + (this.mObj2 == null ? 0 : this.mObj2.hashCode());
        }

        public String toString() {
            return new StringBuffer().append("[").append(this.mObj1).append(':').append(this.mObj2).append(']').toString();
        }
    }
}
