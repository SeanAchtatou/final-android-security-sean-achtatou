package org.apache.commons.lang;

import org.apache.commons.lang.text.StrBuilder;

public final class NumberRange {
    private final Number max;
    private final Number min;

    public NumberRange(Number num) {
        if (num == null) {
            throw new NullPointerException("The number must not be null");
        }
        this.min = num;
        this.max = num;
    }

    public NumberRange(Number min2, Number max2) {
        if (min2 == null) {
            throw new NullPointerException("The minimum value must not be null");
        } else if (max2 == null) {
            throw new NullPointerException("The maximum value must not be null");
        } else if (max2.doubleValue() < min2.doubleValue()) {
            this.max = min2;
            this.min = min2;
        } else {
            this.min = min2;
            this.max = max2;
        }
    }

    public Number getMinimum() {
        return this.min;
    }

    public Number getMaximum() {
        return this.max;
    }

    public boolean includesNumber(Number number) {
        if (number == null) {
            return false;
        }
        return this.min.doubleValue() <= number.doubleValue() && this.max.doubleValue() >= number.doubleValue();
    }

    public boolean includesRange(NumberRange range) {
        if (range == null) {
            return false;
        }
        return includesNumber(range.min) && includesNumber(range.max);
    }

    public boolean overlaps(NumberRange range) {
        if (range == null) {
            return false;
        }
        return range.includesNumber(this.min) || range.includesNumber(this.max) || includesRange(range);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof NumberRange)) {
            return false;
        }
        NumberRange range = (NumberRange) obj;
        return this.min.equals(range.min) && this.max.equals(range.max);
    }

    public int hashCode() {
        int i = 17 * 37;
        return ((this.min.hashCode() + 629) * 37) + this.max.hashCode();
    }

    public String toString() {
        StrBuilder sb = new StrBuilder();
        if (this.min.doubleValue() < 0.0d) {
            sb.append('(').append(this.min).append(')');
        } else {
            sb.append(this.min);
        }
        sb.append('-');
        if (this.max.doubleValue() < 0.0d) {
            sb.append('(').append(this.max).append(')');
        } else {
            sb.append(this.max);
        }
        return sb.toString();
    }
}
