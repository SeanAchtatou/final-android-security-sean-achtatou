package org.apache.commons.lang.mutable;

public class MutableShort extends Number implements Comparable, Mutable {
    private static final long serialVersionUID = -2135791679;
    private short value;

    public MutableShort() {
    }

    public MutableShort(short value2) {
        this.value = value2;
    }

    public MutableShort(Number value2) {
        this.value = value2.shortValue();
    }

    public MutableShort(String value2) throws NumberFormatException {
        this.value = Short.parseShort(value2);
    }

    public Object getValue() {
        return new Short(this.value);
    }

    public void setValue(short value2) {
        this.value = value2;
    }

    public void setValue(Object value2) {
        setValue(((Number) value2).shortValue());
    }

    public void increment() {
        this.value = (short) (this.value + 1);
    }

    public void decrement() {
        this.value = (short) (this.value - 1);
    }

    public void add(short operand) {
        this.value = (short) (this.value + operand);
    }

    public void add(Number operand) {
        this.value = (short) (this.value + operand.shortValue());
    }

    public void subtract(short operand) {
        this.value = (short) (this.value - operand);
    }

    public void subtract(Number operand) {
        this.value = (short) (this.value - operand.shortValue());
    }

    public short shortValue() {
        return this.value;
    }

    public int intValue() {
        return this.value;
    }

    public long longValue() {
        return (long) this.value;
    }

    public float floatValue() {
        return (float) this.value;
    }

    public double doubleValue() {
        return (double) this.value;
    }

    public Short toShort() {
        return new Short(shortValue());
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof MutableShort)) {
            return false;
        }
        if (this.value == ((MutableShort) obj).shortValue()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.value;
    }

    public int compareTo(Object obj) {
        short anotherVal = ((MutableShort) obj).value;
        if (this.value < anotherVal) {
            return -1;
        }
        return this.value == anotherVal ? 0 : 1;
    }

    public String toString() {
        return String.valueOf((int) this.value);
    }
}
