package org.apache.commons.lang;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.commons.lang.text.StrBuilder;

public final class CharRange implements Serializable {
    private static final long serialVersionUID = 8270183163158333422L;
    private final char end;
    private transient String iToString;
    private final boolean negated;
    private final char start;

    /* renamed from: org.apache.commons.lang.CharRange$1  reason: invalid class name */
    static class AnonymousClass1 {
    }

    static boolean access$100(CharRange x0) {
        return x0.negated;
    }

    static char access$200(CharRange x0) {
        return x0.start;
    }

    static char access$300(CharRange x0) {
        return x0.end;
    }

    public static CharRange is(char ch) {
        return new CharRange(ch, ch, false);
    }

    public static CharRange isNot(char ch) {
        return new CharRange(ch, ch, true);
    }

    public static CharRange isIn(char start2, char end2) {
        return new CharRange(start2, end2, false);
    }

    public static CharRange isNotIn(char start2, char end2) {
        return new CharRange(start2, end2, true);
    }

    public CharRange(char ch) {
        this(ch, ch, false);
    }

    public CharRange(char ch, boolean negated2) {
        this(ch, ch, negated2);
    }

    public CharRange(char start2, char end2) {
        this(start2, end2, false);
    }

    public CharRange(char start2, char end2, boolean negated2) {
        if (start2 > end2) {
            char temp = start2;
            start2 = end2;
            end2 = temp;
        }
        this.start = start2;
        this.end = end2;
        this.negated = negated2;
    }

    public char getStart() {
        return this.start;
    }

    public char getEnd() {
        return this.end;
    }

    public boolean isNegated() {
        return this.negated;
    }

    public boolean contains(char ch) {
        return (ch >= this.start && ch <= this.end) != this.negated;
    }

    public boolean contains(CharRange range) {
        if (range != null) {
            return this.negated ? range.negated ? this.start >= range.start && this.end <= range.end : range.end < this.start || range.start > this.end : range.negated ? this.start == 0 && this.end == 65535 : this.start <= range.start && this.end >= range.end;
        }
        throw new IllegalArgumentException("The Range must not be null");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CharRange)) {
            return false;
        }
        CharRange other = (CharRange) obj;
        return this.start == other.start && this.end == other.end && this.negated == other.negated;
    }

    public int hashCode() {
        return this.start + 'S' + (this.end * 7) + (this.negated ? 1 : 0);
    }

    public String toString() {
        if (this.iToString == null) {
            StrBuilder buf = new StrBuilder(4);
            if (isNegated()) {
                buf.append('^');
            }
            buf.append(this.start);
            if (this.start != this.end) {
                buf.append('-');
                buf.append(this.end);
            }
            this.iToString = buf.toString();
        }
        return this.iToString;
    }

    public Iterator iterator() {
        return new CharacterIterator(this, null);
    }

    private static class CharacterIterator implements Iterator {
        private char current;
        private boolean hasNext;
        private final CharRange range;

        CharacterIterator(CharRange x0, AnonymousClass1 x1) {
            this(x0);
        }

        private CharacterIterator(CharRange r) {
            this.range = r;
            this.hasNext = true;
            if (!CharRange.access$100(this.range)) {
                this.current = CharRange.access$200(this.range);
            } else if (CharRange.access$200(this.range) != 0) {
                this.current = 0;
            } else if (CharRange.access$300(this.range) == 65535) {
                this.hasNext = false;
            } else {
                this.current = (char) (CharRange.access$300(this.range) + 1);
            }
        }

        private void prepareNext() {
            if (CharRange.access$100(this.range)) {
                if (this.current == 65535) {
                    this.hasNext = false;
                } else if (this.current + 1 != CharRange.access$200(this.range)) {
                    this.current = (char) (this.current + 1);
                } else if (CharRange.access$300(this.range) == 65535) {
                    this.hasNext = false;
                } else {
                    this.current = (char) (CharRange.access$300(this.range) + 1);
                }
            } else if (this.current < CharRange.access$300(this.range)) {
                this.current = (char) (this.current + 1);
            } else {
                this.hasNext = false;
            }
        }

        public boolean hasNext() {
            return this.hasNext;
        }

        public Object next() {
            if (!this.hasNext) {
                throw new NoSuchElementException();
            }
            char cur = this.current;
            prepareNext();
            return new Character(cur);
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
