package org.apache.commons.lang.reflect;

import com.speakwrite.speakwrite.network.Constants;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import org.apache.commons.lang.ClassUtils;

public class FieldUtils {
    public static Field getField(Class cls, String fieldName) {
        Field field = getField(cls, fieldName, false);
        MemberUtils.setAccessibleWorkaround(field);
        return field;
    }

    public static Field getField(Class cls, String fieldName, boolean forceAccess) {
        if (cls == null) {
            throw new IllegalArgumentException("The class must not be null");
        } else if (fieldName == null) {
            throw new IllegalArgumentException("The field name must not be null");
        } else {
            Class acls = cls;
            while (acls != null) {
                try {
                    Field field = acls.getDeclaredField(fieldName);
                    if (!Modifier.isPublic(field.getModifiers())) {
                        if (forceAccess) {
                            field.setAccessible(true);
                        } else {
                            acls = acls.getSuperclass();
                        }
                    }
                    return field;
                } catch (NoSuchFieldException e) {
                }
            }
            Field match = null;
            for (Class field2 : ClassUtils.getAllInterfaces(cls)) {
                try {
                    Field test = field2.getField(fieldName);
                    if (match != null) {
                        throw new IllegalArgumentException(new StringBuffer().append("Reference to field ").append(fieldName).append(" is ambiguous relative to ").append(cls).append("; a matching field exists on two or more implemented interfaces.").toString());
                    }
                    match = test;
                } catch (NoSuchFieldException e2) {
                }
            }
            return match;
        }
    }

    public static Field getDeclaredField(Class cls, String fieldName) {
        return getDeclaredField(cls, fieldName, false);
    }

    public static Field getDeclaredField(Class cls, String fieldName, boolean forceAccess) {
        if (cls == null) {
            throw new IllegalArgumentException("The class must not be null");
        } else if (fieldName == null) {
            throw new IllegalArgumentException("The field name must not be null");
        } else {
            try {
                Field field = cls.getDeclaredField(fieldName);
                if (!MemberUtils.isAccessible(field)) {
                    if (!forceAccess) {
                        return null;
                    }
                    field.setAccessible(true);
                }
                return field;
            } catch (NoSuchFieldException e) {
                return null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object
     arg types: [java.lang.reflect.Field, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.Class, java.lang.String):java.lang.Object
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object */
    public static Object readStaticField(Field field) throws IllegalAccessException {
        return readStaticField(field, false);
    }

    public static Object readStaticField(Field field, boolean forceAccess) throws IllegalAccessException {
        if (field == null) {
            throw new IllegalArgumentException("The field must not be null");
        } else if (Modifier.isStatic(field.getModifiers())) {
            return readField(field, (Object) null, forceAccess);
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("The field '").append(field.getName()).append("' is not static").toString());
        }
    }

    public static Object readStaticField(Class cls, String fieldName) throws IllegalAccessException {
        return readStaticField(cls, fieldName, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object
     arg types: [java.lang.reflect.Field, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.Class, java.lang.String):java.lang.Object
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object */
    public static Object readStaticField(Class cls, String fieldName, boolean forceAccess) throws IllegalAccessException {
        Field field = getField(cls, fieldName, forceAccess);
        if (field != null) {
            return readStaticField(field, false);
        }
        throw new IllegalArgumentException(new StringBuffer().append("Cannot locate field ").append(fieldName).append(" on ").append(cls).toString());
    }

    public static Object readDeclaredStaticField(Class cls, String fieldName) throws IllegalAccessException {
        return readDeclaredStaticField(cls, fieldName, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object
     arg types: [java.lang.reflect.Field, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.Class, java.lang.String):java.lang.Object
      org.apache.commons.lang.reflect.FieldUtils.readStaticField(java.lang.reflect.Field, boolean):java.lang.Object */
    public static Object readDeclaredStaticField(Class cls, String fieldName, boolean forceAccess) throws IllegalAccessException {
        Field field = getDeclaredField(cls, fieldName, forceAccess);
        if (field != null) {
            return readStaticField(field, false);
        }
        throw new IllegalArgumentException(new StringBuffer().append("Cannot locate declared field ").append(cls.getName()).append(Constants.EXT_SEPARATOR).append(fieldName).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.reflect.Field, java.lang.Object, boolean):java.lang.Object
     arg types: [java.lang.reflect.Field, java.lang.Object, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.Object, java.lang.String, boolean):java.lang.Object
      org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.reflect.Field, java.lang.Object, boolean):java.lang.Object */
    public static Object readField(Field field, Object target) throws IllegalAccessException {
        return readField(field, target, false);
    }

    public static Object readField(Field field, Object target, boolean forceAccess) throws IllegalAccessException {
        if (field == null) {
            throw new IllegalArgumentException("The field must not be null");
        }
        if (!forceAccess || field.isAccessible()) {
            MemberUtils.setAccessibleWorkaround(field);
        } else {
            field.setAccessible(true);
        }
        return field.get(target);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.Object, java.lang.String, boolean):java.lang.Object
     arg types: [java.lang.Object, java.lang.String, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.reflect.Field, java.lang.Object, boolean):java.lang.Object
      org.apache.commons.lang.reflect.FieldUtils.readField(java.lang.Object, java.lang.String, boolean):java.lang.Object */
    public static Object readField(Object target, String fieldName) throws IllegalAccessException {
        return readField(target, fieldName, false);
    }

    public static Object readField(Object target, String fieldName, boolean forceAccess) throws IllegalAccessException {
        if (target == null) {
            throw new IllegalArgumentException("target object must not be null");
        }
        Class cls = target.getClass();
        Field field = getField(cls, fieldName, forceAccess);
        if (field != null) {
            return readField(field, target);
        }
        throw new IllegalArgumentException(new StringBuffer().append("Cannot locate field ").append(fieldName).append(" on ").append(cls).toString());
    }

    public static Object readDeclaredField(Object target, String fieldName) throws IllegalAccessException {
        return readDeclaredField(target, fieldName, false);
    }

    public static Object readDeclaredField(Object target, String fieldName, boolean forceAccess) throws IllegalAccessException {
        if (target == null) {
            throw new IllegalArgumentException("target object must not be null");
        }
        Class cls = target.getClass();
        Field field = getDeclaredField(cls, fieldName, forceAccess);
        if (field != null) {
            return readField(field, target);
        }
        throw new IllegalArgumentException(new StringBuffer().append("Cannot locate declared field ").append(cls.getName()).append(Constants.EXT_SEPARATOR).append(fieldName).toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.writeStaticField(java.lang.reflect.Field, java.lang.Object, boolean):void
     arg types: [java.lang.reflect.Field, java.lang.Object, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.writeStaticField(java.lang.Class, java.lang.String, java.lang.Object):void
      org.apache.commons.lang.reflect.FieldUtils.writeStaticField(java.lang.reflect.Field, java.lang.Object, boolean):void */
    public static void writeStaticField(Field field, Object value) throws IllegalAccessException {
        writeStaticField(field, value, false);
    }

    public static void writeStaticField(Field field, Object value, boolean forceAccess) throws IllegalAccessException {
        if (field == null) {
            throw new IllegalArgumentException("The field must not be null");
        } else if (!Modifier.isStatic(field.getModifiers())) {
            throw new IllegalArgumentException(new StringBuffer().append("The field '").append(field.getName()).append("' is not static").toString());
        } else {
            writeField(field, (Object) null, value, forceAccess);
        }
    }

    public static void writeStaticField(Class cls, String fieldName, Object value) throws IllegalAccessException {
        writeStaticField(cls, fieldName, value, false);
    }

    public static void writeStaticField(Class cls, String fieldName, Object value, boolean forceAccess) throws IllegalAccessException {
        Field field = getField(cls, fieldName, forceAccess);
        if (field == null) {
            throw new IllegalArgumentException(new StringBuffer().append("Cannot locate field ").append(fieldName).append(" on ").append(cls).toString());
        }
        writeStaticField(field, value);
    }

    public static void writeDeclaredStaticField(Class cls, String fieldName, Object value) throws IllegalAccessException {
        writeDeclaredStaticField(cls, fieldName, value, false);
    }

    public static void writeDeclaredStaticField(Class cls, String fieldName, Object value, boolean forceAccess) throws IllegalAccessException {
        Field field = getDeclaredField(cls, fieldName, forceAccess);
        if (field == null) {
            throw new IllegalArgumentException(new StringBuffer().append("Cannot locate declared field ").append(cls.getName()).append(Constants.EXT_SEPARATOR).append(fieldName).toString());
        }
        writeField(field, (Object) null, value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.reflect.Field, java.lang.Object, java.lang.Object, boolean):void
     arg types: [java.lang.reflect.Field, java.lang.Object, java.lang.Object, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.Object, java.lang.String, java.lang.Object, boolean):void
      org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.reflect.Field, java.lang.Object, java.lang.Object, boolean):void */
    public static void writeField(Field field, Object target, Object value) throws IllegalAccessException {
        writeField(field, target, value, false);
    }

    public static void writeField(Field field, Object target, Object value, boolean forceAccess) throws IllegalAccessException {
        if (field == null) {
            throw new IllegalArgumentException("The field must not be null");
        }
        if (!forceAccess || field.isAccessible()) {
            MemberUtils.setAccessibleWorkaround(field);
        } else {
            field.setAccessible(true);
        }
        field.set(target, value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.Object, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.Object, java.lang.String, java.lang.Object, int]
     candidates:
      org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.reflect.Field, java.lang.Object, java.lang.Object, boolean):void
      org.apache.commons.lang.reflect.FieldUtils.writeField(java.lang.Object, java.lang.String, java.lang.Object, boolean):void */
    public static void writeField(Object target, String fieldName, Object value) throws IllegalAccessException {
        writeField(target, fieldName, value, false);
    }

    public static void writeField(Object target, String fieldName, Object value, boolean forceAccess) throws IllegalAccessException {
        if (target == null) {
            throw new IllegalArgumentException("target object must not be null");
        }
        Class cls = target.getClass();
        Field field = getField(cls, fieldName, forceAccess);
        if (field == null) {
            throw new IllegalArgumentException(new StringBuffer().append("Cannot locate declared field ").append(cls.getName()).append(Constants.EXT_SEPARATOR).append(fieldName).toString());
        }
        writeField(field, target, value);
    }

    public static void writeDeclaredField(Object target, String fieldName, Object value) throws IllegalAccessException {
        writeDeclaredField(target, fieldName, value, false);
    }

    public static void writeDeclaredField(Object target, String fieldName, Object value, boolean forceAccess) throws IllegalAccessException {
        if (target == null) {
            throw new IllegalArgumentException("target object must not be null");
        }
        Class cls = target.getClass();
        Field field = getDeclaredField(cls, fieldName, forceAccess);
        if (field == null) {
            throw new IllegalArgumentException(new StringBuffer().append("Cannot locate declared field ").append(cls.getName()).append(Constants.EXT_SEPARATOR).append(fieldName).toString());
        }
        writeField(field, target, value);
    }
}
