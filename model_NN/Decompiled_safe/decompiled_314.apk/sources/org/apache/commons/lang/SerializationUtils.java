package org.apache.commons.lang;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class SerializationUtils {
    public static Object clone(Serializable object) {
        return deserialize(serialize(object));
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0024 A[SYNTHETIC, Splitter:B:18:0x0024] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void serialize(java.io.Serializable r5, java.io.OutputStream r6) {
        /*
            if (r6 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "The OutputStream must not be null"
            r3.<init>(r4)
            throw r3
        L_0x000a:
            r1 = 0
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0019 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0019 }
            r2.writeObject(r5)     // Catch:{ IOException -> 0x002f, all -> 0x002c }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0018:
            return
        L_0x0019:
            r3 = move-exception
            r0 = r3
        L_0x001b:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0021 }
            r3.<init>(r0)     // Catch:{ all -> 0x0021 }
            throw r3     // Catch:{ all -> 0x0021 }
        L_0x0021:
            r3 = move-exception
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x002a }
        L_0x0027:
            throw r3
        L_0x0028:
            r3 = move-exception
            goto L_0x0018
        L_0x002a:
            r4 = move-exception
            goto L_0x0027
        L_0x002c:
            r3 = move-exception
            r1 = r2
            goto L_0x0022
        L_0x002f:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.SerializationUtils.serialize(java.io.Serializable, java.io.OutputStream):void");
    }

    public static byte[] serialize(Serializable obj) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        serialize(obj, baos);
        return baos.toByteArray();
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0025 A[SYNTHETIC, Splitter:B:19:0x0025] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:24:0x002b=Splitter:B:24:0x002b, B:14:0x001c=Splitter:B:14:0x001c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object deserialize(java.io.InputStream r5) {
        /*
            if (r5 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            java.lang.String r4 = "The InputStream must not be null"
            r3.<init>(r4)
            throw r3
        L_0x000a:
            r1 = 0
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x001a, IOException -> 0x0029 }
            r2.<init>(r5)     // Catch:{ ClassNotFoundException -> 0x001a, IOException -> 0x0029 }
            java.lang.Object r3 = r2.readObject()     // Catch:{ ClassNotFoundException -> 0x003c, IOException -> 0x0038, all -> 0x0035 }
            if (r2 == 0) goto L_0x0019
            r2.close()     // Catch:{ IOException -> 0x0031 }
        L_0x0019:
            return r3
        L_0x001a:
            r3 = move-exception
            r0 = r3
        L_0x001c:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0022 }
            r3.<init>(r0)     // Catch:{ all -> 0x0022 }
            throw r3     // Catch:{ all -> 0x0022 }
        L_0x0022:
            r3 = move-exception
        L_0x0023:
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ IOException -> 0x0033 }
        L_0x0028:
            throw r3
        L_0x0029:
            r3 = move-exception
            r0 = r3
        L_0x002b:
            org.apache.commons.lang.SerializationException r3 = new org.apache.commons.lang.SerializationException     // Catch:{ all -> 0x0022 }
            r3.<init>(r0)     // Catch:{ all -> 0x0022 }
            throw r3     // Catch:{ all -> 0x0022 }
        L_0x0031:
            r4 = move-exception
            goto L_0x0019
        L_0x0033:
            r4 = move-exception
            goto L_0x0028
        L_0x0035:
            r3 = move-exception
            r1 = r2
            goto L_0x0023
        L_0x0038:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x002b
        L_0x003c:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.SerializationUtils.deserialize(java.io.InputStream):java.lang.Object");
    }

    public static Object deserialize(byte[] objectData) {
        if (objectData != null) {
            return deserialize(new ByteArrayInputStream(objectData));
        }
        throw new IllegalArgumentException("The byte[] must not be null");
    }
}
