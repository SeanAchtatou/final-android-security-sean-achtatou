package org.apache.commons.lang.time;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.TimeZone;
import org.apache.commons.lang.StringUtils;

public class DateUtils {
    public static final int MILLIS_IN_DAY = 86400000;
    public static final int MILLIS_IN_HOUR = 3600000;
    public static final int MILLIS_IN_MINUTE = 60000;
    public static final int MILLIS_IN_SECOND = 1000;
    public static final long MILLIS_PER_DAY = 86400000;
    public static final long MILLIS_PER_HOUR = 3600000;
    public static final long MILLIS_PER_MINUTE = 60000;
    public static final long MILLIS_PER_SECOND = 1000;
    private static final int MODIFY_CEILING = 2;
    private static final int MODIFY_ROUND = 1;
    private static final int MODIFY_TRUNCATE = 0;
    public static final int RANGE_MONTH_MONDAY = 6;
    public static final int RANGE_MONTH_SUNDAY = 5;
    public static final int RANGE_WEEK_CENTER = 4;
    public static final int RANGE_WEEK_MONDAY = 2;
    public static final int RANGE_WEEK_RELATIVE = 3;
    public static final int RANGE_WEEK_SUNDAY = 1;
    public static final int SEMI_MONTH = 1001;
    public static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone("GMT");
    private static final int[][] fields = {new int[]{14}, new int[]{13}, new int[]{12}, new int[]{11, 10}, new int[]{5, 5, 9}, new int[]{2, SEMI_MONTH}, new int[]{1}, new int[]{0}};

    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameInstant(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            return date1.getTime() == date2.getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameInstant(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.getTime().getTime() == cal2.getTime().getTime();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static boolean isSameLocalTime(Calendar cal1, Calendar cal2) {
        if (cal1 != null && cal2 != null) {
            return cal1.get(14) == cal2.get(14) && cal1.get(13) == cal2.get(13) && cal1.get(12) == cal2.get(12) && cal1.get(10) == cal2.get(10) && cal1.get(6) == cal2.get(6) && cal1.get(1) == cal2.get(1) && cal1.get(0) == cal2.get(0) && cal1.getClass() == cal2.getClass();
        }
        throw new IllegalArgumentException("The date must not be null");
    }

    public static Date parseDate(String str, String[] parsePatterns) throws ParseException {
        return parseDateWithLeniency(str, parsePatterns, true);
    }

    public static Date parseDateStrictly(String str, String[] parsePatterns) throws ParseException {
        return parseDateWithLeniency(str, parsePatterns, false);
    }

    private static Date parseDateWithLeniency(String str, String[] parsePatterns, boolean lenient) throws ParseException {
        if (str == null || parsePatterns == null) {
            throw new IllegalArgumentException("Date and Patterns must not be null");
        }
        SimpleDateFormat parser = new SimpleDateFormat();
        parser.setLenient(lenient);
        ParsePosition pos = new ParsePosition(0);
        for (int i = 0; i < parsePatterns.length; i++) {
            String pattern = parsePatterns[i];
            if (parsePatterns[i].endsWith("ZZ")) {
                pattern = pattern.substring(0, pattern.length() - 1);
            }
            parser.applyPattern(pattern);
            pos.setIndex(0);
            String str2 = str;
            if (parsePatterns[i].endsWith("ZZ")) {
                int signIdx = indexOfSignChars(str2, 0);
                while (signIdx >= 0) {
                    str2 = reformatTimezone(str2, signIdx);
                    signIdx = indexOfSignChars(str2, signIdx + 1);
                }
            }
            Date date = parser.parse(str2, pos);
            if (date != null && pos.getIndex() == str2.length()) {
                return date;
            }
        }
        throw new ParseException(new StringBuffer().append("Unable to parse the date: ").append(str).toString(), -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.lang.StringUtils.indexOf(java.lang.String, char, int):int
     arg types: [java.lang.String, int, int]
     candidates:
      org.apache.commons.lang.StringUtils.indexOf(java.lang.String, java.lang.String, int):int
      org.apache.commons.lang.StringUtils.indexOf(java.lang.String, char, int):int */
    private static int indexOfSignChars(String str, int startPos) {
        int idx = StringUtils.indexOf(str, '+', startPos);
        if (idx < 0) {
            return StringUtils.indexOf(str, '-', startPos);
        }
        return idx;
    }

    private static String reformatTimezone(String str, int signIdx) {
        String str2 = str;
        if (signIdx < 0 || signIdx + 5 >= str.length() || !Character.isDigit(str.charAt(signIdx + 1)) || !Character.isDigit(str.charAt(signIdx + 2)) || str.charAt(signIdx + 3) != ':' || !Character.isDigit(str.charAt(signIdx + 4)) || !Character.isDigit(str.charAt(signIdx + 5))) {
            return str2;
        }
        return new StringBuffer().append(str.substring(0, signIdx + 3)).append(str.substring(signIdx + 4)).toString();
    }

    public static Date addYears(Date date, int amount) {
        return add(date, 1, amount);
    }

    public static Date addMonths(Date date, int amount) {
        return add(date, 2, amount);
    }

    public static Date addWeeks(Date date, int amount) {
        return add(date, 3, amount);
    }

    public static Date addDays(Date date, int amount) {
        return add(date, 5, amount);
    }

    public static Date addHours(Date date, int amount) {
        return add(date, 11, amount);
    }

    public static Date addMinutes(Date date, int amount) {
        return add(date, 12, amount);
    }

    public static Date addSeconds(Date date, int amount) {
        return add(date, 13, amount);
    }

    public static Date addMilliseconds(Date date, int amount) {
        return add(date, 14, amount);
    }

    public static Date add(Date date, int calendarField, int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    public static Date setYears(Date date, int amount) {
        return set(date, 1, amount);
    }

    public static Date setMonths(Date date, int amount) {
        return set(date, 2, amount);
    }

    public static Date setDays(Date date, int amount) {
        return set(date, 5, amount);
    }

    public static Date setHours(Date date, int amount) {
        return set(date, 11, amount);
    }

    public static Date setMinutes(Date date, int amount) {
        return set(date, 12, amount);
    }

    public static Date setSeconds(Date date, int amount) {
        return set(date, 13, amount);
    }

    public static Date setMilliseconds(Date date, int amount) {
        return set(date, 14, amount);
    }

    private static Date set(Date date, int calendarField, int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(calendarField, amount);
        return c.getTime();
    }

    public static Calendar toCalendar(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static Date round(Date date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, 1);
        return gval.getTime();
    }

    public static Calendar round(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar rounded = (Calendar) date.clone();
        modify(rounded, field, 1);
        return rounded;
    }

    public static Date round(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return round((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return round((Calendar) date, field).getTime();
            }
            throw new ClassCastException(new StringBuffer().append("Could not round ").append(date).toString());
        }
    }

    public static Date truncate(Date date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, 0);
        return gval.getTime();
    }

    public static Calendar truncate(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar truncated = (Calendar) date.clone();
        modify(truncated, field, 0);
        return truncated;
    }

    public static Date truncate(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return truncate((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return truncate((Calendar) date, field).getTime();
            }
            throw new ClassCastException(new StringBuffer().append("Could not truncate ").append(date).toString());
        }
    }

    public static Date ceiling(Date date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar gval = Calendar.getInstance();
        gval.setTime(date);
        modify(gval, field, 2);
        return gval.getTime();
    }

    public static Calendar ceiling(Calendar date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar ceiled = (Calendar) date.clone();
        modify(ceiled, field, 2);
        return ceiled;
    }

    public static Date ceiling(Object date, int field) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (date instanceof Date) {
            return ceiling((Date) date, field);
        } else {
            if (date instanceof Calendar) {
                return ceiling((Calendar) date, field).getTime();
            }
            throw new ClassCastException(new StringBuffer().append("Could not find ceiling of for type: ").append(date.getClass()).toString());
        }
    }

    /* JADX INFO: Multiple debug info for r2v27 int: [D('offsetSet' boolean), D('min' int)] */
    /* JADX INFO: Multiple debug info for r2v32 long: [D('seconds' int), D('time' long)] */
    /* JADX INFO: Multiple debug info for r2v33 long: [D('millisecs' int), D('time' long)] */
    private static void modify(Calendar val, int field, int modType) {
        long time;
        boolean roundUp;
        boolean roundUp2;
        boolean min;
        if (val.get(1) > 280000000) {
            throw new ArithmeticException("Calendar value too large for accurate calculations");
        } else if (field != 14) {
            Date date = val.getTime();
            long time2 = date.getTime();
            boolean done = false;
            int millisecs = val.get(14);
            if (modType == 0 || millisecs < 500) {
                time2 -= (long) millisecs;
            }
            if (field == 13) {
                done = true;
            }
            int seconds = val.get(13);
            if (!done && (modType == 0 || seconds < 30)) {
                time2 -= ((long) seconds) * 1000;
            }
            if (field == 12) {
                done = true;
            }
            int minutes = val.get(12);
            if (done || (modType != 0 && minutes >= 30)) {
                time = time2;
            } else {
                time = time2 - (((long) minutes) * MILLIS_PER_MINUTE);
            }
            if (date.getTime() != time) {
                date.setTime(time);
                val.setTime(date);
            }
            int i = 0;
            boolean roundUp3 = false;
            while (i < fields.length) {
                int j = 0;
                while (j < fields[i].length) {
                    if (fields[i][j] != field) {
                        j++;
                    } else if (modType != 2 && (modType != 1 || !roundUp)) {
                        return;
                    } else {
                        if (field == 1001) {
                            if (val.get(5) == 1) {
                                val.add(5, 15);
                                return;
                            }
                            val.add(5, -15);
                            val.add(2, 1);
                            return;
                        } else if (field != 9) {
                            val.add(fields[i][0], 1);
                            return;
                        } else if (val.get(11) == 0) {
                            val.add(11, 12);
                            return;
                        } else {
                            val.add(11, -12);
                            val.add(5, 1);
                            return;
                        }
                    }
                }
                int offset = 0;
                boolean offsetSet = false;
                switch (field) {
                    case 9:
                        if (fields[i][0] == 11) {
                            int offset2 = val.get(11);
                            if (offset2 >= 12) {
                                offset2 -= 12;
                            }
                            if (offset >= 6) {
                                roundUp2 = true;
                            } else {
                                roundUp2 = false;
                            }
                            offsetSet = true;
                            break;
                        }
                        break;
                    case SEMI_MONTH /*1001*/:
                        if (fields[i][0] == 5) {
                            offset = val.get(5) - 1;
                            if (offset >= 15) {
                                offset -= 15;
                            }
                            roundUp = offset > 7;
                            offsetSet = true;
                            break;
                        }
                        break;
                }
                if (!offsetSet) {
                    int min2 = val.getActualMinimum(fields[i][0]);
                    int max = val.getActualMaximum(fields[i][0]);
                    int offset3 = val.get(fields[i][0]) - min2;
                    min = offset3 > (max - min2) / 2;
                    offset = offset3;
                } else {
                    min = roundUp;
                }
                if (offset != 0) {
                    val.set(fields[i][0], val.get(fields[i][0]) - offset);
                }
                i++;
                roundUp3 = min;
            }
            throw new IllegalArgumentException(new StringBuffer().append("The field ").append(field).append(" is not supported").toString());
        }
    }

    public static Iterator iterator(Date focus, int rangeStyle) {
        if (focus == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar gval = Calendar.getInstance();
        gval.setTime(focus);
        return iterator(gval, rangeStyle);
    }

    public static Iterator iterator(Calendar focus, int rangeStyle) {
        Calendar start;
        Calendar end;
        if (focus == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        int startCutoff = 1;
        int endCutoff = 7;
        switch (rangeStyle) {
            case 1:
            case 2:
            case 3:
            case 4:
                start = truncate(focus, 5);
                end = truncate(focus, 5);
                switch (rangeStyle) {
                    case 2:
                        startCutoff = 2;
                        endCutoff = 1;
                        break;
                    case 3:
                        startCutoff = focus.get(7);
                        endCutoff = startCutoff - 1;
                        break;
                    case 4:
                        startCutoff = focus.get(7) - 3;
                        endCutoff = focus.get(7) + 3;
                        break;
                }
            case 5:
            case 6:
                start = truncate(focus, 2);
                end = (Calendar) start.clone();
                end.add(2, 1);
                end.add(5, -1);
                if (rangeStyle == 6) {
                    startCutoff = 2;
                    endCutoff = 1;
                    break;
                }
                break;
            default:
                throw new IllegalArgumentException(new StringBuffer().append("The range style ").append(rangeStyle).append(" is not valid.").toString());
        }
        if (startCutoff < 1) {
            startCutoff += 7;
        }
        if (startCutoff > 7) {
            startCutoff -= 7;
        }
        if (endCutoff < 1) {
            endCutoff += 7;
        }
        if (endCutoff > 7) {
            endCutoff -= 7;
        }
        while (start.get(7) != startCutoff) {
            start.add(5, -1);
        }
        while (end.get(7) != endCutoff) {
            end.add(5, 1);
        }
        return new DateIterator(start, end);
    }

    public static Iterator iterator(Object focus, int rangeStyle) {
        if (focus == null) {
            throw new IllegalArgumentException("The date must not be null");
        } else if (focus instanceof Date) {
            return iterator((Date) focus, rangeStyle);
        } else {
            if (focus instanceof Calendar) {
                return iterator((Calendar) focus, rangeStyle);
            }
            throw new ClassCastException(new StringBuffer().append("Could not iterate based on ").append(focus).toString());
        }
    }

    public static long getFragmentInMilliseconds(Date date, int fragment) {
        return getFragment(date, fragment, 14);
    }

    public static long getFragmentInSeconds(Date date, int fragment) {
        return getFragment(date, fragment, 13);
    }

    public static long getFragmentInMinutes(Date date, int fragment) {
        return getFragment(date, fragment, 12);
    }

    public static long getFragmentInHours(Date date, int fragment) {
        return getFragment(date, fragment, 11);
    }

    public static long getFragmentInDays(Date date, int fragment) {
        return getFragment(date, fragment, 6);
    }

    public static long getFragmentInMilliseconds(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, 14);
    }

    public static long getFragmentInSeconds(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, 13);
    }

    public static long getFragmentInMinutes(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, 12);
    }

    public static long getFragmentInHours(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, 11);
    }

    public static long getFragmentInDays(Calendar calendar, int fragment) {
        return getFragment(calendar, fragment, 6);
    }

    private static long getFragment(Date date, int fragment, int unit) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getFragment(calendar, fragment, unit);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static long getFragment(Calendar calendar, int fragment, int unit) {
        if (calendar == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        long millisPerUnit = getMillisPerUnit(unit);
        long result = 0;
        switch (fragment) {
            case 1:
                result = 0 + ((((long) calendar.get(6)) * MILLIS_PER_DAY) / millisPerUnit);
                break;
            case 2:
                result = 0 + ((((long) calendar.get(5)) * MILLIS_PER_DAY) / millisPerUnit);
                break;
        }
        switch (fragment) {
            case 1:
            case 2:
            case 5:
            case 6:
                result += (((long) calendar.get(11)) * MILLIS_PER_HOUR) / millisPerUnit;
                result += (((long) calendar.get(12)) * MILLIS_PER_MINUTE) / millisPerUnit;
                result += (((long) calendar.get(13)) * 1000) / millisPerUnit;
                break;
            case 3:
            case 4:
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                throw new IllegalArgumentException(new StringBuffer().append("The fragment ").append(fragment).append(" is not supported").toString());
            case 11:
                result += (((long) calendar.get(12)) * MILLIS_PER_MINUTE) / millisPerUnit;
                result += (((long) calendar.get(13)) * 1000) / millisPerUnit;
                break;
            case 12:
                result += (((long) calendar.get(13)) * 1000) / millisPerUnit;
                break;
            case 13:
                break;
            case 14:
                return result;
        }
        return result + (((long) (calendar.get(14) * 1)) / millisPerUnit);
    }

    public static boolean truncatedEquals(Calendar cal1, Calendar cal2, int field) {
        return truncatedCompareTo(cal1, cal2, field) == 0;
    }

    public static boolean truncatedEquals(Date date1, Date date2, int field) {
        return truncatedCompareTo(date1, date2, field) == 0;
    }

    public static int truncatedCompareTo(Calendar cal1, Calendar cal2, int field) {
        return truncate(cal1, field).getTime().compareTo(truncate(cal2, field).getTime());
    }

    public static int truncatedCompareTo(Date date1, Date date2, int field) {
        return truncate(date1, field).compareTo(truncate(date2, field));
    }

    private static long getMillisPerUnit(int unit) {
        switch (unit) {
            case 5:
            case 6:
                return MILLIS_PER_DAY;
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                throw new IllegalArgumentException(new StringBuffer().append("The unit ").append(unit).append(" cannot be represented is milleseconds").toString());
            case 11:
                return MILLIS_PER_HOUR;
            case 12:
                return MILLIS_PER_MINUTE;
            case 13:
                return 1000;
            case 14:
                return 1;
        }
    }

    static class DateIterator implements Iterator {
        private final Calendar endFinal;
        private final Calendar spot;

        DateIterator(Calendar startFinal, Calendar endFinal2) {
            this.endFinal = endFinal2;
            this.spot = startFinal;
            this.spot.add(5, -1);
        }

        public boolean hasNext() {
            return this.spot.before(this.endFinal);
        }

        public Object next() {
            if (this.spot.equals(this.endFinal)) {
                throw new NoSuchElementException();
            }
            this.spot.add(5, 1);
            return this.spot.clone();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
