package org.apache.commons.lang;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.http.HttpStatus;

public final class NumberUtils {
    public static int stringToInt(String str) {
        return stringToInt(str, 0);
    }

    public static int stringToInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    /* JADX INFO: Multiple debug info for r1v14 java.lang.Float: [D('dec' java.lang.String), D('f' java.lang.Float)] */
    /* JADX INFO: Multiple debug info for r1v20 java.lang.String: [D('mant' java.lang.String), D('decPos' int)] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static Number createNumber(String val) throws NumberFormatException {
        String mant;
        String mant2;
        String mant3;
        String exp;
        String exp2;
        String dec;
        if (val == null) {
            return null;
        }
        if (val.length() == 0) {
            throw new NumberFormatException("\"\" is not a valid number.");
        } else if (val.length() == 1 && !Character.isDigit(val.charAt(0))) {
            throw new NumberFormatException(new StringBuffer().append(val).append(" is not a valid number.").toString());
        } else if (val.startsWith("--")) {
            return null;
        } else {
            if (val.startsWith("0x") || val.startsWith("-0x")) {
                return createInteger(val);
            }
            char lastChar = val.charAt(val.length() - 1);
            int decPos = val.indexOf(46);
            int expPos = val.indexOf((int) HttpStatus.SC_SWITCHING_PROTOCOLS) + val.indexOf(69) + 1;
            if (decPos > -1) {
                if (expPos <= -1) {
                    dec = val.substring(decPos + 1);
                } else if (expPos < decPos) {
                    throw new NumberFormatException(new StringBuffer().append(val).append(" is not a valid number.").toString());
                } else {
                    dec = val.substring(decPos + 1, expPos);
                }
                mant2 = val.substring(0, decPos);
                mant3 = dec;
            } else {
                if (expPos > -1) {
                    mant = val.substring(0, expPos);
                } else {
                    mant = val;
                }
                mant2 = mant;
                mant3 = null;
            }
            if (!Character.isDigit(lastChar)) {
                if (expPos <= -1 || expPos >= val.length() - 1) {
                    exp2 = null;
                } else {
                    exp2 = val.substring(expPos + 1, val.length() - 1);
                }
                String numeric = val.substring(0, val.length() - 1);
                boolean allZeros = isAllZeros(mant2) && isAllZeros(exp2);
                switch (lastChar) {
                    case 'D':
                    case 'd':
                        break;
                    case 'F':
                    case HttpStatus.SC_PROCESSING /*102*/:
                        try {
                            Float f = createFloat(numeric);
                            if (!f.isInfinite() && (f.floatValue() != SystemUtils.JAVA_VERSION_FLOAT || allZeros)) {
                                return f;
                            }
                        } catch (NumberFormatException e) {
                            break;
                        }
                    case 'L':
                    case 'l':
                        if (mant3 == null && exp2 == null && ((numeric.charAt(0) == '-' && isDigits(numeric.substring(1))) || isDigits(numeric))) {
                            try {
                                return createLong(numeric);
                            } catch (NumberFormatException e2) {
                                return createBigInteger(numeric);
                            }
                        } else {
                            throw new NumberFormatException(new StringBuffer().append(val).append(" is not a valid number.").toString());
                        }
                    default:
                        throw new NumberFormatException(new StringBuffer().append(val).append(" is not a valid number.").toString());
                }
                try {
                    Double d = createDouble(numeric);
                    if (!d.isInfinite() && (((double) d.floatValue()) != 0.0d || allZeros)) {
                        return d;
                    }
                } catch (NumberFormatException e3) {
                }
                try {
                    return createBigDecimal(numeric);
                } catch (NumberFormatException e4) {
                }
            } else {
                if (expPos <= -1 || expPos >= val.length() - 1) {
                    exp = null;
                } else {
                    exp = val.substring(expPos + 1, val.length());
                }
                if (mant3 == null && exp == null) {
                    try {
                        return createInteger(val);
                    } catch (NumberFormatException e5) {
                        try {
                            return createLong(val);
                        } catch (NumberFormatException e6) {
                            return createBigInteger(val);
                        }
                    }
                } else {
                    boolean allZeros2 = isAllZeros(mant2) && isAllZeros(exp);
                    try {
                        Float f2 = createFloat(val);
                        if (!f2.isInfinite() && (f2.floatValue() != SystemUtils.JAVA_VERSION_FLOAT || allZeros2)) {
                            return f2;
                        }
                    } catch (NumberFormatException e7) {
                    }
                    try {
                        Double d2 = createDouble(val);
                        if (!d2.isInfinite() && (d2.doubleValue() != 0.0d || allZeros2)) {
                            return d2;
                        }
                    } catch (NumberFormatException e8) {
                    }
                    return createBigDecimal(val);
                }
            }
        }
    }

    private static boolean isAllZeros(String s) {
        if (s == null) {
            return true;
        }
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) != '0') {
                return false;
            }
        }
        return s.length() > 0;
    }

    public static Float createFloat(String val) {
        return Float.valueOf(val);
    }

    public static Double createDouble(String val) {
        return Double.valueOf(val);
    }

    public static Integer createInteger(String val) {
        return Integer.decode(val);
    }

    public static Long createLong(String val) {
        return Long.valueOf(val);
    }

    public static BigInteger createBigInteger(String val) {
        return new BigInteger(val);
    }

    public static BigDecimal createBigDecimal(String val) {
        return new BigDecimal(val);
    }

    public static long minimum(long a, long b, long c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static int minimum(int a, int b, int c) {
        if (b < a) {
            a = b;
        }
        if (c < a) {
            return c;
        }
        return a;
    }

    public static long maximum(long a, long b, long c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static int maximum(int a, int b, int c) {
        if (b > a) {
            a = b;
        }
        if (c > a) {
            return c;
        }
        return a;
    }

    public static int compare(double lhs, double rhs) {
        if (lhs < rhs) {
            return -1;
        }
        if (lhs > rhs) {
            return 1;
        }
        long lhsBits = Double.doubleToLongBits(lhs);
        long rhsBits = Double.doubleToLongBits(rhs);
        if (lhsBits == rhsBits) {
            return 0;
        }
        if (lhsBits < rhsBits) {
            return -1;
        }
        return 1;
    }

    public static int compare(float lhs, float rhs) {
        if (lhs < rhs) {
            return -1;
        }
        if (lhs > rhs) {
            return 1;
        }
        int lhsBits = Float.floatToIntBits(lhs);
        int rhsBits = Float.floatToIntBits(rhs);
        if (lhsBits == rhsBits) {
            return 0;
        }
        if (lhsBits < rhsBits) {
            return -1;
        }
        return 1;
    }

    public static boolean isDigits(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x010a, code lost:
        if (r2 == false) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00b6, code lost:
        if (r5 >= r1.length) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00ba, code lost:
        if (r1[r5] < '0') goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00be, code lost:
        if (r1[r5] > '9') goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00c7, code lost:
        if (r1[r5] == 'e') goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x00cd, code lost:
        if (r1[r5] != 'E') goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x00d2, code lost:
        if (r0 != false) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x00d8, code lost:
        if (r1[r5] == 'd') goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00de, code lost:
        if (r1[r5] == 'D') goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00e4, code lost:
        if (r1[r5] == 'f') goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x00ea, code lost:
        if (r1[r5] != 'F') goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00f3, code lost:
        if (r1[r5] == 'l') goto L_0x00fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x00f9, code lost:
        if (r1[r5] != 'L') goto L_0x0105;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x00fb, code lost:
        if (r2 == false) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x00fd, code lost:
        if (r4 != false) goto L_0x0102;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0108, code lost:
        if (r0 != false) goto L_0x010f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isNumber(java.lang.String r15) {
        /*
            r14 = 45
            r13 = 57
            r12 = 48
            r11 = 1
            r10 = 0
            boolean r8 = org.apache.commons.lang.StringUtils.isEmpty(r15)
            if (r8 == 0) goto L_0x0010
            r8 = r10
        L_0x000f:
            return r8
        L_0x0010:
            char[] r1 = r15.toCharArray()
            int r7 = r1.length
            r4 = 0
            r3 = 0
            r0 = 0
            r2 = 0
            char r8 = r1[r10]
            if (r8 != r14) goto L_0x0034
            r6 = r11
        L_0x001e:
            int r8 = r6 + 1
            if (r7 <= r8) goto L_0x005f
            char r8 = r1[r6]
            if (r8 != r12) goto L_0x005f
            int r8 = r6 + 1
            char r8 = r1[r8]
            r9 = 120(0x78, float:1.68E-43)
            if (r8 != r9) goto L_0x005f
            int r5 = r6 + 2
            if (r5 != r7) goto L_0x0038
            r8 = r10
            goto L_0x000f
        L_0x0034:
            r6 = r10
            goto L_0x001e
        L_0x0036:
            int r5 = r5 + 1
        L_0x0038:
            int r8 = r1.length
            if (r5 >= r8) goto L_0x005d
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x0043
            char r8 = r1[r5]
            if (r8 <= r13) goto L_0x0036
        L_0x0043:
            char r8 = r1[r5]
            r9 = 97
            if (r8 < r9) goto L_0x004f
            char r8 = r1[r5]
            r9 = 102(0x66, float:1.43E-43)
            if (r8 <= r9) goto L_0x0036
        L_0x004f:
            char r8 = r1[r5]
            r9 = 65
            if (r8 < r9) goto L_0x005b
            char r8 = r1[r5]
            r9 = 70
            if (r8 <= r9) goto L_0x0036
        L_0x005b:
            r8 = r10
            goto L_0x000f
        L_0x005d:
            r8 = r11
            goto L_0x000f
        L_0x005f:
            int r7 = r7 + -1
            r5 = r6
        L_0x0062:
            if (r5 < r7) goto L_0x006c
            int r8 = r7 + 1
            if (r5 >= r8) goto L_0x00b5
            if (r0 == 0) goto L_0x00b5
            if (r2 != 0) goto L_0x00b5
        L_0x006c:
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x0079
            char r8 = r1[r5]
            if (r8 > r13) goto L_0x0079
            r2 = 1
            r0 = 0
        L_0x0076:
            int r5 = r5 + 1
            goto L_0x0062
        L_0x0079:
            char r8 = r1[r5]
            r9 = 46
            if (r8 != r9) goto L_0x0087
            if (r3 != 0) goto L_0x0083
            if (r4 == 0) goto L_0x0085
        L_0x0083:
            r8 = r10
            goto L_0x000f
        L_0x0085:
            r3 = 1
            goto L_0x0076
        L_0x0087:
            char r8 = r1[r5]
            r9 = 101(0x65, float:1.42E-43)
            if (r8 == r9) goto L_0x0093
            char r8 = r1[r5]
            r9 = 69
            if (r8 != r9) goto L_0x00a0
        L_0x0093:
            if (r4 == 0) goto L_0x0098
            r8 = r10
            goto L_0x000f
        L_0x0098:
            if (r2 != 0) goto L_0x009d
            r8 = r10
            goto L_0x000f
        L_0x009d:
            r4 = 1
            r0 = 1
            goto L_0x0076
        L_0x00a0:
            char r8 = r1[r5]
            r9 = 43
            if (r8 == r9) goto L_0x00aa
            char r8 = r1[r5]
            if (r8 != r14) goto L_0x00b2
        L_0x00aa:
            if (r0 != 0) goto L_0x00af
            r8 = r10
            goto L_0x000f
        L_0x00af:
            r0 = 0
            r2 = 0
            goto L_0x0076
        L_0x00b2:
            r8 = r10
            goto L_0x000f
        L_0x00b5:
            int r8 = r1.length
            if (r5 >= r8) goto L_0x0108
            char r8 = r1[r5]
            if (r8 < r12) goto L_0x00c3
            char r8 = r1[r5]
            if (r8 > r13) goto L_0x00c3
            r8 = r11
            goto L_0x000f
        L_0x00c3:
            char r8 = r1[r5]
            r9 = 101(0x65, float:1.42E-43)
            if (r8 == r9) goto L_0x00cf
            char r8 = r1[r5]
            r9 = 69
            if (r8 != r9) goto L_0x00d2
        L_0x00cf:
            r8 = r10
            goto L_0x000f
        L_0x00d2:
            if (r0 != 0) goto L_0x00ef
            char r8 = r1[r5]
            r9 = 100
            if (r8 == r9) goto L_0x00ec
            char r8 = r1[r5]
            r9 = 68
            if (r8 == r9) goto L_0x00ec
            char r8 = r1[r5]
            r9 = 102(0x66, float:1.43E-43)
            if (r8 == r9) goto L_0x00ec
            char r8 = r1[r5]
            r9 = 70
            if (r8 != r9) goto L_0x00ef
        L_0x00ec:
            r8 = r2
            goto L_0x000f
        L_0x00ef:
            char r8 = r1[r5]
            r9 = 108(0x6c, float:1.51E-43)
            if (r8 == r9) goto L_0x00fb
            char r8 = r1[r5]
            r9 = 76
            if (r8 != r9) goto L_0x0105
        L_0x00fb:
            if (r2 == 0) goto L_0x0102
            if (r4 != 0) goto L_0x0102
            r8 = r11
            goto L_0x000f
        L_0x0102:
            r8 = r10
            goto L_0x000f
        L_0x0105:
            r8 = r10
            goto L_0x000f
        L_0x0108:
            if (r0 != 0) goto L_0x010f
            if (r2 == 0) goto L_0x010f
            r8 = r11
            goto L_0x000f
        L_0x010f:
            r8 = r10
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.lang.NumberUtils.isNumber(java.lang.String):boolean");
    }
}
