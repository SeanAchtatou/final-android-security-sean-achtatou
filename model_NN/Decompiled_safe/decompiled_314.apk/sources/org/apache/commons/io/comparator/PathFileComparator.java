package org.apache.commons.io.comparator;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import org.apache.commons.io.IOCase;

public class PathFileComparator implements Comparator, Serializable {
    public static final Comparator PATH_COMPARATOR = new PathFileComparator();
    public static final Comparator PATH_INSENSITIVE_COMPARATOR = new PathFileComparator(IOCase.INSENSITIVE);
    public static final Comparator PATH_INSENSITIVE_REVERSE = new ReverseComparator(PATH_INSENSITIVE_COMPARATOR);
    public static final Comparator PATH_REVERSE = new ReverseComparator(PATH_COMPARATOR);
    public static final Comparator PATH_SYSTEM_COMPARATOR = new PathFileComparator(IOCase.SYSTEM);
    public static final Comparator PATH_SYSTEM_REVERSE = new ReverseComparator(PATH_SYSTEM_COMPARATOR);
    private final IOCase caseSensitivity;

    public PathFileComparator() {
        this.caseSensitivity = IOCase.SENSITIVE;
    }

    public PathFileComparator(IOCase caseSensitivity2) {
        IOCase iOCase;
        if (caseSensitivity2 == null) {
            iOCase = IOCase.SENSITIVE;
        } else {
            iOCase = caseSensitivity2;
        }
        this.caseSensitivity = iOCase;
    }

    public int compare(Object obj1, Object obj2) {
        return this.caseSensitivity.checkCompareTo(((File) obj1).getPath(), ((File) obj2).getPath());
    }
}
