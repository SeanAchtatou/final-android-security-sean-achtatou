package org.apache.commons.io.output;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import org.apache.commons.io.FileUtils;

public class LockableFileWriter extends Writer {
    private static final String LCK = ".lck";
    static Class class$org$apache$commons$io$output$LockableFileWriter;
    private final File lockFile;
    private final Writer out;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.LockableFileWriter.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      org.apache.commons.io.output.LockableFileWriter.<init>(java.io.File, boolean, java.lang.String):void
      org.apache.commons.io.output.LockableFileWriter.<init>(java.lang.String, boolean, java.lang.String):void */
    public LockableFileWriter(String fileName) throws IOException {
        this(fileName, false, (String) null);
    }

    public LockableFileWriter(String fileName, boolean append) throws IOException {
        this(fileName, append, (String) null);
    }

    public LockableFileWriter(String fileName, boolean append, String lockDir) throws IOException {
        this(new File(fileName), append, lockDir);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.LockableFileWriter.<init>(java.io.File, boolean, java.lang.String):void
     arg types: [java.io.File, int, ?[OBJECT, ARRAY]]
     candidates:
      org.apache.commons.io.output.LockableFileWriter.<init>(java.lang.String, boolean, java.lang.String):void
      org.apache.commons.io.output.LockableFileWriter.<init>(java.io.File, boolean, java.lang.String):void */
    public LockableFileWriter(File file) throws IOException {
        this(file, false, (String) null);
    }

    public LockableFileWriter(File file, boolean append) throws IOException {
        this(file, append, (String) null);
    }

    public LockableFileWriter(File file, boolean append, String lockDir) throws IOException {
        this(file, null, append, lockDir);
    }

    public LockableFileWriter(File file, String encoding) throws IOException {
        this(file, encoding, false, null);
    }

    public LockableFileWriter(File file, String encoding, boolean append, String lockDir) throws IOException {
        File file2 = file.getAbsoluteFile();
        if (file2.getParentFile() != null) {
            FileUtils.forceMkdir(file2.getParentFile());
        }
        if (file2.isDirectory()) {
            throw new IOException("File specified is a directory");
        }
        File lockDirFile = new File(lockDir == null ? System.getProperty("java.io.tmpdir") : lockDir);
        FileUtils.forceMkdir(lockDirFile);
        testLockDir(lockDirFile);
        this.lockFile = new File(lockDirFile, new StringBuffer().append(file2.getName()).append(LCK).toString());
        createLock();
        this.out = initWriter(file2, encoding, append);
    }

    private void testLockDir(File lockDir) throws IOException {
        if (!lockDir.exists()) {
            throw new IOException(new StringBuffer().append("Could not find lockDir: ").append(lockDir.getAbsolutePath()).toString());
        } else if (!lockDir.canWrite()) {
            throw new IOException(new StringBuffer().append("Could not write to lockDir: ").append(lockDir.getAbsolutePath()).toString());
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    private void createLock() throws IOException {
        Class cls;
        if (class$org$apache$commons$io$output$LockableFileWriter == null) {
            cls = class$("org.apache.commons.io.output.LockableFileWriter");
            class$org$apache$commons$io$output$LockableFileWriter = cls;
        } else {
            cls = class$org$apache$commons$io$output$LockableFileWriter;
        }
        synchronized (cls) {
            if (!this.lockFile.createNewFile()) {
                throw new IOException(new StringBuffer().append("Can't write file, lock ").append(this.lockFile.getAbsolutePath()).append(" exists").toString());
            }
            this.lockFile.deleteOnExit();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.Writer initWriter(java.io.File r8, java.lang.String r9, boolean r10) throws java.io.IOException {
        /*
            r7 = this;
            boolean r1 = r8.exists()
            r2 = 0
            r4 = 0
            if (r9 != 0) goto L_0x0013
            java.io.FileWriter r5 = new java.io.FileWriter     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            java.lang.String r6 = r8.getAbsolutePath()     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            r5.<init>(r6, r10)     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            r4 = r5
        L_0x0012:
            return r4
        L_0x0013:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            java.lang.String r6 = r8.getAbsolutePath()     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            r3.<init>(r6, r10)     // Catch:{ IOException -> 0x0024, RuntimeException -> 0x0037 }
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x004e, RuntimeException -> 0x004a }
            r5.<init>(r3, r9)     // Catch:{ IOException -> 0x004e, RuntimeException -> 0x004a }
            r4 = r5
            r2 = r3
            goto L_0x0012
        L_0x0024:
            r6 = move-exception
            r0 = r6
        L_0x0026:
            org.apache.commons.io.IOUtils.closeQuietly(r4)
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            java.io.File r6 = r7.lockFile
            r6.delete()
            if (r1 != 0) goto L_0x0036
            r8.delete()
        L_0x0036:
            throw r0
        L_0x0037:
            r6 = move-exception
            r0 = r6
        L_0x0039:
            org.apache.commons.io.IOUtils.closeQuietly(r4)
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            java.io.File r6 = r7.lockFile
            r6.delete()
            if (r1 != 0) goto L_0x0049
            r8.delete()
        L_0x0049:
            throw r0
        L_0x004a:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0039
        L_0x004e:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.io.output.LockableFileWriter.initWriter(java.io.File, java.lang.String, boolean):java.io.Writer");
    }

    public void close() throws IOException {
        try {
            this.out.close();
        } finally {
            this.lockFile.delete();
        }
    }

    public void write(int idx) throws IOException {
        this.out.write(idx);
    }

    public void write(char[] chr) throws IOException {
        this.out.write(chr);
    }

    public void write(char[] chr, int st, int end) throws IOException {
        this.out.write(chr, st, end);
    }

    public void write(String str) throws IOException {
        this.out.write(str);
    }

    public void write(String str, int st, int end) throws IOException {
        this.out.write(str, st, end);
    }

    public void flush() throws IOException {
        this.out.flush();
    }
}
