package org.apache.commons.io;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

public class FileSystemUtils {
    private static final int INIT_PROBLEM = -1;
    private static final FileSystemUtils INSTANCE = new FileSystemUtils();
    private static final int OS;
    private static final int OTHER = 0;
    private static final int POSIX_UNIX = 3;
    private static final int UNIX = 2;
    private static final int WINDOWS = 1;

    static {
        int os;
        try {
            String osName = System.getProperty("os.name");
            if (osName == null) {
                throw new IOException("os.name not found");
            }
            String osName2 = osName.toLowerCase();
            if (osName2.indexOf("windows") != -1) {
                os = 1;
            } else if (osName2.indexOf("linux") != -1 || osName2.indexOf("sun os") != -1 || osName2.indexOf("sunos") != -1 || osName2.indexOf("solaris") != -1 || osName2.indexOf("mpe/ix") != -1 || osName2.indexOf("freebsd") != -1 || osName2.indexOf("irix") != -1 || osName2.indexOf("digital unix") != -1 || osName2.indexOf("unix") != -1 || osName2.indexOf("mac os x") != -1) {
                os = 2;
            } else if (osName2.indexOf("hp-ux") == -1 && osName2.indexOf("aix") == -1) {
                os = 0;
            } else {
                os = 3;
            }
            OS = os;
        } catch (Exception e) {
            os = -1;
        }
    }

    public static long freeSpace(String path) throws IOException {
        return INSTANCE.freeSpaceOS(path, OS, false);
    }

    public static long freeSpaceKb(String path) throws IOException {
        return INSTANCE.freeSpaceOS(path, OS, true);
    }

    /* access modifiers changed from: package-private */
    public long freeSpaceOS(String path, int os, boolean kb) throws IOException {
        if (path == null) {
            throw new IllegalArgumentException("Path must not be empty");
        }
        switch (os) {
            case 0:
                throw new IllegalStateException("Unsupported operating system");
            case 1:
                return kb ? freeSpaceWindows(path) / FileUtils.ONE_KB : freeSpaceWindows(path);
            case 2:
                return freeSpaceUnix(path, kb, false);
            case 3:
                return freeSpaceUnix(path, kb, true);
            default:
                throw new IllegalStateException("Exception caught when determining operating system");
        }
    }

    /* access modifiers changed from: package-private */
    public long freeSpaceWindows(String path) throws IOException {
        String path2 = FilenameUtils.normalize(path);
        if (path2.length() > 2 && path2.charAt(1) == ':') {
            path2 = path2.substring(0, 2);
        }
        List lines = performCommand(new String[]{"cmd.exe", "/C", new StringBuffer().append("dir /-c ").append(path2).toString()}, Integer.MAX_VALUE);
        for (int i = lines.size() - 1; i >= 0; i--) {
            String line = (String) lines.get(i);
            if (line.length() > 0) {
                return parseDir(line, path2);
            }
        }
        throw new IOException(new StringBuffer().append("Command line 'dir /-c' did not return any info for path '").append(path2).append("'").toString());
    }

    /* access modifiers changed from: package-private */
    public long parseDir(String line, String path) throws IOException {
        int bytesStart = 0;
        int bytesEnd = 0;
        int j = line.length() - 1;
        while (true) {
            if (j < 0) {
                break;
            } else if (Character.isDigit(line.charAt(j))) {
                bytesEnd = j + 1;
                break;
            } else {
                j--;
            }
        }
        while (true) {
            if (j < 0) {
                break;
            }
            char c = line.charAt(j);
            if (!Character.isDigit(c) && c != ',' && c != '.') {
                bytesStart = j + 1;
                break;
            }
            j--;
        }
        if (j < 0) {
            throw new IOException(new StringBuffer().append("Command line 'dir /-c' did not return valid info for path '").append(path).append("'").toString());
        }
        StringBuffer buf = new StringBuffer(line.substring(bytesStart, bytesEnd));
        int k = 0;
        while (k < buf.length()) {
            if (buf.charAt(k) == ',' || buf.charAt(k) == '.') {
                buf.deleteCharAt(k);
                k--;
            }
            k++;
        }
        return parseBytes(buf.toString(), path);
    }

    /* access modifiers changed from: package-private */
    public long freeSpaceUnix(String path, boolean kb, boolean posix) throws IOException {
        String[] cmdAttribs;
        if (path.length() == 0) {
            throw new IllegalArgumentException("Path must not be empty");
        }
        String path2 = FilenameUtils.normalize(path);
        String flags = "-";
        if (kb) {
            flags = new StringBuffer().append(flags).append("k").toString();
        }
        if (posix) {
            flags = new StringBuffer().append(flags).append("P").toString();
        }
        if (flags.length() > 1) {
            cmdAttribs = new String[]{"df", flags, path2};
        } else {
            cmdAttribs = new String[]{"df", path2};
        }
        List lines = performCommand(cmdAttribs, 3);
        if (lines.size() < 2) {
            throw new IOException(new StringBuffer().append("Command line 'df' did not return info as expected for path '").append(path2).append("'- response was ").append(lines).toString());
        }
        StringTokenizer tok = new StringTokenizer((String) lines.get(1), " ");
        if (tok.countTokens() >= 4) {
            tok.nextToken();
        } else if (tok.countTokens() != 1 || lines.size() < 3) {
            throw new IOException(new StringBuffer().append("Command line 'df' did not return data as expected for path '").append(path2).append("'- check path is valid").toString());
        } else {
            tok = new StringTokenizer((String) lines.get(2), " ");
        }
        tok.nextToken();
        tok.nextToken();
        return parseBytes(tok.nextToken(), path2);
    }

    /* access modifiers changed from: package-private */
    public long parseBytes(String freeSpace, String path) throws IOException {
        try {
            long bytes = Long.parseLong(freeSpace);
            if (bytes >= 0) {
                return bytes;
            }
            throw new IOException(new StringBuffer().append("Command line 'df' did not find free space in response for path '").append(path).append("'- check path is valid").toString());
        } catch (NumberFormatException e) {
            throw new IOException(new StringBuffer().append("Command line 'df' did not return numeric data as expected for path '").append(path).append("'- check path is valid").toString());
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List performCommand(java.lang.String[] r13, int r14) throws java.io.IOException {
        /*
            r12 = this;
            java.util.ArrayList r6 = new java.util.ArrayList
            r9 = 20
            r6.<init>(r9)
            r8 = 0
            r2 = 0
            r7 = 0
            r0 = 0
            r3 = 0
            java.lang.Process r8 = r12.openProcess(r13)     // Catch:{ InterruptedException -> 0x00ef }
            java.io.InputStream r2 = r8.getInputStream()     // Catch:{ InterruptedException -> 0x00ef }
            java.io.OutputStream r7 = r8.getOutputStream()     // Catch:{ InterruptedException -> 0x00ef }
            java.io.InputStream r0 = r8.getErrorStream()     // Catch:{ InterruptedException -> 0x00ef }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ InterruptedException -> 0x00ef }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ InterruptedException -> 0x00ef }
            r9.<init>(r2)     // Catch:{ InterruptedException -> 0x00ef }
            r4.<init>(r9)     // Catch:{ InterruptedException -> 0x00ef }
            java.lang.String r5 = r4.readLine()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
        L_0x002a:
            if (r5 == 0) goto L_0x0042
            int r9 = r6.size()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            if (r9 >= r14) goto L_0x0042
            java.lang.String r9 = r5.toLowerCase()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r5 = r9.trim()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            r6.add(r5)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r5 = r4.readLine()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            goto L_0x002a
        L_0x0042:
            r8.waitFor()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            int r9 = r8.exitValue()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            if (r9 == 0) goto L_0x00b7
            java.io.IOException r9 = new java.io.IOException     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            r10.<init>()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r11 = "Command line returned OS error code '"
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            int r11 = r8.exitValue()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r11 = "' for command "
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.util.List r11 = java.util.Arrays.asList(r13)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r10 = r10.toString()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            r9.<init>(r10)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            throw r9     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
        L_0x0076:
            r9 = move-exception
            r1 = r9
            r3 = r4
        L_0x0079:
            java.io.IOException r9 = new java.io.IOException     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ all -> 0x00a4 }
            r10.<init>()     // Catch:{ all -> 0x00a4 }
            java.lang.String r11 = "Command line threw an InterruptedException '"
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ all -> 0x00a4 }
            java.lang.String r11 = r1.getMessage()     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ all -> 0x00a4 }
            java.lang.String r11 = "' for command "
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ all -> 0x00a4 }
            java.util.List r11 = java.util.Arrays.asList(r13)     // Catch:{ all -> 0x00a4 }
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ all -> 0x00a4 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x00a4 }
            r9.<init>(r10)     // Catch:{ all -> 0x00a4 }
            throw r9     // Catch:{ all -> 0x00a4 }
        L_0x00a4:
            r9 = move-exception
        L_0x00a5:
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            org.apache.commons.io.IOUtils.closeQuietly(r7)
            org.apache.commons.io.IOUtils.closeQuietly(r0)
            org.apache.commons.io.IOUtils.closeQuietly(r3)
            if (r8 == 0) goto L_0x00b6
            r8.destroy()
        L_0x00b6:
            throw r9
        L_0x00b7:
            int r9 = r6.size()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            if (r9 != 0) goto L_0x00dd
            java.io.IOException r9 = new java.io.IOException     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.StringBuffer r10 = new java.lang.StringBuffer     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            r10.<init>()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r11 = "Command line did not return any info for command "
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.util.List r11 = java.util.Arrays.asList(r13)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.StringBuffer r10 = r10.append(r11)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            java.lang.String r10 = r10.toString()     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            r9.<init>(r10)     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
            throw r9     // Catch:{ InterruptedException -> 0x0076, all -> 0x00da }
        L_0x00da:
            r9 = move-exception
            r3 = r4
            goto L_0x00a5
        L_0x00dd:
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            org.apache.commons.io.IOUtils.closeQuietly(r7)
            org.apache.commons.io.IOUtils.closeQuietly(r0)
            org.apache.commons.io.IOUtils.closeQuietly(r4)
            if (r8 == 0) goto L_0x00ee
            r8.destroy()
        L_0x00ee:
            return r6
        L_0x00ef:
            r9 = move-exception
            r1 = r9
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.io.FileSystemUtils.performCommand(java.lang.String[], int):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public Process openProcess(String[] cmdAttribs) throws IOException {
        return Runtime.getRuntime().exec(cmdAttribs);
    }
}
