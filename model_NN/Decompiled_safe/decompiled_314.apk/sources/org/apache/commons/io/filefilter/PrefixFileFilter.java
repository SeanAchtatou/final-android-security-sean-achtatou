package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.io.IOCase;

public class PrefixFileFilter extends AbstractFileFilter implements Serializable {
    private final IOCase caseSensitivity;
    private final String[] prefixes;

    public PrefixFileFilter(String prefix) {
        this(prefix, IOCase.SENSITIVE);
    }

    public PrefixFileFilter(String prefix, IOCase caseSensitivity2) {
        IOCase iOCase;
        if (prefix == null) {
            throw new IllegalArgumentException("The prefix must not be null");
        }
        this.prefixes = new String[]{prefix};
        if (caseSensitivity2 == null) {
            iOCase = IOCase.SENSITIVE;
        } else {
            iOCase = caseSensitivity2;
        }
        this.caseSensitivity = iOCase;
    }

    public PrefixFileFilter(String[] prefixes2) {
        this(prefixes2, IOCase.SENSITIVE);
    }

    public PrefixFileFilter(String[] prefixes2, IOCase caseSensitivity2) {
        IOCase iOCase;
        if (prefixes2 == null) {
            throw new IllegalArgumentException("The array of prefixes must not be null");
        }
        this.prefixes = prefixes2;
        if (caseSensitivity2 == null) {
            iOCase = IOCase.SENSITIVE;
        } else {
            iOCase = caseSensitivity2;
        }
        this.caseSensitivity = iOCase;
    }

    public PrefixFileFilter(List prefixes2) {
        this(prefixes2, IOCase.SENSITIVE);
    }

    public PrefixFileFilter(List prefixes2, IOCase caseSensitivity2) {
        IOCase iOCase;
        if (prefixes2 == null) {
            throw new IllegalArgumentException("The list of prefixes must not be null");
        }
        this.prefixes = (String[]) prefixes2.toArray(new String[prefixes2.size()]);
        if (caseSensitivity2 == null) {
            iOCase = IOCase.SENSITIVE;
        } else {
            iOCase = caseSensitivity2;
        }
        this.caseSensitivity = iOCase;
    }

    public boolean accept(File file) {
        String name = file.getName();
        for (String checkStartsWith : this.prefixes) {
            if (this.caseSensitivity.checkStartsWith(name, checkStartsWith)) {
                return true;
            }
        }
        return false;
    }

    public boolean accept(File file, String name) {
        for (String checkStartsWith : this.prefixes) {
            if (this.caseSensitivity.checkStartsWith(name, checkStartsWith)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(super.toString());
        buffer.append("(");
        if (this.prefixes != null) {
            for (int i = 0; i < this.prefixes.length; i++) {
                if (i > 0) {
                    buffer.append(",");
                }
                buffer.append(this.prefixes[i]);
            }
        }
        buffer.append(")");
        return buffer.toString();
    }
}
