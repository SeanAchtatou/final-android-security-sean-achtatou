package org.apache.commons.io;

import com.speakwrite.speakwrite.network.Constants;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.output.NullOutputStream;

public class FileUtils {
    public static final File[] EMPTY_FILE_ARRAY = new File[0];
    public static final long ONE_GB = 1073741824;
    public static final long ONE_KB = 1024;
    public static final long ONE_MB = 1048576;

    public static FileInputStream openInputStream(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("File '").append(file).append("' does not exist").toString());
        } else if (file.isDirectory()) {
            throw new IOException(new StringBuffer().append("File '").append(file).append("' exists but is a directory").toString());
        } else if (file.canRead()) {
            return new FileInputStream(file);
        } else {
            throw new IOException(new StringBuffer().append("File '").append(file).append("' cannot be read").toString());
        }
    }

    public static FileOutputStream openOutputStream(File file) throws IOException {
        if (!file.exists()) {
            File parent = file.getParentFile();
            if (parent != null && !parent.exists() && !parent.mkdirs()) {
                throw new IOException(new StringBuffer().append("File '").append(file).append("' could not be created").toString());
            }
        } else if (file.isDirectory()) {
            throw new IOException(new StringBuffer().append("File '").append(file).append("' exists but is a directory").toString());
        } else if (!file.canWrite()) {
            throw new IOException(new StringBuffer().append("File '").append(file).append("' cannot be written to").toString());
        }
        return new FileOutputStream(file);
    }

    public static String byteCountToDisplaySize(long size) {
        if (size / ONE_GB > 0) {
            return new StringBuffer().append(String.valueOf(size / ONE_GB)).append(" GB").toString();
        }
        if (size / ONE_MB > 0) {
            return new StringBuffer().append(String.valueOf(size / ONE_MB)).append(" MB").toString();
        }
        if (size / ONE_KB > 0) {
            return new StringBuffer().append(String.valueOf(size / ONE_KB)).append(" KB").toString();
        }
        return new StringBuffer().append(String.valueOf(size)).append(" bytes").toString();
    }

    public static void touch(File file) throws IOException {
        if (!file.exists()) {
            IOUtils.closeQuietly(openOutputStream(file));
        }
        if (!file.setLastModified(System.currentTimeMillis())) {
            throw new IOException(new StringBuffer().append("Unable to set the last modification time for ").append(file).toString());
        }
    }

    public static File[] convertFileCollectionToFileArray(Collection files) {
        return (File[]) files.toArray(new File[files.size()]);
    }

    private static void innerListFiles(Collection files, File directory, IOFileFilter filter) {
        File[] found = directory.listFiles((FileFilter) filter);
        if (found != null) {
            for (int i = 0; i < found.length; i++) {
                if (found[i].isDirectory()) {
                    innerListFiles(files, found[i], filter);
                } else {
                    files.add(found[i]);
                }
            }
        }
    }

    public static Collection listFiles(File directory, IOFileFilter fileFilter, IOFileFilter dirFilter) {
        IOFileFilter effDirFilter;
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("Parameter 'directory' is not a directory");
        } else if (fileFilter == null) {
            throw new NullPointerException("Parameter 'fileFilter' is null");
        } else {
            IOFileFilter effFileFilter = FileFilterUtils.andFileFilter(fileFilter, FileFilterUtils.notFileFilter(DirectoryFileFilter.INSTANCE));
            if (dirFilter == null) {
                effDirFilter = FalseFileFilter.INSTANCE;
            } else {
                effDirFilter = FileFilterUtils.andFileFilter(dirFilter, DirectoryFileFilter.INSTANCE);
            }
            Collection files = new LinkedList();
            innerListFiles(files, directory, FileFilterUtils.orFileFilter(effFileFilter, effDirFilter));
            return files;
        }
    }

    public static Iterator iterateFiles(File directory, IOFileFilter fileFilter, IOFileFilter dirFilter) {
        return listFiles(directory, fileFilter, dirFilter).iterator();
    }

    private static String[] toSuffixes(String[] extensions) {
        String[] suffixes = new String[extensions.length];
        for (int i = 0; i < extensions.length; i++) {
            suffixes[i] = new StringBuffer().append(Constants.EXT_SEPARATOR).append(extensions[i]).toString();
        }
        return suffixes;
    }

    public static Collection listFiles(File directory, String[] extensions, boolean recursive) {
        IOFileFilter filter;
        if (extensions == null) {
            filter = TrueFileFilter.INSTANCE;
        } else {
            filter = new SuffixFileFilter(toSuffixes(extensions));
        }
        return listFiles(directory, filter, recursive ? TrueFileFilter.INSTANCE : FalseFileFilter.INSTANCE);
    }

    public static Iterator iterateFiles(File directory, String[] extensions, boolean recursive) {
        return listFiles(directory, extensions, recursive).iterator();
    }

    public static boolean contentEquals(File file1, File file2) throws IOException {
        InputStream input2;
        boolean file1Exists = file1.exists();
        if (file1Exists != file2.exists()) {
            return false;
        }
        if (!file1Exists) {
            return true;
        }
        if (file1.isDirectory() || file2.isDirectory()) {
            throw new IOException("Can't compare directories, only files");
        } else if (file1.length() != file2.length()) {
            return false;
        } else {
            if (file1.getCanonicalFile().equals(file2.getCanonicalFile())) {
                return true;
            }
            InputStream input1 = null;
            InputStream input22 = null;
            try {
                InputStream input12 = new FileInputStream(file1);
                try {
                    input2 = new FileInputStream(file2);
                } catch (Throwable th) {
                    th = th;
                    input1 = input12;
                    IOUtils.closeQuietly(input1);
                    IOUtils.closeQuietly(input22);
                    throw th;
                }
                try {
                    boolean contentEquals = IOUtils.contentEquals(input12, input2);
                    IOUtils.closeQuietly(input12);
                    IOUtils.closeQuietly(input2);
                    return contentEquals;
                } catch (Throwable th2) {
                    th = th2;
                    input22 = input2;
                    input1 = input12;
                    IOUtils.closeQuietly(input1);
                    IOUtils.closeQuietly(input22);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                IOUtils.closeQuietly(input1);
                IOUtils.closeQuietly(input22);
                throw th;
            }
        }
    }

    public static File toFile(URL url) {
        if (url == null || !url.getProtocol().equals("file")) {
            return null;
        }
        String filename = url.getFile().replace((char) IOUtils.DIR_SEPARATOR_UNIX, File.separatorChar);
        int pos = 0;
        while (true) {
            pos = filename.indexOf(37, pos);
            if (pos < 0) {
                return new File(filename);
            }
            if (pos + 2 < filename.length()) {
                filename = new StringBuffer().append(filename.substring(0, pos)).append((char) Integer.parseInt(filename.substring(pos + 1, pos + 3), 16)).append(filename.substring(pos + 3)).toString();
            }
        }
    }

    public static File[] toFiles(URL[] urls) {
        if (urls == null || urls.length == 0) {
            return EMPTY_FILE_ARRAY;
        }
        File[] files = new File[urls.length];
        for (int i = 0; i < urls.length; i++) {
            URL url = urls[i];
            if (url != null) {
                if (!url.getProtocol().equals("file")) {
                    throw new IllegalArgumentException(new StringBuffer().append("URL could not be converted to a File: ").append(url).toString());
                }
                files[i] = toFile(url);
            }
        }
        return files;
    }

    public static URL[] toURLs(File[] files) throws IOException {
        URL[] urls = new URL[files.length];
        for (int i = 0; i < urls.length; i++) {
            urls[i] = files[i].toURL();
        }
        return urls;
    }

    public static void copyFileToDirectory(File srcFile, File destDir) throws IOException {
        copyFileToDirectory(srcFile, destDir, true);
    }

    public static void copyFileToDirectory(File srcFile, File destDir, boolean preserveFileDate) throws IOException {
        if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!destDir.exists() || destDir.isDirectory()) {
            copyFile(srcFile, new File(destDir, srcFile.getName()), preserveFileDate);
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("Destination '").append(destDir).append("' is not a directory").toString());
        }
    }

    public static void copyFile(File srcFile, File destFile) throws IOException {
        copyFile(srcFile, destFile, true);
    }

    public static void copyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
        if (srcFile == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destFile == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!srcFile.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(srcFile).append("' does not exist").toString());
        } else if (srcFile.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(srcFile).append("' exists but is a directory").toString());
        } else if (srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
            throw new IOException(new StringBuffer().append("Source '").append(srcFile).append("' and destination '").append(destFile).append("' are the same").toString());
        } else if (destFile.getParentFile() != null && !destFile.getParentFile().exists() && !destFile.getParentFile().mkdirs()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destFile).append("' directory cannot be created").toString());
        } else if (!destFile.exists() || destFile.canWrite()) {
            doCopyFile(srcFile, destFile, preserveFileDate);
        } else {
            throw new IOException(new StringBuffer().append("Destination '").append(destFile).append("' exists but is read-only").toString());
        }
    }

    private static void doCopyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
        FileOutputStream output;
        if (!destFile.exists() || !destFile.isDirectory()) {
            FileInputStream input = new FileInputStream(srcFile);
            try {
                output = new FileOutputStream(destFile);
                IOUtils.copy(input, output);
                IOUtils.closeQuietly(output);
                IOUtils.closeQuietly(input);
                if (srcFile.length() != destFile.length()) {
                    throw new IOException(new StringBuffer().append("Failed to copy full contents from '").append(srcFile).append("' to '").append(destFile).append("'").toString());
                } else if (preserveFileDate) {
                    destFile.setLastModified(srcFile.lastModified());
                }
            } catch (Throwable th) {
                IOUtils.closeQuietly(input);
                throw th;
            }
        } else {
            throw new IOException(new StringBuffer().append("Destination '").append(destFile).append("' exists but is a directory").toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, java.io.FileFilter):void
      org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, boolean):void */
    public static void copyDirectoryToDirectory(File srcDir, File destDir) throws IOException {
        if (srcDir == null) {
            throw new NullPointerException("Source must not be null");
        } else if (srcDir.exists() && !srcDir.isDirectory()) {
            throw new IllegalArgumentException(new StringBuffer().append("Source '").append(destDir).append("' is not a directory").toString());
        } else if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!destDir.exists() || destDir.isDirectory()) {
            copyDirectory(srcDir, new File(destDir, srcDir.getName()), true);
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("Destination '").append(destDir).append("' is not a directory").toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, java.io.FileFilter):void
      org.apache.commons.io.FileUtils.copyDirectory(java.io.File, java.io.File, boolean):void */
    public static void copyDirectory(File srcDir, File destDir) throws IOException {
        copyDirectory(srcDir, destDir, true);
    }

    public static void copyDirectory(File srcDir, File destDir, boolean preserveFileDate) throws IOException {
        copyDirectory(srcDir, destDir, null, preserveFileDate);
    }

    public static void copyDirectory(File srcDir, File destDir, FileFilter filter) throws IOException {
        copyDirectory(srcDir, destDir, filter, true);
    }

    public static void copyDirectory(File srcDir, File destDir, FileFilter filter, boolean preserveFileDate) throws IOException {
        if (srcDir == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!srcDir.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(srcDir).append("' does not exist").toString());
        } else if (!srcDir.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(srcDir).append("' exists but is not a directory").toString());
        } else if (srcDir.getCanonicalPath().equals(destDir.getCanonicalPath())) {
            throw new IOException(new StringBuffer().append("Source '").append(srcDir).append("' and destination '").append(destDir).append("' are the same").toString());
        } else {
            List exclusionList = null;
            if (destDir.getCanonicalPath().startsWith(srcDir.getCanonicalPath())) {
                File[] srcFiles = filter == null ? srcDir.listFiles() : srcDir.listFiles(filter);
                if (srcFiles != null && srcFiles.length > 0) {
                    exclusionList = new ArrayList(srcFiles.length);
                    for (File name : srcFiles) {
                        exclusionList.add(new File(destDir, name.getName()).getCanonicalPath());
                    }
                }
            }
            doCopyDirectory(srcDir, destDir, filter, preserveFileDate, exclusionList);
        }
    }

    private static void doCopyDirectory(File srcDir, File destDir, FileFilter filter, boolean preserveFileDate, List exclusionList) throws IOException {
        if (destDir.exists()) {
            if (!destDir.isDirectory()) {
                throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' exists but is not a directory").toString());
            }
        } else if (!destDir.mkdirs()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' directory cannot be created").toString());
        } else if (preserveFileDate) {
            destDir.setLastModified(srcDir.lastModified());
        }
        if (!destDir.canWrite()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' cannot be written to").toString());
        }
        File[] files = filter == null ? srcDir.listFiles() : srcDir.listFiles(filter);
        if (files == null) {
            throw new IOException(new StringBuffer().append("Failed to list contents of ").append(srcDir).toString());
        }
        for (int i = 0; i < files.length; i++) {
            File copiedFile = new File(destDir, files[i].getName());
            if (exclusionList == null || !exclusionList.contains(files[i].getCanonicalPath())) {
                if (files[i].isDirectory()) {
                    doCopyDirectory(files[i], copiedFile, filter, preserveFileDate, exclusionList);
                } else {
                    doCopyFile(files[i], copiedFile, preserveFileDate);
                }
            }
        }
    }

    public static void copyURLToFile(URL source, File destination) throws IOException {
        FileOutputStream output;
        InputStream input = source.openStream();
        try {
            output = openOutputStream(destination);
            IOUtils.copy(input, output);
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(input);
        } catch (Throwable th) {
            IOUtils.closeQuietly(input);
            throw th;
        }
    }

    public static void deleteDirectory(File directory) throws IOException {
        if (directory.exists()) {
            cleanDirectory(directory);
            if (!directory.delete()) {
                throw new IOException(new StringBuffer().append("Unable to delete directory ").append(directory).append(Constants.EXT_SEPARATOR).toString());
            }
        }
    }

    public static boolean deleteQuietly(File file) {
        if (file == null) {
            return false;
        }
        try {
            if (file.isDirectory()) {
                cleanDirectory(file);
            }
        } catch (Exception e) {
        }
        try {
            return file.delete();
        } catch (Exception e2) {
            return false;
        }
    }

    public static void cleanDirectory(File directory) throws IOException {
        if (!directory.exists()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" does not exist").toString());
        } else if (!directory.isDirectory()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" is not a directory").toString());
        } else {
            File[] files = directory.listFiles();
            if (files == null) {
                throw new IOException(new StringBuffer().append("Failed to list contents of ").append(directory).toString());
            }
            IOException exception = null;
            for (File file : files) {
                try {
                    forceDelete(file);
                } catch (IOException ioe) {
                    exception = ioe;
                }
            }
            if (exception != null) {
                throw exception;
            }
        }
    }

    public static boolean waitFor(File file, int seconds) {
        int timeout = 0;
        int tick = 0;
        while (!file.exists()) {
            int tick2 = tick + 1;
            if (tick >= 10) {
                tick = 0;
                int timeout2 = timeout + 1;
                if (timeout > seconds) {
                    return false;
                }
                timeout = timeout2;
            } else {
                tick = tick2;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            } catch (Exception e2) {
            }
        }
        return true;
    }

    public static String readFileToString(File file, String encoding) throws IOException {
        InputStream in = null;
        try {
            in = openInputStream(file);
            return IOUtils.toString(in, encoding);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    public static String readFileToString(File file) throws IOException {
        return readFileToString(file, null);
    }

    public static byte[] readFileToByteArray(File file) throws IOException {
        InputStream in = null;
        try {
            in = openInputStream(file);
            return IOUtils.toByteArray(in);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    public static List readLines(File file, String encoding) throws IOException {
        InputStream in = null;
        try {
            in = openInputStream(file);
            return IOUtils.readLines(in, encoding);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    public static List readLines(File file) throws IOException {
        return readLines(file, null);
    }

    public static LineIterator lineIterator(File file, String encoding) throws IOException {
        InputStream in = null;
        try {
            in = openInputStream(file);
            return IOUtils.lineIterator(in, encoding);
        } catch (IOException e) {
            IOException ex = e;
            IOUtils.closeQuietly(in);
            throw ex;
        } catch (RuntimeException e2) {
            RuntimeException ex2 = e2;
            IOUtils.closeQuietly(in);
            throw ex2;
        }
    }

    public static LineIterator lineIterator(File file) throws IOException {
        return lineIterator(file, null);
    }

    public static void writeStringToFile(File file, String data, String encoding) throws IOException {
        OutputStream out = null;
        try {
            out = openOutputStream(file);
            IOUtils.write(data, out, encoding);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public static void writeStringToFile(File file, String data) throws IOException {
        writeStringToFile(file, data, null);
    }

    public static void writeByteArrayToFile(File file, byte[] data) throws IOException {
        OutputStream out = null;
        try {
            out = openOutputStream(file);
            out.write(data);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public static void writeLines(File file, String encoding, Collection lines) throws IOException {
        writeLines(file, encoding, lines, null);
    }

    public static void writeLines(File file, Collection lines) throws IOException {
        writeLines(file, null, lines, null);
    }

    public static void writeLines(File file, String encoding, Collection lines, String lineEnding) throws IOException {
        OutputStream out = null;
        try {
            out = openOutputStream(file);
            IOUtils.writeLines(lines, lineEnding, out, encoding);
        } finally {
            IOUtils.closeQuietly(out);
        }
    }

    public static void writeLines(File file, Collection lines, String lineEnding) throws IOException {
        writeLines(file, null, lines, lineEnding);
    }

    public static void forceDelete(File file) throws IOException {
        if (file.isDirectory()) {
            deleteDirectory(file);
            return;
        }
        boolean filePresent = file.exists();
        if (file.delete()) {
            return;
        }
        if (!filePresent) {
            throw new FileNotFoundException(new StringBuffer().append("File does not exist: ").append(file).toString());
        }
        throw new IOException(new StringBuffer().append("Unable to delete file: ").append(file).toString());
    }

    public static void forceDeleteOnExit(File file) throws IOException {
        if (file.isDirectory()) {
            deleteDirectoryOnExit(file);
        } else {
            file.deleteOnExit();
        }
    }

    private static void deleteDirectoryOnExit(File directory) throws IOException {
        if (directory.exists()) {
            cleanDirectoryOnExit(directory);
            directory.deleteOnExit();
        }
    }

    private static void cleanDirectoryOnExit(File directory) throws IOException {
        if (!directory.exists()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" does not exist").toString());
        } else if (!directory.isDirectory()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" is not a directory").toString());
        } else {
            File[] files = directory.listFiles();
            if (files == null) {
                throw new IOException(new StringBuffer().append("Failed to list contents of ").append(directory).toString());
            }
            IOException exception = null;
            for (File file : files) {
                try {
                    forceDeleteOnExit(file);
                } catch (IOException ioe) {
                    exception = ioe;
                }
            }
            if (exception != null) {
                throw exception;
            }
        }
    }

    public static void forceMkdir(File directory) throws IOException {
        if (directory.exists()) {
            if (directory.isFile()) {
                throw new IOException(new StringBuffer().append("File ").append(directory).append(" exists and is ").append("not a directory. Unable to create directory.").toString());
            }
        } else if (!directory.mkdirs()) {
            throw new IOException(new StringBuffer().append("Unable to create directory ").append(directory).toString());
        }
    }

    public static long sizeOfDirectory(File directory) {
        long length;
        if (!directory.exists()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" does not exist").toString());
        } else if (!directory.isDirectory()) {
            throw new IllegalArgumentException(new StringBuffer().append(directory).append(" is not a directory").toString());
        } else {
            long size = 0;
            File[] files = directory.listFiles();
            if (files == null) {
                return 0;
            }
            for (File file : files) {
                if (file.isDirectory()) {
                    length = sizeOfDirectory(file);
                } else {
                    length = file.length();
                }
                size += length;
            }
            return size;
        }
    }

    public static boolean isFileNewer(File file, File reference) {
        if (reference == null) {
            throw new IllegalArgumentException("No specified reference file");
        } else if (reference.exists()) {
            return isFileNewer(file, reference.lastModified());
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("The reference file '").append(file).append("' doesn't exist").toString());
        }
    }

    public static boolean isFileNewer(File file, Date date) {
        if (date != null) {
            return isFileNewer(file, date.getTime());
        }
        throw new IllegalArgumentException("No specified date");
    }

    public static boolean isFileNewer(File file, long timeMillis) {
        if (file == null) {
            throw new IllegalArgumentException("No specified file");
        } else if (!file.exists()) {
            return false;
        } else {
            return file.lastModified() > timeMillis;
        }
    }

    public static boolean isFileOlder(File file, File reference) {
        if (reference == null) {
            throw new IllegalArgumentException("No specified reference file");
        } else if (reference.exists()) {
            return isFileOlder(file, reference.lastModified());
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("The reference file '").append(file).append("' doesn't exist").toString());
        }
    }

    public static boolean isFileOlder(File file, Date date) {
        if (date != null) {
            return isFileOlder(file, date.getTime());
        }
        throw new IllegalArgumentException("No specified date");
    }

    public static boolean isFileOlder(File file, long timeMillis) {
        if (file == null) {
            throw new IllegalArgumentException("No specified file");
        } else if (!file.exists()) {
            return false;
        } else {
            return file.lastModified() < timeMillis;
        }
    }

    public static long checksumCRC32(File file) throws IOException {
        CRC32 crc = new CRC32();
        checksum(file, crc);
        return crc.getValue();
    }

    public static Checksum checksum(File file, Checksum checksum) throws IOException {
        if (file.isDirectory()) {
            throw new IllegalArgumentException("Checksums can't be computed on directories");
        }
        InputStream in = null;
        try {
            InputStream in2 = new CheckedInputStream(new FileInputStream(file), checksum);
            try {
                IOUtils.copy(in2, new NullOutputStream());
                IOUtils.closeQuietly(in2);
                return checksum;
            } catch (Throwable th) {
                th = th;
                in = in2;
                IOUtils.closeQuietly(in);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            IOUtils.closeQuietly(in);
            throw th;
        }
    }

    public static void moveDirectory(File srcDir, File destDir) throws IOException {
        if (srcDir == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!srcDir.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(srcDir).append("' does not exist").toString());
        } else if (!srcDir.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(srcDir).append("' is not a directory").toString());
        } else if (destDir.exists()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' already exists").toString());
        } else if (!srcDir.renameTo(destDir)) {
            copyDirectory(srcDir, destDir);
            deleteDirectory(srcDir);
            if (srcDir.exists()) {
                throw new IOException(new StringBuffer().append("Failed to delete original directory '").append(srcDir).append("' after copy to '").append(destDir).append("'").toString());
            }
        }
    }

    public static void moveDirectoryToDirectory(File src, File destDir, boolean createDestDir) throws IOException {
        if (src == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination directory must not be null");
        } else {
            if (!destDir.exists() && createDestDir) {
                destDir.mkdirs();
            }
            if (!destDir.exists()) {
                throw new FileNotFoundException(new StringBuffer().append("Destination directory '").append(destDir).append("' does not exist [createDestDir=").append(createDestDir).append("]").toString());
            } else if (!destDir.isDirectory()) {
                throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' is not a directory").toString());
            } else {
                moveDirectory(src, new File(destDir, src.getName()));
            }
        }
    }

    public static void moveFile(File srcFile, File destFile) throws IOException {
        if (srcFile == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destFile == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!srcFile.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(srcFile).append("' does not exist").toString());
        } else if (srcFile.isDirectory()) {
            throw new IOException(new StringBuffer().append("Source '").append(srcFile).append("' is a directory").toString());
        } else if (destFile.exists()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destFile).append("' already exists").toString());
        } else if (destFile.isDirectory()) {
            throw new IOException(new StringBuffer().append("Destination '").append(destFile).append("' is a directory").toString());
        } else if (!srcFile.renameTo(destFile)) {
            copyFile(srcFile, destFile);
            if (!srcFile.delete()) {
                deleteQuietly(destFile);
                throw new IOException(new StringBuffer().append("Failed to delete original file '").append(srcFile).append("' after copy to '").append(destFile).append("'").toString());
            }
        }
    }

    public static void moveFileToDirectory(File srcFile, File destDir, boolean createDestDir) throws IOException {
        if (srcFile == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination directory must not be null");
        } else {
            if (!destDir.exists() && createDestDir) {
                destDir.mkdirs();
            }
            if (!destDir.exists()) {
                throw new FileNotFoundException(new StringBuffer().append("Destination directory '").append(destDir).append("' does not exist [createDestDir=").append(createDestDir).append("]").toString());
            } else if (!destDir.isDirectory()) {
                throw new IOException(new StringBuffer().append("Destination '").append(destDir).append("' is not a directory").toString());
            } else {
                moveFile(srcFile, new File(destDir, srcFile.getName()));
            }
        }
    }

    public static void moveToDirectory(File src, File destDir, boolean createDestDir) throws IOException {
        if (src == null) {
            throw new NullPointerException("Source must not be null");
        } else if (destDir == null) {
            throw new NullPointerException("Destination must not be null");
        } else if (!src.exists()) {
            throw new FileNotFoundException(new StringBuffer().append("Source '").append(src).append("' does not exist").toString());
        } else if (src.isDirectory()) {
            moveDirectoryToDirectory(src, destDir, createDestDir);
        } else {
            moveFileToDirectory(src, destDir, createDestDir);
        }
    }
}
