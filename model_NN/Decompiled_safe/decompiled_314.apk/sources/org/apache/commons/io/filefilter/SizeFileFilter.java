package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class SizeFileFilter extends AbstractFileFilter implements Serializable {
    private final boolean acceptLarger;
    private final long size;

    public SizeFileFilter(long size2) {
        this(size2, true);
    }

    public SizeFileFilter(long size2, boolean acceptLarger2) {
        if (size2 < 0) {
            throw new IllegalArgumentException("The size must be non-negative");
        }
        this.size = size2;
        this.acceptLarger = acceptLarger2;
    }

    public boolean accept(File file) {
        boolean smaller;
        if (file.length() < this.size) {
            smaller = true;
        } else {
            smaller = false;
        }
        if (!this.acceptLarger) {
            return smaller;
        }
        if (!smaller) {
            return true;
        }
        return false;
    }

    public String toString() {
        return new StringBuffer().append(super.toString()).append("(").append(this.acceptLarger ? ">=" : "<").append(this.size).append(")").toString();
    }
}
