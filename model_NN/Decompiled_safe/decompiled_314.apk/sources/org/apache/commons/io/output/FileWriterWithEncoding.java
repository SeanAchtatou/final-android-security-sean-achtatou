package org.apache.commons.io.output;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public class FileWriterWithEncoding extends Writer {
    private final Writer out;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void */
    public FileWriterWithEncoding(String filename, String encoding) throws IOException {
        this(new File(filename), encoding, false);
    }

    public FileWriterWithEncoding(String filename, String encoding, boolean append) throws IOException {
        this(new File(filename), encoding, append);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
     arg types: [java.io.File, java.nio.charset.Charset, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void */
    public FileWriterWithEncoding(String filename, Charset encoding) throws IOException {
        this(new File(filename), encoding, false);
    }

    public FileWriterWithEncoding(String filename, Charset encoding, boolean append) throws IOException {
        this(new File(filename), encoding, append);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
     arg types: [java.io.File, java.nio.charset.CharsetEncoder, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void */
    public FileWriterWithEncoding(String filename, CharsetEncoder encoding) throws IOException {
        this(new File(filename), encoding, false);
    }

    public FileWriterWithEncoding(String filename, CharsetEncoder encoding, boolean append) throws IOException {
        this(new File(filename), encoding, append);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
     arg types: [java.io.File, java.lang.String, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void */
    public FileWriterWithEncoding(File file, String encoding) throws IOException {
        this(file, encoding, false);
    }

    public FileWriterWithEncoding(File file, String encoding, boolean append) throws IOException {
        this.out = initWriter(file, encoding, append);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
     arg types: [java.io.File, java.nio.charset.Charset, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void */
    public FileWriterWithEncoding(File file, Charset encoding) throws IOException {
        this(file, encoding, false);
    }

    public FileWriterWithEncoding(File file, Charset encoding, boolean append) throws IOException {
        this.out = initWriter(file, encoding, append);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void
     arg types: [java.io.File, java.nio.charset.CharsetEncoder, int]
     candidates:
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.lang.String, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.Charset, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.lang.String, java.nio.charset.CharsetEncoder, boolean):void
      org.apache.commons.io.output.FileWriterWithEncoding.<init>(java.io.File, java.nio.charset.CharsetEncoder, boolean):void */
    public FileWriterWithEncoding(File file, CharsetEncoder encoding) throws IOException {
        this(file, encoding, false);
    }

    public FileWriterWithEncoding(File file, CharsetEncoder encoding, boolean append) throws IOException {
        this.out = initWriter(file, encoding, append);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.io.Writer initWriter(java.io.File r8, java.lang.Object r9, boolean r10) throws java.io.IOException {
        /*
            if (r8 != 0) goto L_0x000a
            java.lang.NullPointerException r6 = new java.lang.NullPointerException
            java.lang.String r7 = "File is missing"
            r6.<init>(r7)
            throw r6
        L_0x000a:
            if (r9 != 0) goto L_0x0014
            java.lang.NullPointerException r6 = new java.lang.NullPointerException
            java.lang.String r7 = "Encoding is missing"
            r6.<init>(r7)
            throw r6
        L_0x0014:
            boolean r1 = r8.exists()
            r2 = 0
            r4 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0042, RuntimeException -> 0x0050 }
            r3.<init>(r8, r10)     // Catch:{ IOException -> 0x0042, RuntimeException -> 0x0050 }
            boolean r6 = r9 instanceof java.nio.charset.Charset     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            if (r6 == 0) goto L_0x002c
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            java.nio.charset.Charset r9 = (java.nio.charset.Charset) r9     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r5.<init>(r3, r9)     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r4 = r5
        L_0x002b:
            return r4
        L_0x002c:
            boolean r6 = r9 instanceof java.nio.charset.CharsetEncoder     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            if (r6 == 0) goto L_0x0039
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            java.nio.charset.CharsetEncoder r9 = (java.nio.charset.CharsetEncoder) r9     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r5.<init>(r3, r9)     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r4 = r5
            goto L_0x002b
        L_0x0039:
            java.io.OutputStreamWriter r5 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r5.<init>(r3, r9)     // Catch:{ IOException -> 0x0062, RuntimeException -> 0x005e }
            r4 = r5
            goto L_0x002b
        L_0x0042:
            r6 = move-exception
            r0 = r6
        L_0x0044:
            org.apache.commons.io.IOUtils.closeQuietly(r4)
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            if (r1 != 0) goto L_0x004f
            org.apache.commons.io.FileUtils.deleteQuietly(r8)
        L_0x004f:
            throw r0
        L_0x0050:
            r6 = move-exception
            r0 = r6
        L_0x0052:
            org.apache.commons.io.IOUtils.closeQuietly(r4)
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            if (r1 != 0) goto L_0x005d
            org.apache.commons.io.FileUtils.deleteQuietly(r8)
        L_0x005d:
            throw r0
        L_0x005e:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0052
        L_0x0062:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.io.output.FileWriterWithEncoding.initWriter(java.io.File, java.lang.Object, boolean):java.io.Writer");
    }

    public void write(int idx) throws IOException {
        this.out.write(idx);
    }

    public void write(char[] chr) throws IOException {
        this.out.write(chr);
    }

    public void write(char[] chr, int st, int end) throws IOException {
        this.out.write(chr, st, end);
    }

    public void write(String str) throws IOException {
        this.out.write(str);
    }

    public void write(String str, int st, int end) throws IOException {
        this.out.write(str, st, end);
    }

    public void flush() throws IOException {
        this.out.flush();
    }

    public void close() throws IOException {
        this.out.close();
    }
}
