package com.speakwrite.speakwrite.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.File;

public class ImageUtils {
    public static Bitmap getBitmapFromFile(File file, int width) {
        if (file == null || !file.exists()) {
            return null;
        }
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getPath(), options);
            options.inSampleSize = computeSampleSize(options, width);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFile(file.getPath(), options);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File getFileFromUri(Uri uri, ContentResolver resolver) {
        Cursor cursor;
        if (uri == null || (cursor = resolver.query(uri, null, null, null, null)) == null || cursor.getCount() <= 0) {
            return null;
        }
        cursor.moveToFirst();
        return new File(cursor.getString(cursor.getColumnIndexOrThrow("_data")));
    }

    private static int computeSampleSize(BitmapFactory.Options options, int target) {
        int w = options.outWidth;
        int h = options.outHeight;
        int candidate = Math.max(w / target, h / target);
        if (candidate == 0) {
            return 1;
        }
        if (candidate > 1 && w > target && w / candidate < target) {
            candidate--;
        }
        if (candidate > 1 && h > target && h / candidate < target) {
            candidate--;
        }
        return candidate;
    }
}
