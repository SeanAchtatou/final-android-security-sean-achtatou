package com.speakwrite.speakwrite.utils;

import android.text.format.DateFormat;
import java.util.Calendar;

public class DateUtils {
    private final Calendar mCalendar = Calendar.getInstance();
    private final int mDay = this.mCalendar.get(5);
    private int mDayY;
    private final int mMonth = this.mCalendar.get(2);
    private int mMonthY;
    private final int mYear = this.mCalendar.get(1);
    private int mYearY;
    private final Calendar mYesterday = Calendar.getInstance();

    public static Calendar getToday() {
        return Calendar.getInstance();
    }

    public DateUtils() {
        this.mYesterday.add(5, -1);
        this.mYearY = this.mYesterday.get(1);
        this.mMonthY = this.mYesterday.get(2);
        this.mDayY = this.mYesterday.get(5);
    }

    public boolean isToday(Calendar calendar) {
        if (calendar == null) {
            return false;
        }
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);
        if (year == this.mYear && month == this.mMonth && day == this.mDay) {
            return true;
        }
        return false;
    }

    public boolean isYesterday(Calendar calendar) {
        if (calendar == null) {
            return false;
        }
        int year = calendar.get(1);
        int month = calendar.get(2);
        int day = calendar.get(5);
        if (year == this.mYearY && month == this.mMonthY && day == this.mDayY) {
            return true;
        }
        return false;
    }

    public String dateToString(Calendar calendar, String format) {
        if (calendar == null || format == null) {
            return "";
        }
        return (String) DateFormat.format(format, calendar);
    }
}
