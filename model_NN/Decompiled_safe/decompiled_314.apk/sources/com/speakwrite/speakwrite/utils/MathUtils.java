package com.speakwrite.speakwrite.utils;

public class MathUtils {
    public static long max(long n1, long n2) {
        return n1 > n2 ? n1 : n2;
    }

    public static long min(long n1, long n2) {
        return n1 < n2 ? n1 : n2;
    }
}
