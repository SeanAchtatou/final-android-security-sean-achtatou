package com.speakwrite.speakwrite.utils;

import java.text.DecimalFormat;

public class Converters {
    private static final float MULTIPLIER = 1024.0f;
    private static final DecimalFormat s_decimalFormat = new DecimalFormat("#0.00");
    private static final DecimalFormat s_percentageFormat = new DecimalFormat("#0%");

    public static String formatByteValue(long value) {
        return s_decimalFormat.format((double) ((((float) value) / MULTIPLIER) / MULTIPLIER));
    }

    public static String formatPercentage(double numerator, double denominator) {
        return s_percentageFormat.format(denominator == 0.0d ? 0.0d : numerator / denominator);
    }
}
