package com.speakwrite.speakwrite.utils;

public class TimeUtils {
    public static String millisToString(long millis) {
        int sec = (int) (millis / 1000);
        int min = sec / 60;
        int hours = min / 60;
        int sec2 = sec % 60;
        int min2 = min % 60;
        String retval = hours > 0 ? Integer.toString(hours) + ":" : "";
        if (hours > 0 && min2 < 10) {
            retval = retval + "0";
        }
        return retval + Integer.toString(min2) + ":" + getZeroLeadingTime(sec2);
    }

    public static String getZeroLeadingTime(int timeValue) {
        return (timeValue < 10 ? "0" : "") + timeValue;
    }
}
