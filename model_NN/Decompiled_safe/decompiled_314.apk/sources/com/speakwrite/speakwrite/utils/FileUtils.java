package com.speakwrite.speakwrite.utils;

import com.speakwrite.speakwrite.network.Constants;
import java.io.File;

public class FileUtils {
    public static String getMimeType(File file) {
        if (file != null) {
            String fileName = file.getName();
            if (fileName.endsWith(Constants.FILE_EXT_WAV)) {
                return Constants.MIME_AUDIO_WAV;
            }
            if (fileName.endsWith("amr")) {
                return Constants.MIME_AUDIO_AMR;
            }
            if (fileName.endsWith(Constants.FILE_EXT_3GP)) {
                return Constants.MIME_AUDIO_3GP;
            }
        }
        return Constants.MIME_AUDIO_WAV;
    }
}
