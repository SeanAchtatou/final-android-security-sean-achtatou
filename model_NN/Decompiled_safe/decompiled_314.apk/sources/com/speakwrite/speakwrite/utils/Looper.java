package com.speakwrite.speakwrite.utils;

import android.os.Handler;

public class Looper implements Runnable {
    private Handler handler;
    private Iteration iteration;
    private long period;

    public interface Iteration {
        boolean iterate();
    }

    public Looper(Iteration iteration2, long period2, Handler handler2) {
        this.iteration = iteration2;
        this.period = period2;
        this.handler = handler2;
        this.handler.post(this);
    }

    public Looper(Iteration task, long period2) {
        this(task, period2, new Handler());
    }

    public void run() {
        if (this.iteration.iterate()) {
            this.handler.postDelayed(this, this.period);
        } else {
            this.handler.removeCallbacks(this);
        }
    }

    public static void loop(Iteration task, long period2, Handler handler2) {
        new Looper(task, period2, handler2);
    }

    public static void loop(Iteration task, long period2) {
        new Looper(task, period2);
    }
}
