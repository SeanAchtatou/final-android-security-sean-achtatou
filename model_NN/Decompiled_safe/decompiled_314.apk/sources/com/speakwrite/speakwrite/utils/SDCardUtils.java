package com.speakwrite.speakwrite.utils;

import android.os.Environment;
import java.io.File;

public class SDCardUtils {
    public static final int SDCARD_STATE_MOUNT = 1;
    public static final int SDCARD_STATE_UNMOUNT = 0;

    public static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean ensurePathExist(String path) {
        if (path == null) {
            return false;
        }
        File file = new File(path);
        boolean creted = file.exists();
        if (!creted) {
            return file.mkdirs();
        }
        return creted;
    }
}
