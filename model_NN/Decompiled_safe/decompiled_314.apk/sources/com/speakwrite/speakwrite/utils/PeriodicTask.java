package com.speakwrite.speakwrite.utils;

import android.os.Handler;

public class PeriodicTask implements Runnable {
    private long delay;
    private Handler handler;
    private Task task;

    public interface Task {
        boolean doTask();
    }

    public PeriodicTask(Handler handler2, Task task2, long delay2) {
        this.handler = handler2;
        this.task = task2;
        this.delay = delay2;
        handler2.postDelayed(this, delay2);
    }

    public PeriodicTask(Handler handler2, Task task2, int delay2, boolean runImmediately) {
        this(handler2, task2, (long) delay2);
        if (!runImmediately) {
            handler2.postDelayed(this, (long) delay2);
        } else {
            handler2.post(this);
        }
    }

    public void run() {
        if (this.task.doTask()) {
            this.handler.postDelayed(this, this.delay);
        }
    }
}
