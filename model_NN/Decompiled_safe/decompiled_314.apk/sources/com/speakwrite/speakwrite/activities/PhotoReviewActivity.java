package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.speakwrite.speakwrite.AppConstants;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.jobs.PhotoInfo;
import com.speakwrite.speakwrite.utils.ImageUtils;
import java.io.File;
import java.io.IOException;

public class PhotoReviewActivity extends BaseJobActivity implements View.OnClickListener {
    private static final int DIALOG_PROGRESS_LOADING = 1;
    private static final int DIALOG_RETAKE = 2;
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    private Button mCancelButton;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public final Runnable mImageViewUpdateRunnable = new Runnable() {
        public void run() {
            if (PhotoReviewActivity.this.mImageView != null) {
                PhotoReviewActivity.this.mImageView.setImageBitmap(PhotoReviewActivity.this.mBitmap);
                PhotoReviewActivity.this.dismissDialog(1);
            }
        }
    };
    /* access modifiers changed from: private */
    public PhotoInfo mPhotoInfo;
    private long mTimeShift;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler = new Handler();
        setContentView((int) R.layout.photo_review);
        if (savedInstanceState != null) {
            getJobFromBundle(savedInstanceState);
            this.mTimeShift = PhotoPreviewActivity.getTimeShiftFromBundle(savedInstanceState);
        } else {
            getJobFromIntent();
            this.mTimeShift = PhotoPreviewActivity.getTimeShiftFromBundle(getIntent().getExtras());
        }
        initControls();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                ProgressDialog progress = new ProgressDialog(this);
                progress.setMessage(getResources().getString(R.string.loading_image));
                progress.setCancelable(false);
                return progress;
            case 2:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                TextView text = new TextView(this, null, 16842817);
                text.setMinimumWidth(250);
                text.setGravity(17);
                text.setText("Take picture from?");
                builder.setView(text);
                builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        PhotoReviewActivity.this.onRetake();
                        PhotoReviewActivity.this.finish();
                    }
                });
                builder.setNeutralButton("Camera Roll", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        PhotoReviewActivity.this.onRetakeCameraRoll();
                        PhotoReviewActivity.this.finish();
                    }
                });
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        PhotoPreviewActivity.saveTimeShift(outState, this.mTimeShift);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        PhotoHolder ph = this.job.findPhotoInfoCurrent(this.mTimeShift);
        this.mPhotoInfo = ph.get(ph.getCurrentIndex());
        this.mTimeShift = this.mPhotoInfo.time;
        if (this.mPhotoInfo.isImageLoaded()) {
            this.mBitmap = this.mPhotoInfo.getImage();
            this.mImageView.setImageBitmap(this.mBitmap);
            return;
        }
        showDialog(1);
        new Thread(new Runnable() {
            public void run() {
                Bitmap unused = PhotoReviewActivity.this.mBitmap = ImageUtils.getBitmapFromFile(new File(PhotoReviewActivity.this.mPhotoInfo.filename), AppConstants.IMAGE_PREVIEW_WIDTH);
                PhotoReviewActivity.this.mHandler.post(PhotoReviewActivity.this.mImageViewUpdateRunnable);
            }
        }).start();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retake_button:
                showDialog(2);
                return;
            case R.id.remove_button:
                onRemove();
                finish();
                return;
            case R.id.top_navigation_right_button:
                onCancel();
                finish();
                return;
            default:
                return;
        }
    }

    private void onCancel() {
    }

    private void initControls() {
        this.mImageView = (ImageView) findViewById(R.id.photo);
        ((Button) findViewById(R.id.remove_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.retake_button)).setOnClickListener(this);
        this.mCancelButton = (Button) findViewById(R.id.top_navigation_right_button);
        this.mCancelButton.setVisibility(0);
        this.mCancelButton.setText((int) R.string.cancel);
        this.mCancelButton.setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void onRetakeCameraRoll() {
        Intent intent = new Intent(this, CameraRollPreviewActivity.class);
        putJob(intent);
        CameraRollPreviewActivity.saveTimeShift(intent, this.mTimeShift);
        intent.putExtra("retake_image", true);
        startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void onRetake() {
        Intent intent = new Intent(this, PhotoPreviewActivity.class);
        putJob(intent);
        PhotoPreviewActivity.saveTimeShift(intent, this.mTimeShift);
        intent.putExtra(PhotoPreviewActivity.START_CAMERA, true);
        intent.putExtra("retake_image", true);
        startActivity(intent);
    }

    private void onRemove() {
        this.job.removePhoto(this.job.findPhotoInfoCurrent(this.mTimeShift), this.mPhotoInfo);
        try {
            this.job.save();
        } catch (IOException e) {
            toast((int) R.string.job_save_error, e);
        }
    }
}
