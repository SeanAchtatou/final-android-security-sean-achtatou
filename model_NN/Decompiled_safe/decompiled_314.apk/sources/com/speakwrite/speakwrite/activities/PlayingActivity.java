package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.activities.BaseJobActivity;
import com.speakwrite.speakwrite.audio.AMREditor;
import com.speakwrite.speakwrite.audio.AudioController;
import com.speakwrite.speakwrite.drawables.PhotoMarkerDrawable;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.jobs.PhotoInfo;
import com.speakwrite.speakwrite.utils.PeriodicTask;
import com.speakwrite.speakwrite.utils.TimeUtils;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

public class PlayingActivity extends AudioActivity {
    public static final int ACTION_APPEND = 0;
    public static final int ACTION_INSERT = 1;
    public static final String ACTION_KEY = "action_key";
    public static final int ACTION_NEW = 4;
    public static final int ACTION_OVERWRITE = 2;
    public static final int ACTION_RUN_JOB_LIST = 3;
    private static final String PLAYING_ACTIVITY_TAG = "PlayingActivity";
    private static final int REFRESH_DELAY = 500;
    public static final String START_TIME_KEY = "start_time_key";
    private Button mCameraButton;
    private Button mCameraRollButton;
    /* access modifiers changed from: private */
    public Bitmap mCurrentSmallImage;
    private Button mEraseToEndButton;
    private ViewSwitcher mImagePreviewSwitcher;
    private Button mNextImageButton;
    /* access modifiers changed from: private */
    public ImageView mPhotPreview;
    private View mPhotoMarkers;
    private PhotoMarkerDrawable mPhotoMarkersDrawable;
    private Button mPlayButton;
    private Button mPrevImageButton;
    /* access modifiers changed from: private */
    public SeekBar mProgress;
    private Button mRecordButton;
    private Button mSaveButton;
    private Button mSeekBackButton;
    /* access modifiers changed from: private */
    public final Runnable mSmallImageUpdater = new Runnable() {
        public void run() {
            if (PlayingActivity.this.mCurrentSmallImage != null) {
                PlayingActivity.this.mPhotPreview.setImageBitmap(PlayingActivity.this.mCurrentSmallImage);
                PlayingActivity.this.hideImageLoadingProgress();
            }
        }
    };
    private Button mSubmitButton;
    private TextView mTimeText;
    private PeriodicTask.Task mUpdater;
    /* access modifiers changed from: private */
    public Runnable mUpdaterHandler;

    private class SeekListener implements SeekBar.OnSeekBarChangeListener {
        private SeekListener() {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            PhotoHolder ph = PlayingActivity.this.job.findPhotoInfoCurrent((long) progress);
            if (ph != null) {
                ph.resetCurrentIndex();
            }
            if (fromUser) {
                AudioController ac = PlayingActivity.this.getAudioController();
                ac.pause();
                ac.seek((long) progress);
                PlayingActivity.this.updateProgress(ac);
                PlayingActivity.this.updateButtonState(ac);
                boolean unused = PlayingActivity.this.updateImage(ac);
                PlayingActivity.this.postUpdateHandler();
            }
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        long time;
        long j;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.playing);
        setResult(0);
        initControls();
        this.mHandler = new Handler();
        if (savedInstanceState == null) {
            try {
                getJobFromIntent();
                AudioController ac = getAudioController();
                ac.stop();
                ac.setFileName(this.job.getSoundName());
                if (ac.openFile()) {
                    long total = ac.getTotalTime();
                    int mode = getIntent().getIntExtra(ACTION_KEY, -1);
                    if (mode == 1 || mode == 2) {
                        time = getIntent().getLongExtra(START_TIME_KEY, 0);
                    } else {
                        time = total;
                    }
                    this.mProgress.setMax((int) total);
                    if (time > 0) {
                        j = time;
                    } else {
                        j = total;
                    }
                    ac.seek(j);
                    updateProgress(ac);
                    this.job.duration = total;
                    this.job.save(false);
                } else {
                    this.mProgress.setMax(0);
                    ac.seek(0);
                    updateProgress(ac);
                    this.job.duration = 0;
                    this.job.save(false);
                }
            } catch (IOException e) {
                toast((int) R.string.job_load_error, e);
            }
        } else {
            restoreState(savedInstanceState);
            getJobFromBundle(savedInstanceState);
        }
        setTitle(((String) getTitle()) + " - " + this.job.name);
        this.mProgress.setOnSeekBarChangeListener(new SeekListener());
        this.mPhotoMarkersDrawable = new PhotoMarkerDrawable(this, R.drawable.marker, this.job);
        this.mPhotoMarkers.setBackgroundDrawable(this.mPhotoMarkersDrawable);
        updatePhotoMarkers();
    }

    /* access modifiers changed from: protected */
    public void initControls() {
        super.initControls();
        this.mPlayButton = (Button) findViewById(R.id.play_pause_button);
        this.mPlayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.play();
            }
        });
        this.mRecordButton = (Button) findViewById(R.id.record_button);
        this.mRecordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.record(Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(PlayingActivity.this).getString(PlayingActivity.this.getString(R.string.default_record_action_key), "2")));
            }
        });
        this.mRecordButton.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                if (PlayingActivity.this.job.status != 1) {
                    PlayingActivity.this.askRecordAction();
                }
                return true;
            }
        });
        this.mSeekBackButton = (Button) findViewById(R.id.back_button);
        this.mSeekBackButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.seekBack();
            }
        });
        View.OnClickListener takePhotoClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                int id = v.getId();
                if (id == R.id.camera_button) {
                    PlayingActivity.this.takePhoto();
                } else if (id == R.id.image_preview || id == R.id.photo_preview_button) {
                    long timeShift = PlayingActivity.this.getAudioController().getCurrentTime();
                    Intent intent = new Intent(PlayingActivity.this, PhotoReviewActivity.class);
                    PlayingActivity.this.putJob(intent);
                    PhotoPreviewActivity.saveTimeShift(intent, timeShift);
                    PlayingActivity.this.startActivity(intent);
                }
            }
        };
        ((ImageButton) findViewById(R.id.photo_preview_button)).setOnClickListener(takePhotoClickListener);
        this.mCameraButton = (Button) findViewById(R.id.camera_button);
        this.mCameraButton.setOnClickListener(takePhotoClickListener);
        this.mCameraRollButton = (Button) findViewById(R.id.camera_roll_button);
        this.mCameraRollButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.startCameraRoll();
            }
        });
        this.mNextImageButton = (Button) findViewById(R.id.next_image_button);
        this.mNextImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.seekToNextImage();
            }
        });
        this.mPrevImageButton = (Button) findViewById(R.id.back_image_button);
        this.mPrevImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.seekToPrevImage();
            }
        });
        this.mTimeText = (TextView) findViewById(R.id.time);
        this.mProgress = (SeekBar) findViewById(R.id.progress);
        this.mProgress.setMax(0);
        this.mPhotPreview = (ImageView) findViewById(R.id.photo_preview);
        this.mImagePreviewSwitcher = (ViewSwitcher) findViewById(R.id.image_preview);
        this.mImagePreviewSwitcher.setOnClickListener(takePhotoClickListener);
        this.mEraseToEndButton = (Button) findViewById(R.id.erase_to_end);
        this.mEraseToEndButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.eraseToEnd();
            }
        });
        this.mSaveButton = (Button) findViewById(R.id.save_button);
        this.mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.saveJob();
            }
        });
        this.mSubmitButton = (Button) findViewById(R.id.submit_button);
        this.mSubmitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlayingActivity.this.submitCurrentJob();
            }
        });
        this.mPhotoMarkers = findViewById(R.id.photo_markers);
    }

    private void updatePhotoMarkers() {
        this.mPhotoMarkersDrawable.invalidateSelf();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getAudioController().pause();
        removeUpdater();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        postUpdateHandler();
        updatePhotoMarkers();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        removeUpdater();
    }

    private void restoreState(Bundle savedInstanceState) {
        AudioController ac = getAudioController();
        this.mProgress.setMax((int) ac.getTotalTime());
        updateProgress(ac);
        postUpdateHandler();
    }

    /* access modifiers changed from: private */
    public void play() {
        AudioController ac = getAudioController();
        AudioController.AudioState state = ac.getState();
        if (state == AudioController.AudioState.Paused || state == AudioController.AudioState.Stopped) {
            try {
                enableWakeLock();
                postUpdateHandler();
                updateProgress(ac);
                ac.play();
            } catch (IOException e) {
                toast((int) R.string.play_error, e);
            }
        } else {
            ac.pause();
        }
        updateButtonState(ac);
    }

    /* access modifiers changed from: protected */
    public synchronized void postUpdateHandler() {
        if (this.mUpdater == null) {
            this.mUpdater = new PeriodicTask.Task() {
                public boolean doTask() {
                    boolean shouldContinue;
                    try {
                        AudioController ac = PlayingActivity.this.getAudioController();
                        if (ac.getState() == AudioController.AudioState.Playing) {
                            shouldContinue = true;
                        } else {
                            shouldContinue = false;
                        }
                        boolean shouldContinue2 = shouldContinue | PlayingActivity.this.picturesPending();
                        PlayingActivity.this.updateProgress(ac);
                        PlayingActivity.this.updateButtonState(ac);
                        boolean shouldContinue3 = shouldContinue2 | PlayingActivity.this.updateImage(ac);
                        Log.d(PlayingActivity.PLAYING_ACTIVITY_TAG, "updater: continue? " + shouldContinue3);
                        if (!shouldContinue3) {
                            Runnable unused = PlayingActivity.this.mUpdaterHandler = null;
                        }
                        return shouldContinue3;
                    } catch (Exception e) {
                        return false;
                    }
                }
            };
        }
        if (this.mUpdaterHandler == null) {
            this.mUpdaterHandler = new PeriodicTask(this.mHandler, this.mUpdater, 500, true);
        }
    }

    /* access modifiers changed from: private */
    public boolean picturesPending() {
        return this.job.picturesPending();
    }

    /* access modifiers changed from: private */
    public boolean updateImage(AudioController ac) {
        final PhotoInfo pi = getCurrentPhotoInfo();
        if (pi == null) {
            this.mImagePreviewSwitcher.setVisibility(4);
        } else {
            this.mImagePreviewSwitcher.setVisibility(0);
            final int width = this.mImagePreviewSwitcher.getWidth();
            if (pi.isSmallImageLoaded()) {
                hideImageLoadingProgress();
                this.mCurrentSmallImage = pi.getSmallImage(width);
                this.mHandler.post(this.mSmallImageUpdater);
            } else {
                showImageLoadingProgress();
                new Thread(new Runnable() {
                    public void run() {
                        Bitmap unused = PlayingActivity.this.mCurrentSmallImage = pi.getSmallImage(width);
                        PlayingActivity.this.mHandler.post(PlayingActivity.this.mSmallImageUpdater);
                    }
                }).start();
            }
        }
        return false;
    }

    private void showImageLoadingProgress() {
        if (this.mImagePreviewSwitcher.getCurrentView().getId() == R.id.photo_preview_container) {
            this.mImagePreviewSwitcher.showNext();
        }
    }

    /* access modifiers changed from: private */
    public void hideImageLoadingProgress() {
        if (this.mImagePreviewSwitcher.getCurrentView().getId() != R.id.photo_preview_container) {
            this.mImagePreviewSwitcher.showPrevious();
        }
    }

    private PhotoInfo getCurrentPhotoInfo() {
        long time = getAudioController().getCurrentTime();
        PhotoHolder ph = this.job.findPhotoInfoCurrent(time - (time % 1000));
        if (ph != null) {
            return ph.get(ph.getCurrentIndex());
        }
        return null;
    }

    /* access modifiers changed from: private */
    public synchronized void removeUpdater() {
        if (this.mUpdaterHandler != null) {
            this.mHandler.removeCallbacks(this.mUpdaterHandler);
            this.mUpdaterHandler = null;
        }
    }

    /* access modifiers changed from: private */
    public void updateProgress(AudioController ac) {
        long time = ac.getCurrentTime();
        this.mTimeText.setText(TimeUtils.millisToString(time) + "/" + TimeUtils.millisToString(ac.getTotalTime()));
        this.mProgress.setProgress((int) time);
        this.mProgress.invalidate();
    }

    /* access modifiers changed from: protected */
    public void updateButtonState(AudioController ac) {
        AudioController.AudioState state = ac.getState();
        if (state == AudioController.AudioState.Paused || state == AudioController.AudioState.Stopped) {
            this.mPlayButton.setBackgroundResource(R.drawable.button_play_grey_set);
            this.mRecordButton.setEnabled(true);
        } else if (state == AudioController.AudioState.Playing) {
            this.mPlayButton.setBackgroundResource(R.drawable.button_pause_grey_set);
            this.mRecordButton.setEnabled(false);
        }
        if (this.job.photos == null || this.job.photos.size() <= 0) {
            this.mNextImageButton.setVisibility(4);
            this.mPrevImageButton.setVisibility(4);
            return;
        }
        this.mNextImageButton.setVisibility(0);
        this.mPrevImageButton.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void record(int action) {
        AudioController ac = getAudioController();
        Intent intent = new Intent(this, RecordingActivity.class);
        intent.addFlags(67108864);
        putJob(intent);
        ac.pause();
        long currentTime = ac.getCurrentTime();
        switch (action) {
            case 0:
                Log.d(PLAYING_ACTIVITY_TAG, "ACTION_APPEND");
                intent.putExtra(getString(R.string.time_value), ac.getTotalTime());
                break;
            case 1:
                Log.d(PLAYING_ACTIVITY_TAG, "ACTION_INSERT");
                intent.putExtra(getString(R.string.time_value), currentTime);
                break;
            case 2:
                Log.d(PLAYING_ACTIVITY_TAG, "ACTION_OVERWRITE");
                intent.putExtra(getString(R.string.time_value), currentTime);
                break;
        }
        if (currentTime >= ac.getTotalTime() - 1) {
            intent.putExtra(RecordingActivity.MODE_KEY, 0);
        } else {
            intent.putExtra(RecordingActivity.MODE_KEY, action);
        }
        setResult(2, intent);
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void makePhoto(long time) {
        getAudioController().pause();
        Intent intent = new Intent(this, PhotoPreviewActivity.class);
        putJob(intent);
        PhotoPreviewActivity.saveTimeShift(intent, time);
        intent.putExtra(PhotoPreviewActivity.START_CAMERA, true);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void askRecordAction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.record_action_dialog_title);
        builder.setItems((int) R.array.record_action_entries, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PlayingActivity.this.record(which);
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void eraseToEnd() {
        AudioController ac = getAudioController();
        if (ac.getCurrentTime() < ac.getTotalTime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.erase_to_end_title);
            builder.setMessage((int) R.string.erase_to_end_message);
            builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
            builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PlayingActivity.this.removeUpdater();
                    new BaseJobActivity.ProgressDialogTask(ProgressDialog.show(PlayingActivity.this, PlayingActivity.this.getString(R.string.erase_to_end_progress_title), PlayingActivity.this.getString(R.string.erase_to_end_progress_message), true, false), new Runnable() {
                        public void run() {
                            PlayingActivity.this.doEraseToEnd();
                        }
                    }).start();
                }
            });
            builder.show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: private */
    public void seekBack() {
        AudioController ac = getAudioController();
        ac.seek(Math.max(ac.getCurrentTime() - 5000, 0L));
        updateButtonState(ac);
        updateImage(ac);
        updateProgress(ac);
    }

    /* access modifiers changed from: private */
    public void takePhoto() {
        AudioController ac = getAudioController();
        ac.pause();
        long time = ac.getCurrentTime();
        makePhoto(time - (time % 1000));
    }

    /* access modifiers changed from: private */
    public void seekToPrevImage() {
        AudioController ac = getAudioController();
        long time = ac.getCurrentTime();
        PhotoHolder ph = this.job.findPhotoInfoCurrent(time);
        if (ph != null) {
            if (ph.size() < 2 || ph.decrementIndex() == -1) {
                ph.resetCurrentIndex();
                ph = this.job.findPhotoInfoBefore(time);
                if (ph != null) {
                    ph.resetCurrentIndex();
                }
            }
            if (ph != null) {
                PhotoInfo pi = ph.get(ph.getCurrentIndex());
                if (pi.time != time) {
                    ac.seek(pi.time);
                }
            }
        }
        updateProgress(ac);
        updateImage(ac);
        updateButtonState(ac);
        postUpdateHandler();
    }

    /* access modifiers changed from: private */
    public void seekToNextImage() {
        AudioController ac = getAudioController();
        long time = ac.getCurrentTime();
        PhotoHolder ph = this.job.findPhotoInfoCurrent(time);
        if (ph == null || ph.size() < 2 || ph.incrementIndex() == -1) {
            if (ph != null) {
                ph.resetCurrentIndex();
            }
            PhotoHolder ph2 = this.job.findPhotoInfoAfter(time);
            if (ph2 != null) {
                if (ph2.size() > 1) {
                    ph2.setCurrentIndexToStart();
                }
                ac.seek(ph2.get(ph2.getCurrentIndex()).time);
                updateProgress(ac);
                if (ph2.size() > 1) {
                    ph2.setCurrentIndexToStart();
                }
            }
        }
        updateImage(ac);
        updateButtonState(ac);
        postUpdateHandler();
    }

    /* access modifiers changed from: private */
    public void doEraseToEnd() {
        try {
            removeUpdater();
            AudioController ac = getAudioController();
            long time = ac.getCurrentTime();
            ac.stop();
            AMREditor.eraseToEnd(this.job.getSoundName(), time);
            ac.openFile();
            ac.seek(ac.getTotalTime());
            this.job.duration = ac.getTotalTime();
            PhotoEraser pe = new PhotoEraser(ac.getCurrentTime());
            this.job.findPhoto(pe);
            pe.removePhotos(this.job);
            this.job.save();
            updateAfterErasing();
        } catch (Exception e) {
            toast((int) R.string.job_save_error, e);
        }
    }

    private void updateAfterErasing() {
        updateJobPosition();
        this.mHandler.post(new Runnable() {
            public void run() {
                AudioController ac = PlayingActivity.this.getAudioController();
                int total = (int) ac.getTotalTime();
                PlayingActivity.this.mProgress.setMax(0);
                PlayingActivity.this.mProgress.setProgress(0);
                PlayingActivity.this.mProgress.invalidate();
                PlayingActivity.this.mProgress.setMax(total);
                PlayingActivity.this.mProgress.setProgress(total);
                PlayingActivity.this.mProgress.invalidate();
                PlayingActivity.this.updateButtonState(ac);
                boolean unused = PlayingActivity.this.updateImage(ac);
                PlayingActivity.this.updateProgress(ac);
            }
        });
    }

    /* access modifiers changed from: private */
    public void startCameraRoll() {
        AudioController ac = getAudioController();
        ac.pause();
        long time = ac.getCurrentTime();
        Intent intent = new Intent(this, CameraRollPreviewActivity.class);
        putJob(intent);
        CameraRollPreviewActivity.saveTimeShift(intent, time - (time % 1000));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9 && resultCode == 8) {
            setResult(8, data);
            finish();
        }
    }

    private static class PhotoEraser implements Job.PhotoComparer {
        private long duration;
        private LinkedList<PhotoHolder> removeList = new LinkedList<>();

        public PhotoEraser(long time) {
            this.duration = time;
        }

        public boolean match(PhotoHolder info) {
            if (info.getPhoto().time < this.duration) {
                return false;
            }
            this.removeList.add(info);
            return false;
        }

        public void removePhotos(Job job) {
            Iterator i$ = this.removeList.iterator();
            while (i$.hasNext()) {
                job.removePhoto(i$.next());
            }
        }

        public PhotoInfo getPhotoInfo() {
            return null;
        }
    }
}
