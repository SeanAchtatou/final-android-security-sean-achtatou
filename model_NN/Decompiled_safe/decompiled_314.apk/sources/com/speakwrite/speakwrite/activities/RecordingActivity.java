package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.activities.BaseJobActivity;
import com.speakwrite.speakwrite.audio.AMREditor;
import com.speakwrite.speakwrite.audio.AudioController;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.utils.PeriodicTask;
import com.speakwrite.speakwrite.utils.TimeUtils;
import java.io.IOException;

public class RecordingActivity extends AudioActivity implements View.OnClickListener {
    private static final String ALERTED_RECORD_CALL = "alerted_record_call";
    private static final String END_MILLIS_KEY = "endMillis";
    public static final String MODE_KEY = "record_mode";
    private static final String RECORDING_ACTIVITY_TAG = "RecordingActivity";
    private static final int REFRESH_DELAY = 250;
    private static final String START_MILLIS_KEY = "startMillis";
    private static final String TIME_SHIFT_KEY = "timeShift";
    private static boolean sRecordingCallEnable;
    private Button NavAudioFiles;
    private Button NavJobStatus;
    private Button NavRecord;
    private Button NavRecordCall;
    private Button NavSettings;
    /* access modifiers changed from: private */
    public Runnable mAction;
    private String mCurrentFilename;
    /* access modifiers changed from: private */
    public long mEndTime = 0;
    /* access modifiers changed from: private */
    public ProgressBar mLevelProgressBar;
    private int mMode = -1;
    /* access modifiers changed from: private */
    public boolean mOnPause;
    boolean mPausing = false;
    private Button mRecordButton;
    /* access modifiers changed from: private */
    public long mStartTime = 0;
    /* access modifiers changed from: private */
    public long mTimeShift = 0;
    /* access modifiers changed from: private */
    public TextView mTimeText;
    private PeriodicTask.Task mUpdater;
    private Runnable mUpdaterHandler;

    static {
        checkCallRecording();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.recording);
        this.mHandler = new Handler();
        initControls();
        restoreState(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mOnPause = false;
        this.mPausing = false;
        getJobList();
        if (getIntent() == null || getIntent().getExtras() == null) {
            recordingNew();
        } else {
            recordingResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        AudioController ac = getAudioController();
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mAction = new Runnable() {
                public void run() {
                    RecordingActivity.this.resetJob();
                    boolean unused = RecordingActivity.this.mOnPause = false;
                }
            };
            this.mOnPause = true;
            stopRecording(ac);
        } else {
            resetJob();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void openJobListActivity() {
        startActivityForResult(new Intent(this, JobListActivity.class), 3);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setIntent(null);
        this.mMode = -1;
        this.job = null;
        if (resultCode == 10) {
            openJobListActivity();
        } else if (requestCode == 1 || requestCode == 3) {
            if (resultCode == 2) {
                setIntent(data);
            } else if (resultCode == 8) {
                data.putExtra(PlayingActivity.ACTION_KEY, this.mMode);
                data.putExtra(PlayingActivity.START_TIME_KEY, this.mEndTime);
                data.setClass(this, PlayingActivity.class);
                startActivityForResult(data, 1);
            }
        } else if (requestCode == 13 && resultCode == -1) {
            startRecordingCall(data.getExtras().getString(DialerActivity.PHONE_NUMBER));
        }
    }

    public void onClick(View v) {
        if (!this.mPausing && v.getId() == R.id.record) {
            this.mTimeText.setVisibility(0);
            this.mLevelProgressBar.setVisibility(0);
            record();
        }
    }

    public void doFinish() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button
     arg types: [?, int, ?, com.speakwrite.speakwrite.activities.RecordingActivity$2]
     candidates:
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, java.lang.String, android.view.View$OnClickListener):android.widget.Button
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button */
    /* access modifiers changed from: protected */
    public void initControls() {
        super.initControls();
        this.mLevelProgressBar = (ProgressBar) findViewById(R.id.level);
        this.mLevelProgressBar.setProgress(0);
        this.mRecordButton = (Button) findViewById(R.id.record);
        this.mRecordButton.setOnClickListener(this);
        this.mTimeText = (TextView) findViewById(R.id.time);
        this.NavRecord = (Button) findViewById(R.id.dictate_navigation_button);
        this.NavJobStatus = (Button) findViewById(R.id.jobstatus_navigation_button);
        this.NavAudioFiles = (Button) findViewById(R.id.audiofiles_navigation_button);
        this.NavSettings = (Button) findViewById(R.id.settings_navigation_button);
        this.NavRecordCall = (Button) findViewById(R.id.top_navigation_right_button);
        setJobTitle();
        if (sRecordingCallEnable) {
            initTopNavigationButton((int) R.id.top_navigation_right_button, true, (int) R.string.record_call, (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View v) {
                    AudioController ac = RecordingActivity.this.getAudioController();
                    if (ac.getState() == AudioController.AudioState.Recording) {
                        Runnable unused = RecordingActivity.this.mAction = new Runnable() {
                            public void run() {
                                RecordingActivity.this.doRecordCall();
                            }
                        };
                        RecordingActivity.this.stopRecording(ac);
                        return;
                    }
                    RecordingActivity.this.doRecordCall();
                }
            });
        }
    }

    private void SetRecordingControls(boolean IsRecording) {
        if (!IsRecording) {
            this.NavAudioFiles.setVisibility(0);
            this.NavJobStatus.setVisibility(0);
            this.NavRecord.setVisibility(0);
            this.NavRecordCall.setVisibility(0);
            this.NavSettings.setVisibility(0);
            return;
        }
        this.NavAudioFiles.setVisibility(4);
        this.NavJobStatus.setVisibility(4);
        this.NavRecord.setVisibility(4);
        this.NavRecordCall.setVisibility(4);
        this.NavSettings.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getAudioController().getState() != AudioController.AudioState.Stopped) {
            outState.putLong(START_MILLIS_KEY, this.mStartTime);
            outState.putLong(TIME_SHIFT_KEY, this.mTimeShift);
            outState.putLong(END_MILLIS_KEY, this.mEndTime);
            outState.putInt(MODE_KEY, this.mMode);
            removeUpdater();
        }
    }

    /* access modifiers changed from: protected */
    public void postUpdateHandler() {
        if (this.mUpdater == null) {
            this.mUpdater = new PeriodicTask.Task() {
                public boolean doTask() {
                    long j;
                    AudioController ac = RecordingActivity.this.getAudioController();
                    boolean retval = ac.getState() == AudioController.AudioState.Recording;
                    Log.i("RECORDING ACTIVITY", "retval = " + retval);
                    Log.i("RECORDING ACTIVITY", "mPausing = " + RecordingActivity.this.mPausing);
                    RecordingActivity recordingActivity = RecordingActivity.this;
                    if (RecordingActivity.this.mStartTime > 0) {
                        j = System.currentTimeMillis() - RecordingActivity.this.mStartTime;
                    } else {
                        j = 0;
                    }
                    long unused = recordingActivity.mEndTime = j + RecordingActivity.this.mTimeShift;
                    if (!RecordingActivity.this.mPausing) {
                        RecordingActivity.this.mTimeText.setText(TimeUtils.millisToString(RecordingActivity.this.mEndTime));
                        RecordingActivity.this.mLevelProgressBar.setProgress(ac.getRecordingLevel());
                        RecordingActivity.this.mLevelProgressBar.invalidate();
                    }
                    return retval;
                }
            };
        }
        if (this.mUpdaterHandler == null) {
            this.mUpdaterHandler = new PeriodicTask(this.mHandler, this.mUpdater, 250);
        } else {
            this.mHandler.postDelayed(this.mUpdaterHandler, 250);
        }
    }

    /* access modifiers changed from: protected */
    public void updateButtonState(AudioController ac) {
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mRecordButton.setBackgroundResource(R.drawable.large_pause_set);
            SetRecordingControls(true);
            return;
        }
        SetRecordingControls(false);
        this.mRecordButton.setBackgroundResource(R.drawable.large_record_set);
    }

    /* access modifiers changed from: protected */
    public void onNewJob() {
        if (getAudioController().getState() == AudioController.AudioState.Recording) {
            recordingNew();
            return;
        }
        if (this.job != null) {
            this.job = null;
            setIntent(null);
        }
        setJobTitle();
        updateButtonState(getAudioController());
        this.mTimeText.setText((int) R.string.time_value);
        this.mLevelProgressBar.setProgress(0);
    }

    /* access modifiers changed from: protected */
    public void onJobList() {
        AudioController ac = getAudioController();
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mAction = new Runnable() {
                public void run() {
                    RecordingActivity.super.onJobList();
                }
            };
            stopRecording(ac);
            return;
        }
        super.onJobList();
    }

    public void onJobStatus() {
        AudioController ac = getAudioController();
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mAction = new Runnable() {
                public void run() {
                    RecordingActivity.super.onJobStatus();
                }
            };
            stopRecording(ac);
            return;
        }
        super.onJobStatus();
    }

    public void onSettings() {
        AudioController ac = getAudioController();
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mAction = new Runnable() {
                public void run() {
                    RecordingActivity.super.onSettings();
                }
            };
            stopRecording(ac);
            return;
        }
        super.onSettings();
    }

    private void restoreState(Bundle savedInstanceState) {
        this.mPausing = false;
        if (savedInstanceState != null) {
            this.mStartTime = savedInstanceState.getLong(START_MILLIS_KEY);
            this.mTimeShift = savedInstanceState.getLong(TIME_SHIFT_KEY);
            this.mEndTime = savedInstanceState.getLong(END_MILLIS_KEY);
            this.mMode = savedInstanceState.getInt(MODE_KEY);
            AudioController ac = getAudioController();
            this.mLevelProgressBar.setMax(ac.getMaxRecordingLevel());
            this.mTimeText.setText(TimeUtils.millisToString(this.mTimeShift));
            getJobFromBundle(savedInstanceState);
            postUpdateHandler();
            this.mHandler.post(this.mUpdaterHandler);
            updateButtonState(ac);
        } else {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                this.mMode = bundle.getInt(MODE_KEY);
            }
        }
        if (this.mMode == 3) {
            this.mMode = -1;
            openJobListActivity();
        }
    }

    private void record() {
        record(1);
    }

    private void record(int source) {
        long time;
        AudioController ac = getAudioController();
        if (ac.getState() != AudioController.AudioState.Recording) {
            this.mPausing = false;
            this.mTimeText.setText(TimeUtils.millisToString(this.mTimeShift));
            try {
                this.job = getJob();
                if (this.mMode == -1 || ac.isFileEmpty()) {
                    this.mCurrentFilename = this.job.getSoundName();
                    this.mMode = -1;
                } else {
                    this.mCurrentFilename = this.job.getTempFilename();
                }
                ac.stop();
                playSound(SpeakWriteApplication.BEEP_START_RECORDING_ID);
                ac.setFileName(this.mCurrentFilename);
                ac.record();
                this.mStartTime = System.currentTimeMillis();
                this.mLevelProgressBar.setMax(ac.getMaxRecordingLevel());
                postUpdateHandler();
            } catch (IOException e) {
                toast((int) R.string.record_error, e);
            }
            enableWakeLock();
            updateButtonState(ac);
        } else if (!this.mPausing) {
            this.mPausing = true;
            if (this.mStartTime > 0) {
                time = System.currentTimeMillis() - this.mStartTime;
            } else {
                time = 0;
            }
            if (time > 1500) {
                this.mAction = new Runnable() {
                    public void run() {
                        RecordingActivity.this.startEdit();
                    }
                };
                stopRecording(ac);
                return;
            }
            this.mPausing = false;
        }
    }

    /* access modifiers changed from: private */
    public void stopRecording(AudioController ac) {
        ProgressDialog pd;
        removeUpdater();
        ac.stop();
        if (this.mMode > -1) {
            if (!this.mOnPause) {
                pd = ProgressDialog.show(this, getString(R.string.combine_audio_title), getString(R.string.combine_audio_message), true, false);
            } else {
                pd = null;
            }
            Log.d(RECORDING_ACTIVITY_TAG, "Mode = " + this.mMode);
            new BaseJobActivity.ProgressDialogTask(pd, new Runnable() {
                public void run() {
                    RecordingActivity.this.combineAudio();
                }
            }).start();
            return;
        }
        saveJobAndStartEdit();
    }

    /* access modifiers changed from: private */
    public void combineAudio() {
        try {
            switch (this.mMode) {
                case 0:
                    AMREditor.append(this.job.getTempFilename(), this.job.getSoundName());
                    break;
                case 1:
                    AMREditor.insert(this.job.getTempFilename(), this.job.getSoundName(), this.mTimeShift);
                    break;
                case 2:
                    AMREditor.overwrite(this.job.getTempFilename(), this.job.getSoundName(), this.mTimeShift);
                    break;
            }
        } catch (Exception e) {
            toast((int) R.string.combine_audio_error, e);
        }
        saveJobAndStartEdit();
        updateJobPosition();
    }

    private void saveJobAndStartEdit() {
        stopAndSave(getAudioController());
        if (this.mAction != null) {
            this.mHandler.post(this.mAction);
            this.mAction = null;
        }
    }

    /* access modifiers changed from: private */
    public void startEdit() {
        Intent intent = new Intent(this, PlayingActivity.class);
        intent.putExtra(PlayingActivity.ACTION_KEY, this.mMode);
        intent.putExtra(PlayingActivity.START_TIME_KEY, this.mEndTime);
        putJob(intent);
        startActivityForResult(intent, 1);
    }

    private void stopAndSave(AudioController ac) {
        disableWakeLock();
        ac.stop();
        try {
            ac.setFileName(this.job.getSoundName());
            ac.openFile();
            this.job.duration = ac.getTotalTime();
            this.job.status = 0;
            this.job.save();
        } catch (IOException e) {
            toast((int) R.string.job_save_error, e);
        }
    }

    private Job getJob() {
        if (this.job == null) {
            this.job = getJobList().createJob();
            setJobTitle();
            this.mTimeShift = 0;
        }
        return this.job;
    }

    private void removeUpdater() {
        if (this.mUpdaterHandler != null) {
            this.mHandler.removeCallbacks(this.mUpdaterHandler);
        }
    }

    private void setJobTitle() {
        String title = getResources().getString(R.string.recording_title);
        if (this.job != null) {
            title = title + " - " + this.job.name;
        }
        setTitle(title);
    }

    private void recordingResume() {
        this.mPausing = false;
        getJobFromIntent();
        Bundle extras = getIntent().getExtras();
        this.mMode = extras.getInt(MODE_KEY);
        if (this.job != null) {
            this.mTimeShift = extras.getLong(getString(R.string.time_value), this.job.duration);
            this.mTimeText.setText(TimeUtils.millisToString(this.mTimeShift));
            if (this.job != null) {
                this.mTimeText.setVisibility(0);
                this.mLevelProgressBar.setVisibility(0);
                record();
                return;
            }
            return;
        }
        this.mTimeShift = 0;
    }

    private void recordingNew() {
        AudioController ac = getAudioController();
        if (ac.getState() == AudioController.AudioState.Recording) {
            this.mAction = new Runnable() {
                public void run() {
                    RecordingActivity.this.doRecordNew();
                }
            };
            stopRecording(ac);
            return;
        }
        doRecordNew();
    }

    /* access modifiers changed from: private */
    public void doRecordNew() {
        this.mTimeText.setVisibility(4);
        this.mTimeText.setText((int) R.string.time_value);
        this.mLevelProgressBar.setVisibility(4);
        this.mLevelProgressBar.setProgress(0);
        onNewJob();
    }

    /* access modifiers changed from: private */
    public void resetJob() {
        this.job = null;
        setIntent(null);
    }

    /* access modifiers changed from: private */
    public void doRecordCall() {
        this.job = null;
        this.mMode = -1;
        if (!isRecordCallWarningShown()) {
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setMessage((int) R.string.record_call_warning);
            b.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    RecordingActivity.this.doAskPhoneNumber();
                }
            });
            b.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    RecordingActivity.this.doAskPhoneNumber();
                }
            });
            b.show();
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
            edit.putBoolean(ALERTED_RECORD_CALL, true);
            edit.commit();
            return;
        }
        doAskPhoneNumber();
    }

    /* access modifiers changed from: private */
    public void doAskPhoneNumber() {
        startActivityForResult(new Intent(this, DialerActivity.class), 13);
    }

    private boolean isRecordCallWarningShown() {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(ALERTED_RECORD_CALL, false);
    }

    private static void checkCallRecording() {
        sRecordingCallEnable = true;
    }

    private void startRecordingCall(String number) {
        Intent intent = new Intent(this, AuthenticateActivity.class);
        intent.putExtra(getString(R.string.flow_key), 3);
        intent.putExtra(DialerActivity.PHONE_NUMBER, number);
        startActivity(intent);
    }
}
