package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.network.SendToListFetcher;

public class FileNameActivity extends BaseMenuJobActivity implements View.OnClickListener {
    public static final String SENDTO_KEY = "sendto";
    private EditText mFileName = null;
    private SendToListFetcher.SendToItem[] mSendToList;
    private TextView mSendToListPrompt = null;
    private Spinner mSendToSpinner = null;
    private Intent mSubmitIntent = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button
     arg types: [?, int, ?, com.speakwrite.speakwrite.activities.FileNameActivity]
     candidates:
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, java.lang.String, android.view.View$OnClickListener):android.widget.Button
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String pass;
        String loging;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_name);
        setResult(7);
        initControls();
        this.mFileName = (EditText) findViewById(R.id.file_name);
        FileNameInputFilter.addThisFilter(this.mFileName);
        Button initTopNavigationButton = initTopNavigationButton((int) R.id.top_navigation_left_button, true, (int) R.string.button_back, (View.OnClickListener) this);
        ((Button) findViewById(R.id.submit)).setOnClickListener(this);
        this.mSendToListPrompt = (TextView) findViewById(R.id.sendtolist_prompt);
        this.mSendToSpinner = (Spinner) findViewById(R.id.sendtolist);
        String passName = getString(R.string.password_key);
        String loginName = getString(R.string.login_key);
        this.mSubmitIntent = new Intent(this, SubmitActivity.class);
        if (savedInstanceState == null) {
            getJobFromBundle(getIntent().getExtras());
            pass = getIntent().getStringExtra(passName);
            loging = getIntent().getStringExtra(loginName);
        } else {
            getJobFromBundle(savedInstanceState);
            pass = savedInstanceState.getString(passName);
            loging = savedInstanceState.getString(loginName);
        }
        initSendToSpinner();
        if (this.job.nameIsCustom) {
            this.mFileName.setText(this.job.name);
        }
        this.mSubmitIntent.putExtra(passName, pass);
        this.mSubmitIntent.putExtra(loginName, loging);
        putJob(this.mSubmitIntent);
    }

    private void initSendToSpinner() {
        String sendToListString = getIntent().getStringExtra(SENDTO_KEY);
        if (sendToListString == null || sendToListString.length() <= 0 || !(this.job.photos == null || this.job.photos.size() == 0)) {
            this.mSendToListPrompt.setVisibility(4);
            this.mSendToSpinner.setVisibility(4);
            if (sendToListString != null && sendToListString.length() > 0 && this.job.photos != null && this.job.photos.size() > 0) {
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setMessage((int) R.string.picture_job_to_speakwrite);
                b.setIcon(17301543);
                b.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
                b.show();
                return;
            }
            return;
        }
        this.mSendToList = SendToListFetcher.parseString(sendToListString);
        this.mSendToSpinner.setAdapter((SpinnerAdapter) new SendToListFetcher.SendToItem.Adapter(this, this.mSendToList));
        this.mSendToSpinner.setVisibility(0);
        this.mSendToListPrompt.setVisibility(0);
        int defPos = 0;
        int i = 0;
        while (true) {
            if (i >= this.mSendToList.length) {
                break;
            } else if (this.mSendToList[i].isDefault) {
                defPos = i;
                break;
            } else {
                i++;
            }
        }
        this.mSendToSpinner.setSelection(defPos, false);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.submit) {
            String name = this.mFileName.getText().toString();
            if (name != null && name.length() > 0) {
                this.mSubmitIntent.putExtra(getString(R.string.file_name_key), name);
            }
            if (this.mSendToList != null && this.mSendToList.length > 0) {
                long sendToId = this.mSendToSpinner.getSelectedItemId();
                if (sendToId == Long.MIN_VALUE) {
                    sendToId = 0;
                }
                SendToListFetcher.SendToItem item = this.mSendToList[(int) sendToId];
                if (item.id != null && item.id.length() > 0) {
                    this.mSubmitIntent.putExtra(SENDTO_KEY, item.id);
                }
            }
            startActivity(this.mSubmitIntent);
            finish();
        } else if (id == R.id.top_navigation_left_button) {
            finish();
        }
    }
}
