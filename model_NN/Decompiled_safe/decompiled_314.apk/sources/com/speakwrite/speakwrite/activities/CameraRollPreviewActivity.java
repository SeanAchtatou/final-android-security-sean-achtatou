package com.speakwrite.speakwrite.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.speakwrite.speakwrite.AppConstants;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.jobs.PhotoInfo;
import com.speakwrite.speakwrite.utils.ImageUtils;
import java.io.File;
import java.io.IOException;
import org.apache.commons.lang.SystemUtils;

public class CameraRollPreviewActivity extends BaseJobActivity implements View.OnClickListener {
    private static final int DIALOG_PROGRESS_LOADING = 1;
    private static final int DIALOG_PROGRESS_SAVING = 2;
    private static final String IMAGE_KEY = "image_key";
    public static final String RETAKE_IMAGE = "retake_image";
    private static final int SELECT_IMAGE = 1;
    private static final String TIME_SHIFT_KEY = "timeShift";
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public final Runnable mImageSavingRunnable = new Runnable() {
        public void run() {
            CameraRollPreviewActivity.this.dismissDialog(2);
            CameraRollPreviewActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public final Runnable mImageViewUpdateRunnable = new Runnable() {
        public void run() {
            if (CameraRollPreviewActivity.this.mImageView != null) {
                CameraRollPreviewActivity.this.mImageView.setImageBitmap(CameraRollPreviewActivity.this.mBitmap);
                CameraRollPreviewActivity.this.dismissDialog(1);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsRetake = false;
    /* access modifiers changed from: private */
    public Uri mSelectedImage;
    /* access modifiers changed from: private */
    public long mTimeShift;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.camera_roll_preview);
        initControls();
        if (savedInstanceState == null) {
            getJobFromIntent();
            this.mTimeShift = getTimeShiftFromBundle(getIntent().getExtras());
            startCameraRoll();
        } else {
            getJobFromBundle(savedInstanceState);
            this.mTimeShift = getTimeShiftFromBundle(savedInstanceState);
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle == null || !bundle.containsKey("retake_image")) {
            this.mIsRetake = false;
        } else {
            this.mIsRetake = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mSelectedImage != null) {
            putImageFromUri();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(IMAGE_KEY, this.mSelectedImage);
        saveTimeShift(outState, this.mTimeShift);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(IMAGE_KEY)) {
            this.mSelectedImage = (Uri) savedInstanceState.getParcelable(IMAGE_KEY);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 1) {
            return;
        }
        if (resultCode == -1) {
            this.mSelectedImage = data.getData();
        } else if (resultCode == 0) {
            setResult(0);
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                ProgressDialog progress = new ProgressDialog(this);
                progress.setMessage(getResources().getString(R.string.loading_image));
                progress.setCancelable(false);
                return progress;
            case 2:
                ProgressDialog progress2 = new ProgressDialog(this);
                progress2.setMessage(getResources().getString(R.string.saving_image));
                progress2.setCancelable(false);
                return progress2;
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.cancel_button) {
            startCameraRoll();
        } else if (id == R.id.use_button) {
            showDialog(2);
            savePhoto();
        }
    }

    private void initControls() {
        ((Button) findViewById(R.id.use_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.cancel_button)).setOnClickListener(this);
        this.mImageView = (ImageView) findViewById(R.id.image_preview);
    }

    private void startCameraRoll() {
        startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), 1);
    }

    private void putImageFromUri() {
        showDialog(1);
        new Thread(new Runnable() {
            public void run() {
                Bitmap unused = CameraRollPreviewActivity.this.mBitmap = ImageUtils.getBitmapFromFile(ImageUtils.getFileFromUri(CameraRollPreviewActivity.this.mSelectedImage, CameraRollPreviewActivity.this.getContentResolver()), AppConstants.IMAGE_PREVIEW_WIDTH);
                CameraRollPreviewActivity.this.mHandler.post(CameraRollPreviewActivity.this.mImageViewUpdateRunnable);
            }
        }).start();
    }

    public static long getTimeShiftFromBundle(Bundle bundle) {
        if (bundle != null) {
            return bundle.getLong(TIME_SHIFT_KEY);
        }
        return 0;
    }

    public static void saveTimeShift(Intent intent, long timeShift) {
        intent.putExtra(TIME_SHIFT_KEY, timeShift);
    }

    public static void saveTimeShift(Bundle bundle, long timeShift) {
        bundle.putLong(TIME_SHIFT_KEY, timeShift);
    }

    private void savePhoto() {
        new Thread(new Runnable() {
            public void run() {
                PhotoInfo info;
                File file = ImageUtils.getFileFromUri(CameraRollPreviewActivity.this.mSelectedImage, CameraRollPreviewActivity.this.getContentResolver());
                if (CameraRollPreviewActivity.this.mIsRetake) {
                    PhotoHolder ph = CameraRollPreviewActivity.this.job.findPhotoInfo(CameraRollPreviewActivity.this.mTimeShift);
                    if (!(ph == null || ph.size() <= 0 || (info = ph.get(ph.getCurrentIndex())) == null)) {
                        CameraRollPreviewActivity.this.savePhotoInfo(info, file);
                    }
                } else {
                    CameraRollPreviewActivity.this.savePhotoInfo(CameraRollPreviewActivity.this.job.getPhotoInfo(CameraRollPreviewActivity.this.mTimeShift), file);
                }
                try {
                    CameraRollPreviewActivity.this.job.save();
                } catch (IOException e) {
                    CameraRollPreviewActivity.this.toast((int) R.string.job_save_error, e);
                }
                CameraRollPreviewActivity.this.mHandler.post(CameraRollPreviewActivity.this.mImageSavingRunnable);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void savePhotoInfo(PhotoInfo info, File file) {
        if (info != null && file != null) {
            info.rotation = SystemUtils.JAVA_VERSION_FLOAT;
            info.landscape = this.mBitmap.getHeight() < this.mBitmap.getWidth();
            try {
                info.saveImage(file);
            } catch (IOException ex) {
                toast((int) R.string.saving_image_error, ex);
            }
        }
    }
}
