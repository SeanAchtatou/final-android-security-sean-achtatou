package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.network.Constants;

public class LoginActivity extends BaseMenuJobActivity implements View.OnClickListener {
    private static final int LOGIN_FAILURE_DIALOG = 256;
    private Button mCreateAccountButton;
    private EditText mLoginEdit;
    private EditText mPasswordEdit;
    private ToggleButton mRememberButton;
    private Button mSignInButton;
    private String m_login;
    private String m_password;
    private boolean m_remember = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.login);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.m_password = pref.getString(getString(R.string.password_key), "");
        this.m_login = pref.getString(getString(R.string.login_key), "");
        this.m_remember = pref.getBoolean(getString(R.string.remember_me_key), true);
        initControls();
        if (savedInstanceState == null && this.m_login != null && this.m_login.length() > 0) {
            showDialog(LOGIN_FAILURE_DIALOG);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button
     arg types: [?, int, ?, com.speakwrite.speakwrite.activities.LoginActivity]
     candidates:
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, java.lang.String, android.view.View$OnClickListener):android.widget.Button
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button */
    /* access modifiers changed from: protected */
    public void initControls() {
        super.initControls();
        this.mSignInButton = (Button) findViewById(R.id.sign_in);
        this.mSignInButton.setOnClickListener(this);
        this.mPasswordEdit = (EditText) findViewById(R.id.password);
        this.mPasswordEdit.setText(this.m_password);
        this.mLoginEdit = (EditText) findViewById(R.id.login);
        this.mLoginEdit.setText(this.m_login);
        this.mRememberButton = (ToggleButton) findViewById(R.id.remember_button);
        this.mRememberButton.setChecked(this.m_remember);
        initTopNavigationButton((int) R.id.top_navigation_left_button, true, (int) R.string.button_back, (View.OnClickListener) this);
        this.mCreateAccountButton = (Button) findViewById(R.id.create_account_button);
        this.mCreateAccountButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.sign_in) {
            String password = this.mPasswordEdit.getText().toString();
            String login = this.mLoginEdit.getText().toString();
            String pass_key = getString(R.string.password_key);
            String log_key = getString(R.string.login_key);
            Intent intent = new Intent();
            intent.putExtra(pass_key, password);
            intent.putExtra(log_key, login);
            SharedPreferences.Editor e = PreferenceManager.getDefaultSharedPreferences(this).edit();
            if (this.mRememberButton.isChecked()) {
                e.putString(pass_key, password);
                e.putString(log_key, login);
                e.commit();
            }
            e.putBoolean(getString(R.string.remember_me_key), this.mRememberButton.isChecked());
            e.commit();
            setResult(-1, intent);
            finish();
        } else if (id == R.id.top_navigation_left_button) {
            setResult(0, null);
            finish();
        } else if (v == this.mCreateAccountButton) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Constants.SETUP_URL)));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case LOGIN_FAILURE_DIALOG /*256*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                TextView text = new TextView(this, null, 16842817);
                text.setGravity(17);
                text.setText((int) R.string.authenticating_failure);
                builder.setView(text);
                builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }
}
