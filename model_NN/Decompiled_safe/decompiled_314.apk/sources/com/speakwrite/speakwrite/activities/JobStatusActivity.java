package com.speakwrite.speakwrite.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.lang.time.DateUtils;
import org.apache.http.client.methods.HttpGet;

public class JobStatusActivity extends BaseMenuJobActivity {
    public static final String STATUS_URL_KEY = "status_url";
    private String mUrl;
    private WebView mWebView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Bundle b;
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.job_status);
        initControls();
        this.mWebView = (WebView) findViewById(R.id.status_view);
        this.mWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                try {
                    Log.d("Download", url + " " + mimetype + "+" + contentDisposition);
                    HttpURLConnection c = (HttpURLConnection) new URL(url).openConnection();
                    c.setRequestMethod(HttpGet.METHOD_NAME);
                    c.setDoOutput(true);
                    c.connect();
                    FileOutputStream f = new FileOutputStream(new File(SpeakWriteApplication.getDataFolder(), "tmpspeakwrite"));
                    InputStream in = c.getInputStream();
                    byte[] buffer = new byte[1024];
                    int total = 0;
                    while (true) {
                        int len1 = in.read(buffer);
                        if (len1 > 0) {
                            total += len1;
                            JobStatusActivity.this.setProgress(total * DateUtils.MILLIS_IN_SECOND);
                            f.write(buffer, 0, len1);
                        } else {
                            f.close();
                            String filepath = SpeakWriteApplication.getDataFolder() + "/" + "tmpspeakwrite";
                            File file = new File(filepath);
                            Log.d("Get Intent ", filepath);
                            Intent intent = new Intent();
                            intent.setDataAndType(Uri.fromFile(file), mimetype);
                            Log.d("setAction", intent.getType() + mimetype);
                            intent.setAction("android.intent.action.VIEW");
                            JobStatusActivity.this.startActivity(intent);
                            return;
                        }
                    }
                } catch (Exception e) {
                    JobStatusActivity.this.toast(R.string.network_error, e);
                }
            }
        });
        this.mWebView.getSettings().setPluginsEnabled(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                JobStatusActivity.this.setProgress(progress * 100);
            }
        });
        this.mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(JobStatusActivity.this, "Page loading error! " + description, 1).show();
            }
        });
        if (savedInstanceState == null) {
            b = getIntent().getExtras();
        } else {
            b = savedInstanceState;
        }
        this.mUrl = getUrl(b);
        this.mWebView.loadUrl(this.mUrl);
    }

    public void onJobStatus() {
    }

    private String getUrl(Bundle b) {
        return b.getString(STATUS_URL_KEY);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATUS_URL_KEY, this.mUrl);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mWebView.destroy();
    }
}
