package com.speakwrite.speakwrite.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.JobList;
import com.speakwrite.speakwrite.views.AudioFilesListView;

public class JobListActivity extends BaseMenuJobActivity implements ExpandableListView.OnChildClickListener, View.OnClickListener {
    private JobList mJobList;
    private AudioFilesListView mJobListView;
    private View mNoItems;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.job_list);
        setResult(0);
        initControls();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button
     arg types: [?, int, ?, com.speakwrite.speakwrite.activities.JobListActivity]
     candidates:
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, java.lang.String, android.view.View$OnClickListener):android.widget.Button
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button */
    /* access modifiers changed from: protected */
    public void initControls() {
        super.initControls();
        if (isFlowIsBack()) {
            initTopNavigationButton((int) R.id.top_navigation_left_button, true, (int) R.string.button_back, (View.OnClickListener) this);
        }
        this.mNoItems = findViewById(R.id.no_items);
        this.mJobListView = (AudioFilesListView) findViewById(R.id.job_list_today);
        this.mJobListView.setOnChildClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        updateList();
        updateVisibilityLayout();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean needFinish = true;
        if (resultCode == 10) {
            needFinish = false;
        } else if (resultCode == 5) {
            needFinish = false;
        } else if (requestCode == 3 && resultCode == 2) {
            setResult(resultCode, data);
        } else if (requestCode == 3 && resultCode == 8) {
            startActivityForResult(data, 3);
            needFinish = false;
        } else if (resultCode == 4) {
            needFinish = false;
        }
        if (needFinish) {
            finish();
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.top_navigation_left_button) {
            finish();
        }
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (parent.getId() != R.id.job_list_today) {
            return false;
        }
        int jobIndex = this.mJobList.indexOf(this.mJobListView.getJob(groupPosition, childPosition));
        Intent intent = new Intent(this, JobSummaryActivity.class);
        intent.putExtra("job_key", jobIndex);
        if (isFlowIsBack()) {
            setResult(8, intent);
            finish();
            return false;
        }
        startActivityForResult(intent, 3);
        return false;
    }

    /* access modifiers changed from: protected */
    public void openJobListActivity() {
    }

    private void updateVisibilityLayout() {
        if (this.mJobList == null || this.mJobList.size() <= 0) {
            this.mNoItems.setVisibility(0);
            this.mJobListView.setVisibility(8);
            return;
        }
        this.mNoItems.setVisibility(8);
        this.mJobListView.setVisibility(0);
    }

    private boolean isFlowIsBack() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            return false;
        }
        return getIntent().getExtras().containsKey(getString(R.string.flow_key));
    }

    private void updateList() {
        this.mJobList = getJobList();
        this.mJobListView.setJobList(this.mJobList);
    }
}
