package com.speakwrite.speakwrite.activities;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.speakwrite.speakwrite.R;

public class BaseMenuJobActivity extends BaseJobActivity {
    protected Button mJobListButton;
    protected Button mJobStatusButton;
    protected Button mNewJobButton;
    protected Button mSettingsButton;

    /* access modifiers changed from: protected */
    public void initControls() {
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                if (v == BaseMenuJobActivity.this.mNewJobButton) {
                    BaseMenuJobActivity.this.onNewJob();
                } else if (v == BaseMenuJobActivity.this.mJobListButton) {
                    BaseMenuJobActivity.this.onJobList();
                } else if (v == BaseMenuJobActivity.this.mJobStatusButton) {
                    BaseMenuJobActivity.this.onJobStatus();
                } else if (v == BaseMenuJobActivity.this.mSettingsButton) {
                    BaseMenuJobActivity.this.onSettings();
                }
            }
        };
        this.mNewJobButton = (Button) findViewById(R.id.dictate_navigation_button);
        this.mNewJobButton.setOnClickListener(listener);
        this.mJobListButton = (Button) findViewById(R.id.audiofiles_navigation_button);
        this.mJobListButton.setOnClickListener(listener);
        this.mJobStatusButton = (Button) findViewById(R.id.jobstatus_navigation_button);
        this.mJobStatusButton.setOnClickListener(listener);
        this.mSettingsButton = (Button) findViewById(R.id.settings_navigation_button);
        this.mSettingsButton.setOnClickListener(listener);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.record:
                onNewJob();
                return false;
            case R.id.upload_size:
            case R.id.button_cancel:
            case R.id.top_navigation_left_button:
            case R.id.top_navigation_right_button:
            default:
                return false;
            case R.id.list:
                getJobList();
                openJobListActivity();
                return true;
            case R.id.options:
                onSettings();
                return true;
            case R.id.about:
                onAbout();
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    /* access modifiers changed from: protected */
    public void onNewJob() {
        Intent intent = new Intent(this, RecordingActivity.class);
        intent.addFlags(67108864);
        intent.putExtra(RecordingActivity.MODE_KEY, -1);
        startActivity(intent);
        doFinish();
    }

    /* access modifiers changed from: protected */
    public void onJobList() {
        getJobList();
        openJobListActivity();
    }

    public void onJobStatus() {
        Intent intent = new Intent(this, AuthenticateActivity.class);
        intent.putExtra(getString(R.string.flow_key), 2);
        startActivity(intent);
        doFinish();
    }

    public void onSettings() {
        startActivity(new Intent(this, OptionsActivity.class));
    }

    /* access modifiers changed from: protected */
    public Button initTopNavigationButton(int id, boolean visible, String text, View.OnClickListener listener) {
        Button button = (Button) findViewById(R.id.top_navigation_left_button);
        button.setVisibility(visible ? 0 : 4);
        button.setText(text);
        button.setOnClickListener(listener);
        return button;
    }

    /* access modifiers changed from: protected */
    public Button initTopNavigationButton(int id, boolean visible, int text, View.OnClickListener listener) {
        Button button = (Button) findViewById(id);
        button.setVisibility(visible ? 0 : 4);
        button.setText(text);
        button.setOnClickListener(listener);
        return button;
    }
}
