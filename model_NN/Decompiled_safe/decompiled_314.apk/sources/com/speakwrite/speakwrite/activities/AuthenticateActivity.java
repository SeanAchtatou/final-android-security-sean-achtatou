package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.network.AuthenticateAccount;
import com.speakwrite.speakwrite.network.NetworkException;
import com.speakwrite.speakwrite.network.PhoneNumberFetcher;
import com.speakwrite.speakwrite.network.SendToListFetcher;
import com.speakwrite.speakwrite.network.StatusLinkFetcher;
import java.lang.Thread;

public class AuthenticateActivity extends BaseMenuJobActivity {
    private static final int CONNECTION_FAILURE_DIALOG = 256;
    public static final int FLOW_JOB_STATUS = 2;
    public static final int FLOW_RECORD_CALL = 3;
    public static final int FLOW_SUBMIT = 1;
    private static final int REQUEST_CODE = 1;
    /* access modifiers changed from: private */
    public String login = "";
    private Thread mAuthenticatingThread;
    private int mFlow = 1;
    /* access modifiers changed from: private */
    public String password = "";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.authenticating);
        initControls();
        this.mHandler = new Handler();
        if (savedInstanceState == null) {
            Intent intent = getIntent();
            getFlowFromBundle(intent.getExtras());
            getJobFromBundle(intent.getExtras());
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            this.login = sp.getString(getString(R.string.login_key), "");
            this.password = sp.getString(getString(R.string.password_key), "");
        } else {
            getFlowFromBundle(savedInstanceState);
            getJobFromBundle(savedInstanceState);
            getLoginFromBundle(savedInstanceState);
        }
        initAuthenticatingThread();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case -1:
                this.password = data.getStringExtra(getString(R.string.password_key));
                this.login = data.getStringExtra(getString(R.string.login_key));
                initAuthenticatingThread();
                return;
            case 0:
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(getString(R.string.flow_key), this.mFlow);
        outState.putString(getString(R.string.login_key), this.login);
        outState.putString(getString(R.string.password_key), this.password);
    }

    private void initAuthenticatingThread() {
        SpeakWriteApplication app = (SpeakWriteApplication) getApplication();
        this.mAuthenticatingThread = app.authenticatingThread;
        if (this.mAuthenticatingThread == null || this.mAuthenticatingThread.getState() == Thread.State.TERMINATED) {
            this.mAuthenticatingThread = new Thread(new Runnable() {
                public void run() {
                    AuthenticateAccount aa = new AuthenticateAccount(AuthenticateActivity.this.login, AuthenticateActivity.this.password);
                    if (aa.authenticate()) {
                        try {
                            AuthenticateActivity.this.onAuthenticationSuccess();
                        } catch (NetworkException e) {
                            AuthenticateActivity.this.toast((int) R.string.network_error, e);
                        }
                    } else if (aa.hasConnectionFailed()) {
                        AuthenticateActivity.this.onConnectionFailure();
                    } else {
                        AuthenticateActivity.this.onAuthenticationFailure();
                    }
                }
            });
            app.authenticatingThread = this.mAuthenticatingThread;
            this.mAuthenticatingThread.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mHandler = null;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case CONNECTION_FAILURE_DIALOG /*256*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.connection_failure_title);
                TextView text = new TextView(this, null, 16842817);
                text.setGravity(17);
                text.setText((int) R.string.connection_failure_message);
                builder.setView(text);
                builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AuthenticateActivity.this.setResult(0);
                        AuthenticateActivity.this.finish();
                    }
                });
                builder.setCancelable(false);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: private */
    public void onConnectionFailure() {
        if (this.mHandler != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    AuthenticateActivity.this.showDialog(AuthenticateActivity.CONNECTION_FAILURE_DIALOG);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void onAuthenticationFailure() {
        startActivityForResult(new Intent(this, LoginActivity.class), 1);
    }

    /* access modifiers changed from: private */
    public void onAuthenticationSuccess() throws NetworkException {
        Intent intent = null;
        switch (this.mFlow) {
            case 1:
                String fetchResponse = new SendToListFetcher(this.login, this.password).fetchSendToString();
                intent = new Intent(this, FileNameActivity.class);
                if (fetchResponse != null && fetchResponse.length() > 0) {
                    intent.putExtra(FileNameActivity.SENDTO_KEY, fetchResponse);
                    break;
                }
            case 2:
                String link = new StatusLinkFetcher(this.login, this.password).fetchStatusUrl();
                intent = new Intent(this, JobStatusActivity.class);
                intent.putExtra(JobStatusActivity.STATUS_URL_KEY, link);
                break;
            case 3:
                String number = new PhoneNumberFetcher(this.login, this.password).fetchPhoneNumber(getIntent().getExtras().getString(DialerActivity.PHONE_NUMBER));
                Intent intent2 = new Intent("android.intent.action.CALL");
                intent2.setData(Uri.parse("tel:" + addExtraPauses(number)));
                startActivity(intent2);
                finish();
                return;
        }
        if (intent != null) {
            putJob(intent);
            intent.putExtra(getString(R.string.password_key), this.password);
            intent.putExtra(getString(R.string.login_key), this.login);
            if (this.mFlow == 1) {
                setResult(7);
                startActivityForResult(intent, 6);
            } else {
                startActivity(intent);
            }
        }
        finish();
    }

    private String addExtraPauses(String number) {
        int index = number.lastIndexOf(44);
        StringBuffer sb = new StringBuffer(number);
        sb.insert(index, ",,");
        return sb.toString();
    }

    private void getFlowFromBundle(Bundle bundle) {
        this.mFlow = bundle.getInt(getString(R.string.flow_key), 1);
    }

    private void getLoginFromBundle(Bundle savedInstanceState) {
        this.login = savedInstanceState.getString(getString(R.string.login_key));
        this.password = savedInstanceState.getString(getString(R.string.password_key));
    }
}
