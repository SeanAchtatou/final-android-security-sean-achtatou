package com.speakwrite.speakwrite.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Contacts;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;

public class DialerActivity extends Activity implements View.OnClickListener, View.OnLongClickListener {
    public static final String PHONE_NUMBER = "phone_number";
    private static final int SELECT_CONTACT = 256;
    private ImageButton mBackButton;
    private ImageButton mContactsButton;
    private ImageButton mDialButton;
    private Handler mHandler;
    private EditText mPhoneNumber;
    /* access modifiers changed from: private */
    public int mToneCounter = 0;
    /* access modifiers changed from: private */
    public ToneGenerator mToneGenerator;
    private Runnable mToneStopper = new Runnable() {
        public void run() {
            DialerActivity.access$006(DialerActivity.this);
            if (DialerActivity.this.mToneCounter <= 0) {
                DialerActivity.this.mToneGenerator.stopTone();
                int unused = DialerActivity.this.mToneCounter = 0;
            }
        }
    };

    static /* synthetic */ int access$006(DialerActivity x0) {
        int i = x0.mToneCounter - 1;
        x0.mToneCounter = i;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialer);
        initControls();
        this.mToneGenerator = new ToneGenerator(3, 3);
        this.mHandler = new Handler();
    }

    private void initControls() {
        initButton(R.id.dialer_button_0, R.string.dialer_button_0, R.string.dialer_button_0_text);
        initButton(R.id.dialer_button_1, R.string.dialer_button_1, R.string.dialer_button_1_text);
        initButton(R.id.dialer_button_2, R.string.dialer_button_2, R.string.dialer_button_2_text);
        initButton(R.id.dialer_button_3, R.string.dialer_button_3, R.string.dialer_button_3_text);
        initButton(R.id.dialer_button_4, R.string.dialer_button_4, R.string.dialer_button_4_text);
        initButton(R.id.dialer_button_5, R.string.dialer_button_5, R.string.dialer_button_5_text);
        initButton(R.id.dialer_button_6, R.string.dialer_button_6, R.string.dialer_button_6_text);
        initButton(R.id.dialer_button_7, R.string.dialer_button_7, R.string.dialer_button_7_text);
        initButton(R.id.dialer_button_8, R.string.dialer_button_8, R.string.dialer_button_8_text);
        initButton(R.id.dialer_button_9, R.string.dialer_button_9, R.string.dialer_button_9_text);
        initButton(R.id.dialer_button_asteriks, R.string.dialer_button_asteriks, R.string.dialer_button_asteriks_text);
        initButton(R.id.dialer_button_sharp, R.string.dialer_button_sharp, R.string.dialer_button_sharp_text);
        this.mPhoneNumber = (EditText) findViewById(R.id.dialer_number);
        this.mPhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        this.mBackButton = (ImageButton) findViewById(R.id.dialer_button_back);
        this.mBackButton.setOnClickListener(this);
        this.mBackButton.setOnLongClickListener(this);
        this.mBackButton.setTag(Integer.valueOf((int) R.id.dialer_button_back));
        this.mContactsButton = (ImageButton) findViewById(R.id.contacts);
        this.mContactsButton.setOnClickListener(this);
        this.mContactsButton.setTag(Integer.valueOf((int) R.id.contacts));
        this.mDialButton = (ImageButton) findViewById(R.id.dialer_button_call);
        this.mDialButton.setOnClickListener(this);
        this.mDialButton.setTag(Integer.valueOf((int) R.id.dialer_button_call));
    }

    private void initButton(int id, int tag, int text) {
        View button = findViewById(id);
        button.setOnClickListener(this);
        button.setOnLongClickListener(this);
        button.setTag(Integer.valueOf(tag));
        ((TextView) button.findViewById(R.id.dialer_button_number)).setText(tag);
        ((TextView) button.findViewById(R.id.dialer_button_text)).setText(text);
    }

    public void onClick(View v) {
        switch (((Integer) v.getTag()).intValue()) {
            case R.string.dialer_button_0:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 7));
                playTone(0);
                return;
            case R.string.dialer_button_1:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 8));
                playTone(1);
                return;
            case R.string.dialer_button_2:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 9));
                playTone(2);
                return;
            case R.string.dialer_button_3:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 10));
                playTone(3);
                return;
            case R.string.dialer_button_4:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 11));
                playTone(4);
                return;
            case R.string.dialer_button_5:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 12));
                playTone(5);
                return;
            case R.string.dialer_button_6:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 13));
                playTone(6);
                return;
            case R.string.dialer_button_7:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 14));
                playTone(7);
                return;
            case R.string.dialer_button_8:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 15));
                playTone(8);
                return;
            case R.string.dialer_button_9:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 16));
                playTone(9);
                return;
            case R.string.dialer_button_asteriks:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 17));
                playTone(10);
                return;
            case R.string.dialer_button_sharp:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 18));
                playTone(11);
                return;
            case R.id.contacts:
                startActivityForResult(new Intent("android.intent.action.PICK", Contacts.Phones.CONTENT_URI), SELECT_CONTACT);
                return;
            case R.id.dialer_button_call:
                String number = PhoneNumberUtils.stripSeparators(this.mPhoneNumber.getText().toString());
                if (number.length() > 0) {
                    Intent data = new Intent();
                    data.putExtra(PHONE_NUMBER, number);
                    setResult(-1, data);
                    finish();
                    return;
                }
                return;
            case R.id.dialer_button_back:
                this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 67));
                return;
            default:
                return;
        }
    }

    private void playTone(int tone) {
        this.mToneGenerator.startTone(tone);
        stopTone();
    }

    public boolean onLongClick(View v) {
        int tag = ((Integer) v.getTag()).intValue();
        if (tag == R.string.dialer_button_0) {
            this.mPhoneNumber.dispatchKeyEvent(new KeyEvent(0, 81));
            return true;
        } else if (tag != R.id.dialer_button_back) {
            return false;
        } else {
            this.mPhoneNumber.setText("");
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri mSelectedContact;
        if (requestCode == SELECT_CONTACT && resultCode == -1 && (mSelectedContact = data.getData()) != null) {
            this.mPhoneNumber.setText(getNumberFromUri(mSelectedContact));
        }
    }

    private String getNumberFromUri(Uri uri) {
        Cursor cursor;
        if (uri == null || (cursor = getContentResolver().query(uri, null, null, null, null)) == null || cursor.getCount() <= 0) {
            return null;
        }
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndexOrThrow("number"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mToneGenerator.release();
        super.onDestroy();
    }

    private void stopTone() {
        this.mToneCounter++;
        this.mHandler.postDelayed(this.mToneStopper, 100);
    }
}
