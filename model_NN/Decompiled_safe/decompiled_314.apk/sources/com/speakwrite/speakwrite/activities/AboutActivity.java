package com.speakwrite.speakwrite.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.network.Constants;

public class AboutActivity extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about);
        findViewById(R.id.create_account_button).setOnClickListener(this);
    }

    public void onClick(View v) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Constants.SETUP_URL)));
    }
}
