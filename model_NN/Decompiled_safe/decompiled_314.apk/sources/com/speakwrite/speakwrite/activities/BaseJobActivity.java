package com.speakwrite.speakwrite.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.jobs.JobList;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.jobs.PhotoInfo;
import com.speakwrite.speakwrite.receivers.SDCardMountReceiver;
import com.speakwrite.speakwrite.utils.SDCardUtils;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public abstract class BaseJobActivity extends Activity implements SDCardMountReceiver.SDCardMountReceiverCallback {
    private static final int DIALOG_NO_SDCARD = 130;
    private static final int DIALOG_RESUBMIT_JOB = 129;
    protected static final String JOB_KEY = "job_key";
    private static final String s_wakeLockTag = "SpeakEasy lock";
    protected Job job;
    /* access modifiers changed from: private */
    public final Handler mConversionHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                BaseJobActivity.this.onConvertJobFinish();
                return;
            }
            new File(BaseJobActivity.this.job.getSoundName()).delete();
            BaseJobActivity.this.onConvertJobFail();
        }
    };
    protected Handler mHandler;
    protected Runnable mUpdaterHandler;
    private PowerManager.WakeLock m_wakeLock;

    protected class ProgressDialogTask extends Thread {
        private ProgressDialog pd;
        private Runnable runnable;

        public ProgressDialogTask(ProgressDialog pd2, Runnable r) {
            this.pd = pd2;
            this.runnable = r;
        }

        public void run() {
            this.runnable.run();
            if (this.pd != null) {
                this.pd.dismiss();
            }
            BaseJobActivity.this.postUpdateHandler();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(0);
        this.m_wakeLock = ((PowerManager) getSystemService("power")).newWakeLock(805306394, s_wakeLockTag);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        SDCardMountReceiver.setCallback(null);
        disableWakeLock();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void enableWakeLock() {
        if (this.m_wakeLock != null && !this.m_wakeLock.isHeld()) {
            this.m_wakeLock.acquire();
        }
    }

    /* access modifiers changed from: protected */
    public void disableWakeLock() {
        if (this.m_wakeLock != null && this.m_wakeLock.isHeld()) {
            this.m_wakeLock.release();
        }
    }

    /* access modifiers changed from: protected */
    public void openJobListActivity() {
        Intent intent = new Intent(this, RecordingActivity.class);
        intent.addFlags(67108864);
        intent.putExtra(RecordingActivity.MODE_KEY, 3);
        startActivity(intent);
        doFinish();
    }

    /* access modifiers changed from: protected */
    public void postUpdateHandler() {
    }

    /* access modifiers changed from: protected */
    public JobList getJobList() {
        final SpeakWriteApplication app = (SpeakWriteApplication) getApplication();
        JobList list = app.jobList;
        if (list == null) {
            new ProgressDialogTask(ProgressDialog.show(this, null, getResources().getString(R.string.loading_joblist), true, false), new Runnable() {
                public void run() {
                    app.createJobList();
                }
            }).start();
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public void getJobFromIntent() {
        getJobFromBundle(getIntent().getExtras());
    }

    /* access modifiers changed from: protected */
    public void getJobFromBundle(Bundle bundle) {
        int jobKey;
        JobList jobList;
        if (bundle != null && (jobKey = bundle.getInt(JOB_KEY, -1)) > -1 && (jobList = getJobList()) != null) {
            this.job = (Job) jobList.get(jobKey);
        }
    }

    /* access modifiers changed from: protected */
    public void putJob(Intent intent) {
        intent.putExtra(JOB_KEY, getJobList().indexOf(this.job));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        JobList jl = getJobList();
        if (jl != null) {
            outState.putInt(JOB_KEY, jl.indexOf(this.job));
        }
    }

    /* access modifiers changed from: protected */
    public void askForDiscardingJob() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.delete_job_dialog_title);
        builder.setMessage((int) R.string.delete_job_dialog_message);
        builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BaseJobActivity.this.discardCurrentJob();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void discardCurrentJob() {
        File file = new File(this.job.getSoundName());
        if (file.exists()) {
            file.delete();
        }
        File file2 = new File(this.job.getUnconvertedSoundName());
        if (file2.exists()) {
            file2.delete();
        }
        File file3 = new File(this.job.getFilename());
        if (file3.exists()) {
            file3.delete();
        }
        if (this.job.photos != null) {
            while (this.job.photos.size() > 0) {
                this.job.removePhoto(this.job.photos.getFirst());
            }
        }
        getJobList().remove(this.job);
        this.job = null;
        openJobListActivity();
    }

    /* access modifiers changed from: protected */
    public void submitCurrentJob() {
        if (this.job.status != 1) {
            doSubmitJob();
        } else {
            showDialog(DIALOG_RESUBMIT_JOB);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!SDCardUtils.isSdPresent()) {
            SDCardMountReceiver.setCallback(this);
            showDialog(DIALOG_NO_SDCARD);
        }
        super.onResume();
    }

    public void SDCardCallback(int state) {
        ((SpeakWriteApplication) getApplication()).updateDataFolder();
        switch (state) {
            case 1:
                if (SDCardUtils.isSdPresent()) {
                    SDCardMountReceiver.setCallback(null);
                    dismissDialog(DIALOG_NO_SDCARD);
                    getJobList();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_RESUBMIT_JOB /*129*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                TextView text = new TextView(this, null, 16842817);
                text.setGravity(17);
                text.setText((int) R.string.job_submitted_message);
                builder.setView(text);
                builder.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BaseJobActivity.this.doSubmitJob();
                    }
                });
                builder.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
                return builder.create();
            case DIALOG_NO_SDCARD /*130*/:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle((int) R.string.dialog_no_sdcard_title);
                builder2.setMessage((int) R.string.dialog_no_sdcard);
                builder2.setPositiveButton(getString(R.string.dialog_no_sdcard_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
                builder2.setCancelable(false);
                return builder2.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void doSubmitJob() {
        Intent intent = new Intent(this, AuthenticateActivity.class);
        intent.putExtra(getString(R.string.flow_key), 1);
        putJob(intent);
        startActivityForResult(intent, 6);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void saveJob() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.save_dialog_title);
        View v = LayoutInflater.from(this).inflate((int) R.layout.edit_text_dialog_view, (ViewGroup) null, false);
        ((TextView) v.findViewById(R.id.message)).setText((int) R.string.save_dialog_message);
        EditText et = (EditText) v.findViewById(R.id.text);
        FileNameInputFilter.addThisFilter(et);
        et.setText(this.job.name);
        builder.setView(v);
        builder.setPositiveButton((int) R.string.ok, new JobSaver(v));
        builder.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    class JobSaver implements DialogInterface.OnClickListener {
        private View editView;

        public JobSaver(View v) {
            this.editView = v;
        }

        public void onClick(DialogInterface dialog, int which) {
            String newName = ((EditText) this.editView.findViewById(R.id.text)).getText().toString().trim();
            if (newName.length() <= 0 || !isNewName(newName)) {
                BaseJobActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        AlertDialog.Builder b = new AlertDialog.Builder(BaseJobActivity.this);
                        b.setMessage((int) R.string.job_name_exists);
                        b.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                BaseJobActivity.this.saveJob();
                            }
                        });
                        b.setNegativeButton((int) R.string.cancel, (DialogInterface.OnClickListener) null);
                        b.show();
                    }
                });
                return;
            }
            BaseJobActivity.this.renameJob(newName);
            Intent intent = new Intent(BaseJobActivity.this, JobListActivity.class);
            intent.putExtra(BaseJobActivity.this.getString(R.string.flow_key), 0);
            BaseJobActivity.this.startActivityForResult(intent, 9);
        }

        private boolean isNewName(String newName) {
            Iterator i$ = BaseJobActivity.this.getJobList().iterator();
            while (i$.hasNext()) {
                Job j = (Job) i$.next();
                if (j != BaseJobActivity.this.job && newName.equals(j.name)) {
                    return false;
                }
            }
            return true;
        }
    }

    public void renameJob(String newName) {
        String jobFilename = this.job.getFilename();
        String soundName = this.job.getSoundName();
        String unconvertedSoundName = this.job.getUnconvertedSoundName();
        this.job.name = newName;
        String newSoundName = this.job.getSoundName();
        String newUnconvertedSoundName = this.job.getUnconvertedSoundName();
        new File(jobFilename).delete();
        new File(soundName).renameTo(new File(newSoundName));
        new File(unconvertedSoundName).renameTo(new File(newUnconvertedSoundName));
        if (this.job.photos != null && this.job.photos.size() > 0) {
            Iterator i$ = this.job.photos.iterator();
            while (i$.hasNext()) {
                PhotoHolder ph = i$.next();
                int l = ph.size();
                for (int i = 0; i < l; i++) {
                    PhotoInfo pi = ph.get(i);
                    File file = new File(pi.filename);
                    pi.generateFilename(this.job, pi.time);
                    file.renameTo(new File(pi.filename));
                }
            }
        }
        try {
            this.job.nameIsCustom = true;
            this.job.save();
        } catch (IOException e) {
            toast(R.string.job_save_error, e);
        }
        setTitle(getString(R.string.job_title) + " - " + this.job.name);
    }

    /* access modifiers changed from: protected */
    public void updateJobPosition() {
        JobList jl = getJobList();
        jl.remove(this.job);
        jl.add(this.job);
    }

    public void doFinish() {
        finish();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void doConvertJob() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle((int) R.string.convert_job_title);
        b.setMessage((int) R.string.convert_job_message);
        b.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        b.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new ProgressDialogTask(ProgressDialog.show(BaseJobActivity.this, BaseJobActivity.this.getString(R.string.convert_job_progress_title), BaseJobActivity.this.getString(R.string.convert_job_progress_message), true, true), new Runnable() {
                    public void run() {
                        BaseJobActivity.this.mConversionHandler.sendEmptyMessage(BaseJobActivity.this.job.convertJob() ? 1 : 0);
                    }
                }).start();
            }
        });
        b.show();
    }

    /* access modifiers changed from: protected */
    public void onConvertJobFinish() {
        playSound(SpeakWriteApplication.BEEP_CONVERT_COMPLETED_ID);
    }

    /* access modifiers changed from: protected */
    public void onConvertJobFail() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle((int) R.string.error_title);
        b.setMessage((int) R.string.convert_job_error_message);
        b.setNeutralButton((int) R.string.ok, (DialogInterface.OnClickListener) null);
        b.show();
    }

    public void playSound(int soundID) {
        ((SpeakWriteApplication) getApplication()).playSound(soundID);
    }

    public void toast(int resourceID, Exception ex) {
        try {
            Toast.makeText(this, resourceID, 1).show();
            if (ex != null) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toast(String message, Exception ex) {
        try {
            Toast.makeText(this, message, 1).show();
            if (ex != null) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
