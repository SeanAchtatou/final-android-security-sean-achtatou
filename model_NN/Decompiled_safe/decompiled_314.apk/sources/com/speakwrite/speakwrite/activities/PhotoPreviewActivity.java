package com.speakwrite.speakwrite.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.jobs.PhotoInfo;
import com.speakwrite.speakwrite.utils.ImageUtils;
import java.io.File;
import java.io.IOException;
import org.apache.commons.lang.SystemUtils;

public class PhotoPreviewActivity extends BaseJobActivity implements View.OnClickListener {
    private static final int DIALOG_PROGRESS_LOADING = 2;
    private static final int DIALOG_PROGRESS_SAVING = 1;
    private static final String FILE_NAME = "file_name";
    public static final String RETAKE_IMAGE = "retake_image";
    public static final String START_CAMERA = "start_camera";
    private static final String TIME_SHIFT_KEY = "timeShift";
    /* access modifiers changed from: private */
    public Bitmap mImage;
    /* access modifiers changed from: private */
    public final Runnable mImageSavingRunnable = new Runnable() {
        public void run() {
            PhotoPreviewActivity.this.dismissDialog(1);
            if (PhotoPreviewActivity.this.mIsTakeAnother) {
                PhotoPreviewActivity.this.startCameraActivity();
                return;
            }
            PhotoPreviewActivity.this.setResult(12);
            PhotoPreviewActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public boolean mIsRetake = false;
    /* access modifiers changed from: private */
    public boolean mIsTakeAnother = false;
    private boolean mLandscape = true;
    /* access modifiers changed from: private */
    public final Runnable mLoadePreviedRunnable = new Runnable() {
        public void run() {
            PhotoPreviewActivity.this.dismissDialog(2);
            if (PhotoPreviewActivity.this.mImage != null) {
                PhotoPreviewActivity.this.mImageView.setImageBitmap(PhotoPreviewActivity.this.mImage);
            }
        }
    };
    /* access modifiers changed from: private */
    public File mPhotoTempFile;
    /* access modifiers changed from: private */
    public float mRotation = SystemUtils.JAVA_VERSION_FLOAT;
    /* access modifiers changed from: private */
    public long mTimeShift;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHandler = new Handler();
        setContentView((int) R.layout.photo_preview);
        Bundle bundle = getIntent().getExtras();
        if (savedInstanceState != null) {
            getJobFromBundle(savedInstanceState);
            this.mTimeShift = getTimeShiftFromBundle(savedInstanceState);
        } else {
            getJobFromIntent();
            getTimeShiftFromIntent();
            if (bundle != null && bundle.containsKey(START_CAMERA)) {
                startCameraActivity();
            }
        }
        if (bundle == null || !bundle.containsKey("retake_image")) {
            this.mIsRetake = false;
        } else {
            this.mIsRetake = true;
        }
        initControls();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(START_CAMERA, false);
        if (this.mPhotoTempFile != null) {
            outState.putString(FILE_NAME, this.mPhotoTempFile.toString());
        }
        saveTimeShift(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(FILE_NAME)) {
            this.mPhotoTempFile = new File(savedInstanceState.getString(FILE_NAME));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                ProgressDialog progress = new ProgressDialog(this);
                progress.setMessage(getResources().getString(R.string.saving_image));
                progress.setCancelable(false);
                return progress;
            case 2:
                ProgressDialog progress2 = new ProgressDialog(this);
                progress2.setMessage(getResources().getString(R.string.loading_image));
                progress2.setCancelable(false);
                return progress2;
            default:
                return super.onCreateDialog(id);
        }
    }

    private void initControls() {
        this.mImageView = (ImageView) findViewById(R.id.photo);
        ((Button) findViewById(R.id.use_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.retake_button)).setOnClickListener(this);
        ((Button) findViewById(R.id.take_button)).setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.retake_button) {
            startCameraActivity();
        } else if (id == R.id.take_button) {
            showDialog(1);
            this.mIsTakeAnother = true;
            savePhoto();
            this.mIsRetake = false;
        } else if (id == R.id.use_button) {
            showDialog(1);
            this.mIsTakeAnother = false;
            savePhoto();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mPhotoTempFile != null) {
            this.mPhotoTempFile.delete();
        }
        clearImage();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        final int width = getWindowManager().getDefaultDisplay().getWidth();
        if (this.mPhotoTempFile != null && this.mPhotoTempFile.exists()) {
            showDialog(2);
            clearImage();
            new Thread(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
                 arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
                 candidates:
                  ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
                  ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
                public void run() {
                    Bitmap unused = PhotoPreviewActivity.this.mImage = ImageUtils.getBitmapFromFile(PhotoPreviewActivity.this.mPhotoTempFile, width);
                    if (PhotoPreviewActivity.this.mRotation > SystemUtils.JAVA_VERSION_FLOAT) {
                        Matrix m = new Matrix();
                        m.postRotate(PhotoPreviewActivity.this.mRotation);
                        Bitmap unused2 = PhotoPreviewActivity.this.mImage = Bitmap.createBitmap(PhotoPreviewActivity.this.mImage, 0, 0, PhotoPreviewActivity.this.mImage.getWidth(), PhotoPreviewActivity.this.mImage.getHeight(), m, true);
                    }
                    PhotoPreviewActivity.this.mHandler.post(PhotoPreviewActivity.this.mLoadePreviedRunnable);
                }
            }).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 12 && resultCode == 12) {
            this.mRotation = data.getFloatExtra(PhotoCameraActivity.PHOTO_ROTATION, SystemUtils.JAVA_VERSION_FLOAT);
            this.mLandscape = data.getBooleanExtra(PhotoCameraActivity.PHOTO_LANDSCAPE, true);
            String path = data.getStringExtra(PhotoCameraActivity.PHOTO_TEMP_FILE);
            if (path != null) {
                this.mPhotoTempFile = new File(path);
                return;
            }
            return;
        }
        finish();
    }

    private void getTimeShiftFromIntent() {
        this.mTimeShift = getTimeShiftFromBundle(getIntent().getExtras());
    }

    public static long getTimeShiftFromBundle(Bundle bundle) {
        if (bundle != null) {
            return bundle.getLong(TIME_SHIFT_KEY);
        }
        return 0;
    }

    private void saveTimeShift(Bundle bundle) {
        bundle.putLong(TIME_SHIFT_KEY, this.mTimeShift);
    }

    public static void saveTimeShift(Intent intent, long timeShift) {
        intent.putExtra(TIME_SHIFT_KEY, timeShift);
    }

    public static void saveTimeShift(Bundle bundle, long timeShift) {
        bundle.putLong(TIME_SHIFT_KEY, timeShift);
    }

    /* access modifiers changed from: private */
    public void startCameraActivity() {
        startActivityForResult(new Intent(this, PhotoCameraActivity.class), 12);
    }

    private void clearImage() {
        if (this.mImage != null) {
            this.mImageView.setImageBitmap(null);
            this.mImage.recycle();
            this.mImage = null;
        }
    }

    private void savePhoto() {
        new Thread(new Runnable() {
            public void run() {
                PhotoInfo info;
                if (PhotoPreviewActivity.this.mIsRetake) {
                    PhotoHolder ph = PhotoPreviewActivity.this.job.findPhotoInfo(PhotoPreviewActivity.this.mTimeShift);
                    if (!(ph == null || ph.size() <= 0 || (info = ph.get(ph.getCurrentIndex())) == null)) {
                        PhotoPreviewActivity.this.savePhotoInfo(info, PhotoPreviewActivity.this.mPhotoTempFile);
                    }
                } else {
                    PhotoPreviewActivity.this.savePhotoInfo(PhotoPreviewActivity.this.job.getPhotoInfo(PhotoPreviewActivity.this.mTimeShift), PhotoPreviewActivity.this.mPhotoTempFile);
                }
                try {
                    PhotoPreviewActivity.this.job.save();
                } catch (IOException e) {
                    PhotoPreviewActivity.this.toast((int) R.string.job_save_error, e);
                }
                PhotoPreviewActivity.this.mHandler.post(PhotoPreviewActivity.this.mImageSavingRunnable);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void savePhotoInfo(PhotoInfo info, File file) {
        if (info != null && file != null) {
            info.rotation = this.mRotation;
            info.landscape = this.mLandscape;
            try {
                info.saveImage(file);
            } catch (IOException ex) {
                toast((int) R.string.saving_image_error, ex);
            }
            Log.d("PhotoPreview", "Saved picture with rotation " + this.mRotation);
        }
    }
}
