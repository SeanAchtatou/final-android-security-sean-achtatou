package com.speakwrite.speakwrite.activities;

import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;

public class FileNameInputFilter implements InputFilter {
    public static void addThisFilter(EditText et) {
        InputFilter[] ifiltrs = et.getFilters();
        int newSize = (ifiltrs != null ? ifiltrs.length : 0) + 1;
        InputFilter[] filters = new InputFilter[newSize];
        if (ifiltrs != null) {
            for (int i = 0; i < newSize - 1; i++) {
                filters[i] = ifiltrs[i];
            }
        }
        filters[filters.length - 1] = new FileNameInputFilter();
        et.setFilters(filters);
    }

    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (source == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < source.length(); i++) {
            char ch = source.charAt(i);
            if (isAllowed(ch)) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    private boolean isAllowed(char ch) {
        return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || ((ch >= '0' && ch <= '9') || ch == '_' || ch == '-' || ch == ' ' || ch == '!');
    }
}
