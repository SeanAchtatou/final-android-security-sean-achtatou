package com.speakwrite.speakwrite.activities;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.speakwrite.speakwrite.R;

public class OptionsActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.options);
        String record_action_key = getString(R.string.default_record_action_key);
        ListPreference list = (ListPreference) findPreference(record_action_key);
        list.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                OptionsActivity.setListSummary((ListPreference) preference, (String) newValue);
                return true;
            }
        });
        setListSummary(list, PreferenceManager.getDefaultSharedPreferences(this).getString(record_action_key, "2"));
    }

    /* access modifiers changed from: private */
    public static void setListSummary(ListPreference list, String value) {
        int action = Integer.parseInt(value) - 1;
        CharSequence[] entries = list.getEntries();
        if (action < 0 || action >= entries.length) {
            action = entries.length - 1;
        }
        list.setSummary(entries[action]);
    }
}
