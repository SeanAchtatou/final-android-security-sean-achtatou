package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.network.AsyncSender;
import com.speakwrite.speakwrite.network.InvalidStateException;
import com.speakwrite.speakwrite.utils.Converters;
import com.speakwrite.speakwrite.utils.PeriodicTask;

public class SubmitActivity extends BaseMenuJobActivity implements View.OnClickListener {
    private static final int CONNECTION_FAILURE_DIALOG = 2;
    private static final int DIALOG_CANCEL = 1;
    private static final int DIALOG_COMPLETED = 0;
    private static final String MESSAGE_KEY = "message_key";
    private String fileName;
    private String login;
    private boolean mAborting = false;
    private Handler mHandler;
    private TextView mMessages;
    private ProgressBar mProgress;
    /* access modifiers changed from: private */
    public AsyncSender mSender;
    private TextView mUploadSize;
    private String message = "";
    private String password;
    private final Runnable updateProgress = new Runnable() {
        public void run() {
            SubmitActivity.this.updateProgress();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Bundle b;
        this.mHandler = new Handler();
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.submit);
        this.mProgress = (ProgressBar) findViewById(R.id.progress);
        this.mMessages = (TextView) findViewById(R.id.progress_message);
        this.mUploadSize = (TextView) findViewById(R.id.upload_size);
        this.mUploadSize.setText(getString(R.string.prepare_upload));
        ((Button) findViewById(R.id.button_cancel)).setOnClickListener(this);
        if (savedInstanceState == null) {
            b = getIntent().getExtras();
        } else {
            b = savedInstanceState;
        }
        this.login = b.getString(getString(R.string.login_key));
        this.password = b.getString(getString(R.string.password_key));
        if (b.containsKey(getString(R.string.file_name_key))) {
            this.fileName = b.getString(getString(R.string.file_name_key));
        }
        try {
            this.mSender = ((SpeakWriteApplication) getApplication()).getAsyncSender(this.login, this.password);
        } catch (InvalidStateException e) {
            e.printStackTrace();
        }
        String sendToItem = getIntent().getStringExtra(FileNameActivity.SENDTO_KEY);
        if (sendToItem == null || sendToItem.length() <= 0) {
            this.mSender.setDestination(null);
        } else {
            this.mSender.setDestination(sendToItem);
        }
        if (savedInstanceState != null) {
            getJobFromBundle(savedInstanceState);
            updateProgress();
            this.message = savedInstanceState.getString(MESSAGE_KEY);
            this.mMessages.setText(getMessageFromStatus());
        } else {
            getJobFromIntent();
            try {
                this.mSender.setJob(this.job, this.fileName);
            } catch (InvalidStateException e2) {
                e2.printStackTrace();
            }
            this.mSender.start();
        }
        new PeriodicTask(this.mHandler, new PeriodicTask.Task() {
            public boolean doTask() {
                if (SubmitActivity.this.mSender == null) {
                    return false;
                }
                int status = SubmitActivity.this.mSender.getStatus();
                if (status == 3) {
                    SubmitActivity.this.onAbort();
                    return false;
                }
                if (status == 2) {
                    SubmitActivity.this.onSentFiles(SubmitActivity.this.mSender);
                } else if (status == 4) {
                    SubmitActivity.this.onError();
                }
                SubmitActivity.this.updateProgress();
                return true;
            }
        }, 250);
        enableWakeLock();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MESSAGE_KEY, this.message);
        outState.putString(getString(R.string.login_key), this.login);
        outState.putString(getString(R.string.password_key), this.password);
        if (this.fileName != null) {
            outState.putString(getString(R.string.file_name_key), this.fileName);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            if (this.mSender == null) {
                this.mSender = ((SpeakWriteApplication) getApplication()).getAsyncSender(this.login, this.password);
                this.mSender.setJob(this.job, this.fileName);
                this.mSender.start();
            }
        } catch (InvalidStateException e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                TextView text = new TextView(this, null, 16842817);
                text.setGravity(17);
                if (this.job.customerJobNumber == null) {
                    text.setText((int) R.string.job_not_uploaded);
                } else if (this.fileName != null) {
                    text.setText(this.fileName + " " + getString(R.string.job_custom_filename_message));
                    renameJob(this.fileName);
                } else {
                    text.setText(getString(R.string.job_number_message) + " " + this.job.customerJobNumber);
                }
                builder.setView(text);
                builder.setTitle((int) R.string.job_number_title);
                builder.setIcon(17301659);
                builder.setCancelable(false);
                builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SubmitActivity.this.onNewJob();
                    }
                });
                return builder.create();
            case 1:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                TextView text2 = new TextView(this, null, 16842817);
                text2.setGravity(17);
                text2.setText(getString(R.string.uploading_cancel));
                builder2.setView(text2);
                builder2.setTitle((int) R.string.job_number_title);
                builder2.setIcon(17301659);
                builder2.setCancelable(false);
                builder2.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SubmitActivity.this.onNewJob();
                    }
                });
                return builder2.create();
            case 2:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setTitle((int) R.string.connection_failure_title);
                TextView text3 = new TextView(this, null, 16842817);
                text3.setGravity(17);
                text3.setText((int) R.string.connection_failure_message);
                builder3.setView(text3);
                builder3.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SubmitActivity.this.setResult(0);
                        SubmitActivity.this.finish();
                    }
                });
                builder3.setCancelable(false);
                return builder3.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.button_cancel && this.mSender != null) {
            this.mAborting = true;
            this.mSender.abort();
        }
    }

    public void onAuthenticated(AsyncSender sender, boolean authenticated) {
        addMessage(authenticated ? R.string.done : R.string.failed);
    }

    public void onAuthenticating(AsyncSender sender) {
        addMessage(R.string.authenticating);
    }

    public void onSendingFiles(AsyncSender sender) {
        addMessage(R.string.sending_data);
    }

    public void onSentFiles(AsyncSender sender) {
        if (this.mAborting) {
            onAbort();
            return;
        }
        addMessage(R.string.done);
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    if (SubmitActivity.this.mSender != null) {
                        if (SubmitActivity.this.mSender.getStatus() != 3) {
                            SubmitActivity.this.mSender.join();
                            AsyncSender unused = SubmitActivity.this.mSender = null;
                        } else {
                            return;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SubmitActivity.this.playSound(SpeakWriteApplication.BEEP_UPLOAD_COMPLETED_ID);
                SubmitActivity.this.showDialog(0);
            }
        });
    }

    public void onAbort() {
        this.mAborting = false;
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    if (SubmitActivity.this.mSender != null) {
                        SubmitActivity.this.mSender.join();
                        AsyncSender unused = SubmitActivity.this.mSender = null;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SubmitActivity.this.showDialog(1);
            }
        });
    }

    public void onError() {
        if (this.mAborting) {
            onAbort();
        } else {
            this.mHandler.post(new Runnable() {
                public void run() {
                    try {
                        if (SubmitActivity.this.mSender != null) {
                            SubmitActivity.this.mSender.join();
                            AsyncSender unused = SubmitActivity.this.mSender = null;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SubmitActivity.this.showDialog(2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void updateProgress() {
        String message2 = getString(R.string.sending_data);
        int progress = this.mSender.getUploadProgress();
        int total = this.mSender.getUploadTotal();
        Log.i("SubmitActivity", message2 + " " + progress + " / " + total);
        if (total > 0) {
            this.mProgress.setIndeterminate(false);
            this.mProgress.setMax(total);
            this.mProgress.setProgress(progress);
            this.mUploadSize.setText(message2 + " " + Converters.formatByteValue((long) progress) + " / " + Converters.formatByteValue((long) total) + " MB");
        } else {
            this.mUploadSize.setText(message2 + "...");
            this.mProgress.setIndeterminate(true);
        }
        this.mProgress.invalidate();
        this.mUploadSize.invalidate();
    }

    private void addMessage(int messageId) {
        this.message += getResources().getString(messageId);
        this.mHandler.post(this.updateProgress);
    }

    private String getMessageFromStatus() {
        String message2;
        if (this.mSender == null) {
            return "";
        }
        switch (this.mSender.getStatus()) {
            case 1:
                message2 = getString(R.string.sending_data);
                break;
            case 2:
                message2 = getString(R.string.done);
                break;
            default:
                message2 = getString(R.string.waiting);
                break;
        }
        return message2;
    }
}
