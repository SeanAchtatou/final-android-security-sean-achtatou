package com.speakwrite.speakwrite.activities;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.speakwrite.speakwrite.AppConstants;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.SpeakWriteApplication;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.apache.commons.lang.SystemUtils;

public class PhotoCameraActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener, SensorEventListener {
    public static final String PHOTO_LANDSCAPE = "landscape";
    public static final String PHOTO_ROTATION = "rotation";
    public static final String PHOTO_TEMP_FILE = "tempFile";
    private Camera mCamera;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    private final Camera.PictureCallback mJpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] _data, Camera _camera) {
            File file = new File(SpeakWriteApplication.getDataFolder() + AppConstants.APP_PHOTO_TEMP_FILE_NAME);
            try {
                new FileOutputStream(file).write(_data);
                File unused = PhotoCameraActivity.this.mPhotoTempFile = file;
            } catch (IOException e) {
                Toast.makeText(PhotoCameraActivity.this, (int) R.string.saving_image_error, 1).show();
            } finally {
                PhotoCameraActivity.this.mHandler.post(PhotoCameraActivity.this.mSaveTempImageCompleted);
            }
        }
    };
    /* access modifiers changed from: private */
    public File mPhotoTempFile;
    private boolean mPreviewRunning;
    /* access modifiers changed from: private */
    public float mRotation = SystemUtils.JAVA_VERSION_FLOAT;
    /* access modifiers changed from: private */
    public final Runnable mSaveTempImageCompleted = new Runnable() {
        public void run() {
            Intent intent = new Intent();
            if (PhotoCameraActivity.this.mPhotoTempFile != null) {
                intent.putExtra(PhotoCameraActivity.PHOTO_TEMP_FILE, PhotoCameraActivity.this.mPhotoTempFile.toString());
                intent.putExtra(PhotoCameraActivity.PHOTO_LANDSCAPE, PhotoCameraActivity.this.mRotation != 90.0f);
                intent.putExtra(PhotoCameraActivity.PHOTO_ROTATION, PhotoCameraActivity.this.mRotation);
            }
            PhotoCameraActivity.this.setResult(12, intent);
            PhotoCameraActivity.this.finish();
        }
    };
    private SensorManager mSensorManager;
    private float mSensorZ = SystemUtils.JAVA_VERSION_FLOAT;
    private final Camera.ShutterCallback mShutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            float unused = PhotoCameraActivity.this.mRotation = (float) PhotoCameraActivity.this.getRotation();
        }
    };
    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(-3);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.camera);
        this.mSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        this.mSurfaceHolder = this.mSurfaceView.getHolder();
        this.mSurfaceHolder.addCallback(this);
        this.mSurfaceHolder.setType(3);
        ((ImageButton) findViewById(R.id.take_picture_button)).setOnClickListener(this);
        ((ImageButton) findViewById(R.id.cancel)).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        List<Sensor> list = this.mSensorManager.getSensorList(3);
        if (list != null && list.size() > 0) {
            this.mSensorManager.registerListener(this, list.get(0), 0);
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mSensorManager != null) {
            this.mSensorManager.unregisterListener(this);
        }
        super.onPause();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.mPreviewRunning) {
            this.mCamera.stopPreview();
            this.mPreviewRunning = false;
        }
        try {
            this.mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mCamera.startPreview();
        this.mPreviewRunning = true;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.mCamera == null) {
            this.mCamera = Camera.open();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mCamera != null) {
            this.mCamera.stopPreview();
            this.mPreviewRunning = false;
            this.mCamera.release();
            this.mCamera = null;
        }
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.take_picture_button) {
            this.mCamera.takePicture(this.mShutterCallback, null, this.mJpegCallback);
        } else if (id == R.id.cancel) {
            finish();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        this.mSensorZ = event.values[2];
    }

    /* access modifiers changed from: private */
    public int getRotation() {
        if (this.mSensorZ <= 45.0f && this.mSensorZ >= -45.0f) {
            return 90;
        }
        if (this.mSensorZ < -90.0f || this.mSensorZ > -45.0f) {
            return 0;
        }
        return 180;
    }
}
