package com.speakwrite.speakwrite.activities;

import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.audio.AudioController;

public abstract class AudioActivity extends BaseMenuJobActivity {
    /* access modifiers changed from: protected */
    public abstract void updateButtonState(AudioController audioController);

    /* access modifiers changed from: protected */
    public AudioController getAudioController() {
        return ((SpeakWriteApplication) getApplication()).audioController;
    }
}
