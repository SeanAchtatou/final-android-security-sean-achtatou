package com.speakwrite.speakwrite.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.drawables.HeaderDrawable;
import com.speakwrite.speakwrite.network.NetworkException;
import com.speakwrite.speakwrite.network.SendEmailFetcher;
import com.speakwrite.speakwrite.utils.TimeUtils;

public class JobSummaryActivity extends BaseMenuJobActivity {
    private static final int CONNECTION_FAILURE_DIALOG = 256;
    private static final String WS_STRING = " ";
    private Button mDeleteButton;
    private TextView mDuration;
    private Button mEditButton;
    private Button mEmailButton;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            JobSummaryActivity.this.mProgress.dismiss();
        }
    };
    private TextView mModificationData;
    /* access modifiers changed from: private */
    public ProgressDialog mProgress;
    private TextView mStatus;
    private Button mSubmitButton;
    private TextView mTime;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.job_summary);
        if (savedInstanceState != null) {
            getJobFromBundle(savedInstanceState);
        } else {
            getJobFromIntent();
        }
        initControls();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button
     arg types: [?, int, ?, com.speakwrite.speakwrite.activities.JobSummaryActivity$5]
     candidates:
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, java.lang.String, android.view.View$OnClickListener):android.widget.Button
      com.speakwrite.speakwrite.activities.BaseMenuJobActivity.initTopNavigationButton(int, boolean, int, android.view.View$OnClickListener):android.widget.Button */
    /* access modifiers changed from: protected */
    public void initControls() {
        super.initControls();
        this.mTime = (TextView) findViewById(R.id.time);
        this.mDuration = (TextView) findViewById(R.id.duration);
        this.mStatus = (TextView) findViewById(R.id.status);
        this.mModificationData = (TextView) findViewById(R.id.date);
        this.mEditButton = (Button) findViewById(R.id.edit_button);
        this.mEditButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JobSummaryActivity.this.edit();
            }
        });
        this.mEmailButton = (Button) findViewById(R.id.email_button);
        this.mEmailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JobSummaryActivity.this.email();
            }
        });
        this.mSubmitButton = (Button) findViewById(R.id.submit_button);
        this.mSubmitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JobSummaryActivity.this.submitCurrentJob();
            }
        });
        this.mDeleteButton = (Button) findViewById(R.id.delete_button);
        this.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JobSummaryActivity.this.askForDiscardingJob();
            }
        });
        this.mTime.setText(DateFormat.format(getString(R.string.time_format), this.job.modificationDate));
        this.mDuration.setText(TimeUtils.millisToString(this.job.duration));
        this.mModificationData.setText(getString(R.string.status_modified_ext) + WS_STRING + ((Object) DateFormat.format(getString(R.string.date_format), this.job.modificationDate)));
        this.mStatus.setText(getStatusString());
        initTopNavigationButton((int) R.id.top_navigation_left_button, true, (int) R.string.button_recording, (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View v) {
                JobSummaryActivity.this.setResult(4);
                JobSummaryActivity.this.finish();
            }
        });
        findViewById(R.id.header).setBackgroundDrawable(new HeaderDrawable(getResources().getColor(R.color.audio_file_item_border_color), getResources().getColor(R.color.audio_file_item_background_color)));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mStatus.setText(getStatusString());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean needFinish = true;
        if (resultCode == 10) {
            setResult(resultCode);
        } else if (requestCode == 3 && resultCode == 2) {
            setResult(resultCode, data);
        } else if (requestCode == 3 && resultCode == 8) {
            setResult(resultCode, data);
        } else if (requestCode == 6 && resultCode == 7) {
            needFinish = false;
            edit();
        } else if (requestCode == 6) {
            needFinish = false;
        }
        if (needFinish) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void openJobListActivity() {
        setResult(5);
        finish();
    }

    private CharSequence getStatusString() {
        if (this.job.status == 1) {
            return getString(R.string.status_submitted_ext) + WS_STRING + ((Object) DateFormat.format(getString(R.string.short_date_format), this.job.submitDate));
        }
        return getString(R.string.status_not_submitted);
    }

    /* access modifiers changed from: protected */
    public void onConvertJobFinish() {
        super.onConvertJobFinish();
        edit();
    }

    /* access modifiers changed from: private */
    public void edit() {
        if (this.job != null) {
            if (this.job.shouldConvertJob()) {
                doConvertJob();
                return;
            }
            Intent intent = new Intent(this, PlayingActivity.class);
            putJob(intent);
            startActivityForResult(intent, 3);
        }
    }

    /* access modifiers changed from: private */
    public void email() {
        this.mProgress = ProgressDialog.show(this, null, getResources().getString(R.string.loading_emailfile), true, false);
        new Thread(new Runnable() {
            public void run() {
                try {
                    String downloadLink = new SendEmailFetcher(JobSummaryActivity.this.job).fetchEmailUrl(JobSummaryActivity.this.getApplicationContext());
                    if (downloadLink == null) {
                        JobSummaryActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                JobSummaryActivity.this.showDialog(JobSummaryActivity.CONNECTION_FAILURE_DIALOG);
                            }
                        });
                    } else {
                        Intent sendIntent = new Intent("android.intent.action.SEND");
                        sendIntent.setFlags(268435456);
                        sendIntent.setType("text/html");
                        sendIntent.putExtra("android.intent.extra.TEXT", Html.fromHtml(JobSummaryActivity.this.getString(R.string.job_email_message).replaceAll("URL", downloadLink)));
                        sendIntent.putExtra("android.intent.extra.SUBJECT", JobSummaryActivity.this.getString(R.string.job_email_subject));
                        JobSummaryActivity.this.startActivity(Intent.createChooser(sendIntent, JobSummaryActivity.this.getString(R.string.email_client_select_title)));
                    }
                    JobSummaryActivity.this.mHandler.handleMessage(new Message());
                } catch (NetworkException e) {
                    JobSummaryActivity.this.toast((int) R.string.network_error, e);
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case CONNECTION_FAILURE_DIALOG /*256*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.connection_failure_title);
                TextView text = new TextView(this, null, 16842817);
                text.setGravity(17);
                text.setText((int) R.string.connection_failure_message);
                builder.setView(text);
                builder.setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        JobSummaryActivity.this.setResult(0);
                    }
                });
                builder.setCancelable(false);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }
}
