package com.speakwrite.speakwrite.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SDCardMountReceiver extends BroadcastReceiver {
    private static SDCardMountReceiverCallback m_callback;

    public interface SDCardMountReceiverCallback {
        void SDCardCallback(int i);
    }

    public static void setCallback(SDCardMountReceiverCallback callback) {
        m_callback = callback;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        int state = -1;
        if ("android.intent.action.MEDIA_MOUNTED".equals(action)) {
            state = 1;
        } else if ("android.intent.action.MEDIA_UNMOUNTED".equals(action)) {
            state = 0;
        }
        if (state != -1 && m_callback != null) {
            m_callback.SDCardCallback(state);
        }
    }
}
