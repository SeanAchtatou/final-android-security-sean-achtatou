package com.speakwrite.speakwrite;

import android.app.Application;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Environment;
import android.preference.PreferenceManager;
import com.speakwrite.speakwrite.audio.AudioController;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.jobs.JobList;
import com.speakwrite.speakwrite.network.AsyncSender;
import com.speakwrite.speakwrite.network.InvalidStateException;
import com.speakwrite.speakwrite.utils.SDCardUtils;
import java.io.File;
import java.lang.Thread;

public class SpeakWriteApplication extends Application {
    public static int BEEP_CONVERT_COMPLETED_ID = 0;
    public static int BEEP_START_RECORDING_ID = 0;
    public static int BEEP_UPLOAD_COMPLETED_ID = 0;
    private static File cacheDir = null;
    private static File dataDir = null;
    public AsyncSender asyncSender;
    public final AudioController audioController = new AudioController();
    public Thread authenticatingThread;
    public JobList jobList;
    public SoundPool soundPool;

    public AsyncSender getAsyncSender(String login, String password) throws InvalidStateException {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String number = pref.getString(getResources().getString(R.string.login_key), null);
        String pin = pref.getString(getResources().getString(R.string.password_key), null);
        if (login != null && login.length() > 0) {
            number = login;
            pin = "";
            if (password != null && password.length() > 0) {
                pin = password;
            }
        }
        if (this.asyncSender == null || this.asyncSender.getState() == Thread.State.TERMINATED) {
            this.asyncSender = new AsyncSender(number, pin);
        } else if (!this.asyncSender.getRunning()) {
            this.asyncSender.setAccount(number, pin);
        }
        return this.asyncSender;
    }

    public static File getDataFolder() {
        return dataDir;
    }

    public static File getCacheFolder() {
        return cacheDir;
    }

    public void onCreate() {
        super.onCreate();
        updateDataFolder();
        updateCacheFolder();
        loadSoundPool();
    }

    public void updateDataFolder() {
        File f;
        dataDir = null;
        Job.jobDirectory = null;
        this.jobList = null;
        if (SDCardUtils.isSdPresent() && (f = Environment.getExternalStorageDirectory()) != null && f.exists()) {
            dataDir = new File(f.getPath() + AppConstants.APP_FOLDER);
            Job.jobDirectory = dataDir.getPath();
        }
    }

    public void updateCacheFolder() {
        cacheDir = getCacheDir();
    }

    public void createJobList() {
        this.jobList = new JobList();
    }

    private void loadSoundPool() {
        this.soundPool = new SoundPool(1, 3, 0);
        if (this.soundPool != null) {
            BEEP_START_RECORDING_ID = this.soundPool.load(this, R.raw.beep_start_recording, 1);
            BEEP_UPLOAD_COMPLETED_ID = this.soundPool.load(this, R.raw.beep_upload_completed, 1);
            BEEP_CONVERT_COMPLETED_ID = BEEP_UPLOAD_COMPLETED_ID;
        }
    }

    public void playSound(int soundID) {
        if (this.soundPool != null) {
            this.soundPool.play(soundID, 1.0f, 1.0f, 0, 0, 1.0f);
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
            }
        }
    }
}
