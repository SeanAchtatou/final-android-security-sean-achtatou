package com.speakwrite.speakwrite.jobs;

import android.util.Log;
import com.speakwrite.speakwrite.utils.SDCardUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

public class JobList extends LinkedList<Job> implements FilenameFilter {
    private static final Pattern filenamePattern = Pattern.compile(".+\\.job");
    private static final long serialVersionUID = -5568613174387212409L;
    private static final Pattern unnamedPattern = Pattern.compile("Untitled_([0-9]+)\\.job");
    public static final String untitledName = "Untitled";
    private Comparator<Job> mJobComparator = new Comparator<Job>() {
        public int compare(Job job1, Job job2) {
            return -getJobData(job1).compareTo(getJobData(job2));
        }

        private Calendar getJobData(Job job) {
            Calendar jobDate = job.status == 1 ? job.submitDate : job.modificationDate;
            if (jobDate == null) {
                return new GregorianCalendar();
            }
            return jobDate;
        }
    };
    private int maxUnnamed;

    private String getName() {
        StringBuilder append = new StringBuilder().append("Untitled_");
        int i = this.maxUnnamed + 1;
        this.maxUnnamed = i;
        String name = append.append(i).toString();
        Log.d("JobList", "Create job name: " + name);
        return name;
    }

    public JobList() {
        String[] list;
        if (SDCardUtils.ensurePathExist(Job.jobDirectory) && (list = getList()) != null && list.length > 0) {
            for (String item : list) {
                try {
                    Job job = loadJob(item);
                    checkUntitled(item);
                    add(job);
                } catch (JobException e) {
                    e.printStackTrace();
                    try {
                        Job job2 = recreateJob(item);
                        checkUntitled(item);
                        add(job2);
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            sort();
        }
    }

    public Job createJob() {
        Job job = new Job(getName());
        add(job);
        return job;
    }

    public boolean accept(File dir, String filename) {
        return filenamePattern.matcher(filename).matches();
    }

    public boolean add(Job object) {
        int index = size() > 0 ? Collections.binarySearch(this, object, this.mJobComparator) : 0;
        if (index < 0) {
            index = (-index) - 1;
        }
        add(index, object);
        return true;
    }

    private void checkUntitled(String name) {
        int unnamedIndex;
        Matcher m = unnamedPattern.matcher(name);
        if (m.matches() && (unnamedIndex = Integer.decode(m.group(1)).intValue()) > this.maxUnnamed) {
            this.maxUnnamed = unnamedIndex;
        }
    }

    private static Job loadJob(String name) throws JobException {
        try {
            FileInputStream fis = new FileInputStream(Job.jobDirectory + File.separator + name);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Job job = (Job) ois.readObject();
            job.markPictures();
            job.resetPendingPictures();
            Log.d("JobList", "Loaded job: name = " + job.name + " duration = " + job.duration + " mod. date = " + job.modificationDate.getTime());
            ois.close();
            fis.close();
            return job;
        } catch (Exception e) {
            throw new JobException("Failed to load job.", e);
        }
    }

    private Job recreateJob(String name) throws IOException {
        new File(Job.jobDirectory + File.separator + name).delete();
        Job job = new Job(StringUtils.chomp(name, Job.EXTENTION));
        job.save();
        return job;
    }

    private void sort() {
        Collections.sort(this, this.mJobComparator);
    }

    private String[] getList() {
        return new File(Job.jobDirectory).list(this);
    }
}
