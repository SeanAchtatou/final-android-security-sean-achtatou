package com.speakwrite.speakwrite.jobs;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import com.speakwrite.speakwrite.AppConstants;
import com.speakwrite.speakwrite.utils.ImageUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import org.apache.commons.lang.SystemUtils;

public class PhotoInfo implements Serializable {
    private static final String ANSWER_NO = "NO";
    private static final String ANSWER_YES = "YES";
    private static final String EXTENTION = ".jpg";
    private static final long serialVersionUID = 8072686356867886049L;
    public String filename;
    private Job job;
    public boolean landscape = false;
    private Bitmap mImage;
    private Bitmap mSmallImage;
    private boolean pictureTaken = false;
    public float rotation = SystemUtils.JAVA_VERSION_FLOAT;
    public long time = 0;

    public PhotoInfo(Job job2, long time2) {
        PhotoHolder ph;
        this.time = time2;
        generateFilename(job2, time2);
        if (job2.photos == null) {
            job2.photos = new LinkedList<>();
            ph = new PhotoHolder();
            job2.photos.add(ph);
        } else {
            ph = job2.findPhotoInfo(time2);
            if (ph == null) {
                ph = new PhotoHolder();
                job2.photos.add(ph);
            }
        }
        ph.addPhoto(this);
        this.job = job2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public synchronized Bitmap getImage() {
        if (this.mImage == null && this.pictureTaken) {
            this.mImage = ImageUtils.getBitmapFromFile(new File(this.filename), AppConstants.IMAGE_PREVIEW_WIDTH);
            if (this.rotation != SystemUtils.JAVA_VERSION_FLOAT) {
                Matrix m = new Matrix();
                m.postRotate(90.0f);
                this.mImage = Bitmap.createBitmap(this.mImage, 0, 0, this.mImage.getWidth(), this.mImage.getHeight(), m, true);
            }
        }
        return this.mImage;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public synchronized Bitmap getSmallImage(int width) {
        if (this.mSmallImage == null) {
            this.mSmallImage = ImageUtils.getBitmapFromFile(new File(this.filename), Math.max(80, width));
            if (this.rotation != SystemUtils.JAVA_VERSION_FLOAT) {
                Matrix m = new Matrix();
                m.postRotate(this.rotation);
                this.mSmallImage = Bitmap.createBitmap(this.mSmallImage, 0, 0, this.mSmallImage.getWidth(), this.mSmallImage.getHeight(), m, true);
            }
            if (this.mImage != null) {
                this.mImage.recycle();
                this.mImage = null;
            }
        }
        return this.mSmallImage;
    }

    public boolean isImageLoaded() {
        return this.mImage != null;
    }

    public boolean isSmallImageLoaded() {
        return this.mSmallImage != null;
    }

    public boolean isPictureTaken() {
        return this.pictureTaken;
    }

    public void resetImage() {
        this.pictureTaken = false;
        this.mImage = null;
        this.mSmallImage = null;
    }

    public void generateFilename(Job job2, long time2) {
        this.filename = Job.jobDirectory + File.separator + getImageName(job2, time2);
    }

    public static String getImageName(Job job2, long time2) {
        return job2.name + "_" + time2 + "_" + System.currentTimeMillis() + EXTENTION;
    }

    public String toXmlString() {
        return "<Segment Position=\"" + (this.time / 1000) + "\" ImageName=\"" + new File(this.filename).getName() + "\" Landscape=\"" + answerToXml(this.landscape) + "\" Orientation=\"" + ((int) this.rotation) + "\"" + " IncludeGPS=\"FALSE\" IncludeTime=\"FALSE\"/>";
    }

    public void saveImage(File file) throws IOException {
        resetImage();
        if (file != null && file.exists()) {
            copy(file, new File(this.filename));
            this.job.subPicturesPending();
            this.job.save();
        }
    }

    public void setJob(Job job2) {
        this.job = job2;
    }

    private void copy(File src, File dst) {
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);
            byte[] buf = new byte[1024];
            while (true) {
                int len = in.read(buf);
                if (len > 0) {
                    out.write(buf, 0, len);
                } else {
                    in.close();
                    out.close();
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String answerToXml(boolean value) {
        return value ? ANSWER_YES : ANSWER_NO;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeBoolean(this.landscape);
        out.writeFloat(this.rotation);
        out.writeBoolean(this.pictureTaken);
        out.writeObject(this.filename);
        out.writeLong(this.time);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.landscape = in.readBoolean();
        this.rotation = in.readFloat();
        this.pictureTaken = in.readBoolean();
        this.filename = (String) in.readObject();
        this.time = in.readLong();
    }
}
