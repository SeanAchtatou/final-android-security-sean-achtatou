package com.speakwrite.speakwrite.jobs;

public class JobException extends Exception {
    private static final long serialVersionUID = 8583949441567156521L;

    public JobException(String message) {
        super(message);
    }

    public JobException(String message, Throwable innerException) {
        super(message, innerException);
    }
}
