package com.speakwrite.speakwrite.jobs;

import java.io.File;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

public class PhotoHolder implements Serializable {
    private static final long serialVersionUID = 449076508538325832L;
    private int mCurrentIndex = 0;
    private LinkedList<PhotoInfo> mPhotos = new LinkedList<>();

    public PhotoInfo getPhoto() {
        return this.mPhotos.getLast();
    }

    public void resetCurrentIndex() {
        this.mCurrentIndex = this.mPhotos.size() - 1;
    }

    public int getCurrentIndex() {
        return this.mCurrentIndex;
    }

    public int incrementIndex() {
        this.mCurrentIndex++;
        if (this.mCurrentIndex >= this.mPhotos.size()) {
            this.mCurrentIndex = -1;
        }
        return this.mCurrentIndex;
    }

    public int decrementIndex() {
        this.mCurrentIndex--;
        if (this.mCurrentIndex < 0) {
            this.mCurrentIndex = -1;
        }
        return this.mCurrentIndex;
    }

    public void addPhoto(PhotoInfo pi) {
        this.mPhotos.add(pi);
    }

    public int size() {
        return this.mPhotos.size();
    }

    public PhotoInfo get(int index) {
        return this.mPhotos.get(index);
    }

    public void deletePhotos() {
        Iterator i$ = this.mPhotos.iterator();
        while (i$.hasNext()) {
            new File(i$.next().filename).delete();
        }
    }

    public void remove(PhotoInfo photoInfo) {
        this.mPhotos.remove(photoInfo);
        resetCurrentIndex();
    }

    public void setCurrentIndexToStart() {
        this.mCurrentIndex = 0;
    }
}
