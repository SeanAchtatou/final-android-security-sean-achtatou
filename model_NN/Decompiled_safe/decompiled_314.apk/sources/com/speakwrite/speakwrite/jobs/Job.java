package com.speakwrite.speakwrite.jobs;

import android.util.Log;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.audio.AudioConverter;
import com.speakwrite.speakwrite.utils.DateUtils;
import com.speakwrite.speakwrite.utils.SDCardUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.commons.io.IOUtils;

public class Job implements Serializable {
    public static final String EXTENTION = ".job";
    private static final String JOB_TAG = "Job";
    public static final String SOUND_EXTENTION = ".amr";
    public static final int STATUS_NOT_SUBMITTED = 0;
    public static final int STATUS_SUBMITTED = 1;
    private static final String TEMP_MODIFIER = "_temp";
    public static final String UNCONVERTED_SOUND_EXTENTION = ".3gp";
    public static String jobDirectory = null;
    private static final long serialVersionUID = -6291394464042734031L;
    public String customerJobNumber;
    public long duration = 0;
    public Calendar modificationDate;
    public String name;
    public boolean nameIsCustom = false;
    private PhotoHolder photoChached;
    public LinkedList<PhotoHolder> photos;
    private int picturesInProgress = 0;
    public int status = 0;
    public Calendar submitDate;

    public interface PhotoComparer {
        PhotoInfo getPhotoInfo();

        boolean match(PhotoHolder photoHolder);
    }

    public String getSoundName() {
        return jobDirectory + File.separator + this.name + SOUND_EXTENTION;
    }

    public String getUnconvertedSoundName() {
        return jobDirectory + File.separator + this.name + UNCONVERTED_SOUND_EXTENTION;
    }

    public boolean shouldConvertJob() {
        return !new File(getSoundName()).exists() && new File(getUnconvertedSoundName()).exists();
    }

    public boolean convertJob() {
        return AudioConverter.convert3GPToAMR(getUnconvertedSoundName(), getSoundName());
    }

    public File getFileToSubmit() {
        File unconvertedSoundFile = new File(getUnconvertedSoundName());
        File soundFile = new File(getSoundName());
        if (unconvertedSoundFile.lastModified() > soundFile.lastModified()) {
            return unconvertedSoundFile;
        }
        return soundFile;
    }

    public String getFilename() {
        return jobDirectory + File.separator + this.name + EXTENTION;
    }

    public String getName() {
        return this.name;
    }

    public Job(String name2) {
        SDCardUtils.ensurePathExist(jobDirectory);
        this.name = name2;
    }

    public String photosToXml() {
        String retval = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Job>";
        Iterator i$ = this.photos.iterator();
        while (i$.hasNext()) {
            PhotoHolder ph = i$.next();
            for (int i = 0; i < ph.size(); i++) {
                retval = retval + IOUtils.LINE_SEPARATOR_UNIX + ph.get(i).toXmlString();
            }
        }
        return retval + "\n</Job>";
    }

    public void save(boolean modified) throws IOException {
        if (modified) {
            this.modificationDate = DateUtils.getToday();
        }
        FileOutputStream fos = new FileOutputStream(getFilename());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.close();
        fos.close();
    }

    public void save() throws IOException {
        save(true);
    }

    public int getStatusString() {
        switch (this.status) {
            case 1:
                return R.string.status_submitted;
            default:
                return R.string.status_not_submitted;
        }
    }

    public String getTempFilename() {
        return jobDirectory + File.separator + this.name + TEMP_MODIFIER + SOUND_EXTENTION;
    }

    public PhotoHolder findPhotoInfo(long timeShift) {
        if (this.photoChached != null) {
            if (this.photoChached.size() <= 0) {
                this.photos.remove(this.photoChached);
            } else if (this.photoChached.getPhoto().time == timeShift) {
                return this.photoChached;
            }
        }
        this.photoChached = null;
        if (this.photos != null && this.photos.size() > 0) {
            Log.d(JOB_TAG, "There are " + this.photos.size() + " photos");
            Iterator i$ = this.photos.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                PhotoHolder pi = i$.next();
                if (pi.getPhoto().time == timeShift) {
                    this.photoChached = pi;
                    break;
                }
            }
        }
        return this.photoChached;
    }

    public PhotoInfo getPhotoInfo(long time) {
        return new PhotoInfo(this, time);
    }

    public void markPictures() {
        if (this.photos != null) {
            Iterator i$ = this.photos.iterator();
            while (i$.hasNext()) {
                PhotoHolder ph = i$.next();
                int len = ph.size();
                for (int i = 0; i < len; i++) {
                    ph.get(i).setJob(this);
                }
            }
        }
    }

    public synchronized boolean picturesPending() {
        return this.picturesInProgress > 0;
    }

    public synchronized void addPicturePending() {
        this.picturesInProgress++;
    }

    public synchronized void subPicturesPending() {
        this.picturesInProgress--;
    }

    public void removePhoto(PhotoHolder pi) {
        this.photos.remove(pi);
        if (this.photoChached == pi) {
            this.photoChached = null;
        }
        pi.deletePhotos();
    }

    public PhotoHolder findPhoto(PhotoComparer comparer) {
        PhotoHolder pi = null;
        ArrayList<PhotoHolder> removeList = new ArrayList<>();
        if (this.photos != null && this.photos.size() > 0) {
            Iterator i$ = this.photos.iterator();
            while (i$.hasNext()) {
                PhotoHolder info = i$.next();
                if (info.size() < 1) {
                    removeList.add(info);
                } else if (comparer.match(info)) {
                    pi = info;
                }
            }
            Iterator i$2 = removeList.iterator();
            while (i$2.hasNext()) {
                this.photos.remove((PhotoHolder) i$2.next());
            }
            removeList.clear();
        }
        return pi;
    }

    public PhotoHolder findPhotoInfoBefore(long currentTime) {
        return findPhoto(new prevImageFinder(currentTime));
    }

    public PhotoHolder findPhotoInfoAfter(long currentTime) {
        return findPhoto(new nextImageFinder(currentTime));
    }

    public PhotoHolder findPhotoInfoCurrent(long currentTime) {
        return findPhoto(new currentImageFinder(currentTime));
    }

    public static class nextImageFinder implements PhotoComparer {
        private long currentTime;
        private PhotoInfo mPhotoInfo;
        private long minTime = Long.MAX_VALUE;

        public nextImageFinder(long time) {
            this.currentTime = time;
        }

        public boolean match(PhotoHolder info) {
            PhotoInfo pi = info.getPhoto();
            if (this.minTime <= pi.time || pi.time <= this.currentTime) {
                return false;
            }
            this.minTime = pi.time;
            this.mPhotoInfo = pi;
            return true;
        }

        public PhotoInfo getPhotoInfo() {
            return this.mPhotoInfo;
        }
    }

    public static class prevImageFinder implements PhotoComparer {
        private long currentTime;
        private PhotoInfo mPhotoInfo;
        private long maxTime = -1;

        public prevImageFinder(long time) {
            this.currentTime = time;
        }

        public boolean match(PhotoHolder info) {
            PhotoInfo pi = info.getPhoto();
            if (this.maxTime >= pi.time || pi.time >= this.currentTime) {
                return false;
            }
            this.maxTime = pi.time;
            return true;
        }

        public PhotoInfo getPhotoInfo() {
            return this.mPhotoInfo;
        }
    }

    public static class currentImageFinder implements PhotoComparer {
        private long currentTime;
        private PhotoInfo mPhotoInfo;
        private long maxTime = -1;

        public currentImageFinder(long time) {
            this.currentTime = time;
        }

        public boolean match(PhotoHolder info) {
            PhotoInfo pi = info.getPhoto();
            if (this.maxTime >= pi.time || pi.time > this.currentTime) {
                return false;
            }
            this.maxTime = pi.time;
            this.mPhotoInfo = pi;
            return true;
        }

        public PhotoInfo getPhotoInfo() {
            return this.mPhotoInfo;
        }
    }

    public synchronized void resetPendingPictures() {
        this.picturesInProgress = 0;
    }

    public void removePhoto(PhotoHolder photoHandler, PhotoInfo photoInfo) {
        photoHandler.remove(photoInfo);
        if (photoHandler.size() == 0) {
            this.photos.remove(photoHandler);
            this.photoChached = null;
        }
        new File(photoInfo.filename).delete();
    }
}
