package com.speakwrite.speakwrite.drawables;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.speakwrite.speakwrite.jobs.Job;
import java.util.Iterator;
import org.apache.commons.lang.SystemUtils;

public class PhotoMarkerDrawable extends Drawable {
    private Context m_context;
    private Job m_job;
    private Bitmap m_marker;
    private float m_secondSize;

    public PhotoMarkerDrawable(Context context, int markerId, Job job) {
        this.m_context = context;
        this.m_marker = BitmapFactory.decodeResource(this.m_context.getResources(), markerId);
        this.m_job = job;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        updateStep(bounds.width());
    }

    private void updateStep(int width) {
        if (this.m_job != null) {
            this.m_secondSize = ((float) width) / ((float) this.m_job.duration);
        }
    }

    public void draw(Canvas canvas) {
        if (this.m_job != null && this.m_job.photos != null && this.m_job.photos.size() > 0) {
            updateStep(getBounds().width());
            Iterator i$ = this.m_job.photos.iterator();
            while (i$.hasNext()) {
                float offsetX = ((float) i$.next().getPhoto().time) * this.m_secondSize;
                if (((float) this.m_marker.getWidth()) + offsetX > ((float) getBounds().width())) {
                    offsetX = (float) (getBounds().width() - this.m_marker.getWidth());
                }
                canvas.drawBitmap(this.m_marker, offsetX, (float) SystemUtils.JAVA_VERSION_FLOAT, (Paint) null);
            }
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
