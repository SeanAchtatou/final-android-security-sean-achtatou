package com.speakwrite.speakwrite.drawables;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import org.apache.commons.lang.SystemUtils;

public class HeaderDrawable extends Drawable {
    private static final int s_borderRound = 15;
    private static final int s_borderWidth = 1;
    private int mBackgroundColor;
    private Paint mBackgroundPaint;
    private Path mBackgroundPath;
    private int mBorderColor;
    private Paint mBorderPaint = new Paint();
    private Path mBorderPath;

    public HeaderDrawable(int borderColor, int backgroundColor) {
        this.mBorderColor = borderColor;
        this.mBackgroundColor = backgroundColor;
        this.mBorderPaint.setColor(this.mBorderColor);
        this.mBorderPaint.setAntiAlias(true);
        this.mBackgroundPaint = new Paint();
        this.mBackgroundPaint.setColor(this.mBackgroundColor);
        this.mBackgroundPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect bounds) {
        this.mBorderPath = null;
        this.mBackgroundPath = null;
        super.onBoundsChange(bounds);
    }

    public void draw(Canvas canvas) {
        canvas.drawPath(getBorderPath(), this.mBorderPaint);
        canvas.drawPath(getBackgroundPath(), this.mBackgroundPaint);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }

    private Path getBorderPath() {
        if (this.mBorderPath == null) {
            this.mBorderPath = getSinglePath(new RectF(getBounds()));
        }
        return this.mBorderPath;
    }

    private Path getBackgroundPath() {
        if (this.mBackgroundPath == null) {
            Rect bound = getBounds();
            this.mBackgroundPath = getSinglePath(new RectF((float) (bound.left + 1), (float) (bound.top + 1), (float) (bound.right - 1), (float) (bound.bottom - 1)));
        }
        return this.mBackgroundPath;
    }

    private Path getSinglePath(RectF rect) {
        Path path = new Path();
        path.moveTo(rect.left, rect.bottom - 15.0f);
        path.lineTo(rect.left, rect.top + 15.0f);
        path.arcTo(new RectF(rect.left, rect.top, rect.left + 15.0f, rect.top + 15.0f), -180.0f, 90.0f);
        path.lineTo(rect.left + 15.0f, rect.top);
        path.lineTo(rect.right - 15.0f, rect.top);
        path.arcTo(new RectF(rect.right - 15.0f, rect.top, rect.right, rect.top + 15.0f), -90.0f, 90.0f);
        path.lineTo(rect.right, rect.top + 15.0f);
        path.lineTo(rect.right, rect.bottom - 15.0f);
        path.arcTo(new RectF(rect.right - 15.0f, rect.bottom - 15.0f, rect.right, rect.bottom), SystemUtils.JAVA_VERSION_FLOAT, 90.0f);
        path.lineTo(rect.right - 15.0f, rect.bottom);
        path.lineTo(rect.left + 15.0f, rect.bottom);
        path.arcTo(new RectF(rect.left, rect.bottom - 15.0f, rect.left + 15.0f, rect.bottom), 90.0f, 90.0f);
        return path;
    }
}
