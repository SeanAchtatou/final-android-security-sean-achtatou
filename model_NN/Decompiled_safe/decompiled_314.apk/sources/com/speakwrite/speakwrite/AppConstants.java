package com.speakwrite.speakwrite;

public class AppConstants {
    public static final int ACT_CAMERA_RESULT = 12;
    public static final int ACT_EDIT = 7;
    public static final int ACT_EDIT_NAME = 9;
    public static final int ACT_GET_PHONE = 13;
    public static final int ACT_NULL_CODE = 0;
    public static final int ACT_OPEN_JOB_LIST = 10;
    public static final int ACT_PLAYING_REQUEST = 1;
    public static final int ACT_PLAYING_REQUEST_PASS = 3;
    public static final int ACT_PLAYING_RESULT = 2;
    public static final int ACT_SELECT = 8;
    public static final int ACT_SUBMIT = 6;
    public static final int ACT_SUMMARY_DELETE = 5;
    public static final int ACT_SUMMARY_RECORDINGS = 4;
    public static final String APP_FOLDER = "/speakEasy/";
    public static final String APP_PHOTO_TEMP_FILE_NAME = "photo_temp.tmp";
    public static final int IMAGE_PREVIEW_WIDTH = 320;
    public static final int IMAGE_SMALL_PREVIEW_WIDTH = 80;
    public static final int IMAGE_SMALL_WIDTH = 100;
}
