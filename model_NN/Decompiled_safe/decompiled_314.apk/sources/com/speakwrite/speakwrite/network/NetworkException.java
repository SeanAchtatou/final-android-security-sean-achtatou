package com.speakwrite.speakwrite.network;

public class NetworkException extends Exception {
    private static final long serialVersionUID = 8583949441567156521L;

    public NetworkException(String message) {
        super(message);
    }

    public NetworkException(String message, Throwable innerException) {
        super(message, innerException);
    }
}
