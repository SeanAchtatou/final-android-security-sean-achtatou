package com.speakwrite.speakwrite.network;

public class StatusLinkFetcher extends UrlFetcherBase {
    public StatusLinkFetcher(String login, String password) {
        super(login, password);
    }

    public String fetchStatusUrl() throws NetworkException {
        return doFetchUrl(Constants.GETTING_STATUS_LINK_URL, null, "jobstatuslink");
    }
}
