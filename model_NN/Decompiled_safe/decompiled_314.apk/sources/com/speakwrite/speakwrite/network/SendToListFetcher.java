package com.speakwrite.speakwrite.network;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import java.security.InvalidParameterException;

public class SendToListFetcher extends UrlFetcherBase {

    public static class SendToItem {
        public String id;
        public boolean isDefault;
        public String name;

        public static class Adapter extends BaseAdapter {
            private Context context;
            private SendToItem[] mList;

            public Adapter(Context context2, SendToItem[] items) {
                this.mList = items;
                if (this.mList == null) {
                    throw new InvalidParameterException("Items can't be null");
                }
                this.context = context2;
            }

            public int getCount() {
                return this.mList.length;
            }

            public Object getItem(int position) {
                return this.mList[position];
            }

            public long getItemId(int position) {
                return (long) position;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
             arg types: [?, android.view.ViewGroup, int]
             candidates:
              ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
              ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv;
                if (convertView instanceof TextView) {
                    tv = (TextView) convertView;
                } else {
                    tv = (TextView) ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.spinner_item, parent, false);
                }
                tv.setText(this.mList[position].name);
                return tv;
            }
        }

        public SendToItem(String name2, String id2, String def) {
            this.name = name2;
            this.id = id2;
            this.isDefault = Boolean.parseBoolean(def);
        }

        public SendToItem(String[] separated, int i) {
            this(separated[i], separated[i + 1], separated[i + 2]);
        }

        public String toString() {
            return this.name + " " + this.id + " " + this.isDefault;
        }
    }

    public SendToListFetcher(String login, String password) {
        super(login, password);
    }

    public String fetchSendToString() throws NetworkException {
        return doFetchUrl(Constants.SEND_TO_LIST_URL, null, "sendtolist");
    }

    public SendToItem[] fetchSendToList() throws NetworkException {
        return parseString(fetchSendToString());
    }

    public static SendToItem[] parseString(String sendToString) {
        String[] separated = sendToString.split("\\|");
        int k = 0;
        int limit = separated.length - 3;
        SendToItem[] items = new SendToItem[(separated.length / 3)];
        int i = 0;
        while (i < separated.length && i <= limit) {
            items[k] = new SendToItem(separated, i);
            k++;
            i = i + 2 + 1;
        }
        if (k > 0) {
            return items;
        }
        return null;
    }
}
