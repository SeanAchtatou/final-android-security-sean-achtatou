package com.speakwrite.speakwrite.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class SendEmailFetcher {
    private static final String APPLICATION_ID_STRING = "ApplicationID";
    private static final String HTTP_POST_UPLOADER_TAG = "SendEmailFetcher";
    private static final String SOUNDFILEURL_HEADER = "EmailDownloadUrl";
    private String emailLink;
    private Job job;

    public SendEmailFetcher(Job job2) {
        this.job = job2;
    }

    public String fetchEmailUrl(Context context) throws NetworkException {
        Header h;
        String str;
        HttpClient client = new DefaultHttpClient();
        try {
            if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                HttpPost mPost = new HttpPost(Constants.GETTING_EMAIL_DOWNLOAD_URL + Constants.CARRIER_INFO_STR);
                MultipartEntity me = new MultipartEntity();
                me.addPart(APPLICATION_ID_STRING, new StringBody(Constants.APPLICATION_GUID));
                File file = this.job.getFileToSubmit();
                me.addPart(file.getName(), new ProgressFileBody(file, FileUtils.getMimeType(file)));
                mPost.setEntity(me);
                HttpResponse resp = client.execute(mPost);
                switch (resp.getStatusLine().getStatusCode()) {
                    case HttpStatus.SC_OK /*200*/:
                        Header[] hdrs = resp.getHeaders(SOUNDFILEURL_HEADER);
                        if (hdrs.length > 0) {
                            h = hdrs[0];
                        } else {
                            h = null;
                        }
                        if (h != null) {
                            str = h.getValue();
                        } else {
                            str = null;
                        }
                        this.emailLink = str;
                        break;
                    case HttpStatus.SC_BAD_REQUEST /*400*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP BAD REQUEST");
                        break;
                    case HttpStatus.SC_UNAUTHORIZED /*401*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP UNAUTHORIZED");
                        break;
                    case HttpStatus.SC_FORBIDDEN /*403*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP FORBIDDEN");
                        break;
                    case HttpStatus.SC_NOT_FOUND /*404*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP NOT FOUND");
                        break;
                }
            } else {
                this.emailLink = null;
            }
            client.getConnectionManager().shutdown();
            return this.emailLink;
        } catch (IOException e) {
            throw new NetworkException("Error fetching email URL.", e);
        } catch (Throwable th) {
            client.getConnectionManager().shutdown();
            throw th;
        }
    }

    private class ProgressFileBody extends FileBody {
        private final File file;

        public ProgressFileBody(File file2, String mimeType) {
            super(file2, mimeType);
            this.file = file2;
        }

        public void writeTo(OutputStream out) throws IOException {
            if (out == null) {
                throw new IllegalArgumentException("Output stream may not be null");
            }
            InputStream in = new FileInputStream(this.file);
            try {
                byte[] tmp = new byte[4096];
                while (true) {
                    int l = in.read(tmp);
                    if (l != -1) {
                        out.write(tmp, 0, l);
                    } else {
                        out.flush();
                        return;
                    }
                }
            } finally {
                in.close();
            }
        }
    }
}
