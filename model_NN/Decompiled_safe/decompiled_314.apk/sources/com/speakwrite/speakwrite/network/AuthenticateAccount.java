package com.speakwrite.speakwrite.network;

import android.util.Log;
import com.speakwrite.speakwrite.R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AuthenticateAccount {
    private static final String APPLICATION_ID = "ApplicationID";
    private static final String AUTHENTICATE_ACCOUNT_TAG = "AuthenticateAccount";
    private static final String NEWLINE = "\r\n";
    private static final String PASSWORD = "PIN";
    private static final String PREFIX = "--";
    private static final String PRIMARY_PHONE = "AccountNumber";
    private String BOUNDARY;
    private String _pin;
    private String _primaryPhone;
    private boolean connectFail = false;
    private StatusListener defaultStatusListener = new StatusListener() {
        public void onSuccess(AuthenticateAccount authenticator) {
        }

        public void onError(AuthenticateAccount autenticator, int messageId) {
        }
    };
    private StatusListener statusListener;

    public interface StatusListener {
        void onError(AuthenticateAccount authenticateAccount, int i);

        void onSuccess(AuthenticateAccount authenticateAccount);
    }

    public boolean hasConnectionFailed() {
        return this.connectFail;
    }

    public void setStatusListener(StatusListener listener) {
        this.statusListener = listener;
    }

    private StatusListener getStatusListener() {
        StatusListener sl = this.statusListener;
        if (sl == null) {
            return this.defaultStatusListener;
        }
        return sl;
    }

    public AuthenticateAccount() {
    }

    public AuthenticateAccount(String number, String pin) {
        this._primaryPhone = number;
        this._pin = pin;
    }

    public boolean authenticate() {
        String str;
        try {
            if (authenticateAccount()) {
                getStatusListener().onSuccess(this);
                return true;
            } else if (!this.connectFail) {
                getStatusListener().onError(this, R.string.not_authenticate);
                return false;
            } else {
                Log.d(AUTHENTICATE_ACCOUNT_TAG, "Connection failure");
                getStatusListener().onError(this, R.string.connection_failure_message);
                return false;
            }
        } catch (Exception e) {
            Exception ex = e;
            String msg = ex.getMessage();
            if (msg == null) {
                str = "";
            } else {
                str = msg;
            }
            Log.d("AuthenticateAccount.authenticate", str);
            ex.printStackTrace();
            return false;
        }
    }

    /* JADX WARN: Type inference failed for: r9v21, types: [java.net.URLConnection] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x00cc A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00d1 A[Catch:{ Exception -> 0x011e }] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x00c7 A[SYNTHETIC, Splitter:B:7:0x00c7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean authenticateAccount() {
        /*
            r13 = this;
            r7 = 0
            r9 = 0
            r13.connectFail = r9
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011e }
            r9.<init>()     // Catch:{ Exception -> 0x011e }
            java.lang.String r10 = "---------------------------"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x011e }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x011e }
            r12 = 16
            java.lang.String r10 = java.lang.Long.toString(r10, r12)     // Catch:{ Exception -> 0x011e }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x011e }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x011e }
            r13.BOUNDARY = r9     // Catch:{ Exception -> 0x011e }
            java.net.URL r8 = new java.net.URL     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r9.<init>()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r10 = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/AuthenticateAccount.aspx"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r10 = com.speakwrite.speakwrite.network.Constants.CARRIER_INFO_STR     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = r9.toString()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r8.<init>(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r11 = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/AuthenticateAccount.aspx"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r11 = com.speakwrite.speakwrite.network.Constants.CARRIER_INFO_STR     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.net.URLConnection r9 = r8.openConnection()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r4 = r0
            r9 = 1
            r4.setDoOutput(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "POST"
            r4.setRequestMethod(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "User-Agent"
            java.lang.String r10 = "Profile/MIDP-1.0 Configuration/CLDC-1.0"
            r4.setRequestProperty(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "Accept-Language"
            java.lang.String r10 = "en-us"
            r4.setRequestProperty(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "Content-type"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r11 = "multipart/form-data; boundary="
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r11 = r13.BOUNDARY     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r4.setRequestProperty(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r2.<init>()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "AccountNumber"
            java.lang.String r10 = r13._primaryPhone     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r13.writeField(r2, r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "PIN"
            java.lang.String r10 = r13._pin     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r13.writeField(r2, r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "ApplicationID"
            java.lang.String r10 = "52198644-45ee-4632-9886-9533bd86bcb7"
            r13.writeField(r2, r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r13.close(r2)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r4.connect()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.io.OutputStream r6 = r4.getOutputStream()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            byte[] r9 = r2.toByteArray()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r6.write(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            int r9 = r4.getResponseCode()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            switch(r9) {
                case 200: goto L_0x01a0;
                case 400: goto L_0x012d;
                case 401: goto L_0x0177;
                case 403: goto L_0x0194;
                case 404: goto L_0x00d5;
                default: goto L_0x00c5;
            }
        L_0x00c5:
            if (r5 == 0) goto L_0x00ca
            r5.close()     // Catch:{ Exception -> 0x011e }
        L_0x00ca:
            if (r6 == 0) goto L_0x00cf
            r6.close()     // Catch:{ Exception -> 0x011e }
        L_0x00cf:
            if (r4 == 0) goto L_0x00d4
            r4.disconnect()     // Catch:{ Exception -> 0x011e }
        L_0x00d4:
            return r7
        L_0x00d5:
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.String r10 = "HTTP NOT FOUND"
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r9 = 1
            r13.connectFail = r9     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            goto L_0x00c5
        L_0x00e0:
            r9 = move-exception
            r3 = r9
            r9 = 1
            r13.connectFail = r9     // Catch:{ all -> 0x0183 }
            java.lang.String r9 = "AuthenticateAccount.authenticateAccount"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0183 }
            r10.<init>()     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = ""
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = r3.getMessage()     // Catch:{ all -> 0x0183 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0183 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0183 }
            android.util.Log.d(r9, r10)     // Catch:{ all -> 0x0183 }
            r3.printStackTrace()     // Catch:{ all -> 0x0183 }
            com.speakwrite.speakwrite.network.AuthenticateAccount$StatusListener r9 = r13.getStatusListener()     // Catch:{ all -> 0x0183 }
            r10 = 2131230776(0x7f080038, float:1.8077614E38)
            r9.onError(r13, r10)     // Catch:{ all -> 0x0183 }
            if (r5 == 0) goto L_0x0113
            r5.close()     // Catch:{ Exception -> 0x011e }
        L_0x0113:
            if (r6 == 0) goto L_0x0118
            r6.close()     // Catch:{ Exception -> 0x011e }
        L_0x0118:
            if (r4 == 0) goto L_0x00d4
            r4.disconnect()     // Catch:{ Exception -> 0x011e }
            goto L_0x00d4
        L_0x011e:
            r9 = move-exception
            r3 = r9
            java.lang.String r9 = "AuthenticateAccount.authenticateAccount.Top"
            java.lang.String r10 = r3.getMessage()
            android.util.Log.d(r9, r10)
            r3.printStackTrace()
            goto L_0x00d4
        L_0x012d:
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.String r10 = "HTTP BAD REQUEST"
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r9 = 1
            r13.connectFail = r9     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            goto L_0x00c5
        L_0x0138:
            r9 = move-exception
            r3 = r9
            r9 = 1
            r13.connectFail = r9     // Catch:{ all -> 0x0183 }
            java.lang.String r9 = "AuthenticateAccount.authenticateAccount"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0183 }
            r10.<init>()     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = ""
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0183 }
            java.lang.String r11 = r3.getMessage()     // Catch:{ all -> 0x0183 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x0183 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0183 }
            android.util.Log.d(r9, r10)     // Catch:{ all -> 0x0183 }
            r3.printStackTrace()     // Catch:{ all -> 0x0183 }
            com.speakwrite.speakwrite.network.AuthenticateAccount$StatusListener r9 = r13.getStatusListener()     // Catch:{ all -> 0x0183 }
            r10 = 2131230776(0x7f080038, float:1.8077614E38)
            r9.onError(r13, r10)     // Catch:{ all -> 0x0183 }
            if (r5 == 0) goto L_0x016b
            r5.close()     // Catch:{ Exception -> 0x011e }
        L_0x016b:
            if (r6 == 0) goto L_0x0170
            r6.close()     // Catch:{ Exception -> 0x011e }
        L_0x0170:
            if (r4 == 0) goto L_0x00d4
            r4.disconnect()     // Catch:{ Exception -> 0x011e }
            goto L_0x00d4
        L_0x0177:
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.String r10 = "HTTP UNAUTHORIZED"
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r9 = 1
            r13.connectFail = r9     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            goto L_0x00c5
        L_0x0183:
            r9 = move-exception
            if (r5 == 0) goto L_0x0189
            r5.close()     // Catch:{ Exception -> 0x011e }
        L_0x0189:
            if (r6 == 0) goto L_0x018e
            r6.close()     // Catch:{ Exception -> 0x011e }
        L_0x018e:
            if (r4 == 0) goto L_0x0193
            r4.disconnect()     // Catch:{ Exception -> 0x011e }
        L_0x0193:
            throw r9     // Catch:{ Exception -> 0x011e }
        L_0x0194:
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.String r10 = "HTTP FORBIDDEN"
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r9 = 1
            r13.connectFail = r9     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            goto L_0x00c5
        L_0x01a0:
            java.lang.String r9 = "authenticated"
            java.lang.String r1 = r4.getHeaderField(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r9 = "AuthenticateAccount"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            r10.<init>()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r11 = "authenticate string: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.StringBuilder r10 = r10.append(r1)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            java.lang.String r10 = r10.toString()     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            android.util.Log.d(r9, r10)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            if (r1 == 0) goto L_0x00c5
            java.lang.String r9 = "True"
            boolean r9 = r1.equals(r9)     // Catch:{ UnknownHostException -> 0x00e0, Exception -> 0x0138 }
            if (r9 == 0) goto L_0x00c5
            r7 = 1
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.speakwrite.speakwrite.network.AuthenticateAccount.authenticateAccount():boolean");
    }

    private final void close(ByteArrayOutputStream baos) {
        try {
            StringBuffer rv = new StringBuffer(PREFIX);
            rv.append(this.BOUNDARY);
            rv.append(PREFIX);
            rv.append("\r\n");
            baos.write(rv.toString().getBytes());
            baos.flush();
        } catch (Exception e) {
            Exception ex = e;
            Log.d("AuthenticateAccount.close", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void setAccountNumber(String value) {
        this._primaryPhone = value;
    }

    public void setPin(String value) {
        this._pin = value;
    }

    private final void writeField(ByteArrayOutputStream baos, String name, String value) throws IOException {
        try {
            StringBuffer rv = new StringBuffer(PREFIX);
            rv.append(this.BOUNDARY);
            rv.append("\r\n");
            rv.append("Content-Disposition: form-data; name=\"").append(name).append("\"");
            rv.append("\r\n");
            rv.append("\r\n");
            rv.append(value);
            rv.append("\r\n");
            baos.write(rv.toString().getBytes());
            baos.flush();
        } catch (Exception e) {
            Exception ex = e;
            Log.d("AuthenticateAccount.writeField", ex.getMessage());
            ex.printStackTrace();
        }
    }
}
