package com.speakwrite.speakwrite.network;

public class Constants {
    public static final String ACCEPT_LANGUAGE_KEY = "Accept-Language";
    public static final String ACCEPT_LANGUAGE_VALUE = "en-us";
    public static final String APPLICATION_GUID = "52198644-45ee-4632-9886-9533bd86bcb7";
    public static final String AUTHENTICATE_ACCOUNT_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/AuthenticateAccount.aspx";
    public static String CARRIER_INFO_STR = "";
    public static final int CONNECTION_FAILURE_MESSAGE_ID = 2131230776;
    public static final String CONTENT_TYPE_KEY = "Content-type";
    public static final String CONTENT_TYPE_VALUE = "multipart/form-data; boundary=";
    public static final int DATA_NOT_SENT_MESSAGE_ID = 2131230777;
    public static final String EXT_SEPARATOR = ".";
    public static final String FILE_EXT = "amr";
    public static final String FILE_EXT_3GP = "3gp";
    public static final String FILE_EXT_AMR = "amr";
    public static final String FILE_EXT_WAV = "wav";
    public static final String GETTING_EMAIL_DOWNLOAD_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/UploadForEmail.aspx";
    public static final String GETTING_PHONE_NUMBER_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/PhoneNumber.aspx";
    public static final String GETTING_STATUS_LINK_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/JobStatus2.aspx";
    public static final String MIME_AUDIO_3GP = "audio/3gpp";
    public static final String MIME_AUDIO_AMR = "audio/AMR";
    public static final String MIME_AUDIO_WAV = "audio/wav";
    public static final String MIME_IMAGE_JPEG2 = "image/jpeg";
    public static final String MIME_TEXT_XML = "text/xml";
    public static final int NOT_AUTHENTICATE_MESSAGE_ID = 2131230767;
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String PROTOCOL = "https://";
    public static final String SEND_TO_LIST_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/GetSendToOptions.aspx";
    public static final String SERVER = "https://service.speak-write.com/IntegrationService_Android1.55/";
    public static final String SERVER_AV = "service.speak-write.com/IntegrationService_Android1.55/";
    public static final String SETUP_URL = "http://www.speakwrite.com/AndroidSetup";
    public static final String STRING_EMPTY = "";
    public static final int UPLOAD_ERROR_MESSAGE_ID = 2131230768;
    public static final String UPLOAD_FILE_URL = "https://service.speak-write.com/IntegrationService_Android1.55/BlackBerry/UploadFile.aspx";
    public static final String USER_AGENT_KEY = "User-Agent";
    public static final String USER_AGENT_VALUE = "Profile/MIDP-1.0 Configuration/CLDC-1.0";
}
