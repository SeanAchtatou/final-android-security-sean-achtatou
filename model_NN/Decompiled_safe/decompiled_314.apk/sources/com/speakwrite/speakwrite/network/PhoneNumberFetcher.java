package com.speakwrite.speakwrite.network;

public class PhoneNumberFetcher extends UrlFetcherBase {
    public PhoneNumberFetcher(String login, String password) {
        super(login, password);
    }

    public String fetchPhoneNumber(String callNumber) throws NetworkException {
        return doFetchUrl(Constants.GETTING_PHONE_NUMBER_URL, "&&PhoneNumber=" + callNumber, "phonenumber");
    }
}
