package com.speakwrite.speakwrite.network;

import android.util.Log;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.PhotoHolder;
import com.speakwrite.speakwrite.utils.FileUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpPostUploader {
    private static final String APPLICATION_ID_STRING = "ApplicationID";
    private static final String CUSTOMERJOBNUMBER_HEADER = "customerjobnumber";
    private static final String CUSTOM_FILENAME_STRING = "CustomFilename";
    private static final String HTTP_POST_UPLOADER_TAG = "HttpPostUploader";
    private static final String METADATA_FILENAME = "metadata.xml";
    private static final String PASSWORD_STRING = "PIN";
    private static final String PRIMARY_PHONE_STRING = "AccountNumber";
    private static final String SEND_TO_ENTITY_STRING = "EntityIdString";
    public static final int STATUS_NONE = 0;
    public static final int STATUS_SENDING_AUDIO = 2;
    public static final int STATUS_SENDING_HEADER = 1;
    public static final int STATUS_SENDING_OTHER = 3;
    private String _accountNumber;
    private String _customFileName;
    private File _file;
    private LinkedList<PhotoHolder> _photos;
    private String _pin;
    private boolean connectFail;
    /* access modifiers changed from: private */
    public int fileProgress = 0;
    private int fileSize = 0;
    private String mDestId;
    private HttpPost mPost = null;
    private boolean m_needAbort = false;
    public int status = 0;
    private StatusListener statusListener;

    public interface StatusListener {
        void onError(HttpPostUploader httpPostUploader, int i);

        void onError(HttpPostUploader httpPostUploader, String str);

        void onProgressChanged(HttpPostUploader httpPostUploader, int i, int i2);

        void onStatusChanged(HttpPostUploader httpPostUploader, int i);

        void onSuccess(HttpPostUploader httpPostUploader, String str);
    }

    static /* synthetic */ int access$012(HttpPostUploader x0, int x1) {
        int i = x0.fileSize + x1;
        x0.fileSize = i;
        return i;
    }

    public synchronized void setStatusListener(StatusListener listener) {
        this.statusListener = listener;
    }

    public synchronized StatusListener getStatusListener() {
        return this.statusListener;
    }

    public HttpPostUploader(String account, String pin) {
        this._accountNumber = account;
        this._pin = pin;
    }

    public void setPhotos(LinkedList<PhotoHolder> photos) {
        this._photos = photos;
    }

    public void setAccountNumber(String value) {
        this._accountNumber = value;
    }

    public void setCustomFileName(String value) {
        this._customFileName = value;
    }

    public void setPin(String value) {
        this._pin = value;
    }

    public void setFile(File value) {
        this._file = value;
    }

    public void upload() throws Exception {
        uploadLarge();
        if (this.connectFail) {
            getStatusListener().onError(this, (int) R.string.connection_failure_message);
        }
    }

    public void abort() throws Exception {
        if (this.mPost != null && !this.mPost.isAborted()) {
            this.mPost.abort();
        }
        this.m_needAbort = true;
    }

    private void uploadLarge() throws NetworkException {
        Header h;
        String jobNumber;
        HttpClient client = new DefaultHttpClient();
        try {
            this.fileSize = 0;
            this.fileProgress = 0;
            this.mPost = new HttpPost(Constants.UPLOAD_FILE_URL + Constants.CARRIER_INFO_STR);
            MultipartEntity me = new MultipartEntity();
            me.addPart(PRIMARY_PHONE_STRING, new StringBody(this._accountNumber));
            me.addPart(PASSWORD_STRING, new StringBody(this._pin));
            if (this._customFileName != null && this._customFileName.length() > 0) {
                me.addPart(CUSTOM_FILENAME_STRING, new StringBody(this._customFileName));
            }
            me.addPart(APPLICATION_ID_STRING, new StringBody(Constants.APPLICATION_GUID));
            if (this.mDestId != null && this._photos == null) {
                me.addPart(SEND_TO_ENTITY_STRING, new StringBody(this.mDestId));
            }
            me.addPart(this._file.getName(), new ProgressFileBody(this._file, FileUtils.getMimeType(this._file)));
            appendPhotos(me);
            this.mPost.setEntity(me);
            if (!this.m_needAbort && !this.mPost.isAborted()) {
                HttpResponse resp = client.execute(this.mPost);
                switch (resp.getStatusLine().getStatusCode()) {
                    case HttpStatus.SC_OK /*200*/:
                        Header[] hdrs = resp.getHeaders(CUSTOMERJOBNUMBER_HEADER);
                        if (hdrs.length > 0) {
                            h = hdrs[0];
                        } else {
                            h = null;
                        }
                        if (h != null) {
                            jobNumber = h.getValue();
                        } else {
                            jobNumber = null;
                        }
                        StatusListener sl = getStatusListener();
                        Log.d(HTTP_POST_UPLOADER_TAG, "custom job number = " + jobNumber);
                        Log.d(HTTP_POST_UPLOADER_TAG, "response = " + resp.getStatusLine());
                        if (!this.m_needAbort && !this.mPost.isAborted() && jobNumber != null) {
                            if (jobNumber == null) {
                                sl.onError(this, (int) R.string.data_not_sent);
                                break;
                            } else {
                                sl.onSuccess(this, jobNumber);
                                break;
                            }
                        }
                    case HttpStatus.SC_BAD_REQUEST /*400*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP BAD REQUEST");
                        this.connectFail = true;
                        break;
                    case HttpStatus.SC_UNAUTHORIZED /*401*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP UNAUTHORIZED");
                        this.connectFail = true;
                        break;
                    case HttpStatus.SC_FORBIDDEN /*403*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP FORBIDDEN");
                        this.connectFail = true;
                        break;
                    case HttpStatus.SC_NOT_FOUND /*404*/:
                        Log.d(HTTP_POST_UPLOADER_TAG, "HTTP NOT FOUND");
                        this.connectFail = true;
                        break;
                }
            }
            this.m_needAbort = false;
            client.getConnectionManager().shutdown();
        } catch (IOException e) {
            throw new NetworkException("Error uploading file.", e);
        } catch (Throwable th) {
            this.m_needAbort = false;
            client.getConnectionManager().shutdown();
            throw th;
        }
    }

    private void appendPhotos(MultipartEntity me) {
        if (this._photos != null) {
            StringBuffer sb = new StringBuffer();
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Job>");
            Iterator i$ = this._photos.iterator();
            while (i$.hasNext()) {
                PhotoHolder ph = i$.next();
                int l = ph.size();
                for (int i = 0; i < l; i++) {
                    sb.append(ph.get(i).toXmlString());
                    sb.append(IOUtils.LINE_SEPARATOR_UNIX);
                }
            }
            sb.append("\n</Job>");
            me.addPart(METADATA_FILENAME, new InputStreamBody(new ByteArrayInputStream(sb.toString().getBytes()), Constants.MIME_TEXT_XML, METADATA_FILENAME));
            Iterator i$2 = this._photos.iterator();
            while (i$2.hasNext()) {
                PhotoHolder ph2 = i$2.next();
                int l2 = ph2.size();
                for (int i2 = 0; i2 < l2; i2++) {
                    File f = new File(ph2.get(i2).filename);
                    me.addPart(f.getName(), new ProgressFileBody(f, Constants.MIME_IMAGE_JPEG2));
                }
            }
            this._photos = null;
        }
    }

    static void logResponseHeader(Map<String, List<String>> header) {
        for (String key : header.keySet()) {
            Log.d(HTTP_POST_UPLOADER_TAG, "header key: " + key);
        }
    }

    /* access modifiers changed from: private */
    public void setProgress(int progress) {
        this.fileProgress = progress;
        StatusListener sl = getStatusListener();
        if (sl != null) {
            sl.onProgressChanged(this, this.fileProgress, this.fileSize);
        }
    }

    public int getFileProgress() {
        return this.fileProgress;
    }

    public int getFileSize() {
        return this.fileSize;
    }

    private class ProgressFileBody extends FileBody {
        private final File file;

        public ProgressFileBody(File file2, String mimeType) {
            super(file2, mimeType);
            this.file = file2;
            setTotal();
        }

        private void setTotal() {
            HttpPostUploader.access$012(HttpPostUploader.this, (int) this.file.length());
        }

        public void writeTo(OutputStream out) throws IOException {
            if (out == null) {
                throw new IllegalArgumentException("Output stream may not be null");
            }
            InputStream in = new FileInputStream(this.file);
            try {
                byte[] tmp = new byte[4096];
                while (true) {
                    int l = in.read(tmp);
                    if (l != -1) {
                        out.write(tmp, 0, l);
                        HttpPostUploader.this.setProgress(HttpPostUploader.this.fileProgress + l);
                    } else {
                        out.flush();
                        return;
                    }
                }
            } finally {
                in.close();
            }
        }
    }

    public void setDestination(String id) {
        this.mDestId = id;
    }
}
