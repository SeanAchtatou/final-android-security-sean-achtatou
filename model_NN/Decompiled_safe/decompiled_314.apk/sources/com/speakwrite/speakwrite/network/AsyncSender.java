package com.speakwrite.speakwrite.network;

import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.network.HttpPostUploader;
import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;

public class AsyncSender extends Thread implements HttpPostUploader.StatusListener {
    public static final int STATUS_ABORT = 3;
    public static final int STATUS_DONE = 2;
    public static final int STATUS_ERROR = 4;
    public static final int STATUS_SENDING = 1;
    public static final int STATUS_WAITING = 0;
    private String mDestId;
    private Job mJob;
    private File mJobFile;
    private boolean mRunning = false;
    private int mStatus = 0;
    public HttpPostUploader mUploader;

    public synchronized int getStatus() {
        return this.mStatus;
    }

    private synchronized void setStatus(int status) {
        this.mStatus = status;
    }

    public void setJob(Job job, String filename) throws InvalidStateException {
        if (getRunning()) {
            throw new InvalidStateException();
        }
        this.mJob = job;
        this.mJobFile = job.getFileToSubmit();
        this.mUploader.setCustomFileName(filename);
        this.mUploader.setFile(this.mJobFile);
        this.mUploader.setPhotos(this.mJob.photos);
    }

    public synchronized void setAccount(String number, String pin) throws InvalidStateException {
        if (getRunning()) {
            throw new InvalidStateException();
        }
        this.mUploader.setAccountNumber(number);
        this.mUploader.setPin(pin);
    }

    public synchronized int getUploadProgress() {
        return this.mUploader.getFileProgress();
    }

    public synchronized int getUploadTotal() {
        return this.mUploader.getFileSize();
    }

    public synchronized int getUploaderStatus() {
        return this.mUploader.status;
    }

    private synchronized void setRunning(boolean value) {
        this.mRunning = value;
    }

    public synchronized boolean getRunning() {
        return this.mRunning;
    }

    public AsyncSender(String number, String pin) {
        this.mUploader = new HttpPostUploader(number, pin);
        this.mUploader.setStatusListener(this);
    }

    public void run() {
        try {
            setRunning(true);
            setStatus(1);
            this.mUploader.setDestination(this.mDestId);
            this.mUploader.upload();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            setRunning(false);
            setStatus(2);
        }
    }

    public void abort() {
        if (getStatus() != 2) {
            setRunning(false);
            setStatus(3);
            try {
                this.mUploader.abort();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onError(HttpPostUploader uploader, String message) {
        setStatus(4);
    }

    public void onProgressChanged(HttpPostUploader uploader, int progress, int total) {
    }

    public void onStatusChanged(HttpPostUploader uploader, int status) {
    }

    public void onSuccess(HttpPostUploader uploader, String message) {
        if (message != null) {
            setStatus(2);
            this.mJob.customerJobNumber = message;
            this.mJob.submitDate = new GregorianCalendar();
            this.mJob.status = 1;
            try {
                this.mJob.save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onError(HttpPostUploader uploader, int messageId) {
        setStatus(4);
    }

    public void setDestination(String id) {
        this.mDestId = id;
    }
}
