package com.speakwrite.speakwrite.network;

public abstract class UrlFetcherBase {
    protected String login;
    protected String password;

    public UrlFetcherBase(String login2, String password2) {
        this.login = login2;
        this.password = password2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 16 */
    /* JADX WARN: Type inference failed for: r8v0, types: [java.net.URLConnection] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doFetchUrl(java.lang.String r17, java.lang.String r18, java.lang.String r19) throws com.speakwrite.speakwrite.network.NetworkException {
        /*
            r16 = this;
            r9 = 0
            r6 = 0
            r7 = 0
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r12.<init>()     // Catch:{ Exception -> 0x00ec }
            r0 = r12
            r1 = r17
            java.lang.StringBuilder r12 = r0.append(r1)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = "?AccountNumber="
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            r0 = r16
            java.lang.String r0 = r0.login     // Catch:{ Exception -> 0x00ec }
            r13 = r0
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = "&&PIN="
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            r0 = r16
            java.lang.String r0 = r0.password     // Catch:{ Exception -> 0x00ec }
            r13 = r0
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = "&&ApplicationID="
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = "52198644-45ee-4632-9886-9533bd86bcb7"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            if (r18 == 0) goto L_0x00bb
            r13 = r18
        L_0x003d:
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r11 = r12.toString()     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r12.<init>()     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = "---------------------------"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00ec }
            r15 = 16
            java.lang.String r13 = java.lang.Long.toString(r13, r15)     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r2 = r12.toString()     // Catch:{ Exception -> 0x00ec }
            java.net.URL r10 = new java.net.URL     // Catch:{ Exception -> 0x00ec }
            r10.<init>(r11)     // Catch:{ Exception -> 0x00ec }
            java.net.URLConnection r8 = r10.openConnection()     // Catch:{ Exception -> 0x00ec }
            r0 = r8
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00ec }
            r6 = r0
            r12 = 1
            r6.setDoOutput(r12)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r12 = "POST"
            r6.setRequestMethod(r12)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r12 = "User-Agent"
            java.lang.String r13 = "Profile/MIDP-1.0 Configuration/CLDC-1.0"
            r6.setRequestProperty(r12, r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r12 = "Accept-Language"
            java.lang.String r13 = "en-us"
            r6.setRequestProperty(r12, r13)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r12 = "Content-type"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r13.<init>()     // Catch:{ Exception -> 0x00ec }
            java.lang.String r14 = "multipart/form-data; boundary="
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r13 = r13.append(r2)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x00ec }
            r6.setRequestProperty(r12, r13)     // Catch:{ Exception -> 0x00ec }
            r6.connect()     // Catch:{ Exception -> 0x00ec }
            java.io.OutputStream r7 = r6.getOutputStream()     // Catch:{ Exception -> 0x00ec }
            r7.close()     // Catch:{ Exception -> 0x00ec }
            r7 = 0
            int r12 = r6.getResponseCode()     // Catch:{ Exception -> 0x00ec }
            switch(r12) {
                case 200: goto L_0x00be;
                default: goto L_0x00b0;
            }
        L_0x00b0:
            if (r7 == 0) goto L_0x00b5
            r7.close()     // Catch:{ Exception -> 0x00de }
        L_0x00b5:
            if (r6 == 0) goto L_0x00ba
            r6.disconnect()
        L_0x00ba:
            return r9
        L_0x00bb:
            java.lang.String r13 = ""
            goto L_0x003d
        L_0x00be:
            java.util.Map r5 = r6.getHeaderFields()     // Catch:{ Exception -> 0x00ec }
            com.speakwrite.speakwrite.network.HttpPostUploader.logResponseHeader(r5)     // Catch:{ Exception -> 0x00ec }
            r0 = r5
            r1 = r19
            java.lang.Object r4 = r0.get(r1)     // Catch:{ Exception -> 0x00ec }
            java.util.List r4 = (java.util.List) r4     // Catch:{ Exception -> 0x00ec }
            if (r4 != 0) goto L_0x00d4
            r12 = 0
            r8 = r12
        L_0x00d2:
            r9 = r8
            goto L_0x00b0
        L_0x00d4:
            r12 = 0
            java.lang.Object r16 = r4.get(r12)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r16 = (java.lang.String) r16     // Catch:{ Exception -> 0x00ec }
            r8 = r16
            goto L_0x00d2
        L_0x00de:
            r3 = move-exception
            java.lang.String r12 = "OutputStream.close"
            java.lang.String r13 = r3.getMessage()
            android.util.Log.d(r12, r13)
            r3.printStackTrace()
            goto L_0x00b5
        L_0x00ec:
            r12 = move-exception
            r3 = r12
            com.speakwrite.speakwrite.network.NetworkException r12 = new com.speakwrite.speakwrite.network.NetworkException     // Catch:{ all -> 0x00f6 }
            java.lang.String r13 = "Could not fetch URL."
            r12.<init>(r13, r3)     // Catch:{ all -> 0x00f6 }
            throw r12     // Catch:{ all -> 0x00f6 }
        L_0x00f6:
            r12 = move-exception
            if (r7 == 0) goto L_0x00fc
            r7.close()     // Catch:{ Exception -> 0x0102 }
        L_0x00fc:
            if (r6 == 0) goto L_0x0101
            r6.disconnect()
        L_0x0101:
            throw r12
        L_0x0102:
            r3 = move-exception
            java.lang.String r13 = "OutputStream.close"
            java.lang.String r14 = r3.getMessage()
            android.util.Log.d(r13, r14)
            r3.printStackTrace()
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.speakwrite.speakwrite.network.UrlFetcherBase.doFetchUrl(java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }
}
