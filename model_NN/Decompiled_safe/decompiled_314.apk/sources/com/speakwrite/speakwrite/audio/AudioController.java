package com.speakwrite.speakwrite.audio;

import com.speakwrite.speakwrite.audio.AMRPlayer;
import com.speakwrite.speakwrite.utils.MathUtils;
import java.io.File;
import java.io.IOException;

public class AudioController {
    private AudioState mAudioState = AudioState.Stopped;
    /* access modifiers changed from: private */
    public volatile long mDuration = 0;
    private String mFileName = "/sdcard/file.amr";
    private AMRPlayer mPlayer;
    private AMRRecorder mRecorder;
    /* access modifiers changed from: private */
    public volatile long mTime = 0;

    public enum AudioState {
        Playing,
        Recording,
        Paused,
        Stopped
    }

    public AMRRecorder getRecorder() {
        if (this.mRecorder == null) {
            this.mRecorder = AMRRecorder.getRecorder();
        }
        return this.mRecorder;
    }

    public AMRPlayer getPlayer() {
        if (this.mPlayer == null) {
            this.mPlayer = AMRPlayer.getPlayer();
            this.mPlayer.setOnPlaybackFinishedListener(new AMRPlayer.OnPlaybackFinishedListener() {
                public void onPlaybackFinished(AMRPlayer player) {
                    long unused = AudioController.this.mTime = AudioController.this.mDuration;
                    AudioController.this.setState(AudioState.Stopped);
                }
            });
            this.mPlayer.setOnPlaybackPositionUpdatedListener(new AMRPlayer.OnPlaybackPositionUpdatedListener() {
                public void onPlaybackPositionUpdated(AMRPlayer player) {
                    long unused = AudioController.this.mTime = player.getCurrentTime();
                }
            });
        }
        return this.mPlayer;
    }

    public boolean isFileEmpty() {
        return !getFile().exists();
    }

    public File getFile() {
        return new File(getFileName());
    }

    public String getFileName() {
        return this.mFileName;
    }

    public synchronized void setFileName(String fileName) {
        if (this.mAudioState == AudioState.Stopped) {
            this.mFileName = fileName;
        }
    }

    public synchronized AudioState getState() {
        return this.mAudioState;
    }

    /* access modifiers changed from: private */
    public void setState(AudioState state) {
        this.mAudioState = state;
    }

    public synchronized boolean openFile() throws IOException {
        boolean z;
        if (this.mPlayer != null) {
            releasePlayer();
        }
        this.mPlayer = getPlayer();
        if (!isFileEmpty()) {
            this.mPlayer.setInputFile(this.mFileName);
            this.mDuration = this.mPlayer.getDuration();
            setState(AudioState.Stopped);
            z = true;
        } else {
            this.mDuration = 0;
            setState(AudioState.Stopped);
            z = false;
        }
        return z;
    }

    public synchronized void play() throws IOException {
        if (this.mAudioState == AudioState.Paused) {
            resumePlay();
            this.mPlayer.startPlaying();
            setState(AudioState.Playing);
        } else if (this.mAudioState == AudioState.Stopped && openFile()) {
            resumePlay();
            this.mPlayer.startPlaying();
            setState(AudioState.Playing);
        }
    }

    private void resumePlay() {
        this.mTime = this.mTime == this.mDuration ? 0 : this.mTime;
        this.mPlayer.seekToTime(this.mTime);
    }

    public synchronized void pause() {
        if (this.mAudioState == AudioState.Playing) {
            this.mPlayer.pause();
            setState(AudioState.Paused);
        }
    }

    public synchronized void record() throws IOException {
        if (this.mAudioState == AudioState.Paused) {
            stop();
        }
        if (this.mAudioState == AudioState.Stopped) {
            if (this.mRecorder != null) {
                releaseRecorder();
            }
            this.mRecorder = getRecorder();
            this.mRecorder.setOutputFile(this.mFileName);
            this.mRecorder.startRecording();
            setState(AudioState.Recording);
        }
    }

    public synchronized void stop() {
        if (this.mAudioState == AudioState.Recording) {
            releaseRecorder();
            setState(AudioState.Stopped);
        } else if (this.mAudioState == AudioState.Playing || this.mAudioState == AudioState.Paused) {
            releasePlayer();
            setState(AudioState.Stopped);
        }
    }

    public synchronized void seek(long timeInMilliseconds) {
        if (this.mAudioState != AudioState.Recording) {
            pause();
            this.mTime = MathUtils.min(timeInMilliseconds, this.mDuration);
        }
    }

    public int getMaxRecordingLevel() {
        return 32767;
    }

    public int getRecordingLevel() {
        if (this.mRecorder != null) {
            return this.mRecorder.getMaxAmplitude();
        }
        return 0;
    }

    public synchronized long getCurrentTime() {
        return this.mTime;
    }

    public synchronized long getTotalTime() {
        return this.mDuration;
    }

    public void releasePlayer() {
        if (this.mPlayer != null) {
            this.mPlayer.setOnPlaybackFinishedListener(null);
            this.mPlayer.setOnPlaybackPositionUpdatedListener(null);
            this.mPlayer.release();
            this.mPlayer = null;
        }
    }

    public void releaseRecorder() {
        if (this.mRecorder != null) {
            this.mRecorder.release();
            this.mRecorder = null;
        }
    }
}
