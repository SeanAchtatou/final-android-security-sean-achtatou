package com.speakwrite.speakwrite.audio;

public class AMRException extends Exception {
    private static final long serialVersionUID = -8573845098968967705L;

    public AMRException(String message) {
        super(message);
    }
}
