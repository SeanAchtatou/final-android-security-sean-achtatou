package com.speakwrite.speakwrite.audio;

import android.media.MediaPlayer;
import com.speakwrite.speakwrite.utils.Looper;
import java.io.File;
import java.io.IOException;

public class AMRPlayer {
    private int audioStream;
    private OnPlaybackFinishedListener onPlaybackFinishedListener;
    private OnPlaybackPositionUpdatedListener onPlaybackPositionUpdatedListener;
    /* access modifiers changed from: private */
    public boolean paused = false;
    private MediaPlayer player = new MediaPlayer();
    /* access modifiers changed from: private */
    public boolean playing = false;

    public interface OnPlaybackFinishedListener {
        void onPlaybackFinished(AMRPlayer aMRPlayer);
    }

    public interface OnPlaybackPositionUpdatedListener {
        void onPlaybackPositionUpdated(AMRPlayer aMRPlayer);
    }

    public static AMRPlayer getPlayer() {
        return new AMRPlayer(3);
    }

    public AMRPlayer(int audioStream2) {
        this.audioStream = audioStream2;
    }

    public void setInputFile(File file) throws IOException {
        setInputFile(file.getAbsolutePath());
    }

    public void setInputFile(String fileName) throws IOException {
        if (!isPlaying()) {
            this.player.reset();
            this.player.setDataSource(fileName);
            this.player.setAudioStreamType(this.audioStream);
            this.player.prepare();
            this.playing = false;
            this.paused = false;
        }
    }

    public void startPlaying() {
        if (!isPlaying()) {
            this.player.start();
            this.playing = true;
            this.paused = false;
            Looper.loop(new Looper.Iteration() {
                public boolean iterate() {
                    if (AMRPlayer.this.isPlaying()) {
                        AMRPlayer.this.onPlaybackPositionUpdated();
                        return true;
                    } else if (AMRPlayer.this.isPaused()) {
                        return true;
                    } else {
                        boolean unused = AMRPlayer.this.playing = false;
                        boolean unused2 = AMRPlayer.this.paused = false;
                        AMRPlayer.this.onPlaybackFinished();
                        return false;
                    }
                }
            }, 250);
        }
    }

    public void stopPlaying() {
        if (isPlaying() || isPaused()) {
            this.player.stop();
            this.playing = false;
            this.paused = false;
        }
    }

    public void pause() {
        if (isPlaying() && !isPaused()) {
            this.player.pause();
            this.playing = false;
            this.paused = true;
        }
    }

    public boolean isPaused() {
        return this.paused;
    }

    public boolean isPlaying() {
        return this.playing && this.player != null && this.player.isPlaying();
    }

    public void release() {
        stopPlaying();
        this.player.release();
        this.player = null;
        this.playing = false;
        this.paused = false;
    }

    public long getDuration() {
        if (this.player != null) {
            return (long) this.player.getDuration();
        }
        return 0;
    }

    public long getCurrentTime() {
        if (this.player != null) {
            return (long) this.player.getCurrentPosition();
        }
        return 0;
    }

    public void seekToTime(long time) {
        if (this.player != null) {
            this.player.seekTo((int) time);
        }
    }

    /* access modifiers changed from: private */
    public void onPlaybackPositionUpdated() {
        if (this.onPlaybackPositionUpdatedListener != null) {
            this.onPlaybackPositionUpdatedListener.onPlaybackPositionUpdated(this);
        }
    }

    public void setOnPlaybackPositionUpdatedListener(OnPlaybackPositionUpdatedListener listener) {
        this.onPlaybackPositionUpdatedListener = listener;
    }

    /* access modifiers changed from: private */
    public void onPlaybackFinished() {
        if (this.onPlaybackFinishedListener != null) {
            this.onPlaybackFinishedListener.onPlaybackFinished(this);
        }
    }

    public void setOnPlaybackFinishedListener(OnPlaybackFinishedListener listener) {
        this.onPlaybackFinishedListener = listener;
    }
}
