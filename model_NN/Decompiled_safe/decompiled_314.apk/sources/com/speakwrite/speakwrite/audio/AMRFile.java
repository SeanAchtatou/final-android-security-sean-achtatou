package com.speakwrite.speakwrite.audio;

import org.apache.http.HttpStatus;

public class AMRFile {
    public static final String AMR_NB_HEADER = "#!AMR\n";
    public static final String AMR_WB_HEADER = "#!AMR-WB\n";
    public static final int[] FRAME_SIZE_NB = {95, 103, 118, 134, 148, 159, HttpStatus.SC_NO_CONTENT, 244};
    public static final int[] FRAME_SIZE_WB = {132, 177, 253, 285, 317, 365, 397, 461, 477};
    protected int frameSize;
    protected long length;

    public long getDuration() {
        return 20 * (this.length / ((long) this.frameSize));
    }

    public long getPosition(long time) {
        return ((time - (time % 20)) / 20) * ((long) this.frameSize);
    }

    public long getLength() {
        return this.length;
    }

    public void setLength(long length2) {
        this.length = length2;
    }

    public int getFrameSize() {
        return this.frameSize;
    }

    public void setFrameSize(int frameSize2) {
        this.frameSize = frameSize2;
    }
}
