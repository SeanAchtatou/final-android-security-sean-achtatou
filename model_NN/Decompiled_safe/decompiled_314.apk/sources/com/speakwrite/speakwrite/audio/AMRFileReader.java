package com.speakwrite.speakwrite.audio;

import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

public class AMRFileReader {
    private long dataOffset;
    protected RandomAccessFile input;

    public AMRFileReader(File file) throws FileNotFoundException {
        this(file.getAbsolutePath());
    }

    public AMRFileReader(String fileName) throws FileNotFoundException {
        this.input = new RandomAccessFile(fileName, "r");
    }

    public void close() throws IOException {
        this.input.close();
    }

    public AMRFile read() throws IOException, AMRException {
        AMRFile amrFile = new AMRFile();
        if (readChars(6).equals(AMRFile.AMR_NB_HEADER)) {
            this.dataOffset = 6;
            amrFile.setLength(this.input.length() - 6);
            int mode = (this.input.readByte() >> 3) & 15;
            if (mode < 0 || mode > 8) {
                throw new AMRException("Invalid format.");
            }
            int frameSize = ((AMRFile.FRAME_SIZE_NB[mode] + 7) / 8) + 1;
            amrFile.setFrameSize(frameSize);
            Log.d("AMRFileReader", "Frame size is: " + frameSize);
            this.input.seek(this.dataOffset);
            return amrFile;
        }
        throw new AMRException("Invalid header.");
    }

    private String readChars(int numChars) throws IOException {
        byte[] data = new byte[numChars];
        for (int b = 0; b < numChars; b++) {
            data[b] = this.input.readByte();
        }
        return new String(data, "ISO-8859-1");
    }

    public long getDataOffset() {
        return this.dataOffset;
    }

    public long getDataOffset(long position) {
        return getDataOffset() + getOffset(position);
    }

    public long getOffset(long position) {
        return position;
    }

    public FileChannel getChannel() {
        return this.input.getChannel();
    }

    public long copyDataTo(long position, long count, AMRFileWriter writer) throws IOException {
        return getChannel().transferTo(this.dataOffset + position, count, writer.getChannel());
    }
}
