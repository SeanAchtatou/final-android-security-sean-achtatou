package com.speakwrite.speakwrite.audio;

import com.speakwrite.speakwrite.SpeakWriteApplication;
import com.speakwrite.speakwrite.jobs.Job;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AMREditor {
    public static File tempFileDir = SpeakWriteApplication.getDataFolder();

    public static void insert(File file, File targetFile, long timeInMilliseconds) throws FileNotFoundException, IOException, AMRException {
        insert(file.getAbsolutePath(), targetFile.getAbsolutePath(), timeInMilliseconds);
    }

    /* JADX INFO: Multiple debug info for r21v1 com.speakwrite.speakwrite.audio.AMRFile: [D('fileName' java.lang.String), D('sourceFile' com.speakwrite.speakwrite.audio.AMRFile)] */
    public static void insert(String fileName, String targetFileName, long timeInMilliseconds) throws FileNotFoundException, IOException, AMRException {
        AMRFileReader aMRFileReader = new AMRFileReader(fileName);
        AMRFile sourceFile = aMRFileReader.read();
        long sourceLength = sourceFile.getLength();
        AMRFileReader aMRFileReader2 = new AMRFileReader(targetFileName);
        AMRFile targetFile = aMRFileReader2.read();
        long targetLength = targetFile.getLength();
        String tempFileName = createTempFile("insert");
        AMRFileWriter aMRFileWriter = new AMRFileWriter(tempFileName);
        aMRFileWriter.write(sourceFile);
        long position = targetFile.getPosition(timeInMilliseconds);
        copyData(aMRFileReader2, 0, position, aMRFileWriter);
        copyData(aMRFileReader, 0, sourceLength, aMRFileWriter);
        copyData(aMRFileReader2, position, targetLength - position, aMRFileWriter);
        aMRFileReader.close();
        aMRFileReader2.close();
        aMRFileWriter.close();
        renameTempFile(tempFileName, targetFileName);
    }

    public static void overwrite(File file, File targetFile, long timeInMilliseconds) throws FileNotFoundException, IOException, AMRException {
        overwrite(file.getAbsolutePath(), targetFile.getAbsolutePath(), timeInMilliseconds);
    }

    /* JADX INFO: Multiple debug info for r6v1 com.speakwrite.speakwrite.audio.AMRFile: [D('fileName' java.lang.String), D('sourceFile' com.speakwrite.speakwrite.audio.AMRFile)] */
    /* JADX INFO: Multiple debug info for r6v2 com.speakwrite.speakwrite.audio.AMRFileReader: [D('sourceFile' com.speakwrite.speakwrite.audio.AMRFile), D('target' com.speakwrite.speakwrite.audio.AMRFileReader)] */
    /* JADX INFO: Multiple debug info for r8v1 long: [D('position' long), D('timeInMilliseconds' long)] */
    /* JADX INFO: Multiple debug info for r8v2 long: [D('position' long), D('offset' long)] */
    public static void overwrite(String fileName, String targetFileName, long timeInMilliseconds) throws FileNotFoundException, IOException, AMRException {
        AMRFileReader source = new AMRFileReader(fileName);
        long sourceLength = source.read().getLength();
        AMRFileReader target = new AMRFileReader(targetFileName);
        long offset = target.getDataOffset(target.read().getPosition(timeInMilliseconds));
        target.close();
        AMRFileWriter destination = new AMRFileWriter(targetFileName, false);
        destination.seekToOffset(offset);
        copyData(source, 0, sourceLength, destination);
        source.close();
        destination.close();
    }

    public static void append(File file, File targetFile) throws FileNotFoundException, IOException, AMRException {
        append(file.getAbsolutePath(), targetFile.getAbsolutePath());
    }

    public static void append(String fileName, String targetFileName) throws FileNotFoundException, IOException, AMRException {
        AMRFileReader source = new AMRFileReader(fileName);
        long sourceLength = source.read().getLength();
        AMRFileWriter destination = new AMRFileWriter(targetFileName, false);
        destination.seekToEnd();
        copyData(source, 0, sourceLength, destination);
        source.close();
        destination.close();
    }

    public static void eraseToEnd(File file, long timeInMilliseconds) throws IOException, AMRException {
        eraseToEnd(file.getAbsolutePath(), timeInMilliseconds);
    }

    public static void eraseToEnd(String fileName, long timeInMilliseconds) throws IOException, AMRException {
        AMRFileReader source = new AMRFileReader(fileName);
        long dataSize = source.getOffset(source.read().getPosition(timeInMilliseconds));
        long fileSize = source.getDataOffset() + dataSize;
        source.close();
        if (dataSize == 0) {
            new File(fileName).delete();
            return;
        }
        AMRFileWriter destination = new AMRFileWriter(fileName, false);
        destination.setFileSize(fileSize);
        destination.close();
    }

    private static long copyData(AMRFileReader source, long position, long count, AMRFileWriter destination) throws IOException {
        return source.copyDataTo(position, count, destination);
    }

    private static String createTempFile(String fileName) throws IOException {
        return File.createTempFile(fileName, Job.SOUND_EXTENTION, tempFileDir).getAbsolutePath();
    }

    private static void renameTempFile(String tempFileName, String targetFileName) {
        File tempFile = new File(tempFileName);
        File targetFile = new File(targetFileName);
        targetFile.delete();
        tempFile.renameTo(targetFile);
    }
}
