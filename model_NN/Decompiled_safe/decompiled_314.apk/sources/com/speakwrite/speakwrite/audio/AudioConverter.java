package com.speakwrite.speakwrite.audio;

import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class AudioConverter {
    /* JADX INFO: Multiple debug info for r19v3 byte[]: [D('header' byte[]), D('outputFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r7v1 java.nio.channels.FileChannel: [D('outputChannel' java.nio.channels.FileChannel), D('fileOutput' java.io.FileOutputStream)] */
    public static boolean convert3GPToAMR(File inputFile, File outputFile) {
        boolean success;
        long size;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            DataOutputStream output = new DataOutputStream(fileOutputStream);
            output.write(new byte[]{35, 33, 65, 77, 82, 10});
            FileInputStream fileInputStream = new FileInputStream(inputFile);
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);
            long length = inputFile.length();
            long offset = 0;
            long type = 0;
            while (true) {
                if (offset > length - 8) {
                    success = false;
                    break;
                }
                long size2 = (long) dataInputStream.readInt();
                type = (long) dataInputStream.readInt();
                long offset2 = offset + 8;
                if (size2 == 1) {
                    offset2 += 8;
                    size = dataInputStream.readLong() - 16;
                } else if (size2 == 0) {
                    size = length - offset2;
                } else {
                    size = size2 - 8;
                }
                if (type == 1835295092) {
                    Log.i("AudioConverter", "Found mdat at offset " + offset2);
                    fileInputStream.getChannel().transferTo(offset2, size, fileOutputStream.getChannel());
                    success = true;
                    break;
                }
                Log.i("AudioConverter", "Skipping " + size + " bytes.");
                offset = offset2 + size;
                dataInputStream.skip(size);
            }
            try {
                dataInputStream.close();
                output.close();
                return success;
            } catch (Exception e) {
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public static boolean convert3GPToAMR(String inputFileName, String outputFileName) {
        return convert3GPToAMR(new File(inputFileName), new File(outputFileName));
    }
}
