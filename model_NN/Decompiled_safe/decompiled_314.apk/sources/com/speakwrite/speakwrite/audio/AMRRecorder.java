package com.speakwrite.speakwrite.audio;

import android.media.MediaRecorder;
import java.io.File;
import java.io.IOException;

public class AMRRecorder {
    private int audioSource;
    private MediaRecorder recorder = new MediaRecorder();
    private boolean recording = false;

    public static AMRRecorder getRecorder() {
        return new AMRRecorder(1);
    }

    public AMRRecorder(int audioSource2) {
        this.audioSource = audioSource2;
    }

    public void setOutputFile(File file) throws IOException {
        setOutputFile(file.getAbsolutePath());
    }

    public void setOutputFile(String fileName) throws IOException {
        if (!isRecording()) {
            this.recorder.reset();
            this.recorder.setAudioSource(this.audioSource);
            this.recorder.setOutputFormat(3);
            this.recorder.setAudioEncoder(1);
            this.recorder.setOutputFile(fileName);
            this.recorder.prepare();
        }
    }

    public void startRecording() {
        if (!isRecording()) {
            this.recording = true;
            this.recorder.start();
        }
    }

    public void stopRecording() {
        if (isRecording()) {
            this.recorder.stop();
            this.recording = false;
        }
    }

    public boolean isRecording() {
        return this.recording && this.recorder != null;
    }

    public int getMaxAmplitude() {
        if (this.recorder != null) {
            return this.recorder.getMaxAmplitude();
        }
        return 0;
    }

    public void release() {
        stopRecording();
        this.recorder.release();
        this.recorder = null;
        this.recording = false;
    }
}
