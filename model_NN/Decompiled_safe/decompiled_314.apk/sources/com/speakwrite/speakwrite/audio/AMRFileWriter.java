package com.speakwrite.speakwrite.audio;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

public class AMRFileWriter {
    protected final RandomAccessFile output;

    public AMRFileWriter(File file) throws IOException {
        this(file.getAbsolutePath());
    }

    public AMRFileWriter(String fileName) throws IOException {
        this(fileName, true);
    }

    public AMRFileWriter(String fileName, boolean overwrite) throws IOException {
        this.output = new RandomAccessFile(fileName, "rw");
        if (overwrite) {
            this.output.setLength(0);
        }
    }

    public void close() throws IOException {
        this.output.close();
    }

    public void write(AMRFile wave) throws IOException {
        writeChars(AMRFile.AMR_NB_HEADER);
    }

    private void writeChars(String data) throws IOException {
        this.output.write(data.getBytes("ISO-8859-1"));
    }

    public void seekToEnd() throws IOException {
        this.output.seek(this.output.length());
    }

    public void seekToOffset(long offset) throws IOException {
        this.output.seek(offset);
    }

    public void setFileSize(long size) throws IOException {
        this.output.setLength(size);
    }

    public FileChannel getChannel() {
        return this.output.getChannel();
    }
}
