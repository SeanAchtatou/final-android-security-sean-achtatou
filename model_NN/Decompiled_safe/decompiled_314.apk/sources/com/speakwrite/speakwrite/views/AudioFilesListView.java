package com.speakwrite.speakwrite.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import com.speakwrite.speakwrite.R;
import com.speakwrite.speakwrite.jobs.Job;
import com.speakwrite.speakwrite.jobs.JobList;
import com.speakwrite.speakwrite.utils.DateUtils;
import com.speakwrite.speakwrite.utils.TimeUtils;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import org.apache.commons.lang.SystemUtils;

public class AudioFilesListView extends ExpandableListView {
    private static final int STATE_BOTTOM = 2;
    private static final int STATE_NONE = 0;
    private static final int STATE_SINGLE = 3;
    private static final int STATE_TOP = 1;
    private static final int s_borderRound = 15;
    private static final int s_borderWidth = 1;
    private AudioFilesListAdapter mAdapter;
    /* access modifiers changed from: private */
    public int mBackgroundColor = -1;
    /* access modifiers changed from: private */
    public int mBackgroundPressedColor = Color.rgb(20, 75, 255);
    /* access modifiers changed from: private */
    public int mBackgroundSelectedColor = Color.rgb(150, 174, 255);
    /* access modifiers changed from: private */
    public int mBorderColor = Color.rgb(172, 178, 183);

    private interface IGetState {
        int getState(Job job);
    }

    public AudioFilesListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    public AudioFilesListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public AudioFilesListView(Context context) {
        super(context);
        initialize();
    }

    public boolean performItemClick(View v, int position, long id) {
        if (this.mAdapter.isGroup(position)) {
            return false;
        }
        return super.performItemClick(v, position, id);
    }

    public void setJobList(JobList list) {
        this.mAdapter.setJobList(list);
        if (this.mAdapter.getGroupCount() > 0) {
            for (int i = 0; i < this.mAdapter.getGroupCount(); i++) {
                expandGroup(i);
            }
            setSelection(0);
        }
    }

    public Job getJob(int groupPosition, int childPosition) {
        return (Job) this.mAdapter.getChild(groupPosition, childPosition);
    }

    private void initialize() {
        this.mBorderColor = getResources().getColor(R.color.audio_file_item_border_color);
        this.mBackgroundColor = getResources().getColor(R.color.audio_file_item_background_color);
        this.mBackgroundPressedColor = getResources().getColor(R.color.audio_file_item_background_pressed_color);
        this.mBackgroundSelectedColor = getResources().getColor(R.color.audio_file_item_background_selected_color);
        setGroupIndicator(null);
        setSelector(new EmptyDrawable());
        setDividerHeight(0);
        this.mAdapter = new AudioFilesListAdapter();
        setAdapter(this.mAdapter);
    }

    private class AudioFilesListAdapter extends BaseExpandableListAdapter implements IGetState {
        private static final String s_nameOlder = "Older";
        private static final String s_nameToday = "Today";
        private static final String s_nameYesterday = "Yesterday";
        private static final int s_older_index = 2;
        private static final int s_today_index = 0;
        private static final int s_yesterday_index = 1;
        private LinkedList<String> mGroup = new LinkedList<>();
        private final Comparator<Job> mJobComparator = new Comparator<Job>() {
            public int compare(Job job1, Job job2) {
                return -getJobData(job1).compareTo(getJobData(job2));
            }

            private Calendar getJobData(Job job) {
                Calendar jobDate = job.status == 1 ? job.submitDate : job.modificationDate;
                if (jobDate == null) {
                    return DateUtils.getToday();
                }
                return jobDate;
            }
        };
        private LinkedList<Job> mOlderList = new LinkedList<>();
        private LinkedList<Job> mTodayList = new LinkedList<>();
        private LinkedList<Job> mYesterdayList = new LinkedList<>();

        public AudioFilesListAdapter() {
        }

        public void setJobList(JobList list) {
            clear();
            if (list != null && list.size() > 0) {
                DateUtils util = new DateUtils();
                for (int i = 0; i < list.size(); i++) {
                    Job item = (Job) list.get(i);
                    if (util.isToday(item.modificationDate)) {
                        this.mTodayList.addLast(item);
                    } else if (util.isYesterday(item.modificationDate)) {
                        this.mYesterdayList.addLast(item);
                    } else {
                        this.mOlderList.addLast(item);
                    }
                }
                if (this.mTodayList.size() == 0) {
                    this.mGroup.remove(s_nameToday);
                }
                if (this.mYesterdayList.size() == 0) {
                    this.mGroup.remove(s_nameYesterday);
                }
                if (this.mOlderList.size() == 0) {
                    this.mGroup.remove(s_nameOlder);
                }
                sort(this.mTodayList);
                sort(this.mTodayList);
                sort(this.mOlderList);
                notifyDataSetChanged();
            }
        }

        public boolean isGroup(int position) {
            if (position == 0) {
                return true;
            }
            int size = 0;
            for (int i = 0; i < this.mGroup.size(); i++) {
                size += getList(this.mGroup.get(i)).size() + 1;
                if (position == size) {
                    return true;
                }
            }
            return false;
        }

        public int getState(Job tag) {
            if (tag == null) {
                return 0;
            }
            if ((this.mTodayList.size() == 1 && this.mTodayList.contains(tag)) || ((this.mYesterdayList.size() == 1 && this.mYesterdayList.contains(tag)) || (this.mOlderList.size() == 1 && this.mOlderList.contains(tag)))) {
                return 3;
            }
            if ((!this.mTodayList.isEmpty() && tag == this.mTodayList.getFirst()) || ((!this.mYesterdayList.isEmpty() && tag == this.mYesterdayList.getFirst()) || (!this.mOlderList.isEmpty() && tag == this.mOlderList.getFirst()))) {
                return 1;
            }
            if ((this.mTodayList.isEmpty() || tag != this.mTodayList.getLast()) && ((this.mYesterdayList.isEmpty() || tag != this.mYesterdayList.getLast()) && (this.mOlderList.isEmpty() || tag != this.mOlderList.getLast()))) {
                return 0;
            }
            return 2;
        }

        public Object getChild(int groupPosition, int childPosition) {
            switch (getGroupIndex(groupPosition)) {
                case 0:
                    return this.mTodayList.get(childPosition);
                case 1:
                    return this.mYesterdayList.get(childPosition);
                case 2:
                    return this.mOlderList.get(childPosition);
                default:
                    return null;
            }
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            switch (getGroupIndex(groupPosition)) {
                case 0:
                    return this.mTodayList.size();
                case 1:
                    return this.mYesterdayList.size();
                case 2:
                    return this.mOlderList.size();
                default:
                    return 0;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(AudioFilesListView.this.getContext()).inflate((int) R.layout.job_list_item, parent, false);
            }
            StateListDrawable listDrawable = new StateListDrawable();
            Job job = (Job) getChild(groupPosition, childPosition);
            DateUtils util = new DateUtils();
            listDrawable.addState(AudioFilesListView.PRESSED_ENABLED_STATE_SET, new JobItemDrawable(AudioFilesListView.this.mBorderColor, AudioFilesListView.this.mBackgroundPressedColor, job, this));
            listDrawable.addState(AudioFilesListView.SELECTED_STATE_SET, new JobItemDrawable(AudioFilesListView.this.mBorderColor, AudioFilesListView.this.mBackgroundSelectedColor, job, this));
            listDrawable.addState(AudioFilesListView.ENABLED_STATE_SET, new JobItemDrawable(AudioFilesListView.this.mBorderColor, AudioFilesListView.this.mBackgroundColor, job, this));
            convertView.findViewById(R.id.job_list_item).setBackgroundDrawable(listDrawable);
            ((TextView) convertView.findViewById(R.id.title)).setText(job.name);
            TextView tv = (TextView) convertView.findViewById(R.id.status);
            tv.setText(job.getStatusString());
            if (job.status == 1) {
                tv.setTextAppearance(AudioFilesListView.this.getContext(), R.style.audio_file_item_submited);
            } else if (job.status == 0) {
                tv.setTextAppearance(AudioFilesListView.this.getContext(), R.style.audio_file_item_unsubmited);
            }
            TextView tv2 = (TextView) convertView.findViewById(R.id.date);
            if (job.status == 1) {
                if (util.isToday(job.submitDate) || util.isYesterday(job.submitDate)) {
                    tv2.setText(util.dateToString(job.submitDate, AudioFilesListView.this.getContext().getString(R.string.time_format)));
                } else {
                    tv2.setText(util.dateToString(job.submitDate, AudioFilesListView.this.getContext().getString(R.string.short_date_format)));
                }
            } else if (job.status == 0) {
                if (util.isToday(job.modificationDate) || util.isYesterday(job.modificationDate)) {
                    tv2.setText(util.dateToString(job.modificationDate, AudioFilesListView.this.getContext().getString(R.string.time_format)));
                } else {
                    tv2.setText(util.dateToString(job.modificationDate, AudioFilesListView.this.getContext().getString(R.string.short_date_format)));
                }
            }
            TextView tv3 = (TextView) convertView.findViewById(R.id.time);
            if (job.status == 1) {
                tv3.setText(job.customerJobNumber);
            } else if (job.status == 0) {
                tv3.setText(TimeUtils.millisToString(job.duration));
            }
            return convertView;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(AudioFilesListView.this.getContext()).inflate((int) R.layout.job_group_list_item, parent, false);
            }
            ((TextView) convertView.findViewById(R.id.group_name)).setText(getGroup(groupPosition).toString());
            return convertView;
        }

        public Object getGroup(int groupPosition) {
            return this.mGroup.get(groupPosition);
        }

        public int getGroupCount() {
            return this.mGroup.size();
        }

        public long getGroupId(int groupPosition) {
            return (long) getGroupIndex(groupPosition);
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public boolean hasStableIds() {
            return true;
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        private int getGroupIndex(int groupPosition) {
            if (this.mGroup.get(groupPosition).equals(s_nameToday)) {
                return 0;
            }
            if (this.mGroup.get(groupPosition).equals(s_nameYesterday)) {
                return 1;
            }
            if (this.mGroup.get(groupPosition).equals(s_nameOlder)) {
                return 2;
            }
            return -1;
        }

        private LinkedList<Job> getList(String group) {
            if (group.equals(s_nameToday)) {
                return this.mTodayList;
            }
            if (group.equals(s_nameYesterday)) {
                return this.mYesterdayList;
            }
            if (group.equals(s_nameOlder)) {
                return this.mOlderList;
            }
            return null;
        }

        private void sort(LinkedList<Job> list) {
            if (list != null && list.size() > 0) {
                Collections.sort(this.mTodayList, this.mJobComparator);
            }
        }

        private void clear() {
            if (this.mGroup != null) {
                this.mGroup.clear();
                this.mGroup.addLast(s_nameToday);
                this.mGroup.addLast(s_nameYesterday);
                this.mGroup.addLast(s_nameOlder);
            }
            if (this.mTodayList != null) {
                this.mTodayList.clear();
            }
            if (this.mYesterdayList != null) {
                this.mYesterdayList.clear();
            }
            if (this.mOlderList != null) {
                this.mOlderList.clear();
            }
        }
    }

    private class JobItemDrawable extends Drawable {
        private int mBackgroundColor;
        private Paint mBackgroundPaint;
        private Path mBackgroundPath;
        private int mBorderColor;
        private Paint mBorderPaint = new Paint();
        private Path mBorderPath;
        private IGetState mState;
        private Job mTag;

        public JobItemDrawable(int borderColor, int backgroundColor, Job tag, IGetState state) {
            this.mBorderColor = borderColor;
            this.mBackgroundColor = backgroundColor;
            this.mTag = tag;
            this.mState = state;
            this.mBorderPaint.setColor(this.mBorderColor);
            this.mBorderPaint.setAntiAlias(true);
            this.mBackgroundPaint = new Paint();
            this.mBackgroundPaint.setColor(this.mBackgroundColor);
            this.mBackgroundPaint.setAntiAlias(true);
        }

        /* access modifiers changed from: protected */
        public void onBoundsChange(Rect bounds) {
            this.mBorderPath = null;
            this.mBackgroundPath = null;
            super.onBoundsChange(bounds);
        }

        public void draw(Canvas canvas) {
            int state = this.mState.getState(this.mTag);
            canvas.drawPath(getBorderPath(state), this.mBorderPaint);
            canvas.drawPath(getBackgroundPath(state), this.mBackgroundPaint);
        }

        public int getOpacity() {
            return 0;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }

        private Path getBorderPath(int state) {
            if (this.mBorderPath == null) {
                RectF r = new RectF(getBounds());
                switch (state) {
                    case 0:
                        this.mBorderPath = getNonePath(r);
                        break;
                    case 1:
                        this.mBorderPath = getTopPath(r);
                        break;
                    case 2:
                        this.mBorderPath = getBottomPath(r);
                        break;
                    case 3:
                        this.mBorderPath = getSinglePath(r);
                        break;
                }
            }
            return this.mBorderPath;
        }

        private Path getBackgroundPath(int state) {
            if (this.mBackgroundPath == null) {
                Rect bound = getBounds();
                switch (state) {
                    case 0:
                        this.mBackgroundPath = getNonePath(new RectF((float) (bound.left + 1), (float) bound.top, (float) (bound.right - 1), (float) (bound.bottom - 1)));
                        break;
                    case 1:
                        this.mBackgroundPath = getTopPath(new RectF((float) (bound.left + 1), (float) (bound.top + 1), (float) (bound.right - 1), (float) (bound.bottom - 1)));
                        break;
                    case 2:
                        this.mBackgroundPath = getBottomPath(new RectF((float) (bound.left + 1), (float) bound.top, (float) (bound.right - 1), (float) (bound.bottom - 1)));
                        break;
                    case 3:
                        this.mBackgroundPath = getSinglePath(new RectF((float) (bound.left + 1), (float) (bound.top + 1), (float) (bound.right - 1), (float) (bound.bottom - 1)));
                        break;
                }
            }
            return this.mBackgroundPath;
        }

        private Path getTopPath(RectF rect) {
            Path path = new Path();
            path.moveTo(rect.left, rect.bottom);
            path.lineTo(rect.left, rect.top + 15.0f);
            path.arcTo(new RectF(rect.left, rect.top, rect.left + 15.0f, rect.top + 15.0f), -180.0f, 90.0f);
            path.lineTo(rect.left + 15.0f, rect.top);
            path.lineTo(rect.right - 15.0f, rect.top);
            path.arcTo(new RectF(rect.right - 15.0f, rect.top, rect.right, rect.top + 15.0f), -90.0f, 90.0f);
            path.lineTo(rect.right, rect.top + 15.0f);
            path.lineTo(rect.right, rect.bottom);
            return path;
        }

        private Path getBottomPath(RectF rect) {
            Path path = new Path();
            path.moveTo(rect.right, rect.top);
            path.lineTo(rect.right, rect.top);
            path.lineTo(rect.right, rect.bottom - 15.0f);
            path.arcTo(new RectF(rect.right - 15.0f, rect.bottom - 15.0f, rect.right, rect.bottom), SystemUtils.JAVA_VERSION_FLOAT, 90.0f);
            path.lineTo(rect.right - 15.0f, rect.bottom);
            path.lineTo(rect.left + 15.0f, rect.bottom);
            path.arcTo(new RectF(rect.left, rect.bottom - 15.0f, rect.left + 15.0f, rect.bottom), 90.0f, 90.0f);
            path.lineTo(rect.left, rect.top);
            return path;
        }

        private Path getSinglePath(RectF rect) {
            Path path = new Path();
            path.moveTo(rect.left, rect.bottom - 15.0f);
            path.lineTo(rect.left, rect.top + 15.0f);
            path.arcTo(new RectF(rect.left, rect.top, rect.left + 15.0f, rect.top + 15.0f), -180.0f, 90.0f);
            path.lineTo(rect.left + 15.0f, rect.top);
            path.lineTo(rect.right - 15.0f, rect.top);
            path.arcTo(new RectF(rect.right - 15.0f, rect.top, rect.right, rect.top + 15.0f), -90.0f, 90.0f);
            path.lineTo(rect.right, rect.top + 15.0f);
            path.lineTo(rect.right, rect.bottom - 15.0f);
            path.arcTo(new RectF(rect.right - 15.0f, rect.bottom - 15.0f, rect.right, rect.bottom), SystemUtils.JAVA_VERSION_FLOAT, 90.0f);
            path.lineTo(rect.right - 15.0f, rect.bottom);
            path.lineTo(rect.left + 15.0f, rect.bottom);
            path.arcTo(new RectF(rect.left, rect.bottom - 15.0f, rect.left + 15.0f, rect.bottom), 90.0f, 90.0f);
            return path;
        }

        private Path getNonePath(RectF rect) {
            Path path = new Path();
            path.moveTo(rect.left, rect.top);
            path.lineTo(rect.right, rect.top);
            path.lineTo(rect.right, rect.bottom);
            path.lineTo(rect.left, rect.bottom);
            return path;
        }
    }

    private class EmptyDrawable extends Drawable {
        private EmptyDrawable() {
        }

        public void draw(Canvas canvas) {
        }

        public int getOpacity() {
            return 0;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }
    }
}
