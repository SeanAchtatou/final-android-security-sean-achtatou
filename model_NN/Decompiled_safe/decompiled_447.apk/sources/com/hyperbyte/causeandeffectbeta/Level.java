package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.Timer;
import java.util.TimerTask;

public class Level extends Activity implements View.OnClickListener {
    public static final String PREFERENCE_FILENAME = "AppGamePrefs";
    public static int exit = 0;
    public static int level = 0;
    public static int pack = 0;
    private MediaPlayer mp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView((int) R.layout.challenge);
        game.ref3 = 1;
        setButtons();
        level = 0;
        game.world = 1;
        pack = game.world;
        setTitle("Blank Slate");
        final Handler handler = new Handler();
        new Timer().schedule(new TimerTask() {
            public void run() {
                if (Level.exit == 1) {
                    Level.exit = 0;
                    cancel();
                    Level.this.finish();
                }
                if (game.exit == 3) {
                    game.exit = 0;
                    cancel();
                    Level.this.finish();
                }
                handler.post(new Runnable() {
                    public void run() {
                        if (game.ref3 == 1) {
                            LinearLayout r2 = (LinearLayout) Level.this.findViewById(R.id.myLayout2);
                            Level.this.setButtons();
                            if (game.world == 1) {
                                Level.this.setTitle("Blank Slate");
                                r2.setBackgroundResource(R.drawable.slate);
                            }
                            if (game.world == 5) {
                                Level.this.setTitle("World 5");
                                r2.setBackgroundResource(R.drawable.slate);
                            }
                            game.ref3 = 0;
                        }
                        if (Pause.gomainmenu == 2) {
                            Pause.gomainmenu = 3;
                            Level.exit = 1;
                            game.ref = 1;
                        }
                    }
                });
            }
        }, 1, 500);
    }

    public void setButtons() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        SharedPreferences.Editor edit = gameSettings.edit();
        Button c1 = (Button) findViewById(R.id.c1);
        c1.setOnClickListener(this);
        Button c2 = (Button) findViewById(R.id.c2);
        c2.setOnClickListener(this);
        Button c3 = (Button) findViewById(R.id.c3);
        c3.setOnClickListener(this);
        Button c4 = (Button) findViewById(R.id.c4);
        c4.setOnClickListener(this);
        Button c5 = (Button) findViewById(R.id.c5);
        c5.setOnClickListener(this);
        Button c6 = (Button) findViewById(R.id.c6);
        c6.setOnClickListener(this);
        Button c7 = (Button) findViewById(R.id.c7);
        c7.setOnClickListener(this);
        Button c8 = (Button) findViewById(R.id.c8);
        c8.setOnClickListener(this);
        Button c9 = (Button) findViewById(R.id.c9);
        c9.setOnClickListener(this);
        Button c10 = (Button) findViewById(R.id.c10);
        c10.setOnClickListener(this);
        Button c11 = (Button) findViewById(R.id.c11);
        c11.setOnClickListener(this);
        Button c12 = (Button) findViewById(R.id.c12);
        c12.setOnClickListener(this);
        Button c13 = (Button) findViewById(R.id.c13);
        c13.setOnClickListener(this);
        Button c14 = (Button) findViewById(R.id.c14);
        c14.setOnClickListener(this);
        Button c15 = (Button) findViewById(R.id.c15);
        c15.setOnClickListener(this);
        Button c16 = (Button) findViewById(R.id.c16);
        c16.setOnClickListener(this);
        Button c17 = (Button) findViewById(R.id.c17);
        c17.setOnClickListener(this);
        Button c18 = (Button) findViewById(R.id.c18);
        c18.setOnClickListener(this);
        Button c19 = (Button) findViewById(R.id.c19);
        c19.setOnClickListener(this);
        Button c20 = (Button) findViewById(R.id.c20);
        c20.setOnClickListener(this);
        ((Button) findViewById(R.id.upgrade)).setOnClickListener(this);
        if (pack == 1) {
            if (gameSettings.getBoolean("p1_l1", true)) {
                c1.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l2", false)) {
                c2.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l3", false)) {
                c3.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l4", false)) {
                c4.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l5", false)) {
                c5.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l6", false)) {
                c6.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l7", false)) {
                c7.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l8", false)) {
                c8.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l9", false)) {
                c9.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l10", false)) {
                c10.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l11", false)) {
                c11.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l12", false)) {
                c12.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l13", false)) {
                c13.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l14", false)) {
                c14.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l15", false)) {
                c15.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l16", false)) {
                c16.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l17", false)) {
                c17.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l18", false)) {
                c18.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l19", false)) {
                c19.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p1_l20", false)) {
                c20.setBackgroundResource(R.drawable.cell1);
            }
        }
        if (pack == 1) {
            if (!gameSettings.getBoolean("p1_l1", true)) {
                c1.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l2", false)) {
                c2.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l3", false)) {
                c3.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l4", false)) {
                c4.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l5", false)) {
                c5.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l6", false)) {
                c6.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l7", false)) {
                c7.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l8", false)) {
                c8.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l9", false)) {
                c9.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l10", false)) {
                c10.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l11", false)) {
                c11.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l12", false)) {
                c12.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l13", false)) {
                c13.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l14", false)) {
                c14.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l15", false)) {
                c15.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l16", false)) {
                c16.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l17", false)) {
                c17.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l18", false)) {
                c18.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l19", false)) {
                c19.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p1_l20", false)) {
                c20.setBackgroundResource(R.drawable.cellu);
            }
        }
        if (pack == 2) {
            if (gameSettings.getBoolean("p2_l1", true)) {
                c1.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l2", false)) {
                c2.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l3", false)) {
                c3.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l4", false)) {
                c4.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l5", false)) {
                c5.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l6", false)) {
                c6.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l7", false)) {
                c7.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l8", false)) {
                c8.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l9", false)) {
                c9.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l10", false)) {
                c10.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l11", false)) {
                c11.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l12", false)) {
                c12.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l13", false)) {
                c13.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l14", false)) {
                c14.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l15", false)) {
                c15.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l16", false)) {
                c16.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l17", false)) {
                c17.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l18", false)) {
                c18.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l19", false)) {
                c19.setBackgroundResource(R.drawable.cell1);
            }
            if (gameSettings.getBoolean("p2_l20", false)) {
                c20.setBackgroundResource(R.drawable.cell1);
            }
        }
        if (pack == 2) {
            if (!gameSettings.getBoolean("p2_l1", true)) {
                c1.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l2", false)) {
                c2.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l3", false)) {
                c3.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l4", false)) {
                c4.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l5", false)) {
                c5.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l6", false)) {
                c6.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l7", false)) {
                c7.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l8", false)) {
                c8.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l9", false)) {
                c9.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l10", false)) {
                c10.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l11", false)) {
                c11.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l12", false)) {
                c12.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l13", false)) {
                c13.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l14", false)) {
                c14.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l15", false)) {
                c15.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l16", false)) {
                c16.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l17", false)) {
                c17.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l18", false)) {
                c18.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l19", false)) {
                c19.setBackgroundResource(R.drawable.cellu);
            }
            if (!gameSettings.getBoolean("p2_l20", false)) {
                c20.setBackgroundResource(R.drawable.cellu);
            }
        }
    }

    public void onClick(View v) {
        Button c1 = (Button) findViewById(R.id.c1);
        c1.setOnClickListener(this);
        Button c2 = (Button) findViewById(R.id.c2);
        c2.setOnClickListener(this);
        Button c3 = (Button) findViewById(R.id.c3);
        c3.setOnClickListener(this);
        Button c4 = (Button) findViewById(R.id.c4);
        c4.setOnClickListener(this);
        Button c5 = (Button) findViewById(R.id.c5);
        c5.setOnClickListener(this);
        Button c6 = (Button) findViewById(R.id.c6);
        c6.setOnClickListener(this);
        Button c7 = (Button) findViewById(R.id.c7);
        c7.setOnClickListener(this);
        Button c8 = (Button) findViewById(R.id.c8);
        c8.setOnClickListener(this);
        Button c9 = (Button) findViewById(R.id.c9);
        c9.setOnClickListener(this);
        Button c10 = (Button) findViewById(R.id.c10);
        c10.setOnClickListener(this);
        Button c11 = (Button) findViewById(R.id.c11);
        c11.setOnClickListener(this);
        Button c12 = (Button) findViewById(R.id.c12);
        c12.setOnClickListener(this);
        Button c13 = (Button) findViewById(R.id.c13);
        c13.setOnClickListener(this);
        Button c14 = (Button) findViewById(R.id.c14);
        c14.setOnClickListener(this);
        Button c15 = (Button) findViewById(R.id.c15);
        c15.setOnClickListener(this);
        Button c16 = (Button) findViewById(R.id.c16);
        c16.setOnClickListener(this);
        Button c17 = (Button) findViewById(R.id.c17);
        c17.setOnClickListener(this);
        Button c18 = (Button) findViewById(R.id.c18);
        c18.setOnClickListener(this);
        Button c19 = (Button) findViewById(R.id.c19);
        c19.setOnClickListener(this);
        Button c20 = (Button) findViewById(R.id.c20);
        c20.setOnClickListener(this);
        ((Button) findViewById(R.id.upgrade)).setOnClickListener(this);
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        SharedPreferences.Editor edit = gameSettings.edit();
        Intent intent = new Intent(this, game.class);
        switch (v.getId()) {
            case R.id.c1 /*2131099650*/:
                if ((gameSettings.getBoolean("p1_l1", true) && pack == 1) || ((gameSettings.getBoolean("p2_l1", true) && pack == 2) || ((gameSettings.getBoolean("p3_l1", false) && pack == 3) || ((gameSettings.getBoolean("p4_l1", false) && pack == 4) || (gameSettings.getBoolean("p5_l1", false) && pack == 5))))) {
                    c1.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 1;
                    startActivity(intent);
                    break;
                }
            case R.id.c2 /*2131099651*/:
                if ((gameSettings.getBoolean("p1_l2", false) && pack == 1) || ((gameSettings.getBoolean("p2_l2", false) && pack == 2) || ((gameSettings.getBoolean("p3_l2", false) && pack == 3) || ((gameSettings.getBoolean("p4_l2", false) && pack == 4) || (gameSettings.getBoolean("p5_l2", false) && pack == 5))))) {
                    c2.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 2;
                    startActivity(intent);
                    break;
                }
            case R.id.c3 /*2131099652*/:
                if ((gameSettings.getBoolean("p1_l3", false) && pack == 1) || ((gameSettings.getBoolean("p2_l3", false) && pack == 2) || ((gameSettings.getBoolean("p3_l3", false) && pack == 3) || ((gameSettings.getBoolean("p4_l3", false) && pack == 4) || (gameSettings.getBoolean("p5_l3", false) && pack == 5))))) {
                    c3.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 3;
                    startActivity(intent);
                    break;
                }
            case R.id.c4 /*2131099653*/:
                if ((gameSettings.getBoolean("p1_l4", false) && pack == 1) || ((gameSettings.getBoolean("p2_l4", false) && pack == 2) || ((gameSettings.getBoolean("p3_l4", false) && pack == 3) || ((gameSettings.getBoolean("p4_l4", false) && pack == 4) || (gameSettings.getBoolean("p5_l4", false) && pack == 5))))) {
                    c4.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 4;
                    startActivity(intent);
                    break;
                }
            case R.id.c5 /*2131099654*/:
                if ((gameSettings.getBoolean("p1_l5", false) && pack == 1) || ((gameSettings.getBoolean("p2_l5", false) && pack == 2) || ((gameSettings.getBoolean("p3_l5", false) && pack == 3) || ((gameSettings.getBoolean("p4_l5", false) && pack == 4) || (gameSettings.getBoolean("p5_l5", false) && pack == 5))))) {
                    c5.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 5;
                    startActivity(intent);
                    break;
                }
            case R.id.c6 /*2131099655*/:
                if ((gameSettings.getBoolean("p1_l6", false) && pack == 1) || ((gameSettings.getBoolean("p2_l6", false) && pack == 2) || ((gameSettings.getBoolean("p3_l6", false) && pack == 3) || ((gameSettings.getBoolean("p4_l6", false) && pack == 4) || (gameSettings.getBoolean("p5_l6", false) && pack == 5))))) {
                    c6.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 6;
                    startActivity(intent);
                    break;
                }
            case R.id.c7 /*2131099656*/:
                if ((gameSettings.getBoolean("p1_l7", false) && pack == 1) || ((gameSettings.getBoolean("p2_l7", false) && pack == 2) || ((gameSettings.getBoolean("p3_l7", false) && pack == 3) || ((gameSettings.getBoolean("p4_l7", false) && pack == 4) || (gameSettings.getBoolean("p5_l7", false) && pack == 5))))) {
                    c7.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 7;
                    startActivity(intent);
                    break;
                }
            case R.id.c8 /*2131099657*/:
                if ((gameSettings.getBoolean("p1_l8", false) && pack == 1) || ((gameSettings.getBoolean("p2_l8", false) && pack == 2) || ((gameSettings.getBoolean("p3_l8", false) && pack == 3) || ((gameSettings.getBoolean("p4_l8", false) && pack == 4) || (gameSettings.getBoolean("p5_l8", false) && pack == 5))))) {
                    c8.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 8;
                    startActivity(intent);
                    break;
                }
            case R.id.c9 /*2131099658*/:
                if ((gameSettings.getBoolean("p1_l9", false) && pack == 1) || ((gameSettings.getBoolean("p2_l9", false) && pack == 2) || ((gameSettings.getBoolean("p3_l9", false) && pack == 3) || ((gameSettings.getBoolean("p4_l9", false) && pack == 4) || (gameSettings.getBoolean("p5_l9", false) && pack == 5))))) {
                    c9.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 9;
                    startActivity(intent);
                    break;
                }
            case R.id.c10 /*2131099659*/:
                if ((gameSettings.getBoolean("p1_l10", false) && pack == 1) || ((gameSettings.getBoolean("p2_l10", false) && pack == 2) || ((gameSettings.getBoolean("p3_l10", false) && pack == 3) || ((gameSettings.getBoolean("p4_l10", false) && pack == 4) || (gameSettings.getBoolean("p5_l10", false) && pack == 5))))) {
                    c10.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 10;
                    startActivity(intent);
                    break;
                }
            case R.id.c11 /*2131099660*/:
                if ((gameSettings.getBoolean("p1_l11", false) && pack == 1) || ((gameSettings.getBoolean("p2_l11", false) && pack == 2) || ((gameSettings.getBoolean("p3_l11", false) && pack == 3) || ((gameSettings.getBoolean("p4_l11", false) && pack == 4) || (gameSettings.getBoolean("p5_l11", false) && pack == 5))))) {
                    c11.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 11;
                    startActivity(intent);
                    break;
                }
            case R.id.c12 /*2131099661*/:
                if ((gameSettings.getBoolean("p1_l12", false) && pack == 1) || ((gameSettings.getBoolean("p2_l12", false) && pack == 2) || ((gameSettings.getBoolean("p3_l12", false) && pack == 3) || ((gameSettings.getBoolean("p4_l12", false) && pack == 4) || (gameSettings.getBoolean("p5_l12", false) && pack == 5))))) {
                    c12.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 12;
                    startActivity(intent);
                    break;
                }
            case R.id.c13 /*2131099662*/:
                if ((gameSettings.getBoolean("p1_l13", false) && pack == 1) || ((gameSettings.getBoolean("p2_l13", false) && pack == 2) || ((gameSettings.getBoolean("p3_l13", false) && pack == 3) || ((gameSettings.getBoolean("p4_l13", false) && pack == 4) || (gameSettings.getBoolean("p5_l13", false) && pack == 5))))) {
                    c13.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 13;
                    startActivity(intent);
                    break;
                }
            case R.id.c14 /*2131099663*/:
                if ((gameSettings.getBoolean("p1_l14", false) && pack == 1) || ((gameSettings.getBoolean("p2_l14", false) && pack == 2) || ((gameSettings.getBoolean("p3_l14", false) && pack == 3) || ((gameSettings.getBoolean("p4_l14", false) && pack == 4) || (gameSettings.getBoolean("p5_l14", false) && pack == 5))))) {
                    c14.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 14;
                    startActivity(intent);
                    break;
                }
            case R.id.c15 /*2131099664*/:
                if ((gameSettings.getBoolean("p1_l15", false) && pack == 1) || ((gameSettings.getBoolean("p2_l15", false) && pack == 2) || ((gameSettings.getBoolean("p3_l15", false) && pack == 3) || ((gameSettings.getBoolean("p4_l15", false) && pack == 4) || (gameSettings.getBoolean("p5_l15", false) && pack == 5))))) {
                    c15.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 15;
                    startActivity(intent);
                    break;
                }
            case R.id.c16 /*2131099665*/:
                if ((gameSettings.getBoolean("p1_l16", false) && pack == 1) || ((gameSettings.getBoolean("p2_l16", false) && pack == 2) || ((gameSettings.getBoolean("p3_l16", false) && pack == 3) || ((gameSettings.getBoolean("p4_l16", false) && pack == 4) || (gameSettings.getBoolean("p5_l16", false) && pack == 5))))) {
                    c16.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 16;
                    startActivity(intent);
                    break;
                }
            case R.id.c17 /*2131099666*/:
                if ((gameSettings.getBoolean("p1_l17", false) && pack == 1) || ((gameSettings.getBoolean("p2_l17", false) && pack == 2) || ((gameSettings.getBoolean("p3_l17", false) && pack == 3) || ((gameSettings.getBoolean("p4_l17", false) && pack == 4) || (gameSettings.getBoolean("p5_l17", false) && pack == 5))))) {
                    c17.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 17;
                    startActivity(intent);
                    break;
                }
            case R.id.c18 /*2131099667*/:
                if ((gameSettings.getBoolean("p1_l18", false) && pack == 1) || ((gameSettings.getBoolean("p2_l18", false) && pack == 2) || ((gameSettings.getBoolean("p3_l18", false) && pack == 3) || ((gameSettings.getBoolean("p4_l18", false) && pack == 4) || (gameSettings.getBoolean("p5_l18", false) && pack == 5))))) {
                    c18.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 18;
                    startActivity(intent);
                    break;
                }
            case R.id.c19 /*2131099668*/:
                if ((gameSettings.getBoolean("p1_l19", false) && pack == 1) || ((gameSettings.getBoolean("p2_l19", false) && pack == 2) || ((gameSettings.getBoolean("p3_l19", false) && pack == 3) || ((gameSettings.getBoolean("p4_l19", false) && pack == 4) || (gameSettings.getBoolean("p5_l19", false) && pack == 5))))) {
                    c19.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 19;
                    startActivity(intent);
                    break;
                }
            case R.id.c20 /*2131099669*/:
                if ((gameSettings.getBoolean("p1_l20", false) && pack == 1) || ((gameSettings.getBoolean("p2_l20", false) && pack == 2) || ((gameSettings.getBoolean("p3_l20", false) && pack == 3) || ((gameSettings.getBoolean("p4_l20", false) && pack == 4) || (gameSettings.getBoolean("p5_l20", false) && pack == 5))))) {
                    c20.setBackgroundResource(R.drawable.cellc);
                    Sound(1);
                    game.level = 20;
                    startActivity(intent);
                    break;
                }
            case R.id.upgrade /*2131099670*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.hyperbyte.chaos")));
                break;
        }
        vibe();
    }

    public void Sound(int sound) {
        int resId = 0;
        if (sound == 1) {
            resId = R.raw.button1;
        }
        if (this.mp != null) {
            this.mp.release();
        }
        this.mp = MediaPlayer.create(this, resId);
        this.mp.start();
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) Menu.vduration);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        exit = 1;
        game.ref2 = 1;
        return true;
    }
}
