package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;

public class Options extends Activity implements View.OnClickListener {
    public static final String PREFERENCE_FILENAME = "AppGamePrefs";
    public static int which = 0;
    public int exit = 0;
    private MediaPlayer mp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView((int) R.layout.options);
        game.ref = 1;
        setButtons();
    }

    public void setButtons() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        SharedPreferences.Editor edit = gameSettings.edit();
        Button o1 = (Button) findViewById(R.id.o1);
        o1.setOnClickListener(this);
        Button o2 = (Button) findViewById(R.id.o2);
        o2.setOnClickListener(this);
        Button o3 = (Button) findViewById(R.id.o3);
        o3.setOnClickListener(this);
        if (gameSettings.getBoolean("music", true)) {
            o1.setBackgroundResource(R.drawable.ocellon);
            o1.setTextColor(Color.argb(255, 0, 255, 0));
            o1.setText("Music: ON ");
        }
        if (gameSettings.getBoolean("sound", true)) {
            o2.setBackgroundResource(R.drawable.ocellon);
            o2.setTextColor(Color.argb(255, 0, 255, 0));
            o2.setText("SFX: ON ");
        }
        if (gameSettings.getBoolean("vibration", true)) {
            o3.setBackgroundResource(R.drawable.ocellon);
            o3.setTextColor(Color.argb(255, 0, 255, 0));
            o3.setText("Vibration: ON ");
        }
        if (!gameSettings.getBoolean("music", true)) {
            o1.setBackgroundResource(R.drawable.ocelloff);
            o1.setTextColor(Color.argb(148, 200, 0, 0));
            o1.setText("Music: OFF");
        }
        if (!gameSettings.getBoolean("sound", true)) {
            o2.setBackgroundResource(R.drawable.ocelloff);
            o2.setTextColor(Color.argb(148, 200, 0, 0));
            o2.setText("SFX: OFF");
        }
        if (!gameSettings.getBoolean("vibration", true)) {
            o3.setBackgroundResource(R.drawable.ocelloff);
            o3.setTextColor(Color.argb(148, 200, 0, 0));
            o3.setText("Vibration: OFF");
        }
    }

    public void onClick(View v) {
        int refresh = 0;
        ((Button) findViewById(R.id.o1)).setOnClickListener(this);
        ((Button) findViewById(R.id.o2)).setOnClickListener(this);
        ((Button) findViewById(R.id.o3)).setOnClickListener(this);
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        SharedPreferences.Editor prefEditor = gameSettings.edit();
        switch (v.getId()) {
            case R.id.o1 /*2131099740*/:
                if (gameSettings.getBoolean("music", true) && 0 == 0) {
                    refresh = 1;
                    prefEditor.putBoolean("music", false);
                    prefEditor.commit();
                    setButtons();
                }
                if (!gameSettings.getBoolean("music", true) && refresh == 0) {
                    prefEditor.putBoolean("music", true);
                    prefEditor.commit();
                    setButtons();
                    if (which == 1) {
                        game.ref = 1;
                        game.soundref = 1;
                    }
                    if (which == 2) {
                        game.soundref2 = 1;
                        break;
                    }
                }
                break;
            case R.id.o2 /*2131099741*/:
                if (gameSettings.getBoolean("sound", true) && 0 == 0) {
                    refresh = 1;
                    prefEditor.putBoolean("sound", false);
                    prefEditor.commit();
                    setButtons();
                }
                if (!gameSettings.getBoolean("sound", true) && refresh == 0) {
                    prefEditor.putBoolean("sound", true);
                    prefEditor.commit();
                    setButtons();
                    break;
                }
            case R.id.o3 /*2131099742*/:
                if (gameSettings.getBoolean("vibration", true) && 0 == 0) {
                    refresh = 1;
                    prefEditor.putBoolean("vibration", false);
                    prefEditor.commit();
                    setButtons();
                }
                if (!gameSettings.getBoolean("vibration", true) && refresh == 0) {
                    prefEditor.putBoolean("vibration", true);
                    prefEditor.commit();
                    setButtons();
                    break;
                }
        }
        vibe();
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) Menu.vduration);
        }
    }
}
