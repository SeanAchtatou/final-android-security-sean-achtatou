package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import com.admob.android.ads.AdView;

public class Pause extends Activity implements View.OnClickListener {
    public static int givehint = 0;
    public static int gomainmenu = 0;
    public static int reset = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView((int) R.layout.pause);
        ((AdView) findViewById(R.id.ad)).requestFreshAd();
        ((Button) findViewById(R.id.resume)).setOnClickListener(this);
        ((Button) findViewById(R.id.hint)).setOnClickListener(this);
        ((Button) findViewById(R.id.restart)).setOnClickListener(this);
        ((Button) findViewById(R.id.mainmenu)).setOnClickListener(this);
        ((Button) findViewById(R.id.options2)).setOnClickListener(this);
    }

    public void setButtons() {
        Button resume = (Button) findViewById(R.id.resume);
        resume.setOnClickListener(this);
        Button hint = (Button) findViewById(R.id.hint);
        hint.setOnClickListener(this);
        Button restart = (Button) findViewById(R.id.restart);
        restart.setOnClickListener(this);
        Button mainmenu = (Button) findViewById(R.id.mainmenu);
        mainmenu.setOnClickListener(this);
        Button options2 = (Button) findViewById(R.id.options2);
        options2.setOnClickListener(this);
        resume.setBackgroundResource(R.drawable.next);
        options2.setBackgroundResource(R.drawable.next);
        hint.setBackgroundResource(R.drawable.next);
        restart.setBackgroundResource(R.drawable.next);
        mainmenu.setBackgroundResource(R.drawable.next);
    }

    public void onClick(View v) {
        Button resume = (Button) findViewById(R.id.resume);
        resume.setOnClickListener(this);
        Button hint = (Button) findViewById(R.id.hint);
        hint.setOnClickListener(this);
        Button restart = (Button) findViewById(R.id.restart);
        restart.setOnClickListener(this);
        Button mainmenu = (Button) findViewById(R.id.mainmenu);
        mainmenu.setOnClickListener(this);
        Button options2 = (Button) findViewById(R.id.options2);
        options2.setOnClickListener(this);
        switch (v.getId()) {
            case R.id.resume:
                resume.setBackgroundResource(R.drawable.nextclick);
                finish();
                break;
            case R.id.hint:
                hint.setBackgroundResource(R.drawable.nextclick);
                givehint = 1;
                finish();
                break;
            case R.id.restart:
                restart.setBackgroundResource(R.drawable.nextclick);
                reset = 1;
                finish();
                break;
            case R.id.mainmenu:
                mainmenu.setBackgroundResource(R.drawable.nextclick);
                gomainmenu = 1;
                finish();
                break;
            case R.id.options2:
                Options.which = 2;
                options2.setBackgroundResource(R.drawable.nextclick);
                startActivity(new Intent(this, Options.class));
                break;
        }
        vibe();
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) Menu.vduration);
        }
    }
}
