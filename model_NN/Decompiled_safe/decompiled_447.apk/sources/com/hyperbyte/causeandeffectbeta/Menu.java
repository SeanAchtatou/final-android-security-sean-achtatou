package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import java.util.Timer;
import java.util.TimerTask;

public class Menu extends Activity implements View.OnClickListener {
    public static int stopMainMusic = 0;
    public static int vduration = 15;
    public int exit2 = 0;
    public int music = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView((int) R.layout.menu);
        game.ref = 1;
        game.soundref = 1;
        final MediaPlayer mediaPlayer = MediaPlayer.create(this, (int) R.raw.maintheme);
        SharedPreferences preferences = getPreferences(0);
        final SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        final Handler handler = new Handler();
        new Timer().schedule(new TimerTask() {
            public void run() {
                if (Menu.this.exit2 == 1) {
                    Menu.this.exit2 = 0;
                    cancel();
                    mediaPlayer.stop();
                    Menu.this.finish();
                }
                if (game.exit == 3) {
                    game.exit = 0;
                    cancel();
                    mediaPlayer.stop();
                    Menu.this.finish();
                }
                Handler handler = handler;
                final SharedPreferences sharedPreferences = gameSettings;
                final MediaPlayer mediaPlayer = mediaPlayer;
                handler.post(new Runnable() {
                    public void run() {
                        if (game.ref == 1) {
                            Menu.this.setButtons();
                            game.ref = 0;
                        }
                        if (game.soundref == 1 && sharedPreferences.getBoolean("music", true)) {
                            mediaPlayer.setLooping(true);
                            mediaPlayer.start();
                            game.soundref = 0;
                        }
                        if (Menu.stopMainMusic == 1 || !sharedPreferences.getBoolean("music", true)) {
                            mediaPlayer.pause();
                            Menu.stopMainMusic = 0;
                        }
                        if (Pause.gomainmenu == 3) {
                            Pause.gomainmenu = 0;
                        }
                    }
                });
            }
        }, 1, 250);
    }

    public void setButtons() {
        Button play = (Button) findViewById(R.id.play);
        play.setOnClickListener(this);
        Button bonus = (Button) findViewById(R.id.bonus);
        bonus.setOnClickListener(this);
        Button about = (Button) findViewById(R.id.about);
        about.setOnClickListener(this);
        Button options = (Button) findViewById(R.id.options);
        options.setOnClickListener(this);
        play.setBackgroundResource(R.drawable.next);
        about.setBackgroundResource(R.drawable.next);
        bonus.setBackgroundResource(R.drawable.next);
        options.setBackgroundResource(R.drawable.next);
    }

    public void onClick(View v) {
        Button play = (Button) findViewById(R.id.play);
        play.setOnClickListener(this);
        ((Button) findViewById(R.id.bonus)).setOnClickListener(this);
        Button about = (Button) findViewById(R.id.about);
        about.setOnClickListener(this);
        Button options = (Button) findViewById(R.id.options);
        options.setOnClickListener(this);
        switch (v.getId()) {
            case R.id.play /*2131099735*/:
                play.setBackgroundResource(R.drawable.nextclick);
                game.world = 1;
                startActivity(new Intent(this, Level.class));
                break;
            case R.id.bonus /*2131099736*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.hyperbyte.chaos")));
                break;
            case R.id.about /*2131099737*/:
                about.setBackgroundResource(R.drawable.nextclick);
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("V: 1.0.5");
                alert.setMessage("- A LOT of new stuff added\n- FULL version is finally available!!!\n- Please rate and comment\n- Follow me @HyperByteX on Twitter for updates!");
                game.ref = 1;
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
                break;
            case R.id.options /*2131099738*/:
                Options.which = 1;
                options.setBackgroundResource(R.drawable.nextclick);
                startActivity(new Intent(this, Options.class));
                break;
        }
        vibe();
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) vduration);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.exit2 = 1;
        return true;
    }
}
