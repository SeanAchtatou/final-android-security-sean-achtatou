package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import java.util.Timer;
import java.util.TimerTask;

public class Lesson extends Activity implements View.OnClickListener {
    public int Page = 0;
    public int exit = 0;
    public int ref = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView((int) R.layout.lesson);
        Button title = (Button) findViewById(R.id.title);
        title.setOnClickListener(this);
        Button i1 = (Button) findViewById(R.id.i1);
        i1.setOnClickListener(this);
        Button i2 = (Button) findViewById(R.id.i2);
        i2.setOnClickListener(this);
        Button i3 = (Button) findViewById(R.id.i3);
        i3.setOnClickListener(this);
        Button i4 = (Button) findViewById(R.id.i4);
        i4.setOnClickListener(this);
        Button i5 = (Button) findViewById(R.id.i5);
        i5.setOnClickListener(this);
        Button description = (Button) findViewById(R.id.description);
        description.setOnClickListener(this);
        ((Button) findViewById(R.id.next)).setOnClickListener(this);
        title.setBackgroundResource(R.drawable.empty);
        i1.setBackgroundResource(R.drawable.empty);
        i2.setBackgroundResource(R.drawable.empty);
        i3.setBackgroundResource(R.drawable.empty);
        i4.setBackgroundResource(R.drawable.empty);
        i5.setBackgroundResource(R.drawable.empty);
        description.setBackgroundResource(R.drawable.empty);
        button();
        Page();
        final Handler handler = new Handler();
        new Timer().schedule(new TimerTask() {
            public void run() {
                if (Lesson.this.exit == 1) {
                    Lesson.this.exit = 0;
                    cancel();
                    Lesson.this.finish();
                }
                handler.post(new Runnable() {
                    public void run() {
                        if (Lesson.this.ref == 1) {
                            Lesson.this.button();
                            Lesson.this.ref = 0;
                        }
                    }
                });
            }
        }, 1, 750);
    }

    public void button() {
        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
        next.setBackgroundResource(R.drawable.next);
    }

    public void Page() {
        Button title = (Button) findViewById(R.id.title);
        title.setOnClickListener(this);
        Button i1 = (Button) findViewById(R.id.i1);
        i1.setOnClickListener(this);
        Button i2 = (Button) findViewById(R.id.i2);
        i2.setOnClickListener(this);
        Button i3 = (Button) findViewById(R.id.i3);
        i3.setOnClickListener(this);
        Button i4 = (Button) findViewById(R.id.i4);
        i4.setOnClickListener(this);
        Button i5 = (Button) findViewById(R.id.i5);
        i5.setOnClickListener(this);
        Button description = (Button) findViewById(R.id.description);
        description.setOnClickListener(this);
        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
        if (game.world == 1) {
            if (game.level == 1) {
                next.setText("Next...");
                title.setTextColor(Color.argb(255, 255, 255, 255));
                if (this.Page == 0) {
                    title.setText("Welcome!");
                    description.setText("The objective in Cause & Effect is to use elements in strategic ways to reach the goals of each level.");
                }
                if (this.Page == 1) {
                    title.setText("Selection");
                    i1.setBackgroundResource(R.drawable.fireb);
                    i2.setBackgroundResource(R.drawable.waterb);
                    i3.setBackgroundResource(R.drawable.plantb);
                    i4.setBackgroundResource(R.drawable.rockb);
                    description.setText("You select which elements you want to use by clicking on the element symbols on the bottom of the screen.");
                }
                if (this.Page == 2) {
                    title.setText("Drop Zones");
                    i1.setBackgroundResource(R.drawable.empty);
                    i2.setBackgroundResource(R.drawable.empty);
                    i3.setBackgroundResource(R.drawable.drop);
                    i4.setBackgroundResource(R.drawable.empty);
                    i5.setBackgroundResource(R.drawable.empty);
                    description.setText("After you have selected an element to use, click on a drop zone (blue squares) to drop it.");
                }
                if (this.Page == 3) {
                    title.setText("Objectives");
                    i1.setBackgroundResource(R.drawable.fireb);
                    i2.setBackgroundResource(R.drawable.waterb);
                    i3.setBackgroundResource(R.drawable.plantb);
                    i4.setBackgroundResource(R.drawable.rockb);
                    i5.setBackgroundResource(R.drawable.steamb);
                    description.setText("To win each level you must get the proper element into the goal icon(s) that appear on the screen. (They are the same image as the selection buttons)");
                }
                if (this.Page == 4) {
                    title.setText("Elements");
                    i1.setBackgroundResource(R.drawable.fire1);
                    i2.setBackgroundResource(R.drawable.water1);
                    i3.setBackgroundResource(R.drawable.plant0);
                    i4.setBackgroundResource(R.drawable.rock1);
                    i5.setBackgroundResource(R.drawable.empty);
                    description.setText("There are different elements in the game which interact with each other.");
                }
                if (this.Page == 5) {
                    title.setTextColor(Color.argb(255, 255, 100, 0));
                    title.setText("Fire");
                    i1.setBackgroundResource(R.drawable.fire1);
                    i2.setBackgroundResource(R.drawable.empty);
                    i3.setBackgroundResource(R.drawable.fireb);
                    i4.setBackgroundResource(R.drawable.empty);
                    i5.setBackgroundResource(R.drawable.fire1);
                    description.setText("Fire burns any connected Plants that it comes in contact with. It also evaporates Water turning it into Steam.");
                    next.setText("Start!");
                }
                if (this.Page == 6) {
                    this.exit = 1;
                }
            }
            if (game.level == 2) {
                next.setText("Next...");
                if (this.Page == 0) {
                    title.setTextColor(Color.argb(255, 64, 64, 255));
                    title.setText("Water");
                    i1.setBackgroundResource(R.drawable.water1);
                    i2.setBackgroundResource(R.drawable.water2);
                    i3.setBackgroundResource(R.drawable.waterb);
                    i4.setBackgroundResource(R.drawable.water2);
                    i5.setBackgroundResource(R.drawable.water1);
                    description.setText("Water extinugishes Fire turning it into Steam. Droping Water onto Plants causes the Plant to grow by one in four directions (up, down, left, right).");
                }
                if (this.Page == 1) {
                    title.setTextColor(Color.argb(255, 0, 255, 0));
                    title.setText("Plant");
                    i1.setBackgroundResource(R.drawable.plant0);
                    i2.setBackgroundResource(R.drawable.empty);
                    i3.setBackgroundResource(R.drawable.plantb);
                    i4.setBackgroundResource(R.drawable.empty);
                    i5.setBackgroundResource(R.drawable.plant0);
                    description.setText("Plants burn when contact is made with fire. They grow when Water is dropped on them. But if a Plant is dropped on Water it will float.");
                    next.setText("Start!");
                }
                if (this.Page == 2) {
                    this.exit = 1;
                }
            }
            if (game.level == 7) {
                next.setText("Next...");
                if (this.Page == 0) {
                    title.setTextColor(Color.argb(255, 128, 128, 128));
                    title.setText("Rock");
                    i1.setBackgroundResource(R.drawable.rock1);
                    i2.setBackgroundResource(R.drawable.empty);
                    i3.setBackgroundResource(R.drawable.rockb);
                    i4.setBackgroundResource(R.drawable.empty);
                    i5.setBackgroundResource(R.drawable.rock1);
                    description.setText("Rocks sink to the bottom of Water (causing the Water to rise). Rocks crush Plants when they are dropped on them.");
                    next.setText("Start!");
                }
                if (this.Page == 1) {
                    this.exit = 1;
                }
            }
            if (game.level == 8) {
                next.setText("Next...");
                if (this.Page == 0) {
                    title.setTextColor(Color.argb(255, 220, 220, 255));
                    title.setText("Steam");
                    i1.setBackgroundResource(R.drawable.fire1);
                    i2.setBackgroundResource(R.drawable.empty);
                    i2.setText("+");
                    i3.setBackgroundResource(R.drawable.water1);
                    i4.setBackgroundResource(R.drawable.empty);
                    i4.setText("=");
                    i5.setBackgroundResource(R.drawable.steamb);
                    description.setText("Combining Fire and Water creates Steam.");
                    next.setText("Start!");
                }
                if (this.Page == 1) {
                    this.exit = 1;
                }
            }
            if (game.level == 15) {
                next.setText("Next...");
                i2.setText("");
                i4.setText("");
                if (this.Page == 0) {
                    title.setTextColor(Color.argb(255, 255, 0, 0));
                    title.setText("Single Use Drop Zones");
                    i1.setBackgroundResource(R.drawable.empty);
                    i2.setBackgroundResource(R.drawable.empty);
                    i3.setBackgroundResource(R.drawable.onedrop1);
                    i4.setBackgroundResource(R.drawable.empty);
                    i5.setBackgroundResource(R.drawable.empty);
                    description.setText("RED drop zones can only be used once.");
                    next.setText("Start!");
                }
                if (this.Page == 1) {
                    this.exit = 1;
                }
            }
        }
    }

    public void onClick(View v) {
        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(this);
        switch (v.getId()) {
            case R.id.next /*2131099678*/:
                next.setBackgroundResource(R.drawable.nextclick);
                this.ref = 1;
                this.Page++;
                Page();
                break;
        }
        vibe();
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) Menu.vduration);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.exit = 1;
        return true;
    }
}
