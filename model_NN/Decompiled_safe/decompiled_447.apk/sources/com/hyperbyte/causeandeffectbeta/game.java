package com.hyperbyte.causeandeffectbeta;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.admob.android.ads.AdView;
import java.lang.reflect.Array;
import java.util.Timer;
import java.util.TimerTask;

public class game extends Activity implements View.OnClickListener {
    public static int exit = 0;
    public static int level = 1;
    public static int ref = 0;
    public static int ref2 = 0;
    public static int ref3 = 0;
    public static int soundref = 0;
    public static int soundref2 = 0;
    public static int world = 5;
    public int addPlant = 0;
    public int[][] ani = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int checkC = 0;
    public int complete = 0;
    public int completeC = 0;
    public int[][] d = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int d1 = 0;
    public int d10 = 0;
    public int d11 = 0;
    public int d12 = 0;
    public int d13 = 0;
    public int d14 = 0;
    public int d15 = 0;
    public int d16 = 0;
    public int d17 = 0;
    public int d18 = 0;
    public int d19 = 0;
    public int d2 = 0;
    public int d20 = 0;
    public int d21 = 0;
    public int d22 = 0;
    public int d23 = 0;
    public int d24 = 0;
    public int d25 = 0;
    public int d26 = 0;
    public int d27 = 0;
    public int d28 = 0;
    public int d29 = 0;
    public int d3 = 0;
    public int d30 = 0;
    public int d31 = 0;
    public int d32 = 0;
    public int d33 = 0;
    public int d34 = 0;
    public int d35 = 0;
    public int d36 = 0;
    public int d37 = 0;
    public int d38 = 0;
    public int d39 = 0;
    public int d4 = 0;
    public int d40 = 0;
    public int d41 = 0;
    public int d42 = 0;
    public int d43 = 0;
    public int d44 = 0;
    public int d45 = 0;
    public int d46 = 0;
    public int d47 = 0;
    public int d48 = 0;
    public int d49 = 0;
    public int d5 = 0;
    public int d6 = 0;
    public int d7 = 0;
    public int d8 = 0;
    public int d9 = 0;
    public int eFire = 1;
    public int eGas = 1;
    public int eIce = 0;
    public int ePlant = 1;
    public int ePort = 0;
    public int eRock = 1;
    public int eWater = 1;
    public int element = 1;
    public int[][] fall = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int fireD = 0;
    public int foc = 0;
    public int[][] gas = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int[][] gas2 = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int gasD = 0;
    public int[][] goal = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int grav = 1;
    public int[][] gravity = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int i = 0;
    public int i2 = 0;
    public int iceD = 0;
    public int item = 0;
    public int itemC = 0;
    public int lFire = -1;
    public int lGas = -1;
    public int lIce = -1;
    public int lPlant = -1;
    public int lPort = -1;
    public int lRock = -1;
    public int lWater = -1;
    private MediaPlayer mp;
    private MediaPlayer mp2;
    public int newclick = 0;
    public int nextsound = 0;
    public int obj = 0;
    public int[][] onedrop = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int plantD = 0;
    public int portD = 0;
    public int[][] portal = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int portsound = 0;
    public int portwait = 0;
    public int reFire = 0;
    public int refresh = 0;
    public int regas = 0;
    public int rerock = 0;
    public int resteam = 0;
    public int rockD = 0;
    public int soundswitch = 0;
    public int[][] stage = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int start = 0;
    public int[][] type = ((int[][]) Array.newInstance(Integer.TYPE, 9, 10));
    public int waterD = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setVolumeControlStream(3);
        setContentView(R.layout.main);
        ((AdView) findViewById(R.id.ad)).requestFreshAd();
        Menu.stopMainMusic = 1;
        Button b1 = (Button) findViewById(R.id.b1);
        b1.setOnClickListener(this);
        Button b2 = (Button) findViewById(R.id.b2);
        b2.setOnClickListener(this);
        Button b3 = (Button) findViewById(R.id.b3);
        b3.setOnClickListener(this);
        Button b4 = (Button) findViewById(R.id.b4);
        b4.setOnClickListener(this);
        Button b5 = (Button) findViewById(R.id.b5);
        b5.setOnClickListener(this);
        Button b6 = (Button) findViewById(R.id.b6);
        b6.setOnClickListener(this);
        Button b7 = (Button) findViewById(R.id.b7);
        b7.setOnClickListener(this);
        Button b8 = (Button) findViewById(R.id.b8);
        b8.setOnClickListener(this);
        Button b9 = (Button) findViewById(R.id.b9);
        b9.setOnClickListener(this);
        Button b10 = (Button) findViewById(R.id.b10);
        b10.setOnClickListener(this);
        Button b11 = (Button) findViewById(R.id.b11);
        b11.setOnClickListener(this);
        Button b12 = (Button) findViewById(R.id.b12);
        b12.setOnClickListener(this);
        Button b13 = (Button) findViewById(R.id.b13);
        b13.setOnClickListener(this);
        Button b14 = (Button) findViewById(R.id.b14);
        b14.setOnClickListener(this);
        Button b15 = (Button) findViewById(R.id.b15);
        b15.setOnClickListener(this);
        Button b16 = (Button) findViewById(R.id.b16);
        b16.setOnClickListener(this);
        Button b17 = (Button) findViewById(R.id.b17);
        b17.setOnClickListener(this);
        Button b18 = (Button) findViewById(R.id.b18);
        b18.setOnClickListener(this);
        Button b19 = (Button) findViewById(R.id.b19);
        b19.setOnClickListener(this);
        Button b20 = (Button) findViewById(R.id.b20);
        b20.setOnClickListener(this);
        Button b21 = (Button) findViewById(R.id.b21);
        b21.setOnClickListener(this);
        Button b22 = (Button) findViewById(R.id.b22);
        b22.setOnClickListener(this);
        Button b23 = (Button) findViewById(R.id.b23);
        b23.setOnClickListener(this);
        Button b24 = (Button) findViewById(R.id.b24);
        b24.setOnClickListener(this);
        Button b25 = (Button) findViewById(R.id.b25);
        b25.setOnClickListener(this);
        Button b26 = (Button) findViewById(R.id.b26);
        b26.setOnClickListener(this);
        Button b27 = (Button) findViewById(R.id.b27);
        b27.setOnClickListener(this);
        Button b28 = (Button) findViewById(R.id.b28);
        b28.setOnClickListener(this);
        Button b29 = (Button) findViewById(R.id.b29);
        b29.setOnClickListener(this);
        Button b30 = (Button) findViewById(R.id.b30);
        b30.setOnClickListener(this);
        Button b31 = (Button) findViewById(R.id.b31);
        b31.setOnClickListener(this);
        Button b32 = (Button) findViewById(R.id.b32);
        b32.setOnClickListener(this);
        Button b33 = (Button) findViewById(R.id.b33);
        b33.setOnClickListener(this);
        Button b34 = (Button) findViewById(R.id.b34);
        b34.setOnClickListener(this);
        Button b35 = (Button) findViewById(R.id.b35);
        b35.setOnClickListener(this);
        Button b36 = (Button) findViewById(R.id.b36);
        b36.setOnClickListener(this);
        Button b37 = (Button) findViewById(R.id.b37);
        b37.setOnClickListener(this);
        Button b38 = (Button) findViewById(R.id.b38);
        b38.setOnClickListener(this);
        Button b39 = (Button) findViewById(R.id.b39);
        b39.setOnClickListener(this);
        Button b40 = (Button) findViewById(R.id.b40);
        b40.setOnClickListener(this);
        Button b41 = (Button) findViewById(R.id.b41);
        b41.setOnClickListener(this);
        Button b42 = (Button) findViewById(R.id.b42);
        b42.setOnClickListener(this);
        Button b43 = (Button) findViewById(R.id.b43);
        b43.setOnClickListener(this);
        Button b44 = (Button) findViewById(R.id.b44);
        b44.setOnClickListener(this);
        Button b45 = (Button) findViewById(R.id.b45);
        b45.setOnClickListener(this);
        Button b46 = (Button) findViewById(R.id.b46);
        b46.setOnClickListener(this);
        Button b47 = (Button) findViewById(R.id.b47);
        b47.setOnClickListener(this);
        Button b48 = (Button) findViewById(R.id.b48);
        b48.setOnClickListener(this);
        Button b49 = (Button) findViewById(R.id.b49);
        b49.setOnClickListener(this);
        ((Button) findViewById(R.id.s1)).setOnClickListener(this);
        ((Button) findViewById(R.id.s2)).setOnClickListener(this);
        ((Button) findViewById(R.id.s3)).setOnClickListener(this);
        ((Button) findViewById(R.id.s4)).setOnClickListener(this);
        ((Button) findViewById(R.id.s5)).setOnClickListener(this);
        b1.setBackgroundResource(R.drawable.empty);
        b2.setBackgroundResource(R.drawable.empty);
        b3.setBackgroundResource(R.drawable.empty);
        b4.setBackgroundResource(R.drawable.empty);
        b5.setBackgroundResource(R.drawable.empty);
        b6.setBackgroundResource(R.drawable.empty);
        b7.setBackgroundResource(R.drawable.empty);
        b8.setBackgroundResource(R.drawable.empty);
        b9.setBackgroundResource(R.drawable.empty);
        b10.setBackgroundResource(R.drawable.empty);
        b11.setBackgroundResource(R.drawable.empty);
        b12.setBackgroundResource(R.drawable.empty);
        b13.setBackgroundResource(R.drawable.empty);
        b14.setBackgroundResource(R.drawable.empty);
        b15.setBackgroundResource(R.drawable.empty);
        b16.setBackgroundResource(R.drawable.empty);
        b17.setBackgroundResource(R.drawable.empty);
        b18.setBackgroundResource(R.drawable.empty);
        b19.setBackgroundResource(R.drawable.empty);
        b20.setBackgroundResource(R.drawable.empty);
        b21.setBackgroundResource(R.drawable.empty);
        b22.setBackgroundResource(R.drawable.empty);
        b23.setBackgroundResource(R.drawable.empty);
        b24.setBackgroundResource(R.drawable.empty);
        b25.setBackgroundResource(R.drawable.empty);
        b26.setBackgroundResource(R.drawable.empty);
        b27.setBackgroundResource(R.drawable.empty);
        b28.setBackgroundResource(R.drawable.empty);
        b29.setBackgroundResource(R.drawable.empty);
        b30.setBackgroundResource(R.drawable.empty);
        b31.setBackgroundResource(R.drawable.empty);
        b32.setBackgroundResource(R.drawable.empty);
        b33.setBackgroundResource(R.drawable.empty);
        b34.setBackgroundResource(R.drawable.empty);
        b35.setBackgroundResource(R.drawable.empty);
        b36.setBackgroundResource(R.drawable.empty);
        b37.setBackgroundResource(R.drawable.empty);
        b38.setBackgroundResource(R.drawable.empty);
        b39.setBackgroundResource(R.drawable.empty);
        b40.setBackgroundResource(R.drawable.empty);
        b41.setBackgroundResource(R.drawable.empty);
        b42.setBackgroundResource(R.drawable.empty);
        b43.setBackgroundResource(R.drawable.empty);
        b44.setBackgroundResource(R.drawable.empty);
        b45.setBackgroundResource(R.drawable.empty);
        b46.setBackgroundResource(R.drawable.empty);
        b47.setBackgroundResource(R.drawable.empty);
        b48.setBackgroundResource(R.drawable.empty);
        b49.setBackgroundResource(R.drawable.empty);
        Theme();
        level();
        int resId2 = 0;
        if (world == 1) {
            resId2 = R.raw.blankslate;
        }
        MediaPlayer ST = MediaPlayer.create(this, resId2);
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        if (gameSettings.getBoolean("music", true)) {
            ST.setLooping(true);
            ST.start();
        }
        final MediaPlayer mediaPlayer = ST;
        final Handler handler = new Handler();
        final SharedPreferences sharedPreferences = gameSettings;
        new Timer().schedule(new TimerTask() {
            public void run() {
                if (game.exit == 1) {
                    game.exit = 0;
                    game.soundref = 1;
                    mediaPlayer.stop();
                    Menu.stopMainMusic = 0;
                    cancel();
                    game.this.finish();
                }
                if (game.exit == 2) {
                    game.exit = 3;
                    game.soundref = 1;
                    mediaPlayer.stop();
                    Menu.stopMainMusic = 0;
                    cancel();
                    game.this.finish();
                }
                Handler handler = handler;
                final SharedPreferences sharedPreferences = sharedPreferences;
                final MediaPlayer mediaPlayer = mediaPlayer;
                handler.post(new Runnable() {
                    public void run() {
                        if (!sharedPreferences.getBoolean("music", true)) {
                            mediaPlayer.pause();
                        }
                        if (sharedPreferences.getBoolean("music", true) && game.soundref2 == 1) {
                            mediaPlayer.start();
                            game.soundref2 = 0;
                        }
                        if (Pause.reset == 1) {
                            game.this.level();
                            Pause.reset = 0;
                        }
                        if (Pause.givehint == 1) {
                            game.this.hint();
                            Pause.givehint = 0;
                        }
                        if (Pause.gomainmenu == 1) {
                            Pause.gomainmenu = 2;
                            game.exit = 1;
                        }
                        game.this.checkC++;
                        if (game.this.checkC >= 0) {
                            game.this.Check();
                            game.this.checkC = 0;
                        }
                        game.this.itemC++;
                        if (game.this.itemC >= 50) {
                            game.this.item = game.this.element;
                            game.this.itemC = 0;
                        }
                        if (game.this.complete == 1) {
                            game.this.completeC++;
                        }
                        if (game.this.complete == -1) {
                            game.this.foc = 1;
                            game.this.complete = -2;
                            game.this.fail();
                        }
                        if (game.this.completeC == 15 && game.world < 3) {
                            game.this.foc = 1;
                            game.this.complete();
                        }
                        if (game.this.completeC == 20 && game.world >= 3) {
                            game.this.foc = 1;
                            game.this.complete();
                        }
                    }
                });
            }
        }, 1, 55);
    }

    public void hint() {
        String tip = "";
        if (world == 1) {
            if (level == 1) {
                tip = "Just choose fire and then drop it!";
            }
            if (level == 2) {
                tip = "Just add water!";
            }
            if (level == 3) {
                tip = "You'll have to get rid of the plants somehow...";
            }
            if (level == 4) {
                tip = "You can reach the fire objective using plants...";
            }
            if (level == 5) {
                tip = "use LOTS of water...";
            }
            if (level == 6) {
                tip = "A bit tricky. Only use water 3 times before fire...";
            }
            if (level == 7) {
                tip = "2 Rocks equals 2 splashes...";
            }
            if (level == 8) {
                tip = "Water and fire create steam...";
            }
            if (level == 9) {
                tip = "Get rid of those plants first!";
            }
            if (level == 10) {
                tip = "ALL the objectives must be completed (in any order)...";
            }
            if (level == 11) {
                tip = "Burn then stack new plants up high!";
            }
            if (level == 12) {
                tip = "Climb the stairs...";
            }
            if (level == 13) {
                tip = "Drop fire at the end from the top-left corner...";
            }
            if (level == 14) {
                tip = "Burn some plants before adding water...";
            }
            if (level == 15) {
                tip = "Remember, you can only use the red drop zones ONCE!";
            }
            if (level == 16) {
                tip = "Use a plant first. Rock last.";
            }
            if (level == 17) {
                tip = "Use the lowest drop zone to burn the bottom row of plants...";
            }
            if (level == 18) {
                tip = "Take advantage of the only blue drop zone...";
            }
            if (level == 19) {
                tip = "Don't over think it...";
            }
            if (level == 20) {
                tip = "Save the rock objective till the end...";
            }
        }
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("");
        alert.setMessage("Hint: " + tip);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }

    public void level() {
        reset();
        Intent i3 = new Intent(this, Lesson.class);
        if (world == -1) {
            setTitle("Bonus: Sandbox");
            this.d1 = 1;
            this.d2 = 1;
            this.d3 = 1;
            this.d4 = 1;
            this.d5 = 1;
            this.d6 = 1;
            this.d7 = 1;
            this.obj = 10;
            this.eFire = 1;
            this.eWater = 1;
            this.ePlant = 1;
            this.eRock = 1;
        }
        if (world == 1) {
            setTitle("Blank Slate: Level " + level);
            if (level == 1) {
                startActivity(i3);
                this.d10 = 1;
                this.goal[3][6] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 0;
            }
            if (level == 2) {
                startActivity(i3);
                this.d9 = 1;
                this.type[2][6] = 2;
                this.goal[3][6] = 2;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 0;
            }
            if (level == 3) {
                this.d4 = 1;
                this.type[1][4] = 2;
                this.type[2][4] = 2;
                this.type[3][4] = 2;
                this.type[4][4] = 2;
                this.type[5][4] = 2;
                this.type[6][4] = 2;
                this.type[7][4] = 2;
                this.type[1][7] = 3;
                this.type[2][7] = 3;
                this.type[3][7] = 3;
                this.type[5][7] = 3;
                this.type[6][7] = 3;
                this.type[7][7] = 3;
                this.goal[4][7] = 3;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 0;
            }
            if (level == 4) {
                this.d24 = 1;
                this.d2 = 1;
                this.type[2][6] = 2;
                this.goal[4][6] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 5) {
                this.d1 = 1;
                this.d9 = 1;
                this.d3 = 1;
                this.d11 = 1;
                this.d5 = 1;
                this.d13 = 1;
                this.type[1][4] = 2;
                this.goal[7][4] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 6) {
                this.d19 = 1;
                this.d38 = 1;
                this.d39 = 1;
                this.d40 = 1;
                this.type[1][6] = 2;
                this.type[1][7] = 2;
                this.type[2][6] = 2;
                this.type[2][5] = 2;
                this.type[2][4] = 2;
                this.type[2][3] = 2;
                this.type[2][2] = 2;
                this.type[3][2] = 2;
                this.type[4][2] = 2;
                this.type[5][2] = 2;
                this.type[6][2] = 2;
                this.type[7][2] = 2;
                this.type[4][7] = 2;
                this.type[5][4] = 2;
                this.type[7][3] = 2;
                this.type[7][4] = 2;
                this.goal[4][4] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 7) {
                startActivity(i3);
                this.d4 = 1;
                this.type[3][6] = 3;
                this.type[3][7] = 3;
                this.type[4][6] = 3;
                this.type[4][7] = 3;
                this.type[5][6] = 3;
                this.type[5][7] = 3;
                this.goal[4][4] = 3;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 0;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 8) {
                startActivity(i3);
                this.d18 = 1;
                this.goal[4][1] = 5;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 9) {
                this.d8 = 1;
                this.d9 = 1;
                this.d13 = 1;
                this.d14 = 1;
                this.d25 = 1;
                this.type[3][3] = 2;
                this.type[4][3] = 2;
                this.type[5][3] = 2;
                this.type[1][4] = 2;
                this.type[7][4] = 2;
                this.type[3][5] = 2;
                this.type[5][5] = 2;
                this.type[3][6] = 2;
                this.type[4][6] = 2;
                this.type[5][6] = 2;
                this.type[3][7] = 3;
                this.type[4][7] = 3;
                this.type[5][7] = 3;
                this.goal[4][1] = 5;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 1;
            }
            if (level == 10) {
                this.d2 = 1;
                this.d6 = 1;
                this.d25 = 1;
                this.type[1][3] = 2;
                this.type[2][3] = 2;
                this.type[3][3] = 2;
                this.type[4][3] = 2;
                this.type[5][3] = 2;
                this.type[6][3] = 2;
                this.type[7][3] = 2;
                this.type[2][6] = 2;
                this.type[6][6] = 2;
                this.type[1][7] = 3;
                this.type[2][7] = 3;
                this.type[3][7] = 3;
                this.type[4][7] = 3;
                this.type[5][7] = 3;
                this.type[6][7] = 3;
                this.type[7][7] = 3;
                this.goal[4][1] = 5;
                this.goal[1][5] = 1;
                this.goal[7][5] = 2;
                this.obj = 3;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 11) {
                this.d1 = 1;
                this.d3 = 1;
                this.d5 = 1;
                this.d7 = 1;
                this.type[2][2] = 2;
                this.type[3][3] = 2;
                this.type[5][7] = 2;
                this.type[6][6] = 2;
                this.type[7][7] = 2;
                this.type[6][2] = 2;
                this.type[5][3] = 2;
                this.type[3][7] = 2;
                this.type[2][6] = 2;
                this.type[1][7] = 2;
                this.goal[4][4] = 2;
                this.goal[2][7] = 1;
                this.goal[6][7] = 1;
                this.obj = 3;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 0;
            }
            if (level == 12) {
                this.d2 = 1;
                this.d10 = 1;
                this.d18 = 1;
                this.d34 = 1;
                this.type[1][1] = 2;
                this.type[2][2] = 2;
                this.type[3][3] = 2;
                this.type[4][4] = 2;
                this.type[5][5] = 2;
                this.type[6][6] = 2;
                this.type[7][7] = 2;
                this.goal[7][6] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 13) {
                this.d1 = 1;
                this.d13 = 1;
                this.d8 = 1;
                this.d23 = 1;
                this.d19 = 1;
                this.type[1][3] = 2;
                this.type[2][6] = 2;
                this.type[5][5] = 2;
                this.type[6][3] = 2;
                this.type[7][4] = 2;
                this.goal[7][3] = 1;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 14) {
                this.d1 = 1;
                this.d2 = 1;
                this.d3 = 1;
                this.d4 = 1;
                this.d5 = 1;
                this.d6 = 1;
                this.d7 = 1;
                this.type[1][4] = 2;
                this.type[4][4] = 2;
                this.type[7][4] = 2;
                this.type[3][5] = 2;
                this.type[5][5] = 2;
                this.type[7][5] = 2;
                this.type[1][7] = 2;
                this.type[5][7] = 2;
                this.type[2][5] = 6;
                this.type[6][5] = 6;
                this.type[4][6] = 6;
                this.goal[2][6] = 2;
                this.goal[6][6] = 2;
                this.goal[4][7] = 1;
                this.obj = 3;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 15) {
                startActivity(i3);
                this.d8 = 1;
                this.d9 = 1;
                this.d11 = 1;
                this.d13 = 1;
                this.onedrop[1][2] = 1;
                this.onedrop[2][2] = 1;
                this.onedrop[4][2] = 1;
                this.onedrop[6][2] = 1;
                this.type[4][4] = 2;
                this.type[2][5] = 2;
                this.type[6][5] = 2;
                this.goal[1][5] = 1;
                this.goal[7][5] = 1;
                this.obj = 2;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 0;
                this.eRock = 0;
            }
            if (level == 16) {
                this.d4 = 1;
                this.d11 = 1;
                this.d18 = 1;
                this.d25 = 1;
                this.onedrop[4][1] = 1;
                this.onedrop[4][2] = 1;
                this.onedrop[4][3] = 1;
                this.onedrop[4][4] = 1;
                this.goal[3][7] = 1;
                this.goal[4][7] = 4;
                this.goal[5][7] = 2;
                this.obj = 3;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 17) {
                this.d1 = 1;
                this.d2 = 1;
                this.d3 = 1;
                this.d4 = 1;
                this.d5 = 1;
                this.d6 = 1;
                this.d7 = 1;
                this.d20 = 1;
                this.onedrop[1][1] = 1;
                this.onedrop[2][1] = 1;
                this.onedrop[3][1] = 1;
                this.onedrop[4][1] = 1;
                this.onedrop[5][1] = 1;
                this.onedrop[6][1] = 1;
                this.onedrop[7][1] = 1;
                this.onedrop[6][3] = 1;
                this.type[1][3] = 2;
                this.type[2][4] = 2;
                this.type[4][3] = 2;
                this.type[5][4] = 2;
                this.type[1][6] = 2;
                this.type[2][6] = 2;
                this.type[3][6] = 2;
                this.type[4][6] = 2;
                this.type[5][6] = 2;
                this.type[6][6] = 2;
                this.type[7][6] = 2;
                this.goal[7][3] = 1;
                this.goal[3][7] = 4;
                this.goal[7][7] = 3;
                this.obj = 3;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 18) {
                this.d6 = 1;
                this.d7 = 1;
                this.d10 = 1;
                this.d17 = 1;
                this.onedrop[6][1] = 1;
                this.onedrop[3][2] = 1;
                this.onedrop[3][3] = 1;
                this.type[3][4] = 2;
                this.type[3][6] = 2;
                this.type[1][7] = 3;
                this.type[2][7] = 3;
                this.type[3][7] = 3;
                this.type[4][7] = 3;
                this.type[5][7] = 3;
                this.type[6][7] = 6;
                this.type[7][7] = 6;
                this.goal[3][1] = 5;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
            if (level == 19) {
                this.d1 = 1;
                this.d2 = 1;
                this.d3 = 1;
                this.d5 = 1;
                this.d7 = 1;
                this.d8 = 1;
                this.d9 = 1;
                this.d11 = 1;
                this.d14 = 1;
                this.d22 = 1;
                this.d28 = 1;
                this.onedrop[1][1] = 1;
                this.onedrop[2][1] = 1;
                this.onedrop[3][1] = 1;
                this.onedrop[5][1] = 1;
                this.onedrop[7][1] = 1;
                this.onedrop[1][2] = 1;
                this.onedrop[2][2] = 1;
                this.onedrop[4][2] = 1;
                this.onedrop[7][2] = 1;
                this.onedrop[1][4] = 1;
                this.onedrop[7][4] = 1;
                this.type[1][3] = 2;
                this.type[3][3] = 2;
                this.type[4][4] = 2;
                this.type[5][3] = 2;
                this.type[7][3] = 2;
                this.type[4][6] = 2;
                this.type[3][7] = 3;
                this.type[4][7] = 3;
                this.type[5][7] = 3;
                this.goal[4][1] = 5;
                this.obj = 1;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 0;
            }
            if (level == 20) {
                this.d3 = 1;
                this.d4 = 1;
                this.d5 = 1;
                this.d10 = 1;
                this.d12 = 1;
                this.d17 = 1;
                this.d18 = 1;
                this.d19 = 1;
                this.d24 = 1;
                this.onedrop[3][1] = 1;
                this.onedrop[4][1] = 1;
                this.onedrop[5][1] = 1;
                this.onedrop[3][2] = 1;
                this.onedrop[5][2] = 1;
                this.onedrop[3][3] = 1;
                this.onedrop[4][3] = 1;
                this.onedrop[5][3] = 1;
                this.onedrop[3][4] = 1;
                this.type[4][5] = 6;
                this.type[4][7] = 6;
                this.goal[4][4] = 1;
                this.goal[4][6] = 1;
                this.goal[5][7] = 4;
                this.goal[5][7] = 4;
                this.goal[6][7] = 2;
                this.obj = 4;
                this.eFire = 1;
                this.eWater = 1;
                this.ePlant = 1;
                this.eRock = 1;
            }
        }
        eButtons();
        start();
    }

    public void Theme() {
        LinearLayout rl = (LinearLayout) findViewById(R.id.myLayout);
        if (world == 1) {
            rl.setBackgroundResource(R.drawable.slate);
        }
    }

    public void Sound(int sound) {
        int resId = 0;
        if (sound == 1) {
            resId = R.raw.splash;
        }
        if (sound == 2) {
            resId = R.raw.stonecrack;
        }
        if (sound == 3) {
            resId = R.raw.burn;
        }
        if (sound == 4) {
            resId = R.raw.button1;
        }
        if (sound == 5) {
            resId = R.raw.droplet;
        }
        if (sound == 6) {
            resId = R.raw.rockfall;
        }
        if (sound == 7) {
            resId = R.raw.steam;
        }
        if (sound == 9) {
            resId = R.raw.win;
        }
        if (sound == 10) {
            resId = R.raw.obj;
        }
        if (this.nextsound == 0) {
            if (this.mp != null) {
                this.mp.release();
            }
            this.mp = MediaPlayer.create(this, resId);
            SharedPreferences preferences = getPreferences(0);
            if (getSharedPreferences("MyGamePreferences", 0).getBoolean("sound", true)) {
                this.mp.start();
            }
        }
        if (this.nextsound == 1) {
            if (this.mp2 != null) {
                this.mp2.release();
            }
            this.mp2 = MediaPlayer.create(this, resId);
            SharedPreferences preferences2 = getPreferences(0);
            if (getSharedPreferences("MyGamePreferences", 0).getBoolean("sound", true)) {
                this.mp2.start();
            }
        }
        this.nextsound++;
        if (this.nextsound == 2) {
            this.nextsound = 0;
        }
    }

    public void eButtons() {
        Button s1 = (Button) findViewById(R.id.s1);
        Button s2 = (Button) findViewById(R.id.s2);
        Button s3 = (Button) findViewById(R.id.s3);
        Button s4 = (Button) findViewById(R.id.s4);
        Button button = (Button) findViewById(R.id.s5);
        if (world == 1 || world == -1) {
            if (this.eFire == 1 && this.element != 1) {
                s1.setBackgroundResource(R.drawable.fireb);
            }
            if (this.eFire == 1 && this.element == 1) {
                s1.setBackgroundResource(R.drawable.firebh);
            }
            if (this.eFire == 0) {
                s1.setBackgroundResource(R.drawable.blank);
            }
            if (this.eWater == 1 && this.element != 2) {
                s2.setBackgroundResource(R.drawable.waterb);
            }
            if (this.eWater == 1 && this.element == 2) {
                s2.setBackgroundResource(R.drawable.waterbh);
            }
            if (this.eWater == 0) {
                s2.setBackgroundResource(R.drawable.blank);
            }
            if (this.ePlant == 1 && this.element != 3) {
                s3.setBackgroundResource(R.drawable.plantb);
            }
            if (this.ePlant == 1 && this.element == 3) {
                s3.setBackgroundResource(R.drawable.plantbh);
            }
            if (this.ePlant == 0) {
                s3.setBackgroundResource(R.drawable.blank);
            }
            if (this.eRock == 1 && this.element != 4) {
                s4.setBackgroundResource(R.drawable.rockb);
            }
            if (this.eRock == 1 && this.element == 4) {
                s4.setBackgroundResource(R.drawable.rockbh);
            }
            if (this.eRock == 0) {
                s4.setBackgroundResource(R.drawable.blank);
            }
        }
    }

    public void reset() {
        this.d1 = 0;
        this.d2 = 0;
        this.d3 = 0;
        this.d4 = 0;
        this.d5 = 0;
        this.d6 = 0;
        this.d7 = 0;
        this.d8 = 0;
        this.d9 = 0;
        this.d10 = 0;
        this.d11 = 0;
        this.d12 = 0;
        this.d13 = 0;
        this.d14 = 0;
        this.d15 = 0;
        this.d16 = 0;
        this.d17 = 0;
        this.d18 = 0;
        this.d19 = 0;
        this.d20 = 0;
        this.d21 = 0;
        this.d22 = 0;
        this.d23 = 0;
        this.d24 = 0;
        this.d25 = 0;
        this.d26 = 0;
        this.d27 = 0;
        this.d28 = 0;
        this.d29 = 0;
        this.d30 = 0;
        this.d31 = 0;
        this.d32 = 0;
        this.d33 = 0;
        this.d34 = 0;
        this.d35 = 0;
        this.d36 = 0;
        this.d37 = 0;
        this.d38 = 0;
        this.d39 = 0;
        this.d40 = 0;
        this.d41 = 0;
        this.d42 = 0;
        this.d43 = 0;
        this.d44 = 0;
        this.d45 = 0;
        this.d46 = 0;
        this.d47 = 0;
        this.d48 = 0;
        this.d49 = 0;
        this.item = 0;
        this.i = 0;
        int x = 1;
        int y = 1;
        while (y < 9) {
            this.type[x][y] = 0;
            this.goal[x][y] = 0;
            this.fall[x][y] = 0;
            this.stage[x][y] = 0;
            this.ani[x][y] = 0;
            this.d[x][y] = 0;
            this.onedrop[x][y] = 0;
            this.gas[x][y] = 0;
            this.gas2[x][y] = 0;
            this.gravity[x][y] = 0;
            this.portal[x][y] = 0;
            x++;
            if (x > 8) {
                x = 1;
                y++;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r13v0, types: [com.hyperbyte.causeandeffectbeta.game] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void Check() {
        /*
            r13 = this;
            r12 = 2
            r11 = 8
            r10 = -1
            r9 = 0
            r8 = 1
            r2 = 1
            r3 = 1
            r13.i2 = r9
            r0 = 0
            r1 = 0
            r13.rerock = r9
            int r4 = r13.regas
            r5 = 5
            if (r4 != r5) goto L_0x0015
            r13.regas = r9
        L_0x0015:
            int r4 = r13.regas
            if (r4 < r8) goto L_0x001f
            int r4 = r13.regas
            int r4 = r4 + 1
            r13.regas = r4
        L_0x001f:
            int r4 = r13.reFire
            r5 = 11
            if (r4 != r5) goto L_0x0027
            r13.reFire = r9
        L_0x0027:
            int r4 = r13.reFire
            if (r4 < r8) goto L_0x0031
            int r4 = r13.reFire
            int r4 = r4 + 1
            r13.reFire = r4
        L_0x0031:
            int r4 = r13.portsound
            r5 = 3
            if (r4 < r5) goto L_0x003b
            r13.portsound = r9
            r13.Sound(r11)
        L_0x003b:
            int r4 = r13.portsound
            if (r4 <= 0) goto L_0x0045
            int r4 = r13.portsound
            int r4 = r4 + 1
            r13.portsound = r4
        L_0x0045:
            int r4 = r13.i2
            r5 = 49
            if (r4 < r5) goto L_0x005d
            int r4 = r13.addPlant
            if (r4 <= 0) goto L_0x005c
            int r4 = r13.lPlant
            int r4 = r4 + 2
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            r13.addPlant = r9
        L_0x005c:
            return
        L_0x005d:
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.i2 = r4
            int r4 = r13.complete
            r5 = -2
            if (r4 == r5) goto L_0x00fb
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0083
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x0081
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x0083
        L_0x0081:
            r13.complete = r10
        L_0x0083:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x0096
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x0096
            r13.complete = r10
        L_0x0096:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -3
            if (r4 != r5) goto L_0x00aa
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x00aa
            r13.complete = r10
        L_0x00aa:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -4
            if (r4 != r5) goto L_0x00be
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x00be
            r13.complete = r10
        L_0x00be:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -5
            if (r4 != r5) goto L_0x00d2
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x00d2
            r13.complete = r10
        L_0x00d2:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -8
            if (r4 != r5) goto L_0x00e5
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x00e5
            r13.complete = r10
        L_0x00e5:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -9
            if (r4 != r5) goto L_0x00fb
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x00fb
            r13.complete = r10
        L_0x00fb:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0129
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x0114
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x0129
        L_0x0114:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x0129:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x014e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x014e
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x014e:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x0175
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x0175
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x0175:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x019c
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x019c
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x019c:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x01c3
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x01c3
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x01c3:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x01e8
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x01e8
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x01e8:
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x0211
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x0211
            int[][] r4 = r13.goal
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.obj
            int r4 = r4 - r8
            r13.obj = r4
            r4 = 10
            r13.Sound(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x0211:
            int r4 = r13.obj
            if (r4 > 0) goto L_0x0224
            int r4 = r13.complete
            if (r4 == r8) goto L_0x0224
            int r4 = r13.complete
            if (r4 == r10) goto L_0x0224
            int r4 = r13.complete
            r5 = -2
            if (r4 == r5) goto L_0x0224
            r13.complete = r8
        L_0x0224:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x0274
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x0274
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x0274
            int r4 = r3 - r8
            if (r4 <= r8) goto L_0x0274
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x0274:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x02bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x02bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x02bc
            int r4 = r3 - r8
            if (r4 > r8) goto L_0x02bc
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x02bc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x02ef
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x02ef
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x02ef
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x02ef
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x02ef:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0322
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0322
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x0322
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x0322
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x0322:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0340
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= r8) goto L_0x0340
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r10) goto L_0x0340
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
        L_0x0340:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x03a1
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x03a1
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x03a1
            int r4 = r13.lFire
            if (r4 != r10) goto L_0x03a1
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x03a1:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x03fc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x03fc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x03fc
            int r4 = r13.lFire
            if (r4 <= r10) goto L_0x03fc
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x03fc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x045f
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x045f
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x045f
            int r4 = r13.lWater
            if (r4 != r10) goto L_0x045f
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x045f:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x04bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x04bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x04bc
            int r4 = r13.lWater
            if (r4 <= r10) goto L_0x04bc
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x04bc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x051d
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x051d
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x051d
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x051d
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x051d:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0578
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0578
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x0578
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x0578
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0578:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x05db
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x05db
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x05db
            int r4 = r13.lRock
            if (r4 != r10) goto L_0x05db
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x05db:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0638
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0638
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x0638
            int r4 = r13.lRock
            if (r4 <= r10) goto L_0x0638
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0638:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0699
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0699
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0699
            int r4 = r13.lFire
            if (r4 != r10) goto L_0x0699
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0699:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x06f4
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x06f4
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x06f4
            int r4 = r13.lFire
            if (r4 <= r10) goto L_0x06f4
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x06f4:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0757
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x0757
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0757
            int r4 = r13.lWater
            if (r4 != r10) goto L_0x0757
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0757:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x07b4
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x07b4
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x07b4
            int r4 = r13.lWater
            if (r4 <= r10) goto L_0x07b4
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x07b4:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0815
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x0815
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0815
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x0815
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0815:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0870
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x0870
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0870
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x0870
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0870:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x08d3
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x08d3
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x08d3
            int r4 = r13.lRock
            if (r4 != r10) goto L_0x08d3
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x08d3:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0930
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x0930
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0930
            int r4 = r13.lRock
            if (r4 <= r10) goto L_0x0930
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0930:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0991
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0991
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0991
            int r4 = r13.lPort
            if (r4 != r10) goto L_0x0991
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            r13.ePort = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0991:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x09ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x09ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x09ec
            int r4 = r13.lPort
            if (r4 <= r10) goto L_0x09ec
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            r13.ePort = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x09ec:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0a51
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0a51
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x0a51
            int r4 = r13.lIce
            if (r4 != r10) goto L_0x0a51
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0a51:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0ab0
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0ab0
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x0ab0
            int r4 = r13.lIce
            if (r4 <= r10) goto L_0x0ab0
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0ab0:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0b15
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x0b15
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0b15
            int r4 = r13.lIce
            if (r4 != r10) goto L_0x0b15
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0b15:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0b74
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x0b74
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0b74
            int r4 = r13.lIce
            if (r4 <= r10) goto L_0x0b74
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r11
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
        L_0x0b74:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0bdd
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0bdd
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x0bdd
            int r4 = r13.lFire
            if (r4 != r10) goto L_0x0bdd
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0bdd:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0c40
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0c40
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x0c40
            int r4 = r13.lFire
            if (r4 <= r10) goto L_0x0c40
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0c40:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0caa
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0caa
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x0caa
            int r4 = r13.lWater
            if (r4 != r10) goto L_0x0caa
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0caa:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0d0e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0d0e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x0d0e
            int r4 = r13.lWater
            if (r4 <= r10) goto L_0x0d0e
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0d0e:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0d77
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0d77
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x0d77
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x0d77
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0d77:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0dda
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0dda
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x0dda
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x0dda
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0dda:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0e44
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0e44
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x0e44
            int r4 = r13.lRock
            if (r4 != r10) goto L_0x0e44
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0e44:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0ea8
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x0ea8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x0ea8
            int r4 = r13.lRock
            if (r4 <= r10) goto L_0x0ea8
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0ea8:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0f11
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0f11
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0f11
            int r4 = r13.lFire
            if (r4 != r10) goto L_0x0f11
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0f11:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0f74
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x0f74
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0f74
            int r4 = r13.lFire
            if (r4 <= r10) goto L_0x0f74
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lFire
            int r4 = r4 + 1
            r13.lFire = r4
            r13.eFire = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0f74:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x0fde
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x0fde
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x0fde
            int r4 = r13.lWater
            if (r4 != r10) goto L_0x0fde
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x0fde:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1042
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x1042
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x1042
            int r4 = r13.lWater
            if (r4 <= r10) goto L_0x1042
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lWater
            int r4 = r4 + 1
            r13.lWater = r4
            r13.eWater = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1042:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x10ab
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x10ab
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x10ab
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x10ab
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x10ab:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x110e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x110e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x110e
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x110e
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPlant
            int r4 = r4 + 1
            r13.lPlant = r4
            r13.ePlant = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x110e:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1178
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x1178
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x1178
            int r4 = r13.lRock
            if (r4 != r10) goto L_0x1178
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1178:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x11dc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x11dc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x11dc
            int r4 = r13.lRock
            if (r4 <= r10) goto L_0x11dc
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lRock
            int r4 = r4 + 1
            r13.lRock = r4
            r13.eRock = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x11dc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1245
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x1245
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x1245
            int r4 = r13.lPort
            if (r4 != r10) goto L_0x1245
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            r13.ePort = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1245:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x12a8
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x12a8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x12a8
            int r4 = r13.lPort
            if (r4 <= r10) goto L_0x12a8
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lPort
            int r4 = r4 + 1
            r13.lPort = r4
            r13.ePort = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x12a8:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1313
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x1313
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x1313
            int r4 = r13.lIce
            if (r4 != r10) goto L_0x1313
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1313:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1378
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x1378
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x1378
            int r4 = r13.lIce
            if (r4 <= r10) goto L_0x1378
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1378:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x13e3
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x13e3
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x13e3
            int r4 = r13.lIce
            if (r4 != r10) goto L_0x13e3
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x13e3:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x1448
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x1448
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r11) goto L_0x1448
            int r4 = r13.lIce
            if (r4 <= r10) goto L_0x1448
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            int r7 = r3 - r8
            r6 = r6[r7]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r11
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            int r4 = r13.lIce
            int r4 = r4 + 1
            r13.lIce = r4
            r13.eIce = r8
            r13.eButtons()
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
        L_0x1448:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x148c
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x148c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x148c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            r13.action(r4)
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r0 = 1
        L_0x148c:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x14d0
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x14d0
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x14d0
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r13.action(r4)
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r0 = 1
        L_0x14d0:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x14e0
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1530
        L_0x14e0:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x14f5
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1530
        L_0x14f5:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 == 0) goto L_0x150a
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x1530
        L_0x150a:
            r4 = 7
            if (r3 >= r4) goto L_0x1530
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
        L_0x1530:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x1540
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1598
        L_0x1540:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x1555
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1598
        L_0x1555:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == 0) goto L_0x156e
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x1598
        L_0x156e:
            r4 = 7
            if (r2 >= r4) goto L_0x1598
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
        L_0x1598:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x15a8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x15ff
        L_0x15a8:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x15bd
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x15ff
        L_0x15bd:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == 0) goto L_0x15d6
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x15ff
        L_0x15d6:
            if (r2 <= r8) goto L_0x15ff
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
        L_0x15ff:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x160f
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x163f
        L_0x160f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x163f
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x163f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x164f
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x168f
        L_0x164f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x168f
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x168f
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x168f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x16a0
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x16e5
        L_0x16a0:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x16b5
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x16e5
        L_0x16b5:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != 0) goto L_0x16e5
            r4 = 7
            if (r3 >= r4) goto L_0x16e5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
        L_0x16e5:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x16f6
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1741
        L_0x16f6:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x170b
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1741
        L_0x170b:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1741
            r4 = 7
            if (r2 >= r4) goto L_0x1741
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
        L_0x1741:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x1752
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x179c
        L_0x1752:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 == r5) goto L_0x1767
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x179c
        L_0x1767:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x179c
            if (r2 <= r8) goto L_0x179c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
        L_0x179c:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x17ad
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x17db
        L_0x17ad:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x17db
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x17db:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x17ec
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x182c
        L_0x17ec:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x182c
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x182c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x182c:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x186d
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x186d
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x186d
            r4 = 7
            if (r3 >= r4) goto L_0x186d
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x186d:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x18ae
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x18ae
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x18ae
            r4 = 7
            if (r2 >= r4) goto L_0x18ae
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
        L_0x18ae:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x18ed
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x18ed
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x18ed
            if (r2 <= r8) goto L_0x18ed
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
        L_0x18ed:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x192d
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x192d
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x192d
            if (r3 <= r8) goto L_0x192d
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x192d:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x195d
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x195d
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x195d
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
            r13.regas = r8
        L_0x195d:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x198c
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x198c
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x198c
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
            r13.regas = r8
        L_0x198c:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x19bc
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x19bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x19bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r13.regas = r8
        L_0x19bc:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x19ec
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x19ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x19ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r13.regas = r8
        L_0x19ec:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1a1e
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1a1e
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x1a1e
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1a1e
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
            r13.regas = r8
        L_0x1a1e:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1a4f
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1a4f
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            r5 = 4
            if (r4 == r5) goto L_0x1a4f
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1a4f
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
            r13.regas = r8
        L_0x1a4f:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1a81
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1a81
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 4
            if (r4 == r5) goto L_0x1a81
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1a81
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r13.regas = r8
        L_0x1a81:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1ab3
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1ab3
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 4
            if (r4 == r5) goto L_0x1ab3
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1ab3
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r13.regas = r8
        L_0x1ab3:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1af6
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1af6
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1af6
            int[][] r4 = r13.gas
            int r5 = r2 + 2
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x1add
            int[][] r4 = r13.type
            int r5 = r2 + 2
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x1af6
        L_0x1add:
            int[][] r4 = r13.type
            int r5 = r2 + 2
            r4 = r4[r5]
            r4[r3] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 2
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 2
            r13.action(r4)
            r13.regas = r8
        L_0x1af6:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1b41
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1b41
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1b41
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1b24
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1b41
        L_0x1b24:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
            r13.regas = r8
        L_0x1b41:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1b8c
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1b8c
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1b8c
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1b6f
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1b8c
        L_0x1b6f:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 6
            int r4 = r4 - r5
            r13.action(r4)
            r13.regas = r8
        L_0x1b8c:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1bce
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1bce
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1bce
            int[][] r4 = r13.gas
            int r5 = r2 - r12
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x1bb6
            int[][] r4 = r13.type
            int r5 = r2 - r12
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x1bce
        L_0x1bb6:
            int[][] r4 = r13.type
            int r5 = r2 - r12
            r4 = r4[r5]
            r4[r3] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r12
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r12
            r13.action(r4)
            r13.regas = r8
        L_0x1bce:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1c19
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1c19
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1c19
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1bfc
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1c19
        L_0x1bfc:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
            r13.regas = r8
        L_0x1c19:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1c63
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1c63
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1c63
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1c47
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1c63
        L_0x1c47:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 - r11
            r13.action(r4)
            r13.regas = r8
        L_0x1c63:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1ca7
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1ca7
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1ca7
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r12
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1c8d
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r12
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1ca7
        L_0x1c8d:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r12
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 - r12
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 14
            int r4 = r4 - r5
            r13.action(r4)
            r13.regas = r8
        L_0x1ca7:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1cf2
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1cf2
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1cf2
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1cd5
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1cf2
        L_0x1cd5:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 6
            int r4 = r4 - r5
            r13.action(r4)
            r13.regas = r8
        L_0x1cf2:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1d3c
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1d3c
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1d3c
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1d20
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1d3c
        L_0x1d20:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 - r11
            r13.action(r4)
            r13.regas = r8
        L_0x1d3c:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1d7f
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1d7f
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1d7f
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1d66
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1d7f
        L_0x1d66:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
            r13.regas = r8
        L_0x1d7f:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1dca
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1dca
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1dca
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1dad
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1dca
        L_0x1dad:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
            r13.regas = r8
        L_0x1dca:
            int r4 = r13.regas
            if (r4 != 0) goto L_0x1e15
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x1e15
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1e15
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1df8
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1e15
        L_0x1df8:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
            r13.regas = r8
        L_0x1e15:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1e5f
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x1e5f
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x1e4e
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x1e4e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x1e4e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1e5f
        L_0x1e4e:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x1e5f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1e9c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1e9c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1e9c
            r4 = 7
            if (r3 >= r4) goto L_0x1e9c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
        L_0x1e9c:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1edf
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1edf
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1edf
            r4 = 7
            if (r2 >= r4) goto L_0x1edf
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
        L_0x1edf:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1f21
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1f21
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x1f21
            if (r2 <= r8) goto L_0x1f21
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
        L_0x1f21:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1f4b
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x1f4b
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x1f4b:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1f68
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x1f68
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x1f68:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1f97
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x1f97
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = -2
            r4[r3] = r5
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x1f97:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x1fcb
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x1fcb
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = -2
            r4[r3] = r5
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r4 = 5
            r13.Sound(r4)
        L_0x1fcb:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x200c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x200c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r4 = 5
            r13.Sound(r4)
        L_0x200c:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2054
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2054
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2054
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            r13.action(r4)
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r4 = 3
            r13.Sound(r4)
            r0 = 1
        L_0x2054:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x209c
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x209c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x209c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r13.action(r4)
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r4 = 3
            r13.Sound(r4)
            r0 = 1
        L_0x209c:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x20ea
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x20ea
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 == r5) goto L_0x20c6
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 == r5) goto L_0x20c6
            r4 = 7
            if (r3 != r4) goto L_0x20ea
        L_0x20c6:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x20ea
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x20de
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x20ea
        L_0x20de:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
        L_0x20ea:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x213e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x213e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 == r5) goto L_0x2113
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 == r5) goto L_0x2113
            if (r3 != r8) goto L_0x213e
        L_0x2113:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x213e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x212c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x213e
        L_0x212c:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.gravity
            r4 = r4[r2]
            r4[r3] = r10
        L_0x213e:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2174
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x2174
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2174
            if (r0 != 0) goto L_0x2174
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r0 = 1
        L_0x2174:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x21aa
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x21aa
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x21aa
            if (r0 != 0) goto L_0x21aa
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r0 = 1
        L_0x21aa:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x21e0
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x21e0
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x21e0
            if (r0 != 0) goto L_0x21e0
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
            r0 = 1
        L_0x21e0:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2215
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x2215
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x2215
            if (r0 != 0) goto L_0x2215
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
            r0 = 1
        L_0x2215:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x224a
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x224a
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x224a
            if (r1 != 0) goto L_0x224a
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
            r1 = 1
        L_0x224a:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2280
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x2280
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x2280
            if (r1 != 0) goto L_0x2280
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r5 = 4
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
            r1 = 1
        L_0x2280:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x22b6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x22b6
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x22b6
            if (r1 != 0) goto L_0x22b6
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r1 = 1
        L_0x22b6:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x22ec
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x22ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x22ec
            if (r1 != 0) goto L_0x22ec
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 4
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r1 = 1
        L_0x22ec:
            if (r0 == r8) goto L_0x22f0
            if (r1 != r8) goto L_0x22fa
        L_0x22f0:
            int r4 = r13.reFire
            if (r4 != 0) goto L_0x22fa
            r4 = 3
            r13.Sound(r4)
            r13.reFire = r8
        L_0x22fa:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x234b
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x234b
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != r11) goto L_0x234b
            r4 = 7
            if (r3 >= r4) goto L_0x234b
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x234b
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x234b
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r12
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r11
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x234b:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x23a2
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x23a2
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != r11) goto L_0x23a2
            r4 = 7
            if (r3 >= r4) goto L_0x23a2
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x23a2
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x23a2
            int[][] r4 = r13.portal
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r12
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r11
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x23a2:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x23fb
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x23fb
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x23fb
            r4 = 7
            if (r3 >= r4) goto L_0x23fb
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x23fb
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x23fb
            int[][] r4 = r13.portal
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x23fb:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x245a
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x245a
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x245a
            r4 = 7
            if (r3 >= r4) goto L_0x245a
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x245a
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x245a
            int[][] r4 = r13.portal
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x245a:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x24b3
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x24b3
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x24b3
            r4 = 7
            if (r3 >= r4) goto L_0x24b3
            int r4 = r13.lPlant
            if (r4 <= r10) goto L_0x24b3
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x24b3
            int[][] r4 = r13.portal
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x24b3:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2512
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2512
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r11) goto L_0x2512
            r4 = 7
            if (r3 >= r4) goto L_0x2512
            int r4 = r13.lPlant
            if (r4 != r10) goto L_0x2512
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x2512
            int[][] r4 = r13.portal
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r11
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
            int r4 = r13.addPlant
            int r4 = r4 + 1
            r13.addPlant = r4
        L_0x2512:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2533
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2533
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2533
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x2533:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2572
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2572
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4 = r4[r5]
            if (r4 != 0) goto L_0x2572
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x2572
            r4 = 7
            if (r3 >= r4) goto L_0x2572
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r12
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 2
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 14
            r13.action(r4)
        L_0x2572:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x25b7
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x25b7
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x25b7
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x25b7
            r4 = 7
            if (r2 >= r4) goto L_0x25b7
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 8
            r13.action(r4)
        L_0x25b7:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x25fb
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x25fb
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x25fb
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x25fb
            if (r2 <= r8) goto L_0x25fb
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 6
            r13.action(r4)
        L_0x25fb:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2627
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2627
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x2627
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r12
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x2627:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2666
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2666
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r12
            r4 = r4[r5]
            if (r4 != 0) goto L_0x2666
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2666
            if (r3 <= r8) goto L_0x2666
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r12
            r4[r5] = r12
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r12
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 14
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2666:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x26ab
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x26ab
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x26ab
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x26ab
            r4 = 7
            if (r2 >= r4) goto L_0x26ab
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r12
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 6
            int r4 = r4 - r5
            r13.action(r4)
        L_0x26ab:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x26ee
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x26ee
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x26ee
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x26ee
            if (r2 <= r8) goto L_0x26ee
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r12
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 - r11
            r13.action(r4)
        L_0x26ee:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2720
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2720
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2720
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r12
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x2720:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2746
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2746
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2746
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 5
            r13.Sound(r4)
        L_0x2746:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2768
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2768
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2768
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x2768:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x278e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x278e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x278e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 5
            r13.Sound(r4)
        L_0x278e:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x27b0
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x27b0
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x27b0
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x27b0:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x27cc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x27cc
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
        L_0x27cc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x27ee
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x27ee
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 7
            if (r4 != r5) goto L_0x27ee
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x27ee:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2810
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x2810
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2810
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x2810:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x2835
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 7
            if (r4 != r5) goto L_0x2835
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 == r5) goto L_0x2835
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
        L_0x2835:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x28bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x28bc
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x28bc
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 7
            r13.Sound(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 5
            r4[r3] = r5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x28bc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2943
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x2943
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x2943
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.gas
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.gas2
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.gas
            int r5 = r2 + 1
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.gas2
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.gas
            int r5 = r2 - r8
            r4 = r4[r5]
            int r5 = r3 - r8
            r4[r5] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 7
            r13.Sound(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 5
            r4[r3] = r5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2943:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2992
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2992
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2992
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 7
            r13.Sound(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 5
            r4[r3] = r5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2992:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x29e0
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x29e0
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x29e0
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            r4 = 7
            r13.Sound(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 5
            r4[r3] = r5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
        L_0x29e0:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r10) goto L_0x2a2a
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2a2a
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x2a2a
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 < 0) goto L_0x2a2a
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2a2a:
            int[][] r4 = r13.gravity
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2a74
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2a74
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x2a74
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 < 0) goto L_0x2a74
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2a74:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r10) goto L_0x2aac
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2aac
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x2aac
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = -6
            if (r4 > r5) goto L_0x2aac
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2aac:
            int[][] r4 = r13.gravity
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2ae4
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2ae4
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x2ae4
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = -6
            if (r4 > r5) goto L_0x2ae4
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2ae4:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2b32
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2b32
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r8) goto L_0x2b32
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 < 0) goto L_0x2b32
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r10
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2b32:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2b80
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2b80
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r8) goto L_0x2b80
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 < 0) goto L_0x2b80
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2b80:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2bab
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2bab
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2bab
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x2bab
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2bab
            r13.Sound(r8)
        L_0x2bab:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2c02
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2c02
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2c02
            int r4 = r3 + 1
            r5 = 7
            if (r4 >= r5) goto L_0x2c02
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2c02
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r13.rerock = r8
        L_0x2c02:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2c51
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2c51
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2c51
            int r4 = r3 + 1
            r5 = 6
            if (r4 <= r5) goto L_0x2c51
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2c51
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            r13.rerock = r8
        L_0x2c51:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2c7c
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2c7c
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2c7c
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r10) goto L_0x2c7c
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2c7c
            r13.Sound(r8)
        L_0x2c7c:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2cd2
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2cd2
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2cd2
            int r4 = r3 - r8
            if (r4 <= r8) goto L_0x2cd2
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2cd2
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r10
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r13.rerock = r8
        L_0x2cd2:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2d20
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2d20
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x2d20
            int r4 = r3 - r8
            if (r4 >= r12) goto L_0x2d20
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2d20
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r10
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 6
            r4[r5] = r6
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
            r13.rerock = r8
        L_0x2d20:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2d67
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2d67
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2d67
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x2d67
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x2d67
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            r13.Sound(r12)
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2d67:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2daf
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2daf
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2daf
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2daf
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x2daf
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 3
            r4[r5] = r6
            r13.Sound(r12)
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2daf:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2df6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2df6
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2df6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x2df6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2df6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            r13.Sound(r12)
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2df6:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2e3e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2e3e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 6
            if (r4 != r5) goto L_0x2e3e
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2e3e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2e3e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 3
            r4[r5] = r6
            r13.Sound(r12)
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2e3e:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2e8e
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2e8e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2e8e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2e8e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x2e8e:
            int r4 = r13.rerock
            if (r4 != 0) goto L_0x2edf
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x2edf
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != r12) goto L_0x2edf
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x2edf
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 6
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = -2
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x2edf:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 <= r5) goto L_0x2f0b
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2f0b
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x2f0b
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2f0b:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r11) goto L_0x2f28
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2f28
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2f28:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -8
            if (r4 >= r5) goto L_0x2f53
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2f53
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2f53:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2f82
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2f82
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -4
            if (r4 != r5) goto L_0x2f82
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 - r8
            r4[r3] = r5
            int r4 = r13.i2
            r13.action(r4)
        L_0x2f82:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x2f9d
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x2f9d
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 - r8
            r4[r3] = r5
        L_0x2f9d:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x2fc7
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2fc7
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x2fc7
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2fc7:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 <= r5) goto L_0x2ff3
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 4
            if (r4 != r5) goto L_0x2ff3
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x2ff3
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x2ff3:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 <= r5) goto L_0x300f
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x300f
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x300f:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 <= r5) goto L_0x302c
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x302c
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
        L_0x302c:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -35
            if (r4 != r5) goto L_0x3053
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x3053
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            r13.item = r9
            r13.portwait = r9
        L_0x3053:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x3075
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r11) goto L_0x3075
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 - r8
            r4[r3] = r5
            int r4 = r13.i2
            r13.action(r4)
            r13.item = r9
            r13.portwait = r8
        L_0x3075:
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x3090
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 == r5) goto L_0x3090
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 + 1
            r4[r3] = r5
        L_0x3090:
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x30b5
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == r8) goto L_0x30b5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 + 1
            r4[r3] = r5
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x30b5:
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x30da
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x30da
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 + 1
            r4[r3] = r5
            int[][] r4 = r13.ani
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x30da:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x30f1
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x30f1
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
        L_0x30f1:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x312f
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x312f
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 3
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r8
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
            int r4 = r13.i2
            r13.action(r4)
        L_0x312f:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x3179
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x3148
            int r4 = r3 - r8
            if (r4 == 0) goto L_0x3148
            if (r3 > r8) goto L_0x3179
        L_0x3148:
            int r4 = r13.grav
            if (r4 != r8) goto L_0x3179
            int r4 = r13.resteam
            r5 = 4
            if (r4 != r5) goto L_0x3179
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x3179:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x31cd
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x31cd
            int r4 = r13.grav
            if (r4 != r8) goto L_0x31cd
            int r4 = r13.resteam
            r5 = 3
            if (r4 != r5) goto L_0x31cd
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 5
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.stage
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x31cd:
            int r4 = r13.resteam
            r5 = 5
            if (r4 != r5) goto L_0x31d4
            r13.resteam = r9
        L_0x31d4:
            int r4 = r13.resteam
            int r4 = r4 + 1
            r13.resteam = r4
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x3228
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x3228
            int r4 = r13.grav
            if (r4 != r10) goto L_0x3228
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 5
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            int[][] r6 = r13.stage
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x3228:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 5
            if (r4 != r5) goto L_0x3254
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x323f
            int r4 = r3 + 1
            if (r4 != 0) goto L_0x3254
        L_0x323f:
            int r4 = r13.grav
            if (r4 != r10) goto L_0x3254
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
        L_0x3254:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x329d
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == 0) goto L_0x326f
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x329d
        L_0x326f:
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x329d
            int[][] r4 = r13.fall
            int r5 = r2 + 1
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x329d
            int[][] r4 = r13.type
            int r5 = r2 + 1
            r4 = r4[r5]
            r5 = 9
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 + 1
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 + 1
            r13.action(r4)
        L_0x329d:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x32e5
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == 0) goto L_0x32b8
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x32e5
        L_0x32b8:
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x32e5
            int[][] r4 = r13.fall
            int r5 = r2 - r8
            r4 = r4[r5]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x32e5
            int[][] r4 = r13.type
            int r5 = r2 - r8
            r4 = r4[r5]
            r5 = 9
            r4[r3] = r5
            int[][] r4 = r13.stage
            int r5 = r2 - r8
            r4 = r4[r5]
            r4[r3] = r8
            int r4 = r13.i2
            int r4 = r4 - r8
            r13.action(r4)
        L_0x32e5:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x332e
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == 0) goto L_0x3300
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x332e
        L_0x3300:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x332e
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 != 0) goto L_0x332e
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r8
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x332e:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 9
            if (r4 != r5) goto L_0x3377
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 == 0) goto L_0x3349
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x3377
        L_0x3349:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x3377
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 != 0) goto L_0x3377
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = 9
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r8
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x3377:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x33c2
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == 0) goto L_0x3398
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x33c2
        L_0x3398:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x33c2
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= 0) goto L_0x33c2
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 9
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x33c2:
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 9
            if (r4 != r5) goto L_0x340d
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == 0) goto L_0x33e3
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 3
            if (r4 != r5) goto L_0x340d
        L_0x33e3:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 3
            if (r4 != r5) goto L_0x340d
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 >= 0) goto L_0x340d
            int[][] r4 = r13.type
            r4 = r4[r2]
            r5 = 9
            r4[r3] = r5
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r8
            int r4 = r13.i2
            r13.action(r4)
        L_0x340d:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x3430
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x3430
            r4 = 6
            if (r3 <= r4) goto L_0x3430
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            r4 = 6
            r13.Sound(r4)
        L_0x3430:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x3496
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x3496
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == 0) goto L_0x3460
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x3460
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 5
            if (r4 != r5) goto L_0x3496
        L_0x3460:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            int[][] r6 = r13.type
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r9
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x3496:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r8) goto L_0x34eb
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == 0) goto L_0x34bd
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            if (r4 == r8) goto L_0x34bd
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            r4 = r4[r5]
            r5 = 5
            if (r4 != r5) goto L_0x34eb
        L_0x34bd:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 + 1
            int[][] r6 = r13.type
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 + 1
            r4[r5] = r12
            int r4 = r13.i2
            int r4 = r4 + 7
            r13.action(r4)
        L_0x34eb:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x34fc
            r4 = 7
            if (r3 >= r4) goto L_0x34fc
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r8
        L_0x34fc:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r12) goto L_0x350d
            r4 = 6
            if (r3 <= r4) goto L_0x350d
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x350d:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x3530
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x3530
            if (r3 > r8) goto L_0x3530
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            r4 = 6
            r13.Sound(r4)
        L_0x3530:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x3597
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 6
            if (r4 != r5) goto L_0x3597
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == 0) goto L_0x3560
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x3560
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 5
            if (r4 != r5) goto L_0x3597
        L_0x3560:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = -2
            r4[r5] = r6
            int[][] r4 = r13.stage
            r4 = r4[r2]
            int r5 = r3 - r8
            r4[r5] = r9
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x3597:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != r10) goto L_0x35ed
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == 0) goto L_0x35be
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            if (r4 == r8) goto L_0x35be
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            r4 = r4[r5]
            r5 = 5
            if (r4 != r5) goto L_0x35ed
        L_0x35be:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.type
            r4 = r4[r2]
            int r5 = r3 - r8
            int[][] r6 = r13.type
            r6 = r6[r2]
            r6 = r6[r3]
            r4[r5] = r6
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4[r3] = r9
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.fall
            r4 = r4[r2]
            int r5 = r3 - r8
            r6 = -2
            r4[r5] = r6
            int r4 = r13.i2
            r5 = 7
            int r4 = r4 - r5
            r13.action(r4)
        L_0x35ed:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x35fe
            if (r3 <= r8) goto L_0x35fe
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r10
        L_0x35fe:
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = -2
            if (r4 != r5) goto L_0x360f
            if (r3 != r8) goto L_0x360f
            int[][] r4 = r13.fall
            r4 = r4[r2]
            r4[r3] = r9
        L_0x360f:
            int[][] r4 = r13.onedrop
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 <= r8) goto L_0x3630
            int[][] r4 = r13.onedrop
            r4 = r4[r2]
            r4 = r4[r3]
            r5 = 22
            if (r4 >= r5) goto L_0x3630
            int r4 = r13.i2
            r13.action(r4)
            int[][] r4 = r13.onedrop
            r4 = r4[r2]
            r5 = r4[r3]
            int r5 = r5 + 1
            r4[r3] = r5
        L_0x3630:
            int[][] r4 = r13.type
            r4 = r4[r2]
            r4 = r4[r3]
            if (r4 != 0) goto L_0x3650
            int[][] r4 = r13.stage
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.gravity
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.gas
            r4 = r4[r2]
            r4[r3] = r9
            int[][] r4 = r13.gas2
            r4 = r4[r2]
            r4[r3] = r9
        L_0x3650:
            r4 = 7
            if (r2 != r4) goto L_0x36e3
            r4 = 7
            if (r3 != r4) goto L_0x36e3
            int[][] r4 = r13.type
            r4 = r4[r8]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r4 = r4[r8]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r4 = r4[r8]
            r4[r11] = r9
            int[][] r4 = r13.type
            r4 = r4[r12]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r4 = r4[r12]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r4 = r4[r12]
            r4[r11] = r9
            int[][] r4 = r13.type
            r5 = 3
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r5 = 3
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r5 = 3
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.type
            r5 = 4
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r5 = 4
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r5 = 4
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.type
            r5 = 5
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r5 = 5
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r5 = 5
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.type
            r5 = 6
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r5 = 6
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r5 = 6
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.type
            r5 = 7
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas
            r5 = 7
            r4 = r4[r5]
            r4[r11] = r9
            int[][] r4 = r13.gas2
            r5 = 7
            r4 = r4[r5]
            r4[r11] = r9
        L_0x36e3:
            int r2 = r2 + 1
            if (r2 != r11) goto L_0x0045
            r2 = 1
            int r3 = r3 + 1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hyperbyte.causeandeffectbeta.game.Check():void");
    }

    public void complete() {
        save();
        Sound(9);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("");
        if (this.foc == 1 && level < 20) {
            alert.setMessage("Level Complete!!!");
        }
        if (this.foc == 1 && level == 20) {
            alert.setMessage("You have completed the first 20 levels!");
        }
        if (this.foc == 1 && level < 20) {
            alert.setPositiveButton("Next Level!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    game.this.completeC = 0;
                    game.this.item = 0;
                    game.this.complete = 0;
                    game.this.foc = 0;
                    game.level++;
                    game.this.level();
                }
            });
        }
        if (this.foc == 1 && level == 20) {
            alert.setPositiveButton("MORE LEVELS!!!", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    game.this.completeC = 0;
                    game.this.item = 0;
                    game.this.complete = 0;
                    game.this.foc = 0;
                    game.level = 0;
                    game.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.hyperbyte.chaos")));
                    game.ref = 1;
                    game.ref2 = 1;
                    game.exit = 2;
                    game.ref3 = 1;
                }
            });
        }
        alert.show();
    }

    public void save() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences.Editor prefEditor = getSharedPreferences("MyGamePreferences", 0).edit();
        if (world == 1) {
            if (level == 1) {
                prefEditor.putBoolean("p1_l2", true);
                prefEditor.commit();
            }
            if (level == 2) {
                prefEditor.putBoolean("p1_l3", true);
                prefEditor.commit();
            }
            if (level == 3) {
                prefEditor.putBoolean("p1_l4", true);
                prefEditor.commit();
            }
            if (level == 4) {
                prefEditor.putBoolean("p1_l5", true);
                prefEditor.commit();
            }
            if (level == 5) {
                prefEditor.putBoolean("p1_l6", true);
                prefEditor.commit();
            }
            if (level == 6) {
                prefEditor.putBoolean("p1_l7", true);
                prefEditor.commit();
            }
            if (level == 7) {
                prefEditor.putBoolean("p1_l8", true);
                prefEditor.commit();
            }
            if (level == 8) {
                prefEditor.putBoolean("p1_l9", true);
                prefEditor.commit();
            }
            if (level == 9) {
                prefEditor.putBoolean("p1_l10", true);
                prefEditor.commit();
            }
            if (level == 10) {
                prefEditor.putBoolean("p1_l11", true);
                prefEditor.commit();
            }
            if (level == 11) {
                prefEditor.putBoolean("p1_l12", true);
                prefEditor.commit();
            }
            if (level == 12) {
                prefEditor.putBoolean("p1_l13", true);
                prefEditor.commit();
            }
            if (level == 13) {
                prefEditor.putBoolean("p1_l14", true);
                prefEditor.commit();
            }
            if (level == 14) {
                prefEditor.putBoolean("p1_l15", true);
                prefEditor.commit();
            }
            if (level == 15) {
                prefEditor.putBoolean("p1_l16", true);
                prefEditor.commit();
            }
            if (level == 16) {
                prefEditor.putBoolean("p1_l17", true);
                prefEditor.commit();
            }
            if (level == 17) {
                prefEditor.putBoolean("p1_l18", true);
                prefEditor.commit();
            }
            if (level == 18) {
                prefEditor.putBoolean("p1_l19", true);
                prefEditor.commit();
            }
            if (level == 19) {
                prefEditor.putBoolean("p1_l20", true);
                prefEditor.commit();
            }
            if (level == 20) {
                prefEditor.putBoolean("p2_l1", true);
                prefEditor.commit();
            }
        }
        if (world == 2) {
            if (level == 1) {
                prefEditor.putBoolean("p2_l2", true);
                prefEditor.commit();
            }
            if (level == 2) {
                prefEditor.putBoolean("p2_l3", true);
                prefEditor.commit();
            }
            if (level == 3) {
                prefEditor.putBoolean("p2_l4", true);
                prefEditor.commit();
            }
            if (level == 4) {
                prefEditor.putBoolean("p2_l5", true);
                prefEditor.commit();
            }
            if (level == 5) {
                prefEditor.putBoolean("p2_l6", true);
                prefEditor.commit();
            }
            if (level == 6) {
                prefEditor.putBoolean("p2_l7", true);
                prefEditor.commit();
            }
            if (level == 7) {
                prefEditor.putBoolean("p2_l8", true);
                prefEditor.commit();
            }
            if (level == 8) {
                prefEditor.putBoolean("p2_l9", true);
                prefEditor.commit();
            }
            if (level == 9) {
                prefEditor.putBoolean("p2_l10", true);
                prefEditor.commit();
            }
            if (level == 10) {
                prefEditor.putBoolean("p2_l11", true);
                prefEditor.commit();
            }
            if (level == 11) {
                prefEditor.putBoolean("p2_l12", true);
                prefEditor.commit();
            }
            if (level == 12) {
                prefEditor.putBoolean("p2_l13", true);
                prefEditor.commit();
            }
            if (level == 13) {
                prefEditor.putBoolean("p2_l14", true);
                prefEditor.commit();
            }
            if (level == 14) {
                prefEditor.putBoolean("p2_l15", true);
                prefEditor.commit();
            }
            if (level == 15) {
                prefEditor.putBoolean("p2_l16", true);
                prefEditor.commit();
            }
            if (level == 16) {
                prefEditor.putBoolean("p2_l17", true);
                prefEditor.commit();
            }
            if (level == 17) {
                prefEditor.putBoolean("p2_l18", true);
                prefEditor.commit();
            }
            if (level == 18) {
                prefEditor.putBoolean("p2_l19", true);
                prefEditor.commit();
            }
            if (level == 19) {
                prefEditor.putBoolean("p2_l20", true);
                prefEditor.commit();
            }
            if (level == 20) {
                prefEditor.putBoolean("p3_l1", true);
                prefEditor.commit();
            }
        }
        if (world == 3) {
            if (level == 1) {
                prefEditor.putBoolean("p3_l2", true);
                prefEditor.commit();
            }
            if (level == 2) {
                prefEditor.putBoolean("p3_l3", true);
                prefEditor.commit();
            }
            if (level == 3) {
                prefEditor.putBoolean("p3_l4", true);
                prefEditor.commit();
            }
            if (level == 4) {
                prefEditor.putBoolean("p3_l5", true);
                prefEditor.commit();
            }
            if (level == 5) {
                prefEditor.putBoolean("p3_l6", true);
                prefEditor.commit();
            }
            if (level == 6) {
                prefEditor.putBoolean("p3_l7", true);
                prefEditor.commit();
            }
            if (level == 7) {
                prefEditor.putBoolean("p3_l8", true);
                prefEditor.commit();
            }
            if (level == 8) {
                prefEditor.putBoolean("p3_l9", true);
                prefEditor.commit();
            }
            if (level == 9) {
                prefEditor.putBoolean("p3_l10", true);
                prefEditor.commit();
            }
            if (level == 10) {
                prefEditor.putBoolean("p3_l11", true);
                prefEditor.commit();
            }
            if (level == 11) {
                prefEditor.putBoolean("p3_l12", true);
                prefEditor.commit();
            }
            if (level == 12) {
                prefEditor.putBoolean("p3_l13", true);
                prefEditor.commit();
            }
            if (level == 13) {
                prefEditor.putBoolean("p3_l14", true);
                prefEditor.commit();
            }
            if (level == 14) {
                prefEditor.putBoolean("p3_l15", true);
                prefEditor.commit();
            }
            if (level == 15) {
                prefEditor.putBoolean("p3_l16", true);
                prefEditor.commit();
            }
            if (level == 16) {
                prefEditor.putBoolean("p3_l17", true);
                prefEditor.commit();
            }
            if (level == 17) {
                prefEditor.putBoolean("p3_l18", true);
                prefEditor.commit();
            }
            if (level == 18) {
                prefEditor.putBoolean("p3_l19", true);
                prefEditor.commit();
            }
            if (level == 19) {
                prefEditor.putBoolean("p3_l20", true);
                prefEditor.commit();
            }
            if (level == 20) {
                prefEditor.putBoolean("p4_l1", true);
                prefEditor.commit();
            }
        }
        if (world == 4) {
            if (level == 1) {
                prefEditor.putBoolean("p4_l2", true);
                prefEditor.commit();
            }
            if (level == 2) {
                prefEditor.putBoolean("p4_l3", true);
                prefEditor.commit();
            }
            if (level == 3) {
                prefEditor.putBoolean("p4_l4", true);
                prefEditor.commit();
            }
            if (level == 4) {
                prefEditor.putBoolean("p4_l5", true);
                prefEditor.commit();
            }
            if (level == 5) {
                prefEditor.putBoolean("p4_l6", true);
                prefEditor.commit();
            }
            if (level == 6) {
                prefEditor.putBoolean("p4_l7", true);
                prefEditor.commit();
            }
            if (level == 7) {
                prefEditor.putBoolean("p4_l8", true);
                prefEditor.commit();
            }
            if (level == 8) {
                prefEditor.putBoolean("p4_l9", true);
                prefEditor.commit();
            }
            if (level == 9) {
                prefEditor.putBoolean("p4_l10", true);
                prefEditor.commit();
            }
            if (level == 10) {
                prefEditor.putBoolean("p4_l11", true);
                prefEditor.commit();
            }
            if (level == 11) {
                prefEditor.putBoolean("p4_l12", true);
                prefEditor.commit();
            }
            if (level == 12) {
                prefEditor.putBoolean("p4_l13", true);
                prefEditor.commit();
            }
            if (level == 13) {
                prefEditor.putBoolean("p4_l14", true);
                prefEditor.commit();
            }
            if (level == 14) {
                prefEditor.putBoolean("p4_l15", true);
                prefEditor.commit();
            }
            if (level == 15) {
                prefEditor.putBoolean("p4_l16", true);
                prefEditor.commit();
            }
            if (level == 16) {
                prefEditor.putBoolean("p4_l17", true);
                prefEditor.commit();
            }
            if (level == 17) {
                prefEditor.putBoolean("p4_l18", true);
                prefEditor.commit();
            }
            if (level == 18) {
                prefEditor.putBoolean("p4_l19", true);
                prefEditor.commit();
            }
            if (level == 19) {
                prefEditor.putBoolean("p4_l20", true);
                prefEditor.commit();
            }
            if (level == 20) {
                prefEditor.putBoolean("p4_l1", true);
                prefEditor.commit();
            }
        }
        if (world == 5) {
            if (level == 1) {
                prefEditor.putBoolean("p5_l2", true);
                prefEditor.commit();
            }
            if (level == 2) {
                prefEditor.putBoolean("p5_l3", true);
                prefEditor.commit();
            }
            if (level == 3) {
                prefEditor.putBoolean("p5_l4", true);
                prefEditor.commit();
            }
            if (level == 4) {
                prefEditor.putBoolean("p5_l5", true);
                prefEditor.commit();
            }
            if (level == 5) {
                prefEditor.putBoolean("p5_l6", true);
                prefEditor.commit();
            }
            if (level == 6) {
                prefEditor.putBoolean("p5_l7", true);
                prefEditor.commit();
            }
            if (level == 7) {
                prefEditor.putBoolean("p5_l8", true);
                prefEditor.commit();
            }
            if (level == 8) {
                prefEditor.putBoolean("p5_l9", true);
                prefEditor.commit();
            }
            if (level == 9) {
                prefEditor.putBoolean("p5_l10", true);
                prefEditor.commit();
            }
            if (level == 10) {
                prefEditor.putBoolean("p5_l11", true);
                prefEditor.commit();
            }
            if (level == 11) {
                prefEditor.putBoolean("p5_l12", true);
                prefEditor.commit();
            }
            if (level == 12) {
                prefEditor.putBoolean("p5_l13", true);
                prefEditor.commit();
            }
            if (level == 13) {
                prefEditor.putBoolean("p5_l14", true);
                prefEditor.commit();
            }
            if (level == 14) {
                prefEditor.putBoolean("p5_l15", true);
                prefEditor.commit();
            }
            if (level == 15) {
                prefEditor.putBoolean("p5_l16", true);
                prefEditor.commit();
            }
            if (level == 16) {
                prefEditor.putBoolean("p5_l17", true);
                prefEditor.commit();
            }
            if (level == 17) {
                prefEditor.putBoolean("p5_l18", true);
                prefEditor.commit();
            }
            if (level == 18) {
                prefEditor.putBoolean("p5_l19", true);
                prefEditor.commit();
            }
            if (level == 19) {
                prefEditor.putBoolean("p5_l20", true);
                prefEditor.commit();
            }
            if (level == 20) {
                prefEditor.putBoolean("p5_l1", true);
                prefEditor.commit();
            }
        }
    }

    public void fail() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("");
        alert.setMessage("Level Failed");
        alert.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (game.this.foc == 1) {
                    game.this.completeC = 0;
                    game.this.item = 0;
                    game.this.complete = 0;
                    game.this.foc = 0;
                    game.this.level();
                }
            }
        });
        alert.show();
    }

    public void start() {
        if (this.d1 == 1) {
            this.d[1][1] = 1;
        }
        if (this.d2 == 1) {
            this.d[2][1] = 1;
        }
        if (this.d3 == 1) {
            this.d[3][1] = 1;
        }
        if (this.d4 == 1) {
            this.d[4][1] = 1;
        }
        if (this.d5 == 1) {
            this.d[5][1] = 1;
        }
        if (this.d6 == 1) {
            this.d[6][1] = 1;
        }
        if (this.d7 == 1) {
            this.d[7][1] = 1;
        }
        if (this.d8 == 1) {
            this.d[1][2] = 1;
        }
        if (this.d9 == 1) {
            this.d[2][2] = 1;
        }
        if (this.d10 == 1) {
            this.d[3][2] = 1;
        }
        if (this.d11 == 1) {
            this.d[4][2] = 1;
        }
        if (this.d12 == 1) {
            this.d[5][2] = 1;
        }
        if (this.d13 == 1) {
            this.d[6][2] = 1;
        }
        if (this.d14 == 1) {
            this.d[7][2] = 1;
        }
        if (this.d15 == 1) {
            this.d[1][3] = 1;
        }
        if (this.d16 == 1) {
            this.d[2][3] = 1;
        }
        if (this.d17 == 1) {
            this.d[3][3] = 1;
        }
        if (this.d18 == 1) {
            this.d[4][3] = 1;
        }
        if (this.d19 == 1) {
            this.d[5][3] = 1;
        }
        if (this.d20 == 1) {
            this.d[6][3] = 1;
        }
        if (this.d21 == 1) {
            this.d[7][3] = 1;
        }
        if (this.d22 == 1) {
            this.d[1][4] = 1;
        }
        if (this.d23 == 1) {
            this.d[2][4] = 1;
        }
        if (this.d24 == 1) {
            this.d[3][4] = 1;
        }
        if (this.d25 == 1) {
            this.d[4][4] = 1;
        }
        if (this.d26 == 1) {
            this.d[5][4] = 1;
        }
        if (this.d27 == 1) {
            this.d[6][4] = 1;
        }
        if (this.d28 == 1) {
            this.d[7][4] = 1;
        }
        if (this.d29 == 1) {
            this.d[1][5] = 1;
        }
        if (this.d30 == 1) {
            this.d[2][5] = 1;
        }
        if (this.d31 == 1) {
            this.d[3][5] = 1;
        }
        if (this.d32 == 1) {
            this.d[4][5] = 1;
        }
        if (this.d33 == 1) {
            this.d[5][5] = 1;
        }
        if (this.d34 == 1) {
            this.d[6][5] = 1;
        }
        if (this.d35 == 1) {
            this.d[7][5] = 1;
        }
        if (this.d36 == 1) {
            this.d[1][6] = 1;
        }
        if (this.d37 == 1) {
            this.d[2][6] = 1;
        }
        if (this.d38 == 1) {
            this.d[3][6] = 1;
        }
        if (this.d39 == 1) {
            this.d[4][6] = 1;
        }
        if (this.d40 == 1) {
            this.d[5][6] = 1;
        }
        if (this.d41 == 1) {
            this.d[6][6] = 1;
        }
        if (this.d42 == 1) {
            this.d[7][6] = 1;
        }
        if (this.d43 == 1) {
            this.d[1][7] = 1;
        }
        if (this.d44 == 1) {
            this.d[2][7] = 1;
        }
        if (this.d45 == 1) {
            this.d[3][7] = 1;
        }
        if (this.d46 == 1) {
            this.d[4][7] = 1;
        }
        if (this.d47 == 1) {
            this.d[5][7] = 1;
        }
        if (this.d48 == 1) {
            this.d[6][7] = 1;
        }
        if (this.d49 == 1) {
            this.d[7][7] = 1;
        }
        while (this.i < 49) {
            this.i++;
            action(this.i);
        }
    }

    public void action(int cell) {
        Button b1 = (Button) findViewById(R.id.b1);
        Button b2 = (Button) findViewById(R.id.b2);
        Button b3 = (Button) findViewById(R.id.b3);
        Button b4 = (Button) findViewById(R.id.b4);
        Button b5 = (Button) findViewById(R.id.b5);
        Button b6 = (Button) findViewById(R.id.b6);
        Button b7 = (Button) findViewById(R.id.b7);
        Button b8 = (Button) findViewById(R.id.b8);
        Button b9 = (Button) findViewById(R.id.b9);
        Button b10 = (Button) findViewById(R.id.b10);
        Button b11 = (Button) findViewById(R.id.b11);
        Button b12 = (Button) findViewById(R.id.b12);
        Button b13 = (Button) findViewById(R.id.b13);
        Button b14 = (Button) findViewById(R.id.b14);
        Button b15 = (Button) findViewById(R.id.b15);
        Button b16 = (Button) findViewById(R.id.b16);
        Button b17 = (Button) findViewById(R.id.b17);
        Button b18 = (Button) findViewById(R.id.b18);
        Button b19 = (Button) findViewById(R.id.b19);
        Button b20 = (Button) findViewById(R.id.b20);
        Button b21 = (Button) findViewById(R.id.b21);
        Button b22 = (Button) findViewById(R.id.b22);
        Button b23 = (Button) findViewById(R.id.b23);
        Button b24 = (Button) findViewById(R.id.b24);
        Button b25 = (Button) findViewById(R.id.b25);
        Button b26 = (Button) findViewById(R.id.b26);
        Button b27 = (Button) findViewById(R.id.b27);
        Button b28 = (Button) findViewById(R.id.b28);
        Button b29 = (Button) findViewById(R.id.b29);
        Button b30 = (Button) findViewById(R.id.b30);
        Button b31 = (Button) findViewById(R.id.b31);
        Button b32 = (Button) findViewById(R.id.b32);
        Button b33 = (Button) findViewById(R.id.b33);
        Button b34 = (Button) findViewById(R.id.b34);
        Button b35 = (Button) findViewById(R.id.b35);
        Button b36 = (Button) findViewById(R.id.b36);
        Button b37 = (Button) findViewById(R.id.b37);
        Button b38 = (Button) findViewById(R.id.b38);
        Button b39 = (Button) findViewById(R.id.b39);
        Button b40 = (Button) findViewById(R.id.b40);
        Button b41 = (Button) findViewById(R.id.b41);
        Button b42 = (Button) findViewById(R.id.b42);
        Button b43 = (Button) findViewById(R.id.b43);
        Button b44 = (Button) findViewById(R.id.b44);
        Button b45 = (Button) findViewById(R.id.b45);
        Button b46 = (Button) findViewById(R.id.b46);
        Button b47 = (Button) findViewById(R.id.b47);
        Button b48 = (Button) findViewById(R.id.b48);
        Button b49 = (Button) findViewById(R.id.b49);
        Button button = b1;
        int x = 0;
        int y = 0;
        if (cell == 1) {
            x = 1;
            y = 1;
            button = b1;
        }
        if (cell == 2) {
            x = 2;
            y = 1;
            button = b2;
        }
        if (cell == 3) {
            x = 3;
            y = 1;
            button = b3;
        }
        if (cell == 4) {
            x = 4;
            y = 1;
            button = b4;
        }
        if (cell == 5) {
            x = 5;
            y = 1;
            button = b5;
        }
        if (cell == 6) {
            x = 6;
            y = 1;
            button = b6;
        }
        if (cell == 7) {
            x = 7;
            y = 1;
            button = b7;
        }
        if (cell == 8) {
            x = 1;
            y = 2;
            button = b8;
        }
        if (cell == 9) {
            x = 2;
            y = 2;
            button = b9;
        }
        if (cell == 10) {
            x = 3;
            y = 2;
            button = b10;
        }
        if (cell == 11) {
            x = 4;
            y = 2;
            button = b11;
        }
        if (cell == 12) {
            x = 5;
            y = 2;
            button = b12;
        }
        if (cell == 13) {
            x = 6;
            y = 2;
            button = b13;
        }
        if (cell == 14) {
            x = 7;
            y = 2;
            button = b14;
        }
        if (cell == 15) {
            x = 1;
            y = 3;
            button = b15;
        }
        if (cell == 16) {
            x = 2;
            y = 3;
            button = b16;
        }
        if (cell == 17) {
            x = 3;
            y = 3;
            button = b17;
        }
        if (cell == 18) {
            x = 4;
            y = 3;
            button = b18;
        }
        if (cell == 19) {
            x = 5;
            y = 3;
            button = b19;
        }
        if (cell == 20) {
            x = 6;
            y = 3;
            button = b20;
        }
        if (cell == 21) {
            x = 7;
            y = 3;
            button = b21;
        }
        if (cell == 22) {
            x = 1;
            y = 4;
            button = b22;
        }
        if (cell == 23) {
            x = 2;
            y = 4;
            button = b23;
        }
        if (cell == 24) {
            x = 3;
            y = 4;
            button = b24;
        }
        if (cell == 25) {
            x = 4;
            y = 4;
            button = b25;
        }
        if (cell == 26) {
            x = 5;
            y = 4;
            button = b26;
        }
        if (cell == 27) {
            x = 6;
            y = 4;
            button = b27;
        }
        if (cell == 28) {
            x = 7;
            y = 4;
            button = b28;
        }
        if (cell == 29) {
            x = 1;
            y = 5;
            button = b29;
        }
        if (cell == 30) {
            x = 2;
            y = 5;
            button = b30;
        }
        if (cell == 31) {
            x = 3;
            y = 5;
            button = b31;
        }
        if (cell == 32) {
            x = 4;
            y = 5;
            button = b32;
        }
        if (cell == 33) {
            x = 5;
            y = 5;
            button = b33;
        }
        if (cell == 34) {
            x = 6;
            y = 5;
            button = b34;
        }
        if (cell == 35) {
            x = 7;
            y = 5;
            button = b35;
        }
        if (cell == 36) {
            x = 1;
            y = 6;
            button = b36;
        }
        if (cell == 37) {
            x = 2;
            y = 6;
            button = b37;
        }
        if (cell == 38) {
            x = 3;
            y = 6;
            button = b38;
        }
        if (cell == 39) {
            x = 4;
            y = 6;
            button = b39;
        }
        if (cell == 40) {
            x = 5;
            y = 6;
            button = b40;
        }
        if (cell == 41) {
            x = 6;
            y = 6;
            button = b41;
        }
        if (cell == 42) {
            x = 7;
            y = 6;
            button = b42;
        }
        if (cell == 43) {
            x = 1;
            y = 7;
            button = b43;
        }
        if (cell == 44) {
            x = 2;
            y = 7;
            button = b44;
        }
        if (cell == 45) {
            x = 3;
            y = 7;
            button = b45;
        }
        if (cell == 46) {
            x = 4;
            y = 7;
            button = b46;
        }
        if (cell == 47) {
            x = 5;
            y = 7;
            button = b47;
        }
        if (cell == 48) {
            x = 6;
            y = 7;
            button = b48;
        }
        if (cell == 49) {
            x = 7;
            y = 7;
            button = b49;
        }
        draw(button, x, y);
        if (this.type[1][1] == 0 && this.d[1][1] == 0) {
            b1.setBackgroundResource(R.drawable.emptytopleft);
        }
        if (this.type[1][1] == 2 && this.d[1][1] == 0 && this.goal[1][1] == 0) {
            b1.setBackgroundResource(R.drawable.plant0);
        }
        if (this.type[1][1] == 2 && this.d[1][1] == 0 && this.goal[1][1] == 1) {
            b1.setBackgroundResource(R.drawable.firebplant);
        }
        if (this.type[1][1] == 0 && this.d[1][1] == 1 && this.onedrop[1][1] == 0) {
            b1.setBackgroundResource(R.drawable.drop);
        }
        if (this.type[1][1] == 0 && this.d[1][1] == 1 && this.onedrop[1][1] == 1) {
            b1.setBackgroundResource(R.drawable.onedrop1);
        }
    }

    public void draw(Button button, int x, int y) {
        if (this.type[x][y] == 0 && this.d[x][y] == 0) {
            button.setBackgroundResource(R.drawable.empty);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 7 && x != 1 && x != 7) {
            button.setBackgroundResource(R.drawable.emptybottom);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 7 && x == 1) {
            button.setBackgroundResource(R.drawable.emptybottomleft);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 7 && x == 7) {
            button.setBackgroundResource(R.drawable.emptybottomright);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 1 && x != 1 && x != 7) {
            button.setBackgroundResource(R.drawable.emptytop);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 1 && x == 1) {
            button.setBackgroundResource(R.drawable.emptytopleft);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && y == 1 && x == 7) {
            button.setBackgroundResource(R.drawable.emptytopright);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && x == 1 && y != 1 && y != 7) {
            button.setBackgroundResource(R.drawable.emptyleft);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && x == 7 && y != 1 && y != 7) {
            button.setBackgroundResource(R.drawable.emptyright);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 0) {
            button.setBackgroundResource(R.drawable.drop);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 1) {
            button.setBackgroundResource(R.drawable.onedrop1);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 2) {
            button.setBackgroundResource(R.drawable.onedrop2);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 3) {
            button.setBackgroundResource(R.drawable.onedrop3);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 4) {
            button.setBackgroundResource(R.drawable.onedrop4);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 5) {
            button.setBackgroundResource(R.drawable.onedrop5);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 6) {
            button.setBackgroundResource(R.drawable.onedrop6);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 7) {
            button.setBackgroundResource(R.drawable.onedrop7);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 8) {
            button.setBackgroundResource(R.drawable.onedrop8);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 9) {
            button.setBackgroundResource(R.drawable.onedrop9);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 10) {
            button.setBackgroundResource(R.drawable.onedrop10);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 11) {
            button.setBackgroundResource(R.drawable.onedrop11);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 12) {
            button.setBackgroundResource(R.drawable.onedrop12);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 13) {
            button.setBackgroundResource(R.drawable.onedrop13);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 14) {
            button.setBackgroundResource(R.drawable.onedrop14);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 15) {
            button.setBackgroundResource(R.drawable.onedrop15);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 16) {
            button.setBackgroundResource(R.drawable.onedrop16);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 17) {
            button.setBackgroundResource(R.drawable.onedrop17);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 18) {
            button.setBackgroundResource(R.drawable.onedrop18);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 19) {
            button.setBackgroundResource(R.drawable.onedrop19);
        }
        if (this.type[x][y] == 0 && this.d[x][y] == 0 && this.onedrop[x][y] == 20) {
            button.setBackgroundResource(R.drawable.onedrop20);
        }
        if (this.type[x][y] == 1 && this.stage[x][y] == 0) {
            button.setBackgroundResource(R.drawable.fire1);
        }
        if (this.type[x][y] == 1 && (this.stage[x][y] == 1 || this.stage[x][y] == 5)) {
            button.setBackgroundResource(R.drawable.fire2);
        }
        if (this.type[x][y] == 1 && (this.stage[x][y] == 2 || this.stage[x][y] == 4)) {
            button.setBackgroundResource(R.drawable.fire3);
        }
        if (this.type[x][y] == 1 && (this.stage[x][y] == 3 || this.stage[x][y] == 6)) {
            button.setBackgroundResource(R.drawable.fire4);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 2) {
            button.setBackgroundResource(R.drawable.burn1);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 3) {
            button.setBackgroundResource(R.drawable.burn2);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 4) {
            button.setBackgroundResource(R.drawable.burn3);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 5) {
            button.setBackgroundResource(R.drawable.burn4);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 6) {
            button.setBackgroundResource(R.drawable.burn5);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 7) {
            button.setBackgroundResource(R.drawable.burn6);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 8) {
            button.setBackgroundResource(R.drawable.burn7);
        }
        if (this.type[x][y] == 4 && this.stage[x][y] == 9) {
            button.setBackgroundResource(R.drawable.burn8);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 0 && this.gas[x][y] == 0) {
            button.setBackgroundResource(R.drawable.plant0);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 0 && this.gas[x][y] == 1) {
            button.setBackgroundResource(R.drawable.plantg);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 2) {
            button.setBackgroundResource(R.drawable.plant1);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 3) {
            button.setBackgroundResource(R.drawable.plant2);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 4) {
            button.setBackgroundResource(R.drawable.plant3);
        }
        if (this.type[x][y] == 2 && this.stage[x][y] == 5) {
            button.setBackgroundResource(R.drawable.plant4);
        }
        if (this.type[x][y] == 3 && this.stage[x][y] != -1) {
            button.setBackgroundResource(R.drawable.water1);
        }
        if (this.type[x][y] == 3 && this.stage[x][y] == -1 && this.gas[x][y] == 0) {
            button.setBackgroundResource(R.drawable.water2);
        }
        if (this.type[x][y] == 6 && this.stage[x][y] == 0) {
            button.setBackgroundResource(R.drawable.rock1);
        }
        if (this.type[x][y] == 6 && this.stage[x][y] == 1) {
            button.setBackgroundResource(R.drawable.rock2);
        }
        if (this.type[x][y] == 6 && this.stage[x][y] == 3) {
            button.setBackgroundResource(R.drawable.rock2plant);
        }
        if (this.type[x][y] == 6 && this.stage[x][y] == -1) {
            button.setBackgroundResource(R.drawable.rockwater);
        }
        if (this.type[x][y] == 6 && this.stage[x][y] == 2) {
            button.setBackgroundResource(R.drawable.rockplant);
        }
        if (this.type[x][y] == 5 && this.stage[x][y] == 0) {
            button.setBackgroundResource(R.drawable.steam1);
        }
        if (this.type[x][y] == 5 && this.stage[x][y] == -1) {
            button.setBackgroundResource(R.drawable.steam2);
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 0 && this.lFire != 0 && this.item == 1) {
            this.type[x][y] = 1;
            button.setBackgroundResource(R.drawable.fire1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.fireD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 0 && this.lWater != 0 && this.item == 2) {
            this.type[x][y] = 3;
            button.setBackgroundResource(R.drawable.water1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.waterD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 0 && this.lPlant != 0 && this.item == 3) {
            this.type[x][y] = 2;
            button.setBackgroundResource(R.drawable.plant0);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.plantD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 0 && this.lRock != 0 && this.item == 4) {
            this.type[x][y] = 6;
            button.setBackgroundResource(R.drawable.rock1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.rockD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 1 && this.lFire != 0 && this.item == 1) {
            this.type[x][y] = 1;
            this.d[x][y] = 0;
            this.onedrop[x][y] = 2;
            button.setBackgroundResource(R.drawable.fire1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.fireD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 1 && this.lWater != 0 && this.item == 2) {
            this.type[x][y] = 3;
            this.d[x][y] = 0;
            this.onedrop[x][y] = 2;
            button.setBackgroundResource(R.drawable.water1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.waterD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 1 && this.lPlant != 0 && this.item == 3) {
            this.type[x][y] = 2;
            this.d[x][y] = 0;
            this.onedrop[x][y] = 2;
            button.setBackgroundResource(R.drawable.plant0);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.plantD = 1;
        }
        if (this.type[x][y] == 0 && this.newclick == 0 && this.d[x][y] == 1 && this.onedrop[x][y] == 1 && this.lRock != 0 && this.item == 4) {
            this.type[x][y] = 6;
            this.d[x][y] = 0;
            this.onedrop[x][y] = 2;
            button.setBackgroundResource(R.drawable.rock1);
            this.fall[x][y] = 1;
            this.item = 0;
            this.newclick = 1;
            this.rockD = 1;
        }
        if (this.fireD == 1 && this.lFire > 0) {
            this.lFire--;
            this.refresh = 1;
        }
        if (this.lFire == 0) {
            this.eFire = 0;
        }
        if (this.plantD == 1 && this.lPlant > 0) {
            this.lPlant--;
            this.refresh = 1;
        }
        if (this.lPlant == 0) {
            this.ePlant = 0;
        }
        if (this.waterD == 1 && this.lWater > 0) {
            this.lWater--;
            this.refresh = 1;
        }
        if (this.lWater == 0) {
            this.eWater = 0;
        }
        if (this.rockD == 1 && this.lRock > 0) {
            this.lRock--;
            this.refresh = 1;
        }
        if (this.lRock == 0) {
            this.eRock = 0;
        }
        if (this.gasD == 1 && this.lGas > 0) {
            this.lGas--;
            this.refresh = 1;
        }
        if (this.lGas == 0) {
            this.eGas = 0;
        }
        if (this.portD == 1 && this.lPort > 0) {
            this.lPort--;
            this.refresh = 1;
        }
        if (this.lPort == 0) {
            this.ePort = 0;
        }
        if (this.refresh == 1) {
            eButtons();
        }
        this.refresh = 0;
        this.fireD = 0;
        this.plantD = 0;
        this.waterD = 0;
        this.rockD = 0;
        this.gasD = 0;
        this.portD = 0;
        if (this.goal[x][y] == 1 && this.type[x][y] == 0) {
            button.setBackgroundResource(R.drawable.fireb);
        }
        if (this.goal[x][y] == 1 && this.type[x][y] == 2) {
            button.setBackgroundResource(R.drawable.firebplant);
        }
        if (this.goal[x][y] == 1 && this.type[x][y] == 3) {
            button.setBackgroundResource(R.drawable.firebwater);
        }
        if (this.goal[x][y] == 1 && this.type[x][y] == 6) {
            button.setBackgroundResource(R.drawable.firebrock);
        }
        if (this.goal[x][y] == 2 && this.type[x][y] == 0) {
            button.setBackgroundResource(R.drawable.plantb);
        }
        if (this.goal[x][y] == 2 && this.type[x][y] == 2) {
            button.setBackgroundResource(R.drawable.plantbplant);
        }
        if (this.goal[x][y] == 2 && this.type[x][y] == 3) {
            button.setBackgroundResource(R.drawable.plantbwater);
        }
        if (this.goal[x][y] == 2 && this.type[x][y] == 6) {
            button.setBackgroundResource(R.drawable.plantbrock);
        }
        if (this.goal[x][y] == 3 && this.type[x][y] == 0) {
            button.setBackgroundResource(R.drawable.waterb);
        }
        if (this.goal[x][y] == 3 && this.type[x][y] == 2) {
            button.setBackgroundResource(R.drawable.waterbplant);
        }
        if (this.goal[x][y] == 3 && this.type[x][y] == 3) {
            button.setBackgroundResource(R.drawable.waterbwater);
        }
        if (this.goal[x][y] == 3 && this.type[x][y] == 6) {
            button.setBackgroundResource(R.drawable.waterbrock);
        }
        if (this.goal[x][y] == 4 && this.type[x][y] == 0) {
            button.setBackgroundResource(R.drawable.rockb);
        }
        if (this.goal[x][y] == 4 && this.type[x][y] == 2) {
            button.setBackgroundResource(R.drawable.rockbplant);
        }
        if (this.goal[x][y] == 4 && this.type[x][y] == 3) {
            button.setBackgroundResource(R.drawable.rockbwater);
        }
        if (this.goal[x][y] == 4 && this.type[x][y] == 6) {
            button.setBackgroundResource(R.drawable.rockbrock);
        }
        if (this.goal[x][y] == 5 && this.type[x][y] == 0) {
            button.setBackgroundResource(R.drawable.steamb);
        }
        if (this.goal[x][y] == 5 && this.type[x][y] == 2) {
            button.setBackgroundResource(R.drawable.steambplant);
        }
        if (this.goal[x][y] == 5 && this.type[x][y] == 5) {
            button.setBackgroundResource(R.drawable.steambsteam);
        }
    }

    public void onClick(View v) {
        this.itemC = this.itemC + 35;
        ((Button) findViewById(R.id.b1)).setOnClickListener(this);
        ((Button) findViewById(R.id.b2)).setOnClickListener(this);
        ((Button) findViewById(R.id.b3)).setOnClickListener(this);
        ((Button) findViewById(R.id.b4)).setOnClickListener(this);
        ((Button) findViewById(R.id.b5)).setOnClickListener(this);
        ((Button) findViewById(R.id.b6)).setOnClickListener(this);
        ((Button) findViewById(R.id.b7)).setOnClickListener(this);
        ((Button) findViewById(R.id.b8)).setOnClickListener(this);
        ((Button) findViewById(R.id.b9)).setOnClickListener(this);
        ((Button) findViewById(R.id.b10)).setOnClickListener(this);
        ((Button) findViewById(R.id.b11)).setOnClickListener(this);
        ((Button) findViewById(R.id.b12)).setOnClickListener(this);
        ((Button) findViewById(R.id.b13)).setOnClickListener(this);
        ((Button) findViewById(R.id.b14)).setOnClickListener(this);
        ((Button) findViewById(R.id.b15)).setOnClickListener(this);
        ((Button) findViewById(R.id.b16)).setOnClickListener(this);
        ((Button) findViewById(R.id.b17)).setOnClickListener(this);
        ((Button) findViewById(R.id.b18)).setOnClickListener(this);
        ((Button) findViewById(R.id.b19)).setOnClickListener(this);
        ((Button) findViewById(R.id.b20)).setOnClickListener(this);
        ((Button) findViewById(R.id.b21)).setOnClickListener(this);
        ((Button) findViewById(R.id.b22)).setOnClickListener(this);
        ((Button) findViewById(R.id.b23)).setOnClickListener(this);
        ((Button) findViewById(R.id.b24)).setOnClickListener(this);
        ((Button) findViewById(R.id.b25)).setOnClickListener(this);
        ((Button) findViewById(R.id.b26)).setOnClickListener(this);
        ((Button) findViewById(R.id.b27)).setOnClickListener(this);
        ((Button) findViewById(R.id.b28)).setOnClickListener(this);
        ((Button) findViewById(R.id.b29)).setOnClickListener(this);
        ((Button) findViewById(R.id.b30)).setOnClickListener(this);
        ((Button) findViewById(R.id.b31)).setOnClickListener(this);
        ((Button) findViewById(R.id.b32)).setOnClickListener(this);
        ((Button) findViewById(R.id.b33)).setOnClickListener(this);
        ((Button) findViewById(R.id.b34)).setOnClickListener(this);
        ((Button) findViewById(R.id.b35)).setOnClickListener(this);
        ((Button) findViewById(R.id.b36)).setOnClickListener(this);
        ((Button) findViewById(R.id.b37)).setOnClickListener(this);
        ((Button) findViewById(R.id.b38)).setOnClickListener(this);
        ((Button) findViewById(R.id.b39)).setOnClickListener(this);
        ((Button) findViewById(R.id.b40)).setOnClickListener(this);
        ((Button) findViewById(R.id.b41)).setOnClickListener(this);
        ((Button) findViewById(R.id.b42)).setOnClickListener(this);
        ((Button) findViewById(R.id.b43)).setOnClickListener(this);
        ((Button) findViewById(R.id.b44)).setOnClickListener(this);
        ((Button) findViewById(R.id.b45)).setOnClickListener(this);
        ((Button) findViewById(R.id.b46)).setOnClickListener(this);
        ((Button) findViewById(R.id.b47)).setOnClickListener(this);
        ((Button) findViewById(R.id.b48)).setOnClickListener(this);
        ((Button) findViewById(R.id.b49)).setOnClickListener(this);
        ((Button) findViewById(R.id.s1)).setOnClickListener(this);
        ((Button) findViewById(R.id.s2)).setOnClickListener(this);
        ((Button) findViewById(R.id.s3)).setOnClickListener(this);
        ((Button) findViewById(R.id.s4)).setOnClickListener(this);
        ((Button) findViewById(R.id.s5)).setOnClickListener(this);
        this.newclick = 0;
        switch (v.getId()) {
            case R.id.b1:
                if (this.d1 == 1 && 0 == 0) {
                    action(1);
                    return;
                }
                return;
            case R.id.b2:
                if (this.d2 == 1 && 0 == 0) {
                    action(2);
                    return;
                }
                return;
            case R.id.b3:
                if (this.d3 == 1 && 0 == 0) {
                    action(3);
                    return;
                }
                return;
            case R.id.b4:
                if (this.d4 == 1 && 0 == 0) {
                    action(4);
                    return;
                }
                return;
            case R.id.b5:
                if (this.d5 == 1 && 0 == 0) {
                    action(5);
                    return;
                }
                return;
            case R.id.b6:
                if (this.d6 == 1 && 0 == 0) {
                    action(6);
                    return;
                }
                return;
            case R.id.b7:
                if (this.d7 == 1 && 0 == 0) {
                    action(7);
                    return;
                }
                return;
            case R.id.b8:
                if (this.d8 == 1 && 0 == 0) {
                    action(8);
                    return;
                }
                return;
            case R.id.b9:
                if (this.d9 == 1 && 0 == 0) {
                    action(9);
                    return;
                }
                return;
            case R.id.b10:
                if (this.d10 == 1 && 0 == 0) {
                    action(10);
                    return;
                }
                return;
            case R.id.b11:
                if (this.d11 == 1 && 0 == 0) {
                    action(11);
                    return;
                }
                return;
            case R.id.b12:
                if (this.d12 == 1 && 0 == 0) {
                    action(12);
                    return;
                }
                return;
            case R.id.b13:
                if (this.d13 == 1 && 0 == 0) {
                    action(13);
                    return;
                }
                return;
            case R.id.b14:
                if (this.d14 == 1 && 0 == 0) {
                    action(14);
                    return;
                }
                return;
            case R.id.b15:
                if (this.d15 == 1 && 0 == 0) {
                    action(15);
                    return;
                }
                return;
            case R.id.b16:
                if (this.d16 == 1 && 0 == 0) {
                    action(16);
                    return;
                }
                return;
            case R.id.b17:
                if (this.d17 == 1 && 0 == 0) {
                    action(17);
                    return;
                }
                return;
            case R.id.b18:
                if (this.d18 == 1 && 0 == 0) {
                    action(18);
                    return;
                }
                return;
            case R.id.b19:
                if (this.d19 == 1 && 0 == 0) {
                    action(19);
                    return;
                }
                return;
            case R.id.b20:
                if (this.d20 == 1 && 0 == 0) {
                    action(20);
                    return;
                }
                return;
            case R.id.b21:
                if (this.d21 == 1 && 0 == 0) {
                    action(21);
                    return;
                }
                return;
            case R.id.b22:
                if (this.d22 == 1 && 0 == 0) {
                    action(22);
                    return;
                }
                return;
            case R.id.b23:
                if (this.d23 == 1 && 0 == 0) {
                    action(23);
                    return;
                }
                return;
            case R.id.b24:
                if (this.d24 == 1 && 0 == 0) {
                    action(24);
                    return;
                }
                return;
            case R.id.b25:
                if (this.d25 == 1 && 0 == 0) {
                    action(25);
                    return;
                }
                return;
            case R.id.b26:
                if (this.d26 == 1 && 0 == 0) {
                    action(26);
                    return;
                }
                return;
            case R.id.b27:
                if (this.d27 == 1 && 0 == 0) {
                    action(27);
                    return;
                }
                return;
            case R.id.b28:
                if (this.d28 == 1 && 0 == 0) {
                    action(28);
                    return;
                }
                return;
            case R.id.b29:
                if (this.d29 == 1 && 0 == 0) {
                    action(29);
                    return;
                }
                return;
            case R.id.b30:
                if (this.d30 == 1 && 0 == 0) {
                    action(30);
                    return;
                }
                return;
            case R.id.b31:
                if (this.d31 == 1 && 0 == 0) {
                    action(31);
                    return;
                }
                return;
            case R.id.b32:
                if (this.d32 == 1 && 0 == 0) {
                    action(32);
                    return;
                }
                return;
            case R.id.b33:
                if (this.d33 == 1 && 0 == 0) {
                    action(33);
                    return;
                }
                return;
            case R.id.b34:
                if (this.d34 == 1 && 0 == 0) {
                    action(34);
                    return;
                }
                return;
            case R.id.b35:
                if (this.d35 == 1 && 0 == 0) {
                    action(35);
                    return;
                }
                return;
            case R.id.b36:
                if (this.d36 == 1 && 0 == 0) {
                    action(36);
                    return;
                }
                return;
            case R.id.b37:
                if (this.d37 == 1 && 0 == 0) {
                    action(37);
                    return;
                }
                return;
            case R.id.b38:
                if (this.d38 == 1 && 0 == 0) {
                    action(38);
                    return;
                }
                return;
            case R.id.b39:
                if (this.d39 == 1 && 0 == 0) {
                    action(39);
                    return;
                }
                return;
            case R.id.b40:
                if (this.d40 == 1 && 0 == 0) {
                    action(40);
                    return;
                }
                return;
            case R.id.b41:
                if (this.d41 == 1 && 0 == 0) {
                    action(41);
                    return;
                }
                return;
            case R.id.b42:
                if (this.d42 == 1 && 0 == 0) {
                    action(42);
                    return;
                }
                return;
            case R.id.b43:
                if (this.d43 == 1 && 0 == 0) {
                    action(43);
                    return;
                }
                return;
            case R.id.b44:
                if (this.d44 == 1 && 0 == 0) {
                    action(44);
                    return;
                }
                return;
            case R.id.b45:
                if (this.d45 == 1 && 0 == 0) {
                    action(45);
                    return;
                }
                return;
            case R.id.b46:
                if (this.d46 == 1 && 0 == 0) {
                    action(46);
                    return;
                }
                return;
            case R.id.b47:
                if (this.d47 == 1 && 0 == 0) {
                    action(47);
                    return;
                }
                return;
            case R.id.b48:
                if (this.d48 == 1 && 0 == 0) {
                    action(48);
                    return;
                }
                return;
            case R.id.b49:
                if (this.d49 == 1 && 0 == 0) {
                    action(49);
                    return;
                }
                return;
            case R.id.s1:
                if (this.eFire == 1) {
                    this.element = 1;
                    this.item = this.element;
                    vibe();
                    eButtons();
                    return;
                }
                return;
            case R.id.s2:
                if (this.eWater == 1) {
                    this.element = 2;
                    this.item = this.element;
                    vibe();
                    eButtons();
                    return;
                }
                return;
            case R.id.s3:
                if (this.ePlant == 1) {
                    this.element = 3;
                    this.item = this.element;
                    vibe();
                    eButtons();
                    return;
                }
                return;
            case R.id.s4:
                if (this.eRock == 1) {
                    this.element = 4;
                    this.item = this.element;
                    vibe();
                    eButtons();
                    return;
                }
                return;
            case R.id.s5:
                if (this.eGas == 1) {
                    this.element = 5;
                    this.item = this.element;
                    vibe();
                    eButtons();
                }
                if (this.ePort == 1) {
                    this.element = 6;
                    this.item = this.element;
                    vibe();
                    eButtons();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void vibe() {
        SharedPreferences preferences = getPreferences(0);
        SharedPreferences gameSettings = getSharedPreferences("MyGamePreferences", 0);
        Vibrator v = (Vibrator) getSystemService("vibrator");
        if (gameSettings.getBoolean("vibration", true)) {
            v.vibrate((long) Menu.vduration);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 82) {
            startActivity(new Intent(this, Pause.class));
            return true;
        } else if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        } else {
            ref = 1;
            ref2 = 1;
            ref3 = 1;
            soundref = 1;
            exit = 1;
            return true;
        }
    }
}
