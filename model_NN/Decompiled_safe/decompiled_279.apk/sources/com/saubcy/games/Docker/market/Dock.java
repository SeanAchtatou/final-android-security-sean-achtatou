package com.saubcy.games.Docker.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import com.saubcy.games.Docker.market.ObjectFactory;
import com.saubcy.games.Docker.market.ShowFactory;
import com.saubcy.util.device.CYManager;
import com.waps.AppConnect;
import com.waps.ads.AdGroupLayout;
import com.wiyun.game.WiGame;
import com.wiyun.game.WiGameClient;
import com.wiyun.game.model.ChallengeRequest;
import java.util.List;

public class Dock extends Activity implements SensorEventListener {
    public static final int OP4GESTURE = 0;
    public static final int OP4SENSOR = 1;
    public static final String OPMODE = "com.saubcy.games.Docker.Dock.OPMODE";
    public static final String SETTING_DATA = "com.saubcy.games.Docker.Dock.SETTING_DATA";
    public static final String TIPSTATE = "com.saubcy.games.Docker.Dock.TIPSTATE";
    public static final String TOTALFINISH = "com.saubcy.games.Docker.Dock.TOTALFINISH";
    protected static final String[] achievementIds = {"34edc126daca9f85", "7c5ceb04e44ad916", "043078b1eb047411"};
    protected static final String appKey = "704e06c31049684a";
    protected static final String[] challageIds = {"7d814a62ce9174fe", "b93abafbbf491620", "d7b7db5cacd9a09b", "d44aa5e89087cc0c", "4d171eb03d3df80c", "21c02137b60c4012", "f8e21a3842dba36b", "914ab787eb00d9cb", "d2310e9a9b60acd1", "51b80ad35e3b2533", "5db7c26bd2bbefb8", "ec1f6fc32997fd4e", "7a678915f2990d5a", "2acdcd46da3dd5c1", "0ca24d5504984048", "d584ddff5256a8f7", "0ecd62e3ec7d6e60", "e1193db957a8e02c", "b86a19bd188d0719", "4620e04de26ca375", "82e066cf81a960d4", "4d7daedfea7a0ed6", "5e3d985b148ddd6b", "a2169092918ee122", "80f4100cdf357801", "cde9a028e595d9eb", "0cca7cc26bdf4482", "c9ea1216794a326b", "e67702e474c3914f", "cbbf3f4c19f4d933", "6449ddafe8d89969", "fa8da455c89172c2", "ce57828ac35d0515", "7040394006f62a35", "d65f0a315e666bd2", "b5830d8b17431f0f", "eabcc4cda11fbdfb", "da6cafef45227f20", "f4cb4d9fb5e65d58", "8e5bb380ffbbb886", "9182cb7c90e324b3", "11b3fcbef5801c46", "3ec05b3e024a7347", "88d719c80194618b", "efdff65541d7ff86", "2f7a472dc5f05f4e", "7344ea2fb1e69625", "1100b915f422954d", "ddfe0865729d0b2f", "d704a66612d456e1"};
    protected static final String[] leaderboardIds = {"810fac53b5dfaaa3", "ff9b2a010c74fc2f", "c7f1f3832021334c", "1b25a22d80b6d276", "86cd726a8da23a64", "e5b9efcdfd8e6938", "bdee61bd8170c48e", "3e537e6d536e9622", "fb54c66a19ab2e91", "ee0dfd4ce955cd18", "4ddd2cf8463f073f", "09982bc419071c0f", "43878ecbb466b233", "7ee43f646a3b551a", "3279448ee5b7431b", "a7bc041a66fdc27a", "ebce9b7d9b767bbe", "6bb17c4b899e551a", "a058f7be342c75a5", "1e5868e6b70d578f", "fe587715158ea7c9", "70c8afc17120f22f", "7c05e92bf2832b53", "080c0b78a7b830c5", "10acfe828fb4242d", "2b410d01f3445a91", "aecd51be455035f2", "d4735703d92acb57", "02d060ec68619545", "cf3a52e2ad186a53", "a31a6833153afd43", "1d8ecfe32e781d05", "efdfd51edeb3f844", "05e89942581f642a", "d7697ec4b6810da3", "45ad9e978aa85f07", "a0d50f92ae046d04", "5f683cb4d99a6079", "f5af3a51c30f1f61", "be57ec6f2f8bd8fb", "79621b6595ed4e70", "cd8b2ae3fbcc8b45", "8dccc3e6ce767272", "1439da1ac60be3fd", "bab657d40382f01c", "073641ec7b707373", "e89a36249b5de9c4", "a5ae6248c415078c", "2a995be441c7768f", "7f086bc707455ad8"};
    protected static final String secretKey = "ytSTfYVBvaqZhTyFECAW8G3pqDTsqraC";
    protected List<ObjectFactory.Box> BoxesList = null;
    protected List<ObjectFactory.Box> FlagsList = null;
    protected int Height = 0;
    protected SharedPreferences LevelInfoHander = null;
    protected int OpMode;
    protected float UnitHeight = 0.0f;
    protected float UnitWidth = 0.0f;
    protected int Width = 0;
    private int adHeight = 55;
    protected DockView dv = null;
    /* access modifiers changed from: private */
    public int isItemShow = 1;
    protected String mChallengeToUserId = null;
    private WiGameClient mClient = new WiGameClient() {
        public void wyLoggedIn(String sessionKey) {
        }

        public void wyLogInFailed() {
        }

        public void wyPlayChallenge(ChallengeRequest request) {
            Dock.this.mChallengeToUserId = request.getCtuId();
            Dock.this.mCompetitorScore = request.getScore();
            byte[] bLevel = request.getBlob();
            if (bLevel != null) {
                Dock.this.dv.level = bLevel[1];
                Dock.this.reset();
            }
        }

        public void wyPortraitGot(String userId) {
        }

        public void wyGameSaveStart(String name, int totalSize) {
        }

        public void wyGameSaveProgress(String name, int uploadedSize) {
        }

        public void wyGameSaved(String name) {
        }

        public void wyGameSaveFailed(String name) {
        }

        public void wyLoadGame(String blobPath) {
        }
    };
    protected int mCompetitorScore = -1;
    private Sensor sensor = null;
    private SensorManager sensorManager = null;
    protected int step = 0;
    protected int totalFinish;
    protected Vibrator vibrator = null;
    protected LinearLayout wapsView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        loadViews();
    }

    private void init() {
        AppConnect.getInstance(this);
        WiGame.init(this, appKey, secretKey, "1.0", false, false);
        WiGame.addWiGameClient(this.mClient);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setRequestedOrientation(1);
        if (((double) getResources().getDisplayMetrics().density) > 1.0d) {
            this.adHeight = 75;
        } else {
            this.adHeight = 55;
        }
        int versionCode = Integer.parseInt(Build.VERSION.SDK);
        if (11 == versionCode || 12 == versionCode) {
            this.adHeight += 50;
        }
        this.Width = CYManager.getScreenSize(this).nWidth;
        this.Height = CYManager.getScreenSize(this).nHeight - this.adHeight;
        this.UnitWidth = ((float) this.Width) / ((float) LevelInfo.COL);
        this.UnitHeight = ((float) this.Height) / ((float) LevelInfo.ROW);
    }

    private void loadViews() {
        setContentView((int) R.layout.dock);
        this.dv = (DockView) findViewById(R.id.dv);
        this.dv.level = getIntent().getIntExtra(LevelList.LEVEL_SELECT, 0);
        this.LevelInfoHander = getSharedPreferences(LevelList.LEVEL_LIST_DATA, 0);
        this.OpMode = this.LevelInfoHander.getInt(OPMODE, -1);
        this.isItemShow = this.LevelInfoHander.getInt(TIPSTATE, 1);
        this.totalFinish = this.LevelInfoHander.getInt(TOTALFINISH, 0);
        if (-1 == this.OpMode && checkSensor()) {
            showUseSensorDialog();
        }
        if (checkSensor()) {
            connectSensor();
        }
        this.wapsView = (LinearLayout) findViewById(R.id.AdLinearLayout);
        this.wapsView.setVisibility(0);
        this.wapsView.addView(new AdGroupLayout(this));
    }

    private void showUseSensorDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.sensor_open_tips)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.sensor_open_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.OpMode = 1;
                Dock.this.connectSensor();
                Dock.this.recordOpMode(1);
                dialog.cancel();
                if (1 == Dock.this.isItemShow) {
                    Dock.this.info();
                }
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.sensor_open_not_agree), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.OpMode = 0;
                Dock.this.disconnectSensor();
                Dock.this.recordOpMode(0);
                dialog.cancel();
                if (1 == Dock.this.isItemShow) {
                    Dock.this.info();
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void recordOpMode(int op4sensor) {
        SharedPreferences.Editor editor = this.LevelInfoHander.edit();
        editor.putInt(OPMODE, this.OpMode);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public void recordTipState() {
        SharedPreferences.Editor editor = this.LevelInfoHander.edit();
        editor.putInt(TIPSTATE, this.OpMode);
        editor.commit();
    }

    /* access modifiers changed from: protected */
    public void vibratorBack() {
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.vibrator.vibrate(35);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.vibrator != null) {
            this.vibrator.cancel();
            this.vibrator = null;
        }
        disconnectSensor();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WiGame.destroy(this);
        AppConnect.getInstance(this).finalize();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (1 == this.OpMode) {
            connectSensor();
        }
        this.dv.BoxesList = this.BoxesList;
        this.dv.FlagsList = this.FlagsList;
        this.dv.step = this.step;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        disconnectSensor();
        this.BoxesList = this.dv.BoxesList;
        this.FlagsList = this.dv.FlagsList;
        this.step = this.dv.step;
    }

    /* access modifiers changed from: protected */
    public void showFinishDialog() {
        if (this.mCompetitorScore >= 0) {
            WiGame.submitChallengeResult(this.mChallengeToUserId, this.mCompetitorScore - this.dv.step, this.dv.step, null);
            this.mCompetitorScore = -1;
        }
        if (this.dv.best == 0) {
            this.totalFinish++;
            SharedPreferences.Editor editor = this.LevelInfoHander.edit();
            editor.putInt(TOTALFINISH, this.totalFinish);
            editor.commit();
        }
        unlockAchievement(this.dv.level);
        showSubmitCheckDialog();
    }

    /* access modifiers changed from: protected */
    public void showNextJob() {
        String szMessage;
        String szMessage2 = String.valueOf(getBaseContext().getResources().getString(R.string.info_level)) + (this.dv.level + 1) + "\n" + getBaseContext().getResources().getString(R.string.info_moves) + this.dv.step + "\n" + getBaseContext().getResources().getString(R.string.info_best);
        if (this.dv.best == 0) {
            szMessage = String.valueOf(szMessage2) + "--";
        } else {
            szMessage = String.valueOf(szMessage2) + this.dv.best;
        }
        new AlertDialog.Builder(this).setMessage(szMessage).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.finish_select_level), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.selectLevel();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.finish_next_level), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.dv.newGame();
                dialog.cancel();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void showSubmitCheckDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.submit_check)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WiGame.submitScore(Dock.leaderboardIds[Dock.this.dv.level], Dock.this.dv.step, null, false);
                Dock.this.showNextJob();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.showNextJob();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void unlockAchievement(int level) {
        if (this.totalFinish >= 10) {
            WiGame.unlockAchievement(achievementIds[0]);
        }
        if (this.totalFinish >= 30) {
            WiGame.unlockAchievement(achievementIds[0]);
        }
        if (this.totalFinish >= 50) {
            WiGame.unlockAchievement(achievementIds[0]);
        }
    }

    public void selectLevel() {
        if (this.vibrator != null) {
            this.vibrator.cancel();
            this.vibrator = null;
        }
        disconnectSensor();
        finish();
    }

    /* access modifiers changed from: protected */
    public boolean checkSensor() {
        if (this.sensorManager == null) {
            this.sensorManager = (SensorManager) getSystemService("sensor");
        }
        if (this.sensorManager.getSensorList(3).size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean connectSensor() {
        if (this.sensorManager == null) {
            this.sensorManager = (SensorManager) getSystemService("sensor");
        }
        List<Sensor> sensors = this.sensorManager.getSensorList(3);
        if (sensors.size() <= 0) {
            return false;
        }
        this.sensor = sensors.get(0);
        this.sensorManager.registerListener(this, this.sensor, 1);
        return true;
    }

    /* access modifiers changed from: protected */
    public void disconnectSensor() {
        if (this.sensor != null) {
            this.sensorManager.unregisterListener(this);
            this.sensor = null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_reset /*2131034264*/:
                reset();
                return true;
            case R.id.menu_select_job /*2131034265*/:
                selectJobs();
                return true;
            case R.id.menu_setting /*2131034266*/:
                setting();
                return true;
            case R.id.HighScores /*2131034267*/:
                showHighScoresDialog();
                return true;
            case R.id.menu_more /*2131034268*/:
                more();
                return true;
            case R.id.Challenge /*2131034269*/:
                showChallageDialog();
                return true;
            case R.id.menu_info /*2131034270*/:
                info();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void more() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pub:saubcyj"));
                startActivity(installIntent);
            } catch (Exception e) {
                AppConnect.getInstance(this).showOffers(this);
            }
        } catch (Exception e2) {
            AppConnect.getInstance(this).showOffers(this);
        }
    }

    private void showHighScoresDialog() {
        WiGame.openLeaderboard(leaderboardIds[this.dv.level]);
    }

    private void showChallageDialog() {
        byte[] bLevel = {(byte) this.dv.level};
        int best = this.dv.best;
        if (best == 0) {
            best = 9999;
        }
        WiGame.sendChallenge(challageIds[this.dv.level], best, bLevel, leaderboardIds[this.dv.level]);
    }

    /* access modifiers changed from: private */
    public void reset() {
        this.dv.newGame();
    }

    private void selectJobs() {
        if (this.vibrator != null) {
            this.vibrator.cancel();
            this.vibrator = null;
        }
        disconnectSensor();
        finish();
    }

    private void setting() {
        new AlertDialog.Builder(this).setTitle(getBaseContext().getResources().getString(R.string.op_choice_title)).setIcon(17301659).setSingleChoiceItems(checkSensor() ? new String[]{getBaseContext().getResources().getString(R.string.op_choice_gesture), getBaseContext().getResources().getString(R.string.op_choice_sensor)} : new String[]{getBaseContext().getResources().getString(R.string.op_choice_gesture)}, this.OpMode, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Dock.this.OpMode = which;
                Dock.this.recordOpMode(Dock.this.OpMode);
                dialog.dismiss();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.op_choice_cancel), (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void info() {
        String text = getBaseContext().getResources().getString(R.string.tips_content);
        Log.d("trace", text);
        new AlertDialog.Builder(this).setMessage(text).setCancelable(false).setNegativeButton(getBaseContext().getResources().getString(R.string.tips_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Dock.this.isItemShow = 0;
                Dock.this.recordTipState();
            }
        }).show();
    }

    public void onSensorChanged(SensorEvent event) {
        if (1 != this.OpMode || this.dv.ActiveBox == null || this.dv.GameLock || event.sensor.getType() != 3 || event.values.length < 3) {
            return;
        }
        if (event.values[1] < 45.0f || event.values[1] > 90.0f) {
            if (event.values[1] > -45.0f || event.values[1] < -90.0f) {
                if (event.values[2] < 45.0f || event.values[2] > 90.0f) {
                    if (event.values[2] <= -45.0f && event.values[2] >= -90.0f && !ShowFactory.isBlock(this.dv.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[2], this.dv.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[2], this.dv)) {
                        new Thread(new ShowFactory.BoxMove(this.dv, 2)).start();
                    }
                } else if (!ShowFactory.isBlock(this.dv.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[0], this.dv.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[0], this.dv)) {
                    new Thread(new ShowFactory.BoxMove(this.dv, 0)).start();
                }
            } else if (!ShowFactory.isBlock(this.dv.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[3], this.dv.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[3], this.dv)) {
                new Thread(new ShowFactory.BoxMove(this.dv, 3)).start();
            }
        } else if (!ShowFactory.isBlock(this.dv.ActiveBox.pos.row + ShowFactory.BoxMove.ROWOPVALUE[1], this.dv.ActiveBox.pos.col + ShowFactory.BoxMove.COLOPVALUE[1], this.dv)) {
            new Thread(new ShowFactory.BoxMove(this.dv, 1)).start();
        }
    }

    public void onAccuracyChanged(Sensor sensor2, int accuracy) {
    }
}
