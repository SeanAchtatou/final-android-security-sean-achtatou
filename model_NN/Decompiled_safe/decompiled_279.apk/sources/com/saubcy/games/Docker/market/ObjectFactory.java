package com.saubcy.games.Docker.market;

public class ObjectFactory {

    protected static class Box {
        protected int angle = 0;
        protected int code;
        protected int color;
        protected Position pos;
        protected int state = 0;

        public Box(int row, int col, int color2, int code2) {
            this.pos = new Position(row, col);
            this.color = color2;
            this.code = code2;
        }
    }

    protected static class Position {
        protected int col;
        protected int row;

        public Position(int row2, int col2) {
            this.row = row2;
            this.col = col2;
        }
    }

    protected static class LevelInfo {
        protected int level;
        protected int state;
        protected int step = 0;

        public LevelInfo(int level2) {
            this.level = level2;
        }
    }
}
