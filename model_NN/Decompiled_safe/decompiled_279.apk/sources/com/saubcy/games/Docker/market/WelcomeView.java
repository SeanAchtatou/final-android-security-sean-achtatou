package com.saubcy.games.Docker.market;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.saubcy.games.Docker.market.ShowFactory;

public class WelcomeView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int BaseButtonTextSize = 35;
    public static final int BaseHeight = 480;
    public static final int BaseTitleTextSize = 50;
    public static final int BaseWidth = 320;
    protected static final int BoardId = 2130837504;
    protected static final int CloudId = 2130837510;
    protected static final int GrassId = 2130837517;
    protected final float BaseBoardScale = 0.62f;
    protected final float BaseGrassScale = 0.7f;
    protected int CloudHeight = 0;
    protected int CloudOffset = 0;
    protected int CloudStep = 10;
    protected int CloudWidth = 0;
    protected float[] LeftTopX = new float[2];
    protected float[] LeftTopY = new float[2];
    protected Matrix MatrixBoardRate = null;
    protected Matrix MatrixGrassRate = null;
    protected float RealBoardHeight = 0.0f;
    protected int RealHeight = 480;
    protected float RealHelpButtonWidth = 0.0f;
    protected float RealStartButtonWidth = 0.0f;
    protected float RealTextHeight = 0.0f;
    protected int RealWidth = 320;
    protected Bitmap SawBoard = null;
    protected Bitmap SawCloud = null;
    protected Bitmap SawGrass = null;
    protected float ScreenHeightScale;
    protected float ScreenWidthScale;
    protected int YBoardOffset = 20;
    protected int YCloudOffset = 20;
    protected int YTitleOffset = 100;
    protected SurfaceHolder holder;
    protected int isShow = 1;
    protected Paint pButton = null;
    protected Paint pTitleOut = null;
    protected Welcome parent = null;

    public WelcomeView(Context context) {
        super(context);
        this.parent = (Welcome) context;
        init();
    }

    public WelcomeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (Welcome) context;
        init();
    }

    private void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.RealWidth = this.parent.Width;
        this.RealHeight = this.parent.Height;
        this.ScreenWidthScale = ((float) this.RealWidth) / 320.0f;
        this.ScreenHeightScale = ((float) this.RealHeight) / 480.0f;
        this.pTitleOut = new Paint(1);
        this.pTitleOut.setTextSize(50.0f * this.ScreenWidthScale);
        this.pTitleOut.setColor(getResources().getColor(R.color.TitleColorOut));
        this.pButton = new Paint(1);
        this.pButton.setTextSize(35.0f * this.ScreenWidthScale);
        this.pButton.setColor(getResources().getColor(R.color.ButtonTextColor));
        Paint.FontMetrics fm = this.pButton.getFontMetrics();
        this.RealTextHeight = (float) Math.ceil((double) (fm.descent - fm.ascent));
        loadImages();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void loadImages() {
        this.SawCloud = ((BitmapDrawable) getResources().getDrawable(R.drawable.cloud)).getBitmap();
        this.CloudWidth = this.SawCloud.getWidth();
        this.CloudHeight = this.SawCloud.getHeight();
        this.SawBoard = ((BitmapDrawable) getResources().getDrawable(R.drawable.board)).getBitmap();
        this.SawGrass = ((BitmapDrawable) getResources().getDrawable(R.drawable.grass)).getBitmap();
        this.MatrixBoardRate = new Matrix();
        this.MatrixBoardRate.postScale(this.ScreenWidthScale * 0.62f, this.ScreenHeightScale * 0.62f);
        this.MatrixGrassRate = new Matrix();
        this.MatrixGrassRate.postScale(this.ScreenWidthScale * 0.7f, this.ScreenHeightScale * 0.7f);
        this.RealBoardHeight = (float) Bitmap.createBitmap(this.SawBoard, 0, 0, this.SawBoard.getWidth(), this.SawBoard.getHeight(), this.MatrixBoardRate, true).getHeight();
        this.RealStartButtonWidth = this.pButton.measureText(getResources().getString(R.string.welcome_start));
        this.RealHelpButtonWidth = this.pButton.measureText(getResources().getString(R.string.welcome_help));
        this.LeftTopX[0] = 0.0f;
        this.LeftTopX[1] = 0.0f;
        this.LeftTopY[0] = ((float) (this.YCloudOffset + this.CloudHeight + this.YBoardOffset)) + (this.RealBoardHeight / 3.0f);
        this.LeftTopY[1] = ((float) (this.YCloudOffset + this.CloudHeight + this.YBoardOffset)) + ((this.RealBoardHeight * 2.0f) / 3.0f);
    }

    /* access modifiers changed from: protected */
    public void startShow() {
        Log.d("trace", "startShow");
        this.CloudOffset = this.CloudWidth * -1;
        this.isShow = 1;
        new Thread(new ShowFactory.CloudMove(this)).start();
    }

    /* access modifiers changed from: protected */
    public void stopShow() {
        this.isShow = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        setMeasuredDimension(this.parent.Width, this.parent.Height);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        int action = -1;
        switch (event.getAction()) {
            case 0:
                action = this.parent.checkAction(event.getX(), event.getY());
                break;
        }
        Log.d("trace", "action = " + action);
        switch (action) {
            case 0:
                this.parent.start();
                return true;
            case 1:
                this.parent.help();
                return true;
            default:
                return true;
        }
    }
}
