package com.saubcy.games.Docker.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import com.saubcy.util.device.CYManager;
import com.waps.AppConnect;

public class Welcome extends Activity {
    public static final int ACTION_HELP = 1;
    public static final int ACTION_START = 0;
    protected int Height = 0;
    protected int Width = 0;
    private WelcomeView wv = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        loadViews();
    }

    private void init() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setRequestedOrientation(1);
        this.Width = CYManager.getScreenSize(this).nWidth;
        this.Height = CYManager.getScreenSize(this).nHeight;
    }

    /* access modifiers changed from: protected */
    public int checkAction(float x, float y) {
        Log.d("trace", "checkAction(" + x + "," + y + ")");
        if (y >= this.wv.LeftTopY[0] && y <= this.wv.LeftTopY[0] + (this.wv.RealBoardHeight / 3.0f)) {
            return 0;
        }
        if (y < this.wv.LeftTopY[1] || y > this.wv.LeftTopY[1] + (this.wv.RealBoardHeight / 3.0f)) {
            return -1;
        }
        return 1;
    }

    private void loadViews() {
        setContentView((int) R.layout.main);
        this.wv = (WelcomeView) findViewById(R.id.wv);
        this.wv.startShow();
    }

    /* access modifiers changed from: protected */
    public void start() {
        startActivity(new Intent(this, LevelList.class));
    }

    /* access modifiers changed from: protected */
    public void help() {
        String szMessage = getBaseContext().getResources().getString(R.string.help_content);
        new AlertDialog.Builder(this).setMessage(szMessage).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.Positive_Button), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.wv.stopShow();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.wv.startShow();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.wv.stopShow();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog();
        return true;
    }

    /* access modifiers changed from: protected */
    public void showExitDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.exit)).setCancelable(true).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Welcome.this.finish();
            }
        }).setNeutralButton(getBaseContext().getResources().getString(R.string.rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Welcome.this.rateit();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.menu_more), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Welcome.this.more();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void more() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pub:saubcyj"));
                startActivity(installIntent);
            } catch (Exception e) {
                AppConnect.getInstance(this).showOffers(this);
            }
        } catch (Exception e2) {
            AppConnect.getInstance(this).showOffers(this);
        }
    }

    /* access modifiers changed from: private */
    public void rateit() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pname:com.saubcy.games.Docker.market"));
                startActivity(installIntent);
            } catch (Exception e) {
                finish();
            }
        } catch (Exception e2) {
            finish();
        }
    }
}
