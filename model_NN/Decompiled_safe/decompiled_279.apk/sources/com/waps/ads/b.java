package com.waps.ads;

import java.lang.ref.WeakReference;

class b implements Runnable {
    private WeakReference a;

    public b(AdGroupLayout adGroupLayout) {
        this.a = new WeakReference(adGroupLayout);
    }

    public void run() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null) {
            adGroupLayout.handleAd();
        }
    }
}
