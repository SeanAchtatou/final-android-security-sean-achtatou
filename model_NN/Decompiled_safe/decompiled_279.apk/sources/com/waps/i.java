package com.waps;

import android.os.AsyncTask;

class i extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private i(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ i(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.v.a("http://app.waps.cn/action/account/spend?", this.a.L + "&points=" + this.a.H);
        if (a2 != null) {
            z = this.a.handleSpendPointsResponse(a2);
        }
        if (!z) {
            AppConnect.V.getUpdatePointsFailed("消费积分失败.");
        }
        return Boolean.valueOf(z);
    }
}
