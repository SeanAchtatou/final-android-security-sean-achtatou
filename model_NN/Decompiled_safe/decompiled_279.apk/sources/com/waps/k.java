package com.waps;

import android.content.Context;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.view.View;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class k extends AsyncTask {
    private static Context j;
    l a;
    m b;
    int c;
    float d = 0.0f;
    float e = 0.0f;
    NumberFormat f = new DecimalFormat("#0");
    float g;
    private String h;
    private View i;

    public k(Context context, View view, String str) {
        j = context;
        this.i = view;
        this.h = str;
        this.a = new l(j);
    }

    private String a(String str) {
        return str.substring(str.lastIndexOf("/") + 1);
    }

    private int b(String str) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setConnectTimeout(5000);
        httpURLConnection.setRequestMethod("GET");
        return httpURLConnection.getContentLength();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0194 A[SYNTHETIC, Splitter:B:56:0x0194] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0199 A[Catch:{ Exception -> 0x019d }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r14) {
        /*
            r13 = this;
            r8 = 0
            r12 = -1
            java.lang.String r0 = r13.h     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            java.lang.String r2 = r13.a(r0)     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            java.lang.String r6 = "/sdcard/download/"
            java.lang.String r0 = r13.h     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            int r0 = r13.b(r0)     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            float r0 = (float) r0     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            r13.d = r0     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            int r0 = (int) r0     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            r13.c = r0     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            r0.<init>()     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            r3 = 0
            r3 = r14[r3]     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            java.lang.String r4 = " "
            java.lang.String r5 = "%20"
            java.lang.String r3 = r3.replaceAll(r4, r5)     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            r1.<init>(r3)     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            java.io.InputStream r7 = r0.getContent()     // Catch:{ Exception -> 0x01a9, all -> 0x018f }
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            if (r0 == 0) goto L_0x0111
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r0.<init>(r6)     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r1.<init>(r6, r2)     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            boolean r3 = r0.exists()     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            if (r3 != 0) goto L_0x005a
            r0.mkdir()     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
        L_0x005a:
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            if (r0 != 0) goto L_0x0063
            r1.createNewFile()     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
        L_0x0063:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r0.<init>(r1)     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r8 = r0
        L_0x0069:
            if (r7 == 0) goto L_0x0104
            r0 = 51200(0xc800, float:7.1746E-41)
            byte[] r9 = new byte[r0]     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
        L_0x0070:
            int r0 = r7.read(r9)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            if (r0 == r12) goto L_0x00a3
            java.text.NumberFormat r1 = r13.f     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r3 = r13.g     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            double r3 = (double) r3     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r1 = r1.format(r3)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r3 = 100
            if (r1 <= r3) goto L_0x011b
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.view.View r1 = r13.i     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r3 = r13.c     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r4.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r5 = "下载失败，请重新下载"
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
        L_0x00a3:
            int r0 = r7.read(r9)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r3 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            if (r0 != r12) goto L_0x0104
            java.lang.String r0 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            if (r0 == 0) goto L_0x0186
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r6 = r0
        L_0x00d3:
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.view.View r1 = r13.i     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r3 = r13.c     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r5 = "下载完成,点击安装"
            r0.a(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.setAction(r1)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.net.Uri r1 = android.net.Uri.fromFile(r6)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r3 = "application/vnd.android.package-archive"
            r0.setDataAndType(r1, r3)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.content.Context r1 = com.waps.k.j     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r1.startActivity(r0)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            com.waps.m r0 = new com.waps.m     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            com.waps.l r1 = r13.a     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r3 = r13.c     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.<init>(r1, r3, r2)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r13.b = r0     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r13.a()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
        L_0x0104:
            if (r7 == 0) goto L_0x0109
            r7.close()     // Catch:{ Exception -> 0x01b1 }
        L_0x0109:
            if (r8 == 0) goto L_0x010e
            r8.close()     // Catch:{ Exception -> 0x01b1 }
        L_0x010e:
            java.lang.String r0 = ""
            return r0
        L_0x0111:
            android.content.Context r0 = com.waps.k.j     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r1 = 3
            java.io.FileOutputStream r0 = r0.openFileOutput(r2, r1)     // Catch:{ Exception -> 0x01ad, all -> 0x019f }
            r8 = r0
            goto L_0x0069
        L_0x011b:
            r1 = 0
            r8.write(r9, r1, r0)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r1 = r13.e     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r0 = (float) r0     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r0 = r0 + r1
            r13.e = r0     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r0 = r13.e     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r1 = r13.d     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r0 = r0 / r1
            r1 = 1120403456(0x42c80000, float:100.0)
            float r0 = r0 * r1
            r13.g = r0     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0 = 1
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r1 = 0
            float r3 = r13.e     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r4 = r13.d     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r3 = r3 / r4
            int r3 = (int) r3     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r3 = r3 * 100
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0[r1] = r3     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r13.publishProgress(r0)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.text.NumberFormat r0 = r13.f     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r1 = r13.g     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            double r3 = (double) r1     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.format(r3)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            com.waps.l r0 = r13.a     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            android.view.View r1 = r13.i     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            int r3 = r13.c     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r4.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.text.NumberFormat r5 = r13.f     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            float r10 = r13.g     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            double r10 = (double) r10     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r5 = r5.format(r10)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r5 = " %"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r0.a(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            goto L_0x0070
        L_0x0173:
            r0 = move-exception
            r1 = r8
            r2 = r7
        L_0x0176:
            r0.printStackTrace()     // Catch:{ all -> 0x01a7 }
            if (r2 == 0) goto L_0x017e
            r2.close()     // Catch:{ Exception -> 0x0184 }
        L_0x017e:
            if (r1 == 0) goto L_0x010e
            r1.close()     // Catch:{ Exception -> 0x0184 }
            goto L_0x010e
        L_0x0184:
            r0 = move-exception
            goto L_0x010e
        L_0x0186:
            android.content.Context r0 = com.waps.k.j     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            java.io.File r0 = r0.getFileStreamPath(r2)     // Catch:{ Exception -> 0x0173, all -> 0x01a3 }
            r6 = r0
            goto L_0x00d3
        L_0x018f:
            r0 = move-exception
            r1 = r8
            r2 = r8
        L_0x0192:
            if (r2 == 0) goto L_0x0197
            r2.close()     // Catch:{ Exception -> 0x019d }
        L_0x0197:
            if (r1 == 0) goto L_0x019c
            r1.close()     // Catch:{ Exception -> 0x019d }
        L_0x019c:
            throw r0
        L_0x019d:
            r1 = move-exception
            goto L_0x019c
        L_0x019f:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x0192
        L_0x01a3:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x0192
        L_0x01a7:
            r0 = move-exception
            goto L_0x0192
        L_0x01a9:
            r0 = move-exception
            r1 = r8
            r2 = r8
            goto L_0x0176
        L_0x01ad:
            r0 = move-exception
            r1 = r8
            r2 = r7
            goto L_0x0176
        L_0x01b1:
            r0 = move-exception
            goto L_0x010e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.k.doInBackground(java.lang.String[]):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void a() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        j.registerReceiver(this.b, intentFilter);
    }
}
