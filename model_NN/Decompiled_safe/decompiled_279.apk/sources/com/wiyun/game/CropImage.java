package com.wiyun.game;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.wiyun.game.d.a;
import com.wiyun.game.d.b;
import com.wiyun.game.d.d;
import com.wiyun.game.d.g;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CountDownLatch;

public class CropImage extends MonitoredActivity {
    boolean a;
    boolean b;
    g c;
    Runnable d = new a(this);
    private Bitmap.CompressFormat e = Bitmap.CompressFormat.JPEG;
    private Uri f = null;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public final Handler j = new Handler();
    private int k;
    private int l;
    private boolean m;
    private boolean n = true;
    /* access modifiers changed from: private */
    public boolean o = true;
    /* access modifiers changed from: private */
    public CropImageView p;
    private ContentResolver q;
    /* access modifiers changed from: private */
    public Bitmap r;
    private Bitmap s;
    private final d.c t = new d.c();
    /* access modifiers changed from: private */
    public b u;
    private String v;

    public static int a() {
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().toString());
            return (int) ((((float) statFs.getAvailableBlocks()) * ((float) statFs.getBlockSize())) / 400000.0f);
        } catch (Exception e2) {
            return -2;
        }
    }

    private Uri a(String path) {
        return Uri.fromFile(new File(path));
    }

    public static void a(Activity activity) {
        a(activity, a());
    }

    public static void a(Activity activity, int i2) {
        String str = null;
        if (i2 == -1) {
            str = Environment.getExternalStorageState() == "checking" ? "Preparing card" : "No storage card";
        } else if (i2 < 1) {
            str = "Not enough space";
        }
        if (str != null) {
            Toast.makeText(activity, str, 5000).show();
        }
    }

    /* access modifiers changed from: private */
    public void a(Bitmap bitmap) {
        OutputStream outputStream;
        Throwable th;
        IOException iOException;
        if (this.f != null) {
            try {
                OutputStream openOutputStream = this.q.openOutputStream(this.f);
                if (openOutputStream != null) {
                    try {
                        bitmap.compress(this.e, 75, openOutputStream);
                    } catch (IOException e2) {
                        IOException iOException2 = e2;
                        outputStream = openOutputStream;
                        iOException = iOException2;
                        try {
                            Log.e("CropImage", "Cannot open file: " + this.f, iOException);
                            a.a(outputStream);
                            setResult(-1, new Intent(this.f.toString()).putExtras(new Bundle()));
                            return;
                        } catch (Throwable th2) {
                            th = th2;
                            a.a(outputStream);
                            throw th;
                        }
                    } catch (Throwable th3) {
                        Throwable th4 = th3;
                        outputStream = openOutputStream;
                        th = th4;
                        a.a(outputStream);
                        throw th;
                    }
                }
                a.a(openOutputStream);
            } catch (IOException e3) {
                IOException iOException3 = e3;
                outputStream = null;
                iOException = iOException3;
            } catch (Throwable th5) {
                Throwable th6 = th5;
                outputStream = null;
                th = th6;
                a.a(outputStream);
                throw th;
            }
            setResult(-1, new Intent(this.f.toString()).putExtras(new Bundle()));
            return;
        }
        Log.e("CropImage", "neni definovana adresa pro ulozeni");
    }

    private Bitmap b(String str) {
        try {
            return BitmapFactory.decodeStream(this.q.openInputStream(a(str)));
        } catch (FileNotFoundException e2) {
            Log.e("CropImage", "file " + str + " not found");
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.wiyun.game.CropImageView.a(float, float):void
      com.wiyun.game.d.e.a(android.graphics.Bitmap, int):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, android.graphics.Matrix):void
      com.wiyun.game.d.e.a(android.graphics.Matrix, int):float
      com.wiyun.game.d.e.a(float, float):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, boolean):void
      com.wiyun.game.d.e.a(boolean, boolean):void
      com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void */
    private void b() {
        if (!isFinishing()) {
            this.p.a(this.r, true);
            a.a(this, (String) null, "Please wait…", new Runnable() {
                public void run() {
                    if (CropImage.this.r == null) {
                        CropImage.this.setResult(0);
                        CropImage.this.finish();
                        return;
                    }
                    final CountDownLatch latch = new CountDownLatch(1);
                    final Bitmap b = CropImage.this.u != null ? CropImage.this.u.a(-1, 1048576) : CropImage.this.r;
                    CropImage.this.j.post(new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void
                         arg types: [android.graphics.Bitmap, int]
                         candidates:
                          com.wiyun.game.CropImageView.a(float, float):void
                          com.wiyun.game.d.e.a(android.graphics.Bitmap, int):void
                          com.wiyun.game.d.e.a(com.wiyun.game.d.c, android.graphics.Matrix):void
                          com.wiyun.game.d.e.a(android.graphics.Matrix, int):float
                          com.wiyun.game.d.e.a(float, float):void
                          com.wiyun.game.d.e.a(com.wiyun.game.d.c, boolean):void
                          com.wiyun.game.d.e.a(boolean, boolean):void
                          com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.wiyun.game.d.e.a(boolean, boolean):void
                         arg types: [int, int]
                         candidates:
                          com.wiyun.game.CropImageView.a(float, float):void
                          com.wiyun.game.d.e.a(android.graphics.Bitmap, int):void
                          com.wiyun.game.d.e.a(com.wiyun.game.d.c, android.graphics.Matrix):void
                          com.wiyun.game.d.e.a(android.graphics.Matrix, int):float
                          com.wiyun.game.d.e.a(float, float):void
                          com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void
                          com.wiyun.game.d.e.a(com.wiyun.game.d.c, boolean):void
                          com.wiyun.game.d.e.a(boolean, boolean):void */
                        public void run() {
                            if (!(b == CropImage.this.r || b == null)) {
                                CropImage.this.p.a(b, true);
                                CropImage.this.r.recycle();
                                CropImage.this.r = b;
                            }
                            if (CropImage.this.p.a() == 1.0f) {
                                CropImage.this.p.a(true, true);
                            }
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                        CropImage.this.d.run();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }, this.j);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        final Bitmap bitmap;
        if (!this.b && this.c != null) {
            this.b = true;
            Rect b2 = this.c.b();
            int width = b2.width();
            int height = b2.height();
            Bitmap createBitmap = Bitmap.createBitmap(width, height, this.i ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
            new Canvas(createBitmap).drawBitmap(this.r, b2, new Rect(0, 0, width, height), (Paint) null);
            if (this.i) {
                Canvas canvas = new Canvas(createBitmap);
                Path path = new Path();
                path.addCircle(((float) width) / 2.0f, ((float) height) / 2.0f, ((float) width) / 2.0f, Path.Direction.CW);
                canvas.clipPath(path, Region.Op.DIFFERENCE);
                canvas.drawColor(0, PorterDuff.Mode.CLEAR);
            }
            if (this.k == 0 || this.l == 0) {
                bitmap = createBitmap;
            } else if (this.m) {
                bitmap = a.a(new Matrix(), createBitmap, this.k, this.l, this.n);
                if (createBitmap != bitmap) {
                    createBitmap.recycle();
                }
            } else {
                bitmap = Bitmap.createBitmap(this.k, this.l, Bitmap.Config.RGB_565);
                Canvas canvas2 = new Canvas(bitmap);
                Rect b3 = this.c.b();
                Rect rect = new Rect(0, 0, this.k, this.l);
                int width2 = (b3.width() - rect.width()) / 2;
                int height2 = (b3.height() - rect.height()) / 2;
                b3.inset(Math.max(0, width2), Math.max(0, height2));
                rect.inset(Math.max(0, -width2), Math.max(0, -height2));
                canvas2.drawBitmap(this.r, b3, rect, (Paint) null);
                createBitmap.recycle();
            }
            Bundle extras = getIntent().getExtras();
            if (extras == null || (extras.getParcelable("data") == null && !extras.getBoolean("return-data"))) {
                a.a(this, (String) null, "Saving image", new Runnable() {
                    public void run() {
                        CropImage.this.a(bitmap);
                    }
                }, this.j);
            } else {
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", bitmap);
                setResult(-1, new Intent().setAction("inline-data").putExtras(bundle));
            }
            this.s = bitmap;
            finish();
        }
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.q = getContentResolver();
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_cropimage"));
        this.p = (CropImageView) findViewById(t.d("image"));
        a((Activity) this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getString("circleCrop") != null) {
                this.i = true;
                this.g = 1;
                this.h = 1;
            }
            this.v = extras.getString("image-path");
            this.f = a(this.v);
            this.r = b(this.v);
            this.g = extras.getInt("aspectX");
            this.h = extras.getInt("aspectY");
            this.k = extras.getInt("outputX");
            this.l = extras.getInt("outputY");
            this.m = extras.getBoolean("scale", true);
            this.n = extras.getBoolean("scaleUpIfNeeded", true);
        }
        if (this.r == null) {
            finish();
            return;
        }
        getWindow().addFlags(1024);
        findViewById(t.d("discard")).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImage.this.setResult(0);
                CropImage.this.finish();
            }
        });
        findViewById(t.d("save")).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImage.this.c();
            }
        });
        b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.r != null && !this.r.isRecycled()) {
            this.r.recycle();
            this.r = null;
        }
        if (this.s != null) {
            this.s.recycle();
            this.s = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        d.a().a(this.t);
    }
}
