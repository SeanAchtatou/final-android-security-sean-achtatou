package com.wiyun.game;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.wiyun.game.a.c;
import com.wiyun.game.a.h;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import com.wiyun.game.model.a.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadGameDialog extends ListActivity implements View.OnClickListener, c.a, com.wiyun.game.b.b {
    protected int a;
    protected h b;
    protected Map<String, Bitmap> c;
    /* access modifiers changed from: private */
    public List<i> d;
    private int e;
    /* access modifiers changed from: private */
    public View f;
    /* access modifiers changed from: private */
    public ViewGroup g;
    private BroadcastReceiver h = new g(this);

    protected static class a {
        TextView a;
        TextView b;
        TextView c;
        ImageView d;

        protected a() {
        }
    }

    private final class b extends BaseAdapter {
        private b() {
        }

        /* synthetic */ b(LoadGameDialog loadGameDialog, b bVar) {
            this();
        }

        public int getCount() {
            return LoadGameDialog.this.d.size();
        }

        public Object getItem(int position) {
            return LoadGameDialog.this.d.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return LoadGameDialog.this.a(convertView, parent, (i) LoadGameDialog.this.d.get(position));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap
     arg types: [java.util.Map<java.lang.String, android.graphics.Bitmap>, int, java.lang.String, java.lang.String]
     candidates:
      com.wiyun.game.h.a(double, double, double, double):double
      com.wiyun.game.h.a(android.content.Context, java.lang.String, boolean, int):void
      com.wiyun.game.h.a(java.util.Map<java.lang.String, android.graphics.Bitmap>, boolean, java.lang.String, java.lang.String):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public View a(View view, ViewGroup viewGroup, i iVar) {
        View view2;
        if (view == null) {
            view2 = LayoutInflater.from(this).inflate(t.e("wy_list_item_slot"), (ViewGroup) null);
            a aVar = new a();
            aVar.a = (TextView) view2.findViewById(t.d("wy_tv_name"));
            aVar.b = (TextView) view2.findViewById(t.d("wy_tv_time"));
            aVar.c = (TextView) view2.findViewById(t.d("wy_tv_desc"));
            aVar.d = (ImageView) view2.findViewById(t.d("wy_iv_screenshot"));
            view2.setTag(aVar);
        } else {
            view2 = view;
        }
        a aVar2 = (a) view2.getTag();
        aVar2.a.setText(String.format(t.h("wy_label_slot_name_x"), iVar.d()));
        String a2 = this.b.a(this, iVar.b());
        aVar2.b.setText(String.format(t.h("wy_label_slot_save_time_x"), a2));
        aVar2.c.setText(String.format(t.h("wy_label_slot_desc_x"), iVar.c()));
        aVar2.d.setImageBitmap(h.a(this.c, false, "ss_", iVar.e()));
        return view2;
    }

    private void c() {
        this.f = findViewById(t.d("wy_ll_loading_panel"));
        this.g = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        ((Button) findViewById(t.d("wy_b_more_games"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_close"))).setOnClickListener(this);
        setListAdapter(new b(this, null));
        getListView().setOnCreateContextMenuListener(this);
    }

    private void d() {
        if (this.c != null) {
            for (Bitmap bitmap : this.c.values()) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            this.c.clear();
        }
    }

    private void e() {
        showDialog(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void f() {
        Intent intent = new Intent(this, DownloadBlob.class);
        intent.putExtra("blob_url", this.d.get(this.e).f());
        intent.putExtra("message", t.h("wy_label_downloading_saved_game"));
        intent.putExtra("use_tmp_file", true);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d = new ArrayList();
        IntentFilter filter = new IntentFilter("com.wiyun.game.BLOB_DOWNLOADED");
        filter.addAction("com.wiyun.game.IMAGE_DOWNLOADED");
        registerReceiver(this.h, filter);
        this.c = new HashMap();
        this.b = new h(true);
        d.a().a(this);
    }

    public void a(int id) {
    }

    /* access modifiers changed from: protected */
    public void b() {
        ListAdapter adapter = getListAdapter();
        if (adapter != null) {
            ((BaseAdapter) adapter).notifyDataSetChanged();
        }
    }

    public void b(int id) {
        switch (id) {
            case 1:
                this.f.setVisibility(0);
                h.a(this.g);
                f.j(this.d.get(this.e).a());
                return;
            default:
                return;
        }
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 74:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(LoadGameDialog.this, (String) e2.e, 0).show();
                            LoadGameDialog.this.finish();
                        }
                    });
                    return;
                }
                this.a = e2.g;
                this.d.addAll((List) e2.e);
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (LoadGameDialog.this.a == 0) {
                            Toast.makeText(LoadGameDialog.this, t.f("wy_toast_no_saved_game"), 0).show();
                            LoadGameDialog.this.finish();
                            return;
                        }
                        LoadGameDialog.this.b();
                        LoadGameDialog.this.f.setVisibility(4);
                        h.b(LoadGameDialog.this.g);
                    }
                });
                return;
            case 75:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(LoadGameDialog.this, (String) e2.e, 0).show();
                            LoadGameDialog.this.finish();
                        }
                    });
                    return;
                }
                this.a--;
                this.d.remove(this.e);
                if (this.d.isEmpty()) {
                    f.b((String) null, this.d.size(), 25);
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            LoadGameDialog.this.b();
                            LoadGameDialog.this.f.setVisibility(4);
                            h.b(LoadGameDialog.this.g);
                        }
                    });
                    return;
                }
            default:
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_more_games")) {
            if (this.d.size() < this.a) {
                this.f.setVisibility(0);
                f.b((String) null, this.d.size(), 25);
                return;
            }
            Toast.makeText(this, t.f("wy_toast_no_more_saved_games"), 0).show();
        } else if (id == t.d("wy_b_close")) {
            finish();
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                f();
                break;
            case 2:
                e();
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(t.e("wy_activity_load_game_dialog"));
        a();
        c();
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
        if (adapterContextMenuInfo != null) {
            this.e = adapterContextMenuInfo.position;
        }
        contextMenu.add(0, 1, 0, t.f("wy_context_item_load_slot"));
        contextMenu.add(0, 2, 0, t.f("wy_context_item_delete_slot"));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return c.a(this, id, 17301543, t.h("wy_label_confirm_delete"), t.h("wy_label_delete_game_slot"), t.f("wy_button_ok"), t.f("wy_button_cancel"), this);
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        setListAdapter(null);
        unregisterReceiver(this.h);
        d.a().b(this);
        d();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        this.e = position;
        openContextMenu(l);
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        h.a(this.g);
        f.b((String) null, this.d.size(), 25);
    }
}
