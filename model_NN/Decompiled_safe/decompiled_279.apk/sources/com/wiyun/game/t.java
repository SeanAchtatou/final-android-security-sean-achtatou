package com.wiyun.game;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class t {
    static int a(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "anim", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "anim", pkg);
    }

    private static String a() {
        return WiGame.getContext() != null ? WiGame.getContext().getPackageName() : WiGame.d != null ? WiGame.d.getPackageName() : "";
    }

    static int b(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "array", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "array", pkg);
    }

    private static Resources b() {
        if (WiGame.getContext() != null) {
            return WiGame.getContext().getResources();
        }
        return null;
    }

    public static int c(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "drawable", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "drawable", pkg);
    }

    private static Resources c() {
        if (WiGame.d != null) {
            return WiGame.d.getResources();
        }
        return null;
    }

    public static int d(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "id", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "id", pkg);
    }

    public static int e(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "layout", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "layout", pkg);
    }

    public static int f(String name) {
        Resources r;
        int id = 0;
        String pkg = a();
        Resources r2 = b();
        if (r2 != null) {
            id = r2.getIdentifier(name, "string", pkg);
        }
        return (id != 0 || (r = c()) == null) ? id : r.getIdentifier(name, "string", pkg);
    }

    public static Drawable g(String name) {
        int id = 0;
        String pkg = a();
        Resources r = b();
        if (r != null) {
            id = r.getIdentifier(name, "drawable", pkg);
        }
        if (id != 0) {
            return r.getDrawable(id);
        }
        Resources r2 = c();
        if (r2 != null) {
            id = r2.getIdentifier(name, "drawable", pkg);
        }
        if (id == 0) {
            return null;
        }
        return r2.getDrawable(id);
    }

    public static String h(String name) {
        int id = 0;
        String pkg = a();
        Resources r = b();
        if (r != null) {
            id = r.getIdentifier(name, "string", pkg);
        }
        if (id != 0) {
            return r.getString(id);
        }
        Resources r2 = c();
        if (r2 != null) {
            id = r2.getIdentifier(name, "string", pkg);
        }
        return id == 0 ? "!!" + name + "!!" : r2.getString(id);
    }

    public static int i(String name) {
        int id = 0;
        String pkg = a();
        Resources r = b();
        if (r != null) {
            id = r.getIdentifier(name, "color", pkg);
        }
        if (id != 0) {
            return r.getColor(id);
        }
        Resources r2 = c();
        if (r2 != null) {
            id = r2.getIdentifier(name, "color", pkg);
        }
        if (id == 0) {
            return 0;
        }
        return r2.getColor(id);
    }

    public static float j(String name) {
        int id = 0;
        String pkg = a();
        Resources r = b();
        if (r != null) {
            id = r.getIdentifier(name, "dimen", pkg);
        }
        if (id != 0) {
            return r.getDimension(id);
        }
        Resources r2 = c();
        if (r2 != null) {
            id = r2.getIdentifier(name, "dimen", pkg);
        }
        if (id == 0) {
            return 0.0f;
        }
        return r2.getDimension(id);
    }
}
