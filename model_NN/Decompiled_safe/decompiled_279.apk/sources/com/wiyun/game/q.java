package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class q extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SingleListActivity a;

    q(SingleListActivity singleListActivity) {
        this.a = singleListActivity;
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(intent.getAction())) {
            final String id = intent.getStringExtra("image_id");
            if (this.a.b.containsKey(id)) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        h.b(q.this.a.b, true, id, null);
                        q.this.a.m();
                    }
                });
                return;
            }
            return;
        }
        this.a.b(intent);
    }
}
