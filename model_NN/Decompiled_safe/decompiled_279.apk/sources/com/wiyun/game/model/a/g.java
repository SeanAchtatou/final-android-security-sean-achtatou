package com.wiyun.game.model.a;

import org.json.JSONObject;

public class g {
    private r a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;

    public static g a(JSONObject dict) {
        r d2 = r.b(dict);
        if (d2 == null) {
            return null;
        }
        g h = new g();
        h.a(d2);
        h.a(dict.optInt("win_count"));
        h.b(dict.optInt("loss_count"));
        h.c(dict.optInt("tie_count"));
        h.d(dict.optInt("reject_count"));
        h.e(dict.optInt("win_point"));
        h.f(dict.optInt("loss_point"));
        return h;
    }

    public void a(int win) {
        this.b = win;
    }

    public void a(r challengeDefinition) {
        this.a = challengeDefinition;
    }

    public void b(int loss) {
        this.c = loss;
    }

    public void c(int tie) {
        this.d = tie;
    }

    public void d(int reject) {
        this.e = reject;
    }

    public void e(int winPoints) {
        this.f = winPoints;
    }

    public void f(int lossPoints) {
        this.g = lossPoints;
    }
}
