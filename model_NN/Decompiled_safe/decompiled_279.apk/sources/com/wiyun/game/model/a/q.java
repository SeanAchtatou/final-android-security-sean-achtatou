package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class q {
    private String a;
    private String b;
    private String c;
    private String d;
    private long e;
    private long f;
    private String g;
    private String h;
    private String i;
    private boolean j;

    public static q a(JSONObject dict) {
        q t = new q();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        t.b(id);
        t.f(dict.optString("category"));
        t.e(dict.optString("title"));
        t.a(dict.optString("content"));
        t.a(dict.optLong("create_time"));
        t.b(dict.optLong("last_time"));
        t.a(dict.optBoolean("starred"));
        t.d(dict.optString("id"));
        t.c(dict.optString("username"));
        t.g(dict.optString("last_username"));
        return t;
    }

    public void a(long createTime) {
        this.e = createTime;
    }

    public void a(String content) {
        this.b = content;
    }

    public void a(boolean starred) {
        this.j = starred;
    }

    public void b(long replyTime) {
        this.f = replyTime;
    }

    public void b(String id) {
        this.d = id;
    }

    public void c(String username) {
        this.g = username;
    }

    public void d(String userId) {
        this.i = userId;
    }

    public void e(String title) {
        this.c = title;
    }

    public void f(String category) {
        this.a = category;
    }

    public void g(String lastUsername) {
        this.h = lastUsername;
    }
}
