package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class aa {
    private String a;
    private String b;
    private String c;
    private boolean d;
    private int e;
    private String f;
    private String g;
    private long h;

    public static aa a(JSONObject dict) {
        aa i = new aa();
        String id = dict.optString("id");
        String userId = dict.optString("user_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty(userId)) {
            return null;
        }
        i.a(id);
        i.b(userId);
        i.c(dict.optString("username"));
        i.e(dict.optString("avatar"));
        i.a(dict.optInt("point"));
        i.d(dict.optString("message"));
        i.a(dict.optLong("sent"));
        i.a("F".equalsIgnoreCase(dict.optString("gender")));
        return i;
    }

    public void a(int point) {
        this.e = point;
    }

    public void a(long sentTime) {
        this.h = sentTime;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean female) {
        this.d = female;
    }

    public void b(String userId) {
        this.b = userId;
    }

    public void c(String name) {
        this.c = name;
    }

    public void d(String message) {
        this.f = message;
    }

    public void e(String avatarUrl) {
        this.g = avatarUrl;
    }
}
