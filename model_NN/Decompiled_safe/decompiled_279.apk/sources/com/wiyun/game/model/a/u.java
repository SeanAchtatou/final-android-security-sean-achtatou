package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class u {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private boolean g;
    private int h;
    private int i;
    private int j;
    private boolean k;

    public static u a(JSONObject dict) {
        u c2 = new u();
        String id = dict.optString("challenge_id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        c2.a(id);
        c2.c(dict.optString("username"));
        c2.e(dict.optString("user_id"));
        c2.b(dict.optString("message"));
        c2.a(dict.optInt("bid_point"));
        c2.b(dict.optInt("score"));
        c2.a(dict.optBoolean("has_blob"));
        c2.c(dict.optInt("type"));
        c2.d(dict.optString("avatar"));
        c2.b("F".equalsIgnoreCase(dict.optString("gender")));
        return c2;
    }

    public static u b(JSONObject dict) {
        u c2 = new u();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        c2.a(id);
        c2.c(dict.optString("username"));
        c2.e(dict.optString("user_id"));
        c2.b(dict.optString("message"));
        c2.a(dict.optInt("bid_point"));
        c2.b(dict.optInt("score"));
        c2.a(dict.optBoolean("has_blob"));
        c2.f(dict.optString("blob"));
        c2.c(dict.optInt("type"));
        c2.d(dict.optString("avatar"));
        return c2;
    }

    public static u c(JSONObject dict) {
        u c2 = new u();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        c2.a(id);
        c2.b(dict.optString("message"));
        c2.a(dict.optInt("bid_point"));
        c2.b(dict.optInt("score"));
        c2.a(dict.optBoolean("has_blob"));
        return c2;
    }

    public static u d(JSONObject dict) {
        return b(dict);
    }

    public int a() {
        return this.j;
    }

    public void a(int bid) {
        this.j = bid;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean hasBlob) {
        this.k = hasBlob;
    }

    public String b() {
        return this.b;
    }

    public void b(int score) {
        this.i = score;
    }

    public void b(String message) {
        this.d = message;
    }

    public void b(boolean female) {
        this.g = female;
    }

    public String c() {
        return this.e;
    }

    public void c(int type) {
        this.h = type;
    }

    public void c(String fromUsername) {
        this.b = fromUsername;
    }

    public int d() {
        return this.i;
    }

    public void d(String avatarUrl) {
        this.e = avatarUrl;
    }

    public String e() {
        return this.c;
    }

    public void e(String fromUserId) {
        this.c = fromUserId;
    }

    public void f(String blobUrl) {
        this.f = blobUrl;
    }
}
