package com.wiyun.game.e;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class a {
    private static MessageDigest a() {
        return a("MD5");
    }

    static MessageDigest a(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] a(byte[] data) {
        return a().digest(data);
    }

    public static String b(byte[] data) {
        return d.b(a(data));
    }

    public static byte[] b(String data) {
        return a(d(data));
    }

    public static String c(String data) {
        return d.b(b(data));
    }

    private static byte[] d(String data) {
        return b.a(data);
    }
}
