package com.wiyun.game;

import com.wiyun.game.model.a.c;
import com.wiyun.game.model.a.f;
import java.util.List;

class x {
    static List<c> a;
    static List<f> b;

    x() {
    }

    static f a(String id) {
        if (b != null) {
            for (f a2 : b) {
                if (a2.a().equals(id)) {
                    return a2;
                }
            }
        }
        return null;
    }

    static void a(List<f> achievements) {
        b = achievements;
    }

    static boolean a() {
        return b != null && !b.isEmpty();
    }

    static int b() {
        if (a == null) {
            return 0;
        }
        return a.size();
    }

    static c b(String id) {
        for (c lb : a) {
            if (lb.b().equals(id)) {
                return lb;
            }
        }
        return null;
    }

    static void b(List<c> leaderboards) {
        a = leaderboards;
    }

    static void c() {
        if (a != null) {
            a.clear();
            a = null;
        }
        if (b != null) {
            b.clear();
            b = null;
        }
    }
}
