package com.wiyun.game.c;

public abstract class f extends e {
    private String i;
    private String j;
    private String k;
    private String l;

    public f(String name, String contentType, String charSet, String transferEncoding) {
        if (name == null) {
            throw new IllegalArgumentException("Name must not be null");
        }
        this.i = name;
        this.j = contentType;
        this.k = charSet;
        this.l = transferEncoding;
    }

    public String c() {
        return this.i;
    }

    public String d() {
        return this.j;
    }

    public String e() {
        return this.k;
    }

    public String f() {
        return this.l;
    }
}
