package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.BaseAdapter;
import com.wiyun.game.b.d;

class u extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ Home a;

    u(Home home) {
        this.a = home;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.wiyun.game.RESET".equals(action)) {
            d.a().b();
            WiGame.b();
            this.a.finish();
        } else if ("com.wiyun.game.EXIT".equals(action)) {
            this.a.finish();
        } else if ("com.wiyun.game.RELOAD".equals(action)) {
            this.a.e();
            String url = intent.getStringExtra("url");
            if (TextUtils.isEmpty(url)) {
                this.a.c.reload();
            } else {
                this.a.c.loadUrl(url);
            }
        } else if ("com.wiyun.game.CACHE_CLEARED".equals(action)) {
            this.a.c.reload();
        } else if ("com.wiyun.game.BLOB_DOWNLOADED".equals(action)) {
            this.a.ad = intent.getByteArrayExtra("blob");
            this.a.y();
        } else if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(action)) {
            final String id = intent.getStringExtra("image_id");
            if ("ach_".equals(h.f(id))) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        BaseAdapter adapter;
                        if (u.this.a.M != null && u.this.a.M.length > 0 && (adapter = (BaseAdapter) u.this.a.M[u.this.a.M.length - 1].getAdapter()) != null) {
                            ((Bitmap) u.this.a.Z.remove(id)).recycle();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        }
    }
}
