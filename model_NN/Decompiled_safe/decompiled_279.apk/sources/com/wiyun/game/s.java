package com.wiyun.game;

import android.widget.TextView;

class s extends WiGameClient {
    final /* synthetic */ SaveGameDialog a;

    s(SaveGameDialog saveGameDialog) {
        this.a = saveGameDialog;
    }

    public void wyGameSaveProgress(String name, int uploadedSize) {
        SaveGameDialog saveGameDialog = this.a;
        saveGameDialog.d = saveGameDialog.d + uploadedSize;
        this.a.a.setProgress(this.a.d);
        TextView d = this.a.b;
        Object[] objArr = new Object[2];
        objArr[0] = this.a.e;
        objArr[1] = Integer.valueOf(this.a.c == 0 ? 0 : (this.a.d * 100) / this.a.c);
        d.setText(String.format("%s%d%%", objArr));
    }

    public void wyGameSaveStart(String name, int totalSize) {
        this.a.c = totalSize;
        this.a.a.setMax(this.a.c);
    }
}
