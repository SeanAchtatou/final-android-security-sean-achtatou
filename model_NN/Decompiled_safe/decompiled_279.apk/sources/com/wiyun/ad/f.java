package com.wiyun.ad;

import android.os.Environment;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class f {
    private static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    f() {
    }

    public static int a(int i, String str) {
        return a(i, str, 0);
    }

    public static int a(int i, String str, int i2) {
        if (str == null) {
            return i2;
        }
        try {
            return (int) Long.parseLong(str.trim(), i);
        } catch (NumberFormatException e) {
            return i2;
        }
    }

    private static int a(String str, String str2, int i) {
        int length = str2.length();
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < length; i3++) {
            int indexOf = str.indexOf(str2.charAt(i3), i);
            if (indexOf != -1) {
                i2 = Math.min(i2, indexOf);
            }
        }
        if (i2 == Integer.MAX_VALUE) {
            return -1;
        }
        return i2;
    }

    public static String a(String str) {
        if (str.length() < 1 || "/".equals(str)) {
            return "";
        }
        String substring = str.endsWith("/") ? str.substring(0, str.length() - 1) : str;
        int lastIndexOf = substring.lastIndexOf(File.separatorChar);
        return lastIndexOf != -1 ? substring.substring(lastIndexOf + 1) : substring;
    }

    public static String a(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            int indexOf = str.indexOf("://");
            String[] split = str.substring(indexOf + 3).split("/");
            StringBuilder sb = new StringBuilder(str.substring(0, indexOf + 3));
            sb.append(split[0]);
            sb.append('/');
            for (int i = 1; i < split.length; i++) {
                String str3 = split[i];
                int indexOf2 = str3.indexOf(37);
                if (indexOf2 == -1) {
                    a(sb, str3, str2);
                } else {
                    int i2 = indexOf2;
                    int i3 = 0;
                    while (i2 != -1) {
                        a(sb, str3.substring(i3, i2), str2);
                        if (i2 == str3.length() - 1) {
                            sb.append("%25");
                            i3 = str3.length();
                            i2 = -1;
                        } else if (i2 < str3.length() - 2) {
                            char charAt = str3.charAt(i2 + 1);
                            if (charAt == '%') {
                                sb.append("%25");
                                i3 = i2 + 2;
                                i2 = str3.indexOf(37, i3);
                            } else if ((charAt < '0' || charAt > '9') && ((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z'))) {
                                sb.append("%25");
                                i3 = i2 + 1;
                                i2 = str3.indexOf(37, i3);
                            } else {
                                char charAt2 = str3.charAt(i2 + 2);
                                if ((charAt2 < '0' || charAt2 > '9') && ((charAt2 < 'A' || charAt2 > 'Z') && (charAt2 < 'a' || charAt2 > 'z'))) {
                                    sb.append("%25");
                                    i3 = i2 + 1;
                                    i2 = str3.indexOf(37, i3);
                                } else {
                                    sb.append(str3.substring(i2, i2 + 3));
                                    i3 = i2 + 3;
                                    i2 = str3.indexOf(37, i3);
                                }
                            }
                        } else {
                            sb.append("%25");
                            i3 = i2 + 1;
                            i2 = str3.indexOf(37, i3);
                        }
                    }
                    a(sb, str3.substring(i3), str2);
                }
                if (i < split.length - 1) {
                    sb.append('/');
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    private static void a(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        int a2 = a(str, "?&=:,()+ ", 0);
        if (a2 == -1) {
            sb.append(URLEncoder.encode(str, str2));
            return;
        }
        int i = a2;
        int i2 = 0;
        while (i != -1) {
            sb.append(URLEncoder.encode(str.substring(i2, i), str2));
            char charAt = str.charAt(i);
            if (charAt == ' ') {
                sb.append("%20");
            } else {
                sb.append(charAt);
            }
            i2 = i + 1;
            i = a(str, "?&=:,()+ ", i2);
        }
        sb.append(URLEncoder.encode(str.substring(i2), str2));
    }

    public static boolean a() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static boolean a(File file) {
        if (file == null) {
            return false;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        for (File a2 : file.listFiles()) {
            if (!a(a2)) {
                return false;
            }
        }
        return file.delete();
    }

    public static String b(String str) {
        return a(str, "gbk");
    }

    public static int c(String str) {
        return a(10, str);
    }

    public static final String d(String str) {
        int lastIndexOf = str.lastIndexOf(File.separatorChar);
        if (lastIndexOf == -1) {
            lastIndexOf = 0;
        }
        int lastIndexOf2 = str.substring(lastIndexOf).lastIndexOf(46);
        return (lastIndexOf2 == -1 || lastIndexOf2 == 0) ? "" : str.substring(lastIndexOf + lastIndexOf2 + 1);
    }
}
