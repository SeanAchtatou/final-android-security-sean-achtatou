package com.wiyun.ad;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

class k {
    k() {
    }

    public static int a(AdView adView) {
        int i;
        View view = adView;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams != null && layoutParams.width >= 0) {
                i = layoutParams.width;
                break;
            }
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                i3 += marginLayoutParams.leftMargin;
                i2 += marginLayoutParams.rightMargin;
            }
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View) parent;
                continue;
            } else {
                view = null;
                continue;
            }
            if (view == null) {
                i = 0;
                break;
            }
        }
        return Math.max(0, (i - i3) - i2);
    }
}
