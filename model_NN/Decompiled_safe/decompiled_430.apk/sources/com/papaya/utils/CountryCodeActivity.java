package com.papaya.utils;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.base.TitleActivity;
import com.papaya.si.C0030ba;
import com.papaya.si.C0042c;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.social.PPYSocial;
import java.util.Vector;

public class CountryCodeActivity extends TitleActivity {
    private static String[] gQ = {C0042c.getString("c_ch")};
    private static String[] gR = {"86"};
    public static String[] gS = new String[0];
    public static String[] gT = new String[0];
    private static String[] gU = {"United States of America", "United Kingdom", "Canada", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Argentina", "Armenia", "Aruba", "Ascension Island", "Australia", "Australian External Territories", "Austria", "Azerbaijan", "Bahrain", "Bangladesh", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic of", "Congo, Republic of", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Diego Garcia", "Djibouti", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Guadeloupe", "Guatemala", "Guinea", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Republic of Ireland", "Israel", "Italy and Vatican City", "Japan", "Jordan", "Kenya", "Kiribati (Gilbert Islands)", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte and Reunion", "Mexico", "Micronesia, Federated States of", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Nagorno", "Namibia", "Nauru", "Nepal", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue Island", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia, Kazakhstan.", "Rwanda", "Saint", "Saint Helena, Tristan da Cunha", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Netherlands", "Togo", "Tokelau", "Tonga", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu (Ellice Islands)", "Uganda", "Ukraine", "United Arab Emirates", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "Yemen", "Zambia", "Zimbabwe"};
    private static String[] gV = {"1", "44", "1", "93", "355", "213", "376", "244", "672", "54", "374", "297", "247", "61", "672", "43", "994", "973", "880", "375", "32", "501", "229", "975", "591", "387", "267", "55", "673", "359", "226", "257", "855", "237", "238", "236", "235", "56", "86", "57", "269", "243", "242", "682", "506", "225", "385", "53", "357", "420", "45", "246", "253", "670", "593", "20", "503", "240", "291", "372", "251", "500", "298", "679", "358", "33", "594", "689", "241", "220", "995", "49", "233", "350", "30", "299", "590", "502", "245", "592", "509", "504", "852", "36", "354", "91", "62", "98", "964", "353", "972", "39", "81", "962", "254", "686", "965", "996", "856", "371", "961", "266", "231", "218", "423", "370", "352", "853", "389", "261", "265", "60", "960", "223", "356", "692", "596", "222", "230", "262", "52", "691", "373", "377", "976", "382", "212", "258", "95", "374", "264", "674", "977", "687", "64", "505", "227", "234", "683", "850", "47", "968", "92", "680", "507", "675", "595", "51", "63", "48", "351", "974", "40", "7", "250", "508", "290", "685", "378", "239", "966", "221", "381", "248", "232", "65", "421", "386", "677", "252", "27", "82", "34", "94", "249", "597", "268", "46", "41", "963", "886", "992", "255", "66", "31", "228", "690", "676", "216", "90", "993", "688", "256", "380", "971", "598", "998", "678", "58", "84", "681", "967", "260", "263"};
    a gW;

    class a extends BaseAdapter {
        CountryCodeActivity gX;
        Vector<String> gY = new Vector<>();
        String gZ = null;
        private ImageButton ha;
        EditText hb;
        private Button hc;

        public a(CountryCodeActivity countryCodeActivity) {
            this.gX = countryCodeActivity;
            this.ha = (ImageButton) this.gX.findViewById(C0065z.id("searchbtn"));
            this.hb = (EditText) this.gX.findViewById(C0065z.id("searchname"));
            this.hc = (Button) this.gX.findViewById(C0065z.id("cancelbtn"));
            this.ha.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (a.this.gZ == null || !a.this.gZ.equals(a.this.hb.getText().toString())) {
                        a.this.gZ = a.this.hb.getText().toString();
                        if (a.this.b().booleanValue()) {
                            a.this.gY.removeAllElements();
                            for (int i = 0; i < CountryCodeActivity.gS.length; i++) {
                                if (CountryCodeActivity.gT[i].toLowerCase().contains(a.this.gZ.toLowerCase()) || CountryCodeActivity.gS[i].contains(a.this.gZ)) {
                                    a.this.gY.add(i + "");
                                }
                            }
                            a.this.gX.gW.notifyDataSetChanged();
                        }
                    }
                }
            });
            this.hc.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    a.this.gX.setResult(0);
                    a.this.gX.finish();
                }
            });
            this.hb.addTextChangedListener(new TextWatcher() {
                public final void afterTextChanged(Editable editable) {
                    if (a.this.hb.getText().toString().length() == 0) {
                        a.this.gZ = null;
                        a.this.gX.gW.notifyDataSetChanged();
                    }
                }

                public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }
            });
        }

        /* access modifiers changed from: package-private */
        public final Boolean b() {
            return Boolean.valueOf(this.gZ != null && this.gZ.length() > 0);
        }

        public final int getCount() {
            return b().booleanValue() ? this.gY.size() : CountryCodeActivity.gS.length;
        }

        public final Object getItem(int i) {
            return null;
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final int getItemViewType(int i) {
            return 0;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView = new TextView(this.gX);
            final int intValue = b().booleanValue() ? C0030ba.intValue(this.gY.get(i)) : i;
            textView.setText(C0030ba.format("%s(%s)", CountryCodeActivity.gT[intValue], CountryCodeActivity.gS[intValue]));
            textView.setTextSize(22.0f);
            textView.setTypeface(Typeface.create((String) null, 1));
            textView.setTextColor(-16777216);
            LinearLayout linearLayout = new LinearLayout(this.gX);
            linearLayout.addView(textView);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    a.this.gX.setResult(-1, new Intent().putExtra("CountryIndex", intValue).putExtra("action", CountryCodeActivity.this.getIntent().getStringExtra("action")));
                    a.this.gX.finish();
                }
            });
            return linearLayout;
        }

        public final int getViewTypeCount() {
            return 100;
        }
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0065z.layoutID("countrycode");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ListView listView = (ListView) findViewById(C0065z.id("countries"));
        if (PPYSocial.LANG_ZH_CN.equals(C0061v.bd)) {
            gS = gR;
            gT = gQ;
        } else if (PPYSocial.LANG_EN.equals(C0061v.bd)) {
            gS = gV;
            gT = gU;
        }
        listView.setDividerHeight(1);
        this.gW = new a(this);
        listView.setAdapter((ListAdapter) this.gW);
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public String title() {
        return getString(C0065z.stringID("countrycode"));
    }
}
