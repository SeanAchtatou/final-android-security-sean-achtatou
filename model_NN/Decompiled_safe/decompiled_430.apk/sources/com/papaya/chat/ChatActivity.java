package com.papaya.chat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.base.PotpActivity;
import com.papaya.si.A;
import com.papaya.si.C0007ae;
import com.papaya.si.C0009ag;
import com.papaya.si.C0012aj;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.D;
import com.papaya.si.F;
import com.papaya.si.G;
import com.papaya.si.H;
import com.papaya.si.I;
import com.papaya.si.J;
import com.papaya.si.K;
import com.papaya.si.L;
import com.papaya.si.Q;
import com.papaya.si.S;
import com.papaya.si.T;
import com.papaya.si.X;
import com.papaya.si.aS;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;
import com.papaya.view.CardImageView;
import com.papaya.view.ChatGroupUserListView;
import com.papaya.view.ChatRoomUserListView;
import com.papaya.view.CustomDialog;
import com.papaya.view.EmoticonPanelView;

public class ChatActivity extends PotpActivity implements ChatRoomUserListView.Delegate {
    private ListView cA;
    private ImageButton cB;
    private ImageButton cC;
    /* access modifiers changed from: private */
    public EditText cD;
    private ActionsImageButton cE;
    private K cF;
    /* access modifiers changed from: private */
    public D cG;
    private EmoticonPanelView.Delegate cH = new F(this);
    private aS<T> cI = new G(this);
    private aS<D> cJ = new H(this);
    private ActionsImageButton.Delegate cK = new I(this);
    private ActionsImageButton.Delegate cL = new J(this);
    private ImageView cr;
    private ImageView cs;
    private View ct;
    private View cu;
    private ActionsImageButton cv;
    private CardImageView cw;
    private CardImageView cx;
    private CardImageView cy;
    private TextView cz;

    /* access modifiers changed from: private */
    public void closeActiveChat() {
        T chattings = C0042c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cG);
        chattings.remove(this.cG);
        if (indexOf >= chattings.size()) {
            indexOf--;
        }
        this.cG.fireDataStateChanged();
        if (indexOf >= 0 && indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
        chattings.fireDataStateChanged();
    }

    /* access modifiers changed from: private */
    public void goNext() {
        T chattings = C0042c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cG) + 1;
        if (indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
    }

    /* access modifiers changed from: private */
    public void goPrevious() {
        T chattings = C0042c.getSession().getChattings();
        int indexOf = chattings.indexOf(this.cG) - 1;
        if (indexOf < 0) {
            indexOf = 0;
        }
        if (indexOf < chattings.size()) {
            refreshWithCard(chattings.get(indexOf), false);
        }
    }

    private void refreshActionsButton() {
        this.cv.clearActions();
        this.cv.addAction(new Action(0, C0042c.getDrawable("chat_icon_close"), C0042c.getString("close")));
        if (this.cG instanceof C0012aj) {
            C0012aj ajVar = (C0012aj) this.cG;
            if (ajVar.getUserID() > 0 && ajVar.getUserID() != C0042c.getSession().getUserID()) {
                this.cv.addAction(new Action(1, null, C0042c.getString("action_visit_home")));
                this.cv.addAction(new Action(3, null, C0042c.getString("action_remove_friend")));
            }
        } else if (this.cG instanceof Q) {
            this.cv.addAction(new Action(5, null, C0042c.getString("action_chatroom_users")));
            if (((Q) this.cG).di) {
                this.cv.addAction(new Action(6, null, C0042c.getString("action_disallow_private_chat")));
            } else {
                this.cv.addAction(new Action(7, null, C0042c.getString("action_allow_private_chat")));
            }
        } else if (this.cG instanceof C0009ag) {
            if (C0042c.getSession().getPrivateChatWhiteList().contains(Integer.valueOf(((C0009ag) this.cG).cU))) {
                this.cv.addAction(new Action(6, null, C0042c.getString("action_disallow_private_chat")));
            } else {
                this.cv.addAction(new Action(7, null, C0042c.getString("action_allow_private_chat")));
            }
        } else if (this.cG instanceof L) {
            L l = (L) this.cG;
            if (l.cR) {
                this.cv.addAction(new Action(9, null, C0042c.getString("action_chatgroup_manage")));
            }
            this.cv.addAction(new Action(10, null, C0042c.getString("action_chatgroup_users")));
            if (C0030ba.bitTest(l.cS, 0)) {
                this.cv.addAction(new Action(12, null, C0042c.getString("action_chatgroup_unblock")));
            } else {
                this.cv.addAction(new Action(11, null, C0042c.getString("action_chatgroup_block")));
            }
            if (!l.cR) {
                this.cv.addAction(new Action(8, null, C0042c.getString("action_chatgroup_leave")));
            } else {
                this.cv.addAction(new Action(8, null, C0042c.getString("action_chatgroup_admin_leave")));
            }
        }
        this.cE.clearActions();
        if (this.cG instanceof Q) {
            this.cE.addAction(new Action(1, null, C0042c.getString("action_visit_home")));
            this.cE.addAction(new Action(2, null, C0042c.getString("action_private_chat")));
            if (((Q) this.cG).dh) {
                this.cE.addAction(new Action(4, null, C0042c.getString("action_kick")));
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshChattingBar(T t) {
        int indexOf = t.indexOf(this.cG);
        if (indexOf == -1) {
            indexOf = 0;
        }
        this.cr.setVisibility(indexOf == 0 ? 4 : 0);
        this.cw.setVisibility(indexOf == 0 ? 4 : 0);
        if (indexOf - 1 < 0 || indexOf - 1 >= t.size()) {
            this.cw.refreshWithCard(null);
        } else {
            this.cw.refreshWithCard(t.get(indexOf - 1));
        }
        refreshActionsButton();
        if (this.cG != null) {
            this.cx.refreshWithCard(this.cG);
            this.cz.setText(this.cG.getTitle());
            setTitle(this.cG.getTitle());
        } else {
            this.cx.refreshWithCard(null);
            this.cz.setText((CharSequence) null);
            setTitle(getHintedTitle());
        }
        this.cs.setVisibility(indexOf + 1 < t.size() ? 0 : 4);
        this.cy.setVisibility(indexOf + 1 < t.size() ? 0 : 4);
        if (indexOf + 1 < t.size()) {
            this.cy.refreshWithCard(t.get(indexOf + 1));
        } else {
            this.cy.refreshWithCard(null);
        }
    }

    private void refreshInputArea() {
        this.cB.setVisibility((((this.cG instanceof C0012aj) || (this.cG instanceof C0007ae) || (this.cG instanceof L)) && this.cG.getState() != 0) ? 0 : 8);
    }

    /* access modifiers changed from: private */
    public void refreshWithCard(D d, boolean z) {
        try {
            if (this.cG != d) {
                if (this.cG != null) {
                    this.cG.setChatActive(false);
                    this.cG.unregisterMonitor(this.cJ);
                }
                this.cG = d;
                this.cG.registerMonitor(this.cJ);
            }
            this.cG.setChatActive(true);
            if (!z) {
                refreshChattingBar(C0042c.getSession().getChattings());
            }
            refreshInputArea();
            this.cF.refreshWithCard(this.cG);
        } catch (Exception e) {
            X.e(e, "failed to refresh card", new Object[0]);
        }
    }

    /* access modifiers changed from: private */
    public void sendText() {
        Editable text = this.cD.getText();
        String obj = text == null ? null : text.toString();
        if (!C0030ba.isEmpty(obj) && this.cG != null) {
            if (this.cG.state != 0) {
                this.cG.addSelfMessage(obj);
                C0042c.getSession().sendChatMessage(this.cG, obj);
                this.cG.fireDataStateChanged();
                this.cD.setText((CharSequence) null);
            } else if (this.cG instanceof C0012aj) {
                this.cG.addSystemMessage(((C0012aj) this.cG).getName() + C0042c.getString("base_status_offline"));
                this.cG.fireDataStateChanged();
            }
        }
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0065z.layoutID("chats");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bitmap bitmap = null;
        if (i2 == -1) {
            if (i == 1) {
                try {
                    bitmap = C0036bg.getCameraBitmap(this, intent, 240, 240, true);
                } catch (Exception e) {
                    X.e(e, "Failed to send photo", new Object[0]);
                    return;
                }
            } else if (i == 2) {
                bitmap = C0036bg.createScaledBitmap(getContentResolver(), intent.getData(), 240, 240, true);
            }
            if (bitmap != null) {
                byte[] compressBitmap = C0036bg.compressBitmap(bitmap, Bitmap.CompressFormat.JPEG, 35);
                if (compressBitmap != null && compressBitmap.length > 0) {
                    C0042c.getSession().sendPhoto(this.cG, compressBitmap, "jpg");
                }
                bitmap.recycle();
            }
        }
    }

    public void onChatroomUserSelected(ChatRoomUserListView chatRoomUserListView, S s) {
        if (s.cU != A.bK.getUserID()) {
            this.cE.payload = s;
            this.cE.onClick(this.cE);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.cr = (ImageView) C0036bg.find(this, "previous");
        this.cs = (ImageView) C0036bg.find(this, "next");
        this.ct = (View) C0036bg.find(this, "previous_content");
        this.cu = (View) C0036bg.find(this, "next_content");
        this.ct.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.goPrevious();
            }
        });
        this.cu.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.goNext();
            }
        });
        this.cv = (ActionsImageButton) C0036bg.find(this, "actions");
        this.cv.setActionDelegate(this.cK);
        this.cv.setCombinedDrawable(C0042c.getDrawable("chat_icon_dropdown"));
        this.cw = (CardImageView) C0036bg.find(this, "left_image");
        this.cx = (CardImageView) C0036bg.find(this, "middle_image");
        this.cy = (CardImageView) C0036bg.find(this, "right_image");
        this.cz = (TextView) C0036bg.find(this, "name");
        this.cA = (ListView) C0036bg.find(this, "messages");
        this.cF = new K(this);
        this.cA.setAdapter((ListAdapter) this.cF);
        this.cA.setOnItemClickListener(this.cF);
        this.cB = (ImageButton) C0036bg.find(this, "photo");
        this.cC = (ImageButton) C0036bg.find(this, "emoticon");
        this.cD = (EditText) C0036bg.find(this, "input");
        this.cB.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                if (ChatActivity.this.cG != null && ChatActivity.this.cG.getState() != 0) {
                    ChatActivity.this.showDialog(1);
                }
            }
        });
        this.cD.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return true;
                }
                ChatActivity.this.sendText();
                return true;
            }
        });
        this.cD.setOnKeyListener(new View.OnKeyListener() {
            public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i != 23 && i != 66) {
                    return false;
                }
                ChatActivity.this.sendText();
                return false;
            }
        });
        this.cC.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ChatActivity.this.showDialog(0);
            }
        });
        this.cE = new ActionsImageButton(this);
        this.cE.setActionDelegate(this.cL);
        T chattings = C0042c.getSession().getChattings();
        chattings.registerMonitor(this.cI);
        int intExtra = getIntent().getIntExtra("active", 0);
        if (intExtra >= 0 && intExtra < chattings.size()) {
            refreshWithCard(chattings.get(intExtra), false);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        CustomDialog create;
        switch (i) {
            case 0:
                EmoticonPanelView emoticonPanelView = new EmoticonPanelView(this);
                emoticonPanelView.setDelegate(this.cH);
                create = new CustomDialog.Builder(this).setView(emoticonPanelView).create();
                break;
            case 1:
                create = new CustomDialog.Builder(this).setItems(new CharSequence[]{C0042c.getString("web_up_photo_camera"), C0042c.getString("web_up_photo_pictures")}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            C0036bg.startCameraActivity(ChatActivity.this, 1);
                        } else {
                            C0036bg.startGalleryActivity(ChatActivity.this, 2);
                        }
                    }
                }).create();
                break;
            case 2:
                ChatRoomUserListView chatRoomUserListView = new ChatRoomUserListView(this);
                chatRoomUserListView.setDelegate(this);
                create = new CustomDialog.Builder(this).setView(chatRoomUserListView).create();
                break;
            case 3:
                create = new CustomDialog.Builder(this).setView(new ChatGroupUserListView(this)).create();
                break;
            case 4:
                create = new CustomDialog.Builder(this).setPositiveButton(C0042c.getString("btn_leave"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (ChatActivity.this.cG instanceof L) {
                            L l = (L) ChatActivity.this.cG;
                            ChatActivity.this.closeActiveChat();
                            C0042c.getSession().getChatGroups().remove(l);
                            C0042c.getSession().fireDataStateChanged();
                            if (l.cR) {
                                C0042c.send(306, Integer.valueOf(l.cQ));
                                return;
                            }
                            C0042c.send(303, Integer.valueOf(l.cQ));
                        }
                    }
                }).setNegativeButton(C0042c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).create();
                break;
            case 5:
                create = new CustomDialog.Builder(this).setPositiveButton(C0042c.getString("btn_remove"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        if (ChatActivity.this.cG instanceof C0012aj) {
                            C0012aj ajVar = (C0012aj) ChatActivity.this.cG;
                            C0042c.getSession().removeUserCard(ajVar);
                            C0042c.getSession().increaseRevision();
                            C0042c.send(601, Integer.valueOf(ajVar.getUserID()));
                            ChatActivity.this.closeActiveChat();
                        }
                    }
                }).setNegativeButton(C0042c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).create();
                break;
            default:
                X.e("unknown dialog id: " + i, new Object[0]);
                create = null;
                break;
        }
        if (create != null) {
            create.setCanceledOnTouchOutside(true);
        }
        return create;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.cG != null) {
            this.cG.setChatActive(false);
            this.cG.unregisterMonitor(this.cJ);
        }
        C0042c.getSession().getChattings().unregisterMonitor(this.cI);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.cF.setPaused(true);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        super.onPrepareDialog(i, dialog);
        switch (i) {
            case 2:
                if (this.cG instanceof Q) {
                    ((ChatRoomUserListView) ((CustomDialog) dialog).getCustomView()).refreshWithCard((Q) this.cG);
                    return;
                }
                return;
            case 3:
                if (this.cG instanceof L) {
                    ((ChatGroupUserListView) ((CustomDialog) dialog).getCustomView()).refreshWithGroup((L) this.cG);
                    return;
                }
                return;
            case 4:
                if (this.cG instanceof L) {
                    L l = (L) this.cG;
                    if (l.cR) {
                        ((CustomDialog) dialog).setMessage(C0030ba.format(C0042c.getString("alert_chatgroup_admin_leave_confirm"), l.name));
                        return;
                    }
                    ((CustomDialog) dialog).setMessage(C0030ba.format(C0042c.getString("alert_chatgroup_leave_confirm"), l.name));
                    return;
                }
                return;
            case 5:
                if (this.cG instanceof C0012aj) {
                    ((CustomDialog) dialog).setMessage(C0030ba.format(C0042c.getString("alert_remove_friend"), ((C0012aj) this.cG).getName()));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cF.setPaused(false);
    }
}
