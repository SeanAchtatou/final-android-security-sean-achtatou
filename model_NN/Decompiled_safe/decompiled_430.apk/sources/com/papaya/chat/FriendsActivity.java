package com.papaya.chat;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;
import com.papaya.base.PotpActivity;
import com.papaya.si.A;
import com.papaya.si.C0012aj;
import com.papaya.si.C0029b;
import com.papaya.si.C0031bb;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.Y;
import com.papaya.si.aS;
import com.papaya.view.CardImageView;
import com.papaya.view.CustomDialog;

public class FriendsActivity extends PotpActivity implements C0031bb {
    /* access modifiers changed from: private */
    public CardImageView dp;
    private View dq;
    /* access modifiers changed from: private */
    public TextView dr;
    private ImageButton ds;
    private ExpandableListView dt;
    private Y du;
    private aS<C0012aj> dv = new X(this);

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0065z.layoutID("friends");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        A.bK.registerMonitor(this.dv);
        this.dp = (CardImageView) C0036bg.find(this, "user_image");
        this.dp.setDefaultDrawable(C0042c.getDrawable("avatar_default"));
        this.dq = (View) C0036bg.find(this, "spinner");
        this.dr = (TextView) C0036bg.find(this.dq, "text");
        this.dq.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                FriendsActivity.this.showDialog(0);
            }
        });
        C0036bg.find(this, "search_button");
        this.ds = (ImageButton) C0036bg.find(this, "add_im_button");
        this.ds.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                C0029b.openPRIALink(FriendsActivity.this, "static_addim");
            }
        });
        this.dt = (ExpandableListView) C0036bg.find(this, "friends_list");
        this.dt.setDivider(null);
        this.du = new Y(this);
        this.dt.setAdapter(this.du);
        this.dt.setGroupIndicator(null);
        this.dt.setOnChildClickListener(this.du);
        this.dt.setDivider(new ColorDrawable(0));
        this.dt.setChildDivider(new ColorDrawable(Color.parseColor("#D4D4D4")));
        this.dt.setDividerHeight(1);
        this.dv.onDataStateChanged(A.bK);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != 0) {
            return null;
        }
        return new CustomDialog.Builder(this).setItems(new CharSequence[]{C0042c.getString("state_online"), C0042c.getString("state_busy"), C0042c.getString("state_idle")}, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        A.bK.setState(1);
                        C0042c.send(24, 29);
                        break;
                    case 1:
                        A.bK.setState(3);
                        C0042c.send(24, 72);
                        break;
                    case 2:
                        A.bK.setState(2);
                        C0042c.send(24, 28);
                        break;
                    default:
                        X.e("unknown state: " + i, new Object[0]);
                        break;
                }
                A.bK.fireDataStateChanged();
            }
        }).create();
    }

    public void onDestroy() {
        super.onDestroy();
        A.bK.unregisterMonitor(this.dv);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.du.setPaused(true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.du.setPaused(false);
    }
}
