package com.papaya.purchase;

import android.graphics.drawable.Drawable;
import org.json.JSONObject;

public class PPYPayment {
    private Drawable G;
    private CharSequence description;
    private int eG;
    private String eH;
    private CharSequence eI;
    private int eJ;
    private JSONObject eK;
    private PPYPaymentDelegate eL;
    private String eM;
    private String eN;

    public PPYPayment(CharSequence charSequence, CharSequence charSequence2, int i, JSONObject jSONObject, PPYPaymentDelegate pPYPaymentDelegate) {
        this.eI = charSequence;
        this.description = charSequence2;
        this.eJ = i;
        this.eK = jSONObject;
        this.eL = pPYPaymentDelegate;
    }

    public PPYPaymentDelegate getDelegate() {
        return this.eL;
    }

    public CharSequence getDescription() {
        return this.description;
    }

    public Drawable getIconDrawable() {
        return this.G;
    }

    public int getIconID() {
        return this.eG;
    }

    public String getIconURL() {
        return this.eH;
    }

    public CharSequence getName() {
        return this.eI;
    }

    public int getPapayas() {
        return this.eJ;
    }

    public JSONObject getPayload() {
        return this.eK;
    }

    public String getReceipt() {
        return this.eN;
    }

    public String getTransactionID() {
        return this.eM;
    }

    public void setDelegate(PPYPaymentDelegate pPYPaymentDelegate) {
        this.eL = pPYPaymentDelegate;
    }

    public void setIconDrawable(Drawable drawable) {
        this.G = drawable;
    }

    public void setIconID(int i) {
        this.eG = i;
    }

    public void setIconURL(String str) {
        this.eH = str;
    }

    public void setReceipt(String str) {
        this.eN = str;
    }

    public void setTransactionID(String str) {
        this.eM = str;
    }
}
