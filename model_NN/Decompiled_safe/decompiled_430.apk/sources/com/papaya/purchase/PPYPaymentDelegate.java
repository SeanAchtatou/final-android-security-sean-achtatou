package com.papaya.purchase;

public interface PPYPaymentDelegate {
    public static final int ERROR_NOT_ENOUGH_PAPAYAS = -1;
    public static final int ERROR_UNDEFINED = 0;

    void onPaymentClosed(PPYPayment pPYPayment);

    void onPaymentFailed(PPYPayment pPYPayment, int i, String str);

    void onPaymentFinished(PPYPayment pPYPayment);
}
