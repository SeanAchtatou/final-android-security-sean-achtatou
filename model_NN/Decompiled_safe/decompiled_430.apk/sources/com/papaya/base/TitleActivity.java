package com.papaya.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;

public class TitleActivity extends Activity {
    private TextView titleView;

    /* access modifiers changed from: protected */
    public View createContentView(Bundle bundle) {
        View inflate = getLayoutInflater().inflate(C0065z.layoutID("title"), (ViewGroup) null);
        ViewGroup viewGroup = (ViewGroup) inflate.findViewById(C0065z.id("title"));
        int myLayout = myLayout();
        if (myLayout != 0) {
            getLayoutInflater().inflate(myLayout, viewGroup);
        }
        return inflate;
    }

    public void finish() {
        C0029b.onFinished(this);
        super.finish();
    }

    /* access modifiers changed from: protected */
    public CharSequence getHintedTitle() {
        String stringExtra = getIntent().getStringExtra("hinted_title");
        return C0030ba.isEmpty(stringExtra) ? C0061v.bk : stringExtra;
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean needCustomTitle() {
        return !(getParent() instanceof EntryActivity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0029b.onCreated(this);
        if (!needCustomTitle()) {
            setContentView(createContentView(bundle));
            return;
        }
        requestWindowFeature(1);
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(C0065z.layoutID("content_notabbar"), (ViewGroup) null);
        this.titleView = (TextView) linearLayout.findViewById(C0065z.id("custom_title"));
        linearLayout.addView(createContentView(bundle), new LinearLayout.LayoutParams(-1, -1));
        setContentView(linearLayout);
        setTitle(getHintedTitle());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void onDestroy() {
        C0029b.onDestroyed(this);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (getParent() instanceof EntryActivity) {
            return false;
        }
        finish();
        return true;
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (menuItem.getItemId() == 1) {
            C0029b.openPRIALink(this, "static_more");
        }
        return true;
    }

    public void onPause() {
        C0029b.onPaused(this);
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 1, 0, C0065z.stringID("base_menu_5")).setIcon(C0065z.drawableID("menu_more"));
        return true;
    }

    public void onResume() {
        C0029b.onResumed(this);
        super.onResume();
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        if (this.titleView != null) {
            this.titleView.setText(charSequence);
        }
    }

    /* access modifiers changed from: protected */
    public String title() {
        return null;
    }
}
