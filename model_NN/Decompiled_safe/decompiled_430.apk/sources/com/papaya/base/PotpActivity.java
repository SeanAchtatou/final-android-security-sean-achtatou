package com.papaya.base;

import android.os.Bundle;
import com.papaya.si.C0042c;
import com.papaya.si.C0063x;
import com.papaya.si.C0064y;

public class PotpActivity extends TitleActivity implements C0063x, C0064y {
    public void finish() {
        super.finish();
        C0042c.A.removeConnectionDelegate(this);
    }

    public void onConnectionEstablished() {
    }

    public void onConnectionLost() {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0042c.A.addConnectionDelegate(this);
        C0042c.getSession().addSessionDelegate(this);
    }

    public void onDestroy() {
        super.onDestroy();
        C0042c.A.removeConnectionDelegate(this);
        C0042c.getSession().removeSessionDelegate(this);
    }

    public void onPotpSessionUpdated(String str, String str2) {
    }

    public void onResume() {
        super.onResume();
        C0042c.A.start();
    }
}
