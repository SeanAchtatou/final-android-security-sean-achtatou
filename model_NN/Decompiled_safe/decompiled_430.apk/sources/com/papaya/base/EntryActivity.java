package com.papaya.base;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import com.papaya.chat.FriendsActivity;
import com.papaya.si.C;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0057r;
import com.papaya.si.C0061v;
import com.papaya.si.D;
import com.papaya.si.T;
import com.papaya.si.aS;
import com.papaya.view.TabBar;
import com.papaya.view.TabBarContentView;
import com.papaya.web.WebActivity;
import java.util.HashMap;

public class EntryActivity extends ActivityGroup {
    private static HashMap<String, Integer> aM = new HashMap<>(5);
    private static HashMap<Integer, String> aN = new HashMap<>(5);
    private static HashMap<Integer, a> aO = new HashMap<>();
    private LinearLayout aP;
    private TabBar aQ;
    View aR;
    private HashMap<String, String> aS = new HashMap<>();
    private aS<C> aT = new C0057r(this);

    public static final class a {
        public Class aU;
        public String aV;
        public Drawable icon;
        public String name;

        private a() {
        }

        /* synthetic */ a(String str, Class cls, Drawable drawable, String str2) {
            this(str, cls, drawable, str2, (byte) 0);
        }

        private a(String str, Class cls, Drawable drawable, String str2, byte b) {
            this.name = str;
            this.aU = cls;
            this.icon = drawable;
            this.aV = str2;
        }

        public static void indicateUnread(D d, CharSequence charSequence) {
            refreshUnread();
        }

        public static void refreshUnread() {
            T chattings = C0042c.getSession().getChattings();
            int i = 0;
            for (int i2 = 0; i2 < chattings.size(); i2++) {
                D d = chattings.get(i2);
                i += d.cm == null ? 0 : d.cm.getUnread();
            }
            C0042c.getTabBadgeValues().setLabel("chat", i == 0 ? null : String.valueOf(i));
            C0042c.getTabBadgeValues().fireDataStateChanged();
        }
    }

    static {
        aM.put("home", 0);
        aM.put("chat", 3);
        aM.put("circle", 1);
        aM.put("game", 2);
        aM.put("location", 4);
        aN.put(0, "home");
        aN.put(3, "chat");
        aN.put(1, "circle");
        aN.put(2, "game");
        aN.put(4, "location");
    }

    private int getExtraActiveTabIndex() {
        int tabIndex = getTabIndex(getIntent().getStringExtra("active_tab"));
        if (tabIndex == -1) {
            return 0;
        }
        return tabIndex;
    }

    public static int getTabIndex(String str) {
        Integer num = aM.get(str);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    private static void initTabIndice() {
        if (aO.isEmpty()) {
            aO.put(0, new a(C0042c.getString("tab_home"), WebActivity.class, C0042c.getDrawable("icons_home"), "static_home"));
            aO.put(2, new a(C0061v.bl, WebActivity.class, C0042c.getDrawable("icons_game"), C0061v.bn));
            aO.put(3, new a(C0042c.getString("tab_friend"), FriendsActivity.class, C0042c.getDrawable("icons_chat"), null));
            aO.put(1, new a(C0042c.getString("tab_circles"), WebActivity.class, C0042c.getDrawable("icons_circle"), "static_mycircles"));
        }
    }

    public void finish() {
        C0029b.onFinished(this);
        C0042c.getTabBadgeValues().unregisterMonitor(this.aT);
        super.finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        getLocalActivityManager().getCurrentActivity().onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        C0029b.onCreated(this);
        C0042c.getTabBadgeValues().registerMonitor(this.aT);
        initTabIndice();
        this.aP = new LinearLayout(this) {
            public final void dispatchWindowFocusChanged(boolean z) {
                if (EntryActivity.this.aR != null) {
                    EntryActivity.this.aR.dispatchWindowFocusChanged(z);
                }
            }
        };
        this.aP.setOrientation(1);
        this.aQ = new TabBar(this);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 4) {
                a aVar = aO.get(Integer.valueOf(i2));
                this.aQ.getTabsView().addTab(aVar.icon, aVar.name);
                i = i2 + 1;
            } else {
                this.aP.addView(this.aQ, new LinearLayout.LayoutParams(-1, -2));
                this.aQ.getTabsView().setOnTabSelectionListener(new TabBarContentView.OnTabSelectionListener() {
                    public final void onTabSelected(TabBarContentView tabBarContentView, int i) {
                        EntryActivity.this.setCurrentTab(i);
                    }
                });
                setContentView(this.aP);
                setCurrentTab(getExtraActiveTabIndex());
                refreshBadgeValues();
                return;
            }
        }
    }

    public void onDestroy() {
        C0029b.onDestroyed(this);
        C0042c.getTabBadgeValues().unregisterMonitor(this.aT);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Activity currentActivity = getLocalActivityManager().getCurrentActivity();
        if (currentActivity == null || !currentActivity.onKeyDown(i, keyEvent)) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        C0029b.onPaused(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        C0029b.onResumed(this);
        super.onResume();
    }

    public void refreshBadgeValues() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 4) {
                String str = aN.get(Integer.valueOf(i2));
                String str2 = this.aS.get(str);
                this.aQ.getTabsView().getTabItem(i2).getBadgeView().setBadgeValue(str2 == null ? C0042c.getTabBadgeValues().getLabel(str, null) : str2);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void setCurrentTab(int i) {
        if (i >= 0 || i < aO.size()) {
            this.aQ.getTabsView().setFocusedTab(i);
            LocalActivityManager localActivityManager = getLocalActivityManager();
            String currentId = localActivityManager.getCurrentId();
            String str = "papaya" + i;
            if (currentId == null || !currentId.equals(str)) {
                Intent intent = new Intent(this, aO.get(Integer.valueOf(i)).aU);
                String str2 = null;
                if (getExtraActiveTabIndex() == i) {
                    str2 = getIntent().getStringExtra("active_tab_url");
                }
                if (C0030ba.isEmpty(str2)) {
                    str2 = aO.get(Integer.valueOf(i)).aV;
                }
                intent.putExtra(WebActivity.EXTRA_INIT_URL, str2);
                Window startActivity = localActivityManager.startActivity(str, intent);
                C0036bg.removeFromSuperView(this.aR);
                if (this.aR != null) {
                    this.aR.setVisibility(8);
                }
                this.aR = startActivity.getDecorView();
                this.aR.setVisibility(0);
                this.aR.requestFocus();
                this.aP.addView(this.aR, new LinearLayout.LayoutParams(-1, -1, 1.0f));
            }
        }
    }

    public void setLocalBadgeValue(String str, String str2) {
        this.aS.put(str, str2);
    }
}
