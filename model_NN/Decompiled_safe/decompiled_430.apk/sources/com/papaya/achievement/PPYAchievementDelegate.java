package com.papaya.achievement;

import android.graphics.Bitmap;
import java.util.List;

public interface PPYAchievementDelegate {
    void onDownloadIconSuccess(Bitmap bitmap);

    void onListFailed();

    void onListSuccess(List<PPYAchievement> list);

    void onLoadSuccess(PPYAchievement pPYAchievement);

    void onUnlockSuccess(Boolean bool);
}
