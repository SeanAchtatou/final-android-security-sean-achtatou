package com.papaya.service;

import org.json.JSONArray;

public class AccountManagerWrapper {
    public AppAccount[] listAccounts() {
        return new AppAccount[0];
    }

    public JSONArray listAccounts2JSON() {
        return AppAccount.toJSONArray(listAccounts());
    }

    public AppAccount[] listAccountsByType(String str) {
        return new AppAccount[0];
    }
}
