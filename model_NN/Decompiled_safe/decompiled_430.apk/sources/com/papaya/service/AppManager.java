package com.papaya.service;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0042c;
import com.papaya.si.X;
import com.papaya.si.aZ;
import com.papaya.social.BillingChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class AppManager {
    public static boolean INITIALIZED = false;
    /* access modifiers changed from: private */
    public static HashMap<String, AppInfo> eP = new HashMap<>();

    public static final class AppInfo {
        public String name;
        public String packageName;

        public AppInfo(String str, CharSequence charSequence) {
            this.packageName = str;
            this.name = charSequence == null ? null : charSequence.toString();
        }
    }

    public static synchronized String getName(String str) {
        String str2;
        synchronized (AppManager.class) {
            AppInfo appInfo = eP.get(str);
            str2 = appInfo != null ? appInfo.name : null;
        }
        return str2;
    }

    public static void initialize(Context context) {
        new AsyncTask<Void, Integer, HashMap<String, AppInfo>>() {
            private static HashMap<String, AppInfo> a() {
                HashMap<String, AppInfo> hashMap = new HashMap<>();
                try {
                    PackageManager packageManager = C0042c.getApplicationContext().getPackageManager();
                    for (ApplicationInfo next : packageManager.getInstalledApplications(BillingChannel.MISC)) {
                        hashMap.put(next.packageName, new AppInfo(next.packageName, packageManager.getApplicationLabel(next)));
                    }
                } catch (Exception e) {
                    X.e(e, "Failed in AppManager.initialize", new Object[0]);
                }
                return hashMap;
            }

            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
                return a();
            }

            /* access modifiers changed from: protected */
            public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
                HashMap hashMap = (HashMap) obj;
                synchronized (AppManager.class) {
                    try {
                        AppManager.eP.clear();
                        AppManager.eP.putAll(hashMap);
                        AppManager.INITIALIZED = true;
                        AppManager.sendReport();
                    } catch (Exception e) {
                        X.e(e, "Failed in onPostExecute", new Object[0]);
                    }
                }
            }
        }.execute(new Void[0]);
    }

    public static synchronized boolean isInstalled(String str) {
        boolean containsKey;
        synchronized (AppManager.class) {
            containsKey = eP.containsKey(str);
        }
        return containsKey;
    }

    public static synchronized boolean isInstalledByName(String str) {
        boolean z;
        synchronized (AppManager.class) {
            if (str != null) {
                Iterator<AppInfo> it = eP.values().iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (str.equals(it.next().name)) {
                            z = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public static void launchApplication(String str) {
        try {
            Intent launchIntentForPackage = C0042c.getApplicationContext().getPackageManager().getLaunchIntentForPackage(str);
            if (launchIntentForPackage != null) {
                C0029b.startActivity(launchIntentForPackage);
            }
        } catch (Exception e) {
            X.w(e, "failed to launch " + str, new Object[0]);
        }
    }

    public static synchronized List<String> listInstalled() {
        ArrayList arrayList;
        synchronized (AppManager.class) {
            arrayList = new ArrayList(eP.keySet());
        }
        return arrayList;
    }

    public static synchronized JSONArray listInstalled2JSON() {
        JSONArray jSONArray;
        synchronized (AppManager.class) {
            jSONArray = new JSONArray();
            for (String put : eP.keySet()) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static void sendReport() {
        try {
            if (INITIALIZED && C0042c.getSession().isLoggedIn()) {
                C0042c.send(113, aZ.compressZlib(C0030ba.getBytes(AppAccountManager.getWrapper().listAccounts2JSON().toString())), aZ.compressZlib(C0030ba.getBytes(listInstalled2JSON().toString())));
            }
        } catch (Exception e) {
            X.w(e, "Failed to sendReport", new Object[0]);
        }
    }
}
