package com.papaya.service;

import com.papaya.si.C0030ba;

public class AppAccountManager {
    private static AccountManagerWrapper eO;

    static {
        if (C0030ba.existClass("android.accounts.AccountManager")) {
            eO = (AccountManagerWrapper) C0030ba.newInstance("com.papaya.service.AccountManagerWrapper2x");
        }
        if (eO == null) {
            eO = new AccountManagerWrapper();
        }
    }

    public static AccountManagerWrapper getWrapper() {
        return eO;
    }
}
