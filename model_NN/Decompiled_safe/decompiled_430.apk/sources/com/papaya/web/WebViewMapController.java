package com.papaya.web;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.papaya.si.C0002a;
import com.papaya.si.C0018ap;
import com.papaya.si.C0020ar;
import com.papaya.si.C0021as;
import com.papaya.si.C0031bb;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.bA;
import com.papaya.si.bD;
import com.papaya.si.bp;
import com.papaya.si.by;
import java.net.URL;
import java.util.List;

public class WebViewMapController extends WebViewController {
    MapView oV = null;
    C0021as oW;

    class a extends Overlay implements C0031bb, by.a {
        private bA jT;
        private GeoPoint oZ;
        private int pa;
        private Bitmap pb = null;
        private Bitmap pc;
        private Bitmap pd;

        public a(GeoPoint geoPoint, String str, int i) {
            this.oZ = geoPoint;
            this.pa = i;
            if (bD.isContentUrl(str)) {
                this.pb = C0036bg.bitmapFromFD(C0002a.getWebCache().fdFromContentUrl(str));
                return;
            }
            Bitmap cachedBitmap = by.getCachedBitmap(str);
            if (cachedBitmap == null) {
                URL createURL = C0040bk.createURL(str);
                if (createURL != null) {
                    this.jT = new bA(createURL, true);
                    this.jT.setRequireSid(false);
                    this.jT.setDelegate(this);
                    this.jT.start(false);
                    return;
                }
                return;
            }
            this.pb = cachedBitmap;
        }

        public final void connectionFailed(by byVar, int i) {
        }

        public final void connectionFinished(by byVar) {
            if (byVar.getRequest() == this.jT) {
                try {
                    this.jT = null;
                    this.pb = byVar.getBitmap();
                    WebViewMapController.this.oV.invalidate();
                } catch (Exception e) {
                    X.w("Failed to execute bitmap callback: %s", e);
                }
            }
        }

        public final boolean draw(Canvas canvas, MapView mapView, boolean z, long j) {
            WebViewMapController.super.draw(canvas, mapView, z);
            Paint paint = new Paint();
            paint.setColor(-16777216);
            paint.setTextSize(22.0f);
            Point point = new Point();
            mapView.getProjection().toPixels(this.oZ, point);
            int rp = C0036bg.rp(point.x);
            int rp2 = C0036bg.rp(point.y);
            if (this.pc == null) {
                this.pc = BitmapFactory.decodeResource(C0042c.getApplicationContext().getResources(), C0065z.drawableID("lbs_header"));
            }
            if (this.pb != null) {
                canvas.drawBitmap(this.pc, (Rect) null, new Rect(rp - C0036bg.rp(23), rp2 - C0036bg.rp(56), C0036bg.rp(23) + rp, rp2), (Paint) null);
                canvas.drawBitmap(this.pb, (Rect) null, new Rect((rp - C0036bg.rp(23)) + C0036bg.rp(4), (rp2 - C0036bg.rp(56)) + C0036bg.rp(5), (rp + C0036bg.rp(23)) - C0036bg.rp(5), (rp2 - C0036bg.rp(10)) - C0036bg.rp(5)), (Paint) null);
                return true;
            } else if (this.pa > 1) {
                canvas.drawBitmap(this.pc, (Rect) null, new Rect(rp - C0036bg.rp(23), rp2 - C0036bg.rp(56), C0036bg.rp(23) + rp, rp2), (Paint) null);
                canvas.drawText(this.pa + "+", (float) ((rp - C0036bg.rp(23)) + C0036bg.rp(10)), (float) (rp2 - C0036bg.rp(33)), paint);
                return true;
            } else {
                if (this.pd == null) {
                    this.pd = BitmapFactory.decodeResource(C0042c.getApplicationContext().getResources(), C0065z.drawableID("mylocation"));
                }
                canvas.drawBitmap(this.pd, (float) (rp - (C0036bg.rp(this.pd.getWidth()) / 2)), (float) (rp2 - (C0036bg.rp(this.pd.getHeight()) / 2)), (Paint) null);
                return true;
            }
        }
    }

    public a addOverlay(int i, int i2, String str, int i3) {
        a aVar = new a(new GeoPoint(i, i2), str, i3);
        this.oV.getOverlays().add(aVar);
        this.oV.invalidate();
        return aVar;
    }

    public void animateTo(int i, int i2) {
        if (this.oV != null) {
            this.oV.getController().animateTo(new GeoPoint(i, i2));
            this.oV.getController().setZoom(17);
        }
    }

    public boolean canStartGPS() {
        return ((LocationManager) C0042c.getApplicationContext().getSystemService("location")).isProviderEnabled("network");
    }

    public void hideMap() {
        C0036bg.removeFromSuperView(this.oV);
    }

    public boolean removeOverlay(int i) {
        List overlays = this.oV.getOverlays();
        if (i < 0 || i >= overlays.size()) {
            return false;
        }
        overlays.remove(i);
        this.oV.invalidate();
        return true;
    }

    public void resumeMylocation() {
        if (this.oV != null) {
            this.oW.enableMyLocation();
        }
    }

    public void showMap(int i, int i2, int i3, int i4) {
        C0020ar.getInstance().register(this);
        if (this.oV == null) {
            this.oV = new MapView(getOwnerActivity(), C0018ap.getInstance().ep);
            this.oV.setEnabled(true);
            this.oV.setClickable(true);
            this.oV.setBuiltInZoomControls(true);
            this.oW = new C0021as(getOwnerActivity(), this.oV);
            this.oV.getOverlays().add(this.oW);
            this.oW.runOnFirstFix(new Runnable() {
                public final void run() {
                    C0036bg.runInHandlerThread(new Runnable() {
                        public final void run() {
                            WebViewMapController.this.oV.getController().animateTo(WebViewMapController.this.oW.getMyLocation());
                        }
                    });
                }
            });
        } else {
            List overlays = this.oV.getOverlays();
            overlays.clear();
            overlays.add(this.oW);
            C0036bg.removeFromSuperView(this.oV);
        }
        this.oV.invalidate();
        resumeMylocation();
        this.oV.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(getOwnerActivity(), i3, i4, i, i2));
        bp topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.addView(this.oV);
        }
        Location position = C0020ar.getInstance().getPosition();
        if (position != null) {
            this.oV.getController().animateTo(new GeoPoint((int) (position.getLatitude() * 1000000.0d), (int) (position.getLongitude() * 1000000.0d)));
        }
        this.oV.getController().setZoom(17);
    }

    public void stopLocation(boolean z) {
        if (this.oV != null) {
            this.oW.disableMyLocation();
            this.oW.disableCompass();
            if (z) {
                this.oV.getOverlays().clear();
                C0036bg.removeFromSuperView(this.oV);
            }
        }
        if (z) {
            C0020ar.getInstance().unregister(this);
        } else {
            C0020ar.getInstance().pause(this);
        }
    }
}
