package com.papaya.web;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.papaya.base.TitleActivity;
import com.papaya.si.C0012aj;
import com.papaya.si.C0035bf;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0048i;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.view.MaskLoadingView;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import org.json.JSONArray;

public class ExternalWebActivity extends TitleActivity {
    private WebView lZ;
    /* access modifiers changed from: private */
    public MaskLoadingView ma;

    public static final class a {
        ExternalWebActivity mc;

        public a(ExternalWebActivity externalWebActivity) {
            this.mc = externalWebActivity;
        }

        public final String DUID() {
            return C0035bf.hB;
        }

        public final String androidID() {
            return C0035bf.ANDROID_ID;
        }

        public final void closeAndCallback(final String str) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    Intent intent = new Intent();
                    intent.putExtra("script", str);
                    a.this.mc.setResult(-1, intent);
                    a.this.mc.finish();
                }
            });
        }

        public final void closeAndRedirect(final String str) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    Intent intent = new Intent();
                    intent.putExtra("url", str);
                    a.this.mc.setResult(-1, intent);
                    a.this.mc.finish();
                }
            });
        }

        public final void ga_track_event(final String str, final String str2, final String str3, final int i) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0048i.trackEvent(str, str2, str3, i);
                }
            });
        }

        public final void ga_track_page(final String str) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0048i.trackPageView(str);
                }
            });
        }

        public final String identifier() {
            return C0061v.bm;
        }

        public final int isFriend_(final int i) {
            return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
                public final /* bridge */ /* synthetic */ Object call() throws Exception {
                    return Integer.valueOf(C0042c.getSession().getFriends().findByUserID(i) != null ? 1 : 0);
                }
            }, 0)).intValue();
        }

        public final String language() {
            return C0061v.bd;
        }

        public final String listFriends_(final int i) {
            return (String) C0036bg.callInHandlerThread(new Callable<String>() {
                public final /* bridge */ /* synthetic */ Object call() throws Exception {
                    JSONArray jSONArray = new JSONArray();
                    ArrayList list = C0042c.getSession().getFriends().toList();
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= list.size()) {
                            break;
                        }
                        C0012aj ajVar = (C0012aj) list.get(i2);
                        if (ajVar.getName() != null) {
                            if (i <= 0) {
                                jSONArray.put(ajVar.getUserID());
                                jSONArray.put(ajVar.getName());
                            } else if (ajVar.getState() != 0) {
                                jSONArray.put(ajVar.getUserID());
                                jSONArray.put(ajVar.getName());
                            }
                        }
                        i = i2 + 1;
                    }
                    return jSONArray.length() == 0 ? "[]" : jSONArray.toString();
                }
            }, "[]");
        }

        public final int model() {
            return 6;
        }

        public final int supportPaypal() {
            return 1;
        }

        public final int supportZong() {
            return 1;
        }

        public final int userId() {
            return C0042c.getSession().getUserID();
        }

        public final String userNickname() {
            return C0042c.getSession().getDispname();
        }

        public final int version() {
            return C0061v.bc;
        }
    }

    public static Boolean isExternalUrl(String str) {
        return Boolean.valueOf(str != null && str.indexOf("://") > 0 && str.indexOf(C0061v.bh) < 0);
    }

    public void loadUrl(String str) {
        if (str != null) {
            this.lZ.loadUrl(str);
        }
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return C0065z.layoutID("external_url");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.lZ = (WebView) findViewById(C0065z.id("webview"));
        this.ma = new MaskLoadingView(this, 1, 0);
        this.ma.setVisibility(8);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        ((RelativeLayout) findViewById(C0065z.id("webbase"))).addView(this.ma, layoutParams);
        WebSettings settings = this.lZ.getSettings();
        settings.setCacheMode(-1);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setJavaScriptEnabled(true);
        settings.setLightTouchEnabled(true);
        settings.setSaveFormData(false);
        settings.setLoadsImagesAutomatically(true);
        this.lZ.setWebViewClient(new WebViewClient() {
            public final void onPageFinished(WebView webView, String str) {
                ExternalWebActivity.this.ma.setVisibility(8);
            }

            public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                ExternalWebActivity.this.ma.setVisibility(0);
            }

            public final void onReceivedError(WebView webView, int i, String str, String str2) {
            }

            public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                ExternalWebActivity.this.loadUrl(str);
                return true;
            }
        });
        this.lZ.addJavascriptInterface(new a(this), "_papaya_script");
        loadUrl(getIntent().getExtras().getString("url"));
    }

    public void onDestroy() {
        try {
            this.lZ.destroy();
        } catch (Exception e) {
            X.w("Failed to destroy webView: " + e, new Object[0]);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public String title() {
        return "what is this";
    }
}
