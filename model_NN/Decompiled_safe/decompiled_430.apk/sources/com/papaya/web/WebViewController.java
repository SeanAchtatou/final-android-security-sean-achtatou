package com.papaya.web;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.provider.MediaStore;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.papaya.si.A;
import com.papaya.si.C0002a;
import com.papaya.si.C0014al;
import com.papaya.si.C0017ao;
import com.papaya.si.C0019aq;
import com.papaya.si.C0020ar;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0031bb;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0047h;
import com.papaya.si.C0048i;
import com.papaya.si.C0060u;
import com.papaya.si.C0061v;
import com.papaya.si.C0063x;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.aC;
import com.papaya.si.aS;
import com.papaya.si.aU;
import com.papaya.si.bB;
import com.papaya.si.bG;
import com.papaya.si.bK;
import com.papaya.si.bL;
import com.papaya.si.bp;
import com.papaya.si.bs;
import com.papaya.si.bt;
import com.papaya.utils.CountryCodeActivity;
import com.papaya.view.AdWrapperView;
import com.papaya.view.CustomDialog;
import com.papaya.view.MaskLoadingView;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;

public class WebViewController extends bB<Activity> implements bp.a, bs.a, bt.a, C0063x, aS, C0031bb, C0019aq {
    private String aV;
    private Context cN;
    private MaskLoadingView ma;
    private boolean mz = true;
    private bK nE;
    private HashMap<String, C0014al> nG = new HashMap<>();
    private RelativeLayout oG;
    private FrameLayout oH;
    private URL oI;
    private LinkedList<bG> oJ = new LinkedList<>();
    private int oK = 0;
    private bs oL;
    private bt oM;
    private boolean oN = false;
    protected String oO;
    public AdWrapperView oP = null;

    public Object addOverlay(int i, int i2, String str, int i3) {
        return null;
    }

    public void animateTo(int i, int i2) {
    }

    public void callJS(String str) {
        bp topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.callJS(str);
        }
    }

    public boolean canStartGPS() {
        return false;
    }

    public void clearHistory(int i, bp bpVar) {
        if (i == 1) {
            while (this.oJ.size() > 1) {
                bG first = this.oJ.getFirst();
                if (bpVar != null && bpVar == first.getWebView()) {
                    break;
                }
                first.freeWebView();
                this.oJ.removeFirst();
            }
        } else if (i == 2) {
            int historyIndex = historyIndex(bpVar);
            if (historyIndex > 1) {
                for (int i2 = 1; i2 < historyIndex; i2++) {
                    this.oJ.get(1).freeWebView();
                    this.oJ.remove(1);
                }
            }
        } else {
            X.e("Unknown mode for clear history %d", Integer.valueOf(i));
        }
        updateActivityTitle(bpVar);
    }

    public void close() {
        this.nE.clear();
        for (C0014al close : this.nG.values()) {
            close.close();
        }
        this.nG.clear();
        if (C0042c.A != null) {
            C0042c.A.removeConnectionDelegate(this);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.oJ.size()) {
                bG bGVar = this.oJ.get(i2);
                C0036bg.removeFromSuperView(bGVar.getWebView());
                bGVar.freeWebView();
                i = i2 + 1;
            } else {
                this.oJ.clear();
                bL.getInstance().onControllerClosed(this);
                A.bK.unregisterMonitor(this);
                return;
            }
        }
    }

    public void configWebView(bp bpVar) {
        C0036bg.addView(this.oH, bpVar, true);
        bpVar.setController(this);
        bpVar.setDelegate(this);
        bpVar.setRequireSid(this.mz);
    }

    public int forceFreeWebViews(int i, boolean z) {
        int i2;
        int i3 = 0;
        int size = this.oJ.size();
        int i4 = i < 0 ? size : i;
        int i5 = 0;
        while (true) {
            if (i3 >= (z ? size - 1 : size)) {
                return i5;
            }
            bG bGVar = this.oJ.get(i3);
            bp webView = bGVar.getWebView();
            if (webView == null || !webView.isRecylable()) {
                i2 = i5;
            } else {
                bGVar.freeWebView();
                i2 = i5 + 1;
            }
            if (i2 >= i4) {
                return i2;
            }
            i3++;
            i5 = i2;
        }
    }

    public RelativeLayout getContentLayout() {
        return this.oG;
    }

    public Context getContext() {
        return this.cN;
    }

    public LinkedList<bG> getHistories() {
        return this.oJ;
    }

    public bp getTopWebView() {
        if (!this.oJ.isEmpty()) {
            return this.oJ.getLast().getWebView();
        }
        return null;
    }

    public bK getUIHelper() {
        return this.nE;
    }

    public ViewGroup getWebContentView() {
        return this.oH;
    }

    /* access modifiers changed from: protected */
    public void handlePapayaUrl(bp bpVar, String str, String str2, String str3, URL url) {
        String str4 = str == null ? "" : str;
        String str5 = str3 == null ? "" : str3;
        if (str4.startsWith("slide")) {
            if ("slideback".equals(str4) || !C0030ba.isEmpty(str5)) {
                this.oK = 0;
                if (str4.equals("slidetoright")) {
                    this.oK = 1;
                } else if (str4.equals("slidetoleft")) {
                    this.oK = 2;
                } else if (str4.equals("slidetotop")) {
                    this.oK = 4;
                } else if (str4.equals("slidetobottom")) {
                    this.oK = 3;
                } else if (str4.equals("slidenewpage")) {
                    this.oK = 5;
                } else if (str4.equals("slideback")) {
                    this.oK = 6;
                } else if (str4.startsWith("sliderefresh")) {
                    this.oK = 7;
                } else if (str4.startsWith("slideno")) {
                    this.oK = 0;
                }
            }
            if (!(bpVar == null || getTopWebView() == null || bpVar == getTopWebView())) {
                if (this.oK == 7 || this.oK == 0) {
                    this.oK = 5;
                } else if (this.oK == 6) {
                    X.w("Skip slide back request: %s, %s", bpVar, getTopWebView());
                    return;
                }
            }
            if (this.oK != 6) {
                openURL(C0040bk.createURL(str5, url), this.oK != 7);
            } else if (this.oJ.size() > 1) {
                this.oJ.getLast().freeWebView();
                this.oJ.removeLast();
                bG last = this.oJ.getLast();
                boolean openWebView = last.openWebView(this, null, true);
                C0036bg.addView(this.oH, last.getWebView(), true);
                updateActivityTitle(last.getWebView());
                if (openWebView) {
                    webViewPostProcess(getTopWebView(), false);
                }
            } else {
                X.w("incorrect history size %d", Integer.valueOf(this.oJ.size()));
            }
        } else if ("ajax".equals(str4)) {
            X.e("ajax is deprecated", new Object[0]);
            C0029b.showInfo("ajax is deprecated");
        } else if ("changehp".equals(str4)) {
            Activity ownerActivity = getOwnerActivity();
            if (ownerActivity != null) {
                new CustomDialog.Builder(ownerActivity).setTitle(C0065z.stringID("web_hp_title")).setItems(new CharSequence[]{C0042c.getString(C0065z.stringID("web_hp_camera")), C0042c.getString(C0065z.stringID("web_hp_pictures"))}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        Activity ownerActivity = WebViewController.this.getOwnerActivity();
                        if (i == 0) {
                            C0036bg.startCameraActivity(ownerActivity, 1);
                        } else if (i == 1) {
                            C0036bg.startGalleryActivity(ownerActivity, 2);
                        }
                    }
                }).show();
            }
        } else if ("uploadphoto".equals(str4)) {
            Activity ownerActivity2 = getOwnerActivity();
            if (ownerActivity2 != null) {
                new CustomDialog.Builder(ownerActivity2).setTitle(C0065z.stringID("web_up_photo_title")).setItems(new CharSequence[]{C0042c.getString(C0065z.stringID("web_up_photo_camera")), C0042c.getString(C0065z.stringID("web_up_photo_pictures"))}, new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        Activity ownerActivity = WebViewController.this.getOwnerActivity();
                        if (ownerActivity == null) {
                            return;
                        }
                        if (i == 0) {
                            C0036bg.startCameraActivity(ownerActivity, 3);
                        } else if (i == 1) {
                            ownerActivity.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 4);
                        }
                    }
                }).show();
            }
        } else if ("savetoalbum".equals(str4)) {
            int saveToPictures = this.oL.saveToPictures(str5, bpVar.getPapayaURL(), str2);
            if (saveToPictures == 1) {
                bpVar.callJS(C0030ba.format("photosaved(%d, '%s')", 1, str2));
            } else if (saveToPictures == -1) {
                bpVar.callJS(C0030ba.format("photosaved(%d, '%s')", 0, str2));
            }
        } else if ("uploadtopicasa".equals(str4)) {
            int uploadToPicasa = this.oM.uploadToPicasa(str5, bpVar.getPapayaURL(), str2);
            if (uploadToPicasa == 1) {
                bpVar.noWarnCallJS("picasaupload", C0030ba.format("picasaupload(%d, '%s')", 1, str2));
            } else if (uploadToPicasa == -1) {
                bpVar.noWarnCallJS("picasaupload", C0030ba.format("picasaupload(%d, '%s')", 0, str2));
            }
        } else if ("createselector".equals(str4)) {
            X.w("createselector is deprecated", new Object[0]);
            if (bpVar != null) {
                bpVar.callJS(C0030ba.format("window.Papaya.selectorShow_(%s)", str2));
            }
        } else if ("createdatepicker".equals(str4)) {
            X.w("createdatepicker is deprecated", new Object[0]);
            if (bpVar != null) {
                bpVar.callJS(C0030ba.format("window.Papaya.datePickerShow_(%s)", str2));
            }
        } else if ("showpictures".equals(str4)) {
            if (bpVar != null) {
                bpVar.callJS(C0030ba.format("window.Papaya.picturesShow_(%s)", str2));
            }
        } else if ("showalert".equals(str4)) {
            if (bpVar != null) {
                bpVar.callJS(C0030ba.format("window.Papaya.alertShow_(%s)", str2));
            }
        } else if ("synccontactsimport".equals(str4)) {
            C0060u.b.importContacts();
        } else if ("synccontactsexport".equals(str4)) {
            C0060u.b.exportContacts();
        } else if ("synccontactsmerge".equals(str4)) {
            C0060u.b.mergeContacts();
        } else if ("startchat".equals(str4)) {
            C0042c.getSession().startChat(C0030ba.intValue(str5));
        } else if ("showmultiplelineinput".equals(str4)) {
            if (bpVar != null) {
                bpVar.callJS(C0030ba.format("window.Papaya.multipleLineInputShow_(%s)", str2));
            }
        } else if ("showaddressbook".equals(str4)) {
            X.e("social sdk doesn't support addressbook", new Object[0]);
        } else if ("switchtab".equals(str4)) {
            C0029b.openPRIALink(getOwnerActivity(), str5, str2);
        } else if ("showcountry".equals(str4)) {
            Activity ownerActivity3 = getOwnerActivity();
            if (ownerActivity3 != null) {
                ownerActivity3.startActivityForResult(new Intent(ownerActivity3, CountryCodeActivity.class).putExtra("action", str2), 6);
            }
        } else if ("clearcache".equals(str4)) {
            C0002a.clearCaches();
        } else if ("papayalogout".equals(str4)) {
            C0042c.getSession().logout();
            C0036bg.runInHandlerThreadDelay(new Runnable() {
                public final void run() {
                    C0042c.quit();
                }
            });
        } else if ("showchatgroupinvite".equals(str4)) {
            X.e("showchatgroupinvite is deprecated", new Object[0]);
        } else if ("openMarket".equals(str4)) {
            C0036bg.openExternalUri(getOwnerActivity(), str5);
        } else if ("go".equals(str4)) {
            C0029b.openPRIALink(getOwnerActivity(), str5, str2);
        } else if (!C0042c.getWebGameBridge().handlePapayaUrl(this, bpVar, str4, str2, str5, url)) {
            X.w("unknown papaya uri %s %s, %s, %s", str4, str2, str5, url);
        }
    }

    public void hideLoading() {
        if (C0036bg.isMainThread()) {
            hideLoadingInUIThread();
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    WebViewController.this.hideLoadingInUIThread();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void hideLoadingInUIThread() {
        try {
            this.ma.setVisibility(8);
        } catch (Exception e) {
            X.e(e, "Failed to hide loadingView", new Object[0]);
        }
    }

    public void hideMap() {
    }

    public int historyIndex(bp bpVar) {
        if (bpVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.oJ.size()) {
                    break;
                } else if (bpVar == this.oJ.get(i2).getWebView()) {
                    return i2;
                } else {
                    i = i2 + 1;
                }
            }
        }
        return -1;
    }

    public void initialize(Context context, String str) {
        this.cN = context;
        if (this.cN instanceof Activity) {
            setOwnerActivity((Activity) this.cN);
        }
        this.oG = new RelativeLayout(context);
        this.oG.setBackgroundColor(-1);
        this.aV = str;
        this.oI = C0040bk.createURL(this.aV);
        setupViews();
        this.nE = new bK();
        bL.getInstance().onControllerCreated(this);
        C0042c.A.addConnectionDelegate(this);
        A.bK.registerMonitor(this);
    }

    public boolean isRequireSid() {
        return this.mz;
    }

    public boolean isSupportReload() {
        return this.oN;
    }

    public boolean onBackClicked() {
        bp topWebView = getTopWebView();
        if (topWebView == null) {
            return false;
        }
        String jsonString = C0040bk.getJsonString(C0040bk.getJsonObject(this.oJ.getLast().getTitleCtx(), "leftbtn"), "action");
        if (!C0030ba.isEmpty(jsonString)) {
            topWebView.callJS(jsonString);
            return true;
        } else if (this.oJ.size() <= 1) {
            return false;
        } else {
            shouldOverrideUrlLoading(topWebView, "papaya://slideback");
            return true;
        }
    }

    public void onBestLocation(Location location) {
        bp topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.noWarnCallJS("client_updatePosition", C0030ba.format("client_updatePosition('%s','%s')", Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude())));
        }
    }

    public void onConnectionEstablished() {
        openInitUrlIfPossible();
    }

    public void onConnectionLost() {
    }

    public boolean onDataStateChanged(aU aUVar) {
        openInitUrlIfPossible();
        return false;
    }

    public void onOrientationChanged(int i) {
        bp topWebView = getTopWebView();
        if (topWebView != null) {
            topWebView.changeOrientation(i);
        }
    }

    public void onPageFinished(bp bpVar, String str) {
        if (!bpVar.isClosed()) {
            if (bpVar == getTopWebView()) {
                bpVar.setVisibility(0);
            } else {
                X.e("not the top webview!!!, %s", str);
            }
            bpVar.updateTitleFromHTML();
            bpVar.noWarnCallJS("webloading", "webloading()");
            webViewPostProcess(bpVar, true);
            bpVar.setLoadFromString(false);
            updateActivityTitle(bpVar);
        }
    }

    public void onPageStarted(bp bpVar, String str, Bitmap bitmap) {
        showLoading();
        bpVar.getPapayaScript().clearSessionState();
    }

    public void onPause() {
        bp webView;
        try {
            if (!this.oJ.isEmpty() && (webView = this.oJ.getLast().getWebView()) != null) {
                webView.noWarnCallJS("webdisappeared", "webdisappeared();");
            }
            stopLocation(false);
        } catch (Exception e) {
            X.e(e, "Failed in onPause", new Object[0]);
        }
    }

    public void onPhotoPicasa(URL url, URL url2, String str, boolean z) {
        bp topWebView = getTopWebView();
        if (topWebView != null && url2 == topWebView.getPapayaURL()) {
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(z ? 1 : 0);
            objArr[1] = str;
            topWebView.callJS(C0030ba.format("picasaupload(%d, '%s')", objArr));
        }
    }

    public void onPhotoSaved(URL url, URL url2, String str, boolean z) {
        bp topWebView = getTopWebView();
        if (topWebView != null && url2 == topWebView.getPapayaURL()) {
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(z ? 1 : 0);
            objArr[1] = str;
            topWebView.callJS(C0030ba.format("photosaved(%d, '%s')", objArr));
        }
    }

    public void onReceivedError(bp bpVar, int i, String str, String str2) {
        hideLoading();
        this.oO = str2;
        showLoadError();
    }

    public void onResume() {
        try {
            if (!this.oJ.isEmpty()) {
                bG last = this.oJ.getLast();
                bp webView = last.getWebView();
                if (webView != null) {
                    webView.noWarnCallJS("webappeared", "webappeared(false);");
                } else {
                    last.openWebView(this, last.getURL(), true);
                }
            } else {
                openInitUrlIfPossible();
            }
            C0020ar.getInstance().resume(this);
        } catch (Exception e) {
            X.e(e, "Failed in onResume", new Object[0]);
        }
    }

    public void onWebLoaded(bp bpVar) {
        updateActivityTitle(bpVar);
    }

    public C0014al openDatabase(String str) {
        C0014al alVar = this.nG.get(str);
        if (alVar != null) {
            return alVar;
        }
        C0014al openMemoryDatabase = C0014al.openMemoryDatabase();
        openMemoryDatabase.setDbId(str);
        openMemoryDatabase.setScope(2);
        this.nG.put(str, openMemoryDatabase);
        return openMemoryDatabase;
    }

    public void openInitUrlIfPossible() {
        if ((!this.mz || aC.getInstance().isConnected() || C0017ao.isSessionLess(this.aV)) && getTopWebView() == null && C0017ao.getInstance().isReady()) {
            openUrl(this.aV);
        } else if (getTopWebView() == null) {
            showLoading();
        }
    }

    /* access modifiers changed from: protected */
    public void openURL(URL url, boolean z) {
        if (url != null) {
            if (!this.oJ.isEmpty()) {
                if (this.oK == 5 || this.oK == 6) {
                    this.oJ.getLast().hideWebView();
                } else {
                    this.oJ.getLast().freeWebView();
                    this.oJ.removeLast();
                }
            } else if (this.oI != null && !C0040bk.urlEquals(this.oI, url)) {
                this.oJ.addLast(new bG(this.oI, null));
            }
            bG bGVar = new bG(url, null);
            boolean openWebView = bGVar.openWebView(this, url, z);
            this.oJ.addLast(bGVar);
            C0036bg.addView(this.oH, bGVar.getWebView(), true);
            updateActivityTitle(bGVar.getWebView());
            if (openWebView) {
                webViewPostProcess(getTopWebView(), false);
            }
        }
    }

    public void openUrl(String str) {
        if (C0030ba.isEmpty(str)) {
            X.i("skip null url in openUrl", new Object[0]);
        } else {
            shouldOverrideUrlLoading(null, str);
        }
    }

    public void resumeMyLocation() {
    }

    public void setRequireSid(boolean z) {
        this.mz = z;
    }

    public void setSupportReload(boolean z) {
        this.oN = z;
    }

    /* access modifiers changed from: protected */
    public void setupViews() {
        this.oL = new bs(this.cN);
        this.oL.setDelegate(this);
        this.oM = new bt(this.cN);
        this.oM.setDelegate(this);
        this.oH = new FrameLayout(this.cN);
        this.oG.addView(this.oH, new RelativeLayout.LayoutParams(-1, -1));
        this.ma = new MaskLoadingView(this.cN, 1, 0);
        this.ma.setVisibility(8);
        this.oG.addView(this.ma, new RelativeLayout.LayoutParams(-1, -1));
    }

    public boolean shouldOverrideUrlLoading(bp bpVar, String str) {
        String str2;
        String str3;
        String str4 = null;
        URL papayaURL = bpVar != null ? bpVar.getPapayaURL() : null;
        URL url = papayaURL == null ? C0061v.bj : papayaURL;
        try {
            int indexOf = str.indexOf("://");
            if (indexOf == -1 || indexOf + 3 >= str.length()) {
                str2 = null;
                str3 = str;
            } else {
                str3 = str.substring(indexOf + 3);
                str2 = str.substring(0, indexOf);
            }
            if ("papaya".equals(str2)) {
                int indexOf2 = str3.indexOf(63);
                String substring = (indexOf2 == -1 || indexOf2 == str3.length() - 1) ? null : str3.substring(indexOf2 + 1);
                String substring2 = indexOf2 != -1 ? str3.substring(0, indexOf2) : str3;
                int indexOf3 = substring2.indexOf(126);
                if (!(indexOf3 == -1 || indexOf3 == substring2.length() - 1)) {
                    str4 = substring2.substring(indexOf3 + 1);
                }
                try {
                    handlePapayaUrl(bpVar, indexOf3 != -1 ? substring2.substring(0, indexOf3) : substring2, str4, substring, url);
                } catch (Exception e) {
                    X.e(e, "failed to handle papaya url %s", substring);
                }
                return true;
            }
            if (bpVar != null) {
                if (bpVar.isLoadFromString()) {
                    return bpVar == null || !bpVar.isLoadFromString();
                }
            }
            handlePapayaUrl(bpVar, "slideno", null, str, url);
            return true;
        } catch (Exception e2) {
            X.e(e2, "Failed in shouldOverrideUrlLoading", new Object[0]);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void showLoadError() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController.this.showLoadErrorInUIThread();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showLoadErrorInUIThread() {
        Activity ownerActivity = getOwnerActivity();
        if (ownerActivity == null) {
            return;
        }
        if (this.oN) {
            new CustomDialog.Builder(ownerActivity).setTitle(C0065z.stringID("error")).setMessage(C0065z.stringID("fail_load_page")).setCancelable(false).setNegativeButton(C0065z.stringID("close"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setPositiveButton(C0065z.stringID("retry"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    if (WebViewController.this.oO != null) {
                        bp topWebView = WebViewController.this.getTopWebView();
                        if (topWebView != null) {
                            topWebView.loadPapayaURL(C0040bk.createURL(WebViewController.this.oO, topWebView.getPapayaURL()));
                        }
                        WebViewController.this.oO = null;
                    }
                }
            }).show();
        } else {
            new CustomDialog.Builder(ownerActivity).setMessage(C0065z.stringID("fail_load_page")).setNegativeButton(C0065z.stringID("close"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    /* access modifiers changed from: protected */
    public void showLoading() {
        showLoading(null);
    }

    public void showLoading(final String str) {
        if (C0036bg.isMainThread()) {
            showLoadingInUIThread(str);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    WebViewController.this.showLoadingInUIThread(str);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void showLoadingInUIThread(String str) {
        if (str != null) {
            try {
                this.ma.getLoadingView().getTextView().setText(str);
            } catch (Exception e) {
                X.w("show loading e:%s", e.toString());
                return;
            }
        }
        this.ma.setVisibility(0);
    }

    public void showMap(int i, int i2, int i3, int i4) {
    }

    public void stopLocation(boolean z) {
    }

    public void updateActivityTitle(bp bpVar) {
        Activity ownerActivity = getOwnerActivity();
        if (ownerActivity != null && !this.oJ.isEmpty()) {
            CharSequence title = this.oJ.getLast().getTitle();
            if (C0030ba.isEmpty(title)) {
                title = C0061v.bk;
            }
            ownerActivity.setTitle(title);
        }
    }

    /* access modifiers changed from: protected */
    public void webViewPostProcess(bp bpVar, boolean z) {
        stopLocation(true);
        hideLoading();
        if (bpVar != null) {
            WebViewController controller = bpVar.getController();
            if (controller != null) {
                C0036bg.removeFromSuperView(controller.oP);
            }
            if (z) {
                bpVar.noWarnCallJS("webloaded", "webloaded()");
                bpVar.noWarnCallJS("webappeared", "webappeared(true)");
            } else {
                bpVar.noWarnCallJS("webappeared", "webappeared(false)");
            }
            if (bpVar.getPageName() != null) {
                C0048i.trackPageView(bpVar.getPageName());
            }
        }
        C0047h.pageView();
    }
}
