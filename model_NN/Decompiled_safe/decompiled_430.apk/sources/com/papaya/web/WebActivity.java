package com.papaya.web;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import com.papaya.base.TitleActivity;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.G;
import com.papaya.si.X;
import com.papaya.si.bA;
import com.papaya.si.bJ;
import com.papaya.si.bu;
import com.papaya.si.by;
import com.papaya.social.BillingChannel;
import com.papaya.utils.CountryCodeActivity;
import com.papaya.view.TakePhotoBridge;
import java.io.ByteArrayOutputStream;

public class WebActivity extends TitleActivity implements by.a {
    public static final String EXTRA_BACKABLE = "extra_backable";
    public static final String EXTRA_INIT_URL = "init_url";
    public static final String EXTRA_REQUIRE_SID = "extra_require_sid";
    public static final String EXTRA_SUPPORT_MENU = "extra_support_menu";
    private WebViewController mr;
    private boolean nq = true;
    private boolean nr = true;

    public void connectionFailed(by byVar, int i) {
        if (this.mr != null) {
            this.mr.hideLoading();
        }
    }

    public void connectionFinished(by byVar) {
        if (this.mr != null) {
            this.mr.hideLoading();
        }
    }

    /* access modifiers changed from: protected */
    public View createContentView(Bundle bundle) {
        this.mr = prepareWebViewController();
        this.mr.setOwnerActivity(this);
        this.nq = loadBackable();
        this.nr = loadSupportMenu();
        this.mr.setRequireSid(loadRequireSid());
        return this.mr.getContentLayout();
    }

    public void finish() {
        if (this.mr != null) {
            this.mr.stopLocation(true);
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public String getDefaultInitUrl() {
        return null;
    }

    /* access modifiers changed from: protected */
    public String getInitUrl() {
        String stringExtra = getIntent().getStringExtra(EXTRA_INIT_URL);
        return C0030ba.isNotEmpty(stringExtra) ? stringExtra : getDefaultInitUrl();
    }

    public String getTabName() {
        return "";
    }

    public WebViewController getWebViewController() {
        return this.mr;
    }

    /* access modifiers changed from: protected */
    public boolean loadBackable() {
        return getIntent().getBooleanExtra(EXTRA_BACKABLE, true);
    }

    /* access modifiers changed from: protected */
    public boolean loadRequireSid() {
        return getIntent().getBooleanExtra(EXTRA_REQUIRE_SID, true);
    }

    /* access modifiers changed from: protected */
    public boolean loadSupportMenu() {
        return getIntent().getBooleanExtra(EXTRA_SUPPORT_MENU, true);
    }

    /* access modifiers changed from: protected */
    public int myLayout() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
                if (i2 == -1) {
                    Bitmap bitmap = null;
                    if (i == 1) {
                        try {
                            bitmap = C0036bg.getCameraBitmap(this, intent, BillingChannel.MISC, BillingChannel.MISC, true);
                        } catch (Exception e) {
                            X.w("Failed to handle activity result: %d, %s", Integer.valueOf(i), e);
                            return;
                        }
                    } else if (i == 3) {
                        bitmap = C0036bg.getCameraBitmap(this, intent, 320, 320, true);
                    } else if (i == 2) {
                        bitmap = C0036bg.createScaledBitmap(getContentResolver(), intent.getData(), BillingChannel.MISC, BillingChannel.MISC, true);
                    } else if (i == 4) {
                        bitmap = C0036bg.createScaledBitmap(getContentResolver(), intent.getData(), 320, 320, true);
                    }
                    if (bitmap != null) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 35, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        if (byteArray.length > 0) {
                            if (i == 2 || i == 1) {
                                bA bAVar = new bA(C0040bk.createURL("json_changehp"), false);
                                bAVar.addPostParam("id", C0030ba.format("%d-%d", Integer.valueOf(hashCode()), 74565));
                                bAVar.addPostParam("type", "jpg");
                                bAVar.addPostParam("photo", byteArray, 2);
                                bAVar.setDelegate(this);
                                bAVar.setDispatchable(true);
                                bAVar.start(false);
                            } else {
                                bA bAVar2 = new bA(C0040bk.createURL("json_uploadpic"), false);
                                bAVar2.addPostParam("id", C0030ba.format("%d-%d", Integer.valueOf(hashCode()), 1193046));
                                bAVar2.addPostParam("type", "jpg");
                                bAVar2.addPostParam("photo", byteArray, 2);
                                bAVar2.setDelegate(this);
                                bAVar2.setDispatchable(true);
                                bAVar2.start(false);
                            }
                            bu.startUploading(this);
                            this.mr.showLoading();
                        }
                        bitmap.recycle();
                        return;
                    }
                    return;
                }
                return;
            case 5:
                return;
            case 6:
                if (i2 == -1) {
                    int intExtra = intent.getIntExtra("CountryIndex", 0);
                    String stringExtra = intent.getStringExtra("action");
                    String str = CountryCodeActivity.gT[intExtra];
                    String str2 = CountryCodeActivity.gS[intExtra];
                    if (stringExtra != null) {
                        this.mr.callJS(C0030ba.format("%s('%s', '%s')", stringExtra, C0040bk.escapeJS(str), str2));
                        return;
                    }
                    return;
                }
                return;
            case 7:
            case 9:
            default:
                X.w("unknown request %d", Integer.valueOf(i));
                return;
            case 8:
                G.onActivityFinished(i2, intent);
                return;
            case 10:
                if (i2 == -1 && intent != null) {
                    String stringExtra2 = intent.getStringExtra("url");
                    if (!C0030ba.isEmpty(stringExtra2)) {
                        this.mr.openUrl(stringExtra2);
                        return;
                    }
                    String stringExtra3 = intent.getStringExtra("script");
                    if (!C0030ba.isEmpty(stringExtra2)) {
                        this.mr.callJS(stringExtra3);
                        return;
                    }
                    return;
                }
                return;
            case 11:
            case 12:
                bJ.onPhotoTaken(this, i, i2, intent);
                return;
            case 13:
            case 14:
                TakePhotoBridge.onPhtotoTaken(this, i, i2, intent);
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.mr != null) {
            this.mr.onOrientationChanged(configuration.orientation);
        }
    }

    public void onDestroy() {
        if (this.mr != null) {
            this.mr.close();
            C0036bg.removeFromSuperView(this.mr.getContentLayout());
        }
        this.mr = null;
        super.onDestroy();
    }

    public void onImageUploadFailed(int i) {
        if (this.mr != null) {
            this.mr.hideLoading();
        }
        C0029b.showInfo(C0042c.getApplicationContext().getString(C0065z.stringID("fail_to_upload_photo")));
    }

    public void onImageUploaded(int i, int i2) {
        if (this.mr == null) {
            return;
        }
        if (i == 74565) {
            this.mr.hideLoading();
            this.mr.callJS("changehp()");
        } else if (i == 1193046) {
            this.mr.hideLoading();
            this.mr.callJS(C0030ba.format("photouploaded(%d)", Integer.valueOf(i2)));
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (this.mr != null && this.mr.onBackClicked()) {
                return true;
            }
            if (!this.nq) {
                return true;
            }
        } else if (i == 82 && !this.nr) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.mr != null) {
            this.mr.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.mr != null) {
            this.mr.onResume();
        }
    }

    public void openUrl(String str) {
        if (this.mr != null) {
            this.mr.openUrl(str);
        }
    }

    /* access modifiers changed from: protected */
    public WebViewController prepareWebViewController() {
        WebViewController webViewController = (WebViewController) C0030ba.newInstance("com.papaya.web.WebViewController");
        webViewController.initialize(C0042c.getApplicationContext(), getInitUrl());
        return webViewController;
    }

    public void priaBack(int i) {
        if (i == 0) {
            super.onKeyDown(4, null);
        } else {
            onKeyDown(4, null);
        }
    }
}
