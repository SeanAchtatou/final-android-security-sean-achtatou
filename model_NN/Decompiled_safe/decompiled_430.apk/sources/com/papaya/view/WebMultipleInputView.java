package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.papaya.si.C0030ba;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.bp;
import org.json.JSONObject;

public class WebMultipleInputView extends CustomDialog implements DialogInterface.OnClickListener {
    /* access modifiers changed from: private */
    public JSONObject kJ;
    /* access modifiers changed from: private */
    public bp kK;
    private Button lA;
    private EditText ll;

    public WebMultipleInputView(Context context) {
        super(context);
        View inflate = LayoutInflater.from(context).inflate(C0065z.layoutID("multilayout"), (ViewGroup) null);
        setView(inflate);
        this.lA = (Button) inflate.findViewById(C0065z.id("lbspic"));
        this.ll = (EditText) inflate.findViewById(C0065z.id("multitext"));
        setButton(-1, C0042c.getApplicationContext().getString(C0065z.stringID("base_cancel")), this);
    }

    public void configureWithJson(JSONObject jSONObject) {
        this.kJ = jSONObject;
        if (C0040bk.getJsonString(jSONObject, "initValue") != null) {
            this.ll.setHint(C0040bk.getJsonString(jSONObject, "initValue"));
        }
        String jsonString = C0040bk.getJsonString(jSONObject, "actionbtn", C0042c.getApplicationContext().getString(C0065z.stringID("done")));
        if (jsonString == null) {
            jsonString = C0042c.getApplicationContext().getString(C0065z.stringID("done"));
        }
        setButton(-2, jsonString, this);
        if (C0040bk.getJsonString(jSONObject, "attach") != null && "1".equals(C0040bk.getJsonString(jSONObject, "attach", "0"))) {
            this.lA.setVisibility(0);
            this.lA.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    WebMultipleInputView.this.dismiss();
                    WebMultipleInputView.this.kK.callJS(C0030ba.format("%s('%s')", C0040bk.getJsonString(WebMultipleInputView.this.kJ, "action"), C0040bk.getJsonString(WebMultipleInputView.this.kJ, "lcid")));
                }
            });
        }
        setTitle(C0040bk.getJsonString(jSONObject, "title"));
    }

    public bp getWebView() {
        return this.kK;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -2) {
            this.kK.callJS(C0030ba.format("%s('%s','%s')", C0040bk.getJsonString(this.kJ, "callback"), C0040bk.getJsonString(this.kJ, "id"), C0040bk.escapeJS(this.ll.getText().toString())));
        }
    }

    public void setWebView(bp bpVar) {
        this.kK = bpVar;
    }
}
