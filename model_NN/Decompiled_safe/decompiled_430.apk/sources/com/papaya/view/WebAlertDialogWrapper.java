package com.papaya.view;

import android.content.DialogInterface;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.bp;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebAlertDialogWrapper implements DialogInterface.OnClickListener {
    private bp iI;
    private JSONObject jz;
    private CustomDialog kM;

    public WebAlertDialogWrapper(CustomDialog customDialog, JSONObject jSONObject) {
        this.kM = customDialog;
        this.jz = jSONObject;
        configureWithJson(this.jz);
    }

    private void configureWithJson(JSONObject jSONObject) {
        this.jz = jSONObject;
        this.kM.setTitle(C0040bk.getJsonString(jSONObject, "title"));
        String jsonString = C0040bk.getJsonString(jSONObject, "text");
        if (jsonString == null) {
            jsonString = "Unknown message";
        }
        this.kM.setMessage(jsonString);
        int jsonInt = C0040bk.getJsonInt(jSONObject, "icon", -1);
        switch (jsonInt) {
            case -1:
                this.kM.setIcon(0);
                break;
            case 0:
                this.kM.setIcon(C0065z.drawableID("alert_icon_check"));
                break;
            case 1:
                this.kM.setIcon(C0065z.drawableID("alert_icon_warning"));
                break;
            case 2:
                this.kM.setIcon(C0065z.drawableID("alert_icon_help"));
                break;
            default:
                X.w("unknown icon id %d", Integer.valueOf(jsonInt));
                this.kM.setIcon(0);
                break;
        }
        JSONArray jsonArray = C0040bk.getJsonArray(jSONObject, "buttons");
        if (jsonArray == null || jsonArray.length() <= 0) {
            this.kM.setButton(-1, this.kM.getContext().getString(C0065z.stringID("alert_button_ok")), this);
            return;
        }
        for (int i = 0; i < Math.min(jsonArray.length(), 3); i++) {
            String jsonString2 = C0040bk.getJsonString(C0040bk.getJsonObject(jsonArray, i), "text");
            if (i == 0) {
                this.kM.setButton(-1, jsonString2, this);
            } else if (i == 1) {
                this.kM.setButton(-2, jsonString2, this);
            } else if (i == 2) {
                this.kM.setButton(-3, jsonString2, this);
            }
        }
    }

    public CustomDialog getAlert() {
        return this.kM;
    }

    public JSONObject getCtx() {
        return this.jz;
    }

    public bp getWebView() {
        return this.iI;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        JSONArray jsonArray = C0040bk.getJsonArray(this.jz, "buttons");
        if (jsonArray != null && jsonArray.length() > 0) {
            String jsonString = C0040bk.getJsonString(C0040bk.getJsonObject(jsonArray, i == -1 ? 0 : i == -2 ? 1 : i == -3 ? 2 : 0), "action");
            if (jsonString != null && this.iI != null) {
                this.iI.callJS(jsonString);
            }
        }
    }

    public void setAlert(CustomDialog customDialog) {
        this.kM = customDialog;
    }

    public void setWebView(bp bpVar) {
        this.iI = bpVar;
    }
}
