package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.bp;
import com.papaya.view.CustomDialog;
import com.papaya.view.TakePhotoBridge;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebMixedInputDialog extends CustomDialog implements DialogInterface.OnClickListener, JsonConfigurable, TakePhotoBridge.Receiver {
    private JSONObject kJ;
    /* access modifiers changed from: private */
    public bp kK;
    private String kL;
    private EditText lj;
    private View lk;
    private EditText ll;
    private View lm;
    private LinearLayout ln;
    private LinearLayout lo;
    private ImageButton lp;
    private TextView lq;
    private int lr;
    /* access modifiers changed from: private */
    public boolean ls;
    private int lt;
    private int lu;
    private String lv;
    private JSONArray lw = new JSONArray();
    private a lx = new a(this);
    /* access modifiers changed from: private */
    public int maxHeight;
    /* access modifiers changed from: private */
    public int maxWidth;
    /* access modifiers changed from: private */
    public int quality;

    final class a implements View.OnClickListener {
        /* synthetic */ a(WebMixedInputDialog webMixedInputDialog) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void onClick(View view) {
            new CustomDialog.Builder(WebMixedInputDialog.this.getContext()).setItems(new CharSequence[]{C0042c.getString("from_camera"), C0042c.getString("from_gallery")}, new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    TakePhotoBridge.startTakenPhoto(WebMixedInputDialog.this.kK.getOwnerActivity(), WebMixedInputDialog.this, new TakePhotoBridge.Config(i, WebMixedInputDialog.this.ls ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, WebMixedInputDialog.this.maxWidth, WebMixedInputDialog.this.maxHeight, WebMixedInputDialog.this.quality));
                }
            }).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public WebMixedInputDialog(Context context, String str, bp bpVar) {
        super(context);
        this.kL = str;
        this.kK = bpVar;
        setView(getLayoutInflater().inflate(C0065z.layoutID("mixed_input_view"), (ViewGroup) this.iV, false));
        this.lj = (EditText) f("singleedittext");
        this.lk = (View) f("separator1");
        this.ll = (EditText) f("edittext");
        this.lm = (View) f("separator2");
        this.ln = (LinearLayout) f("photos_layout");
        this.lo = (LinearLayout) f("photos_preview");
        this.lp = (ImageButton) f("photo_upload");
        this.lq = (TextView) f("photo_hint");
        this.lp.setOnClickListener(this.lx);
        this.lq.setOnClickListener(this.lx);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        int dialogButtonToWebIndex = C0040bk.dialogButtonToWebIndex(i);
        if (this.kK == null || C0030ba.isEmpty(this.lv)) {
            X.d("empty callback : webView %s, callback %s", this.kK, this.lv);
            return;
        }
        this.kK.callJSFunc("%s('%s', %d, '%s', '%s', '%s')", this.lv, this.kL, Integer.valueOf(dialogButtonToWebIndex), C0040bk.escapeJS(this.lj.getText()), C0040bk.escapeJS(this.ll.getText()), C0040bk.escapeJS(this.lw.toString()));
    }

    public void onPhotoCancel() {
        C0036bg.showToast(C0042c.getString("photo_can"), 1);
    }

    public void onPhotoTaken(String str) {
        this.lw.put(str);
        LazyImageView lazyImageView = new LazyImageView(getContext());
        lazyImageView.setImageUrl(str);
        lazyImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(C0036bg.rp(this.lt), C0036bg.rp(this.lu));
        layoutParams.leftMargin = C0036bg.rp(5);
        this.lo.addView(lazyImageView, layoutParams);
        this.lq.setVisibility(8);
        if (this.lw.length() >= this.lr) {
            this.lp.setVisibility(8);
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.kJ = jSONObject;
        setTitle(this.kJ.optString("title", C0061v.bk.toString()));
        int optInt = this.kJ.optInt("single_max_length", 0);
        if (optInt > 0) {
            this.lj.setFilters(new InputFilter[]{new InputFilter.LengthFilter(optInt)});
        } else {
            this.lj.setFilters(new InputFilter[0]);
            this.lj.setVisibility(optInt == 0 ? 8 : 0);
        }
        this.lj.setInputType(this.kJ.optInt("single_input_type", 1));
        this.lj.setHint(this.kJ.optString("single_hint"));
        this.lj.setTextColor(Color.parseColor(this.kJ.optString("single_text_color", "#000000")));
        this.lj.setTextSize((float) this.kJ.optInt("single_text_size", 16));
        String optString = this.kJ.optString("single_text", "");
        this.lj.setText(optString);
        this.lj.setSelection(Math.min(this.kJ.optInt("single_cursor", optString.length()), optString.length()));
        int optInt2 = this.kJ.optInt("max_length", -1);
        if (optInt2 > 0) {
            this.ll.setFilters(new InputFilter[]{new InputFilter.LengthFilter(optInt2)});
        } else {
            this.ll.setFilters(new InputFilter[0]);
            this.ll.setVisibility(optInt2 == 0 ? 8 : 0);
        }
        this.ll.setInputType(this.kJ.optInt("input_type", 1));
        this.ll.setHint(this.kJ.optString("hint"));
        this.ll.setTextColor(Color.parseColor(this.kJ.optString("text_color", "#929292")));
        this.ll.setTextSize((float) this.kJ.optInt("text_size", 14));
        int optInt3 = this.kJ.optInt("min_lines", 1);
        this.ll.setVisibility(optInt3 <= 0 ? 8 : 0);
        this.ll.setMinLines(optInt3);
        int optInt4 = this.kJ.optInt("max_lines", optInt3);
        this.ll.setSingleLine(optInt4 <= 1);
        this.ll.setMaxLines(optInt4);
        String optString2 = this.kJ.optString("text", "");
        this.ll.setText(optString2);
        this.ll.setSelection(Math.min(this.kJ.optInt("cursor", optString2.length()), optString2.length()));
        this.lr = this.kJ.optInt("max_photos");
        this.ln.setVisibility(this.lr > 0 ? 0 : 8);
        this.lq.setText(this.kJ.optString("photo_hint", C0042c.getString("photo_input_hint")));
        this.lq.setVisibility(0);
        this.lp.setVisibility(0);
        this.maxWidth = this.kJ.optInt("max_width", 70);
        this.maxHeight = this.kJ.optInt("max_height", 45);
        this.ls = "png".equals(this.kJ.optString("format"));
        this.quality = this.kJ.optInt("quality", 35);
        this.lt = this.kJ.optInt("preview_width", 70);
        this.lu = this.kJ.optInt("preview_width", 45);
        setPositiveButton(this.kJ.optString("positive_button", C0042c.getString("default_post_label")), this);
        setNegativeButton(this.kJ.optString("negative_button", C0042c.getString("default_cancel_label")), this);
        setNeutralButton(this.kJ.optString("neutral_button"), this);
        this.lk.setVisibility(8);
        this.lm.setVisibility(8);
        if (this.lj.getVisibility() == 0 && (this.ll.getVisibility() == 0 || this.ln.getVisibility() == 0)) {
            this.lk.setVisibility(0);
        }
        if (this.ln.getVisibility() == 0 && (this.lj.getVisibility() == 0 || this.ll.getVisibility() == 0)) {
            this.lm.setVisibility(0);
        }
        this.lv = this.kJ.optString("callback");
        this.lw = new JSONArray();
        this.lo.removeAllViews();
    }

    public void show() {
        if (this.kJ != null) {
            refreshWithCtx(this.kJ);
        }
        super.show();
    }
}
