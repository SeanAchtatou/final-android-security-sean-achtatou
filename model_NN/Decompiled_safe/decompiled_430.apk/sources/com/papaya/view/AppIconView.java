package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.papaya.si.C0042c;
import com.papaya.si.aM;
import com.papaya.si.aS;
import com.papaya.si.aU;

public class AppIconView extends LazyImageView implements aS {
    private int db;

    public AppIconView(Context context) {
        super(context);
    }

    public AppIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AppIconView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static String composeAppIconUrl(int i) {
        return "appicon?id=" + i;
    }

    public int getAppID() {
        return this.db;
    }

    public boolean onDataStateChanged(aU aUVar) {
        if (!C0042c.B.contains(this.db)) {
            return false;
        }
        setImageUrl(composeAppIconUrl(this.db));
        return true;
    }

    public void setApp(int i) {
        this.db = i;
        aM aMVar = C0042c.B;
        if (i == 0) {
            setImageUrl(null);
            aMVar.unregisterMonitor(this);
        } else if (aMVar.contains(i)) {
            setImageUrl(composeAppIconUrl(i));
        } else {
            aMVar.registerMonitor(this);
        }
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        setVisibility(drawable == null ? 8 : 0);
    }
}
