package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0030ba;
import com.papaya.si.C0040bk;
import com.papaya.si.bp;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebMenuView extends ListView {
    /* access modifiers changed from: private */
    public bp iI;
    private JSONObject jz;
    private a lc = new a(this);
    private AbsoluteLayout.LayoutParams ld;
    /* access modifiers changed from: private */
    public String le;
    /* access modifiers changed from: private */
    public JSONArray lf;
    /* access modifiers changed from: private */
    public int lg;
    /* access modifiers changed from: private */
    public int lh;

    class a extends BaseAdapter {
        /* synthetic */ a(WebMenuView webMenuView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final int getCount() {
            if (WebMenuView.this.lf != null) {
                return WebMenuView.this.lf.length();
            }
            return 0;
        }

        public final Object getItem(int i) {
            return C0040bk.getJsonObject(WebMenuView.this.lf, i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView;
            if (view == null) {
                textView = new TextView(viewGroup.getContext());
                textView.setLayoutParams(new AbsListView.LayoutParams(-1, WebMenuView.this.lg));
                textView.setGravity(16);
            } else {
                textView = (TextView) view;
            }
            textView.setText(C0040bk.getJsonString(C0040bk.getJsonObject(WebMenuView.this.lf, i), "text"));
            textView.setTextColor(-16777216);
            textView.setTextSize((float) WebMenuView.this.lh);
            return textView;
        }
    }

    public WebMenuView(Context context, String str, AbsoluteLayout.LayoutParams layoutParams) {
        super(context, null, 16842868);
        setClickable(true);
        setCacheColorHint(0);
        this.le = str;
        setAdapter((ListAdapter) this.lc);
        this.ld = layoutParams;
        setLayoutParams(layoutParams);
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (WebMenuView.this.iI != null) {
                    String jsonString = C0040bk.getJsonString(C0040bk.getJsonObject(WebMenuView.this.lf, i), "action");
                    if (jsonString == null) {
                        WebMenuView.this.iI.callJS(C0030ba.format("menutapped('%s', '%d')", WebMenuView.this.le, Integer.valueOf(i)));
                    } else {
                        WebMenuView.this.iI.callJS(jsonString);
                    }
                }
                WebMenuView.this.setSelection(i);
            }
        });
    }

    public String getName() {
        return this.le;
    }

    public bp getWebView() {
        return this.iI;
    }

    public void pack() {
        AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) getLayoutParams();
        int dividerHeight = (this.lg + getDividerHeight()) * this.lc.getCount();
        if (layoutParams.height > dividerHeight) {
            setLayoutParams(new AbsoluteLayout.LayoutParams(layoutParams.width, dividerHeight, layoutParams.x, layoutParams.y));
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.jz = jSONObject;
        setLayoutParams(this.ld);
        this.lg = C0040bk.getJsonInt(this.jz, "rowHeight", 24);
        this.lh = C0040bk.getJsonInt(this.jz, "fontSize");
        this.lf = C0040bk.getJsonArray(this.jz, "items");
        this.lc.notifyDataSetChanged();
        if (this.lf != null) {
            for (int i = 0; i < this.lf.length(); i++) {
                if (C0040bk.getJsonInt(C0040bk.getJsonObject(this.lf, i), "selected", 0) != 0) {
                    setSelection(i);
                }
            }
        }
    }

    public void setName(String str) {
        this.le = str;
    }

    public void setWebView(bp bpVar) {
        this.iI = bpVar;
    }
}
