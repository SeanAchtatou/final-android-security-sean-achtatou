package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.millennialmedia.android.MMAdView;
import com.papaya.si.C0002a;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.aD;
import com.papaya.si.aP;
import com.papaya.si.aZ;
import com.papaya.si.bA;
import com.papaya.si.bp;
import com.papaya.si.by;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebPicturesDialog extends CustomDialog implements View.OnClickListener, by.a {
    private bp iI;
    private JSONObject jz;
    /* access modifiers changed from: private */
    public ArrayList<bA> kR = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kS = new ArrayList<>();
    private GridView lC;
    /* access modifiers changed from: private */
    public int lD;
    /* access modifiers changed from: private */
    public int lE;
    /* access modifiers changed from: private */
    public a lF;

    class a extends BaseAdapter {
        private Context lH;

        public a(Context context) {
            this.lH = context;
        }

        public final int getCount() {
            return WebPicturesDialog.this.kS.size();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.widget.ImageButton} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.widget.ImageButton} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: android.widget.ImageButton} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
            /*
                r6 = this;
                com.papaya.view.WebPicturesDialog r1 = com.papaya.view.WebPicturesDialog.this
                java.util.ArrayList r1 = r1.kS
                java.lang.Object r1 = r1.get(r7)
                android.graphics.drawable.Drawable r1 = (android.graphics.drawable.Drawable) r1
                if (r1 != 0) goto L_0x003c
                com.papaya.view.WebPicturesDialog r2 = com.papaya.view.WebPicturesDialog.this
                java.util.ArrayList r2 = r2.kR
                java.lang.Object r2 = r2.get(r7)
                if (r2 == 0) goto L_0x003c
                if (r8 == 0) goto L_0x0020
                boolean r1 = r8 instanceof android.widget.ProgressBar
                if (r1 != 0) goto L_0x007f
            L_0x0020:
                android.widget.ProgressBar r1 = new android.widget.ProgressBar
                android.content.Context r2 = r6.lH
                r1.<init>(r2)
                android.widget.AbsListView$LayoutParams r2 = new android.widget.AbsListView$LayoutParams
                com.papaya.view.WebPicturesDialog r3 = com.papaya.view.WebPicturesDialog.this
                int r3 = r3.lD
                com.papaya.view.WebPicturesDialog r4 = com.papaya.view.WebPicturesDialog.this
                int r4 = r4.lE
                r2.<init>(r3, r4)
                r1.setLayoutParams(r2)
            L_0x003b:
                return r1
            L_0x003c:
                if (r8 == 0) goto L_0x0042
                boolean r2 = r8 instanceof android.widget.ImageButton
                if (r2 != 0) goto L_0x007d
            L_0x0042:
                android.widget.ImageButton r2 = new android.widget.ImageButton
                android.content.Context r3 = r6.lH
                r2.<init>(r3)
                android.widget.AbsListView$LayoutParams r3 = new android.widget.AbsListView$LayoutParams
                com.papaya.view.WebPicturesDialog r4 = com.papaya.view.WebPicturesDialog.this
                int r4 = r4.lD
                com.papaya.view.WebPicturesDialog r5 = com.papaya.view.WebPicturesDialog.this
                int r5 = r5.lE
                r3.<init>(r4, r5)
                r2.setLayoutParams(r3)
                r3 = 0
                r2.setAdjustViewBounds(r3)
                android.widget.ImageView$ScaleType r3 = android.widget.ImageView.ScaleType.CENTER_CROP
                r2.setScaleType(r3)
                com.papaya.view.WebPicturesDialog r3 = com.papaya.view.WebPicturesDialog.this
                r2.setOnClickListener(r3)
                java.lang.Integer r3 = java.lang.Integer.valueOf(r7)
                r2.setTag(r3)
            L_0x0072:
                if (r1 == 0) goto L_0x007b
                r0 = r2
                android.widget.ImageButton r0 = (android.widget.ImageButton) r0
                r6 = r0
                r6.setImageDrawable(r1)
            L_0x007b:
                r1 = r2
                goto L_0x003b
            L_0x007d:
                r2 = r8
                goto L_0x0072
            L_0x007f:
                r1 = r8
                goto L_0x003b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.papaya.view.WebPicturesDialog.a.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }
    }

    public WebPicturesDialog(Context context, bp bpVar, String str) {
        super(context);
        this.iI = bpVar;
        this.jz = C0040bk.parseJsonObject(str);
        if (this.jz != null) {
            this.lD = C0040bk.getJsonInt(this.jz, MMAdView.KEY_WIDTH, 120);
            this.lE = C0040bk.getJsonInt(this.jz, MMAdView.KEY_HEIGHT, 60);
            JSONArray jsonArray = C0040bk.getJsonArray(this.jz, "icons");
            aD webCache = C0002a.getWebCache();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    String jsonString = C0040bk.getJsonString(jsonArray, i);
                    bA bAVar = new bA();
                    bAVar.setDelegate(this);
                    aP fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString, this.iI.getPapayaURL(), bAVar);
                    if (fdFromPapayaUri != null) {
                        this.kS.add(Drawable.createFromStream(fdFromPapayaUri.openInput(), "icon"));
                        this.kR.add(null);
                    } else {
                        this.kS.add(null);
                        this.kR.add(bAVar);
                        webCache.appendRequest(bAVar);
                    }
                }
            }
        }
        this.lC = (GridView) LayoutInflater.from(context).inflate(C0065z.layoutID("picdlgview"), (ViewGroup) null);
        this.lC.setNumColumns(-1);
        this.lC.setColumnWidth(this.lD);
        this.lC.setStretchMode(2);
        this.lF = new a(context);
        this.lC.setAdapter((ListAdapter) this.lF);
        setView(this.lC);
        setTitle(C0042c.getString("group_id"));
    }

    public void connectionFailed(final by byVar, int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                int indexOf = WebPicturesDialog.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebPicturesDialog.this.kR.set(indexOf, null);
                    WebPicturesDialog.this.lF.notifyDataSetChanged();
                }
            }
        });
    }

    public void connectionFinished(final by byVar) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                int indexOf = WebPicturesDialog.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebPicturesDialog.this.kR.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byVar.getData());
                    try {
                        WebPicturesDialog.this.kS.set(indexOf, Drawable.createFromStream(byteArrayInputStream, "icon"));
                        WebPicturesDialog.this.lF.notifyDataSetChanged();
                    } finally {
                        aZ.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public void dismiss() {
        aD webCache = C0002a.getWebCache();
        Iterator<bA> it = this.kR.iterator();
        while (it.hasNext()) {
            bA next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kR.clear();
        this.kS.clear();
        super.dismiss();
    }

    public void onClick(View view) {
        String jsonString = C0040bk.getJsonString(this.jz, "action");
        if (jsonString != null) {
            JSONArray jsonArray = C0040bk.getJsonArray(this.jz, "icons");
            int intValue = ((Integer) view.getTag()).intValue();
            String jsonString2 = C0040bk.getJsonString(jsonArray, intValue);
            String contentUriFromPapayaUri = C0002a.getWebCache().contentUriFromPapayaUri(jsonString2, this.iI.getPapayaURL(), null);
            if (contentUriFromPapayaUri != null) {
                jsonString2 = contentUriFromPapayaUri;
            }
            this.iI.callJS(C0030ba.format("%s(%d, '%s')", jsonString, Integer.valueOf(intValue), jsonString2));
        }
        dismiss();
    }
}
