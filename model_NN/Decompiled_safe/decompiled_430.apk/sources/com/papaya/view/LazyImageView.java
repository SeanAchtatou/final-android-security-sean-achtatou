package com.papaya.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import com.papaya.si.C0002a;
import com.papaya.si.C0031bb;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.X;
import com.papaya.si.bA;
import com.papaya.si.bD;
import com.papaya.si.by;
import java.net.URL;

public class LazyImageView extends ImageView implements C0031bb, by.a {
    public static final ColorMatrixColorFilter GREYSCALE_FILTER = new ColorMatrixColorFilter(new float[]{0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.3f, 0.59f, 0.11f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f});
    private String da;
    private bA jT;
    private Drawable jU;
    private boolean jV = false;
    private int jW = 1;
    private boolean jX = false;

    public LazyImageView(Context context) {
        super(context);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public LazyImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public LazyImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setScaleType(ImageView.ScaleType.FIT_CENTER);
    }

    public void connectionFailed(by byVar, int i) {
        this.jT = null;
    }

    public void connectionFinished(by byVar) {
        if (byVar.getRequest() == this.jT) {
            try {
                this.jT = null;
                setBitmapWithAnimation(byVar.getBitmap());
            } catch (Exception e) {
                X.w("Failed to execute bitmap callback: %s", e);
            }
        }
    }

    public String getImageUrl() {
        return this.da;
    }

    public int getMaxAnimationCount() {
        return this.jW;
    }

    public boolean isGrayscaled() {
        return this.jX;
    }

    public boolean isRoundedCorner() {
        return this.jV;
    }

    /* access modifiers changed from: protected */
    public void onAnimationEnd() {
        super.onAnimationEnd();
        setAnimation(null);
    }

    /* access modifiers changed from: protected */
    public void setBitmapWithAnimation(Bitmap bitmap) {
        setImageDrawable(new BitmapDrawable(bitmap));
        if (this.jW != 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(667);
            startAnimation(alphaAnimation);
            if (this.jW > 0) {
                this.jW--;
            }
        }
    }

    public void setDefaultDrawable(Drawable drawable) {
        if (getDrawable() == null || getDrawable() == this.jU) {
            setImageDrawable(drawable);
        }
        this.jU = drawable;
    }

    public void setGrayScaled(boolean z) {
        this.jX = z;
        if (z) {
            setColorFilter(GREYSCALE_FILTER);
        } else {
            setColorFilter((ColorFilter) null);
        }
    }

    public void setImageUrl(String str) {
        try {
            if (this.da == null || !this.da.equals(str)) {
                this.da = str;
                setImageDrawable(this.jU);
                if (this.jT != null) {
                    this.jT.cancel();
                    this.jT = null;
                }
                if (str != null && str.length() > 0) {
                    if (bD.isContentUrl(str)) {
                        setBitmapWithAnimation(C0036bg.bitmapFromFD(C0002a.getWebCache().fdFromContentUrl(str)));
                        return;
                    }
                    URL createURL = C0040bk.createURL(str);
                    if (createURL != null) {
                        Bitmap cachedBitmap = by.getCachedBitmap(createURL);
                        if (cachedBitmap != null) {
                            setBitmapWithAnimation(cachedBitmap);
                            return;
                        }
                        this.jT = new bA(createURL, true);
                        this.jT.setRequireSid(false);
                        this.jT.setDelegate(this);
                        this.jT.start(false);
                    }
                }
            }
        } catch (Exception e) {
            X.w("Failed to setImageUrl: %s", e);
        }
    }

    public void setMaxAnimationCount(int i) {
        this.jW = i;
    }

    public void setRoundedCorner(boolean z) {
        this.jV = z;
    }
}
