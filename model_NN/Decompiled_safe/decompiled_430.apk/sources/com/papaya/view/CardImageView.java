package com.papaya.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.papaya.si.C0002a;
import com.papaya.si.C0009ag;
import com.papaya.si.C0012aj;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0060u;
import com.papaya.si.D;
import com.papaya.si.L;
import com.papaya.si.N;
import com.papaya.si.S;

public class CardImageView extends LazyImageView implements C0060u.a {
    private D cP;
    private Drawable iN;
    private int id = -1;
    private int type = -1;

    public CardImageView(Context context) {
        super(context);
    }

    public CardImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CardImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.iN != null) {
            int width = getWidth();
            int height = getHeight();
            int min = Math.min(C0036bg.rp(16), width / 3);
            this.iN.setBounds(width - min, height - min, width, height);
            this.iN.draw(canvas);
        }
    }

    public boolean onImageUpdated(int i, int i2, int i3) {
        if (this.type == i && this.id == i2) {
            if (i == 2) {
                if (i3 > 0) {
                    setImageUrl(C0030ba.format("getavatarhead?uid=%d&amp;v=%d", Integer.valueOf(i2), Integer.valueOf(i3)));
                } else {
                    setDefaultDrawable(C0042c.getBitmapDrawable("avatar_default"));
                }
            } else if (i == 4 && i3 > 0) {
                setImageUrl(C0030ba.format("getgroupicon?cid=%d&amp;v=%d", Integer.valueOf(i2), Integer.valueOf(i3)));
            }
        }
        return true;
    }

    public void refreshWithCard(D d) {
        if (d == this.cP) {
            setStateDrawable(d == null ? 1 : d.getState());
            setGrayScaled(d != null && d.isGrayScaled());
            return;
        }
        this.cP = d;
        C0060u imageVersion = C0002a.getImageVersion();
        if (d == null) {
            setDefaultDrawable(null);
            setImageDrawable(null);
            setGrayScaled(false);
            setStateDrawable(1);
            imageVersion.removeDelegate(this, this.type, this.id);
            return;
        }
        setDefaultDrawable(d.getDefaultDrawable());
        setImageUrl(d.getImageUrl());
        setStateDrawable(d.getState());
        this.type = -1;
        this.id = -1;
        if (d instanceof C0012aj) {
            C0012aj ajVar = (C0012aj) d;
            if (ajVar.getUserID() != 0) {
                setUserID(ajVar.getUserID());
            }
        } else if (d instanceof S) {
            setUserID(((S) d).cU);
        } else if (d instanceof C0009ag) {
            setUserID(((C0009ag) d).cU);
        } else if (d instanceof N) {
            setUserID(((N) d).cU);
        } else if (d instanceof L) {
            setChatGroupID(((L) d).cQ);
        }
        setGrayScaled(d.isGrayScaled());
    }

    /* access modifiers changed from: protected */
    public void setChatGroupID(int i) {
        this.type = 4;
        this.id = i;
        C0060u imageVersion = C0002a.getImageVersion();
        int version = imageVersion.getVersion(this.type, i);
        if (version > 0) {
            setImageUrl(C0030ba.format("getgroupicon?cid=%d&amp;v=%d", Integer.valueOf(i), Integer.valueOf(version)));
        } else if (version == -1) {
            imageVersion.addDelegate(this, this.type, i);
        }
    }

    /* access modifiers changed from: protected */
    public void setStateDrawable(int i) {
        this.iN = null;
        if (i == 3) {
            this.iN = C0042c.getDrawable("state_3");
        } else if (i == 2) {
            this.iN = C0042c.getDrawable("state_2");
        }
    }

    /* access modifiers changed from: protected */
    public void setUserID(int i) {
        this.type = 2;
        this.id = i;
        C0060u imageVersion = C0002a.getImageVersion();
        int version = imageVersion.getVersion(this.type, i);
        if (version > 0) {
            setImageUrl(C0030ba.format("getavatarhead?uid=%d&amp;v=%d", Integer.valueOf(i), Integer.valueOf(version)));
        } else if (version == 0) {
            setDefaultDrawable(C0042c.getBitmapDrawable("avatar_default"));
        } else if (version == -1) {
            imageVersion.addDelegate(this, this.type, i);
        }
    }
}
