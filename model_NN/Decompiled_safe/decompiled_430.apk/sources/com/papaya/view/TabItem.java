package com.papaya.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.papaya.si.C0036bg;
import com.papaya.si.C0065z;

public class TabItem extends RelativeLayout {
    private TextView fm;
    private BadgeView kG;
    private ImageView kw;

    public TabItem(Context context) {
        super(context);
        setBackgroundResource(C0065z.drawableID("tab_indicator"));
        setPadding(0, 0, 0, 0);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.bottomMargin = C0036bg.rp(3);
        addView(linearLayout, layoutParams);
        this.kw = new ImageView(context);
        this.kw.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(C0036bg.rp(40), C0036bg.rp(39));
        layoutParams2.gravity = 1;
        linearLayout.addView(this.kw, layoutParams2);
        this.fm = new TextView(context);
        this.fm.setPadding(0, 0, 0, 0);
        this.fm.setIncludeFontPadding(false);
        this.fm.setTextSize(12.0f);
        this.fm.setTypeface(Typeface.DEFAULT_BOLD);
        this.fm.setTextColor(Color.rgb(185, 185, 185));
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 1;
        linearLayout.addView(this.fm, layoutParams3);
        this.kG = new BadgeView(context);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(C0036bg.rp(32), C0036bg.rp(30));
        layoutParams4.addRule(10);
        layoutParams4.addRule(11);
        addView(this.kG, layoutParams4);
    }

    public BadgeView getBadgeView() {
        return this.kG;
    }

    public ImageView getImageView() {
        return this.kw;
    }

    public TextView getTextView() {
        return this.fm;
    }

    public void setSelected(boolean z) {
        super.setSelected(z);
        this.kw.setSelected(z);
    }
}
