package com.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class ChatEditText extends EditText {
    public ChatEditText(Context context) {
        super(context);
    }

    public ChatEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ChatEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
