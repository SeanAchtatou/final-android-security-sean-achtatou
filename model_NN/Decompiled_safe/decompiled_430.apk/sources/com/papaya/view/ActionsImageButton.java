package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import com.papaya.view.CustomDialog;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ActionsImageButton extends ImageButton implements DialogInterface.OnClickListener, View.OnClickListener {
    private ArrayList<Action> ip = new ArrayList<>(4);
    private WeakReference<Delegate> iq;
    private Drawable ir = null;
    public Object payload;

    public interface Delegate {
        void onActionSelected(ActionsImageButton actionsImageButton, Action action);
    }

    public ActionsImageButton(Context context) {
        super(context);
        setOnClickListener(this);
        refreshSelf();
    }

    public ActionsImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setOnClickListener(this);
        refreshSelf();
    }

    public ActionsImageButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setOnClickListener(this);
        refreshSelf();
    }

    private void fireAction(Action action) {
        Delegate actionDelegate = getActionDelegate();
        if (actionDelegate != null) {
            actionDelegate.onActionSelected(this, action);
        }
    }

    private void refreshSelf() {
        if (this.ip.isEmpty()) {
            setVisibility(4);
            return;
        }
        if (this.ip.size() == 1) {
            setImageDrawable(this.ip.get(0).icon);
        } else {
            setImageDrawable(this.ir);
        }
        setVisibility(0);
    }

    public void addAction(Action action) {
        this.ip.add(action);
        refreshSelf();
    }

    public void clearActions() {
        this.ip.clear();
        refreshSelf();
    }

    public Delegate getActionDelegate() {
        if (this.iq == null) {
            return null;
        }
        return this.iq.get();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        fireAction(this.ip.get(i));
    }

    public void onClick(View view) {
        if (this.ip.size() == 1) {
            fireAction(this.ip.get(0));
        } else if (this.ip.size() > 1) {
            CharSequence[] charSequenceArr = new CharSequence[this.ip.size()];
            for (int i = 0; i < this.ip.size(); i++) {
                charSequenceArr[i] = this.ip.get(i).label;
            }
            new CustomDialog.Builder(getContext()).setItems(charSequenceArr, this).create().show();
        }
    }

    public void setActionDelegate(Delegate delegate) {
        this.iq = new WeakReference<>(delegate);
    }

    public void setCombinedDrawable(Drawable drawable) {
        this.ir = drawable;
        refreshSelf();
    }
}
