package com.papaya.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0065z;

public class BadgeView extends TextView {
    public BadgeView(Context context) {
        super(context);
        init();
    }

    public BadgeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public BadgeView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        setPadding(C0036bg.rp(2), 0, C0036bg.rp(4), C0036bg.rp(5));
        setBackgroundResource(C0065z.drawableID("badge_number"));
        setGravity(17);
        setTextColor(-1);
        setEllipsize(TextUtils.TruncateAt.MIDDLE);
        setTextSize(12.0f);
        setTypeface(Typeface.DEFAULT_BOLD);
        setVisibility(4);
    }

    public void setBadgeValue(String str) {
        setText(str);
        setVisibility(C0030ba.isEmpty(str) ? 4 : 0);
    }
}
