package com.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import com.papaya.si.C0002a;
import com.papaya.si.C0042c;

public class UserImageView extends CardImageView {
    public UserImageView(Context context) {
        super(context);
    }

    public UserImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UserImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setUserID(int i) {
        setDefaultDrawable(C0042c.getBitmapDrawable("avatar_unknown"));
        setImageDrawable(null);
        setGrayScaled(false);
        setStateDrawable(1);
        C0002a.getImageVersion().removeDelegate(this, 2, i);
        super.setUserID(i);
    }
}
