package com.papaya.view;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0002a;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.aD;
import com.papaya.si.aP;
import com.papaya.si.aZ;
import com.papaya.si.bA;
import com.papaya.si.bp;
import com.papaya.si.by;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebListDialogWrapper implements AdapterView.OnItemClickListener, by.a {
    private bp iI;
    private JSONObject jz;
    private CustomDialog kQ;
    /* access modifiers changed from: private */
    public ArrayList<bA> kR = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kS = new ArrayList<>();
    /* access modifiers changed from: private */
    public JSONArray kT;
    private ListView kU;
    /* access modifiers changed from: private */
    public a kV;

    class a extends BaseAdapter {
        private LayoutInflater kY;

        /* synthetic */ a(WebListDialogWrapper webListDialogWrapper, LayoutInflater layoutInflater) {
            this(layoutInflater, (byte) 0);
        }

        private a(LayoutInflater layoutInflater, byte b) {
            this.kY = layoutInflater;
        }

        public final int getCount() {
            if (WebListDialogWrapper.this.kT == null) {
                return 0;
            }
            return WebListDialogWrapper.this.kT.length();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            b bVar;
            View view2;
            if (view == null) {
                View inflate = this.kY.inflate(C0065z.layoutID("list_item_3_part_inverse"), (ViewGroup) null);
                b bVar2 = new b();
                bVar2.kZ = (ImageView) inflate.findViewById(C0065z.id("list_item_3_header"));
                bVar2.la = (TextView) inflate.findViewById(C0065z.id("list_item_3_content"));
                bVar2.lb = (ImageView) inflate.findViewById(C0065z.id("list_item_3_accessory"));
                inflate.setTag(bVar2);
                b bVar3 = bVar2;
                view2 = inflate;
                bVar = bVar3;
            } else {
                bVar = (b) view.getTag();
                view2 = view;
            }
            JSONObject jsonObject = C0040bk.getJsonObject(WebListDialogWrapper.this.kT, i);
            bVar.la.setText(C0040bk.getJsonString(jsonObject, "text"));
            Drawable drawable = (Drawable) WebListDialogWrapper.this.kS.get(i);
            if (drawable != null) {
                bVar.kZ.setImageDrawable(drawable);
                bVar.kZ.setVisibility(0);
                bVar.kZ.setBackgroundColor(0);
            } else {
                bVar.kZ.setVisibility(4);
            }
            if (C0030ba.intValue(C0040bk.getJsonString(jsonObject, "selected"), -1) == 1) {
                bVar.lb.setVisibility(0);
                bVar.lb.setImageDrawable(this.kY.getContext().getResources().getDrawable(C0065z.drawableID("ic_check_mark_light")));
                bVar.lb.setBackgroundColor(0);
            } else {
                bVar.lb.setVisibility(4);
            }
            return view2;
        }
    }

    static class b {
        ImageView kZ;
        TextView la;
        ImageView lb;

        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    public WebListDialogWrapper(CustomDialog customDialog, bp bpVar, JSONObject jSONObject) {
        this.kQ = customDialog;
        this.iI = bpVar;
        this.jz = jSONObject;
        configure();
    }

    private void clearResources() {
        aD webCache = C0002a.getWebCache();
        Iterator<bA> it = this.kR.iterator();
        while (it.hasNext()) {
            bA next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kR.clear();
        this.kS.clear();
    }

    private void configure() {
        String jsonString = C0040bk.getJsonString(this.jz, "title");
        if (jsonString == null) {
            jsonString = this.kQ.getContext().getString(C0065z.stringID("web_selector_title"));
        }
        this.kQ.setTitle(jsonString);
        this.kQ.setIcon(C0065z.drawableID("alert_icon_check"));
        LayoutInflater layoutInflater = (LayoutInflater) this.kQ.getContext().getSystemService("layout_inflater");
        this.kU = (ListView) layoutInflater.inflate(C0065z.layoutID("list_dialog"), (ViewGroup) null);
        this.kT = C0040bk.getJsonArray(this.jz, "options");
        URL papayaURL = this.iI.getPapayaURL();
        if (this.kT != null) {
            aD webCache = C0002a.getWebCache();
            for (int i = 0; i < this.kT.length(); i++) {
                this.kS.add(null);
                this.kR.add(null);
                JSONObject jsonObject = C0040bk.getJsonObject(this.kT, i);
                if (!"separator".equals(C0040bk.getJsonString(jsonObject, "type"))) {
                    String jsonString2 = C0040bk.getJsonString(jsonObject, "icon");
                    if (!C0030ba.isEmpty(jsonString2)) {
                        bA bAVar = new bA();
                        bAVar.setDelegate(this);
                        aP fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString2, papayaURL, bAVar);
                        if (fdFromPapayaUri != null) {
                            this.kS.set(i, C0036bg.drawableFromFD(fdFromPapayaUri));
                        } else if (bAVar.getUrl() != null) {
                            this.kR.set(i, bAVar);
                        }
                    }
                }
            }
            webCache.insertRequests(this.kR);
        }
        this.kV = new a(this, layoutInflater);
        this.kU.setAdapter((ListAdapter) this.kV);
        this.kU.setBackgroundResource(17170447);
        this.kU.setOnItemClickListener(this);
        this.kQ.setView(this.kU);
    }

    public void connectionFailed(final by byVar, int i) {
        C0036bg.post(new Runnable() {
            public final void run() {
                int indexOf = WebListDialogWrapper.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebListDialogWrapper.this.kR.set(indexOf, null);
                }
            }
        });
    }

    public void connectionFinished(final by byVar) {
        C0036bg.post(new Runnable() {
            public final void run() {
                int indexOf = WebListDialogWrapper.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebListDialogWrapper.this.kR.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byVar.getData());
                    try {
                        WebListDialogWrapper.this.kS.set(indexOf, Drawable.createFromStream(byteArrayInputStream, "icon"));
                        WebListDialogWrapper.this.kV.notifyDataSetChanged();
                    } finally {
                        aZ.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public CustomDialog getDialog() {
        return this.kQ;
    }

    public bp getWebView() {
        return this.iI;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        JSONObject jsonObject = C0040bk.getJsonObject(this.kT, i);
        if (this.iI != null) {
            Object jsonValue = C0040bk.getJsonValue(jsonObject, "value");
            String jsonString = C0040bk.getJsonString(jsonObject, "text");
            String jsonString2 = C0040bk.getJsonString(jsonObject, "icon");
            String jsonString3 = C0040bk.getJsonString(this.jz, "valueid");
            String jsonString4 = C0040bk.getJsonString(this.jz, "textid");
            String jsonString5 = C0040bk.getJsonString(this.jz, "action");
            if (!(jsonString3 == null || jsonValue == null)) {
                if (jsonValue instanceof String) {
                    this.iI.callJS(C0030ba.format("%s='%s'", jsonString3, C0040bk.escapeJS((String) jsonValue)));
                } else {
                    this.iI.callJS(C0030ba.format("%s=%s", jsonString3, jsonValue));
                }
            }
            if (jsonString4 != null) {
                if (jsonString != null) {
                    this.iI.callJS(C0030ba.format("%s='%s'", jsonString4, C0040bk.escapeJS(jsonString)));
                } else {
                    this.iI.callJS(C0030ba.format("%s='%s'", jsonString4, C0040bk.escapeJS(jsonString2)));
                }
            }
            if (!(jsonString5 == null || jsonValue == null)) {
                if (jsonValue instanceof String) {
                    this.iI.callJS(C0030ba.format("%s('%s')", jsonString5, C0040bk.escapeJS((String) jsonValue)));
                } else {
                    this.iI.callJS(C0030ba.format("%s(%s)", jsonString5, jsonValue));
                }
            }
        }
        this.kQ.dismiss();
        clearResources();
    }

    public void setWebView(bp bpVar) {
        this.iI = bpVar;
    }
}
