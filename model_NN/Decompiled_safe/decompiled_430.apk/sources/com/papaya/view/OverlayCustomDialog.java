package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.si.X;

public class OverlayCustomDialog extends FrameLayout implements View.OnClickListener {
    private float alpha = 0.95f;
    private ListView cA;
    private ImageView dG;
    private int gravity = 17;
    private LinearLayout iT;
    private LinearLayout iU;
    private FrameLayout iV;
    private View iW;
    private LinearLayout iX;
    Button iY;
    Button iZ;
    Button ja;
    private boolean jm = true;
    private View kd;
    private WindowManager ke;
    DynamicTextView kf;
    private DynamicTextView kg;
    private OnClickListener kh;
    private OnClickListener ki;
    private OnClickListener kj;

    public static class Builder {
        private Drawable G;
        private CharSequence H = C0061v.bk;
        private Context cN;
        private CharSequence cZ;
        private int eG;
        private View iW;
        private CharSequence jg;
        private CharSequence jh;
        private CharSequence ji;
        private boolean jm = true;
        private CharSequence[] jp;
        private ListAdapter jr;
        private AdapterView.OnItemSelectedListener js;
        private OnClickListener km;
        private OnClickListener kn;
        private OnClickListener ko;
        private View.OnKeyListener kp;
        private OnClickListener kq;

        public Builder(Context context) {
            this.cN = context;
        }

        public OverlayCustomDialog create() {
            OverlayCustomDialog overlayCustomDialog = new OverlayCustomDialog(this.cN);
            overlayCustomDialog.kf.setText(this.H);
            if (this.eG != 0) {
                overlayCustomDialog.setIcon(this.eG);
            }
            if (this.G != null) {
                overlayCustomDialog.setIcon(this.G);
            }
            overlayCustomDialog.setView(this.iW);
            overlayCustomDialog.setMessage(this.cZ);
            if (this.jg != "") {
                overlayCustomDialog.setButton(-1, this.jg, this.km);
            } else {
                overlayCustomDialog.iY.setVisibility(8);
            }
            if (this.jh != "") {
                overlayCustomDialog.setButton(-2, this.jh, this.kn);
            } else {
                overlayCustomDialog.ja.setVisibility(8);
            }
            if (this.ji != "") {
                overlayCustomDialog.setButton(-3, this.ji, this.ko);
            } else {
                overlayCustomDialog.iZ.setVisibility(8);
            }
            overlayCustomDialog.setCancelable(this.jm);
            if (this.kp != null) {
                overlayCustomDialog.setOnKeyListener(this.kp);
            }
            if (this.jr == null && this.jp != null) {
                this.jr = new ArrayAdapter(this.cN, 17367057, 16908308, this.jp);
            }
            if (this.jr != null) {
                overlayCustomDialog.initListView(this.jr, this.kq, this.js);
            }
            return overlayCustomDialog;
        }

        public Builder setAdapter(ListAdapter listAdapter, OnClickListener onClickListener) {
            this.jr = listAdapter;
            this.kq = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.jm = z;
            return this;
        }

        public Builder setIcon(int i) {
            this.eG = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.G = drawable;
            return this;
        }

        public Builder setItems(int i, OnClickListener onClickListener) {
            this.jp = this.cN.getResources().getTextArray(i);
            this.kq = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, OnClickListener onClickListener) {
            this.jp = charSequenceArr;
            this.kq = onClickListener;
            return this;
        }

        public Builder setMessage(int i) {
            this.cZ = this.cN.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.cZ = charSequence;
            return this;
        }

        public Builder setNegativeButton(int i, OnClickListener onClickListener) {
            this.jh = this.cN.getText(i);
            this.kn = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.jh = charSequence;
            this.kn = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, OnClickListener onClickListener) {
            this.ji = this.cN.getText(i);
            this.ko = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.ji = charSequence;
            this.ko = onClickListener;
            return this;
        }

        public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
            this.js = onItemSelectedListener;
            return this;
        }

        public Builder setOnKeyListener(View.OnKeyListener onKeyListener) {
            this.kp = onKeyListener;
            return this;
        }

        public Builder setPositiveButton(int i, OnClickListener onClickListener) {
            this.jg = this.cN.getText(i);
            this.km = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, OnClickListener onClickListener) {
            this.jg = charSequence;
            this.km = onClickListener;
            return this;
        }

        public Builder setTitle(int i) {
            this.H = this.cN.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.H = charSequence;
            return this;
        }

        public Builder setView(View view) {
            this.iW = view;
            return this;
        }

        public OverlayCustomDialog show() {
            OverlayCustomDialog create = create();
            create.show();
            return create;
        }
    }

    public interface OnClickListener {
        void onClick(OverlayCustomDialog overlayCustomDialog, int i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.papaya.view.OverlayCustomDialog, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public OverlayCustomDialog(Context context) {
        super(context);
        this.ke = C0036bg.getWindowManager(context);
        this.kd = getLayoutInflater().inflate(C0065z.layoutID("custom_dialog"), (ViewGroup) this, true);
        this.iT = (LinearLayout) f("dialog_title_content");
        this.dG = (ImageView) f("dialog_icon");
        this.kf = (DynamicTextView) f("dialog_title");
        this.iU = (LinearLayout) f("dialog_content");
        this.kg = (DynamicTextView) f("dialog_message");
        this.kg.setMovementMethod(new ScrollingMovementMethod());
        this.iV = (FrameLayout) f("dialog_custom_content");
        this.iX = (LinearLayout) f("dialog_button_content");
        this.iY = (Button) f("dialog_button_positive");
        this.iZ = (Button) f("dialog_button_neutral");
        this.ja = (Button) f("dialog_button_negative");
        this.iY.setOnClickListener(this);
        this.iZ.setOnClickListener(this);
        this.ja.setOnClickListener(this);
    }

    private void dismiss() {
        hide();
    }

    private <T> T f(String str) {
        T findViewById = findViewById(C0065z.id(str));
        if (findViewById == null) {
            X.w("can't find view with id %s", str);
        }
        return findViewById;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (super.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        return keyEvent.dispatch(this);
    }

    public Button getButton(int i) {
        switch (i) {
            case -3:
                return this.iZ;
            case -2:
                return this.ja;
            case -1:
                return this.iY;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public int getDefaultVisibility() {
        return C0029b.getActiveActivity() != null ? 0 : 8;
    }

    /* access modifiers changed from: protected */
    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getContext());
    }

    public ListView getListView() {
        return this.cA;
    }

    public DynamicTextView getMessageView() {
        return this.kg;
    }

    public DynamicTextView getTitleView() {
        return this.kf;
    }

    public void hide() {
        try {
            setVisibility(8);
            if (this.ke != null && getParent() != null) {
                this.ke.removeView(this);
            }
        } catch (Exception e) {
        }
    }

    public void hideLoading() {
        this.iY.setEnabled(true);
        this.iZ.setEnabled(true);
        this.ja.setEnabled(true);
    }

    /* access modifiers changed from: package-private */
    public void initListView(ListAdapter listAdapter, final OnClickListener onClickListener, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.cA = (ListView) getLayoutInflater().inflate(C0065z.layoutID("list_dialog"), (ViewGroup) null);
        this.cA.setAdapter(listAdapter);
        if (onItemSelectedListener != null) {
            this.cA.setOnItemSelectedListener(onItemSelectedListener);
        } else if (onClickListener != null) {
            this.cA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    OverlayCustomDialog.this.hide();
                    onClickListener.onClick(OverlayCustomDialog.this, i);
                }
            });
        }
        this.iU.removeAllViews();
        this.iU.addView(this.cA);
    }

    public boolean isCancelable() {
        return this.jm;
    }

    public boolean isLoading() {
        return !this.iY.isEnabled();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (this.kd != null && this.ke != null && getParent() != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.gravity = this.gravity;
                layoutParams.format = -3;
                layoutParams.flags = 2;
                layoutParams.dimAmount = 0.4f;
                layoutParams.alpha = this.alpha;
                this.ke.updateViewLayout(this, layoutParams);
            }
        } catch (Exception e) {
        }
    }

    public void onClick(View view) {
        hide();
        if (view == this.iY) {
            if (this.kh != null) {
                this.kh.onClick(this, -1);
            }
        } else if (view == this.ja) {
            if (this.kj != null) {
                this.kj.onClick(this, -2);
            }
        } else if (view == this.iZ && this.ki != null) {
            this.ki.onClick(this, -3);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || isLoading() || !this.jm) {
            return super.onKeyDown(i, keyEvent);
        }
        if (this.kj != null) {
            this.kj.onClick(this, -2);
        }
        hide();
        return true;
    }

    public void setButton(int i, CharSequence charSequence, OnClickListener onClickListener) {
        Button button = getButton(i);
        if (button != null) {
            button.setText(charSequence);
            switch (i) {
                case -3:
                    this.ki = onClickListener;
                    return;
                case -2:
                    this.kj = onClickListener;
                    return;
                case -1:
                    this.kh = onClickListener;
                    return;
                default:
                    return;
            }
        }
    }

    public void setCancelable(boolean z) {
        this.jm = z;
    }

    public void setIcon(int i) {
        this.dG.setImageResource(i);
    }

    public void setIcon(Drawable drawable) {
        this.dG.setImageDrawable(drawable);
    }

    public void setMessage(int i) {
        getMessageView().setText(i);
    }

    public void setMessage(CharSequence charSequence) {
        getMessageView().setText(charSequence);
    }

    public void setNegativeButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-2, charSequence, onClickListener);
    }

    public void setNeutralButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-3, charSequence, onClickListener);
    }

    public void setPositiveButton(CharSequence charSequence, OnClickListener onClickListener) {
        setButton(-1, charSequence, onClickListener);
    }

    public void setTitle(int i) {
        this.kf.setText(i);
    }

    public void setTitle(CharSequence charSequence) {
        this.kf.setText(charSequence);
    }

    public void setTitleBgRes(int i) {
        this.iT.setBackgroundResource(i);
    }

    public void setTitleColor(int i) {
        this.kf.setTextColor(i);
    }

    public void setView(View view) {
        this.iW = view;
        this.iV.removeAllViews();
        if (this.iW != null) {
            this.iV.addView(this.iW);
        }
    }

    public void show() {
        show(null);
    }

    public void show(Context context) {
        updateVisibility();
        setVisibility(getDefaultVisibility());
        try {
            if (getParent() != null) {
                this.ke.removeView(this);
            }
        } catch (Exception e) {
            X.e(e, "Failed to removeView in show", new Object[0]);
        }
        if (context != null) {
            this.ke = C0036bg.getWindowManager(context);
        }
        try {
            if (this.ke != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.alpha = this.alpha;
                layoutParams.gravity = this.gravity;
                layoutParams.format = -3;
                layoutParams.type = 2005;
                layoutParams.windowAnimations = 16973828;
                this.ke.addView(this, layoutParams);
                return;
            }
            X.w("wm is null", new Object[0]);
        } catch (Exception e2) {
            X.w(e2, "Failed to showInContext", new Object[0]);
        }
    }

    public void showLoading() {
        this.iY.setEnabled(false);
        this.iZ.setEnabled(false);
        this.ja.setEnabled(false);
    }

    /* access modifiers changed from: package-private */
    public void updateVisibility() {
        if (this.iW != null) {
            this.iV.setVisibility(0);
            this.iU.setVisibility(8);
        } else {
            this.iV.setVisibility(8);
            if (!C0030ba.isEmpty(this.kg.getText()) || this.cA != null) {
                this.iU.setVisibility(0);
            } else {
                this.iU.setVisibility(8);
            }
        }
        if (!C0030ba.isEmpty(this.iY.getText()) || !C0030ba.isEmpty(this.iZ.getText()) || !C0030ba.isEmpty(this.ja.getText())) {
            this.iX.setVisibility(0);
            this.iY.setVisibility(C0030ba.isEmpty(this.iY.getText()) ? 8 : 0);
            this.ja.setVisibility(C0030ba.isEmpty(this.ja.getText()) ? 8 : 0);
            this.iZ.setVisibility(C0030ba.isEmpty(this.iZ.getText()) ? 8 : 0);
            return;
        }
        this.iX.setVisibility(8);
    }
}
