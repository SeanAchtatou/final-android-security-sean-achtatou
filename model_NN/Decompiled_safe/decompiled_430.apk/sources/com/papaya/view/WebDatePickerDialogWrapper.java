package com.papaya.view;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import com.papaya.si.C0030ba;
import com.papaya.si.C0040bk;
import com.papaya.si.bp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONObject;

public class WebDatePickerDialogWrapper implements DatePickerDialog.OnDateSetListener {
    private bp iI;
    private JSONObject jz;
    private DatePickerDialog kN;
    private DateFormat kO = new SimpleDateFormat("MMM d, yyyy");

    public WebDatePickerDialogWrapper(bp bpVar, JSONObject jSONObject) {
        this.iI = bpVar;
        this.jz = jSONObject;
        setupViews();
    }

    private void setupViews() {
        Calendar instance = Calendar.getInstance();
        int intValue = C0030ba.intValue(C0040bk.getJsonString(this.jz, "initYear"), instance.get(1));
        int intValue2 = C0030ba.intValue(C0040bk.getJsonString(this.jz, "initMonth"), instance.get(2));
        int intValue3 = C0030ba.intValue(C0040bk.getJsonString(this.jz, "initDay"), instance.get(5));
        String jsonString = C0040bk.getJsonString(this.jz, "title");
        this.kN = new DatePickerDialog(this.iI.getOwnerActivity(), this, intValue, intValue2, intValue3);
        this.kN.setTitle(jsonString);
    }

    public DatePickerDialog getDialog() {
        return this.kN;
    }

    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        int i4 = i2 + 1;
        String jsonString = C0040bk.getJsonString(this.jz, "action");
        if (jsonString != null) {
            this.iI.callJS(C0030ba.format("%s(%d, %d, %d)", jsonString, Integer.valueOf(i), Integer.valueOf(i4), Integer.valueOf(i3)));
        }
        String jsonString2 = C0040bk.getJsonString(this.jz, "year");
        if (jsonString2 != null) {
            this.iI.callJS(jsonString2 + "=" + i);
        }
        String jsonString3 = C0040bk.getJsonString(this.jz, "month");
        if (jsonString3 != null) {
            this.iI.callJS(jsonString3 + "=" + i4);
        }
        String jsonString4 = C0040bk.getJsonString(this.jz, "day");
        if (jsonString4 != null) {
            this.iI.callJS(jsonString4 + "=" + i3);
        }
        String jsonString5 = C0040bk.getJsonString(this.jz, "datetext");
        if (jsonString5 != null) {
            this.iI.callJS(C0030ba.format("%s=''", jsonString5));
            this.iI.callJS(C0030ba.format("%s='%s'", jsonString5, this.kO.format(new Date(i - 1900, i4 - 1, i3))));
        }
    }
}
