package com.papaya.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.papaya.si.C0002a;
import com.papaya.si.C0030ba;
import com.papaya.si.C0031bb;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.aD;
import com.papaya.si.aP;
import com.papaya.si.aZ;
import com.papaya.si.bA;
import com.papaya.si.bp;
import com.papaya.si.by;
import com.papaya.view.PPYAbsoluteLayout;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AvatarBar extends RelativeLayout implements View.OnClickListener, C0031bb, by.a {
    private ArrayList<bA> iA = new ArrayList<>();
    private bA iB;
    private int iC;
    private HorizontalScrollView iD;
    private PPYAbsoluteLayout iE;
    private ImageView iF;
    private TextView iG;
    private ProgressBar iH;
    private bp iI;
    private int is;
    private int it;
    private URL iu;
    private String iv;
    private JSONObject iw;
    private ArrayList<AvatarItem> ix = new ArrayList<>();
    private ArrayList<ImageButton> iy = new ArrayList<>();
    private ArrayList<bA> iz = new ArrayList<>();

    public static final class AvatarItem {
        String iJ;
        String iK;
        int iL;
        int iM;
        public int itemId;
        int x;
        int y;
    }

    public AvatarBar(Context context) {
        super(context);
        setBackgroundColor(C0036bg.color(8881537));
        setupViews(context);
    }

    private void clearImageRequests() {
        aD webCache = C0002a.getWebCache();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.iA.size()) {
                bA bAVar = this.iA.get(i2);
                if (bAVar != null) {
                    bAVar.setDelegate(null);
                    webCache.removeRequest(bAVar);
                }
                i = i2 + 1;
            } else {
                this.iA.clear();
                this.iC = -1;
                return;
            }
        }
    }

    public void clearResources() {
        aD webCache = C0002a.getWebCache();
        for (int i = 0; i < this.iy.size(); i++) {
            C0036bg.removeFromSuperView(this.iy.get(i));
        }
        this.iy.clear();
        for (int i2 = 0; i2 < this.iz.size(); i2++) {
            bA bAVar = this.iz.get(i2);
            if (bAVar != null) {
                bAVar.setDelegate(null);
                webCache.removeRequest(bAVar);
            }
        }
        this.iz.clear();
        clearImageRequests();
        this.ix.clear();
        if (this.iB != null) {
            this.iB.setDelegate(null);
            webCache.removeRequest(this.iB);
            this.iB = null;
        }
        this.iw = null;
        this.iD.scrollTo(0, 0);
    }

    public void connectionFailed(by byVar, int i) {
        bA request = byVar.getRequest();
        X.w("failed to finish %s", request);
        if (request == this.iB) {
            this.iB = null;
            this.iH.setVisibility(8);
            this.iG.setText(C0065z.stringID("avatarbar_items_load_fail"));
            this.iG.setVisibility(0);
        } else if (this.iz.contains(request)) {
            this.iz.set(this.iz.indexOf(request), null);
        } else if (this.iA.contains(request)) {
            this.iA.remove(request);
        }
    }

    /* JADX INFO: finally extract failed */
    public void connectionFinished(by byVar) {
        bA request = byVar.getRequest();
        if (request == this.iB) {
            this.iH.setVisibility(8);
            this.iw = C0040bk.parseJsonObject(aZ.decodeData(byVar.getData(), "UTF-8"));
            if (this.iw.length() > 0) {
                JSONArray jsonArray = C0040bk.getJsonArray(this.iw, "items");
                this.is = C0040bk.getJsonInt(this.iw, "flag");
                if (jsonArray != null) {
                    int length = jsonArray.length();
                    for (int i = 0; i < length; i += 7) {
                        AvatarItem avatarItem = new AvatarItem();
                        avatarItem.itemId = C0040bk.getJsonInt(jsonArray, i);
                        avatarItem.iJ = C0040bk.getJsonString(jsonArray, i + 1);
                        avatarItem.iK = C0040bk.getJsonString(jsonArray, i + 2);
                        avatarItem.iL = C0040bk.getJsonInt(jsonArray, i + 3);
                        avatarItem.x = C0040bk.getJsonInt(jsonArray, i + 4);
                        avatarItem.y = C0040bk.getJsonInt(jsonArray, i + 5);
                        avatarItem.iM = C0040bk.getJsonInt(jsonArray, i + 6);
                        this.ix.add(avatarItem);
                    }
                }
            }
            if (this.ix.isEmpty()) {
                this.iG.setText(C0065z.stringID("avatarbar_empty"));
                this.iG.setVisibility(0);
                return;
            }
            aD webCache = C0002a.getWebCache();
            Resources resources = getContext().getResources();
            for (int i2 = 0; i2 < this.ix.size(); i2++) {
                ImageButton imageButton = new ImageButton(getContext());
                imageButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageButton.setBackgroundDrawable(null);
                imageButton.setPadding(0, 0, 0, 0);
                bA bAVar = new bA();
                bAVar.setDelegate(this);
                aP fdFromPapayaUri = webCache.fdFromPapayaUri(this.ix.get(i2).iJ, this.iu, bAVar);
                if (fdFromPapayaUri != null) {
                    imageButton.setImageDrawable(C0036bg.drawableFromFD(fdFromPapayaUri));
                    imageButton.setEnabled(true);
                } else {
                    imageButton.setImageDrawable(resources.getDrawable(C0065z.drawableID("avatar_item_default")));
                    imageButton.setEnabled(false);
                }
                if (bAVar.getUrl() != null) {
                    bAVar.setSaveFile(webCache.cachedFile(bAVar.getUrl().toString(), false));
                    this.iz.add(bAVar);
                } else {
                    this.iz.add(null);
                }
                imageButton.setLayoutParams(C0036bg.rawPPYAbsoluteLayoutParams(getContext(), 54, 54, (i2 * 58) + 3, 3));
                imageButton.setOnClickListener(this);
                this.iE.addView(imageButton);
                this.iy.add(imageButton);
            }
            webCache.insertRequests(this.iz);
        } else if (this.iz.contains(request)) {
            int indexOf = this.iz.indexOf(request);
            this.iz.set(indexOf, null);
            ImageButton imageButton2 = this.iy.get(indexOf);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byVar.getData());
            try {
                imageButton2.setImageDrawable(Drawable.createFromStream(byteArrayInputStream, "icon"));
                aZ.close(byteArrayInputStream);
                imageButton2.setEnabled(true);
            } catch (Throwable th) {
                aZ.close(byteArrayInputStream);
                throw th;
            }
        } else {
            this.iA.remove(request);
            if (this.iC >= 0 && this.iA.isEmpty()) {
                aD webCache2 = C0002a.getWebCache();
                AvatarItem avatarItem2 = this.ix.get(this.iC);
                ArrayList arrayList = new ArrayList();
                for (String contentUriFromPapayaUri : avatarItem2.iK.split("~")) {
                    String contentUriFromPapayaUri2 = webCache2.contentUriFromPapayaUri(contentUriFromPapayaUri, this.iu, null);
                    if (contentUriFromPapayaUri2 != null) {
                        arrayList.add(contentUriFromPapayaUri2);
                    } else {
                        X.w("should not be null here", new Object[0]);
                    }
                }
                String contentUriFromPapayaUri3 = webCache2.contentUriFromPapayaUri(avatarItem2.iJ, this.iu, null);
                if (contentUriFromPapayaUri3 == null) {
                    contentUriFromPapayaUri3 = webCache2.relativeUriFromPapayaUri(avatarItem2.iJ);
                }
                String concatStrings = C0030ba.concatStrings("~", (String[]) arrayList.toArray(new String[arrayList.size()]));
                if (this.iI != null) {
                    bp bpVar = this.iI;
                    Object[] objArr = new Object[9];
                    objArr[0] = Integer.valueOf(this.iC == this.it ? 1 : 0);
                    objArr[1] = Integer.valueOf(this.is);
                    objArr[2] = Integer.valueOf(avatarItem2.itemId);
                    objArr[3] = concatStrings;
                    objArr[4] = contentUriFromPapayaUri3;
                    objArr[5] = Integer.valueOf(avatarItem2.iL);
                    objArr[6] = Integer.valueOf(avatarItem2.x);
                    objArr[7] = Integer.valueOf(avatarItem2.y);
                    objArr[8] = Integer.valueOf(avatarItem2.iM);
                    bpVar.callJS(C0030ba.format("avatarbartapped(%d, %d, %d, '%s', '%s', %d, %d, %d, %d)", objArr));
                }
            }
        }
    }

    public int getSelectedIndex() {
        return this.it;
    }

    public bp getWebView() {
        return this.iI;
    }

    public void onClick(View view) {
        int indexOf = this.iy.indexOf(view);
        if (this.iz.get(indexOf) != null) {
            X.w("request is not null %d", Integer.valueOf(indexOf));
            return;
        }
        PPYAbsoluteLayout.LayoutParams layoutParams = (PPYAbsoluteLayout.LayoutParams) this.iF.getLayoutParams();
        PPYAbsoluteLayout.LayoutParams layoutParams2 = (PPYAbsoluteLayout.LayoutParams) ((ImageButton) view).getLayoutParams();
        if (Math.abs(layoutParams.centerX() - layoutParams2.centerX()) >= 1.0f) {
            layoutParams.setCenter(layoutParams2.getCenter());
            this.iF.setVisibility(0);
            this.iF.setLayoutParams(layoutParams);
        } else if (this.iF.getVisibility() == 0) {
            this.iF.setVisibility(4);
        } else {
            this.iF.setVisibility(0);
        }
        this.it = this.iF.getVisibility() == 0 ? indexOf : -1;
        clearImageRequests();
        aD webCache = C0002a.getWebCache();
        ArrayList arrayList = new ArrayList();
        AvatarItem avatarItem = this.ix.get(indexOf);
        if (avatarItem.iK != null) {
            for (String contentUriFromPapayaUri : avatarItem.iK.split("~")) {
                bA bAVar = new bA();
                bAVar.setDelegate(this);
                String contentUriFromPapayaUri2 = webCache.contentUriFromPapayaUri(contentUriFromPapayaUri, this.iu, bAVar);
                if (contentUriFromPapayaUri2 != null) {
                    arrayList.add(contentUriFromPapayaUri2);
                } else if (bAVar.getUrl() != null) {
                    this.iA.add(bAVar);
                }
            }
            String contentUriFromPapayaUri3 = webCache.contentUriFromPapayaUri(avatarItem.iJ, this.iu, null);
            if (contentUriFromPapayaUri3 == null) {
                contentUriFromPapayaUri3 = webCache.relativeUriFromPapayaUri(avatarItem.iJ);
            }
            if (this.iA.isEmpty()) {
                String concatStrings = C0030ba.concatStrings("~", (String[]) arrayList.toArray(new String[arrayList.size()]));
                if (this.iI != null) {
                    bp bpVar = this.iI;
                    Object[] objArr = new Object[9];
                    objArr[0] = Integer.valueOf(indexOf == this.it ? 1 : 0);
                    objArr[1] = Integer.valueOf(this.is);
                    objArr[2] = Integer.valueOf(avatarItem.itemId);
                    objArr[3] = concatStrings;
                    objArr[4] = contentUriFromPapayaUri3;
                    objArr[5] = Integer.valueOf(avatarItem.iL);
                    objArr[6] = Integer.valueOf(avatarItem.x);
                    objArr[7] = Integer.valueOf(avatarItem.y);
                    objArr[8] = Integer.valueOf(avatarItem.iM);
                    bpVar.callJS(C0030ba.format("avatarbartapped(%d, %d, %d, '%s', '%s', %d, %d, %d, %d)", objArr));
                    return;
                }
                return;
            }
            webCache.insertRequests(this.iA);
            this.iC = indexOf;
            if (this.iI != null) {
                bp bpVar2 = this.iI;
                Object[] objArr2 = new Object[8];
                objArr2[0] = Integer.valueOf(indexOf == this.it ? 1 : 0);
                objArr2[1] = Integer.valueOf(this.is);
                objArr2[2] = Integer.valueOf(avatarItem.itemId);
                objArr2[3] = contentUriFromPapayaUri3;
                objArr2[4] = Integer.valueOf(avatarItem.iL);
                objArr2[5] = Integer.valueOf(avatarItem.x);
                objArr2[6] = Integer.valueOf(avatarItem.y);
                objArr2[7] = Integer.valueOf(avatarItem.iM);
                bpVar2.callJS(C0030ba.format("avatarbartapped(%d, %d, %d, '', '%s', %d, %d, %d, %d)", objArr2));
            }
        }
    }

    public void refreshWithCtx(JSONObject jSONObject, URL url) {
        this.iu = url;
        this.iF.setVisibility(8);
        this.iH.setVisibility(8);
        this.iG.setVisibility(8);
        this.iG.setText(C0065z.stringID("avatarbar_empty"));
        this.it = -1;
        this.is = -1;
        clearResources();
        this.iv = C0040bk.getJsonString(jSONObject, "items_url");
        if (this.iv != null) {
            aD webCache = C0002a.getWebCache();
            this.iu = C0040bk.createURL(this.iv, url);
            if (this.iu != null) {
                this.iB = new bA(this.iu, false);
                this.iB.setConnectionType(1);
                this.iB.setDelegate(this);
                webCache.insertRequest(this.iB);
                this.iH.setVisibility(0);
                return;
            }
            return;
        }
        this.iG.setVisibility(0);
    }

    public void scrollToItem(int i, boolean z) {
        PPYAbsoluteLayout.LayoutParams layoutParams = (PPYAbsoluteLayout.LayoutParams) this.iy.get(i).getLayoutParams();
        if (!z) {
            this.iD.scrollTo(layoutParams.x, 0);
        } else {
            this.iD.smoothScrollTo(layoutParams.x, 0);
        }
    }

    public void selectButton(int i, boolean z) {
        if (this.it == i) {
            if (!z) {
                this.it = -1;
                this.iF.setVisibility(8);
            }
        } else if (z) {
            PPYAbsoluteLayout.setCenter(this.iF, PPYAbsoluteLayout.getCenter(this.iy.get(i).getLayoutParams()));
            this.iF.setVisibility(0);
            this.it = i;
            scrollToItem(i, true);
        }
    }

    public void setSelectedIndex(int i) {
        this.it = i;
    }

    public void setWebView(bp bpVar) {
        this.iI = bpVar;
    }

    public void setupViews(Context context) {
        Resources resources = context.getResources();
        this.iD = new HorizontalScrollView(context);
        this.iD.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.iD.setHorizontalScrollBarEnabled(true);
        this.iD.setScrollBarStyle(50331648);
        addView(this.iD);
        this.iH = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(C0036bg.rp(50), -1);
        layoutParams.addRule(13);
        this.iH.setLayoutParams(layoutParams);
        this.iH.setIndeterminate(true);
        addView(this.iH);
        this.iH.setVisibility(8);
        this.iG = new TextView(context);
        this.iG.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.iG.setGravity(17);
        this.iG.setText(C0065z.stringID("avatarbar_empty"));
        this.iG.setTextSize(1, 24.0f);
        this.iG.setVisibility(8);
        addView(this.iG);
        this.iE = new PPYAbsoluteLayout(context);
        this.iD.addView(this.iE, new FrameLayout.LayoutParams(-2, -1));
        this.iF = new ImageView(context);
        this.iF.setImageDrawable(resources.getDrawable(C0065z.drawableID("avatar_selected")));
        this.iF.setLayoutParams(new PPYAbsoluteLayout.LayoutParams(C0036bg.rp(60), C0036bg.rp(60), 0, 0));
        this.iE.addView(this.iF);
    }
}
