package com.papaya.view;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.papaya.si.C0036bg;

public class ListItem4ViewHolder {
    public ImageButton accessoryView;
    public CardImageView imageView;
    public TextView subtitleView;
    public TextView titleView;

    public ListItem4ViewHolder(View view) {
        this.imageView = (CardImageView) C0036bg.find(view, "image");
        this.titleView = (TextView) C0036bg.find(view, "title");
        this.subtitleView = (TextView) C0036bg.find(view, "subtitle");
        this.accessoryView = (ImageButton) C0036bg.find(view, "accessory");
    }
}
