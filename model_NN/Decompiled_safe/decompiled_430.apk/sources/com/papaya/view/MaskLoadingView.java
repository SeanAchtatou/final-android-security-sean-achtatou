package com.papaya.view;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.RelativeLayout;
import com.papaya.si.C0036bg;
import com.papaya.si.C0065z;

public class MaskLoadingView extends RelativeLayout implements ViewControl {
    /* access modifiers changed from: private */
    public LoadingView jZ;
    private int ka;
    private a kb;
    private int position;

    public static final class Position {
        public static final int BOTTOM = 2;
        public static final int CENTER = 0;
        public static final int TOP = 1;
    }

    public static final class Style {
        public static final int FILL = 0;
        public static final int WRAP = 1;
    }

    class a implements Runnable {
        boolean fz;

        /* synthetic */ a(MaskLoadingView maskLoadingView) {
            this((byte) 0);
        }

        private a(byte b) {
            this.fz = false;
        }

        public final void run() {
            if (!this.fz) {
                MaskLoadingView.this.jZ.getTextView().setText(C0065z.stringID("web_loading_timeout"));
            }
        }
    }

    public MaskLoadingView(Context context) {
        this(context, 1, 2);
    }

    public MaskLoadingView(Context context, int i, int i2) {
        super(context);
        setBackgroundColor(0);
        setClickable(true);
        this.position = i2;
        this.ka = i;
        setupViews();
    }

    private void setupViews() {
        this.jZ = new LoadingView(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.ka == 0 ? -1 : -2, -2);
        if (this.position == 0) {
            layoutParams.addRule(14);
            layoutParams.addRule(15);
        } else {
            layoutParams.addRule(this.position == 2 ? 12 : 10);
            layoutParams.addRule(14);
        }
        layoutParams.setMargins(0, 0, 0, C0036bg.rp(20));
        addView(this.jZ, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void cancelTimeoutTask() {
        if (this.kb != null) {
            this.kb.fz = true;
            this.kb = null;
        }
    }

    public LoadingView getLoadingView() {
        return this.jZ;
    }

    public void hide(boolean z) {
        C0036bg.removeFromSuperView(this);
        cancelTimeoutTask();
    }

    public void setText(String str) {
        this.jZ.getTextView().setText(str);
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i == 0) {
            this.jZ.fixAnimationBug();
        }
        this.jZ.getTextView().setText(C0065z.stringID("web_loading"));
        if (i != 0) {
            cancelTimeoutTask();
        } else {
            startTimeoutTask();
        }
    }

    public void showInView(ViewGroup viewGroup, boolean z) {
        if (viewGroup instanceof AbsoluteLayout) {
            setLayoutParams(new AbsoluteLayout.LayoutParams(-1, -1, 0, 0));
        } else if (viewGroup instanceof RelativeLayout) {
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }
        C0036bg.addView(viewGroup, this, true);
        startTimeoutTask();
        this.jZ.fixAnimationBug();
    }

    /* access modifiers changed from: package-private */
    public void startTimeoutTask() {
        cancelTimeoutTask();
        this.kb = new a(this);
        C0036bg.postDelayed(this.kb, 10000);
    }
}
