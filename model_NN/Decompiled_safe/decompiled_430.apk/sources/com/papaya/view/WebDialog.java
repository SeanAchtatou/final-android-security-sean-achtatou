package com.papaya.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.bF;

public class WebDialog extends CustomDialog implements DialogInterface.OnClickListener {
    private bF kP = new bF(C0042c.getApplicationContext(), null);

    public WebDialog(Context context) {
        super(context);
        this.kP.setDialog(this);
        setContentView(this.kP.getContentLayout());
        if (context instanceof Activity) {
            this.kP.setOwnerActivity((Activity) context);
        }
        setButton(-1, C0042c.getApplicationContext().getString(C0065z.stringID("close")), this);
    }

    public void dismiss() {
        this.kP.close();
        C0036bg.removeFromSuperView(this.kP.getContentLayout());
        super.dismiss();
    }

    public bF getController() {
        return this.kP;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
    }
}
