package com.papaya.view;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.FrameLayout;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0061v;
import com.papaya.si.X;
import com.papaya.si.aA;
import com.papaya.si.aQ;

public class OverlayMessage extends FrameLayout implements View.OnClickListener {
    public static final int DEFAULT_TIMEOUT = 5000;
    private float alpha = 0.95f;
    private int gravity = 48;
    private View kd;
    private WindowManager ke;
    private int kr = DEFAULT_TIMEOUT;
    private a ks;
    private View.OnClickListener kt;

    class a extends aQ {
        /* synthetic */ a(OverlayMessage overlayMessage) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            OverlayMessage.this.hide();
        }
    }

    public OverlayMessage(Context context, View view) {
        super(context);
        this.ke = C0036bg.getWindowManager(context);
        this.kd = view;
        addView(view, new FrameLayout.LayoutParams(-2, -2));
        super.setOnClickListener(this);
    }

    public static void showMessage(String str, String str2, final String str3, int i, int i2, int i3) {
        try {
            final Application applicationContext = C0042c.getApplicationContext();
            if (applicationContext != null) {
                OverlayMessageView overlayMessageView = new OverlayMessageView(applicationContext, new LazyImageView(applicationContext));
                ((LazyImageView) overlayMessageView.getImageView()).setImageUrl(str);
                overlayMessageView.getTextView().setText(str2);
                OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayMessageView);
                overlayMessage.setGravity(i2);
                overlayMessage.alpha = (((float) i3) + 0.0f) / 100.0f;
                overlayMessage.kr = i;
                if (!C0030ba.isEmpty(str3)) {
                    overlayMessageView.getAccessoryView().setVisibility(0);
                    overlayMessage.setOnClickListener(new View.OnClickListener() {
                        public final void onClick(View view) {
                            aA.getInstance().showURL(applicationContext, str3);
                        }
                    });
                } else {
                    overlayMessageView.getAccessoryView().setVisibility(8);
                }
                overlayMessage.show();
            }
        } catch (Exception e) {
        }
    }

    public static void showTitleMessage(String str, String str2, String str3, final String str4, int i, int i2, int i3) {
        try {
            Application applicationContext = C0042c.getApplicationContext();
            if (applicationContext != null) {
                OverlayTitleMessageView overlayTitleMessageView = new OverlayTitleMessageView(applicationContext, new LazyImageView(applicationContext));
                overlayTitleMessageView.getTitleView().setText(C0030ba.isEmpty(str) ? C0061v.bk : str);
                overlayTitleMessageView.getTitleView().setText(C0061v.bk);
                ((LazyImageView) overlayTitleMessageView.getImageView()).setImageUrl(str2);
                overlayTitleMessageView.getTextView().setText(str3);
                OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayTitleMessageView);
                overlayMessage.setGravity(i2);
                overlayMessage.alpha = (((float) i3) + 0.0f) / 100.0f;
                overlayMessage.kr = i;
                if (!C0030ba.isEmpty(str4)) {
                    overlayTitleMessageView.getAccessoryView().setVisibility(0);
                    overlayMessage.setOnClickListener(new View.OnClickListener() {
                        public final void onClick(View view) {
                            String str;
                            String str2 = str4;
                            int indexOf = str4.indexOf(124);
                            if (indexOf >= 0) {
                                String substring = str4.substring(0, indexOf);
                                String substring2 = str4.substring(indexOf + 1);
                                str = substring;
                                str2 = substring2;
                            } else {
                                str = null;
                            }
                            aA.getInstance().showWebActivity(null, str2, str, true);
                        }
                    });
                } else {
                    overlayTitleMessageView.getAccessoryView().setVisibility(8);
                }
                overlayMessage.show();
            }
        } catch (Exception e) {
        }
    }

    public int getGravity() {
        return this.gravity;
    }

    public int getHideTimeout() {
        return this.kr;
    }

    public void hide() {
        aQ.cancelTask(this.ks);
        try {
            if (this.ke != null && getParent() != null) {
                this.ke.removeView(this);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (this.kd != null && this.ke != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.gravity = this.gravity;
                layoutParams.windowAnimations = 16973828;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.format = -3;
                layoutParams.flags = 40;
                layoutParams.alpha = this.alpha;
                this.ke.updateViewLayout(this, layoutParams);
                AnimationSet makeSlideAnimation = C0036bg.makeSlideAnimation(this.gravity);
                makeSlideAnimation.setDuration(667);
                setLayoutAnimation(new LayoutAnimationController(makeSlideAnimation));
                startLayoutAnimation();
            }
        } catch (Exception e) {
            X.w(e, "Failed to onAttachedToWindow", new Object[0]);
        }
    }

    public void onClick(View view) {
        try {
            hide();
            if (this.kt != null) {
                this.kt.onClick(this);
            }
        } catch (Exception e) {
        }
    }

    public void setGravity(int i) {
        this.gravity = i;
    }

    public void setHideTimeout(int i) {
        this.kr = i;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.kt = onClickListener;
    }

    public void show() {
        show(null);
    }

    public void show(Context context) {
        try {
            if (getParent() != null) {
                this.ke.removeView(this);
            }
        } catch (Exception e) {
        }
        if (context != null) {
            this.ke = C0036bg.getWindowManager(context);
        }
        try {
            if (this.ke != null) {
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.width = -2;
                layoutParams.height = -2;
                layoutParams.verticalMargin = 0.08f;
                layoutParams.alpha = this.alpha;
                layoutParams.gravity = this.gravity;
                layoutParams.windowAnimations = 16973828;
                layoutParams.format = -3;
                layoutParams.type = 2005;
                this.ke.addView(this, layoutParams);
            } else {
                X.w("wm is null", new Object[0]);
            }
        } catch (Exception e2) {
            X.w(e2, "Failed to showInContext", new Object[0]);
        }
        aQ.cancelTask(this.ks);
        if (this.kr > 0) {
            this.ks = new a(this);
            C0036bg.postDelayed(this.ks, (long) this.kr);
        }
    }
}
