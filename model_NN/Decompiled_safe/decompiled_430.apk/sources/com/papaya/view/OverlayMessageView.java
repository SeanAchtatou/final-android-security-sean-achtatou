package com.papaya.view;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OverlayMessageView<IV extends ImageView> extends LinearLayout {
    private TextView fm;
    private IV kw;
    private ImageView kx;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: IV
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public OverlayMessageView(android.content.Context r7, IV r8) {
        /*
            r6 = this;
            r2 = 24
            r5 = -2
            r4 = 16
            r3 = 5
            r6.<init>(r7)
            r6.kw = r8
            java.lang.String r0 = "toast_bg"
            int r0 = com.papaya.si.C0065z.drawableID(r0)
            r6.setBackgroundResource(r0)
            r0 = 0
            r6.setOrientation(r0)
            android.widget.ImageView$ScaleType r0 = android.widget.ImageView.ScaleType.FIT_CENTER
            r8.setScaleType(r0)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            int r1 = com.papaya.si.C0036bg.rp(r7, r2)
            int r2 = com.papaya.si.C0036bg.rp(r7, r2)
            r0.<init>(r1, r2)
            r0.gravity = r4
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.topMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.bottomMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.leftMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.rightMargin = r1
            IV r1 = r6.kw
            r6.addView(r1, r0)
            android.widget.TextView r0 = new android.widget.TextView
            r0.<init>(r7)
            r6.fm = r0
            android.widget.TextView r0 = r6.fm
            r1 = -1
            r0.setTextColor(r1)
            android.widget.TextView r0 = r6.fm
            android.graphics.Typeface r1 = android.graphics.Typeface.DEFAULT_BOLD
            r0.setTypeface(r1)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r0.<init>(r5, r5)
            r0.gravity = r4
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.topMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.bottomMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.rightMargin = r1
            android.widget.TextView r1 = r6.fm
            r6.addView(r1, r0)
            android.widget.ImageView r0 = new android.widget.ImageView
            r0.<init>(r7)
            r6.kx = r0
            android.widget.ImageView r0 = r6.kx
            java.lang.String r1 = "chat_icon_fild"
            int r1 = com.papaya.si.C0065z.drawableID(r1)
            r0.setImageResource(r1)
            android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams
            r1 = 11
            int r1 = com.papaya.si.C0036bg.rp(r7, r1)
            r2 = 20
            int r2 = com.papaya.si.C0036bg.rp(r7, r2)
            r0.<init>(r1, r2)
            r0.gravity = r4
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.topMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.bottomMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.leftMargin = r1
            int r1 = com.papaya.si.C0036bg.rp(r3)
            r0.rightMargin = r1
            android.widget.ImageView r1 = r6.kx
            r6.addView(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.view.OverlayMessageView.<init>(android.content.Context, android.widget.ImageView):void");
    }

    public ImageView getAccessoryView() {
        return this.kx;
    }

    public IV getImageView() {
        return this.kw;
    }

    public TextView getTextView() {
        return this.fm;
    }
}
