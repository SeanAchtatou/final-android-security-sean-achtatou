package com.papaya.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import com.papaya.si.C0002a;
import com.papaya.si.C0036bg;
import com.papaya.si.X;
import com.papaya.si.aD;
import com.papaya.si.aZ;
import com.papaya.si.bE;
import java.io.File;

public class TakePhotoBridge {
    private static Receiver kH;
    private static Config kI;

    public static final class Config {
        public Bitmap.CompressFormat format;
        public int height;
        public int quality;
        public int source;
        public int width;

        public Config(int i, Bitmap.CompressFormat compressFormat, int i2, int i3, int i4) {
            this.source = i;
            this.format = compressFormat;
            this.width = i2;
            this.height = i3;
            this.quality = i4;
        }
    }

    public interface Receiver {
        void onPhotoCancel();

        void onPhotoTaken(String str);
    }

    public static void clear() {
        kI = null;
        kH = null;
    }

    public static void onPhtotoTaken(Activity activity, int i, int i2, Intent intent) {
        Bitmap createScaledBitmap;
        if (kH == null) {
            X.e("photo receiver is null", new Object[0]);
            return;
        }
        if (i2 == -1) {
            if (i == 13) {
                try {
                    createScaledBitmap = C0036bg.getCameraBitmap(activity, intent, kI.width, kI.height, true);
                } catch (Exception e) {
                    X.e(e, "Failed to process taken photo in bridge", new Object[0]);
                }
            } else {
                createScaledBitmap = C0036bg.createScaledBitmap(activity.getContentResolver(), intent.getData(), kI.width, kI.height, true);
            }
            aD webCache = C0002a.getWebCache();
            if (createScaledBitmap != null) {
                File cacheFile = webCache.getCacheFile(String.valueOf(System.currentTimeMillis()));
                aZ.saveBitmap(createScaledBitmap, cacheFile, kI.format, kI.quality);
                kH.onPhotoTaken(bE.ny + cacheFile.getName());
            } else {
                kH.onPhotoCancel();
            }
        } else {
            kH.onPhotoCancel();
        }
        kH = null;
    }

    public static void startTakenPhoto(Activity activity, Receiver receiver, Config config) {
        kH = receiver;
        kI = config;
        if (config.source == 0) {
            C0036bg.startCameraActivity(activity, 13);
        } else {
            C0036bg.startGalleryActivity(activity, 14);
        }
    }
}
