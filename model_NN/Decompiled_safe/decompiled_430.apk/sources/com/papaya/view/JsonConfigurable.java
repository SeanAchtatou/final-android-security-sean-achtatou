package com.papaya.view;

import org.json.JSONObject;

public interface JsonConfigurable {
    void refreshWithCtx(JSONObject jSONObject);
}
