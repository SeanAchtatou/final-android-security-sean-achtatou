package com.papaya.view;

public interface PausableAdapter {
    boolean isPaused();

    void setPaused(boolean z);
}
