package com.papaya.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;

public class LoadingView extends LinearLayout {
    private TextView fm;
    private ProgressBar jY;

    public LoadingView(Context context) {
        super(context);
        setupViews(context);
    }

    private void setupViews(Context context) {
        this.jY = new ProgressBar(context);
        this.fm = new TextView(context, null, 16842817);
        this.fm.setBackgroundColor(0);
        this.fm.setText(C0042c.getApplicationContext().getString(C0065z.stringID("web_loading")));
        this.fm.setGravity(17);
        setBackgroundResource(17301504);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(C0036bg.rp(28), C0036bg.rp(28));
        layoutParams.setMargins(C0036bg.rp(5), C0036bg.rp(5), C0036bg.rp(5), C0036bg.rp(5));
        addView(this.jY, layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams2.rightMargin = C0036bg.rp(5);
        addView(this.fm, layoutParams2);
    }

    public void fixAnimationBug() {
        this.jY.setVisibility(8);
        this.jY.setVisibility(0);
    }

    public ProgressBar getProgressBar() {
        return this.jY;
    }

    public TextView getTextView() {
        return this.fm;
    }
}
