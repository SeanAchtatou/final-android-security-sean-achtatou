package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.papaya.si.C0065z;

public class OverlayProgressDialog extends OverlayCustomDialog {
    private DynamicTextView kg;

    public OverlayProgressDialog(Context context) {
        super(context);
        View inflate = getLayoutInflater().inflate(C0065z.layoutID("progressview"), (ViewGroup) null);
        setView(inflate);
        this.kg = (DynamicTextView) inflate.findViewById(C0065z.id("message"));
    }

    public DynamicTextView getMessageView() {
        return this.kg;
    }

    public void setMessage(CharSequence charSequence) {
        this.kg.setText(charSequence);
    }
}
