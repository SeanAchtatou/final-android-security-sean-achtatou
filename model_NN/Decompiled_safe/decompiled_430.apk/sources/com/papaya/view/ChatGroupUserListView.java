package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.A;
import com.papaya.si.C0029b;
import com.papaya.si.C0030ba;
import com.papaya.si.C0031bb;
import com.papaya.si.C0042c;
import com.papaya.si.C0059t;
import com.papaya.si.C0065z;
import com.papaya.si.L;
import com.papaya.si.N;
import com.papaya.si.X;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ChatGroupUserListView extends ListView implements AdapterView.OnItemClickListener, C0031bb, C0059t {
    /* access modifiers changed from: private */
    public ArrayList<N> iO = new ArrayList<>();
    private a iP;

    class a extends BaseAdapter {
        /* synthetic */ a(ChatGroupUserListView chatGroupUserListView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final int getCount() {
            return ChatGroupUserListView.this.iO.size();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            TextView textView;
            String str;
            if (view == null) {
                View inflate = View.inflate(ChatGroupUserListView.this.getContext(), C0065z.layoutID("list_item_4"), null);
                inflate.setTag(new ListItem4ViewHolder(inflate));
                view2 = inflate;
            } else {
                view2 = view;
            }
            ListItem4ViewHolder listItem4ViewHolder = (ListItem4ViewHolder) view2.getTag();
            N n = (N) ChatGroupUserListView.this.iO.get(i);
            listItem4ViewHolder.imageView.refreshWithCard(n);
            listItem4ViewHolder.titleView.setText(n.getTitle());
            if (n.cU == A.bK.getUserID()) {
                textView = listItem4ViewHolder.subtitleView;
                str = C0042c.getString("label_chatroom_user_list_you");
            } else {
                TextView textView2 = listItem4ViewHolder.subtitleView;
                if (C0042c.getSession().getFriends().isFriend(n.cU)) {
                    textView = textView2;
                    str = C0042c.getString("label_chatroom_user_list_friend");
                } else {
                    textView = textView2;
                    str = null;
                }
            }
            textView.setText(str);
            listItem4ViewHolder.accessoryView.setVisibility(8);
            return view2;
        }
    }

    public ChatGroupUserListView(Context context) {
        super(context);
        setCacheColorHint(-1);
        setOnItemClickListener(this);
        this.iP = new a(this);
        setAdapter((ListAdapter) this.iP);
    }

    public void close() {
        C0042c.A.unregisterCmd(this, 305);
    }

    public void handleServerResponse(Vector<Object> vector) {
        int sgetInt = C0030ba.sgetInt(vector, 0);
        switch (sgetInt) {
            case 305:
                this.iO.clear();
                for (int i = 2; i < vector.size(); i++) {
                    List list = (List) C0030ba.sget(vector, i);
                    N n = new N();
                    n.cU = C0030ba.sgetInt(list, 0);
                    n.name = (String) C0030ba.sget(list, 1);
                    n.setState(C0030ba.sgetInt(list, 2) > 0 ? 1 : 0);
                    C0030ba.sgetInt(list, 3);
                    this.iO.add(n);
                }
                this.iP.notifyDataSetChanged();
                C0042c.A.unregisterCmd(this, 305);
                return;
            default:
                X.e("unknown cmd " + sgetInt, new Object[0]);
                return;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        N n = this.iO.get(i);
        if (n.cU != A.bK.getUserID()) {
            C0029b.openHome(null, n.cU);
        }
    }

    public void refreshWithGroup(L l) {
        if (l != null) {
            C0042c.send(305, Integer.valueOf(l.cQ), 0);
            C0042c.A.registerCmds(this, 305);
        }
    }
}
