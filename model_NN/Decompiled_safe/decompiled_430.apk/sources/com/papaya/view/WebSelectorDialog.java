package com.papaya.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0002a;
import com.papaya.si.C0030ba;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.aD;
import com.papaya.si.aP;
import com.papaya.si.aR;
import com.papaya.si.aZ;
import com.papaya.si.bA;
import com.papaya.si.bp;
import com.papaya.si.by;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebSelectorDialog extends CustomDialog implements AdapterView.OnItemClickListener, aR, by.a, JsonConfigurable {
    private bp iI;
    private String jG;
    private JSONObject jz;
    /* access modifiers changed from: private */
    public ArrayList<bA> kR = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Drawable> kS = new ArrayList<>();
    /* access modifiers changed from: private */
    public JSONArray kT;
    private ListView kU;
    /* access modifiers changed from: private */
    public a lI;

    class a extends BaseAdapter {
        private LayoutInflater kY;

        /* synthetic */ a(WebSelectorDialog webSelectorDialog, LayoutInflater layoutInflater) {
            this(layoutInflater, (byte) 0);
        }

        private a(LayoutInflater layoutInflater, byte b) {
            this.kY = layoutInflater;
        }

        public final int getCount() {
            if (WebSelectorDialog.this.kT == null) {
                return 0;
            }
            return WebSelectorDialog.this.kT.length();
        }

        public final Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public final long getItemId(int i) {
            return (long) i;
        }

        public final View getView(int i, View view, ViewGroup viewGroup) {
            b bVar;
            View view2;
            if (view == null) {
                View inflate = this.kY.inflate(C0065z.layoutID("list_item_3_part_inverse"), (ViewGroup) null);
                b bVar2 = new b();
                bVar2.kZ = (ImageView) inflate.findViewById(C0065z.id("list_item_3_header"));
                bVar2.la = (TextView) inflate.findViewById(C0065z.id("list_item_3_content"));
                bVar2.lb = (ImageView) inflate.findViewById(C0065z.id("list_item_3_accessory"));
                inflate.setTag(bVar2);
                b bVar3 = bVar2;
                view2 = inflate;
                bVar = bVar3;
            } else {
                bVar = (b) view.getTag();
                view2 = view;
            }
            JSONObject jsonObject = C0040bk.getJsonObject(WebSelectorDialog.this.kT, i);
            bVar.la.setText(C0040bk.getJsonString(jsonObject, "text"));
            Drawable drawable = (Drawable) WebSelectorDialog.this.kS.get(i);
            if (drawable != null) {
                bVar.kZ.setImageDrawable(drawable);
                bVar.kZ.setVisibility(0);
                bVar.kZ.setBackgroundColor(0);
            } else {
                bVar.kZ.setVisibility(4);
            }
            if (C0030ba.intValue(C0040bk.getJsonString(jsonObject, "selected"), -1) == 1) {
                bVar.lb.setVisibility(0);
                bVar.lb.setImageDrawable(this.kY.getContext().getResources().getDrawable(C0065z.drawableID("ic_check_mark_light")));
                bVar.lb.setBackgroundColor(0);
            } else {
                bVar.lb.setVisibility(4);
            }
            return view2;
        }
    }

    static class b {
        ImageView kZ;
        TextView la;
        ImageView lb;

        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    public WebSelectorDialog(Context context) {
        super(context);
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        this.kU = (ListView) layoutInflater.inflate(C0065z.layoutID("list_dialog"), (ViewGroup) null);
        this.lI = new a(this, layoutInflater);
        this.kU.setAdapter((ListAdapter) this.lI);
        this.kU.setBackgroundResource(17170447);
        this.kU.setOnItemClickListener(this);
        setView(this.kU);
    }

    public void clear() {
        aD webCache = C0002a.getWebCache();
        Iterator<bA> it = this.kR.iterator();
        while (it.hasNext()) {
            bA next = it.next();
            if (next != null) {
                webCache.removeRequest(next);
                next.setDelegate(null);
            }
        }
        this.kR.clear();
        this.kS.clear();
    }

    public void connectionFailed(final by byVar, int i) {
        C0036bg.post(new Runnable() {
            public final void run() {
                int indexOf = WebSelectorDialog.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebSelectorDialog.this.kR.set(indexOf, null);
                }
            }
        });
    }

    public void connectionFinished(final by byVar) {
        C0036bg.post(new Runnable() {
            public final void run() {
                int indexOf = WebSelectorDialog.this.kR.indexOf(byVar.getRequest());
                if (indexOf != -1) {
                    WebSelectorDialog.this.kR.set(indexOf, null);
                    ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byVar.getData());
                    try {
                        WebSelectorDialog.this.kS.set(indexOf, Drawable.createFromStream(byteArrayInputStream, "icon"));
                        WebSelectorDialog.this.lI.notifyDataSetChanged();
                    } finally {
                        aZ.close(byteArrayInputStream);
                    }
                }
            }
        });
    }

    public String getViewId() {
        return this.jG;
    }

    public bp getWebView() {
        return this.iI;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        JSONObject jsonObject = C0040bk.getJsonObject(this.kT, i);
        if (this.iI != null) {
            String jsonString = C0040bk.getJsonString(this.jz, "action");
            if (!C0030ba.isEmpty(jsonString)) {
                Object jsonValue = C0040bk.getJsonValue(jsonObject, "value");
                if (jsonValue == null) {
                    this.iI.callJSFunc("%s(%d)", jsonString, Integer.valueOf(i));
                } else if (jsonValue instanceof Number) {
                    this.iI.callJSFunc("%s(%s)", jsonString, jsonValue);
                } else {
                    this.iI.callJSFunc("%s('%s')", jsonString, C0040bk.escapeJS(jsonValue.toString()));
                }
            } else {
                Object jsonValue2 = C0040bk.getJsonValue(jsonObject, "value");
                if (jsonValue2 == null) {
                    this.iI.callJSFunc("onSelectorDialogTapped('%s', %d)", this.jG, Integer.valueOf(i));
                } else if (jsonValue2 instanceof Number) {
                    this.iI.callJSFunc("onSelectorDialogTapped('%s', %d, %s)", this.jG, Integer.valueOf(i), jsonValue2);
                } else {
                    this.iI.callJSFunc("onSelectorDialogTapped('%s', %d, '%s')", this.jG, Integer.valueOf(i), C0040bk.escapeJS(jsonValue2.toString()));
                }
            }
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        String jsonString;
        this.jz = jSONObject;
        String jsonString2 = C0040bk.getJsonString(this.jz, "title");
        if (jsonString2 == null) {
            jsonString2 = getContext().getString(C0065z.stringID("web_selector_title"));
        }
        setTitle(jsonString2);
        clear();
        this.kT = C0040bk.getJsonArray(this.jz, "options");
        URL papayaURL = this.iI.getPapayaURL();
        if (this.kT != null) {
            aD webCache = C0002a.getWebCache();
            for (int i = 0; i < this.kT.length(); i++) {
                this.kS.add(null);
                this.kR.add(null);
                JSONObject jsonObject = C0040bk.getJsonObject(this.kT, i);
                if (!"separator".equals(C0040bk.getJsonString(jsonObject, "type")) && (jsonString = C0040bk.getJsonString(jsonObject, "icon")) != null) {
                    bA bAVar = new bA();
                    bAVar.setDelegate(this);
                    aP fdFromPapayaUri = webCache.fdFromPapayaUri(jsonString, papayaURL, bAVar);
                    if (fdFromPapayaUri != null) {
                        this.kS.set(i, C0036bg.drawableFromFD(fdFromPapayaUri));
                    } else if (bAVar.getUrl() != null) {
                        this.kR.set(i, bAVar);
                    }
                }
            }
            webCache.insertRequests(this.kR);
        }
        this.lI.notifyDataSetChanged();
    }

    public void setViewId(String str) {
        this.jG = str;
    }

    public void setWebView(bp bpVar) {
        this.iI = bpVar;
    }
}
