package com.papaya.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.papaya.si.C0030ba;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.si.X;

public class CustomDialog extends Dialog implements DialogInterface, View.OnClickListener {
    private ListView cA;
    private ImageView dG = ((ImageView) f("dialog_icon"));

    /* renamed from: do  reason: not valid java name */
    private TextView f4do = ((TextView) f("dialog_message"));
    private LinearLayout iT = ((LinearLayout) f("dialog_title_content"));
    private LinearLayout iU = ((LinearLayout) f("dialog_content"));
    FrameLayout iV;
    private View iW;
    private LinearLayout iX;
    Button iY;
    Button iZ;
    Button ja;
    private DialogInterface.OnClickListener jb;
    private DialogInterface.OnClickListener jc;
    private DialogInterface.OnClickListener jd;
    TextView titleView = ((TextView) f("dialog_title"));

    public static class Builder {
        private Drawable G;
        private CharSequence H = C0061v.bk;
        private Context cN;
        private CharSequence cZ;
        private int eG;
        private View iW;
        private CharSequence jg;
        private CharSequence jh;
        private CharSequence ji;
        private DialogInterface.OnClickListener jj;
        private DialogInterface.OnClickListener jk;
        private DialogInterface.OnClickListener jl;
        private boolean jm = true;
        private DialogInterface.OnCancelListener jn;
        private DialogInterface.OnKeyListener jo;
        private CharSequence[] jp;
        private DialogInterface.OnClickListener jq;
        private ListAdapter jr;
        private AdapterView.OnItemSelectedListener js;

        public Builder(Context context) {
            this.cN = context;
        }

        public CustomDialog create() {
            CustomDialog customDialog = new CustomDialog(this.cN);
            customDialog.titleView.setText(this.H);
            if (this.eG != 0) {
                customDialog.setIcon(this.eG);
            }
            if (this.G != null) {
                customDialog.setIcon(this.G);
            }
            customDialog.setView(this.iW);
            customDialog.setMessage(this.cZ);
            if (this.jg != "") {
                customDialog.setButton(-1, this.jg, this.jj);
            } else {
                customDialog.iY.setVisibility(8);
            }
            if (this.jh != "") {
                customDialog.setButton(-2, this.jh, this.jk);
            } else {
                customDialog.ja.setVisibility(8);
            }
            if (this.ji != "") {
                customDialog.setButton(-3, this.ji, this.jl);
            } else {
                customDialog.iZ.setVisibility(8);
            }
            customDialog.setCancelable(this.jm);
            customDialog.setOnCancelListener(this.jn);
            if (this.jo != null) {
                customDialog.setOnKeyListener(this.jo);
            }
            if (this.jr == null && this.jp != null) {
                this.jr = new ArrayAdapter(this.cN, 17367057, 16908308, this.jp);
            }
            if (this.jr != null) {
                customDialog.initListView(this.jr, this.jq, this.js);
            }
            return customDialog;
        }

        public Builder setAdapter(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.jr = listAdapter;
            this.jq = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.jm = z;
            return this;
        }

        public Builder setIcon(int i) {
            this.eG = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.G = drawable;
            return this;
        }

        public Builder setItems(int i, DialogInterface.OnClickListener onClickListener) {
            this.jp = this.cN.getResources().getTextArray(i);
            this.jq = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
            this.jp = charSequenceArr;
            this.jq = onClickListener;
            return this;
        }

        public Builder setMessage(int i) {
            this.cZ = this.cN.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.cZ = charSequence;
            return this;
        }

        public Builder setNegativeButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.jh = this.cN.getText(i);
            this.jk = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.jh = charSequence;
            this.jk = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.ji = this.cN.getText(i);
            this.jl = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.ji = charSequence;
            this.jl = onClickListener;
            return this;
        }

        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
            this.jn = onCancelListener;
            return this;
        }

        public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
            this.js = onItemSelectedListener;
            return this;
        }

        public Builder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
            this.jo = onKeyListener;
            return this;
        }

        public Builder setPositiveButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.jg = this.cN.getText(i);
            this.jj = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.jg = charSequence;
            this.jj = onClickListener;
            return this;
        }

        public Builder setTitle(int i) {
            this.H = this.cN.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.H = charSequence;
            return this;
        }

        public Builder setView(View view) {
            this.iW = view;
            return this;
        }

        public CustomDialog show() {
            CustomDialog create = create();
            create.show();
            return create;
        }
    }

    public CustomDialog(Context context) {
        super(context, C0065z.styleID("DialogTheme"));
        requestWindowFeature(1);
        setContentView(C0065z.layoutID("custom_dialog"));
        this.f4do.setMovementMethod(new ScrollingMovementMethod());
        this.iV = (FrameLayout) f("dialog_custom_content");
        this.iX = (LinearLayout) f("dialog_button_content");
        this.iY = (Button) f("dialog_button_positive");
        this.iZ = (Button) f("dialog_button_neutral");
        this.ja = (Button) f("dialog_button_negative");
        this.iY.setOnClickListener(this);
        this.iZ.setOnClickListener(this);
        this.ja.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public <T> T f(String str) {
        T findViewById = findViewById(C0065z.id(str));
        if (findViewById == null) {
            X.w("can't find view with id %s", str);
        }
        return findViewById;
    }

    public Button getButton(int i) {
        switch (i) {
            case -3:
                return this.iZ;
            case -2:
                return this.ja;
            case -1:
                return this.iY;
            default:
                return null;
        }
    }

    public View getCustomView() {
        return this.iW;
    }

    public ListView getListView() {
        return this.cA;
    }

    /* access modifiers changed from: package-private */
    public void initListView(ListAdapter listAdapter, final DialogInterface.OnClickListener onClickListener, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.cA = (ListView) getLayoutInflater().inflate(C0065z.layoutID("list_dialog"), (ViewGroup) null);
        this.cA.setAdapter(listAdapter);
        if (onItemSelectedListener != null) {
            this.cA.setOnItemSelectedListener(onItemSelectedListener);
        } else if (onClickListener != null) {
            this.cA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    CustomDialog.this.dismiss();
                    onClickListener.onClick(CustomDialog.this, i);
                }
            });
        }
        this.iU.removeAllViews();
        this.iU.addView(this.cA);
    }

    public void onClick(View view) {
        dismiss();
        if (view == this.iY) {
            if (this.jb != null) {
                this.jb.onClick(this, -1);
            }
        } else if (view == this.ja) {
            if (this.jd != null) {
                this.jd.onClick(this, -2);
            }
        } else if (view == this.iZ && this.jc != null) {
            this.jc.onClick(this, -3);
        }
    }

    public void setButton(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        Button button = getButton(i);
        if (button != null) {
            button.setText(charSequence);
            switch (i) {
                case -3:
                    this.jc = onClickListener;
                    return;
                case -2:
                    this.jd = onClickListener;
                    return;
                case -1:
                    this.jb = onClickListener;
                    return;
                default:
                    return;
            }
        }
    }

    public void setIcon(int i) {
        this.dG.setImageResource(i);
    }

    public void setIcon(Drawable drawable) {
        this.dG.setImageDrawable(drawable);
    }

    public void setMessage(CharSequence charSequence) {
        this.f4do.setText(charSequence);
    }

    public void setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-2, charSequence, onClickListener);
    }

    public void setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-3, charSequence, onClickListener);
    }

    public void setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        setButton(-1, charSequence, onClickListener);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.titleView.setText(charSequence);
    }

    public void setTitleBgRes(int i) {
        this.iT.setBackgroundResource(i);
    }

    public void setTitleColor(int i) {
        this.titleView.setTextColor(i);
    }

    public void setView(View view) {
        this.iW = view;
        this.iV.removeAllViews();
        if (this.iW != null) {
            this.iV.addView(this.iW);
        }
    }

    public void show() {
        updateVisibility();
        super.show();
    }

    /* access modifiers changed from: package-private */
    public void updateVisibility() {
        if (this.iW != null) {
            this.iV.setVisibility(0);
            this.iU.setVisibility(8);
        } else {
            this.iV.setVisibility(8);
            if (!C0030ba.isEmpty(this.f4do.getText()) || this.cA != null) {
                this.iU.setVisibility(0);
            } else {
                this.iU.setVisibility(8);
            }
        }
        if (!C0030ba.isEmpty(this.iY.getText()) || !C0030ba.isEmpty(this.iZ.getText()) || !C0030ba.isEmpty(this.ja.getText())) {
            this.iX.setVisibility(0);
            this.iY.setVisibility(C0030ba.isEmpty(this.iY.getText()) ? 8 : 0);
            this.ja.setVisibility(C0030ba.isEmpty(this.ja.getText()) ? 8 : 0);
            this.iZ.setVisibility(C0030ba.isEmpty(this.iZ.getText()) ? 8 : 0);
            return;
        }
        this.iX.setVisibility(8);
    }
}
