package com.papaya.view;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.papaya.si.C0036bg;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.view.HorizontalScrollView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class HorizontalBar extends RelativeLayout implements View.OnClickListener, HorizontalScrollView.Delegate, JsonConfigurable {
    private WeakReference<Delegate> eA;
    private ArrayList<PushButton> jA = new ArrayList<>(10);
    private HorizontalScrollView jB;
    private LinearLayout jC;
    private ImageButton jD;
    private ImageButton jE;
    private boolean jF = false;
    private String jG;
    private JSONObject jz;

    public interface Delegate {
        void barButtonClicked(HorizontalBar horizontalBar, int i, JSONObject jSONObject);
    }

    public HorizontalBar(Context context, AbsoluteLayout.LayoutParams layoutParams) {
        super(context);
        setLayoutParams(layoutParams);
        setupViews();
    }

    private void centerSelected() {
        int i;
        int i2 = 0;
        while (true) {
            if (i2 >= this.jA.size()) {
                i = -1;
                break;
            } else if (this.jA.get(i2).isSelected()) {
                i = i2;
                break;
            } else {
                i2++;
            }
        }
        if (i != -1) {
            PushButton pushButton = this.jA.get(i);
            float left = ((float) pushButton.getLeft()) + (((float) pushButton.getWidth()) / 2.0f);
            float left2 = (float) this.jA.get(0).getLeft();
            float right = (float) this.jA.get(this.jA.size() - 1).getRight();
            float scrollX = (float) this.jB.getScrollX();
            float width = (float) this.jB.getWidth();
            if (left - scrollX > width / 2.0f) {
                float f = left - (width / 2.0f);
                this.jB.smoothScrollTo((int) (right - f < width ? right - width : f), 0);
            } else if (left - scrollX < width / 2.0f) {
                float f2 = left - (width / 2.0f);
                if (f2 < 0.0f || left2 - f2 < 0.0f) {
                    f2 = left2;
                }
                this.jB.smoothScrollTo((int) f2, 0);
            }
        }
    }

    private void refreshArrows() {
        int scrollX = this.jB.getScrollX();
        if (scrollX == 0) {
            this.jD.setVisibility(8);
        } else {
            this.jD.setVisibility(0);
        }
        if (this.jA.size() <= 0) {
            this.jE.setVisibility(8);
        } else if (this.jA.get(this.jA.size() - 1).getRight() - scrollX > this.jB.getWidth()) {
            this.jE.setVisibility(0);
        } else {
            this.jE.setVisibility(8);
        }
    }

    private void setupViews() {
        if (this.jB == null) {
            this.jB = new HorizontalScrollView(getContext());
            this.jB.setDelegate(this);
            this.jB.setHorizontalScrollBarEnabled(false);
            this.jB.setFadingEdgeLength(10);
            this.jB.setHorizontalFadingEdgeEnabled(true);
            this.jD = new ImageButton(getContext());
            this.jE = new ImageButton(getContext());
            this.jD.setImageDrawable(getContext().getResources().getDrawable(C0065z.drawableID("hori_arrow_l")));
            this.jD.setBackgroundColor(0);
            this.jE.setImageDrawable(getContext().getResources().getDrawable(C0065z.drawableID("hori_arrow_r")));
            this.jE.setBackgroundColor(0);
            this.jC = new LinearLayout(getContext());
            this.jC.setBackgroundColor(0);
            this.jB.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.jB.addView(this.jC);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(34, 36);
            layoutParams.addRule(9);
            layoutParams.addRule(15);
            this.jD.setLayoutParams(layoutParams);
            this.jD.setOnClickListener(this);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(34, 36);
            layoutParams2.addRule(11);
            layoutParams2.addRule(15);
            this.jE.setLayoutParams(layoutParams2);
            this.jE.setOnClickListener(this);
            addView(this.jB);
            addView(this.jD);
            addView(this.jE);
        }
    }

    public Delegate getDelegate() {
        if (this.eA != null) {
            return this.eA.get();
        }
        return null;
    }

    public String getViewId() {
        return this.jG;
    }

    public void onClick(View view) {
        if (view == this.jD) {
            this.jB.arrowScroll(17);
        } else if (view == this.jE) {
            this.jB.arrowScroll(66);
        } else {
            Delegate delegate = getDelegate();
            if (delegate != null) {
                int indexOf = this.jA.indexOf(view);
                delegate.barButtonClicked(this, indexOf, C0040bk.getJsonObject(C0040bk.getJsonArray(this.jz, "tabs"), indexOf));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        refreshArrows();
        if (!this.jF) {
            centerSelected();
            this.jF = true;
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.jz = jSONObject;
        for (int i = 0; i < this.jA.size(); i++) {
            PushButton pushButton = this.jA.get(i);
            pushButton.setOnClickListener(null);
            C0036bg.removeFromSuperView(pushButton);
        }
        this.jA.clear();
        JSONArray jsonArray = C0040bk.getJsonArray(this.jz, "tabs");
        if (jsonArray != null) {
            int i2 = 0;
            while (i2 < jsonArray.length()) {
                try {
                    JSONObject jSONObject2 = jsonArray.getJSONObject(i2);
                    PushButton pushButton2 = i2 == 0 ? new PushButton(getContext(), C0065z.drawableID("hori_btn_left"), C0065z.drawableID("hori_btn_hover_left")) : i2 == jsonArray.length() - 1 ? new PushButton(getContext(), C0065z.drawableID("hori_btn_right"), C0065z.drawableID("hori_btn_hover_right")) : new PushButton(getContext(), C0065z.drawableID("hori_btn"), C0065z.drawableID("hori_btn_hover"));
                    pushButton2.setText(C0040bk.getJsonString(jSONObject2, "text"));
                    pushButton2.setTextColor(-16777216);
                    pushButton2.setTypeface(Typeface.DEFAULT_BOLD);
                    pushButton2.setGravity(16);
                    pushButton2.setPadding(C0036bg.rp(10), C0036bg.rp(6), C0036bg.rp(10), C0036bg.rp(10));
                    if (C0040bk.getJsonInt(jSONObject2, "active") == 1) {
                        pushButton2.setSelected(true);
                    }
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.rightMargin = 1;
                    pushButton2.setLayoutParams(layoutParams);
                    this.jC.addView(pushButton2);
                    pushButton2.setOnClickListener(this);
                    this.jA.add(pushButton2);
                } catch (Exception e) {
                    X.w(e, "Failed to config with ctx", new Object[0]);
                }
                i2++;
            }
        }
    }

    public void scrollChanged(int i, int i2, int i3, int i4) {
        refreshArrows();
    }

    public void setActiveButton(int i) {
        int i2 = 0;
        while (i2 < this.jA.size()) {
            this.jA.get(i2).setSelected(i == i2);
            i2++;
        }
    }

    public void setDelegate(Delegate delegate) {
        if (delegate == null) {
            this.eA = null;
        } else {
            this.eA = new WeakReference<>(delegate);
        }
    }

    public void setViewId(String str) {
        this.jG = str;
    }
}
