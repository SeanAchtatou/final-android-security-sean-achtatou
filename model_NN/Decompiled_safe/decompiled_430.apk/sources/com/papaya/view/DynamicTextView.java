package com.papaya.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.papaya.si.aQ;

public class DynamicTextView extends TextView {
    /* access modifiers changed from: private */
    public int index;
    /* access modifiers changed from: private */
    public boolean jt = false;
    /* access modifiers changed from: private */
    public CharSequence[] ju;
    private int jv;
    private a jw;

    class a extends aQ {
        /* synthetic */ a(DynamicTextView dynamicTextView) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            if (DynamicTextView.this.ju != null) {
                DynamicTextView.access$108(DynamicTextView.this);
                if (DynamicTextView.this.index < DynamicTextView.this.ju.length) {
                    DynamicTextView.this.setText2(DynamicTextView.this.ju[DynamicTextView.this.index]);
                    boolean unused = DynamicTextView.this.jt = false;
                }
            }
        }
    }

    public DynamicTextView(Context context) {
        super(context);
    }

    public DynamicTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DynamicTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    static /* synthetic */ int access$108(DynamicTextView dynamicTextView) {
        int i = dynamicTextView.index;
        dynamicTextView.index = i + 1;
        return i;
    }

    private void postShowMessage() {
        stopMessageTask();
        this.jw = new a(this);
        postDelayed(this.jw, (long) this.jv);
    }

    /* access modifiers changed from: private */
    public void setText2(CharSequence charSequence) {
        this.jt = true;
        setText(charSequence);
        this.jt = false;
    }

    private void stopMessageTask() {
        if (this.jw != null) {
            this.jw.fz = true;
            this.jw = null;
        }
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        super.setText(charSequence, bufferType);
        if (!this.jt) {
            stopMessageTask();
            this.ju = null;
            this.index = 0;
        }
    }

    public void setTexts(int i, int... iArr) {
        this.ju = new CharSequence[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            this.ju[i2] = getContext().getResources().getString(iArr[i2]);
        }
        setTexts(i, this.ju);
    }

    public void setTexts(int i, CharSequence... charSequenceArr) {
        this.ju = charSequenceArr;
        this.jv = i;
        stopMessageTask();
        if (this.ju.length > 0) {
            setText2(this.ju[0]);
            if (this.ju.length > 1) {
                postShowMessage();
            }
        }
    }
}
