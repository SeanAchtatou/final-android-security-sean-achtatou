package com.papaya.view;

import android.content.Context;
import android.content.DialogInterface;
import com.papaya.si.C0030ba;
import com.papaya.si.C0040bk;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.bp;
import org.json.JSONArray;
import org.json.JSONObject;

public class WebAlertDialog extends CustomDialog implements DialogInterface.OnClickListener, JsonConfigurable {
    private JSONObject kJ;
    private bp kK;
    private String kL;

    public WebAlertDialog(Context context) {
        super(context);
    }

    public String getViewId() {
        return this.kL;
    }

    public bp getWebView() {
        return this.kK;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.kK != null) {
            JSONArray jsonArray = C0040bk.getJsonArray(this.kJ, "buttons");
            int dialogButtonToWebIndex = C0040bk.dialogButtonToWebIndex(i);
            String jsonString = jsonArray != null ? C0040bk.getJsonString(C0040bk.getJsonObject(jsonArray, dialogButtonToWebIndex), "action") : null;
            if (C0030ba.isEmpty(jsonString)) {
                String jsonString2 = C0040bk.getJsonString(this.kJ, "action");
                bp bpVar = this.kK;
                String str = C0030ba.isEmpty(jsonString2) ? "onAlertViewButtonTapped" : jsonString2;
                Object[] objArr = new Object[3];
                if (C0030ba.isEmpty(jsonString2)) {
                    jsonString2 = "onAlertViewButtonTapped";
                }
                objArr[0] = jsonString2;
                objArr[1] = this.kL == null ? "" : this.kL;
                objArr[2] = Integer.valueOf(dialogButtonToWebIndex);
                bpVar.noWarnCallJS(str, C0030ba.format("%s('%s', %d)", objArr));
                return;
            }
            this.kK.callJS(jsonString);
        }
    }

    public void refreshWithCtx(JSONObject jSONObject) {
        this.kJ = jSONObject;
        if (C0040bk.getJsonString(jSONObject, "title") != null) {
            setTitle(C0040bk.getJsonString(jSONObject, "title"));
        }
        String jsonString = C0040bk.getJsonString(jSONObject, "text");
        if (jsonString != null) {
            setMessage(jsonString);
        }
        int jsonInt = C0040bk.getJsonInt(jSONObject, "icon", -1);
        if (jsonInt != -1) {
            setIcon(jsonInt);
        }
        JSONArray jsonArray = C0040bk.getJsonArray(jSONObject, "buttons");
        if (jsonArray == null || jsonArray.length() <= 0) {
            setButton(-2, "OK", this);
            return;
        }
        for (int i = 0; i < Math.min(jsonArray.length(), 3); i++) {
            String jsonString2 = C0040bk.getJsonString(C0040bk.getJsonObject(jsonArray, i), "text");
            if (C0030ba.isEmpty(jsonString2)) {
                jsonString2 = "Unknown";
            }
            if (i == 0) {
                setButton(-2, jsonString2, this);
            } else if (i == 1) {
                setButton(-3, jsonString2, this);
            } else if (i == 2) {
                setButton(-1, jsonString2, this);
            }
        }
    }

    public void setDefaultTitle(int i) {
        if (C0030ba.isEmpty(null)) {
            switch (i) {
                case -1:
                    setTitle((CharSequence) null);
                    return;
                case 0:
                    setTitle(C0065z.stringID("note"));
                    return;
                case 1:
                    setTitle(C0065z.stringID("warning"));
                    return;
                case 2:
                    setTitle(C0065z.stringID("help"));
                    return;
                default:
                    X.w("unknown icon id %d", Integer.valueOf(i));
                    setTitle((CharSequence) null);
                    return;
            }
        }
    }

    public void setIcon(int i) {
        int drawableID;
        setDefaultTitle(i);
        switch (i) {
            case -1:
                drawableID = 0;
                break;
            case 0:
                drawableID = C0065z.drawableID("alert_icon_check");
                break;
            case 1:
                drawableID = C0065z.drawableID("alert_icon_warning");
                break;
            case 2:
                drawableID = C0065z.drawableID("alert_icon_help");
                break;
            default:
                X.w("unknown icon id %d", Integer.valueOf(i));
                drawableID = 0;
                break;
        }
        super.setIcon(drawableID);
    }

    public void setText(String str) {
        setMessage(str);
    }

    public void setViewId(String str) {
        this.kL = str;
    }

    public void setWebView(bp bpVar) {
        this.kK = bpVar;
    }

    public void show() {
        if (this.kJ == null) {
            setButton(-2, "OK", this);
        }
        super.show();
    }
}
