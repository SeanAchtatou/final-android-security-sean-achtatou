package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.papaya.si.C0036bg;
import com.papaya.si.C0042c;
import com.papaya.si.O;

public class EmoticonPanelView extends GridView implements AdapterView.OnItemClickListener {
    private Delegate jy;

    public interface Delegate {
        void onEmoticonSelected(EmoticonPanelView emoticonPanelView, int i, String str);
    }

    public static class IconAdapter extends BaseAdapter {
        private Context cj;

        public IconAdapter(Context context) {
            this.cj = context;
        }

        public int getCount() {
            return 42;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView;
            if (view == null) {
                imageView = new ImageView(this.cj);
                imageView.setLayoutParams(new AbsListView.LayoutParams(C0036bg.rp(32), C0036bg.rp(32)));
                imageView.setAdjustViewBounds(false);
                imageView.setPadding(C0036bg.rp(2), C0036bg.rp(2), C0036bg.rp(2), C0036bg.rp(2));
            } else {
                imageView = (ImageView) view;
            }
            imageView.setImageDrawable(C0042c.getDrawable("e" + i));
            return imageView;
        }
    }

    public EmoticonPanelView(Context context) {
        super(context);
        setPadding(C0036bg.rp(10), C0036bg.rp(10), C0036bg.rp(10), C0036bg.rp(10));
        setVerticalSpacing(C0036bg.rp(10));
        setHorizontalSpacing(C0036bg.rp(10));
        setColumnWidth(C0036bg.rp(32));
        setStretchMode(2);
        setGravity(80);
        setNumColumns(-1);
        setAdapter((ListAdapter) new IconAdapter(context));
        setOnItemClickListener(this);
    }

    public Delegate getDelegate() {
        return this.jy;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.jy != null) {
            this.jy.onEmoticonSelected(this, i, O.getEmoticonString(i));
        }
    }

    public void setDelegate(Delegate delegate) {
        this.jy = delegate;
    }
}
