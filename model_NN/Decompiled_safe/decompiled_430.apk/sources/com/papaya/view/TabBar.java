package com.papaya.view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.papaya.si.C0036bg;
import com.papaya.si.C0065z;

public class TabBar extends LinearLayout {
    private TabBarContentView kA;
    private View kB;

    public TabBar(Context context) {
        super(context);
        setOrientation(1);
        this.kA = new TabBarContentView(context);
        addView(this.kA, new LinearLayout.LayoutParams(-1, C0036bg.rp(63)));
        this.kB = new View(context);
        this.kB.setBackgroundResource(C0065z.drawableID("tab_bottom_bg"));
        addView(this.kB, new LinearLayout.LayoutParams(-1, C0036bg.rp(5)));
    }

    public TabBarContentView getTabsView() {
        return this.kA;
    }
}
