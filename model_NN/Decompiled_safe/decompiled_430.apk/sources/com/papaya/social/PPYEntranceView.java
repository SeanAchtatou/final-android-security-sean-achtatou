package com.papaya.social;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.papaya.si.C0024av;
import com.papaya.si.C0029b;
import com.papaya.si.C0040bk;
import com.papaya.si.C0042c;
import com.papaya.si.C0061v;
import com.papaya.si.C0065z;
import com.papaya.si.X;
import com.papaya.si.aA;
import com.papaya.si.aC;
import com.papaya.si.bA;
import com.papaya.si.by;
import com.papaya.social.PPYSocial;
import com.papaya.view.LazyImageView;
import org.json.JSONException;
import org.json.JSONObject;

public class PPYEntranceView extends FrameLayout implements ViewSwitcher.ViewFactory {
    private LinearLayout eQ;
    private ImageView eR;
    private ImageView eS;
    /* access modifiers changed from: private */
    public boolean eT = true;
    private PorterDuff.Mode eU = PorterDuff.Mode.MULTIPLY;
    /* access modifiers changed from: private */
    public int eV = 0;
    /* access modifiers changed from: private */
    public LazyImageView eW = null;
    private int eX = -1518938;
    private int eY = -6066900;
    private int eZ = -16777216;
    /* access modifiers changed from: private */
    public String fa = C0042c.getString("papaya_welcome");
    /* access modifiers changed from: private */
    public TextSwitcher fb;
    /* access modifiers changed from: private */
    public JSONObject[] fc = null;
    /* access modifiers changed from: private */
    public int fd = 0;
    /* access modifiers changed from: private */
    public bA fe;
    /* access modifiers changed from: private */
    public by.a ff = new by.b(this);
    private PPYSocial.Delegate fg = new C0024av(this);
    /* access modifiers changed from: private */
    public Handler fh = null;

    public PPYEntranceView(Context context) {
        super(context);
        _intBannerView(context, null, 0);
    }

    public PPYEntranceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        _intBannerView(context, attributeSet, 0);
    }

    public PPYEntranceView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        _intBannerView(context, attributeSet, i);
    }

    private void _intBannerView(Context context, AttributeSet attributeSet, int i) {
        inflate(getContext(), C0065z.layoutID("banner"), this);
        findViewById(C0065z.layoutID("banner"));
        this.eR = (ImageView) findViewById(C0065z.id("ring"));
        this.eR.setColorFilter(this.eY, this.eU);
        this.eS = (ImageView) findViewById(C0065z.id("circle"));
        this.eS.setColorFilter(this.eX, this.eU);
        findViewById(C0065z.id("ppyicon"));
        this.eQ = (LinearLayout) findViewById(C0065z.id("lowlinear"));
        this.eQ.getBackground().setColorFilter(this.eX, this.eU);
        this.eW = (LazyImageView) findViewById(C0065z.id("avaicon"));
        this.fb = (TextSwitcher) findViewById(C0065z.id("bantext"));
        this.fb.setFactory(this);
        this.fb.setText(this.fa);
        this.fb.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                if (aA.getInstance().isForceShowWelcome()) {
                    try {
                        aA.getInstance().showWelcome(C0042c.getApplicationContext());
                    } catch (Exception e) {
                        X.e(e, "Failed to showWelcome", new Object[0]);
                    }
                } else if (PPYEntranceView.this.fc != null) {
                    try {
                        if (PPYEntranceView.this.fd == 0 && ((TextView) PPYEntranceView.this.fb.getCurrentView()).getText().equals(C0042c.getString("papaya_welcome"))) {
                            C0029b.openPRIALink(PPYEntranceView.this.getContext(), "static_home", "home");
                        } else if ("".equals(PPYEntranceView.this.fc[PPYEntranceView.this.fd].getString("tabname"))) {
                            C0029b.openPRIALink(PPYEntranceView.this.getContext(), PPYEntranceView.this.fc[PPYEntranceView.this.fd].getString("url"));
                        } else {
                            C0029b.openPRIALink(PPYEntranceView.this.getContext(), "static_home", PPYEntranceView.this.fc[PPYEntranceView.this.fd].getString("tabname"));
                        }
                    } catch (JSONException e2) {
                        X.e("failed openRPINALink error: %s", e2);
                    }
                } else {
                    C0029b.openPRIALink(PPYEntranceView.this.getContext(), "static_home", "home");
                }
            }
        });
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), C0065z.animID("fade_in"));
        Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), C0065z.animID("fade_out"));
        this.fb.setInAnimation(loadAnimation);
        this.fb.setOutAnimation(loadAnimation2);
        aA.getInstance().addDelegate(this.fg, true);
        if (this.eW.getDrawable() == null && aC.getInstance().isConnected()) {
            this.eW.setImageUrl(PPYSocial.getAvatarUrlString(PPYSession.getInstance().getUID()));
        }
        if (this.fc == null && aC.getInstance().isConnected()) {
            String str = C0061v.bi + "bannernews?uid=" + PPYSession.getInstance().getUID();
            if (this.fe == null) {
                this.fe = new bA(C0040bk.createURL(str), false);
            }
            this.fe.setDelegate(this.ff);
            if (this.eV == 0) {
                this.fe.start(true);
                this.eV++;
            }
        }
    }

    public static /* synthetic */ int access$108(PPYEntranceView pPYEntranceView) {
        int i = pPYEntranceView.eV;
        pPYEntranceView.eV = i + 1;
        return i;
    }

    static /* synthetic */ int access$608(PPYEntranceView pPYEntranceView) {
        int i = pPYEntranceView.fd;
        pPYEntranceView.fd = i + 1;
        return i;
    }

    /* access modifiers changed from: private */
    public void runHandle() {
        if (this.fc == null) {
            return;
        }
        if (this.fc == null || this.fc.length != 0) {
            this.fh = new Handler();
            this.fh.postDelayed(new Runnable() {
                public final void run() {
                    if (!PPYEntranceView.this.eT) {
                        return;
                    }
                    if (PPYEntranceView.this.fc == null || PPYEntranceView.this.fc.length != 0) {
                        PPYEntranceView.this.fh.postDelayed(this, 4000);
                        if (PPYEntranceView.this.eT) {
                            try {
                                if (PPYEntranceView.this.fc == null || PPYEntranceView.this.fc.length <= 0) {
                                    String unused = PPYEntranceView.this.fa;
                                    return;
                                }
                                PPYEntranceView.access$608(PPYEntranceView.this);
                                int unused2 = PPYEntranceView.this.fd = PPYEntranceView.this.fd % PPYEntranceView.this.fc.length;
                                PPYEntranceView.this.fb.setText(PPYEntranceView.this.fc[PPYEntranceView.this.fd].getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }, 4000);
        }
    }

    public int getBackColor() {
        return this.eX;
    }

    public int getCirColor() {
        return this.eY;
    }

    public int getTextColor() {
        return this.eZ;
    }

    public View makeView() {
        TextView textView = new TextView(getContext());
        textView.setTextColor(this.eZ);
        textView.setGravity(19);
        textView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setTextSize(12.0f);
        return textView;
    }

    public void removeAnimation() {
        if (this.fb != null) {
            this.fb.setInAnimation(null);
            this.fb.setOutAnimation(null);
        }
    }

    public void setBackColor(int i) {
        this.eX = i;
        if (this.eS != null) {
            this.eS.setColorFilter(i, this.eU);
        }
        if (this.eQ != null) {
            this.eQ.getBackground().setColorFilter(i, this.eU);
        }
    }

    public void setCirColor(int i) {
        this.eY = i;
        if (this.eR != null) {
            this.eR.setColorFilter(i, this.eU);
        }
    }

    public void setRunThread(boolean z) {
        this.eT = z;
        if (!this.eT || this.fb == null) {
            this.fh = null;
        } else if (this.fh == null) {
            runHandle();
        } else {
            this.fh = null;
            runHandle();
        }
    }

    public void setTextColor(int i) {
        this.eZ = i;
        if (this.fb != null) {
            ((TextView) this.fb.getCurrentView()).setTextColor(i);
            ((TextView) this.fb.getNextView()).setTextColor(i);
        }
    }
}
