package com.papaya.social;

import com.papaya.si.C0027ay;
import com.papaya.si.aA;
import com.papaya.social.PPYSocialQuery;

public class PPYSocialChallengeService {
    public static final int STATUS_FAILED = 3;
    public static final int STATUS_SUCCESS = 2;
    private static PPYSocialChallengeService fx = new PPYSocialChallengeService();

    public interface Delegate {
        void onChallengeAccepted(PPYSocialChallengeRecord pPYSocialChallengeRecord);
    }

    private PPYSocialChallengeService() {
    }

    public static PPYSocialChallengeService getInstance() {
        return fx;
    }

    public void addDelegate(Delegate delegate) {
        C0027ay.instance().addDelegate(delegate);
    }

    public void removedelegate(Delegate delegate) {
        C0027ay.instance().removedelegate(delegate);
    }

    public PPYSocialQuery sendChallenge(PPYSocialChallengeRecord pPYSocialChallengeRecord, PPYSocialQuery.QueryDelegate queryDelegate) {
        return aA.getInstance().sendChallenge(pPYSocialChallengeRecord, queryDelegate);
    }

    public PPYSocialQuery updateChallengeStatus(int i, int i2, PPYSocialQuery.QueryDelegate queryDelegate) {
        return aA.getInstance().updateChallengeStatus(i, i2, queryDelegate);
    }
}
