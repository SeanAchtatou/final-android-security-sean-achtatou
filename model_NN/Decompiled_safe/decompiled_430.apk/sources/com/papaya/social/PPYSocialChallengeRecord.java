package com.papaya.social;

public class PPYSocialChallengeRecord {
    public static final int PAYLOAD_BINARY = 1;
    public static final int PAYLOAD_STRING = 0;
    private String fk;
    private int fq;
    private int fr;
    private int fs;
    private int ft;
    private String fu;
    private byte[] fv;
    private int fw = 0;

    public int getChallengeDefinitionID() {
        return this.fr;
    }

    public String getMessage() {
        return this.fk;
    }

    public String getPayload() {
        return this.fu;
    }

    public byte[] getPayloadBinary() {
        return this.fv;
    }

    public int getPayloadType() {
        return this.fw;
    }

    public int getReceiverUserID() {
        return this.ft;
    }

    public int getRecordID() {
        return this.fq;
    }

    public int getSenderUserID() {
        return this.fs;
    }

    public void setChallengeDefinitionID(int i) {
        this.fr = i;
    }

    public void setMessage(String str) {
        this.fk = str;
    }

    public void setPayload(String str) {
        this.fu = str;
        this.fv = null;
        this.fw = 0;
    }

    public void setPayloadBinary(byte[] bArr) {
        this.fv = bArr;
        this.fu = null;
        this.fw = 1;
    }

    public void setReceiverUserID(int i) {
        this.ft = i;
    }

    public void setRecordID(int i) {
        this.fq = i;
    }

    public void setSenderUserID(int i) {
        this.fs = i;
    }
}
