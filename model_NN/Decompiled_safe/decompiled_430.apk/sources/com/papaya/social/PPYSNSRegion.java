package com.papaya.social;

public enum PPYSNSRegion {
    GLOBAL("Global"),
    CHINA("China");
    
    public final String region;

    private PPYSNSRegion(String str) {
        this.region = str;
    }
}
