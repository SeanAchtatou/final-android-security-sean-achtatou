package com.papaya.social;

import com.papaya.si.aC;
import java.util.HashMap;
import java.util.List;

public final class PPYSession {
    private static PPYSession fp = new PPYSession();

    private PPYSession() {
    }

    public static PPYSession getInstance() {
        return fp;
    }

    public final int getAppID() {
        return aC.getInstance().getAppID();
    }

    public final String getDefaultBoardID() {
        return aC.getInstance().gr;
    }

    public final String getNickname() {
        return aC.getInstance().getNickname();
    }

    public final int getScore() {
        return aC.getInstance().getScore();
    }

    public final int getScore(String str) {
        return aC.getInstance().getScore(str);
    }

    public final HashMap<String, Integer> getScores() {
        return aC.getInstance().getScores();
    }

    public final String getSessionReceipt() {
        return aC.getInstance().getSessionReceipt();
    }

    public final String getSessionSecret() {
        return aC.getInstance().getSessionSecret();
    }

    public final int getUID() {
        return aC.getInstance().getUID();
    }

    public final boolean isConnected() {
        return aC.getInstance().isConnected();
    }

    public final boolean isDev() {
        return aC.getInstance().isDev();
    }

    public final List<PPYUser> listFriends() {
        return aC.getInstance().listFriends();
    }

    public final String toString() {
        return "PPYSession, " + "UID:" + getUID() + ", nickname:" + getNickname() + ", scores:" + getScores();
    }
}
