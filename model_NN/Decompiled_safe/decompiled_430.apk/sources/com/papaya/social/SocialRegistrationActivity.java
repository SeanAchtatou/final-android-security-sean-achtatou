package com.papaya.social;

import android.os.Bundle;
import com.papaya.web.WebActivity;

public class SocialRegistrationActivity extends WebActivity {
    /* access modifiers changed from: protected */
    public String getDefaultInitUrl() {
        return "static_welcome";
    }

    /* access modifiers changed from: protected */
    public boolean loadBackable() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean loadRequireSid() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean loadSupportMenu() {
        return false;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }
}
