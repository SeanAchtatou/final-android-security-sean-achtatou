package com.papaya.social;

import android.content.Context;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.TextView;
import com.papaya.si.C0042c;
import com.papaya.si.C0065z;
import com.papaya.si.aA;
import com.papaya.view.AppIconView;
import com.papaya.view.CustomDialog;
import com.papaya.view.LazyImageView;

public final class PPYPostNewsfeedDialog extends CustomDialog implements DialogInterface.OnClickListener {
    private String fk;
    private String fl;
    private TextView fm;
    private LazyImageView fn;

    public PPYPostNewsfeedDialog(Context context, String str) {
        this(context, str, null);
    }

    public PPYPostNewsfeedDialog(Context context, String str, String str2) {
        super(context);
        this.fk = str;
        this.fl = str2;
        setTitle(C0042c.getString("dlg_post_newsfeed_title"));
        setView(getLayoutInflater().inflate(C0065z.layoutID("newsfeed_dialog_content"), (ViewGroup) null));
        this.fm = (TextView) findViewById(C0065z.id("newsfeed_content"));
        this.fn = (LazyImageView) findViewById(C0065z.id("app_icon"));
        this.fm.setText(str);
        this.fn.setImageUrl(AppIconView.composeAppIconUrl(PPYSession.getInstance().getAppID()));
        setButton(-1, C0042c.getString("button_share"), this);
        setButton(-2, C0042c.getString("button_cancel"), this);
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            aA.getInstance().postNewsfeed(this.fk, this.fl);
        }
    }

    public final void setMessage(String str) {
        this.fk = str;
        this.fm.setText(str);
    }

    public final void setUri(String str) {
        this.fl = str;
    }
}
