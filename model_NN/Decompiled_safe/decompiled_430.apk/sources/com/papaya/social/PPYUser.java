package com.papaya.social;

public interface PPYUser {
    String getNickname();

    int getUserID();
}
