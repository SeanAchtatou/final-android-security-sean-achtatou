package com.papaya.social;

public class PPYLocalScore {
    public int score;
    public long time;

    public PPYLocalScore() {
    }

    public PPYLocalScore(int i, long j) {
        this.score = i;
        this.time = j;
    }

    public int getScore() {
        return this.score;
    }

    public long getTime() {
        return this.time;
    }

    public void setScore(int i) {
        this.score = i;
    }

    public void setTime(long j) {
        this.time = j;
    }
}
