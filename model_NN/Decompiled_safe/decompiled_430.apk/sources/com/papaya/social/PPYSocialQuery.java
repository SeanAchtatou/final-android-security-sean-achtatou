package com.papaya.social;

import com.papaya.si.X;
import com.papaya.si.aZ;
import org.json.JSONException;
import org.json.JSONObject;

public final class PPYSocialQuery {
    private JSONObject eK;
    private QueryDelegate fy;
    private boolean fz;

    public interface QueryDelegate {
        void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str);

        void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject);
    }

    public PPYSocialQuery(String str) {
        this(str, null);
    }

    public PPYSocialQuery(String str, QueryDelegate queryDelegate) {
        this.fz = false;
        this.eK = new JSONObject();
        this.fy = queryDelegate;
        put("api", str);
    }

    public final void cancel() {
        this.fz = true;
        this.fy = null;
    }

    public final String getAPI() {
        if (this.eK == null) {
            return null;
        }
        return this.eK.optString("api");
    }

    public final String getPayloadString() {
        return this.eK.toString();
    }

    public final QueryDelegate getQueryDelegate() {
        return this.fy;
    }

    public final boolean isCanceled() {
        return this.fz;
    }

    public final PPYSocialQuery put(String str, int i) {
        try {
            this.eK.put(str, i);
        } catch (JSONException e) {
            X.w("failed to put int value %d for key %s: %s", Integer.valueOf(i), str, e);
        }
        return this;
    }

    public final PPYSocialQuery put(String str, String str2) {
        if (str2 == null) {
            return this;
        }
        try {
            this.eK.put(str, str2);
        } catch (JSONException e) {
            X.w("failed to put string value %s for key %s: %s", str2, str, e);
        }
        return this;
    }

    public final PPYSocialQuery put(String str, byte[] bArr) {
        if (bArr == null) {
            return this;
        }
        try {
            this.eK.put(str, aZ.a.encode(bArr));
        } catch (JSONException e) {
            X.w("failed to put bytes value %s for key %s: %s", bArr, str, e);
        }
        return this;
    }

    public final PPYSocialQuery setQueryDelegate(QueryDelegate queryDelegate) {
        this.fy = queryDelegate;
        return this;
    }
}
