package com.papaya.si;

import android.graphics.drawable.Drawable;

public final class S extends D {
    public int cU;
    public int dk;
    public String dl;
    public boolean dm;

    public final Drawable getDefaultDrawable() {
        return C0042c.getBitmapDrawable("avatar_unknown");
    }

    public final CharSequence getSubtitle() {
        return C0042c.getSession().getPrivateChatWhiteList().contains(Integer.valueOf(this.cU)) ? C0042c.getString("allow") : C0042c.getString("disallow");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dl;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
