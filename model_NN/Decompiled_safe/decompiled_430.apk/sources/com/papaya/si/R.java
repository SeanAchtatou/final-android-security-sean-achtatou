package com.papaya.si;

public final class R extends E<Q> {
    public R() {
        setName(C0042c.getString("chatroom"));
        setReserveGroupHeader(true);
    }

    public final Q findByRoomID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.co.size()) {
                return null;
            }
            Q q = (Q) this.co.get(i3);
            if (q.dd == i) {
                return q;
            }
            i2 = i3 + 1;
        }
    }
}
