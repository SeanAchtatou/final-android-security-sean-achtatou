package com.papaya.si;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.purchase.PPYPayment;
import com.papaya.social.PPYSNSRegion;
import com.papaya.social.PPYSession;
import com.papaya.social.PPYSocial;
import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialQuery;
import com.papaya.social.SocialRegistrationActivity;
import com.papaya.view.LazyImageView;
import com.papaya.view.OverlayMessage;
import com.papaya.view.OverlayTitleMessageView;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;

public final class aA {
    private static final HashMap<Integer, a> fO;
    private static aA fP = new aA();
    private ArrayList<PPYSocial.Delegate> fJ = new ArrayList<>(4);
    private String fQ;
    private C0037bh<PPYSocial.Delegate> fR = new C0037bh<>(4);
    private aB fS;
    private C0014al fT;
    private C0026ax fU;
    private HashMap<String, aB> fV = new HashMap<>(4);
    private boolean fW = true;
    private PPYSocial.Config fX;

    static class a {
        public final String gh;
        public final String gi;
        public boolean gj;

        /* synthetic */ a(String str) {
            this(str, (byte) 0);
        }

        private a(String str, byte b) {
            this.gj = true;
            this.gh = str;
            this.gi = null;
            this.gj = false;
        }

        /* synthetic */ a(String str, String str2) {
            this(str, str2, (byte) 0);
        }

        private a(String str, String str2, byte b) {
            this.gj = true;
            this.gh = str;
            this.gi = str2;
        }
    }

    static {
        HashMap<Integer, a> hashMap = new HashMap<>(10);
        fO = hashMap;
        hashMap.put(0, new a("static_home", "home"));
        fO.put(1, new a("static_friends", "home"));
        fO.put(2, new a("static_photos", (String) null));
        fO.put(3, new a("static_mail", (String) null));
        fO.put(4, new a("static_newavatarnavi", (String) null));
        fO.put(5, new a("static_leaderboard", (String) null));
        fO.put(6, new a("static_localleaderboard"));
        fO.put(7, new a("static_invite", (String) null));
        fO.put(8, new a("static_achievement", (String) null));
        fO.put(9, new a("static_mycircles", "circle"));
        fO.put(10, new a("static_mylocation", "location"));
        fO.put(11, new a("static_challenge_list", (String) null));
        fO.put(12, new a("static_moreapps", (String) null));
    }

    private aA() {
    }

    private String computeScoreSignature(String str, int i) {
        return aZ.md5(C0030ba.format("%s_%s_%d", "papaya social 1.7", C0030ba.nullAsEmpty(str), Integer.valueOf(i)));
    }

    private PPYSocial.Config copyConfig(Context context, PPYSocial.Config config) {
        final int i = 3;
        final String apiKey = config.getApiKey();
        final String chinaApiKey = config.getChinaApiKey();
        final PPYSNSRegion sNSRegion = config.getSNSRegion();
        final String preferredLanguage = config.getPreferredLanguage();
        final String androidMapsAPIKey = config.getAndroidMapsAPIKey();
        if (config.timeToShowRegistration() <= 3) {
            i = config.timeToShowRegistration();
        }
        final int billingChannels = config.getBillingChannels();
        final boolean isLeaderboardVisible = config.isLeaderboardVisible();
        final boolean isSkipEnabledInRegistration = config.isSkipEnabledInRegistration();
        return new PPYSocial.Config() {
            public final String getAndroidMapsAPIKey() {
                return androidMapsAPIKey;
            }

            public final String getApiKey() {
                return apiKey;
            }

            public final int getBillingChannels() {
                return billingChannels;
            }

            public final String getChinaApiKey() {
                return chinaApiKey;
            }

            public final String getPreferredLanguage() {
                return preferredLanguage;
            }

            public final PPYSNSRegion getSNSRegion() {
                return sNSRegion;
            }

            public final boolean isLeaderboardVisible() {
                return isLeaderboardVisible;
            }

            public final boolean isSkipEnabledInRegistration() {
                return isSkipEnabledInRegistration;
            }

            public final int timeToShowRegistration() {
                return i;
            }
        };
    }

    private void fireAccountChanged(int i, int i2) {
        ArrayList arrayList = new ArrayList(this.fJ);
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i3)).onAccountChanged(i, i2);
            } catch (Exception e) {
                X.w(e, "Failed to invoke onAccountChanged", new Object[0]);
            }
        }
        arrayList.clear();
        C0037bh bhVar = new C0037bh(this.fR.size());
        bhVar.addAll(this.fR);
        int size = bhVar.size();
        for (int i4 = 0; i4 < size; i4++) {
            try {
                PPYSocial.Delegate delegate = (PPYSocial.Delegate) bhVar.get(i4);
                if (delegate != null) {
                    delegate.onAccountChanged(i, i2);
                }
            } catch (Exception e2) {
                X.w(e2, "Failed to invoke onAccountChanged on weak ref", new Object[0]);
            }
        }
    }

    private void fireScoreUpdated() {
        ArrayList arrayList = new ArrayList(this.fJ);
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i)).onScoreUpdated();
            } catch (Exception e) {
                X.w(e, "Failed to invoke onScoreUpdated", new Object[0]);
            }
        }
        arrayList.clear();
        C0037bh bhVar = new C0037bh(this.fR.size());
        bhVar.addAll(this.fR);
        int size = bhVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            try {
                PPYSocial.Delegate delegate = (PPYSocial.Delegate) bhVar.get(i2);
                if (delegate != null) {
                    delegate.onScoreUpdated();
                }
            } catch (Exception e2) {
                X.w(e2, "Failed to invoke onScoreUpdated on weak ref", new Object[0]);
            }
        }
    }

    private void fireSessionUpdated() {
        ArrayList arrayList = new ArrayList(this.fJ);
        X.d("fireSessionUpdated: " + arrayList, new Object[0]);
        for (int i = 0; i < arrayList.size(); i++) {
            try {
                ((PPYSocial.Delegate) arrayList.get(i)).onSessionUpdated();
            } catch (Exception e) {
                X.w(e, "Failed to invoke onSessionUpdated", new Object[0]);
            }
        }
        arrayList.clear();
        C0037bh bhVar = new C0037bh(this.fR.size());
        bhVar.addAll(this.fR);
        int size = bhVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            try {
                PPYSocial.Delegate delegate = (PPYSocial.Delegate) bhVar.get(i2);
                if (delegate != null) {
                    delegate.onSessionUpdated();
                }
            } catch (Exception e2) {
                X.w(e2, "Failed to invoke onSessionUpdated on weak ref", new Object[0]);
            }
        }
    }

    public static aA getInstance() {
        return fP;
    }

    private void validateIntegration(Context context) {
        String[] validatePermissions = validatePermissions(context, "android.permission.INTERNET", "android.permission.GET_ACCOUNTS", "android.permission.READ_PHONE_STATE");
        if (validatePermissions.length > 0) {
            StringBuilder sb = new StringBuilder("Invalid Social SDK integration: required permissions are missing in AndroidManifest.xml");
            for (String append : validatePermissions) {
                sb.append("\n\t").append(append);
            }
            String sb2 = sb.toString();
            X.e(sb2, new Object[0]);
            Toast.makeText(context, sb2, 1).show();
        }
    }

    private String[] validatePermissions(Context context, String... strArr) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (context.checkCallingOrSelfPermission(str) != 0) {
                arrayList.add(str);
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public final synchronized void addDelegate(PPYSocial.Delegate delegate, boolean z) {
        if (z) {
            if (!this.fR.contains(delegate)) {
                this.fR.add(delegate);
            }
        } else if (!this.fJ.contains(delegate)) {
            this.fJ.add(delegate);
        }
    }

    public final void addPayment(Context context, PPYPayment pPYPayment) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            new aG(context, pPYPayment).show();
        }
    }

    public final OverlayMessage createOverlayMessage(String str, String str2, View.OnClickListener onClickListener) {
        if (C0042c.getApplicationContext() == null) {
            return null;
        }
        Application applicationContext = C0042c.getApplicationContext();
        OverlayTitleMessageView overlayTitleMessageView = new OverlayTitleMessageView(applicationContext, new LazyImageView(applicationContext));
        ((LazyImageView) overlayTitleMessageView.getImageView()).setImageUrl(str);
        overlayTitleMessageView.getTextView().setText(str2);
        OverlayMessage overlayMessage = new OverlayMessage(applicationContext, overlayTitleMessageView);
        overlayMessage.setOnClickListener(onClickListener);
        return overlayMessage;
    }

    public final void dispose() {
        C0042c.quit();
    }

    public final C0026ax getAchievementDatabase() {
        return this.fU;
    }

    public final void getAchievementList(PPYAchievementDelegate pPYAchievementDelegate) {
        new aE(pPYAchievementDelegate).getAchievementList();
    }

    public final String getApiKey() {
        return this.fQ;
    }

    public final String getAvatarUrlString(int i) {
        return C0061v.bi + "getavatarhead?uid=" + i;
    }

    public final aB getScoreDatabase() {
        return this.fS;
    }

    public final aB getScoreDatabase(String str) {
        if (C0030ba.isEmpty(str)) {
            return this.fS;
        }
        aB aBVar = this.fV.get(str);
        if (aBVar != null) {
            return aBVar;
        }
        aB aBVar2 = new aB("com.papaya.social.score" + aZ.md5(str));
        this.fV.put(str, aBVar2);
        return aBVar2;
    }

    public final PPYSocial.Config getSocialConfig() {
        return this.fX;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void handleResponse(JSONArray jSONArray) {
        if (jSONArray != null) {
            aC instance = aC.getInstance();
            try {
                int optInt = jSONArray.optInt(0);
                switch (optInt) {
                    case -3:
                        File file = new File(C0042c.getApplicationContext().getFilesDir(), C0028az.fL);
                        String optString = jSONArray.optString(1, null);
                        if (C0030ba.isEmpty(optString)) {
                            aZ.deleteFile(file);
                            return;
                        } else {
                            aZ.writeBytesToFile(file, C0030ba.getBytes(optString));
                            return;
                        }
                    case -2:
                        this.fW = false;
                        return;
                    case -1:
                        C0017ao.getInstance().updatePages(jSONArray);
                        return;
                    case 0:
                        int uid = instance.getUID();
                        C0042c.getSession().logout();
                        C0042c.A.close();
                        instance.setUID(jSONArray.optInt(1));
                        instance.setSessionKey(jSONArray.optString(2));
                        instance.setExpirationDate((System.currentTimeMillis() / 1000) + ((long) jSONArray.optInt(3)));
                        instance.setNickname(jSONArray.optString(4));
                        instance.setAppID(jSONArray.optInt(6));
                        instance.setDev(jSONArray.optInt(7, 0) > 0);
                        instance.gr = jSONArray.optString(8, null);
                        instance.setSessionSecret(jSONArray.optString(9, null));
                        instance.setSessionReceipt(jSONArray.optString(10, null));
                        instance.save();
                        if (uid > 0 && instance.getUID() != uid) {
                            fireAccountChanged(uid, instance.getUID());
                        }
                        C0042c.getSession().fireDataStateChanged();
                        fireSessionUpdated();
                        return;
                    case 1:
                        C0042c.getSession().reupdateFriendList(jSONArray);
                        return;
                    case 2:
                        return;
                    case 3:
                        instance.setNickname(jSONArray.optString(1, instance.getNickname()));
                        fireSessionUpdated();
                        return;
                    case 4:
                        if (instance.isConnected()) {
                            JSONArray optJSONArray = jSONArray.optJSONArray(1);
                            for (int i = 0; i < optJSONArray.length(); i += 2) {
                                instance.setScore(optJSONArray.optString(i, null), optJSONArray.optInt(i + 1, 0));
                            }
                            fireScoreUpdated();
                            return;
                        }
                        return;
                    case 5:
                        OverlayMessage.showTitleMessage(jSONArray.optString(1), jSONArray.optString(2), jSONArray.optString(3), jSONArray.optString(4), jSONArray.optInt(5, OverlayMessage.DEFAULT_TIMEOUT), C0036bg.toGravity(jSONArray.optString(6, "bottom"), 80), jSONArray.optInt(7, 100));
                        return;
                    case 6:
                        OverlayMessage.showMessage(jSONArray.optString(1), jSONArray.optString(2), jSONArray.optString(3), jSONArray.optInt(4, OverlayMessage.DEFAULT_TIMEOUT), C0036bg.toGravity(jSONArray.optString(5, "bottom"), 80), jSONArray.optInt(6, 100));
                        return;
                    case 71:
                        int optInt2 = jSONArray.optInt(1, 0);
                        String[] split = jSONArray.optString(2).split("-");
                        C0030ba.intValue(split[0]);
                        int intValue = C0030ba.intValue(split[1]);
                        if (optInt2 == 1) {
                            bu.onImageUploaded(intValue, jSONArray.optInt(3));
                            return;
                        } else {
                            bu.onImageUploadFailed(intValue);
                            return;
                        }
                    default:
                        X.i("unknown payload cmd, %s", Integer.valueOf(optInt));
                        return;
                }
            } catch (Exception e) {
                X.e(e, "Failed to process payload %s", jSONArray);
            }
            X.e(e, "Failed to process payload %s", jSONArray);
        }
    }

    public final void initialize(Context context, PPYSocial.Config config) {
        boolean z;
        try {
            if (C0042c.getApplicationContext() != context.getApplicationContext()) {
                Context applicationContext = context.getApplicationContext();
                this.fX = copyConfig(applicationContext, config);
                C0042c.initialize(applicationContext);
                C0048i.trackPageView("/sdk_init");
                C0048i.trackEvent("sdk", "init", applicationContext.getPackageName(), 0);
                applicationContext.getResources().getDisplayMetrics();
                validateIntegration(context);
                try {
                    ProviderInfo[] providerInfoArr = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 8).providers;
                    if (providerInfoArr != null) {
                        int length = providerInfoArr.length;
                        int i = 0;
                        while (true) {
                            if (i < length) {
                                ProviderInfo providerInfo = providerInfoArr[i];
                                if (providerInfo != null && "com.papaya.social.PPYSocialContentProvider".equals(providerInfo.name)) {
                                    bE.ny = "content://" + providerInfo.authority + "/cache/";
                                    z = true;
                                    break;
                                }
                                i++;
                            } else {
                                break;
                            }
                        }
                    }
                    z = false;
                } catch (Exception e) {
                    X.e("Failed to read the configuration of PPYSocialContentProvider: %s", e);
                    z = false;
                }
                if (!z) {
                    X.e("Failed to find the configuration of PPYSocialContentProvider", new Object[0]);
                }
                this.fQ = this.fX.getSNSRegion() == PPYSNSRegion.CHINA ? this.fX.getChinaApiKey() : this.fX.getApiKey();
                C0018ap.getInstance().init(this.fX);
                aC.getInstance().setApiKey(this.fQ);
                this.fS = new aB("com.papaya.social.score");
                this.fT = C0015am.getInstance().settingDB();
                this.fU = new C0026ax("com.papaya.social.achievement");
                aC.getInstance().setDev(false);
                if (this.fQ.startsWith("dev-")) {
                    aC.getInstance().setDev(true);
                    C0036bg.showToast("Enabled dev mode of Papaya Social SDK", 1);
                }
                int kvInt = this.fT.kvInt("init_times", 0);
                this.fT.kvSaveInt("init_times", kvInt + 1, -1);
                if (kvInt < this.fX.timeToShowRegistration()) {
                    C0002a.getWebCache().tryLogin();
                } else if (this.fT.kvInt("welcome_page", 0) > 0 || aJ.hasCredentialFile()) {
                    C0002a.getWebCache().tryLogin();
                } else {
                    showWelcome(context);
                }
                X.i("Papaya Social SDK (%s/%s) is initialized. LBS support (%b)", C0028az.fM, "173", false);
            }
        } catch (Exception e2) {
            Toast.makeText(context, "Failed to initialize Papaya Social", 0).show();
            X.e(e2, "Error occurred while initializing Papaya Social SDK", new Object[0]);
        }
    }

    public final boolean isForceShowWelcome() {
        return !this.fW && !PPYSession.getInstance().isConnected() && !C0002a.getWebCache().isLoggingin();
    }

    public final boolean isInitialized() {
        return this.fQ != null;
    }

    public final void loadAchievement(int i, PPYAchievementDelegate pPYAchievementDelegate) {
        new aE(pPYAchievementDelegate).loadAchievement(i);
    }

    public final void postNewsfeed(String str, String str2) {
        postNewsfeed(str, str2, 0);
    }

    public final void postNewsfeed(String str, String str2, int i) {
        if (!C0030ba.isEmpty(str)) {
            StringBuilder append = new StringBuilder("json_postnewsfeed?message=").append(Uri.encode(str));
            if (str2 != null) {
                append.append("&uri=").append(Uri.encode(str2)).append("&type=").append(i);
            }
            bA bAVar = new bA(C0040bk.createURL(append.toString()), false);
            bAVar.setDispatchable(true);
            bAVar.start(true);
        }
    }

    public final void recommendMyApp(String str) {
        new aH(C0042c.getApplicationContext(), str).show();
    }

    public final synchronized void removeDelegate(PPYSocial.Delegate delegate) {
        this.fJ.remove(delegate);
        this.fR.remove(delegate);
    }

    public final PPYSocialQuery sendChallenge(PPYSocialChallengeRecord pPYSocialChallengeRecord, PPYSocialQuery.QueryDelegate queryDelegate) {
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_send_challenge", queryDelegate);
        pPYSocialQuery.put("cid", pPYSocialChallengeRecord.getChallengeDefinitionID()).put("rid", pPYSocialChallengeRecord.getReceiverUserID()).put("msg", pPYSocialChallengeRecord.getMessage());
        if (pPYSocialChallengeRecord.getPayloadType() == 0) {
            pPYSocialQuery.put("payload", pPYSocialChallengeRecord.getPayload()).put("type", pPYSocialChallengeRecord.getPayloadType());
        } else {
            pPYSocialQuery.put("payload", pPYSocialChallengeRecord.getPayloadBinary()).put("type", pPYSocialChallengeRecord.getPayloadType());
        }
        submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }

    public final void setScore(int i, String str) {
        StringBuilder sb = new StringBuilder("json_updatescore?update=1&value=");
        sb.append(i);
        if (str != null) {
            sb.append("&name=").append(str);
        }
        sb.append("&sig=").append(Uri.encode(computeScoreSignature(str, i)));
        bA bAVar = new bA(C0040bk.createURL(sb.toString()), false);
        bAVar.setDispatchable(true);
        bAVar.start(true);
        getScoreDatabase(str).addScore(i);
        aC.getInstance().setScore(str, i);
    }

    public final void show(Context context, int i) {
        if (i == 10) {
            showLBS(context);
            return;
        }
        a aVar = fO.get(Integer.valueOf(i));
        showWebActivity(context, aVar.gh, aVar.gi, aVar.gj);
    }

    public final void showChat(Context context) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            C0029b.openPRIALink(context, "friends://", "chat");
        }
    }

    public final void showHome(Context context, int i) {
        C0029b.openHome(C0036bg.contextAsActivity(context), i);
    }

    public final void showLBS(Context context) {
        if (isForceShowWelcome()) {
            showWelcome(context);
        } else {
            C0036bg.showToast("LBS is not enabled", 1);
        }
    }

    public final void showLeaderboard(Context context, String str, boolean z) {
        a aVar = fO.get(Integer.valueOf(z ? 5 : 6));
        String str2 = aVar.gh;
        if (str != null) {
            str2 = str2 + "?name=" + str;
        }
        showWebActivity(context, str2, aVar.gi, aVar.gj);
    }

    public final void showURL(Context context, String str) {
        try {
            if (!C0030ba.isNotEmpty(str)) {
                return;
            }
            if (str.startsWith("chat://")) {
                showChat(context);
            } else if (str.startsWith("lbs://")) {
                showLBS(context);
            } else if (str.startsWith("login://")) {
                showWelcome(context);
            } else {
                showWebActivity(context, str, null, true);
            }
        } catch (Exception e) {
            X.w(e, "failed to showURL: " + str, new Object[0]);
        }
    }

    public final void showWebActivity(Context context, String str, String str2, boolean z) {
        try {
            if (isForceShowWelcome()) {
                showWelcome(context);
            } else {
                C0029b.openPRIALink(context, str, str2, z, null);
            }
        } catch (Exception e) {
            X.e(e, "Failed to show web activity", new Object[0]);
        }
    }

    public final void showWelcome(Context context) {
        C0029b.startActivity(context, new Intent(C0042c.getApplicationContext(), SocialRegistrationActivity.class));
    }

    public final aK submitQuery(PPYSocialQuery pPYSocialQuery) {
        aK aKVar = new aK(pPYSocialQuery);
        aKVar.start(false);
        return aKVar;
    }

    public final PPYSocialQuery updateChallengeStatus(int i, int i2, PPYSocialQuery.QueryDelegate queryDelegate) {
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_update_challenge_status", queryDelegate);
        pPYSocialQuery.put("rid", i).put("status", i2);
        submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }

    public final void updateScore(int i, String str) {
        StringBuilder sb = new StringBuilder("json_updatescore?update=0&value=");
        sb.append(i);
        if (str != null) {
            sb.append("&name=").append(str);
        }
        sb.append("&sig=").append(Uri.encode(computeScoreSignature(str, i)));
        bA bAVar = new bA(C0040bk.createURL(sb.toString()), false);
        bAVar.setDispatchable(true);
        bAVar.start(true);
        aC instance = aC.getInstance();
        instance.setScore(str, instance.getScore(str) + i);
    }
}
