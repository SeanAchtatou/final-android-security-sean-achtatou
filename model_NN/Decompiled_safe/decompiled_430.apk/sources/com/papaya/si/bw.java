package com.papaya.si;

import org.json.JSONArray;
import org.json.JSONObject;

public final class bw extends bA {
    public JSONObject mX;

    public bw(JSONObject jSONObject) {
        this.mX = jSONObject;
        this.url = C0040bk.createURL(jSONObject.optString("url"));
        this.nj = false;
        JSONArray optJSONArray = jSONObject.optJSONArray("data");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONArray optJSONArray2 = optJSONArray.optJSONArray(i);
                if (optJSONArray2 != null) {
                    addPostParam(optJSONArray2.optString(1), optJSONArray2.optString(2), optJSONArray2.optInt(0, 0));
                }
            }
        }
    }

    public final String getID() {
        return this.mX.optString("id", "");
    }
}
