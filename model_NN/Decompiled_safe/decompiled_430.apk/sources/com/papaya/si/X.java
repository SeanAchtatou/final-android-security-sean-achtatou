package com.papaya.si;

import android.util.Log;
import com.papaya.chat.FriendsActivity;

public class X implements aS<C0012aj> {
    private /* synthetic */ FriendsActivity dw;

    private X() {
    }

    public X(FriendsActivity friendsActivity) {
        this.dw = friendsActivity;
    }

    public static void assertTrue(boolean z) {
        if (!z) {
            e(null, "Assertion failed", new Object[0]);
        }
    }

    public static void d(String str, Object... objArr) {
    }

    public static void d(Throwable th, String str, Object... objArr) {
    }

    public static void dw(String str, Object... objArr) {
    }

    public static void dw(Throwable th, String str, Object... objArr) {
    }

    public static void e(String str, Object... objArr) {
        e(null, str, objArr);
    }

    public static void e(Throwable th, String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            try {
                if (objArr.length > 0) {
                    str2 = C0030ba.format(str, objArr);
                    log(6, str2, th);
                }
            } catch (Exception e) {
                Log.e("PPYSocial", "Failed to e: " + e.getMessage());
                return;
            }
        }
        str2 = str;
        log(6, str2, th);
    }

    public static void i(String str, Object... objArr) {
        i(null, str, objArr);
    }

    public static void i(Throwable th, String str, Object... objArr) {
        if (objArr != null) {
            try {
                if (objArr.length != 0) {
                    log(4, C0030ba.format(str, objArr), th);
                    return;
                }
            } catch (Exception e) {
                Log.e("PPYSocial", "Failed to i: " + e.getMessage());
                return;
            }
        }
        log(4, str, th);
    }

    public static void log(int i, String str, Throwable th) {
        if (th != null) {
            try {
                Log.println(i, "PPYSocial", C0030ba.releaseStringBuilder(C0030ba.acquireStringBuilder(0).append(str).append(", exception:\n\t").append(i >= 6 ? Log.getStackTraceString(th) : C0030ba.toString(th))));
            } catch (Exception e) {
                Log.e("PPYSocial", "Failed to log: " + e.getMessage());
            }
        } else {
            Log.println(i, "PPYSocial", C0030ba.releaseStringBuilder(C0030ba.acquireStringBuilder(0).append(str)));
        }
    }

    public static void missedFeature(String str) {
        e(null, "%s is not supported in Social SDK", new Object[0]);
    }

    private boolean onDataStateChanged(C0012aj ajVar) {
        this.dw.dp.refreshWithCard(ajVar);
        this.dw.dr.setText(D.getStateString(ajVar.getState()));
        return false;
    }

    public static void v(String str, Object... objArr) {
    }

    public static void v(Throwable th, String str, Object... objArr) {
    }

    public static void w(String str, Object... objArr) {
        w(null, str, objArr);
    }

    public static void w(Throwable th, String str, Object... objArr) {
        if (objArr != null) {
            try {
                if (objArr.length != 0) {
                    log(5, C0030ba.format(str, objArr), th);
                    return;
                }
            } catch (Exception e) {
                Log.e("PPYSocial", "Failed to w: " + e.getMessage());
                return;
            }
        }
        log(5, str, th);
    }

    public static void warnIncomplete() {
        Log.println(5, "PPYSocial", "incomplete implementation");
    }

    public final /* bridge */ /* synthetic */ boolean onDataStateChanged(aU aUVar) {
        C0012aj ajVar = (C0012aj) aUVar;
        this.dw.dp.refreshWithCard(ajVar);
        this.dw.dr.setText(D.getStateString(ajVar.getState()));
        return false;
    }
}
