package com.papaya.si;

import java.util.TreeMap;

/* renamed from: com.papaya.si.bc  reason: case insensitive filesystem */
public final class C0032bc<K, V> {
    private TreeMap<K, V> hA;
    private int hz = 10;

    public C0032bc(int i) {
        int i2 = 10;
        this.hz = i > 0 ? i : i2;
        this.hA = new TreeMap<>();
    }

    public final void clear() {
        this.hA.clear();
    }

    public final synchronized V get(K k) {
        return this.hA.get(k);
    }

    public final int getMaxSize() {
        return this.hz;
    }

    public final synchronized void put(K k, V v) {
        this.hA.put(k, v);
        if (this.hA.size() > this.hz) {
            this.hA.remove(this.hA.firstKey());
        }
    }

    public final synchronized V remove(K k) {
        return this.hA.remove(k);
    }

    public final synchronized int size() {
        return this.hA.size();
    }
}
