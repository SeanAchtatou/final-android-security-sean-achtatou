package com.papaya.si;

import java.lang.ref.WeakReference;

/* renamed from: com.papaya.si.bj  reason: case insensitive filesystem */
public class C0039bj<T> {
    private WeakReference<T> hY;

    public T getDelegate() {
        if (this.hY == null) {
            return null;
        }
        return this.hY.get();
    }

    public void setDelegate(T t) {
        if (t == null) {
            this.hY = null;
        } else {
            this.hY = new WeakReference<>(t);
        }
    }
}
