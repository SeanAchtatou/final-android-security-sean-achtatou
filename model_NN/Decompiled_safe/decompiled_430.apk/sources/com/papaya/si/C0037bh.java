package com.papaya.si;

import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: com.papaya.si.bh  reason: case insensitive filesystem */
public final class C0037bh<E> extends AbstractList<E> {
    private static final WeakReference[] hQ = new WeakReference[0];
    /* access modifiers changed from: private */
    public int hR;
    transient WeakReference[] hS;

    /* renamed from: com.papaya.si.bh$a */
    class a implements Iterator<E> {
        private int hT;
        private int hU;
        private int hV;

        /* synthetic */ a(C0037bh bhVar) {
            this((byte) 0);
        }

        private a(byte b) {
            this.hT = C0037bh.this.hR;
            this.hU = -1;
            this.hV = C0037bh.this.modCount;
        }

        public final boolean hasNext() {
            return this.hT != 0;
        }

        public final E next() {
            C0037bh bhVar = C0037bh.this;
            int i = this.hT;
            if (bhVar.modCount != this.hV) {
                throw new ConcurrentModificationException();
            } else if (i == 0) {
                throw new NoSuchElementException();
            } else {
                this.hT = i - 1;
                int access$100 = bhVar.hR - i;
                this.hU = access$100;
                return bhVar.get(access$100);
            }
        }

        public final void remove() {
            WeakReference[] weakReferenceArr = C0037bh.this.hS;
            int i = this.hU;
            if (C0037bh.this.modCount != this.hV) {
                throw new ConcurrentModificationException();
            } else if (i < 0) {
                throw new IllegalStateException();
            } else {
                System.arraycopy(weakReferenceArr, i + 1, weakReferenceArr, i, this.hT);
                weakReferenceArr[C0037bh.access$106(C0037bh.this)] = null;
                this.hU = -1;
                this.hV = C0037bh.access$504(C0037bh.this);
            }
        }
    }

    public C0037bh() {
        this.hS = hQ;
    }

    public C0037bh(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        this.hS = i == 0 ? hQ : new WeakReference[i];
    }

    static /* synthetic */ int access$106(C0037bh bhVar) {
        int i = bhVar.hR - 1;
        bhVar.hR = i;
        return i;
    }

    static /* synthetic */ int access$504(C0037bh bhVar) {
        int i = bhVar.modCount + 1;
        bhVar.modCount = i;
        return i;
    }

    private static int newCapacity(int i) {
        return (i < 6 ? 12 : i >> 1) + i;
    }

    private static void throwIndexOutOfBoundsException(int i, int i2) {
        throw new IndexOutOfBoundsException("Invalid index " + i + ", size is " + i2);
    }

    public final void add(int i, E e) {
        WeakReference[] weakReferenceArr = this.hS;
        int i2 = this.hR;
        if (i > i2) {
            throwIndexOutOfBoundsException(i, i2);
        }
        if (i2 < weakReferenceArr.length) {
            System.arraycopy(weakReferenceArr, i, weakReferenceArr, i + 1, i2 - i);
        } else {
            WeakReference[] weakReferenceArr2 = new WeakReference[newCapacity(i2)];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, i);
            System.arraycopy(weakReferenceArr, i, weakReferenceArr2, i + 1, i2 - i);
            this.hS = weakReferenceArr2;
            weakReferenceArr = weakReferenceArr2;
        }
        weakReferenceArr[i] = new WeakReference(e);
        this.hR = i2 + 1;
        this.modCount++;
    }

    public final boolean add(E e) {
        WeakReference[] weakReferenceArr = this.hS;
        int i = this.hR;
        if (i == weakReferenceArr.length) {
            WeakReference[] weakReferenceArr2 = new WeakReference[((i < 6 ? 12 : i >> 1) + i)];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, i);
            this.hS = weakReferenceArr2;
            weakReferenceArr = weakReferenceArr2;
        }
        weakReferenceArr[i] = new WeakReference(e);
        this.hR = i + 1;
        this.modCount++;
        return true;
    }

    public final void clear() {
        if (this.hR != 0) {
            Arrays.fill(this.hS, 0, this.hR, (Object) null);
            this.hR = 0;
            this.modCount++;
        }
    }

    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    public final void ensureCapacity(int i) {
        WeakReference[] weakReferenceArr = this.hS;
        if (weakReferenceArr.length < i) {
            WeakReference[] weakReferenceArr2 = new WeakReference[i];
            System.arraycopy(weakReferenceArr, 0, weakReferenceArr2, 0, this.hR);
            this.hS = weakReferenceArr2;
            this.modCount++;
        }
    }

    public final E get(int i) {
        if (i >= this.hR) {
            throwIndexOutOfBoundsException(i, this.hR);
        }
        WeakReference weakReference = this.hS[i];
        if (weakReference == null) {
            return null;
        }
        return weakReference.get();
    }

    public final int indexOf(Object obj) {
        WeakReference[] weakReferenceArr = this.hS;
        int i = this.hR;
        for (int i2 = 0; i2 < i; i2++) {
            WeakReference weakReference = weakReferenceArr[i2];
            if (obj != null) {
                if (weakReference != null && obj.equals(weakReference.get())) {
                    return i2;
                }
            } else if (weakReference != null && weakReference.get() == null) {
                return i2;
            }
        }
        return -1;
    }

    public final boolean isEmpty() {
        trimGarbage();
        return this.hR == 0;
    }

    public final Iterator<E> iterator() {
        return new a(this);
    }

    public final E remove(int i) {
        WeakReference[] weakReferenceArr = this.hS;
        int i2 = this.hR;
        if (i >= i2) {
            throwIndexOutOfBoundsException(i, i2);
        }
        E e = get(i);
        int i3 = i2 - 1;
        System.arraycopy(weakReferenceArr, i + 1, weakReferenceArr, i, i3 - i);
        weakReferenceArr[i3] = null;
        this.hR = i3;
        this.modCount++;
        return e;
    }

    public final boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf == -1) {
            return false;
        }
        remove(indexOf);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        WeakReference[] weakReferenceArr = this.hS;
        int i3 = this.hR;
        if (i >= i3) {
            throw new IndexOutOfBoundsException("fromIndex " + i + " >= size " + this.hR);
        } else if (i2 > i3) {
            throw new IndexOutOfBoundsException("toIndex " + i2 + " > size " + this.hR);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("fromIndex " + i + " > toIndex " + i2);
        } else {
            System.arraycopy(weakReferenceArr, i2, weakReferenceArr, i, i3 - i2);
            int i4 = i2 - i;
            Arrays.fill(weakReferenceArr, i3 - i4, i3, (Object) null);
            this.hR = i3 - i4;
            this.modCount++;
        }
    }

    public final E set(int i, E e) {
        WeakReference[] weakReferenceArr = this.hS;
        if (i >= this.hR) {
            throwIndexOutOfBoundsException(i, this.hR);
        }
        E e2 = get(i);
        weakReferenceArr[i] = new WeakReference(e);
        return e2;
    }

    public final int size() {
        trimGarbage();
        return this.hR;
    }

    public final void trimGarbage() {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (it.next() == null) {
                it.remove();
            }
        }
    }
}
