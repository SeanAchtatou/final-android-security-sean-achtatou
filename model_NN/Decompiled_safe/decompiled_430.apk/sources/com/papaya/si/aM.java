package com.papaya.si;

import java.util.HashMap;
import java.util.HashSet;

public final class aM implements aU {
    private aT gK = new aT(this);
    private HashMap<Integer, String> gL = new HashMap<>(20);
    private HashSet<Integer> gM = new HashSet<>(10);

    public final boolean contains(int i) {
        if (this.gL.containsKey(Integer.valueOf(i))) {
            return true;
        }
        get(i);
        return false;
    }

    public final void fireDataStateChanged() {
        this.gK.fireDataStateChanged();
    }

    public final String get(int i) {
        if (this.gL.containsKey(Integer.valueOf(i)) || this.gM.contains(Integer.valueOf(i))) {
            return this.gL.get(Integer.valueOf(i));
        }
        C0042c.A.send(602, Integer.valueOf(i));
        this.gM.add(Integer.valueOf(i));
        return null;
    }

    public final void put(int i, String str) {
        this.gL.put(Integer.valueOf(i), str);
        this.gM.remove(Integer.valueOf(i));
    }

    public final void registerMonitor(aS aSVar) {
        this.gK.registerMonitor(aSVar);
    }

    public final void unregisterMonitor(aS aSVar) {
        this.gK.unregisterMonitor(aSVar);
    }
}
