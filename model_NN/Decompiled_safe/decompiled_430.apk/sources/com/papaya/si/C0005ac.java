package com.papaya.si;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.ac  reason: case insensitive filesystem */
public final class C0005ac extends ArrayList<C0006ad> {
    public final void loadFromFile(String str) {
        clear();
        List list = (List) aZ.loadPotpStorage(str);
        if (list != null) {
            int i = 0;
            while (i < list.size()) {
                try {
                    C0006ad adVar = new C0006ad();
                    adVar.dL = C0030ba.sgetInt(list, i);
                    adVar.dK = C0030ba.sgetInt(list, i + 1);
                    adVar.dM = (String) C0030ba.sget(list, i + 2);
                    adVar.dN = (String) C0030ba.sget(list, i + 3);
                    add(adVar);
                    i += 4;
                } catch (Exception e) {
                    X.e(e, "Failed to load from " + str, new Object[0]);
                    return;
                }
            }
        }
    }

    public final void saveToFile(String str) {
        try {
            Vector vector = new Vector();
            for (int i = 0; i < size(); i++) {
                C0006ad adVar = (C0006ad) get(i);
                vector.add(Integer.valueOf(adVar.dL));
                vector.add(Integer.valueOf(adVar.dK));
                vector.add(adVar.dM);
                vector.add(adVar.dN);
            }
            C0041bl.write(str, C0041bl.dumps(vector));
        } catch (Exception e) {
            X.e(e, "Failed to save to " + str, new Object[0]);
        }
    }
}
