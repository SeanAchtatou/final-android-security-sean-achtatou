package com.papaya.si;

import com.papaya.social.BillingChannel;
import com.papaya.web.WebViewController;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public final class bL implements aR {
    private static bL oR = new bL();
    private static final ArrayList<WebViewController> oS = new ArrayList<>(8);
    private ArrayList<bp> oT = new ArrayList<>(8);
    private ArrayList<bp> oU = new ArrayList<>(8);

    private bL() {
    }

    public static int freeWebViews(int i, WebViewController webViewController) {
        int i2;
        if (i == 0) {
            return 0;
        }
        int i3 = i < 0 ? BillingChannel.ALL : i;
        synchronized (WebViewController.class) {
            int size = oS.size();
            if (size > 0) {
                ArrayList arrayList = new ArrayList();
                for (int i4 = 0; i4 < size; i4++) {
                    arrayList.add(Integer.valueOf(i4));
                }
                Collections.shuffle(arrayList);
                int i5 = 0;
                for (int i6 = 0; i6 < size && i5 < i3; i6++) {
                    WebViewController webViewController2 = oS.get(((Integer) arrayList.get(i6)).intValue());
                    if (webViewController2 != webViewController) {
                        i5 += webViewController2.forceFreeWebViews(i3 - i5, false);
                    }
                }
                if (i5 < i3 && webViewController != null) {
                    webViewController.forceFreeWebViews(i3 - i5, true);
                }
                i2 = i5;
            } else {
                i2 = 0;
            }
        }
        return i2;
    }

    public static bL getInstance() {
        return oR;
    }

    public final void clear() {
        try {
            freeWebViews();
            this.oU.clear();
        } catch (Exception e) {
            X.w(e, "Failed to clear webviews", new Object[0]);
        }
    }

    public final int findReuseWebViewIndex(WebViewController webViewController, URL url) {
        if (url != null) {
            String url2 = url.toString();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.oT.size()) {
                    break;
                }
                bp bpVar = this.oT.get(i2);
                if (bpVar.getLastController() == null || bpVar.getLastController() == webViewController) {
                    if (bpVar.isReusable() && !bpVar.isLoadFromString() && bpVar.getPapayaURL() != null && url2.equals(bpVar.getPapayaURL().toString())) {
                        return i2;
                    }
                } else if (bpVar.isGlobalReusable() && !bpVar.isLoadFromString() && bpVar.getPapayaURL() != null && url2.equals(bpVar.getPapayaURL().toString())) {
                    return i2;
                }
                i = i2 + 1;
            }
        }
        return -1;
    }

    public final void freeWebView(bp bpVar) {
        if (bpVar != null) {
            bpVar.setVisibility(4);
            bpVar.setController(null);
            bpVar.setDelegate(null);
            this.oU.remove(bpVar);
            if (bpVar.getParent() != null) {
                this.oT.add(bpVar);
                if (!bpVar.isReusable()) {
                    bpVar.getPapayaScript().clearSessionState();
                    return;
                }
                return;
            }
            bpVar.close();
        }
    }

    public final void freeWebViews() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.oT.size()) {
                bp bpVar = this.oT.get(i2);
                C0036bg.removeFromSuperView(bpVar);
                bpVar.noWarnCallJS("webdestroyed", "webdestroyed();");
                bpVar.close();
                i = i2 + 1;
            } else {
                this.oT.clear();
                return;
            }
        }
    }

    public final bp getWebView(WebViewController webViewController) {
        return getWebView(webViewController, null, null);
    }

    public final bp getWebView(WebViewController webViewController, URL url, aY<Boolean> aYVar) {
        C0036bg.assertMainThread();
        if (aYVar != null) {
            aYVar.ho = Boolean.FALSE;
        }
        if (this.oU.size() > 8) {
            freeWebViews(1, webViewController);
        }
        bp bpVar = null;
        int findReuseWebViewIndex = findReuseWebViewIndex(webViewController, url);
        if (findReuseWebViewIndex >= 0) {
            bpVar = this.oT.remove(findReuseWebViewIndex);
            if (aYVar != null) {
                aYVar.ho = Boolean.TRUE;
            }
        } else if (!this.oT.isEmpty() && this.oT.size() + this.oU.size() >= 8) {
            bpVar = this.oT.remove(0);
        }
        if (bpVar == null) {
            bpVar = new bp(C0042c.getApplicationContext());
        }
        this.oU.add(bpVar);
        webViewController.configWebView(bpVar);
        return bpVar;
    }

    public final synchronized void onControllerClosed(WebViewController webViewController) {
        oS.remove(webViewController);
    }

    public final synchronized void onControllerCreated(WebViewController webViewController) {
        oS.add(webViewController);
    }

    public final synchronized void startAllControllers() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < oS.size()) {
                oS.get(i2).openInitUrlIfPossible();
                i = i2 + 1;
            }
        }
    }
}
