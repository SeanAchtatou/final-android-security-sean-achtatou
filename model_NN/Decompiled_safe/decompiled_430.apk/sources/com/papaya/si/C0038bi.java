package com.papaya.si;

import java.util.HashMap;

/* renamed from: com.papaya.si.bi  reason: case insensitive filesystem */
public final class C0038bi<K, V> {
    private HashMap<K, C0037bh<V>> hX = new HashMap<>();

    public final C0037bh<V> getList(K k) {
        return this.hX.get(k);
    }

    public final boolean put(K k, V v) {
        C0037bh bhVar = this.hX.get(k);
        if (bhVar == null) {
            bhVar = new C0037bh();
            this.hX.put(k, bhVar);
        }
        if (bhVar.contains(v)) {
            return false;
        }
        bhVar.add(v);
        return true;
    }

    public final void remove(K k, V v) {
        C0037bh bhVar = this.hX.get(k);
        if (bhVar != null) {
            bhVar.remove(v);
        }
    }
}
