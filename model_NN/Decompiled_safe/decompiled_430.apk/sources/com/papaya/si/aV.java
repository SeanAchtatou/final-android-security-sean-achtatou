package com.papaya.si;

import android.os.Handler;
import android.os.Message;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public final class aV extends Handler {
    private final Thread hk = Thread.currentThread();

    public final <T> T callInHandlerThread(Callable<T> callable, T t) {
        T t2 = null;
        try {
            t2 = Thread.currentThread() != this.hk ? postCallable(callable).get() : callable.call();
        } catch (Throwable th) {
            X.e(th, "Error occurred in handler call thread", new Object[0]);
        }
        return t2 == null ? t : t2;
    }

    public final void dispatchMessage(Message message) {
        Runnable callback = message.getCallback();
        if (callback != null) {
            try {
                callback.run();
            } catch (Throwable th) {
                X.e(th, "Error occurred in handler thread, dispatchMessage", new Object[0]);
            }
        } else {
            handleMessage(message);
        }
    }

    public final void handleMessage(Message message) {
        super.handleMessage(message);
        X.e("unhandled message %s", message);
    }

    public final <T> Future<T> postCallable(Callable<T> callable) {
        FutureTask futureTask = new FutureTask(callable);
        post(futureTask);
        return futureTask;
    }

    public final void runInHandlerThread(Runnable runnable) {
        if (Thread.currentThread() != this.hk) {
            post(runnable);
            return;
        }
        try {
            runnable.run();
        } catch (Throwable th) {
            X.e(th, "Error occurred in handler run thread", new Object[0]);
        }
    }

    public final void runInHandlerThreadDelay(Runnable runnable) {
        post(runnable);
    }

    public final boolean testThread() {
        return this.hk == Thread.currentThread();
    }
}
