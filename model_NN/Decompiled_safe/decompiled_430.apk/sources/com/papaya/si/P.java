package com.papaya.si;

import com.papaya.base.EntryActivity;
import java.util.ArrayList;

public final class P extends ArrayList<O> {
    private int dc;

    public final boolean add(O o) {
        boolean add = super.add((Object) o);
        if (size() > 200) {
            remove(0);
        }
        return add;
    }

    public final int getUnread() {
        return this.dc;
    }

    public final void increaseUnread() {
        setUnread(this.dc + 1);
    }

    public final void removeAllMessages() {
        clear();
        this.dc = 0;
    }

    public final void setUnread(int i) {
        this.dc = i;
        EntryActivity.a.refreshUnread();
    }
}
