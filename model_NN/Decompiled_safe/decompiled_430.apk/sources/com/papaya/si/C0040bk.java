package com.papaya.si;

import android.net.Uri;
import com.papaya.si.aZ;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.papaya.si.bk  reason: case insensitive filesystem */
public final class C0040bk {
    private static final byte[] hZ = new byte[8];
    private static final byte[] ia = new byte[8];
    private static final aZ.b ib = new aZ.b("papaya social 1.5", hZ, 1);
    private static final aZ.b ic = new aZ.b("papaya social 1.5", ia, 1);
    private static final C0032bc<String, URL> ie = new C0032bc<>(100);

    static {
        for (int i = 0; i < 8; i++) {
            hZ[i] = 0;
            ia[i] = 0;
        }
    }

    private C0040bk() {
    }

    public static void addSignatureHeaders(URLConnection uRLConnection) {
        if (uRLConnection != null) {
            String str = Integer.toHexString(C0030ba.hv.nextInt()) + Long.toHexString(System.currentTimeMillis()) + Integer.toHexString(aC.getInstance().getUID());
            uRLConnection.setRequestProperty("nonce", str);
            uRLConnection.setRequestProperty("signature", aZ.md5(str + "_" + aA.getInstance().getApiKey() + "_" + "papayasocial"));
        }
    }

    public static void clear() {
        ie.clear();
    }

    public static String compositeUrl(String str, Map<String, Object> map) {
        StringBuilder append = C0030ba.acquireStringBuilder(str.length()).append(str);
        if (map != null && !map.isEmpty()) {
            if (!str.contains("?")) {
                append.append('?');
            } else {
                append.append('&');
            }
            int i = 0;
            Set<Map.Entry<String, Object>> entrySet = map.entrySet();
            for (Map.Entry next : entrySet) {
                int i2 = i + 1;
                append.append((String) next.getKey()).append('=').append(next.getValue() == null ? "" : Uri.encode(next.getValue().toString()));
                if (i2 < entrySet.size()) {
                    append.append('&');
                }
                i = i2;
            }
        }
        return C0030ba.releaseStringBuilder(append);
    }

    public static byte[] contentFromPOSTDict(URL url, Map<String, String> map, int i) {
        ByteArrayOutputStream byteArrayOutputStream;
        InputStream inputStream;
        InputStream inputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        try {
            String createBoundary = bo.createBoundary();
            HttpURLConnection httpURLConnection = (HttpURLConnection) bo.createConnection(url);
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setRequestProperty("Content-Type", bo.getContentType(createBoundary));
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Cache-Control", "no-cache");
            if (i != 0) {
                httpURLConnection.setConnectTimeout(i);
            } else {
                httpURLConnection.setConnectTimeout(10000);
            }
            httpURLConnection.setReadTimeout(60000);
            bo boVar = new bo(httpURLConnection.getOutputStream(), createBoundary);
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    boVar.writeField((String) next.getKey(), (String) next.getValue());
                }
            }
            boVar.close();
            if (httpURLConnection.getResponseCode() != 200) {
                X.e("Failed to open connection, status:%d", Integer.valueOf(httpURLConnection.getResponseCode()));
                return null;
            }
            byte[] acquireBytes = C0030ba.acquireBytes(1024);
            try {
                InputStream inputStream3 = getInputStream(httpURLConnection);
                try {
                    byteArrayOutputStream2 = new ByteArrayOutputStream(httpURLConnection.getContentLength() <= 0 ? 1024 : httpURLConnection.getContentLength());
                    while (true) {
                        try {
                            int read = inputStream3.read(acquireBytes);
                            if (read != -1) {
                                byteArrayOutputStream2.write(acquireBytes, 0, read);
                            } else {
                                byte[] byteArray = byteArrayOutputStream2.toByteArray();
                                aZ.close(inputStream3);
                                aZ.close(byteArrayOutputStream2);
                                C0030ba.releaseBytes(acquireBytes);
                                return byteArray;
                            }
                        } catch (Exception e) {
                            ByteArrayOutputStream byteArrayOutputStream3 = byteArrayOutputStream2;
                            inputStream = inputStream3;
                            byteArrayOutputStream = byteArrayOutputStream3;
                            aZ.close(inputStream);
                            aZ.close(byteArrayOutputStream);
                            C0030ba.releaseBytes(acquireBytes);
                            return null;
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            inputStream2 = inputStream3;
                            th = th2;
                            aZ.close(inputStream2);
                            aZ.close(byteArrayOutputStream2);
                            C0030ba.releaseBytes(acquireBytes);
                            throw th;
                        }
                    }
                } catch (Exception e2) {
                    inputStream = inputStream3;
                    byteArrayOutputStream = null;
                } catch (Throwable th3) {
                    inputStream2 = inputStream3;
                    th = th3;
                    byteArrayOutputStream2 = null;
                    aZ.close(inputStream2);
                    aZ.close(byteArrayOutputStream2);
                    C0030ba.releaseBytes(acquireBytes);
                    throw th;
                }
            } catch (Exception e3) {
                byteArrayOutputStream = null;
                inputStream = null;
            } catch (Throwable th4) {
                th = th4;
                byteArrayOutputStream2 = null;
                inputStream2 = null;
                aZ.close(inputStream2);
                aZ.close(byteArrayOutputStream2);
                C0030ba.releaseBytes(acquireBytes);
                throw th;
            }
        } catch (Exception e4) {
            X.e("Failed to write data into output: %s", e4);
            return null;
        }
    }

    public static byte[] contentFromPOSTString(URL url, byte[] bArr, int i) {
        ByteArrayOutputStream byteArrayOutputStream;
        InputStream inputStream;
        InputStream inputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        try {
            URLConnection openConnection = url.openConnection();
            if (openConnection instanceof HttpURLConnection) {
                ((HttpURLConnection) openConnection).setRequestMethod("POST");
            }
            openConnection.setRequestProperty("Accept-Encoding", "gzip");
            openConnection.setDoInput(true);
            openConnection.setDoOutput(true);
            openConnection.setUseCaches(false);
            openConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            openConnection.setRequestProperty("Content-Length", String.valueOf(bArr.length));
            openConnection.setRequestProperty("Connection", "Keep-Alive");
            openConnection.setRequestProperty("Cache-Control", "no-cache");
            if (i != 0) {
                openConnection.setConnectTimeout(i);
            } else {
                openConnection.setConnectTimeout(10000);
            }
            openConnection.setReadTimeout(60000);
            OutputStream outputStream = openConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.close();
            byte[] acquireBytes = C0030ba.acquireBytes(1024);
            try {
                inputStream = getInputStream(openConnection);
                try {
                    ByteArrayOutputStream byteArrayOutputStream3 = new ByteArrayOutputStream(openConnection.getContentLength() <= 0 ? 1024 : openConnection.getContentLength());
                    while (true) {
                        try {
                            int read = inputStream.read(acquireBytes);
                            if (read != -1) {
                                byteArrayOutputStream3.write(acquireBytes, 0, read);
                            } else {
                                byte[] byteArray = byteArrayOutputStream3.toByteArray();
                                aZ.close(inputStream);
                                aZ.close(byteArrayOutputStream3);
                                C0030ba.releaseBytes(acquireBytes);
                                return byteArray;
                            }
                        } catch (Exception e) {
                            byteArrayOutputStream = byteArrayOutputStream3;
                            aZ.close(inputStream);
                            aZ.close(byteArrayOutputStream);
                            C0030ba.releaseBytes(acquireBytes);
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            ByteArrayOutputStream byteArrayOutputStream4 = byteArrayOutputStream3;
                            inputStream2 = inputStream;
                            byteArrayOutputStream2 = byteArrayOutputStream4;
                            aZ.close(inputStream2);
                            aZ.close(byteArrayOutputStream2);
                            C0030ba.releaseBytes(acquireBytes);
                            throw th;
                        }
                    }
                } catch (Exception e2) {
                    byteArrayOutputStream = null;
                } catch (Throwable th2) {
                    th = th2;
                    inputStream2 = inputStream;
                    byteArrayOutputStream2 = null;
                    aZ.close(inputStream2);
                    aZ.close(byteArrayOutputStream2);
                    C0030ba.releaseBytes(acquireBytes);
                    throw th;
                }
            } catch (Exception e3) {
                byteArrayOutputStream = null;
                inputStream = null;
            } catch (Throwable th3) {
                th = th3;
                byteArrayOutputStream2 = null;
                inputStream2 = null;
                aZ.close(inputStream2);
                aZ.close(byteArrayOutputStream2);
                C0030ba.releaseBytes(acquireBytes);
                throw th;
            }
        } catch (IOException e4) {
            X.w("Failed to post  url: %s, data: %s, ex: %s", url.toString(), C0030ba.utf8_decode(bArr), e4.toString());
            return null;
        }
    }

    public static String contentFromURL(URL url) {
        return contentFromURL(url, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        r3 = null;
        r7 = r1;
        r1 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0067, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0068, code lost:
        r7 = r2;
        r2 = r1;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0072, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0073, code lost:
        r7 = r3;
        r3 = r2;
        r2 = r1;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0067 A[ExcHandler: all (r2v6 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0014] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String contentFromURL(java.net.URL r8, boolean r9) {
        /*
            r5 = 0
            r0 = 1024(0x400, float:1.435E-42)
            java.lang.StringBuilder r0 = com.papaya.si.C0030ba.acquireStringBuilder(r0)
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x006e, all -> 0x005e }
            java.io.InputStream r2 = inputStreamFromURL(r8)     // Catch:{ Exception -> 0x006e, all -> 0x005e }
            java.lang.String r3 = "utf-8"
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x006e, all -> 0x005e }
            r2 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r2]     // Catch:{ Exception -> 0x0022, all -> 0x0067 }
        L_0x0016:
            int r3 = r1.read(r2)     // Catch:{ Exception -> 0x0022, all -> 0x0067 }
            r4 = -1
            if (r3 == r4) goto L_0x003a
            r4 = 0
            r0.append(r2, r4, r3)     // Catch:{ Exception -> 0x0022, all -> 0x0067 }
            goto L_0x0016
        L_0x0022:
            r2 = move-exception
            r3 = r5
            r7 = r1
            r1 = r2
            r2 = r7
        L_0x0027:
            java.lang.String r4 = "failed to get content from url: %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x006c }
            r6 = 0
            r5[r6] = r1     // Catch:{ all -> 0x006c }
            com.papaya.si.X.e(r4, r5)     // Catch:{ all -> 0x006c }
            com.papaya.si.aZ.close(r2)
            com.papaya.si.C0030ba.releaseOnly(r0)
            r0 = r3
        L_0x0039:
            return r0
        L_0x003a:
            java.lang.String r2 = com.papaya.si.C0030ba.releaseStringBuilder(r0)     // Catch:{ Exception -> 0x0022, all -> 0x0067 }
            if (r9 == 0) goto L_0x004a
            com.papaya.si.aZ$b r3 = com.papaya.si.C0040bk.ic     // Catch:{ Exception -> 0x0072, all -> 0x0067 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x0072, all -> 0x0067 }
            com.papaya.si.aZ$b r4 = com.papaya.si.C0040bk.ic     // Catch:{ all -> 0x0052 }
            java.lang.String r2 = r4.decrypt(r2)     // Catch:{ all -> 0x0052 }
            monitor-exit(r3)     // Catch:{ all -> 0x0052 }
        L_0x004a:
            com.papaya.si.aZ.close(r1)
            com.papaya.si.C0030ba.releaseOnly(r0)
            r0 = r2
            goto L_0x0039
        L_0x0052:
            r4 = move-exception
            r7 = r4
            r4 = r2
            r2 = r7
            monitor-exit(r3)     // Catch:{ Exception -> 0x0058, all -> 0x0067 }
            throw r2     // Catch:{ Exception -> 0x0058, all -> 0x0067 }
        L_0x0058:
            r2 = move-exception
            r3 = r4
            r7 = r1
            r1 = r2
            r2 = r7
            goto L_0x0027
        L_0x005e:
            r1 = move-exception
            r2 = r5
        L_0x0060:
            com.papaya.si.aZ.close(r2)
            com.papaya.si.C0030ba.releaseOnly(r0)
            throw r1
        L_0x0067:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0060
        L_0x006c:
            r1 = move-exception
            goto L_0x0060
        L_0x006e:
            r1 = move-exception
            r2 = r5
            r3 = r5
            goto L_0x0027
        L_0x0072:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r1
            r1 = r7
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0040bk.contentFromURL(java.net.URL, boolean):java.lang.String");
    }

    public static URL createURL(String str) {
        return createURL(str, C0061v.bj);
    }

    public static URL createURL(String str, URL url) {
        if (str == null) {
            return url;
        }
        try {
            URL url2 = ie.get(str);
            if (url2 == null) {
                if (str.startsWith("papaya://") || str.startsWith("file:///android_asset/") || str.startsWith(bE.ny)) {
                    X.e("invalid argument for createURL: " + str, new Object[0]);
                } else {
                    url2 = url == null ? new URL(str) : new URL(url, str);
                }
            }
            if (url2 == null) {
                return url2;
            }
            ie.put(str, url2);
            return url2;
        } catch (Exception e) {
            X.e(e, "Failed to createURI %s, %s", str, url);
            return null;
        }
    }

    public static String decrypt(String str) {
        String decrypt;
        if (str == null) {
            return "";
        }
        synchronized (ic) {
            decrypt = ic.decrypt(str);
        }
        return decrypt;
    }

    public static int dialogButtonToWebIndex(int i) {
        if (i == -2) {
            return 0;
        }
        return i == -3 ? 1 : 2;
    }

    public static String encodeUriComponent(String str) {
        return Uri.encode(str);
    }

    public static String encrypt(String str) {
        String encrypt;
        String str2 = str == null ? "" : str;
        synchronized (ib) {
            encrypt = ib.encrypt(str2);
        }
        return encrypt;
    }

    public static String escapeJS(CharSequence charSequence) {
        if (C0030ba.isEmpty(charSequence)) {
            return "";
        }
        StringBuilder acquireStringBuilder = C0030ba.acquireStringBuilder(charSequence.length());
        for (int i = 0; i < charSequence.length(); i++) {
            char charAt = charSequence.charAt(i);
            if (charAt == '\'') {
                acquireStringBuilder.append("\\'");
            } else if (charAt == 13) {
                acquireStringBuilder.append("\\r");
            } else if (charAt == 10) {
                acquireStringBuilder.append("\\n");
            } else if (charAt == 8) {
                acquireStringBuilder.append("\\b");
            } else if (charAt == 9) {
                acquireStringBuilder.append("\\t");
            } else if (charAt == 12) {
                acquireStringBuilder.append("\\f");
            } else {
                acquireStringBuilder.append(charAt);
            }
        }
        return C0030ba.releaseStringBuilder(acquireStringBuilder);
    }

    public static InputStream getInputStream(URLConnection uRLConnection) throws Exception {
        InputStream inputStream = uRLConnection.getInputStream();
        String contentEncoding = uRLConnection.getContentEncoding();
        return (contentEncoding == null || !contentEncoding.toLowerCase().contains("gzip")) ? (contentEncoding == null || !contentEncoding.toLowerCase().contains("deflate")) ? inputStream : new InflaterInputStream(inputStream) : new GZIPInputStream(inputStream);
    }

    public static JSONArray getJsonArray(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return null;
        }
        try {
            if (jSONObject.has(str)) {
                return jSONObject.getJSONArray(str);
            }
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonArray %s", str);
        }
        return null;
    }

    public static int getJsonInt(JSONArray jSONArray, int i) {
        return getJsonInt(jSONArray, i, -1);
    }

    public static int getJsonInt(JSONArray jSONArray, int i, int i2) {
        if (jSONArray == null) {
            return i2;
        }
        try {
            return jSONArray.getInt(i);
        } catch (JSONException e) {
            X.e(e, "Failed to get JsonInt %d, %d", Integer.valueOf(jSONArray.length()), Integer.valueOf(i));
            return i2;
        }
    }

    public static int getJsonInt(JSONObject jSONObject, String str) {
        return getJsonInt(jSONObject, str, -1);
    }

    public static int getJsonInt(JSONObject jSONObject, String str, int i) {
        if (jSONObject == null) {
            return i;
        }
        if (jSONObject.has(str)) {
            try {
                return jSONObject.getInt(str);
            } catch (JSONException e) {
                X.e(e, "Failed to getJsonInt %s", str);
            }
        }
        return i;
    }

    public static JSONObject getJsonObject(JSONArray jSONArray, int i) {
        if (jSONArray == null) {
            return null;
        }
        try {
            return jSONArray.getJSONObject(i);
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonObject %d", Integer.valueOf(i));
            return null;
        }
    }

    public static JSONObject getJsonObject(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return null;
        }
        try {
            if (jSONObject.has(str)) {
                return jSONObject.getJSONObject(str);
            }
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonObject %s", str);
        }
        return null;
    }

    public static String getJsonString(JSONArray jSONArray, int i) {
        return getJsonString(jSONArray, i, (String) null);
    }

    public static String getJsonString(JSONArray jSONArray, int i, String str) {
        if (jSONArray == null) {
            return str;
        }
        try {
            return jSONArray.getString(i);
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonString %d, %d", Integer.valueOf(jSONArray.length()), Integer.valueOf(i));
            return str;
        }
    }

    public static String getJsonString(JSONObject jSONObject, String str) {
        return getJsonString(jSONObject, str, (String) null);
    }

    public static String getJsonString(JSONObject jSONObject, String str, String str2) {
        if (jSONObject == null) {
            return str2;
        }
        try {
            if (jSONObject.has(str)) {
                String string = jSONObject.getString(str);
                return string == null ? str2 : string;
            }
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonString %s", str);
        }
        return null;
    }

    public static Object getJsonValue(JSONObject jSONObject, String str) {
        return getJsonValue(jSONObject, str, null);
    }

    public static Object getJsonValue(JSONObject jSONObject, String str, Object obj) {
        if (jSONObject == null) {
            return obj;
        }
        try {
            if (jSONObject.has(str)) {
                Object obj2 = jSONObject.get(str);
                return obj2 == null ? obj : obj2;
            }
        } catch (JSONException e) {
            X.e(e, "Failed to getJsonValue %s", str);
        }
        return obj;
    }

    public static InputStream inputStreamFromURL(URL url) {
        if (url == null) {
            return null;
        }
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.connect();
            if (httpURLConnection.getResponseCode() == 200) {
                return getInputStream(httpURLConnection);
            }
            X.w("inputstream error code %d", Integer.valueOf(httpURLConnection.getResponseCode()));
            return null;
        } catch (Exception e) {
            X.w("failed to open inputstream, %s", e);
        }
    }

    public static Object java2jsonValue(Object obj) {
        if (obj instanceof List) {
            JSONArray jSONArray = new JSONArray();
            for (Object java2jsonValue : (List) obj) {
                jSONArray.put(java2jsonValue(java2jsonValue));
            }
            return jSONArray;
        } else if (!(obj instanceof Map)) {
            return obj instanceof byte[] ? C0030ba.utf8String((byte[]) obj, "") : obj;
        } else {
            JSONObject jSONObject = new JSONObject();
            Map map = (Map) obj;
            for (Object next : map.keySet()) {
                try {
                    jSONObject.put((String) next, java2jsonValue(map.get(next)));
                } catch (Exception e) {
                    X.e(e, "Failed to convert javamap to json", new Object[0]);
                }
            }
            return jSONObject;
        }
    }

    public static Object jsonToPlain(Object obj) throws Exception {
        if (obj instanceof JSONArray) {
            JSONArray jSONArray = (JSONArray) obj;
            Vector vector = new Vector(jSONArray.length());
            for (int i = 0; i < jSONArray.length(); i++) {
                vector.add(jsonToPlain(jSONArray.get(i)));
            }
            return vector;
        } else if (!(obj instanceof JSONObject)) {
            return obj;
        } else {
            JSONObject jSONObject = (JSONObject) obj;
            HashMap hashMap = new HashMap();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jsonToPlain(jSONObject.opt(next)));
            }
            return hashMap;
        }
    }

    public static String object2JSONString(Object obj) {
        if (obj == null) {
            return "null";
        }
        try {
            if (obj instanceof Number) {
                return JSONObject.numberToString((Number) obj);
            }
            if (obj instanceof Boolean) {
                return ((Boolean) obj).booleanValue() ? "true" : "false";
            }
            if (obj instanceof CharSequence) {
                return JSONObject.quote(obj.toString());
            }
            if (obj instanceof Collection) {
                return new JSONArray((Collection) obj).toString();
            }
            if (obj instanceof Map) {
                return new JSONObject((Map) obj).toString();
            }
            X.w("unknown type to convert to json string, %s", obj);
            return "undefined";
        } catch (JSONException e) {
            X.w(e, "Failed to execute object2JSONString: %s", obj);
        }
    }

    public static Object[] parseJsonAsArray(String str) {
        String str2;
        if (str == null) {
            return new Object[0];
        }
        try {
            String trim = str.trim();
            try {
                if (trim.charAt(0) != '[') {
                    trim = C0030ba.releaseStringBuilder(C0030ba.acquireStringBuilder(trim.length() + 2).append('[').append(trim).append(']'));
                }
                JSONArray jSONArray = new JSONArray(trim);
                Object[] objArr = new Object[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    objArr[i] = jSONArray.get(i);
                }
                return objArr;
            } catch (JSONException e) {
                JSONException jSONException = e;
                str2 = trim;
                e = jSONException;
                X.e(e, "Failed to parseJsonAsArray %s", str2);
                return new Object[0];
            }
        } catch (JSONException e2) {
            e = e2;
            str2 = str;
            X.e(e, "Failed to parseJsonAsArray %s", str2);
            return new Object[0];
        }
    }

    public static JSONObject parseJsonObject(String str) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            X.w(e, "Failed to parseJsonObject, %s", str);
            return new JSONObject();
        }
    }

    public static void setDefault(JSONObject jSONObject, String str, int i) {
        if (jSONObject.optInt(str, -1) == -1) {
            try {
                jSONObject.put(str, i);
            } catch (JSONException e) {
                X.dw(e, "Failed to set %s, %d", str, Integer.valueOf(i));
            }
        }
    }

    public static void setDefault(JSONObject jSONObject, String str, String str2) {
        if (jSONObject.optString(str) == null) {
            try {
                jSONObject.put(str, str2);
            } catch (JSONException e) {
                X.dw(e, "Failed to set %s, %s", str, str2);
            }
        }
    }

    public static void submitPRIALog(String str, String str2) {
    }

    public static boolean urlEquals(URL url, URL url2) {
        if (url == url2) {
            return true;
        }
        if (url == null || url2 == null) {
            return false;
        }
        return url.toString().equals(url2.toString());
    }
}
