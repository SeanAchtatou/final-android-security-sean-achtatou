package com.papaya.si;

import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class O {
    private static Pattern cV = null;
    private static String[] cW = {":)", ":D", ":p", ":(", ":'(", "|-)", ":@", ":s", ":o", ":$", "(y)", "(n)", "(h)", "8o|", "(l)"};
    private static ArrayList<String> cX = new ArrayList<>(42);
    private static HashMap<String, String> cY = new HashMap<>(42);
    public D cP;
    public CharSequence cZ;
    public String da;
    public int db;
    public int type;

    static {
        for (int i = 0; i < cW.length; i++) {
            cX.add(cW[i]);
            cY.put(cW[i], "e" + i);
        }
        for (int length = cW.length; length < 42; length++) {
            String str = "[~" + (length - cW.length) + "~]";
            cX.add(str);
            cY.put(str, "e" + length);
        }
        cX.add("[~pic~]");
        cY.put("[~pic~]", "photo");
    }

    public O() {
    }

    public O(int i, D d, CharSequence charSequence, int i2) {
        this.type = i;
        this.cP = d;
        this.db = i2;
        if (charSequence instanceof String) {
            String string = i == 2 ? C0042c.getString("chat_msg_prefix") : i == 3 ? d.getTitle() + ": " : null;
            this.da = parseUri((String) charSequence);
            this.cZ = parseEmoticon((String) (this.da != null ? C0042c.getString("chat_msg_just_sent_picture") + cX.get(cX.size() - 1) + C0042c.getString("chat_msg_tap_view") : charSequence), string);
            return;
        }
        this.cZ = charSequence;
    }

    private static void buildPattern() {
        StringBuilder sb = new StringBuilder(168);
        sb.append('(');
        for (int i = 0; i < 43; i++) {
            sb.append(Pattern.quote(getEmoticonString(i)));
            if (i < 42) {
                sb.append('|');
            }
        }
        sb.append(')');
        cV = Pattern.compile(sb.toString());
    }

    public static String getEmoticonString(int i) {
        return cX.get(i);
    }

    public static CharSequence parseEmoticon(String str) {
        return parseEmoticon(str, null);
    }

    public static CharSequence parseEmoticon(String str, String str2) {
        if (str == null) {
            return null;
        }
        if (cV == null) {
            buildPattern();
        }
        String str3 = str2 != null ? str2 + str : str;
        SpannableString spannableString = new SpannableString(str3);
        Matcher matcher = cV.matcher(str3);
        int i = 0;
        while (matcher.find() && (i = i + 1) <= 500) {
            Drawable drawable = C0042c.getApplicationContext().getResources().getDrawable(C0065z.drawableID(cY.get(matcher.group())));
            drawable.setBounds(0, 0, C0036bg.rp(16), C0036bg.rp(16));
            spannableString.setSpan(new ImageSpan(drawable, 1), matcher.start(), matcher.end(), 33);
        }
        return spannableString;
    }

    public static String parseUri(String str) {
        int indexOf;
        if (str == null) {
            return null;
        }
        if (!str.startsWith("[Picture ") || (indexOf = str.indexOf(93)) == -1) {
            return null;
        }
        return str.substring(9, indexOf).trim();
    }
}
