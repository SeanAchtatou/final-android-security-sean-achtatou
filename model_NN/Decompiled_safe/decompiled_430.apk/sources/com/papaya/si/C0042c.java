package com.papaya.si;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import com.papaya.analytics.PPYReferralReceiver;
import com.papaya.service.AppManager;
import com.papaya.si.C0065z;
import com.papaya.view.TakePhotoBridge;
import java.util.List;

/* renamed from: com.papaya.si.c  reason: case insensitive filesystem */
public final class C0042c {
    public static C0025aw A = new C0025aw();
    public static final aM B = new aM();
    private static final ColorDrawable C = new ColorDrawable(-65536);
    private static final Bitmap D = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
    private static final aN E = new aN();
    private static C F;
    private static boolean u = false;
    private static Application v;
    /* access modifiers changed from: private */
    public static A w;
    /* access modifiers changed from: private */
    public static bv z;

    public static Application getApplicationContext() {
        return v;
    }

    private static Bitmap getBitmap(int i) {
        try {
            if (v != null) {
                return BitmapFactory.decodeResource(v.getResources(), i);
            }
        } catch (Exception e) {
            X.e(e, "Failde to getBitmap: " + i, new Object[0]);
        }
        return D;
    }

    public static BitmapDrawable getBitmapDrawable(String str) {
        Bitmap bitmap = E.get(str);
        if (bitmap == null) {
            bitmap = getBitmap(C0065z.drawableID(str));
            E.put(str, bitmap);
        }
        if (bitmap == D) {
            X.e("Can't decode bitmap from resource: " + str, new Object[0]);
        }
        return new BitmapDrawable(bitmap);
    }

    public static Drawable getDrawable(int i) {
        try {
            if (v != null) {
                return v.getResources().getDrawable(i);
            }
        } catch (Exception e) {
            X.e(e, "Failed to get drawable " + i, new Object[0]);
        }
        return C;
    }

    public static Drawable getDrawable(String str) {
        Drawable drawable = getDrawable(C0065z.drawableID(str));
        if (drawable == C) {
            X.e("Failed to get drawable: " + str, new Object[0]);
        }
        return drawable;
    }

    public static A getSession() {
        return w;
    }

    public static String getString(int i) {
        try {
            return v.getResources().getString(i);
        } catch (Exception e) {
            X.e(e, "Failed to find string " + i, new Object[0]);
            return "";
        }
    }

    public static String getString(String str) {
        String string = getString(C0065z.stringID(str));
        if (C0030ba.isEmpty(string) && !"plural".equals(str)) {
            X.e("Failed to get string: " + str, new Object[0]);
        }
        return string;
    }

    public static C getTabBadgeValues() {
        return F;
    }

    public static bv getWebGameBridge() {
        return z;
    }

    public static void initialize(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (!u) {
            long currentTimeMillis = System.currentTimeMillis();
            v = (Application) applicationContext;
            C0061v.setup(applicationContext);
            System.currentTimeMillis();
            initializeUtils();
            initializePOTP();
            initializeCaches();
            initializeServices();
            initialize3rd();
            postInitialize();
            X.i("initialization time: %dms", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
            X.i("version: %d, source %s, lang %s", Integer.valueOf(C0061v.bc), C0061v.be, C0061v.bd);
            u = true;
        }
    }

    private static void initialize3rd() {
        C0047h.initialize(v);
        C0048i.initialize(v);
        G.initialize(v);
        C0044e.initialize(v);
        C0045f.initialize(v);
        C0065z.a.initialize(v);
        C0046g.register();
    }

    private static void initializeCaches() {
        C0002a.initialize(v);
        C0017ao.getInstance().initialize(v);
        C0015am.getInstance().initialize(v);
    }

    private static void initializePOTP() {
        try {
            A = new C0025aw();
            A a = new A();
            w = a;
            a.initialize();
            z = new bv();
            F = new C();
        } catch (Exception e) {
            X.e("Failed to start POTP thread: " + e, new Object[0]);
            throw new RuntimeException(e);
        }
    }

    private static void initializeServices() {
        C0043d.initialize(v);
        PPYReferralReceiver.sendReferrerReport(v);
        AppManager.initialize(v);
    }

    private static void initializeUtils() {
        C0035bf.initialize(v);
        C0036bg.initialize();
    }

    public static boolean isInitialized() {
        return u;
    }

    private static void postInitialize() {
        C0017ao.getInstance().sendUpdateRequest();
        C0035bf.checkFreeSpace(v);
    }

    public static void quit() {
        u = false;
        C0029b.hideAllOverlayDialogs();
        C0036bg.post(new Runnable() {
            public final void run() {
                C0029b.finishAllActivities();
                try {
                    C0042c.w.quit();
                    A unused = C0042c.w = new A();
                } catch (Exception e) {
                    X.e(e, "Failed to saverms", new Object[0]);
                }
                C0042c.A.close();
                C0002a.destroy();
                bL.getInstance().clear();
                C0015am.getInstance().clear();
                C0017ao.getInstance().clear();
                C0040bk.clear();
                C0030ba.clear();
                bu.clear();
                TakePhotoBridge.clear();
                C0042c.z.clear();
                C0029b.clear();
                C0045f.destroy();
                C0036bg.destroy();
                C0036bg.forceClearBitmap();
            }
        });
    }

    public static void send(int i, Object... objArr) {
        if (A != null) {
            A.send(i, objArr);
        }
    }

    public static void send(List list) {
        if (A != null) {
            A.send(list);
        }
    }
}
