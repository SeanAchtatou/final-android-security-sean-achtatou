package com.papaya.si;

import android.app.Activity;
import android.app.ProgressDialog;
import com.papaya.si.aZ;
import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialQuery;
import org.json.JSONObject;

public final class aI extends ProgressDialog implements PPYSocialQuery.QueryDelegate {
    private Activity gH;

    public aI(Activity activity) {
        super(activity);
        this.gH = activity;
        setMessage(C0042c.getString("accepting_challenge"));
        setCancelable(false);
    }

    public final void acceptChallenge(int i) {
        show();
        setIndeterminate(true);
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_challenge_accept", this);
        pPYSocialQuery.put("rid", i);
        aA.getInstance().submitQuery(pPYSocialQuery);
    }

    public final void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str) {
        dismiss();
        C0036bg.showToast(C0042c.getString("fail_accept_challenge"), 0);
    }

    public final void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject) {
        dismiss();
        if (jSONObject != null) {
            PPYSocialChallengeRecord pPYSocialChallengeRecord = new PPYSocialChallengeRecord();
            pPYSocialChallengeRecord.setRecordID(jSONObject.optInt("id", 0));
            pPYSocialChallengeRecord.setChallengeDefinitionID(jSONObject.optInt("cid", 0));
            pPYSocialChallengeRecord.setSenderUserID(jSONObject.optInt("sender_id", 0));
            pPYSocialChallengeRecord.setReceiverUserID(jSONObject.optInt("receiver_id", 0));
            pPYSocialChallengeRecord.setMessage(jSONObject.optString("message", null));
            int optInt = jSONObject.optInt("payload_type", 0);
            String optString = jSONObject.optString("payload", null);
            if (optInt == 0) {
                pPYSocialChallengeRecord.setPayload(optString);
            } else {
                pPYSocialChallengeRecord.setPayloadBinary(optString != null ? aZ.a.decode(optString) : null);
            }
            try {
                this.gH.finish();
            } catch (Exception e) {
            }
            C0027ay.instance().fireChallengeAccepted(pPYSocialChallengeRecord);
        }
    }
}
