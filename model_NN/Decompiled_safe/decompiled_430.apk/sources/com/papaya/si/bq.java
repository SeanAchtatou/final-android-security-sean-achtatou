package com.papaya.si;

import android.net.Uri;
import com.papaya.si.by;
import com.papaya.social.PPYSocial;
import java.net.URL;
import java.util.ArrayList;

public final class bq extends C0039bj<br> {
    /* access modifiers changed from: private */
    public URL mJ;
    /* access modifiers changed from: private */
    public bA mK;
    /* access modifiers changed from: private */
    public b mL;
    /* access modifiers changed from: private */
    public String mM;
    /* access modifiers changed from: private */
    public ArrayList mN;
    /* access modifiers changed from: private */
    public ArrayList<bA> mO;
    private c mP;
    /* access modifiers changed from: private */
    public String mv;
    private boolean mz;
    private URL url;

    static final class a extends bA {
        String name;

        public a(String str) {
            this.name = str;
            URL createURL = C0040bk.createURL(C0030ba.format("misc/include?name=%s&identifier=%s&version=%d&m=%s&d=%d&lang=%s", Uri.encode(str), C0028az.fK, 173, "social_170", 0, C0061v.bd));
            if (createURL == null) {
                X.e("invalid include URL: " + str, new Object[0]);
            }
            setUrl(createURL);
            setCacheable(false);
        }
    }

    final class b implements by.a {
        /* synthetic */ b(bq bqVar) {
            this((byte) 0);
        }

        private b(byte b) {
        }

        public final void connectionFailed(by byVar, int i) {
            bA unused = bq.this.mK = null;
            b unused2 = bq.this.mL = null;
            String unused3 = bq.this.mM = C0017ao.getInstance().newPageContent(bq.this.mv, false);
            if (C0030ba.isEmpty(bq.this.mM)) {
                br brVar = (br) bq.this.getDelegate();
                if (brVar != null) {
                    brVar.onPageContentLoadFailed(bq.this);
                    return;
                }
                return;
            }
            bq.this.analyzePageContent();
        }

        public final void connectionFinished(by byVar) {
            bA unused = bq.this.mK = null;
            b unused2 = bq.this.mL = null;
            String unused3 = bq.this.mM = C0030ba.utf8String(byVar.getData(), null);
            if (bq.this.mv != null && bq.this.mv.startsWith("static_") && !C0030ba.isEmpty(bq.this.mM)) {
                C0017ao.getInstance().savePage(bq.this.mv, bq.this.mM);
            }
            URL unused4 = bq.this.mJ = byVar.getRedirectUrl();
            bq.this.analyzePageContent();
        }
    }

    final class c implements by.a {
        /* synthetic */ c(bq bqVar) {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void connectionFailed(by byVar, int i) {
            synchronized (bq.this.mO) {
                if (byVar != null) {
                    a aVar = (a) byVar.getRequest();
                    bq.this.mO.remove(aVar);
                    int indexOf = bq.this.mN.indexOf(aVar);
                    if (indexOf != -1) {
                        String newPageContent = C0017ao.getInstance().newPageContent(aVar.name, false);
                        if (C0030ba.isEmpty(newPageContent)) {
                            bq.this.mN.remove(aVar);
                        } else {
                            bq.this.mN.set(indexOf, newPageContent);
                        }
                    } else {
                        X.w("Can't find segment: " + aVar, new Object[0]);
                    }
                    X.w("Failed to load include segment: " + aVar.getUrl(), new Object[0]);
                }
                if (bq.this.mO.isEmpty()) {
                    bq.this.combineSegments();
                }
            }
        }

        public final void connectionFinished(by byVar) {
            synchronized (bq.this.mO) {
                if (byVar != null) {
                    a aVar = (a) byVar.getRequest();
                    bq.this.mO.remove(aVar);
                    int indexOf = bq.this.mN.indexOf(aVar);
                    if (indexOf != -1) {
                        String utf8String = C0030ba.utf8String(byVar.getData(), "");
                        if (!C0030ba.isEmpty(utf8String)) {
                            C0017ao.getInstance().savePage(aVar.name, utf8String);
                        }
                        bq.this.mN.set(indexOf, utf8String);
                    } else {
                        X.w("Can't find segment: " + aVar, new Object[0]);
                    }
                }
                if (bq.this.mO.isEmpty()) {
                    bq.this.combineSegments();
                }
            }
        }
    }

    public bq(URL url2, boolean z) {
        this.url = url2;
        this.mz = z;
    }

    private String appendLang(String str) {
        return PPYSocial.LANG_ZH_CN.equals(C0061v.bd) ? str + ".zh_CN" : str;
    }

    /* access modifiers changed from: protected */
    public final void analyzePageContent() {
        if (this.mM != null) {
            this.mN = new ArrayList();
            this.mO = new ArrayList<>();
            this.mP = new c(this);
            C0017ao instance = C0017ao.getInstance();
            boolean z = false;
            int i = 0;
            do {
                int indexOf = this.mM.indexOf("<!---include(\"", i);
                if (indexOf != -1) {
                    int indexOf2 = this.mM.indexOf("\")-->", indexOf);
                    if (indexOf2 != -1) {
                        this.mN.add(this.mM.substring(i, indexOf));
                        i = "\")-->".length() + indexOf2;
                        String appendLang = appendLang(this.mM.substring("<!---include(\"".length() + indexOf, indexOf2));
                        String newPageContent = instance.newPageContent(appendLang, true);
                        if (newPageContent != null) {
                            this.mN.add(newPageContent);
                        } else {
                            a aVar = new a(appendLang);
                            aVar.setDelegate(this.mP);
                            aVar.setRequireSid(this.mz);
                            this.mN.add(aVar);
                            this.mO.add(aVar);
                        }
                    } else {
                        int indexOf3 = this.mM.indexOf("-->", indexOf);
                        if (indexOf3 != -1) {
                            i = "-->".length() + indexOf3;
                        } else {
                            z = true;
                        }
                    }
                }
                if (indexOf == -1) {
                    break;
                }
            } while (!z);
            if (z) {
                X.e("Failed to parse include syntax: " + this.url, new Object[0]);
            }
            if (i == 0) {
                this.mN.add(this.mM);
            } else if (i < this.mM.length()) {
                this.mN.add(this.mM.substring(i, this.mM.length()));
            }
            if (this.mO.isEmpty()) {
                combineSegments();
            } else {
                C0002a.insertRequests(this.mO);
            }
        }
    }

    public final void cancel() {
    }

    /* access modifiers changed from: protected */
    public final void combineSegments() {
        if (this.mO.isEmpty()) {
            if (this.mN.size() == 1) {
                this.mM = (String) this.mN.get(0);
            } else {
                int i = 0;
                int i2 = 0;
                while (i < this.mN.size()) {
                    i++;
                    i2 = ((String) this.mN.get(i)).length() + i2;
                }
                StringBuilder acquireStringBuilder = C0030ba.acquireStringBuilder(i2);
                for (int i3 = 0; i3 < this.mN.size(); i3++) {
                    acquireStringBuilder.append((String) this.mN.get(i3));
                }
                this.mM = C0030ba.releaseStringBuilder(acquireStringBuilder);
            }
            if (C0036bg.isMainThread()) {
                br brVar = (br) getDelegate();
                if (brVar != null) {
                    brVar.onPageContentLoaded(this);
                    return;
                }
                return;
            }
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    br brVar = (br) bq.this.getDelegate();
                    if (brVar != null) {
                        brVar.onPageContentLoaded(bq.this);
                    }
                }
            });
            return;
        }
        X.w("segment requests are not empty: " + this.mO, new Object[0]);
    }

    public final String getPageContent() {
        return this.mM;
    }

    public final String getPageName() {
        return this.mv;
    }

    public final URL getRedirectUrl() {
        return this.mJ;
    }

    public final URL getUrl() {
        return this.url;
    }

    /* access modifiers changed from: protected */
    public final void parseRequires() {
    }

    public final void start() {
        if (this.mM == null && this.mK == null) {
            String path = this.url.getPath();
            int indexOf = path.indexOf("/static_");
            if (indexOf != -1) {
                this.mv = appendLang(path.substring(indexOf + 1));
                this.mM = C0017ao.getInstance().newPageContent(this.mv, true);
            }
            if (this.mM == null) {
                if (!C0030ba.isEmpty(this.mv)) {
                    URL createURL = C0040bk.createURL(C0030ba.format("misc/page?name=%s&identifier=%s&version=%d&m=%s&d=%d&lang=%s", Uri.encode(this.mv), C0028az.fK, 173, "social_170", 0, C0061v.bd));
                    if (createURL != null) {
                        this.mK = new bA(createURL, false);
                    } else {
                        X.e("page url is null " + this.mv, new Object[0]);
                    }
                }
                if (this.mK == null) {
                    this.mK = new bA(this.url, false);
                }
                this.mK.setRequireSid(this.mz);
                this.mL = new b(this);
                this.mK.setDelegate(this.mL);
                this.mK.start(true);
                return;
            }
            analyzePageContent();
        }
    }
}
