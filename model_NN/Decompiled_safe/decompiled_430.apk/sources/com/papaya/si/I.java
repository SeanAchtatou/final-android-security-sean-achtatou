package com.papaya.si;

import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;

public class I implements ActionsImageButton.Delegate {
    private /* synthetic */ ChatActivity cM;

    public I(ChatActivity chatActivity) {
        this.cM = chatActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onActionSelected(ActionsImageButton actionsImageButton, Action action) {
        try {
            A session = C0042c.getSession();
            switch (action.id) {
                case 0:
                    if (this.cM.cG != null) {
                        this.cM.closeActiveChat();
                        return;
                    }
                    return;
                case 1:
                    if (this.cM.cG instanceof C0012aj) {
                        C0029b.openHome(this.cM, ((C0012aj) this.cM.cG).getUserID());
                        return;
                    }
                    return;
                case 2:
                case 4:
                default:
                    X.e("unknown action: " + action.id, new Object[0]);
                    return;
                case 3:
                    if (this.cM.cG instanceof C0012aj) {
                        this.cM.showDialog(5);
                        return;
                    }
                    return;
                case 5:
                    if (this.cM.cG instanceof Q) {
                        this.cM.showDialog(2);
                        return;
                    }
                    return;
                case 6:
                    if (this.cM.cG instanceof Q) {
                        Q q = (Q) this.cM.cG;
                        q.addSystemMessage(C0042c.getString("privateoff_info"));
                        q.di = false;
                        for (int i = 0; i < q.dj.size(); i++) {
                            session.getPrivateChatWhiteList().remove(Integer.valueOf(q.dj.get(i).cU));
                        }
                        q.fireDataStateChanged();
                        return;
                    } else if (this.cM.cG instanceof C0009ag) {
                        this.cM.cG.addSystemMessage(C0042c.getString("chat_msg_sys_private_chat_off"));
                        session.getPrivateChatWhiteList().remove(Integer.valueOf(((C0009ag) this.cM.cG).cU));
                        this.cM.cG.fireDataStateChanged();
                        return;
                    } else {
                        return;
                    }
                case 7:
                    if (this.cM.cG instanceof Q) {
                        Q q2 = (Q) this.cM.cG;
                        q2.addSystemMessage(C0042c.getString("privateon_info"));
                        q2.di = true;
                        for (int i2 = 0; i2 < q2.dj.size(); i2++) {
                            session.getPrivateChatWhiteList().add(Integer.valueOf(q2.dj.get(i2).cU));
                        }
                        q2.fireDataStateChanged();
                        return;
                    } else if (this.cM.cG instanceof C0009ag) {
                        this.cM.cG.addSystemMessage(C0042c.getString("chat_msg_sys_private_chat_on"));
                        session.getPrivateChatWhiteList().add(Integer.valueOf(((C0009ag) this.cM.cG).cU));
                        this.cM.cG.fireDataStateChanged();
                        return;
                    } else {
                        return;
                    }
                case 8:
                    this.cM.showDialog(4);
                    return;
                case 9:
                    if (this.cM.cG instanceof L) {
                        C0029b.openPRIALink(this.cM, "static_manage_chatgroup?gid=" + ((L) this.cM.cG).cQ);
                        return;
                    }
                    return;
                case 10:
                    this.cM.showDialog(3);
                    return;
                case 11:
                    if (this.cM.cG instanceof L) {
                        L l = (L) this.cM.cG;
                        C0042c.send(315, Integer.valueOf(l.cQ), 1);
                        l.cS = C0030ba.bitSet(l.cS, 0, 1);
                        l.fireDataStateChanged();
                        return;
                    }
                    return;
                case 12:
                    if (this.cM.cG instanceof L) {
                        L l2 = (L) this.cM.cG;
                        C0042c.send(315, Integer.valueOf(l2.cQ), 0);
                        l2.cS = C0030ba.bitSet(l2.cS, 0, 0);
                        l2.fireDataStateChanged();
                        return;
                    }
                    return;
            }
        } catch (Exception e) {
            X.e(e, "Error occurred in onActionSelected", new Object[0]);
        }
        X.e(e, "Error occurred in onActionSelected", new Object[0]);
    }
}
