package com.papaya.si;

import android.graphics.drawable.Drawable;
import com.papaya.social.PPYUser;

/* renamed from: com.papaya.si.aj  reason: case insensitive filesystem */
public final class C0012aj extends D implements PPYUser {
    private int cU;
    private int dU;
    public String dV;
    public long dW = 0;
    private String dl = "";

    public C0012aj(int i, String str) {
        this.cU = i;
        this.dl = str;
    }

    public final int compareTo(D d) {
        if (getUserID() == A.bK.getUserID()) {
            return -1;
        }
        if (!(d instanceof C0012aj) || ((C0012aj) d).getUserID() != A.bK.getUserID()) {
            return super.compareTo(d);
        }
        return 1;
    }

    public final Drawable getDefaultDrawable() {
        return getUserID() == 0 ? C0042c.getDrawable("contacts") : C0042c.getBitmapDrawable("avatar_unknown");
    }

    public final String getMiniblog() {
        return this.dV;
    }

    public final long getMiniblogTime() {
        return this.dW;
    }

    public final String getName() {
        return this.dl;
    }

    public final String getNickname() {
        return this.dl;
    }

    public final CharSequence getSubtitle() {
        return this.dV;
    }

    public final String getTimeLabel() {
        if (this.dW <= 0) {
            return null;
        }
        long currentTimeMillis = (System.currentTimeMillis() / 1000) - this.dW;
        if (currentTimeMillis < 1) {
            return C0042c.getString("currently");
        }
        if (currentTimeMillis < 60) {
            return C0042c.getString("moment");
        }
        if (currentTimeMillis < 3600) {
            int i = ((int) currentTimeMillis) / 60;
            return String.valueOf(i) + ' ' + C0042c.getString("minute") + (i == 1 ? "" : C0042c.getString("plural")) + ' ' + C0042c.getString("ago");
        } else if (currentTimeMillis < 86400) {
            int i2 = ((int) currentTimeMillis) / 3600;
            return String.valueOf(i2) + ' ' + C0042c.getString("hour") + (i2 == 1 ? "" : C0042c.getString("plural")) + ' ' + C0042c.getString("ago");
        } else if (currentTimeMillis < 2592000) {
            int i3 = (((int) currentTimeMillis) / 3600) / 24;
            return String.valueOf(i3) + ' ' + C0042c.getString("day") + (i3 == 1 ? "" : C0042c.getString("plural")) + ' ' + C0042c.getString("ago");
        } else {
            int i4 = ((((int) currentTimeMillis) / 3600) / 24) / 30;
            return String.valueOf(i4) + ' ' + C0042c.getString("month") + (i4 == 1 ? "" : C0042c.getString("plural")) + ' ' + C0042c.getString("ago");
        }
    }

    public final String getTitle() {
        return getName();
    }

    public final int getUserID() {
        return this.cU;
    }

    public final boolean hasuserid() {
        return getUserID() > 0;
    }

    public final boolean isGrayScaled() {
        return getUserID() != 0 && this.state == 0;
    }

    public final void setMiniblog(String str) {
        this.dV = str;
    }

    public final void setMiniblogTime(long j) {
        this.dW = j;
    }

    public final void setUserID(int i) {
        this.cU = i;
    }

    public final void updateOnline(boolean z) {
        if (z) {
            this.dU++;
        } else if (this.dU > 0) {
            this.dU--;
        }
        if (this.dU > 0 && this.state != 1) {
            this.state = 1;
            if (this.cn) {
                addSystemMessage(getName() + " " + C0042c.getString("chat_msg_sys_online"));
            }
        }
        if (this.dU == 0) {
            this.state = 0;
            if (this.cn) {
                addSystemMessage(getName() + " " + C0042c.getString("chat_msg_sys_offline"));
            }
        }
    }
}
