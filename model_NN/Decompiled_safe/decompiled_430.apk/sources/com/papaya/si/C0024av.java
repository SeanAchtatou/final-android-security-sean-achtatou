package com.papaya.si;

import com.papaya.social.PPYEntranceView;
import com.papaya.social.PPYSession;
import com.papaya.social.PPYSocial;

/* renamed from: com.papaya.si.av  reason: case insensitive filesystem */
public class C0024av implements PPYSocial.Delegate {
    private /* synthetic */ PPYEntranceView fj;

    public C0024av(PPYEntranceView pPYEntranceView) {
        this.fj = pPYEntranceView;
    }

    public final void onAccountChanged(int i, int i2) {
        if (aC.getInstance().isConnected()) {
            this.fj.eW.setImageUrl(PPYSocial.getAvatarUrlString(PPYSession.getInstance().getUID()));
        }
        if (aC.getInstance().isConnected()) {
            String str = C0061v.bi + "bannernews?uid=" + PPYSession.getInstance().getUID();
            if (this.fj.fe == null) {
                bA unused = this.fj.fe = new bA(C0040bk.createURL(str), false);
            }
            this.fj.fe.setDelegate(this.fj.ff);
            if (this.fj.eV == 0) {
                this.fj.fe.start(true);
                PPYEntranceView.access$108(this.fj);
            }
        }
    }

    public final void onScoreUpdated() {
    }

    public final void onSessionUpdated() {
        if (this.fj.eW.getDrawable() == null && aC.getInstance().isConnected()) {
            this.fj.eW.setImageUrl(PPYSocial.getAvatarUrlString(PPYSession.getInstance().getUID()));
        }
        if (this.fj.fc == null && aC.getInstance().isConnected()) {
            String str = C0061v.bi + "bannernews?uid=" + PPYSession.getInstance().getUID();
            if (this.fj.fe == null) {
                bA unused = this.fj.fe = new bA(C0040bk.createURL(str), false);
            }
            this.fj.fe.setDelegate(this.fj.ff);
            if (this.fj.eV == 0) {
                this.fj.fe.start(true);
                PPYEntranceView.access$108(this.fj);
            }
        }
    }
}
