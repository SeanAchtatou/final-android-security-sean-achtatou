package com.papaya.si;

public abstract class aQ implements Runnable {
    public boolean fz = false;

    public static void cancelTask(aQ aQVar) {
        if (aQVar != null) {
            aQVar.fz = true;
        }
    }

    public void run() {
        if (!this.fz) {
            try {
                runTask();
            } catch (Exception e) {
                X.w(e, "Failed to runTask", new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void runTask();
}
