package com.papaya.si;

import android.graphics.drawable.Drawable;

public final class Q extends D {
    public int dd;
    public String de;
    public int df;
    public int dg;
    public boolean dh;
    public boolean di = false;
    public E<S> dj = new E<>();
    public String name;

    public final void addUser(S s) {
        removeUser(s.cU);
        this.dj.add(s);
        this.df = this.dj.size();
    }

    public final S findUser(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.dj.size()) {
                return null;
            }
            S s = this.dj.get(i3);
            if (s.cU == i) {
                return s;
            }
            i2 = i3 + 1;
        }
    }

    public final Drawable getDefaultDrawable() {
        return C0042c.getBitmapDrawable("chatroom_default");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return C0030ba.format("%s (%d/%d)", this.name, Integer.valueOf(this.df), Integer.valueOf(this.dg));
    }

    public final boolean isGrayScaled() {
        return this.state == 0;
    }

    public final void logout() {
        this.state = 0;
        this.dj.clear();
        if (this.df > 1) {
            this.df--;
        }
    }

    public final S removeUser(int i) {
        S s;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.dj.size()) {
                s = null;
                break;
            } else if (this.dj.get(i3).cU == i) {
                s = this.dj.remove(i3);
                break;
            } else {
                i2 = i3 + 1;
            }
        }
        this.df = this.dj.size();
        return s;
    }
}
