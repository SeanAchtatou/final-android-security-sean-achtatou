package com.papaya.si;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.aw  reason: case insensitive filesystem */
public final class C0025aw {
    private C0062w fA;
    private a fB;
    private final HashMap<Integer, C0059t> fC = new HashMap<>(100);
    private final C0037bh<C0063x> fD = new C0037bh<>(8);

    /* renamed from: com.papaya.si.aw$a */
    class a extends aQ {
        /* synthetic */ a(C0025aw awVar) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        /* access modifiers changed from: protected */
        public final void runTask() {
            C0025aw.this.close();
        }
    }

    public final void addConnectionDelegate(C0063x xVar) {
        if (xVar != null) {
            synchronized (this.fD) {
                if (!this.fD.contains(xVar)) {
                    this.fD.add(xVar);
                }
            }
        }
    }

    public final void cancelDisconnectTask() {
        aQ.cancelTask(this.fB);
        this.fB = null;
    }

    public final void close() {
        if (this.fA != null) {
            this.fA.bu = true;
        }
        if (!(this.fA == null || this.fA.bs == null)) {
            this.fA.bs.close();
            this.fA.bs = null;
        }
        this.fA = null;
    }

    public final boolean dispatchCmd(int i, final Vector vector) {
        final C0059t tVar;
        synchronized (this.fC) {
            tVar = this.fC.get(Integer.valueOf(i));
        }
        if (tVar == null) {
            return false;
        }
        if (tVar instanceof C0031bb) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0059t.this.handleServerResponse(vector);
                }
            });
        } else {
            tVar.handleServerResponse(vector);
        }
        return true;
    }

    public final void fireConnectionEstablished() {
        synchronized (this.fD) {
            int size = this.fD.size();
            for (int i = 0; i < size; i++) {
                final C0063x xVar = this.fD.get(i);
                if (xVar != null) {
                    if (xVar instanceof C0031bb) {
                        C0036bg.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0063x.this.onConnectionEstablished();
                            }
                        });
                    } else {
                        xVar.onConnectionEstablished();
                    }
                }
            }
        }
    }

    public final void fireConnectionLost() {
        synchronized (this.fD) {
            int size = this.fD.size();
            for (int i = 0; i < size; i++) {
                final C0063x xVar = this.fD.get(i);
                if (xVar != null) {
                    if (xVar instanceof C0031bb) {
                        C0036bg.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0063x.this.onConnectionLost();
                            }
                        });
                    } else {
                        xVar.onConnectionLost();
                    }
                }
            }
        }
    }

    public final String getFunFact() {
        return this.fA != null ? this.fA.br : C0042c.getString("base_entry_wait");
    }

    public final boolean isRunning() {
        return this.fA != null && !this.fA.bu;
    }

    public final void registerCmds(C0059t tVar, Integer... numArr) {
        synchronized (this.fC) {
            for (Integer put : numArr) {
                this.fC.put(put, tVar);
            }
        }
    }

    public final void removeConnectionDelegate(C0063x xVar) {
        if (xVar != null) {
            synchronized (this.fD) {
                this.fD.remove(xVar);
            }
        }
    }

    public final void send(int i, Object... objArr) {
        if (this.fA != null) {
            this.fA.send(i, objArr);
        }
    }

    public final void send(List list) {
        if (this.fA != null) {
            this.fA.send(list);
        }
    }

    public final void setPaused(boolean z) {
        if (this.fA != null) {
            this.fA.bq = z;
        }
    }

    public final void start() {
        if (this.fA == null) {
            this.fA = new C0062w(this);
            this.fA.start();
        }
        cancelDisconnectTask();
    }

    public final void startDisconnectTask() {
        aQ.cancelTask(this.fB);
        if (this.fA != null) {
            this.fB = new a(this);
            C0036bg.postDelayed(this.fB, 180000);
        }
    }

    public final void unregisterCmd(C0059t tVar, Integer... numArr) {
        synchronized (this.fC) {
            if (numArr.length == 0) {
                Iterator it = new ArrayList(this.fC.keySet()).iterator();
                while (it.hasNext()) {
                    int intValue = ((Integer) it.next()).intValue();
                    if (this.fC.get(Integer.valueOf(intValue)) == tVar) {
                        this.fC.remove(Integer.valueOf(intValue));
                    }
                }
            } else {
                for (Integer num : numArr) {
                    if (this.fC.get(num) == tVar) {
                        this.fC.remove(num);
                    }
                }
            }
        }
    }
}
