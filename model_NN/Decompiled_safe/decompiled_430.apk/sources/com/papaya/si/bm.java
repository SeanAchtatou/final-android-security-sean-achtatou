package com.papaya.si;

import com.papaya.si.by;
import java.net.URL;
import java.util.HashMap;
import org.json.JSONObject;

public final class bm implements aR, C0031bb, by.a {
    private HashMap<String, String> lK = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, bA> lL = new HashMap<>();
    /* access modifiers changed from: private */
    public bH lM;

    public bm(bH bHVar) {
        this.lM = bHVar;
    }

    /* access modifiers changed from: private */
    public void clearOMT() {
        for (bA cancel : this.lL.values()) {
            cancel.cancel();
        }
        this.lL.clear();
        this.lK.clear();
    }

    public final void cancelRequest(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bA bAVar = (bA) bm.this.lL.remove(str);
                if (bAVar != null) {
                    bAVar.cancel();
                }
            }
        });
    }

    public final void clear() {
        if (C0036bg.isMainThread()) {
            clearOMT();
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    bm.this.clearOMT();
                }
            });
        }
    }

    public final synchronized void connectionFailed(by byVar, int i) {
        bA request = byVar.getRequest();
        if (request instanceof bn) {
            bn bnVar = (bn) request;
            if (bnVar == this.lL.get(bnVar.kL)) {
                this.lL.remove(bnVar.kL);
                if (bnVar.lX && this.lM.getWebView() != null) {
                    this.lM.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", bnVar.kL, 0, C0040bk.escapeJS(bnVar.lY));
                }
            }
        } else if (request instanceof bw) {
            bw bwVar = (bw) request;
            if (bwVar == this.lL.get(bwVar.getID())) {
                this.lL.remove(bwVar.getID());
                if (this.lM.getWebView() != null) {
                    this.lM.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", bwVar.getID(), 0, C0040bk.escapeJS(bwVar.mX.optString("url")));
                }
            }
        }
    }

    public final void connectionFinished(by byVar) {
        bA request = byVar.getRequest();
        if (request instanceof bn) {
            bn bnVar = (bn) request;
            if (bnVar == this.lL.get(bnVar.kL)) {
                this.lL.remove(bnVar.kL);
                if (bnVar.lX && this.lM.getWebView() != null) {
                    this.lK.put(bnVar.kL, C0030ba.utf8String(byVar.getData(), ""));
                    this.lM.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", bnVar.kL, 1, C0040bk.escapeJS(bnVar.lY));
                }
            }
        } else if (request instanceof bw) {
            bw bwVar = (bw) request;
            if (bwVar == this.lL.get(bwVar.getID())) {
                this.lL.remove(bwVar.getID());
                if (this.lM.getWebView() != null) {
                    this.lK.put(bwVar.getID(), C0030ba.utf8String(byVar.getData(), ""));
                }
                this.lM.getWebView().callJSFunc("ppy_onAjaxFinished('%s', %d, '%s')", bwVar.getID(), 1, C0040bk.escapeJS(bwVar.mX.optString("url")));
            }
        }
    }

    public final String newRemoveContent(String str) {
        return this.lK.remove(str);
    }

    public final void startPost(final JSONObject jSONObject) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bw bwVar = new bw(jSONObject);
                bwVar.setRequireSid(bm.this.lM.getWebView().isRequireSid());
                bwVar.setDelegate(bm.this);
                bm.this.lL.put(bwVar.getID(), bwVar);
                bwVar.start(false);
            }
        });
    }

    public final void startRequest(String str, String str2, boolean z, String str3, int i, String str4, int i2) {
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        final int i3 = i;
        final int i4 = i2;
        final boolean z2 = z;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                String str = str5 == null ? "" : str5;
                bm.this.cancelRequest(str);
                bp webView = bm.this.lM.getWebView();
                if (webView != null) {
                    String str2 = str6;
                    if (!(str7 == null || str8 == null)) {
                        StringBuilder append = C0030ba.acquireStringBuilder(str6.length()).append(str6);
                        if (str6.contains("?")) {
                            append.append('&');
                        } else {
                            append.append('?');
                        }
                        append.append("__db_cache=");
                        try {
                            append.append(C0040bk.encodeUriComponent(new JSONObject().put("name", str7).put("scope", i3).put("key", str8).put("life", i4).toString()));
                        } catch (Exception e) {
                            X.e(e, "Failed to compose db cache json", new Object[0]);
                        }
                        str2 = C0030ba.releaseStringBuilder(append);
                    }
                    URL createURL = C0040bk.createURL(str2, webView.getPapayaURL());
                    if (createURL != null) {
                        bn bnVar = new bn();
                        bnVar.setUrl(createURL);
                        bnVar.setConnectionType(1);
                        bnVar.setDelegate(bm.this);
                        bnVar.setCacheable(false);
                        bnVar.kL = str;
                        bnVar.lX = z2;
                        bnVar.lY = str6;
                        bnVar.setRequireSid(webView.isRequireSid());
                        bm.this.lL.put(str, bnVar);
                        bnVar.start(false);
                        return;
                    }
                    X.e("Ajax URL is null, %s, %s", str, str6);
                }
            }
        });
    }
}
