package com.papaya.si;

import com.papaya.achievement.PPYAchievement;
import com.papaya.achievement.PPYAchievementDelegate;
import com.papaya.si.by;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public final class aE extends bA implements C0031bb, by.a {
    private PPYAchievementDelegate gw;
    private PPYAchievement gx;

    public aE(PPYAchievement pPYAchievement, PPYAchievementDelegate pPYAchievementDelegate) {
        this.gx = pPYAchievement;
        this.gw = pPYAchievementDelegate;
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
    }

    public aE(PPYAchievementDelegate pPYAchievementDelegate) {
        this.gw = pPYAchievementDelegate;
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
    }

    public final void connectionFailed(by byVar, int i) {
        if (this.gw != null) {
            this.gw.onListFailed();
        }
    }

    public final void connectionFinished(by byVar) {
        try {
            if (this.gw != null) {
                String url = byVar.getRequest().getUrl().toString();
                if (url.contains("json_achievementlist")) {
                    aA.getInstance().getAchievementDatabase().clearAchievements();
                    JSONArray jSONArray = C0040bk.parseJsonObject(C0030ba.utf8String(byVar.getData(), null)).getJSONArray("items");
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i);
                        arrayList.add(new PPYAchievement(jSONObject.getInt("aid"), jSONObject.getString("title"), jSONObject.getString("desc"), jSONObject.getInt("secret") != 0, jSONObject.getInt("unlock") != 0));
                    }
                    this.gw.onListSuccess(arrayList);
                } else if (url.contains("json_loadachievement")) {
                    JSONObject parseJsonObject = C0040bk.parseJsonObject(C0030ba.utf8String(byVar.getData(), null));
                    this.gw.onLoadSuccess(new PPYAchievement(parseJsonObject.getInt("aid"), parseJsonObject.getString("title"), parseJsonObject.getString("desc"), parseJsonObject.getInt("secret") != 0, parseJsonObject.getInt("unlock") != 0));
                } else if (this.gx == null) {
                } else {
                    if (url.contains("achievementicon")) {
                        this.gw.onDownloadIconSuccess(byVar.getBitmap());
                        C0002a.getWebCache().saveCacheWebFile("achievementicon?id=" + this.gx.getId(), byVar.getData());
                    } else if (url.contains("json_unlock")) {
                        aA.getInstance().getAchievementDatabase().deleteAchievement(this.gx.getId());
                        this.gw.onUnlockSuccess(Boolean.valueOf(C0040bk.parseJsonObject(C0030ba.utf8String(byVar.getData(), null)).getInt("ret") != 0));
                    }
                }
            }
        } catch (Exception e) {
            X.e(e, "Failed in PPYUrlAchievementRequest", new Object[0]);
        }
    }

    public final void downloadicon() {
        if (this.gx != null) {
            this.url = C0040bk.createURL("achievementicon?id=" + this.gx.getId());
            start(true);
        }
    }

    public final void getAchievementList() {
        this.url = C0040bk.createURL("json_achievementlist?all=1&unlock=" + aA.getInstance().getAchievementDatabase().stringList());
        start(true);
    }

    public final void loadAchievement(int i) {
        this.url = C0040bk.createURL("json_loadachievement?aid=" + i);
        start(true);
    }

    public final void unlock() {
        if (this.gx != null) {
            aA.getInstance().getAchievementDatabase().addAchievement(this.gx.getId());
            this.url = C0040bk.createURL("json_unlock?aid=" + this.gx.getId());
            start(true);
        }
    }
}
