package com.papaya.si;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.papaya.si.be  reason: case insensitive filesystem */
public final class C0034be {
    private List<StringBuilder> hl = new ArrayList();

    private void debug() {
        int i = 0;
        for (StringBuilder capacity : this.hl) {
            i += capacity.capacity();
        }
        Log.d("PPYSocial", "pool size: " + i);
    }

    public final synchronized StringBuilder acquire(int i) {
        int i2;
        StringBuilder remove;
        int i3 = 0;
        synchronized (this) {
            int size = this.hl.size();
            while (true) {
                if (i3 >= size) {
                    i2 = -1;
                    break;
                } else if (this.hl.get(i3).capacity() >= i) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            remove = i2 != -1 ? this.hl.remove(i2) : this.hl.isEmpty() ? new StringBuilder(i) : this.hl.remove(0);
            remove.setLength(0);
        }
        return remove;
    }

    public final synchronized void clear() {
        this.hl.clear();
    }

    public final synchronized String release(StringBuilder sb) {
        if (!this.hl.contains(sb)) {
            this.hl.add(sb);
        }
        return sb.toString();
    }

    public final synchronized void releaseOnly(StringBuilder sb) {
        if (!this.hl.contains(sb)) {
            this.hl.add(sb);
        }
    }
}
