package com.papaya.si;

import android.net.Uri;
import com.papaya.purchase.PPYPayment;
import com.papaya.purchase.PPYPaymentDelegate;
import com.papaya.si.by;
import org.json.JSONObject;

public final class aF extends bA implements C0031bb, by.a {
    private PPYPaymentDelegate eL;
    private PPYPayment gy;
    private long gz = System.currentTimeMillis();

    public aF(PPYPayment pPYPayment, PPYPaymentDelegate pPYPaymentDelegate) {
        this.gy = pPYPayment;
        this.eL = pPYPaymentDelegate;
        if (this.eL == null) {
            this.eL = pPYPayment.getDelegate();
        }
        setCacheable(false);
        setDispatchable(true);
        setRequireSid(true);
        setDelegate(this);
        StringBuilder sb = new StringBuilder("json_iap?x=");
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.gy.getName());
            jSONObject.put("desc", this.gy.getDescription());
            jSONObject.put("ppys", this.gy.getPapayas());
            if (this.gy.getPayload() != null) {
                jSONObject.put("payload", this.gy.getPayload().toString());
            }
            jSONObject.put("secret", (int) (((double) System.currentTimeMillis()) / 1000.0d));
        } catch (Exception e) {
            X.e("Failed to construct payment request: " + e, new Object[0]);
        }
        sb.append(Uri.encode(C0040bk.encrypt(jSONObject.toString())));
        this.url = C0040bk.createURL(sb.toString());
        this.gz = System.currentTimeMillis();
    }

    public final void connectionFailed(by byVar, int i) {
        if (this.eL != null) {
            this.eL.onPaymentFailed(this.gy, 0, "Failed to finish payment");
        }
    }

    public final void connectionFinished(by byVar) {
        if (this.eL != null) {
            try {
                JSONObject parseJsonObject = C0040bk.parseJsonObject(C0040bk.decrypt(C0030ba.utf8String(byVar.getData(), null)));
                int optInt = parseJsonObject.optInt("status", 0);
                if (optInt > 0) {
                    this.gy.setTransactionID(parseJsonObject.optString("tid", null));
                    this.gy.setReceipt(parseJsonObject.optString("receipt", null));
                    this.eL.onPaymentFinished(this.gy);
                    return;
                }
                this.eL.onPaymentFailed(this.gy, optInt, parseJsonObject.optString("error", "Invalid server response"));
            } catch (Exception e) {
                X.w(e, "Failed to process", new Object[0]);
                this.eL.onPaymentFailed(this.gy, 0, "Internal error: " + e);
            }
        }
    }

    public final boolean isExpired() {
        return System.currentTimeMillis() - this.gz > 15000;
    }
}
