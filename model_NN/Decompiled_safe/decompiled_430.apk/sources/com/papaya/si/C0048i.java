package com.papaya.si;

import android.content.Context;

/* renamed from: com.papaya.si.i  reason: case insensitive filesystem */
public final class C0048i {
    private static C0058s ab;

    private C0048i() {
    }

    public static void destroy() {
        if (ab != null) {
            try {
                dispatch();
                ab.stop();
            } catch (Exception e) {
                X.w("Failed to stop tracker: " + e, new Object[0]);
            }
        }
        ab = null;
    }

    public static void dispatch() {
        try {
            if (ab != null) {
                ab.dispatch();
            }
        } catch (Exception e) {
            X.w("Failed to dispatch:" + e, new Object[0]);
        }
    }

    public static void initialize(Context context) {
        try {
            C0058s instance = C0058s.getInstance();
            ab = instance;
            instance.start(C0028az.fN, 30, context);
            ab.dispatch();
        } catch (Exception e) {
            X.w(e, "Failed to enable google analytics", new Object[0]);
        }
    }

    public static void trackEvent(String str, String str2, String str3, int i) {
        if (ab != null) {
            try {
                ab.trackEvent(str, str2, str3, i);
            } catch (Exception e) {
                X.w("Failed to track event:" + e, new Object[0]);
            }
        }
    }

    public static void trackPageView(String str) {
        if (ab != null) {
            try {
                ab.trackPageView(str);
            } catch (Exception e) {
                X.w("Failed to track page:" + e, new Object[0]);
            }
        }
    }
}
