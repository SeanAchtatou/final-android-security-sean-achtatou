package com.papaya.si;

import com.papaya.view.Action;
import java.util.ArrayList;

public final class M extends E<L> {
    public M() {
        setName(C0042c.getString("friend_chatgroup"));
        setReserveGroupHeader(true);
        this.cq = new ArrayList();
        this.cq.add(new Action(0, null, C0042c.getString("create")));
        this.cq.add(new Action(1, null, C0042c.getString("search")));
    }

    public final L findByGroupID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.co.size()) {
                return null;
            }
            L l = (L) this.co.get(i3);
            if (l.cQ == i) {
                return l;
            }
            i2 = i3 + 1;
        }
    }
}
