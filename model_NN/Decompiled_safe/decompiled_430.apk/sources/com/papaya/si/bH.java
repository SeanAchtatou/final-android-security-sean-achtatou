package com.papaya.si;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.papaya.analytics.PPYReferralReceiver;
import com.papaya.base.EntryActivity;
import com.papaya.service.AppAccount;
import com.papaya.service.AppAccountManager;
import com.papaya.service.AppManager;
import com.papaya.si.C0065z;
import com.papaya.si.bp;
import com.papaya.social.BillingChannel;
import com.papaya.social.PPYLocalScore;
import com.papaya.social.PPYUser;
import com.papaya.view.AdWrapperView;
import com.papaya.view.AvatarBar;
import com.papaya.view.CustomDialog;
import com.papaya.view.HorizontalBar;
import com.papaya.view.JsonConfigurable;
import com.papaya.view.MaskLoadingView;
import com.papaya.view.WebAlertDialog;
import com.papaya.view.WebDatePickerDialogWrapper;
import com.papaya.view.WebDialog;
import com.papaya.view.WebListDialogWrapper;
import com.papaya.view.WebMenuView;
import com.papaya.view.WebMixedInputDialog;
import com.papaya.view.WebMultipleInputView;
import com.papaya.view.WebPicturesDialog;
import com.papaya.view.WebSelectorDialog;
import com.papaya.web.ExternalWebActivity;
import com.papaya.web.WebActivity;
import com.papaya.web.WebViewController;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bH implements C0063x, HorizontalBar.Delegate {
    private bm nD;
    /* access modifiers changed from: private */
    public bK nE;
    private HashSet<String> nF = new HashSet<>();
    private HashMap<String, C0014al> nG = new HashMap<>();
    /* access modifiers changed from: private */
    public bp nH;
    /* access modifiers changed from: private */
    public HashMap<String, HorizontalBar> nI = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, WebMenuView> nJ = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, AvatarBar> nK = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, ProgressBar> nL = new HashMap<>();
    /* access modifiers changed from: private */
    public ProgressDialog nM = null;
    private String nN = null;
    private C0059t nO = new bI(this);

    public bH(bp bpVar) {
        this.nH = bpVar;
        this.nD = new bm(this);
        this.nE = new bK();
        C0042c.A.addConnectionDelegate(this);
    }

    static /* synthetic */ LinearLayout access$700(bH bHVar) {
        return null;
    }

    static /* synthetic */ String access$802(bH bHVar, String str) {
        return str;
    }

    static /* synthetic */ String access$902(bH bHVar, String str) {
        return str;
    }

    /* access modifiers changed from: private */
    public void addView(int i, String str, Object obj) {
        if (!str.startsWith("___")) {
            this.nE.addView(i, str, obj);
        } else if (str.length() > 3) {
            this.nF.add(str);
            char charAt = str.charAt(3);
            if (charAt == 'c') {
                WebViewController controller = getController();
                if (controller != null) {
                    controller.getUIHelper().addView(i, str, obj);
                }
            } else if (charAt == 'a') {
                C0029b.getUIHelper().addView(i, str, obj);
            }
        } else {
            X.e("invalid ___ viewId %s", str);
        }
    }

    /* access modifiers changed from: private */
    public ViewGroup findParent(int i) {
        bp bpVar = this.nH;
        if (bpVar == null) {
            return null;
        }
        switch (i) {
            case 0:
                return bpVar;
            case 1:
                try {
                    return (ViewGroup) bpVar.getParent();
                } catch (Exception e) {
                    X.w(e, "Failed in findParent", new Object[0]);
                    return null;
                }
            case 2:
                return (ViewGroup) bpVar.getParent();
            case 3:
                return (ViewGroup) bpVar.getParent();
            case 4:
                return (ViewGroup) bpVar.getParent();
            case 5:
                return (ViewGroup) bpVar.getParent();
            case 6:
                return (ViewGroup) bpVar.getParent();
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public Object findView(int i, String str) {
        if (!str.startsWith("___")) {
            return i == 10 ? this.nH : this.nE.findView(i, str);
        }
        if (str.length() <= 3) {
            return null;
        }
        char charAt = str.charAt(3);
        if (charAt == 'c') {
            WebViewController controller = getController();
            if (controller != null) {
                return controller.getUIHelper().findView(i, str);
            }
            return null;
        } else if (charAt == 'a') {
            return C0029b.getUIHelper().findView(i, str);
        } else {
            X.e("invalid ___ viewId %s", str);
            return null;
        }
    }

    private C0014al openDatabase(String str, int i) {
        WebViewController controller = getController();
        switch (i) {
            case 0:
                X.w("Never use Scope_Undefined", new Object[0]);
                return null;
            case 1:
                C0014al alVar = this.nG.get(str);
                if (alVar != null) {
                    return alVar;
                }
                C0014al openMemoryDatabase = C0014al.openMemoryDatabase();
                openMemoryDatabase.setDbId(str);
                openMemoryDatabase.setScope(i);
                this.nG.put(str, openMemoryDatabase);
                return openMemoryDatabase;
            case 2:
                if (controller != null) {
                    return controller.openDatabase(str);
                }
                return null;
            default:
                return C0015am.getInstance().openDatabase(str, i);
        }
    }

    /* access modifiers changed from: private */
    public void removeView(int i, String str) {
        if (!str.startsWith("___")) {
            this.nE.removeView(i, str);
        } else if (str.length() > 3) {
            char charAt = str.charAt(3);
            if (charAt == 'c') {
                WebViewController controller = getController();
                if (controller != null) {
                    controller.getUIHelper().removeView(i, str);
                }
            } else if (charAt == 'a') {
                C0029b.getUIHelper().removeView(i, str);
            } else {
                X.e("invalid ___ viewId %s", str);
            }
        }
    }

    public final String DUID() {
        return C0035bf.hB;
    }

    public final void acceptChallenge_(final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (ownerActivity != null) {
                    new aI(ownerActivity).acceptChallenge(i);
                }
            }
        });
    }

    public final void activityViewCreate_style_position_(final String str, final int i, final int i2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                if (bH.this.findView(4, str) == null) {
                    bp access$000 = bH.this.nH;
                    Activity ownerActivity = bH.this.getOwnerActivity();
                    if (ownerActivity != null && access$000 != null) {
                        bH.this.addView(4, str, new MaskLoadingView(ownerActivity, i, i2));
                    }
                }
            }
        });
    }

    public final void addIMAccount(final int i, final String str, final String str2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0042c.getSession().addIMAccount(i, str, str2);
            }
        });
    }

    public final void addOverlay(double d, double d2, String str, int i) {
        final double d3 = d;
        final double d4 = d2;
        final String str2 = str;
        final int i2 = i;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    controller.addOverlay((int) (d3 * 1000000.0d), (int) (d4 * 1000000.0d), str2, i2);
                }
            }
        });
    }

    public final void ajaxCancel_(String str) {
        this.nD.cancelRequest(str);
    }

    public final void ajaxClear() {
        this.nD.clear();
    }

    public final void ajaxFinished_(String str) {
    }

    public final String ajaxGet_convert_(String str, int i) {
        String newRemoveContent = this.nD.newRemoveContent(str);
        try {
            bp bpVar = this.nH;
            return (newRemoveContent == null || i <= 0 || bpVar == null) ? newRemoveContent : C0002a.getWebCache().createLocalRefHtml(newRemoveContent, bpVar.getPapayaURL(), true, bpVar.isRequireSid());
        } catch (Exception e) {
            X.e(e, "Failed in ajaxGet_convert_", new Object[0]);
            return newRemoveContent;
        }
    }

    public final void ajaxStart_url_callback_(String str, String str2, int i) {
        this.nD.startRequest(str, str2, i > 0, null, 0, null, 0);
    }

    public final void ajaxStart_url_callback_db_scope_key_life_(String str, String str2, int i, String str3, int i2, String str4, int i3) {
        this.nD.startRequest(str, str2, i > 0, str3, i2, str4, i3);
    }

    public final void alertShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                bp access$000 = bH.this.nH;
                if (ownerActivity != null && access$000 != null) {
                    WebAlertDialog webAlertDialog = new WebAlertDialog(ownerActivity);
                    webAlertDialog.setWebView(access$000);
                    webAlertDialog.refreshWithCtx(C0040bk.parseJsonObject(str));
                    webAlertDialog.show();
                }
            }
        });
    }

    public final String androidID() {
        return C0035bf.ANDROID_ID;
    }

    public final void animateTo(double d, double d2) {
        final double d3 = d;
        final double d4 = d2;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    controller.animateTo((int) (d3 * 1000000.0d), (int) (d4 * 1000000.0d));
                }
            }
        });
    }

    public final String appName() {
        try {
            return C0061v.bk.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public final void autoLogin() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0002a.getWebCache().tryLogin();
            }
        });
    }

    public final void avatarBarCreate_json_x_y_w_h_(String str, String str2, int i, int i2, int i3, int i4) {
        final String str3 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        final String str4 = str2;
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (access$000 != null && ownerActivity != null) {
                    AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str3);
                    if (avatarBar == null) {
                        avatarBar = new AvatarBar(ownerActivity);
                        avatarBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(ownerActivity, i5, i6, i7, i8));
                        avatarBar.setWebView(access$000);
                        bH.this.nK.put(str3, avatarBar);
                    } else {
                        avatarBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(ownerActivity, i5, i6, i7, i8));
                    }
                    avatarBar.refreshWithCtx(C0040bk.parseJsonObject(str4), access$000.getPapayaURL());
                }
            }
        });
    }

    public final void avatarBarCreate_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        X.warnIncomplete();
    }

    public final void avatarBarDispose_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bH.this.nK.remove(str);
            }
        });
    }

    public final void avatarBarHide_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                C0036bg.removeFromSuperView(bH.this.nK.get(str));
            }
        });
    }

    public final void avatarBarReload_json_(final String str, final String str2) {
        C0036bg.post(new Runnable() {
            public final void run() {
                AvatarBar avatarBar;
                bp access$000 = bH.this.nH;
                if (access$000 != null && (avatarBar = (AvatarBar) bH.this.nK.get(str)) != null) {
                    avatarBar.refreshWithCtx(C0040bk.parseJsonObject(str2), access$000.getPapayaURL());
                }
            }
        });
    }

    public final int avatarBarSelectedIndex_(final String str) {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str);
                if (avatarBar != null) {
                    return Integer.valueOf(avatarBar.getSelectedIndex());
                }
                return -1;
            }
        }, -1)).intValue();
    }

    public final void avatarBarSet_index_selected_(final String str, final int i, final int i2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str);
                if (avatarBar != null) {
                    avatarBar.selectButton(i, i2 > 0);
                }
            }
        });
    }

    public final void avatarBarSet_scroll_animated_(final String str, final int i, final int i2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str);
                if (avatarBar != null) {
                    avatarBar.scrollToItem(i, i2 > 0);
                }
            }
        });
    }

    public final void avatarBarSet_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.post(new Runnable() {
            public final void run() {
                AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str2);
                if (avatarBar != null) {
                    avatarBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(bH.this.getOwnerActivity(), i5, i6, i7, i8));
                }
            }
        });
    }

    public final void avatarBarShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    C0036bg.addView(access$000, (AvatarBar) bH.this.nK.get(str));
                }
            }
        });
    }

    public final int avatarBarVisible_(final String str) {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                AvatarBar avatarBar = (AvatarBar) bH.this.nK.get(str);
                if (avatarBar == null) {
                    return 0;
                }
                return Integer.valueOf(avatarBar.getParent() != null ? 1 : 0);
            }
        }, 0)).intValue();
    }

    public final void back() {
        back(0);
    }

    public final void back(final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (ownerActivity instanceof WebActivity) {
                    ((WebActivity) ownerActivity).priaBack(i);
                }
            }
        });
    }

    public final void barButtonClicked(HorizontalBar horizontalBar, int i, JSONObject jSONObject) {
        bp bpVar = this.nH;
        if (bpVar != null) {
            String jsonString = C0040bk.getJsonString(jSONObject, "action");
            if (jsonString == null || jsonString.length() <= 0) {
                bpVar.callJSFunc("onHoribarTapped('%s', %d)", horizontalBar.getViewId(), Integer.valueOf(i));
                return;
            }
            bpVar.callJS(jsonString);
        }
    }

    public final int billingChannels() {
        try {
            return aA.getInstance().getSocialConfig().getBillingChannels();
        } catch (Exception e) {
            X.e(e, "Failed to get billingChannels", new Object[0]);
            return BillingChannel.ALL;
        }
    }

    public final String cachePath_(String str) {
        try {
            bp bpVar = this.nH;
            if (str.startsWith("file:///android_asset/") || str.startsWith(bE.ny) || bpVar == null) {
                return str;
            }
            URL createURL = C0040bk.createURL(str, bpVar.getPapayaURL());
            if (createURL != null) {
                String cacheOrBundleContentUri = C0002a.getWebCache().cacheOrBundleContentUri(createURL.toString(), str);
                return cacheOrBundleContentUri == null ? str : cacheOrBundleContentUri;
            }
            return str;
        } catch (Exception e) {
            X.w("Failed to find cache path for %s", str);
        }
    }

    public final String cacheUri_(String str) {
        return cachePath_(str);
    }

    public final void callPPY_(String str, String str2) {
    }

    public final int canStartGPS() {
        WebViewController controller = getController();
        if (controller != null) {
            return controller.canStartGPS() ? 1 : 0;
        }
        return 0;
    }

    public final void clearBrowserHistory() {
    }

    public final void clearLocalUnlockAchievement() {
        aA.getInstance().getAchievementDatabase().clearAchievements();
    }

    public final void clearSessionState() {
        if (C0042c.A != null) {
            C0042c.A.unregisterCmd(this.nO, new Integer[0]);
        }
        this.nD.clear();
        WebViewController controller = getController();
        if (controller != null) {
            try {
                controller.getUIHelper().removeNonPersistFromSuperview();
                controller.hideMap();
            } catch (Exception e) {
                X.w(e, "failed to remove view from super", new Object[0]);
            }
        }
        G.hideButton(this.nH);
        C0029b.getUIHelper().removeNonPersistentFromSuperviewInSet(this.nF);
        this.nF.clear();
        this.nE.clear();
        for (C0014al close : this.nG.values()) {
            close.close();
        }
        this.nG.clear();
        for (HorizontalBar removeFromSuperView : this.nI.values()) {
            C0036bg.removeFromSuperView(removeFromSuperView);
        }
        this.nI.clear();
        for (WebMenuView removeFromSuperView2 : this.nJ.values()) {
            C0036bg.removeFromSuperView(removeFromSuperView2);
        }
        this.nJ.clear();
        for (AvatarBar next : this.nK.values()) {
            C0036bg.removeFromSuperView(next);
            next.clearResources();
        }
        this.nK.clear();
        for (ProgressBar removeFromSuperView3 : this.nL.values()) {
            C0036bg.removeFromSuperView(removeFromSuperView3);
        }
        this.nL.clear();
        if (controller != null && controller.oP != null) {
            controller.oP.removeFromSuperView();
        }
    }

    public final int clientType() {
        return 0;
    }

    public final void close() {
        clearSessionState();
        this.nH = null;
    }

    public final String convertHtml_(String str) {
        try {
            bp bpVar = this.nH;
            if (!(str == null || bpVar == null)) {
                return C0002a.getWebCache().createLocalRefHtml(str, bpVar.getPapayaURL(), true, bpVar.isRequireSid());
            }
        } catch (Exception e) {
            X.e(e, "Failed in convertHtml_", new Object[0]);
        }
        return str;
    }

    public final void d_(String str) {
        X.i("js log %s", str);
    }

    public final void datePickerShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    new WebDatePickerDialogWrapper(access$000, C0040bk.parseJsonObject(str)).getDialog().show();
                }
            }
        });
    }

    public final int db_scope_(String str, int i) {
        return openDatabase(str, i) != null ? 1 : 0;
    }

    public final String db_scope_key_(String str, int i, String str2) {
        C0014al openDatabase = openDatabase(str, i);
        if (openDatabase != null) {
            return openDatabase.newKV(str2);
        }
        return null;
    }

    public final void db_scope_key_value_(String str, int i, String str2, String str3) {
        C0014al openDatabase = openDatabase(str, i);
        if (openDatabase != null) {
            openDatabase.update("update kv set value=? where key=?", str3, str2);
        }
    }

    public final void db_scope_key_value_life_(String str, int i, String str2, String str3, int i2) {
        C0014al openDatabase = openDatabase(str, i);
        if (openDatabase != null) {
            openDatabase.kvSave(str2, str3, i2);
        }
    }

    public final String db_scope_query_arguments_(String str, int i, String str2, String str3) {
        C0014al openDatabase = openDatabase(str, i);
        if (openDatabase == null) {
            return null;
        }
        Object[][] newQueryResult = openDatabase.newQueryResult(str2, C0040bk.parseJsonAsArray(str3));
        JSONArray jSONArray = new JSONArray();
        for (Object[] objArr : newQueryResult) {
            JSONArray jSONArray2 = new JSONArray();
            jSONArray.put(jSONArray2);
            for (Object put : objArr) {
                jSONArray2.put(put);
            }
        }
        return jSONArray.toString();
    }

    public final int db_scope_update_arguments_(String str, int i, String str2, String str3) {
        C0014al openDatabase = openDatabase(str, i);
        if (openDatabase != null) {
            return openDatabase.update(str2, C0040bk.parseJsonAsArray(str3)) ? 1 : 0;
        }
        return 0;
    }

    public final String decrypt(String str) {
        try {
            return C0030ba.isEmpty(str) ? "" : C0040bk.decrypt(str);
        } catch (Exception e) {
            return "";
        }
    }

    public final void enableMenu_(final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bp webView = bH.this.getWebView();
                if (webView != null) {
                    webView.setMenuEnabled(i > 0);
                }
            }
        });
    }

    public final String encrypt(String str) {
        try {
            return C0040bk.encrypt(C0030ba.isEmpty(str) ? "" : str);
        } catch (Exception e) {
            return "";
        }
    }

    public final void exchangeChipsViewCreate_papayas_(String str, int i) {
        X.warnIncomplete();
    }

    public final void exchangeChipsViewSet_papayas_(String str, int i) {
        X.warnIncomplete();
    }

    public final void exit() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0029b.finishAllActivities();
            }
        });
    }

    public final String facebookAppID() {
        return C0065z.a.getAppId();
    }

    public final void facebookLogin() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0065z.a.login(bH.this.getOwnerActivity());
            }
        });
    }

    public final int facebookSessionValid() {
        return C0065z.a.isSessionValid() ? 1 : 0;
    }

    public final String facebookToken() {
        return C0065z.a.getAccessToken();
    }

    public final void ga_track_event(final String str, final String str2, final String str3, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0048i.trackEvent(str, str2, str3, i);
            }
        });
    }

    public final void ga_track_page(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0048i.trackPageView(str);
            }
        });
    }

    public final int gameDownloadExists() {
        return 0;
    }

    public final void gameDownloadShow() {
    }

    public final int gameDownload_lang_name_(int i, String str, String str2) {
        return -1;
    }

    public final int gameVersion_lang_(int i, String str) {
        return -1;
    }

    public final String getAppName(String str) {
        try {
            return AppManager.getName(str);
        } catch (Exception e) {
            return null;
        }
    }

    public final WebViewController getController() {
        bp bpVar = this.nH;
        if (bpVar != null) {
            return bpVar.getController();
        }
        return null;
    }

    public final String getCurrentPos() {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("");
        jSONArray.put("");
        jSONArray.put("");
        Location position = C0020ar.getInstance().getPosition();
        if (position != null) {
            try {
                jSONArray.put(0, position.getLatitude() + "");
                jSONArray.put(1, position.getLongitude() + "");
                jSONArray.put(2, C0020ar.getInstance().getAddress(position));
            } catch (JSONException e) {
                X.w("failed to get current pos:%s", e.toString());
            }
        }
        return jSONArray.toString();
    }

    public final String getLocalUnlockedAchievement() {
        return aA.getInstance().getAchievementDatabase().stringList();
    }

    public final Activity getOwnerActivity() {
        if (this.nH != null) {
            return this.nH.getOwnerActivity();
        }
        return null;
    }

    public final String getPhoneNum() {
        return C0035bf.hE;
    }

    public final String getReferralValue(String str, String str2) {
        try {
            return PPYReferralReceiver.getReferrerValue(C0042c.getApplicationContext(), str, str2);
        } catch (Exception e) {
            X.w(e, "Failed to getReferralValue", new Object[0]);
            return "";
        }
    }

    public final String getReferrals() {
        try {
            return PPYReferralReceiver.getReferralJSON(C0042c.getApplicationContext()).toString();
        } catch (Exception e) {
            X.w(e, "Failed in getReferrals", new Object[0]);
            return new JSONObject().toString();
        }
    }

    public final String getReferrer() {
        try {
            return PPYReferralReceiver.getReferrer(C0042c.getApplicationContext());
        } catch (Exception e) {
            X.w(e, "Failed to getReferrer", new Object[0]);
            return "";
        }
    }

    public final String getRequestJson() {
        return this.nN;
    }

    public final String getSystemInfo() {
        JSONObject jSONObject = new JSONObject();
        C0035bf.getSystemInfo(jSONObject, C0042c.getApplicationContext());
        return jSONObject.toString();
    }

    public final EntryActivity getTabBarActivity() {
        Activity ownerActivity;
        bp bpVar = this.nH;
        if (bpVar == null || (ownerActivity = bpVar.getOwnerActivity()) == null || !(ownerActivity.getParent() instanceof EntryActivity)) {
            return null;
        }
        return (EntryActivity) ownerActivity.getParent();
    }

    public final String getTeleInfo() {
        JSONObject jSONObject = new JSONObject();
        C0035bf.getTelephonyInfo(jSONObject, C0042c.getApplicationContext());
        return jSONObject.toString();
    }

    public final bp getWebView() {
        return this.nH;
    }

    public final String getWifiMac() {
        return C0035bf.hD;
    }

    public final void go(String str) {
        go(str, null, null);
    }

    public final void go(String str, String str2) {
        go(str, str2, null);
    }

    public final void go(final String str, final String str2, final String str3) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0029b.openPRIALink(bH.this.getOwnerActivity(), str, str2, true, str3);
            }
        });
    }

    public final int hasTabbar() {
        Activity ownerActivity = getOwnerActivity();
        return (ownerActivity == null || !(ownerActivity.getParent() instanceof EntryActivity)) ? 0 : 1;
    }

    public final String histories() {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                JSONArray jSONArray = new JSONArray();
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    Iterator<bG> it = controller.getHistories().iterator();
                    while (it.hasNext()) {
                        bG next = it.next();
                        if (next.getURL() != null) {
                            jSONArray.put(next.getURL().toString());
                        } else {
                            jSONArray.put("");
                        }
                    }
                }
                return jSONArray.toString();
            }
        }, "[]");
    }

    public final void historyClear_(final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                bp access$000 = bH.this.nH;
                if (controller != null && access$000 != null) {
                    controller.clearHistory(i, access$000);
                }
            }
        });
    }

    public final int historyCount() {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                WebViewController controller = bH.this.getController();
                return Integer.valueOf(controller == null ? 0 : controller.getHistories().size());
            }
        }, 0)).intValue();
    }

    public final int historyIndex() {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                WebViewController controller = bH.this.getController();
                bp access$000 = bH.this.nH;
                if (controller == null || access$000 == null) {
                    return -1;
                }
                return Integer.valueOf(controller.historyIndex(access$000));
            }
        }, -1)).intValue();
    }

    public final void horibarCreate_json_x_y_w_h_(String str, String str2, int i, int i2, int i3, int i4) {
        final String str3 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        final String str4 = str2;
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str3);
                    if (horizontalBar == null) {
                        horizontalBar = new HorizontalBar(access$000.getOwnerActivity(), C0036bg.rawAbsoluteLayoutParams(access$000.getOwnerActivity(), i5, i6, i7, i8));
                        horizontalBar.setDelegate(bH.this);
                        bH.this.nI.put(str3, horizontalBar);
                    } else {
                        horizontalBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(access$000.getOwnerActivity(), i5, i6, i7, i8));
                    }
                    horizontalBar.refreshWithCtx(C0040bk.parseJsonObject(str4));
                }
            }
        });
    }

    public final void horibarCreate_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                if (bH.this.findView(6, str2) == null) {
                    bp access$000 = bH.this.nH;
                    Activity ownerActivity = bH.this.getOwnerActivity();
                    if (access$000 != null && ownerActivity != null) {
                        HorizontalBar horizontalBar = new HorizontalBar(ownerActivity, C0036bg.rawAbsoluteLayoutParams(ownerActivity, i5, i6, i7, i8));
                        horizontalBar.setDelegate(bH.this);
                        horizontalBar.setViewId(str2);
                        bH.this.addView(6, str2, horizontalBar);
                    }
                }
            }
        });
    }

    public final void horibarDispose_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str);
                if (horizontalBar != null) {
                    C0036bg.removeFromSuperView(horizontalBar);
                    bH.this.nI.remove(str);
                }
            }
        });
    }

    public final void horibarHide_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str);
                if (horizontalBar != null) {
                    C0036bg.removeFromSuperView(horizontalBar);
                }
            }
        });
    }

    public final void horibarReload_json_(final String str, final String str2) {
        C0036bg.post(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str);
                if (horizontalBar != null) {
                    horizontalBar.refreshWithCtx(C0040bk.parseJsonObject(str2));
                }
            }
        });
    }

    public final void horibarSet_active_(final String str, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.findView(6, str);
                if (horizontalBar != null) {
                    horizontalBar.setActiveButton(i);
                }
            }
        });
    }

    public final void horibarSet_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.post(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str2);
                if (horizontalBar != null) {
                    horizontalBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(bH.this.getOwnerActivity(), i5, i6, i7, i8));
                } else {
                    X.w("unknown bar", new Object[0]);
                }
            }
        });
    }

    public final void horibarShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str);
                if (horizontalBar != null) {
                    C0036bg.addView(bH.this.nH, horizontalBar, true);
                }
            }
        });
    }

    public final String identifier() {
        return C0061v.bm;
    }

    public final void indicatorCreate_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (access$000 != null && ownerActivity != null) {
                    ProgressBar progressBar = (ProgressBar) bH.this.nL.get(str2);
                    if (progressBar == null) {
                        progressBar = new ProgressBar(access$000.getOwnerActivity());
                        bH.this.nL.put(str2, progressBar);
                    }
                    progressBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(access$000.getOwnerActivity(), i5, i6, i7, i8));
                }
            }
        });
    }

    public final String indicatorFrame_(final String str) {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                HorizontalBar horizontalBar = (HorizontalBar) bH.this.nI.get(str);
                if (horizontalBar == null) {
                    return "[0, 0, 0, 0]";
                }
                AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) horizontalBar.getLayoutParams();
                return C0030ba.format("[%d, %d, %d, %d]", Integer.valueOf(layoutParams.x), Integer.valueOf(layoutParams.y), Integer.valueOf(layoutParams.width), Integer.valueOf(layoutParams.height));
            }
        }, "[0, 0, 0, 0]");
    }

    public final void indicatorHide_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                C0036bg.removeFromSuperView(bH.this.nL.get(str));
            }
        });
    }

    public final void indicatorSet_animating_(String str, int i) {
    }

    public final void indicatorSet_style_(String str, int i) {
    }

    public final void indicatorSet_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.post(new Runnable() {
            public final void run() {
                ProgressBar progressBar = (ProgressBar) bH.this.nL.get(str2);
                if (progressBar != null) {
                    progressBar.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(bH.this.getOwnerActivity(), i5, i6, i7, i8));
                }
            }
        });
    }

    public final void indicatorShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                C0036bg.addView(bH.this.nH, (View) bH.this.nL.get(str));
            }
        });
    }

    public final int indicatorVisible_(final String str) {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                ProgressBar progressBar = (ProgressBar) bH.this.nL.get(str);
                return Integer.valueOf((progressBar == null || progressBar.getParent() == null) ? 0 : 1);
            }
        }, 0)).intValue();
    }

    public final void info_type_(final String str, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (ownerActivity != null) {
                    WebAlertDialog webAlertDialog = new WebAlertDialog(ownerActivity);
                    webAlertDialog.setIcon(i);
                    webAlertDialog.setText(str);
                    webAlertDialog.show();
                }
            }
        });
    }

    public final int isAppInstalled(String str) {
        try {
            return AppManager.isInstalled(str) ? 1 : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public final int isAppInstalledByName(String str) {
        try {
            return AppManager.isInstalledByName(str) ? 1 : 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public final int isDebug() {
        return 0;
    }

    public final int isFriend_(final int i) {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                return Integer.valueOf(aC.getInstance().isFriend(i) ? 1 : 0);
            }
        }, 0)).intValue();
    }

    public final int isInTabbar() {
        try {
            return getTabBarActivity() != null ? 1 : 0;
        } catch (Exception e) {
            X.e(e, "Failed in isInTabbar", new Object[0]);
            return 0;
        }
    }

    public final int isLeaderboardVisible() {
        return aA.getInstance().getSocialConfig().isLeaderboardVisible() ? 1 : 0;
    }

    public final int isSkipEnabled() {
        try {
            return aA.getInstance().getSocialConfig().isSkipEnabledInRegistration() ? 1 : 0;
        } catch (Exception e) {
            X.w(e, "Failed in isSkipEnabled", new Object[0]);
            return 1;
        }
    }

    public final int isVisible() {
        WebViewController controller = getController();
        bp bpVar = this.nH;
        return (bpVar == null || controller == null || bpVar != controller.getTopWebView()) ? 0 : 1;
    }

    public final String language() {
        return C0061v.bd;
    }

    public final void launchApp(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                AppManager.launchApplication(str);
            }
        });
    }

    public final String listAccounts() {
        try {
            return AppAccount.toJSONArray(AppAccountManager.getWrapper().listAccounts()).toString();
        } catch (Exception e) {
            return "[]";
        }
    }

    public final String listAccountsByType(String str) {
        try {
            return AppAccount.toJSONArray(AppAccountManager.getWrapper().listAccountsByType(str)).toString();
        } catch (Exception e) {
            X.i(e, "Failed to list acc", new Object[0]);
            return "[]";
        }
    }

    public final String listApps() {
        try {
            return AppManager.listInstalled2JSON().toString();
        } catch (Exception e) {
            return "[]";
        }
    }

    public final String listFriends_(int i) {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                JSONArray jSONArray = new JSONArray();
                ArrayList<PPYUser> listFriends = aC.getInstance().listFriends();
                Collections.sort(listFriends, new Comparator<PPYUser>() {
                    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                        PPYUser pPYUser = (PPYUser) obj;
                        PPYUser pPYUser2 = (PPYUser) obj2;
                        if (pPYUser == pPYUser2) {
                            return 0;
                        }
                        if (pPYUser == null) {
                            return -1;
                        }
                        return pPYUser.getNickname().compareTo(pPYUser2.getNickname());
                    }
                });
                for (int i = 0; i < listFriends.size(); i++) {
                    PPYUser pPYUser = listFriends.get(i);
                    jSONArray.put(pPYUser.getUserID());
                    jSONArray.put(pPYUser.getNickname());
                }
                return jSONArray.length() == 0 ? "[]" : jSONArray.toString();
            }
        }, "[]");
    }

    public final String listLocalScores_(int i) {
        return listLocalScores_name_(i, null);
    }

    public final String listLocalScores_name_(int i, String str) {
        List<PPYLocalScore> listScores = aA.getInstance().getScoreDatabase(str).listScores(i);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        JSONArray jSONArray = new JSONArray();
        String nickname = aC.getInstance().getNickname();
        if (nickname == null) {
            nickname = "Unregistered";
        }
        for (int i2 = 0; i2 < listScores.size(); i2++) {
            PPYLocalScore pPYLocalScore = listScores.get(i2);
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("rank", i2 + 1);
                jSONObject.put("score", pPYLocalScore.score);
                jSONObject.put("time", simpleDateFormat.format(new Date(pPYLocalScore.time * 1000)));
                jSONObject.put("name", nickname);
            } catch (JSONException e) {
            }
            jSONArray.put(jSONObject);
        }
        return jSONArray.toString();
    }

    public final void log_(String str) {
    }

    public final void login_pwd_(String str, String str2) {
        X.missedFeature("login_pwd_md5_");
    }

    public final void login_pwd_md5_(String str, String str2, int i) {
        X.missedFeature("login_pwd_md5_");
    }

    public final int manifestVersionCode() {
        return C0061v.bf;
    }

    public final String manifestVersionName() {
        return C0061v.bg;
    }

    public final void maskHide() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    controller.hideLoading();
                }
            }
        });
    }

    public final void maskShow_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    controller.showLoading(str);
                }
            }
        });
    }

    public final void menuClearSelection_(String str) {
    }

    public final void menuCreate_json_x_y_w_h_(String str, String str2, int i, int i2, int i3, int i4) {
        final String str3 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        final String str4 = str2;
        C0036bg.post(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                bp access$000 = bH.this.nH;
                if (ownerActivity != null && access$000 != null) {
                    WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str3);
                    if (webMenuView == null) {
                        webMenuView = new WebMenuView(ownerActivity, str3, C0036bg.rawAbsoluteLayoutParams(ownerActivity, i5, i6, i7, i8));
                        webMenuView.setWebView(access$000);
                        bH.this.nJ.put(str3, webMenuView);
                    } else {
                        webMenuView.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(ownerActivity, i5, i6, i7, i8));
                    }
                    webMenuView.refreshWithCtx(C0040bk.parseJsonObject(str4));
                }
            }
        });
    }

    public final void menuCreate_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        X.warnIncomplete();
    }

    public final String menuFrame_(final String str) {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str);
                JSONArray jSONArray = new JSONArray();
                if (webMenuView != null) {
                    AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) webMenuView.getLayoutParams();
                    jSONArray.put(layoutParams.x);
                    jSONArray.put(layoutParams.y);
                    jSONArray.put(layoutParams.width);
                    jSONArray.put(layoutParams.height);
                } else {
                    X.w("can't find menu %s", str);
                    jSONArray.put(0);
                    jSONArray.put(0);
                    jSONArray.put(0);
                    jSONArray.put(0);
                }
                return jSONArray.toString();
            }
        }, "[0, 0, 0, 0]");
    }

    public final void menuHide_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                C0036bg.removeFromSuperView((WebMenuView) bH.this.nJ.get(str));
            }
        });
    }

    public final void menuPack_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str);
                if (webMenuView != null) {
                    webMenuView.pack();
                }
            }
        });
    }

    public final void menuReload_json_(final String str, final String str2) {
        C0036bg.post(new Runnable() {
            public final void run() {
                WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str);
                if (webMenuView != null) {
                    webMenuView.refreshWithCtx(C0040bk.parseJsonObject(str2));
                }
            }
        });
    }

    public final void menuSet_x_y_w_h_(String str, int i, int i2, int i3, int i4) {
        final String str2 = str;
        final int i5 = i3;
        final int i6 = i4;
        final int i7 = i;
        final int i8 = i2;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str2);
                if (webMenuView != null) {
                    webMenuView.setLayoutParams(C0036bg.rawAbsoluteLayoutParams(bH.this.getOwnerActivity(), i5, i6, i7, i8));
                }
            }
        });
    }

    public final void menuShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                WebMenuView webMenuView = (WebMenuView) bH.this.nJ.get(str);
                if (webMenuView != null && webMenuView.getParent() == null && access$000 != null) {
                    access$000.addView(webMenuView);
                }
            }
        });
    }

    public final int model() {
        return 6;
    }

    public final void multipleLineInputShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    WebMultipleInputView webMultipleInputView = new WebMultipleInputView(access$000.getOwnerActivity());
                    webMultipleInputView.setWebView(access$000);
                    webMultipleInputView.configureWithJson(C0040bk.parseJsonObject(str));
                    webMultipleInputView.show();
                }
            }
        });
    }

    public final void onConnectionEstablished() {
    }

    public final void onConnectionLost() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bH.this.nE.hideLoadingViews();
            }
        });
    }

    public final void openExternal_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.openExternalUri(bH.this.getOwnerActivity(), str);
            }
        });
    }

    public final void openMarket(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.openExternalUri(bH.this.getOwnerActivity(), str);
            }
        });
    }

    public final void open_url_in_external_activity(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0029b.startActivityForResult(bH.this.getOwnerActivity(), new Intent(C0042c.getApplicationContext(), ExternalWebActivity.class).putExtra("url", str), 10);
            }
        });
    }

    public final int orientation() {
        if (this.nH != null) {
            return this.nH.getOrientation();
        }
        return 1;
    }

    public final int pageDBVersion() {
        return C0017ao.getInstance().getVersion();
    }

    public final void picturesShow_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    new WebPicturesDialog(access$000.getOwnerActivity(), access$000, str).show();
                }
            }
        });
    }

    public final int poAppID() {
        return aC.getInstance().getAppID();
    }

    public final void potpRegister(int i) {
        try {
            if (C0042c.A != null) {
                C0042c.A.registerCmds(this.nO, Integer.valueOf(i));
            }
        } catch (Exception e) {
            X.e(e, "Failed in potpRegister", new Object[0]);
        }
    }

    public final void potpSend(int i) {
        potpSend(i, null);
    }

    public final void potpSend(int i, String str) {
        try {
            if (C0030ba.isEmpty(str)) {
                C0042c.send(i, new Object[0]);
            } else {
                C0042c.send(i, C0040bk.parseJsonAsArray(str));
            }
        } catch (Exception e) {
            X.e(e, "Failed in potpSend", new Object[0]);
        }
    }

    public final void potpUnregister() {
        if (C0042c.A != null) {
            C0042c.A.unregisterCmd(this.nO, new Integer[0]);
        }
    }

    public final void potpUnregister(int i) {
        if (C0042c.A != null) {
            C0042c.A.unregisterCmd(this.nO, Integer.valueOf(i));
        }
    }

    public final void progressDlgHide() {
        C0036bg.post(new Runnable() {
            public final void run() {
                if (bH.this.nM != null) {
                    bH.this.nM.dismiss();
                }
            }
        });
    }

    public final void progressDlgShow_msg_(final String str, final String str2) {
        C0036bg.post(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (ownerActivity != null) {
                    if (bH.this.nM == null) {
                        ProgressDialog unused = bH.this.nM = new ProgressDialog(ownerActivity);
                        bH.this.nM.setCancelable(false);
                    }
                    bH.this.nM.setTitle(str);
                    bH.this.nM.setMessage(str2);
                    if (!bH.this.nM.isShowing()) {
                        bH.this.nM.show();
                    }
                }
            }
        });
    }

    public final String requestQuery() {
        return C0030ba.isEmpty(this.nN) ? "{}" : this.nN;
    }

    public final void saveUser_pwd_md5_(String str, String str2, int i) {
        X.missedFeature("saveUser_pwd_md5_");
    }

    public final void selectorShow_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    new WebListDialogWrapper(new CustomDialog.Builder(access$000.getOwnerActivity()).create(), access$000, C0040bk.parseJsonObject(str)).getDialog().show();
                }
            }
        });
    }

    public final String sessionId() {
        return aC.getInstance().getSessionKey();
    }

    public final void setBadgeValue(final String str, final String str2, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                if (i == 0) {
                    EntryActivity tabBarActivity = bH.this.getTabBarActivity();
                    if (tabBarActivity != null) {
                        tabBarActivity.setLocalBadgeValue(str, str2);
                        tabBarActivity.refreshBadgeValues();
                        return;
                    }
                    return;
                }
                C0042c.getTabBadgeValues().setLabel(str, str2);
                C0042c.getTabBadgeValues().fireDataStateChanged();
            }
        });
    }

    public final void setRequestJson(String str) {
        this.nN = str;
    }

    public final void setSubmitScoreCountryInfo(final String str, final String str2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                if (bH.access$700(bH.this) != null) {
                    bH.access$802(bH.this, str);
                    bH.access$902(bH.this, str2);
                    TextView textView = (TextView) bH.access$700(bH.this).findViewById(C0065z.id("select_country"));
                    if ("China".equals(str)) {
                        textView.setText("China");
                    } else {
                        textView.setText(str);
                    }
                }
            }
        });
    }

    public final void setTitleJson_(final String str) {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null) {
                    bG history = access$000.getHistory();
                    if (history != null) {
                        history.setTitleCtx(C0040bk.parseJsonObject(str));
                    }
                    WebViewController controller = bH.this.getController();
                    if (controller != null) {
                        controller.updateActivityTitle(access$000);
                    }
                }
            }
        });
    }

    public final void setUrl_(String str) {
        bp bpVar = this.nH;
        if (bpVar != null) {
            if (C0030ba.isEmpty(str)) {
                bpVar.setPapayaURL(null);
                return;
            }
            URL newURI = bpVar.newURI(str);
            if (newURI != null) {
                bpVar.setPapayaURL(newURI);
            }
        }
    }

    public final void setUserNickname_(String str) {
        C0042c.getSession().setDispname(str);
    }

    public final void setWebView(bp bpVar) {
        this.nH = bpVar;
    }

    public final void showAd(String str, int i, int i2, int i3) {
        showAd(str, 0, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3));
    }

    public final void showAd(final String str, Integer... numArr) {
        bp bpVar = this.nH;
        if (bpVar != null) {
            int width = bpVar.getWidth();
            int i = 50;
            final int intValue = numArr.length > 0 ? numArr[0].intValue() : 1;
            int intValue2 = numArr.length > 1 ? numArr[1].intValue() : 0;
            int intValue3 = numArr.length > 2 ? numArr[2].intValue() : 0;
            if (numArr.length > 3) {
                width = numArr[3].intValue();
            }
            if (numArr.length > 4) {
                i = numArr[4].intValue();
            }
            final AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams(width, i, intValue2, intValue3);
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    WebViewController controller = bH.this.getController();
                    bp access$000 = bH.this.nH;
                    if (controller != null && access$000 != null) {
                        int i = 0;
                        if (str.equals("tapjoy")) {
                            i = 1;
                        } else if (str.equals("adsense")) {
                            i = 2;
                        } else if (str.equals("flurry")) {
                            i = 3;
                        }
                        if (controller.oP == null || controller.oP.adType != i) {
                            if (controller.oP != null) {
                                controller.oP.removeFromSuperView();
                                controller.oP = null;
                            }
                            if (intValue == 1) {
                                controller.oP = new AdWrapperView(i);
                            } else {
                                controller.oP = new AdWrapperView(i, layoutParams);
                            }
                        } else {
                            controller.oP.removeFromSuperView();
                            controller.oP.showMode = intValue;
                            controller.oP.setParams(layoutParams);
                        }
                        controller.oP.addToView(access$000);
                    }
                }
            });
        }
    }

    public final void showAdOffer_(String str) {
        if ("papaya".equals(str)) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0044e.show(bH.this.getOwnerActivity());
                }
            });
        }
    }

    public final void showAdmobx_y_w_(int i, int i2, int i3) {
        showAd("admob", new Integer[0]);
    }

    public final void showExchangeChipsx_y_papaya_(int i, int i2, int i3) {
    }

    public final void showFloatAd(String str) {
        showAd(str, new Integer[0]);
    }

    public final void showMap(int i, int i2, int i3, int i4) {
        final int i5 = i;
        final int i6 = i2;
        final int i7 = i3;
        final int i8 = i4;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller != null) {
                    controller.showMap(i5, i6, i7, i8);
                }
            }
        });
    }

    public final void showPayment(final float f, final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                G.showPayment(bH.this.getOwnerActivity(), f, str);
            }
        });
    }

    public final void showPaypalButton(int i, int i2, int i3, String str) {
        final int i4 = i3;
        final int i5 = i;
        final int i6 = i2;
        final String str2 = str;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                G.showCheckoutButton(bH.this.nH, i4, i5, i6, str2);
            }
        });
    }

    public final void showSubmitScoreDialog(int i) {
    }

    public final void showToast(String str) {
        C0036bg.showToast(str, 0);
    }

    public final void showToast(String str, int i) {
        C0036bg.showToast(str, i > 0 ? 1 : 0);
    }

    public final String signature(String str) {
        try {
            return aZ.md5(str);
        } catch (Exception e) {
            return "";
        }
    }

    public final String socialAPIKey() {
        return aA.getInstance().getApiKey();
    }

    public final int socialAPIVersion() {
        return 173;
    }

    public final String socialIdentifier() {
        return C0028az.fK;
    }

    public final String source() {
        return C0061v.be;
    }

    public final void startLocationManager() {
        WebViewController controller = getController();
        if (controller != null) {
            C0020ar.getInstance().register(controller);
        }
    }

    public final void startPost(String str) {
        this.nD.startPost(C0040bk.parseJsonObject(str));
    }

    public final void startZong(String str) {
        H.start(getOwnerActivity(), getController(), str);
    }

    public final void stopLocationManager() {
        WebViewController controller = getController();
        if (controller != null) {
            C0020ar.getInstance().unregister(controller);
        }
    }

    public final int supportAccountManager() {
        return 1;
    }

    public final int supportAppManager() {
        return AppManager.INITIALIZED ? 1 : 0;
    }

    public final int supportFacebook() {
        return 1;
    }

    public final int supportLBS() {
        return 0;
    }

    public final int supportMarket() {
        return C0035bf.supportMarket() ? 1 : 0;
    }

    public final int supportPaypal() {
        return 1;
    }

    public final int supportPicasa() {
        return C0035bf.supportPicasa() ? 1 : 0;
    }

    public final int supportPurchase() {
        return 0;
    }

    public final int supportZong() {
        return 0;
    }

    public final void switchGlobalReusable_(int i) {
        if (this.nH != null) {
            this.nH.setGlobalReusable(i > 0);
        }
    }

    public final void switchRecyclable_(int i) {
        if (this.nH != null) {
            this.nH.setRecylable(i > 0);
        }
    }

    public final void switchReusable_(int i) {
        if (this.nH != null) {
            this.nH.setReusable(i > 0);
        }
    }

    public final String tabName() {
        Activity ownerActivity = getOwnerActivity();
        return ownerActivity instanceof WebActivity ? ((WebActivity) ownerActivity).getTabName() : "";
    }

    public final void takePhoto(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                bp access$000 = bH.this.nH;
                if (ownerActivity != null && access$000 != null) {
                    bJ.takePhoto(access$000, C0040bk.parseJsonObject(str));
                }
            }
        });
    }

    public final void updateTitle_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bp access$000 = bH.this.nH;
                if (access$000 != null && access$000 != null) {
                    bG history = access$000.getHistory();
                    if (history != null) {
                        history.setTitle(str);
                    }
                    WebViewController controller = bH.this.getController();
                    if (controller != null) {
                        controller.updateActivityTitle(access$000);
                    }
                }
            }
        });
    }

    public final void updateViewsVisibility(int i) {
        this.nE.updateViewsVisibility(i);
    }

    public final void uploadCamera() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.startCameraActivity(bH.this.getOwnerActivity(), 3);
            }
        });
    }

    public final void uploadLocal() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0029b.startActivityForResult(bH.this.getOwnerActivity(), new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 4);
            }
        });
    }

    public final String urlSuffix() {
        return C0030ba.format("__m=%s&__v=%d&__id=%s&__lang=%s", "social_170", 173, C0028az.fK, C0061v.bd);
    }

    public final int userId() {
        return C0042c.getSession().getUserID();
    }

    public final String userNickname() {
        return C0042c.getSession().getDispname();
    }

    public final int version() {
        return C0061v.bc;
    }

    public final void viewConfig_type_json_(final String str, final int i, final String str2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Object access$1000 = bH.this.findView(i, str);
                if (access$1000 instanceof JsonConfigurable) {
                    ((JsonConfigurable) access$1000).refreshWithCtx(C0040bk.parseJsonObject(str2));
                }
            }
        });
    }

    public final void viewCreate_type_(final String str, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                bp access$000 = bH.this.nH;
                if (access$000 != null && bH.this.findView(i, str) == null && ownerActivity != null && ownerActivity != null) {
                    switch (i) {
                        case 1:
                            X.w("menu view should not be here", new Object[0]);
                            return;
                        case 2:
                            X.w("avatar bar should not be here", new Object[0]);
                            return;
                        case 3:
                        case 5:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            return;
                        case 4:
                            X.w("activity should not be here", new Object[0]);
                            return;
                        case 6:
                            X.w("Horibar should not be here", new Object[0]);
                            return;
                        case 7:
                            WebSelectorDialog webSelectorDialog = new WebSelectorDialog(ownerActivity);
                            webSelectorDialog.setViewId(str);
                            webSelectorDialog.setIcon(C0065z.drawableID("alert_icon_check"));
                            bH.this.addView(i, str, webSelectorDialog);
                            webSelectorDialog.setWebView(access$000);
                            return;
                        case 8:
                            WebAlertDialog webAlertDialog = new WebAlertDialog(ownerActivity);
                            webAlertDialog.setViewId(str);
                            webAlertDialog.setWebView(access$000);
                            bH.this.addView(i, str, webAlertDialog);
                            return;
                        case 13:
                            bH.this.addView(i, str, new WebMixedInputDialog(ownerActivity, str, access$000));
                            return;
                        default:
                            X.w("Unknown type of view to create, %s, %d", str, Integer.valueOf(i));
                            return;
                    }
                }
            }
        });
    }

    public final void viewDestroy_type_(final String str, final int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bH.this.removeView(i, str);
            }
        });
    }

    public final String viewFrame_type_(final String str, final int i) {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            /* access modifiers changed from: private */
            /* renamed from: d */
            public String call() {
                Object obj;
                try {
                    Object access$1000 = bH.this.findView(i, str);
                    try {
                        if (access$1000 instanceof View) {
                            AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) ((View) access$1000).getLayoutParams();
                            return C0030ba.format("[%d, %d, %d, %d]", Integer.valueOf(layoutParams.x), Integer.valueOf(layoutParams.y), Integer.valueOf(layoutParams.width), Integer.valueOf(layoutParams.height));
                        }
                    } catch (Exception e) {
                        Exception exc = e;
                        obj = access$1000;
                        e = exc;
                        X.e(e, "Failed to get frame of %s", obj);
                        return null;
                    }
                } catch (Exception e2) {
                    e = e2;
                    obj = null;
                }
                return null;
            }
        }, "[0, 0, 0, 0]");
    }

    public final void viewHide_type_animated_(final String str, final int i, final int i2) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.hide(bH.this.findView(i, str), i2 > 0);
            }
        });
    }

    public final void viewResetDelegate_type_(String str, int i) {
    }

    public final void viewSetColor_type_property_color_alpha_(String str, int i, String str2, int i2, int i3) {
        final int i4 = i;
        final String str3 = str;
        final String str4 = str2;
        final int i5 = i3;
        final int i6 = i2;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Object access$1000 = bH.this.findView(i4, str3);
                if (access$1000 != null) {
                    bC.setProperty(access$1000, str4, (i5 << 24) + i6);
                }
            }
        });
    }

    public final void viewSetFont_type_property_fontName_size_(String str, int i, String str2, String str3, int i2) {
    }

    public final void viewSet_type_property_value_(String str, int i, String str2, String str3) {
        final int i2 = i;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bC.setProperty(bH.this.findView(i2, str4), str5, str6);
            }
        });
    }

    public final void viewSet_type_x_y_w_h_(String str, int i, int i2, int i3, int i4, int i5) {
        final int i6 = i;
        final String str2 = str;
        final int i7 = i2;
        final int i8 = i3;
        final int i9 = i4;
        final int i10 = i5;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.setViewFrame(bH.this.findView(i6, str2), i7, i8, i9, i10);
            }
        });
    }

    public final void viewShow_type_parent_animated_(String str, int i, int i2, int i3) {
        final int i4 = i;
        final String str2 = str;
        final int i5 = i2;
        final int i6 = i3;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0036bg.showInView(bH.this.findView(i4, str2), bH.this.findParent(i5), i6 > 0);
            }
        });
    }

    public final int viewVisible_type_(final String str, final int i) {
        return ((Integer) C0036bg.callInHandlerThread(new Callable<Integer>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                Object access$1000 = bH.this.findView(i, str);
                return Integer.valueOf(access$1000 instanceof View ? ((View) access$1000).getParent() != null ? 1 : 0 : access$1000 instanceof Dialog ? ((Dialog) access$1000).isShowing() ? 1 : 0 : 0);
            }
        }, 0)).intValue();
    }

    public final String view_type_property_(final String str, final int i, final String str2) {
        return (String) C0036bg.callInHandlerThread(new Callable<String>() {
            public final /* bridge */ /* synthetic */ Object call() throws Exception {
                Object access$1000 = bH.this.findView(i, str);
                return C0040bk.object2JSONString(access$1000 != null ? bC.getProperty(access$1000, str2) : null);
            }
        }, "undefined");
    }

    public final void webDlgHide() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController controller = bH.this.getController();
                if (controller instanceof bF) {
                    ((bF) controller).getDialog().dismiss();
                }
            }
        });
    }

    public final void webDlgParentCall_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                WebViewController parentController;
                WebViewController controller = bH.this.getController();
                if ((controller instanceof bF) && (parentController = ((bF) controller).getParentController()) != null) {
                    parentController.callJS(str);
                }
            }
        });
    }

    public final void webDlgShow_(final String str) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                Activity ownerActivity = bH.this.getOwnerActivity();
                if (ownerActivity != null) {
                    WebDialog webDialog = new WebDialog(ownerActivity);
                    webDialog.show();
                    webDialog.getController().openUrl(str);
                    webDialog.getController().setParentController(bH.this.getController());
                }
            }
        });
    }

    public final void webloaded() {
        C0036bg.post(new Runnable() {
            public final void run() {
                bp.a delegate;
                bp access$000 = bH.this.nH;
                if (access$000 != null && (delegate = access$000.getDelegate()) != null) {
                    delegate.onWebLoaded(access$000);
                }
            }
        });
    }
}
