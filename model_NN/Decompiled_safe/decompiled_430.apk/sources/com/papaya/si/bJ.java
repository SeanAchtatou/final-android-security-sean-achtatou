package com.papaya.si;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import com.millennialmedia.android.MMAdView;
import com.papaya.social.BillingChannel;
import java.io.File;
import org.json.JSONObject;

public final class bJ {
    private static bp oD;
    private static JSONObject oE;

    public static void onPhotoTaken(Activity activity, int i, int i2, Intent intent) {
        try {
            if (oD == null || oE == null) {
                X.e("webview or config is null", new Object[0]);
                oD = null;
                oE = null;
            }
            if (activity != oD.getOwnerActivity()) {
                X.dw("activity is not equal!", new Object[0]);
            }
            String optString = oE.optString("callback");
            String optString2 = oE.optString("id", "");
            if (i2 == -1) {
                int optInt = oE.optInt(MMAdView.KEY_WIDTH, BillingChannel.MISC);
                int optInt2 = oE.optInt(MMAdView.KEY_HEIGHT, BillingChannel.MISC);
                Bitmap cameraBitmap = i == 11 ? C0036bg.getCameraBitmap(activity, intent, optInt, optInt2, true) : C0036bg.createScaledBitmap(activity.getContentResolver(), intent.getData(), optInt, optInt2, true);
                if (cameraBitmap != null) {
                    File cacheFile = C0002a.getWebCache().getCacheFile("__pria_take_" + optString2);
                    aZ.saveBitmap(cameraBitmap, cacheFile, "png".equals(oE.optString("format")) ? Bitmap.CompressFormat.PNG : Bitmap.CompressFormat.JPEG, oE.optInt("quality", 35));
                    oD.callJSFunc("%s('%s', %d, '%s')", optString, optString2, 1, bE.ny + cacheFile.getName());
                } else {
                    oD.callJSFunc("%s('%s', %d, null)", optString, optString2, 0);
                }
                oD = null;
                oE = null;
            }
            oD.callJSFunc("%s('%s', %d, null)", optString, optString2, 0);
            oD = null;
            oE = null;
        } catch (Exception e) {
            X.e(e, "Failed to process taken picture", new Object[0]);
        }
    }

    public static void takePhoto(bp bpVar, JSONObject jSONObject) {
        if (!(oD == null && oE == null)) {
            X.dw("invalid states for takePhoto", new Object[0]);
        }
        oD = bpVar;
        oE = jSONObject;
        try {
            Activity ownerActivity = bpVar.getOwnerActivity();
            if (ownerActivity == null) {
                return;
            }
            if (jSONObject.optInt("source", 0) == 0) {
                C0036bg.startCameraActivity(ownerActivity, 11);
            } else {
                C0036bg.startGalleryActivity(ownerActivity, 12);
            }
        } catch (Exception e) {
            X.e(e, "Failed to start take picture", new Object[0]);
        }
    }
}
