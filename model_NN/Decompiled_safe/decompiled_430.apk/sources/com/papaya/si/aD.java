package com.papaya.si;

import android.app.Application;
import android.content.Context;
import java.util.List;

public final class aD extends bD {
    private aJ gt;
    private int gu = 1500;

    public aD(String str, Context context) {
        super(str, context);
    }

    private void increateLoginInterval() {
        if (this.gu * 2 < 45000) {
            this.gu *= 2;
        }
    }

    public final void appendRequest(bA bAVar) {
        super.appendRequest(bAVar);
        poll();
    }

    public final void appendRequests(List<bA> list) {
        super.appendRequests(list);
        poll();
    }

    public final void connectionFailed(by byVar, int i) {
        if (byVar.getRequest() == this.gt) {
            this.gt = null;
            increateLoginInterval();
            C0036bg.postDelayed(new Runnable() {
                public final void run() {
                    aD.this.tryLogin();
                }
            }, (long) this.gu);
        }
        super.connectionFailed(byVar, i);
        poll();
    }

    public final void connectionFinished(by byVar) {
        if (byVar.getRequest() == this.gt) {
            this.gt = null;
            this.gu = 1500;
        }
        super.connectionFinished(byVar);
        poll();
    }

    public final boolean encapsuleHttpInTcp() {
        return false;
    }

    public final void insertRequest(bA bAVar) {
        super.insertRequest(bAVar);
        poll();
    }

    public final void insertRequests(List<bA> list) {
        super.insertRequests(list);
        poll();
    }

    public final boolean isLoggingin() {
        return this.gt != null;
    }

    public final boolean removeRequest(bA bAVar) {
        boolean removeRequest = super.removeRequest(bAVar);
        poll();
        return removeRequest;
    }

    public final synchronized void tryLogin() {
        if (this.gt == null) {
            Application applicationContext = C0042c.getApplicationContext();
            if (applicationContext == null || C0035bf.isNetworkAvailable(applicationContext)) {
                this.gt = new aJ();
                this.gt.start(true);
                if (C0029b.getActiveActivity() != null) {
                    C0036bg.showToast(C0042c.getString("base_login"), 1);
                }
            } else {
                C0036bg.postDelayed(new Runnable() {
                    public final void run() {
                        aD.this.tryLogin();
                    }
                }, (long) this.gu);
                increateLoginInterval();
            }
        }
        poll();
    }
}
