package com.papaya.si;

import com.papaya.social.PPYSocialQuery;
import com.papaya.social.PPYUser;
import java.util.ArrayList;
import java.util.HashMap;

public final class aC {
    private static aC gk = new aC();
    private int db;
    private String dl;
    private String fQ;
    private String gl;
    private String gm;
    private String gn;
    private long go = 0;
    private int gp = 0;
    private HashMap<String, Integer> gq = new HashMap<>(4);
    public String gr;
    private boolean gs = false;

    private aC() {
    }

    public static aC getInstance() {
        return gk;
    }

    public final void clear() {
        this.gl = null;
        this.go = 0;
        this.gp = 0;
        this.dl = null;
        this.gq.clear();
        this.db = 0;
        this.gr = null;
    }

    public final String getApiKey() {
        return this.fQ;
    }

    public final int getAppID() {
        return this.db;
    }

    public final long getExpirationDate() {
        return this.go;
    }

    public final String getNickname() {
        return this.dl;
    }

    public final int getScore() {
        return getScore(this.gr);
    }

    public final int getScore(String str) {
        Integer num = this.gq.get(str);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    public final HashMap<String, Integer> getScores() {
        return new HashMap<>(this.gq);
    }

    public final String getSessionKey() {
        return this.gl;
    }

    public final String getSessionReceipt() {
        return this.gn;
    }

    public final String getSessionSecret() {
        return this.gm;
    }

    public final int getUID() {
        return this.gp;
    }

    public final boolean isAppFriend(int i) {
        return C0042c.getSession().getAppFriends().isFriend(i);
    }

    public final boolean isConnected() {
        return this.gl != null;
    }

    public final boolean isDev() {
        return this.gs;
    }

    public final boolean isFriend(int i) {
        return C0042c.getSession().getFriends().isFriend(i);
    }

    public final boolean isNonappFriend(int i) {
        return C0042c.getSession().getNonappFriends().isFriend(i);
    }

    public final ArrayList<PPYUser> listFriends() {
        return C0042c.getSession().getFriends().listUsers();
    }

    public final void save() {
    }

    public final void setApiKey(String str) {
        this.fQ = str;
    }

    public final void setAppID(int i) {
        this.db = i;
    }

    public final void setDev(boolean z) {
        this.gs = z;
    }

    public final void setExpirationDate(long j) {
        this.go = j;
    }

    public final void setNickname(String str) {
        this.dl = str;
    }

    public final void setScore(int i) {
        setScore(this.gr, i);
    }

    public final void setScore(String str, int i) {
        this.gq.put(str == null ? this.gr : str, Integer.valueOf(i));
    }

    public final void setSessionKey(String str) {
        this.gl = str;
    }

    public final void setSessionReceipt(String str) {
        this.gn = str;
    }

    public final void setSessionSecret(String str) {
        this.gm = str;
    }

    public final void setUID(int i) {
        this.gp = i;
        A.bK.setUserID(i);
    }

    public final String toString() {
        return "PPYSession: [UID=" + this.gp + ", nickname=" + this.dl + ", scores=" + this.gq + ']';
    }

    public final PPYSocialQuery updateNickname(String str, PPYSocialQuery.QueryDelegate queryDelegate) {
        if (C0030ba.isEmpty(str)) {
            return null;
        }
        PPYSocialQuery pPYSocialQuery = new PPYSocialQuery("w_update_nickname", queryDelegate);
        pPYSocialQuery.put("name", str);
        aA.getInstance().submitQuery(pPYSocialQuery);
        return pPYSocialQuery;
    }
}
