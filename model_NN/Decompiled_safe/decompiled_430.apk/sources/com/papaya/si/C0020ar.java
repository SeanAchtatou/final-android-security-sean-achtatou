package com.papaya.si;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* renamed from: com.papaya.si.ar  reason: case insensitive filesystem */
public final class C0020ar implements LocationListener {
    private static C0020ar eq = new C0020ar();
    private Location er;
    private ArrayList<C0019aq> es = new ArrayList<>(4);
    private ArrayList<C0019aq> et = new ArrayList<>(4);

    public static C0020ar getInstance() {
        return eq;
    }

    private void onBestLocationChanged(Location location) {
        if (location != null) {
            this.er = location;
            location.getLatitude();
            location.getLongitude();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.es.size()) {
                    this.es.get(i2).onBestLocation(location);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void updateLocation(Location location) {
        if (location != null && this.er == null) {
            onBestLocationChanged(location);
        } else if (location != null) {
            long time = new Date().getTime();
            long time2 = time - location.getTime();
            long time3 = time - this.er.getTime();
            boolean z = time2 <= 300000;
            boolean z2 = time3 <= 300000;
            boolean z3 = location.hasAccuracy() || this.er.hasAccuracy();
            boolean z4 = z3 ? (!location.hasAccuracy() || this.er.hasAccuracy()) ? (location.hasAccuracy() || !this.er.hasAccuracy()) && location.getAccuracy() <= this.er.getAccuracy() : true : false;
            if (z3 && z4 && z) {
                onBestLocationChanged(location);
            } else if (z && !z2) {
                onBestLocationChanged(location);
            }
        }
    }

    public final String getAddress(Location location) {
        String str;
        String str2;
        if (location != null) {
            try {
                List<Address> fromLocation = new Geocoder(C0042c.getApplicationContext(), Locale.getDefault()).getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (fromLocation.size() > 0) {
                    int maxAddressLineIndex = fromLocation.get(0).getMaxAddressLineIndex();
                    int i = 0;
                    String str3 = "";
                    while (i < maxAddressLineIndex) {
                        try {
                            str3 = str3 + fromLocation.get(0).getAddressLine(i);
                            if (i != maxAddressLineIndex - 1) {
                                str3 = str3 + ", ";
                            }
                            i++;
                        } catch (Exception e) {
                            e = e;
                            str2 = str3;
                            X.e(e, "Failed in getAddress", new Object[0]);
                            str = str2;
                            X.i("Map view get addess:%s", str);
                            return str;
                        }
                    }
                    str = str3;
                    X.i("Map view get addess:%s", str);
                    return str;
                }
            } catch (Exception e2) {
                e = e2;
                str2 = "";
                X.e(e, "Failed in getAddress", new Object[0]);
                str = str2;
                X.i("Map view get addess:%s", str);
                return str;
            }
        }
        str = "";
        X.i("Map view get addess:%s", str);
        return str;
    }

    public final String getCurrentAddress() {
        return getAddress(this.er);
    }

    public final Location getPosition() {
        return this.er;
    }

    public final void onLocationChanged(Location location) {
        updateLocation(location);
    }

    public final void onProviderDisabled(String str) {
        C0029b.showInfo(C0042c.getString("gps_dis"));
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public final void pause(C0019aq aqVar) {
        if (this.es.indexOf(aqVar) != -1 && this.et.indexOf(aqVar) == -1) {
            this.et.add(aqVar);
        }
        unregister(aqVar);
    }

    public final synchronized void register(C0019aq aqVar) {
        if (this.es.indexOf(aqVar) == -1) {
            if (this.et.indexOf(aqVar) != -1) {
                this.et.remove(aqVar);
            }
            this.es.add(aqVar);
            LocationManager locationManager = (LocationManager) C0042c.getApplicationContext().getSystemService("location");
            aqVar.resumeMyLocation();
            List<String> providers = locationManager.getProviders(true);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= providers.size()) {
                    break;
                }
                String str = providers.get(i2);
                if (locationManager.isProviderEnabled(str)) {
                    updateLocation(locationManager.getLastKnownLocation(str));
                }
                locationManager.requestLocationUpdates(str, 0, 0.0f, this);
                i = i2 + 1;
            }
        }
    }

    public final void resume(C0019aq aqVar) {
        if (this.et.indexOf(aqVar) != -1) {
            register(aqVar);
        }
    }

    public final synchronized void unregister(C0019aq aqVar) {
        this.es.remove(aqVar);
        try {
            if (this.es.isEmpty()) {
                ((LocationManager) C0042c.getApplicationContext().getSystemService("location")).removeUpdates(this);
            }
        } catch (Exception e) {
        }
    }
}
