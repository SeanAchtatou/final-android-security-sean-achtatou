package com.papaya.si;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public final class aP {
    private String c;
    private File file;
    private boolean gP = false;

    public aP(File file2) {
        this.file = file2;
    }

    public aP(File file2, String str) {
        this.file = new File(file2, str);
    }

    public aP(String str) {
        this.c = str;
    }

    public static boolean copy(aP aPVar, aP aPVar2) {
        if (!aPVar2.gP) {
            return aPVar2.writeData(aZ.dataFromStream(aPVar.openInput()));
        }
        return false;
    }

    public static aP fromPriaContentUrl(String str) {
        return str.startsWith("file:///android_asset/") ? new aP(str.substring("file:///android_asset/".length())) : new aP(new File(C0002a.getWebCache().getCacheDir(), str.substring(bE.ny.length())));
    }

    public static boolean move(aP aPVar, aP aPVar2) {
        if (aPVar.gP || aPVar2.gP || aPVar2.file.exists()) {
            return false;
        }
        return aPVar.file.renameTo(aPVar2.file);
    }

    private InputStream openAssetInput() {
        if (!this.gP || this.c == null) {
            X.w("not an asset fd", new Object[0]);
        } else {
            try {
                return C0042c.getApplicationContext().getAssets().open(this.c);
            } catch (IOException e) {
                X.w(e, "Failed to openAssetInput %s", this.c);
            }
        }
        return null;
    }

    private InputStream openFileInput() {
        if (this.gP || this.file == null) {
            X.w("not a valid file fd", new Object[0]);
        } else {
            try {
                return new FileInputStream(this.file);
            } catch (FileNotFoundException e) {
                X.w(e, "failed to open input", new Object[0]);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0054 A[SYNTHETIC, Splitter:B:30:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int assetLength() {
        /*
            r8 = this;
            r6 = 0
            boolean r0 = r8.gP
            if (r0 == 0) goto L_0x0043
            java.lang.String r0 = r8.c
            if (r0 == 0) goto L_0x0043
            r0 = 0
            android.app.Application r1 = com.papaya.si.C0042c.getApplicationContext()     // Catch:{ Exception -> 0x002d, all -> 0x004e }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x002d, all -> 0x004e }
            java.lang.String r2 = r8.c     // Catch:{ Exception -> 0x002d, all -> 0x004e }
            android.content.res.AssetFileDescriptor r0 = r1.openFd(r2)     // Catch:{ Exception -> 0x002d, all -> 0x004e }
            long r1 = r0.getLength()     // Catch:{ Exception -> 0x0068, all -> 0x0061 }
            int r1 = (int) r1
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0022:
            r0 = r1
        L_0x0023:
            return r0
        L_0x0024:
            r0 = move-exception
            java.lang.String r2 = "Failed to close fd"
            java.lang.Object[] r3 = new java.lang.Object[r6]
            com.papaya.si.X.w(r0, r2, r3)
            goto L_0x0022
        L_0x002d:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0031:
            java.lang.String r2 = "Failed to get assetLength %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0066 }
            r4 = 0
            java.lang.String r5 = r8.c     // Catch:{ all -> 0x0066 }
            r3[r4] = r5     // Catch:{ all -> 0x0066 }
            com.papaya.si.X.e(r0, r2, r3)     // Catch:{ all -> 0x0066 }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0043:
            r0 = -1
            goto L_0x0023
        L_0x0045:
            r0 = move-exception
            java.lang.String r1 = "Failed to close fd"
            java.lang.Object[] r2 = new java.lang.Object[r6]
            com.papaya.si.X.w(r0, r1, r2)
            goto L_0x0043
        L_0x004e:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ IOException -> 0x0058 }
        L_0x0057:
            throw r0
        L_0x0058:
            r1 = move-exception
            java.lang.String r2 = "Failed to close fd"
            java.lang.Object[] r3 = new java.lang.Object[r6]
            com.papaya.si.X.w(r1, r2, r3)
            goto L_0x0057
        L_0x0061:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0052
        L_0x0066:
            r0 = move-exception
            goto L_0x0052
        L_0x0068:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.aP.assetLength():int");
    }

    public final int length() {
        if (this.gP) {
            return assetLength();
        }
        if (this.file.exists()) {
            return (int) this.file.length();
        }
        return -1;
    }

    public final boolean mkdir(int i) {
        if (this.gP) {
            return false;
        }
        if (this.file.exists()) {
            return false;
        }
        return i > 0 ? this.file.mkdirs() : this.file.mkdir();
    }

    public final InputStream openInput() {
        return this.gP ? openAssetInput() : openFileInput();
    }

    public final boolean remove() {
        if (!this.gP) {
            return this.file.delete();
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x008b A[SYNTHETIC, Splitter:B:39:0x008b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean setMediaDataSource(android.media.MediaPlayer r11) {
        /*
            r10 = this;
            r8 = 1
            r7 = 0
            boolean r0 = r10.gP
            if (r0 != 0) goto L_0x0033
            java.io.InputStream r10 = r10.openInput()
            if (r10 == 0) goto L_0x001f
            java.io.FileInputStream r10 = (java.io.FileInputStream) r10     // Catch:{ IllegalArgumentException -> 0x0017, IllegalStateException -> 0x0021, Exception -> 0x002a }
            java.io.FileDescriptor r0 = r10.getFD()     // Catch:{ IllegalArgumentException -> 0x0017, IllegalStateException -> 0x0021, Exception -> 0x002a }
            r11.setDataSource(r0)     // Catch:{ IllegalArgumentException -> 0x0017, IllegalStateException -> 0x0021, Exception -> 0x002a }
            r0 = r8
        L_0x0016:
            return r0
        L_0x0017:
            r0 = move-exception
            java.lang.String r0 = "failed to set media player data source"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.papaya.si.X.e(r0, r1)
        L_0x001f:
            r0 = r7
            goto L_0x0016
        L_0x0021:
            r0 = move-exception
            java.lang.String r0 = "failed to set media player data source"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.papaya.si.X.e(r0, r1)
            goto L_0x001f
        L_0x002a:
            r0 = move-exception
            java.lang.String r0 = "failed to set media player data source"
            java.lang.Object[] r1 = new java.lang.Object[r7]
            com.papaya.si.X.e(r0, r1)
            goto L_0x001f
        L_0x0033:
            r0 = 0
            android.app.Application r1 = com.papaya.si.C0042c.getApplicationContext()     // Catch:{ Exception -> 0x0063, all -> 0x0085 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x0063, all -> 0x0085 }
            java.lang.String r2 = r10.c     // Catch:{ Exception -> 0x0063, all -> 0x0085 }
            android.content.res.AssetFileDescriptor r6 = r1.openFd(r2)     // Catch:{ Exception -> 0x0063, all -> 0x0085 }
            java.io.FileDescriptor r1 = r6.getFileDescriptor()     // Catch:{ Exception -> 0x009d, all -> 0x0098 }
            long r2 = r6.getStartOffset()     // Catch:{ Exception -> 0x009d, all -> 0x0098 }
            long r4 = r6.getLength()     // Catch:{ Exception -> 0x009d, all -> 0x0098 }
            r0 = r11
            r0.setDataSource(r1, r2, r4)     // Catch:{ Exception -> 0x009d, all -> 0x0098 }
            if (r6 == 0) goto L_0x00a3
            r6.close()     // Catch:{ IOException -> 0x0059 }
            r0 = r8
            goto L_0x0016
        L_0x0059:
            r0 = move-exception
            java.lang.String r1 = "Failed to close fd"
            java.lang.Object[] r2 = new java.lang.Object[r7]
            com.papaya.si.X.w(r0, r1, r2)
            r0 = r8
            goto L_0x0016
        L_0x0063:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0067:
            java.lang.String r2 = "Failed to get asset filedescriptor %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x009b }
            r4 = 0
            java.lang.String r5 = r10.c     // Catch:{ all -> 0x009b }
            r3[r4] = r5     // Catch:{ all -> 0x009b }
            com.papaya.si.X.e(r0, r2, r3)     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x00a0
            r1.close()     // Catch:{ IOException -> 0x007b }
            r0 = r7
            goto L_0x0016
        L_0x007b:
            r0 = move-exception
            java.lang.String r1 = "Failed to close fd"
            java.lang.Object[] r2 = new java.lang.Object[r7]
            com.papaya.si.X.w(r0, r1, r2)
            r0 = r7
            goto L_0x0016
        L_0x0085:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0089:
            if (r1 == 0) goto L_0x008e
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x008e:
            throw r0
        L_0x008f:
            r1 = move-exception
            java.lang.String r2 = "Failed to close fd"
            java.lang.Object[] r3 = new java.lang.Object[r7]
            com.papaya.si.X.w(r1, r2, r3)
            goto L_0x008e
        L_0x0098:
            r0 = move-exception
            r1 = r6
            goto L_0x0089
        L_0x009b:
            r0 = move-exception
            goto L_0x0089
        L_0x009d:
            r0 = move-exception
            r1 = r6
            goto L_0x0067
        L_0x00a0:
            r0 = r7
            goto L_0x0016
        L_0x00a3:
            r0 = r8
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.aP.setMediaDataSource(android.media.MediaPlayer):boolean");
    }

    public final Vector subFiles() {
        Vector vector = new Vector();
        if (!this.gP) {
            String[] list = this.file.list();
            if (list != null) {
                for (String aPVar : list) {
                    vector.add(new aP(this.file, aPVar));
                }
            }
        } else {
            try {
                String[] list2 = C0042c.getApplicationContext().getAssets().list(this.c);
                if (list2 != null) {
                    for (String aPVar2 : list2) {
                        vector.add(new aP(aPVar2));
                    }
                }
            } catch (IOException e) {
            }
        }
        return vector;
    }

    public final boolean writeData(byte[] bArr) {
        if (this.gP) {
            return false;
        }
        if (!this.file.exists()) {
            try {
                if (!this.file.createNewFile()) {
                    return false;
                }
            } catch (IOException e) {
                X.e("create new file %s failed", this.file.getAbsolutePath());
                return false;
            }
        }
        return aZ.writeBytesToFile(this.file, bArr);
    }
}
