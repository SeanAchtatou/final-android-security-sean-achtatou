package com.papaya.si;

import com.papaya.si.by;

public final class bx extends by implements Runnable {
    private boolean mY = false;

    public bx(bA bAVar) {
        super(bAVar);
    }

    public bx(bA bAVar, by.a aVar) {
        super(bAVar, aVar);
    }

    public final void cancel() {
        this.mY = true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0233 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0262 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e0 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d3 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:111:0x024a=Splitter:B:111:0x024a, B:80:0x01bf=Splitter:B:80:0x01bf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r14 = this;
            r12 = 1
            r11 = -1
            r9 = 0
            r10 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = com.papaya.si.C0030ba.acquireBytes(r0)
            r0 = 0
            r14.nb = r0     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bA r0 = r14.jT     // Catch:{ Exception -> 0x010b }
            java.net.URL r4 = r0.getUrl()     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bA r0 = r14.jT     // Catch:{ Exception -> 0x010b }
            java.util.HashMap<java.lang.String, com.papaya.si.bd<java.lang.Integer, java.lang.Object>> r0 = r0.nm     // Catch:{ Exception -> 0x010b }
            if (r0 == 0) goto L_0x00ee
            java.net.URLConnection r0 = com.papaya.si.bo.createConnection(r4)     // Catch:{ Exception -> 0x010b }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = com.papaya.si.bo.createBoundary()     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = "Content-Type"
            java.lang.String r5 = com.papaya.si.bo.getContentType(r1)     // Catch:{ Exception -> 0x010b }
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = "Connection"
            java.lang.String r5 = "Keep-Alive"
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = "Cache-Control"
            java.lang.String r5 = "no-cache"
            r0.setRequestProperty(r2, r5)     // Catch:{ Exception -> 0x010b }
            r5 = r0
            r0 = r1
        L_0x003d:
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            boolean r1 = r1.isRequireSid()     // Catch:{ Exception -> 0x010b }
            if (r1 == 0) goto L_0x0064
            com.papaya.si.aC r1 = com.papaya.si.aC.getInstance()     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = r1.getSessionKey()     // Catch:{ Exception -> 0x010b }
            if (r1 != 0) goto L_0x0057
            java.lang.String r1 = "sid should NOT be null"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x010b }
            com.papaya.si.X.e(r1, r2)     // Catch:{ Exception -> 0x010b }
        L_0x0057:
            java.lang.String r1 = "social-sid"
            com.papaya.si.aC r2 = com.papaya.si.aC.getInstance()     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = r2.getSessionKey()     // Catch:{ Exception -> 0x010b }
            r5.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x010b }
        L_0x0064:
            com.papaya.si.C0040bk.addSignatureHeaders(r5)     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = "Accept-Encoding"
            java.lang.String r2 = "gzip"
            r5.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            int r1 = r1.nn     // Catch:{ Exception -> 0x010b }
            if (r1 <= 0) goto L_0x00f8
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            int r1 = r1.nn     // Catch:{ Exception -> 0x010b }
        L_0x0078:
            r5.setConnectTimeout(r1)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            int r1 = r1.no     // Catch:{ Exception -> 0x010b }
            if (r1 <= 0) goto L_0x00fd
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            int r1 = r1.no     // Catch:{ Exception -> 0x010b }
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x010b }
        L_0x0088:
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            java.util.HashMap<java.lang.String, com.papaya.si.bd<java.lang.Integer, java.lang.Object>> r1 = r1.nm     // Catch:{ Exception -> 0x010b }
            if (r1 != 0) goto L_0x011f
            r5.connect()     // Catch:{ Exception -> 0x010b }
        L_0x0091:
            int r0 = r5.getResponseCode()     // Catch:{ Exception -> 0x010b }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x026d
            com.papaya.si.bA r0 = r14.jT     // Catch:{ Exception -> 0x010b }
            java.io.File r0 = r0.getSaveFile()     // Catch:{ Exception -> 0x010b }
            if (r0 != 0) goto L_0x01de
            java.io.InputStream r0 = com.papaya.si.C0040bk.getInputStream(r5)     // Catch:{ Exception -> 0x02b1, all -> 0x01c9 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x02b6, all -> 0x02a2 }
            int r2 = r5.getContentLength()     // Catch:{ Exception -> 0x02b6, all -> 0x02a2 }
            if (r2 != r11) goto L_0x01b4
            r2 = 1024(0x400, float:1.435E-42)
        L_0x00af:
            r1.<init>(r2)     // Catch:{ Exception -> 0x02b6, all -> 0x02a2 }
        L_0x00b2:
            boolean r2 = r14.mY     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            if (r2 != 0) goto L_0x01ba
            int r2 = r0.read(r3)     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            if (r2 == r11) goto L_0x01ba
            long r5 = r14.nb     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            long r7 = (long) r2     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            long r5 = r5 + r7
            r14.nb = r5     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            r5 = 0
            r1.write(r3, r5, r2)     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            goto L_0x00b2
        L_0x00c7:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
        L_0x00cb:
            java.lang.String r5 = "Failed to get data from %s"
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x02ae }
            r7 = 0
            r6[r7] = r4     // Catch:{ all -> 0x02ae }
            com.papaya.si.X.w(r0, r5, r6)     // Catch:{ all -> 0x02ae }
            com.papaya.si.aZ.close(r2)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r1)     // Catch:{ Exception -> 0x010b }
            r0 = r1
            r1 = r10
        L_0x00de:
            if (r1 == 0) goto L_0x01d3
            byte[] r0 = r0.toByteArray()     // Catch:{ Exception -> 0x010b }
            r14.setData(r0)     // Catch:{ Exception -> 0x010b }
            r14.fireConnectionFinished()     // Catch:{ Exception -> 0x010b }
        L_0x00ea:
            com.papaya.si.C0030ba.releaseBytes(r3)
            return
        L_0x00ee:
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ Exception -> 0x010b }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x010b }
            r5 = r0
            r0 = r9
            goto L_0x003d
        L_0x00f8:
            r1 = 45000(0xafc8, float:6.3058E-41)
            goto L_0x0078
        L_0x00fd:
            com.papaya.si.bA r1 = r14.jT     // Catch:{ Exception -> 0x010b }
            boolean r1 = r1.nj     // Catch:{ Exception -> 0x010b }
            if (r1 != 0) goto L_0x0117
            r1 = 60000(0xea60, float:8.4078E-41)
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x010b }
            goto L_0x0088
        L_0x010b:
            r0 = move-exception
            java.lang.String r1 = "http connection failed"
            java.lang.Object[] r2 = new java.lang.Object[r10]
            com.papaya.si.X.w(r0, r1, r2)
            r14.fireConnectionFailed(r11)
            goto L_0x00ea
        L_0x0117:
            r1 = 180000(0x2bf20, float:2.52234E-40)
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x010b }
            goto L_0x0088
        L_0x011f:
            com.papaya.si.bo r6 = new com.papaya.si.bo     // Catch:{ Exception -> 0x010b }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ Exception -> 0x010b }
            r6.<init>(r1, r0)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bA r0 = r14.jT     // Catch:{ Exception -> 0x010b }
            java.util.HashMap<java.lang.String, com.papaya.si.bd<java.lang.Integer, java.lang.Object>> r0 = r0.nm     // Catch:{ Exception -> 0x010b }
            java.util.Set r0 = r0.entrySet()     // Catch:{ Exception -> 0x010b }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ Exception -> 0x010b }
        L_0x0134:
            boolean r0 = r7.hasNext()     // Catch:{ Exception -> 0x010b }
            if (r0 == 0) goto L_0x01af
            java.lang.Object r0 = r7.next()     // Catch:{ Exception -> 0x010b }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ Exception -> 0x010b }
            java.lang.Object r1 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            if (r1 == 0) goto L_0x0134
            java.lang.Object r1 = r0.getValue()     // Catch:{ Exception -> 0x010b }
            com.papaya.si.bd r1 = (com.papaya.si.C0033bd) r1     // Catch:{ Exception -> 0x010b }
            A r2 = r1.first     // Catch:{ Exception -> 0x010b }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x010b }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x010b }
            if (r2 != 0) goto L_0x0164
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010b }
            B r1 = r1.second     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x010b }
            r6.writeField(r0, r1)     // Catch:{ Exception -> 0x010b }
            goto L_0x0134
        L_0x0164:
            A r2 = r1.first     // Catch:{ Exception -> 0x010b }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x010b }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x010b }
            if (r2 != r12) goto L_0x018c
            java.lang.Object r2 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x010b }
            java.lang.String r8 = "binary"
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010b }
            B r1 = r1.second     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aP r1 = com.papaya.si.aP.fromPriaContentUrl(r1)     // Catch:{ Exception -> 0x010b }
            java.io.InputStream r1 = r1.openInput()     // Catch:{ Exception -> 0x010b }
            r6.writeFile(r2, r8, r0, r1)     // Catch:{ Exception -> 0x010b }
            goto L_0x0134
        L_0x018c:
            A r2 = r1.first     // Catch:{ Exception -> 0x010b }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x010b }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x010b }
            r8 = 2
            if (r2 != r8) goto L_0x0134
            java.lang.Object r2 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x010b }
            java.lang.String r8 = "binary"
            java.lang.Object r0 = r0.getKey()     // Catch:{ Exception -> 0x010b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010b }
            B r1 = r1.second     // Catch:{ Exception -> 0x010b }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x010b }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x010b }
            r6.writeFile(r2, r8, r0, r1)     // Catch:{ Exception -> 0x010b }
            goto L_0x0134
        L_0x01af:
            r6.close()     // Catch:{ Exception -> 0x010b }
            goto L_0x0091
        L_0x01b4:
            int r2 = r5.getContentLength()     // Catch:{ Exception -> 0x02b6, all -> 0x02a2 }
            goto L_0x00af
        L_0x01ba:
            boolean r2 = r14.mY     // Catch:{ Exception -> 0x00c7, all -> 0x02a8 }
            if (r2 != 0) goto L_0x02be
            r2 = r12
        L_0x01bf:
            com.papaya.si.aZ.close(r0)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r1)     // Catch:{ Exception -> 0x010b }
            r0 = r1
            r1 = r2
            goto L_0x00de
        L_0x01c9:
            r0 = move-exception
            r1 = r9
            r2 = r9
        L_0x01cc:
            com.papaya.si.aZ.close(r2)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r1)     // Catch:{ Exception -> 0x010b }
            throw r0     // Catch:{ Exception -> 0x010b }
        L_0x01d3:
            r0 = -1
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x010b }
            r0 = -2
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x010b }
            goto L_0x00ea
        L_0x01de:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x010b }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010b }
            r2.<init>()     // Catch:{ Exception -> 0x010b }
            java.lang.String r4 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x010b }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x010b }
            java.lang.String r4 = ".tmp"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x010b }
            r1.<init>(r2)     // Catch:{ Exception -> 0x010b }
            java.io.InputStream r2 = com.papaya.si.C0040bk.getInputStream(r5)     // Catch:{ Exception -> 0x0298, all -> 0x0255 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x029c, all -> 0x028b }
            r4.<init>(r1)     // Catch:{ Exception -> 0x029c, all -> 0x028b }
        L_0x0203:
            boolean r5 = r14.mY     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            if (r5 != 0) goto L_0x023f
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            if (r5 == r11) goto L_0x023f
            long r6 = r14.nb     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            long r8 = (long) r5     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            long r6 = r6 + r8
            r14.nb = r6     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            goto L_0x0203
        L_0x0218:
            r5 = move-exception
            r13 = r5
            r5 = r2
            r2 = r13
        L_0x021c:
            java.lang.String r6 = "Failed to save to file %s"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0294 }
            r8 = 0
            r7[r8] = r0     // Catch:{ all -> 0x0294 }
            com.papaya.si.X.e(r2, r6, r7)     // Catch:{ all -> 0x0294 }
            com.papaya.si.aZ.close(r5)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r4)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.deleteFile(r1)     // Catch:{ Exception -> 0x010b }
            r1 = r10
        L_0x0231:
            if (r1 == 0) goto L_0x0262
            long r0 = r0.length()     // Catch:{ Exception -> 0x010b }
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x010b }
            r14.fireConnectionFinished()     // Catch:{ Exception -> 0x010b }
            goto L_0x00ea
        L_0x023f:
            boolean r5 = r14.mY     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            if (r5 != 0) goto L_0x02bc
            com.papaya.si.aZ.deleteFile(r0)     // Catch:{ Exception -> 0x0218, all -> 0x028f }
            boolean r5 = r1.renameTo(r0)     // Catch:{ Exception -> 0x0218, all -> 0x028f }
        L_0x024a:
            com.papaya.si.aZ.close(r2)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r4)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.deleteFile(r1)     // Catch:{ Exception -> 0x010b }
            r1 = r5
            goto L_0x0231
        L_0x0255:
            r0 = move-exception
            r2 = r9
            r4 = r9
        L_0x0258:
            com.papaya.si.aZ.close(r4)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.close(r2)     // Catch:{ Exception -> 0x010b }
            com.papaya.si.aZ.deleteFile(r1)     // Catch:{ Exception -> 0x010b }
            throw r0     // Catch:{ Exception -> 0x010b }
        L_0x0262:
            r0 = -1
            r14.setDataLength(r0)     // Catch:{ Exception -> 0x010b }
            r0 = -2
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x010b }
            goto L_0x00ea
        L_0x026d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010b }
            r1.<init>()     // Catch:{ Exception -> 0x010b }
            java.lang.String r2 = "http connection failed: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x010b }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x010b }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x010b }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x010b }
            com.papaya.si.X.w(r1, r2)     // Catch:{ Exception -> 0x010b }
            r14.fireConnectionFailed(r0)     // Catch:{ Exception -> 0x010b }
            goto L_0x00ea
        L_0x028b:
            r0 = move-exception
            r4 = r2
            r2 = r9
            goto L_0x0258
        L_0x028f:
            r0 = move-exception
            r13 = r4
            r4 = r2
            r2 = r13
            goto L_0x0258
        L_0x0294:
            r0 = move-exception
            r2 = r4
            r4 = r5
            goto L_0x0258
        L_0x0298:
            r2 = move-exception
            r4 = r9
            r5 = r9
            goto L_0x021c
        L_0x029c:
            r4 = move-exception
            r5 = r2
            r2 = r4
            r4 = r9
            goto L_0x021c
        L_0x02a2:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x01cc
        L_0x02a8:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
            goto L_0x01cc
        L_0x02ae:
            r0 = move-exception
            goto L_0x01cc
        L_0x02b1:
            r0 = move-exception
            r1 = r9
            r2 = r9
            goto L_0x00cb
        L_0x02b6:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r9
            goto L_0x00cb
        L_0x02bc:
            r5 = r10
            goto L_0x024a
        L_0x02be:
            r2 = r10
            goto L_0x01bf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.bx.run():void");
    }
}
