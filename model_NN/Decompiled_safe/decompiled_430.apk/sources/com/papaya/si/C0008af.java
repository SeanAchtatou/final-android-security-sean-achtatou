package com.papaya.si;

/* renamed from: com.papaya.si.af  reason: case insensitive filesystem */
public final class C0008af extends E<C0009ag> {
    public final C0009ag findBySID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.co.size()) {
                return null;
            }
            C0009ag agVar = (C0009ag) this.co.get(i3);
            if (agVar.dk == i) {
                return agVar;
            }
            i2 = i3 + 1;
        }
    }
}
