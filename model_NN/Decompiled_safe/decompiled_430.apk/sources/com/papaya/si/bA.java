package com.papaya.si;

import com.papaya.si.by;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashMap;

public class bA {
    private WeakReference<by.a> hY;
    protected boolean mz = true;
    private int nh = 0;
    private int ni = 0;
    protected boolean nj = false;
    protected boolean nk = false;
    private File nl;
    protected HashMap<String, C0033bd<Integer, Object>> nm;
    public int nn = -1;
    public int no = -1;
    protected URL url;

    public bA() {
    }

    public bA(URL url2, boolean z) {
        this.url = url2;
        this.nj = z;
    }

    public void addPostParam(String str, Object obj, int i) {
        if (str != null && obj != null) {
            if (this.nm == null) {
                this.nm = new HashMap<>();
            }
            this.nm.put(str, new C0033bd(Integer.valueOf(i), obj));
        }
    }

    public void addPostParam(String str, String str2) {
        addPostParam(str, str2, 0);
    }

    public void cancel() {
        setDelegate(null);
        try {
            C0002a.getWebCache().removeRequest(this);
        } catch (Exception e) {
            X.e(e, "Failed to cancel request", new Object[0]);
        }
    }

    public int getConnectionType() {
        return this.ni;
    }

    public by.a getDelegate() {
        if (this.hY != null) {
            return this.hY.get();
        }
        return null;
    }

    public int getRequestType() {
        return this.nh;
    }

    public File getSaveFile() {
        return this.nl;
    }

    public URL getUrl() {
        return this.url;
    }

    public boolean isCacheable() {
        return this.nj;
    }

    public boolean isDispatchable() {
        return this.nk;
    }

    public boolean isRequireSid() {
        return this.mz;
    }

    public void setCacheable(boolean z) {
        this.nj = z;
    }

    public void setConnectionType(int i) {
        this.ni = i;
    }

    public void setDelegate(by.a aVar) {
        if (aVar == null) {
            this.hY = null;
        } else {
            this.hY = new WeakReference<>(aVar);
        }
    }

    public void setDispatchable(boolean z) {
        this.nk = z;
    }

    public void setRequestType(int i) {
        this.nh = i;
    }

    public void setRequireSid(boolean z) {
        this.mz = z;
    }

    public void setSaveFile(File file) {
        this.nl = file;
    }

    public void setUrl(URL url2) {
        this.url = url2;
    }

    public void start(boolean z) {
        if (z) {
            C0002a.getWebCache().insertRequest(this);
        } else {
            C0002a.getWebCache().appendRequest(this);
        }
    }

    public String toString() {
        return "UrlRequest{_url=" + (this.url == null ? null : this.url.getPath()) + ", _requestType=" + this.nh + ", _connectionType=" + this.ni + ", _cacheable=" + this.nj + ", _requireSid=" + this.mz + ", _delegateRef=" + this.hY + '}';
    }
}
