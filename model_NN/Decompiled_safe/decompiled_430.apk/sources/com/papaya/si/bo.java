package com.papaya.si;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public final class bo {
    private DataOutputStream mn = null;
    private String mo = null;

    public bo(OutputStream outputStream, String str) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream is required.");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Boundary stream is required.");
        } else {
            this.mn = new DataOutputStream(outputStream);
            this.mo = str;
        }
    }

    public static String createBoundary() {
        return "--------------------" + Long.toString(System.currentTimeMillis(), 16);
    }

    public static URLConnection createConnection(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection instanceof HttpURLConnection) {
            ((HttpURLConnection) openConnection).setRequestMethod("POST");
        }
        openConnection.setDoInput(true);
        openConnection.setDoOutput(true);
        openConnection.setUseCaches(false);
        return openConnection;
    }

    public static String getContentType(String str) {
        return "multipart/form-data; boundary=" + str;
    }

    public final void close() throws IOException {
        this.mn.writeBytes("--");
        this.mn.writeBytes(this.mo);
        this.mn.writeBytes("--");
        this.mn.writeBytes("\r\n");
        this.mn.flush();
        this.mn.close();
    }

    public final void flush() throws IOException {
    }

    public final String getBoundary() {
        return this.mo;
    }

    public final void writeField(String str, String str2) throws IOException {
        String str3 = str2 == null ? "" : str2;
        this.mn.writeBytes("--");
        this.mn.writeBytes(this.mo);
        this.mn.writeBytes("\r\n");
        this.mn.writeBytes("Content-Disposition: form-data; name=\"" + str + '\"');
        this.mn.writeBytes("\r\n");
        this.mn.writeBytes("\r\n");
        byte[] bytes = str3.getBytes("UTF-8");
        this.mn.write(bytes, 0, bytes.length);
        this.mn.writeBytes("\r\n");
        this.mn.flush();
    }

    public final void writeFile(String str, String str2, String str3, InputStream inputStream) throws IOException {
        if (inputStream == null) {
            X.w("Input stream is null", new Object[0]);
        } else if (str3 == null || str3.length() == 0) {
            X.w("fileName is empty", new Object[0]);
        } else {
            byte[] acquireBytes = C0030ba.acquireBytes(1024);
            try {
                this.mn.writeBytes("--");
                this.mn.writeBytes(this.mo);
                this.mn.writeBytes("\r\n");
                this.mn.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str3 + '\"');
                this.mn.writeBytes("\r\n");
                if (str2 != null) {
                    this.mn.writeBytes("Content-Type: " + str2);
                    this.mn.writeBytes("\r\n");
                }
                this.mn.writeBytes("\r\n");
                while (true) {
                    int read = inputStream.read(acquireBytes, 0, acquireBytes.length);
                    if (read != -1) {
                        this.mn.write(acquireBytes, 0, read);
                    } else {
                        this.mn.writeBytes("\r\n");
                        this.mn.flush();
                        aZ.close(inputStream);
                        C0030ba.releaseBytes(acquireBytes);
                        return;
                    }
                }
            } catch (IOException e) {
                throw e;
            } catch (Throwable th) {
                aZ.close(inputStream);
                C0030ba.releaseBytes(acquireBytes);
                throw th;
            }
        }
    }

    public final void writeFile(String str, String str2, String str3, byte[] bArr) throws IOException {
        if (bArr == null) {
            throw new IllegalArgumentException("Data cannot be null.");
        } else if (str3 == null || str3.length() == 0) {
            throw new IllegalArgumentException("File name cannot be null or empty.");
        } else {
            this.mn.writeBytes("--");
            this.mn.writeBytes(this.mo);
            this.mn.writeBytes("\r\n");
            this.mn.writeBytes("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str3 + '\"');
            this.mn.writeBytes("\r\n");
            if (str2 != null) {
                this.mn.writeBytes("Content-Type: " + str2);
                this.mn.writeBytes("\r\n");
            }
            this.mn.writeBytes("\r\n");
            this.mn.write(bArr, 0, bArr.length);
            this.mn.writeBytes("\r\n");
            this.mn.flush();
        }
    }
}
