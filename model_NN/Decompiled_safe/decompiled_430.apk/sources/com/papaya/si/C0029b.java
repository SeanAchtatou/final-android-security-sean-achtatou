package com.papaya.si;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.SparseArray;
import com.papaya.social.SocialRegistrationActivity;
import com.papaya.view.OverlayCustomDialog;
import com.papaya.view.OverlayProgressDialog;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.papaya.si.b  reason: case insensitive filesystem */
public final class C0029b {
    private static final List<Class> d = Arrays.asList(SocialRegistrationActivity.class);
    private static final bK e = new bK();
    private static final C0037bh<Activity> f = new C0037bh<>(8);
    private static final HashMap<Class, WeakReference<Activity>> g = new HashMap<>(2);
    private static WeakReference<Activity> h;
    private static final SparseArray<OverlayCustomDialog> i = new SparseArray<>(8);

    private C0029b() {
    }

    public static void clear() {
        i.clear();
        f.clear();
        h = null;
    }

    public static synchronized <T extends Activity> T findActivity(Class<T> cls) {
        T t;
        int i2 = 0;
        synchronized (C0029b.class) {
            try {
                int size = f.size();
                while (true) {
                    if (i2 < size) {
                        t = (Activity) f.get(i2);
                        if (t != null && cls.equals(t.getClass())) {
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
            } catch (Exception e2) {
                X.e(e2, "Failed to find activity: " + cls, new Object[0]);
            }
        }
        return t;
        t = null;
        return t;
    }

    public static synchronized <T extends Activity> T findActivity(Class<T> cls, int i2) {
        T t;
        synchronized (C0029b.class) {
            int size = f.size();
            int i3 = 0;
            while (true) {
                if (i3 < size) {
                    t = (Activity) f.get(i3);
                    if (t != null && cls.isInstance(t) && t.hashCode() == i2) {
                        break;
                    }
                    i3++;
                } else {
                    t = null;
                    break;
                }
            }
        }
        return t;
    }

    public static synchronized <T extends Activity> T findSingletonActivity(Class<T> cls) {
        T t;
        synchronized (C0029b.class) {
            WeakReference weakReference = g.get(cls);
            t = weakReference == null ? null : (Activity) weakReference.get();
        }
        return t;
    }

    public static void finishAllActivities() {
        C0037bh bhVar = new C0037bh();
        bhVar.addAll(f);
        int size = bhVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            Activity activity = (Activity) bhVar.get(i2);
            if (activity != null) {
                try {
                    activity.finish();
                } catch (Exception e2) {
                    X.e(e2, "Failed to finish activity: " + activity, new Object[0]);
                }
            }
        }
    }

    public static void finishLoginActivity() {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                SocialRegistrationActivity socialRegistrationActivity = (SocialRegistrationActivity) C0029b.findActivity(SocialRegistrationActivity.class);
                if (socialRegistrationActivity != null) {
                    socialRegistrationActivity.finish();
                }
            }
        });
    }

    public static synchronized Activity getActiveActivity() {
        Activity activity;
        synchronized (C0029b.class) {
            activity = h == null ? null : h.get();
        }
        return activity;
    }

    private static int getDialogID(int i2) {
        switch (i2) {
            case 1:
                return 5;
            case 2:
                return 5;
            case 3:
                return 5;
            case 4:
                return 0;
            case 5:
                return 0;
            case 6:
                return 1;
            case 7:
                return 4;
            case 8:
                return -1;
            case 9:
                return 2;
            case 10:
                return 3;
            default:
                X.e("Unknown dialog style: " + i2, new Object[0]);
                return -1;
        }
    }

    private static OverlayCustomDialog getOverlayDialog(int i2) {
        OverlayCustomDialog overlayCustomDialog = i.get(i2);
        if (overlayCustomDialog == null) {
            switch (i2) {
                case 0:
                    overlayCustomDialog = new OverlayProgressDialog(C0042c.getApplicationContext());
                    overlayCustomDialog.setCancelable(false);
                    break;
                case 1:
                    overlayCustomDialog = new OverlayCustomDialog.Builder(C0042c.getApplicationContext()).setCancelable(false).setNegativeButton(C0065z.stringID("base_no"), new OverlayCustomDialog.OnClickListener() {
                        public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
                            C0042c.A.close();
                        }
                    }).setPositiveButton(C0065z.stringID("base_yes"), new OverlayCustomDialog.OnClickListener() {
                        public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
                            overlayCustomDialog.hide();
                            C0042c.A.setPaused(false);
                        }
                    }).create();
                    break;
                case 2:
                    overlayCustomDialog = new OverlayCustomDialog.Builder(C0042c.getApplicationContext()).setTitle(C0065z.stringID("warning")).setMessage(C0065z.stringID("base_exit_confirm")).setNegativeButton(C0065z.stringID("base_no"), new OverlayCustomDialog.OnClickListener() {
                        public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
                            overlayCustomDialog.hide();
                        }
                    }).setPositiveButton(C0065z.stringID("base_yes"), new OverlayCustomDialog.OnClickListener() {
                        public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
                            overlayCustomDialog.hide();
                            C0042c.quit();
                        }
                    }).create();
                    break;
                case 3:
                    overlayCustomDialog = new OverlayProgressDialog(C0042c.getApplicationContext());
                    overlayCustomDialog.setCancelable(false);
                    break;
                case 4:
                    overlayCustomDialog = new OverlayProgressDialog(C0042c.getApplicationContext());
                    overlayCustomDialog.setCancelable(false);
                    break;
                case 5:
                    overlayCustomDialog = new OverlayProgressDialog(C0042c.getApplicationContext());
                    overlayCustomDialog.setCancelable(false);
                    break;
                default:
                    X.e("unknown dialog id: " + i2, new Object[0]);
                    break;
            }
            i.put(i2, overlayCustomDialog);
        }
        return overlayCustomDialog;
    }

    public static bK getUIHelper() {
        return e;
    }

    public static void hideAllOverlayDialogs() {
        for (int i2 = 0; i2 < i.size(); i2++) {
            try {
                OverlayCustomDialog valueAt = i.valueAt(i2);
                if (valueAt != null) {
                    valueAt.hide();
                }
            } catch (Exception e2) {
                X.e(e2, "Failed to hide dialog", new Object[0]);
            }
        }
        i.clear();
    }

    public static synchronized int historyActivityCount() {
        int i2;
        int i3 = 0;
        synchronized (C0029b.class) {
            int size = f.size();
            i2 = 0;
            while (i3 < size) {
                Activity activity = f.get(i3);
                i3++;
                i2 = (activity == null || activity.getParent() != null) ? i2 : i2 + 1;
            }
        }
        return i2;
    }

    private static boolean isSingletonIntent(Intent intent) {
        ComponentName component = intent.getComponent();
        if (component != null) {
            for (int i2 = 0; i2 < d.size(); i2++) {
                if (d.get(i2).getName().equals(component.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static synchronized int liveActivityCount() {
        int size;
        synchronized (C0029b.class) {
            size = f.size();
        }
        return size;
    }

    public static synchronized void onCreated(Activity activity) {
        synchronized (C0029b.class) {
            C0047h.startSession(activity);
            if (d.contains(activity.getClass())) {
                if (findSingletonActivity(activity.getClass()) != null) {
                    X.e("Duplicated singleton activity INSTANCEs: " + activity.getClass(), new Object[0]);
                }
                g.put(activity.getClass(), new WeakReference(activity));
            }
            f.add(activity);
        }
    }

    public static synchronized void onDestroyed(Activity activity) {
        synchronized (C0029b.class) {
            f.remove(activity);
            g.remove(activity.getClass());
            C0047h.endSession(activity);
            if (f.isEmpty()) {
                C0042c.A.startDisconnectTask();
            }
        }
    }

    public static synchronized void onFinished(Activity activity) {
        synchronized (C0029b.class) {
            f.remove(activity);
            g.remove(activity.getClass());
            if (f.isEmpty()) {
                C0042c.A.startDisconnectTask();
            }
        }
    }

    public static synchronized void onPaused(Activity activity) {
        synchronized (C0029b.class) {
            if (getActiveActivity() == activity) {
                h = null;
            }
            setOverdialogVisibilities(4);
        }
    }

    public static synchronized void onResumed(Activity activity) {
        synchronized (C0029b.class) {
            h = new WeakReference<>(activity);
            setOverdialogVisibilities(0);
            if (!C0035bf.isNetworkAvailable()) {
                C0036bg.showToast(C0042c.getString("toast_network"), 1);
            } else {
                C0036bg.showMoreTipToast();
            }
        }
    }

    public static void openHome(Activity activity, int i2) {
        if (i2 == A.bK.getUserID() || C0042c.getSession().getFriends().isFriend(i2)) {
            openPRIALink(activity, "static_friend_home?uid=" + i2, null, true, null);
        } else {
            openPRIALink(activity, "static_userinfo?uid=" + i2, null, true, null);
        }
    }

    public static void openPRIALink(Context context, String str) {
        openPRIALink(context, str, null, true, null);
    }

    public static void openPRIALink(Context context, String str, String str2) {
        openPRIALink(context, str, str2, true, null);
    }

    public static void openPRIALink(Context context, String str, String str2, boolean z, String str3) {
        if (C0036bg.isMainThread()) {
            openPRIALinkIMT(context, str, str2, z, str3);
            return;
        }
        final Context context2 = context;
        final String str4 = str;
        final String str5 = str2;
        final boolean z2 = z;
        final String str6 = str3;
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                C0029b.openPRIALinkIMT(context2, str4, str5, z2, str6);
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void openPRIALinkIMT(android.content.Context r4, java.lang.String r5, java.lang.String r6, boolean r7, java.lang.String r8) {
        /*
            boolean r1 = com.papaya.si.C0030ba.isEmpty(r6)
            if (r1 == 0) goto L_0x0061
            boolean r1 = com.papaya.si.C0030ba.isEmpty(r5)
            if (r1 != 0) goto L_0x0026
            java.lang.String r1 = "friends://"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x0027
            android.content.Intent r1 = new android.content.Intent
            android.app.Application r2 = com.papaya.si.C0042c.getApplicationContext()
            java.lang.Class<com.papaya.chat.FriendsActivity> r3 = com.papaya.chat.FriendsActivity.class
            r1.<init>(r2, r3)
            android.content.Intent r1 = setActivityModes(r1, r8)
            startActivity(r4, r1)
        L_0x0026:
            return
        L_0x0027:
            java.lang.String r1 = "chat://"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x0042
            android.content.Intent r1 = new android.content.Intent
            android.app.Application r2 = com.papaya.si.C0042c.getApplicationContext()
            java.lang.Class<com.papaya.chat.ChatActivity> r3 = com.papaya.chat.ChatActivity.class
            r1.<init>(r2, r3)
            android.content.Intent r1 = setActivityModes(r1, r8)
            startActivity(r4, r1)
            goto L_0x0026
        L_0x0042:
            android.content.Intent r1 = new android.content.Intent
            android.app.Application r2 = com.papaya.si.C0042c.getApplicationContext()
            java.lang.Class<com.papaya.web.WebActivity> r3 = com.papaya.web.WebActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "init_url"
            android.content.Intent r1 = r1.putExtra(r2, r5)
            java.lang.String r2 = "extra_require_sid"
            android.content.Intent r1 = r1.putExtra(r2, r7)
            android.content.Intent r1 = setActivityModes(r1, r8)
            startActivity(r4, r1)
            goto L_0x0026
        L_0x0061:
            r2 = 0
            if (r4 == 0) goto L_0x00e7
            boolean r1 = r4 instanceof com.papaya.base.EntryActivity
            if (r1 == 0) goto L_0x0091
            r0 = r4
            com.papaya.base.EntryActivity r0 = (com.papaya.base.EntryActivity) r0
            r1 = r0
        L_0x006c:
            int r2 = com.papaya.base.EntryActivity.getTabIndex(r6)
            if (r2 < 0) goto L_0x00cc
            if (r1 == 0) goto L_0x00ac
            r1.setCurrentTab(r2)
            boolean r2 = com.papaya.si.C0030ba.isEmpty(r5)
            if (r2 != 0) goto L_0x0026
            android.app.Activity r2 = r1.getCurrentActivity()
            boolean r2 = r2 instanceof com.papaya.web.WebActivity
            if (r2 == 0) goto L_0x0026
            android.app.Activity r4 = r1.getCurrentActivity()
            com.papaya.web.WebActivity r4 = (com.papaya.web.WebActivity) r4
            com.papaya.web.WebActivity r4 = (com.papaya.web.WebActivity) r4
            r4.openUrl(r5)
            goto L_0x0026
        L_0x0091:
            boolean r1 = r4 instanceof android.app.Activity
            if (r1 == 0) goto L_0x00e7
            r0 = r4
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            android.app.Activity r1 = r1.getParent()
            boolean r1 = r1 instanceof com.papaya.base.EntryActivity
            if (r1 == 0) goto L_0x00e7
            r0 = r4
            android.app.Activity r0 = (android.app.Activity) r0
            r1 = r0
            android.app.Activity r1 = r1.getParent()
            com.papaya.base.EntryActivity r1 = (com.papaya.base.EntryActivity) r1
            goto L_0x006c
        L_0x00ac:
            android.content.Intent r1 = new android.content.Intent
            android.app.Application r2 = com.papaya.si.C0042c.getApplicationContext()
            java.lang.Class<com.papaya.base.EntryActivity> r3 = com.papaya.base.EntryActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "active_tab"
            android.content.Intent r1 = r1.putExtra(r2, r6)
            java.lang.String r2 = "active_tab_url"
            android.content.Intent r1 = r1.putExtra(r2, r5)
            android.content.Intent r1 = setActivityModes(r1, r8)
            startActivity(r4, r1)
            goto L_0x0026
        L_0x00cc:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "unknown tab name: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.papaya.si.X.w(r1, r2)
            goto L_0x0026
        L_0x00e7:
            r1 = r2
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.C0029b.openPRIALinkIMT(android.content.Context, java.lang.String, java.lang.String, boolean, java.lang.String):void");
    }

    public static void removeOverlayDialog(final int i2) {
        if (C0036bg.isMainThread()) {
            removeOverlayDialogIMT(i2);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0029b.removeOverlayDialogIMT(i2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void removeOverlayDialogIMT(int i2) {
        OverlayCustomDialog overlayCustomDialog = i.get(getDialogID(i2));
        if (overlayCustomDialog != null) {
            overlayCustomDialog.hide();
        }
    }

    private static Intent setActivityModes(Intent intent, String str) {
        if (str != null) {
            for (String str2 : str.split(",")) {
                if ("newtask".equals(str2)) {
                    intent.addFlags(268435456);
                } else if ("cleartop".equals(str2)) {
                    intent.addFlags(67108864);
                } else if ("nohistory".equals(str2)) {
                    intent.addFlags(1073741824);
                } else if ("singletop".equals(str2)) {
                    intent.addFlags(536870912);
                } else if ("reorder".equals(str2)) {
                    intent.addFlags(131072);
                } else if ("multitask".equals(str2)) {
                    intent.addFlags(134217728);
                }
            }
        }
        return intent;
    }

    private static void setOverdialogVisibilities(int i2) {
        int size = i.size();
        for (int i3 = 0; i3 < size; i3++) {
            OverlayCustomDialog valueAt = i.valueAt(i3);
            if (valueAt != null) {
                valueAt.setVisibility(i2);
            }
        }
    }

    public static void showInfo(String str) {
        C0036bg.showToast(str, 1);
    }

    public static void showOverlayDialog(final int i2) {
        if (C0036bg.isMainThread()) {
            showOverlayDialogIMT(i2);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0029b.showOverlayDialogIMT(i2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void showOverlayDialogIMT(int i2) {
        OverlayCustomDialog overlayDialog = getOverlayDialog(getDialogID(i2));
        if (overlayDialog != null) {
            overlayDialog.setVisibility(getActiveActivity() != null ? 0 : 4);
            switch (i2) {
                case 1:
                    overlayDialog.setMessage(C0065z.stringID("alert_sync_importing"));
                    break;
                case 2:
                    overlayDialog.setMessage(C0065z.stringID("alert_sync_exporting"));
                    break;
                case 3:
                    overlayDialog.setMessage(C0065z.stringID("alert_sync_merging"));
                    break;
                case 4:
                    overlayDialog.setTitle(C0042c.getSession().isLoggedIn() ? C0065z.stringID("base_reconnect") : C0065z.stringID("base_connect"));
                    overlayDialog.setMessage(C0042c.A.getFunFact());
                    break;
                case 5:
                    overlayDialog.getTitleView().setTexts(15000, C0065z.stringID("base_login"), C0065z.stringID("base_stilllogin"));
                    overlayDialog.setMessage(C0042c.A.getFunFact());
                    break;
                case 6:
                    overlayDialog.setMessage(C0065z.stringID("base_tryconnect"));
                    overlayDialog.setTitle(C0065z.stringID("warning"));
                    break;
                case 7:
                    overlayDialog.setMessage(C0065z.stringID("base_entry_wait"));
                    overlayDialog.setTitle(C0065z.stringID("base_entry_clear"));
                    break;
                case 8:
                case 9:
                    break;
                case 10:
                    overlayDialog.setMessage(C0065z.stringID("alert_updating"));
                    break;
                default:
                    X.e("Unknown dialog style: " + i2, new Object[0]);
                    break;
            }
            overlayDialog.show();
            return;
        }
        X.e("Overlay dialog %d is not implemented yet", Integer.valueOf(i2));
    }

    public static void startActivity(final Context context, final Intent intent) {
        if (C0036bg.isMainThread()) {
            startActivityIMT(context, intent);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    C0029b.startActivityIMT(context, intent);
                }
            });
        }
    }

    public static synchronized void startActivity(Intent intent) {
        synchronized (C0029b.class) {
            startActivity(getActiveActivity(), intent);
        }
    }

    public static void startActivityForResult(final Activity activity, final Intent intent, final int i2) {
        if (activity == null) {
            X.e("null activity or intent to startActivityForResult: %s, %d", intent, Integer.valueOf(i2));
        } else if (C0036bg.isMainThread()) {
            activity.startActivityForResult(intent, i2);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    activity.startActivityForResult(intent, i2);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void startActivityIMT(Context context, Intent intent) {
        Context activeActivity = context == null ? getActiveActivity() : context;
        if ((activeActivity instanceof Activity) && !f.contains(activeActivity)) {
            activeActivity = getActiveActivity();
        }
        if (activeActivity == null) {
            activeActivity = C0042c.getApplicationContext();
        }
        if (!(activeActivity instanceof Activity)) {
            intent.addFlags(268435456);
        }
        try {
            activeActivity.startActivity(intent);
        } catch (Exception e2) {
            X.e("Failed to start activity %s, error: %s", intent, e2);
        }
    }
}
