package com.papaya.si;

import com.papaya.web.WebViewController;
import java.net.URL;
import java.util.Vector;

public final class bv implements C0059t {
    private WebViewController mr;

    private void hideLoading() {
    }

    public final void callJS(String str) {
        if (this.mr != null) {
            this.mr.callJS(str);
        }
    }

    public final void clear() {
        this.mr = null;
    }

    public final WebViewController getController() {
        return this.mr;
    }

    public final boolean handlePapayaUrl(WebViewController webViewController, bp bpVar, String str, String str2, String str3, URL url) {
        return false;
    }

    public final void handleServerResponse(final Vector<Object> vector) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                bv.this.handlerServerResponseInUIThread(vector);
            }
        });
    }

    public final void handlerServerResponseInUIThread(Vector<Object> vector) {
    }

    public final void onGameClosed() {
    }

    public final void openUrl(String str) {
    }

    public final void setController(WebViewController webViewController) {
        this.mr = webViewController;
    }

    public final void setWebView(bp bpVar) {
    }

    public final void startGame(int i, int i2, String[] strArr, String[] strArr2, byte[] bArr, int i3, int i4, int i5) {
    }

    public final void startScript(byte[] bArr) {
    }
}
