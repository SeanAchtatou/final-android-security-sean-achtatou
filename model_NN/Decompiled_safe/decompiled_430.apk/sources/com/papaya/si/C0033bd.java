package com.papaya.si;

/* renamed from: com.papaya.si.bd  reason: case insensitive filesystem */
public final class C0033bd<A, B> {
    public A first;
    public B second;

    public C0033bd() {
    }

    public C0033bd(A a, B b) {
        this.first = a;
        this.second = b;
    }

    public static <F, S> C0033bd<F, S> make(F f, S s) {
        return new C0033bd<>(f, s);
    }

    public final String toString() {
        return "Pair{a=" + ((Object) this.first) + ", b=" + ((Object) this.second) + '}';
    }
}
