package com.papaya.si;

import android.content.Context;
import android.os.Environment;
import java.io.File;

public abstract class B {
    private String ch;
    protected File ci;
    protected Context cj;
    protected boolean ck = true;

    public B(String str, Context context) {
        this.ch = str;
        this.cj = context;
    }

    public boolean clearCache() {
        aZ.clearDir(this.ci);
        return true;
    }

    public void close() {
        doClose();
        this.ck = true;
    }

    /* access modifiers changed from: protected */
    public abstract void doClose();

    /* access modifiers changed from: protected */
    public abstract boolean doInitCache();

    public File getCacheDir() {
        return this.ci;
    }

    public String getCacheDirName() {
        return this.ch;
    }

    public File getCacheFile(String str) {
        return new File(this.ci, keyToStoreName(str));
    }

    public Context getContext() {
        return this.cj;
    }

    public boolean initCache() {
        prepareCacheDir();
        this.ck = false;
        return aZ.exist(this.ci) && doInitCache();
    }

    /* access modifiers changed from: protected */
    public abstract String keyToStoreName(String str);

    public byte[] loadBytesWithKey(String str) {
        return aZ.dataFromFile(getCacheFile(str));
    }

    /* access modifiers changed from: protected */
    public void prepareCacheDir() {
        if (C0035bf.hF) {
            this.ci = new File(Environment.getExternalStorageDirectory(), this.ch);
            if (!this.ci.exists()) {
                this.ci.mkdirs();
            }
            X.i("cache dir in external storage", new Object[0]);
        } else {
            this.ci = this.cj.getDir(this.ch, 1);
            X.i("cache dir in phone storage", new Object[0]);
        }
        if (!this.ci.exists()) {
            X.w("cache dir %s, doesn't exist", this.ci);
        }
    }

    public boolean saveBytesWithKey(String str, byte[] bArr) {
        return aZ.writeBytesToFile(getCacheFile(str), bArr);
    }
}
