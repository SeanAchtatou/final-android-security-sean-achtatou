package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.papaya.view.BadgeView;
import com.papaya.view.CardImageView;

/* renamed from: com.papaya.si.aa  reason: case insensitive filesystem */
public final class C0003aa {
    public TextView dE;
    public BadgeView dF;
    public CardImageView imageView;
    public TextView subtitleView;
    public TextView titleView;

    public C0003aa(View view) {
        this.imageView = (CardImageView) C0036bg.find(view, "image");
        this.titleView = (TextView) C0036bg.find(view, "title");
        this.subtitleView = (TextView) C0036bg.find(view, "subtitle");
        this.dE = (TextView) C0036bg.find(view, "time");
        this.dF = (BadgeView) C0036bg.find(view, "unread");
    }
}
