package com.papaya.si;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;
import com.papaya.service.AppAccountManager;
import java.io.File;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

/* renamed from: com.papaya.si.bf  reason: case insensitive filesystem */
public final class C0035bf {
    public static String ANDROID_ID;
    public static String hB;
    private static String hC;
    public static String hD;
    public static String hE;
    public static boolean hF = false;

    private C0035bf() {
    }

    public static void checkFreeSpace(Context context) {
        if (context != null) {
            try {
                if (getFreeSpaceSize(context.getFilesDir().getAbsolutePath()) < 2097152) {
                    C0036bg.showToast(C0042c.getString("sys_nospace"), 1);
                }
                if (hF && getFreeSpaceSize(Environment.getExternalStorageDirectory().getAbsolutePath()) < 2097152) {
                    C0036bg.showToast(C0042c.getString("sys_nospace"), 1);
                }
            } catch (Exception e) {
                X.w(e, "Failed to checkFreeSpace", new Object[0]);
            }
        }
    }

    public static long getFreeSpaceSize(String str) {
        long j = 0;
        if (str == null) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(str);
            j = ((long) statFs.getBlockSize()) * 1 * ((long) statFs.getAvailableBlocks());
            X.i("freespace %d at %s", Long.valueOf(j), str);
            return j;
        } catch (Exception e) {
            X.w(e, "Failed to get free space size at " + str, new Object[0]);
            return j;
        }
    }

    public static JSONObject getSystemInfo(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2 = jSONObject == null ? new JSONObject() : jSONObject;
        try {
            jSONObject2.putOpt("A_ID", C0030ba.getDeviceID(context));
            jSONObject2.putOpt("B.ID", Build.ID);
            jSONObject2.putOpt("B.PRODUCT", Build.PRODUCT);
            jSONObject2.putOpt("B.DEVICE", Build.DEVICE);
            jSONObject2.putOpt("B.BOARD", Build.BOARD);
            jSONObject2.putOpt("B.BRAND", Build.BRAND);
            jSONObject2.putOpt("B.MODEL", Build.MODEL);
            jSONObject2.putOpt("B.TYPE", Build.TYPE);
            jSONObject2.putOpt("B.TAGS", Build.TAGS);
            jSONObject2.putOpt("B.V.INCREMENTAL", Build.VERSION.INCREMENTAL);
            jSONObject2.putOpt("B.V.RELEASE", Build.VERSION.RELEASE);
            jSONObject2.putOpt("B.V.SDK", Build.VERSION.SDK);
            jSONObject2.putOpt("Locale", Locale.getDefault().toString());
            jSONObject2.putOpt("G.MARKET", Integer.valueOf(supportMarket() ? 1 : 0));
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            jSONObject2.putOpt("D.width", Integer.valueOf(defaultDisplay.getWidth()));
            jSONObject2.putOpt("D.height", Integer.valueOf(defaultDisplay.getHeight()));
            jSONObject2.putOpt("P.id", context.getApplicationContext().getPackageName());
            jSONObject2.putOpt("P.v", Integer.valueOf(C0061v.bc));
            jSONObject2.putOpt("P.s", C0061v.be);
            jSONObject2.putOpt("P.l", C0061v.bd);
            jSONObject2.putOpt("D.ACCOUNTS", AppAccountManager.getWrapper().listAccounts2JSON());
        } catch (Exception e) {
        }
        return jSONObject2;
    }

    public static JSONObject getTelephonyInfo(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2 = jSONObject == null ? new JSONObject() : jSONObject;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            jSONObject2.putOpt("T.DeviceId", C0030ba.emptyAsNull(telephonyManager.getDeviceId()));
            jSONObject2.putOpt("T.NetworkType", Integer.valueOf(telephonyManager.getNetworkType()));
            jSONObject2.putOpt("T.PhoneType", Integer.valueOf(telephonyManager.getPhoneType()));
            jSONObject2.putOpt("T.NetworkCountryIso", C0030ba.emptyAsNull(telephonyManager.getNetworkCountryIso()));
            jSONObject2.putOpt("T.NetworkOperator", C0030ba.emptyAsNull(telephonyManager.getNetworkOperator()));
            jSONObject2.putOpt("T.NetworkOperatorName", C0030ba.emptyAsNull(telephonyManager.getNetworkOperatorName()));
            jSONObject2.putOpt("T.SimCountryIso", C0030ba.emptyAsNull(telephonyManager.getSimCountryIso()));
            jSONObject2.putOpt("T.SimOperator", C0030ba.emptyAsNull(telephonyManager.getSimOperator()));
            jSONObject2.putOpt("T.SimOperatorName", C0030ba.emptyAsNull(telephonyManager.getSimOperatorName()));
            jSONObject2.putOpt("T.SubscriberId", C0030ba.emptyAsNull(telephonyManager.getSubscriberId()));
            jSONObject2.putOpt("T.Line1Number", C0030ba.emptyAsNull(telephonyManager.getLine1Number()));
            jSONObject2.putOpt("T.MacAddress", C0030ba.emptyAsNull(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress()));
        } catch (Exception e) {
        }
        return jSONObject2;
    }

    public static void initialize(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            hB = telephonyManager.getDeviceId();
            hC = telephonyManager.getSubscriberId();
        } catch (Exception e) {
        }
        try {
            ANDROID_ID = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e2) {
            X.w(e2, "Failed to get ANDROID_ID", new Object[0]);
        }
        if (C0030ba.isEmpty(ANDROID_ID)) {
            ANDROID_ID = "emulator";
        }
        if (hC == null) {
            hC = "";
        }
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            defaultDisplay.getWidth();
            defaultDisplay.getHeight();
        } catch (Exception e3) {
            X.e(e3, "Failed to get display info", new Object[0]);
        }
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                File file = new File(Environment.getExternalStorageDirectory(), "__ppy_tmp");
                if (file.exists()) {
                    file.delete();
                }
                if (file.createNewFile()) {
                    hF = true;
                    file.delete();
                }
            }
        } catch (Exception e4) {
        }
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo.getMacAddress() != null) {
                hD = connectionInfo.getMacAddress();
            } else {
                hD = "";
            }
        } catch (Exception e5) {
        }
        try {
            TelephonyManager telephonyManager2 = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager2.getLine1Number() != null) {
                hE = telephonyManager2.getLine1Number();
            } else {
                hE = "";
            }
        } catch (Exception e6) {
        }
    }

    public static boolean isNetworkAvailable() {
        return isNetworkAvailable(null);
    }

    public static boolean isNetworkAvailable(Context context) {
        Context context2;
        if (context == null) {
            try {
                context2 = C0042c.getApplicationContext();
            } catch (Exception e) {
                X.w("Failed to check network status: " + e, new Object[0]);
                return true;
            }
        } else {
            context2 = context;
        }
        if (context2 == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context2.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean supportIntent(Intent intent) {
        if (intent == null) {
            return false;
        }
        try {
            List<ResolveInfo> queryIntentActivities = C0042c.getApplicationContext().getPackageManager().queryIntentActivities(intent, 0);
            return queryIntentActivities != null && queryIntentActivities.size() > 0;
        } catch (Exception e) {
            X.e(e, "Failed in supportIntent", new Object[0]);
            return false;
        }
    }

    public static boolean supportMarket() {
        try {
            return supportIntent(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.papaya.papayafree2")));
        } catch (Exception e) {
            X.e(e, "Failed in supportMarket", new Object[0]);
            return false;
        }
    }

    public static boolean supportPicasa() {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("image/jpeg");
            intent.addFlags(1);
            intent.setComponent(new ComponentName("com.google.android.apps.uploader", "com.google.android.apps.uploader.picasa.PicasaSettingsActivity"));
            return supportIntent(intent);
        } catch (Exception e) {
            X.e(e, "Failed in supportPicasa", new Object[0]);
            return false;
        }
    }
}
