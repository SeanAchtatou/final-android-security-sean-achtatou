package com.papaya.si;

import java.io.File;
import java.util.HashMap;

public final class aJ extends bA {
    public aJ() {
        this.mz = false;
        this.nk = true;
        HashMap hashMap = new HashMap();
        hashMap.put("api_version", 173);
        hashMap.put("model", 6);
        hashMap.put("device", C0035bf.ANDROID_ID);
        hashMap.put("device2", C0035bf.hB);
        hashMap.put("app_id", C0061v.bm);
        hashMap.put("api_key", aA.getInstance().getApiKey());
        hashMap.put("db_version", Integer.valueOf(C0017ao.getInstance().getVersion()));
        hashMap.put("lang", C0061v.bd);
        hashMap.put("sig", aZ.md5(C0030ba.format("%s_%s_%s_%s", C0035bf.ANDROID_ID, C0035bf.hB, aA.getInstance().getApiKey(), "Papaya Social 1.7")));
        hashMap.put("__s", C0040bk.encrypt(C0035bf.getSystemInfo(null, C0042c.getApplicationContext()).toString()));
        hashMap.put("__t", C0040bk.encrypt(C0035bf.getTelephonyInfo(null, C0042c.getApplicationContext()).toString()));
        File file = new File(C0042c.getApplicationContext().getFilesDir(), C0028az.fL);
        if (file.exists() && file.length() > 0) {
            String utf8String = C0030ba.utf8String(aZ.dataFromFile(file), null);
            if (C0030ba.isNotEmpty(utf8String)) {
                hashMap.put("__credential", utf8String);
                hashMap.put("__sig", aZ.md5(utf8String + aA.getInstance().getApiKey()));
            }
        }
        this.url = C0040bk.createURL(C0040bk.compositeUrl("json_login", hashMap));
    }

    public static boolean hasCredentialFile() {
        File file = new File(C0042c.getApplicationContext().getFilesDir(), C0028az.fL);
        return file.exists() && file.length() > 0;
    }
}
