package com.papaya.si;

import com.papaya.social.PPYUser;
import java.util.ArrayList;
import org.json.JSONArray;

public final class V extends C0013ak {
    public V() {
        setName(C0042c.getString("Papayafriends"));
        setReserveGroupHeader(true);
    }

    public final void addUserFromJSON(JSONArray jSONArray, int i) {
        if (jSONArray != null) {
            for (int i2 = i; i2 < jSONArray.length(); i2 += 2) {
                add(new C0012aj(jSONArray.optInt(i2), jSONArray.optString(i2 + 1)));
            }
        }
    }

    public final C0012aj findByUserID(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.co.size()) {
                return null;
            }
            C0012aj ajVar = (C0012aj) this.co.get(i3);
            if (ajVar.getUserID() == i) {
                return ajVar;
            }
            i2 = i3 + 1;
        }
    }

    public final boolean isFriend(int i) {
        return findByUserID(i) != null;
    }

    public final ArrayList<PPYUser> listUsers() {
        return new ArrayList<>(this.co);
    }

    public final void updateMiniblog(int i, String str, int i2) {
        C0012aj findByUserID = findByUserID(i);
        if (findByUserID != null) {
            findByUserID.dV = str;
            findByUserID.dW = (System.currentTimeMillis() / 1000) - ((long) i2);
        }
    }
}
