package com.papaya.si;

import android.content.DialogInterface;
import android.view.View;
import com.papaya.view.Action;
import com.papaya.view.CustomDialog;

class Z implements View.OnClickListener {
    private /* synthetic */ Y dA;

    Z(Y y) {
        this.dA = y;
    }

    public final void onClick(View view) {
        Action action = (Action) view.getTag();
        switch (action.id) {
            case 0:
                C0029b.openPRIALink(this.dA.dx, "static_newchatgroup");
                return;
            case 1:
                C0029b.openPRIALink(this.dA.dx, "static_searchchatgroup");
                return;
            case 2:
                C0006ad adVar = (C0006ad) action.data;
                adVar.setImState(2);
                C0042c.send(4, Integer.valueOf(adVar.dK), adVar.dM, adVar.dN);
                C0042c.getSession().fireDataStateChanged();
                return;
            case 3:
                C0006ad adVar2 = (C0006ad) action.data;
                C0042c.send(5, Integer.valueOf(adVar2.dL));
                C0042c.getSession().closeIM(adVar2);
                return;
            case 4:
                return;
            case 5:
                final C0006ad adVar3 = (C0006ad) action.data;
                if (adVar3 != null) {
                    new CustomDialog.Builder(this.dA.dx).setMessage(C0042c.getString("im_delete_confirm")).setPositiveButton(C0042c.getString("delete"), new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            C0042c.getSession().closeIM(C0006ad.this);
                            C0042c.getSession().getIms().remove(C0006ad.this);
                            C0042c.getSession().getIms().saveToFile("papayaim1");
                            C0042c.getSession().refreshCardLists();
                            C0042c.getSession().fireDataStateChanged();
                        }
                    }).setNegativeButton(C0042c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).show();
                    return;
                }
                return;
            default:
                X.e("unknown action id: " + action.id, new Object[0]);
                return;
        }
    }
}
