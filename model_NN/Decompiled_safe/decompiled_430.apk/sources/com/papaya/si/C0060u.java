package com.papaya.si;

import android.util.SparseArray;
import android.util.SparseIntArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/* renamed from: com.papaya.si.u  reason: case insensitive filesystem */
public final class C0060u implements C0031bb, C0059t, C0063x {
    private SparseArray<SparseIntArray> aW = new SparseArray<>();
    private SparseArray<C0038bi<Integer, a>> aX = new SparseArray<>();
    /* access modifiers changed from: private */
    public ArrayList<b> aY = new ArrayList<>(10);
    /* access modifiers changed from: private */
    public LinkedList<b> aZ = new LinkedList<>();
    private long ba;

    /* renamed from: com.papaya.si.u$a */
    public interface a {
        boolean onImageUpdated(int i, int i2, int i3);
    }

    /* renamed from: com.papaya.si.u$b */
    public static final class b {
        int id;
        int type;

        private b() {
        }

        /* synthetic */ b(int i, int i2) {
            this(i, i2, (byte) 0);
        }

        private b(int i, int i2, byte b) {
            this.type = i;
            this.id = i2;
        }

        public static void exportContacts() {
        }

        public static void importContacts() {
        }

        public static void mergeContacts() {
        }

        private static void updateProgress(int i) {
            X.i("progress: " + i, new Object[0]);
        }
    }

    public C0060u() {
        if (C0042c.A != null) {
            C0042c.A.addConnectionDelegate(this);
            C0042c.A.registerCmds(this, 24);
            return;
        }
        X.e("Papaya.papaya is null", new Object[0]);
    }

    private void addRequest(int i, int i2) {
        if (findRequest(this.aY, i, i2) == -1) {
            int findRequest = findRequest(this.aZ, i, i2);
            if (findRequest != -1) {
                this.aZ.addFirst(this.aZ.get(findRequest));
            } else {
                this.aZ.add(new b(i, i2));
            }
        }
    }

    private int findRequest(List<b> list, int i, int i2) {
        for (int i3 = 0; i3 < list.size(); i3++) {
            b bVar = list.get(i3);
            if (bVar.type == i && bVar.id == i2) {
                return i3;
            }
        }
        return -1;
    }

    private void fireUpdate(int i, int i2, int i3) {
        C0037bh list;
        C0038bi biVar = this.aX.get(i);
        if (biVar != null && (list = biVar.getList(Integer.valueOf(i2))) != null) {
            list.trimGarbage();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                if (aVar == null || aVar.onImageUpdated(i, i2, i3)) {
                    it.remove();
                }
            }
        }
    }

    private void removeRequest(List<b> list, int i, int i2) {
        int findRequest = findRequest(list, i, i2);
        if (findRequest != -1) {
            list.remove(findRequest);
        }
    }

    public final void addDelegate(a aVar, int i, int i2) {
        C0038bi biVar = this.aX.get(i);
        if (biVar == null) {
            biVar = new C0038bi();
            this.aX.put(i, biVar);
        }
        biVar.put(Integer.valueOf(i2), aVar);
    }

    public final void close() {
        if (C0042c.A != null) {
            C0042c.A.removeConnectionDelegate(this);
            C0042c.A.unregisterCmd(this, 24);
            this.aY.clear();
            this.aZ.clear();
            this.aX.clear();
        }
    }

    public final int getVersion(int i, int i2) {
        if (findRequest(this.aY, i, i2) != -1 || findRequest(this.aZ, i, i2) != -1) {
            return -1;
        }
        SparseIntArray sparseIntArray = this.aW.get(i);
        int i3 = sparseIntArray != null ? sparseIntArray.get(i2, -1) : -1;
        if (i3 != -1) {
            return i3;
        }
        addRequest(i, i2);
        return i3;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        int sgetInt = C0030ba.sgetInt(vector, 0);
        if (sgetInt == 24) {
            int sgetInt2 = C0030ba.sgetInt(vector, 1);
            int sgetInt3 = C0030ba.sgetInt(vector, 2);
            int sgetInt4 = C0030ba.sgetInt(vector, 3);
            removeRequest(this.aY, sgetInt2, sgetInt3);
            removeRequest(this.aZ, sgetInt2, sgetInt3);
            putVersion(sgetInt2, sgetInt3, sgetInt4);
            fireUpdate(sgetInt2, sgetInt3, sgetInt4);
            return;
        }
        X.e("unknown cmd: " + sgetInt, new Object[0]);
    }

    public final void onConnectionEstablished() {
    }

    public final void onConnectionLost() {
        this.aZ.addAll(this.aY);
        this.aY.clear();
    }

    public final void poll() {
        if (System.currentTimeMillis() - this.ba > 2000) {
            this.ba = System.currentTimeMillis();
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    int size;
                    if (C0042c.getSession().isLoggedIn() && (size = 10 - C0060u.this.aY.size()) > 0 && !C0060u.this.aZ.isEmpty()) {
                        int min = Math.min(size, C0060u.this.aZ.size());
                        for (int i = 0; i < min; i++) {
                            b bVar = (b) C0060u.this.aZ.removeFirst();
                            C0060u.this.aY.add(bVar);
                            C0042c.send(321, Integer.valueOf(bVar.type), Integer.valueOf(bVar.id));
                        }
                    }
                }
            });
        }
    }

    public final void putVersion(int i, int i2, int i3) {
        SparseIntArray sparseIntArray = this.aW.get(i);
        if (sparseIntArray == null) {
            sparseIntArray = new SparseIntArray();
            this.aW.put(i, sparseIntArray);
        }
        sparseIntArray.put(i2, i3);
    }

    public final void removeDelegate(a aVar, int i, int i2) {
        C0038bi biVar = this.aX.get(i);
        if (biVar != null) {
            biVar.remove(Integer.valueOf(i2), aVar);
        }
    }
}
