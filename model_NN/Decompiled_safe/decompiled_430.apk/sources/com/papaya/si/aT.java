package com.papaya.si;

import java.util.Iterator;

public final class aT implements aU {
    private aU hg;
    private C0037bh<aS> hh;
    private boolean hi = false;

    public aT(aU aUVar) {
        this.hg = aUVar;
    }

    public final void fireDataStateChanged() {
        if (C0036bg.isMainThread()) {
            fireDataStateChangedInUIThread();
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    aT.this.fireDataStateChangedInUIThread();
                }
            });
        }
    }

    public final void fireDataStateChangedInUIThread() {
        if (!this.hi) {
            this.hi = true;
            if (this.hh != null) {
                this.hh.trimGarbage();
                Iterator<aS> it = this.hh.iterator();
                while (it.hasNext()) {
                    aS next = it.next();
                    if (next != null) {
                        try {
                            if (next.onDataStateChanged(this.hg)) {
                                it.remove();
                            }
                        } catch (Exception e) {
                            X.e(e, "Failed to callback onDataStateChanged on " + next, new Object[0]);
                        }
                    }
                }
            }
            this.hi = false;
        }
    }

    public final int indexOf(aS aSVar) {
        if (this.hh != null) {
            for (int i = 0; i < this.hh.size(); i++) {
                if (this.hh.get(i) == aSVar) {
                    return i;
                }
            }
        }
        return -1;
    }

    public final void registerMonitor(aS aSVar) {
        if (this.hh == null) {
            this.hh = new C0037bh<>(4);
        }
        if (indexOf(aSVar) == -1) {
            this.hh.add(aSVar);
        }
    }

    public final void unregisterMonitor(aS aSVar) {
        int indexOf = indexOf(aSVar);
        if (indexOf != -1) {
            this.hh.remove(indexOf);
        }
    }
}
