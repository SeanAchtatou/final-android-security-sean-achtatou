package com.papaya.si;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.papaya.purchase.PPYPayment;
import com.papaya.purchase.PPYPaymentDelegate;
import com.papaya.social.PPYSession;
import com.papaya.social.PPYSocialQuery;
import com.papaya.view.LazyImageView;
import com.papaya.view.OverlayCustomDialog;
import com.papaya.view.UserImageView;
import org.json.JSONObject;

public final class aG extends OverlayCustomDialog implements PPYPaymentDelegate, PPYSocialQuery.QueryDelegate, OverlayCustomDialog.OnClickListener {
    private UserImageView gA;
    private TextView gB;
    private TextView gC;
    private int gD = -1;
    private ImageView gE;
    private LinearLayout gF;
    private PPYPayment gy;

    public aG(Context context, PPYPayment pPYPayment) {
        super(context);
        this.gy = pPYPayment;
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        LazyImageView lazyImageView = new LazyImageView(context);
        lazyImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (pPYPayment.getIconID() > 0) {
            lazyImageView.setImageResource(pPYPayment.getIconID());
        } else if (pPYPayment.getIconDrawable() != null) {
            lazyImageView.setImageDrawable(pPYPayment.getIconDrawable());
        } else if (pPYPayment.getIconURL() != null) {
            lazyImageView.setImageUrl(pPYPayment.getIconURL());
        } else {
            lazyImageView.setVisibility(8);
        }
        linearLayout2.addView(lazyImageView, new LinearLayout.LayoutParams(C0036bg.rp(45), C0036bg.rp(45)));
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = C0036bg.rp(5);
        linearLayout2.addView(linearLayout3, layoutParams);
        TextView textView = new TextView(context);
        textView.setText(pPYPayment.getName());
        textView.setTextColor(Color.parseColor("#eca337"));
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setTextSize(16.0f);
        linearLayout3.addView(textView, new LinearLayout.LayoutParams(-2, -2));
        ScrollView scrollView = new ScrollView(context);
        linearLayout3.addView(scrollView, new LinearLayout.LayoutParams(-2, -2));
        TextView textView2 = new TextView(context);
        textView2.setText(pPYPayment.getDescription());
        textView2.setTextColor(-12303292);
        scrollView.addView(textView2);
        this.gE = new ImageView(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, C0036bg.rp(1));
        layoutParams2.topMargin = C0036bg.rp(2);
        layoutParams2.bottomMargin = C0036bg.rp(5);
        this.gE.setBackgroundResource(C0065z.drawableID("line"));
        linearLayout.addView(this.gE, layoutParams2);
        this.gF = new LinearLayout(context);
        this.gF.setOrientation(0);
        linearLayout.addView(this.gF, new LinearLayout.LayoutParams(-1, -2));
        this.gA = new UserImageView(context);
        this.gF.addView(this.gA, new LinearLayout.LayoutParams(C0036bg.rp(45), C0036bg.rp(45)));
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setOrientation(1);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams3.leftMargin = C0036bg.rp(5);
        this.gF.addView(linearLayout4, layoutParams3);
        this.gB = new TextView(context);
        this.gB.setTextColor(-16777216);
        this.gB.setTypeface(Typeface.DEFAULT_BOLD);
        this.gB.setEllipsize(TextUtils.TruncateAt.MIDDLE);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        linearLayout4.addView(this.gB, layoutParams4);
        this.gC = new TextView(context);
        this.gC.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.weight = 1.0f;
        linearLayout4.addView(this.gC, layoutParams5);
        setView(linearLayout);
        setIcon(C0065z.drawableID("papaya"));
        setTitle(pPYPayment.getPapayas() + " " + C0042c.getString("papayas"));
        setButton(-2, C0042c.getString("close"), this);
        setButton(-3, C0036bg.prefixWithDrawable(C0042c.getDrawable("morepapayas"), " " + C0042c.getString("button_more"), C0036bg.rp(16), C0036bg.rp(16)), this);
        setButton(-1, C0036bg.prefixWithDrawable(C0042c.getDrawable("purchase"), " " + C0042c.getString("button_purchase"), C0036bg.rp(16), C0036bg.rp(16)), this);
        this.gE.setVisibility(8);
        this.gF.setVisibility(8);
        aA.getInstance().submitQuery(new PPYSocialQuery("papayas", this));
    }

    private void hideCallDelegate() {
        hide();
        PPYPaymentDelegate delegate = this.gy.getDelegate();
        if (delegate != null) {
            delegate.onPaymentClosed(this.gy);
        }
    }

    private void showMorePapayasTips() {
        C0036bg.showToast(C0042c.getString("tip_get_more_papayas"), 0);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultVisibility() {
        return 0;
    }

    public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
        if (i == -1) {
            if (this.gD < 0 || this.gD >= this.gy.getPapayas()) {
                showLoading();
                new aF(this.gy, this).start(true);
                return;
            }
            showMorePapayasTips();
        } else if (i == -3) {
            hideCallDelegate();
            C0036bg.post(new Runnable() {
                public final void run() {
                    C0029b.openPRIALink(null, "static_getpapayas");
                }
            });
        } else if (i == -2) {
            hideCallDelegate();
        }
    }

    public final void onPaymentClosed(PPYPayment pPYPayment) {
    }

    public final void onPaymentFailed(PPYPayment pPYPayment, int i, String str) {
        hideLoading();
        hide();
        PPYPaymentDelegate delegate = pPYPayment.getDelegate();
        if (delegate != null) {
            delegate.onPaymentFailed(pPYPayment, i, str);
        }
        X.w("onPaymentFailed: %d, %s", Integer.valueOf(i), str);
    }

    public final void onPaymentFinished(PPYPayment pPYPayment) {
        hideLoading();
        hide();
        PPYPaymentDelegate delegate = pPYPayment.getDelegate();
        if (delegate != null) {
            delegate.onPaymentFinished(pPYPayment);
        }
    }

    public final void onQueryFailed(PPYSocialQuery pPYSocialQuery, String str) {
    }

    public final void onQueryResponse(PPYSocialQuery pPYSocialQuery, JSONObject jSONObject) {
        this.gE.setVisibility(0);
        this.gF.setVisibility(0);
        this.gA.setImageUrl(C0030ba.format("getavatarhead?uid=%d", Integer.valueOf(PPYSession.getInstance().getUID())));
        this.gB.setText(aC.getInstance().getNickname());
        this.gD = jSONObject.optInt("papayas", 0);
        this.gC.setText(this.gD + " " + C0042c.getString("text_papayas_left"));
        if (this.gD < this.gy.getPapayas()) {
            showMorePapayasTips();
        }
    }
}
