package com.papaya.si;

import android.graphics.drawable.Drawable;
import com.papaya.si.D;
import com.papaya.view.Action;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class E<T extends D> extends aL implements Serializable {
    protected ArrayList<T> co = new ArrayList<>();
    private boolean cp = true;
    protected List<Action> cq;
    private transient Drawable icon;
    private String name = "";

    public synchronized boolean add(T t) {
        boolean z;
        if (this.co.contains(t)) {
            z = false;
        } else {
            this.co.add(t);
            z = true;
        }
        return z;
    }

    public synchronized void clear() {
        this.co.clear();
    }

    public synchronized boolean contains(T t) {
        return this.co.contains(t);
    }

    public synchronized void fireAllDataStateChanged() {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.co.size()) {
                    try {
                        ((D) this.co.get(i2)).fireDataStateChanged();
                    } catch (Exception e) {
                        X.e(e, "Failed to relay state changed", new Object[0]);
                    }
                    i = i2 + 1;
                } else {
                    fireDataStateChanged();
                }
            }
        }
    }

    public synchronized T get(int i) {
        return (D) this.co.get(i);
    }

    public List<Action> getActions() {
        return this.cq;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public String getLabel() {
        return C0030ba.format("%s (%d)", getName(), Integer.valueOf(size()));
    }

    public String getName() {
        return this.name;
    }

    public synchronized int indexOf(T t) {
        return this.co.indexOf(t);
    }

    public synchronized void insertSort(T t) {
        int binarySearch = Collections.binarySearch(this.co, t);
        if (binarySearch < 0) {
            this.co.add((-binarySearch) - 1, t);
        }
    }

    public boolean isReserveGroupHeader() {
        return this.cp;
    }

    public synchronized T remove(int i) {
        return (D) this.co.remove(i);
    }

    public synchronized boolean remove(D d) {
        return this.co.remove(d);
    }

    public synchronized boolean sectionHeaderVisible() {
        return this.co.size() > 0 || this.cp;
    }

    public void setCardStates(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.co.size()) {
                ((D) this.co.get(i3)).setState(i);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void setIcon(Drawable drawable) {
        this.icon = drawable;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setReserveGroupHeader(boolean z) {
        this.cp = z;
    }

    public synchronized int size() {
        return this.co.size();
    }

    public synchronized void sort() {
        Collections.sort(this.co);
    }

    public synchronized ArrayList<T> toList() {
        return new ArrayList<>(this.co);
    }
}
