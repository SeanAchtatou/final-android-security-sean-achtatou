package com.papaya.si;

import android.os.Handler;
import android.os.HandlerThread;

/* renamed from: com.papaya.si.o  reason: case insensitive filesystem */
public final class C0054o extends HandlerThread {
    public int W;
    private Handler af;
    public final C0046g ag;
    public final String ah;
    public final String ai;
    public int aj;
    public long ak;
    public C0049j al;
    public final C0044e am;

    public C0054o(C0044e eVar, C0046g gVar, String str, String str2) {
        super("DT");
        this.aj = 30;
        this.al = null;
        this.am = eVar;
        this.ah = str;
        this.ai = str2;
        this.ag = gVar;
        this.ag.installCallbacks$1489076e(new C0047h(this));
    }

    public C0054o(C0044e eVar, String str, String str2) {
        this(eVar, new C0046g(C0056q.ay), str, str2);
    }

    public final void dispatchEvents(C0055p[] pVarArr) {
        if (this.af != null) {
            try {
                this.af.post(new C0049j(this, pVarArr));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onLooperPrepared() {
        this.af = new Handler();
    }
}
