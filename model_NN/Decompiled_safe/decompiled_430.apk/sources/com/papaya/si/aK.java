package com.papaya.si;

import com.papaya.si.by;
import com.papaya.social.PPYSocialQuery;
import java.util.HashMap;
import org.json.JSONObject;

public final class aK extends bA implements C0031bb, by.a {
    private PPYSocialQuery gI;
    private long gz = System.currentTimeMillis();

    public aK(PPYSocialQuery pPYSocialQuery) {
        this.gI = pPYSocialQuery;
        HashMap hashMap = new HashMap(1);
        hashMap.put("payload", pPYSocialQuery.getPayloadString());
        this.url = C0040bk.createURL(C0040bk.compositeUrl("query", hashMap), C0061v.bj);
        this.nk = true;
        setDelegate(this);
    }

    public final void connectionFailed(by byVar, int i) {
        PPYSocialQuery.QueryDelegate queryDelegate = this.gI.getQueryDelegate();
        if (queryDelegate != null) {
            queryDelegate.onQueryFailed(this.gI, null);
        }
    }

    public final void connectionFinished(by byVar) {
        PPYSocialQuery.QueryDelegate queryDelegate = this.gI.getQueryDelegate();
        if (queryDelegate != null) {
            JSONObject parseJsonObject = C0040bk.parseJsonObject(C0030ba.utf8String(byVar.getData(), "{}"));
            if (C0040bk.getJsonInt(parseJsonObject, "status", 0) == 1) {
                queryDelegate.onQueryResponse(this.gI, parseJsonObject);
            } else {
                queryDelegate.onQueryFailed(this.gI, C0040bk.getJsonString(parseJsonObject, "error"));
            }
        }
    }

    public final boolean isExpired() {
        return this.gI.isCanceled() || System.currentTimeMillis() - this.gz > 45000;
    }
}
