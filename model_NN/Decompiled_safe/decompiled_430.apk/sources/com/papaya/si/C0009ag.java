package com.papaya.si;

import android.graphics.drawable.Drawable;

/* renamed from: com.papaya.si.ag  reason: case insensitive filesystem */
public final class C0009ag extends D {
    public int cU;
    public int dk;
    public String dl;

    public C0009ag() {
        this.state = 1;
    }

    public final Drawable getDefaultDrawable() {
        return C0042c.getBitmapDrawable("avatar_unknown");
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dl;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
