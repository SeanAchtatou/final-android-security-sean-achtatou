package com.papaya.si;

import android.content.Context;
import android.content.res.AssetManager;
import com.papaya.social.PPYSNSRegion;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* renamed from: com.papaya.si.a  reason: case insensitive filesystem */
public final class C0002a {
    /* access modifiers changed from: private */
    public static aD a;
    private static C0060u b;
    private String c;

    private C0002a() {
    }

    public C0002a(String str) {
        this.c = str;
    }

    public static void clearCaches() {
        C0029b.showOverlayDialog(7);
        new Thread(new Runnable() {
            public final void run() {
                C0002a.a.clearCache();
                C0029b.removeOverlayDialog(7);
            }
        }).start();
    }

    public static void destroy() {
        if (a != null) {
            a.close();
        }
        if (b != null) {
            b.close();
        }
    }

    public static C0060u getImageVersion() {
        return b;
    }

    public static aD getWebCache() {
        return a;
    }

    public static void initialize(Context context) {
        aD aDVar = new aD(aA.getInstance().getSocialConfig().getSNSRegion() == PPYSNSRegion.CHINA ? "social_cn_cache" : "social_cache", context);
        a = aDVar;
        aDVar.initCache();
        b = new C0060u();
    }

    public static void insertRequests(List<bA> list) {
        if (a != null) {
            a.insertRequests(list);
        } else {
            X.e("skip insertRequests", new Object[0]);
        }
    }

    public final InputStream openAssetInput(AssetManager assetManager) {
        if (this.c != null) {
            try {
                return assetManager.open(this.c);
            } catch (IOException e) {
                X.w(e, "Failed to openAssetInput %s", this.c);
            }
        } else {
            X.w("not an asset fd", new Object[0]);
            return null;
        }
    }

    public final InputStream openInput(AssetManager assetManager) {
        return openAssetInput(assetManager);
    }
}
