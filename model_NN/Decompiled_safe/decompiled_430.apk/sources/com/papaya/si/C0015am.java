package com.papaya.si;

import android.content.Context;
import java.util.HashMap;
import java.util.Vector;

/* renamed from: com.papaya.si.am  reason: case insensitive filesystem */
public final class C0015am implements aR, C0063x {
    private static C0015am ec;
    private HashMap<Integer, HashMap<String, C0014al>> ed = new HashMap<>();

    private C0015am() {
    }

    public static synchronized C0015am getInstance() {
        C0015am amVar;
        synchronized (C0015am.class) {
            if (ec == null) {
                ec = new C0015am();
            }
            amVar = ec;
        }
        return amVar;
    }

    public final synchronized void addDatabase(C0014al alVar) {
        if (alVar != null) {
            if (alVar.getDbId() != null) {
                HashMap<String, C0014al> ensureMap = ensureMap(alVar.getScope());
                if (!ensureMap.containsKey(alVar.getDbId())) {
                    ensureMap.put(alVar.getDbId(), alVar);
                } else {
                    X.w("key already exists %s", alVar.getDbId());
                }
            }
        }
        X.e("db or db name is null %s", alVar);
    }

    public final synchronized void clear() {
        try {
            if (C0042c.A != null) {
                C0042c.A.removeConnectionDelegate(this);
            }
        } catch (Exception e) {
            X.w(e, "Failed to removeConnectionDelegate", new Object[0]);
        }
        try {
            for (HashMap<String, C0014al> values : this.ed.values()) {
                for (C0014al close : values.values()) {
                    close.close();
                }
            }
            this.ed.clear();
        } catch (Exception e2) {
            X.e("Failed to close databases: " + e2, new Object[0]);
        }
        return;
    }

    public final C0014al connectionDB() {
        return openDatabase("__connection__", 3);
    }

    /* access modifiers changed from: protected */
    public final C0014al createDatabase(String str, int i) {
        String format;
        C0014al openFileDatabase;
        if (i <= 3) {
            if (i != 3) {
                X.w("DB with scope below connection SHOULD be managed at other places, %s, %d", str, Integer.valueOf(i));
            }
            openFileDatabase = C0014al.openMemoryDatabase();
            openFileDatabase.setDbId(str);
            openFileDatabase.setScope(i);
        } else {
            String md5 = aZ.md5(aA.getInstance().getSocialConfig().getSNSRegion() + "." + str);
            if (i != 6) {
                format = C0030ba.format("%d.%d.%s.db", Integer.valueOf(i), Integer.valueOf(C0042c.getSession().getUserID()), md5);
            } else {
                format = C0030ba.format("%d.%s.db", Integer.valueOf(i), md5);
            }
            openFileDatabase = C0014al.openFileDatabase(format);
            openFileDatabase.setDbId(str);
            openFileDatabase.setScope(i);
            if (i <= 4) {
                openFileDatabase.setSynchronousLevel(0);
            }
        }
        addDatabase(openFileDatabase);
        return openFileDatabase;
    }

    /* access modifiers changed from: protected */
    public final HashMap<String, C0014al> ensureMap(int i) {
        HashMap<String, C0014al> hashMap = this.ed.get(Integer.valueOf(i));
        if (hashMap != null) {
            return hashMap;
        }
        HashMap<String, C0014al> hashMap2 = new HashMap<>();
        this.ed.put(Integer.valueOf(i), hashMap2);
        return hashMap2;
    }

    public final synchronized int featureVisible(String str) {
        int kvInt;
        kvInt = sessionDB().kvInt("k_visible_" + str, -1);
        if (kvInt == -1) {
            C0042c.A.send(109, str);
            kvInt = 0;
        }
        return kvInt;
    }

    public final synchronized C0014al findDatabase(String str, int i) {
        HashMap hashMap;
        hashMap = this.ed.get(Integer.valueOf(i));
        return hashMap != null ? (C0014al) hashMap.get(str) : null;
    }

    public final void initialize(Context context) {
        try {
            String[] databaseList = context.databaseList();
            for (String str : databaseList) {
                if (str.startsWith("4.") && str.endsWith(".db")) {
                    if (!context.deleteDatabase(str)) {
                        X.w("Failed to delete database %s", str);
                    } else {
                        X.d("Deleted database %s", str);
                    }
                }
            }
            C0042c.A.addConnectionDelegate(this);
        } catch (Exception e) {
            X.e(e, "Failed in WebDatabaseManager.initialize", new Object[0]);
        }
    }

    public final void onConnectionEstablished() {
    }

    public final synchronized void onConnectionLost() {
        HashMap hashMap = this.ed.get(3);
        if (hashMap != null) {
            for (C0014al close : hashMap.values()) {
                close.close();
            }
            this.ed.remove(3);
        }
    }

    public final synchronized C0014al openDatabase(String str, int i) {
        C0014al findDatabase;
        findDatabase = findDatabase(str, i);
        if (findDatabase == null) {
            findDatabase = createDatabase(str, i);
        }
        return findDatabase;
    }

    public final synchronized void serverPushedUpdate(Vector vector) {
        Object[] objArr;
        String str = (String) vector.get(1);
        int intValue = C0030ba.intValue(vector.get(2));
        if (intValue >= 3) {
            C0014al openDatabase = openDatabase(str, intValue);
            if (vector.size() >= 5) {
                Vector vector2 = (Vector) vector.get(5);
                objArr = vector2.toArray(new Object[vector2.size()]);
            } else {
                objArr = null;
            }
            openDatabase.update((String) vector.get(3), objArr);
        }
    }

    public final C0014al sessionDB() {
        return openDatabase("__session__", 4);
    }

    public final C0014al settingDB() {
        return openDatabase("__setting__", 6);
    }

    public final synchronized void updateFeatureVisible(Vector vector) {
        int i = 0;
        synchronized (this) {
            Vector vector2 = (Vector) vector.get(1);
            C0014al sessionDB = sessionDB();
            while (true) {
                int i2 = i;
                if (i2 < vector2.size()) {
                    sessionDB.kvSaveInt("k_visible_" + ((String) vector2.get(i2)), C0030ba.intValue(vector2.get(i2 + 1), 0), -1);
                    i = i2 + 2;
                }
            }
        }
    }
}
