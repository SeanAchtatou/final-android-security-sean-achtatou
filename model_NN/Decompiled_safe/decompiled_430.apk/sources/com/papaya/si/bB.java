package com.papaya.si;

import android.app.Activity;
import java.lang.ref.WeakReference;

public class bB<T extends Activity> {
    private WeakReference<T> np;

    public T getOwnerActivity() {
        if (this.np == null) {
            return null;
        }
        return (Activity) this.np.get();
    }

    public void setOwnerActivity(T t) {
        if (t == null) {
            this.np = null;
        } else {
            this.np = new WeakReference<>(t);
        }
    }
}
