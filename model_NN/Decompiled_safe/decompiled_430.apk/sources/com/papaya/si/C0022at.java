package com.papaya.si;

import java.lang.ref.WeakReference;

/* renamed from: com.papaya.si.at  reason: case insensitive filesystem */
public abstract class C0022at {
    private WeakReference<a> eA;
    public long ez;
    protected int state = 0;

    /* renamed from: com.papaya.si.at$a */
    public interface a {
        void onConnectionStateUpdated(int i, int i2);
    }

    public C0022at(a aVar) {
        this.eA = new WeakReference<>(aVar);
    }

    public static C0022at getConnecter(int i, a aVar) {
        return new C0023au(aVar);
    }

    public abstract void close();

    public abstract void connect(String str, int i);

    public abstract byte[] recv();

    public abstract boolean send(Object obj);

    public void setState(int i) {
        int i2 = this.state;
        this.state = i;
        a aVar = this.eA == null ? null : this.eA.get();
        if (aVar != null) {
            aVar.onConnectionStateUpdated(i2, i);
        }
    }

    public abstract int state();
}
