package com.papaya.si;

import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpConnectionMetrics;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.params.BasicHttpParams;

/* renamed from: com.papaya.si.g  reason: case insensitive filesystem */
public final class C0046g {
    private DefaultHttpClientConnection U;
    private C0047h V;
    private int W;
    private boolean X;
    private HttpHost Y;
    private SocketFactory Z;

    public C0046g() {
    }

    public C0046g(HttpHost httpHost) {
        this(httpHost, new PlainSocketFactory());
    }

    public C0046g(HttpHost httpHost, SocketFactory socketFactory) {
        this.U = new DefaultHttpClientConnection();
        this.X = true;
        this.Y = httpHost;
        this.Z = socketFactory;
    }

    public static void register() {
    }

    public final void addRequest(HttpRequest httpRequest) throws HttpException, IOException {
        maybeOpenConnection();
        this.U.sendRequestHeader(httpRequest);
    }

    public final void closeConnection() {
        if (this.U != null && this.U.isOpen()) {
            try {
                this.U.close();
            } catch (IOException e) {
            }
        }
    }

    public final void finishedCurrentRequests() {
        closeConnection();
    }

    public final void installCallbacks$1489076e(C0047h hVar) {
        this.V = hVar;
    }

    public final void maybeOpenConnection() throws IOException {
        if (this.U == null || !this.U.isOpen()) {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            this.U.bind(this.Z.connectSocket(this.Z.createSocket(), this.Y.getHostName(), this.Y.getPort(), null, 0, basicHttpParams), basicHttpParams);
        }
    }

    public final void sendRequests() throws IOException, HttpException {
        this.U.flush();
        HttpConnectionMetrics metrics = this.U.getMetrics();
        while (metrics.getResponseCount() < metrics.getRequestCount()) {
            HttpResponse receiveResponseHeader = this.U.receiveResponseHeader();
            if (!receiveResponseHeader.getStatusLine().getProtocolVersion().greaterEquals(HttpVersion.HTTP_1_1)) {
                this.V.pipelineModeChanged(false);
                this.X = false;
            }
            Header[] headers = receiveResponseHeader.getHeaders("Connection");
            if (headers != null) {
                for (Header value : headers) {
                    if ("close".equalsIgnoreCase(value.getValue())) {
                        this.V.pipelineModeChanged(false);
                        this.X = false;
                    }
                }
            }
            this.W = receiveResponseHeader.getStatusLine().getStatusCode();
            if (this.W != 200) {
                this.V.serverError(this.W);
                closeConnection();
                return;
            }
            this.U.receiveResponseEntity(receiveResponseHeader);
            receiveResponseHeader.getEntity().consumeContent();
            this.V.requestSent();
            if (!this.X) {
                closeConnection();
                return;
            }
        }
    }
}
