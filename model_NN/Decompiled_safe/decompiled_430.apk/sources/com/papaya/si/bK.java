package com.papaya.si;

import android.content.DialogInterface;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class bK implements aR {
    private HashMap<Integer, HashMap<String, Object>> oF = new HashMap<>();

    public final void addView(int i, String str, Object obj) {
        HashMap hashMap = this.oF.get(Integer.valueOf(i));
        if (hashMap == null) {
            hashMap = new HashMap();
            this.oF.put(Integer.valueOf(i), hashMap);
        }
        if (hashMap.containsKey(str)) {
            X.w("UI existed, type %d, uiid %s, ui %s", Integer.valueOf(i), str, obj);
            return;
        }
        hashMap.put(str, obj);
    }

    public final void clear() {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 < 14) {
                HashMap hashMap = this.oF.get(Integer.valueOf(i2));
                if (hashMap != null) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        try {
                            C0036bg.removeFromSuperView(entry.getValue());
                            if (entry.getValue() instanceof aR) {
                                ((aR) entry.getValue()).clear();
                            }
                            if (entry.getValue() instanceof DialogInterface) {
                                ((DialogInterface) entry.getValue()).dismiss();
                            }
                        } catch (Exception e) {
                            X.w(e, "Failed in clear", new Object[0]);
                        }
                    }
                    hashMap.clear();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final Object findView(int i, String str) {
        HashMap hashMap = this.oF.get(Integer.valueOf(i));
        if (hashMap != null) {
            return hashMap.get(str);
        }
        return null;
    }

    public final void hideLoadingViews() {
        HashMap hashMap = this.oF.get(4);
        if (hashMap != null) {
            for (Object removeFromSuperView : hashMap.values()) {
                C0036bg.removeFromSuperView(removeFromSuperView);
            }
        }
    }

    public final void removeAllFromSuperview() {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 < 14) {
                HashMap hashMap = this.oF.get(Integer.valueOf(i2));
                if (hashMap != null) {
                    for (Map.Entry value : hashMap.entrySet()) {
                        C0036bg.removeFromSuperView(value.getValue());
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void removeNonPersistFromSuperview() {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 < 14) {
                HashMap hashMap = this.oF.get(Integer.valueOf(i2));
                if (hashMap != null) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        if (!((String) entry.getKey()).contains("__p")) {
                            C0036bg.removeFromSuperView(entry.getValue());
                        }
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void removeNonPersistentFromSuperviewInSet(Collection<String> collection) {
        int i = 1;
        while (true) {
            int i2 = i;
            if (i2 < 14) {
                HashMap hashMap = this.oF.get(Integer.valueOf(i2));
                if (hashMap != null) {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        String str = (String) entry.getKey();
                        if (collection.contains(str) && !str.contains("__p")) {
                            C0036bg.removeFromSuperView(entry.getValue());
                        }
                    }
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void removeView(int i, String str) {
        HashMap hashMap = this.oF.get(Integer.valueOf(i));
        if (hashMap != null) {
            hashMap.remove(str);
        }
    }

    public final void updateViewsVisibility(int i) {
        for (HashMap<String, Object> values : this.oF.values()) {
            for (Object visibility : values.values()) {
                C0036bg.setVisibility(visibility, i);
            }
        }
    }
}
