package com.papaya.si;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.papaya.si.by;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

public final class bt implements C0031bb, by.a {
    private WeakReference<a> eA;
    private ArrayList<bA> kR = new ArrayList<>(4);
    private Context lH;
    private ArrayList<URL> mR = new ArrayList<>(4);
    private ArrayList<String> mS = new ArrayList<>(4);

    public interface a {
        void onPhotoPicasa(URL url, URL url2, String str, boolean z);
    }

    public bt(Context context) {
        this.lH = context;
    }

    public final synchronized void connectionFailed(by byVar, int i) {
        bA request = byVar.getRequest();
        int indexOf = this.kR.indexOf(request);
        if (indexOf > 0) {
            a delegate = getDelegate();
            if (delegate != null) {
                delegate.onPhotoPicasa(request.getUrl(), this.mR.get(indexOf), this.mS.get(indexOf), false);
            }
            this.kR.remove(indexOf);
            this.mR.remove(indexOf);
            this.mS.remove(indexOf);
        }
    }

    public final synchronized void connectionFinished(by byVar) {
        byte[] data = byVar.getData();
        uploadtoPicasa(Uri.parse(MediaStore.Images.Media.insertImage(this.lH.getContentResolver(), BitmapFactory.decodeByteArray(data, 0, data.length), "Title", "papaya")));
        synchronized (this) {
            bA request = byVar.getRequest();
            int indexOf = this.kR.indexOf(request);
            if (indexOf > 0) {
                a delegate = getDelegate();
                if (delegate != null) {
                    delegate.onPhotoPicasa(request.getUrl(), this.mR.get(indexOf), this.mS.get(indexOf), true);
                }
                this.kR.remove(indexOf);
                this.mR.remove(indexOf);
                this.mS.remove(indexOf);
            }
        }
    }

    public final a getDelegate() {
        if (this.eA == null) {
            return null;
        }
        return this.eA.get();
    }

    public final void setDelegate(a aVar) {
        if (aVar == null) {
            this.eA = null;
        } else {
            this.eA = new WeakReference<>(aVar);
        }
    }

    public final int uploadToPicasa(String str, URL url, String str2) {
        if (str == null || str.length() == 0) {
            return -1;
        }
        if (!C0035bf.supportPicasa()) {
            C0036bg.showToast("Google Picasa is not supported on your device.", 0);
            return -1;
        }
        bA bAVar = new bA();
        bAVar.setDelegate(this);
        aD webCache = C0002a.getWebCache();
        aP fdFromPapayaUri = webCache.fdFromPapayaUri(str, url, bAVar);
        if (fdFromPapayaUri != null) {
            uploadtoPicasa(Uri.parse(MediaStore.Images.Media.insertImage(this.lH.getContentResolver(), C0036bg.bitmapFromFD(fdFromPapayaUri), "Title", "papaya")));
            return 1;
        } else if (bAVar.getUrl() == null) {
            return -1;
        } else {
            synchronized (this) {
                this.kR.add(bAVar);
                this.mR.add(url);
                this.mS.add(str2);
            }
            webCache.insertRequest(bAVar);
            return 0;
        }
    }

    public final void uploadtoPicasa(Uri uri) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("image/jpeg");
        intent.addFlags(1);
        intent.putExtra("android.intent.extra.STREAM", uri);
        intent.setComponent(new ComponentName("com.google.android.apps.uploader", "com.google.android.apps.uploader.picasa.PicasaSettingsActivity"));
        C0029b.startActivity(intent);
    }
}
