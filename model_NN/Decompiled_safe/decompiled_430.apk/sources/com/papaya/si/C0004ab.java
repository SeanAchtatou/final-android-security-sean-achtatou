package com.papaya.si;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/* renamed from: com.papaya.si.ab  reason: case insensitive filesystem */
public final class C0004ab {
    public ImageView dG;
    public TextView dH;
    public ImageView dI;
    public LinearLayout dJ;

    public C0004ab(View view) {
        this.dG = (ImageView) C0036bg.find(view, "icon");
        this.dH = (TextView) C0036bg.find(view, "label");
        this.dI = (ImageView) C0036bg.find(view, "group_indicator");
        this.dJ = (LinearLayout) C0036bg.find(view, "actions_content");
    }
}
