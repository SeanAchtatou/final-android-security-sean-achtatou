package com.papaya.si;

import com.papaya.social.PPYLocalScore;
import java.util.ArrayList;
import java.util.List;

public final class aB extends C0014al {
    protected aB(String str) {
        super(str);
    }

    public final void addScore(int i) {
        update("insert into score (score, utime) values (?, ?)", Integer.valueOf(i), Long.valueOf(System.currentTimeMillis() / 1000));
    }

    /* access modifiers changed from: protected */
    public final void createInitialTables() {
        super.createInitialTables();
        createTable("create table if not exists score (score integer, utime integer)");
    }

    public final List<PPYLocalScore> listScores() {
        return listScores(-1);
    }

    public final List<PPYLocalScore> listScores(int i) {
        Object[][] newQueryResult = newQueryResult(i <= 0 ? "select score, utime from score order by score desc" : "select score, utime from score order by score desc limit " + i, null);
        ArrayList arrayList = new ArrayList(newQueryResult.length);
        for (int i2 = 0; i2 < newQueryResult.length; i2++) {
            arrayList.add(new PPYLocalScore(C0030ba.intValue(newQueryResult[i2][0]), Long.parseLong((String) newQueryResult[i2][1])));
        }
        return arrayList;
    }
}
