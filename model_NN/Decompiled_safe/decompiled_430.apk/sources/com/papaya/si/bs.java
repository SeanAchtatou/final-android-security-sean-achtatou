package com.papaya.si;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import com.papaya.si.by;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

public final class bs implements by.a {
    private WeakReference<a> hY;
    /* access modifiers changed from: private */
    public ArrayList<bA> kR = new ArrayList<>(4);
    /* access modifiers changed from: private */
    public Context lH;
    /* access modifiers changed from: private */
    public ArrayList<URL> mR = new ArrayList<>(4);
    /* access modifiers changed from: private */
    public ArrayList<String> mS = new ArrayList<>(4);

    public interface a {
        void onPhotoSaved(URL url, URL url2, String str, boolean z);
    }

    public bs(Context context) {
        this.lH = context;
    }

    public final void connectionFailed(final by byVar, int i) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                synchronized (this) {
                    bA request = byVar.getRequest();
                    int indexOf = bs.this.kR.indexOf(request);
                    if (indexOf >= 0) {
                        a delegate = bs.this.getDelegate();
                        if (delegate != null) {
                            delegate.onPhotoSaved(request.getUrl(), (URL) bs.this.mR.get(indexOf), (String) bs.this.mS.get(indexOf), false);
                        }
                        bs.this.kR.remove(indexOf);
                        bs.this.mR.remove(indexOf);
                        bs.this.mS.remove(indexOf);
                    }
                }
            }
        });
    }

    public final void connectionFinished(final by byVar) {
        C0036bg.runInHandlerThread(new Runnable() {
            public final void run() {
                byte[] data = byVar.getData();
                MediaStore.Images.Media.insertImage(bs.this.lH.getContentResolver(), BitmapFactory.decodeByteArray(data, 0, data.length), "Title", "papaya");
                synchronized (this) {
                    bA request = byVar.getRequest();
                    int indexOf = bs.this.kR.indexOf(request);
                    if (indexOf >= 0) {
                        a delegate = bs.this.getDelegate();
                        if (delegate != null) {
                            delegate.onPhotoSaved(request.getUrl(), (URL) bs.this.mR.get(indexOf), (String) bs.this.mS.get(indexOf), true);
                        }
                        bs.this.kR.remove(indexOf);
                        bs.this.mR.remove(indexOf);
                        bs.this.mS.remove(indexOf);
                    }
                }
            }
        });
    }

    public final a getDelegate() {
        if (this.hY == null) {
            return null;
        }
        return this.hY.get();
    }

    public final int saveToPictures(String str, URL url, String str2) {
        if (str == null || str.length() == 0) {
            return -1;
        }
        bA bAVar = new bA();
        bAVar.setDelegate(this);
        aD webCache = C0002a.getWebCache();
        aP fdFromPapayaUri = webCache.fdFromPapayaUri(str, url, bAVar);
        if (fdFromPapayaUri != null) {
            MediaStore.Images.Media.insertImage(this.lH.getContentResolver(), C0036bg.bitmapFromFD(fdFromPapayaUri), "Title", "papaya");
            return 1;
        } else if (bAVar.getUrl() == null) {
            return -1;
        } else {
            synchronized (this) {
                this.kR.add(bAVar);
                this.mR.add(url);
                this.mS.add(str2);
            }
            webCache.insertRequest(bAVar);
            return 0;
        }
    }

    public final void setDelegate(a aVar) {
        if (aVar == null) {
            this.hY = null;
        } else {
            this.hY = new WeakReference<>(aVar);
        }
    }
}
