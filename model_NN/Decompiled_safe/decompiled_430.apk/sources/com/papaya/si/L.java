package com.papaya.si;

import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import java.lang.ref.WeakReference;

public final class L extends D {
    public int cQ;
    public boolean cR;
    public int cS;
    private SparseArray<WeakReference<N>> cT;
    public String description;
    public String name;

    public L() {
        this.state = 1;
    }

    public final Drawable getDefaultDrawable() {
        return C0042c.getDrawable("chatgroup_default_1");
    }

    public final CharSequence getSubtitle() {
        return this.cR ? C0042c.getString("administrator") : this.description;
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.name;
    }

    public final N getUserCard(int i, String str) {
        N n;
        if (this.cT == null) {
            this.cT = new SparseArray<>();
        }
        WeakReference weakReference = this.cT.get(i);
        if (weakReference != null) {
            n = (N) weakReference.get();
            if (n == null) {
                this.cT.remove(i);
            }
        } else {
            n = null;
        }
        if (n == null) {
            n = new N();
            n.cU = i;
            n.name = str;
            this.cT.put(i, new WeakReference(n));
        }
        n.name = str;
        return n;
    }

    public final boolean isGrayScaled() {
        return false;
    }
}
