package com.papaya.si;

import com.papaya.web.WebViewController;
import java.net.URL;
import org.json.JSONObject;

public final class bG {
    private bp kK;
    private String nB;
    private JSONObject nC;
    private URL url;

    public bG(URL url2, String str) {
        this.url = url2;
        this.nB = str;
    }

    public final void assignWebView(bp bpVar) {
        if (this.kK != null) {
            X.e("duplicated assign?", new Object[0]);
        }
        this.kK = bpVar;
        bpVar.setHistory(this);
    }

    public final void freeWebView() {
        if (this.kK != null) {
            this.kK.noWarnCallJS("webdisappeared", "webdisappeared()");
            this.kK.setHistory(null);
            bL.getInstance().freeWebView(this.kK);
            this.kK = null;
        }
    }

    public final String getTitle() {
        return this.nB;
    }

    public final JSONObject getTitleCtx() {
        return this.nC;
    }

    public final URL getURL() {
        return this.url;
    }

    public final bp getWebView() {
        return this.kK;
    }

    public final void hideWebView() {
        if (this.kK != null) {
            this.kK.setVisibility(4);
            this.kK.noWarnCallJS("webdisappeared", "webdisappeared()");
        }
    }

    public final boolean openWebView(WebViewController webViewController, URL url2, boolean z) {
        if (webViewController == null) {
            X.e("nil controller ?!", new Object[0]);
            return false;
        } else if (url2 == null && this.url == null) {
            X.e("both uris are null!!!", new Object[0]);
            return false;
        } else {
            if (this.kK == null) {
                aY aYVar = new aY(Boolean.FALSE);
                assignWebView(bL.getInstance().getWebView(webViewController, z ? url2 : null, aYVar));
                if (((Boolean) aYVar.ho).booleanValue()) {
                    this.kK.setVisibility(0);
                    this.kK.noWarnCallJS("webappeared", "webappeared(false)");
                    return true;
                }
                this.kK.setVisibility(4);
                this.kK.noWarnCallJS("webdisappeared", "webdisappeared()");
                this.kK.loadPapayaURL(url2 == null ? this.url : url2);
            } else {
                if (this.kK.getController() != webViewController) {
                    X.w("Inconsistent controller of webview !!!", new Object[0]);
                    webViewController.configWebView(this.kK);
                }
                if (url2 != null || !z) {
                    this.kK.setVisibility(4);
                    this.kK.noWarnCallJS("webdisappeared", "webdisappeared()");
                    this.kK.loadPapayaURL(url2 == null ? this.url : url2);
                } else if (!this.kK.isReusable() || this.kK.isLoadFromString()) {
                    this.kK.setVisibility(4);
                    this.kK.noWarnCallJS("webdisappeared", "webdisappeared()");
                    this.kK.loadPapayaURL(this.url);
                } else {
                    this.kK.setVisibility(0);
                    this.kK.noWarnCallJS("webappeared", "webappeared(false)");
                    return true;
                }
            }
            return false;
        }
    }

    public final void setTitle(String str) {
        this.nB = str;
    }

    public final void setTitleCtx(JSONObject jSONObject) {
        this.nC = jSONObject;
        this.nB = C0030ba.nonNullString(C0040bk.getJsonString(this.nC, "title"), this.nB);
    }

    public final void setURL(URL url2) {
        this.url = url2;
    }
}
