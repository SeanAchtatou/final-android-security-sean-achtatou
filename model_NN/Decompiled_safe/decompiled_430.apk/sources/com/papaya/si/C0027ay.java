package com.papaya.si;

import com.papaya.social.PPYSocialChallengeRecord;
import com.papaya.social.PPYSocialChallengeService;
import java.util.ArrayList;

/* renamed from: com.papaya.si.ay  reason: case insensitive filesystem */
public final class C0027ay {
    private static C0027ay fI = new C0027ay();
    private ArrayList<PPYSocialChallengeService.Delegate> fJ = new ArrayList<>();

    private C0027ay() {
    }

    public static C0027ay instance() {
        return fI;
    }

    public final synchronized void addDelegate(PPYSocialChallengeService.Delegate delegate) {
        if (delegate != null) {
            if (!this.fJ.contains(delegate)) {
                this.fJ.add(delegate);
            }
        }
    }

    public final synchronized void fireChallengeAccepted(PPYSocialChallengeRecord pPYSocialChallengeRecord) {
        int i = 0;
        synchronized (this) {
            while (true) {
                int i2 = i;
                if (i2 < this.fJ.size()) {
                    PPYSocialChallengeService.Delegate delegate = this.fJ.get(i2);
                    try {
                        delegate.onChallengeAccepted(pPYSocialChallengeRecord);
                    } catch (Exception e) {
                        X.w(e, "Failed to invoke onChallengeAccepted of " + delegate, new Object[0]);
                    }
                    i = i2 + 1;
                }
            }
        }
    }

    public final synchronized void removedelegate(PPYSocialChallengeService.Delegate delegate) {
        this.fJ.remove(delegate);
    }
}
