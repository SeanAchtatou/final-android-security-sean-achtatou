package com.papaya.si;

import android.content.Context;
import android.text.Html;
import com.papaya.view.OverlayCustomDialog;

public final class aH extends OverlayCustomDialog implements OverlayCustomDialog.OnClickListener {
    private CharSequence cZ;
    private boolean gG = false;

    public aH(Context context, CharSequence charSequence) {
        super(context);
        CharSequence charSequence2;
        if (C0030ba.isEmpty(charSequence)) {
            charSequence2 = Html.fromHtml(C0030ba.format(C0042c.getString("dlg_recommend_message"), C0061v.bk));
            this.gG = true;
        } else {
            charSequence2 = charSequence;
        }
        this.cZ = charSequence2;
        setMessage(charSequence2);
        setTitle(C0030ba.format(C0042c.getString("dlg_recommend_title"), C0061v.bk));
        setPositiveButton(C0042c.getString("button_recommend"), this);
        setNegativeButton(C0042c.getString("button_cancel"), this);
    }

    /* access modifiers changed from: protected */
    public final int getDefaultVisibility() {
        return 0;
    }

    public final void onClick(OverlayCustomDialog overlayCustomDialog, int i) {
        if (i == -1) {
            aA.getInstance().postNewsfeed(C0030ba.nullAsEmpty(this.cZ), null, this.gG ? 1 : 2);
        }
    }
}
