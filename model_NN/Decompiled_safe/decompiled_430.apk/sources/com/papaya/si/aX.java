package com.papaya.si;

import java.util.Formatter;

public final class aX {
    private Formatter hm = new Formatter(this.hn);
    private StringBuilder hn = new StringBuilder(256);

    aX() {
    }

    public final String format(String str, Object... objArr) {
        try {
            this.hn.setLength(0);
            this.hm.format(str, objArr);
            return this.hn.toString();
        } catch (Exception e) {
            X.e(e, "Failed to format " + str, new Object[0]);
            return str;
        }
    }
}
