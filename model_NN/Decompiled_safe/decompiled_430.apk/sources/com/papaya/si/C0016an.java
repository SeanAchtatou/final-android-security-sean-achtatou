package com.papaya.si;

import android.database.Cursor;

/* renamed from: com.papaya.si.an  reason: case insensitive filesystem */
public final class C0016an extends C0014al {
    public C0016an(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public final void createInitialTables() {
        super.createInitialTables();
        createTable("create table if not exists page (name TEXT, content TEXT, type INTEGER, version INTEGER, PRIMARY KEY(name))");
    }

    /* access modifiers changed from: protected */
    public final void createPageTable() {
        createTable("create table if not exists page (name TEXT, content TEXT, type INTEGER, version INTEGER, PRIMARY KEY(name))");
    }

    public final int getVersion() {
        return kvInt("k_version", 0);
    }

    public final synchronized String newPageContent(String str, boolean z) {
        String str2;
        Cursor rawQuery;
        str2 = null;
        if (this.ea != null) {
            if (z) {
                rawQuery = this.ea.rawQuery("SELECT content FROM page WHERE name=? and version > -1", new String[]{str});
            } else {
                rawQuery = this.ea.rawQuery("SELECT content FROM page WHERE name=?", new String[]{str});
            }
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToNext()) {
                        str2 = rawQuery.getString(0);
                    }
                    rawQuery.close();
                } catch (Exception e) {
                    X.w(e, "Failed to newPageContent %s", str);
                    rawQuery.close();
                } catch (Throwable th) {
                    rawQuery.close();
                    throw th;
                }
            }
        }
        return str2;
    }

    public final void savePage(String str, String str2) {
        if (str != null && str2 != null) {
            update("REPLACE INTO page (name, content, version) VALUES (?, ?, ?)", str, str2, Integer.valueOf(getVersion()));
        }
    }

    public final void setVersion(int i) {
        kvSaveInt("k_version", i, -1);
    }

    public final void updatePage(String str, int i) {
        update("REPLACE INTO page (name, type, version) VALUES(?, ?, -1)", str, Integer.valueOf(i));
    }
}
