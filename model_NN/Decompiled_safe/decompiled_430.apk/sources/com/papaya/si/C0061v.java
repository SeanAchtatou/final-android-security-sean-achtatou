package com.papaya.si;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.papaya.social.BillingChannel;
import com.papaya.social.PPYSNSRegion;
import com.papaya.social.PPYSocial;
import java.net.URL;

/* renamed from: com.papaya.si.v  reason: case insensitive filesystem */
public final class C0061v {
    public static String DEFAULT_HOST;
    public static int bc = 144;
    public static String bd;
    public static String be = "social";
    public static int bf = 0;
    public static String bg = null;
    public static String bh;
    public static String bi;
    public static URL bj;
    public static CharSequence bk;
    public static String bl = "Apps";
    public static String bm;
    public static String bn = "static_moreapps";
    public static String bo = "android";

    private C0061v() {
    }

    public static void changeHost(PPYSNSRegion pPYSNSRegion) {
        if (pPYSNSRegion == PPYSNSRegion.CHINA) {
            C0028az.fK = "com.papaya.socialsdk.new.lite.cn";
            C0028az.fL = "__ppy_secret_cn";
            DEFAULT_HOST = "cn.papayamobile.com";
            bh = "http://cn.papayamobile.com/";
            bi = bh + "social/";
        } else {
            C0028az.fK = "com.papaya.socialsdk.new.lite";
            C0028az.fL = "__ppy_secret";
            DEFAULT_HOST = "connect.papayamobile.com";
            bh = "http://connect.papayamobile.com:8080/";
            bi = bh + "social/";
        }
        try {
            bj = new URL(bi);
        } catch (Exception e) {
            X.e("invalid url string for default_web_url: %s", bi);
        }
        C0028az.fM = pPYSNSRegion.region + "";
    }

    public static void setup(Context context) {
        C0065z.setup(context);
        bm = context.getPackageName();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(bm, 0);
            bf = packageInfo.versionCode;
            bg = packageInfo.versionName;
            bk = context.getPackageManager().getApplicationLabel(context.getPackageManager().getApplicationInfo(bm, BillingChannel.MISC));
            String preferredLanguage = aA.getInstance().getSocialConfig().getPreferredLanguage();
            if (PPYSocial.LANG_AUTO.equals(preferredLanguage)) {
                bd = context.getResources().getString(C0065z.stringID("lang"));
            } else {
                bd = preferredLanguage;
            }
            X.i("lang: setting %s, selected %s", preferredLanguage, bd);
        } catch (Exception e) {
            X.e(e, "Failed to getApplicationInfo", new Object[0]);
        }
        changeHost(aA.getInstance().getSocialConfig().getSNSRegion());
        bl = context.getString(C0065z.stringID("tab_apps"));
    }
}
