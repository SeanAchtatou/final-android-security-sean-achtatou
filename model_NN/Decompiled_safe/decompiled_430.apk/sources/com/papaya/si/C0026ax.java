package com.papaya.si;

/* renamed from: com.papaya.si.ax  reason: case insensitive filesystem */
public final class C0026ax extends C0014al {
    protected C0026ax(String str) {
        super(str);
    }

    public final boolean addAchievement(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean update = update("insert into unlocked_achievement (aid) values (?)", Integer.valueOf(i));
        X.d("add achievement time: %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
        return update;
    }

    public final boolean clearAchievements() {
        long currentTimeMillis = System.currentTimeMillis();
        boolean update = update("delete from unlocked_achievement", new Object[0]);
        X.d("clear achievements time: %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
        return update;
    }

    /* access modifiers changed from: protected */
    public final void createInitialTables() {
        super.createInitialTables();
        createTable("create table if not exists unlocked_achievement (aid integer)");
    }

    public final boolean deleteAchievement(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean update = update("delete from unlocked_achievement where aid=(?)", Integer.valueOf(i));
        X.d("deleteachivement time: %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
        return update;
    }

    public final String stringList() {
        Object[][] newQueryResult = newQueryResult("select aid from unlocked_achievement order by aid", null);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < newQueryResult.length; i++) {
            sb.append(newQueryResult[i][0]);
            if (i != newQueryResult.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
