package com.papaya.si;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import com.papaya.si.by;
import com.papaya.social.PPYEntranceView;
import java.lang.ref.WeakReference;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class by {
    private static aN nc = new aN(10);
    private long gz;
    private WeakReference<a> hY;
    protected bA jT;
    private URL mZ;
    private byte[] na;
    protected long nb;

    public interface a {
        void connectionFailed(by byVar, int i);

        void connectionFinished(by byVar);
    }

    public static abstract class b implements C0031bb, a {
        public final /* synthetic */ PPYEntranceView fj;

        public b() {
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public b(PPYEntranceView pPYEntranceView) {
            this();
            this.fj = pPYEntranceView;
        }

        public final void connectionFailed(by byVar, int i) {
            X.w("failed to get dada", new Object[0]);
            C0036bg.postDelayed(new Runnable() {
                public final void run() {
                    if (by.b.this.fj.eV == 0) {
                        by.b.this.fj.fe.start(true);
                        PPYEntranceView.access$108(by.b.this.fj);
                    }
                }
            }, 15000);
            int unused = this.fj.eV = 0;
        }

        public final void connectionFinished(by byVar) {
            JSONArray jSONArray;
            try {
                jSONArray = new JSONArray(C0030ba.utf8String(byVar.getData(), null));
            } catch (Exception e) {
                jSONArray = null;
            }
            if (!(jSONArray == null || jSONArray.length() == 0)) {
                JSONObject[] unused = this.fj.fc = new JSONObject[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.fj.fc[i] = C0040bk.getJsonObject(jSONArray, i);
                }
            }
            int unused2 = this.fj.eV = 0;
            this.fj.runHandle();
        }
    }

    public by(bA bAVar) {
        this(bAVar, null);
    }

    public by(bA bAVar, a aVar) {
        this.jT = bAVar;
        setDelegate(aVar);
        this.gz = System.currentTimeMillis();
    }

    public static void delegateConnectionFailed(final by byVar, final a aVar, final int i) {
        if (aVar == null) {
            return;
        }
        if (C0036bg.isMainThread() || !(aVar instanceof C0031bb)) {
            aVar.connectionFailed(byVar, i);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    aVar.connectionFailed(byVar, i);
                }
            });
        }
    }

    public static void delegateConnectionFinished(final by byVar, final a aVar) {
        if (aVar == null) {
            return;
        }
        if (C0036bg.isMainThread() || !(aVar instanceof C0031bb)) {
            aVar.connectionFinished(byVar);
        } else {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    aVar.connectionFinished(byVar);
                }
            });
        }
    }

    public static Bitmap getCachedBitmap(String str) {
        return nc.get(str);
    }

    public static Bitmap getCachedBitmap(URL url) {
        return nc.get(url.toString());
    }

    public void cancel() {
    }

    public void fireConnectionFailed(int i) {
        delegateConnectionFailed(this, getDelegate(), i);
    }

    public void fireConnectionFailedToRequest(int i) {
        if (this.jT != null) {
            delegateConnectionFailed(this, this.jT.getDelegate(), i);
        }
    }

    public void fireConnectionFinished() {
        delegateConnectionFinished(this, getDelegate());
    }

    public void fireConnectionFinishedToRequest() {
        if (this.jT != null) {
            delegateConnectionFinished(this, this.jT.getDelegate());
        } else {
            X.i("request is null??", new Object[0]);
        }
    }

    public Bitmap getBitmap() {
        byte[] data;
        try {
            String url = this.jT.getUrl().toString();
            Bitmap bitmap = nc.get(url);
            if (bitmap == null && (data = getData()) != null && data.length > 0 && (bitmap = BitmapFactory.decodeByteArray(data, 0, data.length)) != null) {
                nc.put(url, bitmap);
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public byte[] getData() {
        if (this.na == null && this.jT.getSaveFile() != null) {
            this.na = aZ.dataFromFile(this.jT.getSaveFile());
        }
        return this.na;
    }

    public long getDataLength() {
        return this.nb;
    }

    public a getDelegate() {
        if (this.hY != null) {
            return this.hY.get();
        }
        return null;
    }

    public URL getRedirectUrl() {
        return this.mZ;
    }

    public bA getRequest() {
        return this.jT;
    }

    public long getStartTime() {
        return this.gz;
    }

    public void setData(byte[] bArr) {
        JSONObject parseJsonObject;
        this.na = bArr;
        setDataLength(this.na == null ? 0 : (long) this.na.length);
        if (this.jT.isDispatchable() && this.na != null) {
            String utf8String = C0030ba.utf8String(this.na, "");
            if (utf8String.contains("__COMPOSITE__") && (parseJsonObject = C0040bk.parseJsonObject(utf8String)) != null) {
                String baVar = C0030ba.toString(C0040bk.getJsonObject(parseJsonObject, "__ORIGINAL__"), "");
                if (C0030ba.intValue(Build.VERSION.SDK, 0) >= 8) {
                    baVar = baVar.replaceAll("\\\\/", "/");
                }
                this.na = C0030ba.getBytes(baVar);
                setDataLength((long) this.na.length);
                final int jsonInt = C0040bk.getJsonInt(parseJsonObject, "__COMPOSITE__");
                final JSONArray jsonArray = C0040bk.getJsonArray(parseJsonObject, "__PAYLOAD__");
                C0036bg.runInHandlerThread(new Runnable() {
                    public final void run() {
                        if (jsonInt == 1) {
                            aA.getInstance().handleResponse(jsonArray);
                            return;
                        }
                        for (int i = 0; i < jsonArray.length(); i++) {
                            aA.getInstance().handleResponse(jsonArray.optJSONArray(i));
                        }
                    }
                });
            }
        }
        if (this.na != null && this.jT.getSaveFile() != null) {
            aZ.writeBytesToFile(this.jT.getSaveFile(), this.na);
        }
    }

    public void setDataLength(long j) {
        this.nb = j;
    }

    public void setDelegate(a aVar) {
        if (aVar != null) {
            this.hY = new WeakReference<>(aVar);
        } else {
            this.hY = null;
        }
    }

    public void setRedirectUrl(URL url) {
        this.mZ = url;
    }

    public void setRequest(bA bAVar) {
        this.jT = bAVar;
    }

    public void setStartTime(long j) {
        this.gz = j;
    }

    public String toString() {
        return "UrlConnection{_request=" + this.jT + ", _redirectUri=" + this.mZ + ", _startTime=" + this.gz + '}';
    }
}
