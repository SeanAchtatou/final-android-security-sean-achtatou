package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import com.papaya.chat.ChatActivity;

public class G implements aS<T> {
    private /* synthetic */ ChatActivity cM;

    private G() {
    }

    public G(ChatActivity chatActivity) {
        this.cM = chatActivity;
    }

    public static Button getPaymentButton(Activity activity, int i, int i2) {
        return null;
    }

    public static void hideButton(View view) {
    }

    public static void initialize(Context context) {
    }

    public static void onActivityFinished(int i, Intent intent) {
    }

    private boolean onDataStateChanged(T t) {
        if (t.size() == 0) {
            this.cM.finish();
        } else {
            if (this.cM.cG == null) {
                D unused = this.cM.cG = t.get(0);
            }
            if (!t.contains(this.cM.cG)) {
                this.cM.closeActiveChat();
            } else {
                this.cM.refreshChattingBar(t);
            }
        }
        return false;
    }

    public static void showCheckoutButton(bp bpVar, int i, int i2, int i3, String str) {
    }

    public static void showPayment(Activity activity, float f, String str) {
    }

    public final /* bridge */ /* synthetic */ boolean onDataStateChanged(aU aUVar) {
        T t = (T) aUVar;
        if (t.size() == 0) {
            this.cM.finish();
        } else {
            if (this.cM.cG == null) {
                D unused = this.cM.cG = t.get(0);
            }
            if (!t.contains(this.cM.cG)) {
                this.cM.closeActiveChat();
            } else {
                this.cM.refreshChattingBar(t);
            }
        }
        return false;
    }
}
