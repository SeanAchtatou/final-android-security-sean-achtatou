package com.papaya.si;

/* renamed from: com.papaya.si.p  reason: case insensitive filesystem */
public final class C0055p {
    final int N;
    public final long an;
    public final int ao;
    public final String ap;
    final int aq;
    final int ar;
    final int as;
    final int at;
    public final String au;
    public final String av;
    public final int aw;
    public final int ax;
    public final String label;
    public final int value;

    C0055p(int i, String str, String str2, String str3, String str4, int i2, int i3, int i4) {
        this(-1, i, str, -1, -1, -1, -1, -1, str2, str3, str4, i2, i3, i4);
    }

    public C0055p(long j, int i, String str, int i2, int i3, int i4, int i5, int i6, String str2, String str3, String str4, int i7, int i8, int i9) {
        this.an = j;
        this.ao = i;
        this.ap = str;
        this.aq = i2;
        this.ar = i3;
        this.as = i4;
        this.at = i5;
        this.N = i6;
        this.au = str2;
        this.av = str3;
        this.label = str4;
        this.value = i7;
        this.ax = i9;
        this.aw = i8;
    }

    public final String toString() {
        return "id:" + this.an + " " + "random:" + this.aq + " " + "timestampCurrent:" + this.at + " " + "timestampPrevious:" + this.as + " " + "timestampFirst:" + this.ar + " " + "visits:" + this.N + " " + "value:" + this.value + " " + "category:" + this.au + " " + "action:" + this.av + " " + "label:" + this.label + " " + "width:" + this.aw + " " + "height:" + this.ax;
    }
}
