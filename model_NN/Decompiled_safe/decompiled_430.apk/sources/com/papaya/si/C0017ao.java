package com.papaya.si;

import android.content.Context;
import com.papaya.si.by;
import com.papaya.view.OverlayMessage;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.papaya.si.ao  reason: case insensitive filesystem */
public final class C0017ao implements aR, C0031bb, by.a {
    private static HashSet<String> ee;
    private static C0017ao ef = new C0017ao();
    /* access modifiers changed from: private */
    public C0016an eg;
    private int eh;
    private HashMap<String, String> ei = new HashMap<>();
    private long ej;

    static {
        HashSet<String> hashSet = new HashSet<>();
        ee = hashSet;
        hashSet.add("static_gameengine");
        ee.add("static_gameengine.zh_CN");
        ee.add("static_localleaderboard");
        ee.add("static_localleaderboard.zh_CN");
        ee.add("static_welcome");
        ee.add("static_welcome.zh_CN");
    }

    private C0017ao() {
    }

    public static synchronized C0017ao getInstance() {
        C0017ao aoVar;
        synchronized (C0017ao.class) {
            aoVar = ef;
        }
        return aoVar;
    }

    public static boolean isSessionContent(String str) {
        return str != null && str.startsWith("__");
    }

    public static boolean isSessionLess(String str) {
        return ee.contains(str);
    }

    public final void checkUpdateRequest(Vector vector) {
        if (this.eh != 1 && vector.size() > 1 && this.eg != null && this.eg.getVersion() != C0030ba.intValue(vector.get(1))) {
            sendUpdateRequest();
        }
    }

    public final void clear() {
        C0029b.removeOverlayDialog(10);
        this.eh = 0;
        if (this.eg != null) {
            this.eg.close();
            this.eg = null;
        }
        this.ei.clear();
    }

    public final synchronized void connectionFailed(by byVar, int i) {
        C0029b.removeOverlayDialog(10);
        setUpdateStatus(2);
    }

    public final synchronized void connectionFinished(by byVar) {
        JSONArray optJSONArray;
        C0029b.removeOverlayDialog(10);
        X.i("got update response. time: %dms", Long.valueOf(System.currentTimeMillis() - this.ej));
        if (!(byVar == null || this.eg == null)) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                JSONObject parseJsonObject = C0040bk.parseJsonObject(C0030ba.utf8String(byVar.getData(), null));
                int optInt = parseJsonObject.optInt("version", -1);
                int version = this.eg.getVersion();
                if (!(optInt == -1 || version == optInt || (optJSONArray = parseJsonObject.optJSONArray("updates")) == null)) {
                    this.eg.update("BEGIN TRANSACTION", new Object[0]);
                    for (int i = 0; i < optJSONArray.length(); i += 2) {
                        String optString = optJSONArray.optString(i);
                        int optInt2 = optJSONArray.optInt(i + 1);
                        if (optString != null) {
                            this.eg.updatePage(optString, optInt2);
                        }
                    }
                    this.eg.setVersion(optInt);
                    this.eg.update("END TRANSACTION", new Object[0]);
                    X.i("updated db from %d to %d, %d updates", Integer.valueOf(version), Integer.valueOf(optInt), Integer.valueOf(optJSONArray.length()));
                }
            } catch (Exception e) {
                X.e(e, "Failed to update pages", new Object[0]);
            }
            X.i("update total time: %d", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
        }
        setUpdateStatus(2);
        return;
    }

    public final C0016an getPageDB() {
        return this.eg;
    }

    public final int getUpdateStatus() {
        return this.eh;
    }

    public final int getVersion() {
        if (this.eg != null) {
            return this.eg.getVersion();
        }
        return -1;
    }

    public final void initialize(Context context) {
        final String str = C0028az.fK + "." + 173 + ".webpage.db";
        final File databasePath = context.getDatabasePath(str);
        this.eh = 0;
        if (!databasePath.exists()) {
            new Thread() {
                public final void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    aP aPVar = new aP(str + ".bin");
                    File file = new File(databasePath.getAbsolutePath() + ".tmp");
                    if (!aZ.decompressGZipToFile(aPVar.openInput(), file)) {
                        X.e("Failed to decompress page db", new Object[0]);
                    } else if (!file.renameTo(databasePath)) {
                        X.e("Failed to rename %s to %s", file, databasePath);
                    } else {
                        C0016an unused = C0017ao.this.eg = new C0016an(str);
                        C0036bg.runInHandlerThread(new Runnable() {
                            public final void run() {
                                C0017ao.this.sendUpdateRequest();
                            }
                        });
                    }
                    X.i("decompress db time: %dms", Long.valueOf(System.currentTimeMillis() - currentTimeMillis));
                }
            }.start();
            return;
        }
        this.eg = new C0016an(str);
        X.i("Local page db: %d pages, version %d", Integer.valueOf(this.eg.countForTable("page")), Integer.valueOf(this.eg.getVersion()));
    }

    public final boolean isReady() {
        return this.eg == null || this.eh == 2;
    }

    public final synchronized String newPageContent(String str, boolean z) {
        return isSessionContent(str) ? this.ei.get(str) : this.eg != null ? this.eg.newPageContent(str, z) : null;
    }

    public final synchronized void savePage(String str, String str2) {
        if (isSessionContent(str)) {
            saveSessionPage(str, str2);
        } else if (this.eg != null) {
            this.eg.savePage(str, str2);
        }
    }

    public final synchronized void saveSessionPage(String str, String str2) {
        if (!C0030ba.isEmpty(str) && !C0030ba.isEmpty(str2)) {
            this.ei.put(str, str2);
        }
    }

    public final void sendUpdateRequest() {
        if (this.eg == null || this.eh == 1) {
            X.i("skip sendUpdateRequest: page db %s, status %d", this.eg, Integer.valueOf(this.eh));
            return;
        }
        this.ej = System.currentTimeMillis();
        URL createURL = C0040bk.createURL(C0030ba.format("misc/json_update?version=%d&identifier=%s&db_version=%d", 173, C0028az.fK, Integer.valueOf(this.eg.getVersion())));
        if (createURL != null) {
            X.i("sending update request...", new Object[0]);
            setUpdateStatus(1);
            C0029b.showOverlayDialog(10);
            bA bAVar = new bA(createURL, false);
            bAVar.nn = OverlayMessage.DEFAULT_TIMEOUT;
            bAVar.no = OverlayMessage.DEFAULT_TIMEOUT;
            bAVar.setRequireSid(false);
            bAVar.setDelegate(this);
            bAVar.start(true);
            return;
        }
        X.e("invalid update url", new Object[0]);
    }

    public final void setUpdateStatus(int i) {
        this.eh = i;
        if (i == 2) {
            C0036bg.runInHandlerThread(new Runnable() {
                public final void run() {
                    bL.getInstance().startAllControllers();
                }
            });
        }
    }

    public final void setVersion(int i) {
        if (this.eg != null) {
            this.eg.setVersion(i);
        }
    }

    public final synchronized void updatePages(JSONArray jSONArray) {
        try {
            int i = jSONArray.getInt(1);
            if (this.eg != null) {
                X.i("Begin to update client db: %d", Integer.valueOf(jSONArray.length() - 2));
                if (jSONArray.length() > 2) {
                    JSONArray optJSONArray = jSONArray.optJSONArray(2);
                    this.eg.update("BEGIN TRANSACTION", new Object[0]);
                    int i2 = 0;
                    while (i2 < optJSONArray.length()) {
                        try {
                            String string = optJSONArray.getString(i2);
                            int i3 = optJSONArray.getInt(i2 + 1);
                            optJSONArray.getInt(i2 + 2);
                            String string2 = optJSONArray.getString(i2 + 3);
                            if (i3 == 1 || i3 == 2) {
                                C0002a.getWebCache().saveCacheWebFile(string, C0030ba.getBytes(string2));
                            } else {
                                this.eg.savePage(string, string2);
                            }
                            i2 += 4;
                        } catch (Exception e) {
                            X.w("Failed to update data: " + e, new Object[0]);
                        }
                    }
                    this.eg.update("END TRANSACTION", new Object[0]);
                }
                this.eg.setVersion(i);
            }
        } catch (Exception e2) {
            X.w("Failed to update database: %s", e2);
        }
        return;
    }
}
