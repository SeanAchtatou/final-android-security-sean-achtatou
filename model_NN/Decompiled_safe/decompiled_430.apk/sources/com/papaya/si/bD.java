package com.papaya.si;

import android.content.Context;
import android.net.Uri;
import com.papaya.si.by;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public class bD extends B implements by.a {
    private HashSet<String> nt = new HashSet<>(2000);
    private LinkedList<bA> nu = new LinkedList<>();
    private ArrayList<bx> nv = new ArrayList<>(4);
    private ArrayList<bz> nw = new ArrayList<>(4);
    private ThreadPoolExecutor nx;

    public bD(String str, Context context) {
        super(str, context);
    }

    private void collectAssets() {
        try {
            aZ.linesFromStream(this.cj.getAssets().open("web-resources.lst"), this.nt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isContentUrl(String str) {
        return str != null && (str.startsWith("file:///android_asset/") || str.startsWith(bE.ny));
    }

    public static String normalizeBundleUri(String str) {
        if (!str.contains("/")) {
            return "web-resources/" + str;
        }
        String[] split = str.split("/");
        StringBuilder acquireStringBuilder = C0030ba.acquireStringBuilder(str.length());
        acquireStringBuilder.append("web-resources").append('/');
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String str2 = split[i];
            if (!str2.equals("..") && !str2.equals(".")) {
                acquireStringBuilder.append(str2);
                if (i < length - 1) {
                    acquireStringBuilder.append('/');
                }
            }
        }
        return C0030ba.releaseStringBuilder(acquireStringBuilder);
    }

    private void setup() {
        this.nx = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
    }

    private static String stripHost(String str) {
        return str.startsWith(C0061v.bh) ? str.substring(C0061v.bh.length()) : str;
    }

    public synchronized void appendRequest(bA bAVar) {
        try {
            this.nu.add(bAVar);
        } catch (Exception e) {
            X.e(e, "Failed to appendRequest: " + bAVar, new Object[0]);
        }
        return;
    }

    public synchronized void appendRequests(List<bA> list) {
        int i = 0;
        synchronized (this) {
            while (true) {
                try {
                    int i2 = i;
                    if (i2 >= list.size()) {
                        break;
                    }
                    bA bAVar = list.get(i2);
                    if (bAVar != null) {
                        this.nu.addLast(bAVar);
                    }
                    i = i2 + 1;
                } catch (Exception e) {
                    X.e(e, "Failed to appendRequests", new Object[0]);
                }
            }
        }
        return;
    }

    public String bundleContentUri(String str) {
        return bundleContentUri(str, true);
    }

    public String bundleContentUri(String str, boolean z) {
        String bundleFilename = bundleFilename(str, z);
        if (bundleFilename != null) {
            return "file:///android_asset/" + bundleFilename;
        }
        return null;
    }

    public aP bundleFD(String str) {
        String bundleFilename = bundleFilename(str, false);
        if (bundleFilename != null) {
            return new aP(bundleFilename);
        }
        return null;
    }

    public String bundleFilename(String str, boolean z) {
        String normalizeBundleUri = normalizeBundleUri(str);
        if (this.nt.contains(normalizeBundleUri)) {
            return normalizeBundleUri;
        }
        if (z) {
            X.w("not found in bundle %s", str);
        }
        return null;
    }

    public String bundleOrCacheContentUri(String str, String str2) {
        String bundleContentUri = bundleContentUri(str2, false);
        return bundleContentUri != null ? bundleContentUri : cacheContentUri(str);
    }

    public synchronized String cacheContentUri(String str) {
        String str2;
        try {
            File cachedFile = cachedFile(str, true);
            str2 = cachedFile != null ? bE.ny + cachedFile.getName() : null;
        } catch (Exception e) {
            e.printStackTrace();
            str2 = null;
        }
        return str2;
    }

    public String cacheOrBundleContentUri(String str, String str2) {
        String cacheContentUri = cacheContentUri(str);
        return cacheContentUri != null ? cacheContentUri : bundleContentUri(str2, false);
    }

    public aP cacheOrBundleFD(String str, String str2) {
        aP cachedFD = cachedFD(str);
        return cachedFD == null ? bundleFD(str2) : cachedFD;
    }

    public String cacheOrBundleFilename(String str, String str2) {
        File cachedFile = cachedFile(str, false);
        return (cachedFile == null || !cachedFile.exists()) ? bundleFilename(str2, false) : cachedFile.getName();
    }

    public synchronized aP cachedFD(String str) {
        aP aPVar;
        try {
            File cachedFile = cachedFile(str, true);
            aPVar = cachedFile != null ? new aP(cachedFile) : null;
        } catch (Exception e) {
            e.printStackTrace();
            aPVar = null;
        }
        return aPVar;
    }

    public synchronized File cachedFile(String str, boolean z) {
        File file;
        try {
            file = getCacheFile(str);
            if (z && !file.exists()) {
                file = null;
            }
        } catch (Exception e) {
            X.e(e, "Failed to invoke cachedFile", new Object[0]);
            file = null;
        }
        return file;
    }

    public synchronized void connectionFailed(by byVar, int i) {
        try {
            X.w("connection failed %d, %s", Integer.valueOf(i), byVar);
            this.nv.remove(byVar);
            this.nw.remove(byVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byVar.fireConnectionFailedToRequest(i);
        return;
    }

    public void connectionFinished(by byVar) {
        String url;
        int indexOf;
        synchronized (this) {
            try {
                bA request = byVar.getRequest();
                if (byVar.getDataLength() <= 0) {
                    X.w("null data from %s", request.getUrl());
                } else if (request.isCacheable() && request.getSaveFile() == null) {
                    saveBytesWithKey(request.getUrl().toString(), byVar.getData());
                }
                if (!request.isCacheable()) {
                    try {
                        byte[] data = byVar.getData();
                        if (data != null && (indexOf = (url = request.getUrl().toString()).indexOf("__db_cache=")) >= 0) {
                            int indexOf2 = url.indexOf(38, indexOf);
                            if (indexOf2 == -1) {
                                indexOf2 = url.length();
                            }
                            JSONObject parseJsonObject = C0040bk.parseJsonObject(Uri.decode(url.substring("__db_cache=".length() + indexOf, indexOf2)));
                            if (parseJsonObject != null) {
                                String utf8String = C0030ba.utf8String(data, null);
                                if (utf8String != null) {
                                    int optInt = parseJsonObject.optInt("life", 0);
                                    if (utf8String.contains("__life__")) {
                                        optInt = C0040bk.parseJsonObject(utf8String).optInt("life", 0);
                                    }
                                    C0014al findDatabase = C0015am.getInstance().findDatabase(parseJsonObject.optString("name", ""), parseJsonObject.optInt("scope", 0));
                                    if (findDatabase == null) {
                                        X.w("db is null %s", parseJsonObject);
                                    } else if (utf8String.contains("__redirect__")) {
                                        findDatabase.kvSave(parseJsonObject.optString("key"), null, 0);
                                    } else {
                                        findDatabase.kvSave(parseJsonObject.optString("key"), utf8String, optInt);
                                    }
                                } else {
                                    X.w("content is null", new Object[0]);
                                }
                            } else {
                                X.w("invalid db cache json", new Object[0]);
                            }
                        }
                    } catch (Exception e) {
                        X.w("failed to handle db cache: %s", e);
                    }
                }
                this.nv.remove(byVar);
                this.nw.remove(byVar);
            } catch (Exception e2) {
                X.w(e2, "error occurred in connectionFinished", new Object[0]);
                e2.printStackTrace();
                this.nv.remove(byVar);
                this.nw.remove(byVar);
            } catch (Throwable th) {
                this.nv.remove(byVar);
                this.nw.remove(byVar);
                throw th;
            }
        }
        try {
            byVar.fireConnectionFinishedToRequest();
            return;
        } catch (Exception e3) {
            X.w(e3, "Failed in fireConnectionFinishedToRequest", new Object[0]);
            return;
        }
    }

    public String contentUriFromPapayaUri(String str, URL url, bA bAVar) {
        String str2 = null;
        int indexOf = str.indexOf("://");
        if (indexOf == -1) {
            URL createURL = C0040bk.createURL(str, url);
            if (createURL == null) {
                X.w("malformed url %s, base %s", str, url);
            } else if (bAVar != null) {
                bAVar.setUrl(createURL);
                bAVar.setCacheable(false);
            }
        } else {
            String substring = str.substring(0, indexOf);
            String substring2 = str.substring(indexOf + 3);
            if ("papaya_cache_bundle".equals(substring)) {
                str2 = bundleContentUri(substring2, true);
                if (str2 == null) {
                    URL createURL2 = C0040bk.createURL(substring2, url);
                    if (createURL2 == null) {
                        X.w("malformed url %s, base %s", substring2, url);
                    } else if (bAVar != null) {
                        bAVar.setUrl(createURL2);
                        bAVar.setCacheable(false);
                    }
                }
            } else if ("papaya_cache_file".equals(substring)) {
                URL createURL3 = C0040bk.createURL(substring2, url);
                if (createURL3 != null) {
                    str2 = cacheOrBundleContentUri(createURL3.toString(), substring2);
                    if (str2 == null && bAVar != null) {
                        bAVar.setUrl(createURL3);
                        bAVar.setCacheable(true);
                    }
                } else {
                    X.w("malformed url %s, base %s", substring2, url);
                }
            } else {
                X.w("unsupported scheme %s, %s", substring, str);
            }
        }
        return str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public String createLocalRefHtml(String str, URL url, boolean z, boolean z2) {
        if (!C0042c.isInitialized()) {
            return "";
        }
        if (str == null || str.length() == 0) {
            X.w("empty html content", new Object[0]);
            return "";
        }
        ArrayList arrayList = new ArrayList(8);
        StringBuilder acquireStringBuilder = C0030ba.acquireStringBuilder(str.length());
        int indexOf = str.indexOf("\"papaya_cache");
        int i = 0;
        while (indexOf != -1) {
            acquireStringBuilder.append((CharSequence) str, i, indexOf);
            int indexOf2 = str.indexOf(34, indexOf + 1);
            if (indexOf2 != -1) {
                int i2 = indexOf2 + 1;
                String substring = str.substring(indexOf + 1, indexOf2);
                int indexOf3 = substring.indexOf("://");
                if (indexOf3 != -1) {
                    String substring2 = substring.substring(0, indexOf3);
                    String substring3 = substring.substring(indexOf3 + 3);
                    try {
                        URL createURL = C0040bk.createURL(substring3, url);
                        if (createURL != null) {
                            String url2 = createURL.toString();
                            if ("papaya_cache_file".equals(substring2)) {
                                String cacheOrBundleContentUri = cacheOrBundleContentUri(url2, substring3);
                                if (cacheOrBundleContentUri != null) {
                                    acquireStringBuilder.append('\"').append(cacheOrBundleContentUri).append('\"');
                                    i = i2;
                                } else {
                                    bA bAVar = new bA(createURL, true);
                                    bAVar.setRequireSid(z2);
                                    if (substring3.endsWith(".js")) {
                                        bAVar.setConnectionType(1);
                                    }
                                    arrayList.add(bAVar);
                                    bAVar.setSaveFile(new File(getCacheDir(), keyToStoreName(url2)));
                                    StringBuilder append = acquireStringBuilder.append('\"');
                                    if (z) {
                                        substring3 = url2;
                                    }
                                    append.append(substring3).append('\"');
                                    i = i2;
                                }
                            } else if ("papaya_cache_bundle".equals(substring2)) {
                                String bundleOrCacheContentUri = bundleOrCacheContentUri(url2, substring3);
                                if (bundleOrCacheContentUri != null) {
                                    acquireStringBuilder.append('\"').append(bundleOrCacheContentUri).append('\"');
                                    i = i2;
                                } else {
                                    StringBuilder append2 = acquireStringBuilder.append('\"');
                                    if (z) {
                                        substring3 = url2;
                                    }
                                    append2.append(substring3).append('\"');
                                    i = i2;
                                }
                            } else if ("papaya_cache_css".equals(substring2)) {
                                X.e("papaya_cache_css is not supported", new Object[0]);
                                StringBuilder append3 = acquireStringBuilder.append('\"');
                                if (z) {
                                    substring3 = url2;
                                }
                                append3.append(substring3).append('\"');
                                i = i2;
                            } else {
                                X.e("unknown papaya_cache scheme: " + substring2, new Object[0]);
                                i = i2;
                            }
                        } else {
                            X.e("uri is null: " + substring3, new Object[0]);
                            i = i2;
                        }
                    } catch (Exception e) {
                        X.e(e, "Failed to parse html", new Object[0]);
                        i = i2;
                    }
                } else {
                    i = i2;
                }
            } else {
                i = indexOf;
            }
            indexOf = str.indexOf("\"papaya_cache", indexOf + 1);
        }
        if (i < str.length()) {
            acquireStringBuilder.append((CharSequence) str, i, str.length());
        }
        if (!arrayList.isEmpty()) {
            insertRequests(arrayList);
        }
        return C0030ba.releaseStringBuilder(acquireStringBuilder);
    }

    /* access modifiers changed from: protected */
    public void doClose() {
        for (int i = 0; i < this.nv.size(); i++) {
            bx bxVar = this.nv.get(i);
            try {
                bxVar.setDelegate(null);
            } catch (Exception e) {
            }
            try {
                bxVar.getRequest().setDelegate(null);
            } catch (Exception e2) {
            }
        }
        this.nv.clear();
        try {
            if (this.nx != null) {
                this.nx.shutdownNow();
            }
            this.nx = null;
        } catch (Exception e3) {
            X.w(e3, "failed to shutdown the httpservice", new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public boolean doInitCache() {
        collectAssets();
        setup();
        return true;
    }

    public boolean encapsuleHttpInTcp() {
        return true;
    }

    public synchronized boolean existInQueue(URL url) {
        boolean z;
        try {
            if (!existInWorker(url)) {
                int i = 0;
                while (true) {
                    if (i >= this.nu.size()) {
                        z = false;
                        break;
                    } else if (C0040bk.urlEquals(this.nu.get(i).getUrl(), url)) {
                        z = true;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                z = true;
            }
        } catch (Exception e) {
            X.w(e, "Failed in existQueue", new Object[0]);
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0030, code lost:
        if (r1 >= r4.nw.size()) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        if (com.papaya.si.C0040bk.urlEquals(r4.nw.get(r1).getRequest().getUrl(), r5) == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean existInWorker(java.net.URL r5) {
        /*
            r4 = this;
            r2 = 1
            r3 = 0
            monitor-enter(r4)
            r1 = r3
        L_0x0004:
            java.util.ArrayList<com.papaya.si.bx> r0 = r4.nv     // Catch:{ Exception -> 0x004e }
            int r0 = r0.size()     // Catch:{ Exception -> 0x004e }
            if (r1 >= r0) goto L_0x0029
            java.util.ArrayList<com.papaya.si.bx> r0 = r4.nv     // Catch:{ Exception -> 0x004e }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x004e }
            com.papaya.si.bx r0 = (com.papaya.si.bx) r0     // Catch:{ Exception -> 0x004e }
            com.papaya.si.bA r0 = r0.getRequest()     // Catch:{ Exception -> 0x004e }
            java.net.URL r0 = r0.getUrl()     // Catch:{ Exception -> 0x004e }
            boolean r0 = com.papaya.si.C0040bk.urlEquals(r0, r5)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x0025
            r0 = r2
        L_0x0023:
            monitor-exit(r4)
            return r0
        L_0x0025:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0004
        L_0x0029:
            r1 = r3
        L_0x002a:
            java.util.ArrayList<com.papaya.si.bz> r0 = r4.nw     // Catch:{ Exception -> 0x004e }
            int r0 = r0.size()     // Catch:{ Exception -> 0x004e }
            if (r1 >= r0) goto L_0x0057
            java.util.ArrayList<com.papaya.si.bz> r0 = r4.nw     // Catch:{ Exception -> 0x004e }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x004e }
            com.papaya.si.bz r0 = (com.papaya.si.bz) r0     // Catch:{ Exception -> 0x004e }
            com.papaya.si.bA r0 = r0.getRequest()     // Catch:{ Exception -> 0x004e }
            java.net.URL r0 = r0.getUrl()     // Catch:{ Exception -> 0x004e }
            boolean r0 = com.papaya.si.C0040bk.urlEquals(r0, r5)     // Catch:{ Exception -> 0x004e }
            if (r0 == 0) goto L_0x004a
            r0 = r2
            goto L_0x0023
        L_0x004a:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x002a
        L_0x004e:
            r0 = move-exception
            java.lang.String r1 = "Failed in existInWorker"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0059 }
            com.papaya.si.X.w(r0, r1, r2)     // Catch:{ all -> 0x0059 }
        L_0x0057:
            r0 = r3
            goto L_0x0023
        L_0x0059:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.papaya.si.bD.existInWorker(java.net.URL):boolean");
    }

    public aP fdFromContentUrl(String str) {
        if (str == null) {
            return null;
        }
        if (str.startsWith("file:///android_asset/")) {
            return new aP(str.substring("file:///android_asset/".length()));
        }
        if (str.startsWith(bE.ny)) {
            return new aP(new File(this.ci, str.substring(bE.ny.length())));
        }
        return null;
    }

    public aP fdFromPapayaUri(String str, URL url, bA bAVar) {
        int indexOf = str.indexOf("://");
        if (indexOf == -1) {
            URL createURL = C0040bk.createURL(str, url);
            if (createURL == null) {
                X.w("malformed url %s, base %s", str, url);
                return null;
            } else if (bAVar == null) {
                return null;
            } else {
                bAVar.setUrl(createURL);
                bAVar.setCacheable(false);
                return null;
            }
        } else {
            String substring = str.substring(0, indexOf);
            String substring2 = str.substring(indexOf + 3);
            if ("papaya_cache_bundle".equals(substring)) {
                aP bundleFD = bundleFD(substring2);
                if (bundleFD != null) {
                    return bundleFD;
                }
                URL createURL2 = C0040bk.createURL(substring2, url);
                if (createURL2 == null) {
                    X.w("malformed url %s, base %s", substring2, url);
                    return bundleFD;
                } else if (bAVar == null) {
                    return bundleFD;
                } else {
                    bAVar.setUrl(createURL2);
                    bAVar.setCacheable(false);
                    return bundleFD;
                }
            } else if ("papaya_cache_file".equals(substring)) {
                URL createURL3 = C0040bk.createURL(substring2, url);
                if (createURL3 != null) {
                    aP cacheOrBundleFD = cacheOrBundleFD(createURL3.toString(), substring2);
                    if (cacheOrBundleFD != null || bAVar == null) {
                        return cacheOrBundleFD;
                    }
                    bAVar.setUrl(createURL3);
                    bAVar.setCacheable(true);
                    return cacheOrBundleFD;
                }
                X.w("malformed url %s, base %s", substring2, url);
                return null;
            } else if ("http".equals(substring)) {
                if (bAVar == null) {
                    return null;
                }
                bAVar.setUrl(C0040bk.createURL(str, url));
                return null;
            } else if ("content".equals(substring)) {
                File file = new File(this.ci, str.substring(bE.ny.length()));
                if (file.exists()) {
                    return new aP(file);
                }
                X.w("cache file doesn't exist %s", file);
                return null;
            } else if (str.startsWith("file:///android_asset/")) {
                return new aP(str.substring("file:///android_asset/".length()));
            } else {
                X.w("unsupported scheme %s, %s", substring, str);
                return null;
            }
        }
    }

    public synchronized void insertRequest(bA bAVar) {
        try {
            this.nu.add(0, bAVar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public synchronized void insertRequests(List<bA> list) {
        try {
            for (int size = list.size() - 1; size >= 0; size--) {
                bA bAVar = list.get(size);
                if (bAVar != null) {
                    this.nu.addFirst(bAVar);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: protected */
    public String keyToStoreName(String str) {
        return aZ.md5(stripHost(str));
    }

    public void poll() {
        bA bAVar;
        int i = 0;
        if (!this.ck) {
            synchronized (this) {
                try {
                    String sessionKey = aC.getInstance().getSessionKey();
                    if (!this.nu.isEmpty()) {
                        while (this.nv.size() < 4 && !this.nu.isEmpty() && i != -1) {
                            int i2 = i;
                            while (true) {
                                if (i2 >= this.nu.size()) {
                                    bAVar = null;
                                    break;
                                }
                                bAVar = this.nu.get(i2);
                                if ((!encapsuleHttpInTcp() || bAVar.getConnectionType() == 0) && ((sessionKey != null || !bAVar.isRequireSid()) && (!bAVar.isCacheable() || !existInWorker(bAVar.getUrl())))) {
                                    this.nu.remove(i2);
                                    i = i2;
                                } else {
                                    i2++;
                                }
                            }
                            this.nu.remove(i2);
                            i = i2;
                            if (bAVar == null) {
                                i = -1;
                            } else {
                                File cachedFile = bAVar.isCacheable() ? cachedFile(bAVar.getUrl().toString(), true) : null;
                                if (cachedFile == null) {
                                    bx bxVar = new bx(bAVar, this);
                                    this.nv.add(bxVar);
                                    this.nx.submit(bxVar);
                                } else if (bAVar.getDelegate() != null) {
                                    byte[] dataFromFile = aZ.dataFromFile(cachedFile);
                                    bx bxVar2 = new bx(bAVar);
                                    bxVar2.setData(dataFromFile);
                                    bxVar2.fireConnectionFinishedToRequest();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    X.w(e, "error occurred in web cache loop", new Object[0]);
                }
            }
        }
    }

    public String relativeUriFromPapayaUri(String str) {
        int indexOf = str.indexOf("://");
        return indexOf == -1 ? str : str.substring(indexOf + 3);
    }

    public boolean removeRequest(bA bAVar) {
        if (bAVar == null) {
            return false;
        }
        synchronized (this) {
            if (this.nu.contains(bAVar)) {
                this.nu.remove(bAVar);
                return true;
            }
            int i = 0;
            while (i < this.nv.size()) {
                try {
                    if (this.nv.get(i).getRequest() == bAVar) {
                        this.nv.remove(i);
                        return true;
                    }
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            for (int i2 = 0; i2 < this.nw.size(); i2++) {
                if (this.nw.get(i2).getRequest() == bAVar) {
                    this.nw.remove(i2);
                    return true;
                }
            }
            return false;
        }
    }

    public synchronized void saveCacheWebFile(String str, byte[] bArr) {
        try {
            aZ.writeBytesToFile(cachedFile(str, false), bArr);
        } catch (Exception e) {
            X.e(e, "Failed to saveCacheWebFile", new Object[0]);
        }
        return;
    }
}
