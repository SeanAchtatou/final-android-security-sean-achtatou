package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.papaya.view.AppIconView;
import com.papaya.view.CardImageView;

public final class W {
    public AppIconView dn;

    /* renamed from: do  reason: not valid java name */
    public TextView f0do;
    public CardImageView imageView;

    public W(View view) {
        this.dn = (AppIconView) C0036bg.find(view, "app_icon");
        this.dn.setFocusable(false);
        this.imageView = (CardImageView) C0036bg.find(view, "image");
        this.f0do = (TextView) C0036bg.find(view, "message");
    }
}
