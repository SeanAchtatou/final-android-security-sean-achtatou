package com.papaya.si;

import java.util.Iterator;

public final class T extends E<D> {
    public T() {
        setName(C0042c.getString("chattingnow"));
        setReserveGroupHeader(true);
    }

    private void afterRemove(D d) {
        if (d != null) {
            d.setChatActive(false);
            d.cm = null;
            if (d instanceof Q) {
                Q q = (Q) d;
                C0042c.send(23, Integer.valueOf(q.dd));
                q.logout();
            } else if (d instanceof C0009ag) {
                C0042c.getSession().getPrivateChats().remove((C0009ag) d);
            }
        }
    }

    public final synchronized boolean add(D d) {
        boolean add;
        add = super.add(d);
        if (add) {
            if (d instanceof C0012aj) {
                if (d.state != 0) {
                    d.addSystemMessage(d.getTitle() + C0042c.getString("base_status_online"));
                } else {
                    d.addSystemMessage(d.getTitle() + C0042c.getString("base_status_offline"));
                }
            } else if (d instanceof Q) {
                if (d.state == 0) {
                    C0042c.send(21, Integer.valueOf(((Q) d).dd));
                    d.addSystemMessage(C0042c.getString("base_login"));
                }
            } else if (!(d instanceof C0009ag) && !(d instanceof L) && !(d instanceof C0007ae)) {
                X.e("unknown card added: " + d, new Object[0]);
            }
        }
        return add;
    }

    public final void clear() {
        Iterator it = this.co.iterator();
        while (it.hasNext()) {
            afterRemove((D) it.next());
        }
        super.clear();
    }

    public final D remove(int i) {
        D remove = super.remove(i);
        if (remove != null) {
            afterRemove(remove);
        }
        return remove;
    }

    public final boolean remove(D d) {
        boolean remove = super.remove(d);
        if (remove && d != null) {
            afterRemove(d);
        }
        return remove;
    }
}
