package com.papaya.si;

import com.papaya.web.WebActivity;

public final class bu {
    private static WebActivity mU;

    private bu() {
    }

    public static void clear() {
        mU = null;
    }

    public static synchronized void fireImageUploadFailed(int i, int i2) {
        synchronized (bu.class) {
            try {
                WebActivity webActivity = (WebActivity) C0029b.findActivity(WebActivity.class, i);
                if (webActivity != null) {
                    webActivity.onImageUploadFailed(i2);
                }
            } catch (Exception e) {
                X.e(e, "Failed to fireImageUploaded", new Object[0]);
            }
        }
        return;
    }

    public static synchronized void fireImageUploaded(int i, int i2, int i3) {
        synchronized (bu.class) {
            try {
                WebActivity webActivity = (WebActivity) C0029b.findActivity(WebActivity.class, i);
                if (webActivity != null) {
                    webActivity.onImageUploaded(i2, i3);
                }
            } catch (Exception e) {
                X.e(e, "Failed to fireImageUploaded", new Object[0]);
            }
        }
        return;
    }

    public static void onImageUploadFailed(int i) {
        if (mU != null) {
            try {
                mU.onImageUploadFailed(i);
            } catch (Exception e) {
                X.e(e, "Failed to invoke onImageUploadFailed", new Object[0]);
            }
        }
        mU = null;
    }

    public static void onImageUploaded(int i, int i2) {
        if (mU != null) {
            try {
                mU.onImageUploaded(i, i2);
            } catch (Exception e) {
                X.e(e, "Failed to invoke onImageUploaded", new Object[0]);
            }
        }
        mU = null;
    }

    public static void startUploading(WebActivity webActivity) {
        mU = webActivity;
    }
}
