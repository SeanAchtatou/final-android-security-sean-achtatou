package com.papaya.si;

import android.app.Dialog;
import android.content.Context;
import com.papaya.web.WebViewController;

public final class bF extends WebViewController {
    private Dialog nA;
    private WebViewController nz;

    public bF(Context context, String str) {
        initialize(context, str);
    }

    public final Dialog getDialog() {
        return this.nA;
    }

    public final WebViewController getParentController() {
        return this.nz;
    }

    public final void setDialog(Dialog dialog) {
        this.nA = dialog;
    }

    public final void setParentController(WebViewController webViewController) {
        this.nz = webViewController;
    }
}
