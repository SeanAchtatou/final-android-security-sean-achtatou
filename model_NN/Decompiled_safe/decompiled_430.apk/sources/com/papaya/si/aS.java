package com.papaya.si;

import com.papaya.si.aU;

public interface aS<PT extends aU> {
    boolean onDataStateChanged(PT pt);
}
