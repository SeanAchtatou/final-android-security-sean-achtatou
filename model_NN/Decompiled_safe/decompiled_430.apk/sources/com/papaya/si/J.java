package com.papaya.si;

import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.ActionsImageButton;

public class J implements ActionsImageButton.Delegate {
    private /* synthetic */ ChatActivity cM;

    public J(ChatActivity chatActivity) {
        this.cM = chatActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onActionSelected(ActionsImageButton actionsImageButton, Action action) {
        C0009ag agVar;
        A session = C0042c.getSession();
        try {
            S s = (S) actionsImageButton.payload;
            switch (action.id) {
                case 1:
                    this.cM.dismissDialog(2);
                    C0029b.openHome(this.cM, s.cU);
                    return;
                case 2:
                    this.cM.dismissDialog(2);
                    session.getPrivateChatWhiteList().add(Integer.valueOf(s.cU));
                    D findByUserID = session.getFriends().findByUserID(s.cU);
                    if (findByUserID == null) {
                        findByUserID = session.getPrivateChats().findBySID(s.dk);
                    }
                    if (findByUserID == null) {
                        C0009ag agVar2 = new C0009ag();
                        agVar2.dl = s.dl;
                        agVar2.cU = s.cU;
                        agVar2.dk = s.dk;
                        session.getPrivateChats().add(agVar2);
                        agVar = agVar2;
                    } else {
                        agVar = findByUserID;
                    }
                    session.getChattings().add(agVar);
                    this.cM.refreshWithCard(agVar, false);
                    agVar.fireDataStateChanged();
                    session.fireDataStateChanged();
                    return;
                case 3:
                default:
                    X.e("unknown action id: " + action.id, new Object[0]);
                    return;
                case 4:
                    this.cM.dismissDialog(2);
                    if (this.cM.cG instanceof Q) {
                        C0042c.send(48, Integer.valueOf(((Q) this.cM.cG).dd), Integer.valueOf(s.cU));
                        return;
                    }
                    return;
            }
        } catch (Exception e) {
            X.e(e, "Failed in onActionSelected", new Object[0]);
        }
        X.e(e, "Failed in onActionSelected", new Object[0]);
    }
}
