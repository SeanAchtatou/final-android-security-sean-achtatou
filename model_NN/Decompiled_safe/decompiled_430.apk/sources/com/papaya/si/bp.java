package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.papaya.web.WebViewController;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public final class bp extends WebView implements br {
    private static final Pattern mp = Pattern.compile("&");
    private static int mq = 0;
    /* access modifiers changed from: private */
    public boolean ck = false;
    private long gz;
    private WeakReference<a> hY;
    private boolean mA = true;
    private boolean mB = true;
    private boolean mC = false;
    private boolean mD = true;
    private bG mE;
    private WebViewController mr;
    private WebViewController ms;
    private URL mt;
    private bq mu = null;
    /* access modifiers changed from: private */
    public String mv = null;
    private boolean mw = false;
    private String mx;
    private bH my;
    private boolean mz = true;

    public interface a {
        void onPageFinished(bp bpVar, String str);

        void onPageStarted(bp bpVar, String str, Bitmap bitmap);

        void onReceivedError(bp bpVar, int i, String str, String str2);

        void onWebLoaded(bp bpVar);

        boolean shouldOverrideUrlLoading(bp bpVar, String str);
    }

    final class b extends WebChromeClient {
        /* synthetic */ b() {
            this((byte) 0);
        }

        private b(byte b) {
        }
    }

    static final class c extends WebViewClient {
        /* synthetic */ c() {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void onPageFinished(WebView webView, final String str) {
            final bp bpVar = (bp) webView;
            final a delegate = bpVar.getDelegate();
            if (delegate != null) {
                C0036bg.postDelayed(new Runnable() {
                    public final void run() {
                        a.this.onPageFinished(bpVar, str);
                    }
                }, 200);
            }
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            bp bpVar = (bp) webView;
            a delegate = bpVar.getDelegate();
            if (delegate != null) {
                delegate.onPageStarted(bpVar, str, bitmap);
            }
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            bp bpVar = (bp) webView;
            a delegate = bpVar.getDelegate();
            if (delegate != null) {
                delegate.onReceivedError(bpVar, i, str, str2);
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (str != null && (str.startsWith("tel:") || str.startsWith("geo:0,0?q=") || str.startsWith("mailto:"))) {
                return true;
            }
            bp bpVar = (bp) webView;
            a delegate = bpVar.getDelegate();
            if (delegate != null) {
                return delegate.shouldOverrideUrlLoading(bpVar, str);
            }
            X.w("webview shouldOverrideUrlLoading: delegate is null?", new Object[0]);
            return true;
        }
    }

    public bp(Context context) {
        super(context);
        setupHooks();
        mq++;
    }

    public bp(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setupHooks();
    }

    public bp(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setupHooks();
    }

    /* access modifiers changed from: private */
    public void callJSOMT(String str) {
        if (!this.ck) {
            loadUrl("javascript:" + str);
        }
    }

    private void setupHooks() {
        setScrollBarStyle(0);
        this.mx = getContext().getString(C0065z.stringID("web_default_title"));
        WebSettings settings = getSettings();
        settings.setCacheMode(2);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setJavaScriptEnabled(true);
        settings.setLightTouchEnabled(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setLoadsImagesAutomatically(true);
        setWebChromeClient(new b());
        setWebViewClient(new c());
        this.my = new bH(this);
        addJavascriptInterface(this.my, "Papaya");
        setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                try {
                    switch (motionEvent.getAction()) {
                        case 0:
                        case 1:
                            if (!view.hasFocus()) {
                                view.requestFocus();
                                break;
                            }
                            break;
                    }
                } catch (Exception e) {
                    X.e(e, "Failed in PPYWebView.onTouch", new Object[0]);
                }
                return false;
            }
        });
    }

    public final void callJS(final String str) {
        if (!this.ck) {
            if (C0036bg.isMainThread()) {
                callJSOMT(str);
            } else {
                C0036bg.runInHandlerThread(new Runnable() {
                    public final void run() {
                        if (!bp.this.ck) {
                            bp.this.callJSOMT(str);
                        }
                    }
                });
            }
        }
    }

    public final void callJSFunc(String str, Object... objArr) {
        callJS(C0030ba.format(str, objArr));
    }

    public final void changeOrientation(int i) {
        noWarnCallJS("changeorientation", "changeorientation(" + i + ')');
    }

    public final void close() {
        mq--;
        this.ck = true;
        try {
            this.my.close();
            setDelegate(null);
            stopLoading();
        } catch (Exception e) {
            X.e(e, "Failed to close webview", new Object[0]);
        }
        this.mr = null;
        this.ms = null;
        try {
            destroy();
        } catch (Exception e2) {
            X.e(e2, "Failed to destroy webview", new Object[0]);
        }
    }

    public final void fireLoadError(URL url, int i, String str) {
        a delegate = getDelegate();
        if (delegate != null) {
            delegate.onReceivedError(this, i, str, url.toString());
        }
    }

    public final void fireStartLoad(URL url) {
        a delegate = getDelegate();
        if (delegate != null) {
            delegate.onPageStarted(this, url.toString(), null);
        }
    }

    public final WebViewController getController() {
        return this.mr;
    }

    public final String getDefaultTitle() {
        return this.mx;
    }

    public final a getDelegate() {
        if (this.hY != null) {
            return this.hY.get();
        }
        return null;
    }

    public final bG getHistory() {
        return this.mE;
    }

    public final WebViewController getLastController() {
        return this.ms;
    }

    public final int getOrientation() {
        return C0036bg.getOrientation(getOwnerActivity());
    }

    public final Activity getOwnerActivity() {
        if (this.mr != null) {
            return this.mr.getOwnerActivity();
        }
        if (getContext() instanceof Activity) {
            return (Activity) getContext();
        }
        return null;
    }

    public final String getPageName() {
        return this.mv;
    }

    public final bH getPapayaScript() {
        return this.my;
    }

    public final URL getPapayaURL() {
        return this.mt;
    }

    public final long getStartTime() {
        return this.gz;
    }

    public final boolean isClosed() {
        return this.ck;
    }

    public final boolean isGlobalReusable() {
        return this.mC;
    }

    public final boolean isLoadFromString() {
        return this.mw;
    }

    public final boolean isMenuEnabled() {
        return this.mD;
    }

    public final boolean isRecylable() {
        return this.mB;
    }

    public final boolean isRequireSid() {
        return this.mz;
    }

    public final boolean isReusable() {
        return this.mA;
    }

    public final void loadPapayaURL(URL url) {
        if (url != null) {
            if (this.mu != null) {
                this.mu.cancel();
                this.mu.setDelegate(null);
                this.mu = null;
            }
            if (C0030ba.isNotEmpty(this.mv)) {
                noWarnCallJS("webdestroyed", "webdestroyed();");
            }
            this.my.setRequestJson(null);
            this.mA = true;
            this.mC = false;
            this.mB = true;
            this.mD = true;
            String path = url.getPath();
            try {
                if (!C0030ba.isEmpty(path)) {
                    String query = url.getQuery();
                    JSONObject jSONObject = new JSONObject();
                    if (!C0030ba.isEmpty(query)) {
                        String replaceAll = query.replaceAll("&amp;", "&");
                        String[] split = mp.split(replaceAll);
                        for (String str : split) {
                            int indexOf = str.indexOf("=");
                            int indexOf2 = str.indexOf("#");
                            if (indexOf2 != -1) {
                                jSONObject.put("__anchor__", str.substring(indexOf2 + 1));
                            }
                            if (indexOf != -1) {
                                String substring = str.substring(0, indexOf);
                                String substring2 = indexOf2 == -1 ? str.substring(indexOf + 1) : str.substring(indexOf + 1, indexOf2);
                                if (!C0030ba.isEmpty(substring) && !C0030ba.isEmpty(substring2)) {
                                    jSONObject.put(substring, Uri.decode(substring2));
                                }
                            }
                        }
                        jSONObject.put("__current__", path + '?' + replaceAll);
                    } else {
                        jSONObject.put("__current__", path);
                    }
                    jSONObject.put("__page__", path);
                    this.my.setRequestJson(jSONObject.toString());
                }
            } catch (JSONException e) {
                X.w("Failed to parse page url: " + e, new Object[0]);
            }
            this.mw = false;
            fireStartLoad(url);
            this.mu = new bq(url, this.mz);
            this.mu.setDelegate(this);
            this.mu.start();
        }
    }

    public final URL newURI(String str) {
        if (C0030ba.isEmpty(str)) {
            return null;
        }
        return this.mt != null ? C0040bk.createURL(str, this.mt) : str.contains("://") ? C0040bk.createURL(str, null) : C0040bk.createURL(C0061v.bi + str, null);
    }

    public final void noWarnCallJS(String str, String str2) {
        callJS(C0030ba.format("if (typeof %s != 'undefined') %s;", str, str2));
    }

    public final void onPageContentLoadFailed(bq bqVar) {
        if (bqVar == this.mu) {
            this.mu = null;
            fireLoadError(bqVar.getUrl(), -1, null);
        }
    }

    public final void onPageContentLoaded(bq bqVar) {
        if (!this.ck) {
            this.mv = this.mu.getPageName();
            aD webCache = C0002a.getWebCache();
            String pageContent = bqVar.getPageContent();
            this.mu = null;
            this.mw = true;
            URL redirectUrl = bqVar.getRedirectUrl();
            if (redirectUrl != null) {
                this.mt = redirectUrl;
            } else {
                this.mt = bqVar.getUrl();
            }
            this.gz = System.currentTimeMillis();
            String createLocalRefHtml = webCache.createLocalRefHtml(pageContent, this.mt, true, this.mz);
            C0036bg.assertMainThread();
            loadDataWithBaseURL(this.mt.toString(), createLocalRefHtml, "text/html", "UTF-8", null);
        }
    }

    public final void openUriString(String str) {
        loadUrl(str);
    }

    public final void setClosed(boolean z) {
        this.ck = z;
    }

    public final void setController(WebViewController webViewController) {
        this.mr = webViewController;
        if (webViewController != null) {
            this.ms = webViewController;
        }
    }

    public final void setDefaultTitle(String str) {
        this.mx = str;
    }

    public final void setDelegate(a aVar) {
        this.hY = new WeakReference<>(aVar);
    }

    public final void setGlobalReusable(boolean z) {
        this.mC = z;
    }

    public final void setHistory(bG bGVar) {
        this.mE = bGVar;
    }

    public final void setLastController(WebViewController webViewController) {
        this.ms = webViewController;
    }

    public final void setLoadFromString(boolean z) {
        this.mw = z;
    }

    public final void setMenuEnabled(boolean z) {
        this.mD = z;
    }

    public final void setPapayaScript(bH bHVar) {
        this.my = bHVar;
    }

    public final void setPapayaURL(URL url) {
        this.mt = url;
        if (this.mE != null) {
            this.mE.setURL(url);
        }
    }

    public final void setRecylable(boolean z) {
        this.mB = z;
    }

    public final void setRequireSid(boolean z) {
        this.mz = z;
    }

    public final void setReusable(boolean z) {
        this.mA = z;
    }

    public final void setStartTime(long j) {
        this.gz = j;
    }

    public final void setVisibility(int i) {
        super.setVisibility(i);
        this.my.updateViewsVisibility(i);
        if (i == 0) {
            requestFocus(130);
            WebViewController webViewController = this.mr;
            if (webViewController == null) {
                X.w("controller is null, parent %s", getParent());
            } else if (getParent() != webViewController.getWebContentView()) {
                X.w("inconsistent parent views, %s, %s", getParent(), webViewController.getWebContentView());
                C0036bg.addView(webViewController.getWebContentView(), this, true);
            }
        }
    }

    public final String toString() {
        return "PPYWebView [controller=" + this.mr + ", lastController=" + this.ms + ", papayaURL=" + this.mt + ", loadFromString=" + this.mw + ", closed=" + this.ck + ", reusable=" + this.mA + ", recylable=" + this.mB + ", globalReusable=" + this.mC + "]";
    }

    public final void updateTitleFromHTML() {
        callJS("window.Papaya.updateTitle_(document.title)");
    }
}
