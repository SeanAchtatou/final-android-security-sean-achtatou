package com.papaya.si;

import java.util.LinkedList;

public final class aO {
    private LinkedList<byte[]> gO = new LinkedList<>();

    public final synchronized byte[] acquire(int i) {
        int i2;
        byte[] remove;
        int i3 = 0;
        synchronized (this) {
            int size = this.gO.size();
            while (true) {
                if (i3 >= size) {
                    i2 = -1;
                    break;
                } else if (this.gO.get(i3).length >= i) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            remove = i2 != -1 ? this.gO.remove(i2) : this.gO.isEmpty() ? new byte[i] : this.gO.remove(0);
        }
        return remove;
    }

    public final synchronized void clear() {
        this.gO.clear();
    }

    public final synchronized void release(byte[] bArr) {
        if (!this.gO.contains(bArr)) {
            this.gO.addLast(bArr);
        }
    }
}
