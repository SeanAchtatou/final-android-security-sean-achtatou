package com.papaya.si;

import android.graphics.drawable.Drawable;

/* renamed from: com.papaya.si.ae  reason: case insensitive filesystem */
public final class C0007ae extends D {
    public String dM;
    public String dR;
    public String dS;
    public C0006ad dT;

    public final Drawable getDefaultDrawable() {
        return C0042c.getBitmapDrawable("im_face_" + this.dT.dK);
    }

    public final CharSequence getSubtitle() {
        return this.dS;
    }

    public final String getTimeLabel() {
        return null;
    }

    public final String getTitle() {
        return this.dR;
    }

    public final boolean isGrayScaled() {
        return this.state == 0;
    }
}
