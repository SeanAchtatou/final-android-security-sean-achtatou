package com.papaya.si;

import android.view.View;
import android.widget.TextView;
import com.papaya.view.CardImageView;

/* renamed from: com.papaya.si.ah  reason: case insensitive filesystem */
public final class C0010ah {

    /* renamed from: do  reason: not valid java name */
    public TextView f1do;
    public CardImageView imageView;

    public C0010ah(View view) {
        this.imageView = (CardImageView) C0036bg.find(view, "image");
        this.f1do = (TextView) C0036bg.find(view, "message");
    }
}
