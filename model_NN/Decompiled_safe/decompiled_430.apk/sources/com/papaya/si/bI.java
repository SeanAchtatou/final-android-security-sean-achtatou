package com.papaya.si;

import java.util.Vector;

class bI implements C0059t {
    private /* synthetic */ bH nP;

    bI(bH bHVar) {
        this.nP = bHVar;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        bp access$000 = this.nP.nH;
        if (access$000 != null) {
            try {
                Object java2jsonValue = C0040bk.java2jsonValue(vector);
                if (java2jsonValue != null) {
                    access$000.noWarnCallJS("onPotpData", C0030ba.format("onPotpData('%s')", C0040bk.escapeJS(java2jsonValue.toString())));
                    return;
                }
                X.w("java2jsonValue is null", new Object[0]);
            } catch (Exception e) {
                X.e(e, "Failed in webscript potp callback", new Object[0]);
            }
        }
    }
}
