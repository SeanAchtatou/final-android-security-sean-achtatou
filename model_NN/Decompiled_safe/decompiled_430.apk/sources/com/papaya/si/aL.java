package com.papaya.si;

public abstract class aL implements aU {
    private aT gJ;

    public void fireDataStateChanged() {
        if (this.gJ != null) {
            this.gJ.fireDataStateChanged();
        }
    }

    public void registerMonitor(aS aSVar) {
        if (this.gJ == null) {
            this.gJ = new aT(this);
        }
        this.gJ.registerMonitor(aSVar);
    }

    public void unregisterMonitor(aS aSVar) {
        if (this.gJ != null) {
            this.gJ.unregisterMonitor(aSVar);
        }
    }
}
