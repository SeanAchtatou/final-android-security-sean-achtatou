package com.papaya.si;

import java.util.ArrayList;
import java.util.List;

public final class aW {
    private List<aX> hl = new ArrayList();

    public final synchronized aX acquire() {
        return this.hl.isEmpty() ? new aX() : this.hl.remove(0);
    }

    public final synchronized void clear() {
        this.hl.clear();
    }

    public final synchronized void release(aX aXVar) {
        if (!this.hl.contains(aXVar)) {
            this.hl.add(aXVar);
        }
    }
}
