package com.papaya.si;

import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import java.util.HashMap;

public final class aN {
    private HashMap<String, SoftReference<Bitmap>> gN;

    public aN() {
        this(10);
    }

    public aN(int i) {
        this.gN = new HashMap<>(i);
    }

    public final Bitmap get(String str) {
        SoftReference softReference = this.gN.get(str);
        if (softReference == null) {
            return null;
        }
        Bitmap bitmap = (Bitmap) softReference.get();
        if (bitmap != null && !bitmap.isRecycled()) {
            return bitmap;
        }
        this.gN.remove(str);
        return null;
    }

    public final void put(String str, Bitmap bitmap) {
        if (str != null && bitmap != null) {
            this.gN.put(str, new SoftReference(bitmap));
        }
    }

    public final void refresh() {
        C0030ba.clearReferences(this.gN);
    }

    public final int size() {
        C0030ba.clearReferences(this.gN);
        return this.gN.size();
    }
}
