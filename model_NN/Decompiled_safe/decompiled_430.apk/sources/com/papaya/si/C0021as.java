package com.papaya.si;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Projection;

/* renamed from: com.papaya.si.as  reason: case insensitive filesystem */
public final class C0021as extends MyLocationOverlay {
    private boolean eu = false;
    private Paint ev;
    private Point ew;
    private Point ex;
    private Drawable ey;
    private int height;
    private int width;

    public C0021as(Context context, MapView mapView) {
        super(context, mapView);
    }

    /* access modifiers changed from: protected */
    public final void drawMyLocation(Canvas canvas, MapView mapView, Location location, GeoPoint geoPoint, long j) {
        if (!this.eu) {
            try {
                C0021as.super.drawMyLocation(canvas, mapView, location, geoPoint, j);
            } catch (Exception e) {
                X.e("draw myLocation bug :%s", e.toString());
                this.eu = true;
            }
        }
        if (this.eu) {
            if (this.ey == null) {
                this.ev = new Paint();
                this.ev.setAntiAlias(true);
                this.ev.setStrokeWidth(2.0f);
                this.ey = mapView.getContext().getResources().getDrawable(C0065z.drawableID("mylocation"));
                this.width = this.ey.getIntrinsicWidth();
                this.height = this.ey.getIntrinsicHeight();
                this.ew = new Point();
                this.ex = new Point();
            }
            Projection projection = mapView.getProjection();
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            float accuracy = location.getAccuracy();
            float[] fArr = new float[1];
            Location.distanceBetween(latitude, longitude, latitude, longitude + 1.0d, fArr);
            projection.toPixels(new GeoPoint((int) (latitude * 1000000.0d), (int) ((longitude - ((double) (accuracy / fArr[0]))) * 1000000.0d)), this.ex);
            projection.toPixels(geoPoint, this.ew);
            int i = this.ew.x - this.ex.x;
            this.ev.setColor(-10066177);
            this.ev.setStyle(Paint.Style.STROKE);
            canvas.drawCircle((float) this.ew.x, (float) this.ew.y, (float) i, this.ev);
            this.ev.setColor(409364223);
            this.ev.setStyle(Paint.Style.FILL);
            canvas.drawCircle((float) this.ew.x, (float) this.ew.y, (float) i, this.ev);
            this.ey.setBounds(this.ew.x - (this.width / 2), this.ew.y - (this.height / 2), this.ew.x + (this.width / 2), this.ew.y + (this.height / 2));
            this.ey.draw(canvas);
        }
    }
}
