package com.papaya.si;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.papaya.chat.ChatActivity;
import com.papaya.view.Action;
import com.papaya.view.CustomDialog;
import com.papaya.view.PausableAdapter;
import java.util.ArrayList;
import java.util.List;

public final class Y extends BaseExpandableListAdapter implements ExpandableListView.OnChildClickListener, aS, PausableAdapter {
    private boolean bq;
    private LayoutInflater cO;
    /* access modifiers changed from: private */
    public Activity dx;
    /* access modifiers changed from: private */
    public List<E> dy = new ArrayList();
    private View.OnClickListener dz = new Z(this);

    public Y(Activity activity) {
        this.dx = activity;
        this.cO = LayoutInflater.from(activity);
        refreshVisibleCardLists();
        C0042c.getSession().registerMonitor(this);
    }

    private void refreshVisibleCardLists() {
        List<E> cardLists = C0042c.getSession().getCardLists();
        this.dy.clear();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < cardLists.size()) {
                E e = cardLists.get(i2);
                if (e.sectionHeaderVisible()) {
                    this.dy.add(e);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void startChat(int i, int i2) {
        A session = C0042c.getSession();
        E e = this.dy.get(i);
        if (e != session.getContacts()) {
            D d = e.get(i2);
            session.getChattings().add(d);
            Intent intent = new Intent(this.dx, ChatActivity.class);
            intent.putExtra("active", session.getChattings().indexOf(d));
            session.getChattings().fireDataStateChanged();
            session.fireDataStateChanged();
            C0029b.startActivity(this.dx, intent);
        }
    }

    public final Object getChild(int i, int i2) {
        return this.dy.get(i).get(i2);
    }

    public final long getChildId(int i, int i2) {
        return (long) i2;
    }

    public final View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            View inflate = this.cO.inflate(C0065z.layoutID("friends_child"), (ViewGroup) null);
            inflate.setTag(new C0003aa(inflate));
            view2 = inflate;
        } else {
            view2 = view;
        }
        C0003aa aaVar = (C0003aa) view2.getTag();
        D d = this.dy.get(i).get(i2);
        aaVar.imageView.refreshWithCard(d);
        aaVar.titleView.setText(d.getTitle());
        aaVar.subtitleView.setText(d.getSubtitle());
        aaVar.dE.setText(d.getTimeLabel());
        if (d.cm == null || d.cm.getUnread() <= 0) {
            aaVar.dF.setBadgeValue(null);
        } else {
            aaVar.dF.setBadgeValue(String.valueOf(d.cm.getUnread()));
        }
        return view2;
    }

    public final int getChildrenCount(int i) {
        return this.dy.get(i).size();
    }

    public final Object getGroup(int i) {
        return this.dy.get(i);
    }

    public final int getGroupCount() {
        return this.dy.size();
    }

    public final long getGroupId(int i) {
        return (long) i;
    }

    public final View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        View view2;
        if (view == null) {
            view2 = this.cO.inflate(C0065z.layoutID("friends_group"), (ViewGroup) null);
            view2.setTag(new C0004ab(view2));
        } else {
            view2 = view;
        }
        C0004ab abVar = (C0004ab) view2.getTag();
        E e = this.dy.get(i);
        abVar.dG.setImageDrawable(e.getIcon());
        abVar.dH.setText(e.getLabel());
        abVar.dI.setImageState(z ? new int[]{16842920} : new int[0], false);
        if (z) {
            abVar.dJ.setVisibility(0);
            abVar.dJ.removeAllViews();
            List<Action> actions = e.getActions();
            if (actions != null) {
                for (int i2 = 0; i2 < actions.size(); i2++) {
                    Action action = actions.get(i2);
                    TextView textView = new TextView(this.dx);
                    textView.setTag(action);
                    textView.setText(action.label);
                    textView.setOnClickListener(this.dz);
                    textView.setEnabled(action.enabled);
                    textView.setFocusable(false);
                    textView.setBackgroundDrawable(C0042c.getDrawable("gray_button_bgs"));
                    textView.setTextColor(-16777216);
                    textView.setGravity(16);
                    textView.setPadding(C0036bg.rp(3), C0036bg.rp(0), C0036bg.rp(3), C0036bg.rp(0));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.rightMargin = C0036bg.rp(4);
                    abVar.dJ.addView(textView, layoutParams);
                }
            }
        } else {
            abVar.dJ.setVisibility(8);
        }
        return view2;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isChildSelectable(int i, int i2) {
        return true;
    }

    public final boolean isPaused() {
        return this.bq;
    }

    public final void notifyDataSetChanged() {
        if (!this.bq) {
            refreshVisibleCardLists();
            super.notifyDataSetChanged();
        }
    }

    public final boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
        D d = this.dy.get(i).get(i2);
        final A session = C0042c.getSession();
        if (d instanceof Q) {
            final Q q = (Q) d;
            if (q.de != null && q.state == 0 && !session.getAcceptedChatRoomRules().contains(Integer.valueOf(q.dd))) {
                new CustomDialog.Builder(this.dx).setTitle(C0042c.getString("accept_roomrule")).setMessage(q.de).setPositiveButton(C0042c.getString("accept"), new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        int indexOf = Y.this.dy.indexOf(session.getChatRooms());
                        int indexOf2 = session.getChatRooms().indexOf(q);
                        session.getAcceptedChatRoomRules().add(Integer.valueOf(q.dd));
                        Y.this.startChat(indexOf, indexOf2);
                    }
                }).setNegativeButton(C0042c.getString("btn_cancel"), (DialogInterface.OnClickListener) null).show();
                return true;
            }
        }
        startChat(i, i2);
        return true;
    }

    public final boolean onDataStateChanged(aU aUVar) {
        notifyDataSetChanged();
        return false;
    }

    public final void setPaused(boolean z) {
        this.bq = z;
        if (!z) {
            notifyDataSetChanged();
        }
    }
}
