package com.papaya.si;

import com.papaya.view.Action;
import java.util.ArrayList;

/* renamed from: com.papaya.si.ad  reason: case insensitive filesystem */
public final class C0006ad extends E<C0007ae> {
    public int dK;
    public int dL;
    public String dM;
    public String dN;
    private boolean dO = true;
    private int dP = 1;
    private ArrayList<C0007ae> dQ = new ArrayList<>();

    public C0006ad() {
        setReserveGroupHeader(true);
        this.cq = new ArrayList(3);
        setImState(1);
    }

    private Action<C0006ad> createAction(int i, String str) {
        Action<C0006ad> action = new Action<>(i, null, str);
        action.data = this;
        return action;
    }

    public final boolean add(C0007ae aeVar) {
        if (aeVar.getState() == 1) {
            this.dQ.add(aeVar);
        }
        return super.add((D) aeVar);
    }

    public final C0007ae get(int i) {
        return this.dO ? (C0007ae) this.co.get(i) : this.dQ.get(i);
    }

    public final C0007ae getIMUser(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.co.size()) {
                C0007ae aeVar = (C0007ae) this.co.get(i2);
                if (C0030ba.equal(aeVar.dM, str)) {
                    return aeVar;
                }
                i = i2 + 1;
            } else {
                C0007ae aeVar2 = new C0007ae();
                aeVar2.dM = str;
                aeVar2.dT = this;
                insertSort(aeVar2);
                return aeVar2;
            }
        }
    }

    public final int getImState() {
        return this.dP;
    }

    public final String getName() {
        return this.dM;
    }

    public final C0007ae remove(int i) {
        C0007ae aeVar = (C0007ae) super.remove(i);
        this.dQ.remove(aeVar);
        return aeVar;
    }

    public final boolean remove(C0007ae aeVar) {
        this.dQ.remove(aeVar);
        return super.remove((D) aeVar);
    }

    public final void setImState(int i) {
        this.dP = i;
        if (this.dP == 1) {
            this.co.clear();
            this.dQ.clear();
        }
        this.cq.clear();
        if (this.dP == 3) {
            this.cq.add(createAction(3, C0042c.getString("action_im_sign_out")));
        } else if (this.dP == 1 || this.dP == 4) {
            this.cq.add(createAction(2, C0042c.getString("action_im_sign_in")));
            this.cq.add(createAction(5, C0042c.getString("action_im_delete")));
        } else if (this.dP == 2) {
            Action<C0006ad> createAction = createAction(4, C0042c.getString("action_im_signing_in"));
            createAction.enabled = false;
            this.cq.add(createAction);
        }
    }

    public final int size() {
        return this.dO ? this.co.size() : this.dQ.size();
    }
}
