package com.papaya.si;

import com.papaya.social.PPYSocial;

/* renamed from: com.papaya.si.ap  reason: case insensitive filesystem */
public final class C0018ap {
    private static final C0018ap eo = new C0018ap();
    public String ep;

    public static C0018ap getInstance() {
        return eo;
    }

    public final void init(PPYSocial.Config config) {
        this.ep = config.getAndroidMapsAPIKey();
    }
}
