package com.papaya.si;

import android.content.Context;

/* renamed from: com.papaya.si.e  reason: case insensitive filesystem */
public final class C0044e {
    private C0058s K;

    private C0044e() {
    }

    public C0044e(C0058s sVar) {
        this.K = sVar;
    }

    public static void initialize(Context context) {
    }

    public static void show(Context context) {
    }

    public final void dispatchFinished() {
        this.K.handler.post(new C0051l());
    }

    public final void eventDispatched(long j) {
        this.K.aH.deleteEvent(j);
    }
}
