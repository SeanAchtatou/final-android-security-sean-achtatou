package com.papaya.si;

import android.content.Intent;
import com.adwhirl.util.AdWhirlUtil;
import com.papaya.base.EntryActivity;
import com.papaya.chat.ChatActivity;
import com.papaya.si.C0065z;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;

public final class A extends aL implements C0031bb, C0059t, C0063x {
    public static final C0012aj bK = new C0012aj(-1, C0042c.getString("me"));
    private String bL;
    private String bM;
    private int bN = -1;
    private C0013ak bO;
    private V bP;
    private U bQ;
    private T bR;
    private R bS;
    private M bT;
    private C0008af bU;
    private C0005ac bV;
    private V bW;
    private V bX;
    private List<E> bY;
    private HashSet<Integer> bZ;
    private HashSet<Integer> ca;
    private C0013ak cb;
    private List<List> cc;
    private C0037bh<C0064y> cd;

    public A() {
        new C0013ak();
        this.bO = new C0013ak();
        this.bP = new V();
        this.bQ = new U();
        this.bR = new T();
        this.bS = new R();
        this.bT = new M();
        this.bU = new C0008af();
        this.bV = new C0005ac();
        this.bW = new V();
        this.bX = new V();
        this.bY = new ArrayList();
        this.bZ = new HashSet<>(100);
        this.ca = new HashSet<>(4);
        new HashMap();
        this.cb = new C0013ak();
        this.cc = new ArrayList();
        this.cd = new C0037bh<>(4);
        C0042c.A.addConnectionDelegate(this);
    }

    private void addChatGroups(List list) {
        for (int i = 1; i < list.size(); i++) {
            List list2 = (List) C0030ba.sget(list, i);
            L l = new L();
            l.cQ = C0030ba.sgetInt(list2, 0);
            l.cR = C0030ba.sgetInt(list2, 1) == 1;
            C0030ba.sgetInt(list2, 2);
            l.name = (String) C0030ba.sget(list2, 3);
            C0030ba.sgetInt(list2, 4);
            C0030ba.sgetInt(list2, 5);
            l.description = (String) C0030ba.sget(list2, 6);
            l.cS = C0030ba.sgetInt(list2, 7);
            this.bT.add(l);
        }
        this.bT.fireDataStateChanged();
        fireDataStateChanged();
    }

    private void clearRMSData() {
        C0041bl.del("papayacontact1");
        C0041bl.del("papayaaccount1");
        C0041bl.del("papayaim1");
        C0041bl.del("papayafact");
        C0041bl.del("papayacontactorder");
        C0041bl.del("papayaregister");
    }

    private void clearSessionData() {
        this.bO.clear();
        this.bP.clear();
        this.bQ.clear();
        this.bR.clear();
        this.bV.clear();
        refreshCardLists();
        this.bN = -1;
        this.bL = null;
        bK.setUserID(0);
    }

    private void firePotpSessionUpdated(final String str, final String str2) {
        int size = this.cd.size();
        for (int i = 0; i < size; i++) {
            final C0064y yVar = this.cd.get(i);
            if (yVar != null) {
                if (yVar instanceof C0031bb) {
                    C0036bg.runInHandlerThread(new Runnable() {
                        public final void run() {
                            C0064y.this.onPotpSessionUpdated(str, str2);
                        }
                    });
                } else {
                    yVar.onPotpSessionUpdated(str, str2);
                }
            }
        }
    }

    private void loadCards() {
    }

    private boolean loadLogin() {
        return false;
    }

    private void registerCommands() {
        C0042c.A.registerCmds(this, 0, 1, 2, 3, 5, 6, 7, 8, 11, 13, 14, 15, 16, 17, 25, 26, 27, 28, 29, 34, 37, 56, 57, 58, 59, 72, 80, 90, 91, 92, 93, 200, Integer.valueOf((int) AdWhirlUtil.VERSION), 303, 305, 306, 307, 309, 310, 311, 312, 313, 314, 317, 600, 601, 10001);
    }

    private void removeChatGroup(int i) {
        L findByGroupID = this.bT.findByGroupID(i);
        if (findByGroupID != null) {
            this.bR.remove(findByGroupID);
            this.bT.remove(findByGroupID);
            this.bR.fireDataStateChanged();
            fireDataStateChanged();
        }
    }

    private void saveCards() {
    }

    public final void addIMAccount(int i, String str, String str2) {
        if (findIM(i, str) == null) {
            C0006ad adVar = new C0006ad();
            adVar.dK = i;
            adVar.dM = str;
            adVar.dN = str2;
            this.bV.add(adVar);
            refreshCardLists();
            C0042c.send(4, Integer.valueOf(i), str, str2);
            adVar.setImState(2);
            fireDataStateChanged();
            this.bV.saveToFile("papayaim1");
        }
    }

    public final void addSessionDelegate(C0064y yVar) {
        if (yVar != null) {
            this.cd.add(yVar);
        }
    }

    public final void closeIM(C0006ad adVar) {
        Iterator it = adVar.toList().iterator();
        while (it.hasNext()) {
            this.bR.remove((C0007ae) it.next());
        }
        adVar.setImState(1);
        this.bR.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final C0006ad findIM(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.bV.size()) {
                return null;
            }
            C0006ad adVar = (C0006ad) this.bV.get(i3);
            if (adVar.dL == i) {
                return adVar;
            }
            i2 = i3 + 1;
        }
    }

    public final C0006ad findIM(int i, String str) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.bV.size()) {
                return null;
            }
            C0006ad adVar = (C0006ad) this.bV.get(i3);
            if (adVar.dK == i && C0030ba.equal(adVar.dM, str)) {
                return adVar;
            }
            i2 = i3 + 1;
        }
    }

    public final HashSet<Integer> getAcceptedChatRoomRules() {
        return this.ca;
    }

    public final C0013ak getAllContacts() {
        return this.bO;
    }

    public final V getAppFriends() {
        return this.bW;
    }

    public final List<E> getCardLists() {
        return this.bY;
    }

    public final M getChatGroups() {
        return this.bT;
    }

    public final R getChatRooms() {
        return this.bS;
    }

    public final T getChattings() {
        return this.bR;
    }

    public final U getContacts() {
        return this.bQ;
    }

    public final String getDispname() {
        return this.bM;
    }

    public final V getFriends() {
        return this.bP;
    }

    public final C0005ac getIms() {
        return this.bV;
    }

    public final V getNonappFriends() {
        return this.bX;
    }

    public final HashSet<Integer> getPrivateChatWhiteList() {
        return this.bZ;
    }

    public final C0008af getPrivateChats() {
        return this.bU;
    }

    public final int getUserID() {
        return bK.getUserID();
    }

    public final C0013ak getWaitAdds() {
        return this.cb;
    }

    public final void handleServerResponse(Vector<Object> vector) {
        L l;
        Q findByRoomID;
        C0009ag agVar;
        S s;
        D iMUser;
        int sgetInt = C0030ba.sgetInt(vector, 0);
        switch (sgetInt) {
            case 0:
            case 1:
            case 2:
            case 17:
            case 57:
            case 200:
            case 601:
            case 10001:
                return;
            case 3:
                String str = (String) C0030ba.sget(vector, 2);
                C0012aj ajVar = null;
                if (!(vector.get(1) instanceof String) && (vector.get(1) instanceof Integer)) {
                    ajVar = this.bP.findByUserID(C0030ba.sgetInt(vector, 1, 0));
                }
                if (ajVar != null) {
                    ajVar.addChatMessage(str, ajVar, C0030ba.sgetInt(vector, 3, 0));
                    this.bR.add(ajVar);
                    this.bR.fireDataStateChanged();
                    ajVar.fireDataStateChanged();
                    return;
                }
                return;
            case 5:
                C0006ad findIM = findIM(C0030ba.sgetInt(vector, 2), (String) C0030ba.sget(vector, 3));
                if (findIM != null) {
                    findIM.dL = C0030ba.sgetInt(vector, 1);
                    if (findIM.dL > 0) {
                        findIM.setImState(3);
                    } else {
                        if (findIM.getImState() == 2) {
                            C0029b.showInfo(C0030ba.format(C0042c.getString("info_im_failed_login"), findIM.dM));
                        }
                        findIM.setImState(1);
                    }
                    fireDataStateChanged();
                    return;
                }
                return;
            case 6:
                C0006ad findIM2 = findIM(C0030ba.sgetInt(vector, 1));
                if (findIM2 != null) {
                    C0007ae iMUser2 = findIM2.getIMUser((String) vector.get(2));
                    iMUser2.dR = (String) C0030ba.sget(vector, 3);
                    iMUser2.setState(C0030ba.sgetInt(vector, 4));
                    if (vector.size() > 5) {
                        iMUser2.dS = (String) C0030ba.sget(vector, 5);
                    }
                    iMUser2.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 7:
                this.cc.add((List) vector.get(1));
                return;
            case 8:
                C0006ad findIM3 = findIM(C0030ba.sgetInt(vector, 1));
                if (findIM3 != null) {
                    C0007ae iMUser3 = findIM3.getIMUser((String) vector.get(2));
                    iMUser3.addChatMessage((String) vector.get(3), iMUser3, 0);
                    iMUser3.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 11:
                int sgetInt2 = C0030ba.sgetInt(vector, 1);
                if (sgetInt2 == -1) {
                    iMUser = this.bP.findByUserID(C0030ba.sgetInt(vector, 2));
                } else if (sgetInt2 == -2) {
                    iMUser = this.bT.findByGroupID(C0030ba.sgetInt(vector, 2));
                } else {
                    C0006ad findIM4 = findIM(sgetInt2);
                    iMUser = findIM4 != null ? findIM4.getIMUser((String) vector.get(2)) : null;
                }
                if (iMUser != null) {
                    iMUser.addSelfMessage((String) vector.get(3));
                    iMUser.fireDataStateChanged();
                    return;
                }
                return;
            case 13:
                for (int i = 0; i < vector.size(); i++) {
                    C0012aj findByUserID = this.bP.findByUserID(C0030ba.sgetInt(vector, i));
                    if (findByUserID != null) {
                        findByUserID.updateOnline(true);
                        this.bP.remove(findByUserID);
                        this.bP.insertSort(findByUserID);
                        findByUserID.fireDataStateChanged();
                    }
                }
                this.bP.fireDataStateChanged();
                fireDataStateChanged();
                return;
            case 14:
                for (int i2 = 0; i2 < vector.size(); i2++) {
                    C0012aj findByUserID2 = this.bP.findByUserID(C0030ba.sgetInt(vector, i2));
                    if (findByUserID2 != null) {
                        findByUserID2.updateOnline(false);
                        this.bP.remove(findByUserID2);
                        this.bP.insertSort(findByUserID2);
                        findByUserID2.fireDataStateChanged();
                    }
                }
                this.bP.fireDataStateChanged();
                fireDataStateChanged();
                return;
            case 15:
                C0012aj findByUserID3 = this.bP.findByUserID(C0030ba.sgetInt(vector, 1));
                if (findByUserID3 != null) {
                    findByUserID3.setMiniblog((String) vector.get(3));
                    findByUserID3.setMiniblogTime(System.currentTimeMillis() / 1000);
                    this.bP.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 16:
                List list = (List) C0030ba.sget(vector, 1);
                for (int i3 = 0; i3 < list.size(); i3 += 4) {
                    C0012aj findByUserID4 = this.bP.findByUserID(C0030ba.sgetInt(list, i3));
                    if (findByUserID4 != null) {
                        findByUserID4.setMiniblog((String) list.get(i3 + 2));
                        findByUserID4.setMiniblogTime((System.currentTimeMillis() / 1000) - ((long) C0030ba.sgetInt(list, i3 + 3)));
                    }
                    this.bP.fireDataStateChanged();
                    fireDataStateChanged();
                }
                this.bP.fireDataStateChanged();
                return;
            case 25:
                Q findByRoomID2 = this.bS.findByRoomID(C0030ba.sgetInt(vector, 1));
                if (findByRoomID2 != null) {
                    List list2 = (List) C0030ba.sget(vector, 2);
                    String str2 = (String) C0030ba.sget(list2, 0);
                    int sgetInt3 = C0030ba.sgetInt(list2, 1);
                    String str3 = (String) C0030ba.sget(list2, 3);
                    if ("login".equals(str2)) {
                        S s2 = new S();
                        s2.cU = sgetInt3;
                        s2.dk = C0030ba.sgetInt(list2, 4);
                        s2.dl = str3;
                        s2.dm = this.bP.isFriend(sgetInt3);
                        C0030ba.sgetInt(list2, 5);
                        findByRoomID2.addUser(s2);
                        findByRoomID2.addSystemMessage(C0030ba.format(C0042c.getString("chat_msg_sys_enter"), str3));
                    } else if ("logout".equals(str2)) {
                        findByRoomID2.removeUser(sgetInt3);
                        findByRoomID2.addSystemMessage(C0030ba.format(C0042c.getString("chat_msg_sys_left"), str3));
                    } else {
                        X.e("unknown chatroom action: %s", str2);
                    }
                    findByRoomID2.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 26:
                int sgetInt4 = C0030ba.sgetInt(vector, 1);
                int sgetInt5 = C0030ba.sgetInt(vector, 2);
                Q findByRoomID3 = this.bS.findByRoomID(sgetInt4);
                if (findByRoomID3 != null) {
                    if (sgetInt5 < 0) {
                        findByRoomID3.addSystemMessage((String) C0030ba.sget(vector, 3));
                    } else {
                        S findUser = findByRoomID3.findUser(sgetInt5);
                        if (findUser == null) {
                            S s3 = new S();
                            s3.cU = sgetInt5;
                            s3.dl = (String) C0030ba.sget(vector, 3);
                            s = s3;
                        } else {
                            s = findUser;
                        }
                        findByRoomID3.addChatMessage((String) C0030ba.sget(vector, 4), s, C0030ba.sgetInt(vector, 5, 0));
                    }
                    findByRoomID3.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 27:
                Q findByRoomID4 = this.bS.findByRoomID(C0030ba.sgetInt(vector, 1));
                if (findByRoomID4 != null) {
                    findByRoomID4.dj.clear();
                    findByRoomID4.setState(1);
                    List list3 = (List) C0030ba.sget(vector, 2);
                    for (int i4 = 0; i4 < list3.size(); i4++) {
                        List list4 = (List) list3.get(i4);
                        S s4 = new S();
                        s4.cU = C0030ba.sgetInt(list4, 0);
                        s4.dk = C0030ba.sgetInt(list4, 4);
                        s4.dm = this.bP.isFriend(s4.cU);
                        s4.dl = (String) C0030ba.sget(list4, 2);
                        C0030ba.sgetInt(list4, 5);
                        findByRoomID4.addUser(s4);
                    }
                    findByRoomID4.addSystemMessage(C0042c.getString("chat_msg_sys_you_joined") + findByRoomID4.name);
                    findByRoomID4.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 28:
                for (int i5 = 1; i5 < vector.size(); i5++) {
                    C0012aj findByUserID5 = this.bP.findByUserID(C0030ba.intValue(vector.get(i5)));
                    if (findByUserID5 != null) {
                        findByUserID5.setState(2);
                    }
                }
                fireDataStateChanged();
                return;
            case 29:
                for (int i6 = 1; i6 < vector.size(); i6++) {
                    C0012aj findByUserID6 = this.bP.findByUserID(C0030ba.intValue(vector.get(i6)));
                    if (findByUserID6 != null) {
                        findByUserID6.setState(1);
                    }
                }
                fireDataStateChanged();
                return;
            case 34:
                Q findByRoomID5 = this.bS.findByRoomID(C0030ba.sgetInt(vector, 1));
                if (findByRoomID5 != null) {
                    findByRoomID5.logout();
                    findByRoomID5.addSystemMessage((String) C0030ba.sget(vector, 3));
                    findByRoomID5.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 37:
                int sgetInt6 = C0030ba.sgetInt(vector, 1);
                int sgetInt7 = C0030ba.sgetInt(vector, 2);
                String str4 = (String) C0030ba.sget(vector, 3);
                List list5 = (List) C0030ba.sget(vector, 4);
                if (!this.bZ.contains(Integer.valueOf(sgetInt7))) {
                    C0042c.send(62, Integer.valueOf(sgetInt6));
                    return;
                }
                C0009ag findBySID = this.bU.findBySID(sgetInt6);
                if (findBySID == null) {
                    C0009ag agVar2 = new C0009ag();
                    agVar2.dk = sgetInt6;
                    agVar2.cU = sgetInt7;
                    agVar2.dl = str4;
                    this.bR.add(agVar2);
                    this.bU.add(agVar2);
                    this.bR.fireDataStateChanged();
                    fireDataStateChanged();
                    agVar = agVar2;
                } else {
                    agVar = findBySID;
                }
                if (agVar != null) {
                    int sgetInt8 = C0030ba.sgetInt(list5, 0);
                    String str5 = (String) C0030ba.sget(list5, 1);
                    if (sgetInt8 == -1) {
                        agVar.addChatMessage(str5, agVar, C0030ba.sgetInt(vector, 5));
                    } else {
                        agVar.addSystemMessage(str5);
                    }
                    agVar.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 56:
                List list6 = (List) C0030ba.sget(vector, 1);
                int sgetInt9 = C0030ba.sgetInt(list6, 0);
                String str6 = (String) C0030ba.sget(list6, 1);
                int sgetInt10 = C0030ba.sgetInt(list6, 2);
                int sgetInt11 = C0030ba.sgetInt(vector, 3);
                Q findByRoomID6 = this.bS.findByRoomID(sgetInt9);
                if (findByRoomID6 != null) {
                    if (sgetInt10 > 0) {
                        findByRoomID6.removeUser(sgetInt11);
                        findByRoomID6.addSystemMessage(str6 + C0042c.getString("base_kickout"));
                    } else {
                        findByRoomID6.addSystemMessage(C0042c.getString("chat_msg_sys_fail_kick") + str6);
                    }
                    findByRoomID6.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 58:
                C0012aj findByUserID7 = this.bP.findByUserID(C0030ba.intValue(vector.get(1)));
                if (findByUserID7 != null) {
                    findByUserID7.addSystemMessage((String) vector.get(2));
                    findByUserID7.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 59:
                Q findByRoomID7 = this.bS.findByRoomID(C0030ba.sgetInt(vector, 1));
                if (findByRoomID7 != null) {
                    findByRoomID7.logout();
                    findByRoomID7.fireDataStateChanged();
                    findByRoomID7.addSystemMessage(C0042c.getString("base_meout"));
                    fireDataStateChanged();
                    return;
                }
                return;
            case 72:
                for (int i7 = 1; i7 < vector.size(); i7++) {
                    C0012aj findByUserID8 = this.bP.findByUserID(C0030ba.intValue(vector.get(i7)));
                    if (findByUserID8 != null) {
                        findByUserID8.setState(3);
                    }
                }
                fireDataStateChanged();
                return;
            case 80:
                List list7 = (List) vector.get(1);
                for (int i8 = 0; i8 < list7.size(); i8 += 3) {
                    this.bP.updateMiniblog(C0030ba.intValue(list7.get(i8)), (String) list7.get(i8 + 1), C0030ba.intValue(list7.get(i8 + 2)));
                }
                fireDataStateChanged();
                return;
            case 90:
                this.bS.clear();
                List list8 = (List) vector.get(1);
                for (int i9 = 0; i9 < list8.size(); i9++) {
                    List list9 = (List) C0030ba.sget(list8, i9);
                    Q q = new Q();
                    q.dd = C0030ba.sgetInt(list9, 0);
                    q.name = (String) C0030ba.sget(list9, 1);
                    q.df = C0030ba.sgetInt(list9, 2);
                    q.dg = C0030ba.sgetInt(list9, 3);
                    C0030ba.sgetInt(list9, 4);
                    q.dh = C0030ba.sgetInt(list9, 5) > 0;
                    q.de = (String) C0030ba.sget(list9, 6);
                    this.bS.add(q);
                }
                fireDataStateChanged();
                return;
            case 91:
                for (int i10 = 0; i10 < vector.size(); i10++) {
                    if (i10 % 2 == 1 && (findByRoomID = this.bS.findByRoomID(C0030ba.sgetInt(vector, i10))) != null) {
                        findByRoomID.df = C0030ba.sgetInt(vector, i10 + 1);
                        findByRoomID.fireDataStateChanged();
                    }
                }
                fireDataStateChanged();
                return;
            case 92:
                for (int i11 = 1; i11 < vector.size(); i11++) {
                    if (i11 % 2 == 1) {
                        int sgetInt12 = C0030ba.sgetInt(vector, 1);
                        String str7 = (String) C0030ba.sget(vector, i11 + 1);
                        Q findByRoomID8 = this.bS.findByRoomID(sgetInt12);
                        if (findByRoomID8 != null) {
                            findByRoomID8.de = str7;
                            findByRoomID8.fireDataStateChanged();
                        }
                    }
                }
                return;
            case 93:
                C0030ba.sgetInt(vector, 1);
                C0030ba.stringValue(C0030ba.sget(vector, 2));
                return;
            case AdWhirlUtil.VERSION:
                this.bT.clear();
                addChatGroups(vector);
                return;
            case 303:
                if (C0030ba.sgetInt(vector, 2) != 0) {
                    String str8 = (String) C0030ba.sget(vector, 3);
                    C0029b.showInfo(str8 == null ? C0042c.getString("alert_chatgroup_failed_leave") : str8);
                    return;
                }
                return;
            case 306:
                removeChatGroup(C0030ba.sgetInt(vector, 1));
                return;
            case 307:
                removeChatGroup(C0030ba.sgetInt(vector, 1));
                return;
            case 309:
                L findByGroupID = this.bT.findByGroupID(C0030ba.sgetInt(vector, 1));
                if (findByGroupID != null) {
                    for (int i12 = 2; i12 < vector.size(); i12++) {
                        List list10 = (List) C0030ba.sget(vector, i12);
                        N userCard = findByGroupID.getUserCard(C0030ba.sgetInt(list10, 0), (String) list10.get(1));
                        userCard.setState(1);
                        findByGroupID.addChatMessage((String) C0030ba.sget(list10, 2), userCard, C0030ba.sgetInt(list10, 3));
                    }
                    findByGroupID.fireDataStateChanged();
                    fireDataStateChanged();
                    return;
                }
                return;
            case 312:
                for (int i13 = 1; i13 < vector.size(); i13++) {
                    List list11 = (List) C0030ba.sget(vector, i13);
                    L findByGroupID2 = this.bT.findByGroupID(C0030ba.sgetInt(list11, 0));
                    if (findByGroupID2 == null) {
                        L l2 = new L();
                        this.bT.add(l2);
                        l = l2;
                    } else {
                        l = findByGroupID2;
                    }
                    l.cR = C0030ba.sgetInt(list11, 1) == 1;
                    C0030ba.sgetInt(list11, 2);
                    l.name = (String) C0030ba.sget(list11, 3);
                    C0030ba.sgetInt(list11, 4);
                    C0030ba.sgetInt(list11, 5);
                    l.description = (String) C0030ba.sget(list11, 6);
                    l.cS = C0030ba.sgetInt(list11, 7);
                    l.fireDataStateChanged();
                }
                fireDataStateChanged();
                return;
            case 313:
                addChatGroups(vector);
                return;
            case 314:
                removeChatGroup(C0030ba.sgetInt(vector, 1));
                return;
            case 317:
                L findByGroupID3 = this.bT.findByGroupID(C0030ba.sgetInt(vector, 1));
                if (findByGroupID3 != null) {
                    findByGroupID3.addSystemMessage((String) vector.get(2));
                    findByGroupID3.fireDataStateChanged();
                    return;
                }
                return;
            case 600:
                String str9 = this.bL;
                this.bL = (String) C0030ba.sget(vector, 1);
                firePotpSessionUpdated(str9, this.bL);
                bK.setState(1);
                bK.fireDataStateChanged();
                C0012aj findByUserID9 = this.bP.findByUserID(bK.getUserID());
                if (findByUserID9 != null) {
                    this.bP.remove(findByUserID9);
                    this.bP.insertSort(findByUserID9);
                }
                C0042c.send(AdWhirlUtil.VERSION, new Object[0]);
                C0042c.send(82, new Object[0]);
                C0029b.removeOverlayDialog(5);
                return;
            default:
                X.e("Unknown cmd: " + sgetInt, new Object[0]);
                return;
        }
    }

    public final void increaseRevision() {
        this.bN++;
    }

    public final void initialize() {
        registerCommands();
        refreshCardLists();
    }

    public final void insertUserCard(C0012aj ajVar) {
        if (ajVar != null) {
            this.bO.insertSort(ajVar);
            if (ajVar.getUserID() != 0) {
                this.bP.insertSort(ajVar);
            } else {
                this.bQ.insertSort(ajVar);
            }
        }
    }

    public final boolean isLoggedIn() {
        return !C0030ba.isEmpty(this.bL);
    }

    public final synchronized void loadCoreData() {
        loadLogin();
    }

    public final void loadMiscData() {
        try {
            this.bV.loadFromFile("papayaim1");
            refreshCardLists();
            fireDataStateChanged();
        } catch (Exception e) {
            X.e(e, "Failed in loadMiscData", new Object[0]);
        }
    }

    public final void login() {
        if (C0042c.A.isRunning() && aC.getInstance().isConnected()) {
            C0029b.showOverlayDialog(5);
            C0042c.send(600, 9, 144, C0035bf.ANDROID_ID, aA.getInstance().getApiKey(), 0, C0061v.bm, 0, C0061v.bd, Integer.valueOf(getUserID()), aC.getInstance().getSessionKey());
        }
    }

    public final void logout() {
        if (isLoggedIn()) {
            X.i("Logging out papaya!", new Object[0]);
        }
        C0065z.a.logout();
        clearRMSData();
        clearSessionData();
    }

    public final void onConnectionEstablished() {
    }

    public final void onConnectionLost() {
        this.bR.clear();
        for (int i = 0; i < this.bV.size(); i++) {
            ((C0006ad) this.bV.get(i)).setImState(1);
        }
        bK.setState(0);
        this.bT.clear();
        this.bS.setCardStates(0);
        this.bP.setCardStates(0);
        EntryActivity.a.refreshUnread();
        this.bR.fireDataStateChanged();
        bK.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final void quit() {
        if (isLoggedIn()) {
            saveData();
        }
        if (C0042c.A != null) {
            C0042c.A.removeConnectionDelegate(this);
        }
    }

    public final void refreshCardLists() {
        this.bY.clear();
        this.bY.add(this.bR);
        this.bY.addAll(this.bV);
        this.bY.add(this.bT);
        this.bY.add(this.bS);
        this.bY.add(this.bP);
    }

    public final void removeSessionDelegate(C0064y yVar) {
        if (yVar != null) {
            this.cd.remove(yVar);
        }
    }

    public final void removeUserCard(C0012aj ajVar) {
        if (ajVar != null) {
            this.bO.remove(ajVar);
            if (ajVar.getUserID() != 0) {
                this.bP.remove(ajVar);
            } else {
                this.bQ.remove(ajVar);
            }
        }
    }

    public final void reupdateFriendList(JSONArray jSONArray) {
        this.bP.clear();
        this.bW.clear();
        this.bX.clear();
        this.bP.addUserFromJSON(jSONArray.optJSONArray(1), 0);
        this.bW.addUserFromJSON(jSONArray.optJSONArray(1), 0);
        this.bP.addUserFromJSON(jSONArray.optJSONArray(2), 0);
        this.bX.addUserFromJSON(jSONArray.optJSONArray(2), 0);
        this.bP.fireDataStateChanged();
        fireDataStateChanged();
    }

    public final void saveData() {
        if (isLoggedIn()) {
            this.bV.saveToFile("papayaim1");
        }
    }

    public final void saveGameGuide() {
    }

    public final void saveLogin() {
    }

    public final void sendChatMessage(D d, String str) {
        if (d instanceof C0012aj) {
            C0042c.send(6, -1, Integer.valueOf(((C0012aj) d).getUserID()), str);
        } else if (d instanceof C0007ae) {
            C0042c.send(6, Integer.valueOf(((C0007ae) d).dT.dL), ((C0007ae) d).dM, str);
        } else if (d instanceof Q) {
            C0042c.send(22, Integer.valueOf(((Q) d).dd), str);
        } else if (d instanceof C0009ag) {
            C0042c.send(32, Integer.valueOf(((C0009ag) d).dk), str);
        } else if (d instanceof L) {
            C0042c.send(308, Integer.valueOf(((L) d).cQ), str);
        } else {
            X.e("Unknown card to send text: " + d, new Object[0]);
        }
    }

    public final void sendPhoto(D d, byte[] bArr, String str) {
        boolean z;
        if (d instanceof C0012aj) {
            C0042c.send(7, -1, Integer.valueOf(((C0012aj) d).getUserID()), bArr, str);
            z = true;
        } else if (d instanceof C0007ae) {
            C0042c.send(7, Integer.valueOf(((C0007ae) d).dT.dL), ((C0007ae) d).dM, bArr, str);
            z = true;
        } else if (d instanceof L) {
            C0042c.send(7, -2, Integer.valueOf(((L) d).cQ), bArr, str);
            z = true;
        } else {
            z = false;
        }
        if (z) {
            d.addSystemMessage(C0042c.getString("chat_msg_sending_photo"));
            d.fireDataStateChanged();
        }
    }

    public final void setDispname(String str) {
        this.bM = str;
    }

    public final void startChat(int i) {
        C0012aj findByUserID = this.bP.findByUserID(i);
        if (findByUserID != null) {
            this.bR.add(findByUserID);
            C0029b.startActivity(new Intent(C0042c.getApplicationContext(), ChatActivity.class).putExtra("active", this.bR.indexOf(findByUserID)));
        }
    }
}
