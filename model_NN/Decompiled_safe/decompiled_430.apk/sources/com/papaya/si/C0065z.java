package com.papaya.si;

import android.app.Activity;
import android.content.Context;
import java.util.HashMap;

/* renamed from: com.papaya.si.z  reason: case insensitive filesystem */
public final class C0065z {
    private static final a bD = new a("layout");
    private static final a bE = new a("drawable");
    private static final a bF = new a("id");
    private static final a bG = new a("string");
    private static final a bH = new a("style");
    private static final a bI = new a("anim");
    /* access modifiers changed from: private */
    public static String bm;

    /* renamed from: com.papaya.si.z$a */
    public static final class a {
        private HashMap<String, Integer> bJ;
        private String type;

        private a() {
        }

        public a(String str) {
            this.bJ = new HashMap<>();
            this.type = str;
        }

        public static String getAccessToken() {
            return null;
        }

        public static String getAppId() {
            return null;
        }

        public static void initialize(Context context) {
        }

        public static boolean isSessionValid() {
            return false;
        }

        public static void login(Activity activity) {
        }

        public static void logout() {
        }

        public final int get(String str) {
            return get(str, true);
        }

        public final int get(String str, boolean z) {
            String str2 = z ? "ppy_" + str : str;
            Integer num = this.bJ.get(str2);
            if (num == null) {
                num = Integer.valueOf(C0042c.getApplicationContext().getResources().getIdentifier(str2, this.type, C0065z.bm));
                if (num.intValue() == 0) {
                    X.e("id of %s for %s is 0", this.type, str2);
                }
                this.bJ.put(str2, num);
            }
            return num.intValue();
        }
    }

    public static int animID(String str) {
        try {
            return bI.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get anim id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int drawableID(String str) {
        try {
            return bE.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get drawable id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int drawableID(String str, boolean z) {
        try {
            return bE.get(str, z);
        } catch (Exception e) {
            X.e(e, "Failed to get drawable id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int id(String str) {
        try {
            return bF.get(str, false);
        } catch (Exception e) {
            X.e(e, "Failed to get id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int layoutID(String str) {
        try {
            return bD.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get layout id: " + str, new Object[0]);
            return 0;
        }
    }

    public static void setup(Context context) {
        bm = context.getPackageName();
    }

    public static int stringID(String str) {
        try {
            return bG.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get string id: " + str, new Object[0]);
            return 0;
        }
    }

    public static int styleID(String str) {
        try {
            return bH.get(str, true);
        } catch (Exception e) {
            X.e(e, "Failed to get style id: " + str, new Object[0]);
            return 0;
        }
    }
}
