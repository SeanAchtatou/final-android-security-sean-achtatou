package com.celliecraze.rotateroll;

import android.app.Application;
import com.papaya.social.PPYSNSRegion;
import com.papaya.social.PPYSocial;

public class RRApplication extends Application {
    public void onCreate() {
        super.onCreate();
        PPYSocial.initWithConfig(this, new PPYSocial.Config() {
            public String getApiKey() {
                return "8NWomRmAXqXu1261";
            }

            public String getChinaApiKey() {
                return null;
            }

            public PPYSNSRegion getSNSRegion() {
                return PPYSNSRegion.GLOBAL;
            }

            public String getPreferredLanguage() {
                return PPYSocial.LANG_AUTO;
            }

            public int timeToShowRegistration() {
                return 1;
            }
        });
    }
}
