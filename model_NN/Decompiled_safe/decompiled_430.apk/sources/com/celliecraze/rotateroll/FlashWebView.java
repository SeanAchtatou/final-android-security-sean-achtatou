package com.celliecraze.rotateroll;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.papaya.social.PPYSocial;
import com.vdopia.client.android.VDO;
import com.vdopia.client.android.VDOActivity;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FlashWebView extends Activity implements AdListener {
    public static final int MENU_EXIT = 4;
    public static final int MENU_HAPTIC_OFF = 3;
    public static final int MENU_HAPTIC_ON = 2;
    public static final int MENU_HEYZAP = 0;
    public static final int MENU_KEYBOARD = 1;
    public static final int MENU_PAPAYA = 6;
    public static final int MENU_PAPAYA_INVITE = 7;
    public static final int MENU_SHARE = 5;
    public static boolean hapticPref = true;
    private static char[] hextable = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    static SharedPreferences prefs;
    public int admobRefresh = 0;
    public Bitmap[] bitmap = new Bitmap[4];
    public Button[] button = new Button[10];
    public int buttonLayout;
    public ButtonsView buttonsView;
    public int desiredHeight = 480;
    public int desiredWidth = 800;
    public int dispatchedTouchCount = 0;
    public boolean dispatchedUp = false;
    public FlashWebView flashWebView;
    public int leftPointer = -1;
    public int moveCount = 0;
    public int numberOfButtons;
    public boolean onTouchMove;
    public int rapidCount;
    public boolean rapidFire;
    public int rightPointer = -1;
    public int screenHeight;
    public int screenWidth;
    public boolean touchDown = false;
    public String urlToSWFHigh;
    public String urlToSWFLow;
    public String urlToSWFMed;
    public Vibrator v;
    public int vibrateTime;
    public WebView webview;
    public int webviewPointer = -1;

    public void initializeGame() {
        this.urlToSWFLow = "file:///android_asset/rrlow.html";
        this.urlToSWFMed = "file:///android_asset/rrmed.html";
        this.urlToSWFHigh = "file:///android_asset/rrhigh.html";
        this.admobRefresh = 30;
        this.buttonLayout = 6;
        this.button[0] = new Button(21, "<");
        this.button[1] = new Button(62, "R");
        this.button[2] = new Button(22, ">");
        this.button[3] = new Button(29, "A");
        this.button[4] = new Button(29, "A");
        this.button[5] = new Button(29, "A");
        this.button[0].setOffset(0, 0);
        this.button[1].setOffset(0, 0);
        this.button[2].setOffset(0, 0);
        this.button[3].setOffset(0, 0);
        this.button[4].setOffset(0, 0);
        this.button[5].setOffset(0, 0);
        this.button[0].setSecondary(0);
        this.button[1].setSecondary(0);
        this.button[2].setSecondary(0);
        this.button[3].setSecondary(0);
        this.button[4].setSecondary(0);
        this.button[5].setSecondary(0);
        this.rapidFire = false;
        this.rapidCount = 8;
        this.onTouchMove = false;
        this.vibrateTime = 30;
        if (this.onTouchMove) {
            this.rapidFire = false;
        }
    }

    public static synchronized boolean getHapticPref() {
        boolean z;
        synchronized (FlashWebView.class) {
            z = hapticPref;
        }
        return z;
    }

    public static synchronized void setHapticPref(boolean haptic) {
        synchronized (FlashWebView.class) {
            hapticPref = haptic;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        String urlToAdobe = "com.celliecraze.rotateroll";
        try {
            urlToAdobe = FlashWebPlugin.decrypt("poiahgfioh4567457aj><!@#$&%^(%)()%(sogija[oisdjf[o55661234iasjdf", "8974CDDB3090BA10304D97C6F0E5D6E0C81E0A81E2E6E660BB14B1CA262695C8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!getPackageName().equals(urlToAdobe)) {
            this.urlToSWFLow = null;
            this.urlToSWFMed = null;
            this.urlToSWFHigh = null;
            System.exit(0);
        }
        super.onCreate(savedInstanceState);
        this.flashWebView = this;
        File file = new File("/data/data/com.adobe.flashplayer/lib/libflashplayer.so");
        File file2 = new File("/system/lib/plugins/com.adobe.flashplayer/libflashplayer.so");
        boolean z2 = !file.exists();
        if (file2.exists()) {
            z = false;
        } else {
            z = true;
        }
        if (z2 && z) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_name);
            builder.setIcon(R.drawable.dialog);
            builder.setMessage(getText(R.string.no_flash));
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        Toast.makeText(FlashWebView.this.getApplicationContext(), (int) R.string.rate_s, 1).show();
                        Intent i = new Intent("android.intent.action.VIEW");
                        i.setData(Uri.parse("market://details?id=com.adobe.flashplayer"));
                        FlashWebView.this.startActivity(i);
                        System.exit(0);
                    } catch (Exception e) {
                        Toast.makeText(FlashWebView.this.getApplicationContext(), (int) R.string.market_missing, 1).show();
                    }
                }
            });
            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
        setVolumeControlStream(3);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        setHapticPref(prefs.getBoolean("setHapticPref", true));
        AppRater.app_launched(this);
        if (Build.VERSION.SDK_INT >= 9) {
            setRequestedOrientation(6);
        } else {
            setRequestedOrientation(0);
        }
        Display display = getWindowManager().getDefaultDisplay();
        this.screenWidth = display.getWidth();
        this.screenHeight = display.getHeight();
        if (this.screenWidth < this.screenHeight) {
            this.screenHeight = display.getWidth();
            this.screenWidth = display.getHeight();
        }
        this.v = (Vibrator) getSystemService("vibrator");
        initializeGame();
        LinearLayout linearLayout = new LinearLayout(this);
        AdWhirlManager.setConfigExpireTimeout(300000);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "b01e4ddb8ed4441fbe972eedcdf9a994");
        float density = getResources().getDisplayMetrics().density;
        adWhirlLayout.setMaxWidth((int) (((float) 320) * density));
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
        linearLayout.addView(adWhirlLayout, layoutParams);
        linearLayout.invalidate();
        int adOffset = 0;
        if (this.buttonLayout == 6) {
            adOffset = (int) ((36.0f / ((float) this.desiredWidth)) * ((float) this.screenWidth));
        }
        if (this.screenWidth == 854) {
            linearLayout.setPadding((((int) (((float) this.screenWidth) - ((9.0f / ((float) this.desiredWidth)) * ((float) this.screenWidth)))) / 5) + 15 + adOffset, ((this.screenHeight / 6) * 5) + 2, 0, 0);
        } else {
            linearLayout.setPadding(((((int) (((float) this.screenWidth) - ((9.0f / ((float) this.desiredWidth)) * ((float) this.screenWidth)))) / 5) - 3) + adOffset, ((this.screenHeight / 6) * 5) + 2, 0, 0);
        }
        setContentView(linearLayout, layoutParams);
        this.webview = new WebView(this);
        this.webview.setScrollBarStyle(1);
        this.webview.setBackgroundColor(-16777216);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.getSettings().setPluginsEnabled(true);
        this.webview.getSettings().setAllowFileAccess(true);
        this.webview.getSettings().setSupportZoom(false);
        this.webview.getSettings().setBuiltInZoomControls(false);
        this.webview.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        this.webview.setSoundEffectsEnabled(false);
        new AlertDialog.Builder(this);
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
        builder2.setTitle(R.string.app_name);
        builder2.setIcon(R.drawable.dialog);
        builder2.setMessage(R.string.flash_quality);
        builder2.setPositiveButton(R.string.low, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                FlashWebView.this.webview.loadUrl(FlashWebView.this.urlToSWFLow);
                final Dialog mPPYbuilder = new Dialog(FlashWebView.this.flashWebView, 16973841);
                mPPYbuilder.setContentView((int) R.layout.entrance);
                mPPYbuilder.setCancelable(false);
                mPPYbuilder.show();
                new Thread() {
                    public void run() {
                        try {
                            sleep(10000);
                        } catch (Exception e) {
                            Log.e("tag", e.getMessage());
                        }
                        mPPYbuilder.dismiss();
                    }
                }.start();
            }
        });
        builder2.setNeutralButton(R.string.medium, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                FlashWebView.this.webview.loadUrl(FlashWebView.this.urlToSWFMed);
                final Dialog mPPYbuilder = new Dialog(FlashWebView.this.flashWebView, 16973841);
                mPPYbuilder.setContentView((int) R.layout.entrance);
                mPPYbuilder.setCancelable(false);
                mPPYbuilder.show();
                new Thread() {
                    public void run() {
                        try {
                            sleep(10000);
                        } catch (Exception e) {
                            Log.e("tag", e.getMessage());
                        }
                        mPPYbuilder.dismiss();
                    }
                }.start();
            }
        });
        builder2.setNegativeButton(R.string.high, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                FlashWebView.this.webview.loadUrl(FlashWebView.this.urlToSWFHigh);
                final Dialog mPPYbuilder = new Dialog(FlashWebView.this.flashWebView, 16973841);
                mPPYbuilder.setContentView((int) R.layout.entrance);
                mPPYbuilder.setCancelable(false);
                mPPYbuilder.show();
                new Thread() {
                    public void run() {
                        try {
                            sleep(10000);
                        } catch (Exception e) {
                            Log.e("tag", e.getMessage());
                        }
                        mPPYbuilder.dismiss();
                    }
                }.start();
            }
        });
        AlertDialog dialog2 = builder2.create();
        dialog2.setCancelable(false);
        dialog2.show();
        this.webview.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 4 || event.getAction() != 0) {
                    return false;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(FlashWebView.this.flashWebView);
                builder.setCancelable(true);
                builder.setTitle((int) R.string.app_name);
                builder.setIcon((int) R.drawable.dialog);
                builder.setMessage("Are you sure you want to quit?");
                builder.setInverseBackgroundForced(true);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FlashWebView.this.finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
                return false;
            }
        });
        addContentView(this.webview, new LinearLayout.LayoutParams(this.screenWidth, (this.screenHeight / 6) * 5));
        addContentView(new LoadingFlashGame(this), new LinearLayout.LayoutParams(this.screenWidth, (this.screenHeight / 6) * 5));
        this.webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent ev) {
                MotionEvent ev2 = ev;
                switch (ev.getAction() & 255) {
                    case 0:
                        if (!FlashWebView.this.touchDown) {
                            FlashWebView.this.touchDown = true;
                            if (FlashWebView.this.onTouchMove && ev.getY() < ((float) ((FlashWebView.this.screenHeight / 6) * 5))) {
                                ev2.setAction(2);
                                view.dispatchTouchEvent(ev2);
                                ev2.setAction(1);
                                FlashWebView.this.dispatchedUp = true;
                                view.dispatchTouchEvent(ev2);
                                ev2.setAction(0);
                                view.dispatchTouchEvent(ev2);
                            }
                        }
                        return false;
                    case 1:
                        if (FlashWebView.this.dispatchedUp) {
                            FlashWebView.this.dispatchedUp = false;
                        } else {
                            FlashWebView.this.touchDown = false;
                        }
                        return false;
                    case 2:
                        if (FlashWebView.this.rapidFire && ev.getY() < ((float) ((FlashWebView.this.screenHeight / 6) * 5))) {
                            if (FlashWebView.this.moveCount < FlashWebView.this.rapidCount) {
                                FlashWebView.this.moveCount++;
                            } else {
                                ev2.setAction(1);
                                view.dispatchTouchEvent(ev2);
                                ev2.setAction(0);
                                view.dispatchTouchEvent(ev2);
                                FlashWebView.this.moveCount = 0;
                            }
                        }
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.bitmap[0] = loadBitmap(R.drawable.button);
        this.bitmap[0] = Bitmap.createScaledBitmap(this.bitmap[0], (this.bitmap[0].getWidth() / 7) * 6, this.screenHeight / 6, true);
        this.bitmap[1] = Bitmap.createScaledBitmap(this.bitmap[0], this.bitmap[0].getWidth(), this.bitmap[0].getHeight() / 2, true);
        this.bitmap[2] = loadBitmap(R.drawable.buttonpressed);
        this.bitmap[2] = Bitmap.createScaledBitmap(this.bitmap[2], (this.bitmap[2].getWidth() / 7) * 6, this.screenHeight / 6, true);
        this.bitmap[3] = Bitmap.createScaledBitmap(this.bitmap[2], this.bitmap[2].getWidth(), this.bitmap[2].getHeight() / 2, true);
        this.button[0].setCoords(0, this.screenHeight - this.bitmap[0].getHeight());
        this.button[1].setCoords(this.bitmap[0].getWidth(), this.screenHeight - this.bitmap[0].getHeight());
        this.button[2].setCoords(this.screenWidth - (this.bitmap[0].getWidth() * 2), this.screenHeight - this.bitmap[0].getHeight());
        this.button[3].setCoords(this.screenWidth - this.bitmap[0].getWidth(), this.screenHeight - this.bitmap[0].getHeight());
        if (this.buttonLayout == 1) {
            this.numberOfButtons = 1;
        } else if (this.buttonLayout == 2) {
            this.numberOfButtons = 4;
        } else if (this.buttonLayout == 3) {
            this.button[3].setHalfSized(1);
            this.button[4].setCoords(this.screenWidth - this.bitmap[0].getWidth(), this.screenHeight - (this.bitmap[0].getHeight() / 2));
            this.button[4].setHalfSized(1);
            this.numberOfButtons = 5;
        } else if (this.buttonLayout == 4) {
            this.button[2].setHalfSized(1);
            this.button[3].setHalfSized(1);
            this.button[4].setCoords(this.screenWidth - (this.bitmap[0].getWidth() * 2), this.screenHeight - (this.bitmap[0].getHeight() / 2));
            this.button[4].setHalfSized(1);
            this.button[5].setCoords(this.screenWidth - this.bitmap[0].getWidth(), this.screenHeight - (this.bitmap[0].getHeight() / 2));
            this.button[5].setHalfSized(1);
            this.numberOfButtons = 6;
        } else if (this.buttonLayout == 5) {
            this.button[1].setCoords(this.screenWidth - this.bitmap[0].getWidth(), this.screenHeight - this.bitmap[0].getHeight());
            this.numberOfButtons = 2;
        } else if (this.buttonLayout == 6) {
            this.button[2].setCoords(this.screenWidth - this.bitmap[0].getWidth(), this.screenHeight - this.bitmap[0].getHeight());
            this.numberOfButtons = 3;
        }
        if (this.buttonLayout != 0) {
            this.buttonsView = new ButtonsView(this);
            addContentView(this.buttonsView, layoutParams);
            this.buttonsView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent ev) {
                    boolean _return = false;
                    switch (ev.getAction() & 255) {
                        case 0:
                            float x = ev.getX();
                            float y = ev.getY();
                            for (int i = 0; i < FlashWebView.this.numberOfButtons; i++) {
                                if (x > ((float) FlashWebView.this.button[i].getX()) && x < ((float) (FlashWebView.this.button[i].getX() + FlashWebView.this.bitmap[FlashWebView.this.button[i].getHalfSized()].getWidth())) && y > ((float) FlashWebView.this.button[i].getY()) && y < ((float) (FlashWebView.this.button[i].getY() + FlashWebView.this.bitmap[FlashWebView.this.button[i].getHalfSized()].getHeight())) && !FlashWebView.this.button[i].getPressed()) {
                                    FlashWebView.this.button[i].setPressed(true);
                                    FlashWebView.this.button[i].setPointer(0);
                                    _return = true;
                                }
                            }
                            return _return;
                        case 1:
                            for (int i2 = 0; i2 < FlashWebView.this.numberOfButtons; i2++) {
                                if (FlashWebView.this.button[i2].getPressed()) {
                                    FlashWebView.this.button[i2].setPressed(false);
                                    FlashWebView.this.button[i2].setPointer(-1);
                                    _return = true;
                                }
                            }
                            return _return;
                        case 2:
                            for (int i3 = 0; i3 < FlashWebView.this.numberOfButtons; i3++) {
                                if (FlashWebView.this.button[i3].getPointer() != -1) {
                                    int thePointer = FlashWebView.this.button[i3].getPointer();
                                    float x2 = ev.getX(FlashWebView.this.button[i3].getPointer());
                                    float y2 = ev.getY(FlashWebView.this.button[i3].getPointer());
                                    for (int i22 = 0; i22 < FlashWebView.this.numberOfButtons; i22++) {
                                        if (x2 > ((float) FlashWebView.this.button[i22].getX()) && x2 < ((float) (FlashWebView.this.button[i22].getX() + FlashWebView.this.bitmap[FlashWebView.this.button[i22].getHalfSized()].getWidth())) && y2 > ((float) FlashWebView.this.button[i22].getY()) && y2 < ((float) (FlashWebView.this.button[i22].getY() + FlashWebView.this.bitmap[FlashWebView.this.button[i22].getHalfSized()].getHeight())) && !FlashWebView.this.button[i22].getPressed()) {
                                            FlashWebView.this.button[i3].setPressed(false);
                                            FlashWebView.this.button[i3].setPointer(-1);
                                            FlashWebView.this.button[i22].setPressed(true);
                                            FlashWebView.this.button[i22].setPointer(thePointer);
                                        }
                                    }
                                }
                            }
                            if (FlashWebView.this.webviewPointer != -1) {
                                MotionEvent ev2 = ev;
                                ev2.setLocation(ev2.getX(FlashWebView.this.webviewPointer), ev2.getY(FlashWebView.this.webviewPointer));
                                FlashWebView.this.webview.dispatchTouchEvent(ev);
                            }
                            return true;
                        case 3:
                        case 4:
                        default:
                            return true;
                        case 5:
                            int pointerIndex = (ev.getAction() & 65280) >> 8;
                            Log.d("debug", "ACTION_POINTER_DOWN in key area???");
                            float x3 = ev.getX(pointerIndex);
                            float y3 = ev.getY(pointerIndex);
                            if (pointerIndex < ev.getPointerCount() - 1) {
                                for (int i4 = 0; i4 < FlashWebView.this.numberOfButtons; i4++) {
                                    if (FlashWebView.this.button[i4].getPointer() != -1) {
                                        FlashWebView.this.button[i4].setPointer(FlashWebView.this.button[i4].getPointer() + 1);
                                    }
                                }
                                if (FlashWebView.this.webviewPointer != -1) {
                                    FlashWebView.this.webviewPointer++;
                                }
                            }
                            for (int i5 = 0; i5 < FlashWebView.this.numberOfButtons; i5++) {
                                if (x3 > ((float) FlashWebView.this.button[i5].getX()) && x3 < ((float) (FlashWebView.this.button[i5].getX() + FlashWebView.this.bitmap[FlashWebView.this.button[i5].getHalfSized()].getWidth())) && y3 > ((float) FlashWebView.this.button[i5].getY()) && y3 < ((float) (FlashWebView.this.button[i5].getY() + FlashWebView.this.bitmap[FlashWebView.this.button[i5].getHalfSized()].getHeight())) && !FlashWebView.this.button[i5].getPressed()) {
                                    FlashWebView.this.button[i5].setPressed(true);
                                    FlashWebView.this.button[i5].setPointer(pointerIndex);
                                    _return = true;
                                }
                            }
                            if (y3 < ((float) ((FlashWebView.this.screenHeight / 6) * 5))) {
                                ev.setAction(0);
                                ev.setLocation(ev.getX(pointerIndex), ev.getY(pointerIndex));
                                FlashWebView.this.webview.dispatchTouchEvent(ev);
                                FlashWebView.this.webviewPointer = pointerIndex;
                                _return = true;
                            }
                            return _return;
                        case 6:
                            int pointerIndex2 = (ev.getAction() & 65280) >> 8;
                            if (FlashWebView.this.webviewPointer == pointerIndex2) {
                                Log.d("debug", "wut pointer_up");
                                ev.setAction(1);
                                ev.setLocation(ev.getX(pointerIndex2), ev.getY(pointerIndex2));
                                FlashWebView.this.webview.dispatchTouchEvent(ev);
                                FlashWebView.this.webviewPointer = -1;
                                _return = true;
                            }
                            for (int i6 = 0; i6 < FlashWebView.this.numberOfButtons; i6++) {
                                if (FlashWebView.this.button[i6].getPointer() == pointerIndex2 && FlashWebView.this.button[i6].getPressed()) {
                                    FlashWebView.this.button[i6].setPressed(false);
                                    FlashWebView.this.button[i6].setPointer(-1);
                                    _return = true;
                                }
                            }
                            if (pointerIndex2 < ev.getPointerCount() - 1) {
                                for (int i7 = 0; i7 < FlashWebView.this.numberOfButtons; i7++) {
                                    if (FlashWebView.this.button[i7].getPointer() != -1) {
                                        FlashWebView.this.button[i7].setPointer(FlashWebView.this.button[i7].getPointer() - 1);
                                    }
                                }
                                if (FlashWebView.this.webviewPointer != -1) {
                                    FlashWebView.this.webviewPointer--;
                                }
                            }
                            return _return;
                    }
                }
            });
        }
        VDO.initialize("86ef44914f92660f8d3a184c54e7ad1f", this);
        Intent next = new Intent();
        next.setClass(this, VDOActivity.class);
        next.putExtra(VDO.INTENT_EXTRA_KEY_AD_TYPE, (int) VDO.AD_TYPE_PRE_APP_VIDEO);
        startActivity(next);
    }

    public Bitmap loadBitmap(int resId) {
        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), resId);
        float widthDivider = ((float) bitmap2.getWidth()) / ((float) this.desiredWidth);
        float heightDivider = ((float) bitmap2.getHeight()) / ((float) this.desiredHeight);
        return Bitmap.createScaledBitmap(bitmap2, (int) (((float) this.screenWidth) * widthDivider), (int) (((float) this.screenHeight) * heightDivider), true);
    }

    public class ButtonsView extends View {
        public Paint paint = new Paint();
        public Paint paint2;

        public ButtonsView(Context context) {
            super(context);
            this.paint.setAlpha(255);
            this.paint.setColor(-12303292);
            this.paint.setTextSize(42.0f);
            this.paint2 = new Paint();
            this.paint2.setAlpha(255);
            this.paint2.setColor(-12303292);
            this.paint2.setTextSize(30.0f);
            if (FlashWebView.this.screenWidth < 750) {
                this.paint.setTextSize(18.0f);
                this.paint2.setTextSize(12.0f);
            }
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            for (int i = 0; i < FlashWebView.this.numberOfButtons; i++) {
                int drawPressed = 0;
                if (FlashWebView.this.button[i].getPressed()) {
                    drawPressed = 2 - FlashWebView.this.button[i].getHalfSized();
                }
                canvas.drawBitmap(FlashWebView.this.bitmap[FlashWebView.this.button[i].getHalfSized() + drawPressed], (float) FlashWebView.this.button[i].getX(), (float) FlashWebView.this.button[i].getY(), (Paint) null);
                if (FlashWebView.this.button[i].getHalfSized() != 0) {
                    canvas.drawText(FlashWebView.this.button[i].buttonText, (float) (FlashWebView.this.button[i].getX() + ((FlashWebView.this.bitmap[0].getWidth() * 3) / 8) + FlashWebView.this.button[i].getOffsetX()), ((float) FlashWebView.this.button[i].getY()) + ((((float) FlashWebView.this.bitmap[1].getHeight()) / 5.0f) * 4.0f) + ((float) FlashWebView.this.button[i].getOffsetY()), this.paint2);
                } else if (FlashWebView.this.screenWidth > 854) {
                    canvas.drawText(FlashWebView.this.button[i].buttonText, (float) (FlashWebView.this.button[i].getX() + ((FlashWebView.this.bitmap[0].getWidth() / 13) * 4) + FlashWebView.this.button[i].getOffsetX()), (float) (FlashWebView.this.button[i].getY() + ((FlashWebView.this.bitmap[0].getHeight() / 3) * 2) + FlashWebView.this.button[i].getOffsetY()), this.paint);
                } else {
                    canvas.drawText(FlashWebView.this.button[i].buttonText, (float) (FlashWebView.this.button[i].getX() + (FlashWebView.this.bitmap[0].getWidth() / 3) + FlashWebView.this.button[i].getOffsetX()), (float) (FlashWebView.this.button[i].getY() + ((FlashWebView.this.bitmap[0].getHeight() / 3) * 2) + 4 + FlashWebView.this.button[i].getOffsetY()), this.paint);
                }
            }
        }
    }

    public class LoadingFlashGame extends View {
        Bitmap loadingFlashGame;

        public LoadingFlashGame(Context context) {
            super(context);
            this.loadingFlashGame = FlashWebView.this.loadBitmap(R.drawable.loadingflashgame);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(this.loadingFlashGame, (float) ((FlashWebView.this.screenWidth / 2) - (this.loadingFlashGame.getWidth() / 2)), (float) ((FlashWebView.this.screenHeight / 2) - (this.loadingFlashGame.getHeight() / 2)), (Paint) null);
        }
    }

    public class Button {
        public String buttonText;
        public int halfSize;
        public boolean isPressed = false;
        public int keycode;
        public int offsetX;
        public int offsetY;
        public int pointer = -1;
        public int secondaryControl;
        public int x;
        public int y;

        public Button(int _keycode, String _buttonText) {
            this.keycode = _keycode;
            this.buttonText = _buttonText;
        }

        public void setPointer(int _pointer) {
            this.pointer = _pointer;
        }

        public void setSecondary(int _secondary) {
            this.secondaryControl = _secondary;
        }

        public void setPressed(boolean pressed) {
            this.isPressed = pressed;
            if (this.isPressed) {
                if (FlashWebView.hapticPref) {
                    FlashWebView.this.v.vibrate((long) FlashWebView.this.vibrateTime);
                }
                FlashWebView.this.flashWebView.dispatchKeyEvent(new KeyEvent(0, this.keycode));
                if (this.secondaryControl != 0) {
                    FlashWebView.this.flashWebView.dispatchKeyEvent(new KeyEvent(0, this.secondaryControl));
                }
                FlashWebView.this.buttonsView.invalidate(this.x, this.y, this.x + FlashWebView.this.bitmap[this.halfSize].getWidth(), this.y + FlashWebView.this.bitmap[this.halfSize].getHeight());
                return;
            }
            FlashWebView.this.flashWebView.dispatchKeyEvent(new KeyEvent(1, this.keycode));
            if (this.secondaryControl != 0) {
                FlashWebView.this.flashWebView.dispatchKeyEvent(new KeyEvent(1, this.secondaryControl));
            }
            FlashWebView.this.buttonsView.invalidate(this.x, this.y, this.x + FlashWebView.this.bitmap[this.halfSize].getWidth(), this.y + FlashWebView.this.bitmap[this.halfSize].getHeight());
        }

        public void setX(int _x) {
            this.x = _x;
        }

        public void setY(int _y) {
            this.y = _y;
        }

        public void setCoords(int _x, int _y) {
            this.x = _x;
            this.y = _y;
        }

        public void setOffset(int _x, int _y) {
            this.offsetX = (int) ((((float) _x) / ((float) FlashWebView.this.desiredWidth)) * ((float) FlashWebView.this.screenWidth));
            this.offsetY = (int) ((((float) _y) / ((float) FlashWebView.this.desiredHeight)) * ((float) FlashWebView.this.screenHeight));
        }

        public void setHalfSized(int _halfSize) {
            this.halfSize = _halfSize;
        }

        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }

        public int getHalfSized() {
            return this.halfSize;
        }

        public int getOffsetX() {
            return this.offsetX;
        }

        public int getOffsetY() {
            return this.offsetY;
        }

        public boolean getPressed() {
            return this.isPressed;
        }

        public int getPointer() {
            return this.pointer;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.webview != null) {
            this.webview.destroy();
            this.webview = null;
        }
    }

    public void onPause() {
        Log.d("FlashWebView", "onPause called");
        super.onPause();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("setHapticPref", hapticPref);
        editor.commit();
        try {
            Class.forName("android.webkit.WebView").getMethod("onPause", null).invoke(this.webview, null);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
        }
    }

    public void onResume() {
        Log.d("FlashWebView", "onResume called");
        super.onResume();
        try {
            Class.forName("android.webkit.WebView").getMethod("onResume", null).invoke(this.webview, null);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 6, 0, "Papaya Chat").setIcon((int) R.drawable.papayachat);
        menu.add(0, 7, 0, "Invite Friends").setIcon((int) R.drawable.papayachat);
        menu.add(0, 1, 0, "Toggle Keyboard").setIcon(17301660);
        if (this.buttonLayout != 0) {
            menu.add(0, 2, 0, "Toggle Haptic On").setIcon(17301578);
            menu.add(0, 3, 0, "Toggle Haptic Off").setIcon(17301578);
        }
        menu.add(0, 5, 0, "Share").setIcon(17301586);
        menu.add(0, 4, 0, "Quit Game").setIcon(17301560);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        super.onPrepareOptionsMenu(menu);
        if (this.buttonLayout != 0) {
            MenuItem findItem = menu.findItem(2);
            if (getHapticPref()) {
                z = false;
            } else {
                z = true;
            }
            findItem.setVisible(z);
            menu.findItem(3).setVisible(getHapticPref());
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                ((InputMethodManager) this.flashWebView.getSystemService("input_method")).toggleSoftInput(0, 0);
                return true;
            case 2:
                setHapticPref(true);
                return true;
            case 3:
                setHapticPref(false);
                return true;
            case 4:
                finish();
                return true;
            case 5:
                shareApp();
                return true;
            case 6:
                PPYSocial.showChat(this);
                return true;
            case 7:
                PPYSocial.recommendMyApp("Do you like Rotate and Roll? \n\n Invite friends to try it too!");
                return true;
            default:
                return false;
        }
    }

    private void shareApp() {
        String PhoneModel = Build.MODEL;
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.SUBJECT", getText(R.string.app_name));
        intent.putExtra("android.intent.extra.TEXT", getString(R.string.share_app, new Object[]{PhoneModel}));
        startActivity(Intent.createChooser(intent, getText(R.string.app_name)));
    }

    public static String byteArrayToHex(byte[] array) {
        String s = "";
        for (byte b : array) {
            int di = (b + 256) & 255;
            s = String.valueOf(s) + hextable[(di >> 4) & 15] + hextable[di & 15];
        }
        return s;
    }

    public static String digest(String s, String algorithm) {
        try {
            MessageDigest m = MessageDigest.getInstance(algorithm);
            m.update(s.getBytes(), 0, s.length());
            return byteArrayToHex(m.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return s;
        }
    }

    public static String md5(String s) {
        return digest(s, "MD5");
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
    }
}
