package com.vdopia.client.android;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

class n implements ThreadFactory {
    private final AtomicInteger a = new AtomicInteger(1);

    n() {
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "UserTask #" + this.a.getAndIncrement());
    }
}
