package com.vdopia.client.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

final class c {
    static String a = "skipas";
    static String b = "channelid";
    static String c = "maxcached";
    static AtomicInteger d = new AtomicInteger(5);
    static AtomicInteger e = new AtomicInteger(-1);
    static AtomicInteger f = new AtomicInteger(30);
    static String g = "vdo_239vhqeqf_";
    private static boolean h = false;
    private static String i = "noinit";
    private static String j = "vdo_239vhqeqf";

    enum a {
        tvi,
        tcl,
        tsk,
        tae
    }

    c() {
    }

    static String a(Object obj) {
        if (obj == null) {
            return "";
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        String name = obj.getClass().getName();
        int lastIndexOf = name.lastIndexOf(46) + 1;
        if (lastIndexOf > 0) {
            return name.substring(lastIndexOf);
        }
        return name;
    }

    static String a(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exc.printStackTrace(printWriter);
        printWriter.close();
        return stringWriter.toString();
    }

    static class b {
        private static int a = 4;

        b() {
        }

        static void a(Object obj, String str) {
            a(6, obj, str);
        }

        static void b(Object obj, String str) {
            a(5, obj, str);
        }

        static void c(Object obj, String str) {
            a(4, obj, str);
        }

        static void d(Object obj, String str) {
            a(3, obj, str);
        }

        static void e(Object obj, String str) {
            a(2, obj, str);
        }

        private static void a(int i, Object obj, String str) {
            if (i >= a) {
                Log.println(i, "VDO", String.valueOf(c.a(obj)) + ":" + str);
            }
        }
    }

    static String a(String str, Map<String, String> map) {
        String str2 = str;
        for (String next : map.keySet()) {
            str2 = str2.replaceAll(next, map.get(next));
        }
        String.valueOf(str) + " converted to " + str2;
        return str2;
    }

    static Map<String, String> a(String str, String str2, AdType adType, String str3) {
        HashMap hashMap = new HashMap();
        hashMap.put("\\[vdo_ver\\]", "2_8_A");
        hashMap.put("\\[vdo_ak\\]", str3);
        hashMap.put("\\[vdo_ci\\]", str);
        hashMap.put("\\[vdo_ai\\]", str2);
        try {
            hashMap.put("\\[vdo_dt\\]", URLEncoder.encode("A_" + Build.MODEL, "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
        }
        hashMap.put("\\[vdo_di\\]", i);
        hashMap.put("\\[vdo_at\\]", adType.toString());
        return hashMap;
    }

    private static Map<String, String> a(String str, String str2, AdType adType, String str3, Context context) {
        String str4;
        HashMap hashMap = (HashMap) a(str, str2, adType, str3);
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
            NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
            if (networkInfo.isAvailable()) {
                str4 = "wifi";
            } else if (networkInfo2.isAvailable()) {
                str4 = "carrier";
            } else {
                str4 = "unknown";
            }
        } catch (Exception e2) {
            b.d("VDOPIA", "No permission to access network state");
            str4 = "unknown";
        }
        b.d("VDOPIA", Build.VERSION.RELEASE);
        b.d("VDOPIA", "network type = " + str4);
        b.d("VDOPIA", "StartTime = " + VDO.getStartTimeSec(context));
        b.d("VDOPIA", "Duration " + ((System.currentTimeMillis() / 1000) - VDO.getStartTimeSec(context)));
        hashMap.put("\\[vdo_nt\\]", str4);
        hashMap.put("\\[vdo_st\\]", new StringBuilder().append(VDO.getStartTimeSec(context)).toString());
        hashMap.put("\\[vdo_du\\]", new StringBuilder().append((System.currentTimeMillis() / 1000) - VDO.getStartTimeSec(context)).toString());
        hashMap.put("\\[vdo_os\\]", Build.VERSION.RELEASE);
        return hashMap;
    }

    static Map<String, String> a(int i2) {
        String str = "na";
        if (i2 >= 0) {
            str = new Integer(i2).toString();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("\\[vdo_du\\]", str);
        hashMap.put("\\[vdo_ts\\]", new Long(System.currentTimeMillis() / 1000).toString());
        return hashMap;
    }

    static void a(Map<String, String> map, a aVar) {
        map.put("\\[vdo_measure\\]", aVar.toString().replaceFirst("t", ""));
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f0 A[SYNTHETIC, Splitter:B:47:0x00f0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void a(android.content.Context r9) {
        /*
            java.lang.Class<com.vdopia.client.android.c> r0 = com.vdopia.client.android.c.class
            monitor-enter(r0)
            boolean r1 = com.vdopia.client.android.c.h     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r0)
            return
        L_0x0009:
            r1 = 0
            com.vdopia.client.android.w r2 = new com.vdopia.client.android.w     // Catch:{ Throwable -> 0x0151, all -> 0x014a }
            r2.<init>(r9)     // Catch:{ Throwable -> 0x0151, all -> 0x014a }
            com.vdopia.client.android.w r1 = r2.a()     // Catch:{ Throwable -> 0x0151, all -> 0x014a }
            java.lang.String r2 = com.vdopia.client.android.c.a     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r2 = r1.a(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            if (r2 == 0) goto L_0x00a1
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.d     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r3.set(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r3 = "Using DB value for skipSecs - "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.d     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r3 = r3.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r2.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
        L_0x0038:
            java.lang.String r2 = com.vdopia.client.android.c.b     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r2 = r1.a(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            if (r2 == 0) goto L_0x00d4
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.e     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r3.set(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r3 = "Using DB value for chId - "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.e     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r3 = r3.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r2.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
        L_0x005d:
            com.vdopia.client.android.AdType[] r2 = com.vdopia.client.android.AdType.values()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r3 = r2.length     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r4 = 0
        L_0x0063:
            if (r4 < r3) goto L_0x00f4
            java.lang.String r2 = "bannersec"
            java.lang.String r2 = r1.a(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            if (r2 == 0) goto L_0x0076
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.f     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r3.set(r2)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.b()     // Catch:{ all -> 0x009e }
        L_0x007b:
            r1 = 1
            com.vdopia.client.android.c.h = r1     // Catch:{ all -> 0x009e }
            android.content.ContentResolver r1 = r9.getContentResolver()     // Catch:{ all -> 0x009e }
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.Secure.getString(r1, r2)     // Catch:{ all -> 0x009e }
            com.vdopia.client.android.c.i = r1     // Catch:{ all -> 0x009e }
            if (r1 != 0) goto L_0x0090
            java.lang.String r1 = "NO_ID_FROM_ANDROID"
            com.vdopia.client.android.c.i = r1     // Catch:{ all -> 0x009e }
        L_0x0090:
            com.vdopia.client.android.w r1 = new com.vdopia.client.android.w     // Catch:{ all -> 0x009e }
            r1.<init>(r9)     // Catch:{ all -> 0x009e }
            com.vdopia.client.android.w r1 = r1.a()     // Catch:{ all -> 0x009e }
            r1.b()     // Catch:{ all -> 0x009e }
            goto L_0x0007
        L_0x009e:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x00a1:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r3 = "Keeping default value for skipSecs - "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.d     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r3 = r3.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r2.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            goto L_0x0038
        L_0x00b6:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x00ba:
            java.lang.String r3 = "Vdopia.initialize"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x014f }
            java.lang.String r5 = "Exception while updating channel settings. "
            r4.<init>(r5)     // Catch:{ all -> 0x014f }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x014f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x014f }
            com.vdopia.client.android.c.b.c(r3, r1)     // Catch:{ all -> 0x014f }
            if (r2 == 0) goto L_0x007b
            r2.b()     // Catch:{ all -> 0x009e }
            goto L_0x007b
        L_0x00d4:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r3 = "Keeping default value for chId - "
            r2.<init>(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r3 = com.vdopia.client.android.c.d     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r3 = r3.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r2.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            goto L_0x005d
        L_0x00ea:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
        L_0x00ee:
            if (r2 == 0) goto L_0x00f3
            r2.b()     // Catch:{ all -> 0x009e }
        L_0x00f3:
            throw r1     // Catch:{ all -> 0x009e }
        L_0x00f4:
            r5 = r2[r4]     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r6 = a(r5)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r6 = r1.a(r6)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            if (r6 == 0) goto L_0x012b
            java.util.concurrent.atomic.AtomicInteger r7 = r5.a     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r7.set(r6)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r7 = "Using DB value for maxCached: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r6 = r6.append(r5)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r7 = " - "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r5 = r5.a     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r5 = r5.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r5.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
        L_0x0127:
            int r4 = r4 + 1
            goto L_0x0063
        L_0x012b:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r7 = "Keeping default value for maxCached: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r6 = r6.append(r5)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.String r7 = " - "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.util.concurrent.atomic.AtomicInteger r5 = r5.a     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            int r5 = r5.get()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            r5.toString()     // Catch:{ Throwable -> 0x00b6, all -> 0x00ea }
            goto L_0x0127
        L_0x014a:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00ee
        L_0x014f:
            r1 = move-exception
            goto L_0x00ee
        L_0x0151:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.c.a(android.content.Context):void");
    }

    static int a() {
        return f.get() * 1000;
    }

    static String a(AdType adType) {
        return String.valueOf(c) + "." + adType;
    }

    static String a(String str) {
        String str2;
        int indexOf = str.indexOf(63) + 1;
        int indexOf2 = str.indexOf("&t=");
        if (indexOf < 0 || indexOf >= str.length() || indexOf2 <= indexOf || indexOf2 >= str.length()) {
            str2 = "";
        } else {
            String str3 = String.valueOf(str.substring(indexOf, indexOf2)) + "test string to check logging";
            try {
                String a2 = a(MessageDigest.getInstance("MD5").digest(str3.getBytes()));
                try {
                    "substr:" + str3 + ". md5:" + a2 + ". str:" + str;
                    str2 = a2;
                } catch (Exception e2) {
                    str2 = a2;
                    b.c("Vdopia", "Exception while creating digest");
                    return str.replaceFirst("\\[vdo_sign\\]", str2);
                }
            } catch (Exception e3) {
                str2 = "";
            }
        }
        return str.replaceFirst("\\[vdo_sign\\]", str2);
    }

    static FileInputStream a(Context context, String str) throws FileNotFoundException {
        try {
            return new FileInputStream(new File(new File(context.getCacheDir(), j), str));
        } catch (Exception e2) {
            return null;
        }
    }

    static void b(Context context, String str) {
        try {
            new File(new File(context.getCacheDir(), j), str).delete();
        } catch (Exception e2) {
        }
    }

    static void a(Context context, String str, String str2) {
        File file = new File(context.getCacheDir(), j);
        new File(file, str).renameTo(new File(file, str2));
    }

    static FileOutputStream c(Context context, String str) throws FileNotFoundException {
        File file = new File(context.getCacheDir(), j);
        try {
            file.mkdir();
        } catch (Exception e2) {
            b.b("", "Exception while creating Vdopia cache dir: " + file);
        }
        return new FileOutputStream(new File(file, str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean d(android.content.Context r6, java.lang.String r7) {
        /*
            r3 = 0
            java.io.FileInputStream r0 = a(r6, r7)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            if (r0 == 0) goto L_0x0039
            r0 = 1
            java.lang.Throwable r1 = new java.lang.Throwable     // Catch:{ Exception -> 0x0010, Throwable -> 0x006d }
            java.lang.String r2 = "We won't check the signature for the file."
            r1.<init>(r2)     // Catch:{ Exception -> 0x0010, Throwable -> 0x006d }
            throw r1     // Catch:{ Exception -> 0x0010, Throwable -> 0x006d }
        L_0x0010:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0014:
            java.lang.String r2 = "checkAdFileValidity"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exception while checking file "
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r4 = ": "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.vdopia.client.android.c.b.c(r2, r0)
            r0 = r1
        L_0x0033:
            if (r0 != 0) goto L_0x0038
            b(r6, r7)
        L_0x0038:
            return r0
        L_0x0039:
            java.lang.String r0 = "checkAdFileValidity"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            java.lang.String r2 = "fileValid="
            r1.<init>(r2)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            r2 = 0
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            java.lang.String r2 = ". bytes = "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            r2 = 0
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            com.vdopia.client.android.c.b.d(r0, r1)     // Catch:{ Exception -> 0x0072, Throwable -> 0x005b }
            r0 = r3
            goto L_0x0033
        L_0x005b:
            r0 = move-exception
            r1 = r3
        L_0x005d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Throwable: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            r0.toString()
            r0 = r1
            goto L_0x0033
        L_0x006d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005d
        L_0x0072:
            r0 = move-exception
            r1 = r3
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.c.d(android.content.Context, java.lang.String):boolean");
    }

    private static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if ((bArr[i2] & 255) < 16) {
                stringBuffer.append("0");
            }
            stringBuffer.append(Integer.toHexString(bArr[i2] & 255));
        }
        return stringBuffer.toString();
    }

    static File b(Context context) {
        return new File(context.getCacheDir(), j);
    }

    static String a(String str, Context context) {
        return a(a("http://serve.vdopia.com/adserver/iphone.php?ver=[vdo_ver]&di=[vdo_di]&ak=[vdo_ak]&nt=[vdo_nt]&dt=[vdo_dt]&al=0.0&lo=0.0&lt=0.0&st=[vdo_st]&du=[vdo_du]&rf=1&os=[vdo_os]&t=[vdo_sign]", a("cid", "adid", AdType.preapp, str, context)));
    }
}
