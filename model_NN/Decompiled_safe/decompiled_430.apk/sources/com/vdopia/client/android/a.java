package com.vdopia.client.android;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import java.io.FileDescriptor;
import java.io.IOException;

final class a extends SurfaceView implements MediaController.MediaPlayerControl {
    private SurfaceHolder.Callback A = new p(this);
    /* access modifiers changed from: private */
    public String a = "VVidView";
    private int b;
    private Context c;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public SurfaceHolder f = null;
    /* access modifiers changed from: private */
    public MediaPlayer g = null;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener l;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public boolean q;
    /* access modifiers changed from: private */
    public boolean r;
    private Uri s;
    private FileDescriptor t;
    private MediaPlayer.OnVideoSizeChangedListener u = new u(this);
    private MediaPlayer.OnPreparedListener v = new q(this);
    private MediaPlayer.OnCompletionListener w = new r(this);
    private MediaPlayer.OnErrorListener x = new s(this);
    private MediaPlayer.OnInfoListener y = new t(this);
    private MediaPlayer.OnBufferingUpdateListener z = new o(this);

    public a(Context context) {
        super(context);
        this.c = context;
        b();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int defaultSize = getDefaultSize(this.h, i2);
        int defaultSize2 = getDefaultSize(this.i, i3);
        if (this.h > 0 && this.i > 0) {
            if (this.h * defaultSize2 > this.i * defaultSize) {
                i4 = defaultSize;
                i5 = (this.i * defaultSize) / this.h;
            } else if (this.h * defaultSize2 < this.i * defaultSize) {
                int i6 = defaultSize2;
                i4 = (this.h * defaultSize2) / this.i;
                i5 = i6;
            }
            setMeasuredDimension(i4, i5);
        }
        int i7 = defaultSize2;
        i4 = defaultSize;
        i5 = i7;
        setMeasuredDimension(i4, i5);
    }

    private void b() {
        this.h = 0;
        this.i = 0;
        getHolder().addCallback(this.A);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.d = 0;
        this.e = 0;
    }

    public final void a(Uri uri) {
        this.p = 0;
        this.s = uri;
        c();
        requestLayout();
        invalidate();
    }

    public final void a(FileDescriptor fileDescriptor) {
        this.p = 0;
        this.t = fileDescriptor;
        c();
        requestLayout();
        invalidate();
    }

    public final void a() {
        if (this.g != null) {
            this.g.stop();
            this.g.release();
            this.g = null;
            this.d = 0;
            this.e = 0;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.f == null) {
            return;
        }
        if (this.s != null || this.t != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra("command", "pause");
            this.c.sendBroadcast(intent);
            a(false);
            try {
                this.g = new MediaPlayer();
                this.g.reset();
                this.g.setOnPreparedListener(this.v);
                this.g.setOnVideoSizeChangedListener(this.u);
                this.b = -1;
                this.g.setOnCompletionListener(this.w);
                this.g.setOnErrorListener(this.x);
                this.g.setOnInfoListener(this.y);
                this.g.setOnBufferingUpdateListener(this.z);
                this.n = 0;
                if (this.s != null) {
                    this.g.setDataSource(this.c, this.s);
                } else {
                    this.g.setDataSource(this.t);
                }
                this.g.setDisplay(this.f);
                this.g.setAudioStreamType(3);
                this.g.setScreenOnWhilePlaying(true);
                this.g.prepareAsync();
                this.d = 1;
            } catch (IOException e2) {
                Log.w(this.a, "Unable to open content: u=" + this.s + ", f=" + this.t, e2);
                this.d = -1;
                this.e = -1;
                this.x.onError(this.g, 1, 0);
            } catch (IllegalArgumentException e3) {
                Log.w(this.a, "Unable to open content: u=" + this.s + ", f=" + this.t, e3);
                this.d = -1;
                this.e = -1;
                this.x.onError(this.g, 1, 0);
            }
        }
    }

    public final void a(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.m = onPreparedListener;
    }

    public final void a(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.l = onCompletionListener;
    }

    public final void a(MediaPlayer.OnErrorListener onErrorListener) {
        this.o = onErrorListener;
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (this.g != null) {
            this.g.reset();
            this.g.release();
            this.g = null;
            this.d = 0;
            if (z2) {
                this.e = 0;
            }
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onTrackballEvent(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return super.onKeyDown(i2, keyEvent);
    }

    public final void start() {
        if (d()) {
            this.g.start();
            this.d = 3;
        }
        this.e = 3;
    }

    public final void pause() {
        if (d() && this.g.isPlaying()) {
            this.g.pause();
            this.d = 4;
        }
        this.e = 4;
    }

    public final int getDuration() {
        if (!d()) {
            this.b = -1;
            return this.b;
        } else if (this.b > 0) {
            return this.b;
        } else {
            this.b = this.g.getDuration();
            return this.b;
        }
    }

    public final int getCurrentPosition() {
        if (d()) {
            return this.g.getCurrentPosition();
        }
        return 0;
    }

    public final void seekTo(int i2) {
        if (d()) {
            this.g.seekTo(i2);
            this.p = 0;
            return;
        }
        this.p = i2;
    }

    public final boolean isPlaying() {
        return d() && this.g.isPlaying();
    }

    public final int getBufferPercentage() {
        if (this.g != null) {
            return this.n;
        }
        return 0;
    }

    private boolean d() {
        return (this.g == null || this.d == -1 || this.d == 0 || this.d == 1) ? false : true;
    }

    public final boolean canPause() {
        return this.q;
    }

    public final boolean canSeekBackward() {
        return false;
    }

    public final boolean canSeekForward() {
        return this.r;
    }
}
