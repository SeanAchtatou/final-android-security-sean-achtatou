package com.vdopia.client.android;

import android.os.Process;
import com.vdopia.client.android.c;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

abstract class m<Params, Progress, Result> {
    private static final BlockingQueue<Runnable> a = new LinkedBlockingQueue(5);
    private static final ThreadFactory b = new n();
    private static final ThreadPoolExecutor c = new ThreadPoolExecutor(2, 5, 10, TimeUnit.SECONDS, a, b);
    private final a<Params, Result> d = new a<Params, Result>() {
        public final Result call() throws Exception {
            Process.setThreadPriority(10);
            return m.this.a(this.a);
        }
    };
    private final FutureTask<Result> e = new FutureTask<Result>(this, this.d) {
        /* access modifiers changed from: protected */
        public final void done() {
            try {
                get();
            } catch (InterruptedException e) {
                c.b.b(this, e.toString());
            } catch (ExecutionException e2) {
                c.b.c(this, "An error occured while executing doInBackground(). " + e2.getCause());
            } catch (CancellationException e3) {
                c.b.c(this, "Action was cancelled while executing doInBackground(). " + e3);
            } catch (Throwable th) {
                c.b.c(this, "An error occured while executing doInBackground(). " + th);
            }
        }
    };

    public abstract Result a(Object... objArr);

    public final m<Params, Progress, Result> b(Params... paramsArr) {
        try {
            this.d.a = paramsArr;
            c.execute(this.e);
        } catch (Throwable th) {
            c.b.c(this, "Exception while executing in background - " + th);
        }
        return this;
    }

    private static abstract class a<Params, Result> implements Callable<Result> {
        Params[] a;

        /* synthetic */ a() {
            this((byte) 0);
        }

        private a(byte b) {
        }
    }
}
