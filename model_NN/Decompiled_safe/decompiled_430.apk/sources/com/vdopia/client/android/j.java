package com.vdopia.client.android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import com.vdopia.client.android.c;
import com.vdopia.client.android.g;

final class j extends v {
    private k c;
    private g.b d;
    private a e;
    private x f;
    private b g;
    private int h;
    private String i;

    interface a {
        Dialog a(int i);

        boolean a(int i, KeyEvent keyEvent);

        void b();

        void c();

        void d();
    }

    private j() {
    }

    j(Activity activity, AdType adType, String str) {
        super(activity, adType);
        this.i = str;
    }

    /* access modifiers changed from: package-private */
    public final Dialog a(int i2) {
        if (this.e != null) {
            return this.e.a(i2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (this.e != null) {
            this.e.d();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(int i2, KeyEvent keyEvent) {
        if (this.e != null) {
            return this.e.a(i2, keyEvent);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (this.e != null) {
            this.e.b();
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (this.e != null) {
            this.e.c();
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        c.b.d(this, "SETUPLAYOUT IS BEING CALLED");
        this.a.requestWindowFeature(1);
        this.a.getWindow().setFlags(1024, 1024);
        this.a.getWindow().setFormat(-3);
        this.c = new k(this.a);
        this.c.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.c.setBackgroundColor(-16777216);
        this.g = new b(this.a, this);
        this.c.addView(this.g.f(), 0, new ViewGroup.LayoutParams(-1, -1));
        TranslateAnimation translateAnimation = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation2.setDuration(500);
        this.c.setInAnimation(translateAnimation);
        this.c.setOutAnimation(translateAnimation2);
        this.g.a();
        this.a.setContentView(this.c);
        this.h = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b
     arg types: [android.app.Activity, com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.g.a(com.vdopia.client.android.g, java.lang.Object, java.lang.String):java.util.List
      com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b */
    /* access modifiers changed from: package-private */
    public final g.b e() {
        if (this.i != null) {
            if (this.i.equals("talk2me")) {
                g gVar = new g();
                gVar.getClass();
                this.d = new g.b(gVar, "1", Environment.getExternalStorageDirectory() + "/demo/vampire.mp4", null, null, "http://www.imdb.com/title/tt1666186/", null, -1, null, null, "http://localhost:8080/sdcard/demo/ttm/ttm.html");
            } else if (this.i.equals("preapp")) {
                g gVar2 = new g();
                gVar2.getClass();
                this.d = new g.b(gVar2, "2", Environment.getExternalStorageDirectory() + "/demo/pop.mp4", null, null, "http://cdn.vdopia.com/advertisements/pop/", null, -1, null, null, null);
            }
            return this.d;
        }
        this.d = g.a((Context) this.a, this.b, false);
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final g.b f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final AdType g() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void b(int i2) {
        this.a.dismissDialog(7);
    }

    /* access modifiers changed from: package-private */
    public final void c(int i2) {
        switch (i2) {
            case 1:
            case 3:
                c.b.d(this, "Either skipped or Completed = " + i2 + "\t and talk2me = " + this.f);
                if (this.f != null) {
                    if (this.h != 1) {
                        this.c.showNext();
                        this.a.setRequestedOrientation(1);
                        this.e = this.f;
                        this.h = 1;
                    }
                    this.c.removeView(this.g.f());
                    this.f.a();
                    return;
                }
                c.b.d(this, "Finishinh the activity");
                this.a.finish();
                return;
            case 2:
                if (this.f != null) {
                    if (this.h != 1) {
                        this.c.showNext();
                        this.a.setRequestedOrientation(1);
                        this.e = this.f;
                        this.h = 1;
                    }
                    this.c.removeView(this.g.f());
                    this.f.f();
                    return;
                }
                if (!(this.d == null || this.d.a == null)) {
                    this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.d.a)));
                }
                this.a.finish();
                return;
            case 4:
                if (this.c.getChildCount() <= 1 && this.d != null && this.d.d != null && this.f == null) {
                    this.f = new x(this.a, this);
                    this.c.addView(this.f.e(), 1, new ViewGroup.LayoutParams(-1, -1));
                }
                if (this.b == AdType.preapp) {
                    VDO.adStart(VDO.AD_TYPE_PRE_APP_VIDEO);
                    return;
                } else {
                    VDO.adStart(VDO.AD_TYPE_IN_APP_VIDEO);
                    return;
                }
            case 5:
                if (this.b == AdType.preapp) {
                    VDO.noAdsAvailable(VDO.AD_TYPE_PRE_APP_VIDEO, 0);
                } else {
                    VDO.noAdsAvailable(VDO.AD_TYPE_IN_APP_VIDEO, 0);
                }
                this.a.finish();
                return;
            case 16:
                if (this.c.getChildCount() <= 1 && this.g != null) {
                    this.c.addView(this.g.f(), 1, new ViewGroup.LayoutParams(-1, -1));
                    this.a.setRequestedOrientation(0);
                    this.c.showNext();
                    this.h = 0;
                    return;
                }
                return;
            default:
                c.b.d(this, "Unexpected Action " + i2);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(a aVar) {
        this.e = aVar;
    }
}
