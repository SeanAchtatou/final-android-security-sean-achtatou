package com.vdopia.client.android;

import android.media.MediaPlayer;

class u implements MediaPlayer.OnVideoSizeChangedListener {
    private /* synthetic */ a a;

    u(a aVar) {
        this.a = aVar;
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        this.a.h = mediaPlayer.getVideoWidth();
        this.a.i = mediaPlayer.getVideoHeight();
        if (this.a.h != 0 && this.a.i != 0) {
            this.a.getHolder().setFixedSize(this.a.h, this.a.i);
        }
    }
}
