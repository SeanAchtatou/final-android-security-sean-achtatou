package com.vdopia.client.android;

import java.util.concurrent.atomic.AtomicInteger;

public enum AdType {
    preapp(2),
    inapp(2),
    banner(10),
    preinter(20),
    ininter(20);
    
    AtomicInteger a;

    private AdType(int i) {
        this.a = new AtomicInteger(i);
    }
}
