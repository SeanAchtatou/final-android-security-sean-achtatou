package com.vdopia.client.android;

import android.media.MediaPlayer;
import android.util.Log;

class s implements MediaPlayer.OnErrorListener {
    private /* synthetic */ a a;

    s(a aVar) {
        this.a = aVar;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        Log.e(this.a.a, "Error: " + i + "," + i2);
        this.a.d = -1;
        this.a.e = -1;
        a aVar = this.a;
        return (this.a.o == null || !this.a.o.onError(this.a.g, i, i2)) ? true : true;
    }
}
