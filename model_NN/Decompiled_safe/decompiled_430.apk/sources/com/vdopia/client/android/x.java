package com.vdopia.client.android;

import android.app.Activity;
import android.app.Dialog;
import android.view.KeyEvent;
import android.view.View;
import com.vdopia.client.android.c;
import com.vdopia.client.android.d;
import com.vdopia.client.android.e;
import com.vdopia.client.android.g;
import com.vdopia.client.android.j;

final class x implements d.a, e.a, j.a {
    private Activity a;
    private j b;
    private e c = new e(this.a);
    private g.b d;

    x(Activity activity, j jVar) {
        c.b.d(this, "TALK2ME VIEW CONSTRUCTOR");
        this.a = activity;
        this.b = jVar;
        this.c.a(this);
        this.c.a(new d(this), "Vdopia");
        this.c.setId(65537);
        this.d = this.b.f();
        String str = this.d.d;
        if (str != null) {
            this.c.a(str);
            a(this.d, c.a.tvi);
        }
    }

    public final Dialog a(int i) {
        return null;
    }

    public final void d() {
    }

    public final boolean a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        String url = this.c.a().getUrl();
        if (url == null) {
            this.a.finish();
            return true;
        }
        if (url.equals(this.d.d)) {
            this.a.finish();
        } else if (this.c.a().getUrl().equals(this.d.a)) {
            g();
        } else if (this.c.a().canGoBack()) {
            this.c.a().goBack();
        }
        return true;
    }

    public final void b() {
    }

    public final void c() {
    }

    public final void a() {
        g();
    }

    public final void b(String str) {
        c.b.d(this, "Loaded SCHEME = " + str);
        if (str.equals("replay:")) {
            this.b.c(16);
        }
        if (str.equals("replay:") || str.equals("geo:0,0?q=") || str.equals("mailto:") || str.equals("tel:") || str.equals("sms:")) {
            g();
        }
    }

    public final void a(String str) {
        c.b.d(this, "ONLOADURL Loading URL = " + str);
        if (!str.equals(this.d.d)) {
            this.c.c();
        } else {
            this.c.a(4);
        }
    }

    public final void c(String str) {
        c.b.d(this, "ONLOADED_URL LOADED URL = " + str);
        if (str.equals(this.d.d)) {
            this.c.b();
            this.c.a("javascript:var evt=document.createEvent('Event');evt.initEvent('androidTalk2MeLoaded',true,true);window.dispatchEvent(evt);");
        }
    }

    private void g() {
        this.c.a(4);
        this.c.a(this.d.d);
    }

    /* access modifiers changed from: package-private */
    public final View e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.c.a(this.b.f().a);
    }

    private static void a(g.b bVar, c.a aVar) {
        if (bVar != null && bVar.f != null && bVar.f.get(aVar) != null) {
            for (String a2 : bVar.f.get(aVar)) {
                String a3 = c.a(c.a(a2, c.a(-1)));
                new f().b(a3);
            }
        }
    }
}
