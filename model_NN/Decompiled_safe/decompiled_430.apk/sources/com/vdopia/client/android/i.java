package com.vdopia.client.android;

import android.content.Context;
import com.vdopia.client.android.c;
import com.vdopia.client.android.g;
import java.util.Vector;

final class i {
    private static Vector<g.b> a = new Vector<>();
    private static final Object b = new Object();

    i() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b
     arg types: [android.content.Context, com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.g.a(com.vdopia.client.android.g, java.lang.Object, java.lang.String):java.util.List
      com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b */
    static g.b a(Context context) {
        synchronized (b) {
            if (a.size() != 0) {
                g.b remove = a.remove(0);
                return remove;
            }
            g.b a2 = g.a(context, AdType.banner, false);
            return a2;
        }
    }

    static void a(g.b bVar) {
        synchronized (b) {
            if (bVar.g > 0) {
                a.add(bVar);
            }
        }
    }

    static void a(g.b bVar, c.a aVar) {
        if (bVar != null && bVar.f != null && bVar.f.get(aVar) != null) {
            if (bVar.g >= ((long) c.a()) || aVar != c.a.tvi) {
                for (String a2 : bVar.f.get(aVar)) {
                    String a3 = c.a(c.a(a2, c.a(-1)));
                    new f().b(a3);
                }
            }
        }
    }
}
