package com.vdopia.client.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.vdopia.client.android.c;
import com.vdopia.client.android.g;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class VDOView extends RelativeLayout {
    private static l<String, Bitmap> o;
    /* access modifiers changed from: private */
    public g.b a;
    /* access modifiers changed from: private */
    public ImageView b;
    private Context c;
    /* access modifiers changed from: private */
    public Bitmap d;
    private Timer e;
    private boolean f;
    private boolean g;
    /* access modifiers changed from: private */
    public long h;
    private long i;
    private int j;
    /* access modifiers changed from: private */
    public int k;
    private boolean l;
    private int m;
    private Handler n;

    public VDOView(Context context) {
        this(context, (AttributeSet) null, 0);
    }

    public VDOView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VDOView(Context context, boolean z, int i2) {
        this(context, (AttributeSet) null, 0);
        this.l = z;
        this.m = i2;
    }

    public VDOView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.a = null;
        this.g = false;
        this.j = 0;
        this.l = true;
        this.m = 0;
        this.n = new Handler();
        if (!VDO.isInitialized(getContext().getApplicationContext())) {
            setVisibility(4);
        }
        this.c = context;
        this.k = Math.min(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
        c.b.d(this, "deviceWidth = " + this.k + "\t height = " + context.getResources().getDisplayMetrics().heightPixels);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + this.c.getPackageName();
            this.l = attributeSet.getAttributeBooleanValue(str, "shouldRefresh", true);
            this.m = attributeSet.getAttributeUnsignedIntValue(str, "animationStyle", 1);
        }
        c.b.d(this, "VDOView contructor");
        b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b() {
        /*
            r5 = this;
            r1 = 0
            r4 = 1
            monitor-enter(r5)
            android.content.Context r0 = r5.c     // Catch:{ all -> 0x004f }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x004f }
            boolean r0 = com.vdopia.client.android.VDO.isInitialized(r0)     // Catch:{ all -> 0x004f }
            if (r0 == 0) goto L_0x004c
            boolean r0 = r5.g     // Catch:{ all -> 0x004f }
            if (r0 != 0) goto L_0x004c
            java.lang.String r0 = "ENABLING ADS"
            com.vdopia.client.android.c.b.d(r5, r0)     // Catch:{ all -> 0x004f }
            android.content.Context r0 = r5.c     // Catch:{ all -> 0x004f }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ all -> 0x004f }
            com.vdopia.client.android.c.a(r0)     // Catch:{ all -> 0x004f }
            android.content.Context r0 = r5.c     // Catch:{ all -> 0x004f }
            android.content.Context r1 = r5.c     // Catch:{ all -> 0x004f }
            android.content.Context r1 = r1.getApplicationContext()     // Catch:{ all -> 0x004f }
            java.lang.String r1 = com.vdopia.client.android.VDO.getApiKey(r1)     // Catch:{ all -> 0x004f }
            com.vdopia.client.android.h.a(r0, r1)     // Catch:{ all -> 0x004f }
            r0 = 0
            super.setVisibility(r0)     // Catch:{ all -> 0x004f }
            r0 = 1
            r5.f = r0     // Catch:{ all -> 0x004f }
            r0 = 1
            r5.setFocusable(r0)     // Catch:{ all -> 0x004f }
            r0 = 1
            r5.setClickable(r0)     // Catch:{ all -> 0x004f }
            r0 = 1000(0x3e8, double:4.94E-321)
            r2 = 1000(0x3e8, double:4.94E-321)
            r5.a(r0, r2)     // Catch:{ all -> 0x004f }
            r0 = 1
            r5.g = r0     // Catch:{ all -> 0x004f }
            monitor-exit(r5)     // Catch:{ all -> 0x004f }
            r0 = r4
        L_0x004b:
            return r0
        L_0x004c:
            monitor-exit(r5)     // Catch:{ all -> 0x004f }
            r0 = r1
            goto L_0x004b
        L_0x004f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x004f }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.VDOView.b():boolean");
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        c.b.d(this, "Visibility in request Ad = " + getVisibility());
        if (getVisibility() != 0) {
            c.b.b(this, "Banner Ad View is not visible so aborting the ad request");
        } else if (getWindowVisibility() != 0 || !hasWindowFocus()) {
            c.b.b(this, "Window is not visible so aborting");
            if (this.e != null) {
                this.e.cancel();
                this.e = null;
            }
        } else {
            synchronized (this) {
                this.a = i.a(this.c);
                if (this.a != null) {
                    this.d = a(this.a.c);
                    c.b.d(this, "lastAd = " + this.a.toString());
                    c.b.d(this, "IMAGE INFO: Width = " + this.d.getWidth() + "\t Height = " + this.d.getHeight());
                    final ImageView imageView = new ImageView(this.c);
                    this.n.post(new Runnable() {
                        public final void run() {
                            try {
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(Math.min(VDOView.this.getMeasuredWidth(), VDOView.this.k), (Math.min(VDOView.this.getMeasuredWidth(), VDOView.this.k) * 48) / 320);
                                c.b.d(this, "device Width = " + VDOView.this.k + "\twidth = " + layoutParams.width + "\theight = " + layoutParams.height);
                                imageView.setLayoutParams(layoutParams);
                                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                imageView.setImageBitmap(VDOView.this.d);
                                imageView.setId(1);
                                VDOView.this.h = System.currentTimeMillis();
                                VDOView.this.addView(imageView);
                                VDOView.b(VDOView.this, imageView);
                                i.a(VDOView.this.a, c.a.tvi);
                                VDO.adShown(VDO.AD_TYPE_BANNER);
                            } catch (Exception e) {
                                c.b.d(this, "SIGHHHHHHHHHHHHHHHH!!!!");
                            }
                        }
                    });
                    if (this.f) {
                        this.f = false;
                        if (this.e != null) {
                            this.e.cancel();
                        }
                        this.e = null;
                        c.b.d(this, "THis was the first AD:");
                        a(this.a.g, (long) c.a());
                    }
                    this.j = 0;
                } else if (this.f) {
                    this.j++;
                    if (this.j > 120) {
                        VDO.noAdsAvailable(VDO.AD_TYPE_BANNER, 1);
                        this.j = 0;
                    }
                } else {
                    this.j += c.a();
                    if (this.j > 120) {
                        VDO.noAdsAvailable(VDO.AD_TYPE_BANNER, c.a());
                        this.j = 0;
                    }
                }
            }
        }
    }

    private void a(long j2, long j3) {
        long j4;
        c.b.d(this, "Visibility in request timer = " + getVisibility());
        if (getVisibility() != 0) {
            c.b.d(this, "View is not visible so ignoring the ad request timer setup");
        } else if (!VDO.isInitialized(this.c.getApplicationContext())) {
            c.b.b(this, "VDO is not initialized so aborting the timer setup");
        } else if (this.l || this.f) {
            c.b.d(this, "Invalidating the view");
            synchronized (this) {
                c.b.d(this, "Setting up Timer:" + j2 + "\t" + j3);
                if (this.e == null) {
                    this.e = new Timer();
                    Timer timer = this.e;
                    AnonymousClass1 r1 = new TimerTask() {
                        public final void run() {
                            c.b.d(this, "Requesting new AD");
                            VDOView.this.a();
                        }
                    };
                    if (j2 > 0) {
                        j4 = j2;
                    } else {
                        j4 = 0;
                    }
                    timer.schedule(r1, j4, j3);
                }
            }
        } else {
            c.b.b(this, "Refreshing is disabled");
        }
    }

    public void rotateBanners(boolean z) {
        this.l = z;
        if (this.l) {
            if (this.f) {
                a(1000, (long) c.a());
            } else if (this.a != null) {
                a(this.a.g, (long) c.a());
            } else {
                a((long) c.a(), (long) c.a());
            }
        } else if (!this.f && this.e != null) {
            this.e.cancel();
            this.e = null;
        }
    }

    public void setAnimationStyle(int i2) {
        this.m = i2;
    }

    public void setVisibility(int i2) {
        if (i2 == 0) {
            b();
        }
        c.b.d(this, "SUPER SETTING VISIBILITY =" + i2);
        super.setVisibility(i2);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int measuredWidth = getMeasuredWidth();
        setMeasuredDimension(Math.min(measuredWidth, this.k), (Math.min(measuredWidth, this.k) * 48) / 320);
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i2, Rect rect) {
        c.b.d(this, "onFocusChanged");
        super.onFocusChanged(z, i2, rect);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (hasFocus() && (i2 == 66 || i2 == 23)) {
            c.b.d(this, "Key " + i2 + " has been clicked... so bye bye kansas!!!");
            c();
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            c();
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 < 160 || i3 < 24) {
            if (getVisibility() == 0) {
                setVisibility(8);
            }
        } else if (getVisibility() == 8) {
            setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        if (i2 == 8) {
            a(false);
        } else if (i2 == 0 && this.e == null) {
            if (!this.g) {
                b();
            } else if (this.f) {
                a(1000, 1000);
            } else if (this.a != null) {
                a(this.a.g, (long) c.a());
            } else {
                a((long) c.a(), (long) c.a());
            }
        }
        super.onWindowVisibilityChanged(i2);
    }

    public int getVisibility() {
        return super.getVisibility();
    }

    public void onWindowFocusChanged(boolean z) {
        c.b.d(this, "Windows Focus has been changed to " + z);
        if (!z) {
            a(false);
        } else if (!this.g) {
            b();
        } else if (this.e == null) {
            if (this.f) {
                a(1000, 1000);
            } else if (this.a != null) {
                a(this.a.g, (long) c.a());
            }
        }
        super.onWindowFocusChanged(z);
    }

    private void c() {
        if (this.a != null) {
            i.a(this.a, c.a.tcl);
            this.c.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.a.a)));
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c.b.d(this, "onDetachedFromWindows");
        a(true);
        super.onDetachedFromWindow();
    }

    private void a(boolean z) {
        if (this.e != null) {
            this.e.cancel();
            this.e = null;
        }
        if (this.a != null) {
            this.i = System.currentTimeMillis();
            this.a.g -= this.i - this.h;
            if (z) {
                i.a(this.a);
                this.a = null;
            }
        }
    }

    private Bitmap a(String str) {
        if (o == null) {
            o = new l<>(10);
        }
        Bitmap a2 = o.a(str);
        if (a2 != null || str == null) {
            return a2;
        }
        c.b.d(this, "Unable to find the Image in the cache so reading it from the disk");
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = c.a(this.c, str);
            a2 = BitmapFactory.decodeStream(fileInputStream);
            o.a(str, a2);
            if (fileInputStream == null) {
                return a2;
            }
            try {
                fileInputStream.close();
                return a2;
            } catch (IOException e2) {
                return a2;
            }
        } catch (Throwable th) {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    static /* synthetic */ void b(VDOView vDOView, final ImageView imageView) {
        if (vDOView.m != 1) {
            if (vDOView.b != null) {
                vDOView.removeView(vDOView.b);
            }
            vDOView.b = imageView;
            return;
        }
        imageView.setVisibility(8);
        y yVar = new y(0.0f, -90.0f, ((float) vDOView.getWidth()) / 2.0f, ((float) vDOView.getHeight()) / 2.0f, -0.4f * ((float) vDOView.getWidth()), true);
        yVar.setDuration(700);
        yVar.setFillAfter(true);
        yVar.setInterpolator(new AccelerateInterpolator());
        yVar.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                VDOView.this.post(new a(imageView));
            }

            public final void onAnimationRepeat(Animation animation) {
            }
        });
        vDOView.startAnimation(yVar);
    }

    private final class a implements Runnable {
        /* access modifiers changed from: private */
        public ImageView b;
        /* access modifiers changed from: private */
        public ImageView c;

        public a(ImageView imageView) {
            this.b = imageView;
        }

        public final void run() {
            this.c = VDOView.this.b;
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            this.b.setVisibility(0);
            y yVar = new y(90.0f, 0.0f, ((float) VDOView.this.getWidth()) / 2.0f, ((float) VDOView.this.getHeight()) / 2.0f, -0.4f * ((float) VDOView.this.getWidth()), false);
            yVar.setDuration(700);
            yVar.setFillAfter(true);
            yVar.setInterpolator(new DecelerateInterpolator());
            yVar.setAnimationListener(new Animation.AnimationListener() {
                public final void onAnimationStart(Animation animation) {
                }

                public final void onAnimationEnd(Animation animation) {
                    if (a.this.c != null) {
                        VDOView.this.removeView(a.this.c);
                    }
                    VDOView.this.b = a.this.b;
                }

                public final void onAnimationRepeat(Animation animation) {
                }
            });
            VDOView.this.startAnimation(yVar);
        }
    }
}
