package com.vdopia.client.android;

import android.app.Activity;
import android.app.Dialog;
import android.view.KeyEvent;
import com.vdopia.client.android.g;

abstract class v {
    Activity a;
    AdType b;

    /* access modifiers changed from: package-private */
    public abstract Dialog a(int i);

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    public abstract void b();

    /* access modifiers changed from: package-private */
    public abstract void c();

    /* access modifiers changed from: package-private */
    public abstract void d();

    /* access modifiers changed from: package-private */
    public abstract g.b f();

    /* access modifiers changed from: package-private */
    public abstract AdType g();

    v(Activity activity, AdType adType) {
        this.a = activity;
        this.b = adType;
    }

    v() {
    }
}
