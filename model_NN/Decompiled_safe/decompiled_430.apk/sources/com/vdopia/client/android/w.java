package com.vdopia.client.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.vdopia.client.android.c;
import java.util.HashMap;
import java.util.Map;

final class w {
    protected static final String[] a = {"_id", "plid", "cid", "adid", "adType", "fileUrl", "fileUrlLq", "campaignUrl", "validFrom", "validTo", "localFile", "md5", "adBytes", "duration", "closeImageUrl", "vdmsg", "talk2meUrl"};
    private static final String[] b = {"_id", "validTo"};
    private static final String[] c = {"_id", "adId", "url", "type"};
    private a d;
    private SQLiteDatabase e;
    private final Context f;

    static {
        String[] strArr = {"lockState", "updatedAt"};
    }

    w(Context context) {
        this.f = context;
    }

    /* access modifiers changed from: package-private */
    public final w a() throws SQLException {
        this.d = new a(this.f);
        this.e = this.d.getWritableDatabase();
        return this;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.d.close();
    }

    /* access modifiers changed from: package-private */
    public final long a(long j, String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("adId", Long.valueOf(j));
        contentValues.put("url", str2);
        contentValues.put("type", str);
        return this.e.insert("trs", null, contentValues);
    }

    /* access modifiers changed from: package-private */
    public final Cursor c() {
        return this.e.query("trs", c, null, null, null, null, null);
    }

    /* access modifiers changed from: package-private */
    public final Cursor a(long j) {
        return this.e.query("trs", c, "adId=" + j, null, null, null, "type");
    }

    /* access modifiers changed from: package-private */
    public final int b(long j) {
        return this.e.delete("trs", "_id=" + j, null);
    }

    /* access modifiers changed from: package-private */
    public final long c(long j) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("validTo", Long.valueOf(j));
        return this.e.insert("pls", null, contentValues);
    }

    /* access modifiers changed from: package-private */
    public final Cursor d(long j) {
        return this.e.query("pls", b, "validTo>" + j, null, null, null, "validTo desc", "1");
    }

    /* access modifiers changed from: package-private */
    public final int e(long j) {
        return this.e.delete("pls", "validTo<" + j, null);
    }

    /* access modifiers changed from: package-private */
    public final long a(long j, String str, String str2, String str3, String str4, String str5, String str6, long j2, long j3, String str7, String str8, long j4, int i, String str9, String str10, String str11) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cid", str);
        contentValues.put("adid", str2);
        contentValues.put("adType", str3);
        contentValues.put("plid", Long.valueOf(j));
        contentValues.put("fileUrl", str4);
        contentValues.put("fileUrlLq", str5);
        contentValues.put("campaignUrl", str6);
        contentValues.put("validFrom", Long.valueOf(j2));
        contentValues.put("validTo", Long.valueOf(j3));
        contentValues.put("localFile", str7);
        contentValues.put("md5", str8);
        contentValues.put("adBytes", Long.valueOf(j4));
        contentValues.put("duration", Integer.valueOf(i));
        contentValues.put("closeImageUrl", str9);
        contentValues.put("vdmsg", str10);
        contentValues.put("talk2meUrl", str11);
        c.b.d(this, "So the VALUES ARE = " + j + "\t" + str + "\t" + str2 + "\t" + str3 + "\t" + str4 + "\t" + str5 + "\t" + str6 + "\t" + j2 + "\t" + j3 + "\t" + str7 + "\t" + str8 + "\t" + j4);
        return this.e.insert("ads", null, contentValues);
    }

    /* access modifiers changed from: package-private */
    public final Cursor d() {
        return this.e.query("ads", a, null, null, null, null, null);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.database.Cursor a(com.vdopia.client.android.AdType r12, boolean r13) {
        /*
            r11 = this;
            r10 = 0
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x009f }
            r2 = 1000(0x3e8, double:4.94E-321)
            long r3 = r0 / r2
            android.database.Cursor r8 = r11.d(r3)     // Catch:{ Exception -> 0x009f }
            boolean r0 = r8.moveToFirst()     // Catch:{ Exception -> 0x00b6 }
            if (r0 == 0) goto L_0x00b9
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00b6 }
            r1 = 0
            java.lang.String r2 = "_id"
            r0[r1] = r2     // Catch:{ Exception -> 0x00b6 }
            java.util.Map r0 = a(r8, r0)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r1 = "_id"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x00b6 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x00b6 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x00b6 }
            long r5 = r8.getLong(r0)     // Catch:{ Exception -> 0x00b6 }
            android.database.sqlite.SQLiteDatabase r0 = r11.e     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r1 = "ads"
            java.lang.String[] r2 = com.vdopia.client.android.w.a     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r9 = "adType='"
            r7.<init>(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r9 = r12.toString()     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r9 = "' AND "
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r9 = "plid"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r9 = "="
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = " AND "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = "validFrom"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = "<="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = " AND "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = "validTo"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r6 = ">"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x00b6 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00b6 }
            r4 = 0
            r5 = 0
            r6 = 0
            if (r13 == 0) goto L_0x009d
            java.lang.String r7 = "RANDOM()"
        L_0x0091:
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00b6 }
            r1 = r0
            r0 = r8
        L_0x0097:
            if (r0 == 0) goto L_0x009c
            r0.close()
        L_0x009c:
            return r1
        L_0x009d:
            r7 = r10
            goto L_0x0091
        L_0x009f:
            r0 = move-exception
            r1 = r10
        L_0x00a1:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exception while fetching current ads "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.vdopia.client.android.c.b.c(r11, r0)
            r0 = r1
            r1 = r10
            goto L_0x0097
        L_0x00b6:
            r0 = move-exception
            r1 = r8
            goto L_0x00a1
        L_0x00b9:
            r0 = r8
            r1 = r10
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor");
    }

    /* access modifiers changed from: package-private */
    public final int a(String str, String str2, String str3) {
        return this.e.delete("ads", "cid='" + str + "' AND " + "adid" + "='" + str2 + "' AND " + "adType" + "='" + str3 + "'", null);
    }

    /* access modifiers changed from: package-private */
    public final int f(long j) {
        return this.e.delete("ads", "validTo<" + j, null);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0062  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            android.database.sqlite.SQLiteDatabase r0 = r10.e     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.String r1 = "ch"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            r3 = 0
            java.lang.String r4 = "val"
            r2[r3] = r4     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.String r4 = "key='"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0043, all -> 0x005e }
            boolean r1 = r0.moveToFirst()     // Catch:{ Throwable -> 0x006d, all -> 0x0066 }
            if (r1 == 0) goto L_0x0074
            java.lang.String r1 = "val"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Throwable -> 0x006d, all -> 0x0066 }
            if (r1 < 0) goto L_0x0074
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x006d, all -> 0x0066 }
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()
        L_0x0041:
            r0 = r1
        L_0x0042:
            return r0
        L_0x0043:
            r0 = move-exception
            r1 = r8
        L_0x0045:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            java.lang.String r3 = "Exception while trying to retreive value from ch. "
            r2.<init>(r3)     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x006b }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006b }
            com.vdopia.client.android.c.b.c(r10, r0)     // Catch:{ all -> 0x006b }
            if (r1 == 0) goto L_0x0072
            r1.close()
            r0 = r8
            goto L_0x0042
        L_0x005e:
            r0 = move-exception
            r1 = r8
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()
        L_0x0065:
            throw r0
        L_0x0066:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0060
        L_0x006b:
            r0 = move-exception
            goto L_0x0060
        L_0x006d:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0045
        L_0x0072:
            r0 = r8
            goto L_0x0042
        L_0x0074:
            r1 = r8
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.w.a(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", str);
        contentValues.put("val", str2);
        if (this.e.replace("ch", null, contentValues) < 0) {
            c.b.c(this, "The update to the channel settings table was not applied - (" + str + "," + str2 + ")");
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.e.beginTransaction();
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.e.endTransaction();
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.e.setTransactionSuccessful();
    }

    static class a extends SQLiteOpenHelper {
        a(Context context) {
            super(context, "vdopia_2423779_adclient", (SQLiteDatabase.CursorFactory) null, 31);
        }

        public final void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("create table ads (_id integer primary key autoincrement, plid integer not null, cid text not null, adid text not null, adType text not null, campaignUrl text, fileUrl text not null, fileUrlLq text, validFrom integer not null, validTo integer not null, localFile text not null, md5 text not null, adBytes integer not null, duration text, closeImageUrl text, vdmsg text, talk2meUrl text);");
            sQLiteDatabase.execSQL("create table pls (_id integer primary key autoincrement, validTo integer not null);");
            sQLiteDatabase.execSQL("create table trs (_id integer primary key autoincrement, adId integer not null, url text not null, type text not null);");
            sQLiteDatabase.execSQL("create table ch (key String primary key not null, val String not null);");
            sQLiteDatabase.execSQL("create table lck (lockState integer not null, updatedAt integer not null);");
            c.b.c(this, "Creating database vdopia_2423779_adclient");
        }

        public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            c.b.c(this, "Upgrading db from v " + i + " to " + i2 + ", all data destroyed.");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ads");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS pls");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS trs");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS ch");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS lck");
            onCreate(sQLiteDatabase);
        }
    }

    static Map<String, Integer> a(Cursor cursor, String[] strArr) {
        HashMap hashMap = new HashMap();
        if (cursor != null) {
            for (String str : strArr) {
                hashMap.put(str, Integer.valueOf(cursor.getColumnIndexOrThrow(str)));
            }
        }
        return hashMap;
    }
}
