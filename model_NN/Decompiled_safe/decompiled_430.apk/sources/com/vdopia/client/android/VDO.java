package com.vdopia.client.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Process;
import android.preference.PreferenceManager;
import com.vdopia.client.android.c;

public final class VDO {
    public static final int AD_TYPE_BANNER = 44294;
    public static final int AD_TYPE_IN_APP_VIDEO = 44290;
    public static final int AD_TYPE_IN_INTER = 44292;
    public static final int AD_TYPE_PRE_APP_VIDEO = 44289;
    public static final int AD_TYPE_PRE_INTER = 44291;
    public static final int AD_TYPE_TALK2ME = 44293;
    public static final int ANIMATION_NONE = 0;
    public static final int ANIMATION_ROTATE = 1;
    public static final String INTENT_EXTRA_KEY_AD_ID = "vik_adId";
    public static final String INTENT_EXTRA_KEY_AD_TYPE = "vik_adType";
    public static final String INTENT_EXTRA_KEY_API_KEY = "vik_apiKey";
    public static final String INTENT_EXTRA_KEY_URL = "vik_url";
    public static final String SDK_VERSION = "1.4.4";
    public static final String SDK_VERSION_DATE = "11-02-2011";
    private static AdEventListener a;
    protected static boolean isInitialized = false;

    public interface AdEventListener {
        void adShown(int i);

        void adStart(int i);

        void noAdsAvailable(int i, int i2);
    }

    public static void beginSession(Context context, String str) {
    }

    public static void endSession(Context context, String str) {
    }

    public static void initialize(String str, Context context) {
        if (!isInitialized) {
            Context applicationContext = context.getApplicationContext();
            c.b.d("VDO", "Setting apikey into the preferences " + str);
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(applicationContext.getApplicationContext()).edit();
            edit.putString("vdo_api_key", str);
            edit.commit();
            setProcessPid(context.getApplicationContext());
            h.a(context.getApplicationContext(), str);
            isInitialized = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b
     arg types: [android.content.Context, com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.g.a(com.vdopia.client.android.g, java.lang.Object, java.lang.String):java.util.List
      com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b */
    public static boolean isAdAvailable(int i, Context context) {
        AdType adType;
        switch (i) {
            case AD_TYPE_PRE_APP_VIDEO /*44289*/:
                adType = AdType.preapp;
                break;
            case AD_TYPE_IN_APP_VIDEO /*44290*/:
                adType = AdType.inapp;
                break;
            case AD_TYPE_PRE_INTER /*44291*/:
            case AD_TYPE_IN_INTER /*44292*/:
            case AD_TYPE_TALK2ME /*44293*/:
            default:
                return false;
            case AD_TYPE_BANNER /*44294*/:
                adType = AdType.banner;
                break;
        }
        if (g.a(context, adType, false) != null) {
            return true;
        }
        return false;
    }

    public static void setListener(AdEventListener adEventListener) {
        a = adEventListener;
    }

    protected static void adStart(int i) {
        if (a != null) {
            a.adStart(i);
        }
    }

    protected static void adShown(int i) {
        if (a != null) {
            a.adShown(i);
        }
    }

    protected static void noAdsAvailable(int i, int i2) {
        if (a != null) {
            a.noAdsAvailable(i, i2);
        }
    }

    protected static boolean isInitialized(Context context) {
        if (getApiKey(context) != null) {
            return true;
        }
        return false;
    }

    protected static String getApiKey(Context context) {
        String string = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getString("vdo_api_key", null);
        c.b.d("VDO", "getting the api key = " + string);
        return string;
    }

    protected static void setProcessPid(Context context) {
        c.b.d("VDO", "Setting process pid into prefernces");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        int i = defaultSharedPreferences.getInt("vdo_pid", -1);
        if (i != -1) {
            try {
                if (i == Process.myPid()) {
                    return;
                }
            } catch (Exception e) {
                return;
            }
        }
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        edit.putInt("vdo_pid", Process.myPid());
        edit.putLong("vdo_st", System.currentTimeMillis());
        edit.commit();
    }

    protected static long getStartTimeSec(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getLong("vdo_st", -1) / 1000;
    }
}
