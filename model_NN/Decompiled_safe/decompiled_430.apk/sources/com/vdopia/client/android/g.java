package com.vdopia.client.android;

import android.content.Context;
import com.vdopia.client.android.c;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

final class g {
    private static /* synthetic */ int[] A;
    private static String a = "plist";
    private static String b = "dict";
    private static String c = "ads";
    private static String d = "valid";
    private static String e = "basetracker";
    private static String f = "key";
    /* access modifiers changed from: private */
    public static String g = "ci";
    /* access modifiers changed from: private */
    public static String h = "ai";
    /* access modifiers changed from: private */
    public static String i = "cu";
    /* access modifiers changed from: private */
    public static String j = "at";
    /* access modifiers changed from: private */
    public static String k = "fu";
    /* access modifiers changed from: private */
    public static String l = "fu_low";
    /* access modifiers changed from: private */
    public static String m = "vf";
    /* access modifiers changed from: private */
    public static String n = "vt";
    /* access modifiers changed from: private */
    public static String o = "md";
    /* access modifiers changed from: private */
    public static String p = "sz";
    private static String q = "chsettings";
    /* access modifiers changed from: private */
    public static String r = "dur";
    /* access modifiers changed from: private */
    public static String s = "closeimg";
    /* access modifiers changed from: private */
    public static String t = "vdmsg";
    /* access modifiers changed from: private */
    public static String u = "tu";
    private static String v = "version";
    private static String w = "1.0";
    private long x;
    private List<b> y;
    private String z;

    private enum c {
        dict,
        array,
        string,
        integer
    }

    private static /* synthetic */ int[] p() {
        int[] iArr = A;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.array.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.dict.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[c.integer.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[c.string.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            A = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        return this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor
     arg types: [com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.w.a(android.database.Cursor, java.lang.String[]):java.util.Map<java.lang.String, java.lang.Integer>
      com.vdopia.client.android.w.a(java.lang.String, java.lang.String):void
      com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0067, code lost:
        r9 = r17.getString(r3.get("campaignUrl").intValue());
        r18 = r17.getLong(r3.get("_id").intValue());
        r5 = r17.getString(r3.get("adid").intValue());
        r10 = r17.getString(r3.get("fileUrl").intValue());
        r11 = r17.getInt(r3.get("duration").intValue());
        r12 = r17.getString(r3.get("closeImageUrl").intValue());
        r13 = r17.getString(r3.get("vdmsg").intValue());
        r14 = r17.getString(r3.get("talk2meUrl").intValue());
        r22 = r16.a(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r3 = com.vdopia.client.android.w.a(r22, new java.lang.String[]{"type", "url"});
        r8 = new java.util.HashMap();
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0151, code lost:
        if (r4 < r22.getCount()) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0153, code lost:
        r4 = new com.vdopia.client.android.g();
        r4.getClass();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0161, code lost:
        r21 = r22;
        r22 = new com.vdopia.client.android.g.b(r4, r5, r6, null, r8, r9, r10, r11, r12, r13, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x017f, code lost:
        if (r22.moveToPosition(r4) != false) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0181, code lost:
        com.vdopia.client.android.c.b.b("findRandomAdToPlay", "Unexpected break from cursor");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x018b, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0190, code lost:
        r7 = r22.getString(r3.get("type").intValue());
        r18 = r22.getString(r3.get("url").intValue());
        r21 = com.vdopia.client.android.c.a.valueOf(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01c9, code lost:
        if (r8.containsKey(r21) != false) goto L_0x01d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01cb, code lost:
        r8.put(r21, new java.util.LinkedList());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01d7, code lost:
        ((java.util.List) r8.get(r21)).add(r18);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01e8, code lost:
        r21 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01e9, code lost:
        r3 = r17;
        r4 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0279, code lost:
        r21 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x027a, code lost:
        r3 = r17;
        r4 = r16;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0167 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0171  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0211 A[SYNTHETIC, Splitter:B:49:0x0211] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x021b  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0259 A[SYNTHETIC, Splitter:B:65:0x0259] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x025e  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x029c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.vdopia.client.android.g.b a(android.content.Context r21, com.vdopia.client.android.AdType r22, boolean r23) {
        /*
            android.content.Context r23 = r21.getApplicationContext()
            monitor-enter(r23)
            r15 = 0
            r3 = 0
            r4 = 0
            r7 = 0
            com.vdopia.client.android.w r5 = new com.vdopia.client.android.w     // Catch:{ Exception -> 0x0281, all -> 0x024f }
            r0 = r5
            r1 = r21
            r0.<init>(r1)     // Catch:{ Exception -> 0x0281, all -> 0x024f }
            com.vdopia.client.android.w r16 = r5.a()     // Catch:{ Exception -> 0x0281, all -> 0x024f }
            r3 = 1
            r0 = r16
            r1 = r22
            r2 = r3
            android.database.Cursor r17 = r0.a(r1, r2)     // Catch:{ Exception -> 0x028b, all -> 0x026a }
            if (r17 == 0) goto L_0x022e
            boolean r3 = r17.moveToFirst()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            if (r3 == 0) goto L_0x022e
            java.lang.String[] r22 = com.vdopia.client.android.w.a     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r22
            java.util.Map r3 = com.vdopia.client.android.w.a(r0, r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
        L_0x0031:
            java.lang.String r22 = "md5"
            r0 = r3
            r1 = r22
            java.lang.Object r22 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r22 = (java.lang.Integer) r22     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r22 = r22.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r22
            r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r22 = "localFile"
            r0 = r3
            r1 = r22
            java.lang.Object r22 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r22 = (java.lang.Integer) r22     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r22 = r22.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r22
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r21
            r1 = r6
            boolean r22 = com.vdopia.client.android.c.d(r0, r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            if (r22 == 0) goto L_0x0222
            java.lang.String r21 = "campaignUrl"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "_id"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            long r18 = r0.getLong(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "adid"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "fileUrl"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "duration"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            int r11 = r0.getInt(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "closeImageUrl"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "vdmsg"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r21 = "talk2meUrl"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r17
            r1 = r21
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r16
            r1 = r18
            android.database.Cursor r22 = r0.a(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r21 = 2
            r0 = r21
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r21 = r0
            r3 = 0
            java.lang.String r4 = "type"
            r21[r3] = r4     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r3 = 1
            java.lang.String r4 = "url"
            r21[r3] = r4     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r22
            r1 = r21
            java.util.Map r3 = com.vdopia.client.android.w.a(r0, r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            java.util.HashMap r8 = new java.util.HashMap     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r8.<init>()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r21 = 0
            r4 = r21
        L_0x014a:
            int r21 = r22.getCount()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r4
            r1 = r21
            if (r0 < r1) goto L_0x0178
            com.vdopia.client.android.g$b r3 = new com.vdopia.client.android.g$b     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            com.vdopia.client.android.g r4 = new com.vdopia.client.android.g     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r4.<init>()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r4.getClass()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r7 = 0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r21 = r22
            r22 = r3
        L_0x0165:
            if (r17 == 0) goto L_0x016a
            r17.close()     // Catch:{ all -> 0x0267 }
        L_0x016a:
            if (r21 == 0) goto L_0x016f
            r21.close()     // Catch:{ all -> 0x0267 }
        L_0x016f:
            if (r16 == 0) goto L_0x0174
            r16.b()     // Catch:{ all -> 0x0267 }
        L_0x0174:
            r21 = r22
        L_0x0176:
            monitor-exit(r23)     // Catch:{ all -> 0x0267 }
            return r21
        L_0x0178:
            r0 = r22
            r1 = r4
            boolean r21 = r0.moveToPosition(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            if (r21 != 0) goto L_0x0190
            java.lang.String r21 = "findRandomAdToPlay"
            java.lang.String r7 = "Unexpected break from cursor"
            r0 = r21
            r1 = r7
            com.vdopia.client.android.c.b.b(r0, r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
        L_0x018b:
            int r21 = r4 + 1
            r4 = r21
            goto L_0x014a
        L_0x0190:
            java.lang.String r21 = "type"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r22
            r1 = r21
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            java.lang.String r21 = "url"
            r0 = r3
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            java.lang.Integer r21 = (java.lang.Integer) r21     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            int r21 = r21.intValue()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r22
            r1 = r21
            java.lang.String r18 = r0.getString(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            com.vdopia.client.android.c$a r21 = com.vdopia.client.android.c.a.valueOf(r7)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r8
            r1 = r21
            boolean r7 = r0.containsKey(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            if (r7 != 0) goto L_0x01d7
            java.util.LinkedList r7 = new java.util.LinkedList     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r7.<init>()     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r8
            r1 = r21
            r2 = r7
            r0.put(r1, r2)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
        L_0x01d7:
            r0 = r8
            r1 = r21
            java.lang.Object r21 = r0.get(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            java.util.List r21 = (java.util.List) r21     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            r0 = r21
            r1 = r18
            r0.add(r1)     // Catch:{ Exception -> 0x01e8, all -> 0x0279 }
            goto L_0x018b
        L_0x01e8:
            r21 = move-exception
            r3 = r17
            r4 = r16
        L_0x01ed:
            java.lang.String r5 = "findRandomAdToPlay"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x027f }
            java.lang.String r7 = "Exception - "
            r6.<init>(r7)     // Catch:{ all -> 0x027f }
            java.lang.String r7 = com.vdopia.client.android.c.a(r21)     // Catch:{ all -> 0x027f }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x027f }
            r0 = r6
            r1 = r21
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ all -> 0x027f }
            java.lang.String r21 = r21.toString()     // Catch:{ all -> 0x027f }
            r0 = r5
            r1 = r21
            com.vdopia.client.android.c.b.c(r0, r1)     // Catch:{ all -> 0x027f }
            if (r3 == 0) goto L_0x0214
            r3.close()     // Catch:{ all -> 0x0267 }
        L_0x0214:
            if (r22 == 0) goto L_0x0219
            r22.close()     // Catch:{ all -> 0x0267 }
        L_0x0219:
            if (r4 == 0) goto L_0x029c
            r4.b()     // Catch:{ all -> 0x0267 }
            r21 = r15
            goto L_0x0176
        L_0x0222:
            boolean r22 = r17.moveToNext()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            if (r22 != 0) goto L_0x0031
            r21 = r7
            r22 = r15
            goto L_0x0165
        L_0x022e:
            java.lang.String r21 = "findRandomAdToPlay"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r4 = "Didn't find any current and ready ad, type="
            r3.<init>(r4)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r0 = r3
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            com.vdopia.client.android.c.b.c(r21, r22)     // Catch:{ Exception -> 0x0293, all -> 0x0271 }
            r21 = r7
            r22 = r15
            goto L_0x0165
        L_0x024f:
            r21 = move-exception
            r22 = r7
            r20 = r4
            r4 = r3
            r3 = r20
        L_0x0257:
            if (r3 == 0) goto L_0x025c
            r3.close()     // Catch:{ all -> 0x0267 }
        L_0x025c:
            if (r22 == 0) goto L_0x0261
            r22.close()     // Catch:{ all -> 0x0267 }
        L_0x0261:
            if (r4 == 0) goto L_0x0266
            r4.b()     // Catch:{ all -> 0x0267 }
        L_0x0266:
            throw r21     // Catch:{ all -> 0x0267 }
        L_0x0267:
            r21 = move-exception
            monitor-exit(r23)     // Catch:{ all -> 0x0267 }
            throw r21
        L_0x026a:
            r21 = move-exception
            r22 = r7
            r3 = r4
            r4 = r16
            goto L_0x0257
        L_0x0271:
            r21 = move-exception
            r22 = r7
            r3 = r17
            r4 = r16
            goto L_0x0257
        L_0x0279:
            r21 = move-exception
            r3 = r17
            r4 = r16
            goto L_0x0257
        L_0x027f:
            r21 = move-exception
            goto L_0x0257
        L_0x0281:
            r21 = move-exception
            r22 = r7
            r20 = r4
            r4 = r3
            r3 = r20
            goto L_0x01ed
        L_0x028b:
            r21 = move-exception
            r22 = r7
            r3 = r4
            r4 = r16
            goto L_0x01ed
        L_0x0293:
            r21 = move-exception
            r22 = r7
            r3 = r17
            r4 = r16
            goto L_0x01ed
        L_0x029c:
            r21 = r15
            goto L_0x0176
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.g.a(android.content.Context, com.vdopia.client.android.AdType, boolean):com.vdopia.client.android.g$b");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node
     arg types: [org.w3c.dom.Node, int]
     candidates:
      com.vdopia.client.android.g.a(java.lang.Object, java.lang.String):java.util.List<java.lang.Object>
      com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.String):java.lang.String
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.Long):java.lang.Long
      com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.String):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, int, ?[OBJECT, ARRAY]]
     candidates:
      com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.String):java.lang.String
      com.vdopia.client.android.g.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, boolean, java.lang.Long):java.lang.Long */
    g(InputStream inputStream, Context context) throws ParserConfigurationException, SAXException, IOException, a {
        int i2;
        Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getDocumentElement();
        documentElement.normalize();
        if (!a.equalsIgnoreCase(documentElement.getTagName())) {
            throw new a(this, "No plist at root");
        }
        if (!w.equalsIgnoreCase(documentElement.getAttribute(v))) {
            c.b.b(this, "Invalid XML version " + documentElement.getAttribute(v) + ", expected " + w);
        }
        Node a2 = a(documentElement.getFirstChild(), true);
        if (a2 == null || !b.equalsIgnoreCase(a2.getNodeName())) {
            throw new a(this, "No dict found in plist -" + a(a2));
        }
        Map<String, Object> c2 = c(a2);
        this.x = a("DUR_SEC_MAX", a(c2, d, false, (String) null), 0, 2678400, false, 600);
        this.z = a(c2, e, false, (String) null);
        List<Object> a3 = a(c2.get(c), "adList");
        this.y = new ArrayList(a3.size());
        w wVar = new w(context);
        wVar.a();
        wVar.e();
        long c3 = wVar.c((System.currentTimeMillis() / 1000) + this.x);
        int i3 = 0;
        for (Object b2 : a3) {
            try {
                b bVar = new b(this, b(b2, "ad dictionary"), c3, this.z, wVar);
                if (bVar.e) {
                    i2 = i3 + 1;
                    try {
                        this.y.add(bVar);
                        i3 = i2;
                    } catch (a e2) {
                        e = e2;
                        c.b.c(this, "Ignoring invalid ad - moving on to other ads." + e.toString());
                        i3 = i2;
                    }
                }
            } catch (a e3) {
                e = e3;
                i2 = i3;
                c.b.c(this, "Ignoring invalid ad - moving on to other ads." + e.toString());
                i3 = i2;
            }
        }
        if (i3 > 0) {
            wVar.g();
        } else {
            this.x = 0;
        }
        wVar.f();
        try {
            Object obj = c2.get(q);
            if (obj != null) {
                Map<String, Object> b3 = b(obj, "ChannelSettings");
                Long a4 = a(b3, c.a, false, (Long) null);
                if (a4 != null) {
                    c.d.set((int) a4.longValue());
                    wVar.a(c.a, a4.toString());
                    c.b.e(this, "skipSecs set to - " + a4);
                }
                Long a5 = a(b3, c.b, false, (Long) null);
                if (a5 != null) {
                    c.e.set((int) a5.longValue());
                    wVar.a(c.b, a5.toString());
                    "chId set to - " + a5;
                }
                Long a6 = a(b3, "bannersec", false, (Long) null);
                if (a6 != null) {
                    c.f.set((int) a6.longValue());
                }
                Object obj2 = b3.get(c.c);
                if (obj2 != null) {
                    Map<String, Object> b4 = b(obj2, "MaxCached");
                    for (AdType adType : AdType.values()) {
                        Long a7 = a(b4, adType.toString(), false, (Long) null);
                        if (a7 != null) {
                            adType.a.set((int) a7.longValue());
                            wVar.a(c.a(adType), a7.toString());
                            "maxCached set to: " + adType + " - " + a7;
                        }
                    }
                }
            }
        } catch (Throwable th) {
            c.b.c(this, "Exception while parsing channel settings (ignored). " + th);
        }
        wVar.b();
    }

    private static String a(Node node) {
        if (node == null) {
            return "null";
        }
        return "{ t:" + ((int) node.getNodeType()) + ",p:" + node.getPrefix() + ", n:" + node.getNodeName() + ", v:" + node.getNodeValue() + ", a:" + node.hasAttributes() + ", c:" + node.hasChildNodes() + "-" + (node.hasChildNodes() ? node.getFirstChild().getNodeValue() : "-") + "}";
    }

    private Node a(Node node, boolean z2) {
        Node node2;
        if (!z2) {
            try {
                node2 = node.getNextSibling();
            } catch (IndexOutOfBoundsException e2) {
                return null;
            }
        } else {
            node2 = node;
        }
        while (node2 != null && node2.getNodeType() != 1) {
            "Skipping a useless node - " + a(node2);
            node2 = node2.getNextSibling();
        }
        return node2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node
     arg types: [org.w3c.dom.Node, int]
     candidates:
      com.vdopia.client.android.g.a(java.lang.Object, java.lang.String):java.util.List<java.lang.Object>
      com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node */
    private List<Object> b(Node node) {
        LinkedList linkedList = new LinkedList();
        Node a2 = a(node.getFirstChild(), true);
        while (a2 != null) {
            "Found useful node " + a(a2);
            try {
                linkedList.add(d(a2));
                a2 = a(a2, false);
            } catch (RuntimeException e2) {
                c.b.c(this, "Skipping this value - unexpected error encountered- " + e2 + " - " + a(a2));
                a2 = a(a2, false);
            }
        }
        return linkedList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node
     arg types: [org.w3c.dom.Node, int]
     candidates:
      com.vdopia.client.android.g.a(java.lang.Object, java.lang.String):java.util.List<java.lang.Object>
      com.vdopia.client.android.g.a(org.w3c.dom.Node, boolean):org.w3c.dom.Node */
    private Map<String, Object> c(Node node) {
        HashMap hashMap = new HashMap();
        Node a2 = a(node.getFirstChild(), true);
        while (true) {
            if (a2 == null) {
                break;
            }
            "Found useful node - " + a(a2);
            if (!f.equalsIgnoreCase(a2.getNodeName())) {
                c.b.c(this, "Skipping unexpected non-key node - " + a(a2));
                a2 = a(a2, false);
            } else {
                String nodeValue = a2.getFirstChild().getNodeValue();
                if (nodeValue == null) {
                    c.b.c(this, "Skipping blank key.");
                    a2 = a(a2, false);
                } else {
                    Node a3 = a(a2, false);
                    if (a3 == null) {
                        c.b.c(this, "No more nodes left - skipping key - " + nodeValue + ".");
                        break;
                    }
                    try {
                        hashMap.put(nodeValue, d(a3));
                        a2 = a(a3, false);
                    } catch (RuntimeException e2) {
                        c.b.d(this, "Skipping this key: " + nodeValue + " - unexpected value encountered - " + a(a3));
                        a2 = a(a3, false);
                    }
                }
            }
        }
        return hashMap;
    }

    private Object d(Node node) throws RuntimeException {
        switch (p()[c.valueOf(node.getNodeName()).ordinal()]) {
            case 1:
                return c(node);
            case 2:
                return b(node);
            case 3:
                "value = " + node.getFirstChild().getNodeValue();
                return node.getFirstChild().getNodeValue();
            case 4:
                "value = " + Long.parseLong(node.getFirstChild().getNodeValue());
                return new Long(Long.parseLong(node.getFirstChild().getNodeValue()));
            default:
                throw new a(this, "Found an unexpected node -  (" + node.getNodeName() + "," + node.getNodeValue() + ").");
        }
    }

    /* access modifiers changed from: private */
    public long a(String str, String str2, long j2, long j3, boolean z2, long j4) throws a {
        String str3;
        boolean z3 = false;
        if (str2 == null) {
            str3 = "";
            z3 = true;
        } else {
            str3 = str2;
        }
        long j5 = 0;
        try {
            j5 = Long.parseLong(str3);
        } catch (NumberFormatException e2) {
            z3 = true;
        }
        if (!z3 && j5 >= j2 && j5 <= j3) {
            return j5;
        }
        if (z2) {
            throw new a(this, "Required value didn't match spec (" + str + ") - " + str3 + " - [" + j2 + "," + j3 + "].");
        }
        c.b.c(this, "Replacing value that didn't match spec - " + str3 + " - [" + j2 + "," + j3 + "] with " + j4 + ".");
        return j4;
    }

    /* access modifiers changed from: private */
    public String a(Map<String, Object> map, String str, boolean z2, String str2) {
        Object obj;
        Object obj2 = map.get(str);
        if (obj2 != null && (obj2 instanceof String)) {
            obj = obj2;
        } else if (z2) {
            throw new a(this, "Could not find key - " + str + " - in map.");
        } else {
            "Replacing bad value from map [" + str + "," + obj2 + "] with " + str2 + ".";
            obj = str2;
        }
        return (String) obj;
    }

    private Long a(Map<String, Object> map, String str, boolean z2, Long l2) {
        Long l3;
        Object obj = map.get(str);
        if (obj == null || !(obj instanceof Long)) {
            "Replacing bad value from map [" + str + "," + obj + "] with " + ((Object) null) + ".";
            l3 = null;
        } else {
            l3 = obj;
        }
        return l3;
    }

    /* access modifiers changed from: private */
    public List<Object> a(Object obj, String str) throws a {
        if (obj != null && (obj instanceof List)) {
            return (List) obj;
        }
        throw new a(this, "Error trying to parse document - " + str);
    }

    private Map<String, Object> b(Object obj, String str) throws a {
        if (obj != null && (obj instanceof Map)) {
            return (Map) obj;
        }
        throw new a(this, "Error trying to parse document - " + str);
    }

    g() {
    }

    class b {
        protected String a;
        protected String b;
        protected String c;
        protected String d;
        protected boolean e;
        protected HashMap<c.a, List<String>> f;
        protected long g;
        private String h;
        private String i;
        private AdType j;
        private String k;
        private long l;
        private long m;
        private long n;
        private String o;
        private long p;
        private int q;
        private String r;
        private String s;

        public final String toString() {
            return "canvasUrl:" + this.a + "\t:fileName" + this.c + "fileUrlLq:" + this.b;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
            jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 29, insn: 0x02bc: MOVE  (r1 I:?[long, double]) = (r29 I:?[long, double]), block:B:19:?
            	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:165)
            	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:137)
            	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:55)
            	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
            */
        b(
/*
Method generation error in method: com.vdopia.client.android.g.b.<init>(com.vdopia.client.android.g, java.util.Map<java.lang.String, java.lang.Object>, long, long, com.vdopia.client.android.w):void, dex: classes.dex
        jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r26v0 ?
        	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:189)
        	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:161)
        	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:133)
        	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:317)
        	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
        	at jadx.core.codegen.ClassGen.addInnerClass(ClassGen.java:253)
        	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:242)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
        	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
        	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
        	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
        	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
        	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
        	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
        	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
        	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
        	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
        	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
        	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
        	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
        	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
        	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        
*/

        b(g gVar, String str, String str2, String str3, HashMap<c.a, List<String>> hashMap, String str4, String str5, int i2, String str6, String str7, String str8) {
            this.e = false;
            this.g = (long) c.a();
            this.i = str;
            this.c = str2;
            this.b = str3;
            this.f = hashMap;
            this.a = str4;
            this.k = str5;
            this.q = i2;
            this.r = str6;
            this.s = str7;
            this.d = str8;
        }
    }

    class a extends RuntimeException {
        public static final long serialVersionUID = 2394121512363628590L;

        public a(g gVar, String str) {
            super(str);
        }
    }
}
