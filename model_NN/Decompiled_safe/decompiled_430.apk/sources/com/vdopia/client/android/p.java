package com.vdopia.client.android;

import android.view.SurfaceHolder;

class p implements SurfaceHolder.Callback {
    private /* synthetic */ a a;

    p(a aVar) {
        this.a = aVar;
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        boolean z;
        this.a.j = i2;
        this.a.k = i3;
        boolean z2 = this.a.e == 3;
        if (this.a.h == i2 && this.a.i == i3) {
            z = true;
        } else {
            z = false;
        }
        if (this.a.g != null && z2 && z) {
            if (this.a.p != 0) {
                this.a.seekTo(this.a.p);
            }
            this.a.start();
            a aVar = this.a;
        }
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.a.f = surfaceHolder;
        this.a.c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.a.c(com.vdopia.client.android.a, boolean):void
     arg types: [com.vdopia.client.android.a, int]
     candidates:
      com.vdopia.client.android.a.c(com.vdopia.client.android.a, int):void
      com.vdopia.client.android.a.c(com.vdopia.client.android.a, boolean):void */
    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.a.f = (SurfaceHolder) null;
        a aVar = this.a;
        this.a.a(true);
    }
}
