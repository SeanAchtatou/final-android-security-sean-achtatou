package com.vdopia.client.android;

import java.util.LinkedHashMap;
import java.util.Map;

final class l<K, V> {
    private LinkedHashMap<K, V> a = new LinkedHashMap<K, V>(((int) Math.ceil(13.333333015441895d)) + 1, 0.75f, true) {
        private static final long serialVersionUID = 1;

        /* access modifiers changed from: protected */
        public final boolean removeEldestEntry(Map.Entry<K, V> entry) {
            return size() > l.this.b;
        }
    };
    /* access modifiers changed from: private */
    public int b = 10;

    public l(int i) {
    }

    public final synchronized V a(String str) {
        return this.a.get(str);
    }

    public final synchronized void a(K k, V v) {
        this.a.put(k, v);
    }
}
