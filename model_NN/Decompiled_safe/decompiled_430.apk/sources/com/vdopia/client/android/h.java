package com.vdopia.client.android;

import android.content.Context;
import android.database.Cursor;
import com.vdopia.client.android.c;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.Thread;
import java.net.URL;

final class h implements Runnable {
    private static h a = null;
    private static final Object b = new Object();
    private Thread c = new Thread(this);
    /* access modifiers changed from: private */
    public Context d;
    private String e;
    private long f;

    static synchronized h a(Context context, String str) {
        h hVar;
        synchronized (h.class) {
            if (a == null) {
                a = new h(context, str);
            }
            a.e = str;
            h hVar2 = a;
            if (hVar2.c == null || hVar2.c.getState() == Thread.State.TERMINATED) {
                hVar2.d = context;
                hVar2.c = new Thread(hVar2);
                hVar2.c.start();
            }
            synchronized (b) {
                b.notifyAll();
            }
            hVar = a;
        }
        return hVar;
    }

    private h(Context context, String str) {
        this.d = context;
        this.e = str;
        this.c.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [long, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public final void run() {
        c.b.d(this, "run method called on thread:" + this.c.getId());
        long j = 0;
        while (true) {
            this.f = 0;
            try {
                if (a()) {
                    try {
                        synchronized (b) {
                            b.wait(1);
                            this.f = this.f;
                        }
                    } catch (InterruptedException e2) {
                    }
                    c();
                }
                b();
                j = 0;
                if (0 > 0) {
                    this.f = Math.min((long) Math.pow(2.0d, 0.0d), 600L);
                }
                if (this.f <= 1) {
                    this.f = 1;
                }
                try {
                    synchronized (b) {
                        c.b.d(this, "Calling after seconds " + this.f);
                        b.wait(this.f * 1000);
                    }
                } catch (InterruptedException e3) {
                }
            } catch (Exception e4) {
                j++;
                c.b.d(this, "Exception in background thread: " + c.a(e4) + e4);
                if (j > 0) {
                    this.f = Math.min((long) Math.pow(2.0d, (double) j), 600L);
                }
                if (this.f <= 1) {
                    this.f = 1;
                }
                try {
                    synchronized (b) {
                        c.b.d(this, "Calling after seconds " + this.f);
                        b.wait(this.f * 1000);
                    }
                } catch (InterruptedException e5) {
                }
            } catch (Throwable th) {
                long j2 = j;
                Throwable th2 = th;
                if (j2 > 0) {
                    this.f = Math.min((long) Math.pow(2.0d, (double) j2), 600L);
                }
                if (this.f <= 1) {
                    this.f = 1;
                }
                try {
                    synchronized (b) {
                        c.b.d(this, "Calling after seconds " + this.f);
                        b.wait(this.f * 1000);
                        throw th2;
                    }
                } catch (InterruptedException e6) {
                }
            }
        }
    }

    private boolean a() throws Exception {
        Cursor cursor;
        w a2 = new w(this.d).a();
        if (a2 == null) {
            throw new Exception(" No dbhelper");
        }
        try {
            long currentTimeMillis = System.currentTimeMillis() / 1000;
            Cursor d2 = a2.d(currentTimeMillis);
            try {
                if (d2.moveToFirst()) {
                    int columnIndex = d2.getColumnIndex("validTo");
                    if (columnIndex < 0) {
                        throw new Exception(" No valid_to col in play-list result. Possible db corruption");
                    }
                    long j = d2.getLong(columnIndex);
                    if (currentTimeMillis < j) {
                        this.f = j - currentTimeMillis;
                        d2.close();
                        a2.b();
                        return false;
                    }
                }
                c.b.d(this, "Starting plist download");
                String a3 = c.a(this.e, this.d);
                c.b.e(this, "PL url = " + a3);
                g gVar = new g(new URL(a3).openConnection().getInputStream(), this.d);
                if (gVar.a() <= 0) {
                    throw new Exception("Playlist with non-positive duration.");
                }
                this.f = gVar.a();
                if (d2 != null) {
                    d2.close();
                }
                if (a2 != null) {
                    a2.b();
                }
                return true;
            } catch (Throwable th) {
                th = th;
                cursor = d2;
                if (cursor != null) {
                    cursor.close();
                }
                if (a2 != null) {
                    a2.b();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor
     arg types: [com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.w.a(android.database.Cursor, java.lang.String[]):java.util.Map<java.lang.String, java.lang.Integer>
      com.vdopia.client.android.w.a(java.lang.String, java.lang.String):void
      com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() throws java.lang.Exception {
        /*
            r15 = this;
            com.vdopia.client.android.w r0 = new com.vdopia.client.android.w
            android.content.Context r1 = r15.d
            r0.<init>(r1)
            com.vdopia.client.android.w r1 = r0.a()
            r0 = 0
            if (r1 != 0) goto L_0x0042
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            java.lang.String r3 = "Can't proceed because we can't open db."
            r2.<init>(r3)     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            throw r2     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
        L_0x0016:
            r2 = move-exception
            r14 = r2
            r2 = r1
            r1 = r0
            r0 = r14
        L_0x001b:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0036 }
            java.lang.String r4 = "finished maybeFetchAdFiles with exception: "
            r3.<init>(r4)     // Catch:{ all -> 0x0036 }
            java.lang.String r4 = com.vdopia.client.android.c.a(r0)     // Catch:{ all -> 0x0036 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0036 }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0036 }
            com.vdopia.client.android.c.b.c(r15, r3)     // Catch:{ all -> 0x0036 }
            throw r0     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.b()
        L_0x0041:
            throw r0
        L_0x0042:
            java.util.Map r2 = r15.a(r1)     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            r3.<init>()     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            r4.<init>()     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            com.vdopia.client.android.AdType[] r5 = com.vdopia.client.android.AdType.values()     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            int r6 = r5.length     // Catch:{ Exception -> 0x0016, all -> 0x0136 }
            r7 = 0
            r8 = r0
        L_0x0057:
            if (r7 < r6) goto L_0x006b
            r8.close()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r1.b()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r2 = 0
            r5 = 0
            com.vdopia.client.android.AdType[] r6 = com.vdopia.client.android.AdType.values()     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            int r7 = r6.length     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            r0 = 0
            r8 = r0
        L_0x0068:
            if (r8 < r7) goto L_0x0101
            return
        L_0x006b:
            r9 = r5[r7]     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.util.concurrent.atomic.AtomicInteger r0 = r9.a     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r10 = r0.get()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.Object r0 = r2.get(r9)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r0 = r10 - r0
            java.util.LinkedList r10 = new java.util.LinkedList     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r10.<init>()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r4.put(r9, r11)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r3.put(r9, r10)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            if (r8 == 0) goto L_0x0094
            r8.close()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r8 = 0
        L_0x0094:
            r11 = 1
            android.database.Cursor r8 = r1.a(r9, r11)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            if (r8 == 0) goto L_0x00fc
            boolean r9 = r8.moveToFirst()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            if (r9 == 0) goto L_0x00fc
            if (r0 <= 0) goto L_0x00fc
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r9 = 0
            java.lang.String r11 = "fileUrl"
            r0[r9] = r11     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r9 = 1
            java.lang.String r11 = "localFile"
            r0[r9] = r11     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r9 = 2
            java.lang.String r11 = "adType"
            r0[r9] = r11     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r9 = 3
            java.lang.String r11 = "md5"
            r0[r9] = r11     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.util.Map r9 = com.vdopia.client.android.w.a(r8, r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
        L_0x00be:
            java.lang.String r0 = "fileUrl"
            java.lang.Object r0 = r9.get(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.String r11 = r8.getString(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.String r0 = "localFile"
            java.lang.Object r0 = r9.get(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.String r12 = r8.getString(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.String r0 = "md5"
            java.lang.Object r0 = r9.get(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            com.vdopia.client.android.h$a r13 = new com.vdopia.client.android.h$a     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r13.<init>(r11, r0, r12)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            r10.add(r13)     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x0147, all -> 0x013d }
            if (r0 != 0) goto L_0x00be
        L_0x00fc:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0057
        L_0x0101:
            r1 = r6[r8]     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            java.lang.Object r0 = r4.get(r1)     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            int r9 = r0.intValue()     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            java.lang.Object r0 = r3.get(r1)     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            int r10 = r0.size()     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            r1 = 0
            r11 = r9
            r9 = r1
        L_0x011a:
            if (r9 >= r10) goto L_0x011e
            if (r11 > 0) goto L_0x0123
        L_0x011e:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0068
        L_0x0123:
            r1 = 0
            java.lang.Object r1 = r0.remove(r1)     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            com.vdopia.client.android.h$a r1 = (com.vdopia.client.android.h.a) r1     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            boolean r1 = r1.a()     // Catch:{ Exception -> 0x014c, all -> 0x0142 }
            if (r1 == 0) goto L_0x0151
            int r1 = r11 + -1
        L_0x0132:
            int r9 = r9 + 1
            r11 = r1
            goto L_0x011a
        L_0x0136:
            r2 = move-exception
            r14 = r2
            r2 = r1
            r1 = r0
            r0 = r14
            goto L_0x0037
        L_0x013d:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x0037
        L_0x0142:
            r0 = move-exception
            r1 = r2
            r2 = r5
            goto L_0x0037
        L_0x0147:
            r0 = move-exception
            r2 = r1
            r1 = r8
            goto L_0x001b
        L_0x014c:
            r0 = move-exception
            r1 = r2
            r2 = r5
            goto L_0x001b
        L_0x0151:
            r1 = r11
            goto L_0x0132
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.h.b():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor
     arg types: [com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.w.a(android.database.Cursor, java.lang.String[]):java.util.Map<java.lang.String, java.lang.Integer>
      com.vdopia.client.android.w.a(java.lang.String, java.lang.String):void
      com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<com.vdopia.client.android.AdType, java.util.List<java.io.File>> a(com.vdopia.client.android.w r14) {
        /*
            r13 = this;
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            com.vdopia.client.android.AdType[] r0 = com.vdopia.client.android.AdType.values()
            int r3 = r0.length
            r4 = 0
        L_0x0010:
            if (r4 < r3) goto L_0x0031
            r0 = 0
            com.vdopia.client.android.AdType[] r3 = com.vdopia.client.android.AdType.values()     // Catch:{ Exception -> 0x007a, all -> 0x0096 }
            int r4 = r3.length     // Catch:{ Exception -> 0x007a, all -> 0x0096 }
            r5 = 0
        L_0x0019:
            if (r5 < r4) goto L_0x0046
            if (r0 == 0) goto L_0x0020
            r0.close()
        L_0x0020:
            android.content.Context r0 = r13.d
            java.io.File r0 = com.vdopia.client.android.c.b(r0)
            java.io.File[] r0 = r0.listFiles()
            if (r0 == 0) goto L_0x0030
            int r3 = r0.length
            r4 = 0
        L_0x002e:
            if (r4 < r3) goto L_0x00a0
        L_0x0030:
            return r1
        L_0x0031:
            r5 = r0[r4]
            java.util.LinkedList r6 = new java.util.LinkedList
            r6.<init>()
            r1.put(r5, r6)
            java.util.HashSet r6 = new java.util.HashSet
            r6.<init>()
            r2.put(r5, r6)
            int r4 = r4 + 1
            goto L_0x0010
        L_0x0046:
            r6 = r3[r5]     // Catch:{ Exception -> 0x00fc, all -> 0x00f1 }
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ Exception -> 0x00fc, all -> 0x00f1 }
        L_0x004d:
            r7 = 0
            android.database.Cursor r7 = r14.a(r6, r7)     // Catch:{ Exception -> 0x00fc, all -> 0x00f1 }
            if (r7 == 0) goto L_0x0075
            boolean r0 = r7.moveToFirst()     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            if (r0 == 0) goto L_0x0075
            java.lang.String r0 = "localFile"
            int r8 = r7.getColumnIndex(r0)     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            if (r8 < 0) goto L_0x0075
        L_0x0062:
            java.lang.Object r0 = r2.get(r6)     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            java.lang.String r9 = r7.getString(r8)     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            r0.add(r9)     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            boolean r0 = r7.moveToNext()     // Catch:{ Exception -> 0x00f9, all -> 0x00ee }
            if (r0 != 0) goto L_0x0062
        L_0x0075:
            int r0 = r5 + 1
            r5 = r0
            r0 = r7
            goto L_0x0019
        L_0x007a:
            r3 = move-exception
            r12 = r3
            r3 = r0
            r0 = r12
        L_0x007e:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f6 }
            java.lang.String r5 = "Ignoring exception while creating file map: "
            r4.<init>(r5)     // Catch:{ all -> 0x00f6 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x00f6 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f6 }
            com.vdopia.client.android.c.b.c(r13, r0)     // Catch:{ all -> 0x00f6 }
            if (r3 == 0) goto L_0x0020
            r3.close()
            goto L_0x0020
        L_0x0096:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()
        L_0x009f:
            throw r0
        L_0x00a0:
            r5 = r0[r4]
            r6 = 0
            com.vdopia.client.android.AdType[] r7 = com.vdopia.client.android.AdType.values()
            int r8 = r7.length
            r9 = 0
            r12 = r9
            r9 = r6
            r6 = r12
        L_0x00ac:
            if (r6 < r8) goto L_0x00cd
            if (r9 != 0) goto L_0x00c9
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            java.lang.String r7 = "Deleting uncategorized file: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r7 = r5.getName()     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00ec }
            com.vdopia.client.android.c.b.e(r13, r6)     // Catch:{ Exception -> 0x00ec }
            r5.delete()     // Catch:{ Exception -> 0x00ec }
        L_0x00c9:
            int r4 = r4 + 1
            goto L_0x002e
        L_0x00cd:
            r10 = r7[r6]
            java.lang.Object r14 = r2.get(r10)
            java.util.Set r14 = (java.util.Set) r14
            java.lang.String r11 = r5.getName()
            boolean r11 = r14.contains(r11)
            if (r11 == 0) goto L_0x00e9
            java.lang.Object r14 = r1.get(r10)
            java.util.List r14 = (java.util.List) r14
            r14.add(r5)
            r9 = 1
        L_0x00e9:
            int r6 = r6 + 1
            goto L_0x00ac
        L_0x00ec:
            r5 = move-exception
            goto L_0x00c9
        L_0x00ee:
            r0 = move-exception
            r1 = r7
            goto L_0x009a
        L_0x00f1:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x009a
        L_0x00f6:
            r0 = move-exception
            r1 = r3
            goto L_0x009a
        L_0x00f9:
            r0 = move-exception
            r3 = r7
            goto L_0x007e
        L_0x00fc:
            r3 = move-exception
            r12 = r3
            r3 = r0
            r0 = r12
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.h.a(com.vdopia.client.android.w):java.util.Map");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor
     arg types: [com.vdopia.client.android.AdType, int]
     candidates:
      com.vdopia.client.android.w.a(android.database.Cursor, java.lang.String[]):java.util.Map<java.lang.String, java.lang.Integer>
      com.vdopia.client.android.w.a(java.lang.String, java.lang.String):void
      com.vdopia.client.android.w.a(com.vdopia.client.android.AdType, boolean):android.database.Cursor */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x027e  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0300  */
    /* JADX WARNING: Removed duplicated region for block: B:161:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0190  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01a3 A[SYNTHETIC, Splitter:B:66:0x01a3] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01df A[Catch:{ Exception -> 0x0256, all -> 0x027a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r14 = this;
            r11 = 0
            r10 = 0
            com.vdopia.client.android.w r0 = new com.vdopia.client.android.w
            android.content.Context r1 = r14.d
            r0.<init>(r1)
            com.vdopia.client.android.w r1 = r0.a()
            if (r1 != 0) goto L_0x0015
            java.lang.String r0 = "Can't proceed with cleanup because we can't open db."
            com.vdopia.client.android.c.b.b(r14, r0)
        L_0x0014:
            return
        L_0x0015:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x012d }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            int r0 = r1.e(r2)     // Catch:{ Exception -> 0x012d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012d }
            java.lang.String r3 = "playlists deleted = "
            r2.<init>(r3)     // Catch:{ Exception -> 0x012d }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x012d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x012d }
            com.vdopia.client.android.c.b.d(r14, r0)     // Catch:{ Exception -> 0x012d }
        L_0x0032:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0142 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            int r0 = r1.f(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "ads deleted = "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0142 }
            com.vdopia.client.android.c.b.d(r14, r0)     // Catch:{ Exception -> 0x0142 }
        L_0x004f:
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x0173, all -> 0x0195 }
            r2.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x0195 }
            android.database.Cursor r3 = r1.c()     // Catch:{ Exception -> 0x0173, all -> 0x0195 }
            boolean r0 = r3.moveToFirst()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            if (r0 == 0) goto L_0x00a2
            r0 = 2
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r4 = 0
            java.lang.String r5 = "_id"
            r0[r4] = r5     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r4 = 1
            java.lang.String r5 = "adId"
            r0[r4] = r5     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.util.Map r4 = com.vdopia.client.android.w.a(r3, r0)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
        L_0x006f:
            java.lang.Long r5 = new java.lang.Long     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.lang.String r0 = "_id"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            long r6 = r3.getLong(r0)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.lang.Long r6 = new java.lang.Long     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.lang.String r0 = "adId"
            java.lang.Object r0 = r4.get(r0)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            long r7 = r3.getLong(r0)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r2.put(r5, r6)     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            boolean r0 = r3.moveToNext()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            if (r0 != 0) goto L_0x006f
        L_0x00a2:
            java.util.HashSet r4 = new java.util.HashSet     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            r4.<init>()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            android.database.Cursor r5 = r1.d()     // Catch:{ Exception -> 0x02f5, all -> 0x02e6 }
            boolean r0 = r5.moveToFirst()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            if (r0 == 0) goto L_0x00db
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            r6 = 0
            java.lang.String r7 = "_id"
            r0[r6] = r7     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.util.Map r6 = com.vdopia.client.android.w.a(r5, r0)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
        L_0x00bd:
            java.lang.Long r7 = new java.lang.Long     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.String r0 = "_id"
            java.lang.Object r0 = r6.get(r0)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            long r8 = r5.getLong(r0)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            r7.<init>(r8)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            r4.add(r7)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            boolean r0 = r5.moveToNext()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            if (r0 != 0) goto L_0x00bd
        L_0x00db:
            java.util.Set r0 = r2.keySet()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.util.Iterator r6 = r0.iterator()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            r7 = r10
        L_0x00e4:
            boolean r0 = r6.hasNext()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            if (r0 != 0) goto L_0x0157
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.String r2 = "trackers deleted = "
            r0.<init>(r2)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            com.vdopia.client.android.c.b.d(r14, r0)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            if (r5 == 0) goto L_0x02fd
            r5.close()
            r0 = r11
        L_0x0102:
            if (r3 == 0) goto L_0x0107
            r3.close()
        L_0x0107:
            java.util.HashSet r2 = new java.util.HashSet     // Catch:{ Exception -> 0x02e0, all -> 0x02d9 }
            r2.<init>()     // Catch:{ Exception -> 0x02e0, all -> 0x02d9 }
            com.vdopia.client.android.AdType[] r3 = com.vdopia.client.android.AdType.values()     // Catch:{ Exception -> 0x02e0, all -> 0x02d9 }
            int r4 = r3.length     // Catch:{ Exception -> 0x02e0, all -> 0x02d9 }
            r5 = r10
            r6 = r0
        L_0x0113:
            if (r5 < r4) goto L_0x01a3
            java.util.Map r3 = r14.a(r1)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            com.vdopia.client.android.AdType[] r4 = com.vdopia.client.android.AdType.values()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r5 = r4.length     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            r7 = r10
        L_0x011f:
            if (r7 < r5) goto L_0x01df
            if (r6 == 0) goto L_0x0126
            r6.close()
        L_0x0126:
            if (r1 == 0) goto L_0x0014
            r1.b()
            goto L_0x0014
        L_0x012d:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exception while deleting playlists: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.vdopia.client.android.c.b.c(r14, r0)
            goto L_0x0032
        L_0x0142:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exception while deleting ads: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.vdopia.client.android.c.b.c(r14, r0)
            goto L_0x004f
        L_0x0157:
            java.lang.Object r0 = r6.next()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            java.lang.Object r8 = r2.get(r0)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            boolean r8 = r4.contains(r8)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            if (r8 != 0) goto L_0x00e4
            long r8 = r0.longValue()     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            int r0 = r1.b(r8)     // Catch:{ Exception -> 0x02f9, all -> 0x02eb }
            int r0 = r0 + r7
            r7 = r0
            goto L_0x00e4
        L_0x0173:
            r0 = move-exception
            r2 = r11
            r3 = r11
        L_0x0176:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f0 }
            java.lang.String r5 = "Exception while deleting old trackers: "
            r4.<init>(r5)     // Catch:{ all -> 0x02f0 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x02f0 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f0 }
            com.vdopia.client.android.c.b.c(r14, r0)     // Catch:{ all -> 0x02f0 }
            if (r2 == 0) goto L_0x0300
            r2.close()
            r0 = r11
        L_0x018e:
            if (r3 == 0) goto L_0x0107
            r3.close()
            goto L_0x0107
        L_0x0195:
            r0 = move-exception
            r1 = r11
            r2 = r11
        L_0x0198:
            if (r1 == 0) goto L_0x019d
            r1.close()
        L_0x019d:
            if (r2 == 0) goto L_0x01a2
            r2.close()
        L_0x01a2:
            throw r0
        L_0x01a3:
            r0 = r3[r5]     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r6 == 0) goto L_0x01aa
            r6.close()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
        L_0x01aa:
            r7 = 0
            android.database.Cursor r6 = r1.a(r0, r7)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            boolean r0 = r6.moveToFirst()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 == 0) goto L_0x01da
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            r7 = 0
            java.lang.String r8 = "localFile"
            r0[r7] = r8     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.Map r7 = com.vdopia.client.android.w.a(r6, r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
        L_0x01c1:
            java.lang.String r0 = "localFile"
            java.lang.Object r0 = r7.get(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            r2.add(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            boolean r0 = r6.moveToNext()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 != 0) goto L_0x01c1
        L_0x01da:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x0113
        L_0x01df:
            r8 = r4[r7]     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.Object r0 = r3.get(r8)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.concurrent.atomic.AtomicInteger r9 = r8.a     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r9 = r9.get()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r9 = r0 - r9
            if (r9 <= 0) goto L_0x023d
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.Object r0 = r3.get(r8)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            r10.<init>(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.Object r0 = r3.get(r8)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            int r0 = r0.size()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            r11.<init>(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.Object r0 = r3.get(r8)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.List r0 = (java.util.List) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
        L_0x021d:
            boolean r0 = r8.hasNext()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 != 0) goto L_0x0242
            java.util.Collections.shuffle(r11)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.Iterator r8 = r11.iterator()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
        L_0x022a:
            boolean r0 = r8.hasNext()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 != 0) goto L_0x0287
        L_0x0230:
            java.util.Collections.shuffle(r10)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.util.Iterator r8 = r10.iterator()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
        L_0x0237:
            boolean r0 = r8.hasNext()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 != 0) goto L_0x02b0
        L_0x023d:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x011f
        L_0x0242:
            java.lang.Object r0 = r8.next()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.io.File r0 = (java.io.File) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r12 = r0.getName()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            boolean r12 = r2.contains(r12)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r12 == 0) goto L_0x0276
            r10.add(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            goto L_0x021d
        L_0x0256:
            r0 = move-exception
            r2 = r6
        L_0x0258:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x02de }
            java.lang.String r4 = "Exception while deleting old files: "
            r3.<init>(r4)     // Catch:{ all -> 0x02de }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x02de }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02de }
            com.vdopia.client.android.c.b.c(r14, r0)     // Catch:{ all -> 0x02de }
            if (r2 == 0) goto L_0x026f
            r2.close()
        L_0x026f:
            if (r1 == 0) goto L_0x0014
            r1.b()
            goto L_0x0014
        L_0x0276:
            r11.add(r0)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            goto L_0x021d
        L_0x027a:
            r0 = move-exception
            r2 = r6
        L_0x027c:
            if (r2 == 0) goto L_0x0281
            r2.close()
        L_0x0281:
            if (r1 == 0) goto L_0x0286
            r1.b()
        L_0x0286:
            throw r0
        L_0x0287:
            java.lang.Object r0 = r8.next()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.io.File r0 = (java.io.File) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r9 <= 0) goto L_0x0230
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r12 = "Deleting old file: "
            r11.<init>(r12)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r12 = r0.getName()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            com.vdopia.client.android.c.b.d(r14, r11)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            boolean r0 = r0.delete()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 == 0) goto L_0x022a
            int r0 = r9 + -1
            r9 = r0
            goto L_0x022a
        L_0x02b0:
            java.lang.Object r0 = r8.next()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.io.File r0 = (java.io.File) r0     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r9 <= 0) goto L_0x023d
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r11 = "Deleting new file: "
            r10.<init>(r11)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r11 = r0.getName()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            com.vdopia.client.android.c.b.d(r14, r10)     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            boolean r0 = r0.delete()     // Catch:{ Exception -> 0x0256, all -> 0x027a }
            if (r0 == 0) goto L_0x0237
            int r0 = r9 + -1
            r9 = r0
            goto L_0x0237
        L_0x02d9:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
            goto L_0x027c
        L_0x02de:
            r0 = move-exception
            goto L_0x027c
        L_0x02e0:
            r2 = move-exception
            r13 = r2
            r2 = r0
            r0 = r13
            goto L_0x0258
        L_0x02e6:
            r0 = move-exception
            r1 = r11
            r2 = r3
            goto L_0x0198
        L_0x02eb:
            r0 = move-exception
            r1 = r5
            r2 = r3
            goto L_0x0198
        L_0x02f0:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0198
        L_0x02f5:
            r0 = move-exception
            r2 = r11
            goto L_0x0176
        L_0x02f9:
            r0 = move-exception
            r2 = r5
            goto L_0x0176
        L_0x02fd:
            r0 = r5
            goto L_0x0102
        L_0x0300:
            r0 = r2
            goto L_0x018e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vdopia.client.android.h.c():void");
    }

    private class a {
        private int a = 65536;
        private String b;
        private String c;
        private String d;

        a(String str, String str2, String str3) {
            this.b = str;
            this.d = str2;
            this.c = str3;
        }

        /* access modifiers changed from: package-private */
        public final boolean a() throws Exception {
            int i;
            boolean z;
            try {
                byte[] bArr = new byte[this.a];
                boolean d2 = c.d(h.this.d, this.c);
                if (!d2) {
                    c.b.d(this, "Going to dowload file: " + this.b);
                    c.b(h.this.d, "vdo_298379dks_partial");
                    InputStream inputStream = new URL(this.b).openConnection().getInputStream();
                    FileOutputStream c2 = c.c(h.this.d, "vdo_298379dks_partial");
                    int i2 = 0;
                    int i3 = 0;
                    while (i2 >= 0) {
                        i2 = inputStream.read(bArr);
                        if (i2 > 0) {
                            i3 += i2;
                            c2.write(bArr, 0, i2);
                        }
                    }
                    c2.close();
                    inputStream.close();
                    boolean d3 = c.d(h.this.d, "vdo_298379dks_partial");
                    if (d3) {
                        c.a(h.this.d, "vdo_298379dks_partial", this.c);
                        z = true;
                        d2 = d3;
                        i = i3;
                    } else {
                        d2 = d3;
                        z = false;
                        i = i3;
                    }
                } else {
                    i = 0;
                    z = false;
                }
                c.b.e(this, String.valueOf(this.c) + ": valid=" + d2 + ". bytes = " + i);
                return z;
            } catch (Exception e2) {
                c.b.c(this, "Caught exception during file download - " + e2);
                throw new Exception("Error in Download");
            }
        }
    }
}
