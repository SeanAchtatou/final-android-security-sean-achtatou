package com.vdopia.client.android;

import com.vdopia.client.android.c;
import java.net.URL;

final class f extends m<String, Void, Void> {
    f() {
    }

    /* access modifiers changed from: private */
    public Void a(String... strArr) {
        for (String str : strArr) {
            try {
                c.b.d(this, "FOLLOWING URL = " + str);
                new URL(str).openConnection().getInputStream().read();
            } catch (Exception e) {
                c.b.c(this, "Exception while following URL connection " + e + " - " + str);
            }
        }
        return null;
    }
}
