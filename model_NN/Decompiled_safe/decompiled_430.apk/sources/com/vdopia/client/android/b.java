package com.vdopia.client.android;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import com.vdopia.client.android.c;
import com.vdopia.client.android.g;
import com.vdopia.client.android.j;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

final class b implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnTouchListener, j.a {
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public j b;
    private LinearLayout c;
    /* access modifiers changed from: private */
    public a d;
    private FileInputStream e;
    /* access modifiers changed from: private */
    public Map<String, View> f = new HashMap();
    /* access modifiers changed from: private */
    public g.b g;
    /* access modifiers changed from: private */
    public long h = 0;
    /* access modifiers changed from: private */
    public final AtomicBoolean i = new AtomicBoolean(false);

    private void a(String str, String str2, boolean z) {
        Button button = new Button(this.a);
        button.setText(str);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1433892728, -1439485133});
        gradientDrawable.setCornerRadius(10.0f);
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-13421773, -7829368});
        gradientDrawable2.setCornerRadius(10.0f);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(new int[0], gradientDrawable);
        button.setBackgroundDrawable(stateListDrawable);
        button.setTextSize(15.0f);
        button.setPadding(10, 0, 10, 0);
        button.setTextColor(-1426063361);
        this.f.put(str2, button);
        button.setClickable(z);
    }

    b(Activity activity, j jVar) {
        this.a = activity;
        this.b = jVar;
        jVar.a(this);
        this.c = new LinearLayout(activity);
        this.c.setOrientation(1);
        this.c.setGravity(1);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.c.setBackgroundColor(-16777216);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.g = this.b.e();
        if (this.g != null) {
            g();
        } else if (this.b.b == AdType.preapp) {
            this.b.a.showDialog(7);
            a(300L);
        } else {
            this.b.c(5);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vdopia.client.android.b.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.vdopia.client.android.b.a(com.vdopia.client.android.g$b, com.vdopia.client.android.a, com.vdopia.client.android.c$a):void
      com.vdopia.client.android.b.a(java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public void g() {
        String str;
        if (this.b.b == AdType.preapp) {
            str = "Loading app ...";
        } else {
            str = "";
        }
        a("Ads by iVdopia", "bVdopia", false);
        a(str, "bSkip", false);
        a("Learn more", "bLearnMore", true);
        ((Button) this.f.get("bLearnMore")).setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                b.a(b.this.g, b.this.d, c.a.tcl);
                b.this.b.c(2);
            }
        });
        new LinearLayout(this.a).setOrientation(0);
        TableLayout tableLayout = new TableLayout(this.a);
        tableLayout.setStretchAllColumns(true);
        tableLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        TableRow tableRow = new TableRow(this.a);
        tableRow.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        tableRow.addView(this.f.get("bVdopia"));
        tableRow.addView(this.f.get("bSkip"));
        tableLayout.addView(tableRow);
        this.c.addView(tableLayout);
        this.d = new a(this.a);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 10);
        layoutParams.setMargins(1, 2, 1, 2);
        layoutParams.weight = 1.0f;
        this.d.setLayoutParams(layoutParams);
        this.d.a((MediaPlayer.OnPreparedListener) this);
        this.d.a((MediaPlayer.OnCompletionListener) this);
        this.d.a((MediaPlayer.OnErrorListener) this);
        this.c.addView(this.d);
        this.c.addView(this.f.get("bLearnMore"));
        new Timer("ChangeSkipButton", true).schedule(new TimerTask() {
            public final void run() {
                try {
                    Activity d = b.this.a;
                    final View.OnTouchListener onTouchListener = this;
                    d.runOnUiThread(new Runnable() {
                        public final void run() {
                            b.this.d.setOnTouchListener(onTouchListener);
                            ((Button) b.this.f.get("bSkip")).setText("Skip");
                            ((Button) b.this.f.get("bSkip")).setClickable(true);
                            ((Button) b.this.f.get("bSkip")).setOnClickListener(new View.OnClickListener() {
                                public final void onClick(View view) {
                                    b.this.b.c(1);
                                    b.a(b.this.g, b.this.d, c.a.tsk);
                                    b.this.e();
                                }
                            });
                        }
                    });
                } catch (Exception e) {
                    c.b.c(this, "caught expception:" + e.toString());
                }
            }
        }, (long) (c.d.get() * 1000));
        c.b.d(this, "Allow skip after " + (c.d.get() * 1000) + "ms.");
        if (!h()) {
            this.a.finish();
        } else {
            this.b.c(4);
        }
    }

    /* access modifiers changed from: private */
    public void a(final long j) {
        new Timer("AdCheckTimer", true).schedule(new TimerTask() {
            public final void run() {
                b.this.g = b.this.b.e();
                if (b.this.g != null) {
                    try {
                        b.this.a.runOnUiThread(new Runnable() {
                            public final void run() {
                                b.this.b.b(7);
                                b.this.g();
                            }
                        });
                    } catch (Exception e) {
                        c.b.c(this, "caught expception:" + e.toString());
                        b.this.a.finish();
                    }
                } else {
                    try {
                        Activity d = b.this.a;
                        final long j = j;
                        d.runOnUiThread(new Runnable() {
                            public final void run() {
                                b bVar = b.this;
                                bVar.h = bVar.h + j;
                                if (b.this.h > 2000) {
                                    b.this.b.b(7);
                                    b.this.b.c(5);
                                    return;
                                }
                                b.this.a(j);
                            }
                        });
                    } catch (Exception e2) {
                        c.b.c(this, "caught expception:" + e2.toString());
                        b.this.b.a.finish();
                    }
                }
            }
        }, j);
    }

    public final void b() {
        i();
    }

    public final void c() {
        e();
    }

    private boolean h() {
        this.i.set(true);
        try {
            if (this.g.c != null) {
                this.e = c.a(this.a, this.g.c);
            }
            if (this.e == null) {
                throw new FileNotFoundException(this.g.c);
            }
            final FileInputStream fileInputStream = this.e;
            final String str = this.g.b;
            this.a.runOnUiThread(new Runnable() {
                public final void run() {
                    try {
                        if (fileInputStream != null) {
                            c.b.c(this, "requested file=" + b.this.a.getFilesDir().getAbsolutePath() + "/" + b.this.g.c);
                            b.this.d.a(fileInputStream.getFD());
                        } else {
                            c.b.c(this, "requested url=" + str);
                            b.this.d.a(Uri.parse(str));
                        }
                        b.a(b.this.g, b.this.d, c.a.tvi);
                    } catch (Exception e) {
                        c.b.c(this, e.toString());
                    }
                }
            });
            return true;
        } catch (Exception e2) {
            c.b.c(this, "Error setting up video to run. " + e2.toString());
            return false;
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        c.b.a(this, "OnError called");
        this.a.finish();
        return true;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        c.b.e(this, "Prepared called");
        this.i.set(false);
        i();
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        c.b.e(this, "Completion called");
        new Timer("WaitForABitAtEnd", true).schedule(new TimerTask() {
            public final void run() {
                try {
                    b.this.a.runOnUiThread(new Runnable() {
                        public final void run() {
                            b.this.b.c(3);
                            b.a(b.this.g, b.this.d, c.a.tae);
                        }
                    });
                } catch (Exception e) {
                    c.b.c(this, "caught expception:" + e.toString());
                }
            }
        }, 1000);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean a(int i2, KeyEvent keyEvent) {
        switch (i2) {
            case 3:
                this.a.finish();
                return false;
            case 4:
                if (((Button) this.f.get("bSkip")).isClickable()) {
                    this.b.c(1);
                    a(this.g, this.d, c.a.tsk);
                    e();
                    return false;
                }
                break;
            case 24:
            case 25:
            case 91:
                return false;
        }
        c.b.e(this, "Ignoring " + keyEvent.toString() + "button");
        return true;
    }

    public final void d() {
        if (this.d != null && this.d.isPlaying()) {
            this.d.a();
        }
        if (this.e != null) {
            try {
                this.e.close();
            } catch (Exception e2) {
                c.b.c(this, "Error while closing the ad's file input stream: " + e2);
            }
            this.e = null;
        }
    }

    public final Dialog a(int i2) {
        switch (i2) {
            case 7:
                ProgressDialog progressDialog = new ProgressDialog(this.a);
                progressDialog.setMessage("Loading...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                return progressDialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    if (b.this.d != null && b.this.d.isPlaying()) {
                        b.this.d.pause();
                    }
                } catch (Exception e) {
                    c.b.c(this, "Caught exception trying to resume video - " + e);
                }
            }
        });
    }

    private void i() {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    if (b.this.d != null && !b.this.d.isPlaying() && !b.this.i.get()) {
                        b.this.d.start();
                    }
                } catch (Exception e) {
                    c.b.e(this, "Caught exception trying to resume video - " + e);
                }
            }
        });
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        try {
            this.a.runOnUiThread(new Runnable() {
                public final void run() {
                    try {
                        if (b.this.d.isPlaying()) {
                            b.this.d.pause();
                        } else {
                            b.this.d.start();
                        }
                    } catch (Exception e) {
                        c.b.e(this, "Caught exception trying to respond to touch - " + e);
                    }
                }
            });
            return false;
        } catch (Exception e2) {
            c.b.c(this, "caught expception:" + e2.toString());
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final View f() {
        return this.c;
    }

    static void a(g.b bVar, a aVar, c.a aVar2) {
        if (bVar != null && bVar.f != null && bVar.f.get(aVar2) != null) {
            int currentPosition = aVar.getCurrentPosition() / 1000;
            if (currentPosition < 0) {
                currentPosition = 0;
            }
            for (String a2 : bVar.f.get(aVar2)) {
                new f().b(c.a(c.a(a2, c.a(currentPosition))));
            }
        }
    }
}
