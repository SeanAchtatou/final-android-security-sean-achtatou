package com.vdopia.client.android;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.vdopia.client.android.c;

final class e extends LinearLayout {
    /* access modifiers changed from: private */
    public RelativeLayout a;
    private WebView b;
    /* access modifiers changed from: private */
    public Activity c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public View e;
    private View f;
    private ViewGroup.LayoutParams g;
    private Button h;
    private ProgressBar i;

    interface a {
        void a();

        void a(String str);

        void b(String str);

        void c(String str);
    }

    public e(Activity activity) {
        this(activity, null, null, null);
    }

    private e(Activity activity, View view, View view2, ViewGroup.LayoutParams layoutParams) {
        super(activity);
        this.c = activity;
        this.f = null;
        this.e = null;
        this.g = new LinearLayout.LayoutParams(-1, -1);
        this.b = new WebView(this.c) {
            public final boolean onTouchEvent(MotionEvent motionEvent) {
                motionEvent.getAction();
                return super.onTouchEvent(motionEvent);
            }

            public final void loadUrl(String str) {
                super.loadUrl(str);
            }
        };
        this.b.clearCache(true);
        this.b.setBackgroundColor(-16777216);
        this.b.setId(1048578);
        c.b.d(this, "THIS IS MY BUSINESS");
        this.b.setWebViewClient(new WebViewClient() {
            public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(str));
                c.b.d(this, "URL = " + str + "\tMIMETYPE = " + mimeTypeFromExtension);
                if (!a(str, mimeTypeFromExtension)) {
                    webView.clearView();
                }
                if (e.this.d != null) {
                    e.this.d.a(str);
                }
            }

            private boolean a(String str, String str2) {
                if (str2 == null || !str2.startsWith("video/")) {
                    return false;
                }
                c.b.d(this, "Launching VIEW for video URL...");
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.parse(str), str2);
                e.this.c.startActivity(intent);
                return true;
            }

            public final void onPageFinished(WebView webView, String str) {
                if (e.this.d != null) {
                    e.this.d.c(str);
                }
            }

            public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                c.b.d(this, "LOADING URL = " + str);
                c.b.d(this, "Listener = " + e.this.d);
                if (!str.startsWith("replay:")) {
                    if (str.startsWith("tel:")) {
                        c.b.d(this, "Launching dialer for " + str);
                        e.this.c.startActivity(Intent.createChooser(new Intent("android.intent.action.DIAL", Uri.parse(str)), "Choose Dialer"));
                    } else if (str.startsWith("mailto:")) {
                        c.b.d(this, "Launching mailer for " + str);
                        e.this.c.startActivity(Intent.createChooser(new Intent("android.intent.action.SENDTO", Uri.parse(str)), "Send Message"));
                    } else if (str.startsWith("geo:0,0?q=")) {
                        c.b.d(this, "Launching app for " + str);
                        e.this.c.startActivity(Intent.createChooser(new Intent("android.intent.action.VIEW", Uri.parse(str)), "Choose Viewer"));
                    } else if (str.startsWith("sms")) {
                        c.b.d(this, "Launching sms app for " + str);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.putExtra("address", str.substring(str.indexOf(":") + 1));
                        intent.setType("vnd.android-dir/mms-sms");
                        e.this.c.startActivity(intent);
                    } else {
                        String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(str));
                        c.b.d(this, "URL = " + str + "\tMIMETYPE = " + mimeTypeFromExtension);
                        if (!a(str, mimeTypeFromExtension)) {
                            webView.loadUrl(str);
                        }
                    }
                }
                if (e.this.d == null) {
                    return true;
                }
                e.this.d.b(str.substring(0, str.indexOf(":") + 1));
                return true;
            }
        });
        this.b.setWebChromeClient(new WebChromeClient() {
            public final void onProgressChanged(WebView webView, int i) {
                if (e.this.e instanceof ProgressBar) {
                    if (i == 100) {
                        e.this.e.setVisibility(4);
                        return;
                    }
                    e.this.a.setVisibility(0);
                    e.this.e.setVisibility(0);
                }
            }
        });
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.getSettings().setBuiltInZoomControls(true);
        this.i = new ProgressBar(this.c);
        this.i.setLayoutParams(new LinearLayout.LayoutParams(30, 30));
        this.i.setIndeterminate(true);
        this.i.setClickable(false);
        this.i.setVisibility(4);
        this.h = new Button(this.c);
        this.h.setText("Done");
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1434804758, -1440455714});
        gradientDrawable.setCornerRadius(10.0f);
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1440455714, -1434804758});
        gradientDrawable2.setCornerRadius(10.0f);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(new int[0], gradientDrawable);
        this.h.setBackgroundDrawable(stateListDrawable);
        this.h.setPadding(10, 0, 10, 0);
        this.h.setTextColor(-1);
        this.h.setClickable(true);
        this.h.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                if (e.this.d != null) {
                    e.this.d.a();
                }
            }
        });
        this.a = new RelativeLayout(this.c);
        if (this.e == null) {
            this.e = this.i;
        }
        if (this.f == null) {
            this.f = this.h;
        }
        this.a.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-10855846, -16645630}));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(10, 4, 0, 4);
        this.a.addView(this.f, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.e.getLayoutParams());
        layoutParams3.addRule(11, -1);
        this.a.addView(this.e, layoutParams3);
        setOrientation(1);
        addView(this.a, new LinearLayout.LayoutParams(-1, -2));
        addView(this.b, this.g);
        setId(1232);
    }

    public final void a(a aVar) {
        this.d = aVar;
    }

    public final void a(String str) {
        this.b.loadUrl(str);
    }

    public final WebView a() {
        return this.b;
    }

    public final void b() {
        this.a.setVisibility(8);
    }

    public final void c() {
        this.a.setVisibility(0);
        this.f.setVisibility(0);
    }

    public final void a(int i2) {
        this.f.setVisibility(4);
    }

    public final void a(Object obj, String str) {
        this.b.addJavascriptInterface(obj, str);
    }
}
