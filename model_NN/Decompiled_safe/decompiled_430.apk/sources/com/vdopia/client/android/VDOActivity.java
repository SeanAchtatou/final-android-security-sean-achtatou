package com.vdopia.client.android;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import com.vdopia.client.android.c;
import java.util.concurrent.atomic.AtomicBoolean;

public class VDOActivity extends Activity {
    private static AtomicBoolean a = new AtomicBoolean(false);
    protected v helper;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c.b.d(this, "Activity onCreate()");
        if (!VDO.isInitialized(getApplicationContext())) {
            c.b.b(this, "VDO needs to be initialized prior to creating this activity");
            finish();
            return;
        }
        Integer num = (Integer) getIntent().getExtras().get(VDO.INTENT_EXTRA_KEY_AD_TYPE);
        String str = (String) getIntent().getExtras().get("demo_type");
        String apiKey = VDO.getApiKey(getApplicationContext());
        c.a(getApplicationContext());
        h.a(getApplicationContext(), apiKey);
        if (a.getAndSet(true)) {
            c.b.d(this, "Sorry!!! LOCK IS NOT AVAILABLE");
            finish();
            return;
        }
        switch (num.intValue()) {
            case VDO.AD_TYPE_PRE_APP_VIDEO:
                this.helper = new j(this, AdType.preapp, str);
                break;
            case VDO.AD_TYPE_IN_APP_VIDEO:
                this.helper = new j(this, AdType.inapp, str);
                break;
            case VDO.AD_TYPE_PRE_INTER:
            case VDO.AD_TYPE_IN_INTER:
                finish();
                break;
            case VDO.AD_TYPE_TALK2ME:
                finish();
                break;
            default:
                c.b.b(this, "Unrecognized onCreate intent");
                finish();
                return;
        }
        if (this.helper == null) {
            finish();
        } else {
            this.helper.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        c.b.d(this, "Activity onStart()");
        if (this.helper == null) {
            finish();
        } else {
            this.helper.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.helper == null) {
            finish();
        } else {
            c.b.d(this, "Activity onPause()");
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        c.b.d(this, "Activity onStop()");
        if (this.helper == null) {
            finish();
        } else {
            this.helper.c();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        c.b.d(this, "Activity onResume()");
        if (this.helper == null) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.helper != null) {
            return this.helper.a(i, keyEvent);
        }
        finish();
        return false;
    }

    public void onUserLeaveHint() {
        if (this.helper == null) {
            finish();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        c.b.d(this, "ACTIVITY is being detroyed");
        if (this.helper != null) {
            this.helper.a();
            this.helper = null;
            a.set(false);
        }
    }

    public void finish() {
        if (!(this.helper == null || this.helper.f() == null)) {
            if (this.helper.g() == AdType.preapp) {
                VDO.adShown(VDO.AD_TYPE_PRE_APP_VIDEO);
            } else if (this.helper.g() == AdType.inapp) {
                VDO.adShown(VDO.AD_TYPE_IN_APP_VIDEO);
            }
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (this.helper != null) {
            return this.helper.a(i);
        }
        finish();
        return null;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        c.b.d(this, "FINISING ACTIVITY requestCOde = " + i + "\tresultCOde = " + i2);
        if (this.helper == null) {
            finish();
        }
    }
}
