function $(id) {
	return document.getElementById(id);
}
function del(el) {
	if (el && el.parentNode)
		el.parentNode.removeChild(el);
}
function getElementByTagNameAndAttr(tagName, attrName, v) {
	var es = this.getElementsByTagName(tagName);
	if (attrName != '') {
		var els = [];
		var l = es.length;
		for (var i = 0; i < l; i++) {
			if (es[i].getAttribute(attrName) == v) els.push(es[i]);
		}
		return els;
	}
	else return es;
}
function getElementByTagNameAndNotAttr(tagName, attrName, v) {
	var es = this.getElementsByTagName(tagName);
	if (attrName != '') {
		var els = [];
		var l = es.length;
		for (var i = 0; i < l; i++) {
			if (es[i].getAttribute(attrName) != v) els.push(es[i]);
		}
		return els;
	}
	else return es;
}
function gc(tagName, className) {
	var es = this.getElementsByTagName(tagName);
	if (className != '') {
		var els = [];
		var l = es.length;
		for (var i = 0; i < l; i++) {
			if (es[i].hasClass(className)) els.push(es[i]);
		}
		return els;
	}
	else return es;
}
function injectBefore(el) {
	el.parentNode.insertBefore(this, el);
}
function injectInto(el) {
	if (el.childNodes.length != 0)el.insertBefore(this, el.childNodes[0]); else el.appendChild(this);
}
function hasClass(c) {
	return this.className.contains(c, ' ');
}
function addClass(c) {
	if (!this.hasClass(c))this.className = (this.className + ' ' + c).clean();
}
function removeClass(c) {
	this.className = this.className.replace(new RegExp('(^|\\s)' + c + '(?:\\s|$)'), '$1').clean();
}
Element.prototype.G = getElementByTagNameAndAttr;
Element.prototype.GN = getElementByTagNameAndNotAttr;
Element.prototype.GC = gc;
Element.prototype.injectBefore = injectBefore;
Element.prototype.injectInto = injectInto;
Element.prototype.hasClass = hasClass;
Element.prototype.addClass = addClass;
Element.prototype.removeClass = removeClass;
Element.prototype.getNextSibling = function() {
	var el = this.nextSibling;
	while (el && !el.tagName) el = el.nextSibling;
	return el;
};
function toQS(o) {
	var qs = [];
	for (var property in o)
		qs.push(encodeURIComponent(property) + '=' + encodeURIComponent(o[property]));
	return qs.join('&');
}
;
String.prototype.test = function(regex, params) {
	return ((typeof(regex) == 'string') ? new RegExp(regex, params) : regex).test(this);
};
String.prototype.replaceAll = function(AFindText, ARepText) {
	var raRegExp = new RegExp(AFindText, "g");
	return this.replace(raRegExp, ARepText);
};
String.prototype.sliceBetween = function(start, end) {
	return this.slice(this.indexOf(start) + start.length, this.indexOf(end));
};
String.prototype.contains = function(string, s) {
	return(s) ? (s + this + s).indexOf(s + string + s) > -1 : this.indexOf(string) > -1;
};
String.prototype.clean = function() {
	return this.replace(/\s{2,}/g, ' ').trim();
};
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, '');
};
String.prototype.toInt = function() {
	return parseInt(this);
};
String.prototype.startsWith = function(s) {
	return this.substr(0, s.length) == s;
};
function evalJSON(str, secure) {
	if (secure)
		return ((typeof(str) != 'string') || !str.test(/^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/)) ? str : eval('(' + str + ')');
	else
		return eval(['(', str, ')'].join(''));
}

var __background__ = 1;

function show(el) {
	if (el) {
		el.style.display = 'block';
        //ppy_html.forceRefresh();
	}
	else
		ppy_funcs.log('nil el for show');
}
function hide(el) {
	if (el) {
		el.style.display = 'none';
        //ppy_html.forceRefresh();
	}
}
function disable(el) {
	if (el) {
		el.setAttribute('_onclick', el.getAttribute('onclick'));
		el.setAttribute('onclick', 'return false');
	}
}
function enable(el) {
	if (el) {
		el.setAttribute('onclick', el.getAttribute('_onclick'));
		el.removeAttribute('_onclick');
	}
}
//delete htmlpalert

function palert(msg, warn) {
	if (warn)
		PPYAlertView.alert(msg);
	else
		PPYAlertView.info(msg);
}

(function() {
	var re = /[\x00-\x1f\\"]/;
	var stringescape = {
		'\b': '\\b',
		'\t': '\\t',
		'\n': '\\n',
		'\f': '\\f',
		'\r': '\\r',
		'"' : '\\"',
		'\\': '\\\\'
	};

	this.ppy_to_json_string = function ppy_to_json_string(obj, secure) {
		switch (typeof obj) {
			case 'string':
				return ['"', (re.test(obj) ? encodeString(obj) : obj), '"'].join('');
			case 'number':
			case 'boolean':
				return String(obj);
			case 'object':
				if (obj) {
					switch (obj.constructor) {
						case Array:
							var a = [];
							for (var i = 0, l = obj.length; i < l; i++)
								a[a.length] = ppy_to_json_string(obj[i]);
							return ['[', a.join(','), ']'].join('');
						case Object:
							var a = [];
							if (secure) {
								for (var i in obj)
									if (obj.hasOwnProperty(i))
										a[a.length] = ['"', re.test(i) ? encodeString(i) : i, '":', ppy_to_json_string(obj[i])].join('');
							} else {
								for (var i in obj)
									a[a.length] = ['"', i, '":', ppy_to_json_string(obj[i])].join('');
							}
							return ['{', a.join(','), '}'].join('');
						case String:
							return ['"', (re.test(obj) ? encodeString(obj) : obj), '"'].join('');
						case Number:
						case Boolean:
							return String(obj);
						case Function:
						case Date:
						case RegExp:
							return 'undefined';
					}
				}
				return 'null';
			case 'function':
			case 'undefined':
			case 'unknown':
				return 'undefined';
			default:
				return 'null';
		}
	};

	function encodeString(string) {
		return string.replace(
			/[\x00-\x1f\\"]/g,
			function(a) {
				var b = stringescape[a];
				if (b)
					return b;
				b = a.charCodeAt();
				return ['\\u00' , Math.floor(b / 16).toString(16) , (b % 16).toString(16)].join('');
			}
			);
	}
})();

function toJSONString(obj, method) {
	if (!method) return ppy_to_json_string(obj);

	var t = typeof obj;
	if (t == 'object' && obj.constructor == Array) t = 'array';
	switch (t) {
		case 'string':
			return '"' + obj.replace(/(["\\])/g, '\\$1') + '"';
		case 'array':
			var a = [];
			for (var i = 0; i < obj.length; i++) a.push(toJSONString(obj[i]));
			return '[' + a.join(',') + ']';
		case 'object':
			var s = [];
			for (var property in obj) s.push(toJSONString(property) + ':' + toJSONString(obj[property]));
			return '{' + s.join(',') + '}';
		case 'number':
			if (isFinite(obj)) break;
		case false:
			return 'null';
	}
	return String(obj);
}

function getBaseURL() {
	try {
		return window.Papaya.baseURL();
	}
	catch(E) {
		return '';
	}
}

//delete doAJAX/loadJSON
//delete getValue
//delete submitformJSON
function cropImage(el, width, height) {
	if (!width) width = 60;
	if (!height) height = 60;
	if (el.width == 0) {
		setTimeout(cropImage, 500, el, width, height);
		return;
	}
	if (el.width < width && el.height < height) return;
	if (el.width <= el.height) {
		el.style.marginTop = (el.width - el.height) * (height / 2) / el.height + 'px';
		el.width = width;
	}
	else {
		el.style.marginLeft = (el.height - el.width) * (width / 2) / el.width + 'px';
		el.height = height;
	}
}
function checkmail() {
	var cnt = 0;
	var u = $('unreadmailcnt');
	if (u && !$('donotcheckmail')) {
		if (window.Papaya.mailUnreadCount) {
			cnt = window.Papaya.mailUnreadCount();
			if (cnt != 0) {
				u.innerHTML = cnt;
				u.style.display = "inline-block";
			}
			else hide(u);
		}
	}
}
function updatemailstatus() {
	if (!$('unreadmailcnt') || !window.Papaya) return;
	var u = $('unreadmailcnt');
	var cnt = window.Papaya.mailUnreadCount();
	if (cnt != 0) {
		u.innerHTML = cnt;
		u.style.display = "inline-block";
	}
	else hide(u);
}

//deleted showuserframe

Element.prototype.getTop = function() {
	var el = this, left = 0, top = 0;
	do {
		left += el.offsetLeft || 0;
		top += el.offsetTop || 0;
		el = el.offsetParent;
	} while (el);
	return top;
};
Array.prototype.remove = function(el) {
	var i = this.indexOf(el);
	if (i < 0) return;
	var rest = this.slice(i + 1 || this.length);
	this.length = i < 0 ? this.length + i : i;
	return this.push.apply(this, rest);
};

//delete updatesid/getsid
//delete genHoriBar

var lazy_image_queue = [];
var lazy_image_timer = null;
var lazy_image_ticket = 0;

function getAbsPath(s) {
	var l = location,h,p,f,i;
	if (/^\w+:/.test(s))
		return s;
	h = l.protocol + '//' + l.host;
	if (s.indexOf('/') == 0)
		return h + s;
	p = l.pathname.replace(/\/[^\/]*$/, '');
	f = s.match(/\.\.\//g);
	if (f) {
		s = s.substring(f.length * 3);
		for (i = f.length; i > 0; i--)
			p = p.substring(0, p.lastIndexOf('/'));
	}
	return h + p + '/' + s;
}

function stringStartWith(s, subs) {
	return s.substr(0, subs.length) == subs;
}

function parseimgsrc(imgsrc) {
	var result = imgsrc;
	var type = 0;
	if (stringStartWith(imgsrc, "papaya_cache_")) {
		if (stringStartWith(imgsrc, "papaya_cache_file://")) {
			result = imgsrc.substr(20);
			type = 1;
		}
		else if (stringStartWith(imgsrc, "papaya_cache_bundle://")) {
			result = imgsrc.substr(21);
			type = 2;
		}
	}

	result = result.replaceAll('&amp;', '&').replaceAll('&', '&amp;');
	return result;
}

function ppy_string(s) {
	if (s && typeof s != 'string') return String(s);
	else return s;
}

function lazyImageTimerHandler(interval) {
	lazy_image_ticket += interval;
	var papaya = window.Papaya;
//    papaya.log_('cacheTimerHandler, '+lazy_image_queue.length);
	var tmp = [];

	while (lazy_image_queue.length > 0) {
		var img = lazy_image_queue.pop();
		var imgsrc = img.getAttribute('imgsrc');
		if (imgsrc != null && imgsrc.length > 0) {
			imgsrc = parseimgsrc(imgsrc);
			var cachePath = ppy_string(papaya.cachePath_(imgsrc));
			if (cachePath != imgsrc) {
//                papaya.log_('use cachepath ' + cachePath);
				img.src = cachePath;
			}
			else
				tmp.push(img);

		}
	}
	lazy_image_queue = tmp;
	if (lazy_image_queue.length > 0 && lazy_image_ticket < 180 * 1000)
		lazy_image_timer = setTimeout(lazyImageTimerHandler, interval, interval);// setTimeout('lazyImageTimerHandler('+interval+')', interval);
	else {
		lazy_image_timer = null;
		lazy_image_ticket = 0;
	}
//    papaya.log_("cacheTimerHandler ends, "+lazy_image_queue.length);
}

function processLazyImage(interval) {
	interval = interval == undefined ? 1000 : interval;
	var lazyimgs = document.body.G('img', 'lazy', '1');
	lazy_image_ticket = 0;
	var papaya = window.Papaya;
//    papaya.log_("processLazyImage " + lazyimgs.length);
	if (lazyimgs.length > 0) {
		var prefix1 = 'content://';
		var prefix2 = 'file:///';
		for (var i = 0; i < lazyimgs.length; i++) {
			var img = lazyimgs[i];
			var cache = img.getAttribute('cache');
			var imgsrc = img.getAttribute('imgsrc');

			if (imgsrc != null && imgsrc.length > 0) {
				if (imgsrc.substr(0, prefix1.length) == prefix1 || imgsrc.substr(0, prefix2.length) == prefix2) {
					img.src = imgsrc;
				} else {
					if (cache == '0')
						img.src = imgsrc;
					else {
						var tmp = parseimgsrc(imgsrc);
						var cachePath = ppy_string(papaya.cachePath_(tmp));
						if (cachePath == tmp)
							lazy_image_queue.push(img);
						else {
//                            papaya.log_('use cachepath ' + cachePath);
							img.src = cachePath;
						}
					}
				}
			}
		}
	}

	if (lazy_image_timer == null && lazy_image_queue.length > 0) {
//        papaya.log_('start timer');
		lazy_image_timer = setTimeout(lazyImageTimerHandler, interval, interval);//setTimeout('lazyImageTimerHandler('+interval+')', interval);
	}
	/*else if (lazy_image_timer) papaya.log_('dont start timer, exists');
	 else papaya.log_('dont start timer, empty lazy images');*/
}
function createInputs() {
	var lis = document.body.GC('li', 'input');
	var i;
	for (i = 0; i < lis.length; i++) {
		var s = '';
		var lb = lis[i].getAttribute('label');
		if (lb)
			s += '<label>' + lb + '</label>';
		else s += '<label></label>';
		var ph = lis[i].getAttribute('placeholder');
		if (ph)
			s += '<placeholder>' + ph + '</placeholder>';
		else s += '<placeholder></placeholder>';
		var txt = lis[i].getAttribute('text');
		var tid = '';
		if (lis[i].getAttribute('textid'))
			tid = ' id="' + lis[i].getAttribute('textid') + '"';
		if (txt)
			s += '<text' + tid + '>' + txt + '</text>';
		else
			s += '<text' + tid + '></text>';
		lis[i].innerHTML = s;
	}

	var lblength = 0;
	var lbs = document.body.getElementsByTagName('label');
	for (i = 0; i < lbs.length; i++) {
		var lblen = lbs[i].clientWidth;
		if (lbs[i].parentNode.tagName.toLowerCase() == 'li' && lblen > lblength)
			lblength = lblen;
	}
	for (i = 0; i < lis.length; i++) {
		lis[i].getElementsByTagName('label')[0].style.width = lblength + 'px';
		var clk = lis[i].getAttribute('onclick');
		if (clk) lis[i].setAttribute('onclick', 'hide(this.getElementsByTagName("placeholder")[0]);' + clk);
		else lis[i].setAttribute('onclick', 'hide(this.getElementsByTagName("placeholder")[0]);');
		if (!lis[i].getElementsByTagName('text')[0].innerHTML)
			lis[i].getElementsByTagName('placeholder')[0].style.display = 'inline';
	}
}
function refreshInputs() {
	var lis = document.body.GC('li', 'input');
	for (var i = 0; i < lis.length; i++) {
		if (!lis[i].getElementsByTagName('text')[0].innerHTML)
			lis[i].getElementsByTagName('placeholder')[0].style.display = 'inline';
	}
}

var papayajs = {
	go : function(uri) {
		document.location = 'papaya://slideno?' + uri;
	},
	slidenewpage : function(uri) {
		document.location = 'papaya://slidenewpage?' + uri;
	},
	slideto : function(uri, direction) {
		if (!direction)direction = 'right';
		document.location = 'papaya://slideto' + direction + '?' + uri;
	},
	slideback : function() {
		document.location = "papaya://slideback";
	},
	createselector : function(jsonvar) {
		window.customselectorstr = toJSONString(jsonvar);
		document.location = 'papaya://createselector~customselectorstr';
	},
	switchtab : function(tabname, uri) {
		document.location = "papaya://switchtab~" + tabname + "?" + uri;
	},
	showkeyboard : function(jsonvar) {
		window.kbjsonstr = toJSONString(jsonvar);
		document.location = 'papaya://showkeyboard~kbjsonstr';
	},
	createkeyboard : function(cb, titl, id, init, returntyp, typ) {
		var jsonvar = {'title':titl ? titl : '','type':typ ? typ : "Default",'callback':cb,'initValue':init ? init : '','returnKeyType':returntyp ? returntyp :
			"Default",'id':id ? id :
			'nosuchelement'};
		window.kbjsonstr = toJSONString(jsonvar);
		document.location = 'papaya://showkeyboard~kbjsonstr';
	},
	showtextarea : function(jsonvar) {
		window.tajsonstr = toJSONString(jsonvar);
		document.location = 'papaya://showmultiplelineinput~tajsonstr';
	},
	createtextarea : function(cb, id, typ, init, returntyp) {
		var jsonvar = {'type':typ ? typ : 'Default','callback':cb,'initValue':init ? init : '','actionbtn':returntyp ? returntyp : '','id':id ? id :
			'nosuchelement'};
		window.tajsonstr = toJSONString(jsonvar);
		document.location = 'papaya://showmultiplelineinput~tajsonstr';
	},
	showphotoselector : function(jsonvar) {
		window.psjsonstr = toJSONString(jsonvar);
		document.location = 'papaya://showpictures~psjsonstr';
	}
};

function PPYRequestQuery() {
	this.query = evalJSON(ppy_string(window.Papaya.requestQuery()));

	this.url = function() {
		return this.query['__current__'];
	};

	this.get = function(key, defaultValue) {
		defaultValue = defaultValue != undefined ? defaultValue : undefined;
		var ret = this.query[key];
		return ret == undefined ? defaultValue : ret;
	};

	this.getInt = function(key, defaultValue) {
		defaultValue = defaultValue != undefined ? defaultValue : -1;
		var v = this.query[key];
		return v == undefined ? defaultValue : parseInt(v);
	};
}
//window.addEventListener('load', genHoriBar, false);

var ppy_html = {
	getValue : function(el) {
		if (!el) return false;
		switch (el.tagName.toLowerCase()) {
			case 'select': return el.value;
			case 'input':
				if ((el.checked && (el.type == 'checkbox' || el.type == 'radio')) || ((el.type == 'hidden' || el.type == 'text' || el.type == 'password')))
					return el.value;
				break;
			case 'textarea': if (el.value != '') return el.value;
		}
		return false;
	},
	html : function(el, html) {
		var oldEl = typeof el === "string" ? document.getElementById(el) : el;
		/*@cc_on // Pure innerHTML is slightly faster in IE
		 oldEl.innerHTML = html;
		 return oldEl;
		 @*/
		var newEl = oldEl.cloneNode(false);
		newEl.innerHTML = html;
		oldEl.parentNode.replaceChild(newEl, oldEl);
		/* Since we just removed the old element from the DOM, return a reference
		 to the new element, which can be used to restore variable references. */
		return newEl;
	},
	forceRefresh : function() {
        //document.body.style.backgroundColor="rgba("+(__background__%255)+",0,0,0)";
        //__background__ = __background__+1;
	}
};

var ppy_funcs = {
	d : function(msg) {
		window.Papaya.d_(String(msg));
	},
	log : function(msg) {
		window.Papaya.log_(String(msg));
	},
	setTitle : function(ctx) {
		window.Papaya.setTitleJson_(toJSONString(ctx));
	},
	convertHtml : function(html) {
		if (html)
			return ppy_string(window.Papaya.convertHtml_(html));
		return html;
	},
	appendUrlQueries : function(url, params) {
		if (params) {
			var tokens = [];
			var i = 0;
			if (url.indexOf("?") < 0)
				tokens.push('?');
			else
				tokens.push('&');
			for (var key in params) {
				if (params.hasOwnProperty(key)) {
					if (i > 0)
						tokens.push('&');
					i++;
					tokens.push(encodeURIComponent(key));
					tokens.push('=');
					tokens.push(encodeURIComponent(params[key]));
				}
			}

			return url + tokens.join("");
		} else
			return url;
	},
	setBadgeText : function(text, index) {
		index = index == undefined ? -1 : index;
		if (index >= 0)
			window.Papaya.badgeTextSet_(text);
		else
			window.Papaya.badgeTextSet_tab_(text, index);
	},
	setUrl : function(url) {
		window.Papaya.setUrl_(url);
	},
	historyCount : function() {
		return window.Papaya.historyCount();
	},
	historyIndex : function() {
		return window.Papaya.historyIndex();
	},
	historyClear : function(mode) {
		mode = mode == undefined ? 1 : mode;
		window.Papaya.historyClear_(mode);
	},
	switchRecyclable : function(on) {
		window.Papaya.switchRecyclable_(on ? 1 : 0);
	},
	switchReusable : function(on) {
		window.Papaya.switchReusable_(on ? 1 : 0);
	},
	isFriend : function(uid) {
		return uid == ppy_client.userId() || window.Papaya.isFriend_(uid);
	},
	goHome : function(uid, slideNew, switchToHome) {
		slideNew = slideNew == undefined ? true : slideNew;
		switchToHome = switchToHome == undefined ? true : switchToHome;
		var slide = slideNew ? "slidenewpage" : "slideno";
		if (uid == ppy_client.userId() || ppy_funcs.isFriend(uid))
			window.Papaya.go("static_friend_home?uid=" + uid);
		else
			window.Papaya.go("static_userinfo?uid=" + uid);
		/*var prefix = switchToHome ? "papaya://switchtab~home?" : "";
		 if (uid == ppy_client.userId())
		 document.location = [prefix, "papaya://", slide, "?static_home"].join("");
		 else if (ppy_funcs.isFriend(uid))
		 document.location = [prefix, "papaya://", slide, "?static_friend_home?uid=", uid].join("");
		 else
		 document.location = [prefix, "papaya://", slide, "?static_userinfo?uid=", uid].join("");*/
	},
	isVisible : function() {
		return window.Papaya.isVisible();
	},
	listFriends : function(onlineOnly) {
		return evalJSON(ppy_string(window.Papaya.listFriends_(onlineOnly ? 1 : 0)));
	},
	cacheUri : function(uri) {
		return ppy_string(window.Papaya.cacheUri_(uri));
	},
	info : function(msg, type) {
		type = type == undefined ? 0 : type;
		window.Papaya.info_type_(msg, type);
	}
};

var ppy_client = {
	identifier : function() {
		return ppy_string(window.Papaya.identifier());
	},
	DUID : function() {
		return ppy_string(window.Papaya.DUID());
	},
	version : function() {
		return window.Papaya.version();
	},
	model : function() {
		return window.Papaya.model();
	},
	lang : function() {
		return ppy_string(window.Papaya.language());
	},
	userId : function() {
		return window.Papaya.userId();
	},
	userNickname : function() {
		return ppy_string(window.Papaya.userNickname());
	},
	setUserNickname : function(nick) {
		window.Papaya.setUserNickname_(nick);
	},
	sessionId : function() {
		return ppy_string(window.Papaya.sessionId());
	},
	login : function(u, p, m) {
		window.Papaya.login_pwd_md5_(u, p, m ? 1 : 0);
	},
	loginTwitter : function(u, p) {
		window.Papaya.login_twitter_(u, p);
	},
	isDebug : function() {
		return window.Papaya.isDebug();
	},
	supportPurchase : function() {
		return window.Papaya.supportPurchase();
	},
	canPurchase : function() {
		return window.Papaya.canPurchase();
	},
	startPurchase : function(id, quantity) {
		if (id == undefined)
			window.Papaya.startPurchase();
		else {
			quantity = (quantity == undefined || quantity < 1) ? 1 : quantity;
			window.Papaya.startPurchase_quantity_(id, quantity);
		}
	},
	containsTransaction : function(id) {
		return id == undefined ? window.Papaya.containsTransaction() : window.Papaya.containsTransaction_(id);
	},
	transactionCount : function() {
		return window.Papaya.transactionCount();
	},
	getTransaction : function(index) {
		return window.Papaya.transaction_(index);
	},
	orientation : function() {
		return window.Papaya.orientation();
	},
	socialAPIKey : function() {return window.Papaya.socialAPIKey();},
    socialAPIVersion : function() {return window.Papaya.socialAPIVersion();},
    poAppID : function() {return window.Papaya.poAppID();},
    appName : function() {return window.Papaya.appName();}, //since v156
    pageDBVersion : function() {return window.Papaya.pageDBVersion();}, //since v156
    urlSuffix : function() {return window.Papaya.urlSuffix();},
    exit : function() {window.Papaya.exit();},
    autoLogin : function() {window.Papaya.autoLogin();}
};