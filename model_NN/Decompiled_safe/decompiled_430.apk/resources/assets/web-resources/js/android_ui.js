function PPYView(id, type, parent, scope) {
    this.type = type;
    this.parent = parent == undefined ? PPYView.PARENT_DEFAULT : parent;
    this.scope = scope == undefined ? PPYView.SCOPE_WEB : scope;
    this.id = this.scope + id;

    this.setFrame = function(x, y, w, h) {
        window.Papaya.viewSet_type_x_y_w_h_(this.id, this.type, x, y, w, h);
    };

    this.setFrameWithEl = function(el) {
        this.setFrame(el.offsetLeft, el.offsetTop, el.offsetWidth, el.offsetHeight);
    };

    this.frame = function() {
        return evalJSON(ppy_string(window.Papaya.viewFrame_type_(this.id, this.type)));
    };

    this.config = function(json) {
        window.Papaya.viewConfig_type_json_(this.id, this.type, toJSONString(json));
    };

    this.configWithString = function(jsonString) {
        window.Papaya.viewConfig_type_json_(this.id, this.type, jsonString);
    };

    this.show = function(animated, p) {
        animated = animated == undefined ? true : animated;
        p = p == undefined ? this.parent : p;
        window.Papaya.viewShow_type_parent_animated_(this.id, this.type, p, animated ? 1 : 0);
    };

    this.hide = function(animated) {
        window.Papaya.viewHide_type_animated_(this.id, this.type, animated ? 1 : 0);
    };

    this.visible = function() {
        return window.Papaya.viewVisible_type_(this.id, this.type) > 0;
    };

    this.getViewProperty = function(key) {
        return evalJSON(ppy_string(window.Papaya.view_type_property_(this.id, this.type, key)));
    };

    this.setViewProperty = function(key, value) {
        window.Papaya.viewSet_type_property_value_(this.id, this.type, key, toJSONString(value));
    };

    this.setViewColorProperty = function(key, color, alpha) {
        alpha = alpha == undefined ? 255 : alpha;
        window.Papaya.viewSetColor_type_property_color_alpha_(this.id, this.type, key, color, alpha);
    };

    this.setViewFontProperty = function(key, fontName, size) {
        window.Papaya.viewSetFont_type_property_fontName_size_(this.id, this.type, key, fontName, size);
    };

    this.destroy = function() {
        window.Papaya.viewDestroy_type_(this.id, this.type);
    };

    this.resetDelegate = function() {
        window.Papaya.viewResetDelegate_type_(this.id, this.type);
    };
}

PPYView.SCOPE_WEB = "";
PPYView.SCOPE_CONTROLLER = "___c";
PPYView.SCOPE_CONTROLLER_PERSIST = "___c__p";
PPYView.SCOPE_APP = "___a";
PPYView.SCOPE_APP_PERSIST = "___a__p";

PPYView.PARENT_DEFAULT = 0;
PPYView.PARENT_WEB = 1;
PPYView.PARENT_CONTROLLER = 2;
PPYView.PARENT_NAVIGATION = 3;
PPYView.PARENT_TABBAR = 4;
PPYView.PARENT_WINDOW = 5;
PPYView.PARENT_CONTENT = 6;

PPYView.TYPE_MENU = 1;
PPYView.TYPE_AVATARBAR = 2;
PPYView.TYPE_INDICATOR = 3;
PPYView.TYPE_ACTIVITY = 4;
PPYView.TYPE_EXCHANGECHIPS = 5;
PPYView.TYPE_HORIBAR = 6;
PPYView.TYPE_SELECTOR_DIALOG = 7;
PPYView.TYPE_ALERTVIEW = 8;
PPYView.TYPE_FACEBOOK_LOGIN_BUTTON = 9;
PPYView.TYPE_WEBVIEW = 10;
PPYView.TYPE_INPUTVIEW = 11;
PPYView.TYPE_MULTI_INPUTVIEW = 12;
PPYView.TYPE_MIXED_INPUT_DIALOG = 13;

function PPYActivityView(id, style, position, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_ACTIVITY, parent == undefined ? PPYView.PARENT_WEB : parent, scope);

    this.style = style;
    this.position = position;
    window.Papaya.activityViewCreate_style_position_(this.id, this.style, this.position);

    this.setText = function (text) {
        this.setViewProperty('text', text);
    };

    this.getText = function () {
        return this.getViewProperty('text');
    };

    this.showWithText = function(text) {
        text = text == undefined ? "Loading..." : text;
        this.setText(text);
        this.show();
    };
}

PPYActivityView.STYLE_FILL = 0;
PPYActivityView.STYLE_WRAP = 1;

PPYActivityView.POSITION_CENTER = 0;
PPYActivityView.POSITION_TOP = 1;
PPYActivityView.POSITION_BOTTOM = 2;

function PPYExchangeChipsView(id, papayas, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_EXCHANGECHIPS, parent, scope);

    this.papayas = papayas;
    window.Papaya.exchangeChipsViewCreate_papayas_(this.id, this.papayas);

    this.setPapayas = function(papayas) {
        this.papayas = papayas;
        window.Papaya.exchangeChipsViewSet_papayas_(this.id, papayas);
    };
}

function PPYHoribar(id, placeHolder, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_HORIBAR, parent, scope);

    window.Papaya.horibarCreate_x_y_w_h_(this.id, placeHolder ? placeHolder.offsetLeft : 0, placeHolder ? placeHolder.offsetTop : 0, placeHolder ? placeHolder.offsetWidth : 320, placeHolder ? placeHolder.offsetHeight : 36);

    this.setActive = function(index) {
        window.Papaya.horibarSet_active_(this.id, index);
    }
}

function PPYAvatarbar(id, placeHolder, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_AVATARBAR, parent, scope);

    window.Papaya.avatarBarCreate_x_y_w_h_(this.id, placeHolder ? placeHolder.offsetLeft : 0, placeHolder ? placeHolder.offsetTop : 0, placeHolder ? placeHolder.offsetWidth : 320, placeHolder ? placeHolder.offsetHeight : 60);

    this.getSelectedIndex = function() {
        return window.Papaya.avatarBarSelectedIndex_(this.id);
    };

    this.setSelectedIndex = function(index, selected) {
        window.Papaya.avatarBarSet_index_selected_(this.id, index, selected ? 1 : 0);
    };

    this.scrollTo = function(index, animated) {
        window.Papaya.avatarBarSet_scroll_animated_(this.id, index, animated ? 1 : 0);
    };
}

function PPYIndicatorView (id, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_INDICATOR, parent, scope);

    window.Papaya.viewCreate_type_(this.id, this.type);

    this.setStyle = function(style) {
        window.Papaya.indicatorSet_style_(this.id, style);
    };

    this.setAnimating = function(animating) {
        window.Papaya.indicatorSet_animating_(this.id, animating ? 1 : 0);
    };
}

function PPYMenuView (id, placeHolder, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_MENU, parent, scope);

    window.Papaya.menuCreate_x_y_w_h_(this.id, placeHolder ? placeHolder.offsetLeft : 0, placeHolder ? placeHolder.offsetTop : 0, placeHolder ? placeHolder.offsetWidth : 150, placeHolder ? placeHolder.offsetHeight : 150);

    this.pack = function() {window.Papaya.menuPack_(this.id);};
    this.reload = function() {window.Papaya.menuReload_(this.id);};
    this.getSelectedIndex = function() {return window.Papaya.menuSelectedIndex_(this.id);};
    this.getSelectedIndices = function() {return evalJSON(window.Papaya.menuSelectedIndices_(this.id));};
    this.clearSelection = function() {window.Papaya.menuClearSelection_(this.id);};
}

function PPYSelectorDialog(id, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_SELECTOR_DIALOG, parent, scope);
    window.Papaya.viewCreate_type_(this.id, this.type);
}

function PPYAlertView(id, parent, scope) {//XXX:
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_ALERTVIEW, parent == undefined ? PPYView.PARENT_NAVIGATION : parent, scope);
    window.Papaya.viewCreate_type_(this.id, this.type);

    this.setText = function(text) {
        this.setViewProperty('text', text);
    };

    this.text = function() {
        return this.getViewProperty('text');
    };

    this.setIcon = function(icon) {
        this.setViewProperty('icon', icon);
    };

    this.setButtons = function(buttons, action) {
        var ctx = [];
        for (var i = 0; i < buttons.length; i++) {
            ctx.push({"text":buttons[i]});
        }
        if (action)
            this.config({"action":action, "buttons":ctx});
        else
            this.config({"buttons":ctx});
    };
}

PPYAlertView.ICON_NONE = -1;
PPYAlertView.ICON_CHECK = 0;
PPYAlertView.ICON_WARN = 1;
PPYAlertView.ICON_HELP = 2;

PPYAlertView.info = function(msg) {
    var alert = new PPYAlertView('__global');
    alert.hide();
    alert.setIcon(PPYAlertView.ICON_CHECK);
    alert.setText(typeof(msg) == "string" ? msg : toJSONString(msg));
    alert.show();
};

PPYAlertView.alert = function(msg) {
    var alert = new PPYAlertView('__global');
    alert.hide();
    alert.setIcon(PPYAlertView.ICON_WARN);
    alert.setText(typeof(msg) == "string" ? msg : toJSONString(msg));
    alert.show();
};

PPYAlertView.help = function(msg) {
    var alert = new PPYAlertView('__global');
    alert.hide();
    alert.setIcon(PPYAlertView.ICON_HELP);
    alert.setText(typeof(msg) == "string" ? msg : toJSONString(msg));
    alert.show();
};

function PPYFacebookLoginButton(id, parent, scope) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_FACEBOOK_LOGIN_BUTTON, parent, scope);
    window.Papaya.viewCreate_type_(this.id, this.type);

    this.setStyle = function(style) {this.setViewProperty('style', style);};
}

function PPYWebView() {
    this.extendsFrom = PPYView;
    this.extendsFrom('__web__', PPYView.TYPE_WEBVIEW, PPYView.PARENT_CONTROLLER, PPYView.SCOPE_WEB);
    window.Papaya.viewCreate_type_(this.id, this.type);
}

function PPYInputView(id) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_INPUTVIEW, PPYView.PARENT_CONTENT, PPYView.SCOPE_WEB);
    window.Papaya.viewCreate_type_(this.id, this.type);
    this.ctx = {};

    this.setTitle = function(title) {this.ctx['title'] = title;};
    this.setKeyboard = function(keyboard) {this.ctx['type'] = keyboard;};
    this.setText = function(text) {this.ctx['initValue'] = text;};
    this.setPlaceholder = function(placeholder) {this.ctx['placeholder'] = placeholder;};
    this.setReturnKey = function(type) {this.ctx['returnKeyType'] = type;};
    this.setSecure = function(secure) {this.ctx['secure'] = secure ? 1:0;};
    this.setCapitalization = function(type) {this.ctx['capitalization'] = type;};
    this.setCallback = function(callback) {this.ctx['action'] = callback;};
    this.refresh = function() {this.config(this.ctx);};
}

function PPYMultiInputView(id) {
    this.extendsFrom = PPYView;
    this.extendsFrom(id, PPYView.TYPE_MULTI_INPUTVIEW, PPYView.PARENT_CONTENT, PPYView.SCOPE_WEB);
    window.Papaya.viewCreate_type_(this.id, this.type);
    this.ctx = {};

    this.setKeyboard = function(keyboard) {this.ctx['type'] = keyboard;};
    this.setText = function(text) {this.ctx['initValue'] = text;};
    this.setReturnKey = function(type) {this.ctx['returnKeyType'] = type;};
    this.setSecure = function(secure) {this.ctx['secure'] = secure ? 1:0;};
    this.setCapitalization = function(type) {this.ctx['capitalization'] = type;};
    this.setDoneTitle = function(title) {this.ctx['actionbtn'] = title;};
    this.setCallback = function(callback) {this.ctx['action'] = callback;};
    this.refresh = function() {this.config(this.ctx);};
}

function PPYMixedInputDialog(id) {
	this.extendsFrom = PPYView;
	this.extendsFrom(id, PPYView.TYPE_MIXED_INPUT_DIALOG, PPYView.PARENT_CONTENT, PPYView.SCOPE_WEB)
	window.Papaya.viewCreate_type_(this.id, this.type);
}