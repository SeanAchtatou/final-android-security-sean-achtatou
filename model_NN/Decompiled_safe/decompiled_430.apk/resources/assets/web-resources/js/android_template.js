(function() {
    var cache = {};
    var tmpls = {};

    this.ppy_tmpl = function ppy_tmpl(str, data, tmplCache) {
        tmplCache = tmplCache == undefined ? true : tmplCache;
        var fn = tmplCache ? tmpls[str] : null;

        if (!fn) {
            fn = !/\W/.test(str) ?
                     cache[str] = cache[str] ||
                                  ppy_tmpl(document.getElementById(str).innerHTML) :
                     new Function("obj",
                             "var p=[],print=function(){p.push.apply(p,arguments);};" +
                             "with(obj){p.push('" +
                             str.replace(/[\r\t\n]/g, " ")
                                     .replace(/'(?=[^%]*%>)/g, "\t")
                                     .split("'").join("\\'")
                                     .split("\t").join("'")
                                     .replace(/<%=(.+?)%>/g, "',$1,'")
                                     .split("<%").join("');")
                                     .split("%>").join("p.push('")
                                     + "');}return p.join('');");
            if (tmplCache) tmpls[str] = fn;
        }
        return data ? fn(data) : fn;
    };
})();