
if (!Array.prototype.each) {
    Array.prototype.each = function(callback/*, scope*/) {
        if (typeof callback != "function")
            throw new TypeError("Array.prototype.each: callback must be a function");
        var t = Object(this),
            scope = arguments[1];
        for (var i = 0, l = t.length; i < l; i++) {
            if (i in t)
                callback.call(scope, t[i], i, t);
        }
    }
}

if (!Array.prototype.reduce) {
    Array.prototype.reduce = function(callback /*, initValue*/) {
        if (this == undefined)
            throw new TypeError("Array.prototype.reduce: no array found.");
        if (typeof callback != "function")
            throw new TypeError("Array.prototype.reduce: callback must be a function.");

        var t = Object(this),
            hasInit = arguments.length > 1;
        if (!t.length && !hasInit)
            throw new TypeError("Reduce of empty array with no initial value");
        var len = t.length,
            ret = hasInit ? arguments[1] : t[0];
        for (var i = hasInit ? 0 : 1; i < len; i++) {
            if (i in t)
                ret = callback(ret, t[i], i, t);
        }

        return ret;
    }
}

if (!Array.prototype.map) {
    Array.prototype.map = function(callback/*, scope */) {
        if (this == undefined)
            throw new TypeError("Array.prototype.map: no array found.");
        if (typeof callback != "function")
            throw new TypeError("Array.prototype.map: callback must be a function.");
        
        var t = Object(this),
            len = t.length,
            ret = new Array(len),
            scope = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in t)
                ret[i] = callback.call(scope, t[i], i, t);
        }
        return ret;
    }
}

if (!Array.prototype.filter) {
    Array.prototype.filter = function(callback/*, scope */) {
        if (this == undefined)
            throw new TypeError("Array.prototype.map: no array found.");
        if (typeof callback != "function")
            throw new TypeError("Array.prototype.map: callback must be a function.");

        var t = Object(this),
            ret = [],
            scope = arguments[1],
            val;
        for (var i = 0, l = t.length; i < l; i++) {
            if (i in t) {
                val = t[i];
                if (callback.call(scope, val, i, t))
                    ret.push(val);
            }
        }
        return ret;
    }
}

if (!Function.prototype.bind) {
    Function.prototype.bind = function(scope/*, arg1, arg2, ...*/) {
        var fn = this;
        if (typeof fn != "function")
            throw TypeError("Function.prototype.bind: must be a function.");
        var _slice = Array.prototype.slice,
            args = _slice.call(arguments, 1);
        return function() {
            fn.apply(scope, args.concat(_slice.call(arguments, 0)));   
        }
    }
}
String.prototype.test = function(regex, params) {
    return ((typeof(regex) == 'string') ? new RegExp(regex, params) : regex).test(this);
};
String.prototype.replaceAll = function(AFindText, ARepText) {
    var raRegExp = new RegExp(AFindText, "g");
    return this.replace(raRegExp, ARepText);
};
String.prototype.sliceBetween = function(start, end) {
    return this.slice(this.indexOf(start) + start.length, this.indexOf(end));
};
String.prototype.contains = function(string, s) {
    return(s) ? (s + this + s).indexOf(s + string + s) > -1 : this.indexOf(string) > -1;
};
String.prototype.clean = function() {
    return this.replace(/\s{2,}/g, ' ').trim();
};
if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}
String.prototype.toInt = function() {
    return parseInt(this);
};
String.prototype.startsWith = function(s) {
    return this.substr(0, s.length) == s;
};

String.prototype.dasherize = function() {
    return this.replace(/[A-Z]/g, function(m){return '-' + m.toLowerCase();});
};
String.prototype.camelize = function() {
    return this.replace(/\-[a-z]/g, function(m) {return m[1].toUpperCase();});
};

/**
 * Format a string
 *
 * syntax:
 * "What the %s are you talking about %s".format("hell", 123)
 * will be:  "What the hell are you talking about 123"
 */
String.prototype.format = function() {
    var args = arguments, i = 0;
    return this.replace(/%s/g, function() {
        return String(args[i++]) || '';   
    });
};
/**
 * Envolving from Zepto
 */
(function() {
    
var document = window.document,
    undefined,
    _push = [].push,
    _slice = [].slice,
    tagExpr = /<([\w:]+)/,
    classRE = function(name){ return new RegExp("(^|\\s)"+name+"(\\s|$)") },
    _ = function(q, context) {
        var els = [], tmpNode, list, i, l;
        
        if (!q)
            return this;
        if (typeof q == "string") {
            if (tagExpr.test(q)) {
                tmpNode = document.createElement("i");
                tmpNode.innerHTML = q;
                list = tmpNode.getElementsByTagName('SCRIPT');
                for (i = 0, l = list.length; i < l; i++) {
                    $.globalEval(list[i].text);
                }
                _push.apply(els, _slice.call(tmpNode.childNodes));
            } else {
                els = _query(q, context);
            }
        } else if (q instanceof Array) {
            els = q;
        } else if (q.toString() == '[object NodeList]') {
            els = _slice.call(q);
        } else if (q.nodeName && q.nodeType || q === window) {
            els = [q];
        } else if (q instanceof _) {
            return q;
        }

        _push.apply(this, els);
    },
    _query = function(q, context) {
        if (typeof q == "string" && q.trim() == '') return [];
        context = context || document;
        return typeof context.querySelectorAll == "function" ? _slice.call(context.querySelectorAll(q)) : [];
    },
    extend = function(target, src) {
        var k;
        for (k in src) {target[k] = src[k];}
    },
    generateUniqueId = function() {
        return "" + Date.now() + (Math.round(Math.random() * 89999) + 10000);  
    },
    getElementPPYId = function(el) {
        return el.__ppyid == undefined ? (el.__ppyid = generateUniqueId()) : el.__ppyid;
    }, getElText = function(elems) {
        var ret = "", el;
        for (var i = 0; elems[i]; i++) {
            el = elems[i];
            if (el.nodeType == 3 || el.nodeType == 4) {
                ret += el.nodeValue;
            } else {
                ret += getElText(el.childNodes);
            }
        }
        return ret;
    };

/**
 * Interface object of ppy lib
 */
window.ppy = window.$ = function(q, context) {
    return new _(q, context);   
};

$.extend = extend;

$.generateUniqueId = generateUniqueId;

$.getElementPPYId = getElementPPYId;

// A safely toString method
$.toString = function(s) {
    if (s && typeof s != "string" || s == 0) return String(s);
    return s;
};

$.fn = $.prototype = _.prototype = {
    length: 0,

    extend: function(o) {
        for (var i in o) {ppy.fn[i] = o[i];}
    },
    
    find: function(q) {
        var els = [];
        this.each(function(el) {
            els = els.concat(_query(q, el));
        });
        return new _(els);
    },

    /**
     * traverse the elements, if callback return false, stop loop
     * @param callback
     */
    each: function(callback) {
        for (var i = 0, l = this.length, el; i < l; i++) {
            el = this[i];
            if (el && callback.call(el, el, i, this) === false) break;
        }
        return this;
    },

    map: function(callback) {
        return new _([].map.call(this, callback), null);
    },

    /**
     * Get elements from current collection filter by selector or function
     * @param s     selector or function
     */
    filter: function(s) {
        var isString = typeof s == "string";
        return new _([].filter.call(this, function(el, i, self) {
            return isString ? _query(s, el.parentNode).indexOf(el) >= 0 : s.call(el, el, i, self);
        }), null);
    },

    /**
     * Get a single element by index
     * @param idx
     */
    eq: function(idx) {
        return new _(this[idx] || null);   
    },

    /**
     * Check if all selected elements match the given selector
     * @param s     selector
     */
    is: function(s) {
        var ret = true;
        this.each(function(el) {
            return (ret = _query(s, el.parentNode).indexOf(el) >= 0);
        });
        return ret
    },

    /***************************************************
     * Traverse 
     ***************************************************/

    parent: function() {
        return this[0] ? new _(this[0].parentNode) : this;
    },

    parents: function() {
        var els = [], p = this[0] && this[0].parentNode;
        while(p && p !== document.documentElement) {
            els.push(p);
            p = p.parentNode;
        }
        return new _(els);
    },

    children: function() {
        var els = [], p = this[0];
        if (p && p.hasChildNodes())
            els = _slice.call(p.childNodes);
        return new _(els);
    },

    /***************************************************
     * Attribute and CSS style
     ***************************************************/

    /**
     * Get or set html elements' css style
     * @param n     style name
     * @param v     style value
     */
    css: function(n, v) {
        if (v == undefined && typeof n == "string")
            return document.defaultView.getComputedStyle(this[0], null).getPropertyValue(n.dasherize());
        if (typeof n == "string") {
            n = n.camelize();
            return this.each(function(el) {
                if (el.style)
                    el.style[n] = v
            });
        } else if(Object.prototype.toString.call(n) == "[object Object]") {
            for (var k in n)
                this.css(k, n[k]);
        }
        return this;
    },

    /**
     * Get or set html elements' attribute
     * @param n     attribute name
     * @param val   attribute value
     */
    attr: function(n, val) {
        return typeof n == "string" && val == undefined ?
            (this[0] && this[0][n]) :
            this.each(typeof n == "object" ? function(el) {
                for (var k in n) {el.setAttribute(k, n[k])}
            } : function(el) {el.setAttribute(n, val);});
    },

    /**
     * Get or set a html input's value
     * @param v
     */
    val: function(v) {
        return typeof v === "undefined" ?
            (this[0] && this[0].value) :
            this.each(function(el) {
                el.value = v;
            });
    },

    /**
     * Get or set inner text of matched html elements
     * @param text
     */
    text: function(text) {
        if (typeof text == "string") {
            return this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode( text ));
        }
        return getElText(this);
    },

    /**
     * Set the all matched elements' inner html if val is not null,
     * or return the first matched element's inner html
     * @param val
     */
    html: function(val) {
        if (val === undefined) {
            return this[0] && this[0].innerHTML || null;
        } else if (typeof val == "string") {
            var list, i, l;
            this.empty().each(function(el) {
                el.innerHTML = val;
                // eval script
                list = el.getElementsByTagName('SCRIPT');
                for (i = 0, l = list.length; i < l; i++) {
                    $.globalEval(list[i].text);
                }
            });   
        } else {
            this.empty().append(val);
        }
        return this;
    },

    hasClass: function(c) {
        return this[0] && classRE(c).test(this[0].className) || false;   
    },

    addClass: function(c) {
        return this.each(function(el) {
            if (!$(el).hasClass(c)) {
                el.className = (el.className + " " + c).trim();
            }
        });
    },

    removeClass: function(c) {
        return this.each(function(el) {
            el.className = el.className.replace(classRE(c), ' ').trim();
        });  
    },

    saveStyle: function(n) {
        return typeof n == "string" ? this.each(function(el) {
            el.__savedStyle = el.__savedStyle || {};
            el.__savedStyle[n] = $(el).css(n);
        }) : this;
    },

    restoreStyle: function(n) {
        return typeof n == "string" ? this.each(function(el) {
            el.__savedStyle && el.__savedStyle[n]
                && $(el).css(n, el.__savedStyle[n]);
        }) : this;
    },

    /**
     * Get an element's offset position(left, top) from rel
     * @param rel   relative element
     */
    pos: function(rel) {
        if (!this[0]) return null;
        var el = this[0], ret = {left: 0, top: 0}, body = document.body;
        if (el == body || el == document || el == window || el == document.documentElement)
            return ret;
        if (rel) {
            if (rel == document || rel == window) rel = document.body;
            do {
                ret.left += el.offsetLeft;
                ret.top += el.offsetTop;
                el = el.offsetParent;
            } while (el && el != rel);
            return ret;
        } else {
            return {left: this[0].offsetLeft, top: this[0].offsetTop};
        }
    },

    /**
     * Get an element's offset width and height
     */
    dim: function() {
        if (!this[0]) return null;
        return {width: this[0].offsetWidth, height: this[0].offsetHeight};
    },

    /**
     * Check if an element's box area contains a certain point
     * @param x     x of point
     * @param y     y of point
     */
    containPos: function(x, y) {
        if (!this[0]) return false;
        var p = this.pos(document), d = this.dim(), left = p.left, top = p.top;
        return x > left && x < left + d.width && y > top && y < top + d.height;
    },

    /***************************************************
     * Manipulate 
     ***************************************************/

    append: function(elements) {
        var p = this[0];
        if (!p) return this;
        $(elements).each(function(el) {
            p.appendChild(el);
        });
        return this;
    },

    appendTo: function(el) {
        var p = $(el)[0];
        if (!p) return this;
        return this.each(function(el) {
            p.appendChild(el);   
        });
    },

    prepend: function(elements) {
        var p = this[0];
        if (!p) return this;
        var f = p.firstChild;
        $(elements).each(function(el) {
            p.insertBefore(el, f);
        });
        return this;
    },

    prependTo: function(el) {
        var p = $(el)[0];
        if (!p) return this;
        var f = p.firstChild;
        return this.each(function(el) {
            p.insertBefore(el, f);  
        });
    },

    clone: function() {
        return this.map(function(el) {
            return el.cloneNode(true);   
        });
    },

    remove: function() {
        return this.each(function(el) {
            if (el && el.parentNode)
                el.parentNode.removeChild(el);
        });  
    },

    /**
     * Remove all child nodes of the matched elements
     */
    empty: function() {
        for (var i = 0, elem; (elem = this[i]) != null; i++) {
            while(elem.firstChild)
                elem.removeChild(elem.firstChild);
        }
        return this;
    }
};

// Detect the os version, from zepto
function detect(ua){
    var os = {},
        android = ua.match(/(Android)\s+([0-9a-zA-Z\.]+)/),
        iphone = ua.match(/(iPhone\sOS)\s([0-9_]+)/),
        ipad = ua.match(/(iPad).*OS\s([0-9_]+)/),
        webos = ua.match(/(webOS)\/([0-9\.]+)/);
    if(android) os.android = true, os.version = android[2];
    if(iphone) os.ios = true, os.version = iphone[2].replace(/_/g,'.'), os.iphone = true;
    if(ipad) os.ios = true, os.version = ipad[2].replace(/_/g,'.'), os.ipad = true;
    if(webos) os.webos = true, os.version = webos[2];
    return os;
}
$.os = detect(navigator.userAgent);

// Convert an object to json string
var _strre = /[\x00-\x1f\\"]/,
    stringescape = {
        '\b': '\\b',
        '\t': '\\t',
        '\n': '\\n',
        '\f': '\\f',
        '\r': '\\r',
        '"' : '\\"',
        '\\': '\\\\'
    }, toJSONString = function(obj, secure) {
        var a, i, l;
        switch(typeof obj) {
            case "string":
                return ['"', (_strre.test(obj) ? encodeString(obj) : obj), '"'].join('');
            case 'number':
            case 'boolean':
                return String(obj);
            case "object":
                if (obj) {
                    switch (obj.constructor) {
                        case Array:
                            a = [];
                            for (i = 0, l = obj.length; i < l; i++)
                                a.push(toJSONString(obj[i], secure));
                            return ['[', a.join(','), ']'].join('');
                        case Object:
                            a = [];
                            if (secure) {
                                for (i in obj)
                                    if (obj.hasOwnProperty(i))
                                        a[a.length] = ['"', _strre.test(i) ? encodeString(i) : i, '":', toJSONString(obj[i], secure)].join('');
                            } else {
                                for (i in obj)
                                    a[a.length] = ['"', i, '":', toJSONString(obj[i], secure)].join('');
                            }
                            return ['{', a.join(','), '}'].join('');
                        case String:
                            return ['"', (_strre.test(obj) ? encodeString(obj) : obj), '"'].join('');
                        case Number:
                        case Boolean:
                            return String(obj);
                        case Function:
                        case Date:
                        case RegExp:
                            return 'undefined';
                    }
                }
                return 'null';
            case 'function':
            case 'undefined':
            case 'unknown':
                return 'undefined';
            default:
                return 'null';
        }
    }, encodeString = function(string) {
        return string.replace(
            /[\x00-\x1f\\"]/g, function(a) {
                var b = stringescape[a];
                if (b) return b;
                b = a.charCodeAt();
                return ['\\u00' , Math.floor(b / 16).toString(16) , (b % 16).toString(16)].join('');
            });
    };
$.toJSONString = toJSONString;
$.evalJSON = function(str, secure) {
    if (secure)
        return ((typeof(str) != 'string') || !str.test(/^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/)) ? str : eval('(' + str + ')');
    else
        return eval(['(', str, ')'].join(''));
};

$.globalEval = function(data) {
    if (data && /\S/.test(data)) {
        var head = document.getElementsByTagName("head")[0] || document.documentElement,
            script = document.createElement("script");
        script.type = "text/javascript";
        script.appendChild(document.createTextNode(data));
        head.insertBefore(script, head.firstChild);
        head.removeChild(script);
    }
}
})();

(function($) {

    var eventCache = {},
        getElementEventCache = function(el) {
            var id = $.getElementPPYId(el);
            return eventCache[id] || (eventCache[id] = {});
        },
        getDelegates = function(el, type) {
            var c = getElementEventCache(el);
            return c[type] || (c[type] = []);
        },
        createDelegate = function(el, type, fn) {
            var t = getDelegates(el, type),
                delegate = function(e) {
                    if (fn.call(el, e) === false) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                };
            delegate.handler = fn;
            t.push(delegate);
            return delegate;
        };

    $.fn.extend({
        /**
         * Bind event handler to element
         * @param type   event type
         * @param fn   event handler, must be a function
         */
        on: function(type, fn) {
            return this.each(function(el) {
                var delegate = createDelegate(el, type, fn);
                el.addEventListener(type, delegate, false);
            });
        },

        /**
         * Unbind event handler from element
         * @param type
         * @param fn
         */
        un: function(type, fn) {
            if (typeof type != "string") return;
            return this.each(function(el) {
                var delegates = getDelegates(el, type), i = delegates.length, d;
                if (!delegates.length) return;
                while(i--) {
                    d = delegates[i];
                    if (fn == undefined || d.handler === fn) {
                        delegates.splice(i, 1);
                        el.removeEventListener(type, d, false);
                    }
                }
            });
        },

        /**
         * Fire a given event from element
         * @param type
         */
        fire: function(type) {
            return this.each(function(el) {
                if (el == document && !el.dispatchEvent)
                    el = document.documentElement;
                var event = document.createEvent(type);
                event.initEvent(type, true, true);
                event.eventName = type;
                el.dispatchEvent(event);
            });
        }
    });
})(ppy);
(function($) {

    /**
     * changed per step: timer cur
     * need to keep: elements
     */
    var fxElements = {},    // fxId: elements
        fxTimer = {},   // fxId: timer
        fxState = {},   // fxId: state [elStyle, prop, start, end, fn, unit]
        interval = 30,
        normalProps = ('backgroundColor borderBottomColor borderBottomWidth borderLeftColor borderLeftWidth '+
            'borderRightColor borderRightWidth borderSpacing borderTopColor borderTopWidth bottom color fontSize '+
            'fontWeight height left letterSpacing lineHeight marginBottom marginLeft marginRight marginTop maxHeight '+
            'maxWidth minHeight minWidth opacity outlineColor outlineOffset outlineWidth paddingBottom paddingLeft '+
            'paddingRight paddingTop right textIndent top width wordSpacing zIndex').split(' '),
        parseEl = document.createElement("div"),
        clearCache = function(id) {
            delete fxElements[id];
            delete fxTimer[id];
            delete fxState[id];
        },
        findAndCancelCacheByElId = function(elId) {
            var k, f;
            for (k in fxElements) {
                if (fxElements[k].indexOf(elId) >= 0 && (f = k)) break;
            }
            if (f) {
                clearTimeout(fxTimer[f].timer);
                clearCache(f);
            }
        },
        interpolate = function(source,target,pos){ return (source+(target-source)*pos).toFixed(3); },
        s = function(str, p, c){ return str.substr(p,c||1); },
        color = function(source,target,pos){
            var i = 2, j, c, tmp, v = [], r = [];
            while(j=3,c=arguments[i-1],i--)
                if(s(c,0)=='r') { c = c.match(/\d+/g); while(j--) v.push(~~c[j]); } else {
                    if(c.length==4) c='#'+s(c,1)+s(c,1)+s(c,2)+s(c,2)+s(c,3)+s(c,3);
                    while(j--) v.push(parseInt(s(c,1+j*2,2), 16));
                }
                while(j--) { tmp = ~~(v[j+3]+(v[j]-v[j+3])*pos); r.push(tmp<0?0:tmp>255?255:tmp); }
                return 'rgb('+r.join(',')+')';
        },
        parse = function(prop) {
            var p = parseFloat(prop), q = prop.replace(/^[\-\d\.]+/,'');
            return isNaN(p) ? { v: q, f: color, u: ''} : { v: p, f: interpolate, u: q };
        },
        prepare = function(fx){
            var parsed = parse(fx.start);
            fx.start = parsed.v;
            fx.unit = parsed.u;
            fx.fn = parsed.f;
            parsed = parse(fx.end);
            fx.end = parsed.v;
        },
        normalize = function(style){
            var css, rules = {}, i = normalProps.length, v, serialisedStyle = [], key;
            if (typeof style != "string") {
                for (key in style) {
                    serialisedStyle.push(key.dasherize() + ':' + style[key]);
                }
                serialisedStyle = serialisedStyle.join(';');
            } else {
                serialisedStyle = style;
            }
            parseEl.innerHTML = '<div style="'+serialisedStyle+'"></div>';
            css = parseEl.childNodes[0].style;
            while(i--) if(v = css[normalProps[i]]) rules[normalProps[i]] = v;
            return rules;
        },
        go = function(id, fxList, opts) {
            opts = opts || {};
            var timer, duration = opts.duration || 1000,
                startTime = +new Date,
                endTime = startTime + duration,
                easing = opts.easing || function(pos){ return (-Math.cos(pos*Math.PI)/2) + 0.5; },
                step = function() {
                    var time = +new Date(), pos = time > endTime ? 1 : (time - startTime) / duration,
                        len = fxList.length, i = 0, fx;
                    for (;i < len;i++) {
                        fx = fxList[i];
                        fx.elStyle[fx.prop] = fx.fn(fx.start, fx.end, easing(pos)) + fx.unit;
                    }
                    if (time < endTime) {
                        timer = setTimeout(step, interval);
                        fxTimer[id] = timer;
                    } else {
                        clearCache(id);
                        if (typeof opts.onafter == "function")
                            opts.onafter.call(null);
                    }
                };
            fxState[id] = fxList;
            timer = setTimeout(step, interval);
            fxTimer[id] = timer;
        };

    $.fn.extend({
        /**
         * Effects
         * @param props     The properties to be changed
         * @param options   duration, easing, onafter
         */
        anim: function(props, options) {
            if (!this[0]) return;
            options = options || {};
            var fxList = [],
                cancelPre =  true,  // Just cancel all, since I have not implement fx chain
                k, id = $.generateUniqueId(), els = [];

            // Normalize the props first
            props = normalize(props);

            this.each(function(el) {
                var elId = $.getElementPPYId(el), fx;
                cancelPre && findAndCancelCacheByElId(elId);
                els.push(elId);
                for (k in props) {
                    fx = {
                        elStyle: el.style,
                        prop: k.camelize(),
                        start: $(el).css(k),
                        end: props[k]
                    };
                    prepare(fx);
                    fxList.push(fx);
                }
            });
            fxElements[id] = els;
            go(id, fxList, options);

            return this;
        },

        /**
         * Display the matched elements or fading them to opaque. 
         * @param opts
         */
        show: function(opts) {
            opts = opts || {};
            if (opts.duration && opts.duration > 0)
                this.css("opacity", 0);
            if (this.restoreStyle("display").css("display") == "none")
                this.css("display", "block");
            if (opts.duration && opts.duration > 0) {
                this.anim({
                    opacity: 1
                }, opts);
            } else {
                this.css("opacity", 1);
            }
            return this;
        },

        /**                                                                                        
         * Hide the matched elements or fading them to transparent.
         * @param opts  see anim options
         * if opts.duration > 0, it will be faded out
         */
        hide: function(opts) {
            opts = opts || {};
            var onafter = opts.onafter || function() {}, self = this;
            opts.onafter = function() {
                onafter(arguments);
                self.css("display", "none");
            };
            this.saveStyle("display");
            if (opts.duration && opts.duration > 0) {
                this.anim({
                    opacity: 0
                }, opts);
            } else {
                opts.onafter();  
            }
            return this;
        }
    });
})(ppy);

(function($) {
    $.fn.extend({
        /**
         * Make an element draggable
         * if you use beginDrag to start a drag, you can only drag one element
         * @param opts
         * trigger      use what to trigger drag? can be mouse and finger, default to finger
         * onbefore     callback before drag starting
         * onmove       callback when element is moving
         * onafter      callback after drag ended
         */
        draggable: function(opts) {
            opts = opts || {};
            var trigger = opts.trigger || "finger",
                start = trigger == "finger" ? "touchstart" : "mousedown",
                move = trigger == "finger" ? "touchmove" : "mousemove",
                end = trigger == "finger" ? "touchend" : "mouseup",
                moveFn,
                onbefore = opts.onbefore || false,
                onmove = opts.onmove || false,
                onafter = opts.onafter || false,
                bindDocEvent = function(el) {
                    var endFn = function(e) {
                        onafter && onafter.call(el, e);
                        $(document).un(move, moveFn).un(end, endFn);
                        return false;
                    };
                    $(document).on(move, moveFn).on(end, endFn);
                };

            // Set a handler to begin drag, this method can only drag one element
            this.beginDrag = function() {
                var startTriggerX = false, startTriggerY = false,
                    self = this[0],
                    selfStyle = self.style,
                    startElPosLeft = $(self).pos().left,
                    startElPosTop = $(self).pos().top;
                moveFn = function(e) {
                    onmove && onmove.call(self, e);
                    startTriggerX === false && (startTriggerX = e.pageX);
                    startTriggerY === false && (startTriggerY = e.pageY);
                    selfStyle.left = startElPosLeft + e.pageX - startTriggerX + 'px';
                    selfStyle.top = startElPosTop + e.pageY - startTriggerY + 'px';
                    return false;
                };
                bindDocEvent(self);
            };

            this.eq(0).css("position", "absolute").on(start, function(e) {
                onbefore && onbefore.call(this, e);
                var startTriggerX= e.pageX,
                    startTriggerY = e.pageY,
                    self = this,
                    selfStyle = this.style,
                    startElPosLeft = $(this).pos().left,
                    startElPosTop = $(this).pos().top;
                moveFn = function(e) {
                    onmove && onmove.call(self, e);
                    selfStyle.left = startElPosLeft + e.pageX - startTriggerX + 'px';
                    selfStyle.top = startElPosTop + e.pageY - startTriggerY + 'px';
                    return false;
                };
                bindDocEvent(self);
                return false;
            });
            return this;
        }
    });
})(ppy);
(function($){
    var cache = {}, tmpls = {}, elTmpls = {},
        selectorRe = /^.[^<%:#\[\.,]*$/,
        _tmpl = function(str, data, tmplCache) {
            tmplCache = tmplCache == undefined ? true : tmplCache;
            var fn = tmplCache ? tmpls[str] : null;

            if (!fn) {
                fn = !/\W/.test(str) ?
                         cache[str] = cache[str] ||
                                      $.tmpl(document.getElementById(str).innerHTML) :
                         new Function("obj",
                                 "var p=[],print=function(){p.push.apply(p,arguments);};" +
                                 "with(obj){p.push('" +
                                 str.replace(/[\r\t\n]/g, " ")
                                         .replace(/'(?=[^%]*%>)/g, "\t")
                                         .split("'").join("\\'")
                                         .split("\t").join("'")
                                         .replace(/<%=(.+?)%>/g, "',$1,'")
                                         .split("<%").join("');")
                                         .split("%>").join("p.push('")
                                         + "');}return p.join('');");
                if (tmplCache) tmpls[str] = fn;
            }
            return data ? fn(data) : fn;
        };

    $.tmpl = _tmpl;

    $.fn.extend({
        /**
         * $(selector).tmpl({...options...})
         * options:
         * str:             template str or template container's selector(#id, .class)
         * data:            data apply to template
         * iterateArray:    true to iterate an array data, default to true
         * append           true to append new html elemnts, false to replace, default to false
         * rendering:       callback when data is rendering
         * rendered:        callback when data has been rendered
         * @param opts
         */
        tmpl: function(opts) {
            opts = opts || {};
            var str = opts.str,
                data = opts.data,
                iterateArray = opts.iterateArray !== false,
                append = opts.append || false,
                rendering = opts.rendering,
                rendered = opts.rendered,
                el = this[0], dom, i, l,
                applyData = function(context) {
                    if (typeof rendering == "function" && rendering.call(this, context) === false) {
                        return;
                    }
                    dom = $(fn(context.data));
                    this.append(dom);
                    if (typeof rendered == "function")
                        rendered.call(this, dom, context);   
                };
            if (!el)
                throw Error("$.tmpl: no element found.");
            var elID = $.getElementPPYId(el),
                fn = str ? _tmpl(str) : elTmpls[elID];
            if (!fn)
                throw Error("$.tmpl: no template found for element.");
            else
                elTmpls[elID] = fn;
            if (data) {
                el = $(el);
                if (!append) el.empty();
                if (iterateArray && data.constructor == Array && data.length) {
                    for (i = 0, l = data.length; i < l; i++) {
                        applyData.call(el, {data: data[i], index: i});
                    }
                } else {
                    applyData.call(el, {data: data});  
                }
            }
            return this;
        }
    });

})(ppy);

(function($, $b) {
    if (!$b) return;

    // Add a link to global $ object
    $.bridge = $.b = {
        d : function(msg) {
            $b.d_(String(msg));
        },
        log : function(msg) {
            $b.log_(String(msg));
        },
        setTitle : function(ctx) {
            $b.setTitleJson_($.toJSONString(ctx));
        },
        convertHtml : function(html) {
            if (html)
                return $.toString($b.convertHtml_(html));
            return html;
        },
        appendUrlQueries : function(url, params) {
            if (params) {
            var tokens = [];
            var i = 0;
            if (url.indexOf("?") < 0)
                tokens.push('?');
            else
                tokens.push('&');
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    if (i > 0)
                        tokens.push('&');
                    i++;
                    tokens.push(encodeURIComponent(key));
                    tokens.push('=');
                    tokens.push(encodeURIComponent(params[key]));
                }
            }
    
            return url + tokens.join("");
        } else
            return url;
        },
        setBadgeText : function(text, index) {
            index = index == undefined ? -1 : index;
            if (index >= 0)
                $b.badgeTextSet_(text);
            else
                $b.badgeTextSet_tab_(text, index);
        },
        setUrl : function(url) {
            $b.setUrl_(url);
        },
        historyCount : function() {
            return $b.historyCount();
        },
        historyIndex : function() {
            return $b.historyIndex();
        },
        historyClear : function(mode) {
            mode = mode == undefined ? 1 : mode;
            $b.historyClear_(mode);
        },
        switchRecyclable : function(on) {
            $b.switchRecyclable_(on?1:0);
        },
        switchReusable : function(on) {
            $b.switchReusable_(on?1:0);
        },
        isFriend : function(uid) {
            return uid == $.bridge.userId() || $b.isFriend_(uid);
        },
        goHome : function(uid, slideNew, switchToHome) {
            slideNew = slideNew == undefined ? true : slideNew;
            switchToHome = switchToHome == undefined ? true : switchToHome;
            var slide = slideNew ? "slidenewpage" : "slideno";
            var prefix = switchToHome ? "papaya://switchtab~home?" : "";
            if (uid == $.bridge.userId())
                document.location = [prefix, "papaya://", slide, "?static_home"].join("");
            else if ($.bridge.isFriend(uid))
                document.location = [prefix, "papaya://", slide, "?static_home?uid=", uid].join("");
            else
                document.location = [prefix, "papaya://", slide, "?static_userinfo?uid=", uid].join("");
        },
        goLBSFriend: function(uid, slideNew, switchToHome) {
            if (uid != $.bridge.userId() && $.bridge.isFriend(uid))
                document.location = ["", "papaya://", "slidenewpage", "?static_friendlbsinfo?uid=", uid].join("");
            else if(uid != $.bridge.userId())
                document.location = ["papaya://switchtab~home?", "papaya://", "slidenewpage", "?static_userinfo?uid=", uid].join("");    
        }, 
        isVisible : function() {
            return $b.isVisible();
        },
        listFriends : function(onlineOnly) {
            return $.evalJSON($.toString($b.listFriends_(onlineOnly ? 1 : 0)));
        },
        cacheUri : function(uri) {
            return $.toString($b.cacheUri_(uri));
        },
        info : function(msg, type) {
            type = type == undefined ? 0 : type;
            $b.info_type_(msg, type);
        },
        identifier : function() {return ppy_string($b.identifier());},
        DUID : function() {return ppy_string($b.DUID());},
        version : function() {return $b.version();},
        model : function() {return $b.model();},
        lang : function() {return ppy_string($b.language());},
        userId : function() {return $b.userId();},
        userNickname : function() {return ppy_string($b.userNickname());},
        setUserNickname : function(nick) {$b.setUserNickname_(nick);},
        sessionId : function() {return String($b.sessionId());},
        login : function(u, p, m) {$b.login_pwd_md5_(u, p, m?1:0);},
        loginTwitter : function(u, p) {$b.login_twitter_(u, p);},
        isDebug : function() {return $b.isDebug();},
        supportPurchase : function() {return $b.supportPurchase();},
        canPurchase : function() {return $b.canPurchase();},
        startPurchase : function(id, quantity) {
            if (id == undefined)
                $b.startPurchase();
            else {
                quantity = (quantity == undefined || quantity < 1) ? 1 : quantity;
                $b.startPurchase_quantity_(id, quantity);
            }
        },
        containsTransaction : function(id) {return id == undefined ? $b.containsTransaction() : $b.containsTransaction_(id);},
        transactionCount : function() {return $b.transactionCount();},
        getTransaction : function(index) {return $b.transaction_(index);},
        orientation : function() {return $b.orientation();}, 
        gaTrackPage: function(pageName) {
            if ($b.ga_track_page) {
                $b.ga_track_page(pageName);
                return 1;    
            } else {
                return 0; 
            }
        }, 
        gaTrackEvent: function(category, action, label, value) {
            if ($b.ga_track_event) {
                $b.ga_track_event(category, action, label, value);
                return 1;
            } else {
                return 0;  
            }
        },

        go: function(link, tabName) {
            tabName ? $b.go(link, tabName) : $b.go(link);
        }
    };
})(ppy, window.Papaya);
(function($, $b) {
    var query, pageMethodCache = {};

    $.getRequestParam = function(name, defaultValue) {
        if (!query)
            query = $.evalJSON(String($b.requestQuery()));
        defaultValue = defaultValue || undefined;
        var ret = query[name];
        return ret == undefined ? defaultValue : ret;
    };

    $.getRequestIntParam = function(name, defaultValue) {
        var ret = $.getRequestParam(name, defaultValue);
        return ret == undefined ? ret : parseInt(ret);
    };

    /**
     * Add webloading, webloaded, webappeared method to context
     * @param name
     */
    function addPageInitMethod(name, fn) {
        if (typeof window[name] != "function") {
            window[name] = function() {
                pageMethodCache[name].each(function(cb) {
                    cb.call();
                });
            }
        }
        pageMethodCache[name].push(fn);
    }

    ["webloading", "webloaded", "webappeared"].each(function(name) {
        pageMethodCache[name] = [];
        $[name] = function(fn) {
            addPageInitMethod(name, fn);
        }
    });

})(ppy, window.Papaya);

/**
 * Papaya client database bridge
 */
(function($, $b) {
    if (!$b) return;

    var _db = function(id, scope) {
        this.id = id;
        this.scope = scope;
        $b.db_scope_(id, scope);

        this.kvSave = function (key, value, life) {
            life = life != undefined ? life : -1;
            $b.db_scope_key_value_life_(this.id, this.scope, key, value, life);
        };

        this.kvUpdate = function(key, value) {
            this.kvExpireBatch(key+"__tmpl__%");
            $b.db_scope_key_value_(this.id, this.scope, key, value);
        };

        this.kvLoad = function (key, defaultValue) {
            var v = String($b.db_scope_key_(this.id, this.scope, key));
            return (v == undefined || v == null) ? defaultValue : v;
        };

        this.kvLoadInt = function (key, defaultValue) {
            defaultValue = defaultValue != undefined ? defaultValue : -1;
            var value = this.kvLoad(key);
            if (value)
                return parseInt(value);
            else
                return defaultValue;
        };

        this.kvExpire = function (key) {
            if (key)
                return this.update('delete from kv where key=?', [key]);
            return false;
        };

        this.kvExpireBatch = function (keyPattern) {
            if (keyPattern)
                this.update('delete from kv where key like ?', [keyPattern]);
        };

        this.update = function(sql, arguments) {
            arguments = arguments == undefined ? '' : arguments;
            return $b.db_scope_update_arguments_(this.id, this.scope, sql, $.toJSONString(arguments));
        };

        this.query = function(sql, arguments) {
            arguments = arguments == undefined ? '' : arguments;
            return $.evalJSON($.toString($b.db_scope_query_arguments_(this.id, this.scope, sql, $.toJSONString(arguments))));
        };
    };

    $.db = function(id, scope) {
        return new _db(id, scope);   
    };

    $.db.NAME_CONNECTION = "__connection__";
    $.db.NAME_SESSION = "__session__";

    $.db.SCOPE_UNDEFINED = 0;
    $.db.SCOPE_PAGE = 1;
    $.db.SCOPE_WINDOW = 2;
    $.db.SCOPE_CONNECTION = 3;
    $.db.SCOPE_SESSION = 4;
    $.db.SCOPE_USER = 5;
    $.db.SCOPE_GLOBAL = 6;
})(ppy, window.Papaya);

/**
 * Papaya client tcp remote bridge
 */
(function($, $b) {
    if (!$b) return;

    var ajaxCache = {},
        ppy_ajax_in_redirect = false,
        _success = function(id, status, ctx, content, cache) {
            if (typeof ctx.onsuccess == "function") {
                if (!status)
                    $.bridge.switchReusable(false);
                var obj = {
                    id: id,
                    content: content,
                    status: status,
                    ctx: ctx,
                    cache: cache   
                };
                if (ctx.dataType == 'json') {
                    obj.getJSON = function() {
                        this.__json = this.__json || $.evalJSON(this.content);
                        return this.__json;
                    }
                } else if (ctx.dataType == 'script') {
                    $.globalEval(content); 
                }
                ctx.onsuccess.call(null, obj);
            }
        };

    /**
     * Start a remote call
     * @param opts
     * url:         remote url, required
     * onsuccess:    callback when remote calling finished successfully
     * onerror:     callback when error occurs
     * id:          request id, optional
     * cache:       true to use papaya cache
     * method:      get or post
     * data:        if method is post, data is required
     * db:
     * key:
     * life:
     * dataType:    json, script
     */
    $.ajax = function(opts) {
        opts = opts || {};
        var url = opts.url,
            id = opts.id || $.generateUniqueId(),
            onsuccess = opts.onsuccess,
            onerror = opts.onerror,
            method = opts.method || "get",
            data = opts.data,
            cache = opts.cache,
            db = opts.db,
            key = opts.key,
            life = opts.life, value,
            dataType = opts.dataType || 'json';

        if (!url)
            throw Error("$.ajax: url is required.");

        ajaxCache[id] = {
            'onsuccess' : onsuccess,
            'onerror': onerror,
            'url' : url,
            'method': method,
            'db' : db,
            'key' : key,
            'life' : life,
            'dataType': dataType
        };

        if (method == "get") {
            if (!db || !key)
                $b.ajaxStart_url_callback_(id, url, (onsuccess || onerror) ? 1 : 0);
            else {
                if (cache && (value = db.kvLoad(key))) {
                    if (onsuccess)
                        __ppy_ajax_lateInvoke(id, $.ajax.SUCC, {'onsuccess':onsuccess, 'db' : db, 'key' : key, 'life' : life}, $.bridge.convertHtml(value), true);
                    return false;
                } else {
                    db.kvExpire(key);
                    db.kvExpireBatch(key+"__tmpl__%");
                    $b.ajaxStart_url_callback_db_scope_key_life_(id, url, (onsuccess || onerror) ? 1 : 0, db.id, db.scope, key, life == undefined ? -1 : life);
                }
            }
        } else {
            $b.startPost($.toJSONString({
                id: id,
                url: url,
                data: data
            }));
        }
        return id;
    };

    $.ajax.SUCC = 1;
    $.ajax.FAILED = 0;

    /**
     * Cancel an potp request
     * @param id
     */
    $.ajaxCancel = function(id) {
        $b.ajaxCancel_(id);
        delete ajaxCache[id];
    };

    // Papaya client potp callback
    // TODO: implement ppy_funcs
    window.ppy_onAjaxFinished = function(id ,status, url) {
        if (ppy_ajax_in_redirect) return;
        var ctx = ajaxCache[id],                          
            json, content, msg, redirect;
        delete ajaxCache[id];
        if (status == $.ajax.SUCC) {
            content = $.toString($b.ajaxGet_convert_(id, 1));
            if (content) {
                if (content.indexOf("__redirect__") >= 0) {
                    json = $.evalJSON(content);
                    if (json) {
                        redirect = json['__redirect__'], msg = json['__message__'];
                        if (msg)
                            ppy_funcs.info(msg, json['__message_type__']);

                        if (redirect && redirect.length > 0) {
                            ppy_ajax_in_redirect = true;
                            ppy_funcs.switchReusable(false);
                            document.location = redirect;
                            return;
                        }
                    }
                } else if (content.indexOf("__post_error__")>=0) {
                    json = $.evalJSON(content);
                    if (json) {
                        msg = json['__message__'];
                        if (msg)
                            ppy_funcs.info(msg, json['__message_type__']);
                        var snippet = json['__post_error__'];
                        if (snippet)
                            eval(snippet);
                    }
                } else if (content.indexOf("__message__")>=0) {
                    json = $.evalJSON(content);
                    if (json) {
                        msg = json['__message__'];
                        if (msg)
                            ppy_funcs.info(msg, json['__message_type__']);
                    }
                }
            }
            if (ctx)
                _success(id, status, ctx, content, false);
        } else {
            if (ctx && ctx.onerror)
                ctx.onerror.call(null);
        }
    }
})(ppy, window.Papaya);
(function($, $b) {

    var requireCache = {},
        pagelets = {},
        getJs = function(js, id) {
            $.ajax({
                url: 'testGetJs?fn=%s'.format(js),
                dataType: "script",
                onsuccess: function() {
                    checkRequireCount(id); 
                },
                onerror: function() {
                    
                }
            });
        },
        getCss = function(css) {

        },
        checkRequireCount = function(id) {
            var cache = requireCache[id];
            if (cache && !cache.complete && --cache.requireCount <= 0) {
                cache.complete = true;
                if (typeof cache.onPageletArrive == "function") {
                    cache.onPageletArrive();
                }
            }
        };

    /**
     * $.require({
     *   id: '',
     *   css: [],
     *   js: [],
     *   html: [],
     *   onPageletArrive: function() {
     *
     *   }
     *});
     * @param pagelet
     */
//    $.require = function(pagelet) {
//        if (!pagelet)
//            return;
//        var id = pagelet.id || $.generateUniqueId(),
//            js = pagelet.js || [],
//            css = pagelet.css || [],
//            html = pagelet.html || [],
//            onPageletArrive = pagelet.onPageletArrive,
//            i, l, rc = 0;
//        // Count files
//        rc = js.length + css.length + html.length;
//        requireCache[id] = {
//            requireCount: rc,
//            onPageletArrive: onPageletArrive,
//            complete: false
//        };
//        for (i = 0, l = js.length; i < l; i++) {
//            getJs(js, id);
//        }
//    };

    $.pagelet = function(opts) {
        opts = opts || {};
        var id = opts.id,
            init = opts.init;
        if (!id || typeof init != "function")
            return;
        pagelets[id] = opts;
    };

    /**
     * A simple implementation of require
     * @param id
     */
    $.require = function(id) {
        var p = pagelets[id];
        if (p && typeof p.init == "function")
            p.init.call();
    };

    $.PageletController = function(opts) {
        opts = opts || {};  
    };

})(ppy, window.Papaya);
