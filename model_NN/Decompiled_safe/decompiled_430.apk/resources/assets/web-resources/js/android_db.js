function PPYDatabase(id, scope) {
    this.id = id;
    this.scope = scope;
    window.Papaya.db_scope_(id, scope);

    this.kvSave = function (key, value, life) {
        life = life != undefined ? life : -1;
        window.Papaya.db_scope_key_value_life_(this.id, this.scope, key, value, life);
    };

    this.kvUpdate = function(key, value) {
        this.kvExpireBatch(key+"__tmpl__%");
        window.Papaya.db_scope_key_value_(this.id, this.scope, key, value);
    };

    this.kvLoad = function (key, defaultValue) {
        var v = ppy_string(window.Papaya.db_scope_key_(this.id, this.scope, key));
        return (v == undefined || v == null) ? defaultValue : v;
    };

    this.kvLoadInt = function (key, defaultValue) {
        defaultValue = defaultValue != undefined ? defaultValue : -1;
        var value = this.kvLoad(key);
        if (value)
            return parseInt(value);
        else
            return defaultValue;
    };

    this.kvExpire = function (key) {
        if (key)
            return this.update('delete from kv where key=?', [key]);
        return false;
    };

    this.kvExpireBatch = function (keyPattern) {
        if (keyPattern)
            this.update('delete from kv where key like ?', [keyPattern]);
    };

    this.update = function(sql, arguments) {
        arguments = arguments == undefined ? '' : arguments;
        return window.Papaya.db_scope_update_arguments_(this.id, this.scope, sql, toJSONString(arguments));
    };

    this.query = function(sql, arguments) {
        arguments = arguments == undefined ? '' : arguments;
        return evalJSON(ppy_string(window.Papaya.db_scope_query_arguments_(this.id, this.scope, sql, toJSONString(arguments))));
    };
}

PPYDatabase.NAME_CONNECTION = "__connection__";
PPYDatabase.NAME_SESSION = "__session__";

PPYDatabase.SCOPE_UNDEFINED = 0;
PPYDatabase.SCOPE_PAGE = 1;
PPYDatabase.SCOPE_WINDOW = 2;
PPYDatabase.SCOPE_CONNECTION = 3;
PPYDatabase.SCOPE_SESSION = 4;
PPYDatabase.SCOPE_USER = 5;
PPYDatabase.SCOPE_GLOBAL = 6;

PPYDatabase.connectionDB = function() {return new PPYDatabase(PPYDatabase.NAME_CONNECTION, PPYDatabase.SCOPE_CONNECTION);};
PPYDatabase.sessionDB = function() {return new PPYDatabase(PPYDatabase.NAME_SESSION, PPYDatabase.SCOPE_SESSION);};
