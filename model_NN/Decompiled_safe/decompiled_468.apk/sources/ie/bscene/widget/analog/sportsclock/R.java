package ie.bscene.widget.analog.sportsclock;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int widgetdial = 2130837505;
        public static final int widgethour = 2130837506;
        public static final int widgetminute = 2130837507;
    }

    public static final class id {
        public static final int AnalogClock = 2131099649;
        public static final int Widget = 2131099648;
    }

    public static final class layout {
        public static final int info = 2130903040;
        public static final int widget = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class xml {
        public static final int widget = 2130968576;
    }
}
