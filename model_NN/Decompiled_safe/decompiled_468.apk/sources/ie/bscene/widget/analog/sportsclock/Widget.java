package ie.bscene.widget.analog.sportsclock;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.RemoteViews;

public class Widget extends AppWidgetProvider {
    public void onReceive(Context context, Intent intent) {
        if ("android.appwidget.action.APPWIDGET_UPDATE".equals(intent.getAction())) {
            RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
            views.setOnClickPendingIntent(R.id.Widget, PendingIntent.getActivity(context, 0, getAlarmPackage(context), 0));
            AppWidgetManager.getInstance(context).updateAppWidget(intent.getIntArrayExtra("appWidgetIds"), views);
        }
    }

    public Intent getAlarmPackage(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent AlarmClockIntent = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.LAUNCHER");
        String[][] clockImpls = {new String[]{"Standard Alarm", "com.android.alarmclock", "com.android.alarmclock.AlarmClock"}, new String[]{"HTC Alarm ClockDT", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl"}, new String[]{"Standard Alarm ClockDT", "com.android.deskclock", "com.android.deskclock.AlarmClock"}, new String[]{"Froyo Nexus Alarm ClockDT", "com.google.android.deskclock", "com.android.deskclock.DeskClock"}, new String[]{"Moto Blur Alarm ClockDT", "com.motorola.blur.alarmclock", "com.motorola.blur.alarmclock.AlarmClock"}, new String[]{"Samsung Galaxy S", "com.sec.android.app.clockpackage", "com.sec.android.app.clockpackage.ClockPackage"}};
        boolean foundClockImpl = false;
        for (int i = 0; i < clockImpls.length; i++) {
            try {
                ComponentName cn = new ComponentName(clockImpls[i][1], clockImpls[i][2]);
                packageManager.getActivityInfo(cn, 128);
                AlarmClockIntent.setComponent(cn);
                foundClockImpl = true;
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        if (foundClockImpl) {
            return AlarmClockIntent;
        }
        return null;
    }
}
