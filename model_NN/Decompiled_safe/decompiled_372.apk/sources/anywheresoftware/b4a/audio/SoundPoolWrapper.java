package anywheresoftware.b4a.audio;

import android.media.SoundPool;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.streams.File;
import java.io.IOException;

@BA.ShortName("SoundPool")
public class SoundPoolWrapper extends AbsObjectWrapper<SoundPool> {
    public void Initialize(int MaxStreams) {
        setObject(new SoundPool(MaxStreams, 3, 0));
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public int Load(String Dir, String File) throws IOException {
        if (Dir.equals(File.getDirAssets())) {
            return ((SoundPool) getObject()).load(BA.applicationContext.getAssets().openFd(File.toLowerCase(BA.cul)), 1);
        }
        return ((SoundPool) getObject()).load(File.Combine(Dir, File), 1);
    }

    public int Play(int LoadId, float LeftVolume, float RightVolume, int Priority, int Loop, float Rate) {
        return ((SoundPool) getObject()).play(LoadId, LeftVolume, RightVolume, Priority, Loop, Rate);
    }

    public void Pause(int PlayId) {
        ((SoundPool) getObject()).pause(PlayId);
    }

    public void Resume(int PlayId) {
        ((SoundPool) getObject()).resume(PlayId);
    }

    public void Stop(int PlayId) {
        ((SoundPool) getObject()).stop(PlayId);
    }

    public void SetVolume(int PlayId, float Left, float Right) {
        ((SoundPool) getObject()).setVolume(PlayId, Left, Right);
    }

    public void Unload(int LoadId) {
        ((SoundPool) getObject()).unload(LoadId);
    }

    public void SetRate(int PlayId, float Rate) {
        ((SoundPool) getObject()).setRate(PlayId, Rate);
    }

    public void Release() {
        ((SoundPool) getObject()).release();
    }
}
