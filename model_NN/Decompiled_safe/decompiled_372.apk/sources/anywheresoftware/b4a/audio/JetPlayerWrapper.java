package anywheresoftware.b4a.audio;

import android.media.JetPlayer;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.streams.File;
import java.io.IOException;

@BA.ShortName("JetPlayer")
public class JetPlayerWrapper extends AbsObjectWrapper<JetPlayer> {
    public void Initialize(final BA ba, String EventName) {
        JetPlayer jp = JetPlayer.getJetPlayer();
        setObject(jp);
        jp.clearQueue();
        final String e = EventName.toLowerCase(BA.cul);
        jp.setEventListener(new JetPlayer.OnJetEventListener() {
            public void onJetEvent(JetPlayer player, short segment, byte track, byte channel, byte controller, byte value) {
            }

            public void onJetNumQueuedSegmentUpdate(JetPlayer player, int nbSegments) {
                ba.raiseEvent(JetPlayerWrapper.this.getObject(), String.valueOf(e) + "_queuedsegmentscountchanged", Integer.valueOf(nbSegments));
            }

            public void onJetPauseUpdate(JetPlayer player, int paused) {
            }

            public void onJetUserIdUpdate(JetPlayer player, int userId, int repeatCount) {
                ba.raiseEvent(JetPlayerWrapper.this.getObject(), String.valueOf(e) + "_currentuseridchanged", Integer.valueOf(userId), Integer.valueOf(repeatCount));
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void LoadFile(String Dir, String File) throws IOException {
        boolean result;
        if (Dir.equals(File.getDirAssets())) {
            result = ((JetPlayer) getObject()).loadJetFile(BA.applicationContext.getAssets().openFd(File.toLowerCase(BA.cul)));
        } else {
            result = ((JetPlayer) getObject()).loadJetFile(File.Combine(Dir, File));
        }
        if (!result) {
            throw new IOException("Error loading Jet file.");
        }
    }

    public void CloseFile() {
        if (IsInitialized()) {
            ((JetPlayer) getObject()).closeJetFile();
        }
    }

    public void ClearQueue() {
        ((JetPlayer) getObject()).clearQueue();
    }

    public void QueueSegment(int SegmentNum, int LibNum, int RepeatCount, int Transpose, boolean[] MuteArray, byte UserId) {
        if (!((JetPlayer) getObject()).queueJetSegmentMuteArray(SegmentNum, LibNum, RepeatCount, Transpose, MuteArray, UserId)) {
            throw new RuntimeException("Error queuing segment.");
        }
    }

    public void SetMute(boolean[] MuteArray, boolean Sync) {
        if (!((JetPlayer) getObject()).setMuteArray(MuteArray, Sync)) {
            throw new RuntimeException("Error setting mute state.");
        }
    }

    public void SetTrackMute(int Track, boolean Mute, boolean Sync) {
        if (!((JetPlayer) getObject()).setMuteFlag(Track, Mute, Sync)) {
            throw new RuntimeException("Error setting mute state.");
        }
    }

    public void Play() {
        if (!((JetPlayer) getObject()).play()) {
            throw new RuntimeException("Error playing file.");
        }
    }

    public void Pause() {
        if (!((JetPlayer) getObject()).pause()) {
            throw new RuntimeException("Error pausing.");
        }
    }

    public int getMaxTracks() {
        getObject();
        return JetPlayer.getMaxTracks();
    }

    public void Release() {
        ((JetPlayer) getObject()).release();
    }
}
