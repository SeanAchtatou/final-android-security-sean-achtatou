package anywheresoftware.b4a.audio;

import android.media.MediaPlayer;
import android.widget.MediaController;
import android.widget.VideoView;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.streams.File;

@BA.ShortName("VideoView")
@BA.ActivityObject
public class VideoViewWrapper extends ViewWrapper<VideoView> {
    public void Initialize(BA ba, String EventName) {
        super.Initialize(ba, EventName);
    }

    @BA.Hide
    public void innerInitialize(final BA ba, final String eventName, boolean keepOldObject) {
        if (!keepOldObject) {
            setObject(new VideoView(ba.context));
        }
        super.innerInitialize(ba, eventName, true);
        setMediaControllerEnabled(true);
        ((VideoView) getObject()).setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                ba.raiseEvent(VideoViewWrapper.this, String.valueOf(eventName) + "_complete", new Object[0]);
            }
        });
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void LoadVideo(String Dir, String FileName) {
        if (Dir.equals(File.getDirAssets())) {
            throw new RuntimeException("Cannot load video from assets folder.");
        } else if (Dir.equals(File.ContentDir)) {
            ((VideoView) getObject()).setVideoPath(FileName);
        } else if (Dir.equals("http")) {
            ((VideoView) getObject()).setVideoPath(FileName);
        } else {
            ((VideoView) getObject()).setVideoPath(new java.io.File(Dir, FileName).toString());
        }
    }

    public void Play() {
        ((VideoView) getObject()).start();
    }

    public void Pause() {
        ((VideoView) getObject()).pause();
    }

    public void Stop() {
        ((VideoView) getObject()).stopPlayback();
    }

    public boolean IsPlaying() {
        return ((VideoView) getObject()).isPlaying();
    }

    public int getPosition() {
        return ((VideoView) getObject()).getCurrentPosition();
    }

    public void setPosition(int v) {
        ((VideoView) getObject()).seekTo(v);
    }

    public int getDuration() {
        return ((VideoView) getObject()).getDuration();
    }

    public void setMediaControllerEnabled(boolean v) {
        ((VideoView) getObject()).setMediaController(v ? new MediaController(this.ba.context) : null);
    }

    public String toString() {
        if (getObjectOrNull() == null) {
            return super.toString();
        }
        if (!((VideoView) getObject()).isPlaying()) {
            return "Not playing";
        }
        return "Playing, Position=" + getPosition() + ", Duration=" + getDuration();
    }
}
