package anywheresoftware.b4a.objects;

import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.keywords.constants.Colors;
import java.util.Map;

@BA.Hide
public class TextViewWrapper<T extends TextView> extends ViewWrapper<T> {
    public String getText() {
        return ((TextView) getObject()).getText().toString();
    }

    public void setText(Object Text) {
        CharSequence cs;
        if (Text instanceof CharSequence) {
            cs = (CharSequence) Text;
        } else {
            cs = Text.toString();
        }
        ((TextView) getObject()).setText(cs);
    }

    public void setTextColor(int Color) {
        ((TextView) getObject()).setTextColor(Color);
    }

    public int getTextColor() {
        return ((TextView) getObject()).getTextColors().getDefaultColor();
    }

    public void setTextSize(float TextSize) {
        ((TextView) getObject()).setTextSize(TextSize);
    }

    public float getTextSize() {
        return ((TextView) getObject()).getTextSize() / ((TextView) getObject()).getContext().getResources().getDisplayMetrics().scaledDensity;
    }

    public void setGravity(int Gravity) {
        ((TextView) getObject()).setGravity(Gravity);
    }

    public int getGravity() {
        return ((TextView) getObject()).getGravity();
    }

    public void setTypeface(Typeface Typeface) {
        ((TextView) getObject()).setTypeface(Typeface);
    }

    public Typeface getTypeface() {
        return ((TextView) getObject()).getTypeface();
    }

    @BA.Hide
    public String toString() {
        String s = super.toString();
        if (IsInitialized()) {
            return String.valueOf(s) + ", Text=" + getText();
        }
        return s;
    }

    @BA.Hide
    public static View build(Object prev, Map<String, Object> props, boolean designer) throws Exception {
        Class<Gravity> cls = Gravity.class;
        Class<Typeface> cls2 = Typeface.class;
        TextView v = (TextView) ViewWrapper.build(prev, props, designer);
        v.setText((CharSequence) props.get("text"));
        Class<Typeface> cls3 = Typeface.class;
        Class<Typeface> cls4 = Typeface.class;
        int style = ((Integer) cls2.getField((String) props.get("style")).get(null)).intValue();
        v.setTextSize(((Float) props.get("fontsize")).floatValue());
        v.setTypeface((Typeface) cls2.getField((String) props.get("typeface")).get(null), style);
        Class<Gravity> cls5 = Gravity.class;
        Class<Gravity> cls6 = Gravity.class;
        v.setGravity(((Integer) cls.getField((String) props.get("vAlignment")).get(null)).intValue() | ((Integer) cls.getField((String) props.get("hAlignment")).get(null)).intValue());
        v.setTextColor(((Integer) props.get("textColor")).intValue());
        if (designer && v.getText().length() == 0 && (v.getHint() == null || v.getHint().length() == 0)) {
            v.setText((String) props.get("name"));
            v.setTextColor((int) Colors.Gray);
        }
        return v;
    }
}
