package anywheresoftware.b4a.objects;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.DynamicBuilder;
import anywheresoftware.b4a.keywords.LayoutBuilder;
import anywheresoftware.b4a.keywords.LayoutValues;
import anywheresoftware.b4a.objects.drawable.BitmapDrawable;
import java.util.HashMap;

@BA.ShortName("Activity")
@BA.ActivityObject
public class ActivityWrapper extends ViewWrapper<BALayout> {
    public static final int ACTION_DOWN = 0;
    public static final int ACTION_MOVE = 2;
    public static final int ACTION_UP = 1;

    public ActivityWrapper(final BA ba, String name) {
        setObject(ba.vg);
        innerInitialize(ba, name, true);
        if (ba.subExists("activity_touch")) {
            ((BALayout) getObject()).setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ba.raiseEvent(ActivityWrapper.this, "activity_touch", Integer.valueOf(event.getAction()), Float.valueOf(event.getX()), Float.valueOf(event.getY()));
                    return true;
                }
            });
        }
    }

    public IntentWrapper GetStartingIntent() {
        IntentWrapper iw = new IntentWrapper();
        iw.setObject(this.ba.activity.getIntent());
        return iw;
    }

    public void SetActivityResult(int Result, IntentWrapper Data) {
        this.ba.activity.setResult(Result, (Intent) Data.getObject());
    }

    public void AddView(View View, int Left, int Top, int Width, int Height) {
        ((BALayout) getObject()).addView(View, new BALayout.LayoutParams(Left, Top, Width, Height));
    }

    public ConcreteViewWrapper GetView(int Index) {
        ConcreteViewWrapper vw = new ConcreteViewWrapper();
        vw.setObject(((BALayout) getObject()).getChildAt(Index));
        return vw;
    }

    public void RemoveViewAt(int Index) {
        ((BALayout) getObject()).removeViewAt(Index);
    }

    public int getNumberOfViews() {
        return ((BALayout) getObject()).getChildCount();
    }

    public void AddMenuItem(String Title, String EventName) {
        ((B4AActivity) this.ba.activity).addMenuItem(new B4AMenuItem(Title, null, EventName));
    }

    public void AddMenuItem2(String Title, String EventName, Bitmap Bitmap) {
        BitmapDrawable bd = new BitmapDrawable();
        bd.Initialize(Bitmap);
        ((B4AActivity) this.ba.activity).addMenuItem(new B4AMenuItem(Title, (Drawable) bd.getObject(), EventName));
    }

    public LayoutValues LoadLayout(String Layout, BA ba) throws Exception {
        AbsObjectWrapper.Activity_LoadLayout_Was_Called = true;
        return LayoutBuilder.loadLayout(Layout, ba, true, ba.vg);
    }

    public void OpenMenu() {
        this.ba.activity.openOptionsMenu();
    }

    public void CloseMenu() {
        this.ba.activity.closeOptionsMenu();
    }

    public void setTitle(Object Title) {
        CharSequence cs;
        if (Title instanceof CharSequence) {
            cs = (CharSequence) Title;
        } else {
            cs = Title.toString();
        }
        this.ba.activity.setTitle(cs);
    }

    public CharSequence getTitle() {
        return this.ba.activity.getTitle();
    }

    public int getTitleColor() {
        return this.ba.activity.getTitleColor();
    }

    public void setTitleColor(int Color) {
        this.ba.activity.setTitleColor(Color);
    }

    public int getWidth() {
        return ((BALayout) getObject()).getWidth();
    }

    public int getHeight() {
        return ((BALayout) getObject()).getHeight();
    }

    public int getLeft() {
        return 0;
    }

    public int getTop() {
        return 0;
    }

    @BA.Hide
    public void setVisible(boolean Visible) {
    }

    @BA.Hide
    public boolean getVisible() {
        return true;
    }

    @BA.Hide
    public void setEnabled(boolean Enabled) {
    }

    @BA.Hide
    public boolean getEnabled() {
        return true;
    }

    @BA.Hide
    public void BringToFront() {
    }

    @BA.Hide
    public void SendToBack() {
    }

    @BA.Hide
    public void RemoveView() {
    }

    public void Finish() {
        this.ba.activity.finish();
    }

    @BA.Hide
    public static View build(Object prev, HashMap<String, Object> props, boolean designer, Object tag) throws Exception {
        View v = (View) prev;
        v.setBackgroundDrawable((Drawable) DynamicBuilder.build(prev, (HashMap) props.get("drawable"), designer, null));
        ((Activity) v.getContext()).setTitle((String) props.get("title"));
        ((Activity) v.getContext()).setTitleColor(((Integer) props.get("titleColor")).intValue());
        boolean fullScreen = ((Boolean) props.get("fullScreen")).booleanValue();
        boolean includeTitle = ((Boolean) props.get("includeTitle")).booleanValue();
        if (designer) {
            Class<?> cls = Class.forName("anywheresoftware.b4a.designer.Designer");
            boolean prevFullScreen = cls.getField("fullScreen").getBoolean(v.getContext());
            boolean prevIncludeTitle = cls.getField("includeTitle").getBoolean(v.getContext());
            if (!(prevFullScreen == fullScreen && includeTitle == prevIncludeTitle)) {
                Intent i = new Intent(v.getContext().getApplicationContext(), cls);
                i.putExtra("anywheresoftware.b4a.designer.includeTitle", includeTitle);
                i.putExtra("anywheresoftware.b4a.designer.fullScreen", fullScreen);
                cls.getMethod("restartActivity", Intent.class).invoke(v.getContext(), i);
            }
        }
        return (View) prev;
    }
}
