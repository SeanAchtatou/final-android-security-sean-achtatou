package anywheresoftware.b4a.objects.collections;

import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@BA.ShortName("Map")
public class Map extends AbsObjectWrapper<MyMap> {
    public void Initialize() {
        setObject(new MyMap());
    }

    public Object Put(Object Key, Object Value) {
        return ((MyMap) getObject()).put(Key, Value);
    }

    public Object Remove(Object Key) {
        return ((MyMap) getObject()).remove(Key);
    }

    public Object Get(Object Key) {
        return ((MyMap) getObject()).get(Key);
    }

    public Object GetDefault(Object Key, Object Default) {
        Object res = ((MyMap) getObject()).get(Key);
        if (res == null) {
            return Default;
        }
        return res;
    }

    public void Clear() {
        ((MyMap) getObject()).clear();
    }

    public Object GetKeyAt(int Index) {
        return ((MyMap) getObject()).getKey(Index);
    }

    public Object GetValueAt(int Index) {
        return ((MyMap) getObject()).getValue(Index);
    }

    public int getSize() {
        return ((MyMap) getObject()).size();
    }

    public boolean ContainsKey(Object Key) {
        return ((MyMap) getObject()).containsKey(Key);
    }

    @BA.Hide
    public static class MyMap implements java.util.Map<Object, Object> {
        private Map.Entry<Object, Object> currentEntry;
        private LinkedHashMap<Object, Object> innerMap = new LinkedHashMap<>();
        private Iterator<Map.Entry<Object, Object>> iterator;
        private int iteratorPosition;

        public Object getKey(int index) {
            return getEntry(index).getKey();
        }

        public Object getValue(int index) {
            return getEntry(index).getValue();
        }

        private Map.Entry<Object, Object> getEntry(int index) {
            if (!(this.iterator == null || this.iteratorPosition == index)) {
                if (this.iteratorPosition == index - 1) {
                    this.currentEntry = this.iterator.next();
                    this.iteratorPosition++;
                } else {
                    this.iterator = null;
                }
            }
            if (this.iterator == null) {
                this.iterator = this.innerMap.entrySet().iterator();
                for (int i = 0; i <= index; i++) {
                    this.currentEntry = this.iterator.next();
                }
                this.iteratorPosition = index;
            }
            return this.currentEntry;
        }

        public void clear() {
            this.iterator = null;
            this.innerMap.clear();
        }

        public boolean containsKey(Object key) {
            return this.innerMap.containsKey(key);
        }

        public boolean containsValue(Object value) {
            return this.innerMap.containsValue(value);
        }

        public Set<Map.Entry<Object, Object>> entrySet() {
            return this.innerMap.entrySet();
        }

        public Object get(Object key) {
            return this.innerMap.get(key);
        }

        public boolean isEmpty() {
            return this.innerMap.isEmpty();
        }

        public Set<Object> keySet() {
            return this.innerMap.keySet();
        }

        public Object put(Object key, Object value) {
            this.iterator = null;
            return this.innerMap.put(key, value);
        }

        public void putAll(java.util.Map<? extends Object, ? extends Object> m) {
            this.iterator = null;
            this.innerMap.putAll(m);
        }

        public Object remove(Object key) {
            this.iterator = null;
            return this.innerMap.remove(key);
        }

        public int size() {
            return this.innerMap.size();
        }

        public Collection<Object> values() {
            return this.innerMap.values();
        }

        public String toString() {
            return this.innerMap.toString();
        }
    }
}
