package com.madelephantstudios.basketballtapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.constants.TypefaceWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.AnimationWrapper;
import anywheresoftware.b4a.objects.ButtonWrapper;
import anywheresoftware.b4a.objects.IntentWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.Timer;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.streams.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class main extends Activity implements B4AActivity {
    public static int _atype = 0;
    public static String _score = "";
    public static Timer _tmra = null;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static main mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activitygame _activitygame = null;
    public activitysettings _activitysettings = null;
    public AdViewWrapper _ad = null;
    public AnimationWrapper _animball = null;
    public ButtonWrapper _btnblitz = null;
    public ButtonWrapper _btnmaths = null;
    public ButtonWrapper _button1 = null;
    public ButtonWrapper _button2 = null;
    public ButtonWrapper _button3 = null;
    public LabelWrapper _label3 = null;
    public LabelWrapper _lblscore = null;
    public LabelWrapper _lbltitle = null;
    public PanelWrapper _pnlball = null;
    public PanelWrapper _pnlmaths = null;
    public TypefaceWrapper _tf = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.basketballtapp", "main");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (main).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!main.afterFirstLayout) {
                if (main.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                main.mostCurrent.layout.getLayoutParams().height = main.mostCurrent.layout.getHeight();
                main.mostCurrent.layout.getLayoutParams().width = main.mostCurrent.layout.getWidth();
                main.afterFirstLayout = true;
                main.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.basketballtapp", "main");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (main) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (main) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            main.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return main.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (main) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (main.mostCurrent != null && main.mostCurrent == this.activity.get()) {
                main.processBA.setActivityPaused(false);
                Common.Log("** Activity (main) Resume **");
                main.processBA.raiseEvent(main.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        mostCurrent._activity.LoadLayout("layoutMain2", mostCurrent.activityBA);
        if (z) {
            _tmra.Initialize(processBA, "tmra", 10);
        }
        TypefaceWrapper typefaceWrapper = mostCurrent._tf;
        TypefaceWrapper typefaceWrapper2 = Common.Typeface;
        typefaceWrapper.setObject(TypefaceWrapper.LoadFromAssets("Corben-Bold.ttf"));
        mostCurrent._button1.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._button2.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._button3.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._lbltitle.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._btnmaths.setText("Math Genius!\nHighly addictive game! - FREE!");
        mostCurrent._activity.AddMenuItem("Settings", "config");
        mostCurrent._pnlball.SendToBack();
        mostCurrent._animball.InitializeRotateCenter(mostCurrent.activityBA, "animball", Common.Density, 360.0f, (View) mostCurrent._pnlball.getObject());
        mostCurrent._animball.setDuration(2200);
        AnimationWrapper animationWrapper = mostCurrent._animball;
        AnimationWrapper animationWrapper2 = mostCurrent._animball;
        animationWrapper.setRepeatMode(2);
        mostCurrent._animball.setRepeatCount(-1);
        mostCurrent._animball.Start((View) mostCurrent._pnlball.getObject());
        _loadscore();
        _loadads();
        _atype = 0;
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        AnimationWrapper animationWrapper = mostCurrent._animball;
        AnimationWrapper.Stop((View) mostCurrent._pnlball.getObject());
        _tmra.setEnabled(false);
        return "";
    }

    public static String _activity_resume() throws Exception {
        _loadscore();
        mostCurrent._ad.LoadAd();
        mostCurrent._animball.Start((View) mostCurrent._pnlball.getObject());
        mostCurrent._button1.BringToFront();
        mostCurrent._button2.BringToFront();
        mostCurrent._button3.BringToFront();
        _tmra.setEnabled(false);
        return "";
    }

    public static String _animball_animationend() throws Exception {
        return "";
    }

    public static String _btnblitz_click() throws Exception {
        IntentWrapper intentWrapper = new IntentWrapper();
        intentWrapper.Initialize(IntentWrapper.ACTION_VIEW, "market://details?id=com.madelephantstudios.blitz");
        Common.StartActivity(mostCurrent.activityBA, intentWrapper.getObject());
        return "";
    }

    public static String _btnmaths_click() throws Exception {
        IntentWrapper intentWrapper = new IntentWrapper();
        intentWrapper.Initialize(IntentWrapper.ACTION_VIEW, "market://details?id=com.madelephantstudios.MathGenius");
        Common.StartActivity(mostCurrent.activityBA, intentWrapper.getObject());
        return "";
    }

    public static String _button1_click() throws Exception {
        AnimationWrapper animationWrapper = mostCurrent._animball;
        AnimationWrapper.Stop((View) mostCurrent._pnlball.getObject());
        _atype = 1;
        _tmra.setEnabled(true);
        return "";
    }

    public static String _button2_click() throws Exception {
        Common.Msgbox("Tap the basketball before it hits the ground!\nKeep it in the air to score points on each tap.\nMore balls will be added to the challenge, and the speed increases as you progress through the game.\nGOOD LUCK!", "Instructions", mostCurrent.activityBA);
        return "";
    }

    public static String _button3_click() throws Exception {
        AnimationWrapper animationWrapper = mostCurrent._animball;
        AnimationWrapper.Stop((View) mostCurrent._pnlball.getObject());
        _atype = 3;
        _tmra.setEnabled(true);
        return "";
    }

    public static String _config_click() throws Exception {
        BA ba = mostCurrent.activityBA;
        activitysettings activitysettings = mostCurrent._activitysettings;
        Common.StartActivity(ba, activitysettings.getObject());
        return "";
    }

    public static void initializeProcessGlobals() {
        if (!processGlobalsRun) {
            processGlobalsRun = true;
            try {
                _process_globals();
                activitygame._process_globals();
                activitysettings._process_globals();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static String _globals() throws Exception {
        main main = mostCurrent;
        _score = "";
        mostCurrent._button1 = new ButtonWrapper();
        mostCurrent._lblscore = new LabelWrapper();
        mostCurrent._label3 = new LabelWrapper();
        mostCurrent._button2 = new ButtonWrapper();
        mostCurrent._ad = new AdViewWrapper();
        mostCurrent._btnblitz = new ButtonWrapper();
        mostCurrent._button3 = new ButtonWrapper();
        mostCurrent._btnmaths = new ButtonWrapper();
        mostCurrent._pnlmaths = new PanelWrapper();
        mostCurrent._pnlball = new PanelWrapper();
        mostCurrent._animball = new AnimationWrapper();
        mostCurrent._lbltitle = new LabelWrapper();
        mostCurrent._tf = new TypefaceWrapper();
        _atype = 0;
        return "";
    }

    public static String _label3_click() throws Exception {
        return "";
    }

    public static String _loadads() throws Exception {
        mostCurrent._ad.Initialize(mostCurrent.activityBA, "ad", "a14df0e187bbd86");
        mostCurrent._activity.AddView((View) mostCurrent._ad.getObject(), Common.DipToCurrent(0), 0, Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _loadscore() throws Exception {
        main main = mostCurrent;
        _score = "0";
        try {
            main main2 = mostCurrent;
            File file = Common.File;
            File file2 = Common.File;
            _score = File.ReadString(File.getDirInternal(), "ballflipscore.dat");
        } catch (Exception e) {
            processBA.setLastException(e);
        }
        LabelWrapper labelWrapper = mostCurrent._lblscore;
        main main3 = mostCurrent;
        labelWrapper.setText(_score);
        return "";
    }

    public static String _pnlmaths_click() throws Exception {
        IntentWrapper intentWrapper = new IntentWrapper();
        intentWrapper.Initialize(IntentWrapper.ACTION_VIEW, "market://details?id=com.madelephantstudios.basketballtapp");
        Common.StartActivity(mostCurrent.activityBA, intentWrapper.getObject());
        return "";
    }

    public static String _process_globals() throws Exception {
        _tmra = new Timer();
        return "";
    }

    public static String _tmra_tick() throws Exception {
        if (_atype == 1) {
            BA ba = mostCurrent.activityBA;
            activitygame activitygame = mostCurrent._activitygame;
            Common.StartActivity(ba, activitygame.getObject());
        }
        if (_atype != 3) {
            return "";
        }
        IntentWrapper intentWrapper = new IntentWrapper();
        intentWrapper.Initialize(IntentWrapper.ACTION_VIEW, "market://details?id=com.madelephantstudios.basketballtapp");
        Common.StartActivity(mostCurrent.activityBA, intentWrapper.getObject());
        return "";
    }
}
