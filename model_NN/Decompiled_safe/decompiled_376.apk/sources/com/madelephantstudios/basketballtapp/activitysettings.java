package com.madelephantstudios.basketballtapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.ButtonWrapper;
import anywheresoftware.b4a.objects.CompoundButtonWrapper;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.streams.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class activitysettings extends Activity implements B4AActivity {
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static activitysettings mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activitygame _activitygame = null;
    public ButtonWrapper _btncancel = null;
    public ButtonWrapper _btndone = null;
    public CompoundButtonWrapper.CheckBoxWrapper _cbsound = null;
    public CompoundButtonWrapper.CheckBoxWrapper _cbvibration = null;
    public main _main = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.basketballtapp", "activitysettings");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (activitysettings).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!activitysettings.afterFirstLayout) {
                if (activitysettings.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                activitysettings.mostCurrent.layout.getLayoutParams().height = activitysettings.mostCurrent.layout.getHeight();
                activitysettings.mostCurrent.layout.getLayoutParams().width = activitysettings.mostCurrent.layout.getWidth();
                activitysettings.afterFirstLayout = true;
                activitysettings.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.basketballtapp", "activitysettings");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (activitysettings) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (activitysettings) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            activitysettings.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return activitysettings.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (activitysettings) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (activitysettings.mostCurrent != null && activitysettings.mostCurrent == this.activity.get()) {
                activitysettings.processBA.setActivityPaused(false);
                Common.Log("** Activity (activitysettings) Resume **");
                activitysettings.processBA.raiseEvent(activitysettings.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        mostCurrent._activity.LoadLayout("layoutSetting", mostCurrent.activityBA);
        _loadsettings();
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        return "";
    }

    public static String _activity_resume() throws Exception {
        return "";
    }

    public static String _btncancel_click() throws Exception {
        mostCurrent._activity.Finish();
        return "";
    }

    public static String _btndone_click() throws Exception {
        File file = Common.File;
        File file2 = Common.File;
        File.WriteString(File.getDirInternal(), "balltappbs.dat", String.valueOf(mostCurrent._cbsound.getChecked()));
        File file3 = Common.File;
        File file4 = Common.File;
        File.WriteString(File.getDirInternal(), "balltappbv.dat", String.valueOf(mostCurrent._cbvibration.getChecked()));
        mostCurrent._activity.Finish();
        return "";
    }

    public static void initializeProcessGlobals() {
    }

    public static String _globals() throws Exception {
        mostCurrent._btncancel = new ButtonWrapper();
        mostCurrent._btndone = new ButtonWrapper();
        mostCurrent._cbsound = new CompoundButtonWrapper.CheckBoxWrapper();
        mostCurrent._cbvibration = new CompoundButtonWrapper.CheckBoxWrapper();
        return "";
    }

    public static String _loadsettings() throws Exception {
        boolean z;
        boolean z2;
        try {
            File file = Common.File;
            File file2 = Common.File;
            boolean ObjectToBoolean = BA.ObjectToBoolean(File.ReadString(File.getDirInternal(), "balltappbs.dat"));
            try {
                File file3 = Common.File;
                File file4 = Common.File;
                z = ObjectToBoolean;
                z2 = BA.ObjectToBoolean(File.ReadString(File.getDirInternal(), "balltappbv.dat"));
            } catch (Exception e) {
                Exception exc = e;
                z = ObjectToBoolean;
                e = exc;
                processBA.setLastException(e);
                z2 = true;
                mostCurrent._cbsound.setChecked(z);
                mostCurrent._cbvibration.setChecked(z2);
                return "";
            }
        } catch (Exception e2) {
            e = e2;
            z = true;
        }
        mostCurrent._cbsound.setChecked(z);
        mostCurrent._cbvibration.setChecked(z2);
        return "";
    }

    public static String _process_globals() throws Exception {
        return "";
    }
}
