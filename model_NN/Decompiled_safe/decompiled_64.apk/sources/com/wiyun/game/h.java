package com.wiyun.game;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.wiyun.game.a.i;
import com.wiyun.game.e.c;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class h {
    public static final Pattern a = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
    private static char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static List<a> c = new ArrayList();
    private static Map<FrameLayout, View> d = new HashMap();
    private static final Paint e = new Paint();
    private static final Rect f = new Rect();
    private static final Rect g = new Rect();
    private static Canvas h = new Canvas();

    public interface a {
        void a();
    }

    private static double a(double d2) {
        return (3.141592653589793d * d2) / 180.0d;
    }

    /* JADX INFO: Multiple debug info for r8v1 double: [D('lat2' double), D('radLat2' double)] */
    /* JADX INFO: Multiple debug info for r4v1 double: [D('a' double), D('lat1' double)] */
    public static double a(double lat1, double lon1, double lat2, double lon2) {
        double radLat1 = a(lat1);
        double radLat2 = a(lat2);
        double pow = Math.pow(Math.sin((radLat1 - radLat2) / 2.0d), 2.0d);
        double radLat12 = Math.cos(radLat1);
        return Math.asin(Math.sqrt(pow + (Math.pow(Math.sin((a(lon1) - a(lon2)) / 2.0d), 2.0d) * Math.cos(radLat2) * radLat12))) * 2.0d * 6378.137d;
    }

    public static float a(String value) {
        if (value == null) {
            return 0.0f;
        }
        try {
            return Float.parseFloat(value.trim());
        } catch (NumberFormatException e2) {
            return 0.0f;
        }
    }

    private static int a(int c2) {
        return c2 + 1;
    }

    public static int a(int radix, String value) {
        return a(radix, value, 0);
    }

    public static int a(int radix, String value, int faultValue) {
        if (value == null) {
            return faultValue;
        }
        try {
            return (int) Long.parseLong(value.trim(), radix);
        } catch (NumberFormatException e2) {
            return faultValue;
        }
    }

    private static int a(String str, String str2, int i) {
        int length = str2.length();
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < length; i3++) {
            int indexOf = str.indexOf(str2.charAt(i3), i);
            if (indexOf != -1) {
                i2 = Math.min(i2, indexOf);
            }
        }
        if (i2 == Integer.MAX_VALUE) {
            return -1;
        }
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static Intent a(int i, int i2, int i3, int i4, Uri uri) {
        Intent intent;
        if (uri != null) {
            intent = new Intent(WiGame.getContext(), CropImage.class);
            intent.putExtra("image-path", uri.getPath());
        } else {
            intent = new Intent("android.intent.action.GET_CONTENT");
        }
        intent.setType("image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", i3);
        intent.putExtra("aspectY", i4);
        intent.putExtra("outputX", i);
        intent.putExtra("outputY", i2);
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("return-data", true);
        return intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static Intent a(Uri uri) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", uri);
        intent.putExtra("return-data", true);
        return intent;
    }

    public static Bitmap a(Bitmap bitmap, int width, int height) {
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        if (width <= 0 || height <= 0 || (width >= bitmapWidth && height >= bitmapHeight)) {
            return bitmap;
        }
        float ratio = ((float) bitmapWidth) / ((float) bitmapHeight);
        if (bitmapWidth > bitmapHeight) {
            height = (int) (((float) width) / ratio);
        } else if (bitmapHeight > bitmapWidth) {
            width = (int) (((float) height) * ratio);
        }
        Bitmap.Config c2 = bitmap.getConfig();
        if (c2 == null) {
            c2 = Bitmap.Config.ARGB_8888;
        }
        Bitmap thumb = Bitmap.createBitmap(width, height, c2);
        Canvas canvas = h;
        Paint paint = e;
        canvas.setBitmap(thumb);
        paint.setDither(false);
        paint.setFilterBitmap(true);
        f.set(0, 0, width, height);
        g.set(0, 0, bitmapWidth, bitmapHeight);
        canvas.drawBitmap(bitmap, g, f, paint);
        return thumb;
    }

    public static Bitmap a(Map<String, Bitmap> map, boolean force, String type, String url) {
        return b(map, force, b(type, url), url, false);
    }

    public static Bitmap a(Map<String, Bitmap> map, boolean force, String id, String url, boolean female) {
        return b(map, force, id, url, female);
    }

    public static String a(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            int indexOf = str.indexOf("://");
            String[] split = str.substring(indexOf + 3).split("/");
            StringBuilder sb = new StringBuilder(str.substring(0, indexOf + 3));
            sb.append(split[0]);
            sb.append('/');
            for (int i = 1; i < split.length; i++) {
                String str3 = split[i];
                int indexOf2 = str3.indexOf(37);
                if (indexOf2 == -1) {
                    a(sb, str3, str2);
                } else {
                    int i2 = indexOf2;
                    int i3 = 0;
                    while (i2 != -1) {
                        a(sb, str3.substring(i3, i2), str2);
                        if (i2 == str3.length() - 1) {
                            sb.append("%25");
                            i3 = str3.length();
                            i2 = -1;
                        } else if (i2 < str3.length() - 2) {
                            char charAt = str3.charAt(i2 + 1);
                            if (charAt == '%') {
                                sb.append("%25");
                                i3 = i2 + 2;
                                i2 = str3.indexOf(37, i3);
                            } else if ((charAt < '0' || charAt > '9') && ((charAt < 'A' || charAt > 'Z') && (charAt < 'a' || charAt > 'z'))) {
                                sb.append("%25");
                                i3 = i2 + 1;
                                i2 = str3.indexOf(37, i3);
                            } else {
                                char charAt2 = str3.charAt(i2 + 2);
                                if ((charAt2 < '0' || charAt2 > '9') && ((charAt2 < 'A' || charAt2 > 'Z') && (charAt2 < 'a' || charAt2 > 'z'))) {
                                    sb.append("%25");
                                    i3 = i2 + 1;
                                    i2 = str3.indexOf(37, i3);
                                } else {
                                    sb.append(str3.substring(i2, i2 + 3));
                                    i3 = i2 + 3;
                                    i2 = str3.indexOf(37, i3);
                                }
                            }
                        } else {
                            sb.append("%25");
                            i3 = i2 + 1;
                            i2 = str3.indexOf(37, i3);
                        }
                    }
                    a(sb, str3.substring(i3), str2);
                }
                if (i < split.length - 1) {
                    sb.append('/');
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e2) {
            return str;
        }
    }

    public static String a(Map<String, String> map) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('{');
        for (Map.Entry next : map.entrySet()) {
            stringBuffer.append('\"').append((String) next.getKey()).append("\":\"").append((String) next.getValue()).append("\",");
        }
        if (stringBuffer.length() > 1) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        stringBuffer.append('}');
        return stringBuffer.toString();
    }

    public static String a(byte[] b2) {
        return a(b2, 0, b2.length);
    }

    public static String a(byte[] b2, int offset, int len) {
        if (b2 == null) {
            return "null";
        }
        int end = offset + len;
        if (end > b2.length) {
            end = b2.length;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = offset; i < end; i++) {
            sb.append(b[(b2[i] & 240) >>> 4]).append(b[b2[i] & 15]);
        }
        return sb.toString();
    }

    public static void a(Context context, String str, int i, View.OnClickListener onClickListener, int... iArr) {
        if (context instanceof Activity) {
            View inflate = LayoutInflater.from(context).inflate(t.e("wy_view_challenge_toast"), (ViewGroup) null);
            ((TextView) inflate.findViewById(t.d("wy_tv_toast"))).setText(str);
            if (onClickListener != null) {
                for (int findViewById : iArr) {
                    View findViewById2 = inflate.findViewById(findViewById);
                    if (findViewById2 != null) {
                        findViewById2.setOnClickListener(onClickListener);
                        findViewById2.setTag(inflate);
                    }
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            switch (WiGame.getToastSide()) {
                case 0:
                    layoutParams.gravity = 49;
                    layoutParams.topMargin = 30;
                    break;
                case 1:
                    layoutParams.gravity = 81;
                    layoutParams.bottomMargin = 30;
                    break;
            }
            ((Activity) context).addContentView(inflate, layoutParams);
            inflate.startAnimation(AnimationUtils.loadAnimation(context, i));
            WiGame.w().sendMessageDelayed(Message.obtain(WiGame.w(), 11, inflate), 7000);
        }
    }

    public static void a(Context context, String str, boolean z) {
        a(context, str, z, t.a("wy_toast_enter"));
    }

    public static void a(Context context, String str, boolean z, int i) {
        if (context instanceof Activity) {
            View inflate = LayoutInflater.from(context).inflate(t.e("wy_view_toast"), (ViewGroup) null);
            inflate.findViewById(t.d("wy_iv_fail_icon")).setVisibility(z ? 0 : 8);
            ((TextView) inflate.findViewById(t.d("wy_tv_toast"))).setText(str);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            switch (WiGame.getToastSide()) {
                case 0:
                    layoutParams.gravity = 49;
                    layoutParams.topMargin = 30;
                    break;
                case 1:
                    layoutParams.gravity = 81;
                    layoutParams.bottomMargin = 30;
                    break;
            }
            ((Activity) context).addContentView(inflate, layoutParams);
            inflate.startAnimation(AnimationUtils.loadAnimation(context, i));
            WiGame.w().sendMessageDelayed(Message.obtain(WiGame.w(), 11, inflate), 3300);
        }
    }

    public static void a(View view) {
        a(view, t.a("wy_toast_exit"));
    }

    public static void a(final View toastView, int animId) {
        try {
            if (toastView.getContext() != null) {
                Animation anim = AnimationUtils.loadAnimation(WiGame.getContext(), animId);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation a2) {
                        WiGame.w().sendMessage(Message.obtain(WiGame.w(), 12, toastView));
                    }

                    public void onAnimationRepeat(Animation a2) {
                    }

                    public void onAnimationStart(Animation a2) {
                    }
                });
                toastView.startAnimation(anim);
            }
        } catch (Exception e2) {
        }
    }

    public static void a(ViewGroup g2) {
        int count = g2.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = g2.getChildAt(i);
            if (v instanceof ViewGroup) {
                a((ViewGroup) v);
            } else {
                v.setEnabled(false);
            }
        }
    }

    public static void a(FrameLayout layout) {
        View loadingView = d.remove(layout);
        if (loadingView != null) {
            layout.removeView(loadingView);
            for (a callback : c) {
                callback.a();
            }
        }
    }

    public static void a(String str, boolean z) {
        a(str, z, t.a("wy_toast_enter"));
    }

    public static void a(String text, boolean fail, int animId) {
        a(WiGame.getContext(), text, fail, animId);
    }

    private static void a(StringBuilder sb, String str, String str2) throws UnsupportedEncodingException {
        int a2 = a(str, "?&=:,()+ ", 0);
        if (a2 == -1) {
            sb.append(URLEncoder.encode(str, str2));
            return;
        }
        int i = a2;
        int i2 = 0;
        while (i != -1) {
            sb.append(URLEncoder.encode(str.substring(i2, i), str2));
            char charAt = str.charAt(i);
            if (charAt == ' ') {
                sb.append("%20");
            } else {
                sb.append(charAt);
            }
            i2 = i + 1;
            i = a(str, "?&=:,()+ ", i2);
        }
        sb.append(URLEncoder.encode(str.substring(i2), str2));
    }

    public static boolean a(File file) {
        if (file == null) {
            return false;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        File[] children = file.listFiles();
        if (children != null) {
            for (File child : children) {
                if (!a(child)) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048 A[SYNTHETIC, Splitter:B:20:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0053 A[SYNTHETIC, Splitter:B:26:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r5, byte[] r6) {
        /*
            java.io.File r0 = r5.getParentFile()
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0011
            java.io.File r0 = r5.getParentFile()
            r0.mkdirs()
        L_0x0011:
            r0 = 0
            boolean r1 = r5.exists()     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            if (r1 != 0) goto L_0x001b
            r5.createNewFile()     // Catch:{ IOException -> 0x002d, all -> 0x004d }
        L_0x001b:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            r1.write(r6)     // Catch:{ IOException -> 0x0064, all -> 0x005d }
            r1.flush()     // Catch:{ IOException -> 0x0064, all -> 0x005d }
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ IOException -> 0x0057 }
        L_0x002b:
            r0 = 1
        L_0x002c:
            return r0
        L_0x002d:
            r1 = move-exception
        L_0x002e:
            java.lang.String r1 = "WiYun"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
            java.lang.String r3 = "Failed to save to file: "
            r2.<init>(r3)     // Catch:{ all -> 0x005f }
            java.lang.String r3 = r5.getAbsolutePath()     // Catch:{ all -> 0x005f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x005f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.w(r1, r2)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ IOException -> 0x0059 }
        L_0x004b:
            r0 = 0
            goto L_0x002c
        L_0x004d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0056:
            throw r0
        L_0x0057:
            r0 = move-exception
            goto L_0x002b
        L_0x0059:
            r0 = move-exception
            goto L_0x004b
        L_0x005b:
            r1 = move-exception
            goto L_0x0056
        L_0x005d:
            r0 = move-exception
            goto L_0x0051
        L_0x005f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x0064:
            r0 = move-exception
            r0 = r1
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.h.a(java.io.File, byte[]):boolean");
    }

    public static boolean a(CharSequence text) {
        return a.matcher(text).matches();
    }

    public static boolean a(byte[] b2, byte[] sub, boolean skipZero) {
        int bLen = b2.length;
        int subLen = sub.length;
        int len = bLen - subLen;
        for (int i = 0; i < len; i++) {
            boolean found = true;
            int j = 0;
            while (true) {
                if (j >= subLen) {
                    break;
                }
                if (b2[((skipZero ? 2 : 1) * j) + i] != sub[j]) {
                    found = false;
                    break;
                }
                j++;
            }
            if (found) {
                return true;
            }
        }
        return false;
    }

    public static byte[] a(Context context, int id) {
        InputStream in = null;
        byte[] bytes = null;
        if (context == null) {
            return null;
        }
        try {
            in = context.getResources().openRawResource(id);
            byte[] buf = new byte[1024];
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            for (int i = 0; i != -1; i = in.read(buf)) {
                out.write(buf, 0, i);
            }
            bytes = out.toByteArray();
            out.flush();
            out.close();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e2) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
        return bytes;
    }

    public static byte[] a(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight() * 4);
            try {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            } catch (IOException e2) {
                Log.w("WiYun", "unable to compress bitmap to png data");
            }
        }
        return null;
    }

    public static byte[] a(i c2, String plain, String name) {
        if (plain == null) {
            return new byte[0];
        }
        if (c2 == null) {
            c2 = new i();
        }
        return c.a(c2.b(d(plain), k(name)));
    }

    public static byte[] a(i c2, byte[] plain, String name) {
        if (plain == null) {
            return new byte[0];
        }
        if (c2 == null) {
            c2 = new i();
        }
        return c.a(c2.b(plain, k(name)));
    }

    public static long b(String value) {
        if (value == null) {
            return 0;
        }
        try {
            return Long.parseLong(value.trim());
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    public static Bitmap b(Map<String, Bitmap> map, boolean force, String id, String url) {
        return b(map, force, id, url, false);
    }

    private static Bitmap b(Map<String, Bitmap> map, boolean z, String str, String str2, boolean z2) {
        Bitmap bitmap = map == null ? null : map.get(str);
        if (z && bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            map.remove(str);
            bitmap = null;
        }
        if (bitmap == null || bitmap.isRecycled()) {
            String f2 = f(str);
            bitmap = WiGame.a(str, str2, "p_".equals(f2) ? z2 ? t.c("wy_default_avatar_female") : t.c("wy_default_avatar_male") : "app_".equals(f2) ? t.c("wy_default_app_icon") : "ach_".equals(f2) ? t.c("wy_icon_unlocked") : "sscr_".equals(f2) ? 0 : "bscr_".equals(f2) ? 0 : "cha_".equals(f2) ? t.c("wy_default_challenge_icon") : "mrk_".equals(f2) ? t.c("wy_default_market_icon") : "ss_".equals(f2) ? t.c("wy_default_game_slot_shot") : "gii_".equals(f2) ? t.c("wy_default_market_icon") : 0);
            if (map != null) {
                map.put(str, bitmap);
            }
        }
        return bitmap;
    }

    public static String b(i c2, String crypt, String name) {
        if (TextUtils.isEmpty(crypt)) {
            return null;
        }
        if (c2 == null) {
            c2 = new i();
        }
        try {
            byte[] dec = c2.a(c.d(d(crypt)), k(name));
            if (dec == null) {
                return null;
            }
            return b(dec);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String b(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer(str);
        if (str2 == null || !str2.startsWith("http")) {
            stringBuffer.append(str2);
        } else {
            stringBuffer.append(h(str2));
        }
        return stringBuffer.toString();
    }

    public static String b(byte[] b2) {
        return b2 == null ? "" : b(b2, 0, b2.length);
    }

    public static String b(byte[] b2, int start, int length) {
        if (b2 == null) {
            return "";
        }
        try {
            return new String(b2, start, length, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            return "";
        }
    }

    public static void b(ViewGroup g2) {
        int count = g2.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = g2.getChildAt(i);
            if (v instanceof ViewGroup) {
                b((ViewGroup) v);
            } else {
                v.setEnabled(true);
            }
        }
    }

    public static byte[] b(i c2, byte[] crypt, String name) {
        if (crypt == null) {
            return null;
        }
        if (c2 == null) {
            c2 = new i();
        }
        try {
            return c2.a(c.d(crypt), k(name));
        } catch (Exception e2) {
            return crypt;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003c A[SYNTHETIC, Splitter:B:24:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(java.io.File r8) {
        /*
            r3 = 0
            r1 = 0
            byte[] r1 = (byte[]) r1
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0030, all -> 0x0039 }
            r4.<init>(r8)     // Catch:{ Throwable -> 0x0030, all -> 0x0039 }
            r6 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r6]     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            r5.<init>()     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            r2 = 0
        L_0x0013:
            r6 = -1
            if (r2 != r6) goto L_0x0027
            byte[] r1 = r5.toByteArray()     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            r5.flush()     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            r5.close()     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            if (r4 == 0) goto L_0x004b
            r4.close()     // Catch:{ IOException -> 0x0040 }
            r3 = r4
        L_0x0026:
            return r1
        L_0x0027:
            r6 = 0
            r5.write(r0, r6, r2)     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            int r2 = r4.read(r0)     // Catch:{ Throwable -> 0x0048, all -> 0x0045 }
            goto L_0x0013
        L_0x0030:
            r6 = move-exception
        L_0x0031:
            if (r3 == 0) goto L_0x0026
            r3.close()     // Catch:{ IOException -> 0x0037 }
            goto L_0x0026
        L_0x0037:
            r6 = move-exception
            goto L_0x0026
        L_0x0039:
            r6 = move-exception
        L_0x003a:
            if (r3 == 0) goto L_0x003f
            r3.close()     // Catch:{ IOException -> 0x0043 }
        L_0x003f:
            throw r6
        L_0x0040:
            r6 = move-exception
            r3 = r4
            goto L_0x0026
        L_0x0043:
            r7 = move-exception
            goto L_0x003f
        L_0x0045:
            r6 = move-exception
            r3 = r4
            goto L_0x003a
        L_0x0048:
            r6 = move-exception
            r3 = r4
            goto L_0x0031
        L_0x004b:
            r3 = r4
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.h.b(java.io.File):byte[]");
    }

    public static int c(String value) {
        return a(10, value);
    }

    public static String c(byte[] b2) {
        return b2 == null ? "null" : a(com.wiyun.game.e.a.a(b2));
    }

    public static byte[] d(String string) {
        if (string == null) {
            return new byte[0];
        }
        try {
            return string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(bos);
                dos.writeUTF(string);
                byte[] jdata = bos.toByteArray();
                bos.close();
                dos.close();
                byte[] buff = new byte[(jdata.length - 2)];
                System.arraycopy(jdata, 2, buff, 0, buff.length);
                return buff;
            } catch (IOException e3) {
                return new byte[0];
            }
        }
    }

    public static String e(String str) {
        return a(str, "utf-8");
    }

    public static String f(String normalId) {
        if (normalId.startsWith("app_")) {
            return "app_";
        }
        if (normalId.startsWith("p_")) {
            return "p_";
        }
        if (normalId.startsWith("ach_")) {
            return "ach_";
        }
        if (normalId.startsWith("cha_")) {
            return "cha_";
        }
        if (normalId.startsWith("sscr_")) {
            return "sscr_";
        }
        if (normalId.startsWith("bscr_")) {
            return "bscr_";
        }
        if (normalId.startsWith("mrk_")) {
            return "mrk_";
        }
        if (normalId.startsWith("ss_")) {
            return "ss_";
        }
        if (normalId.startsWith("gii_")) {
            return "gii_";
        }
        return null;
    }

    public static String g(String normalId) {
        String type = f(normalId);
        return type == null ? normalId : normalId.substring(type.length());
    }

    public static String h(String url) {
        return url == null ? "null" : a(com.wiyun.game.e.a.b(url));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.wiyun.game.h.a(int, java.lang.String):int
      com.wiyun.game.h.a(java.lang.String, java.lang.String):java.lang.String
      com.wiyun.game.h.a(android.view.View, int):void
      com.wiyun.game.h.a(java.io.File, byte[]):boolean
      com.wiyun.game.h.a(android.content.Context, int):byte[]
      com.wiyun.game.h.a(java.lang.String, boolean):void */
    public static void i(String text) {
        a(text, false);
    }

    public static String j(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e2) {
            return str;
        }
    }

    private static byte[] k(String name) {
        byte[] b1 = d(WiGame.getContext().getPackageName());
        for (int i = 0; i < b1.length; i++) {
            b1[i] = (byte) (b1[i] + (2010 % (i + 6)));
            a((b1[i] + 1001) ^ 33);
            b1[i] = (byte) (b1[i] ^ (i + 111));
            a((b1[0] + b1[i]) ^ 44);
            b1[i] = (byte) (b1[i] + (449 % (i + 6)));
        }
        byte[] b12 = com.wiyun.game.e.a.a(b1);
        byte[] b2 = d(name);
        int e2 = Math.abs(b12[0] - b12[4]);
        a(e2 ^ b12[e2 % b12.length]);
        byte[] b22 = com.wiyun.game.e.a.a(b2);
        for (int i2 = 0; i2 < b12.length; i2++) {
            b12[i2] = (byte) (b12[i2] ^ b22[(b12.length - 1) - i2]);
        }
        return b12;
    }
}
