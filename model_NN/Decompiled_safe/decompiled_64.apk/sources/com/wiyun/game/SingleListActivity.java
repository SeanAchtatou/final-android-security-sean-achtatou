package com.wiyun.game;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.wiyun.game.a.h;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import java.util.HashMap;
import java.util.Map;

abstract class SingleListActivity extends ListActivity implements View.OnClickListener, com.wiyun.game.b.a, com.wiyun.game.b.b {
    protected String a;
    protected Map<String, Bitmap> b;
    protected FrameLayout c;
    protected h d;
    private BroadcastReceiver e = new q(this);

    private final class a extends BaseAdapter {
        private a() {
        }

        /* synthetic */ a(SingleListActivity singleListActivity, a aVar) {
            this();
        }

        public int getCount() {
            return SingleListActivity.this.j();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return SingleListActivity.this.a(SingleListActivity.this.a(position), position, convertView, parent);
        }
    }

    protected static class b {
        int a;
        TextView b;
        TextView c;
        TextView d;
        ImageView e;

        protected b() {
        }
    }

    SingleListActivity() {
    }

    private void n() {
        if (this.b != null) {
            for (Bitmap bitmap : this.b.values()) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
            this.b.clear();
        }
    }

    /* access modifiers changed from: protected */
    public abstract int a(int i);

    /* access modifiers changed from: protected */
    public abstract View a(int i, int i2, View view, ViewGroup viewGroup);

    /* access modifiers changed from: protected */
    public void a(Intent intent) {
        this.a = intent.getStringExtra("app_id");
    }

    /* access modifiers changed from: protected */
    public void a(IntentFilter filter) {
    }

    public void a(e e2) {
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void b(Intent intent) {
    }

    public void b(e e2) {
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void d();

    /* access modifiers changed from: protected */
    public boolean e() {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public void g() {
        a(getIntent());
        if (k()) {
            this.b = new HashMap();
        }
        if (b()) {
            this.d = new h(true);
        }
        if (k()) {
            IntentFilter filter = new IntentFilter("com.wiyun.game.IMAGE_DOWNLOADED");
            a(filter);
            registerReceiver(this.e, filter);
        }
        d.a().a((com.wiyun.game.b.b) this);
        if (a()) {
            d.a().a((com.wiyun.game.b.a) this);
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        l();
        i();
        getListView().setOnCreateContextMenuListener(this);
        setListAdapter(new a(this, null));
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    /* access modifiers changed from: protected */
    public abstract int j();

    /* access modifiers changed from: protected */
    public boolean k() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void l() {
        this.c = (FrameLayout) findViewById(16908290);
    }

    /* access modifiers changed from: protected */
    public void m() {
        ListAdapter adapter = getListAdapter();
        if (adapter != null) {
            ((BaseAdapter) adapter).notifyDataSetChanged();
        }
    }

    public void onClick(View v) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (e()) {
            setRequestedOrientation(WiGame.i);
        }
        requestWindowFeature(1);
        setContentView(f());
        g();
        h();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        setListAdapter(null);
        h.a(this.c);
        if (k()) {
            unregisterReceiver(this.e);
        }
        d.a().b((com.wiyun.game.b.b) this);
        if (a()) {
            d.a().b((com.wiyun.game.b.a) this);
        }
        n();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (c()) {
            d();
        }
    }

    public boolean onSearchRequested() {
        return false;
    }
}
