package com.wiyun.game;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class w extends BroadcastReceiver {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SendChallenge a;

    w(SendChallenge sendChallenge) {
        this.a = sendChallenge;
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.wiyun.game.IMAGE_DOWNLOADED".equals(intent.getAction())) {
            final String id = intent.getStringExtra("image_id");
            if (this.a.v.containsKey(id)) {
                this.a.runOnUiThread(new Runnable() {
                    public void run() {
                        h.b(w.this.a.v, true, id, null);
                        w.this.a.d();
                    }
                });
            }
        }
    }
}
