package com.wiyun.game;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.google.ads.AdActivity;
import com.wiyun.game.a.d;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class e {
    static boolean a = false;
    private static int b = 10000;
    /* access modifiers changed from: private */
    public static boolean c;
    private static String d;

    static String a(Context context) {
        if (TextUtils.isEmpty(d)) {
            Configuration config = context.getResources().getConfiguration();
            d = String.valueOf((config.mcc * 100) + config.mnc);
        }
        return d;
    }

    static void a(final Context context, final String appId) {
        new Thread() {
            public void run() {
                Process.setThreadPriority(10);
                if (!TextUtils.isEmpty(appId)) {
                    DefaultHttpClient client = e.b();
                    try {
                        StringBuilder buf = new StringBuilder("http://d.wiyun.com/was/m");
                        String did = WiGame.p();
                        String uid = WiGame.q();
                        if (did.equals(uid)) {
                            did = "000000000000000";
                        }
                        buf.append('?').append(AdActivity.URL_PARAM).append("=").append(did);
                        e.b(buf, AdActivity.INTENT_ACTION_PARAM, uid);
                        e.b(buf, "a", appId);
                        e.b(buf, "c", e.a(context));
                        e.b(buf, "n", e.b(context));
                        e.b(buf, "l", Locale.getDefault().getLanguage());
                        e.b(buf, AdActivity.ORIENTATION_PARAM, WiGame.r() ? "Android Emulator" : "Android");
                        e.b(buf, "v", Build.VERSION.RELEASE);
                        e.b(buf, "b", Build.BRAND);
                        e.b(buf, AdActivity.TYPE_PARAM, Build.MODEL);
                        e.b(buf, "ch", WiGame.getChannel());
                        DisplayMetrics dm = WiGame.getContext().getResources().getDisplayMetrics();
                        e.b(buf, "d", String.format("%dx%dx%d", Integer.valueOf(dm.widthPixels), Integer.valueOf(dm.heightPixels), Integer.valueOf((int) (dm.density * 160.0f))));
                        client.execute(new HttpGet(h.e(buf.toString())));
                    } catch (Exception e) {
                    } finally {
                        client.getConnectionManager().shutdown();
                    }
                }
            }
        }.start();
    }

    static int b(Context context) {
        if (d.a(context)) {
            return 4;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return 0;
        }
        int networkType = telephonyManager.getNetworkType();
        return (networkType == 1 || networkType == 2 || networkType == 0) ? 2 : 3;
    }

    /* access modifiers changed from: private */
    public static DefaultHttpClient b() {
        HttpHost proxy;
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(params, false);
        HttpConnectionParams.setConnectionTimeout(params, b);
        HttpConnectionParams.setSoTimeout(params, b);
        DefaultHttpClient client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
        if (!d.b() && d.c() && (proxy = d.d()) != null) {
            client.getParams().setParameter("http.route.default-proxy", proxy);
        }
        return client;
    }

    static void b(final Context context, final String appKey) {
        if (!a) {
            synchronized (e.class) {
                if (!c) {
                    c = true;
                    new Thread() {
                        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public void run() {
                            /*
                                r14 = this;
                                r8 = 10
                                android.os.Process.setThreadPriority(r8)
                                java.lang.String r8 = r3
                                java.lang.String r0 = com.wiyun.game.a.e.a(r8)
                                boolean r8 = android.text.TextUtils.isEmpty(r0)
                                if (r8 == 0) goto L_0x001d
                                java.lang.Class<com.wiyun.game.e> r8 = com.wiyun.game.e.class
                                monitor-enter(r8)
                                r9 = 0
                                com.wiyun.game.e.c = r9     // Catch:{ all -> 0x001a }
                                monitor-exit(r8)     // Catch:{ all -> 0x001a }
                            L_0x0019:
                                return
                            L_0x001a:
                                r9 = move-exception
                                monitor-exit(r8)     // Catch:{ all -> 0x001a }
                                throw r9
                            L_0x001d:
                                org.apache.http.impl.client.DefaultHttpClient r2 = com.wiyun.game.e.b()
                                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "http://d.wiyun.com/was/r"
                                r1.<init>(r8)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r3 = com.wiyun.game.WiGame.p()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r7 = com.wiyun.game.WiGame.q()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                boolean r8 = r3.equals(r7)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                if (r8 == 0) goto L_0x0038
                                java.lang.String r3 = "000000000000000"
                            L_0x0038:
                                r8 = 63
                                java.lang.StringBuilder r8 = r1.append(r8)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r9 = "u"
                                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r9 = "="
                                java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r8.append(r3)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "i"
                                com.wiyun.game.e.b(r1, r8, r7)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "a"
                                com.wiyun.game.e.b(r1, r8, r0)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "c"
                                android.content.Context r9 = r2     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r9 = com.wiyun.game.e.a(r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "n"
                                android.content.Context r9 = r2     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                int r9 = com.wiyun.game.e.b(r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "l"
                                java.util.Locale r9 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r9 = r9.getLanguage()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "o"
                                boolean r9 = com.wiyun.game.WiGame.r()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                if (r9 == 0) goto L_0x0112
                                java.lang.String r9 = "Android Emulator"
                            L_0x0084:
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "v"
                                java.lang.String r9 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "b"
                                java.lang.String r9 = android.os.Build.BRAND     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "m"
                                java.lang.String r9 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "ch"
                                java.lang.String r9 = com.wiyun.game.WiGame.getChannel()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                android.content.Context r8 = com.wiyun.game.WiGame.getContext()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                android.content.res.Resources r8 = r8.getResources()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                android.util.DisplayMetrics r4 = r8.getDisplayMetrics()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = "d"
                                java.lang.String r9 = "%dx%dx%d"
                                r10 = 3
                                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r11 = 0
                                int r12 = r4.widthPixels     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r10[r11] = r12     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r11 = 1
                                int r12 = r4.heightPixels     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r10[r11] = r12     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r11 = 2
                                float r12 = r4.density     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r13 = 1126170624(0x43200000, float:160.0)
                                float r12 = r12 * r13
                                int r12 = (int) r12     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r10[r11] = r12     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r9 = java.lang.String.format(r9, r10)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                com.wiyun.game.e.b(r1, r8, r9)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = r1.toString()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                java.lang.String r8 = com.wiyun.game.h.e(r8)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r5.<init>(r8)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                org.apache.http.HttpResponse r6 = r2.execute(r5)     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                org.apache.http.StatusLine r8 = r6.getStatusLine()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                int r8 = r8.getStatusCode()     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                                r9 = 300(0x12c, float:4.2E-43)
                                if (r8 >= r9) goto L_0x00fe
                                r8 = 1
                                com.wiyun.game.e.a = r8     // Catch:{ Exception -> 0x0116, all -> 0x012b }
                            L_0x00fe:
                                org.apache.http.conn.ClientConnectionManager r8 = r2.getConnectionManager()
                                r8.shutdown()
                                java.lang.Class<com.wiyun.game.e> r8 = com.wiyun.game.e.class
                                monitor-enter(r8)
                                r9 = 0
                                com.wiyun.game.e.c = r9     // Catch:{ all -> 0x010f }
                                monitor-exit(r8)     // Catch:{ all -> 0x010f }
                                goto L_0x0019
                            L_0x010f:
                                r9 = move-exception
                                monitor-exit(r8)     // Catch:{ all -> 0x010f }
                                throw r9
                            L_0x0112:
                                java.lang.String r9 = "Android"
                                goto L_0x0084
                            L_0x0116:
                                r8 = move-exception
                                org.apache.http.conn.ClientConnectionManager r8 = r2.getConnectionManager()
                                r8.shutdown()
                                java.lang.Class<com.wiyun.game.e> r8 = com.wiyun.game.e.class
                                monitor-enter(r8)
                                r9 = 0
                                com.wiyun.game.e.c = r9     // Catch:{ all -> 0x0128 }
                                monitor-exit(r8)     // Catch:{ all -> 0x0128 }
                                goto L_0x0019
                            L_0x0128:
                                r9 = move-exception
                                monitor-exit(r8)     // Catch:{ all -> 0x0128 }
                                throw r9
                            L_0x012b:
                                r8 = move-exception
                                org.apache.http.conn.ClientConnectionManager r9 = r2.getConnectionManager()
                                r9.shutdown()
                                java.lang.Class<com.wiyun.game.e> r9 = com.wiyun.game.e.class
                                monitor-enter(r9)
                                r10 = 0
                                com.wiyun.game.e.c = r10     // Catch:{ all -> 0x013c }
                                monitor-exit(r9)     // Catch:{ all -> 0x013c }
                                throw r8
                            L_0x013c:
                                r8 = move-exception
                                monitor-exit(r9)     // Catch:{ all -> 0x013c }
                                throw r8
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.e.AnonymousClass2.run():void");
                        }
                    }.start();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static void b(StringBuilder param, String key, int val) {
        param.append("&").append(key).append("=").append(val);
    }

    /* access modifiers changed from: private */
    public static void b(StringBuilder param, String key, String val) throws UnsupportedEncodingException {
        if (val != null && val.length() > 0) {
            param.append("&").append(key).append("=").append(URLEncoder.encode(val, "UTF-8"));
        }
    }
}
