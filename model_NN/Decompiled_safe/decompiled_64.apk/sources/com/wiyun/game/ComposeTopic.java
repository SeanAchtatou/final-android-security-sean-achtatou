package com.wiyun.game;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.saubcy.games.ColorfulBalls.market.ColorfulBalls;
import com.wiyun.game.b.b;
import com.wiyun.game.b.d;
import com.wiyun.game.b.e;
import java.io.File;

public class ComposeTopic extends Activity implements TextWatcher, View.OnClickListener, View.OnTouchListener, b {
    private String a;
    /* access modifiers changed from: private */
    public String b;
    private String c;
    private String d;
    /* access modifiers changed from: private */
    public int e;
    private EditText f;
    private EditText g;
    private ImageView h;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public ViewGroup j;
    private TextView k;
    private TextView l;
    private TableLayout m;
    private Uri n;
    private Bitmap o;
    private Bitmap p;
    private String[] q;
    private boolean r;
    private int s;

    /* access modifiers changed from: private */
    public void a() {
        InputMethodManager imm = (InputMethodManager) getSystemService("input_method");
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    private void b() {
        Intent intent = getIntent();
        this.a = intent.getStringExtra("app_id");
        this.e = intent.getIntExtra("post_mode", 1);
        this.b = intent.getStringExtra("topic_id");
        this.c = intent.getStringExtra("prefill");
        this.d = intent.getStringExtra("title");
        this.q = getResources().getStringArray(t.b("wy_smily_array"));
        d.a().a(this);
    }

    private void c() {
        this.f = (EditText) findViewById(t.d("wy_et_title"));
        this.k = (TextView) findViewById(t.d("wy_tv_title"));
        this.l = (TextView) findViewById(t.d("wy_tv_hint"));
        this.g = (EditText) findViewById(t.d("wy_et_content"));
        this.h = (ImageView) findViewById(t.d("wy_iv_picture"));
        this.i = findViewById(t.d("wy_ll_progress_panel"));
        this.j = (ViewGroup) findViewById(t.d("wy_ll_main_panel"));
        this.m = (TableLayout) findViewById(t.d("wy_tl_smily_panel"));
        switch (this.e) {
            case 1:
                this.k.setText(t.f("wy_title_compose_topic"));
                break;
            case 2:
                this.k.setText(t.f("wy_title_reply_topic"));
                break;
            case 3:
                this.k.setText(t.f("wy_title_compose_activity"));
                break;
        }
        this.f.setVisibility(this.e == 1 ? 0 : 8);
        this.l.setVisibility(this.e == 1 ? 8 : 0);
        if (this.e == 2) {
            this.l.setText(this.d);
        }
        ((Button) findViewById(t.d("wy_b_cancel"))).setOnClickListener(this);
        ((Button) findViewById(t.d("wy_b_post"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_camera"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_images"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_smily"))).setOnClickListener(this);
        ((ImageButton) findViewById(t.d("wy_ib_my_location"))).setOnClickListener(this);
        this.g.addTextChangedListener(this);
        this.g.setOnTouchListener(this);
        this.f.addTextChangedListener(this);
        this.f.setOnTouchListener(this);
        d();
    }

    private void d() {
        int i2;
        for (int i3 = 0; i3 < this.q.length; i3 = i2) {
            TableRow tableRow = new TableRow(this);
            this.m.addView(tableRow);
            i2 = i3;
            int i4 = 0;
            while (i4 < 5 && i2 < this.q.length) {
                ImageButton imageButton = new ImageButton(this);
                imageButton.setBackgroundResource(t.c("wy_smily_bg"));
                imageButton.setImageResource(t.c(String.format("wy_smily_%d", Integer.valueOf(i2 + 1))));
                imageButton.setId(i2 + 10000);
                imageButton.setOnClickListener(this);
                tableRow.addView(imageButton);
                if (i4 < 4) {
                    ImageView imageView = new ImageView(this);
                    imageView.setImageResource(t.c("wy_separator_v_style1"));
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    tableRow.addView(imageView, new TableRow.LayoutParams(2, -1));
                }
                i4++;
                i2++;
            }
            if (i2 < this.q.length) {
                TableRow tableRow2 = new TableRow(this);
                this.m.addView(tableRow2);
                ImageView imageView2 = new ImageView(this);
                imageView2.setImageResource(t.c("wy_separator_h_style1"));
                imageView2.setScaleType(ImageView.ScaleType.FIT_XY);
                TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-1, 2);
                layoutParams.span = 9;
                tableRow2.addView(imageView2, layoutParams);
            }
        }
    }

    public void afterTextChanged(Editable s2) {
        if (this.r) {
            this.r = false;
            int index = s2.toString().lastIndexOf(91);
            if (index != -1) {
                s2.delete(index, this.s);
            }
        }
    }

    public void b(final e e2) {
        switch (e2.a) {
            case 55:
            case 58:
            case 60:
                if (e2.c) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            ComposeTopic.this.i.setVisibility(4);
                            h.b(ComposeTopic.this.j);
                            Toast.makeText(ComposeTopic.this, (String) e2.e, 0).show();
                        }
                    });
                    return;
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Intent intent = new Intent("com.wiyun.game.RELOAD");
                            intent.setFlags(1073741824);
                            int id = 0;
                            switch (ComposeTopic.this.e) {
                                case 1:
                                    id = t.f("wy_toast_post_topic_successful");
                                    break;
                                case 2:
                                    id = t.f("wy_toast_reply_topic_successful");
                                    intent.putExtra("url", String.format("http://%s:%s/m/topic?id=%s&start=-1&last=true", f.d, f.a, ComposeTopic.this.b));
                                    break;
                                case 3:
                                    id = t.f("wy_toast_post_activity_successful");
                                    break;
                            }
                            if (id != 0) {
                                Toast.makeText(ComposeTopic.this, id, 0).show();
                            }
                            ComposeTopic.this.sendBroadcast(intent);
                            ComposeTopic.this.a();
                            ComposeTopic.this.finish();
                        }
                    });
                    return;
                }
            case 56:
            case 57:
            case 59:
            default:
                return;
        }
    }

    public void beforeTextChanged(CharSequence s2, int start, int count, int after) {
        if (s2.length() > start && s2.charAt(start) == ']' && count == 1 && after == 0) {
            this.r = true;
            this.s = start;
            return;
        }
        this.r = false;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras;
        switch (requestCode) {
            case 1:
                if (resultCode == -1 && (extras = data.getExtras()) != null) {
                    this.o = (Bitmap) extras.getParcelable("data");
                    Log.d("test", "picture: " + this.o);
                    if (this.o != null) {
                        int size = (int) TypedValue.applyDimension(1, 32.0f, getResources().getDisplayMetrics());
                        this.p = h.a(this.o, size, size);
                        this.h.setVisibility(0);
                        this.h.setImageBitmap(this.p);
                        this.h.requestLayout();
                    }
                }
                if (this.n != null) {
                    File f2 = new File(this.n.getPath());
                    if (f2.exists()) {
                        f2.delete();
                    }
                    this.n = null;
                    return;
                }
                return;
            case 2:
                if (resultCode == -1) {
                    startActivityForResult(h.a((int) ColorfulBalls.BaseWidth, 480, 2, 3, this.n), 1);
                    return;
                }
                return;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                return;
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == t.d("wy_b_post")) {
            String trim = this.g.getText().toString().trim();
            String trim2 = this.f.getText().toString().trim();
            if (TextUtils.isEmpty(trim)) {
                Toast.makeText(this, t.f("wy_toast_content_cannot_be_empty"), 0).show();
            } else if (this.e != 1 || !TextUtils.isEmpty(trim2)) {
                this.i.setVisibility(0);
                h.a(this.j);
                switch (this.e) {
                    case 1:
                        f.b(this.a, trim2, trim, h.a(this.o));
                        return;
                    case 2:
                        if (!TextUtils.isEmpty(this.c)) {
                            trim = String.valueOf(this.c) + trim;
                        }
                        f.a(this.a, this.b, trim, h.a(this.o));
                        return;
                    case 3:
                        f.a(trim, h.a(this.o));
                        return;
                    default:
                        return;
                }
            } else {
                Toast.makeText(this, t.f("wy_toast_title_cannot_be_empty"), 0).show();
            }
        } else if (id == t.d("wy_b_cancel")) {
            a();
            finish();
        } else if (id == t.d("wy_ib_camera")) {
            try {
                this.n = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "pic_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                startActivityForResult(h.a(this.n), 2);
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(this, t.f("wy_toast_no_camera"), 0).show();
            }
        } else if (id == t.d("wy_ib_images")) {
            try {
                startActivityForResult(h.a((int) ColorfulBalls.BaseWidth, (int) ColorfulBalls.BaseWidth, 1, 1, (Uri) null), 1);
            } catch (ActivityNotFoundException e3) {
                Toast.makeText(this, t.f("wy_toast_no_image_gallery"), 0).show();
            }
        } else if (id == t.d("wy_ib_smily")) {
            if (this.m.getVisibility() == 0) {
                this.m.setVisibility(8);
                return;
            }
            this.m.setVisibility(0);
            a();
        } else if (id == t.d("wy_ib_my_location")) {
            if (WiGame.h()) {
                if (this.g.getText().length() > 0) {
                    this.g.append("\n\n");
                }
                this.g.append(t.h("wy_label_my_location_is_colon"));
                this.g.append(" [location=");
                this.g.append(String.valueOf(WiGame.getLatitude()));
                this.g.append(",");
                this.g.append(String.valueOf(WiGame.getLongitude()));
                this.g.append("]");
                return;
            }
            Toast.makeText(this, t.f("wy_toast_no_my_location"), 0).show();
        } else if (id >= 10000) {
            int selectionEnd = this.g.getSelectionEnd();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("[").append(this.q[id - 10000]).append("]");
            this.g.getEditableText().insert(selectionEnd, stringBuffer.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (WiGame.j) {
            getWindow().addFlags(1024);
        }
        setContentView(t.e("wy_activity_compose_topic"));
        b();
        c();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.p != null && !this.p.isRecycled()) {
            this.p.recycle();
            this.p = null;
        }
        if (this.o != null && !this.o.isRecycled()) {
            this.o.recycle();
            this.o = null;
        }
        d.a().b(this);
        super.onDestroy();
    }

    public boolean onSearchRequested() {
        return false;
    }

    public void onTextChanged(CharSequence s2, int start, int before, int count) {
    }

    public boolean onTouch(View v, MotionEvent e2) {
        if (v != this.g && v != this.f) {
            return false;
        }
        switch (e2.getAction()) {
            case 1:
                this.m.setVisibility(8);
                return false;
            default:
                return false;
        }
    }
}
