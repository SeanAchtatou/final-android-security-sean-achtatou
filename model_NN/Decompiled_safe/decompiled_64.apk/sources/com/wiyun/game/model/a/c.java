package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class c {
    private String a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private boolean f;

    public static c a(JSONObject dict) {
        c leaderboard = new c();
        leaderboard.a(dict.optString("name"));
        leaderboard.b(dict.optString("id"));
        leaderboard.c(dict.optString("description"));
        leaderboard.b(!dict.optBoolean("sort_order"));
        leaderboard.a("T".equalsIgnoreCase(dict.optString("data_format")));
        leaderboard.c(!dict.optBoolean("multi_score"));
        if (TextUtils.isEmpty(leaderboard.b())) {
            return null;
        }
        return leaderboard;
    }

    public String a() {
        return this.a;
    }

    public void a(String name) {
        this.a = name;
    }

    public void a(boolean mIsTime) {
        this.d = mIsTime;
    }

    public String b() {
        return this.b;
    }

    public void b(String id) {
        this.b = id;
    }

    public void b(boolean mAscend) {
        this.e = mAscend;
    }

    public void c(String description) {
        this.c = description;
    }

    public void c(boolean mUnique) {
        this.f = mUnique;
    }

    public boolean c() {
        return this.d;
    }

    public boolean d() {
        return this.e;
    }

    public boolean e() {
        return this.f;
    }
}
