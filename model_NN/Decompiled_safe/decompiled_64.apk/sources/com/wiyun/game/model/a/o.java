package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class o {
    private String a;
    private String b;
    private String c;
    private long d;
    private String e;
    private String f;
    private boolean g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private int r;

    public static o a(JSONObject dict) {
        o n2 = new o();
        String id = dict.optString("id");
        String type = dict.optString("type");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        n2.a(id);
        n2.b(type);
        n2.c(dict.optString("body"));
        n2.a(dict.optLong("time"));
        JSONObject params = dict.optJSONObject("params");
        if (params == null) {
            return null;
        }
        if ("app_share".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.g(params.optString("app_id"));
            n2.h(params.optString("app_name"));
            n2.i(params.optString("app_icon"));
            n2.m(params.optString("message"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if ("friends_accept".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if ("friends_invite".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.m(params.optString("message"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if ("join_accept".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.j(params.optString("email"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if ("messages_received".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if ("comment_new_reply".equals(type)) {
            n2.g(params.optString("app_id"));
            n2.k(params.optString("topic_id"));
            n2.l(params.optString("topic_title"));
        } else if ("challenge_completed".equals(type) || "challenge_to_user".equals(type)) {
            n2.d(params.optString("user_id"));
            n2.e(params.optString("username"));
            n2.f(params.optString("avatar"));
            n2.n(params.optString("challenge_id"));
            n2.g(params.optString("app_id"));
            n2.a("F".equalsIgnoreCase(dict.optString("gender")));
        } else if (!"unlock_sys_achievement".equals(type)) {
            return null;
        } else {
            n2.o(params.optString("achievement_name"));
            n2.a(params.optInt("achievement_point"));
        }
        return n2;
    }

    public void a(int sysAchievementPoint) {
        this.r = sysAchievementPoint;
    }

    public void a(long time) {
        this.d = time;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean female) {
        this.g = female;
    }

    public void b(String type) {
        this.b = type;
    }

    public void c(String body) {
        this.c = body;
    }

    public void d(String userId) {
        this.e = userId;
    }

    public void e(String username) {
        this.f = username;
    }

    public void f(String avatarUrl) {
        this.h = avatarUrl;
    }

    public void g(String appId) {
        this.j = appId;
    }

    public void h(String appName) {
        this.k = appName;
    }

    public void i(String appIconUrl) {
        this.l = appIconUrl;
    }

    public void j(String email) {
        this.m = email;
    }

    public void k(String topicId) {
        this.n = topicId;
    }

    public void l(String topicTitle) {
        this.o = topicTitle;
    }

    public void m(String message) {
        this.i = message;
    }

    public void n(String challengeId) {
        this.p = challengeId;
    }

    public void o(String sysAchievementName) {
        this.q = sysAchievementName;
    }
}
