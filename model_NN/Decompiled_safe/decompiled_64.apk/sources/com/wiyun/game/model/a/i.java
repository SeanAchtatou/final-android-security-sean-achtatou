package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class i {
    private String a;
    private long b;
    private String c;
    private String d;
    private String e;
    private String f;

    public static i a(JSONObject dict) {
        i s = new i();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        s.a(id);
        s.c(dict.optString("name", ""));
        s.b(dict.optString("description"));
        s.a(dict.optLong("save_time"));
        s.d(dict.optString("image"));
        s.e(dict.optString("blob"));
        return s;
    }

    public String a() {
        return this.a;
    }

    public void a(long time) {
        this.b = time;
    }

    public void a(String id) {
        this.a = id;
    }

    public long b() {
        return this.b;
    }

    public void b(String desc) {
        this.c = desc;
    }

    public String c() {
        return this.c;
    }

    public void c(String name) {
        this.d = name;
    }

    public String d() {
        return this.d;
    }

    public void d(String imageUrl) {
        this.e = imageUrl;
    }

    public String e() {
        return this.e;
    }

    public void e(String blobUrl) {
        this.f = blobUrl;
    }

    public String f() {
        return this.f;
    }
}
