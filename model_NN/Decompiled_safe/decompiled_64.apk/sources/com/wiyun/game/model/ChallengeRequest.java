package com.wiyun.game.model;

public class ChallengeRequest {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private String f;
    private int g;
    private byte[] h;

    public int getBid() {
        return this.g;
    }

    public byte[] getBlob() {
        return this.h;
    }

    public String getChallengeDefinitionId() {
        return this.a;
    }

    public String getCtuId() {
        return this.b;
    }

    public String getFromUserId() {
        return this.d;
    }

    public String getFromUsername() {
        return this.c;
    }

    public String getPortraitUrl() {
        return this.f;
    }

    public int getScore() {
        return this.e;
    }

    public void setBid(int bid) {
        this.g = bid;
    }

    public void setBlob(byte[] blob) {
        this.h = blob;
    }

    public void setChallengeDefinitionId(String challengeDefinitionId) {
        this.a = challengeDefinitionId;
    }

    public void setCtuId(String ctuId) {
        this.b = ctuId;
    }

    public void setFromUserId(String fromUserId) {
        this.d = fromUserId;
    }

    public void setFromUsername(String fromUsername) {
        this.c = fromUsername;
    }

    public void setPortraitUrl(String portraitUrl) {
        this.f = portraitUrl;
    }

    public void setScore(int score) {
        this.e = score;
    }
}
