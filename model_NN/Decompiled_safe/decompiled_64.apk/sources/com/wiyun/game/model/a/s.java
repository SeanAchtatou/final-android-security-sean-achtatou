package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class s {
    private String a;
    private String b;
    private boolean c;
    private int d;
    private String e;
    private long f;
    private int g;

    public static s a(JSONObject dict) {
        s u = new s();
        String id = dict.optString("user_id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        u.a(id);
        u.b(dict.optString("username"));
        u.a(dict.optInt("point"));
        u.c(dict.optString("avatar"));
        u.b(dict.optInt("status"));
        u.a(dict.optLong("complete_time"));
        u.a("F".equalsIgnoreCase(dict.optString("gender")));
        return u;
    }

    public void a(int point) {
        this.d = point;
    }

    public void a(long time) {
        this.f = time;
    }

    public void a(String id) {
        this.a = id;
    }

    public void a(boolean female) {
        this.c = female;
    }

    public void b(int state) {
        this.g = state;
    }

    public void b(String name) {
        this.b = name;
    }

    public void c(String avatarUrl) {
        this.e = avatarUrl;
    }
}
