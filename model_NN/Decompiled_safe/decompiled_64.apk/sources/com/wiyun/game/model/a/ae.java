package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class ae {
    private long a;
    private String b;
    private String c;
    private boolean d;
    private String e;
    private int f;
    private String g;

    public static ae a(JSONObject dict) {
        ae t = new ae();
        String userId = dict.optString("user_id");
        if (TextUtils.isEmpty(userId)) {
            return null;
        }
        t.a(userId);
        t.c(dict.optString("avatar"));
        t.b(dict.optString("username"));
        t.a(dict.optInt("unread_message_count"));
        t.a(dict.optLong("last_sent_time"));
        t.d(dict.optString("last_message"));
        t.a("F".equalsIgnoreCase(dict.optString("gender")));
        return t;
    }

    public void a(int unreadMessageCount) {
        this.f = unreadMessageCount;
    }

    public void a(long lastSentTime) {
        this.a = lastSentTime;
    }

    public void a(String userId) {
        this.b = userId;
    }

    public void a(boolean female) {
        this.d = female;
    }

    public void b(String username) {
        this.c = username;
    }

    public void c(String avatarUrl) {
        this.e = avatarUrl;
    }

    public void d(String lastMessage) {
        this.g = lastMessage;
    }
}
