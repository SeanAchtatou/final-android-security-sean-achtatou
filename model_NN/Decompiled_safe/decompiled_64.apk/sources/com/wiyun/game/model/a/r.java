package com.wiyun.game.model.a;

import android.text.TextUtils;
import org.json.JSONObject;

public class r {
    private String a;
    private String b;
    private String c;
    private String d;
    private int e;
    private int f;
    private int g;
    private String h;

    public static r a(JSONObject dict) {
        r d2 = new r();
        String id = dict.optString("definition_id");
        String appId = dict.optString("app_id");
        if (TextUtils.isEmpty(id) || TextUtils.isEmpty("app_id")) {
            return null;
        }
        d2.a(id);
        d2.e(appId);
        d2.b(dict.optString("definition_name"));
        d2.c(dict.optString("definition_icon"));
        return d2;
    }

    public static r b(JSONObject dict) {
        r d2 = new r();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        d2.a(id);
        d2.b(dict.optString("name"));
        d2.c(dict.optString("icon"));
        return d2;
    }

    public static r c(JSONObject dict) {
        r d2 = new r();
        String appId = dict.optString("app_id");
        if (TextUtils.isEmpty("app_id")) {
            return null;
        }
        d2.e(appId);
        d2.b(dict.optString("name"));
        d2.c(dict.optString("icon"));
        return d2;
    }

    public static r d(JSONObject dict) {
        return c(dict);
    }

    public static r e(JSONObject dict) {
        return a(dict);
    }

    public static r f(JSONObject dict) {
        r d2 = new r();
        String id = dict.optString("id");
        if (TextUtils.isEmpty(id)) {
            return null;
        }
        d2.a(id);
        d2.b(dict.optString("name"));
        d2.c(dict.optString("icon"));
        d2.d(dict.optString("description"));
        d2.b(dict.optInt("min_point"));
        d2.c(dict.optInt("max_point"));
        d2.a(dict.optInt("challenge_type"));
        return d2;
    }

    public String a() {
        return this.a;
    }

    public void a(int type) {
        this.e = type;
    }

    public void a(String id) {
        this.a = id;
    }

    public String b() {
        return this.b;
    }

    public void b(int minBid) {
        this.f = minBid;
    }

    public void b(String name) {
        this.b = name;
    }

    public int c() {
        return this.f;
    }

    public void c(int maxBid) {
        this.g = maxBid;
    }

    public void c(String iconUrl) {
        this.c = iconUrl;
    }

    public int d() {
        return this.g;
    }

    public void d(String desc) {
        this.d = desc;
    }

    public void e(String appId) {
        this.h = appId;
    }
}
