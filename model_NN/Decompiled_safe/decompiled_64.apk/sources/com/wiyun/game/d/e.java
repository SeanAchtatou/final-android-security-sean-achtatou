package com.wiyun.game.d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.ImageView;

public abstract class e extends ImageView {
    protected Matrix a = new Matrix();
    protected Matrix b = new Matrix();
    protected final c c = new c(null);
    int d = -1;
    int e = -1;
    float f;
    public int g;
    public int h;
    public int i;
    public int j;
    protected Handler k = new Handler();
    private final Matrix l = new Matrix();
    private final float[] m = new float[9];
    private a n;
    private Runnable o = null;

    public interface a {
        void a(Bitmap bitmap);
    }

    public e(Context context) {
        super(context);
        d();
    }

    public e(Context context, AttributeSet attrs) {
        super(context, attrs);
        d();
    }

    private void a(Bitmap bitmap, int rotation) {
        super.setImageBitmap(bitmap);
        Drawable d2 = getDrawable();
        if (d2 != null) {
            d2.setDither(true);
        }
        Bitmap old = this.c.b();
        this.c.a(bitmap);
        this.c.a(rotation);
        if (old != null && old != bitmap && this.n != null) {
            this.n.a(old);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private void a(c bitmap, Matrix matrix) {
        float viewWidth = (float) getWidth();
        float viewHeight = (float) getHeight();
        float w = (float) bitmap.f();
        float h2 = (float) bitmap.e();
        matrix.reset();
        float scale = Math.min(Math.min(viewWidth / w, 2.0f), Math.min(viewHeight / h2, 2.0f));
        matrix.postConcat(bitmap.c());
        matrix.postScale(scale, scale);
        matrix.postTranslate((viewWidth - (w * scale)) / 2.0f, (viewHeight - (h2 * scale)) / 2.0f);
    }

    private void d() {
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public float a() {
        return a(this.b);
    }

    /* access modifiers changed from: protected */
    public float a(Matrix matrix) {
        return a(matrix, 0);
    }

    /* access modifiers changed from: protected */
    public float a(Matrix matrix, int whichValue) {
        matrix.getValues(this.m);
        return this.m[whichValue];
    }

    /* access modifiers changed from: protected */
    public void a(float scale) {
        a(scale, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public void a(float dx, float dy) {
        this.b.postTranslate(dx, dy);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.d.e.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.wiyun.game.d.e.a(android.graphics.Bitmap, int):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, android.graphics.Matrix):void
      com.wiyun.game.d.e.a(android.graphics.Matrix, int):float
      com.wiyun.game.d.e.a(float, float):void
      com.wiyun.game.d.e.a(android.graphics.Bitmap, boolean):void
      com.wiyun.game.d.e.a(com.wiyun.game.d.c, boolean):void
      com.wiyun.game.d.e.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void a(float scale, float centerX, float centerY) {
        if (scale > this.f) {
            scale = this.f;
        }
        float deltaScale = scale / a();
        this.b.postScale(deltaScale, deltaScale, centerX, centerY);
        setImageMatrix(b());
        a(true, true);
    }

    /* access modifiers changed from: protected */
    public void a(float scale, float centerX, float centerY, float durationMs) {
        final float incrementPerMs = (scale - a()) / durationMs;
        final float oldScale = a();
        final long startTime = System.currentTimeMillis();
        final float f2 = durationMs;
        final float f3 = centerX;
        final float f4 = centerY;
        this.k.post(new Runnable() {
            public void run() {
                float currentMs = Math.min(f2, (float) (System.currentTimeMillis() - startTime));
                e.this.a(oldScale + (incrementPerMs * currentMs), f3, f4);
                if (currentMs < f2) {
                    e.this.k.post(this);
                }
            }
        });
    }

    public void a(Bitmap bitmap, boolean resetSupp) {
        a(new c(bitmap), resetSupp);
    }

    public void a(final c bitmap, final boolean resetSupp) {
        if (getWidth() <= 0) {
            this.o = new Runnable() {
                public void run() {
                    e.this.a(bitmap, resetSupp);
                }
            };
            return;
        }
        if (bitmap.b() != null) {
            a(bitmap, this.a);
            a(bitmap.b(), bitmap.a());
        } else {
            this.a.reset();
            setImageBitmap(null);
        }
        if (resetSupp) {
            this.b.reset();
        }
        setImageMatrix(b());
        this.f = c();
    }

    public void a(boolean horizontal, boolean vertical) {
        if (this.c.b() != null) {
            Matrix m2 = b();
            RectF rect = new RectF(0.0f, 0.0f, (float) this.c.b().getWidth(), (float) this.c.b().getHeight());
            m2.mapRect(rect);
            float height = rect.height();
            float width = rect.width();
            float deltaX = 0.0f;
            float deltaY = 0.0f;
            if (vertical) {
                int viewHeight = getHeight();
                if (height < ((float) viewHeight)) {
                    deltaY = ((((float) viewHeight) - height) / 2.0f) - rect.top;
                } else if (rect.top > 0.0f) {
                    deltaY = -rect.top;
                } else if (rect.bottom < ((float) viewHeight)) {
                    deltaY = ((float) getHeight()) - rect.bottom;
                }
            }
            if (horizontal) {
                int viewWidth = getWidth();
                if (width < ((float) viewWidth)) {
                    deltaX = ((((float) viewWidth) - width) / 2.0f) - rect.left;
                } else if (rect.left > 0.0f) {
                    deltaX = -rect.left;
                } else if (rect.right < ((float) viewWidth)) {
                    deltaX = ((float) viewWidth) - rect.right;
                }
            }
            a(deltaX, deltaY);
            setImageMatrix(b());
        }
    }

    /* access modifiers changed from: protected */
    public Matrix b() {
        this.l.set(this.a);
        this.l.postConcat(this.b);
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void b(float dx, float dy) {
        a(dx, dy);
        setImageMatrix(b());
    }

    /* access modifiers changed from: protected */
    public float c() {
        if (this.c.b() == null) {
            return 1.0f;
        }
        return Math.max(((float) this.c.f()) / ((float) this.d), ((float) this.c.e()) / ((float) this.e)) * 4.0f;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || a() <= 1.0f) {
            return super.onKeyDown(keyCode, event);
        }
        a(1.0f);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.g = left;
        this.h = right;
        this.i = top;
        this.j = bottom;
        this.d = right - left;
        this.e = bottom - top;
        Runnable r = this.o;
        if (r != null) {
            this.o = null;
            r.run();
        }
        if (this.c.b() != null) {
            a(this.c, this.a);
            setImageMatrix(b());
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        a(bitmap, 0);
    }
}
