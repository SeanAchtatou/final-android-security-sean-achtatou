package com.wiyun.game.a;

import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;

public class a extends SSLSocketFactory {
    private javax.net.ssl.SSLSocketFactory a;

    public a() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        super(null);
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[]{new b()}, null);
            this.a = sslcontext.getSocketFactory();
            setHostnameVerifier(new AllowAllHostnameVerifier());
        } catch (Exception e) {
        }
    }

    public Socket createSocket() throws IOException {
        return this.a.createSocket();
    }

    public Socket createSocket(Socket socket, String s, int i, boolean flag) throws IOException {
        return this.a.createSocket(socket, s, i, flag);
    }
}
