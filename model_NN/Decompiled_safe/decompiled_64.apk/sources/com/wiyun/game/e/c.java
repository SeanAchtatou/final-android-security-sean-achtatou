package com.wiyun.game.e;

public class c {
    static final byte[] a = {13, 10};
    private static final byte[] b = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] c = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] d;
    private final byte[] e;
    private final int f;
    private final byte[] g;
    private final int h;
    private final int i;
    private byte[] j;
    private int k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private int p;

    static {
        byte[] bArr = new byte[123];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = 62;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = 63;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        d = bArr;
    }

    public c() {
        this(false);
    }

    public c(int lineLength, byte[] lineSeparator, boolean urlSafe) {
        if (lineSeparator == null) {
            lineLength = 0;
            lineSeparator = a;
        }
        this.f = lineLength > 0 ? (lineLength / 4) * 4 : 0;
        this.g = new byte[lineSeparator.length];
        System.arraycopy(lineSeparator, 0, this.g, 0, lineSeparator.length);
        if (lineLength > 0) {
            this.i = lineSeparator.length + 4;
        } else {
            this.i = 4;
        }
        this.h = this.i - 1;
        if (f(lineSeparator)) {
            throw new IllegalArgumentException("lineSeperator must not contain base64 characters: [" + b.a(lineSeparator) + "]");
        }
        this.e = urlSafe ? c : b;
    }

    public c(boolean urlSafe) {
        this(76, a, urlSafe);
    }

    private static long a(byte[] pArray, int chunkSize, byte[] chunkSeparator) {
        int chunkSize2 = (chunkSize / 4) * 4;
        long len = (long) ((pArray.length * 4) / 3);
        long mod = len % 4;
        if (mod != 0) {
            len += 4 - mod;
        }
        if (chunkSize2 <= 0) {
            return len;
        }
        boolean lenChunksPerfectly = len % ((long) chunkSize2) == 0;
        long len2 = len + ((len / ((long) chunkSize2)) * ((long) chunkSeparator.length));
        return !lenChunksPerfectly ? len2 + ((long) chunkSeparator.length) : len2;
    }

    public static boolean a(byte octet) {
        return octet == 61 || (octet >= 0 && octet < d.length && d[octet] != -1);
    }

    public static byte[] a(byte[] binaryData) {
        return a(binaryData, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.game.e.c.a(byte[], boolean, boolean):byte[]
     arg types: [byte[], boolean, int]
     candidates:
      com.wiyun.game.e.c.a(byte[], int, byte[]):long
      com.wiyun.game.e.c.a(byte[], int, int):int
      com.wiyun.game.e.c.a(byte[], boolean, boolean):byte[] */
    public static byte[] a(byte[] binaryData, boolean isChunked) {
        return a(binaryData, isChunked, false);
    }

    public static byte[] a(byte[] binaryData, boolean isChunked, boolean urlSafe) {
        return a(binaryData, isChunked, urlSafe, Integer.MAX_VALUE);
    }

    public static byte[] a(byte[] binaryData, boolean isChunked, boolean urlSafe, int maxResultSize) {
        if (binaryData == null || binaryData.length == 0) {
            return binaryData;
        }
        long len = a(binaryData, 76, a);
        if (len > ((long) maxResultSize)) {
            throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + len + ") than the specified maxium size of " + maxResultSize);
        }
        return (isChunked ? new c(urlSafe) : new c(0, a, urlSafe)).e(binaryData);
    }

    public static String b(byte[] binaryData) {
        return b.a(a(binaryData, true));
    }

    private void c() {
        if (this.j == null) {
            this.j = new byte[8192];
            this.k = 0;
            this.l = 0;
            return;
        }
        byte[] b2 = new byte[(this.j.length * 2)];
        System.arraycopy(this.j, 0, b2, 0, this.j.length);
        this.j = b2;
    }

    private void d() {
        this.j = null;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = false;
    }

    public static byte[] d(byte[] base64Data) {
        return new c().c(base64Data);
    }

    private static boolean f(byte[] arrayOctet) {
        for (byte a2 : arrayOctet) {
            if (a(a2)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int a(byte[] b2, int bPos, int bAvail) {
        if (this.j == null) {
            return this.o ? -1 : 0;
        }
        int len = Math.min(b(), bAvail);
        if (this.j != b2) {
            System.arraycopy(this.j, this.l, b2, bPos, len);
            this.l += len;
            if (this.l >= this.k) {
                this.j = null;
            }
        } else {
            this.j = null;
        }
        return len;
    }

    public boolean a() {
        return this.e == c;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        if (this.j != null) {
            return this.k - this.l;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void b(byte[] out, int outPos, int outAvail) {
        if (out != null && out.length == outAvail) {
            this.j = out;
            this.k = outPos;
            this.l = outPos;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(byte[] r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 61
            r7 = 0
            boolean r3 = r9.o
            if (r3 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r12 >= 0) goto L_0x00d1
            r3 = 1
            r9.o = r3
            byte[] r3 = r9.j
            if (r3 == 0) goto L_0x001b
            byte[] r3 = r9.j
            int r3 = r3.length
            int r4 = r9.k
            int r3 = r3 - r4
            int r4 = r9.i
            if (r3 >= r4) goto L_0x001e
        L_0x001b:
            r9.c()
        L_0x001e:
            int r3 = r9.n
            switch(r3) {
                case 1: goto L_0x0040;
                case 2: goto L_0x0083;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r3 = r9.f
            if (r3 <= 0) goto L_0x0007
            int r3 = r9.k
            if (r3 <= 0) goto L_0x0007
            byte[] r3 = r9.g
            byte[] r4 = r9.j
            int r5 = r9.k
            byte[] r6 = r9.g
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.k
            byte[] r4 = r9.g
            int r4 = r4.length
            int r3 = r3 + r4
            r9.k = r3
            goto L_0x0007
        L_0x0040:
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 << 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.e
            byte[] r4 = com.wiyun.game.e.c.b
            if (r3 != r4) goto L_0x0023
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            r3[r4] = r8
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x0083:
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 10
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 << 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.e
            byte[] r4 = com.wiyun.game.e.c.b
            if (r3 != r4) goto L_0x0023
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x00d1:
            r1 = 0
            r2 = r11
        L_0x00d3:
            if (r1 < r12) goto L_0x00d8
            r11 = r2
            goto L_0x0007
        L_0x00d8:
            byte[] r3 = r9.j
            if (r3 == 0) goto L_0x00e6
            byte[] r3 = r9.j
            int r3 = r3.length
            int r4 = r9.k
            int r3 = r3 - r4
            int r4 = r9.i
            if (r3 >= r4) goto L_0x00e9
        L_0x00e6:
            r9.c()
        L_0x00e9:
            int r3 = r9.n
            int r3 = r3 + 1
            r9.n = r3
            int r3 = r3 % 3
            r9.n = r3
            int r11 = r2 + 1
            byte r0 = r10[r2]
            if (r0 >= 0) goto L_0x00fb
            int r0 = r0 + 256
        L_0x00fb:
            int r3 = r9.p
            int r3 = r3 << 8
            int r3 = r3 + r0
            r9.p = r3
            int r3 = r9.n
            if (r3 != 0) goto L_0x017a
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 18
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 12
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            int r6 = r6 >> 6
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.j
            int r4 = r9.k
            int r5 = r4 + 1
            r9.k = r5
            byte[] r5 = r9.e
            int r6 = r9.p
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            int r3 = r9.m
            int r3 = r3 + 4
            r9.m = r3
            int r3 = r9.f
            if (r3 <= 0) goto L_0x017a
            int r3 = r9.f
            int r4 = r9.m
            if (r3 > r4) goto L_0x017a
            byte[] r3 = r9.g
            byte[] r4 = r9.j
            int r5 = r9.k
            byte[] r6 = r9.g
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.k
            byte[] r4 = r9.g
            int r4 = r4.length
            int r3 = r3 + r4
            r9.k = r3
            r9.m = r7
        L_0x017a:
            int r1 = r1 + 1
            r2 = r11
            goto L_0x00d3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.game.e.c.c(byte[], int, int):void");
    }

    public byte[] c(byte[] pArray) {
        d();
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        byte[] buf = new byte[((int) ((long) ((pArray.length * 3) / 4)))];
        b(buf, 0, buf.length);
        d(pArray, 0, pArray.length);
        d(pArray, 0, -1);
        byte[] result = new byte[this.k];
        a(result, 0, result.length);
        return result;
    }

    /* access modifiers changed from: package-private */
    public void d(byte[] in, int inPos, int inAvail) {
        byte b2;
        if (!this.o) {
            if (inAvail < 0) {
                this.o = true;
            }
            int i2 = 0;
            int inPos2 = inPos;
            while (true) {
                if (i2 >= inAvail) {
                    break;
                }
                if (this.j == null || this.j.length - this.k < this.h) {
                    c();
                }
                int inPos3 = inPos2 + 1;
                byte b3 = in[inPos2];
                if (b3 == 61) {
                    this.o = true;
                    break;
                }
                if (b3 >= 0 && b3 < d.length && (b2 = d[b3]) >= 0) {
                    int i3 = this.n + 1;
                    this.n = i3;
                    this.n = i3 % 4;
                    this.p = (this.p << 6) + b2;
                    if (this.n == 0) {
                        byte[] bArr = this.j;
                        int i4 = this.k;
                        this.k = i4 + 1;
                        bArr[i4] = (byte) ((this.p >> 16) & 255);
                        byte[] bArr2 = this.j;
                        int i5 = this.k;
                        this.k = i5 + 1;
                        bArr2[i5] = (byte) ((this.p >> 8) & 255);
                        byte[] bArr3 = this.j;
                        int i6 = this.k;
                        this.k = i6 + 1;
                        bArr3[i6] = (byte) (this.p & 255);
                    }
                }
                i2++;
                inPos2 = inPos3;
            }
            if (this.o && this.n != 0) {
                this.p <<= 6;
                switch (this.n) {
                    case 2:
                        this.p <<= 6;
                        byte[] bArr4 = this.j;
                        int i7 = this.k;
                        this.k = i7 + 1;
                        bArr4[i7] = (byte) ((this.p >> 16) & 255);
                        return;
                    case 3:
                        byte[] bArr5 = this.j;
                        int i8 = this.k;
                        this.k = i8 + 1;
                        bArr5[i8] = (byte) ((this.p >> 16) & 255);
                        byte[] bArr6 = this.j;
                        int i9 = this.k;
                        this.k = i9 + 1;
                        bArr6[i9] = (byte) ((this.p >> 8) & 255);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public byte[] e(byte[] pArray) {
        d();
        if (pArray == null || pArray.length == 0) {
            return pArray;
        }
        byte[] buf = new byte[((int) a(pArray, this.f, this.g))];
        b(buf, 0, buf.length);
        c(pArray, 0, pArray.length);
        c(pArray, 0, -1);
        if (this.j != buf) {
            a(buf, 0, buf.length);
        }
        if (a() && this.k < buf.length) {
            byte[] smallerBuf = new byte[this.k];
            System.arraycopy(buf, 0, smallerBuf, 0, this.k);
            buf = smallerBuf;
        }
        return buf;
    }
}
