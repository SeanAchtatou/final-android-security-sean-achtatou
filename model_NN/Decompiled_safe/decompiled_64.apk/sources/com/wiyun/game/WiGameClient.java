package com.wiyun.game;

import com.wiyun.game.model.ChallengeRequest;
import com.wiyun.game.model.ChallengeResult;
import com.wiyun.game.model.User;
import java.util.ArrayList;

public class WiGameClient {
    public void wyChallengeResultSubmitted(ChallengeResult result) {
    }

    public void wyFriendListGot(long callId, String userId, ArrayList<User> friends, int start) {
    }

    public void wyFriendRequestSent(String friendId) {
    }

    public void wyGameSaveFailed(String name) {
    }

    public void wyGameSaveProgress(String name, int uploadedSize) {
    }

    public void wyGameSaveStart(String name, int totalSize) {
    }

    public void wyGameSaved(String name) {
    }

    public void wyGetFriendListFailed(long callId, String userId, int start) {
    }

    public void wyGetUserInfoFailed(long callId, String userId) {
    }

    public void wyLoadGame(String blobPath) {
    }

    public void wyLogInFailed() {
    }

    public void wyLoggedIn(String sessionKey) {
    }

    public void wyPlayChallenge(ChallengeRequest request) {
    }

    public void wyPortraitGot(String userId) {
    }

    public void wyScoreSubmitted(String leaderboardId, int score, int rank) {
    }

    public void wySendFriendRequestFailed(String userId) {
    }

    public void wyUserInfoGot(long callId, User user) {
    }
}
