package com.wiyun.game;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

class b {

    static final class a implements d {
        public static final String[] a = {"_id", "uid", "iid", "n", "d", "c", "ct", "p", "sig", "uc", "a", "iu", "did"};

        a() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE i (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,iid TEXT,n TEXT,a TEXT,d TEXT,c INTEGER,ct INTEGER,uc INTEGER,p TEXT,sig TEXT,iu TEXT,did TEXT)");
        }
    }

    /* renamed from: com.wiyun.game.b$b  reason: collision with other inner class name */
    static final class C0002b implements i {
        public static final String[] a = {"_id", "ach_id", "done", "ut", "sig", "fc", "uid"};

        C0002b() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE ach (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,ach_id TEXT,done INTEGER,ut LONG,sig BLOB,fc INTEGER)");
        }

        public static final void b(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS ach");
        }
    }

    interface c extends BaseColumns {
    }

    interface d extends BaseColumns {
    }

    static final class e implements k {
        public static final String[] a = {"_id", "uid", "lb_id", "score"};

        e() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE hscore (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,lb_id TEXT,score INTEGER)");
        }
    }

    interface f extends BaseColumns {
    }

    static final class g implements c {
        public static final String[] a = {"_id", "lb_id", "done", "score", "ct", "sig", "_blob", "fc", "lat", "lon", "uid"};

        g() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE score (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,lb_id TEXT,done INTEGER,score INTEGER,ct LONG,sig BLOB,_blob BLOB,fc INTEGER,lat FLOAT,lon FLOAT)");
        }

        public static final void b(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS score");
        }
    }

    static final class h implements d {
        public static final String[] a = {"_id", "uid", "sig"};

        h() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE itsig (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,sig TEXT)");
        }
    }

    interface i extends BaseColumns {
    }

    static final class j implements f {
        public static final String[] a = {"_id", "ctu_id", "done", "score", "result", "sig", "_blob", "fc", "uid"};

        j() {
        }

        public static final void a(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE pcr (_id INTEGER PRIMARY KEY autoincrement,uid TEXT,ctu_id TEXT,done INTEGER,score INTEGER,result INTEGER,sig BLOB,_blob BLOB,fc INTEGER)");
        }

        public static final void b(SQLiteDatabase db) {
            db.execSQL("DROP TABLE IF EXISTS pcr");
        }
    }

    interface k extends BaseColumns {
    }

    b() {
    }
}
