package com.wiyun.ad;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import java.io.ByteArrayInputStream;
import java.util.Locale;
import java.util.ResourceBundle;

class s {
    private static String a;
    private static String b = Locale.getDefault().getCountry();

    s() {
    }

    public static Bitmap a(Resources resources, int i) {
        try {
            return BitmapFactory.decodeResource(resources, i);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static Bitmap a(byte[] bArr, int i, int i2) {
        try {
            return BitmapFactory.decodeByteArray(bArr, i, i2);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static Drawable a(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        Bitmap decodeByteArray;
        try {
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            try {
                bitmap2 = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
                try {
                    decodeByteArray = BitmapFactory.decodeByteArray(bArr3, 0, bArr3.length);
                } catch (OutOfMemoryError e) {
                    bitmap = decodeByteArray2;
                    bitmap3 = null;
                }
                try {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray2);
                    BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                    BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray);
                    StateListDrawable stateListDrawable = new StateListDrawable();
                    stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                    stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                    stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                    stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                    stateListDrawable.addState(new int[0], bitmapDrawable);
                    return stateListDrawable;
                } catch (OutOfMemoryError e2) {
                    Bitmap bitmap4 = decodeByteArray;
                    bitmap = decodeByteArray2;
                    bitmap3 = bitmap4;
                    bitmap.recycle();
                    bitmap2.recycle();
                    bitmap3.recycle();
                    return null;
                }
            } catch (OutOfMemoryError e3) {
                bitmap2 = null;
                bitmap = decodeByteArray2;
                bitmap3 = null;
                bitmap.recycle();
                bitmap2.recycle();
                bitmap3.recycle();
                return null;
            }
        } catch (OutOfMemoryError e4) {
            bitmap3 = null;
            bitmap2 = null;
            bitmap = null;
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
            if (bitmap2 != null && !bitmap2.isRecycled()) {
                bitmap2.recycle();
            }
            if (bitmap3 != null && !bitmap3.isRecycled()) {
                bitmap3.recycle();
            }
            return null;
        }
    }

    public static String a() {
        if (a == null || e()) {
            a = ResourceBundle.getBundle("com.wiyun.ad.sdk").getString("loading_ad");
        }
        return a;
    }

    public static String a(String str) {
        return ResourceBundle.getBundle("com.wiyun.ad.sdk").getString(str);
    }

    public static Drawable b() {
        try {
            return Drawable.createFromStream(new ByteArrayInputStream(n.e), "wy_dialog_bg");
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable c() {
        try {
            return Drawable.createFromStream(new ByteArrayInputStream(n.f), "wy_progress_dialog_bg");
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable d() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        try {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(n.c, 0, n.c.length);
            try {
                bitmap2 = BitmapFactory.decodeByteArray(n.b, 0, n.b.length);
                try {
                    Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(n.d, 0, n.d.length);
                    try {
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray);
                        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                        BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray2);
                        StateListDrawable stateListDrawable = new StateListDrawable();
                        stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                        stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                        stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                        stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                        stateListDrawable.addState(new int[0], bitmapDrawable);
                        return stateListDrawable;
                    } catch (OutOfMemoryError e) {
                        Bitmap bitmap4 = decodeByteArray2;
                        bitmap = decodeByteArray;
                        bitmap3 = bitmap4;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            bitmap.recycle();
                        }
                        if (bitmap2 != null && !bitmap2.isRecycled()) {
                            bitmap2.recycle();
                        }
                        if (bitmap3 != null && !bitmap3.isRecycled()) {
                            bitmap3.recycle();
                        }
                        return null;
                    }
                } catch (OutOfMemoryError e2) {
                    bitmap = decodeByteArray;
                    bitmap3 = null;
                }
            } catch (OutOfMemoryError e3) {
                bitmap2 = null;
                bitmap = decodeByteArray;
                bitmap3 = null;
            }
        } catch (OutOfMemoryError e4) {
            bitmap3 = null;
            bitmap2 = null;
            bitmap = null;
        }
    }

    private static boolean e() {
        String country = Locale.getDefault().getCountry();
        if (b.equals(country)) {
            return false;
        }
        b = country;
        return true;
    }
}
