package com.wiyun.ad;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

class q extends FrameLayout {
    public q(Context context) {
        super(context);
    }

    public q(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public q(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        int childCount = getChildCount();
        for (int i5 = 0; i5 < childCount; i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getId() == 4096) {
                childAt.layout(i, i2, i3, i4);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth() + 14, getMeasuredHeight() + 8);
    }
}
