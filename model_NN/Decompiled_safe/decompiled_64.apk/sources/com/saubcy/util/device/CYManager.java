package com.saubcy.util.device;

import android.app.Activity;
import android.os.Environment;
import android.util.DisplayMetrics;

public class CYManager {
    public static ScreenInfo getScreenSize(Activity a) {
        DisplayMetrics dm = new DisplayMetrics();
        a.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return new ScreenInfo(dm.heightPixels, dm.widthPixels);
    }

    public static boolean checkSDCard() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static class ScreenInfo {
        public int nHeight;
        public int nWidth;

        public ScreenInfo(int h, int w) {
            this.nHeight = h;
            this.nWidth = w;
        }
    }
}
