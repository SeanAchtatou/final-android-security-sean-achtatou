package com.saubcy.games.ColorfulBalls.market;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.saubcy.games.ColorfulBalls.market.ObjectFactory;
import com.saubcy.util.device.CYManager;
import com.waps.AppConnect;
import com.waps.ads.AdGroupLayout;
import com.wiyun.game.WiGame;
import com.wiyun.game.WiGameClient;
import com.wiyun.game.model.ChallengeRequest;

public class ChessBoard extends Activity {
    public static final String BALL_STYLES = "com.saubcy.games.ColorfulBalls.ChessBoard.STYLES";
    public static final String BEST_LEVEL_EASY = "com.saubcy.games.ColorfulBalls.wiyun.BEST_LEVEL_EASY";
    public static final String BEST_LEVEL_HARD = "com.saubcy.games.ColorfulBalls.wiyun.BEST_LEVEL_HARD";
    public static final String BEST_LEVEL_NORMAL = "com.saubcy.games.ColorfulBalls.wiyun.BEST_LEVEL_NORMAL";
    public static final int EASY = 0;
    public static final String GAME_LEVEL = "com.saubcy.games.ColorfulBalls.ChessBoard.LEVEL";
    public static final int HARD = 2;
    public static final String HIGHSCORES_LAST_INPUT_NAME = "com.saubcy.games.ColorfulBalls.ChessBoard.HIGHSCORES.RANKING.LASTINPUTNAME";
    public static final String HIGHSCORES_NAME = "com.saubcy.games.ColorfulBalls.ChessBoard.HIGHSCORES.RANKING.NAME";
    public static final String HIGHSCORES_RANKING = "com.saubcy.games.ColorfulBalls.ChessBoard.HIGHSCORES.RANKING";
    public static final String HIGHSCORES_SCORE = "com.saubcy.games.ColorfulBalls.ChessBoard.HIGHSCORES.RANKING.SCORE";
    public static final int NORMAL = 1;
    protected static final String appKey = "31860625125c2316";
    protected static final String[] challageIds = {"6e2995dd4e678ff4", "8784a2ab64f533e0", "0ef9b32b72a0a677"};
    protected static final String[] leaderboardIds = {"727ceba828ee20f1", "2c7c5be9b9411abc", "62b1c0fd2d1e62c9"};
    protected static final String secretKey = "b4qsgT24M6qkDLHw5NtfrNjuZZ5vC7PK";
    protected ObjectFactory.Ball[][] Board = null;
    protected SharedPreferences GameSettings = null;
    protected int[][] LevelData = {new int[]{5, 5, 3}, new int[]{7, 7, 4}, new int[]{9, 9, 5}};
    protected int UserScore = 0;
    protected int best = 0;
    protected ColorfulBalls cb = null;
    protected String mChallengeToUserId = null;
    private WiGameClient mClient = new WiGameClient() {
        public void wyLoggedIn(String sessionKey) {
        }

        public void wyLogInFailed() {
        }

        public void wyPlayChallenge(ChallengeRequest request) {
            Log.d("trace", "wyPlayChallenge()");
            ChessBoard.this.mChallengeToUserId = request.getCtuId();
            ChessBoard.this.mCompetitorScore = request.getScore();
            byte[] bLevel = request.getBlob();
            if (bLevel != null) {
                ChessBoard.this.nLevel = bLevel[1];
                ChessBoard.this.changeLevel(ChessBoard.this.nLevel);
            }
        }

        public void wyPortraitGot(String userId) {
        }

        public void wyGameSaveStart(String name, int totalSize) {
        }

        public void wyGameSaveProgress(String name, int uploadedSize) {
        }

        public void wyGameSaved(String name) {
        }

        public void wyGameSaveFailed(String name) {
        }

        public void wyLoadGame(String blobPath) {
        }
    };
    protected int mCompetitorScore = -1;
    protected int nLevel = 0;
    protected int nStyle = 0;
    protected int nWidth = 0;
    protected Vibrator vibrator = null;
    protected LinearLayout wapsView = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        getWindow().setFlags(128, 128);
        init();
        loadViews();
    }

    private void loadViews() {
        setContentView((int) R.layout.main);
        this.cb = (ColorfulBalls) findViewById(R.id.cbs);
        this.wapsView = (LinearLayout) findViewById(R.id.AdLinearLayout);
        this.wapsView.setVisibility(0);
        this.wapsView.addView(new AdGroupLayout(this));
    }

    private void init() {
        AppConnect.getInstance(this);
        WiGame.init(this, appKey, secretKey, "1.0", false, false);
        WiGame.addWiGameClient(this.mClient);
        this.nWidth = CYManager.getScreenSize(this).nWidth;
        this.GameSettings = getPreferences(0);
        this.nStyle = this.GameSettings.getInt(BALL_STYLES, 0);
        this.nLevel = this.GameSettings.getInt(GAME_LEVEL, 1);
    }

    /* access modifiers changed from: protected */
    public void recordScore(int score) {
        if (this.mCompetitorScore >= 0) {
            WiGame.submitChallengeResult(this.mChallengeToUserId, score - this.mCompetitorScore, score, null);
            this.mCompetitorScore = -1;
        }
        unlockAchievement(score);
        if (score > this.best) {
            SharedPreferences.Editor editor = this.GameSettings.edit();
            switch (this.nLevel) {
                case 0:
                    editor.putInt(BEST_LEVEL_EASY, score);
                    break;
                case 1:
                    editor.putInt(BEST_LEVEL_NORMAL, score);
                    break;
                case 2:
                    editor.putInt(BEST_LEVEL_HARD, score);
                    break;
            }
            editor.commit();
            this.best = score;
        }
    }

    private void unlockAchievement(int score) {
        if (score >= 100) {
            WiGame.unlockAchievement("e1ef4bde04345e42");
        }
        if (score >= 200) {
            WiGame.unlockAchievement("3a9883123695894a");
        }
        if (score >= 300) {
            WiGame.unlockAchievement("ca5d3d46e0104298");
        }
        if (score >= 400) {
            WiGame.unlockAchievement("afa6d5ca3154ddb8");
        }
        if (score >= 500) {
            WiGame.unlockAchievement("070c1b3d65b0ab31");
        }
        if (score >= 600) {
            WiGame.unlockAchievement("43f97b1b0cda5093");
        }
        if (score >= 700) {
            WiGame.unlockAchievement("c11848c3009f8c6e");
        }
        if (score >= 800) {
            WiGame.unlockAchievement("f7602f538a831a24");
        }
        if (score >= 999) {
            WiGame.unlockAchievement("d34245c03c55de3c");
        }
    }

    /* access modifiers changed from: protected */
    public int getBest() {
        switch (this.nLevel) {
            case 0:
                this.best = this.GameSettings.getInt(BEST_LEVEL_EASY, 0);
                break;
            case 1:
                this.best = this.GameSettings.getInt(BEST_LEVEL_NORMAL, 0);
                break;
            case 2:
                this.best = this.GameSettings.getInt(BEST_LEVEL_HARD, 0);
                break;
        }
        return this.best;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.cb.GameLock) {
            return false;
        }
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.NewGame:
                this.cb.newGame();
                return true;
            case R.id.Style:
                showBallsStyleChooseDialog();
                return true;
            case R.id.Level:
                showLevelChooseDialog();
                return true;
            case R.id.HighScores:
                showHighScoresDialog();
                return true;
            case R.id.menu_more:
                more();
                return true;
            case R.id.Challenge:
                showChallageDialog();
                return true;
            case R.id.Rules:
                showRulesDialog();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void more() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pub:saubcyj"));
                startActivity(installIntent);
            } catch (Exception e) {
                AppConnect.getInstance(this).showOffers(this);
            }
        } catch (Exception e2) {
            AppConnect.getInstance(this).showOffers(this);
        }
    }

    private void showBallsStyleChooseDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.style_choose_title).setItems((int) R.array.BallStyleList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                ChessBoard.this.nStyle = i;
                ChessBoard.this.changeStyle(ChessBoard.this.nStyle);
            }
        }).show();
    }

    private void showLevelChooseDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.level_choose_title).setItems((int) R.array.LevelList, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                ChessBoard.this.nLevel = i;
                ChessBoard.this.changeLevel(ChessBoard.this.nLevel);
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void changeStyle(int style) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        editor.putInt(BALL_STYLES, style);
        editor.commit();
        this.cb.loadStyle();
        this.cb.RePaint();
    }

    /* access modifiers changed from: private */
    public void changeLevel(int level) {
        SharedPreferences.Editor editor = this.GameSettings.edit();
        editor.putInt(GAME_LEVEL, level);
        editor.commit();
        this.cb.setLevel(this.LevelData[this.nLevel][0], this.LevelData[this.nLevel][1], this.LevelData[this.nLevel][2]);
        this.cb.newGame();
        getBest();
    }

    private void showHighScoresDialog() {
        WiGame.openLeaderboard(leaderboardIds[this.nLevel]);
    }

    /* access modifiers changed from: protected */
    public void showGameOverDialog() {
        this.cb.newGame();
    }

    private void showChallageDialog() {
        WiGame.sendChallenge(challageIds[this.nLevel], this.best, new byte[]{(byte) this.nLevel}, leaderboardIds[this.nLevel]);
    }

    private void showRulesDialog() {
        startActivity(new Intent(this, Rules.class));
    }

    /* access modifiers changed from: protected */
    public void showNameInputDialog() {
        String LastName = this.GameSettings.getString(HIGHSCORES_LAST_INPUT_NAME, getResources().getString(R.string.DefaultName));
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setText(LastName);
        input.setSingleLine();
        alert.setView(input);
        alert.setMessage(getResources().getString(R.string.Name_Input_Text));
        alert.setPositiveButton(getResources().getString(R.string.Name_Input_btn), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String name = input.getText().toString().trim();
                SharedPreferences.Editor editor = ChessBoard.this.GameSettings.edit();
                editor.putString(ChessBoard.HIGHSCORES_LAST_INPUT_NAME, name);
                editor.commit();
                ChessBoard.this.cb.sm.addNewRecord(name, ChessBoard.this.cb.UserScore);
                ChessBoard.this.cb.newGame();
            }
        });
        alert.show();
    }

    /* access modifiers changed from: protected */
    public void vibratorBack() {
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.vibrator.vibrate(35);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.Board = this.cb.Board;
        this.UserScore = this.cb.UserScore;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cb.Board = this.Board;
        this.cb.UserScore = this.UserScore;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.vibrator != null) {
            this.vibrator.cancel();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WiGame.destroy(this);
        AppConnect.getInstance(this).finalize();
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDialog();
        return true;
    }

    /* access modifiers changed from: protected */
    public void showExitDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.exit)).setCancelable(true).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.finish();
            }
        }).setNeutralButton(getBaseContext().getResources().getString(R.string.rate), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.rateit();
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.menu_more), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ChessBoard.this.more();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void rateit() {
        try {
            Intent installIntent = new Intent("android.intent.action.VIEW");
            try {
                installIntent.setData(Uri.parse("market://search?q=pname:com.saubcy.games.ColorfulBalls.market"));
                startActivity(installIntent);
            } catch (Exception e) {
                finish();
            }
        } catch (Exception e2) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void showSubmitCheckDialog() {
        new AlertDialog.Builder(this).setMessage(getBaseContext().getResources().getString(R.string.submit_check)).setCancelable(false).setPositiveButton(getBaseContext().getResources().getString(R.string.positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WiGame.submitScore(ChessBoard.leaderboardIds[ChessBoard.this.nLevel], ChessBoard.this.UserScore, null, false);
            }
        }).setNegativeButton(getBaseContext().getResources().getString(R.string.negative), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).show();
    }
}
