package com.saubcy.games.ColorfulBalls.market;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.saubcy.games.ColorfulBalls.market.ObjectFactory;
import com.saubcy.games.ColorfulBalls.market.ShowFactory;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ColorfulBalls extends SurfaceView implements SurfaceHolder.Callback {
    protected static final int[] BallId_Crystal = {R.drawable.crystal_0, R.drawable.crystal_1, R.drawable.crystal_2, R.drawable.crystal_3, R.drawable.crystal_4, R.drawable.crystal_5, R.drawable.crystal_6};
    protected static final int[] BallId_Default = {R.drawable.ball_0, R.drawable.ball_1, R.drawable.ball_2, R.drawable.ball_3, R.drawable.ball_4, R.drawable.ball_5, R.drawable.ball_6};
    protected static final int[] BallId_Halloween = {R.drawable.halloween_0, R.drawable.halloween_1, R.drawable.halloween_2, R.drawable.halloween_3, R.drawable.halloween_4, R.drawable.halloween_5, R.drawable.halloween_6};
    protected static final int[] BallId_Sports = {R.drawable.sports_0, R.drawable.sports_1, R.drawable.sports_2, R.drawable.sports_3, R.drawable.sports_4, R.drawable.sports_5, R.drawable.sports_6};
    public static final int BaseWidth = 320;
    public static final float BlankWidth = 2.0f;
    public static final int COL = 1;
    public static final int ColNum = 9;
    public static final int[][] Grid_Ids = {new int[]{11, 12, 13, 14, 15, 16, 17, 18, 19}, new int[]{21, 22, 23, 24, 25, 26, 27, 28, 29}, new int[]{31, 32, 33, 34, 35, 36, 37, 38, 39}, new int[]{41, 42, 43, 44, 45, 46, 47, 48, 49}, new int[]{51, 52, 53, 54, 55, 56, 57, 58, 59}, new int[]{61, 62, 63, 64, 65, 66, 67, 68, 69}, new int[]{71, 72, 73, 74, 75, 76, 77, 78, 79}, new int[]{81, 82, 83, 84, 85, 86, 87, 88, 89}, new int[]{91, 92, 93, 94, 95, 96, 97, 98, 99}};
    protected static final int InitBallNum = 5;
    protected static final int NextBallNum = 3;
    protected static final int[] NumberId = {R.drawable.n_0, R.drawable.n_1, R.drawable.n_2, R.drawable.n_3, R.drawable.n_4, R.drawable.n_5, R.drawable.n_6, R.drawable.n_7, R.drawable.n_8, R.drawable.n_9};
    public static final int ROW = 0;
    public static final int RowNum = 9;
    protected static final int ScoreOfBall = 2;
    public static final float ScoreRate = 0.5f;
    public static final int StatusBarHeight = 35;
    protected ObjectFactory.Ball ActiveBall = null;
    protected final float BaseScale = 0.6f;
    protected final float BaseTextSize = 15.0f;
    protected ObjectFactory.Ball[][] Board = null;
    protected float BoardXOffset;
    protected float BoardYOffset;
    protected ObjectFactory.Ball ChoiceBall = null;
    protected int ChoiceGridId = -1;
    protected List<Integer> EmptyGrid = null;
    protected float FrameWidth = 1.0f;
    protected boolean GameLock = false;
    protected float GridHeight = 10.0f;
    protected float GridTipsHeight = 10.0f;
    protected float GridTipsWidth = 10.0f;
    protected float GridWidth = 10.0f;
    protected int HighScore = 0;
    protected List<ObjectFactory.Ball> KillBalls = null;
    protected int KillNeedNum = 5;
    protected Matrix MatrixActive = null;
    protected Matrix MatrixNext = null;
    protected Matrix MatrixScore = null;
    protected Matrix MatrixStill = null;
    protected Matrix MatrixTips = null;
    protected ObjectFactory.Ball[] NextBalls = null;
    protected boolean OverFlag = false;
    protected float RealBaseScale;
    protected float RealBaseTextSize;
    protected int RealWidth = BaseWidth;
    protected float ScreenScale;
    public float TipsRate = 1.5f;
    protected int UserScore = 0;
    protected ShowFactory.BallHeartBeat ab = null;
    public int colNum = 9;
    protected drawOverHandler doh = null;
    protected SurfaceHolder holder;
    protected Paint pFrameLineLeftTop = null;
    protected Paint pFrameLineRightBottom = null;
    protected Paint pScore = null;
    protected Paint pText = null;
    protected Paint pTiles = null;
    protected Paint pTipsBack = null;
    protected Paint pTipsFrame = null;
    protected Paint pTipsText = null;
    protected ChessBoard parent = null;
    protected Random random = null;
    public int rowNum = 9;
    protected Bitmap[] sawBalls = null;
    protected Bitmap sawCrown = null;
    protected Bitmap sawNext = null;
    protected Bitmap[] sawNumbers = null;
    protected Bitmap sawVS = null;
    protected ScoreManager sm = null;

    public ColorfulBalls(Context context) {
        super(context);
        this.parent = (ChessBoard) context;
        init();
        setLevel(this.parent.LevelData[this.parent.nLevel][0], this.parent.LevelData[this.parent.nLevel][1], this.parent.LevelData[this.parent.nLevel][2]);
    }

    public ColorfulBalls(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.parent = (ChessBoard) context;
        init();
        setLevel(this.parent.LevelData[this.parent.nLevel][0], this.parent.LevelData[this.parent.nLevel][1], this.parent.LevelData[this.parent.nLevel][2]);
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        if (this.parent.Board == null) {
            newGame();
        } else {
            RePaint();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouchEvent(MotionEvent event) {
        if (this.GameLock) {
            return false;
        }
        ObjectFactory.Ball tmpBall = this.ChoiceBall;
        switch (event.getAction()) {
            case 0:
                this.ChoiceBall = getBallByCoordinate(event.getX(), event.getY(), false);
                this.ChoiceGridId = getGridIdByCoordinate(event.getX(), event.getY());
                if (!(this.ChoiceBall == null || this.ChoiceBall == tmpBall)) {
                    this.parent.vibratorBack();
                }
                RePaint();
                return true;
            case 1:
                this.ChoiceBall = null;
                this.ChoiceGridId = -1;
                if (this.ActiveBall == null) {
                    this.ActiveBall = getBallByCoordinate(event.getX(), event.getY(), false);
                    if (this.ActiveBall != null) {
                        this.ab = new ShowFactory.BallHeartBeat(this);
                        this.ab.start();
                    }
                } else {
                    ObjectFactory.Ball tmpBall2 = getBallByCoordinate(event.getX(), event.getY(), false);
                    if (tmpBall2 != null) {
                        this.ActiveBall = null;
                    } else if (tmpBall2 == null) {
                        this.ActiveBall = getBallByCoordinate(event.getX(), event.getY(), true);
                    }
                }
                RePaint();
                break;
            case 2:
                this.ChoiceBall = getBallByCoordinate(event.getX(), event.getY(), false);
                this.ChoiceGridId = getGridIdByCoordinate(event.getX(), event.getY());
                if (!(this.ChoiceBall == null || this.ChoiceBall == tmpBall)) {
                    this.parent.vibratorBack();
                }
                RePaint();
                return true;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void RePaint() {
        this.parent.Board = null;
        new ShowFactory.RePaintChessBoard(this).start();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int wMeasureSpec, int hMeasureSpec) {
        setMeasuredDimension(this.parent.nWidth, (int) (this.BoardYOffset + (this.GridHeight * ((float) this.colNum))));
    }

    private int getGridIdByCoordinate(float x, float y) {
        if (x < this.BoardXOffset || x > (this.GridWidth * ((float) this.rowNum)) + this.BoardXOffset || y < this.BoardYOffset || y > (this.GridHeight * ((float) this.colNum)) + this.BoardYOffset) {
            return -1;
        }
        int row = (int) ((y - this.BoardYOffset) / this.GridHeight);
        try {
            return Grid_Ids[row][(int) ((x - this.BoardXOffset) / this.GridWidth)];
        } catch (ArrayIndexOutOfBoundsException e) {
            return -1;
        }
    }

    /* access modifiers changed from: protected */
    public ObjectFactory.Position getPositionByGridId(int GridId) {
        if (GridId < 0) {
            return null;
        }
        ObjectFactory.Position pos = null;
        for (int i = 0; i < this.rowNum; i++) {
            for (int j = 0; j < this.colNum; j++) {
                if (Grid_Ids[i][j] == GridId) {
                    pos = new ObjectFactory.Position(i, j);
                }
            }
        }
        return pos;
    }

    private ObjectFactory.Ball getBallByCoordinate(float x, float y, boolean isMove) {
        if (x < this.BoardXOffset || x > (this.GridWidth * ((float) this.rowNum)) + this.BoardXOffset || y < this.BoardYOffset || y > (this.GridHeight * ((float) this.colNum)) + this.BoardYOffset) {
            return null;
        }
        int row = (int) ((y - this.BoardYOffset) / this.GridHeight);
        int col = (int) ((x - this.BoardXOffset) / this.GridWidth);
        try {
            ObjectFactory.Ball b = this.Board[row][col];
            if (this.ActiveBall == null || !isMove || this.Board[row][col] != null) {
                return b;
            }
            List<ObjectFactory.Position> routing = getRouting(this.ActiveBall.pos, new ObjectFactory.Position(row, col));
            if (routing != null) {
                int startIndex = transPosition2Index(this.ActiveBall.pos);
                int targetIndex = transPosition2Index(new ObjectFactory.Position(row, col));
                int i = 0;
                while (true) {
                    if (i >= this.EmptyGrid.size()) {
                        break;
                    } else if (this.EmptyGrid.get(i).intValue() == targetIndex) {
                        this.EmptyGrid.remove(i);
                        break;
                    } else {
                        i++;
                    }
                }
                this.EmptyGrid.add(Integer.valueOf(startIndex));
                new ShowFactory.BallMove(this, routing, this.ActiveBall).start();
            }
            return null;
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        } catch (NullPointerException e2) {
            return null;
        } catch (Exception e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.pTiles = new Paint();
        this.pFrameLineLeftTop = new Paint();
        this.pFrameLineRightBottom = new Paint();
        this.pText = new Paint();
        this.pTipsText = new Paint();
        this.pScore = new Paint();
        this.pTipsBack = new Paint();
        this.pTipsFrame = new Paint();
        this.pTiles.setColor(getResources().getColor(R.color.Tile));
        this.pFrameLineLeftTop.setColor(getResources().getColor(R.color.FrameLineLeftTop));
        this.pFrameLineRightBottom.setColor(getResources().getColor(R.color.FrameLineRightBottom));
        this.pText.setColor(getResources().getColor(R.color.Text));
        this.pText.setTextAlign(Paint.Align.CENTER);
        this.pTipsText.setColor(getResources().getColor(R.color.TipsText));
        this.pTipsText.setTextAlign(Paint.Align.CENTER);
        this.pScore.setColor(getResources().getColor(R.color.FailScore));
        this.pTipsBack.setColor(getResources().getColor(R.color.TipsBack));
        this.pTipsBack.setAlpha(144);
        this.pTipsBack.setAntiAlias(true);
        this.pTipsFrame.setColor(getResources().getColor(R.color.TipsFrame));
        this.pTipsFrame.setAntiAlias(true);
        this.RealWidth = this.parent.nWidth;
        loadStyle();
        this.sawNumbers = new Bitmap[NumberId.length];
        for (int i = 0; i < NumberId.length; i++) {
            this.sawNumbers[i] = ((BitmapDrawable) getResources().getDrawable(NumberId[i])).getBitmap();
        }
        this.sawNext = ((BitmapDrawable) getResources().getDrawable(R.drawable.next)).getBitmap();
        this.sawCrown = ((BitmapDrawable) getResources().getDrawable(R.drawable.crown)).getBitmap();
        this.sawVS = ((BitmapDrawable) getResources().getDrawable(R.drawable.vs)).getBitmap();
        this.ScreenScale = ((float) this.RealWidth) / 320.0f;
        this.GridTipsWidth = (((float) this.RealWidth) - 2.0f) / 9.0f;
        this.GridTipsHeight = this.GridTipsWidth;
        this.BoardXOffset = 1.0f;
        this.BoardYOffset = this.GridTipsHeight + 35.0f;
        this.random = new Random();
        this.sm = new ScoreManager(this.parent, getResources().getString(R.string.DefaultName), getResources().getString(R.string.EmptyName), Integer.parseInt(getResources().getString(R.string.DefaultScore)));
        this.doh = new drawOverHandler();
    }

    /* access modifiers changed from: protected */
    public void setLevel(int r, int c, int k) {
        this.rowNum = r;
        this.colNum = c;
        this.KillNeedNum = k;
        this.RealBaseTextSize = 135.0f / ((float) this.rowNum);
        this.RealBaseScale = 5.4f / ((float) this.rowNum);
        switch (this.rowNum) {
            case 5:
                this.TipsRate = 1.0f;
                break;
            case 7:
                this.TipsRate = 1.2f;
                break;
            case 9:
                this.TipsRate = 1.5f;
                break;
        }
        this.GridWidth = (((float) this.RealWidth) - 2.0f) / ((float) this.rowNum);
        this.GridHeight = this.GridWidth;
        this.pText.setTextSize((((float) this.RealWidth) * 15.0f) / 320.0f);
        this.pTipsText.setTextSize(((this.TipsRate * this.RealBaseTextSize) * ((float) this.RealWidth)) / 320.0f);
        this.pScore.setTextSize((((float) this.RealWidth) * 15.0f) / 320.0f);
        this.MatrixStill = new Matrix();
        this.MatrixStill.postScale(this.RealBaseScale * this.ScreenScale, this.RealBaseScale * this.ScreenScale);
        this.MatrixActive = new Matrix();
        this.MatrixActive.postScale(this.RealBaseScale * this.ScreenScale * 1.2f, this.RealBaseScale * this.ScreenScale * 0.8f);
        this.MatrixNext = new Matrix();
        this.MatrixNext.postScale((this.ScreenScale * 0.6f) / 1.5f, (this.ScreenScale * 0.6f) / 1.5f);
        this.MatrixTips = new Matrix();
        this.MatrixTips.postScale(this.RealBaseScale * this.ScreenScale * this.TipsRate, this.RealBaseScale * this.ScreenScale * this.TipsRate);
        this.MatrixScore = new Matrix();
        this.MatrixScore.postScale(this.ScreenScale * 0.6f * 0.5f, this.ScreenScale * 0.6f);
    }

    /* access modifiers changed from: protected */
    public void loadStyle() {
        int[] BallId = null;
        switch (this.parent.nStyle) {
            case 0:
                BallId = BallId_Default;
                break;
            case 1:
                BallId = BallId_Sports;
                break;
            case 2:
                BallId = BallId_Crystal;
                break;
            case 3:
                BallId = BallId_Halloween;
                break;
        }
        this.sawBalls = new Bitmap[BallId.length];
        for (int i = 0; i < BallId.length; i++) {
            this.sawBalls[i] = ((BitmapDrawable) getResources().getDrawable(BallId[i])).getBitmap();
        }
    }

    /* access modifiers changed from: protected */
    public void newGame() {
        setGame();
        new Thread(new ShowFactory.ChessBoardBuild(this)).start();
    }

    private void setGame() {
        this.EmptyGrid = new ArrayList();
        for (int i = 0; i < this.rowNum * this.colNum; i++) {
            this.EmptyGrid.add(Integer.valueOf(i));
        }
        this.KillBalls = new ArrayList();
        this.Board = (ObjectFactory.Ball[][]) Array.newInstance(ObjectFactory.Ball.class, this.rowNum, this.colNum);
        for (int i2 = 0; i2 < this.rowNum; i2++) {
            for (int j = 0; j < this.colNum; j++) {
                this.Board[i2][j] = null;
            }
        }
        this.ChoiceGridId = -1;
        this.ChoiceBall = null;
        this.ActiveBall = null;
        this.GameLock = false;
        this.pScore.setColor(getResources().getColor(R.color.FailScore));
        this.HighScore = this.parent.getBest();
        this.UserScore = 0;
        this.OverFlag = false;
    }

    /* access modifiers changed from: protected */
    public void getKillBalls(ObjectFactory.Position pos) {
        int color = this.Board[pos.row][pos.col].color;
        for (int i = 0; i < 4; i++) {
            boolean direction_1 = true;
            boolean direction_2 = true;
            int step = 1;
            List<ObjectFactory.Ball> tempList = new ArrayList<>();
            while (true) {
                if (!direction_1 && !direction_2) {
                    break;
                }
                int row = pos.row + (ObjectFactory.Node.ROWOPVALUE[i] * step);
                int col = pos.col + (ObjectFactory.Node.COLOPVALUE[i] * step);
                if (row < 0 || col < 0 || row >= this.rowNum || col >= this.colNum) {
                    direction_1 = false;
                }
                if (!direction_1 || this.Board[row][col] == null || this.Board[row][col].state >= 2 || color != this.Board[row][col].color) {
                    direction_1 = false;
                } else {
                    tempList.add(this.Board[row][col]);
                }
                int row2 = pos.row + (ObjectFactory.Node.ROWOPVALUE[i + 4] * step);
                int col2 = pos.col + (ObjectFactory.Node.COLOPVALUE[i + 4] * step);
                if (row2 < 0 || col2 < 0 || row2 >= this.rowNum || col2 >= this.colNum) {
                    direction_2 = false;
                }
                if (!direction_2 || this.Board[row2][col2] == null || this.Board[row2][col2].state >= 2 || color != this.Board[row2][col2].color) {
                    direction_2 = false;
                } else {
                    tempList.add(this.Board[row2][col2]);
                }
                step++;
            }
            if (tempList.size() >= this.KillNeedNum - 1) {
                for (int j = 0; j < tempList.size(); j++) {
                    ObjectFactory.Ball b = (ObjectFactory.Ball) tempList.get(j);
                    this.KillBalls.add(b);
                    this.Board[b.pos.row][b.pos.col].state = 2;
                }
                this.Board[pos.row][pos.col].state = 2;
                this.KillBalls.add(this.Board[pos.row][pos.col]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void removeBalls() {
        for (int i = 0; i < this.KillBalls.size(); i++) {
            ObjectFactory.Ball b = this.KillBalls.get(i);
            this.Board[b.pos.row][b.pos.col] = null;
            this.UserScore += 2;
            if (this.UserScore > this.HighScore) {
                this.pScore.setColor(getResources().getColor(R.color.WinScore));
            }
            this.EmptyGrid.add(Integer.valueOf(transPosition2Index(b.pos)));
        }
    }

    /* access modifiers changed from: protected */
    public void getNextBalls() {
        this.NextBalls = BallGenerator(3, 1);
    }

    private List<ObjectFactory.Position> getRouting(ObjectFactory.Position start, ObjectFactory.Position end) {
        boolean inOpen;
        boolean inClose;
        int newG;
        ObjectFactory.Node[][] RoutingMap = (ObjectFactory.Node[][]) Array.newInstance(ObjectFactory.Node.class, this.rowNum, this.colNum);
        List<ObjectFactory.Node> OpenList = new ArrayList<>();
        for (int i = 0; i < this.rowNum; i++) {
            for (int j = 0; j < this.colNum; j++) {
                RoutingMap[i][j] = new ObjectFactory.Node();
                if (this.Board[i][j] == null) {
                    RoutingMap[i][j].pass = true;
                } else {
                    RoutingMap[i][j].pass = false;
                }
                RoutingMap[i][j].index = transPosition2Index(new ObjectFactory.Position(i, j));
                RoutingMap[i][j].state = 0;
            }
        }
        ObjectFactory.Node node = RoutingMap[start.row][start.col];
        node.setH(getManhattan(start, end));
        node.state = 1;
        OpenList.add(node);
        while (!OpenList.isEmpty()) {
            Collections.sort(OpenList, Collections.reverseOrder());
            ObjectFactory.Node node2 = OpenList.get(OpenList.size() - 1);
            OpenList.remove(OpenList.size() - 1);
            ObjectFactory.Position pos = transIndex2Position(node2.index);
            if (end.row == pos.row && end.col == pos.col) {
                List<ObjectFactory.Position> routing = new ArrayList<>();
                ObjectFactory.Node node3 = RoutingMap[end.row][end.col];
                routing.add(transIndex2Position(node3.index));
                while (true) {
                    ObjectFactory.Position pos2 = transIndex2Position(node3.index);
                    if (pos2.row == start.row && pos2.col == start.col) {
                        return routing;
                    }
                    ObjectFactory.Position pos3 = transIndex2Position(node3.father);
                    node3 = RoutingMap[pos3.row][pos3.col];
                    routing.add(pos3);
                }
            } else {
                node2.state = 2;
                for (int i2 = 0; i2 < ObjectFactory.Node.ROWOPVALUE.length; i2 += 2) {
                    ObjectFactory.Position position = new ObjectFactory.Position(pos.row + ObjectFactory.Node.ROWOPVALUE[i2], pos.col + ObjectFactory.Node.COLOPVALUE[i2]);
                    if (position.row >= 0 && position.col >= 0 && position.row < this.rowNum && position.col < this.colNum) {
                        ObjectFactory.Node opNode = RoutingMap[position.row][position.col];
                        if (opNode.pass) {
                            if (1 == opNode.state) {
                                inOpen = true;
                                inClose = false;
                            } else if (2 == opNode.state) {
                                inOpen = false;
                                inClose = true;
                            } else {
                                inOpen = false;
                                inClose = false;
                            }
                            if (!inOpen && !inClose) {
                                opNode.father = node2.index;
                                opNode.setG(node2.G + 1);
                                opNode.setH(getManhattan(opNode.index, transPosition2Index(end)));
                                opNode.state = 1;
                                OpenList.add(opNode);
                            } else if (inOpen && (newG = node2.G + 1) < opNode.G) {
                                opNode.father = node2.index;
                                opNode.setG(newG);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public ObjectFactory.Ball[] BallGenerator(int number, int mode) {
        int total;
        if (this.EmptyGrid.size() <= number) {
            total = this.EmptyGrid.size();
        } else {
            total = number;
        }
        ObjectFactory.Ball[] balls = new ObjectFactory.Ball[number];
        for (int i = 0; i < balls.length; i++) {
            balls[i] = new ObjectFactory.Ball(-1, -1, this.random.nextInt(BallId_Default.length));
        }
        if (mode == 0) {
            for (int i2 = 0; i2 < total; i2++) {
                balls[i2].pos = getRandomPosition();
            }
        }
        return balls;
    }

    private ObjectFactory.Position getRandomPosition() {
        Collections.shuffle(this.EmptyGrid);
        int index = this.EmptyGrid.get(this.EmptyGrid.size() - 1).intValue();
        this.EmptyGrid.remove(this.EmptyGrid.size() - 1);
        return transIndex2Position(index);
    }

    private ObjectFactory.Position transIndex2Position(int index) {
        return new ObjectFactory.Position(index / this.rowNum, index % this.colNum);
    }

    private int transPosition2Index(ObjectFactory.Position p) {
        return (p.row * this.rowNum) + p.col;
    }

    private int getManhattan(ObjectFactory.Position a, ObjectFactory.Position b) {
        return Math.abs(a.row - b.row) + Math.abs(a.col - b.col);
    }

    private int getManhattan(int start, int end) {
        ObjectFactory.Position a = transIndex2Position(start);
        ObjectFactory.Position b = transIndex2Position(end);
        return Math.abs(a.row - b.row) + Math.abs(a.col - b.col);
    }

    /* access modifiers changed from: protected */
    public void addNextBalls() {
        int total;
        if (this.EmptyGrid.size() <= this.NextBalls.length) {
            total = this.EmptyGrid.size();
            this.OverFlag = true;
        } else {
            total = this.NextBalls.length;
        }
        for (int i = 0; i < total; i++) {
            this.NextBalls[i].pos = getRandomPosition();
            this.Board[this.NextBalls[i].pos.row][this.NextBalls[i].pos.col] = this.NextBalls[i];
            getKillBalls(this.NextBalls[i].pos);
        }
    }

    class drawOverHandler extends Handler {
        drawOverHandler() {
        }

        public void handleMessage(Message msg) {
            if (ColorfulBalls.this.OverFlag) {
                ColorfulBalls.this.parent.recordScore(ColorfulBalls.this.UserScore);
                ColorfulBalls.this.parent.showGameOverDialog();
                ColorfulBalls.this.parent.showSubmitCheckDialog();
            }
        }
    }
}
