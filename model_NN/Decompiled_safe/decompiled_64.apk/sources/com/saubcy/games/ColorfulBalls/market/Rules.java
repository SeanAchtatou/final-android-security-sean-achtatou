package com.saubcy.games.ColorfulBalls.market;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;

public class Rules extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.rules);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return false;
        }
        finish();
        return true;
    }
}
