package com.saubcy.games.ColorfulBalls.market;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.saubcy.games.ColorfulBalls.market.ScoreManager;
import java.util.ArrayList;
import java.util.List;

public class HighScores extends ListActivity {
    protected List<ScoreManager.ScoreStructure> ScoreList = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
        showList();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        finish();
    }

    private void getData() {
        ArrayList<Integer> ranking = getIntent().getIntegerArrayListExtra(ChessBoard.HIGHSCORES_RANKING);
        ArrayList<String> name = getIntent().getStringArrayListExtra(ChessBoard.HIGHSCORES_NAME);
        ArrayList<Integer> score = getIntent().getIntegerArrayListExtra(ChessBoard.HIGHSCORES_SCORE);
        this.ScoreList = new ArrayList();
        for (int i = 0; i < ranking.size(); i++) {
            ScoreManager.ScoreStructure ss = new ScoreManager.ScoreStructure();
            ss.ranking = ranking.get(i).intValue();
            ss.name = name.get(i);
            ss.score = score.get(i).intValue();
            this.ScoreList.add(ss);
        }
    }

    private void showList() {
        setListAdapter(new ScoreAdapter(this));
    }

    protected static class ScoreRecord {
        public TextView name;
        public TextView ranking;
        public TextView score;

        protected ScoreRecord() {
        }
    }

    protected static class ScoreAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private HighScores parent;

        public ScoreAdapter(Context context) {
            this.parent = (HighScores) context;
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return this.parent.ScoreList.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        public View getView(int position, View convertView, ViewGroup parent2) {
            ScoreRecord sr;
            if (convertView == null) {
                sr = new ScoreRecord();
                convertView = this.mInflater.inflate((int) R.layout.high_score, (ViewGroup) null);
                sr.ranking = (TextView) convertView.findViewById(R.id.ranking);
                sr.name = (TextView) convertView.findViewById(R.id.name);
                sr.score = (TextView) convertView.findViewById(R.id.score);
                convertView.setTag(sr);
            } else {
                sr = (ScoreRecord) convertView.getTag();
            }
            sr.ranking.setText(String.format("%1$4s", String.valueOf(this.parent.ScoreList.get(position).ranking) + "."));
            sr.name.setText(this.parent.ScoreList.get(position).name);
            sr.score.setText(String.valueOf(this.parent.ScoreList.get(position).score) + "  ");
            return convertView;
        }
    }
}
