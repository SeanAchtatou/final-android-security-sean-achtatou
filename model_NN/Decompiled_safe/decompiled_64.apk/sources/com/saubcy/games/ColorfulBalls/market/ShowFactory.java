package com.saubcy.games.ColorfulBalls.market;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import com.saubcy.games.ColorfulBalls.market.ObjectFactory;
import java.util.List;

public class ShowFactory {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static void drawHead(Canvas canvas, ColorfulBalls cb) {
        canvas.drawColor(cb.getResources().getColor(R.color.BackGround));
        int i = 3;
        while (true) {
            int i2 = i;
            if (i2 >= 3 + 3) {
                break;
            }
            float left = cb.BoardXOffset + (cb.GridTipsWidth * ((float) i2));
            float top = (((cb.GridTipsHeight * 0.0f) + cb.BoardYOffset) - ((cb.BoardYOffset * 1.0f) / 5.0f)) - cb.GridTipsHeight;
            float right = cb.GridTipsWidth + (cb.GridTipsWidth * ((float) i2)) + cb.BoardXOffset;
            float bottom = ((((cb.GridTipsHeight * 0.0f) + cb.BoardYOffset) + cb.GridTipsHeight) - ((cb.BoardYOffset * 1.0f) / 5.0f)) - cb.GridTipsHeight;
            canvas.drawRect(left, top, right, bottom, cb.pTiles);
            canvas.drawLine(left, top, left, bottom, cb.pFrameLineLeftTop);
            canvas.drawLine(left, top, right, top, cb.pFrameLineLeftTop);
            canvas.drawLine(right - cb.FrameWidth, top, right - cb.FrameWidth, bottom, cb.pFrameLineRightBottom);
            canvas.drawLine(left, top - cb.FrameWidth, right, top - cb.FrameWidth, cb.pFrameLineRightBottom);
            i = i2 + 1;
        }
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < cb.NextBalls.length) {
                Bitmap bitmap = Bitmap.createBitmap(cb.sawBalls[cb.NextBalls[i4].color], 0, 0, cb.sawBalls[cb.NextBalls[i4].color].getWidth(), cb.sawBalls[cb.NextBalls[i4].color].getHeight(), cb.MatrixNext, true);
                canvas.drawBitmap(bitmap, (((cb.GridTipsWidth * ((float) (i4 + 3))) + cb.BoardXOffset) + (cb.GridTipsWidth / 2.0f)) - ((float) (bitmap.getWidth() / 2)), (((((cb.GridTipsHeight * 0.0f) + cb.BoardYOffset) + (cb.GridTipsHeight / 2.0f)) - ((float) (bitmap.getHeight() / 2))) - ((cb.BoardYOffset * 1.0f) / 5.0f)) - cb.GridTipsHeight, (Paint) null);
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    protected static void drawChessBoard(Canvas canvas, ColorfulBalls cb) {
        drawHead(canvas, cb);
        drawScore(canvas, cb);
        for (int i = 0; i < cb.rowNum; i++) {
            for (int j = 0; j < cb.colNum; j++) {
                drawBlankGrid(canvas, i, j, cb);
            }
        }
    }

    private static void drawBlankGrid(Canvas canvas, int row, int col, ColorfulBalls cb) {
        float left = (cb.GridWidth * ((float) col)) + cb.BoardXOffset;
        float top = (cb.GridHeight * ((float) row)) + cb.BoardYOffset;
        float right = (cb.GridWidth * ((float) col)) + cb.BoardXOffset + cb.GridWidth;
        float bottom = (cb.GridHeight * ((float) row)) + cb.BoardYOffset + cb.GridHeight;
        canvas.drawRect(left, top, right, bottom, cb.pTiles);
        canvas.drawLine(left, top, left, bottom, cb.pFrameLineLeftTop);
        canvas.drawLine(left, top, right, top, cb.pFrameLineLeftTop);
        canvas.drawLine(right - cb.FrameWidth, top, right - cb.FrameWidth, bottom, cb.pFrameLineRightBottom);
        canvas.drawLine(left, top - cb.FrameWidth, right, top - cb.FrameWidth, cb.pFrameLineRightBottom);
        if (cb.Board[row][col] == null) {
            canvas.drawText(new StringBuilder(String.valueOf(ColorfulBalls.Grid_Ids[row][col])).toString(), (cb.GridWidth / 2.0f) + left, (cb.GridHeight / 2.0f) + top, cb.pText);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static void drawBall(Canvas canvas, int row, int col, int color, int state, ColorfulBalls cb) {
        Matrix matrix = null;
        switch (state) {
            case 0:
                matrix = cb.MatrixStill;
                break;
            case 1:
                Matrix matrix2 = cb.MatrixActive;
            case 2:
                matrix = cb.MatrixNext;
                break;
        }
        Bitmap bitmap = Bitmap.createBitmap(cb.sawBalls[color], 0, 0, cb.sawBalls[color].getWidth(), cb.sawBalls[color].getHeight(), matrix, true);
        canvas.drawBitmap(bitmap, (((((float) col) * cb.GridWidth) + cb.BoardXOffset) + (cb.GridWidth / 2.0f)) - ((float) (bitmap.getWidth() / 2)), (((((float) row) * cb.GridHeight) + cb.BoardYOffset) + (cb.GridHeight / 2.0f)) - ((float) (bitmap.getHeight() / 2)), (Paint) null);
    }

    protected static void drawBalls(Canvas canvas, ColorfulBalls cb) {
        for (int i = 0; i < cb.rowNum; i++) {
            for (int j = 0; j < cb.colNum; j++) {
                if (cb.Board[i][j] != null) {
                    drawBall(canvas, cb.Board[i][j].pos.row, cb.Board[i][j].pos.col, cb.Board[i][j].color, cb.Board[i][j].state, cb);
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r12v3 android.graphics.Bitmap: [D('bitmap' android.graphics.Bitmap), D('cb' com.saubcy.games.ColorfulBalls.market.ColorfulBalls)] */
    /* JADX INFO: Multiple debug info for r8v2 float: [D('left' float), D('top' float)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static void drawTips(Canvas canvas, ColorfulBalls cb) {
        int row;
        float bottom = 0.0f;
        float left = 0.0f;
        Bitmap bitmap = null;
        if (cb.ChoiceGridId > 0 || cb.ChoiceBall != null) {
            Bitmap bitmap2 = Bitmap.createBitmap(cb.sawBalls[0], 0, 0, cb.sawBalls[0].getWidth(), cb.sawBalls[0].getHeight(), cb.MatrixTips, true);
            ObjectFactory.Position pos = cb.getPositionByGridId(cb.ChoiceGridId);
            if (pos != null) {
                float top = (((cb.GridWidth * ((float) pos.col)) + cb.BoardXOffset) + ((cb.GridWidth * cb.TipsRate) / 2.0f)) - ((float) (bitmap2.getWidth() / 2));
                if (pos.row - 1 >= 0) {
                    row = pos.row - 1;
                } else {
                    row = 0;
                }
                float top2 = (((((((float) row) * cb.GridHeight) + cb.BoardYOffset) + ((cb.GridHeight * cb.TipsRate) / 2.0f)) - ((float) (bitmap2.getHeight() / 2))) - ((cb.BoardYOffset * 1.0f) / 5.0f)) - (cb.GridHeight * cb.TipsRate);
                float right = top + (cb.GridWidth * cb.TipsRate);
                float bottom2 = top2 + (cb.GridHeight * cb.TipsRate);
                canvas.drawRect(top - 2.0f, top2 - 2.0f, right + 2.0f, bottom2 + 2.0f, cb.pTipsFrame);
                canvas.drawRect(top, top2, right, bottom2, cb.pTipsBack);
                bitmap = bitmap2;
                bottom = top;
                left = top2;
            } else {
                return;
            }
        }
        if (cb.ChoiceBall != null) {
            canvas.drawBitmap(Bitmap.createBitmap(cb.sawBalls[cb.ChoiceBall.color], 0, 0, cb.sawBalls[cb.ChoiceBall.color].getWidth(), cb.sawBalls[cb.ChoiceBall.color].getHeight(), cb.MatrixTips, true), bottom, left, (Paint) null);
        } else if (cb.ChoiceGridId > 0) {
            canvas.drawText(new StringBuilder(String.valueOf(cb.ChoiceGridId)).toString(), ((cb.GridWidth * cb.TipsRate) / 2.0f) + bottom, ((cb.GridHeight * cb.TipsRate) / 2.0f) + left, cb.pTipsText);
        }
    }

    /* JADX INFO: Multiple debug info for r8v1 java.lang.String: [D('score' java.lang.String), D('leftOffset' float)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static void drawScore(Canvas canvas, ColorfulBalls cb) {
        float offset;
        float tipZoneSize = cb.GridTipsWidth * 3.0f;
        float unitSpace = ((cb.GridTipsWidth * 9.0f) - tipZoneSize) / 8.0f;
        float leftOffset = cb.BoardXOffset;
        float rightOffset = leftOffset + tipZoneSize + (4.0f * unitSpace);
        Bitmap bitmap = Bitmap.createBitmap(cb.sawCrown, 0, 0, cb.sawCrown.getWidth(), cb.sawCrown.getHeight(), cb.MatrixScore, true);
        if (cb.UserScore > cb.HighScore) {
            offset = leftOffset;
        } else {
            offset = (3.0f * unitSpace) + rightOffset;
        }
        float left = offset;
        float top = (((cb.GridTipsHeight * 0.0f) + cb.BoardYOffset) - ((cb.BoardYOffset * 1.0f) / 5.0f)) - cb.GridTipsHeight;
        canvas.drawBitmap(bitmap, left, top, (Paint) null);
        String score = new StringBuilder(String.valueOf(cb.UserScore)).toString();
        for (int i = score.length() - 1; i >= 0; i--) {
            int index = Integer.parseInt(new StringBuilder(String.valueOf(score.charAt(i))).toString());
            Bitmap bitmap2 = Bitmap.createBitmap(cb.sawNumbers[index], 0, 0, cb.sawNumbers[index].getWidth(), cb.sawNumbers[index].getHeight(), cb.MatrixScore, true);
            left = (((leftOffset + rightOffset) - tipZoneSize) - ((float) (bitmap2.getWidth() / 2))) - (((float) (score.length() - i)) * unitSpace);
            canvas.drawBitmap(bitmap2, left, top, (Paint) null);
        }
        String score2 = new StringBuilder(String.valueOf(cb.HighScore)).toString();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < score2.length()) {
                int index2 = Integer.parseInt(new StringBuilder(String.valueOf(score2.charAt(i3))).toString());
                left = rightOffset + (((float) i3) * unitSpace);
                canvas.drawBitmap(Bitmap.createBitmap(cb.sawNumbers[index2], 0, 0, cb.sawNumbers[index2].getWidth(), cb.sawNumbers[index2].getHeight(), cb.MatrixScore, true), left, top, (Paint) null);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public static class ChessBoardBuild implements Runnable {
        ColorfulBalls parent = null;
        SurfaceHolder surfaceHolder;

        public ChessBoardBuild(ColorfulBalls p) {
            this.parent = p;
            this.surfaceHolder = this.parent.holder;
        }

        public void run() {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    this.parent.getNextBalls();
                    ShowFactory.drawChessBoard(canvas, this.parent);
                    ObjectFactory.Ball[] initBalls = this.parent.BallGenerator(5, 0);
                    for (int i = 0; i < initBalls.length; i++) {
                        ShowFactory.drawBall(canvas, initBalls[i].pos.row, initBalls[i].pos.col, initBalls[i].color, 0, this.parent);
                        this.parent.Board[initBalls[i].pos.row][initBalls[i].pos.col] = initBalls[i];
                    }
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    protected static class BallHeartBeat extends Thread {
        private ColorfulBalls parent = null;
        private final int sleepTime = 50;
        private int state;
        SurfaceHolder surfaceHolder;

        public BallHeartBeat(ColorfulBalls p) {
            this.parent = p;
            this.surfaceHolder = this.parent.holder;
            this.state = 1;
        }

        public void run() {
            Canvas canvas = null;
            ObjectFactory.Ball b = this.parent.ActiveBall;
            b.state = this.state % 2;
            while (this.parent.ActiveBall != null && b == this.parent.ActiveBall) {
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        ShowFactory.drawChessBoard(canvas, this.parent);
                        ShowFactory.drawBalls(canvas, this.parent);
                        ShowFactory.drawTips(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    try {
                        e.printStackTrace();
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (Throwable th) {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th;
                    }
                }
                this.state++;
                b.state = this.state % 2;
                try {
                    sleep(50);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    if (b != null) {
                        b.state = 0;
                    }
                    ShowFactory.drawChessBoard(canvas, this.parent);
                    ShowFactory.drawBalls(canvas, this.parent);
                    ShowFactory.drawTips(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e3) {
                try {
                    e3.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    protected static class RePaintChessBoard extends Thread {
        private ColorfulBalls parent = null;
        SurfaceHolder surfaceHolder;

        public RePaintChessBoard(ColorfulBalls p) {
            this.parent = p;
            this.surfaceHolder = this.parent.holder;
        }

        public void run() {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    ShowFactory.drawChessBoard(canvas, this.parent);
                    ShowFactory.drawBalls(canvas, this.parent);
                    ShowFactory.drawTips(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                } finally {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }

    protected static class BallMove extends Thread {
        private ObjectFactory.Ball MoveBall = null;
        private ColorfulBalls parent = null;
        private List<ObjectFactory.Position> routing = null;
        private final int sleepTime = 5;
        private final int sleepTime4Disappear = 10;
        SurfaceHolder surfaceHolder;

        public BallMove(ColorfulBalls p, List<ObjectFactory.Position> r, ObjectFactory.Ball b) {
            this.parent = p;
            this.surfaceHolder = this.parent.holder;
            this.routing = r;
            this.MoveBall = b;
            this.parent.ActiveBall = null;
        }

        public void run() {
            Canvas canvas = null;
            this.parent.GameLock = true;
            ObjectFactory.Ball b = this.MoveBall;
            ObjectFactory.Position NowStep = b.pos;
            for (int i = this.routing.size() - 1; i >= 0; i--) {
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        ObjectFactory.Position NextStep = this.routing.get(i);
                        this.parent.Board[NowStep.row][NowStep.col] = null;
                        this.parent.Board[NextStep.row][NextStep.col] = b;
                        b.pos = NextStep;
                        ShowFactory.drawChessBoard(canvas, this.parent);
                        ShowFactory.drawBalls(canvas, this.parent);
                        ShowFactory.drawTips(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e) {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th) {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    throw th;
                }
                NowStep = b.pos;
                try {
                    sleep(5);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    this.parent.getKillBalls(this.routing.get(0));
                    if (this.parent.KillBalls.size() < this.parent.KillNeedNum) {
                        this.parent.addNextBalls();
                        this.parent.getNextBalls();
                    }
                    ShowFactory.drawChessBoard(canvas, this.parent);
                    ShowFactory.drawBalls(canvas, this.parent);
                    ShowFactory.drawTips(canvas, this.parent);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e3) {
                try {
                    e3.printStackTrace();
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th2) {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    throw th2;
                }
            }
            if (this.parent.KillBalls.size() >= this.parent.KillNeedNum) {
                try {
                    sleep(10);
                } catch (InterruptedException e4) {
                    e4.printStackTrace();
                }
                try {
                    canvas = this.surfaceHolder.lockCanvas(null);
                    synchronized (this.surfaceHolder) {
                        this.parent.removeBalls();
                        ShowFactory.drawChessBoard(canvas, this.parent);
                        ShowFactory.drawBalls(canvas, this.parent);
                        ShowFactory.drawTips(canvas, this.parent);
                    }
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Exception e5) {
                    try {
                        e5.printStackTrace();
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    } catch (Throwable th3) {
                        if (canvas != null) {
                            this.surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                        throw th3;
                    }
                }
            }
            this.parent.KillBalls.clear();
            this.parent.GameLock = false;
            this.parent.doh.sendEmptyMessage(0);
        }
    }
}
