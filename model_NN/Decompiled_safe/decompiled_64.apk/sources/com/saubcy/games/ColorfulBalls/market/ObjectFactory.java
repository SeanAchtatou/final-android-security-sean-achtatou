package com.saubcy.games.ColorfulBalls.market;

public class ObjectFactory {

    protected static class Ball {
        protected int color;
        protected Position pos;
        protected int state = 0;

        public Ball(int r, int c1, int c2) {
            this.pos = new Position(r, c1);
            this.color = c2;
        }
    }

    protected static class Position {
        protected int col;
        protected int row;

        public Position(int r, int c) {
            this.row = r;
            this.col = c;
        }
    }

    protected static class Node implements Comparable<Node> {
        public static final int[] COLOPVALUE;
        public static final int NODE_IN_CLOSE = 2;
        public static final int NODE_IN_FREE = 0;
        public static final int NODE_IN_OPEN = 1;
        public static final int[] ROWOPVALUE;
        protected int F = 0;
        protected int G = 0;
        protected int H = 0;
        protected int father = -1;
        protected int index;
        protected boolean pass;
        protected int state;

        static {
            int[] iArr = new int[8];
            iArr[1] = -1;
            iArr[2] = -1;
            iArr[3] = -1;
            iArr[5] = 1;
            iArr[6] = 1;
            iArr[7] = 1;
            ROWOPVALUE = iArr;
            int[] iArr2 = new int[8];
            iArr2[0] = -1;
            iArr2[1] = -1;
            iArr2[3] = 1;
            iArr2[4] = 1;
            iArr2[5] = 1;
            iArr2[7] = -1;
            COLOPVALUE = iArr2;
        }

        public void setG(int value) {
            this.G = value;
            this.F = this.G + this.H;
        }

        public void setH(int value) {
            this.H = value;
            this.F = this.G + this.H;
        }

        public int compareTo(Node another) {
            return this.F - another.F;
        }
    }
}
