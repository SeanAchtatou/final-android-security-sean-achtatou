package com.saubcy.games.ColorfulBalls.market;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.saubcy.util.date.CYConverter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScoreManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "colorfulballs.db";
    private static final String DATABASE_TABLE_SCORES = "t_scores";
    private static final int DATABASE_VERSION = 1;
    public static final int RecordNum = 10;
    private final String CREATE_TABLE_SCORES = "CREATE TABLE IF NOT EXISTS t_scores(id INTEGER PRIMARY KEY, ranking INTEGER NOT NULL , name TEXT NOT NULL , score INTEGER NOT NULL , timestamp INTEGER NOT NULL)";
    private String defaultEmptyName = null;
    private String defaultName = null;
    private int defaultScore = 0;

    public ScoreManager(Context context, String name, String emptyName, int score) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        this.defaultName = name;
        this.defaultEmptyName = emptyName;
        this.defaultScore = score;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS t_scores(id INTEGER PRIMARY KEY, ranking INTEGER NOT NULL , name TEXT NOT NULL , score INTEGER NOT NULL , timestamp INTEGER NOT NULL)");
        initData(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public int getHighScore(int ranking) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT score FROM t_scores WHERE ranking = " + ranking, null);
        int HighScore = this.defaultScore;
        c.moveToFirst();
        if (!c.isAfterLast()) {
            HighScore = c.getInt(0);
        }
        c.close();
        db.close();
        return HighScore;
    }

    public List<ScoreStructure> getScoreList() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT ranking, name, score, timestamp FROM t_scores ORDER BY ranking", null);
        List<ScoreStructure> ScoreList = new ArrayList<>();
        c.moveToFirst();
        while (!c.isAfterLast()) {
            ScoreStructure ss = new ScoreStructure();
            ss.ranking = c.getInt(0);
            ss.name = c.getString(1);
            ss.score = c.getInt(2);
            ss.timestamp = c.getLong(3);
            ScoreList.add(ss);
            c.moveToNext();
        }
        c.close();
        db.close();
        return ScoreList;
    }

    private void initData(SQLiteDatabase db) {
        ScoreStructure ss = new ScoreStructure();
        ss.ranking = 1;
        ss.name = this.defaultName;
        ss.score = this.defaultScore;
        ss.timestamp = CYConverter.getTimestamp();
        insert(ss, db);
        for (int i = 1; i < 10; i++) {
            ScoreStructure ss2 = new ScoreStructure();
            ss2.ranking = i + 1;
            ss2.name = this.defaultEmptyName;
            ss2.score = 0;
            ss2.timestamp = CYConverter.getTimestamp();
            insert(ss2, db);
        }
    }

    private void insert(ScoreStructure ss, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put("ranking", Integer.valueOf(ss.ranking));
        values.put("name", ss.name);
        values.put("score", Integer.valueOf(ss.score));
        values.put("timestamp", Long.valueOf(ss.timestamp));
        db.insert(DATABASE_TABLE_SCORES, null, values);
    }

    private void update(ScoreStructure ss, SQLiteDatabase db) {
        db.execSQL("UPDATE t_scores SET ranking = ?, name = ?, score = ?, timestamp = ? WHERE ranking = ?", new Object[]{Integer.valueOf(ss.ranking), ss.name, Integer.valueOf(ss.score), Long.valueOf(ss.timestamp), Integer.valueOf(ss.ranking)});
    }

    /* access modifiers changed from: protected */
    public void addNewRecord(String name, int score) {
        List<ScoreStructure> ScoreList = getScoreList();
        ScoreStructure ss = ScoreList.get(ScoreList.size() - 1);
        ss.name = name;
        ss.score = score;
        ss.timestamp = CYConverter.getTimestamp();
        Collections.sort(ScoreList, Collections.reverseOrder());
        SQLiteDatabase db = getWritableDatabase();
        for (int i = 0; i < ScoreList.size(); i++) {
            ScoreStructure ss2 = ScoreList.get(i);
            ss2.ranking = i + 1;
            update(ss2, db);
        }
        db.close();
    }

    protected static class ScoreStructure implements Comparable<ScoreStructure> {
        public int id;
        public String name;
        public int ranking;
        public int score;
        public long timestamp;

        protected ScoreStructure() {
        }

        public int compareTo(ScoreStructure arg0) {
            return this.score - arg0.score;
        }
    }
}
