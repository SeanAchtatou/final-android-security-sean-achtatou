package com.mobfox.sdk.data;

import com.mobfox.sdk.AdType;

public class Response {
    private int bannerHeight;
    private int bannerWidth;
    private ClickType clickType;
    private String clickUrl;
    private String imageUrl;
    private int refresh;
    private boolean scale;
    private boolean skipPreflight;
    private String text;
    private AdType type;
    private UrlType urlType;

    public AdType getType() {
        return this.type;
    }

    public void setType(AdType adType) {
        this.type = adType;
    }

    public int getBannerWidth() {
        return this.bannerWidth;
    }

    public void setBannerWidth(int bannerWidth2) {
        this.bannerWidth = bannerWidth2;
    }

    public int getBannerHeight() {
        return this.bannerHeight;
    }

    public void setBannerHeight(int bannerHeight2) {
        this.bannerHeight = bannerHeight2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public ClickType getClickType() {
        return this.clickType;
    }

    public void setClickType(ClickType clickType2) {
        this.clickType = clickType2;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public void setClickUrl(String clickUrl2) {
        this.clickUrl = clickUrl2;
    }

    public UrlType getUrlType() {
        return this.urlType;
    }

    public void setUrlType(UrlType urlType2) {
        this.urlType = urlType2;
    }

    public int getRefresh() {
        return this.refresh;
    }

    public void setRefresh(int refresh2) {
        this.refresh = refresh2;
    }

    public boolean isScale() {
        return this.scale;
    }

    public void setScale(boolean scale2) {
        this.scale = scale2;
    }

    public boolean isSkipPreflight() {
        return this.skipPreflight;
    }

    public void setSkipPreflight(boolean skipPreflight2) {
        this.skipPreflight = skipPreflight2;
    }

    public String toString() {
        return "Response [refresh=" + this.refresh + ", type=" + this.type + ", bannerWidth=" + this.bannerWidth + ", bannerHeight=" + this.bannerHeight + ", text=" + this.text + ", imageUrl=" + this.imageUrl + ", clickType=" + this.clickType + ", clickUrl=" + this.clickUrl + ", urlType=" + this.urlType + ", scale=" + this.scale + ", skipPreflight=" + this.skipPreflight + "]";
    }
}
