package com.mobfox.sdk.data;

import android.os.Build;
import com.mobfox.sdk.Mode;

public class Request {
    private String deviceId;
    private String headers;
    private double latitude = 0.0d;
    private double longitude = 0.0d;
    private Mode mode;
    private String protocolVersion;
    private String publisherId;
    private String requestType;
    private String userAgent;

    public String getRequestType() {
        if (this.requestType == null) {
            return "";
        }
        return this.requestType;
    }

    public void setRequestType(String requestType2) {
        this.requestType = requestType2;
    }

    public String getUserAgent() {
        if (this.userAgent == null) {
            return "";
        }
        return this.userAgent;
    }

    public void setUserAgent(String userAgent2) {
        this.userAgent = userAgent2;
    }

    public String getHeaders() {
        if (this.headers == null) {
            return "";
        }
        return this.headers;
    }

    public void setHeaders(String headers2) {
        this.headers = headers2;
    }

    public String getDeviceId() {
        if (this.deviceId == null) {
            return "";
        }
        return this.deviceId;
    }

    public void setDeviceId(String deviceId2) {
        this.deviceId = deviceId2;
    }

    public Mode getMode() {
        if (this.mode == null) {
            this.mode = Mode.LIVE;
        }
        return this.mode;
    }

    public void setMode(Mode mode2) {
        this.mode = mode2;
    }

    public String getPublisherId() {
        if (this.publisherId == null) {
            return "";
        }
        return this.publisherId;
    }

    public void setPublisherId(String publisherId2) {
        this.publisherId = publisherId2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public String getDeviceMode() {
        return Build.MODEL;
    }

    public String getAndroidVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getProtocolVersion() {
        if (this.protocolVersion == null) {
            return "";
        }
        return this.protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion2) {
        this.protocolVersion = protocolVersion2;
    }
}
