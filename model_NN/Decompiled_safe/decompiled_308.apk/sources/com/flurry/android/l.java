package com.flurry.android;

import java.util.LinkedHashMap;
import java.util.Map;

final class l extends LinkedHashMap {
    private /* synthetic */ ae a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    l(ae aeVar, int i, float f) {
        super(i, f, true);
        this.a = aeVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a.b;
    }
}
