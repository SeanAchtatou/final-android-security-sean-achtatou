package twitter4j;

import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

class HashtagEntityJSONImpl implements HashtagEntity {
    private static final long serialVersionUID = 4068992372784813200L;
    private int end = -1;
    private int start = -1;
    private String text;

    HashtagEntityJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        try {
            JSONArray indicesArray = json.getJSONArray("indices");
            this.start = indicesArray.getInt(0);
            this.end = indicesArray.getInt(1);
            if (!json.isNull("text")) {
                this.text = json.getString("text");
            }
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public String getText() {
        return this.text;
    }

    public int getStart() {
        return this.start;
    }

    public int getEnd() {
        return this.end;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HashtagEntityJSONImpl that = (HashtagEntityJSONImpl) o;
        if (this.end != that.end) {
            return false;
        }
        if (this.start != that.start) {
            return false;
        }
        return this.text == null ? that.text == null : this.text.equals(that.text);
    }

    public int hashCode() {
        return (((this.start * 31) + this.end) * 31) + (this.text != null ? this.text.hashCode() : 0);
    }

    public String toString() {
        return new StringBuffer().append("HashtagEntityJSONImpl{start=").append(this.start).append(", end=").append(this.end).append(", text='").append(this.text).append('\'').append('}').toString();
    }
}
