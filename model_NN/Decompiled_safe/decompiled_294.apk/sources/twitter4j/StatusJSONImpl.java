package twitter4j;

import java.io.Serializable;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class StatusJSONImpl extends TwitterResponseImpl implements Status, Serializable {
    static Class class$twitter4j$StatusJSONImpl = null;
    private static final Logger logger;
    private static final long serialVersionUID = 1608000492860584608L;
    private Annotations annotations = null;
    private String[] contributors;
    private Date createdAt;
    private GeoLocation geoLocation = null;
    private HashtagEntity[] hashtagEntities;
    private String[] hashtags;
    private long id;
    private String inReplyToScreenName;
    private long inReplyToStatusId;
    private int inReplyToUserId;
    private boolean isFavorited;
    private boolean isTruncated;
    private Place place = null;
    private long retweetCount;
    private Status retweetedStatus;
    private String source;
    private String text;
    private URLEntity[] urlEntities;
    private URL[] urls;
    private User user = null;
    private UserMentionEntity[] userMentionEntities;
    private User[] userMentions;
    private boolean wasRetweetedByMe;

    public int compareTo(Object x0) {
        return compareTo((Status) x0);
    }

    static {
        Class cls;
        if (class$twitter4j$StatusJSONImpl == null) {
            cls = class$("twitter4j.StatusJSONImpl");
            class$twitter4j$StatusJSONImpl = cls;
        } else {
            cls = class$twitter4j$StatusJSONImpl;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    StatusJSONImpl(HttpResponse res) throws TwitterException {
        super(res);
        JSONObject json = res.asJSONObject();
        init(json);
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, json);
    }

    StatusJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        this.id = ParseUtil.getLong("id", json);
        this.text = ParseUtil.getUnescapedString("text", json);
        this.source = ParseUtil.getUnescapedString(PropertyConfiguration.SOURCE, json);
        this.createdAt = ParseUtil.getDate("created_at", json);
        this.isTruncated = ParseUtil.getBoolean("truncated", json);
        this.inReplyToStatusId = ParseUtil.getLong("in_reply_to_status_id", json);
        this.inReplyToUserId = ParseUtil.getInt("in_reply_to_user_id", json);
        this.isFavorited = ParseUtil.getBoolean("favorited", json);
        this.inReplyToScreenName = ParseUtil.getUnescapedString("in_reply_to_screen_name", json);
        this.retweetCount = ParseUtil.getLong("retweet_count", json);
        this.wasRetweetedByMe = ParseUtil.getBoolean("retweeted", json);
        try {
            if (!json.isNull("user")) {
                this.user = new UserJSONImpl(json.getJSONObject("user"));
            }
            this.geoLocation = GeoLocation.getInstance(json);
            if (!json.isNull("place")) {
                try {
                    this.place = new PlaceJSONImpl(json.getJSONObject("place"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    logger.warn(new StringBuffer().append("failed to parse place:").append(json).toString());
                }
            }
            if (!json.isNull("retweeted_status")) {
                try {
                    this.retweetedStatus = new StatusJSONImpl(json.getJSONObject("retweeted_status"));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                    logger.warn(new StringBuffer().append("failed to parse retweeted_status:").append(json).toString());
                }
            }
            if (!json.isNull("contributors")) {
                try {
                    JSONArray contributorsArray = json.getJSONArray("contributors");
                    this.contributors = new String[contributorsArray.length()];
                    for (int i = 0; i < contributorsArray.length(); i++) {
                        this.contributors[i] = contributorsArray.getString(i);
                    }
                } catch (JSONException e3) {
                    e3.printStackTrace();
                    logger.warn(new StringBuffer().append("failed to parse contributors:").append(json).toString());
                }
            } else {
                this.contributors = null;
            }
            if (!json.isNull("entities")) {
                try {
                    JSONObject entities = json.getJSONObject("entities");
                    JSONArray userMentionsArray = entities.getJSONArray("user_mentions");
                    int len = userMentionsArray.length();
                    this.userMentions = new User[len];
                    this.userMentionEntities = new UserMentionEntity[len];
                    for (int i2 = 0; i2 < len; i2++) {
                        this.userMentionEntities[i2] = new UserMentionEntityJSONImpl(userMentionsArray.getJSONObject(i2));
                        this.userMentions[i2] = new UserJSONImpl(userMentionsArray.getJSONObject(i2));
                    }
                    JSONArray urlsArray = entities.getJSONArray("urls");
                    int len2 = urlsArray.length();
                    this.urls = new URL[len2];
                    this.urlEntities = new URLEntity[len2];
                    for (int i3 = 0; i3 < len2; i3++) {
                        this.urlEntities[i3] = new URLEntityJSONImpl(urlsArray.getJSONObject(i3));
                        this.urls[i3] = this.urlEntities[i3].getURL();
                    }
                    JSONArray hashtagsArray = entities.getJSONArray("hashtags");
                    int len3 = hashtagsArray.length();
                    this.hashtags = new String[len3];
                    this.hashtagEntities = new HashtagEntity[len3];
                    for (int i4 = 0; i4 < len3; i4++) {
                        this.hashtagEntities[i4] = new HashtagEntityJSONImpl(hashtagsArray.getJSONObject(i4));
                        this.hashtags[i4] = hashtagsArray.getJSONObject(i4).getString("text");
                    }
                } catch (JSONException e4) {
                }
            }
            if (!json.isNull("annotations")) {
                try {
                    this.annotations = new Annotations(json.getJSONArray("annotations"));
                } catch (JSONException e5) {
                }
            }
        } catch (JSONException e6) {
            throw new TwitterException(e6);
        }
    }

    public int compareTo(Status that) {
        long delta = this.id - that.getId();
        if (delta < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        if (delta > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) delta;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public String getSource() {
        return this.source;
    }

    public boolean isTruncated() {
        return this.isTruncated;
    }

    public long getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public int getInReplyToUserId() {
        return this.inReplyToUserId;
    }

    public String getInReplyToScreenName() {
        return this.inReplyToScreenName;
    }

    public GeoLocation getGeoLocation() {
        return this.geoLocation;
    }

    public Place getPlace() {
        return this.place;
    }

    public String[] getContributors() {
        return this.contributors;
    }

    public Annotations getAnnotations() {
        return this.annotations;
    }

    public boolean isFavorited() {
        return this.isFavorited;
    }

    public User getUser() {
        return this.user;
    }

    public boolean isRetweet() {
        return this.retweetedStatus != null;
    }

    public Status getRetweetedStatus() {
        return this.retweetedStatus;
    }

    public long getRetweetCount() {
        return this.retweetCount;
    }

    public boolean isRetweetedByMe() {
        return this.wasRetweetedByMe;
    }

    public User[] getUserMentions() {
        return this.userMentions;
    }

    public UserMentionEntity[] getUserMentionEntities() {
        return this.userMentionEntities;
    }

    public URL[] getURLs() {
        return this.urls;
    }

    public URLEntity[] getURLEntities() {
        return this.urlEntities;
    }

    public String[] getHashtags() {
        return this.hashtags;
    }

    public HashtagEntity[] getHashtagEntities() {
        return this.hashtagEntities;
    }

    static ResponseList<Status> createStatusList(HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            JSONArray list = res.asJSONArray();
            int size = list.length();
            ResponseList<Status> statuses = new ResponseListImpl<>(size, res);
            for (int i = 0; i < size; i++) {
                JSONObject json = list.getJSONObject(i);
                Status status = new StatusJSONImpl(json);
                DataObjectFactoryUtil.registerJSONObject(status, json);
                statuses.add(status);
            }
            DataObjectFactoryUtil.registerJSONObject(statuses, list);
            return statuses;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof Status) && ((Status) obj).getId() == this.id;
    }

    public String toString() {
        return new StringBuffer().append("StatusJSONImpl{createdAt=").append(this.createdAt).append(", id=").append(this.id).append(", text='").append(this.text).append('\'').append(", source='").append(this.source).append('\'').append(", isTruncated=").append(this.isTruncated).append(", inReplyToStatusId=").append(this.inReplyToStatusId).append(", inReplyToUserId=").append(this.inReplyToUserId).append(", isFavorited=").append(this.isFavorited).append(", inReplyToScreenName='").append(this.inReplyToScreenName).append('\'').append(", geoLocation=").append(this.geoLocation).append(", place=").append(this.place).append(", retweetCount=").append(this.retweetCount).append(", wasRetweetedByMe=").append(this.wasRetweetedByMe).append(", contributors=").append(this.contributors == null ? null : Arrays.asList(this.contributors)).append(", annotations=").append(this.annotations).append(", retweetedStatus=").append(this.retweetedStatus).append(", userMentions=").append(this.userMentions == null ? null : Arrays.asList(this.userMentions)).append(", userMentionEntities=").append(this.userMentionEntities == null ? null : Arrays.asList(this.userMentionEntities)).append(", urls=").append(this.urls == null ? null : Arrays.asList(this.urls)).append(", urlEntities=").append(this.urlEntities == null ? null : Arrays.asList(this.urlEntities)).append(", hashtags=").append(this.hashtags == null ? null : Arrays.asList(this.hashtags)).append(", hashtagEntities=").append(this.hashtagEntities == null ? null : Arrays.asList(this.hashtagEntities)).append(", user=").append(this.user).append('}').toString();
    }
}
