package twitter4j;

import java.io.Serializable;
import twitter4j.internal.http.HttpResponse;

abstract class TwitterResponseImpl implements TwitterResponse, Serializable {
    private static final long serialVersionUID = -7284708239736552059L;
    private transient RateLimitStatus rateLimitStatus = null;

    public TwitterResponseImpl() {
    }

    public TwitterResponseImpl(HttpResponse res) {
        this.rateLimitStatus = RateLimitStatusJSONImpl.createFromResponseHeader(res);
    }

    public RateLimitStatus getRateLimitStatus() {
        return this.rateLimitStatus;
    }
}
