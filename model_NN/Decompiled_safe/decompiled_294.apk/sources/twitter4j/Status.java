package twitter4j;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;

public interface Status extends Comparable<Status>, TwitterResponse, Serializable {
    Annotations getAnnotations();

    String[] getContributors();

    Date getCreatedAt();

    GeoLocation getGeoLocation();

    HashtagEntity[] getHashtagEntities();

    String[] getHashtags();

    long getId();

    String getInReplyToScreenName();

    long getInReplyToStatusId();

    int getInReplyToUserId();

    Place getPlace();

    long getRetweetCount();

    Status getRetweetedStatus();

    String getSource();

    String getText();

    URLEntity[] getURLEntities();

    URL[] getURLs();

    User getUser();

    UserMentionEntity[] getUserMentionEntities();

    User[] getUserMentions();

    boolean isFavorited();

    boolean isRetweet();

    boolean isRetweetedByMe();

    boolean isTruncated();
}
