package twitter4j;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class UserListJSONImpl extends TwitterResponseImpl implements UserList, Serializable {
    private static final long serialVersionUID = -6345893237975349030L;
    private String description;
    private boolean following;
    private String fullName;
    private int id;
    private int memberCount;
    private boolean mode;
    private String name;
    private String slug;
    private int subscriberCount;
    private String uri;
    private User user;

    public int compareTo(Object x0) {
        return compareTo((UserList) x0);
    }

    UserListJSONImpl(HttpResponse res) throws TwitterException {
        super(res);
        DataObjectFactoryUtil.clearThreadLocalMap();
        JSONObject json = res.asJSONObject();
        init(json);
        DataObjectFactoryUtil.registerJSONObject(this, json);
    }

    UserListJSONImpl(JSONObject json) throws TwitterException {
        init(json);
    }

    private void init(JSONObject json) throws TwitterException {
        this.id = ParseUtil.getInt("id", json);
        this.name = ParseUtil.getRawString("name", json);
        this.fullName = ParseUtil.getRawString("full_name", json);
        this.slug = ParseUtil.getRawString("slug", json);
        this.description = ParseUtil.getRawString("description", json);
        this.subscriberCount = ParseUtil.getInt("subscriber_count", json);
        this.memberCount = ParseUtil.getInt("member_count", json);
        this.uri = ParseUtil.getRawString("uri", json);
        this.mode = "public".equals(ParseUtil.getRawString("mode", json));
        this.following = ParseUtil.getBoolean("following", json);
        try {
            if (!json.isNull("user")) {
                this.user = new UserJSONImpl(json.getJSONObject("user"));
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    public int compareTo(UserList that) {
        return this.id - that.getId();
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getSlug() {
        return this.slug;
    }

    public String getDescription() {
        return this.description;
    }

    public int getSubscriberCount() {
        return this.subscriberCount;
    }

    public int getMemberCount() {
        return this.memberCount;
    }

    public URI getURI() {
        try {
            return new URI(this.uri);
        } catch (URISyntaxException e) {
            return null;
        }
    }

    public boolean isPublic() {
        return this.mode;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public User getUser() {
        return this.user;
    }

    static PagableResponseList<UserList> createPagableUserListList(HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            JSONObject json = res.asJSONObject();
            JSONArray list = json.getJSONArray("lists");
            int size = list.length();
            PagableResponseList<UserList> users = new PagableResponseListImpl<>(size, json, res);
            for (int i = 0; i < size; i++) {
                JSONObject userListJson = list.getJSONObject(i);
                UserList userList = new UserListJSONImpl(userListJson);
                users.add(userList);
                DataObjectFactoryUtil.registerJSONObject(userList, userListJson);
            }
            DataObjectFactoryUtil.registerJSONObject(users, json);
            return users;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    static ResponseList<UserList> createUserListList(HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            JSONArray list = res.asJSONArray();
            int size = list.length();
            ResponseList<UserList> users = new ResponseListImpl<>(size, res);
            for (int i = 0; i < size; i++) {
                JSONObject userListJson = list.getJSONObject(i);
                UserList userList = new UserListJSONImpl(userListJson);
                users.add(userList);
                DataObjectFactoryUtil.registerJSONObject(userList, userListJson);
            }
            DataObjectFactoryUtil.registerJSONObject(users, list);
            return users;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return (obj instanceof UserList) && ((UserList) obj).getId() == this.id;
    }

    public String toString() {
        return new StringBuffer().append("UserListJSONImpl{id=").append(this.id).append(", name='").append(this.name).append('\'').append(", fullName='").append(this.fullName).append('\'').append(", slug='").append(this.slug).append('\'').append(", description='").append(this.description).append('\'').append(", subscriberCount=").append(this.subscriberCount).append(", memberCount=").append(this.memberCount).append(", uri='").append(this.uri).append('\'').append(", mode=").append(this.mode).append(", user=").append(this.user).append(", following=").append(this.following).append('}').toString();
    }
}
