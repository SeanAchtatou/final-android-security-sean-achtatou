package twitter4j;

import java.util.ArrayList;
import twitter4j.internal.http.HttpResponse;

class ResponseListImpl<T> extends ArrayList<T> implements ResponseList<T> {
    private static final long serialVersionUID = 5646617841989265312L;
    private transient RateLimitStatus featureSpecificRateLimitStatus = null;
    private transient RateLimitStatus rateLimitStatus = null;

    ResponseListImpl(int size, HttpResponse res) {
        super(size);
        this.rateLimitStatus = RateLimitStatusJSONImpl.createFromResponseHeader(res);
        this.featureSpecificRateLimitStatus = RateLimitStatusJSONImpl.createFeatureSpecificRateLimitStatusFromResponseHeader(res);
    }

    public RateLimitStatus getRateLimitStatus() {
        return this.rateLimitStatus;
    }

    public RateLimitStatus getFeatureSpecificRateLimitStatus() {
        return this.featureSpecificRateLimitStatus;
    }
}
