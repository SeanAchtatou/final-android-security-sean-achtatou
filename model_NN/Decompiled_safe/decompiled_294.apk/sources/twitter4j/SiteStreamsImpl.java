package twitter4j;

import java.io.IOException;
import java.io.InputStream;
import twitter4j.conf.Configuration;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class SiteStreamsImpl extends AbstractStreamImplementation implements StreamImplementation, StreamListener {
    private static ThreadLocal<Integer> forUser = new ThreadLocal<Integer>() {
        /* access modifiers changed from: protected */
        /* renamed from: initialValue  reason: collision with other method in class */
        public Object m0initialValue() {
            return initialValue();
        }

        /* access modifiers changed from: protected */
        public Integer initialValue() {
            return new Integer(0);
        }
    };
    SiteStreamsListener listener;

    SiteStreamsImpl(Dispatcher dispatcher, InputStream stream, Configuration conf) throws IOException {
        super(dispatcher, stream, conf);
    }

    SiteStreamsImpl(Dispatcher dispatcher, HttpResponse response, Configuration conf) throws IOException {
        super(dispatcher, response, conf);
    }

    public void next(StreamListener[] listeners) throws TwitterException {
        this.listener = (SiteStreamsListener) listeners[0];
        handleNextElement();
    }

    /* access modifiers changed from: protected */
    public String parseLine(String line) {
        if ("".equals(line) || line == null) {
            return line;
        }
        int userIdEnd = line.indexOf(44, 12);
        if (line.charAt(12) == '\"') {
            forUser.set(new Integer(Integer.parseInt(line.substring(13, userIdEnd - 1))));
            return line.substring(userIdEnd + 11, line.length() - 1);
        }
        forUser.set(new Integer(Integer.parseInt(line.substring(12, userIdEnd))));
        return line.substring(userIdEnd + 11, line.length() - 1);
    }

    /* access modifiers changed from: protected */
    public void onStatus(JSONObject json) throws TwitterException {
        this.listener.onStatus(forUser.get().intValue(), asStatus(json));
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* access modifiers changed from: protected */
    public void onDelete(JSONObject json) throws JSONException {
        JSONObject deletionNotice = json.getJSONObject("delete");
        if (deletionNotice.has("status")) {
            this.listener.onDeletionNotice(forUser.get().intValue(), new StatusDeletionNoticeImpl(deletionNotice.getJSONObject("status")));
            return;
        }
        JSONObject directMessage = deletionNotice.getJSONObject("direct_message");
        this.listener.onDeletionNotice(forUser.get().intValue(), ParseUtil.getInt("id", directMessage), ParseUtil.getInt("user_id", directMessage));
    }

    /* access modifiers changed from: protected */
    public void onDirectMessage(JSONObject json) throws TwitterException {
        this.listener.onDirectMessage(forUser.get().intValue(), asDirectMessage(json));
    }

    /* access modifiers changed from: protected */
    public void onFriends(JSONObject json) throws TwitterException, JSONException {
        this.listener.onFriendList(forUser.get().intValue(), asFriendList(json));
    }

    /* access modifiers changed from: protected */
    public void onFavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        this.listener.onFavorite(forUser.get().intValue(), asUser(source), asUser(target), asStatus(targetObject));
    }

    /* access modifiers changed from: protected */
    public void onUnfavorite(JSONObject source, JSONObject target, JSONObject targetObject) throws TwitterException {
        this.listener.onUnfavorite(forUser.get().intValue(), asUser(source), asUser(target), asStatus(targetObject));
    }

    /* access modifiers changed from: protected */
    public void onFollow(JSONObject source, JSONObject target) throws TwitterException {
        this.listener.onFollow(forUser.get().intValue(), asUser(source), asUser(target));
    }

    /* access modifiers changed from: protected */
    public void onUnfollow(JSONObject source, JSONObject target) throws TwitterException {
        this.listener.onUnfollow(forUser.get().intValue(), asUser(source), asUser(target));
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberAddition(JSONObject addedMember, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListMemberAddition(forUser.get().intValue(), asUser(addedMember), asUser(owner), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListMemberDeletion(JSONObject deletedMember, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListMemberDeletion(forUser.get().intValue(), asUser(deletedMember), asUser(owner), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListSubscription(JSONObject source, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListSubscription(forUser.get().intValue(), asUser(source), asUser(owner), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListUnsubscription(JSONObject source, JSONObject owner, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListUnsubscription(forUser.get().intValue(), asUser(source), asUser(owner), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListCreation(JSONObject source, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListCreation(forUser.get().intValue(), asUser(source), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListUpdated(JSONObject source, JSONObject userList) throws TwitterException, JSONException {
        this.listener.onUserListUpdate(forUser.get().intValue(), asUser(source), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserListDestroyed(JSONObject source, JSONObject userList) throws TwitterException {
        this.listener.onUserListDeletion(forUser.get().intValue(), asUser(source), asUserList(userList));
    }

    /* access modifiers changed from: protected */
    public void onUserUpdate(JSONObject source, JSONObject target) throws TwitterException {
        this.listener.onUserProfileUpdate(forUser.get().intValue(), asUser(source));
    }

    /* access modifiers changed from: protected */
    public void onBlock(JSONObject source, JSONObject target) throws TwitterException {
        this.listener.onBlock(forUser.get().intValue(), asUser(source), asUser(target));
    }

    /* access modifiers changed from: protected */
    public void onUnblock(JSONObject source, JSONObject target) throws TwitterException {
        this.listener.onUnblock(forUser.get().intValue(), asUser(source), asUser(target));
    }

    public void onException(Exception ex) {
        this.listener.onException(ex);
    }
}
