package twitter4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.RequestToken;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.async.DispatcherFactory;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpResponseEvent;
import twitter4j.internal.logging.Logger;
import twitter4j.internal.util.StringUtil;

public final class TwitterStream extends TwitterOAuthSupportBaseImpl {
    private static final int HTTP_ERROR_INITIAL_WAIT = 10000;
    private static final int HTTP_ERROR_WAIT_CAP = 240000;
    private static final int NO_WAIT = 0;
    private static final int TCP_ERROR_INITIAL_WAIT = 250;
    private static final int TCP_ERROR_WAIT_CAP = 16000;
    static Class class$twitter4j$TwitterStream = null;
    static int count = 0;
    private static transient Dispatcher dispatcher = null;
    private static final Logger logger;
    private static final long serialVersionUID = 5529611191443189901L;
    private static boolean shutdown = false;
    private TwitterStreamConsumer handler;
    private final HttpClientWrapper http;
    private List<ConnectionLifeCycleListener> lifeCycleListeners;
    private StreamListener[] streamListeners;

    static StatusStream access$000(TwitterStream x0, String x1, int x2) throws TwitterException {
        return x0.getCountStream(x1, x2);
    }

    static Dispatcher access$100(TwitterStream x0) {
        return x0.getDispatcher();
    }

    static Logger access$200() {
        return logger;
    }

    static List access$300(TwitterStream x0) {
        return x0.lifeCycleListeners;
    }

    static StreamListener[] access$400(TwitterStream x0) {
        return x0.streamListeners;
    }

    public Configuration getConfiguration() {
        return super.getConfiguration();
    }

    public AccessToken getOAuthAccessToken() throws TwitterException {
        return super.getOAuthAccessToken();
    }

    public AccessToken getOAuthAccessToken(String x0) throws TwitterException {
        return super.getOAuthAccessToken(x0);
    }

    public AccessToken getOAuthAccessToken(String x0, String x1) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1);
    }

    public AccessToken getOAuthAccessToken(String x0, String x1, String x2) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1, x2);
    }

    public AccessToken getOAuthAccessToken(RequestToken x0) throws TwitterException {
        return super.getOAuthAccessToken(x0);
    }

    public AccessToken getOAuthAccessToken(RequestToken x0, String x1) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return super.getOAuthRequestToken();
    }

    public RequestToken getOAuthRequestToken(String x0) throws TwitterException {
        return super.getOAuthRequestToken(x0);
    }

    public void httpResponseReceived(HttpResponseEvent x0) {
        super.httpResponseReceived(x0);
    }

    public boolean isOAuthEnabled() {
        return super.isOAuthEnabled();
    }

    public void setOAuthAccessToken(String x0, String x1) {
        super.setOAuthAccessToken(x0, x1);
    }

    public void setOAuthAccessToken(AccessToken x0) {
        super.setOAuthAccessToken(x0);
    }

    public void setOAuthConsumer(String x0, String x1) {
        super.setOAuthConsumer(x0, x1);
    }

    public void setRateLimitStatusListener(RateLimitStatusListener x0) {
        super.setRateLimitStatusListener(x0);
    }

    public String toString() {
        return super.toString();
    }

    static {
        Class cls;
        if (class$twitter4j$TwitterStream == null) {
            cls = class$("twitter4j.TwitterStream");
            class$twitter4j$TwitterStream = cls;
        } else {
            cls = class$twitter4j$TwitterStream;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public TwitterStream() {
        super(ConfigurationContext.getInstance());
        this.streamListeners = new StreamListener[0];
        this.lifeCycleListeners = new ArrayList(0);
        this.handler = null;
        this.http = new HttpClientWrapper(new StreamingReadTimeoutConfiguration(this.conf));
    }

    public TwitterStream(String screenName, String password) {
        super(ConfigurationContext.getInstance(), screenName, password);
        this.streamListeners = new StreamListener[0];
        this.lifeCycleListeners = new ArrayList(0);
        this.handler = null;
        this.http = new HttpClientWrapper(new StreamingReadTimeoutConfiguration(this.conf));
    }

    public TwitterStream(String screenName, String password, StreamListener listener) {
        super(ConfigurationContext.getInstance(), screenName, password);
        this.streamListeners = new StreamListener[0];
        this.lifeCycleListeners = new ArrayList(0);
        this.handler = null;
        if (listener != null) {
            addListener(listener);
        }
        this.http = new HttpClientWrapper(new StreamingReadTimeoutConfiguration(this.conf));
    }

    TwitterStream(Configuration conf, Authorization auth, StreamListener listener) {
        super(conf, auth);
        this.streamListeners = new StreamListener[0];
        this.lifeCycleListeners = new ArrayList(0);
        this.handler = null;
        if (listener != null) {
            addListener(listener);
        }
        this.http = new HttpClientWrapper(new StreamingReadTimeoutConfiguration(conf));
    }

    public void firehose(int count2) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this, count2) {
            private final TwitterStream this$0;
            private final int val$count;

            {
                this.this$0 = r1;
                this.val$count = r2;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m1getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFirehoseStream(this.val$count);
            }
        });
    }

    public StatusStream getFirehoseStream(int count2) throws TwitterException {
        ensureAuthorizationEnabled();
        return getCountStream("statuses/firehose.json", count2);
    }

    public void links(int count2) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this, count2) {
            private final TwitterStream this$0;
            private final int val$count;

            {
                this.this$0 = r1;
                this.val$count = r2;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m2getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getLinksStream(this.val$count);
            }
        });
    }

    public StatusStream getLinksStream(int count2) throws TwitterException {
        ensureAuthorizationEnabled();
        return getCountStream("statuses/links.json", count2);
    }

    public void stream(String relativeUrl, int count2, boolean handleUserStream) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this, relativeUrl, count2) {
            private final TwitterStream this$0;
            private final int val$count;
            private final String val$relativeUrl;

            {
                this.this$0 = r1;
                this.val$relativeUrl = r2;
                this.val$count = r3;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m3getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return TwitterStream.access$000(this.this$0, this.val$relativeUrl, this.val$count);
            }
        });
    }

    private StatusStream getCountStream(String relativeUrl, int count2) throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            return new StatusStreamImpl(getDispatcher(), this.http.post(new StringBuffer().append(this.conf.getStreamBaseURL()).append(relativeUrl).toString(), new HttpParameter[]{new HttpParameter("count", String.valueOf(count2))}, this.auth), this.conf);
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void retweet() {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m4getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getRetweetStream();
            }
        });
    }

    public StatusStream getRetweetStream() throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            return new StatusStreamImpl(getDispatcher(), this.http.post(new StringBuffer().append(this.conf.getStreamBaseURL()).append("statuses/retweet.json").toString(), new HttpParameter[0], this.auth), this.conf);
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void sample() {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this) {
            private final TwitterStream this$0;

            {
                this.this$0 = r1;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m5getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getSampleStream();
            }
        });
    }

    public StatusStream getSampleStream() throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            return new StatusStreamImpl(getDispatcher(), this.http.get(new StringBuffer().append(this.conf.getStreamBaseURL()).append("statuses/sample.json").toString(), this.auth), this.conf);
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void user() {
        user(null);
    }

    public void user(String[] track) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        for (StreamListener listener : this.streamListeners) {
            if (!(listener instanceof UserStreamListener)) {
                throw new IllegalStateException(new StringBuffer().append("Only UserStreamListener is supported. found: ").append(listener.getClass()).toString());
            }
        }
        startHandler(new TwitterStreamConsumer(this, track) {
            private final TwitterStream this$0;
            private final String[] val$track;

            {
                this.this$0 = r1;
                this.val$track = r2;
            }

            public StreamImplementation getStream() throws TwitterException {
                return m6getStream();
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public UserStream m6getStream() throws TwitterException {
                return this.this$0.getUserStream(this.val$track);
            }
        });
    }

    public UserStream getUserStream() throws TwitterException {
        return getUserStream(null);
    }

    public UserStream getUserStream(String[] track) throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            List<HttpParameter> params = new ArrayList<>();
            if (this.conf.isUserStreamRepliesAllEnabled()) {
                params.add(new HttpParameter("replies", "all"));
            }
            if (track != null) {
                params.add(new HttpParameter("track", StringUtil.join(track)));
            }
            return new UserStreamImpl(getDispatcher(), this.http.post(new StringBuffer().append(this.conf.getUserStreamBaseURL()).append("user.json").toString(), (HttpParameter[]) params.toArray(new HttpParameter[params.size()]), this.auth), this.conf);
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void site(boolean withFollowings, int[] follow) {
        ensureOAuthEnabled();
        ensureListenerIsSet();
        for (StreamListener listener : this.streamListeners) {
            if (!(listener instanceof SiteStreamsListener)) {
                throw new IllegalStateException(new StringBuffer().append("Only SiteStreamListener is supported. found: ").append(listener.getClass()).toString());
            }
        }
        startHandler(new TwitterStreamConsumer(this, withFollowings, follow) {
            private final TwitterStream this$0;
            private final int[] val$follow;
            private final boolean val$withFollowings;

            {
                this.this$0 = r1;
                this.val$withFollowings = r2;
                this.val$follow = r3;
            }

            public StreamImplementation getStream() throws TwitterException {
                try {
                    return new SiteStreamsImpl(TwitterStream.access$100(this.this$0), this.this$0.getSiteStream(this.val$withFollowings, this.val$follow), this.this$0.conf);
                } catch (IOException e) {
                    throw new TwitterException(e);
                }
            }
        });
    }

    private Dispatcher getDispatcher() {
        Class cls;
        Dispatcher dispatcher2;
        if (class$twitter4j$TwitterStream == null) {
            cls = class$("twitter4j.TwitterStream");
            class$twitter4j$TwitterStream = cls;
        } else {
            cls = class$twitter4j$TwitterStream;
        }
        synchronized (cls) {
            if (shutdown) {
                throw new IllegalStateException("Already shut down");
            }
            if (dispatcher == null) {
                dispatcher = new DispatcherFactory(this.conf).getInstance();
            }
            dispatcher2 = dispatcher;
        }
        return dispatcher2;
    }

    public synchronized void shutdown() {
        Class cls;
        super.shutdown();
        cleanUp();
        if (class$twitter4j$TwitterStream == null) {
            cls = class$("twitter4j.TwitterStream");
            class$twitter4j$TwitterStream = cls;
        } else {
            cls = class$twitter4j$TwitterStream;
        }
        synchronized (cls) {
            if (dispatcher != null) {
                dispatcher.shutdown();
                dispatcher = null;
            }
            shutdown = true;
        }
    }

    /* access modifiers changed from: package-private */
    public InputStream getSiteStream(boolean withFollowings, int[] follow) throws TwitterException {
        ensureOAuthEnabled();
        HttpClientWrapper httpClientWrapper = this.http;
        String stringBuffer = new StringBuffer().append(this.conf.getSiteStreamBaseURL()).append("site.json").toString();
        HttpParameter[] httpParameterArr = new HttpParameter[2];
        httpParameterArr[0] = new HttpParameter("with", withFollowings ? "followings" : "user");
        httpParameterArr[1] = new HttpParameter("follow", StringUtil.join(follow));
        return httpClientWrapper.post(stringBuffer, httpParameterArr, this.auth).asStream();
    }

    public void filter(FilterQuery query) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this, query) {
            private final TwitterStream this$0;
            private final FilterQuery val$query;

            {
                this.this$0 = r1;
                this.val$query = r2;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m7getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFilterStream(this.val$query);
            }
        });
    }

    public StatusStream getFilterStream(FilterQuery query) throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            return new StatusStreamImpl(getDispatcher(), this.http.post(new StringBuffer().append(this.conf.getStreamBaseURL()).append("statuses/filter.json").toString(), query.asHttpParameterArray(), this.auth), this.conf);
        } catch (IOException e) {
            throw new TwitterException(e);
        }
    }

    public void filter(int count2, int[] follow, String[] track) {
        ensureAuthorizationEnabled();
        ensureListenerIsSet();
        ensureStatusStreamListenerIsSet();
        startHandler(new TwitterStreamConsumer(this, count2, follow, track) {
            private final TwitterStream this$0;
            private final int val$count;
            private final int[] val$follow;
            private final String[] val$track;

            {
                this.this$0 = r1;
                this.val$count = r2;
                this.val$follow = r3;
                this.val$track = r4;
            }

            /* renamed from: getStream  reason: collision with other method in class */
            public StreamImplementation m8getStream() throws TwitterException {
                return getStream();
            }

            public StatusStream getStream() throws TwitterException {
                return this.this$0.getFilterStream(this.val$count, this.val$follow, this.val$track);
            }
        });
    }

    public StatusStream getFilterStream(int count2, int[] follow, String[] track) throws TwitterException {
        ensureAuthorizationEnabled();
        return getFilterStream(new FilterQuery(count2, follow, track, null));
    }

    private void ensureListenerIsSet() {
        if (this.streamListeners.length == 0) {
            throw new IllegalStateException("No listener is set.");
        }
    }

    private void ensureStatusStreamListenerIsSet() {
        for (StreamListener listener : this.streamListeners) {
            if (!(listener instanceof StatusListener)) {
                throw new IllegalStateException(new StringBuffer().append("Only StatusListener is supported. found: ").append(listener.getClass()).toString());
            }
        }
    }

    private synchronized void startHandler(TwitterStreamConsumer handler2) {
        cleanUp();
        if (this.streamListeners.length == 0) {
            throw new IllegalStateException("StatusListener is not set.");
        }
        this.handler = handler2;
        this.handler.start();
    }

    public synchronized void cleanUp() {
        if (this.handler != null) {
            this.handler.close();
        }
    }

    public void cleanup() {
        cleanUp();
    }

    public void addConnectionLifeCycleListener(ConnectionLifeCycleListener listener) {
        this.lifeCycleListeners.add(listener);
    }

    public void setStatusListener(StatusListener listener) {
        this.streamListeners = new StreamListener[1];
        this.streamListeners[0] = listener;
    }

    public void addStatusListener(StatusListener statusListener) {
        addListener((StreamListener) statusListener);
    }

    public void setUserStreamListener(UserStreamListener listener) {
        this.streamListeners = new StreamListener[1];
        this.streamListeners[0] = listener;
    }

    public void addUserStreamListener(UserStreamListener userStreamListener) {
        addListener((StreamListener) userStreamListener);
    }

    public void addListener(UserStreamListener listener) {
        addListener((StreamListener) listener);
    }

    public void addListener(StatusListener listener) {
        addListener((StreamListener) listener);
    }

    public void addListener(SiteStreamsListener listener) {
        addListener((StreamListener) listener);
    }

    private synchronized void addListener(StreamListener listener) {
        StreamListener[] newListeners = new StreamListener[(this.streamListeners.length + 1)];
        System.arraycopy(this.streamListeners, 0, newListeners, 0, this.streamListeners.length);
        newListeners[newListeners.length - 1] = listener;
        this.streamListeners = newListeners;
    }

    abstract class TwitterStreamConsumer extends Thread {
        private final String NAME;
        private volatile boolean closed;
        private StreamImplementation stream = null;
        private final TwitterStream this$0;

        /* access modifiers changed from: package-private */
        public abstract StreamImplementation getStream() throws TwitterException;

        TwitterStreamConsumer(TwitterStream twitterStream) {
            this.this$0 = twitterStream;
            StringBuffer append = new StringBuffer().append("Twitter Stream consumer-");
            int i = TwitterStream.count + 1;
            TwitterStream.count = i;
            this.NAME = append.append(i).toString();
            this.closed = false;
            setName(new StringBuffer().append(this.NAME).append("[initializing]").toString());
        }

        /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        public void run() {
            /*
                r15 = this;
                r14 = 1
                r13 = 200(0xc8, float:2.8E-43)
                r9 = 0
                r1 = 0
            L_0x0005:
                boolean r10 = r15.closed
                if (r10 != 0) goto L_0x007c
                boolean r10 = r15.closed     // Catch:{ TwitterException -> 0x0056 }
                if (r10 != 0) goto L_0x0005
                twitter4j.StreamImplementation r10 = r15.stream     // Catch:{ TwitterException -> 0x0056 }
                if (r10 != 0) goto L_0x0005
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = "Establishing connection."
                r10.info(r11)     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r10 = "[Establishing connection]"
                r15.setStatus(r10)     // Catch:{ TwitterException -> 0x0056 }
                twitter4j.StreamImplementation r10 = r15.getStream()     // Catch:{ TwitterException -> 0x0056 }
                r15.stream = r10     // Catch:{ TwitterException -> 0x0056 }
                r1 = 1
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = "Connection established."
                r10.info(r11)     // Catch:{ TwitterException -> 0x0056 }
                twitter4j.TwitterStream r10 = r15.this$0     // Catch:{ TwitterException -> 0x0056 }
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)     // Catch:{ TwitterException -> 0x0056 }
                java.util.Iterator r3 = r10.iterator()     // Catch:{ TwitterException -> 0x0056 }
            L_0x0039:
                boolean r10 = r3.hasNext()     // Catch:{ TwitterException -> 0x0056 }
                if (r10 == 0) goto L_0x00ae
                java.lang.Object r6 = r3.next()     // Catch:{ TwitterException -> 0x0056 }
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6     // Catch:{ TwitterException -> 0x0056 }
                r6.onConnect()     // Catch:{ Exception -> 0x0049 }
                goto L_0x0039
            L_0x0049:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = r2.getMessage()     // Catch:{ TwitterException -> 0x0056 }
                r10.warn(r11)     // Catch:{ TwitterException -> 0x0056 }
                goto L_0x0039
            L_0x0056:
                r10 = move-exception
                r8 = r10
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r8.getMessage()
                r10.info(r11)
                boolean r10 = r15.closed
                if (r10 != 0) goto L_0x0005
                if (r9 != 0) goto L_0x0136
                int r10 = r8.getStatusCode()
                r11 = 403(0x193, float:5.65E-43)
                if (r10 != r11) goto L_0x0106
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = "This account is not in required role."
                r10.warn(r11)
                r15.closed = r14
            L_0x007c:
                twitter4j.StreamImplementation r10 = r15.stream
                if (r10 == 0) goto L_0x0271
                if (r1 == 0) goto L_0x0271
                twitter4j.StreamImplementation r10 = r15.stream     // Catch:{ IOException -> 0x01e9, Exception -> 0x0211 }
                r10.close()     // Catch:{ IOException -> 0x01e9, Exception -> 0x0211 }
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x0091:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x0271
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x00a1 }
                goto L_0x0091
            L_0x00a1:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x0091
            L_0x00ae:
                r9 = 0
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = "Receiving status stream."
                r10.info(r11)     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r10 = "[Receiving stream]"
                r15.setStatus(r10)     // Catch:{ TwitterException -> 0x0056 }
            L_0x00bd:
                boolean r10 = r15.closed     // Catch:{ TwitterException -> 0x0056 }
                if (r10 != 0) goto L_0x0005
                twitter4j.StreamImplementation r10 = r15.stream     // Catch:{ IllegalStateException -> 0x00cd, TwitterException -> 0x00dc, Exception -> 0x00ef }
                twitter4j.TwitterStream r11 = r15.this$0     // Catch:{ IllegalStateException -> 0x00cd, TwitterException -> 0x00dc, Exception -> 0x00ef }
                twitter4j.StreamListener[] r11 = twitter4j.TwitterStream.access$400(r11)     // Catch:{ IllegalStateException -> 0x00cd, TwitterException -> 0x00dc, Exception -> 0x00ef }
                r10.next(r11)     // Catch:{ IllegalStateException -> 0x00cd, TwitterException -> 0x00dc, Exception -> 0x00ef }
                goto L_0x00bd
            L_0x00cd:
                r10 = move-exception
                r4 = r10
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = r4.getMessage()     // Catch:{ TwitterException -> 0x0056 }
                r10.warn(r11)     // Catch:{ TwitterException -> 0x0056 }
                goto L_0x0005
            L_0x00dc:
                r10 = move-exception
                r2 = r10
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = r2.getMessage()     // Catch:{ TwitterException -> 0x0056 }
                r10.info(r11)     // Catch:{ TwitterException -> 0x0056 }
                twitter4j.StreamImplementation r10 = r15.stream     // Catch:{ TwitterException -> 0x0056 }
                r10.onException(r2)     // Catch:{ TwitterException -> 0x0056 }
                throw r2     // Catch:{ TwitterException -> 0x0056 }
            L_0x00ef:
                r10 = move-exception
                r2 = r10
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ TwitterException -> 0x0056 }
                java.lang.String r11 = r2.getMessage()     // Catch:{ TwitterException -> 0x0056 }
                r10.info(r11)     // Catch:{ TwitterException -> 0x0056 }
                twitter4j.StreamImplementation r10 = r15.stream     // Catch:{ TwitterException -> 0x0056 }
                r10.onException(r2)     // Catch:{ TwitterException -> 0x0056 }
                r10 = 1
                r15.closed = r10     // Catch:{ TwitterException -> 0x0056 }
                goto L_0x0005
            L_0x0106:
                r1 = 0
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x0111:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x012e
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x0121 }
                goto L_0x0111
            L_0x0121:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x0111
            L_0x012e:
                int r10 = r8.getStatusCode()
                if (r10 <= r13) goto L_0x016b
                r9 = 10000(0x2710, float:1.4013E-41)
            L_0x0136:
                int r10 = r8.getStatusCode()
                if (r10 <= r13) goto L_0x0142
                r10 = 10000(0x2710, float:1.4013E-41)
                if (r9 >= r10) goto L_0x0142
                r9 = 10000(0x2710, float:1.4013E-41)
            L_0x0142:
                if (r1 == 0) goto L_0x0170
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x014e:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x0170
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x015e }
                goto L_0x014e
            L_0x015e:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x014e
            L_0x016b:
                if (r9 != 0) goto L_0x0136
                r9 = 250(0xfa, float:3.5E-43)
                goto L_0x0136
            L_0x0170:
                boolean r10 = r15.closed
                if (r10 != 0) goto L_0x01c3
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.StringBuffer r11 = new java.lang.StringBuffer
                r11.<init>()
                java.lang.String r12 = "Waiting for "
                java.lang.StringBuffer r11 = r11.append(r12)
                java.lang.StringBuffer r11 = r11.append(r9)
                java.lang.String r12 = " milliseconds"
                java.lang.StringBuffer r11 = r11.append(r12)
                java.lang.String r11 = r11.toString()
                r10.info(r11)
                java.lang.StringBuffer r10 = new java.lang.StringBuffer
                r10.<init>()
                java.lang.String r11 = "[Waiting for "
                java.lang.StringBuffer r10 = r10.append(r11)
                java.lang.StringBuffer r10 = r10.append(r9)
                java.lang.String r11 = " milliseconds]"
                java.lang.StringBuffer r10 = r10.append(r11)
                java.lang.String r10 = r10.toString()
                r15.setStatus(r10)
                long r10 = (long) r9
                java.lang.Thread.sleep(r10)     // Catch:{ InterruptedException -> 0x0298 }
            L_0x01b4:
                int r10 = r9 * 2
                int r11 = r8.getStatusCode()
                if (r11 <= r13) goto L_0x01e3
                r11 = 240000(0x3a980, float:3.36312E-40)
            L_0x01bf:
                int r9 = java.lang.Math.min(r10, r11)
            L_0x01c3:
                r10 = 0
                r15.stream = r10
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r8.getMessage()
                r10.debug(r11)
                twitter4j.TwitterStream r10 = r15.this$0
                twitter4j.StreamListener[] r0 = twitter4j.TwitterStream.access$400(r10)
                int r5 = r0.length
                r3 = 0
            L_0x01d9:
                if (r3 >= r5) goto L_0x01e6
                r7 = r0[r3]
                r7.onException(r8)
                int r3 = r3 + 1
                goto L_0x01d9
            L_0x01e3:
                r11 = 16000(0x3e80, float:2.2421E-41)
                goto L_0x01bf
            L_0x01e6:
                r1 = 0
                goto L_0x0005
            L_0x01e9:
                r10 = move-exception
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x01f4:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x0271
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x0204 }
                goto L_0x01f4
            L_0x0204:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x01f4
            L_0x0211:
                r10 = move-exception
                r2 = r10
                r2.printStackTrace()     // Catch:{ all -> 0x0248 }
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()     // Catch:{ all -> 0x0248 }
                java.lang.String r11 = r2.getMessage()     // Catch:{ all -> 0x0248 }
                r10.warn(r11)     // Catch:{ all -> 0x0248 }
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x022b:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x0271
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x023b }
                goto L_0x022b
            L_0x023b:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x022b
            L_0x0248:
                r10 = move-exception
                twitter4j.TwitterStream r11 = r15.this$0
                java.util.List r11 = twitter4j.TwitterStream.access$300(r11)
                java.util.Iterator r3 = r11.iterator()
            L_0x0253:
                boolean r11 = r3.hasNext()
                if (r11 == 0) goto L_0x0270
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onDisconnect()     // Catch:{ Exception -> 0x0263 }
                goto L_0x0253
            L_0x0263:
                r2 = move-exception
                twitter4j.internal.logging.Logger r11 = twitter4j.TwitterStream.access$200()
                java.lang.String r12 = r2.getMessage()
                r11.warn(r12)
                goto L_0x0253
            L_0x0270:
                throw r10
            L_0x0271:
                twitter4j.TwitterStream r10 = r15.this$0
                java.util.List r10 = twitter4j.TwitterStream.access$300(r10)
                java.util.Iterator r3 = r10.iterator()
            L_0x027b:
                boolean r10 = r3.hasNext()
                if (r10 == 0) goto L_0x029b
                java.lang.Object r6 = r3.next()
                twitter4j.ConnectionLifeCycleListener r6 = (twitter4j.ConnectionLifeCycleListener) r6
                r6.onCleanUp()     // Catch:{ Exception -> 0x028b }
                goto L_0x027b
            L_0x028b:
                r2 = move-exception
                twitter4j.internal.logging.Logger r10 = twitter4j.TwitterStream.access$200()
                java.lang.String r11 = r2.getMessage()
                r10.warn(r11)
                goto L_0x027b
            L_0x0298:
                r10 = move-exception
                goto L_0x01b4
            L_0x029b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: twitter4j.TwitterStream.TwitterStreamConsumer.run():void");
        }

        public synchronized void close() {
            setStatus("[Disposing thread]");
            try {
                if (this.stream != null) {
                    this.stream.close();
                }
            } catch (IOException e) {
            } catch (Exception e2) {
                Exception e3 = e2;
                e3.printStackTrace();
                TwitterStream.access$200().warn(e3.getMessage());
            } catch (Throwable th) {
                this.closed = true;
                throw th;
            }
            this.closed = true;
        }

        private void setStatus(String message) {
            String actualMessage = new StringBuffer().append(this.NAME).append(message).toString();
            setName(actualMessage);
            TwitterStream.access$200().debug(actualMessage);
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TwitterStream that = (TwitterStream) o;
        if (shutdown != shutdown) {
            return false;
        }
        if (this.handler == null ? that.handler != null : !this.handler.equals(that.handler)) {
            return false;
        }
        if (this.http == null ? that.http != null : !this.http.equals(that.http)) {
            return false;
        }
        if (this.lifeCycleListeners == null ? that.lifeCycleListeners != null : !this.lifeCycleListeners.equals(that.lifeCycleListeners)) {
            return false;
        }
        if (!Arrays.equals(this.streamListeners, that.streamListeners)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int hashCode = ((super.hashCode() * 31) + (this.http != null ? this.http.hashCode() : 0)) * 31;
        if (this.streamListeners != null) {
            i = Arrays.hashCode(this.streamListeners);
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 31;
        if (this.lifeCycleListeners != null) {
            i2 = this.lifeCycleListeners.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.handler != null) {
            i3 = this.handler.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 31;
        if (shutdown) {
            i4 = 1;
        } else {
            i4 = 0;
        }
        return i7 + i4;
    }
}
