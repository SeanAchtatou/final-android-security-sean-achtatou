package twitter4j;

import java.io.Serializable;
import java.util.Date;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class TweetJSONImpl implements Tweet, Serializable {
    private static final long serialVersionUID = 4299736733993211587L;
    private Annotations annotations = null;
    private Date createdAt;
    private String fromUser;
    private int fromUserId;
    private GeoLocation geoLocation = null;
    private long id;
    private String isoLanguageCode = null;
    private String location;
    private String profileImageUrl;
    private String source;
    private String text;
    private String toUser = null;
    private int toUserId = -1;

    public int compareTo(Object x0) {
        return compareTo((Tweet) x0);
    }

    TweetJSONImpl(JSONObject tweet) throws TwitterException {
        this.text = ParseUtil.getUnescapedString("text", tweet);
        this.toUserId = ParseUtil.getInt("to_user_id", tweet);
        this.toUser = ParseUtil.getRawString("to_user", tweet);
        this.fromUser = ParseUtil.getRawString("from_user", tweet);
        this.id = ParseUtil.getLong("id", tweet);
        this.fromUserId = ParseUtil.getInt("from_user_id", tweet);
        this.isoLanguageCode = ParseUtil.getRawString("iso_language_code", tweet);
        this.source = ParseUtil.getUnescapedString(PropertyConfiguration.SOURCE, tweet);
        this.profileImageUrl = ParseUtil.getUnescapedString("profile_image_url", tweet);
        this.createdAt = ParseUtil.getDate("created_at", tweet, "EEE, dd MMM yyyy HH:mm:ss z");
        this.location = ParseUtil.getRawString("location", tweet);
        this.geoLocation = GeoLocation.getInstance(tweet);
        if (!tweet.isNull("annotations")) {
            try {
                this.annotations = new Annotations(tweet.getJSONArray("annotations"));
            } catch (JSONException e) {
            }
        }
    }

    public int compareTo(Tweet that) {
        long delta = this.id - that.getId();
        if (delta < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        if (delta > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) delta;
    }

    public String getText() {
        return this.text;
    }

    public int getToUserId() {
        return this.toUserId;
    }

    public String getToUser() {
        return this.toUser;
    }

    public String getFromUser() {
        return this.fromUser;
    }

    public long getId() {
        return this.id;
    }

    public int getFromUserId() {
        return this.fromUserId;
    }

    public String getIsoLanguageCode() {
        return this.isoLanguageCode;
    }

    public String getSource() {
        return this.source;
    }

    public String getProfileImageUrl() {
        return this.profileImageUrl;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public GeoLocation getGeoLocation() {
        return this.geoLocation;
    }

    public String getLocation() {
        return this.location;
    }

    public Annotations getAnnotations() {
        return this.annotations;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tweet)) {
            return false;
        }
        return this.id == ((Tweet) o).getId();
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int hashCode = ((((((((((this.text.hashCode() * 31) + this.toUserId) * 31) + (this.toUser != null ? this.toUser.hashCode() : 0)) * 31) + this.fromUser.hashCode()) * 31) + ((int) (this.id ^ (this.id >>> 32)))) * 31) + this.fromUserId) * 31;
        if (this.isoLanguageCode != null) {
            i = this.isoLanguageCode.hashCode();
        } else {
            i = 0;
        }
        int hashCode2 = (((((((hashCode + i) * 31) + this.source.hashCode()) * 31) + this.profileImageUrl.hashCode()) * 31) + this.createdAt.hashCode()) * 31;
        if (this.geoLocation != null) {
            i2 = this.geoLocation.hashCode();
        } else {
            i2 = 0;
        }
        int i4 = (hashCode2 + i2) * 31;
        if (this.annotations != null) {
            i3 = this.annotations.hashCode();
        } else {
            i3 = 0;
        }
        return i4 + i3;
    }

    public String toString() {
        return new StringBuffer().append("TweetJSONImpl{text='").append(this.text).append('\'').append(", toUserId=").append(this.toUserId).append(", toUser='").append(this.toUser).append('\'').append(", fromUser='").append(this.fromUser).append('\'').append(", id=").append(this.id).append(", fromUserId=").append(this.fromUserId).append(", isoLanguageCode='").append(this.isoLanguageCode).append('\'').append(", source='").append(this.source).append('\'').append(", profileImageUrl='").append(this.profileImageUrl).append('\'').append(", createdAt=").append(this.createdAt).append(", geoLocation=").append(this.geoLocation).append(", annotations=").append(this.annotations).append('}').toString();
    }
}
