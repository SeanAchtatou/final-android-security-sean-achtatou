package twitter4j;

import java.io.IOException;

interface StreamImplementation {
    void close() throws IOException;

    void next(StreamListener[] streamListenerArr) throws TwitterException;

    void onException(Exception exc);
}
