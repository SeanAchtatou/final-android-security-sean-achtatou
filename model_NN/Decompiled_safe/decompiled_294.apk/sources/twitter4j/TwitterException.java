package twitter4j;

import com.scoreloop.client.android.ui.component.base.TrackerEvents;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.http.HttpResponseCode;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public class TwitterException extends Exception implements TwitterResponse, HttpResponseCode {
    private static final String[] FILTER = {"twitter4j"};
    private static final long serialVersionUID = -2623309261327598087L;
    private ExceptionDiagnosis exceptionDiagnosis;
    private RateLimitStatus featureSpecificRateLimitStatus;
    boolean nested;
    private RateLimitStatus rateLimitStatus;
    private Map<String, List<String>> responseHeaderFields;
    private int retryAfter;
    private int statusCode;

    public TwitterException(String message) {
        super(decode(message));
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
        this.rateLimitStatus = null;
    }

    public TwitterException(Exception cause) {
        super(decode(cause.getMessage()), cause);
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
        if (cause instanceof TwitterException) {
            ((TwitterException) cause).setNested();
        }
        this.rateLimitStatus = null;
    }

    public TwitterException(String message, HttpResponse res) {
        super(new StringBuffer().append(getCause(res)).append("\n").append(decode(message)).toString());
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
        if (res.getStatusCode() == 420) {
            try {
                String retryAfterStr = res.getResponseHeader("Retry-After");
                if (retryAfterStr != null) {
                    this.retryAfter = Integer.valueOf(retryAfterStr).intValue();
                }
            } catch (NumberFormatException e) {
                this.retryAfter = -1;
            }
        }
        this.responseHeaderFields = res.getResponseHeaderFields();
        this.statusCode = res.getStatusCode();
        this.rateLimitStatus = RateLimitStatusJSONImpl.createFromResponseHeader(res);
        this.featureSpecificRateLimitStatus = RateLimitStatusJSONImpl.createFeatureSpecificRateLimitStatusFromResponseHeader(res);
    }

    public TwitterException(String message, int retryAfter2, Map<String, List<String>> responseHeaderFields2, int statusCode2, RateLimitStatus rateLimitStatus2, RateLimitStatus featureSpecificLateLimitStatus) {
        super(decode(message));
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
        this.retryAfter = retryAfter2;
        this.responseHeaderFields = responseHeaderFields2;
        this.statusCode = statusCode2;
        this.rateLimitStatus = rateLimitStatus2;
        this.featureSpecificRateLimitStatus = featureSpecificLateLimitStatus;
    }

    public TwitterException(String msg, Exception cause) {
        super(decode(msg), cause);
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
    }

    public TwitterException(String msg, Exception cause, int statusCode2) {
        super(decode(msg), cause);
        this.statusCode = -1;
        this.featureSpecificRateLimitStatus = null;
        this.responseHeaderFields = null;
        this.exceptionDiagnosis = null;
        this.nested = false;
        this.statusCode = statusCode2;
    }

    private static String decode(String str) {
        StringBuffer value = new StringBuffer(str.length());
        try {
            JSONObject json = new JSONObject(str);
            if (!json.isNull(TrackerEvents.LABEL_ERROR)) {
                value.append("error - ").append(json.getString(TrackerEvents.LABEL_ERROR)).append("\n");
            }
            if (!json.isNull(TrackerEvents.CAT_REQUEST)) {
                value.append("request - ").append(json.getString(TrackerEvents.CAT_REQUEST)).append("\n");
            }
        } catch (JSONException e) {
            value.append(str);
        }
        return value.toString();
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public String getResponseHeader(String name) {
        if (this.responseHeaderFields == null) {
            return null;
        }
        List<String> header = this.responseHeaderFields.get(name);
        if (header.size() > 0) {
            return header.get(0);
        }
        return null;
    }

    public RateLimitStatus getRateLimitStatus() {
        return this.rateLimitStatus;
    }

    public RateLimitStatus getFeatureSpecificRateLimitStatus() {
        return this.featureSpecificRateLimitStatus;
    }

    public int getRetryAfter() {
        if (this.statusCode == 420) {
            return this.retryAfter;
        }
        throw new IllegalStateException("Rate limitation is not exceeded");
    }

    public boolean isCausedByNetworkIssue() {
        return getCause() instanceof IOException;
    }

    public boolean exceededRateLimitation() {
        return (this.statusCode == 400 && this.rateLimitStatus != null) || this.statusCode == 420;
    }

    public boolean resourceNotFound() {
        return this.statusCode == 404;
    }

    public String getExceptionCode() {
        return getExceptionDiagnosis().asHexString();
    }

    private ExceptionDiagnosis getExceptionDiagnosis() {
        if (this.exceptionDiagnosis == null) {
            this.exceptionDiagnosis = new ExceptionDiagnosis(this, FILTER);
        }
        return this.exceptionDiagnosis;
    }

    /* access modifiers changed from: package-private */
    public void setNested() {
        this.nested = true;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TwitterException that = (TwitterException) o;
        if (this.retryAfter != that.retryAfter) {
            return false;
        }
        if (this.statusCode != that.statusCode) {
            return false;
        }
        return this.rateLimitStatus == null ? that.rateLimitStatus == null : this.rateLimitStatus.equals(that.rateLimitStatus);
    }

    public int hashCode() {
        return (((this.statusCode * 31) + this.retryAfter) * 31) + (this.rateLimitStatus != null ? this.rateLimitStatus.hashCode() : 0);
    }

    public String toString() {
        return new StringBuffer().append(getMessage()).append(this.nested ? "" : new StringBuffer().append("Relevant discussions can be on the Internet at:\n\thttp://www.google.co.jp/search?q=").append(getExceptionDiagnosis().getStackLineHashAsHex()).append(" or\n\thttp://www.google.co.jp/search?q=").append(getExceptionDiagnosis().getLineNumberHashAsHex()).toString()).append("\nTwitterException{").append(this.nested ? "" : new StringBuffer().append("exceptionCode=[").append(getExceptionCode()).append("], ").toString()).append("statusCode=").append(this.statusCode).append(", retryAfter=").append(this.retryAfter).append(", rateLimitStatus=").append(this.rateLimitStatus).append(", version=").append(Version.getVersion()).append('}').toString();
    }

    private static String getCause(HttpResponse res) {
        String cause;
        int statusCode2 = res.getStatusCode();
        switch (statusCode2) {
            case HttpResponseCode.NOT_MODIFIED /*304*/:
                cause = "There was no new data to return.";
                break;
            case HttpResponseCode.BAD_REQUEST /*400*/:
                cause = "The request was invalid.  An accompanying error message will explain why. This is the status code will be returned during rate limiting.";
                break;
            case HttpResponseCode.UNAUTHORIZED /*401*/:
                cause = "Authentication credentials (http://dev.twitter.com/pages/auth) were missing or incorrect. Ensure that you have set valid conumer key/secret, access token/secret, and the system clock in in sync.";
                break;
            case HttpResponseCode.FORBIDDEN /*403*/:
                cause = "The request is understood, but it has been refused. An accompanying error message will explain why. This code is used when requests are being denied due to update limits (http://support.twitter.com/forums/10711/entries/15364).";
                break;
            case HttpResponseCode.NOT_FOUND /*404*/:
                cause = "The URI requested is invalid or the resource requested, such as a user, does not exist.";
                break;
            case HttpResponseCode.NOT_ACCEPTABLE /*406*/:
                cause = "Returned by the Search API when an invalid format is specified in the request.\nReturned by the Streaming API when one or more of the parameters are not suitable for the resource. The track parameter, for example, would throw this error if:\n The track keyword is too long or too short.\n The bounding box specified is invalid.\n No predicates defined for filtered resource, for example, neither track nor follow parameter defined.\n Follow userid cannot be read.";
                break;
            case HttpResponseCode.TOO_LONG /*413*/:
                cause = "A parameter list is too long. The track parameter, for example, would throw this error if:\n Too many track tokens specified for role; contact API team for increased access.\n Too many bounding boxes specified for role; contact API team for increased access.\n Too many follow userids specified for role; contact API team for increased access.";
                break;
            case HttpResponseCode.ENHANCE_YOUR_CLAIM /*420*/:
                cause = "Returned by the Search and Trends API when you are being rate limited (http://dev.twitter.com/pages/rate-limiting).\nReturned by the Streaming API:\n Too many login attempts in a short period of time.\n Running too many copies of the same application authenticating with the same account name.";
                break;
            case HttpResponseCode.INTERNAL_SERVER_ERROR /*500*/:
                cause = "Something is broken. Please post to the group (http://dev.twitter.com/pages/support) so the Twitter team can investigate.";
                break;
            case HttpResponseCode.BAD_GATEWAY /*502*/:
                cause = "Twitter is down or being upgraded.";
                break;
            case HttpResponseCode.SERVICE_UNAVAILABLE /*503*/:
                cause = "The Twitter servers are up, but overloaded with requests. Try again later.";
                break;
            default:
                cause = "";
                break;
        }
        return new StringBuffer().append(statusCode2).append(":").append(cause).toString();
    }
}
