package twitter4j;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public class Annotation implements Comparable<Annotation>, Serializable {
    private static final long serialVersionUID = -6515375141284988754L;
    private Map<String, String> attributes = null;
    private String type = null;

    public int compareTo(Object x0) {
        return compareTo((Annotation) x0);
    }

    public Annotation(String type2) {
        setType(type2);
        setAttributes(null);
    }

    public Annotation(String type2, Map<String, String> attributes2) {
        setType(type2);
        setAttributes(attributes2);
    }

    Annotation(JSONObject jsonObject) {
        String typ = null;
        Map<String, String> attrs = null;
        Iterator it = jsonObject.keys();
        if (it.hasNext()) {
            typ = (String) it.next();
            if (it.hasNext()) {
                this.type = null;
            } else {
                try {
                    JSONObject jo = jsonObject.getJSONObject(typ);
                    Map<String, String> attrs2 = new LinkedHashMap<>();
                    try {
                        Iterator it2 = jo.keys();
                        while (it2.hasNext()) {
                            String key = (String) it2.next();
                            attrs2.put(key, jo.getString(key));
                        }
                        attrs = attrs2;
                    } catch (JSONException e) {
                        typ = null;
                        attrs = null;
                        setType(typ);
                        setAttributes(attrs);
                    }
                } catch (JSONException e2) {
                    typ = null;
                    attrs = null;
                    setType(typ);
                    setAttributes(attrs);
                }
            }
        }
        setType(typ);
        setAttributes(attrs);
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        String str;
        if (type2 == null) {
            str = "";
        } else {
            str = type2;
        }
        this.type = str;
    }

    public Annotation type(String type2) {
        setType(type2);
        return this;
    }

    public Map<String, String> getAttributes() {
        return this.attributes;
    }

    public void setAttributes(Map<String, String> attributes2) {
        Map<String, String> map;
        if (attributes2 == null) {
            map = new LinkedHashMap<>();
        } else {
            map = attributes2;
        }
        this.attributes = map;
    }

    public Annotation attributes(Map<String, String> attributes2) {
        setAttributes(attributes2);
        return this;
    }

    public void addAttribute(String name, String value) {
        this.attributes.put(name, value);
    }

    public Annotation attribute(String name, String value) {
        addAttribute(name, value);
        return this;
    }

    public boolean isEmpty() {
        return this.attributes.isEmpty();
    }

    public Integer size() {
        return new Integer(this.attributes.size());
    }

    /* access modifiers changed from: package-private */
    public String asParameterValue() {
        return asJSONObject().toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.internal.org.json.JSONObject.put(java.lang.String, java.util.Map):twitter4j.internal.org.json.JSONObject
     arg types: [java.lang.String, java.util.Map<java.lang.String, java.lang.String>]
     candidates:
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, double):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, int):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, long):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, java.lang.Object):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, java.util.Collection):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, boolean):twitter4j.internal.org.json.JSONObject
      twitter4j.internal.org.json.JSONObject.put(java.lang.String, java.util.Map):twitter4j.internal.org.json.JSONObject */
    /* access modifiers changed from: package-private */
    public JSONObject asJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(this.type, (Map) this.attributes);
        } catch (JSONException e) {
        }
        return jsonObject;
    }

    public int compareTo(Annotation other) {
        if (other == null) {
            return 1;
        }
        if (this == other) {
            return 0;
        }
        int result = getType().compareTo(other.getType());
        if (result != 0) {
            return result;
        }
        int result2 = size().compareTo(other.size());
        if (result2 != 0) {
            return result2;
        }
        Iterator<String> otherNamesIt = other.sortedNames().iterator();
        for (String thisName : sortedNames()) {
            String otherName = otherNamesIt.next();
            int result3 = thisName.compareTo(otherName);
            if (result3 != 0) {
                return result3;
            }
            int result4 = getAttributes().get(thisName).compareTo(other.getAttributes().get(otherName));
            if (result4 != 0) {
                return result4;
            }
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Annotation)) {
            return false;
        }
        Annotation other = (Annotation) obj;
        return getType().equals(other.getType()) && getAttributes().equals(other.getAttributes());
    }

    public int hashCode() {
        return (this.type.hashCode() * 31) + this.attributes.hashCode();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("Annotation{type='");
        sb.append(this.type).append("', attributes={");
        Iterator<String> nameIt = this.attributes.keySet().iterator();
        while (nameIt.hasNext()) {
            String name = nameIt.next();
            sb.append('\'').append(name).append("'='").append(this.attributes.get(name)).append('\'');
            if (nameIt.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("}}");
        return sb.toString();
    }

    private SortedSet<String> sortedNames() {
        SortedSet<String> names = new TreeSet<>();
        names.addAll(getAttributes().keySet());
        return names;
    }
}
