package twitter4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import twitter4j.internal.http.HttpParameter;

public final class StatusUpdate implements Serializable {
    private static final long serialVersionUID = -3595502688477609916L;
    private Annotations annotations = null;
    private boolean displayCoordinates = true;
    private long inReplyToStatusId = -1;
    private GeoLocation location = null;
    private String placeId = null;
    private String status;

    public StatusUpdate(String status2) {
        this.status = status2;
    }

    public String getStatus() {
        return this.status;
    }

    public long getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public void setInReplyToStatusId(long inReplyToStatusId2) {
        this.inReplyToStatusId = inReplyToStatusId2;
    }

    public StatusUpdate inReplyToStatusId(long inReplyToStatusId2) {
        setInReplyToStatusId(inReplyToStatusId2);
        return this;
    }

    public GeoLocation getLocation() {
        return this.location;
    }

    public void setLocation(GeoLocation location2) {
        this.location = location2;
    }

    public StatusUpdate location(GeoLocation location2) {
        setLocation(location2);
        return this;
    }

    public String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(String placeId2) {
        this.placeId = placeId2;
    }

    public StatusUpdate placeId(String placeId2) {
        setPlaceId(placeId2);
        return this;
    }

    public boolean isDisplayCoordinates() {
        return this.displayCoordinates;
    }

    public void setDisplayCoordinates(boolean displayCoordinates2) {
        this.displayCoordinates = displayCoordinates2;
    }

    public StatusUpdate displayCoordinates(boolean displayCoordinates2) {
        setDisplayCoordinates(displayCoordinates2);
        return this;
    }

    public Annotations getAnnotations() {
        return this.annotations;
    }

    public void setAnnotations(Annotations annotations2) {
        this.annotations = annotations2;
    }

    public StatusUpdate annotations(Annotations annotations2) {
        setAnnotations(annotations2);
        return this;
    }

    public void addAnnotation(Annotation annotation) {
        if (this.annotations == null) {
            this.annotations = new Annotations();
        }
        this.annotations.addAnnotation(annotation);
    }

    public StatusUpdate annotation(Annotation annotation) {
        addAnnotation(annotation);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.StatusUpdate.appendParameter(java.lang.String, long, java.util.List<twitter4j.internal.http.HttpParameter>):void
     arg types: [java.lang.String, long, java.util.ArrayList<twitter4j.internal.http.HttpParameter>]
     candidates:
      twitter4j.StatusUpdate.appendParameter(java.lang.String, double, java.util.List<twitter4j.internal.http.HttpParameter>):void
      twitter4j.StatusUpdate.appendParameter(java.lang.String, java.lang.String, java.util.List<twitter4j.internal.http.HttpParameter>):void
      twitter4j.StatusUpdate.appendParameter(java.lang.String, long, java.util.List<twitter4j.internal.http.HttpParameter>):void */
    /* access modifiers changed from: package-private */
    public HttpParameter[] asHttpParameterArray() {
        ArrayList<HttpParameter> params = new ArrayList<>();
        appendParameter("status", this.status, params);
        if (-1 != this.inReplyToStatusId) {
            appendParameter("in_reply_to_status_id", this.inReplyToStatusId, (List<HttpParameter>) params);
        }
        if (this.location != null) {
            appendParameter("lat", this.location.getLatitude(), params);
            appendParameter("long", this.location.getLongitude(), params);
        }
        appendParameter("place_id", this.placeId, params);
        if (!this.displayCoordinates) {
            appendParameter("display_coordinates", "false", params);
        }
        if (this.annotations != null && !this.annotations.isEmpty()) {
            appendParameter("annotations", this.annotations.asParameterValue(), params);
        }
        return (HttpParameter[]) params.toArray(new HttpParameter[params.size()]);
    }

    private void appendParameter(String name, String value, List<HttpParameter> params) {
        if (value != null) {
            params.add(new HttpParameter(name, value));
        }
    }

    private void appendParameter(String name, double value, List<HttpParameter> params) {
        params.add(new HttpParameter(name, String.valueOf(value)));
    }

    private void appendParameter(String name, long value, List<HttpParameter> params) {
        params.add(new HttpParameter(name, String.valueOf(value)));
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StatusUpdate that = (StatusUpdate) o;
        if (this.displayCoordinates != that.displayCoordinates) {
            return false;
        }
        if (this.inReplyToStatusId != that.inReplyToStatusId) {
            return false;
        }
        if (this.location == null ? that.location != null : !this.location.equals(that.location)) {
            return false;
        }
        if (this.placeId == null ? that.placeId != null : !this.placeId.equals(that.placeId)) {
            return false;
        }
        if (this.annotations == null ? that.annotations != null : !this.annotations.equals(that.annotations)) {
            return false;
        }
        return this.status.equals(that.status);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int hashCode = ((this.status.hashCode() * 31) + ((int) (this.inReplyToStatusId ^ (this.inReplyToStatusId >>> 32)))) * 31;
        if (this.location != null) {
            i = this.location.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 31;
        if (this.placeId != null) {
            i2 = this.placeId.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.displayCoordinates) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 31;
        if (this.annotations != null) {
            i4 = this.annotations.hashCode();
        } else {
            i4 = 0;
        }
        return i7 + i4;
    }

    public String toString() {
        return new StringBuffer().append("StatusUpdate{status='").append(this.status).append('\'').append(", inReplyToStatusId=").append(this.inReplyToStatusId).append(", location=").append(this.location).append(", placeId='").append(this.placeId).append('\'').append(", displayCoordinates=").append(this.displayCoordinates).append(", annotations=").append(this.annotations).append('}').toString();
    }
}
