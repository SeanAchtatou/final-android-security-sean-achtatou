package twitter4j.api;

import twitter4j.IDs;
import twitter4j.TwitterException;

public interface FriendsFollowersMethods {
    IDs getFollowersIDs() throws TwitterException;

    IDs getFollowersIDs(int i) throws TwitterException;

    IDs getFollowersIDs(int i, long j) throws TwitterException;

    IDs getFollowersIDs(long j) throws TwitterException;

    IDs getFollowersIDs(String str) throws TwitterException;

    IDs getFollowersIDs(String str, long j) throws TwitterException;

    IDs getFriendsIDs() throws TwitterException;

    IDs getFriendsIDs(int i) throws TwitterException;

    IDs getFriendsIDs(int i, long j) throws TwitterException;

    IDs getFriendsIDs(long j) throws TwitterException;

    IDs getFriendsIDs(String str) throws TwitterException;

    IDs getFriendsIDs(String str, long j) throws TwitterException;
}
