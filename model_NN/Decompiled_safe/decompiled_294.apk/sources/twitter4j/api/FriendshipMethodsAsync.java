package twitter4j.api;

public interface FriendshipMethodsAsync {
    void createFriendship(int i);

    void createFriendship(int i, boolean z);

    void createFriendship(String str);

    void createFriendship(String str, boolean z);

    void destroyFriendship(int i);

    void destroyFriendship(String str);

    void existsFriendship(String str, String str2);

    void getIncomingFriendships(long j);

    void getOutgoingFriendships(long j);

    void lookupFriendships(int[] iArr);

    void lookupFriendships(String[] strArr);

    void showFriendship(int i, int i2);

    void showFriendship(String str, String str2);

    void updateFriendship(int i, boolean z, boolean z2);

    void updateFriendship(String str, boolean z, boolean z2);
}
