package twitter4j.api;

public interface SpamReportingMethodsAsync {
    void reportSpam(int i);

    void reportSpam(String str);
}
