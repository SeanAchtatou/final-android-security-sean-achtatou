package twitter4j.api;

import twitter4j.TwitterException;
import twitter4j.User;

public interface NotificationMethods {
    User disableNotification(int i) throws TwitterException;

    User disableNotification(String str) throws TwitterException;

    User enableNotification(int i) throws TwitterException;

    User enableNotification(String str) throws TwitterException;
}
