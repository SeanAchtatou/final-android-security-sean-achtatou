package twitter4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class QueryResultJSONImpl implements QueryResult, Serializable {
    private static final long serialVersionUID = -9059136565234613286L;
    private double completedIn;
    private long maxId;
    private int page;
    private String query;
    private String refreshUrl;
    private int resultsPerPage;
    private long sinceId;
    private List<Tweet> tweets;
    private String warning;

    QueryResultJSONImpl(HttpResponse res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        try {
            this.sinceId = ParseUtil.getLong("since_id", json);
            this.maxId = ParseUtil.getLong("max_id", json);
            this.refreshUrl = ParseUtil.getUnescapedString("refresh_url", json);
            this.resultsPerPage = ParseUtil.getInt("results_per_page", json);
            this.warning = ParseUtil.getRawString("warning", json);
            this.completedIn = ParseUtil.getDouble("completed_in", json);
            this.page = ParseUtil.getInt("page", json);
            this.query = ParseUtil.getURLDecodedString("query", json);
            JSONArray array = json.getJSONArray("results");
            this.tweets = new ArrayList(array.length());
            for (int i = 0; i < array.length(); i++) {
                this.tweets.add(new TweetJSONImpl(array.getJSONObject(i)));
            }
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    QueryResultJSONImpl(Query query2) {
        this.sinceId = query2.getSinceId();
        this.resultsPerPage = query2.getRpp();
        this.page = query2.getPage();
        this.tweets = new ArrayList(0);
    }

    public long getSinceId() {
        return this.sinceId;
    }

    public long getMaxId() {
        return this.maxId;
    }

    public String getRefreshUrl() {
        return this.refreshUrl;
    }

    public int getResultsPerPage() {
        return this.resultsPerPage;
    }

    public String getWarning() {
        return this.warning;
    }

    public double getCompletedIn() {
        return this.completedIn;
    }

    public int getPage() {
        return this.page;
    }

    public String getQuery() {
        return this.query;
    }

    public List<Tweet> getTweets() {
        return this.tweets;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QueryResult that = (QueryResult) o;
        if (Double.compare(that.getCompletedIn(), this.completedIn) != 0) {
            return false;
        }
        if (this.maxId != that.getMaxId()) {
            return false;
        }
        if (this.page != that.getPage()) {
            return false;
        }
        if (this.resultsPerPage != that.getResultsPerPage()) {
            return false;
        }
        if (this.sinceId != that.getSinceId()) {
            return false;
        }
        if (!this.query.equals(that.getQuery())) {
            return false;
        }
        if (this.refreshUrl == null ? that.getRefreshUrl() != null : !this.refreshUrl.equals(that.getRefreshUrl())) {
            return false;
        }
        if (this.tweets == null ? that.getTweets() != null : !this.tweets.equals(that.getTweets())) {
            return false;
        }
        return this.warning == null ? that.getWarning() == null : this.warning.equals(that.getWarning());
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = ((((int) (this.sinceId ^ (this.sinceId >>> 32))) * 31) + ((int) (this.maxId ^ (this.maxId >>> 32)))) * 31;
        if (this.refreshUrl != null) {
            i = this.refreshUrl.hashCode();
        } else {
            i = 0;
        }
        int i5 = (((i4 + i) * 31) + this.resultsPerPage) * 31;
        if (this.warning != null) {
            i2 = this.warning.hashCode();
        } else {
            i2 = 0;
        }
        int result = i5 + i2;
        long temp = this.completedIn != 0.0d ? Double.doubleToLongBits(this.completedIn) : 0;
        int hashCode = ((((((result * 31) + ((int) ((temp >>> 32) ^ temp))) * 31) + this.page) * 31) + this.query.hashCode()) * 31;
        if (this.tweets != null) {
            i3 = this.tweets.hashCode();
        } else {
            i3 = 0;
        }
        return hashCode + i3;
    }

    public String toString() {
        return new StringBuffer().append("QueryResultJSONImpl{sinceId=").append(this.sinceId).append(", maxId=").append(this.maxId).append(", refreshUrl='").append(this.refreshUrl).append('\'').append(", resultsPerPage=").append(this.resultsPerPage).append(", warning='").append(this.warning).append('\'').append(", completedIn=").append(this.completedIn).append(", page=").append(this.page).append(", query='").append(this.query).append('\'').append(", tweets=").append(this.tweets).append('}').toString();
    }
}
