package twitter4j.internal.http;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import twitter4j.internal.logging.Logger;

public final class HttpClientFactory {
    private static final Constructor HTTP_CLIENT_CONSTRUCTOR;
    static Class class$twitter4j$internal$http$HttpClientConfiguration;
    static Class class$twitter4j$internal$http$HttpClientFactory;
    private static final Logger logger;

    static {
        Class cls;
        Class cls2;
        if (class$twitter4j$internal$http$HttpClientFactory == null) {
            cls = class$("twitter4j.internal.http.HttpClientFactory");
            class$twitter4j$internal$http$HttpClientFactory = cls;
        } else {
            cls = class$twitter4j$internal$http$HttpClientFactory;
        }
        logger = Logger.getLogger(cls);
        Class clazz = null;
        try {
            clazz = Class.forName("twitter4j.internal.http.alternative.HttpClientImpl");
        } catch (ClassNotFoundException e) {
        }
        if (clazz == null) {
            try {
                clazz = Class.forName("twitter4j.internal.http.HttpClientImpl");
            } catch (ClassNotFoundException e2) {
                throw new AssertionError(e2);
            }
        }
        logger.debug(new StringBuffer().append("Will use ").append(clazz.getName()).append(" as HttpClient implementation.").toString());
        try {
            Class[] clsArr = new Class[1];
            if (class$twitter4j$internal$http$HttpClientConfiguration == null) {
                cls2 = class$("twitter4j.internal.http.HttpClientConfiguration");
                class$twitter4j$internal$http$HttpClientConfiguration = cls2;
            } else {
                cls2 = class$twitter4j$internal$http$HttpClientConfiguration;
            }
            clsArr[0] = cls2;
            HTTP_CLIENT_CONSTRUCTOR = clazz.getConstructor(clsArr);
        } catch (NoSuchMethodException e3) {
            throw new AssertionError(e3);
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public static HttpClient getInstance(HttpClientConfiguration conf) {
        try {
            return (HttpClient) HTTP_CLIENT_CONSTRUCTOR.newInstance(conf);
        } catch (InstantiationException e) {
            throw new AssertionError(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new AssertionError(e3);
        }
    }
}
