package twitter4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.util.StringUtil;

public final class FilterQuery implements Serializable {
    private static final long serialVersionUID = 430966623248982833L;
    private int count;
    private int[] follow;
    private boolean includeEntities;
    private double[][] locations;
    private String[] track;

    public FilterQuery() {
        this.count = 0;
        this.follow = null;
        this.track = null;
        this.locations = null;
    }

    public FilterQuery(int[] follow2) {
        this();
        this.count = 0;
        this.follow = follow2;
    }

    public FilterQuery(int count2, int[] follow2) {
        this();
        this.count = count2;
        this.follow = follow2;
    }

    public FilterQuery(int count2, int[] follow2, String[] track2) {
        this();
        this.count = count2;
        this.follow = follow2;
        this.track = track2;
    }

    public FilterQuery(int count2, int[] follow2, String[] track2, double[][] locations2) {
        this.count = count2;
        this.follow = follow2;
        this.track = track2;
        this.locations = locations2;
    }

    public FilterQuery count(int count2) {
        this.count = count2;
        return this;
    }

    public FilterQuery follow(int[] follow2) {
        this.follow = follow2;
        return this;
    }

    public FilterQuery track(String[] track2) {
        this.track = track2;
        return this;
    }

    public FilterQuery locations(double[][] locations2) {
        this.locations = locations2;
        return this;
    }

    public FilterQuery setIncludeEntities(boolean include) {
        this.includeEntities = include;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: twitter4j.internal.http.HttpParameter.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, double):void
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, int):void
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, long):void
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, java.io.File):void
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, java.lang.String):void
      twitter4j.internal.http.HttpParameter.<init>(java.lang.String, boolean):void */
    /* access modifiers changed from: package-private */
    public HttpParameter[] asHttpParameterArray() {
        ArrayList<HttpParameter> params = new ArrayList<>();
        params.add(new HttpParameter("count", this.count));
        if (this.follow != null && this.follow.length > 0) {
            params.add(new HttpParameter("follow", StringUtil.join(this.follow)));
        }
        if (this.track != null && this.track.length > 0) {
            params.add(new HttpParameter("track", StringUtil.join(this.track)));
        }
        if (this.locations != null && this.locations.length > 0) {
            params.add(new HttpParameter("locations", toLocationsString(this.locations)));
        }
        if (this.includeEntities) {
            params.add(new HttpParameter("include_entities", true));
        }
        return (HttpParameter[]) params.toArray(new HttpParameter[params.size()]);
    }

    private String toLocationsString(double[][] keywords) {
        StringBuffer buf = new StringBuffer(keywords.length * 20 * 2);
        for (int c = 0; c < keywords.length; c++) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(keywords[c][0]);
            buf.append(",");
            buf.append(keywords[c][1]);
        }
        return buf.toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FilterQuery that = (FilterQuery) o;
        if (this.count != that.count) {
            return false;
        }
        if (!Arrays.equals(this.follow, that.follow)) {
            return false;
        }
        return Arrays.equals(this.track, that.track);
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = this.count * 31;
        if (this.follow != null) {
            i = Arrays.hashCode(this.follow);
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.track != null) {
            i2 = Arrays.hashCode(this.track);
        } else {
            i2 = 0;
        }
        return i4 + i2;
    }

    public String toString() {
        return new StringBuffer().append("FilterQuery{count=").append(this.count).append(", follow=").append(this.follow).append(", track=").append(this.track == null ? null : Arrays.asList(this.track)).append(", locations=").append(this.locations == null ? null : Arrays.asList(this.locations)).append('}').toString();
    }
}
