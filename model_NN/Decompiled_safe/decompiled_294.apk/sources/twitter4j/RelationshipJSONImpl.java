package twitter4j;

import java.io.Serializable;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

class RelationshipJSONImpl extends TwitterResponseImpl implements Relationship, Serializable {
    private static final long serialVersionUID = 697705345506281849L;
    private final boolean sourceBlockingTarget;
    private final boolean sourceFollowedByTarget;
    private final boolean sourceFollowingTarget;
    private final boolean sourceNotificationsEnabled;
    private final int sourceUserId;
    private final String sourceUserScreenName;
    private final int targetUserId;
    private final String targetUserScreenName;

    RelationshipJSONImpl(HttpResponse res) throws TwitterException {
        this(res, res.asJSONObject());
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, res.asJSONObject());
    }

    RelationshipJSONImpl(JSONObject json) throws TwitterException {
        this(null, json);
    }

    RelationshipJSONImpl(HttpResponse res, JSONObject json) throws TwitterException {
        super(res);
        try {
            JSONObject relationship = json.getJSONObject("relationship");
            JSONObject sourceJson = relationship.getJSONObject(PropertyConfiguration.SOURCE);
            JSONObject targetJson = relationship.getJSONObject("target");
            this.sourceUserId = ParseUtil.getInt("id", sourceJson);
            this.targetUserId = ParseUtil.getInt("id", targetJson);
            this.sourceUserScreenName = ParseUtil.getUnescapedString("screen_name", sourceJson);
            this.targetUserScreenName = ParseUtil.getUnescapedString("screen_name", targetJson);
            this.sourceBlockingTarget = ParseUtil.getBoolean("blocking", sourceJson);
            this.sourceFollowingTarget = ParseUtil.getBoolean("following", sourceJson);
            this.sourceFollowedByTarget = ParseUtil.getBoolean("followed_by", sourceJson);
            this.sourceNotificationsEnabled = ParseUtil.getBoolean("notifications_enabled", sourceJson);
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    static ResponseList<Relationship> createRelationshipList(HttpResponse res) throws TwitterException {
        try {
            DataObjectFactoryUtil.clearThreadLocalMap();
            JSONArray list = res.asJSONArray();
            int size = list.length();
            ResponseList<Relationship> relationships = new ResponseListImpl<>(size, res);
            for (int i = 0; i < size; i++) {
                JSONObject json = list.getJSONObject(i);
                Relationship relationship = new RelationshipJSONImpl(json);
                DataObjectFactoryUtil.registerJSONObject(relationship, json);
                relationships.add(relationship);
            }
            DataObjectFactoryUtil.registerJSONObject(relationships, list);
            return relationships;
        } catch (JSONException e) {
            throw new TwitterException(e);
        } catch (TwitterException e2) {
            throw e2;
        }
    }

    public int getSourceUserId() {
        return this.sourceUserId;
    }

    public int getTargetUserId() {
        return this.targetUserId;
    }

    public boolean isSourceBlockingTarget() {
        return this.sourceBlockingTarget;
    }

    public String getSourceUserScreenName() {
        return this.sourceUserScreenName;
    }

    public String getTargetUserScreenName() {
        return this.targetUserScreenName;
    }

    public boolean isSourceFollowingTarget() {
        return this.sourceFollowingTarget;
    }

    public boolean isTargetFollowingSource() {
        return this.sourceFollowedByTarget;
    }

    public boolean isSourceFollowedByTarget() {
        return this.sourceFollowedByTarget;
    }

    public boolean isTargetFollowedBySource() {
        return this.sourceFollowingTarget;
    }

    public boolean isSourceNotificationsEnabled() {
        return this.sourceNotificationsEnabled;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Relationship)) {
            return false;
        }
        Relationship that = (Relationship) o;
        if (this.sourceUserId != that.getSourceUserId()) {
            return false;
        }
        if (this.targetUserId != that.getTargetUserId()) {
            return false;
        }
        if (!this.sourceUserScreenName.equals(that.getSourceUserScreenName())) {
            return false;
        }
        if (!this.targetUserScreenName.equals(that.getTargetUserScreenName())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((this.sourceUserId * 31) + this.targetUserId) * 31) + this.sourceUserScreenName.hashCode()) * 31) + this.targetUserScreenName.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("RelationshipJSONImpl{sourceUserId=").append(this.sourceUserId).append(", targetUserId=").append(this.targetUserId).append(", sourceUserScreenName='").append(this.sourceUserScreenName).append('\'').append(", targetUserScreenName='").append(this.targetUserScreenName).append('\'').append(", sourceFollowingTarget=").append(this.sourceFollowingTarget).append(", sourceFollowedByTarget=").append(this.sourceFollowedByTarget).append(", sourceNotificationsEnabled=").append(this.sourceNotificationsEnabled).append('}').toString();
    }
}
