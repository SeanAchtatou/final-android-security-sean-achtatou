package twitter4j;

import java.util.List;

public class TwitterAdapter implements TwitterListener {
    public void searched(QueryResult result) {
    }

    public void gotTrends(Trends trends) {
    }

    public void gotCurrentTrends(Trends trends) {
    }

    public void gotDailyTrends(List<Trends> list) {
    }

    public void gotWeeklyTrends(List<Trends> list) {
    }

    public void gotPublicTimeline(ResponseList<Status> responseList) {
    }

    public void gotHomeTimeline(ResponseList<Status> responseList) {
    }

    public void gotFriendsTimeline(ResponseList<Status> responseList) {
    }

    public void gotUserTimeline(ResponseList<Status> responseList) {
    }

    public void gotMentions(ResponseList<Status> responseList) {
    }

    public void gotRetweetedByMe(ResponseList<Status> responseList) {
    }

    public void gotRetweetedToMe(ResponseList<Status> responseList) {
    }

    public void gotRetweetsOfMe(ResponseList<Status> responseList) {
    }

    public void gotRetweetedByUser(ResponseList<Status> responseList) {
    }

    public void gotRetweetedToUser(ResponseList<Status> responseList) {
    }

    public void gotShowStatus(Status statuses) {
    }

    public void updatedStatus(Status statuses) {
    }

    public void destroyedStatus(Status destroyedStatus) {
    }

    public void retweetedStatus(Status retweetedStatus) {
    }

    public void gotRetweets(ResponseList<Status> responseList) {
    }

    public void gotRetweetedBy(ResponseList<User> responseList) {
    }

    public void gotRetweetedByIDs(IDs ids) {
    }

    public void gotUserDetail(User user) {
    }

    public void lookedupUsers(ResponseList<User> responseList) {
    }

    public void searchedUser(ResponseList<User> responseList) {
    }

    public void gotSuggestedUserCategories(ResponseList<Category> responseList) {
    }

    public void gotUserSuggestions(ResponseList<User> responseList) {
    }

    public void gotMemberSuggestions(ResponseList<User> responseList) {
    }

    public void gotProfileImage(ProfileImage image) {
    }

    public void gotFriendsStatuses(PagableResponseList<User> pagableResponseList) {
    }

    public void gotFollowersStatuses(PagableResponseList<User> pagableResponseList) {
    }

    public void createdUserList(UserList userList) {
    }

    public void updatedUserList(UserList userList) {
    }

    public void gotUserLists(PagableResponseList<UserList> pagableResponseList) {
    }

    public void gotShowUserList(UserList userList) {
    }

    public void destroyedUserList(UserList userList) {
    }

    public void gotUserListStatuses(ResponseList<Status> responseList) {
    }

    public void gotUserListMemberships(PagableResponseList<UserList> pagableResponseList) {
    }

    public void gotUserListSubscriptions(PagableResponseList<UserList> pagableResponseList) {
    }

    public void gotAllUserLists(ResponseList<UserList> responseList) {
    }

    public void gotUserListMembers(PagableResponseList<User> pagableResponseList) {
    }

    public void addedUserListMember(UserList userList) {
    }

    public void addedUserListMembers(UserList userList) {
    }

    public void deletedUserListMember(UserList userList) {
    }

    public void checkedUserListMembership(User user) {
    }

    public void gotUserListSubscribers(PagableResponseList<User> pagableResponseList) {
    }

    public void subscribedUserList(UserList userList) {
    }

    public void unsubscribedUserList(UserList userList) {
    }

    public void checkedUserListSubscription(User user) {
    }

    public void gotDirectMessages(ResponseList<DirectMessage> responseList) {
    }

    public void gotSentDirectMessages(ResponseList<DirectMessage> responseList) {
    }

    public void sentDirectMessage(DirectMessage message) {
    }

    public void destroyedDirectMessage(DirectMessage message) {
    }

    public void gotDirectMessage(DirectMessage message) {
    }

    public void createdFriendship(User user) {
    }

    public void destroyedFriendship(User user) {
    }

    public void gotExistsFriendship(boolean exists) {
    }

    public void gotShowFriendship(Relationship relationship) {
    }

    public void gotIncomingFriendships(IDs ids) {
    }

    public void gotOutgoingFriendships(IDs ids) {
    }

    public void lookedUpFriendships(ResponseList<Friendship> responseList) {
    }

    public void updatedFriendship(Relationship relationship) {
    }

    public void gotFriendsIDs(IDs ids) {
    }

    public void gotFollowersIDs(IDs ids) {
    }

    public void verifiedCredentials(User user) {
    }

    public void gotRateLimitStatus(RateLimitStatus status) {
    }

    public void updatedProfileColors(User user) {
    }

    public void gotAccountTotals(AccountTotals totals) {
    }

    public void gotAccountSettings(AccountSettings settings) {
    }

    public void updatedProfileImage(User user) {
    }

    public void updatedProfileBackgroundImage(User user) {
    }

    public void updatedProfile(User user) {
    }

    public void gotFavorites(ResponseList<Status> responseList) {
    }

    public void createdFavorite(Status status) {
    }

    public void destroyedFavorite(Status status) {
    }

    public void enabledNotification(User user) {
    }

    public void disabledNotification(User user) {
    }

    public void createdBlock(User user) {
    }

    public void destroyedBlock(User user) {
    }

    public void gotExistsBlock(boolean blockExists) {
    }

    public void gotBlockingUsers(ResponseList<User> responseList) {
    }

    public void gotBlockingUsersIDs(IDs blockingUsersIDs) {
    }

    public void reportedSpam(User reportedSpammer) {
    }

    public void gotAvailableTrends(ResponseList<Location> responseList) {
    }

    public void gotLocationTrends(Trends trends) {
    }

    public void searchedPlaces(ResponseList<Place> responseList) {
    }

    public void gotSimilarPlaces(SimilarPlaces places) {
    }

    public void gotNearByPlaces(ResponseList<Place> responseList) {
    }

    public void gotReverseGeoCode(ResponseList<Place> responseList) {
    }

    public void gotGeoDetails(Place place) {
    }

    public void createdPlace(Place place) {
    }

    public void gotTermsOfService(String tof) {
    }

    public void gotPrivacyPolicy(String privacyPolicy) {
    }

    public void gotRelatedResults(RelatedResults relatedResults) {
    }

    public void tested(boolean test) {
    }

    public void onException(TwitterException ex, TwitterMethod method) {
    }
}
