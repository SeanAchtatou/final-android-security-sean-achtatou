package com.celliecraze.marblemadness.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.util.Log;
import com.celliecraze.marblemadness.FrozenBubble;

public class BubbleBusterConstants {
    public static final String ANONYMOUS = "Anonymous";
    public static final int EXTENDED_GAMEFIELD_WIDTH = 640;
    public static final int FALSE = 0;
    public static final int GAMEFIELD_HEIGHT = 480;
    public static final int GAMEFIELD_WIDTH = 320;
    public static final int LEVELS_IN_PACK = 600;
    public static final int MENU_ABOUT = 11;
    public static final int MENU_BUDDYLIST = 4;
    public static final int MENU_EDITOR = 2;
    public static final int MENU_EXIT = 3;
    public static final int MENU_HELP = 1;
    public static final int MENU_HSRESET = 9;
    public static final int MENU_LEADERBOARDS = 10;
    public static final int MENU_PREFERENCES = 8;
    public static final int MENU_SCORELOOP = 7;
    public static final int MENU_SELECTOR = 5;
    public static final int MENU_SHARE = 6;
    public static final int MSG_LEVEL_FINISH = 300;
    public static final int MSG_LEVEL_FINISH_OK = 301;
    public static final int NUM_SOUNDS = 10;
    public static final int PACK_STEP = 100;
    public static final String PLAYER = "player";
    public static final int SOUND_DESTROY = 3;
    public static final int SOUND_HURRY = 6;
    public static final int SOUND_LAUNCH = 2;
    public static final int SOUND_LOST = 1;
    public static final int SOUND_NEWROOT = 7;
    public static final int SOUND_NOH = 8;
    public static final int SOUND_REBOUND = 4;
    public static final int SOUND_STICK = 5;
    public static final int SOUND_SWAP = 9;
    public static final int SOUND_WON = 0;
    public static final String SUBMIT = "submit";
    public static final String TAG = "MarbleMadness";
    public static final int TRUE = 1;
    private static final boolean active = true;
    public static final int longVibe = 175;
    public static final int shortVibe = 50;
    public static Vibrator v = null;

    private BubbleBusterConstants() {
    }

    public static void vibrate(Context ctx, int t) {
        if (FrozenBubble.vibrationKey) {
            if (v == null) {
                v = (Vibrator) ctx.getSystemService("vibrator");
            }
            v.vibrate((long) t);
        }
    }

    public static boolean isNetworkAvailable(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService("connectivity");
        if (connectivity == null) {
            return false;
        }
        NetworkInfo[] info = connectivity.getAllNetworkInfo();
        if (info != null) {
            for (NetworkInfo state : info) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return active;
                }
            }
        }
        return false;
    }

    public static void d(Object msg) {
        Log.d(TAG, msg.toString());
    }

    public static void e(Object ctx, Exception e) {
        Log.e(TAG, String.valueOf(ctx.getClass().getName()) + ": " + e.getMessage());
    }

    public static void e(Exception e) {
        Log.e(TAG, e.getMessage());
    }

    public static void e(Object e) {
        Log.e(TAG, e.toString());
    }
}
