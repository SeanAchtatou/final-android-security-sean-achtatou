package com.celliecraze.marblemadness;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.celliecraze.marblemadness.GameView;
import com.celliecraze.marblemadness.activity.BubbleBusterAboutActivity;
import com.celliecraze.marblemadness.activity.BubbleBusterHowToActivity;
import com.celliecraze.marblemadness.activity.BubbleBusterOverviewActivity;
import com.celliecraze.marblemadness.activity.BubbleBusterPreferencesActivity;
import com.celliecraze.marblemadness.activity.BubbleBusterSplashActivity;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.helper.ErrorReporter;
import com.celliecraze.marblemadness.highscores.HighscoreManager;
import com.scoreloop.client.android.ui.BuddiesScreenActivity;
import com.scoreloop.client.android.ui.EntryScreenActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.io.File;

public class FrozenBubble extends Activity implements AccelerometerListener, AdWhirlLayout.AdWhirlInterface {
    public static final String EDITORACTION = "com.celliecraze.marblemadness.GAME";
    public static final int GAME_ACCELEROMETER = 2;
    public static final int GAME_AUTO = 0;
    public static final int GAME_MANUAL = 1;
    private static final int MSG_SHOW_PAUSED = 500;
    private static final int REQ_SELECTOR = 300;
    private static final int REQ_SETTINGS = 302;
    private static final int SHOW_PAUSED = 501;
    public static boolean bubbleBlindKey;
    public static boolean dontRushMeKey;
    public static boolean fullScreenActive = true;
    public static boolean fullScreenSwitch = false;
    public static int gameModeKey;
    public static boolean gamePausedActive = false;
    public static boolean invertAccelKey;
    static GameView.GameThread mGameThread;
    public static boolean prefPause = false;
    public static SharedPreferences prefs;
    public static boolean soundKey;
    public static boolean swapBubblesKey;
    public static boolean vibrationKey;
    private boolean activityCustomStarted = false;
    AdWhirlLayout adWhirlLayout;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            FrozenBubble.this.adWhirlLayout = (AdWhirlLayout) FrozenBubble.this.findViewById(R.id.adwhirl_layout);
            switch (msg.what) {
                case 0:
                    FrozenBubble.this.adWhirlLayout.setVisibility(0);
                    return;
                case 1:
                    FrozenBubble.this.adWhirlLayout.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    };
    private GameView mGameView;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            ImageView pauseImage = (ImageView) FrozenBubble.this.findViewById(R.id.i_game_paused);
            switch (msg.what) {
                case 500:
                    if (msg.arg1 == FrozenBubble.SHOW_PAUSED) {
                        if (!FrozenBubble.this.isFinishing()) {
                            pauseImage.setBackgroundResource(R.drawable.game_paused);
                            pauseImage.startAnimation(AnimationUtils.loadAnimation(FrozenBubble.this.getBaseContext(), R.anim.hyper_anim));
                            return;
                        }
                        return;
                    } else if (!FrozenBubble.this.isFinishing()) {
                        pauseImage.setBackgroundResource(0);
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    };
    private boolean selectorEnabled = true;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 7, 0, (int) R.string.menu_scoreloop).setIcon((int) R.drawable.ic_menu_sl);
        menu.add(0, 4, 0, (int) R.string.menu_buddylist).setIcon((int) R.drawable.ic_menu_friends_sl);
        if (this.selectorEnabled) {
            menu.add(0, 5, 0, (int) R.string.level_selector).setIcon((int) R.drawable.ic_menu_selector);
        }
        menu.add(0, 2, 0, (int) R.string.menu_editor).setIcon((int) R.drawable.ic_menu_editor);
        menu.add(0, 8, 0, (int) R.string.menu_preferences).setIcon((int) R.drawable.ic_menu_preferences);
        menu.add(0, 6, 0, (int) R.string.menu_share).setIcon((int) R.drawable.ic_menu_share);
        menu.add(0, 1, 0, (int) R.string.menu_help).setIcon(17301568);
        menu.add(0, 11, 0, (int) R.string.menu_about);
        MenuItem menu_item = menu.add(0, 3, 0, (int) R.string.menu_exit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, BubbleBusterHowToActivity.class));
                return true;
            case 2:
                startEditor();
                return true;
            case 3:
                finish();
                return true;
            case 4:
                startActivity(new Intent(this, BuddiesScreenActivity.class));
                return true;
            case 5:
                Intent i = new Intent("com.celliecraze.marblemadness.OVERVIEW");
                i.putExtra("currentLevel", this.mGameView.getThread().getCurrentLevelIndex());
                i.putExtra("unlockedLevel", getUnlocked());
                startActivityForResult(i, 300);
                return true;
            case 6:
                shareApp();
                return true;
            case 7:
                startActivity(new Intent(this, EntryScreenActivity.class));
                return true;
            case 8:
                startActivityForResult(new Intent(this, BubbleBusterPreferencesActivity.class), 302);
                return true;
            case 9:
            case 10:
            default:
                return false;
            case 11:
                startActivity(new Intent(this, BubbleBusterAboutActivity.class));
                return true;
        }
    }

    public void onAccelerationChanged(float x, float y, float z) {
        if (mGameThread != null && !gamePausedActive) {
            if (invertAccelKey) {
                mGameThread.setPosition((double) ((3.0f * x) + 20.0f));
            } else {
                mGameThread.setPosition((double) (20.0f - (3.0f * x)));
            }
        }
    }

    private void setAccelerometer(int gameModeKey2) {
        boolean z;
        if ((gameModeKey2 == 2) && AccelerometerManager.isSupported(getApplicationContext())) {
            AccelerometerManager.startListening(getApplicationContext(), this);
        }
        if (gameModeKey2 != 1) {
            if (gameModeKey2 == 0) {
                z = true;
            } else {
                z = false;
            }
            if (!z || !AccelerometerManager.isListening()) {
                return;
            }
        }
        AccelerometerManager.stopListening();
    }

    private final void setGamePaused(boolean gamePausedKey) {
        if (gamePausedKey) {
            if (mGameThread != null) {
                mGameThread.pauseOn();
            }
            prefPause = true;
            gamePausedActive = true;
            HighscoreManager.pause(gamePausedActive);
            showPausedImage(gamePausedActive);
            return;
        }
        gamePausedActive = false;
        if (mGameThread != null) {
            mGameThread.pauseOff();
        }
        prefPause = false;
        HighscoreManager.pause(gamePausedActive);
        showPausedImage(gamePausedActive);
    }

    private final void setFullScreen(boolean fullScreenKey) {
        if (fullScreenKey) {
            fullScreenActive = true;
            fullScreenSwitch = true;
            getWindow().addFlags(1024);
            getWindow().clearFlags(2048);
        } else {
            fullScreenActive = false;
            fullScreenSwitch = true;
            getWindow().clearFlags(1024);
            getWindow().addFlags(2048);
        }
        if (this.mGameView != null) {
            this.mGameView.requestLayout();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initCrashReporter();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                FrozenBubble.this.readPrefs();
            }
        });
        readPrefs();
        AppRater.app_launched(this);
        setVolumeControlStream(3);
        Intent i = getIntent();
        if (i == null || i.getExtras() == null || !i.getExtras().containsKey("levels")) {
            this.selectorEnabled = true;
            this.activityCustomStarted = false;
            setContentView((int) R.layout.main);
            this.mGameView = (GameView) findViewById(R.id.game);
            this.mGameView.setHandler(this.handler);
            this.mGameView.initThread(null, 0);
            HighscoreManager.reset();
        } else {
            this.selectorEnabled = false;
            int startingLevel = prefs.getInt("levelCustom", 0);
            int startingLevelIntent = i.getIntExtra("startingLevel", -2);
            if (startingLevelIntent != -2) {
                startingLevel = startingLevelIntent;
            }
            this.activityCustomStarted = true;
            setContentView((int) R.layout.main);
            this.mGameView = (GameView) findViewById(R.id.game);
            this.mGameView.setHandler(this.handler);
            this.mGameView.initThread(i.getExtras().getByteArray("levels"), startingLevel);
            HighscoreManager.reset();
        }
        mGameThread = this.mGameView.getThread();
        if (savedInstanceState != null) {
            mGameThread.restoreState(savedInstanceState);
        }
        this.mGameView.requestFocus();
        if (gameModeKey == 2) {
            AccelerometerManager.startListening(getApplicationContext(), this);
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) BubbleBusterConstants.GAMEFIELD_WIDTH) * density);
        AdWhirlManager.setConfigExpireTimeout(Constant.MARKET_REFRESH_TIME);
        this.adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        this.adWhirlLayout.setAdWhirlInterface(this);
        this.adWhirlLayout.setMaxWidth(width);
        this.adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
        onWindowFocusChanged(false);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (missingResources(true) || missingResources(false)) {
            restoreResources();
        }
        if (!isEditorInstalled(this, "com.celliecraze.marblemadnesseditor.EDIT")) {
            firstRun();
        }
        showPausedImage(gamePausedActive);
    }

    private boolean missingResources(boolean fullscreen) {
        String png = fullscreen ? ".png" : ".stat.png";
        String jpg = fullscreen ? ".jpg" : ".stat.jpg";
        File root = getFilesDir();
        if (!root.exists()) {
            return true;
        }
        for (int i = 0; i < BubbleBusterSplashActivity.names.length; i++) {
            if (!new File(root, String.valueOf(BubbleBusterSplashActivity.names[i]) + png).exists() && !new File(root, String.valueOf(BubbleBusterSplashActivity.names[i]) + jpg).exists()) {
                return true;
            }
        }
        return false;
    }

    private void restoreResources() {
        startActivity(new Intent(this, BubbleBusterSplashActivity.class));
        System.runFinalizersOnExit(true);
        System.exit(0);
    }

    public static boolean isEditorInstalled(Context context, String action) {
        return context.getPackageManager().queryIntentActivities(new Intent(action), 65536).size() > 0;
    }

    private void firstRun() {
        onWindowFocusChanged(false);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 128);
            if (prefs.getLong("lastRunVersionCode", 0) < ((long) pInfo.versionCode)) {
                Typeface localTypeface = Typeface.createFromAsset(getAssets(), "fonts/utb.ttf");
                View myDialog = LayoutInflater.from(this).inflate((int) R.layout.de_dialog, (ViewGroup) null);
                ((TextView) myDialog.findViewById(R.id.app_name)).setTypeface(localTypeface);
                ((TextView) myDialog.findViewById(R.id.dialog_firstrun)).setTypeface(localTypeface);
                ((Button) myDialog.findViewById(R.id.b_ok)).setTypeface(localTypeface);
                ((Button) myDialog.findViewById(R.id.b_cancel)).setTypeface(localTypeface);
                final AlertDialog a = new AlertDialog.Builder(this).setView(myDialog).create();
                a.setCancelable(false);
                myDialog.findViewById(R.id.b_ok).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        BubbleBusterConstants.vibrate(FrozenBubble.this.getBaseContext(), 50);
                        new Intent();
                        try {
                            Toast.makeText(FrozenBubble.this.getApplicationContext(), (int) R.string.rate_bb, 1).show();
                            try {
                                FrozenBubble.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.celliecraze.marblemadnesseditor")));
                            } catch (Exception e) {
                                Toast.makeText(FrozenBubble.this.getApplicationContext(), (int) R.string.market_missing, 1).show();
                                a.dismiss();
                            }
                        } catch (Exception e2) {
                            Toast.makeText(FrozenBubble.this.getApplicationContext(), (int) R.string.market_missing, 1).show();
                            a.dismiss();
                        }
                        a.dismiss();
                    }
                });
                myDialog.findViewById(R.id.b_cancel).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        BubbleBusterConstants.vibrate(FrozenBubble.this.getBaseContext(), 50);
                        a.dismiss();
                        HighscoreManager.reset();
                    }
                });
                a.show();
                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong("lastRunVersionCode", (long) pInfo.versionCode);
                editor.commit();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void showPausedImage(boolean paused) {
        Message msg = this.mHandler.obtainMessage();
        msg.what = 500;
        if (paused) {
            msg.arg1 = SHOW_PAUSED;
        } else {
            msg.arg1 = -1;
        }
        this.mHandler.sendMessage(msg);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (!prefPause) {
            if (!hasWindowFocus) {
                if (mGameThread != null) {
                    mGameThread.pauseOn();
                }
                gamePausedActive = true;
            } else {
                gamePausedActive = false;
                if (mGameThread != null) {
                    mGameThread.pauseOff();
                }
            }
            showPausedImage(gamePausedActive);
            HighscoreManager.pause(gamePausedActive);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(null);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }
        if (this.mGameView != null) {
            this.mGameView.cleanUp();
        }
        this.mGameView = null;
        mGameThread = null;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mGameThread != null) {
            mGameThread.saveState(outState);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 300 && resultCode == -1) {
            if (this.mGameView != null) {
                this.mGameView.cleanUp();
            }
            this.mGameView = null;
            mGameThread = null;
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("level", data.getExtras().getInt("selectedLevel") - 1);
            edit.commit();
            HighscoreManager.reset();
            this.activityCustomStarted = false;
            this.selectorEnabled = true;
            setContentView((int) R.layout.main);
            this.mGameView = (GameView) findViewById(R.id.game);
            this.mGameView.setHandler(this.handler);
            this.mGameView.initThread(null, 0);
            mGameThread = this.mGameView.getThread();
            this.mGameView.requestFocus();
        } else if (requestCode == 302) {
            readPrefs();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if (intent != null && EDITORACTION.equals(intent.getAction())) {
            if (this.mGameView != null) {
                this.mGameView.cleanUp();
            }
            this.mGameView = null;
            mGameThread = null;
            if (!this.activityCustomStarted) {
                this.activityCustomStarted = true;
                this.selectorEnabled = false;
                int startingLevel = prefs.getInt("levelCustom", 0);
                int startingLevelIntent = intent.getIntExtra("startingLevel", -2);
                if (startingLevelIntent != -2) {
                    startingLevel = startingLevelIntent;
                }
                HighscoreManager.reset();
                setContentView((int) R.layout.main);
                this.mGameView = (GameView) findViewById(R.id.game);
                this.mGameView.setHandler(this.handler);
                this.mGameView.initThread(intent.getExtras().getByteArray("levels"), startingLevel);
                mGameThread = this.mGameView.getThread();
                mGameThread.newGame();
                this.mGameView.requestFocus();
            }
        }
    }

    private void startEditor() {
        Intent i = new Intent();
        i.setClassName("com.celliecraze.marblemadnesseditorplus", "com.celliecraze.marblemadnesseditorplus.EditorActivity");
        try {
            startActivity(i);
            finish();
        } catch (ActivityNotFoundException e) {
            i.setClassName("com.celliecraze.marblemadnesseditor", "com.celliecraze.marblemadnesseditor.EditorActivity");
            try {
                startActivity(i);
                finish();
            } catch (ActivityNotFoundException e2) {
                try {
                    Toast.makeText(getApplicationContext(), (int) R.string.install_editor, 1).show();
                    try {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.celliecraze.marblemadnesseditor")));
                    } catch (Exception e3) {
                        Toast.makeText(getApplicationContext(), (int) R.string.market_missing, 1).show();
                    }
                } catch (Exception e4) {
                    Toast.makeText(getApplicationContext(), (int) R.string.market_missing, 1).show();
                }
            }
        }
    }

    private int getUnlocked() {
        return prefs.getInt(BubbleBusterOverviewActivity.MAX_LEVEL, 0);
    }

    private void shareApp() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.SUBJECT", getText(R.string.app_name));
        intent.putExtra("android.intent.extra.TEXT", getText(R.string.dialog_chooser));
        startActivity(Intent.createChooser(intent, ((Object) getText(R.string.menu_share)) + " " + ((Object) getText(R.string.app_name))));
    }

    /* access modifiers changed from: private */
    public final void readPrefs() {
        gameModeKey = Integer.parseInt(prefs.getString("gameModeKey", "0"));
        setAccelerometer(gameModeKey);
        setFullScreen(prefs.getBoolean("fullScreenKey", true));
        setGamePaused(prefs.getBoolean("gamePausedKey", false));
        soundKey = prefs.getBoolean("soundKey", true);
        vibrationKey = prefs.getBoolean("vibrationKey", true);
        swapBubblesKey = prefs.getBoolean("swapBubblesKey", false);
        invertAccelKey = prefs.getBoolean("invertAccelKey", false);
        dontRushMeKey = prefs.getBoolean("dontRushMeKey", true);
        bubbleBlindKey = prefs.getBoolean("bubbleBlindKey", false);
    }

    private void initCrashReporter() {
        final ErrorReporter reporter = ErrorReporter.getInstance();
        reporter.Init(this);
        if (reporter.bIsThereAnyErrorFile()) {
            new AlertDialog.Builder(this).setTitle(getText(R.string.crashreport_title)).setMessage(getText(R.string.crashreport_msg)).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    reporter.CheckErrorAndSendMail(FrozenBubble.this);
                }
            }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    reporter.deleteErrorFiles();
                    dialog.dismiss();
                }
            }).show();
        }
    }

    public void adWhirlGeneric() {
    }
}
