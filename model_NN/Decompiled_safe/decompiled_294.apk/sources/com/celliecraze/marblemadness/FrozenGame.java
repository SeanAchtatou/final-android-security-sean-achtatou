package com.celliecraze.marblemadness;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.highscores.HighscoreManager;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.Vector;
import twitter4j.internal.http.HttpResponseCode;

public class FrozenGame extends GameScreen {
    public static final int FIRE = 1;
    public static final int HORIZONTAL_MOVE = 0;
    public static final int KEY_LEFT = 37;
    public static final int KEY_M = 77;
    public static final int KEY_RIGHT = 39;
    public static final int KEY_S = 83;
    public static final int KEY_SHIFT = 16;
    public static final int KEY_UP = 38;
    public static String PARAMETER_OFFLINE = "offline";
    public static String PARAMETER_PLAYER = BubbleBusterConstants.PLAYER;
    public static int nbBubbles;
    BmpWrap background;
    int blinkDelay;
    BmpWrap bubbleBlink;
    BubbleManager bubbleManager;
    BubbleSprite[][] bubblePlay;
    BmpWrap[] bubbles;
    BmpWrap[] bubblesBlind;
    Compressor compressor;
    int currentColor;
    boolean endOfGame;
    Vector<Sprite> falling;
    int fixedBubbles;
    BmpWrap[] frozenBubbles;
    boolean frozenify;
    int frozenifyX;
    int frozenifyY;
    BmpWrap gameLost;
    Handler handler;
    ImageSprite hurrySprite;
    int hurryTime;
    Vector<Sprite> jumping;
    LaunchBubbleSprite launchBubble;
    double launchBubblePosition;
    Drawable launcher;
    boolean levelCompleted = false;
    LevelManager levelManager;
    boolean modeKeyPressed;
    double moveDown;
    BubbleSprite movingBubble;
    ImageSprite nextBubble;
    int nextColor;
    BmpWrap penguins;
    Random random;
    boolean readyToFire;
    boolean soundKeyPressed;
    SoundManager soundManager;
    BmpWrap[] targetedBubbles;

    public FrozenGame(BmpWrap background_arg, BmpWrap[] bubbles_arg, BmpWrap[] bubblesBlind_arg, BmpWrap[] frozenBubbles_arg, BmpWrap[] targetedBubbles_arg, BmpWrap bubbleBlink_arg, BmpWrap gameLost_arg, BmpWrap hurry_arg, BmpWrap compressorHead_arg, BmpWrap compressor_arg, Drawable launcher_arg, SoundManager soundManager_arg, LevelManager levelManager_arg, Handler handler2) {
        this.handler = handler2;
        this.random = new Random(System.currentTimeMillis());
        this.launcher = launcher_arg;
        this.background = background_arg;
        this.bubbles = bubbles_arg;
        this.bubblesBlind = bubblesBlind_arg;
        this.frozenBubbles = frozenBubbles_arg;
        this.targetedBubbles = targetedBubbles_arg;
        this.bubbleBlink = bubbleBlink_arg;
        this.gameLost = gameLost_arg;
        this.soundManager = soundManager_arg;
        this.levelManager = levelManager_arg;
        this.launchBubblePosition = 20.0d;
        this.compressor = new Compressor(compressorHead_arg, compressor_arg);
        this.hurrySprite = new ImageSprite(new Rect(315, 285, 443, HttpResponseCode.TOO_LONG), hurry_arg);
        this.jumping = new Vector<>();
        this.falling = new Vector<>();
        this.bubblePlay = (BubbleSprite[][]) Array.newInstance(BubbleSprite.class, 8, 13);
        this.bubbleManager = new BubbleManager(this.bubbles);
        byte[][] currentLevel = this.levelManager.getCurrentLevel();
        if (currentLevel != null) {
            for (int j = 0; j < 12; j++) {
                for (int i = j % 2; i < 8; i++) {
                    if (currentLevel[i][j] != -1) {
                        BubbleSprite newOne = new BubbleSprite(new Rect(((i * 32) + 190) - ((j % 2) * 16), (j * 28) + 44, 32, 32), currentLevel[i][j], this.bubbles[currentLevel[i][j]], this.bubblesBlind[currentLevel[i][j]], this.frozenBubbles[currentLevel[i][j]], this.bubbleBlink, this.bubbleManager, this.soundManager, this);
                        this.bubblePlay[i][j] = newOne;
                        addSprite(newOne);
                    }
                }
            }
            this.currentColor = this.bubbleManager.nextBubbleIndex(this.random);
            this.nextColor = this.bubbleManager.nextBubbleIndex(this.random);
            if (!FrozenBubble.bubbleBlindKey) {
                this.nextBubble = new ImageSprite(new Rect(HttpResponseCode.FOUND, 440, 334, 472), this.bubbles[this.nextColor]);
            } else {
                this.nextBubble = new ImageSprite(new Rect(HttpResponseCode.FOUND, 440, 334, 472), this.bubblesBlind[this.nextColor]);
            }
            addSprite(this.nextBubble);
            this.launchBubble = new LaunchBubbleSprite(this.currentColor, (int) this.launchBubblePosition, this.launcher, this.bubbles, this.bubblesBlind);
            spriteToBack(this.launchBubble);
            nbBubbles = 0;
            HighscoreManager.startLevel();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public void saveState(Bundle map) {
        Vector<Sprite> savedSprites = new Vector<>();
        saveSprites(map, savedSprites);
        for (int i = 0; i < this.jumping.size(); i++) {
            this.jumping.elementAt(i).saveState(map, savedSprites);
            map.putInt(String.format("jumping-%d", Integer.valueOf(i)), this.jumping.elementAt(i).getSavedId());
        }
        map.putInt("numJumpingSprites", this.jumping.size());
        for (int i2 = 0; i2 < this.falling.size(); i2++) {
            this.falling.elementAt(i2).saveState(map, savedSprites);
            map.putInt(String.format("falling-%d", Integer.valueOf(i2)), this.falling.elementAt(i2).getSavedId());
        }
        map.putInt("numFallingSprites", this.falling.size());
        for (int i3 = 0; i3 < 8; i3++) {
            for (int j = 0; j < 13; j++) {
                if (this.bubblePlay[i3][j] != null) {
                    this.bubblePlay[i3][j].saveState(map, savedSprites);
                    map.putInt(String.format("play-%d-%d", Integer.valueOf(i3), Integer.valueOf(j)), this.bubblePlay[i3][j].getSavedId());
                } else {
                    map.putInt(String.format("play-%d-%d", Integer.valueOf(i3), Integer.valueOf(j)), -1);
                }
            }
        }
        this.launchBubble.saveState(map, savedSprites);
        map.putInt("launchBubbleId", this.launchBubble.getSavedId());
        map.putDouble("launchBubblePosition", this.launchBubblePosition);
        this.compressor.saveState(map);
        this.nextBubble.saveState(map, savedSprites);
        map.putInt("nextBubbleId", this.nextBubble.getSavedId());
        map.putInt("currentColor", this.currentColor);
        map.putInt("nextColor", this.nextColor);
        if (this.movingBubble != null) {
            this.movingBubble.saveState(map, savedSprites);
            map.putInt("movingBubbleId", this.movingBubble.getSavedId());
        } else {
            map.putInt("movingBubbleId", -1);
        }
        this.bubbleManager.saveState(map);
        map.putInt("fixedBubbles", this.fixedBubbles);
        map.putDouble("moveDown", this.moveDown);
        map.putInt("nbBubbles", nbBubbles);
        map.putInt("blinkDelay", this.blinkDelay);
        this.hurrySprite.saveState(map, savedSprites);
        map.putInt("hurryId", this.hurrySprite.getSavedId());
        map.putInt("hurryTime", this.hurryTime);
        map.putBoolean("readyToFire", this.readyToFire);
        map.putBoolean("endOfGame", this.endOfGame);
        map.putBoolean("frozenify", this.frozenify);
        map.putInt("frozenifyX", this.frozenifyX);
        map.putInt("frozenifyY", this.frozenifyY);
        map.putInt("numSavedSprites", savedSprites.size());
        for (int i4 = 0; i4 < savedSprites.size(); i4++) {
            ((Sprite) savedSprites.elementAt(i4)).clearSavedId();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 37 */
    private Sprite restoreSprite(Bundle map, Vector<BmpWrap> imageList, int i) {
        int left = map.getInt(String.format("%d-left", Integer.valueOf(i)));
        int right = map.getInt(String.format("%d-right", Integer.valueOf(i)));
        int top = map.getInt(String.format("%d-top", Integer.valueOf(i)));
        int bottom = map.getInt(String.format("%d-bottom", Integer.valueOf(i)));
        int type = map.getInt(String.format("%d-type", Integer.valueOf(i)));
        if (type == Sprite.TYPE_BUBBLE) {
            int color = map.getInt(String.format("%d-color", Integer.valueOf(i)));
            return new BubbleSprite(new Rect(left, top, right, bottom), color, map.getDouble(String.format("%d-moveX", Integer.valueOf(i))), map.getDouble(String.format("%d-moveY", Integer.valueOf(i))), map.getDouble(String.format("%d-realX", Integer.valueOf(i))), map.getDouble(String.format("%d-realY", Integer.valueOf(i))), map.getBoolean(String.format("%d-fixed", Integer.valueOf(i))), map.getBoolean(String.format("%d-blink", Integer.valueOf(i))), map.getBoolean(String.format("%d-released", Integer.valueOf(i))), map.getBoolean(String.format("%d-checkJump", Integer.valueOf(i))), map.getBoolean(String.format("%d-checkFall", Integer.valueOf(i))), map.getInt(String.format("%d-fixedAnim", Integer.valueOf(i))), map.getBoolean(String.format("%d-frozen", new Object[]{Integer.valueOf(i)})) ? this.frozenBubbles[color] : this.bubbles[color], this.bubblesBlind[color], this.frozenBubbles[color], this.targetedBubbles, this.bubbleBlink, this.bubbleManager, this.soundManager, this);
        } else if (type == Sprite.TYPE_IMAGE) {
            return new ImageSprite(new Rect(left, top, right, bottom), imageList.elementAt(map.getInt(String.format("%d-imageId", Integer.valueOf(i)))));
        } else if (type == Sprite.TYPE_LAUNCH_BUBBLE) {
            return new LaunchBubbleSprite(map.getInt(String.format("%d-currentColor", Integer.valueOf(i))), map.getInt(String.format("%d-currentDirection", Integer.valueOf(i))), this.launcher, this.bubbles, this.bubblesBlind);
        } else {
            Log.e(BubbleBusterConstants.TAG, "Unrecognized sprite type: " + type);
            return null;
        }
    }

    public void restoreState(Bundle map, Vector<BmpWrap> imageList) {
        Vector<Sprite> savedSprites = new Vector<>();
        int numSavedSprites = map.getInt("numSavedSprites");
        for (int i = 0; i < numSavedSprites; i++) {
            savedSprites.addElement(restoreSprite(map, imageList, i));
        }
        restoreSprites(map, savedSprites);
        this.jumping = new Vector<>();
        int numJumpingSprites = map.getInt("numJumpingSprites");
        for (int i2 = 0; i2 < numJumpingSprites; i2++) {
            this.jumping.addElement((Sprite) savedSprites.elementAt(map.getInt(String.format("jumping-%d", Integer.valueOf(i2)))));
        }
        this.falling = new Vector<>();
        int numFallingSprites = map.getInt("numFallingSprites");
        for (int i3 = 0; i3 < numFallingSprites; i3++) {
            this.falling.addElement((Sprite) savedSprites.elementAt(map.getInt(String.format("falling-%d", Integer.valueOf(i3)))));
        }
        this.bubblePlay = (BubbleSprite[][]) Array.newInstance(BubbleSprite.class, 8, 13);
        for (int i4 = 0; i4 < 8; i4++) {
            for (int j = 0; j < 13; j++) {
                int spriteIdx = map.getInt(String.format("play-%d-%d", Integer.valueOf(i4), Integer.valueOf(j)));
                if (spriteIdx == -1 || spriteIdx <= 0) {
                    this.bubblePlay[i4][j] = null;
                } else {
                    this.bubblePlay[i4][j] = (BubbleSprite) savedSprites.elementAt(spriteIdx);
                }
            }
        }
        this.launchBubble = (LaunchBubbleSprite) savedSprites.elementAt(map.getInt("launchBubbleId"));
        this.launchBubblePosition = map.getDouble("launchBubblePosition");
        this.compressor.restoreState(map);
        this.nextBubble = (ImageSprite) savedSprites.elementAt(map.getInt("nextBubbleId"));
        this.currentColor = map.getInt("currentColor");
        this.nextColor = map.getInt("nextColor");
        int movingBubbleId = map.getInt("movingBubbleId");
        if (movingBubbleId == -1) {
            this.movingBubble = null;
        } else {
            this.movingBubble = (BubbleSprite) savedSprites.elementAt(movingBubbleId);
        }
        this.bubbleManager.restoreState(map);
        this.fixedBubbles = map.getInt("fixedBubbles");
        this.moveDown = map.getDouble("moveDown");
        nbBubbles = map.getInt("nbBubbles");
        this.blinkDelay = map.getInt("blinkDelay");
        this.hurrySprite = (ImageSprite) savedSprites.elementAt(map.getInt("hurryId"));
        this.hurryTime = map.getInt("hurryTime");
        this.readyToFire = map.getBoolean("readyToFire");
        this.endOfGame = map.getBoolean("endOfGame");
        this.frozenify = map.getBoolean("frozenify");
        this.frozenifyX = map.getInt("frozenifyX");
        this.frozenifyY = map.getInt("frozenifyY");
    }

    private void initFrozenify() {
        ImageSprite freezeLaunchBubble = new ImageSprite(new Rect(BubbleBusterConstants.MSG_LEVEL_FINISH_OK, 389, 34, 42), this.frozenBubbles[this.currentColor]);
        ImageSprite freezeNextBubble = new ImageSprite(new Rect(BubbleBusterConstants.MSG_LEVEL_FINISH_OK, 439, 34, 42), this.frozenBubbles[this.nextColor]);
        addSprite(freezeLaunchBubble);
        addSprite(freezeNextBubble);
        this.frozenifyX = 7;
        this.frozenifyY = 12;
        this.frozenify = true;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 120 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x005d, code lost:
        r8.frozenify = false;
        addSprite(new com.celliecraze.marblemadness.ImageSprite(new android.graphics.Rect(170, 190, 318, 307), r8.gameLost));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0046, code lost:
        r0 = new android.os.Message();
        r0.what = 0;
        r8.handler.sendMessage(r0);
        r8.frozenifyX = 7;
        r8.frozenifyY--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x005b, code lost:
        if (r8.frozenifyY >= 0) goto L_0x0073;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void frozenify() {
        /*
            r8 = this;
            r7 = 190(0xbe, float:2.66E-43)
            r6 = 170(0xaa, float:2.38E-43)
            r5 = 7
            r4 = 1
            r3 = 0
            int r1 = r8.frozenifyX
            int r1 = r1 - r4
            r8.frozenifyX = r1
            int r1 = r8.frozenifyX
            if (r1 >= 0) goto L_0x0073
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            r0.what = r3
            android.os.Handler r1 = r8.handler
            r1.sendMessage(r0)
            r8.frozenifyX = r5
            int r1 = r8.frozenifyY
            int r1 = r1 - r4
            r8.frozenifyY = r1
            int r1 = r8.frozenifyY
            if (r1 >= 0) goto L_0x0073
            r8.frozenify = r3
            com.celliecraze.marblemadness.ImageSprite r1 = new com.celliecraze.marblemadness.ImageSprite
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 318(0x13e, float:4.46E-43)
            r4 = 307(0x133, float:4.3E-43)
            r2.<init>(r6, r7, r3, r4)
            com.celliecraze.marblemadness.BmpWrap r3 = r8.gameLost
            r1.<init>(r2, r3)
            r8.addSprite(r1)
        L_0x003c:
            return
        L_0x003d:
            int r1 = r8.frozenifyX
            int r1 = r1 - r4
            r8.frozenifyX = r1
            int r1 = r8.frozenifyX
            if (r1 >= 0) goto L_0x0073
            android.os.Message r0 = new android.os.Message
            r0.<init>()
            r0.what = r3
            android.os.Handler r1 = r8.handler
            r1.sendMessage(r0)
            r8.frozenifyX = r5
            int r1 = r8.frozenifyY
            int r1 = r1 - r4
            r8.frozenifyY = r1
            int r1 = r8.frozenifyY
            if (r1 >= 0) goto L_0x0073
            r8.frozenify = r3
            com.celliecraze.marblemadness.ImageSprite r1 = new com.celliecraze.marblemadness.ImageSprite
            android.graphics.Rect r2 = new android.graphics.Rect
            r3 = 318(0x13e, float:4.46E-43)
            r4 = 307(0x133, float:4.3E-43)
            r2.<init>(r6, r7, r3, r4)
            com.celliecraze.marblemadness.BmpWrap r3 = r8.gameLost
            r1.<init>(r2, r3)
            r8.addSprite(r1)
            goto L_0x003c
        L_0x0073:
            com.celliecraze.marblemadness.BubbleSprite[][] r1 = r8.bubblePlay
            int r2 = r8.frozenifyX
            r1 = r1[r2]
            int r2 = r8.frozenifyY
            r1 = r1[r2]
            if (r1 != 0) goto L_0x0083
            int r1 = r8.frozenifyY
            if (r1 >= 0) goto L_0x003d
        L_0x0083:
            com.celliecraze.marblemadness.BubbleSprite[][] r1 = r8.bubblePlay
            int r2 = r8.frozenifyX
            r1 = r1[r2]
            int r2 = r8.frozenifyY
            r1 = r1[r2]
            r8.spriteToBack(r1)
            com.celliecraze.marblemadness.BubbleSprite[][] r1 = r8.bubblePlay
            int r2 = r8.frozenifyX
            r1 = r1[r2]
            int r2 = r8.frozenifyY
            r1 = r1[r2]
            r1.frozenify()
            com.celliecraze.marblemadness.LaunchBubbleSprite r1 = r8.launchBubble
            r8.spriteToBack(r1)
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.marblemadness.FrozenGame.frozenify():void");
    }

    public BubbleSprite[][] getGrid() {
        return this.bubblePlay;
    }

    public void addFallingBubble(BubbleSprite sprite) {
        spriteToFront(sprite);
        this.falling.addElement(sprite);
    }

    public void deleteFallingBubble(BubbleSprite sprite) {
        removeSprite(sprite);
        this.falling.removeElement(sprite);
    }

    public void addJumpingBubble(BubbleSprite sprite) {
        spriteToFront(sprite);
        this.jumping.addElement(sprite);
    }

    public void deleteJumpingBubble(BubbleSprite sprite) {
        removeSprite(sprite);
        this.jumping.removeElement(sprite);
    }

    public Random getRandom() {
        return this.random;
    }

    public double getMoveDown() {
        return this.moveDown;
    }

    private void sendBubblesDown() {
        this.soundManager.playSound(7);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 12; j++) {
                if (this.bubblePlay[i][j] != null) {
                    this.bubblePlay[i][j].moveDown();
                    if (this.bubblePlay[i][j].getSpritePosition().y >= 380) {
                        this.endOfGame = true;
                        initFrozenify();
                        this.soundManager.playSound(1);
                    }
                }
            }
        }
        this.moveDown += 28.0d;
        this.compressor.moveDown();
    }

    private void blinkLine(int number) {
        int column = (number + 1) >> 1;
        for (int i = number % 2; i < 13; i++) {
            if (this.bubblePlay[column][i] != null) {
                this.bubblePlay[column][i].blink();
            }
        }
    }

    public boolean play(boolean key_left, boolean key_right, boolean key_fire, double trackball_dx, double touch_dx) {
        int[] move = new int[2];
        if (key_left && !key_right) {
            move[0] = 37;
        } else if (!key_right || key_left) {
            move[0] = 0;
        } else {
            move[0] = 39;
        }
        if (key_fire) {
            move[1] = 38;
        } else {
            move[1] = 0;
        }
        if (move[1] == 0) {
            this.readyToFire = true;
        }
        if (!FrozenBubble.dontRushMeKey) {
            this.hurryTime = 1;
        }
        if (this.endOfGame) {
            if (move[1] == 38 && this.readyToFire) {
                return true;
            }
            if (this.frozenify) {
                frozenify();
            }
        } else if (move[1] != 38 && this.hurryTime <= 480) {
            double dx = 0.0d;
            if (move[0] == 37) {
                dx = 0.0d - 1.0d;
            }
            if (move[0] == 39) {
                dx += 1.0d;
            }
            this.launchBubblePosition = this.launchBubblePosition + dx + trackball_dx + touch_dx;
            if (this.launchBubblePosition < 1.0d) {
                this.launchBubblePosition = 1.0d;
            }
            if (this.launchBubblePosition > 39.0d) {
                this.launchBubblePosition = 39.0d;
            }
            this.launchBubble.changeDirection((int) this.launchBubblePosition);
        } else if (this.movingBubble == null && this.readyToFire) {
            nbBubbles++;
            this.movingBubble = new BubbleSprite(new Rect(HttpResponseCode.FOUND, 390, 32, 32), (int) this.launchBubblePosition, this.currentColor, this.bubbles[this.currentColor], this.bubblesBlind[this.currentColor], this.frozenBubbles[this.currentColor], this.targetedBubbles, this.bubbleBlink, this.bubbleManager, this.soundManager, this);
            addSprite(this.movingBubble);
            this.currentColor = this.nextColor;
            this.nextColor = this.bubbleManager.nextBubbleIndex(this.random);
            if (!FrozenBubble.bubbleBlindKey) {
                this.nextBubble.changeImage(this.bubbles[this.nextColor]);
            } else {
                this.nextBubble.changeImage(this.bubblesBlind[this.nextColor]);
            }
            this.launchBubble.changeColor(this.currentColor);
            this.soundManager.playSound(2);
            this.readyToFire = false;
            this.hurryTime = 0;
            removeSprite(this.hurrySprite);
        }
        if (this.movingBubble != null) {
            this.movingBubble.move();
            if (this.movingBubble.fixed()) {
                if (this.movingBubble.getSpritePosition().y >= 380 && !this.movingBubble.released()) {
                    this.endOfGame = true;
                    initFrozenify();
                    this.soundManager.playSound(1);
                } else if (this.bubbleManager.countBubbles() == 0) {
                    HighscoreManager.endLevel(nbBubbles);
                    this.levelManager.saveNextLevel();
                    this.levelManager.goToNextLevel();
                    this.levelCompleted = true;
                    this.endOfGame = true;
                    this.soundManager.playSound(0);
                } else {
                    this.fixedBubbles = this.fixedBubbles + 1;
                    this.blinkDelay = 0;
                    if (this.fixedBubbles == 8) {
                        this.fixedBubbles = 0;
                        sendBubblesDown();
                    }
                }
                this.movingBubble = null;
            }
            if (this.movingBubble != null) {
                this.movingBubble.move();
                if (this.movingBubble.fixed()) {
                    if (this.movingBubble.getSpritePosition().y >= 380 && !this.movingBubble.released()) {
                        this.endOfGame = true;
                        initFrozenify();
                        this.soundManager.playSound(1);
                    } else if (this.bubbleManager.countBubbles() == 0) {
                        HighscoreManager.endLevel(nbBubbles);
                        this.levelManager.saveNextLevel();
                        this.levelManager.goToNextLevel();
                        this.endOfGame = true;
                        this.levelCompleted = true;
                        this.soundManager.playSound(0);
                    } else {
                        this.fixedBubbles = this.fixedBubbles + 1;
                        this.blinkDelay = 0;
                        if (this.fixedBubbles == 8) {
                            this.fixedBubbles = 0;
                            sendBubblesDown();
                        }
                    }
                    this.movingBubble = null;
                }
            }
        }
        if (!FrozenBubble.gamePausedActive && this.movingBubble == null && !this.endOfGame) {
            this.hurryTime = this.hurryTime + 1;
            if (this.hurryTime == 2) {
                removeSprite(this.hurrySprite);
            }
            if (this.hurryTime >= 240) {
                if (this.hurryTime % 40 == 10) {
                    addSprite(this.hurrySprite);
                    this.soundManager.playSound(6);
                } else if (this.hurryTime % 40 == 35) {
                    removeSprite(this.hurrySprite);
                }
            }
        }
        if (this.fixedBubbles == 6) {
            if (this.blinkDelay < 15) {
                blinkLine(this.blinkDelay);
            }
            this.blinkDelay = this.blinkDelay + 1;
            if (this.blinkDelay == 40) {
                this.blinkDelay = 0;
            }
        } else if (this.fixedBubbles == 7) {
            if (this.blinkDelay < 15) {
                blinkLine(this.blinkDelay);
            }
            this.blinkDelay = this.blinkDelay + 1;
            if (this.blinkDelay == 25) {
                this.blinkDelay = 0;
            }
        }
        for (int i = 0; i < this.falling.size(); i++) {
            ((BubbleSprite) this.falling.elementAt(i)).fall();
        }
        for (int i2 = 0; i2 < this.jumping.size(); i2++) {
            ((BubbleSprite) this.jumping.elementAt(i2)).jump();
        }
        return false;
    }

    public void paint(Canvas c, double scale, int dx, int dy) {
        this.compressor.paint(c, scale, dx, dy);
        if (!FrozenBubble.bubbleBlindKey) {
            this.nextBubble.changeImage(this.bubbles[this.nextColor]);
        } else {
            this.nextBubble.changeImage(this.bubblesBlind[this.nextColor]);
        }
        super.paint(c, scale, dx, dy);
    }

    public void swapBubbles() {
        int temp = this.nextColor;
        this.nextColor = this.currentColor;
        this.currentColor = temp;
        this.launchBubble.changeColor(this.currentColor);
        this.soundManager.playSound(9);
    }

    public void setPosition(double value) {
        this.launchBubblePosition = value;
        if (this.launchBubblePosition < 1.0d) {
            this.launchBubblePosition = 1.0d;
        }
        if (this.launchBubblePosition > 39.0d) {
            this.launchBubblePosition = 39.0d;
        }
        this.launchBubble.changeDirection((int) this.launchBubblePosition);
    }
}
