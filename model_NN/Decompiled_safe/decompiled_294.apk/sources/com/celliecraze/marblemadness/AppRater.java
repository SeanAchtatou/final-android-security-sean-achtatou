package com.celliecraze.marblemadness;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {
    private static final int DAYS_UNTIL_PROMPT = 1;
    private static final int LAUNCHES_UNTIL_PROMPT = 2;

    public static void app_launched(Context mContext) {
        FrozenBubble.prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (!FrozenBubble.prefs.getBoolean("dontshowagain", false)) {
            SharedPreferences.Editor editor = FrozenBubble.prefs.edit();
            long launch_count = FrozenBubble.prefs.getLong("launch_count", 0) + 1;
            editor.putLong("launch_count", launch_count);
            Long date_firstLaunch = Long.valueOf(FrozenBubble.prefs.getLong("date_firstlaunch", 0));
            if (date_firstLaunch.longValue() == 0) {
                date_firstLaunch = Long.valueOf(System.currentTimeMillis());
                editor.putLong("date_firstlaunch", date_firstLaunch.longValue());
            }
            if (launch_count >= 2 && System.currentTimeMillis() >= date_firstLaunch.longValue() + 86400000) {
                showRateDialog(mContext, editor);
            }
            editor.commit();
        }
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle(((Object) mContext.getText(R.string.rate)) + " " + ((Object) mContext.getText(R.string.app_name)));
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(1);
        TextView tv = new TextView(mContext);
        CharSequence APP_TITLE = mContext.getString(R.string.app_name);
        tv.setText(mContext.getString(R.string.rate_title, APP_TITLE));
        tv.setWidth(240);
        tv.setPadding(4, 0, 4, 10);
        ll.addView(tv);
        Button b1 = new Button(mContext);
        b1.setText(((Object) mContext.getText(R.string.rate)) + " " + ((Object) APP_TITLE));
        b1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + mContext.getPackageName())));
                dialog.dismiss();
            }
        });
        ll.addView(b1);
        Button b2 = new Button(mContext);
        b2.setText((int) R.string.remind_later);
        b2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll.addView(b2);
        Button b3 = new Button(mContext);
        b3.setText((int) R.string.no_thanks);
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    editor.putBoolean("dontshowagain", true);
                    editor.commit();
                }
                dialog.dismiss();
            }
        });
        ll.addView(b3);
        dialog.setContentView(ll);
        dialog.show();
    }
}
