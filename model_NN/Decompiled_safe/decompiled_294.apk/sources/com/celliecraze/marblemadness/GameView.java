package com.celliecraze.marblemadness;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.celliecraze.marblemadness.activity.BubbleBusterSplashActivity;
import com.celliecraze.marblemadness.highscores.HighscoreManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

class GameView extends SurfaceView implements SurfaceHolder.Callback {
    public static int bubblesFired;
    public static boolean centerFired = false;
    public static int mCanvasHeight = 1;
    public static int mCanvasWidth = 1;
    /* access modifiers changed from: private */
    public Context mContext;
    public Handler mHandler;
    private GameThread thread;

    public void setHandler(Handler handler) {
        this.mHandler = handler;
    }

    public class GameThread extends Thread {
        private static final int FRAME_DELAY = 40;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_RUNNING = 1;
        private static final double TOUCH_COEFFICIENT = 0.2d;
        private static final double TOUCH_FIRE_Y_THRESHOLD = 390.0d;
        private static final double TRACKBALL_COEFFICIENT = 5.0d;
        private int IMG_BUFFER_LEN = 1024;
        private long LAST_TOUCH_BUBBLE_TIME;
        private final long SWITCH_BUBBLE_THRESHOLD = 300;
        private BmpWrap mBackground;
        private BmpWrap mBubbleBlink;
        private BmpWrap[] mBubbles;
        private BmpWrap[] mBubblesBlind;
        private BmpWrap mCompressor;
        private BmpWrap mCompressorHead;
        private int mDisplayDX;
        private int mDisplayDY;
        private double mDisplayScale;
        private boolean mFire = false;
        private BubbleFont mFont;
        private BmpWrap mFontImage;
        private BmpWrap[] mFrozenBubbles;
        private FrozenGame mFrozenGame;
        private BmpWrap mGameLost;
        private BmpWrap mHurry;
        Vector<BmpWrap> mImageList;
        private boolean mImagesReady = false;
        private long mLastTime;
        private Drawable mLauncher;
        private boolean mLeft = false;
        private LevelManager mLevelManager;
        private BmpWrap mLife;
        private int mMode;
        private boolean mRight = false;
        private boolean mRun = false;
        private SoundManager mSoundManager;
        private SurfaceHolder mSurfaceHolder;
        private boolean mSurfaceOK = false;
        private BmpWrap[] mTargetedBubbles;
        private double mTouchDX = 0.0d;
        private boolean mTouchFire = false;
        private double mTouchLastX;
        private double mTrackballDX = 0.0d;
        private boolean mUp = false;
        private boolean mWasFire = false;
        private boolean mWasLeft = false;
        private boolean mWasRight = false;
        private boolean mWasUp = false;

        public int getCurrentLevelIndex() {
            int levelIndex;
            synchronized (this.mSurfaceHolder) {
                levelIndex = this.mLevelManager.getLevelIndex();
            }
            return levelIndex;
        }

        public void setCurrentLevelIndex(int level) {
            synchronized (this.mSurfaceHolder) {
                this.mLevelManager.setCurrentLevel(level);
            }
        }

        private BmpWrap NewBmpWrap(String name) {
            BmpWrap new_img = new BmpWrap(this.mImageList.size(), name);
            this.mImageList.addElement(new_img);
            return new_img;
        }

        public GameThread(SurfaceHolder surfaceHolder, byte[] customLevels, int startingLevel) {
            this.mSurfaceHolder = surfaceHolder;
            Resources res = GameView.this.mContext.getResources();
            setState(2);
            this.mImageList = new Vector<>();
            this.mBackground = NewBmpWrap("mBackground");
            this.mBubbles = new BmpWrap[8];
            for (int i = 0; i < this.mBubbles.length; i++) {
                this.mBubbles[i] = NewBmpWrap("mBubbles" + i);
            }
            this.mBubblesBlind = new BmpWrap[8];
            for (int i2 = 0; i2 < this.mBubblesBlind.length; i2++) {
                this.mBubblesBlind[i2] = NewBmpWrap("mBubblesBlind" + i2);
            }
            this.mFrozenBubbles = new BmpWrap[8];
            for (int i3 = 0; i3 < this.mFrozenBubbles.length; i3++) {
                this.mFrozenBubbles[i3] = NewBmpWrap("mFrozenBubbles" + i3);
            }
            this.mTargetedBubbles = new BmpWrap[6];
            for (int i4 = 0; i4 < this.mTargetedBubbles.length; i4++) {
                this.mTargetedBubbles[i4] = NewBmpWrap("mTargetedBubbles" + i4);
            }
            this.mBubbleBlink = NewBmpWrap("mBubbleBlink");
            this.mGameLost = NewBmpWrap("mGameLost");
            this.mHurry = NewBmpWrap("mHurry");
            this.mCompressorHead = NewBmpWrap("mCompressorHead");
            this.mCompressor = NewBmpWrap("mCompressor");
            this.mLife = NewBmpWrap("mLife");
            this.mFontImage = NewBmpWrap("mFontImage");
            this.mFont = new BubbleFont(this.mFontImage);
            this.mLauncher = res.getDrawable(R.drawable.launcher);
            this.mSoundManager = new SoundManager(GameView.this.mContext);
            if (customLevels == null) {
                try {
                    InputStream is = GameView.this.mContext.getAssets().open("levels.txt");
                    byte[] levels = new byte[is.available()];
                    is.read(levels);
                    is.close();
                    this.mLevelManager = new LevelManager(levels, FrozenBubble.prefs.getInt("level", 0), GameView.this.mContext, false, GameView.this);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            } else {
                this.mLevelManager = new LevelManager(customLevels, startingLevel, GameView.this.mContext, true, GameView.this);
            }
            this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameLost, this.mHurry, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager, GameView.this.mHandler);
        }

        private void scaleFrom(BmpWrap image, BitmapFactory.Options options) {
            String name;
            options.inTempStorage = new byte[(this.IMG_BUFFER_LEN * 32)];
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = false;
            try {
                options.getClass().getField("inPurgeable").set(options, Boolean.TRUE);
                options.getClass().getField("inInputShareable").set(options, Boolean.TRUE);
            } catch (Exception e) {
            }
            File root = GameView.this.mContext.getFilesDir();
            if (FrozenBubble.fullScreenActive) {
                name = String.valueOf(image.name) + ".png";
            } else {
                name = String.valueOf(image.name) + ".stat.png";
            }
            File img = new File(root, name);
            if (!img.exists()) {
                img = new File(root, name.replace(".png", ".jpg"));
            }
            if (!img.exists()) {
                image.bmp = BitmapFactory.decodeResource(GameView.this.getResources(), GameView.this.getResourceByString(image.name), options);
            } else if (image.bmp == null || FrozenBubble.fullScreenSwitch) {
                if (image.bmp != null) {
                    image.bmp.recycle();
                }
                image.bmp = null;
                image.bmp = BitmapFactory.decodeFile(img.getAbsolutePath(), options);
            } else if (image.bmp == null) {
                image.bmp = BitmapFactory.decodeResource(GameView.this.getResources(), GameView.this.getResourceByString(image.name), options);
            }
        }

        private void resizeBitmaps() {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inTempStorage = new byte[(this.IMG_BUFFER_LEN * 32)];
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = false;
            try {
                options.getClass().getField("inPurgeable").set(options, Boolean.TRUE);
                options.getClass().getField("inInputShareable").set(options, Boolean.TRUE);
            } catch (Exception e) {
            }
            scaleFrom(this.mBackground, options);
            for (BmpWrap scaleFrom : this.mBubbles) {
                scaleFrom(scaleFrom, options);
            }
            for (BmpWrap scaleFrom2 : this.mBubblesBlind) {
                scaleFrom(scaleFrom2, options);
            }
            for (BmpWrap scaleFrom3 : this.mFrozenBubbles) {
                scaleFrom(scaleFrom3, options);
            }
            for (BmpWrap scaleFrom4 : this.mTargetedBubbles) {
                scaleFrom(scaleFrom4, options);
            }
            scaleFrom(this.mBubbleBlink, options);
            scaleFrom(this.mGameLost, options);
            scaleFrom(this.mHurry, options);
            scaleFrom(this.mCompressorHead, options);
            scaleFrom(this.mCompressor, options);
            scaleFrom(this.mLife, options);
            scaleFrom(this.mFontImage, options);
            this.mImagesReady = true;
            FrozenBubble.fullScreenSwitch = false;
        }

        public void pauseOn() {
            synchronized (this.mSurfaceHolder) {
                if (this.mMode == 1) {
                    setState(2);
                }
            }
        }

        public void pauseOff() {
            synchronized (this.mSurfaceHolder) {
            }
            setState(1);
        }

        public void newGame() {
            synchronized (this.mSurfaceHolder) {
                this.mLevelManager.goToFirstLevel();
                this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameLost, this.mHurry, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager, GameView.this.mHandler);
            }
        }

        public void run() {
            while (this.mRun) {
                long now = System.currentTimeMillis();
                long delay = (40 + this.mLastTime) - now;
                if (delay > 0) {
                    try {
                        sleep(delay);
                    } catch (InterruptedException e) {
                    }
                }
                this.mLastTime = now;
                Canvas c = null;
                try {
                    if (surfaceOK() && (c = this.mSurfaceHolder.lockCanvas(null)) != null) {
                        synchronized (this.mSurfaceHolder) {
                            if (this.mRun) {
                                if (this.mMode == 1) {
                                    updateGameState();
                                }
                                doDraw(c);
                            }
                        }
                    }
                } finally {
                    if (c != null) {
                        this.mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        private void updateGameState() {
            if (this.mFrozenGame != null) {
                if (this.mFrozenGame.play(this.mLeft || this.mWasLeft, this.mRight || this.mWasRight, this.mFire || this.mUp || this.mWasFire || this.mWasUp || this.mTouchFire, this.mTrackballDX, this.mTouchDX)) {
                    Message msg = new Message();
                    msg.what = 1;
                    GameView.this.mHandler.sendMessage(msg);
                    this.mFrozenGame = new FrozenGame(this.mBackground, this.mBubbles, this.mBubblesBlind, this.mFrozenBubbles, this.mTargetedBubbles, this.mBubbleBlink, this.mGameLost, this.mHurry, this.mCompressorHead, this.mCompressor, this.mLauncher, this.mSoundManager, this.mLevelManager, GameView.this.mHandler);
                }
            }
            this.mWasLeft = false;
            this.mWasRight = false;
            this.mWasFire = false;
            this.mWasUp = false;
            this.mTrackballDX = 0.0d;
            this.mTouchFire = false;
            this.mTouchDX = 0.0d;
        }

        public Bundle saveState(Bundle map) {
            synchronized (this.mSurfaceHolder) {
                if (map != null) {
                    this.mFrozenGame.saveState(map);
                    this.mLevelManager.saveState(map);
                }
            }
            return map;
        }

        public synchronized void restoreState(Bundle map) {
            synchronized (this.mSurfaceHolder) {
                setState(2);
                this.mFrozenGame.restoreState(map, this.mImageList);
                this.mLevelManager.restoreState(map);
            }
        }

        public void setRunning(boolean b) {
            this.mRun = b;
        }

        public void setState(int mode) {
            synchronized (this.mSurfaceHolder) {
                this.mMode = mode;
            }
        }

        public void setSurfaceOK(boolean ok) {
            synchronized (this.mSurfaceHolder) {
                this.mSurfaceOK = ok;
            }
        }

        public boolean surfaceOK() {
            boolean z;
            synchronized (this.mSurfaceHolder) {
                z = this.mSurfaceOK;
            }
            return z;
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this.mSurfaceHolder) {
                GameView.mCanvasWidth = width;
                GameView.mCanvasHeight = height;
                if (width / height >= 0) {
                    this.mDisplayScale = (((double) height) * 1.0d) / 480.0d;
                    this.mDisplayDX = (int) ((((double) width) - (this.mDisplayScale * 640.0d)) / 2.0d);
                    this.mDisplayDY = 0;
                } else {
                    this.mDisplayScale = (((double) width) * 1.0d) / 320.0d;
                    this.mDisplayDX = (int) (((-this.mDisplayScale) * 320.0d) / 2.0d);
                    this.mDisplayDY = (int) ((((double) height) - (this.mDisplayScale * 480.0d)) / 2.0d);
                }
                resizeBitmaps();
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doKeyDown(int r4, android.view.KeyEvent r5) {
            /*
                r3 = this;
                r2 = 1
                android.view.SurfaceHolder r0 = r3.mSurfaceHolder
                monitor-enter(r0)
                boolean r1 = com.celliecraze.marblemadness.FrozenBubble.gamePausedActive     // Catch:{ all -> 0x004b }
                if (r1 != 0) goto L_0x0048
                int r1 = r3.mMode     // Catch:{ all -> 0x004b }
                if (r1 == r2) goto L_0x0010
                r1 = 1
                r3.setState(r1)     // Catch:{ all -> 0x004b }
            L_0x0010:
                int r1 = r3.mMode     // Catch:{ all -> 0x004b }
                if (r1 != r2) goto L_0x0048
                r1 = 21
                if (r4 != r1) goto L_0x0021
                r1 = 1
                r3.mLeft = r1     // Catch:{ all -> 0x004b }
                r1 = 1
                r3.mWasLeft = r1     // Catch:{ all -> 0x004b }
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                r0 = r2
            L_0x0020:
                return r0
            L_0x0021:
                r1 = 22
                if (r4 != r1) goto L_0x002e
                r1 = 1
                r3.mRight = r1     // Catch:{ all -> 0x004b }
                r1 = 1
                r3.mWasRight = r1     // Catch:{ all -> 0x004b }
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                r0 = r2
                goto L_0x0020
            L_0x002e:
                r1 = 23
                if (r4 != r1) goto L_0x003b
                r1 = 1
                r3.mFire = r1     // Catch:{ all -> 0x004b }
                r1 = 1
                r3.mWasFire = r1     // Catch:{ all -> 0x004b }
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                r0 = r2
                goto L_0x0020
            L_0x003b:
                r1 = 19
                if (r4 != r1) goto L_0x0048
                r1 = 1
                r3.mUp = r1     // Catch:{ all -> 0x004b }
                r1 = 1
                r3.mWasUp = r1     // Catch:{ all -> 0x004b }
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                r0 = r2
                goto L_0x0020
            L_0x0048:
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                r0 = 0
                goto L_0x0020
            L_0x004b:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x004b }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.marblemadness.GameView.GameThread.doKeyDown(int, android.view.KeyEvent):boolean");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doKeyUp(int r5, android.view.KeyEvent r6) {
            /*
                r4 = this;
                r3 = 0
                r2 = 1
                android.view.SurfaceHolder r0 = r4.mSurfaceHolder
                monitor-enter(r0)
                boolean r1 = com.celliecraze.marblemadness.FrozenBubble.gamePausedActive     // Catch:{ all -> 0x0041 }
                if (r1 != 0) goto L_0x003e
                int r1 = r4.mMode     // Catch:{ all -> 0x0041 }
                if (r1 != r2) goto L_0x003e
                r1 = 21
                if (r5 != r1) goto L_0x0017
                r1 = 0
                r4.mLeft = r1     // Catch:{ all -> 0x0041 }
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                r0 = r2
            L_0x0016:
                return r0
            L_0x0017:
                r1 = 22
                if (r5 != r1) goto L_0x0021
                r1 = 0
                r4.mRight = r1     // Catch:{ all -> 0x0041 }
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                r0 = r2
                goto L_0x0016
            L_0x0021:
                r1 = 23
                if (r5 != r1) goto L_0x0034
                r1 = 1
                com.celliecraze.marblemadness.GameView.centerFired = r1     // Catch:{ all -> 0x0041 }
                r1 = 0
                r4.mFire = r1     // Catch:{ all -> 0x0041 }
                int r1 = com.celliecraze.marblemadness.GameView.bubblesFired     // Catch:{ all -> 0x0041 }
                int r1 = r1 + 1
                com.celliecraze.marblemadness.GameView.bubblesFired = r1     // Catch:{ all -> 0x0041 }
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                r0 = r2
                goto L_0x0016
            L_0x0034:
                r1 = 19
                if (r5 != r1) goto L_0x003e
                r1 = 0
                r4.mUp = r1     // Catch:{ all -> 0x0041 }
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                r0 = r2
                goto L_0x0016
            L_0x003e:
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                r0 = r3
                goto L_0x0016
            L_0x0041:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0041 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.marblemadness.GameView.GameThread.doKeyUp(int, android.view.KeyEvent):boolean");
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doTrackballEvent(android.view.MotionEvent r9) {
            /*
                r8 = this;
                r7 = 1
                android.view.SurfaceHolder r0 = r8.mSurfaceHolder
                monitor-enter(r0)
                boolean r1 = com.celliecraze.marblemadness.FrozenBubble.gamePausedActive     // Catch:{ all -> 0x002e }
                if (r1 != 0) goto L_0x002b
                int r1 = r8.mMode     // Catch:{ all -> 0x002e }
                if (r1 == r7) goto L_0x0010
                r1 = 1
                r8.setState(r1)     // Catch:{ all -> 0x002e }
            L_0x0010:
                int r1 = r8.mMode     // Catch:{ all -> 0x002e }
                if (r1 != r7) goto L_0x002b
                int r1 = r9.getAction()     // Catch:{ all -> 0x002e }
                r2 = 2
                if (r1 != r2) goto L_0x002b
                double r1 = r8.mTrackballDX     // Catch:{ all -> 0x002e }
                float r3 = r9.getX()     // Catch:{ all -> 0x002e }
                double r3 = (double) r3     // Catch:{ all -> 0x002e }
                r5 = 4617315517961601024(0x4014000000000000, double:5.0)
                double r3 = r3 * r5
                double r1 = r1 + r3
                r8.mTrackballDX = r1     // Catch:{ all -> 0x002e }
                monitor-exit(r0)     // Catch:{ all -> 0x002e }
                r0 = r7
            L_0x002a:
                return r0
            L_0x002b:
                monitor-exit(r0)     // Catch:{ all -> 0x002e }
                r0 = 0
                goto L_0x002a
            L_0x002e:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x002e }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.marblemadness.GameView.GameThread.doTrackballEvent(android.view.MotionEvent):boolean");
        }

        private double xFromScr(float x) {
            return ((double) (x - ((float) this.mDisplayDX))) / this.mDisplayScale;
        }

        private double yFromScr(float y) {
            return ((double) (y - ((float) this.mDisplayDY))) / this.mDisplayScale;
        }

        /* access modifiers changed from: package-private */
        public boolean doTouchEvent(MotionEvent event) {
            synchronized (this.mSurfaceHolder) {
                if (!FrozenBubble.gamePausedActive) {
                    if (this.mMode != 1) {
                        setState(1);
                    }
                    double x = xFromScr(event.getX());
                    double y = yFromScr(event.getY());
                    if (FrozenBubble.swapBubblesKey && x >= 305.0d && x <= 330.0d && y >= 450.0d && y <= 475.0d) {
                        long now = System.currentTimeMillis();
                        if (now - this.LAST_TOUCH_BUBBLE_TIME > 300) {
                            this.LAST_TOUCH_BUBBLE_TIME = now;
                            this.mFrozenGame.swapBubbles();
                        }
                    }
                    if (event.getAction() == 0) {
                        if (y < TOUCH_FIRE_Y_THRESHOLD) {
                            GameView.centerFired = false;
                            if (FrozenBubble.gameModeKey == 0) {
                                setPosition(20.0d + ((Math.atan((x - 301.0d) / (389.0d - y)) / 1.5707963267948966d) * 19.0d));
                            }
                            this.mTouchFire = true;
                            GameView.bubblesFired++;
                        }
                        this.mTouchLastX = x;
                    } else if (event.getAction() == 2) {
                        if (y >= TOUCH_FIRE_Y_THRESHOLD) {
                            this.mTouchDX = (x - this.mTouchLastX) * TOUCH_COEFFICIENT;
                        }
                        this.mTouchLastX = x;
                    }
                }
            }
            return true;
        }

        public void setPosition(double value) {
            this.mFrozenGame.setPosition(value);
        }

        private void drawBackground(Canvas c) {
            Sprite.drawImage(this.mBackground, 0, 0, c, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
        }

        private void drawLevelNumber(Canvas canvas) {
            int level = this.mLevelManager.getLevelIndex() + 1;
            if (level < 10) {
                this.mFont.paintChar(Character.forDigit(level, 10), 204, 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else if (level < 100) {
                BubbleFont localBubbleFont2 = this.mFont;
                localBubbleFont2.paintChar(Character.forDigit(level % 10, 10), 204 + localBubbleFont2.paintChar(Character.forDigit(level / 10, 10), 204, 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else {
                BubbleFont localBubbleFont3 = this.mFont;
                int x = 204 + localBubbleFont3.paintChar(Character.forDigit(level / 100, 10), 204, 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
                int level2 = level - ((level / 100) * 100);
                localBubbleFont3.paintChar(Character.forDigit(level2 % 10, 10), x + localBubbleFont3.paintChar(Character.forDigit(level2 / 10, 10), x, 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), 430, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            }
        }

        private void drawScoreBubbles(Canvas canvas, int x, int y, int score) {
            if (score < 10) {
                this.mFont.paintChar(Character.forDigit(score, 10), x, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else if (score < 100) {
                BubbleFont localBubbleFont2 = this.mFont;
                localBubbleFont2.paintChar(Character.forDigit(score % 10, 10), x + localBubbleFont2.paintChar(Character.forDigit(score / 10, 10), x, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else if (score < 1000) {
                BubbleFont localBubbleFont3 = this.mFont;
                int x2 = x + localBubbleFont3.paintChar(Character.forDigit(score / 100, 10), x, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
                int score2 = score - ((score / 100) * 100);
                localBubbleFont3.paintChar(Character.forDigit(score2 % 10, 10), x2 + localBubbleFont3.paintChar(Character.forDigit(score2 / 10, 10), x2, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            } else {
                BubbleFont localBubbleFont4 = this.mFont;
                int x3 = x + localBubbleFont4.paintChar(Character.forDigit(score / 1000, 10), x, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
                int score3 = score - ((score / 1000) * 1000);
                int x4 = x3 + localBubbleFont4.paintChar(Character.forDigit(score3 / 100, 10), x3, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
                int score4 = score3 - ((score3 / 100) * 100);
                localBubbleFont4.paintChar(Character.forDigit(score4 % 10, 10), x4 + localBubbleFont4.paintChar(Character.forDigit(score4 / 10, 10), x4, y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY), y, canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            }
        }

        private void doDraw(Canvas canvas) {
            if (this.mImagesReady) {
                if (this.mDisplayDX > 0 || this.mDisplayDY > 0) {
                    canvas.drawRGB(0, 0, 0);
                }
                drawBackground(canvas);
                drawLevelNumber(canvas);
                drawScoreBubbles(canvas, 378, 430, HighscoreManager.getActualScore(FrozenGame.nbBubbles));
                this.mFrozenGame.paint(canvas, this.mDisplayScale, this.mDisplayDX, this.mDisplayDY);
            }
        }

        public void cleanUp() {
            synchronized (this.mSurfaceHolder) {
                this.mImagesReady = false;
                if (this.mBackground.bmp != null) {
                    this.mBackground.bmp.recycle();
                }
                for (int i = 0; i < this.mBubbles.length; i++) {
                    if (this.mBubbles[i].bmp != null) {
                        this.mBubbles[i].bmp.recycle();
                    }
                }
                for (int i2 = 0; i2 < this.mBubblesBlind.length; i2++) {
                    if (this.mBubblesBlind[i2].bmp != null) {
                        this.mBubblesBlind[i2].bmp.recycle();
                    }
                }
                for (int i3 = 0; i3 < this.mFrozenBubbles.length; i3++) {
                    if (this.mFrozenBubbles[i3].bmp != null) {
                        this.mFrozenBubbles[i3].bmp.recycle();
                    }
                }
                for (int i4 = 0; i4 < this.mTargetedBubbles.length; i4++) {
                    if (this.mTargetedBubbles[i4].bmp != null) {
                        this.mTargetedBubbles[i4].bmp.recycle();
                    }
                }
                if (this.mBubbleBlink.bmp != null) {
                    this.mBubbleBlink.bmp.recycle();
                }
                if (this.mGameLost.bmp != null) {
                    this.mGameLost.bmp.recycle();
                }
                if (this.mHurry.bmp != null) {
                    this.mHurry.bmp.recycle();
                }
                if (this.mCompressorHead.bmp != null) {
                    this.mCompressorHead.bmp.recycle();
                }
                if (this.mCompressor.bmp != null) {
                    this.mCompressor.bmp.recycle();
                }
                if (this.mLife.bmp != null) {
                    this.mLife.bmp.recycle();
                }
                this.mBackground.bmp = null;
                this.mBackground = null;
                for (int i5 = 0; i5 < this.mBubbles.length; i5++) {
                    this.mBubbles[i5].bmp = null;
                    this.mBubbles[i5] = null;
                }
                this.mBubbles = null;
                for (int i6 = 0; i6 < this.mBubblesBlind.length; i6++) {
                    this.mBubblesBlind[i6].bmp = null;
                    this.mBubblesBlind[i6] = null;
                }
                this.mBubblesBlind = null;
                for (int i7 = 0; i7 < this.mFrozenBubbles.length; i7++) {
                    this.mFrozenBubbles[i7].bmp = null;
                    this.mFrozenBubbles[i7] = null;
                }
                this.mFrozenBubbles = null;
                for (int i8 = 0; i8 < this.mTargetedBubbles.length; i8++) {
                    this.mTargetedBubbles[i8].bmp = null;
                    this.mTargetedBubbles[i8] = null;
                }
                this.mTargetedBubbles = null;
                this.mBubbleBlink.bmp = null;
                this.mBubbleBlink = null;
                this.mGameLost.bmp = null;
                this.mGameLost = null;
                this.mHurry.bmp = null;
                this.mHurry = null;
                this.mCompressorHead.bmp = null;
                this.mCompressorHead = null;
                this.mCompressor.bmp = null;
                this.mCompressor = null;
                this.mLife.bmp = null;
                this.mLife = null;
                this.mImageList = null;
                this.mSoundManager.cleanUp();
                this.mSoundManager = null;
                this.mLevelManager = null;
                this.mFrozenGame = null;
            }
        }
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void initThread(byte[] levels, int startingLevel) {
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.thread = new GameThread(holder, levels, startingLevel);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.thread.setRunning(true);
        this.thread.start();
    }

    public GameThread getThread() {
        return this.thread;
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            this.thread.pauseOn();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return this.thread.doKeyDown(keyCode, msg);
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return this.thread.doKeyUp(keyCode, msg);
    }

    public boolean onTrackballEvent(MotionEvent event) {
        return this.thread.doTrackballEvent(event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.thread.doTouchEvent(event);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.thread.setSurfaceOK(true);
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.thread.setSurfaceOK(false);
    }

    public void cleanUp() {
        this.thread.cleanUp();
        this.mContext = null;
        System.gc();
    }

    /* access modifiers changed from: private */
    public int getResourceByString(String name) {
        for (int i = 0; i < BubbleBusterSplashActivity.names.length; i++) {
            if (BubbleBusterSplashActivity.names[i].equals(name)) {
                return BubbleBusterSplashActivity.namesID[i];
            }
        }
        return 0;
    }
}
