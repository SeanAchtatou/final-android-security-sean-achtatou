package com.celliecraze.marblemadness;

public interface AccelerometerListener {
    void onAccelerationChanged(float f, float f2, float f3);
}
