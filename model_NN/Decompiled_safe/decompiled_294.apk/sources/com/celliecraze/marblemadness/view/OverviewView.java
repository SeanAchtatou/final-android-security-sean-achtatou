package com.celliecraze.marblemadness.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.celliecraze.marblemadness.LevelManager;
import com.celliecraze.marblemadness.R;
import com.celliecraze.marblemadness.activity.BubbleBusterOverviewActivity;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.highscores.DB;
import com.celliecraze.marblemadness.highscores.HighScore;
import java.util.Arrays;

public class OverviewView extends View {
    protected static int EDGE_SIZE = 10;
    protected static final int levelSize = 75;
    protected static int mHeight = BubbleBusterConstants.GAMEFIELD_HEIGHT;
    protected static int mWidth = BubbleBusterConstants.GAMEFIELD_WIDTH;
    protected static Bitmap small1;
    protected static Bitmap small2;
    protected static Bitmap small3;
    protected static Bitmap small4;
    protected static Bitmap small5;
    protected static Bitmap small6;
    protected static Bitmap small7;
    protected static Bitmap small8;
    protected static Bitmap smallLocked;
    protected boolean clickable = false;
    private boolean dimmSquare = false;
    protected boolean drawNumbers;
    protected int levelClicked = -1;
    protected int[] levelCounts;
    protected byte[] levels;
    private boolean litUpSquare = false;
    protected Paint mBitmapPaint;
    protected Canvas mCanvas;
    protected Context mContext;
    protected Paint mPaint;
    protected int startingLevel = 0;
    private Typeface tf;
    protected float touchedX = 0.0f;
    protected float touchedY = 0.0f;

    public int getLevelClicked() {
        return this.levelClicked;
    }

    public float getTouchedX() {
        return this.touchedX;
    }

    public float getTouchedY() {
        return this.touchedY;
    }

    public OverviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        determineDisplaySize();
        this.mPaint = new Paint();
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeJoin(Paint.Join.MITER);
        this.mPaint.setStrokeCap(Paint.Cap.SQUARE);
        this.mPaint.setColor(-16777216);
        this.mBitmapPaint = new Paint(4);
        this.drawNumbers = true;
        this.tf = Typeface.createFromAsset(this.mContext.getAssets(), "fonts/utb.ttf");
    }

    /* access modifiers changed from: protected */
    public void determineDisplaySize() {
        Display display = ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay();
        mWidth = display.getWidth();
        mHeight = display.getHeight();
        EDGE_SIZE = mWidth / 30;
        small1 = BitmapFactory.decodeResource(getResources(), R.drawable.small_1);
        small2 = BitmapFactory.decodeResource(getResources(), R.drawable.small_2);
        small3 = BitmapFactory.decodeResource(getResources(), R.drawable.small_3);
        small4 = BitmapFactory.decodeResource(getResources(), R.drawable.small_4);
        small5 = BitmapFactory.decodeResource(getResources(), R.drawable.small_5);
        small6 = BitmapFactory.decodeResource(getResources(), R.drawable.small_6);
        small7 = BitmapFactory.decodeResource(getResources(), R.drawable.small_7);
        small8 = BitmapFactory.decodeResource(getResources(), R.drawable.small_8);
        smallLocked = BitmapFactory.decodeResource(getResources(), R.drawable.small_locked);
    }

    public byte[] getLevels() {
        return this.levels;
    }

    public void setLevels(byte[] levels2) {
        this.levels = levels2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int size;
        super.onDraw(canvas);
        this.mCanvas = canvas;
        if (this.litUpSquare) {
            double whichX = Math.floor((double) (this.touchedX / ((float) (mWidth / 3))));
            double whichY = Math.floor((double) (this.touchedY / ((float) (mHeight / 4))));
            this.mPaint.setColor(-16724737);
            this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            this.mCanvas.drawRect(((float) whichX) * ((float) (mWidth / 3)), ((float) whichY) * ((float) (mHeight / 4)), (((float) whichX) * ((float) (mWidth / 3))) + ((float) (mWidth / 3)), (((float) whichY) * ((float) (mHeight / 4))) + ((float) (mHeight / 4)), this.mPaint);
            this.mPaint.setStyle(Paint.Style.STROKE);
        }
        if (this.dimmSquare) {
            double whichX2 = Math.floor((double) (this.touchedX / ((float) (mWidth / 3))));
            double whichY2 = Math.floor((double) (this.touchedY / ((float) (mHeight / 4))));
            this.mPaint.setColor(-16777216);
            this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            this.mCanvas.drawRect(((float) whichX2) * ((float) (mWidth / 3)), ((float) whichY2) * ((float) (mHeight / 4)), (((float) whichX2) * ((float) (mWidth / 3))) + ((float) (mWidth / 3)), (((float) whichY2) * ((float) (mHeight / 4))) + ((float) (mHeight / 4)), this.mPaint);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.litUpSquare = false;
        }
        this.mPaint.setColor(-6710887);
        this.mPaint.setStrokeWidth(1.0f);
        canvas.drawLine((float) (mWidth / 3), 0.0f, (float) (mWidth / 3), (float) ((mHeight * 3) / 4), this.mPaint);
        canvas.drawLine((float) ((mWidth * 2) / 3), 0.0f, (float) ((mWidth * 2) / 3), (float) ((mHeight * 3) / 4), this.mPaint);
        canvas.drawLine(0.0f, 0.0f, (float) mWidth, 0.0f, this.mPaint);
        canvas.drawLine(0.0f, (float) (mHeight / 4), (float) mWidth, (float) (mHeight / 4), this.mPaint);
        canvas.drawLine(0.0f, (float) ((mHeight * 2) / 4), (float) mWidth, (float) ((mHeight * 2) / 4), this.mPaint);
        canvas.drawLine(0.0f, (float) ((mHeight * 3) / 4), (float) mWidth, (float) ((mHeight * 3) / 4), this.mPaint);
        byte[] levelsToDraw = new byte[675];
        Arrays.fill(levelsToDraw, (byte) -1);
        if (this.levels != null) {
            if (this.levels.length - (this.startingLevel * levelSize) < 675) {
                size = this.levels.length - (this.startingLevel * levelSize);
            } else {
                size = 675;
            }
            System.arraycopy(this.levels, this.startingLevel * levelSize, levelsToDraw, 0, size);
            int levelPointer = 0;
            int rowPointer = 0;
            int colPointer = -1;
            int levelRow = 0;
            int levelCol = 0;
            boolean even = false;
            for (int i = 0; i < 675; i++) {
                if (levelPointer > 74) {
                    levelPointer = 0;
                    rowPointer = -1;
                    levelCol++;
                    if (levelCol > 2) {
                        levelRow++;
                        levelCol = 0;
                        if (levelRow > 2) {
                            levelRow = 0;
                        }
                    }
                }
                if (even) {
                    if (colPointer >= 6) {
                        colPointer = 0;
                        rowPointer++;
                        even = !even;
                    } else {
                        colPointer++;
                    }
                } else if (colPointer >= 7) {
                    colPointer = 0;
                    rowPointer++;
                    even = !even;
                } else {
                    colPointer++;
                }
                if ((this.startingLevel * levelSize) + i > (BubbleBusterOverviewActivity.unlockedLevel * levelSize) - 1) {
                    drawBubble(levelRow, levelCol, rowPointer, colPointer, smallLocked, canvas);
                } else {
                    switch (levelsToDraw[i]) {
                        case 0:
                        case 48:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small1, canvas);
                            continue;
                        case 1:
                        case 49:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small2, canvas);
                            continue;
                        case 2:
                        case BubbleBusterConstants.shortVibe:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small3, canvas);
                            continue;
                        case 3:
                        case 51:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small4, canvas);
                            continue;
                        case 4:
                        case 52:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small5, canvas);
                            continue;
                        case 5:
                        case 53:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small6, canvas);
                            continue;
                        case 6:
                        case 54:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small7, canvas);
                            continue;
                        case 7:
                        case 55:
                            drawBubble(levelRow, levelCol, rowPointer, colPointer, small8, canvas);
                            continue;
                    }
                }
                levelPointer++;
            }
            if (this.drawNumbers) {
                this.mPaint.setColor(-1);
                this.mPaint.setTypeface(this.tf);
                this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                String text = "";
                for (int col = 0; col < 3; col++) {
                    int row = 1;
                    while (row <= 3) {
                        if (this.levelCounts == null) {
                            int level = this.startingLevel + ((row - 1) * 3) + col + 1;
                            if (level <= this.levels.length / levelSize) {
                                text = new StringBuilder(String.valueOf(level - (BubbleBusterOverviewActivity.levelPack * 100))).toString();
                            } else {
                                text = "";
                            }
                        }
                        int level2 = this.startingLevel + ((row - 1) * 3) + col;
                        int levelText = (int) ((28.0f * getResources().getDisplayMetrics().density) + 0.5f);
                        this.mPaint.setTextSize((float) levelText);
                        canvas.drawText(text, (float) ((mWidth * col) / 3), (float) ((((row - 1) * mHeight) / 4) + levelText), this.mPaint);
                        this.mPaint.setTextSize((float) ((int) ((34.0f * getResources().getDisplayMetrics().density) + 0.5f)));
                        try {
                            long score = BubbleBusterOverviewActivity.highscores[level2];
                            if (score != 0 || level2 < BubbleBusterOverviewActivity.unlockedLevel) {
                                text = String.valueOf(score);
                                canvas.drawText(text, (float) ((mWidth * col) / 3), (float) ((mHeight * row) / 4), this.mPaint);
                                row++;
                            } else {
                                text = "";
                                canvas.drawText(text, (float) ((mWidth * col) / 3), (float) ((mHeight * row) / 4), this.mPaint);
                                row++;
                            }
                        } catch (Exception e) {
                            text = "";
                        }
                    }
                }
                return;
            }
            return;
        }
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this.mContext);
        alt_bld.setMessage((int) R.string.overview_levels_not_found).setCancelable(false).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alt_bld.create();
        alert.setTitle((int) R.string.app_name);
        alert.setIcon((int) R.drawable.dialog);
        alert.show();
    }

    /* access modifiers changed from: protected */
    public void drawBubble(int levelRow, int levelCol, int rowPointer, int colPointer, Bitmap smallBubble, Canvas canvas) {
        float left = (float) (((mWidth / 3) * levelCol) + (EDGE_SIZE * colPointer) + ((EDGE_SIZE * 3) / 2));
        float top = (float) (((mHeight / 4) * levelRow) + (EDGE_SIZE * rowPointer) + EDGE_SIZE);
        if (rowPointer % 2 == 1) {
            left += (float) (EDGE_SIZE / 2);
        }
        canvas.drawBitmap(smallBubble, left, top, this.mPaint);
    }

    public void setStartingLevel(int startingLevel2) {
        if (startingLevel2 >= 0 && startingLevel2 < this.levels.length / levelSize) {
            this.startingLevel = startingLevel2;
        } else if (startingLevel2 < 0) {
            this.startingLevel = 0;
        }
    }

    public int getStartingLevel() {
        return this.startingLevel;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.clickable) {
            return true;
        }
        switch (event.getAction()) {
            case 0:
                BubbleBusterConstants.vibrate(this.mContext, 50);
                this.touchedX = event.getX();
                this.touchedY = event.getY();
                litUpSquare(this.touchedX, this.touchedY);
                invalidate();
                break;
            case 1:
                dimmSquare(this.touchedX, this.touchedY);
                invalidate();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void litUpSquare(float x, float y) {
        this.litUpSquare = true;
        this.dimmSquare = false;
    }

    /* access modifiers changed from: protected */
    public void dimmSquare(float x, float y) {
        this.dimmSquare = true;
        double whichX = Math.floor((double) (x / ((float) (mWidth / 3))));
        double whichY = Math.floor((double) (y / ((float) (mHeight / 4))));
        this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mPaint.setStyle(Paint.Style.STROKE);
        int whichLevelClicked = (int) ((3.0d * whichY) + whichX + 1.0d + ((double) this.startingLevel));
        if (whichLevelClicked <= this.levels.length / levelSize) {
            this.levelClicked = whichLevelClicked;
            if (y >= ((float) ((mHeight * 3) / 4))) {
                return;
            }
            if (whichLevelClicked <= BubbleBusterOverviewActivity.unlockedLevel) {
                showActionsDialog(whichLevelClicked);
                return;
            }
            View myDialog = LayoutInflater.from(this.mContext).inflate((int) R.layout.ll_dialog, (ViewGroup) null);
            ((TextView) myDialog.findViewById(R.id.app_name)).setTypeface(this.tf);
            ((TextView) myDialog.findViewById(R.id.level_locked)).setTypeface(this.tf);
            ((Button) myDialog.findViewById(R.id.b_cancel)).setTypeface(this.tf);
            final AlertDialog a = new AlertDialog.Builder(this.mContext).setView(myDialog).create();
            a.setCancelable(false);
            myDialog.findViewById(R.id.b_cancel).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                    a.dismiss();
                }
            });
            a.show();
        }
    }

    /* access modifiers changed from: protected */
    public void showActionsDialog(int which) {
        long timeBonus;
        setClickable(false);
        HighScore hs = DB.getHighScore(this.mContext, which - 1);
        if (hs.getScore() == 0) {
            View textEntryView2 = LayoutInflater.from(this.mContext).inflate((int) R.layout.standard_dialog_noscore, (ViewGroup) null);
            ((TextView) textEntryView2.findViewById(R.id.app_name)).setTypeface(this.tf);
            ((TextView) textEntryView2.findViewById(R.id.starting_level_title)).setTypeface(this.tf);
            ((TextView) textEntryView2.findViewById(R.id.starting_level_note)).setTypeface(this.tf);
            ((Button) textEntryView2.findViewById(R.id.b_ok)).setTypeface(this.tf);
            ((Button) textEntryView2.findViewById(R.id.b_cancel)).setTypeface(this.tf);
            AlertDialog a = new AlertDialog.Builder(this.mContext).setView(textEntryView2).create();
            a.setCancelable(false);
            final AlertDialog alertDialog = a;
            textEntryView2.findViewById(R.id.b_ok).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                    ((BubbleBusterOverviewActivity) OverviewView.this.mContext).onClick(alertDialog, -1);
                    alertDialog.dismiss();
                }
            });
            final AlertDialog alertDialog2 = a;
            textEntryView2.findViewById(R.id.b_cancel).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    OverviewView.this.setClickable(true);
                    BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                    alertDialog2.dismiss();
                }
            });
            ((TextView) textEntryView2.findViewById(R.id.starting_level_title)).setText(this.mContext.getString(R.string.over_selected_level, Integer.valueOf(which)));
            a.show();
            return;
        }
        View textEntryView = LayoutInflater.from(this.mContext).inflate((int) R.layout.standard_dialog, (ViewGroup) null);
        ((TextView) textEntryView.findViewById(R.id.app_name)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.starting_level_title)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.starting_level_note)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_score_total)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_bubbles)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_time)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_score)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_time_bonus)).setTypeface(this.tf);
        ((Button) textEntryView.findViewById(R.id.b_ok)).setTypeface(this.tf);
        ((Button) textEntryView.findViewById(R.id.b_reset_level_score)).setTypeface(this.tf);
        ((Button) textEntryView.findViewById(R.id.b_cancel)).setTypeface(this.tf);
        ((TextView) textEntryView.findViewById(R.id.t_msg_score_total)).setText(this.mContext.getString(R.string.level_high_score_total, Long.valueOf(hs.getScore())));
        ((TextView) textEntryView.findViewById(R.id.t_msg_bubbles)).setText(this.mContext.getString(R.string.bubbles_fired, Integer.valueOf(hs.getBubbles())));
        ((TextView) textEntryView.findViewById(R.id.t_msg_time)).setText(this.mContext.getString(R.string.time_played, LevelManager.getFormattedTime(hs.getTime(), true)));
        TextView textView = (TextView) textEntryView.findViewById(R.id.t_msg_score);
        Context context = this.mContext;
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(hs.getBubbles() == 0 ? 0 : 1040 - (hs.getBubbles() * 5));
        textView.setText(context.getString(R.string.score, objArr));
        if (hs.getTime() > 300) {
            timeBonus = 0;
        } else {
            timeBonus = hs.getScore() - ((long) (1040 - (hs.getBubbles() * 5)));
        }
        if (timeBonus < 0) {
            timeBonus = 0;
        }
        ((TextView) textEntryView.findViewById(R.id.t_msg_time_bonus)).setText(this.mContext.getString(R.string.time_bonus, Long.valueOf(timeBonus)));
        AlertDialog b = new AlertDialog.Builder(this.mContext).setView(textEntryView).create();
        b.setCancelable(false);
        final AlertDialog alertDialog3 = b;
        textEntryView.findViewById(R.id.b_ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                ((BubbleBusterOverviewActivity) OverviewView.this.mContext).onClick(alertDialog3, -1);
                alertDialog3.dismiss();
            }
        });
        final int i = which;
        final AlertDialog alertDialog4 = b;
        textEntryView.findViewById(R.id.b_reset_level_score).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                DB.resetLevelHighScore(OverviewView.this.mContext, i - 1);
                ((BubbleBusterOverviewActivity) OverviewView.this.mContext).onClick(alertDialog4, -1);
                alertDialog4.dismiss();
            }
        });
        final AlertDialog alertDialog5 = b;
        textEntryView.findViewById(R.id.b_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BubbleBusterConstants.vibrate(OverviewView.this.mContext, 50);
                OverviewView.this.setClickable(true);
                alertDialog5.dismiss();
            }
        });
        ((TextView) textEntryView.findViewById(R.id.starting_level_title)).setText(this.mContext.getString(R.string.over_selected_level, Integer.valueOf(which)));
        b.show();
    }

    public void setLevelCounts(int[] levelCounts2) {
        this.levelCounts = levelCounts2;
    }

    public int[] getLevelCounts() {
        return this.levelCounts;
    }

    public boolean isClickable() {
        return this.clickable;
    }

    public void setClickable(boolean clickable2) {
        this.clickable = clickable2;
    }
}
