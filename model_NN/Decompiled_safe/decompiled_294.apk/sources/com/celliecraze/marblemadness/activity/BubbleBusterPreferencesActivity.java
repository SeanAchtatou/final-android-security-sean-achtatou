package com.celliecraze.marblemadness.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.util.Log;
import android.view.KeyEvent;
import com.celliecraze.marblemadness.AccelerometerManager;
import com.celliecraze.marblemadness.MMApplication;
import com.celliecraze.marblemadness.R;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.social.FacebookSessionEvents;
import com.celliecraze.marblemadness.social.FacebookSessionStore;
import com.celliecraze.marblemadness.social.TwitterLoginPreference;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class BubbleBusterPreferencesActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    /* access modifiers changed from: private */
    public MMApplication app;
    private FacebookSessionEvents.AuthListener authListener;
    /* access modifiers changed from: private */
    public Preference fbLogin;
    private FacebookSessionEvents.LogoutListener logoutListener;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private TwitterLoginPreference twLogin;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        this.authListener = new OpcionesAuthListener();
        this.logoutListener = new OpcionesLogoutListener();
        FacebookSessionEvents.addAuthListener(this.authListener);
        FacebookSessionEvents.addLogoutListener(this.logoutListener);
        this.mHandler = new Handler();
        if (!AccelerometerManager.isSupported(getApplicationContext())) {
            ListPreference accelMode = (ListPreference) findPreference("gameModeKey");
            accelMode.setEntries(new String[]{"Auto Mode", "Manual Mode"});
            accelMode.setEntryValues(new String[]{"0", "1"});
            ((PreferenceGroup) findPreference("categoryLauncherControlKey")).removePreference(findPreference("invertAccelKey"));
        }
        this.app = (MMApplication) getApplication();
        this.twLogin = (TwitterLoginPreference) findPreference("twLogin");
        this.twLogin.setWidgetLayoutResource(R.layout.twitter_login_pref);
        this.twLogin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (!BubbleBusterPreferencesActivity.this.app.isTwitterAuthorized()) {
                    BubbleBusterPreferencesActivity.this.startActivity(new Intent(BubbleBusterPreferencesActivity.this.getApplicationContext(), TwitterAuthorizationActivity.class));
                    return true;
                }
                BubbleBusterPreferencesActivity.this.app.logoutTwitter();
                BubbleBusterPreferencesActivity.this.twitterCheck();
                return true;
            }
        });
        this.fbLogin = findPreference("fbLogin");
        this.fbLogin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (!BubbleBusterPreferencesActivity.this.app.isFacebookAuthorized().booleanValue()) {
                    BubbleBusterPreferencesActivity.this.app.beginFacebookAuthorization(BubbleBusterPreferencesActivity.this, new FacebookSessionEvents.LoginDialogListener());
                    return true;
                }
                BubbleBusterPreferencesActivity.this.app.logoutFacebook(BubbleBusterPreferencesActivity.this.mHandler);
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        FacebookSessionEvents.removeAuthListener(this.authListener);
        FacebookSessionEvents.removeLogoutListener(this.logoutListener);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        twitterCheck();
        facebookCheck();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: private */
    public void facebookCheck() {
        String title = getString(R.string.fb_login_title);
        String summary = getString(R.string.fb_login_summary);
        if (this.app.isFacebookAuthorized().booleanValue()) {
            title = getString(R.string.fb_logout_title);
            summary = getString(R.string.fb_logout_summary);
        }
        this.fbLogin.setTitle(title);
        this.fbLogin.setSummary(summary);
    }

    /* access modifiers changed from: private */
    public void twitterCheck() {
        String title = getString(R.string.tw_login_title);
        String summary = getString(R.string.tw_login_summary);
        if (this.app.isTwitterAuthorized()) {
            try {
                Twitter tw = this.app.getTwitter();
                title = getString(R.string.tw_logout_title);
                summary = String.format(getString(R.string.tw_logout_summary), tw.getScreenName());
            } catch (TwitterException e) {
                Log.e(BubbleBusterConstants.TAG, e.getMessage());
            } catch (IllegalStateException e2) {
                Log.e(BubbleBusterConstants.TAG, e2.getMessage());
            }
        } else {
            this.twLogin.mTwitter = null;
        }
        this.twLogin.setTitle(title);
        this.twLogin.setSummary(summary);
    }

    public class OpcionesAuthListener implements FacebookSessionEvents.AuthListener {
        public OpcionesAuthListener() {
        }

        public void onAuthSucceed() {
            BubbleBusterPreferencesActivity.this.facebookCheck();
        }

        public void onAuthFail(String error) {
        }
    }

    public class OpcionesLogoutListener implements FacebookSessionEvents.LogoutListener {
        public OpcionesLogoutListener() {
        }

        public void onLogoutBegin() {
            BubbleBusterPreferencesActivity.this.fbLogin.setEnabled(false);
        }

        public void onLogoutFinish() {
            FacebookSessionStore.clear(BubbleBusterPreferencesActivity.this.getApplicationContext());
            BubbleBusterPreferencesActivity.this.facebookCheck();
            BubbleBusterPreferencesActivity.this.fbLogin.setEnabled(true);
        }

        public boolean onPreferenceChange(Preference preference, Object newValue) {
            return false;
        }
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return false;
    }
}
