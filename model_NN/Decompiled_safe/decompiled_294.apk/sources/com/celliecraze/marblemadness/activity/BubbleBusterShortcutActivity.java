package com.celliecraze.marblemadness.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.celliecraze.marblemadness.R;

public class BubbleBusterShortcutActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupShortcut();
        finish();
    }

    private void setupShortcut() {
        Intent shortcutIntent = new Intent();
        shortcutIntent.setClass(this, BubbleBusterSplashActivity.class);
        shortcutIntent.addFlags(268435456);
        shortcutIntent.addFlags(67108864);
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent);
        intent.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        setResult(-1, intent);
        Toast.makeText(this, (int) R.string.app_name, 0).show();
    }
}
