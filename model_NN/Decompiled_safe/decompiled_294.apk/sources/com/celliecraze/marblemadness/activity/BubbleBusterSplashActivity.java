package com.celliecraze.marblemadness.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.celliecraze.marblemadness.FrozenBubble;
import com.celliecraze.marblemadness.R;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import java.io.BufferedOutputStream;
import java.io.File;

public class BubbleBusterSplashActivity extends Activity {
    private static final String HEIGHT = "height";
    private static final String STATUS = "status";
    private static final int STATUS_HEIGHT_SET = 300;
    private static final String WIDTH = "width";
    public static final String[] names = {"mBackground", "mBubbles0", "mBubbles1", "mBubbles2", "mBubbles3", "mBubbles4", "mBubbles5", "mBubbles6", "mBubbles7", "mBubblesBlind0", "mBubblesBlind1", "mBubblesBlind2", "mBubblesBlind3", "mBubblesBlind4", "mBubblesBlind5", "mBubblesBlind6", "mBubblesBlind7", "mFrozenBubbles0", "mFrozenBubbles1", "mFrozenBubbles2", "mFrozenBubbles3", "mFrozenBubbles4", "mFrozenBubbles5", "mFrozenBubbles6", "mFrozenBubbles7", "mTargetedBubbles0", "mTargetedBubbles1", "mTargetedBubbles2", "mTargetedBubbles3", "mTargetedBubbles4", "mTargetedBubbles5", "mBubbleBlink", "mGameLost", "mHurry", "mCompressorHead", "mCompressor", "mLife", "mFontImage"};
    public static final int[] namesID = {R.drawable.background, R.drawable.bubble_1, R.drawable.bubble_2, R.drawable.bubble_3, R.drawable.bubble_4, R.drawable.bubble_5, R.drawable.bubble_6, R.drawable.bubble_7, R.drawable.bubble_8, R.drawable.bubble_colourblind_1, R.drawable.bubble_colourblind_2, R.drawable.bubble_colourblind_3, R.drawable.bubble_colourblind_4, R.drawable.bubble_colourblind_5, R.drawable.bubble_colourblind_6, R.drawable.bubble_colourblind_7, R.drawable.bubble_colourblind_8, R.drawable.frozen_1, R.drawable.frozen_2, R.drawable.frozen_3, R.drawable.frozen_4, R.drawable.frozen_5, R.drawable.frozen_6, R.drawable.frozen_7, R.drawable.frozen_8, R.drawable.fixed_1, R.drawable.fixed_2, R.drawable.fixed_3, R.drawable.fixed_4, R.drawable.fixed_5, R.drawable.fixed_6, R.drawable.bubble_blink, R.drawable.lose_panel, R.drawable.hurry, R.drawable.compressor, R.drawable.compressor_body, R.drawable.life, R.drawable.bubble_font};
    private int IMG_BUFFER_LEN = 1024;
    private Handler handler = new Handler() {
        boolean running = false;

        public void handleMessage(Message msg) {
            if (!this.running) {
                switch (msg.what) {
                    case 300:
                        this.running = true;
                        final int width = msg.getData().getInt("width");
                        final int height = msg.getData().getInt("height");
                        final int status = msg.getData().getInt(BubbleBusterSplashActivity.STATUS);
                        BubbleBusterSplashActivity.this.mSplashThread = new Thread() {
                            public void run() {
                                BubbleBusterSplashActivity.this.resizeImages(false, width, height, status);
                                System.gc();
                                BubbleBusterSplashActivity.this.resizeImages(true, width, height, status);
                                BubbleBusterSplashActivity.this.resizeFinished = true;
                                try {
                                    synchronized (this) {
                                        wait(4000);
                                    }
                                    BubbleBusterSplashActivity.this.proceed();
                                } catch (InterruptedException e) {
                                }
                            }
                        };
                        BubbleBusterSplashActivity.this.mSplashThread.start();
                        return;
                    default:
                        return;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public Thread mSplashThread;
    /* access modifiers changed from: private */
    public boolean resizeFinished = false;
    ImageView splashImageView;

    public void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(1024, 1024);
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            ScoreloopManagerSingleton.get().showWelcomeBackToast(0);
        }
        setContentView((int) R.layout.splash);
        this.splashImageView = (ImageView) findViewById(R.id.SplashImageView);
        this.splashImageView.setBackgroundResource(R.anim.splash_anim);
        final AnimationDrawable frameAnimation = (AnimationDrawable) this.splashImageView.getBackground();
        this.splashImageView.post(new Runnable() {
            public void run() {
                frameAnimation.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void resizeImages(boolean withStatusBar, int width, int height, int status) {
        double mDisplayScale;
        String imagePng;
        String image;
        if (withStatusBar) {
            height -= status;
        }
        if (width / height >= 0) {
            mDisplayScale = (1.0d * ((double) height)) / 480.0d;
        } else {
            mDisplayScale = (1.0d * ((double) width)) / 320.0d;
        }
        Resources res = getResources();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inTempStorage = new byte[(this.IMG_BUFFER_LEN * 32)];
        try {
            options.getClass().getField("inScaled").set(options, Boolean.FALSE);
        } catch (Exception e) {
        }
        for (int i = 0; i < names.length; i++) {
            String imageJpg = withStatusBar ? String.valueOf(names[i]) + ".stat.jpg" : String.valueOf(names[i]) + ".jpg";
            if (withStatusBar) {
                imagePng = String.valueOf(names[i]) + ".stat.png";
            } else {
                imagePng = String.valueOf(names[i]) + ".png";
            }
            File root = getFilesDir();
            File file = new File(root, imageJpg);
            File file2 = new File(root, imagePng);
            if (!file.exists() && !file2.exists()) {
                boolean jpeg = false;
                Bitmap source = BitmapFactory.decodeResource(res, namesID[i], options);
                if (options.outMimeType.contains("jpeg")) {
                    jpeg = true;
                    image = imageJpg;
                } else {
                    image = imagePng;
                }
                Bitmap target = Bitmap.createScaledBitmap(source, (int) (((double) source.getWidth()) * mDisplayScale), (int) (((double) source.getHeight()) * mDisplayScale), true);
                try {
                    BufferedOutputStream bos = new BufferedOutputStream(openFileOutput(image, 0));
                    if (jpeg) {
                        target.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    } else {
                        target.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    }
                    bos.flush();
                    bos.close();
                } catch (Exception e2) {
                    Log.e(BubbleBusterConstants.TAG, "bitmap could not be converted", e2);
                }
                source.recycle();
                target.recycle();
                System.gc();
            }
        }
    }

    public void handleResize(int width, int height, int status) {
        Message msg = Message.obtain();
        msg.what = 300;
        Bundle bundle = new Bundle();
        bundle.putInt("width", width);
        bundle.putInt("height", height);
        bundle.putInt(STATUS, status);
        msg.setData(bundle);
        this.handler.sendMessage(msg);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.resizeFinished && event.getAction() == 0) {
            proceed();
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.resizeFinished && (keyCode == 23 || keyCode == 21 || keyCode == 22 || keyCode == 19 || keyCode == 20)) {
            proceed();
        }
        return super.onKeyDown(keyCode, msg);
    }

    /* access modifiers changed from: private */
    public void proceed() {
        if (!isFinishing()) {
            startActivity(new Intent(this, FrozenBubble.class));
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        unbindDrawables(findViewById(R.id.SplashLayout));
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
}
