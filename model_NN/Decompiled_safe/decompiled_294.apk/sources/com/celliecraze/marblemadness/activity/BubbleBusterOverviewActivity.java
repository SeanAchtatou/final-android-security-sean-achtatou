package com.celliecraze.marblemadness.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.celliecraze.marblemadness.MMApplication;
import com.celliecraze.marblemadness.R;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.highscores.DB;
import com.celliecraze.marblemadness.social.BaseRequestListener;
import com.celliecraze.marblemadness.social.FacebookSessionEvents;
import com.celliecraze.marblemadness.view.OverviewView;
import com.facebook.android.AsyncFacebookRunner;
import com.scoreloop.client.android.ui.BuddiesScreenActivity;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class BubbleBusterOverviewActivity extends Activity implements DialogInterface.OnClickListener {
    public static final String MAX_LEVEL = "max_level";
    public static final String UNLOCKED_LEVEL = "unlocked_level";
    public static long[] highscores;
    public static int levelPack = 0;
    protected static OverviewView mView;
    public static int unlockedLevel;
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.b_minus_pack:
                    BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                    if (BubbleBusterOverviewActivity.levelPack > 0) {
                        BubbleBusterOverviewActivity.levelPack--;
                        BubbleBusterOverviewActivity.mView.setStartingLevel(BubbleBusterOverviewActivity.levelPack * 100);
                        break;
                    }
                    break;
                case R.id.b_minus:
                    BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                    if (BubbleBusterOverviewActivity.mView.getStartingLevel() - 10 <= BubbleBusterOverviewActivity.levelPack * 100) {
                        BubbleBusterOverviewActivity.mView.setStartingLevel(BubbleBusterOverviewActivity.levelPack * 100);
                        break;
                    } else {
                        BubbleBusterOverviewActivity.mView.setStartingLevel(BubbleBusterOverviewActivity.mView.getStartingLevel() - 9);
                        break;
                    }
                case R.id.b_plus:
                    BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                    if (BubbleBusterOverviewActivity.mView.getStartingLevel() + 10 >= (BubbleBusterOverviewActivity.levelPack + 1) * 100) {
                        BubbleBusterOverviewActivity.mView.setStartingLevel(((BubbleBusterOverviewActivity.levelPack + 1) * 100) - 9);
                        break;
                    } else {
                        BubbleBusterOverviewActivity.mView.setStartingLevel(BubbleBusterOverviewActivity.mView.getStartingLevel() + 9);
                        break;
                    }
                case R.id.b_plus_pack:
                    BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                    if (BubbleBusterOverviewActivity.unlockedLevel > (BubbleBusterOverviewActivity.levelPack + 1) * 100) {
                        if (BubbleBusterOverviewActivity.levelPack < 5) {
                            BubbleBusterOverviewActivity.levelPack++;
                            BubbleBusterOverviewActivity.mView.setStartingLevel(BubbleBusterOverviewActivity.levelPack * 100);
                            break;
                        }
                    } else {
                        Typeface localTypeface = Typeface.createFromAsset(BubbleBusterOverviewActivity.this.getAssets(), "fonts/utb.ttf");
                        View myDialog = LayoutInflater.from(BubbleBusterOverviewActivity.this).inflate((int) R.layout.lpl_dialog, (ViewGroup) null);
                        ((TextView) myDialog.findViewById(R.id.app_name)).setTypeface(localTypeface);
                        ((TextView) myDialog.findViewById(R.id.level_pack_locked)).setTypeface(localTypeface);
                        ((Button) myDialog.findViewById(R.id.b_cancel)).setTypeface(localTypeface);
                        final AlertDialog a = new AlertDialog.Builder(BubbleBusterOverviewActivity.this).setView(myDialog).create();
                        a.setCancelable(false);
                        myDialog.findViewById(R.id.b_cancel).setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                                a.dismiss();
                            }
                        });
                        a.show();
                        break;
                    }
                    break;
            }
            BubbleBusterOverviewActivity.mView.invalidate();
            ((TextView) BubbleBusterOverviewActivity.this.findViewById(R.id.t_levels)).setText(BubbleBusterOverviewActivity.formatLevelNumbers(BubbleBusterOverviewActivity.mView.getStartingLevel()));
            ((TextView) BubbleBusterOverviewActivity.this.findViewById(R.id.level_pack_name)).setText(BubbleBusterOverviewActivity.this.getString(R.string.level_pack_no, new Object[]{Integer.valueOf(BubbleBusterOverviewActivity.levelPack + 1), Long.valueOf(DB.getLevelPackHighScore(BubbleBusterOverviewActivity.this, BubbleBusterOverviewActivity.levelPack))}));
            BubbleBusterOverviewActivity.this.setButtons();
        }
    };
    private boolean facebooked = false;
    private MMApplication mApp;
    private boolean tweeted = false;

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 10, 0, (int) R.string.menu_leaderboards).setIcon((int) R.drawable.ic_menu_leaderboards_sl);
        menu.add(0, 9, 0, (int) R.string.menu_hsreset).setIcon((int) R.drawable.ic_menu_hsreset);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 4:
                startActivity(new Intent(this, BuddiesScreenActivity.class));
                return true;
            case 9:
                TextView textView = new TextView(this);
                SpannableString s = new SpannableString(getString(R.string.dialog_hsreset, new Object[]{Integer.valueOf(levelPack + 1)}));
                AlertDialog.Builder alt_hsreset = new AlertDialog.Builder(this);
                alt_hsreset.setMessage(s);
                alt_hsreset.setCancelable(false);
                alt_hsreset.setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DB.resetLevelPackHighScore(BubbleBusterOverviewActivity.this, BubbleBusterOverviewActivity.levelPack);
                        BubbleBusterOverviewActivity.this.onStart();
                    }
                });
                alt_hsreset.setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert_hsreset = alt_hsreset.create();
                alert_hsreset.setTitle((int) R.string.app_name);
                alert_hsreset.setIcon((int) R.drawable.dialog);
                alert_hsreset.show();
                textView.findViewById(16908299);
                return true;
            case 10:
                startActivity(new Intent(this, LeaderboardsScreenActivity.class));
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(null);
        requestWindowFeature(5);
        setContentView((int) R.layout.overview);
        mView = (OverviewView) findViewById(R.id.overview);
        this.mApp = (MMApplication) getApplication();
        Typeface localTypeface = Typeface.createFromAsset(getAssets(), "fonts/utb.ttf");
        ((TextView) findViewById(R.id.level_pack_name)).setTypeface(localTypeface);
        ((TextView) findViewById(R.id.t_levels)).setTypeface(localTypeface);
        ((ImageButton) findViewById(R.id.fs_share_twitter)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                BubbleBusterOverviewActivity.this.compartirTwitter();
            }
        });
        ((ImageButton) findViewById(R.id.fs_share_facebook)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BubbleBusterConstants.vibrate(BubbleBusterOverviewActivity.this.getBaseContext(), 50);
                BubbleBusterOverviewActivity.this.compartirFacebook();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("startingLevel", mView.getStartingLevel());
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (!hasWindowFocus) {
            checkStatus();
        } else {
            checkStatus();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private void checkStatus() {
        if (this.mApp.isTwitterAuthorized()) {
            ((ImageButton) findViewById(R.id.fs_share_twitter)).setImageResource(R.drawable.ic_twitter);
        } else {
            ((ImageButton) findViewById(R.id.fs_share_twitter)).setImageResource(R.drawable.ic_twitter_disabled);
        }
        if (this.mApp.isFacebookAuthorized().booleanValue()) {
            ((ImageButton) findViewById(R.id.fs_share_facebook)).setImageResource(R.drawable.ic_facebook);
        } else {
            ((ImageButton) findViewById(R.id.fs_share_facebook)).setImageResource(R.drawable.ic_facebook_disabled);
        }
    }

    /* access modifiers changed from: private */
    public void compartirFacebook() {
        if (this.facebooked) {
            Toast.makeText(this, (int) R.string.fsFacebookAlready, 0).show();
        } else if (!BubbleBusterConstants.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, (int) R.string.internet_required, 0).show();
        } else {
            MMApplication app = (MMApplication) getApplication();
            if (!app.isFacebookAuthorized().booleanValue()) {
                Toast.makeText(this, (int) R.string.fsFacebookNotConnected, 0).show();
                app.beginFacebookAuthorization(this, new FacebookSessionEvents.LoginDialogListener());
                return;
            }
            String scSocial = getString(R.string.scSocialTemplate, new Object[]{Integer.valueOf(levelPack + 1), Integer.valueOf(unlockedLevel), Long.valueOf(DB.getLevelPackHighScore(this, levelPack))});
            Bundle parameters = new Bundle();
            parameters.putString("message", scSocial);
            final Handler h = new Handler();
            new AsyncFacebookRunner(app.getFacebook()).request("me/feed", parameters, "POST", new BaseRequestListener() {
                public void onComplete(String response, Object state) {
                    h.post(new Runnable() {
                        public void run() {
                            Toast.makeText(BubbleBusterOverviewActivity.this, (int) R.string.fsFacebookSent, 0).show();
                        }
                    });
                }
            }, null);
        }
    }

    /* access modifiers changed from: private */
    public void compartirTwitter() {
        if (this.tweeted) {
            Toast.makeText(this, (int) R.string.fsTweetAlready, 0).show();
        } else if (!BubbleBusterConstants.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, (int) R.string.internet_required, 0).show();
        } else {
            MMApplication app = (MMApplication) getApplication();
            if (!app.isTwitterAuthorized()) {
                startActivity(new Intent(getApplicationContext(), TwitterAuthorizationActivity.class));
                Toast.makeText(this, (int) R.string.fsTweetNotConnected, 0).show();
                return;
            }
            Twitter tw = app.getTwitter();
            String scSocial = getString(R.string.scSocialTemplate, new Object[]{Integer.valueOf(levelPack + 1), Integer.valueOf(unlockedLevel), Long.valueOf(DB.getLevelPackHighScore(this, levelPack))});
            if (scSocial.length() <= 140) {
                try {
                    tw.updateStatus(scSocial);
                    Toast.makeText(this, (int) R.string.fsTweetSent, 0).show();
                    this.tweeted = true;
                } catch (TwitterException e) {
                    BubbleBusterConstants.e((Exception) e);
                }
            } else {
                Log.e(BubbleBusterConstants.TAG, "Over 140 Characters!");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mApp.authorizeFacebookCallback(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        mView.setLevels(loadDefaultLevels());
        mView.setClickable(true);
        Intent i = getIntent();
        unlockedLevel = i.getIntExtra("unlockedLevel", 0);
        int startingLevel = i.getIntExtra("currentLevel", 0);
        levelPack = startingLevel / 100;
        if (startingLevel + 9 > (levelPack + 1) * 100) {
            startingLevel = ((levelPack + 1) * 100) - 9;
        }
        mView.setStartingLevel(startingLevel);
        ((TextView) findViewById(R.id.t_levels)).setText(formatLevelNumbers(mView.getStartingLevel()));
        mView.invalidate();
        loadDefaultLevels();
        findViewById(R.id.b_minus_pack).setOnClickListener(this.clickListener);
        findViewById(R.id.b_minus).setOnClickListener(this.clickListener);
        findViewById(R.id.b_plus).setOnClickListener(this.clickListener);
        findViewById(R.id.b_plus_pack).setOnClickListener(this.clickListener);
        setButtons();
        highscores = DB.getScores(this);
        ((TextView) findViewById(R.id.level_pack_name)).setText(getString(R.string.level_pack_no, new Object[]{Integer.valueOf(levelPack + 1), Long.valueOf(DB.getLevelPackHighScore(this, levelPack))}));
    }

    protected static String formatLevelNumbers(int startingLevel) {
        NumberFormat formatter = new DecimalFormat("000");
        return String.valueOf(formatter.format((long) (startingLevel + 1))) + "-" + formatter.format((long) (startingLevel + 9));
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -1:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("selectedLevel", mView.getLevelClicked());
                setResult(-1, returnIntent);
                finish();
                return;
            default:
                dialog.dismiss();
                return;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        r1 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] loadDefaultLevels() {
        /*
            r9 = this;
            android.content.res.AssetManager r7 = r9.getAssets()     // Catch:{ IOException -> 0x0022 }
            java.lang.String r8 = "levels.txt"
            java.io.InputStream r3 = r7.open(r8)     // Catch:{ IOException -> 0x0022 }
            int r6 = r3.available()     // Catch:{ IOException -> 0x0022 }
            byte[] r4 = new byte[r6]     // Catch:{ IOException -> 0x0022 }
            r3.read(r4)     // Catch:{ IOException -> 0x0022 }
            r3.close()     // Catch:{ IOException -> 0x0022 }
            r6 = 45000(0xafc8, float:6.3058E-41)
            byte[] r0 = new byte[r6]
            r5 = 0
            r2 = 0
        L_0x001d:
            int r7 = r4.length     // Catch:{ Exception -> 0x0047 }
            if (r2 < r7) goto L_0x0026
        L_0x0020:
            r7 = r0
        L_0x0021:
            return r7
        L_0x0022:
            r7 = move-exception
            r1 = r7
            r7 = 0
            goto L_0x0021
        L_0x0026:
            byte r7 = r4[r2]     // Catch:{ Exception -> 0x0047 }
            r8 = 32
            if (r7 == r8) goto L_0x003e
            byte r7 = r4[r2]     // Catch:{ Exception -> 0x0047 }
            r8 = 10
            if (r7 == r8) goto L_0x003e
            byte r7 = r4[r2]     // Catch:{ Exception -> 0x0047 }
            r8 = 45
            if (r7 != r8) goto L_0x0041
            r7 = -1
        L_0x0039:
            byte r7 = (byte) r7     // Catch:{ Exception -> 0x0047 }
            r0[r5] = r7     // Catch:{ Exception -> 0x0047 }
            int r5 = r5 + 1
        L_0x003e:
            int r2 = r2 + 1
            goto L_0x001d
        L_0x0041:
            byte r7 = r4[r2]     // Catch:{ Exception -> 0x0047 }
            r8 = 48
            int r7 = r7 - r8
            goto L_0x0039
        L_0x0047:
            r7 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.celliecraze.marblemadness.activity.BubbleBusterOverviewActivity.loadDefaultLevels():byte[]");
    }

    /* access modifiers changed from: private */
    public void setButtons() {
        if (levelPack < 1) {
            findViewById(R.id.b_minus_pack).setEnabled(false);
        } else {
            findViewById(R.id.b_minus_pack).setEnabled(true);
        }
        if (levelPack + 1 >= 6) {
            findViewById(R.id.b_plus_pack).setEnabled(false);
        } else {
            findViewById(R.id.b_plus_pack).setEnabled(true);
        }
        if (mView.getStartingLevel() + 9 < (levelPack + 1) * 100) {
            findViewById(R.id.b_plus).setEnabled(true);
        } else {
            findViewById(R.id.b_plus).setEnabled(false);
        }
    }
}
