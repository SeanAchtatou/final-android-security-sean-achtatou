package com.celliecraze.marblemadness.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.celliecraze.marblemadness.R;

public class BubbleBusterAboutActivity extends Activity {
    private String getArrayAsString(int paramInt) {
        String str1 = "";
        CharSequence[] arrayOfCharSequence = getResources().getTextArray(paramInt);
        for (int i = 0; i < arrayOfCharSequence.length; i++) {
            str1 = String.valueOf(arrayOfCharSequence[i].toString()) + "\n\n" + ((Object) new StringBuilder(String.valueOf(str1)));
        }
        return str1;
    }

    public static String getVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(new ComponentName(context, context.getClass()).getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_layout);
        ((TextView) findViewById(R.id.t_about)).setText(getResources().getText(R.string.about_text));
        ((TextView) findViewById(R.id.t_app_name)).setText(((Object) new StringBuilder(String.valueOf(getResources().getString(R.string.app_name))).append(" ")) + " " + getVersionName(this));
        ((TextView) findViewById(R.id.t_permissions_txt)).setText(getArrayAsString(R.array.permissions));
        ((TextView) findViewById(R.id.t_features_txt)).setText(getArrayAsString(R.array.features));
        ((TextView) findViewById(R.id.t_change_log_data)).setText(getArrayAsString(R.array.change_log_data));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        ((Button) findViewById(R.id.b_skitapps)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.no_market_homepage))));
                } catch (Exception e) {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.facebook_page))));
                }
            }
        });
        ((Button) findViewById(R.id.b_email)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String[] recipicients = {BubbleBusterAboutActivity.this.getString(R.string.email)};
                Intent sendIntent = new Intent("android.intent.action.SEND");
                sendIntent.putExtra("android.intent.extra.TEXT", "");
                sendIntent.putExtra("android.intent.extra.SUBJECT", String.valueOf(BubbleBusterAboutActivity.this.getString(R.string.app_name)) + " " + BubbleBusterAboutActivity.getVersionName(BubbleBusterAboutActivity.this));
                sendIntent.putExtra("android.intent.extra.EMAIL", recipicients);
                sendIntent.setType("message/rfc822");
                BubbleBusterAboutActivity.this.startActivity(Intent.createChooser(sendIntent, BubbleBusterAboutActivity.this.getString(R.string.email_me)));
            }
        });
        ((Button) findViewById(R.id.b_homepage)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.about_homepage))));
                } catch (Exception e) {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.no_market_homepage))));
                }
            }
        });
        ((Button) findViewById(R.id.b_facebook)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.facebook_page))));
                } catch (Exception e) {
                    BubbleBusterAboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(BubbleBusterAboutActivity.this.getString(R.string.no_market_homepage))));
                }
            }
        });
    }
}
