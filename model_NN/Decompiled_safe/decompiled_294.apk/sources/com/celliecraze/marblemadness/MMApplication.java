package com.celliecraze.marblemadness;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import com.celliecraze.marblemadness.activity.TwitterAuthorizationActivity;
import com.celliecraze.marblemadness.helper.BubbleBusterConstants;
import com.celliecraze.marblemadness.social.BaseRequestListener;
import com.celliecraze.marblemadness.social.FacebookSessionEvents;
import com.celliecraze.marblemadness.social.FacebookSessionStore;
import com.celliecraze.marblemadness.social.OAuthHelper;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.scoreloop.client.android.ui.OnCanStartGamePlayObserver;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.RequestToken;

public class MMApplication extends Application implements OnCanStartGamePlayObserver {
    public static final String EXTRA_MODE = "extraMode";
    public static Integer _gamePlaySessionMode;
    public static GamePlaySessionStatus _gamePlaySessionStatus;
    private RequestToken currentRequestToken;
    private AsyncFacebookRunner mAsyncRunner;
    private Facebook mFacebook;
    private Twitter mTwitter;
    private OAuthHelper oAuthHelper;

    enum GamePlaySessionStatus {
        CHALLENGE,
        NONE,
        NORMAL
    }

    public static Integer getGamePlaySessionMode() {
        return _gamePlaySessionMode;
    }

    public static GamePlaySessionStatus getGamePlaySessionStatus() {
        return _gamePlaySessionStatus;
    }

    public static void setGamePlaySessionMode(Integer mode) {
        _gamePlaySessionMode = mode;
    }

    public static void setGamePlaySessionStatus(GamePlaySessionStatus status) {
        _gamePlaySessionStatus = status;
    }

    public boolean onCanStartGamePlay() {
        return policy2();
    }

    public void onCreate() {
        super.onCreate();
        this.oAuthHelper = new OAuthHelper(this);
        this.mTwitter = new TwitterFactory().getInstance();
        this.oAuthHelper.configureOAuth(this.mTwitter);
        this.mFacebook = new Facebook("219210828099592");
        this.mAsyncRunner = new AsyncFacebookRunner(this.mFacebook);
        FacebookSessionStore.restore(this.mFacebook, this);
        FacebookSessionEvents.addAuthListener(new FacebookSessionEvents.AuthListener() {
            public void onAuthSucceed() {
                FacebookSessionStore.save(MMApplication.this.getFacebook(), MMApplication.this.getApplicationContext());
            }

            public void onAuthFail(String error) {
            }
        });
        ScoreloopManagerSingleton.init(this, "RSTAIAOJw673oMzodJanvQ8VbXN+8PyvPkCkqSDEVquO38uQqFxPag==");
        ScoreloopManagerSingleton.get().setOnCanStartGamePlayObserver(this);
        _gamePlaySessionStatus = GamePlaySessionStatus.NONE;
        _gamePlaySessionMode = null;
    }

    public void onTerminate() {
        super.onTerminate();
        ScoreloopManagerSingleton.destroy();
        _gamePlaySessionMode = null;
        _gamePlaySessionStatus = null;
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.d(BubbleBusterConstants.TAG, "Application: onLowMemory() - ending application");
        try {
            finalize();
        } catch (Throwable e) {
            Log.e(BubbleBusterConstants.TAG, "Error ending application: " + e.toString());
        }
    }

    public Twitter getTwitter() {
        return this.mTwitter;
    }

    public boolean isTwitterAuthorized() {
        return this.oAuthHelper.hasAccessToken();
    }

    public String beginTwitterAuthorization() {
        try {
            this.currentRequestToken = this.mTwitter.getOAuthRequestToken();
            return this.currentRequestToken.getAuthorizationURL();
        } catch (TwitterException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public boolean authorizeTwitter(String pin) {
        try {
            this.oAuthHelper.storeAccessToken(this.mTwitter.getOAuthAccessToken(this.currentRequestToken, pin));
            return true;
        } catch (TwitterException e) {
            throw new RuntimeException("Unable to authorize user", e);
        }
    }

    public void authorizedTwitter() {
        try {
            this.oAuthHelper.storeAccessToken(this.mTwitter.getOAuthAccessToken());
        } catch (TwitterException e) {
            throw new RuntimeException("Unable to authorize user", e);
        }
    }

    public void logoutTwitter() {
        this.oAuthHelper.removeAccessToken();
        this.oAuthHelper.configureOAuth(this.mTwitter);
    }

    public void loginTwitter() {
        startActivity(new Intent(this, TwitterAuthorizationActivity.class));
    }

    public Facebook getFacebook() {
        return this.mFacebook;
    }

    public void beginFacebookAuthorization(Activity activity, Facebook.DialogListener listener) {
        this.mFacebook.authorize(activity, new String[]{"publish_stream"}, listener);
    }

    public Boolean isFacebookAuthorized() {
        return Boolean.valueOf(this.mFacebook.isSessionValid());
    }

    public void logoutFacebook(final Handler h) {
        FacebookSessionEvents.onLogoutBegin();
        this.mAsyncRunner.logout(getApplicationContext(), new BaseRequestListener() {
            public void onComplete(String response, Object state) {
                h.post(new Runnable() {
                    public void run() {
                        FacebookSessionEvents.onLogoutFinish();
                    }
                });
            }

            public void onFacebookError(FacebookError e, Object state) {
            }
        });
    }

    public void authorizeFacebookCallback(int requestCode, int resultCode, Intent data) {
        this.mFacebook.authorizeCallback(requestCode, resultCode, data);
    }

    private boolean policy1() {
        if (_gamePlaySessionStatus != GamePlaySessionStatus.NORMAL) {
            return true;
        }
        _gamePlaySessionStatus = GamePlaySessionStatus.NONE;
        _gamePlaySessionMode = null;
        return true;
    }

    private boolean policy2() {
        if (_gamePlaySessionStatus == GamePlaySessionStatus.NORMAL) {
            return false;
        }
        return true;
    }
}
