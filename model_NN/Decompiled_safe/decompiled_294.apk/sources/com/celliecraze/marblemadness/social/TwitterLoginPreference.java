package com.celliecraze.marblemadness.social;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.celliecraze.marblemadness.R;
import twitter4j.Twitter;

public class TwitterLoginPreference extends Preference {
    private static final int COMPLETE = 0;
    private static final int FAILED = 1;
    private final Handler imageLoadedHandler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            TwitterLoginPreference.this.mImageView.setVisibility(0);
            TwitterLoginPreference.this.mProgressBar.setVisibility(8);
            switch (msg.what) {
                case 0:
                    TwitterLoginPreference.this.mImageView.setImageDrawable(TwitterLoginPreference.this.mDrawable);
                    return true;
                default:
                    TwitterLoginPreference.this.mImageView.setImageResource(R.drawable.ic_menu_report_image);
                    return true;
            }
        }
    });
    /* access modifiers changed from: private */
    public Drawable mDrawable;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    public Twitter mTwitter;

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        super.onBindView(view);
    }

    public TwitterLoginPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TwitterLoginPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TwitterLoginPreference(Context context) {
        super(context);
    }
}
