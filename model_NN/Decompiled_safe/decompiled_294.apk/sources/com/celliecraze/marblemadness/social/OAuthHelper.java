package com.celliecraze.marblemadness.social;

import android.content.Context;
import android.content.SharedPreferences;
import com.celliecraze.marblemadness.R;
import java.util.Properties;
import twitter4j.Twitter;
import twitter4j.http.AccessToken;

public class OAuthHelper {
    private static final String APPLICATION_PREFERENCES = "app_prefs";
    private static final String AUTH_KEY = "auth_key";
    private static final String AUTH_SEKRET_KEY = "auth_secret_key";
    private AccessToken accessToken = loadAccessToken();
    private String consumerKey;
    private String consumerSecretKey;
    private Context context;
    private SharedPreferences prefs;

    public OAuthHelper(Context context2) {
        this.context = context2;
        this.prefs = context2.getSharedPreferences(APPLICATION_PREFERENCES, 0);
        loadConsumerKeys();
    }

    public void configureOAuth(Twitter twitter) {
        try {
            twitter.setOAuthConsumer(this.consumerKey, this.consumerSecretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        twitter.setOAuthAccessToken(this.accessToken);
    }

    public boolean hasAccessToken() {
        return this.accessToken != null;
    }

    public void storeAccessToken(AccessToken accessToken2) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(AUTH_KEY, accessToken2.getToken());
        editor.putString(AUTH_SEKRET_KEY, accessToken2.getTokenSecret());
        editor.commit();
        this.accessToken = accessToken2;
    }

    public void removeAccessToken() {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(AUTH_KEY, null);
        editor.putString(AUTH_SEKRET_KEY, null);
        editor.commit();
        this.accessToken = null;
    }

    private AccessToken loadAccessToken() {
        String token = this.prefs.getString(AUTH_KEY, null);
        String tokenSecret = this.prefs.getString(AUTH_SEKRET_KEY, null);
        if (token == null || tokenSecret == null) {
            return null;
        }
        return new AccessToken(token, tokenSecret);
    }

    private void loadConsumerKeys() {
        try {
            Properties props = new Properties();
            props.load(this.context.getResources().openRawResource(R.raw.oauth));
            this.consumerKey = (String) props.get("consumer_key");
            this.consumerSecretKey = (String) props.get("consumer_secret_key");
        } catch (Exception e) {
            throw new RuntimeException("Unable to load consumer keys from oauth.properties", e);
        }
    }
}
