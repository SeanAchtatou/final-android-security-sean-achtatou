package com.celliecraze.marblemadness.highscores;

public final class HighScore {
    private final int bubbles;
    private final long score;
    private final long time;

    public long getScore() {
        return this.score;
    }

    public int getBubbles() {
        return this.bubbles;
    }

    public long getTime() {
        return this.time;
    }

    public HighScore(long score2, int bubbles2, long time2) {
        this.score = score2;
        this.bubbles = bubbles2;
        this.time = time2;
    }
}
