package com.scoreloop.client.android.ui.component.news;

import android.os.Bundle;
import com.celliecraze.marblemadness.R;
import com.scoreloop.client.android.ui.component.base.ComponentHeaderActivity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class NewsHeaderActivity extends ComponentHeaderActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.sl_header_default);
        setCaption(getGame().getName());
        setTitle(getString(R.string.sl_news));
        getImageView().setImageDrawable(StringFormatter.getNewsDrawable(this, getUserValues(), true));
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NEWS_NUMBER_UNREAD_ITEMS));
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        setSubTitle(StringFormatter.getNewsSubTitle(this, getUserValues()));
        getImageView().setImageDrawable(StringFormatter.getNewsDrawable(this, getUserValues(), true));
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (key.equals(Constant.NEWS_NUMBER_UNREAD_ITEMS)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_OLDER_THAN, Long.valueOf((long) Constant.NEWS_FEED_REFRESH_TIME));
        }
    }
}
