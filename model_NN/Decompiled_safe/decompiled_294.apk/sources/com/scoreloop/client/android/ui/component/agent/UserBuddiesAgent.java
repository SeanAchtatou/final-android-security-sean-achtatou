package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.UserController;
import com.scoreloop.client.android.core.model.Entity;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class UserBuddiesAgent extends BaseAgent {
    public static final String[] SUPPORTED_KEYS = {Constant.USER_BUDDIES};
    private UserController _userController;

    public UserBuddiesAgent() {
        super(SUPPORTED_KEYS);
    }

    /* access modifiers changed from: protected */
    public void onFinishRetrieve(RequestController aRequestController, ValueStore valueStore) {
        putValue(Constant.USER_BUDDIES, this._userController.getUser().getBuddyUsers());
    }

    /* access modifiers changed from: protected */
    public void onStartRetrieve(ValueStore valueStore) {
        this._userController = new UserController(this);
        this._userController.setUser((Entity) valueStore.getValue("user"));
        this._userController.loadBuddies();
    }
}
