package com.scoreloop.client.android.ui.component.challenge;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.celliecraze.marblemadness.R;
import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import java.math.BigDecimal;

public class ChallengeHistoryListItem extends BaseListItem {
    private final Challenge _challenge;
    private boolean _showPrize;

    static class ViewHolder {
        TextView contenderName;
        TextView contenderScore;
        TextView contestantName;
        TextView contestantScore;
        ImageView icon;
        TextView prize;
        LinearLayout scores;

        ViewHolder() {
        }
    }

    public ChallengeHistoryListItem(ComponentActivity componentActivity, Challenge challenge, boolean showPrize) {
        super(componentActivity, null, null);
        this._challenge = challenge;
        this._showPrize = showPrize;
    }

    public int getType() {
        return 4;
    }

    public View getView(View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_challenge_history, (ViewGroup) null);
            holder = new ViewHolder();
            holder.icon = (ImageView) view.findViewById(R.id.sl_icon);
            holder.contenderName = (TextView) view.findViewById(R.id.sl_contender_name);
            holder.contenderScore = (TextView) view.findViewById(R.id.sl_contender_score);
            holder.contestantName = (TextView) view.findViewById(R.id.sl_contestant_name);
            holder.contestantScore = (TextView) view.findViewById(R.id.sl_contestant_score);
            holder.scores = (LinearLayout) view.findViewById(R.id.sl_scores);
            holder.prize = (TextView) view.findViewById(R.id.sl_prize);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        prepareView(holder);
        return view;
    }

    public boolean isEnabled() {
        return true;
    }

    private void fillView(ViewHolder holder, Drawable drawable, String contestantName, String contestantScore, String prize) {
        Drawable drawable2;
        String displayName;
        String string;
        String str;
        ImageView imageView = holder.icon;
        if (drawable != null) {
            drawable2 = drawable;
        } else {
            drawable2 = getContext().getResources().getDrawable(R.drawable.sl_icon_challenges);
        }
        imageView.setImageDrawable(drawable2);
        holder.contenderName.setText(this._challenge.getContender().getDisplayName());
        holder.contenderScore.setText(StringFormatter.formatChallengesScore(this._challenge.getContenderScore(), getComponentActivity().getConfiguration()));
        TextView textView = holder.contestantName;
        if (contestantName != null) {
            displayName = contestantName;
        } else {
            displayName = this._challenge.getContestant().getDisplayName();
        }
        textView.setText(displayName);
        TextView textView2 = holder.contestantScore;
        if (contestantScore != null) {
            string = contestantScore;
        } else {
            string = getContext().getResources().getString(R.string.sl_pending);
        }
        textView2.setText(string);
        TextView textView3 = holder.prize;
        if (prize != null) {
            str = prize;
        } else {
            str = "-" + StringFormatter.formatMoney(this._challenge.getStake(), getComponentActivity().getConfiguration());
        }
        textView3.setText(str);
        if (this._showPrize) {
            holder.prize.setVisibility(0);
            holder.scores.setVisibility(8);
            return;
        }
        holder.prize.setVisibility(8);
        holder.scores.setVisibility(0);
    }

    private ComponentActivity getComponentActivity() {
        return (ComponentActivity) getContext();
    }

    /* access modifiers changed from: protected */
    public void prepareView(ViewHolder holder) {
        Drawable drawable;
        if (this._challenge.isComplete()) {
            String sign = "";
            BigDecimal prize = BigDecimal.ZERO.subtract(this._challenge.getStake().getAmount());
            if (getComponentActivity().getSession().isOwnedByUser(this._challenge.getWinner())) {
                drawable = getContext().getResources().getDrawable(R.drawable.sl_icon_challenge_won);
                prize = prize.add(this._challenge.getPrize().getAmount());
                sign = "+";
            } else {
                drawable = getContext().getResources().getDrawable(R.drawable.sl_icon_challenge_lost);
            }
            fillView(holder, drawable, null, StringFormatter.formatChallengesScore(this._challenge.getContestantScore(), getComponentActivity().getConfiguration()), String.valueOf(sign) + StringFormatter.formatMoney(new Money(prize), getComponentActivity().getConfiguration()));
        } else if (this._challenge.isOpen()) {
            fillView(holder, null, getContext().getResources().getString(R.string.sl_anyone), getContext().getResources().getString(R.string.sl_pending), null);
        } else if (this._challenge.isAssigned()) {
            fillView(holder, null, null, getContext().getResources().getString(R.string.sl_pending), null);
        } else if (this._challenge.isRejected()) {
            fillView(holder, null, null, getContext().getResources().getString(R.string.sl_rejected), StringFormatter.formatMoney(new Money(BigDecimal.ZERO), getComponentActivity().getConfiguration()));
        } else if (this._challenge.isAccepted()) {
            fillView(holder, null, null, getContext().getResources().getString(R.string.sl_pending), null);
        }
    }

    /* access modifiers changed from: package-private */
    public void setShowPrize(boolean showPrize) {
        this._showPrize = showPrize;
    }
}
