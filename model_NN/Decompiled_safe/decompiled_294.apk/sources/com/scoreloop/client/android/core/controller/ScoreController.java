package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__2_3_0;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.ScoreSubmitException;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.persistence.LocalScoreStore;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class ScoreController extends RequestController {
    private ChallengeController c;
    /* access modifiers changed from: private */
    public Score d;
    private boolean e;
    private boolean f;
    /* access modifiers changed from: private */
    public final LocalScoreStore g;

    private class a implements ChallengeControllerObserver {
        private a() {
        }

        public void challengeControllerDidFailOnInsufficientBalance(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void challengeControllerDidFailToAcceptChallenge(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void challengeControllerDidFailToRejectChallenge(ChallengeController challengeController) {
            throw new IllegalStateException();
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            ScoreController.this.f().requestControllerDidFail(ScoreController.this, exc);
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            ScoreController.this.f().requestControllerDidReceiveResponse(ScoreController.this);
        }
    }

    private class b implements RequestControllerObserver {
        private final RequestControllerObserver b;

        private b(RequestControllerObserver requestControllerObserver) {
            this.b = requestControllerObserver;
        }

        public void requestControllerDidFail(RequestController requestController, Exception exc) {
            ScoreController.this.g.a(ScoreController.this.d);
            this.b.requestControllerDidFail(requestController, new ScoreSubmitException("Score could not be submitted. Stored in local leaderboard.", exc));
        }

        public void requestControllerDidReceiveResponse(RequestController requestController) {
            if (ScoreController.this.d.c() != null) {
                ScoreController.this.g.b(ScoreController.this.d);
            }
            this.b.requestControllerDidReceiveResponse(requestController);
        }
    }

    private static class c extends Request {
        private final Game a;
        private final Score b;

        public c(RequestCompletionCallback requestCompletionCallback, Game game, Score score, Session session) {
            super(requestCompletionCallback);
            if (game == null) {
                throw new IllegalStateException("internal error: null game passed");
            }
            this.a = game;
            this.b = score;
        }

        public String a() {
            return String.format("/service/games/%s/scores", this.a.getIdentifier());
        }

        public JSONObject b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(Score.a, this.b.d());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException("Invalid score data", e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    @PublishedFor__1_0_0
    public ScoreController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public ScoreController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.a = new b(requestControllerObserver);
        this.g = new LocalScoreStore(h().d(), h().getGame().getIdentifier(), h().a().h(), h().getUser());
    }

    private void a(Score score) {
        this.d = score;
    }

    private void b() {
        if (h().getChallenge() != null) {
            if (this.e) {
                if (this.c == null) {
                    this.c = new ChallengeController(h(), new a());
                }
                this.c.submitChallengeScore(this.d);
                return;
            }
            Logger.c("It seems that a challenge is in progess. Submitted score won't get associated with that challenge. Call setShouldSubmitScoreForChallenge(true); to make this go away.");
        }
        this.d.a(i());
        a_();
        if (this.f) {
            this.g.a(this.d);
            j();
            return;
        }
        b(new c(g(), getGame(), this.d, h()));
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        int f2 = response.f();
        JSONObject jSONObject = response.e().getJSONObject(Score.a);
        if (getScore() == null) {
            a(new Score(jSONObject));
        } else {
            getScore().a(jSONObject);
        }
        if (f2 == 200 || f2 == 201) {
            return true;
        }
        throw new Exception("Request failed");
    }

    @PublishedFor__1_0_0
    public Score getScore() {
        return this.d;
    }

    @PublishedFor__1_0_0
    public void setShouldSubmitScoreForChallenge(boolean z) {
        this.e = z;
    }

    @PublishedFor__2_3_0
    public void setShouldSubmitScoreLocally(boolean z) {
        this.f = z;
    }

    @PublishedFor__1_0_0
    public boolean shouldSubmitScoreForChallenge() {
        return this.e;
    }

    @PublishedFor__2_3_0
    public boolean shouldSubmitScoreLocally() {
        return this.f;
    }

    @PublishedFor__1_0_0
    public void submitScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter cannot be null");
        }
        a(score);
        b();
    }
}
