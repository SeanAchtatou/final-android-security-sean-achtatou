package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.PublishedFor__2_0_0;

public interface ImageSource {
    @PublishedFor__2_0_0
    public static final ImageSource IMAGE_SOURCE_DEFAULT = new d();
    @PublishedFor__2_0_0
    public static final ImageSource IMAGE_SOURCE_SCORELOOP = new c();

    String getName();
}
