package com.scoreloop.client.android.ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.scoreloop.client.android.ui.util.ImageDownloader;
import java.io.File;
import java.io.FileOutputStream;

public class LocalImageStorage {
    static boolean isStorageWritable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    static boolean isStorageReadable() {
        return isStorageWritable() || Environment.getExternalStorageState().equals("mounted_ro");
    }

    private static File getCacheDir(Context context) {
        File storageDir = Environment.getExternalStorageDirectory();
        if (storageDir == null) {
            return null;
        }
        File tmp = new File(storageDir, "/Android/data/" + context.getPackageName() + "/cache/");
        if ((!tmp.exists() || !tmp.isDirectory()) && !tmp.mkdirs()) {
            return null;
        }
        return tmp;
    }

    private static File getCacheFile(Context context, String url) {
        File cacheDir = getCacheDir(context);
        if (cacheDir != null) {
            return new File(cacheDir, Base64.encodeBytes(url.getBytes()));
        }
        return null;
    }

    public static ImageDownloader.BitmapResult getBitmap(Context context, String url) {
        File cacheFile;
        if (!isStorageReadable() || (cacheFile = getCacheFile(context, url)) == null || !cacheFile.exists() || !cacheFile.canRead()) {
            return null;
        }
        if (cacheFile.length() == 0) {
            return ImageDownloader.BitmapResult.createNotFound();
        }
        return new ImageDownloader.BitmapResult(BitmapFactory.decodeFile(cacheFile.getAbsolutePath()));
    }

    public static boolean putBitmap(Context context, String url, Bitmap bitmap) {
        return putBitmap(context, url, new ImageDownloader.BitmapResult(bitmap));
    }

    public static boolean putBitmap(Context context, String url, ImageDownloader.BitmapResult bitmapResult) {
        if (isStorageWritable()) {
            File cacheFile = getCacheFile(context, url);
            try {
                if (bitmapResult.isNotFound()) {
                    cacheFile.createNewFile();
                    return true;
                }
                FileOutputStream os = new FileOutputStream(cacheFile);
                bitmapResult.getBitmap().compress(Bitmap.CompressFormat.PNG, 90, os);
                os.close();
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0034 A[SYNTHETIC, Splitter:B:26:0x0034] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File putStream(android.content.Context r8, java.lang.String r9, java.io.InputStream r10) {
        /*
            boolean r6 = isStorageWritable()
            if (r6 == 0) goto L_0x002f
            java.io.File r2 = getCacheFile(r8, r9)
            r4 = 0
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0041, all -> 0x0031 }
            r5.<init>(r2)     // Catch:{ Exception -> 0x0041, all -> 0x0031 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r1]     // Catch:{ Exception -> 0x0028, all -> 0x003e }
            r3 = 0
        L_0x0015:
            int r3 = r10.read(r0)     // Catch:{ Exception -> 0x0028, all -> 0x003e }
            r6 = -1
            if (r3 != r6) goto L_0x0023
            if (r5 == 0) goto L_0x0021
            r5.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0021:
            r6 = r2
        L_0x0022:
            return r6
        L_0x0023:
            r6 = 0
            r5.write(r0, r6, r3)     // Catch:{ Exception -> 0x0028, all -> 0x003e }
            goto L_0x0015
        L_0x0028:
            r6 = move-exception
            r4 = r5
        L_0x002a:
            if (r4 == 0) goto L_0x002f
            r4.close()     // Catch:{ IOException -> 0x003a }
        L_0x002f:
            r6 = 0
            goto L_0x0022
        L_0x0031:
            r6 = move-exception
        L_0x0032:
            if (r4 == 0) goto L_0x0037
            r4.close()     // Catch:{ IOException -> 0x003c }
        L_0x0037:
            throw r6
        L_0x0038:
            r6 = move-exception
            goto L_0x0021
        L_0x003a:
            r6 = move-exception
            goto L_0x002f
        L_0x003c:
            r7 = move-exception
            goto L_0x0037
        L_0x003e:
            r6 = move-exception
            r4 = r5
            goto L_0x0032
        L_0x0041:
            r6 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.util.LocalImageStorage.putStream(android.content.Context, java.lang.String, java.io.InputStream):java.io.File");
    }
}
