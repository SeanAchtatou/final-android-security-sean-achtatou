package com.scoreloop.client.android.ui.component.user;

import android.app.Dialog;
import android.os.Bundle;
import com.celliecraze.marblemadness.R;
import com.scoreloop.client.android.core.controller.MessageController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.CaptionListItem;
import com.scoreloop.client.android.ui.component.base.ComponentListActivity;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.component.base.Factory;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.scoreloop.client.android.ui.framework.OkCancelDialog;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class UserDetailListActivity extends ComponentListActivity<BaseListItem> implements BaseDialog.OnActionListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserDetailListActivity$GameSectionDisplayOption;
    protected UserDetailListItem _achievementsListItem;
    protected UserDetailListItem _buddiesListItem;
    protected BaseListItem _challengesListItem;
    protected GameSectionDisplayOption _gameSectionDisplayOption = GameSectionDisplayOption.UNKNOWN;
    protected UserDetailListItem _gamesListItem;
    protected BaseListItem _recommendListItem;

    protected enum GameSectionDisplayOption {
        HIDE,
        RECOMMEND,
        SHOW,
        UNKNOWN
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserDetailListActivity$GameSectionDisplayOption() {
        int[] iArr = $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserDetailListActivity$GameSectionDisplayOption;
        if (iArr == null) {
            iArr = new int[GameSectionDisplayOption.values().length];
            try {
                iArr[GameSectionDisplayOption.HIDE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameSectionDisplayOption.RECOMMEND.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameSectionDisplayOption.SHOW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[GameSectionDisplayOption.UNKNOWN.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserDetailListActivity$GameSectionDisplayOption = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public UserDetailListItem getAchievementsListItem() {
        if (this._achievementsListItem == null) {
            this._achievementsListItem = new UserDetailListItem(this, getResources().getDrawable(R.drawable.sl_icon_achievements), getString(R.string.sl_achievements), StringFormatter.getAchievementsSubTitle(this, getUserValues(), false));
        }
        return this._achievementsListItem;
    }

    /* access modifiers changed from: protected */
    public UserDetailListItem getBuddiesListItem() {
        if (this._buddiesListItem == null) {
            this._buddiesListItem = new UserDetailListItem(this, getResources().getDrawable(R.drawable.sl_icon_friends), getString(R.string.sl_friends), StringFormatter.getBuddiesSubTitle(this, getUserValues()));
        }
        return this._buddiesListItem;
    }

    /* access modifiers changed from: protected */
    public BaseListItem getChallengesListItem() {
        if (this._challengesListItem == null) {
            this._challengesListItem = new StandardListItem(this, getResources().getDrawable(R.drawable.sl_icon_challenges), getString(R.string.sl_format_challenges_title), getString(R.string.sl_format_challenges_subtitle, new Object[]{getUser().getDisplayName()}), null);
        }
        return this._challengesListItem;
    }

    /* access modifiers changed from: protected */
    public CaptionListItem getCommunityCaptionListItem() {
        return new CaptionListItem(this, null, getString(R.string.sl_community));
    }

    /* access modifiers changed from: protected */
    public CaptionListItem getGameCaptionListItem() {
        return new CaptionListItem(this, null, getGame().getName());
    }

    /* access modifiers changed from: protected */
    public UserDetailListItem getGamesListItem() {
        if (this._gamesListItem == null) {
            this._gamesListItem = new UserDetailListItem(this, getResources().getDrawable(R.drawable.sl_icon_games), getString(R.string.sl_games), StringFormatter.getGamesSubTitle(this, getUserValues()));
        }
        return this._gamesListItem;
    }

    /* access modifiers changed from: protected */
    public BaseListItem getRecommendListItem() {
        if (getGame() != null && this._recommendListItem == null) {
            User user = getUser();
            Game game = getGame();
            this._recommendListItem = new StandardListItem(this, getResources().getDrawable(R.drawable.sl_icon_recommend), String.format(getString(R.string.sl_format_recommend_title), game.getName()), String.format(getString(R.string.sl_format_recommend_subtitle), user.getDisplayName()), null);
        }
        return this._recommendListItem;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new BaseListAdapter(this));
        addObservedKeys(ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_BUDDIES), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_GAMES), ValueStore.concatenateKeys(Constant.USER_VALUES, Constant.NUMBER_ACHIEVEMENTS));
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        setNeedsRefresh();
    }

    public void onListItemClick(BaseListItem item) {
        User user = getUser();
        Factory factory = getFactory();
        if (item == getRecommendListItem()) {
            showDialogSafe(11, true);
        } else if (item == getAchievementsListItem()) {
            display(factory.createAchievementScreenDescription(user));
        } else if (item == getChallengesListItem()) {
            display(factory.createChallengeCreateScreenDescription(user, null));
        } else if (item == getBuddiesListItem()) {
            display(factory.createUserScreenDescription(user));
        } else if (item == getGamesListItem()) {
            display(factory.createGameScreenDescription(user, 0));
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 11:
                OkCancelDialog dialog = new OkCancelDialog(this);
                dialog.setOnActionListener(this);
                dialog.setOkButtonText(getResources().getString(R.string.sl_leave_accept_game_recommendation_ok));
                dialog.setCancelable(true);
                dialog.setOnDismissListener(this);
                return dialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 11:
                OkCancelDialog okCancelDialog = (OkCancelDialog) dialog;
                okCancelDialog.setText(getResources().getString(R.string.sl_leave_accept_game_recommendation, getGame().getName(), getUser().getDisplayName()));
                return;
            default:
                super.onPrepareDialog(id, dialog);
                return;
        }
    }

    public void onAction(BaseDialog dialog, int actionId) {
        dialog.dismiss();
        if (actionId == 0) {
            postRecommendation();
        }
    }

    public void onRefresh(int flags) {
        if (this._gameSectionDisplayOption == GameSectionDisplayOption.UNKNOWN) {
            if (isSessionGame()) {
                Boolean userPlaysGame = (Boolean) getActivityArguments().getValue(Constant.USER_PLAYS_SESSION_GAME);
                if (userPlaysGame == null) {
                    this._gameSectionDisplayOption = GameSectionDisplayOption.UNKNOWN;
                } else if (userPlaysGame.booleanValue()) {
                    this._gameSectionDisplayOption = GameSectionDisplayOption.SHOW;
                } else {
                    this._gameSectionDisplayOption = GameSectionDisplayOption.RECOMMEND;
                }
            } else {
                this._gameSectionDisplayOption = GameSectionDisplayOption.HIDE;
            }
            updateList();
        }
    }

    public void onValueChanged(ValueStore valueStore, String key, Object oldValue, Object newValue) {
        if (isValueChangedFor(key, Constant.NUMBER_BUDDIES, oldValue, newValue)) {
            getBuddiesListItem().setSubTitle(StringFormatter.getBuddiesSubTitle(this, getUserValues()));
            getBaseListAdapter().notifyDataSetChanged();
        } else if (isValueChangedFor(key, Constant.NUMBER_GAMES, oldValue, newValue)) {
            getGamesListItem().setSubTitle(StringFormatter.getGamesSubTitle(this, getUserValues()));
            getBaseListAdapter().notifyDataSetChanged();
        } else if (isValueChangedFor(key, Constant.NUMBER_ACHIEVEMENTS, oldValue, newValue)) {
            getAchievementsListItem().setSubTitle(StringFormatter.getAchievementsSubTitle(this, getUserValues(), false));
            getBaseListAdapter().notifyDataSetChanged();
        }
    }

    public void onValueSetDirty(ValueStore valueStore, String key) {
        if (Constant.NUMBER_BUDDIES.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
        if (Constant.NUMBER_GAMES.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
        if (getConfiguration().isFeatureEnabled(Configuration.Feature.ACHIEVEMENT) && Constant.NUMBER_ACHIEVEMENTS.equals(key)) {
            getUserValues().retrieveValue(key, ValueStore.RetrievalMode.NOT_DIRTY, null);
        }
    }

    /* access modifiers changed from: protected */
    public void postRecommendation() {
        MessageController controller = new MessageController(getRequestControllerObserver());
        controller.setTarget(getGame());
        controller.setMessageType(MessageController.TYPE_RECOMMENDATION);
        controller.addReceiverWithUsers(MessageController.RECEIVER_USER, getUser());
        if (controller.isSubmitAllowed()) {
            showSpinnerFor(controller);
            controller.submitMessage();
        }
    }

    public void requestControllerDidReceiveResponseSafe(RequestController aRequestController) {
        showToast(getString(R.string.sl_recommend_sent));
    }

    /* access modifiers changed from: protected */
    public void updateList() {
        BaseListAdapter<BaseListItem> adapter = getBaseListAdapter();
        adapter.clear();
        Configuration configuration = getConfiguration();
        switch ($SWITCH_TABLE$com$scoreloop$client$android$ui$component$user$UserDetailListActivity$GameSectionDisplayOption()[this._gameSectionDisplayOption.ordinal()]) {
            case 2:
                adapter.add(getGameCaptionListItem());
                adapter.add(getRecommendListItem());
                break;
            case 3:
                boolean showAchievements = configuration.isFeatureEnabled(Configuration.Feature.ACHIEVEMENT);
                boolean showChallenges = configuration.isFeatureEnabled(Configuration.Feature.CHALLENGE);
                if (showAchievements || showChallenges) {
                    adapter.add(getGameCaptionListItem());
                    if (showAchievements) {
                        adapter.add(getAchievementsListItem());
                    }
                    if (showChallenges) {
                        adapter.add(getChallengesListItem());
                        break;
                    }
                }
                break;
        }
        adapter.add(getCommunityCaptionListItem());
        adapter.add(getBuddiesListItem());
        adapter.add(getGamesListItem());
    }
}
