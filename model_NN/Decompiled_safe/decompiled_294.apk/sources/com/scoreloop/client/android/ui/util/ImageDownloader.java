package com.scoreloop.client.android.ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

public class ImageDownloader {
    private static final int DELAY_BEFORE_PURGE = 180000;
    private static final int HARD_CACHE_CAPACITY = 150;
    private static Cache<String, Bitmap> _cache = null;
    private static ImageDownloader imageDownloader = null;

    private static void assertImageDownloader() {
        if (imageDownloader == null) {
            imageDownloader = new ImageDownloader();
        }
    }

    private static void assertCache() {
        if (_cache == null) {
            _cache = new Cache<>(HARD_CACHE_CAPACITY);
        }
    }

    public static void downloadImage(String url, Drawable loadingDrawable, ImageView imageView, Drawable errorDrawable) {
        if (url != null) {
            assertImageDownloader();
            assertCache();
            Cache<K, V>.CacheEntry cacheEntry = _cache.getCacheEntry(url);
            if (cacheEntry == null) {
                imageDownloader.forceDownload(url, loadingDrawable, imageView, errorDrawable);
                return;
            }
            cancelPotentialDownload(url, imageView);
            Bitmap bitmap = (Bitmap) cacheEntry.getValue();
            if (bitmap != null || errorDrawable == null) {
                imageView.setImageBitmap(bitmap);
            } else {
                imageView.setImageDrawable(errorDrawable);
            }
        }
    }

    private void forceDownload(String url, Drawable drawable, ImageView imageView, Drawable errorDrawable) {
        if (cancelPotentialDownload(url, imageView)) {
            BitmapDownloaderTask task = new BitmapDownloaderTask(imageView, errorDrawable);
            imageView.setImageDrawable(new DownloadedDrawable(drawable, task));
            task.execute(url);
        }
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.url;
            if (bitmapUrl != null && bitmapUrl.equals(url)) {
                return false;
            }
            bitmapDownloaderTask.cancel(true);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                return ((DownloadedDrawable) drawable).getBitmapDownloaderTask();
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.scoreloop.client.android.ui.util.ImageDownloader.BitmapResult downloadBitmapHttp(java.lang.String r10) {
        /*
            r9 = this;
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
            r3.<init>(r10)
            org.apache.http.HttpResponse r5 = r0.execute(r3)     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            org.apache.http.StatusLine r7 = r5.getStatusLine()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            int r6 = r7.getStatusCode()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            r7 = 404(0x194, float:5.66E-43)
            if (r6 != r7) goto L_0x001f
            com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult r7 = com.scoreloop.client.android.ui.util.ImageDownloader.BitmapResult.createNotFound()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
        L_0x001e:
            return r7
        L_0x001f:
            r7 = 200(0xc8, float:2.8E-43)
            if (r6 == r7) goto L_0x0028
            com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult r7 = com.scoreloop.client.android.ui.util.ImageDownloader.BitmapResult.createError()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            goto L_0x001e
        L_0x0028:
            org.apache.http.HttpEntity r2 = r5.getEntity()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            if (r2 == 0) goto L_0x004f
            r4 = 0
            java.io.InputStream r4 = r2.getContent()     // Catch:{ all -> 0x0054 }
            com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult r7 = new com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult     // Catch:{ all -> 0x0054 }
            com.scoreloop.client.android.ui.util.ImageDownloader$FlushedInputStream r8 = new com.scoreloop.client.android.ui.util.ImageDownloader$FlushedInputStream     // Catch:{ all -> 0x0054 }
            r8.<init>(r4)     // Catch:{ all -> 0x0054 }
            android.graphics.Bitmap r8 = android.graphics.BitmapFactory.decodeStream(r8)     // Catch:{ all -> 0x0054 }
            r7.<init>(r8)     // Catch:{ all -> 0x0054 }
            if (r4 == 0) goto L_0x0046
            r4.close()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
        L_0x0046:
            r2.consumeContent()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            goto L_0x001e
        L_0x004a:
            r7 = move-exception
            r1 = r7
            r3.abort()
        L_0x004f:
            com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult r7 = com.scoreloop.client.android.ui.util.ImageDownloader.BitmapResult.createError()
            goto L_0x001e
        L_0x0054:
            r7 = move-exception
            if (r4 == 0) goto L_0x005a
            r4.close()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
        L_0x005a:
            r2.consumeContent()     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
            throw r7     // Catch:{ IOException -> 0x004a, IllegalStateException -> 0x005e, Exception -> 0x0064 }
        L_0x005e:
            r7 = move-exception
            r1 = r7
            r3.abort()
            goto L_0x004f
        L_0x0064:
            r7 = move-exception
            r1 = r7
            r3.abort()
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.scoreloop.client.android.ui.util.ImageDownloader.downloadBitmapHttp(java.lang.String):com.scoreloop.client.android.ui.util.ImageDownloader$BitmapResult");
    }

    /* access modifiers changed from: package-private */
    public BitmapResult downloadBitmap(Context context, String url) {
        BitmapResult bitmapResult = LocalImageStorage.getBitmap(context, url);
        if (bitmapResult == null) {
            bitmapResult = downloadBitmapHttp(url);
            if (bitmapResult.isCachable()) {
                LocalImageStorage.putBitmap(context, url, bitmapResult);
            }
        }
        return bitmapResult;
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    class BitmapDownloaderTask extends AsyncTask<String, Void, BitmapResult> {
        private final Drawable errorDrawable;
        private final WeakReference<ImageView> imageViewReference;
        /* access modifiers changed from: private */
        public String url;

        public BitmapDownloaderTask(ImageView imageView, Drawable errorDrawable2) {
            this.imageViewReference = new WeakReference<>(imageView);
            this.errorDrawable = errorDrawable2;
        }

        /* access modifiers changed from: protected */
        public BitmapResult doInBackground(String... params) {
            ImageView imageView = this.imageViewReference.get();
            this.url = params[0];
            if (imageView != null) {
                return ImageDownloader.this.downloadBitmap(imageView.getContext(), this.url);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(BitmapResult bitmapResult) {
            Bitmap bitmap;
            if (isCancelled() || bitmapResult == null) {
                bitmap = null;
            } else {
                bitmap = bitmapResult.getBitmap();
            }
            if (bitmapResult != null && bitmapResult.isCachable()) {
                ImageDownloader.this.addBitmapToCache(this.url, bitmap);
            }
            if (this.imageViewReference != null) {
                ImageView imageView = this.imageViewReference.get();
                if (this != ImageDownloader.getBitmapDownloaderTask(imageView)) {
                    return;
                }
                if (bitmap != null || this.errorDrawable == null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    imageView.setImageDrawable(this.errorDrawable);
                }
            }
        }
    }

    static class DownloadedDrawable extends BitmapDrawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(Drawable drawable, BitmapDownloaderTask bitmapDownloaderTask) {
            super(((BitmapDrawable) drawable).getBitmap());
            this.bitmapDownloaderTaskReference = new WeakReference<>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return this.bitmapDownloaderTaskReference.get();
        }
    }

    /* access modifiers changed from: private */
    public void addBitmapToCache(String url, Bitmap bitmap) {
        _cache.put(url, bitmap, 180000);
    }

    static class BitmapResult {
        private final Bitmap bitmap;
        private final Status status;

        private enum Status {
            OK,
            NOT_FOUND,
            ERROR
        }

        static BitmapResult createNotFound() {
            return new BitmapResult(null, Status.NOT_FOUND);
        }

        static BitmapResult createError() {
            return new BitmapResult(null, Status.ERROR);
        }

        BitmapResult(Bitmap bitmap2) {
            this.bitmap = bitmap2;
            this.status = Status.OK;
        }

        BitmapResult(Bitmap bitmap2, Status status2) {
            this.bitmap = bitmap2;
            this.status = status2;
        }

        /* access modifiers changed from: package-private */
        public Bitmap getBitmap() {
            return this.bitmap;
        }

        /* access modifiers changed from: package-private */
        public boolean isCachable() {
            return this.status != Status.ERROR;
        }

        /* access modifiers changed from: package-private */
        public boolean isNotFound() {
            return this.status == Status.NOT_FOUND;
        }
    }
}
