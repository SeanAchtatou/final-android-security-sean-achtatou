package wall.bt.wp.P090;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.xl.util.BookMarkUtil;
import com.xl.util.Constant;
import com.xl.util.FileUtil;
import com.xl.util.Folder;
import com.xl.util.GenUtil;
import com.xl.util.MultiDownloadNew;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import wall.bt.wp.P090.Protocals;

public class LocalImg extends BaseImgActivity {
    private static final int DIALOG_ADDFAVORITE = 0;
    private static final int NumPerPage = 6;
    private static final String TAG = "SortByViews";
    private static boolean bPageInterrupt;
    private static String mPath;
    /* access modifiers changed from: private */
    public boolean bIndownload = false;
    private Boolean bLocal = true;
    private ImageButton btnNext;
    private ImageButton btnPrev;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public int iDeletePosition = -1;
    private int iRunCounter = 0;
    private MyAdapter mAdapter;
    private List<Protocals.Album> mDataAlbum = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> mDataAlbumName = new ArrayList();
    /* access modifiers changed from: private */
    public List<Protocals.Album> mDataAlbumProc = new ArrayList();
    private List mDataImg = new ArrayList();
    private List<String> mDataURLList = new ArrayList();
    private TextView mDownloadPercent;
    private GridView mGridView;
    private int mPageIndex = 0;
    private TextView mPageInfo;
    private int mPageTotal;
    private ProgressBar mProgress;
    private RelativeLayout mProgressLayout;
    /* access modifiers changed from: private */
    public MyHandler myHandler = new MyHandler();
    private String strRelativePath = "";
    private String strWebImgRootURL = "";

    static /* synthetic */ int access$208(LocalImg x0) {
        int i = x0.mPageIndex;
        x0.mPageIndex = i + 1;
        return i;
    }

    static /* synthetic */ int access$210(LocalImg x0) {
        int i = x0.mPageIndex;
        x0.mPageIndex = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.moreimg);
        initData();
        initView();
        this.iRunCounter++;
        showAlbums(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1);
    }

    public void onResume() {
        GenUtil.systemPrintln("-------------- OnResume called !");
        if (this.iRunCounter > 0) {
            showAlbums(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1);
        }
        super.onResume();
    }

    private void showAlbums(String strURL) {
        this.mDataURLList.add(strURL);
        this.mDataAlbum.clear();
        if (this.bLocal.booleanValue()) {
            this.mDataAlbum = getAlbumBookMark(strURL);
        } else {
            this.mDataAlbum = Protocals.getInstance().getCategories(strURL);
        }
        if (this.mDataAlbum.size() == 0) {
            MainActivity.tabHost.setCurrentTab(1);
            return;
        }
        calTotals();
        if (this.mPageIndex + 1 > this.mPageTotal) {
            this.mPageIndex--;
        }
        downAlbum();
        updateView();
    }

    private void calTotals() {
        this.mPageTotal = this.mDataAlbum.size() / NumPerPage;
        if (this.mPageTotal * NumPerPage != this.mDataAlbum.size()) {
            this.mPageTotal++;
        }
    }

    private List<Protocals.Album> getAlbumBookMark(String strBookMarkLoc) {
        List<Protocals.Album> list = new ArrayList<>();
        List<String> listBookMark = new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").getElementList();
        GenUtil.systemPrintln("Folder.MYBOOKMARK = /sdcard/PopImg/mycollection/bookmark.xml");
        GenUtil.systemPrintln("listBookMark = " + listBookMark.size());
        for (int i = 0; i < listBookMark.size(); i++) {
            String[] strTmpArray = listBookMark.get(i).split("\\|\\|\\|");
            if (strTmpArray.length >= 3) {
                Protocals p = new Protocals();
                p.getClass();
                Protocals.Album album = new Protocals.Album();
                album.name = strTmpArray[0];
                album.indexurl = strTmpArray[1];
                album.image = strTmpArray[2];
                album.category = "S";
                list.add(album);
            }
        }
        return list;
    }

    /* access modifiers changed from: private */
    public void downAlbum() {
        clearImg();
        this.bIndownload = false;
        this.mDataAlbumProc.clear();
        this.mAdapter.notifyDataSetChanged();
        this.myHandler.showMyDialog();
        GenUtil.systemPrintln("getRelativePath = " + getRelativePath(this.mDataURLList));
        this.strRelativePath = getRelativePath(this.mDataURLList);
        int iTask = 0;
        int iStarts = this.mPageIndex * NumPerPage;
        int iEnds = (iStarts + NumPerPage) - 1;
        if (iStarts < this.mDataAlbum.size()) {
            if (iEnds >= this.mDataAlbum.size()) {
                iEnds = this.mDataAlbum.size() - 1;
            }
            GenUtil.systemPrintln("iStarts = " + iStarts);
            GenUtil.systemPrintln("iEnds = " + iEnds);
            for (int iIndex = iStarts; iIndex <= iEnds; iIndex++) {
                Protocals.Album album = this.mDataAlbum.get(iIndex);
                if (album != null) {
                    Object[] arrObjects = getObjectList(album);
                    if (FileUtil.fileExist((String) arrObjects[1])) {
                        addAdapterData(album.name);
                    } else if (!this.bLocal.booleanValue()) {
                        if (iTask <= 2) {
                            try {
                                Thread.currentThread();
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            this.bIndownload = true;
                            new QueryImgTask().execute(arrObjects);
                            iTask++;
                        } else {
                            this.mDataAlbumProc.add(album);
                        }
                    }
                }
            }
            this.myHandler.dismissMyDialog();
        }
    }

    /* access modifiers changed from: private */
    public Object[] getObjectList(Protocals.Album album) {
        Object[] arrObjects = new Object[4];
        if (this.bLocal.booleanValue()) {
            arrObjects[0] = album.indexurl;
            arrObjects[1] = album.image;
            arrObjects[2] = "";
            arrObjects[3] = "";
        } else {
            arrObjects[0] = this.strWebImgRootURL + this.strRelativePath + album.image;
            arrObjects[1] = mPath + this.strRelativePath + album.image;
            arrObjects[2] = String.valueOf(3);
            arrObjects[3] = album.name;
        }
        GenUtil.systemPrintln("arrObjects[0] = " + arrObjects[0]);
        GenUtil.systemPrintln("arrObjects[1] = " + arrObjects[1]);
        GenUtil.systemPrintln("arrObjects[2] = " + arrObjects[2]);
        GenUtil.systemPrintln("arrObjects[3] = " + arrObjects[3]);
        return arrObjects;
    }

    private void clearImg() {
        this.mDataAlbumName.clear();
        for (int i = 0; i < this.mDataImg.size(); i++) {
            Bitmap bitmap = (Bitmap) this.mDataImg.get(i);
        }
        this.mDataImg.clear();
    }

    private String getRelativePath(List<String> mURLList) {
        if (mURLList.size() <= 1) {
            return "";
        }
        return mURLList.get(mURLList.size() - 1).substring(mURLList.get(0).length());
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.mPageIndex <= 0) {
            this.btnPrev.setVisibility(8);
        } else {
            this.btnPrev.setVisibility(0);
        }
        if (this.mPageIndex < this.mPageTotal - 1) {
            this.btnNext.setVisibility(0);
        } else {
            this.btnNext.setVisibility(8);
        }
        StringBuilder sb = new StringBuilder(getResources().getString(R.string.page_info_head));
        sb.append(this.mPageIndex + 1).append("/");
        sb.append(this.mPageTotal);
        this.mPageInfo.setText(sb.toString());
    }

    private void initData() {
        this.dialog = new ProgressDialog(this);
        if (Environment.getExternalStorageState().equals("mounted")) {
            mPath = Folder.MYCOLLECTION;
        } else {
            mPath = Folder.MYCOLLECTION1;
        }
    }

    private void initView() {
        this.mProgressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        this.mProgress = (ProgressBar) findViewById(R.id.progress);
        this.mDownloadPercent = (TextView) findViewById(R.id.download_percent);
        this.mGridView = (GridView) findViewById(R.id.album_list);
        this.mPageInfo = (TextView) findViewById(R.id.page_info);
        this.btnPrev = (ImageButton) findViewById(R.id.prev);
        this.btnNext = (ImageButton) findViewById(R.id.next);
        this.mAdapter = new MyAdapter();
        this.mGridView.setAdapter((ListAdapter) this.mAdapter);
        this.btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LocalImg.access$210(LocalImg.this);
                LocalImg.this.downAlbum();
                LocalImg.this.updateView();
            }
        });
        this.btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LocalImg.access$208(LocalImg.this);
                LocalImg.this.downAlbum();
                LocalImg.this.updateView();
            }
        });
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                GenUtil.systemPrintln("arg2 = " + arg2);
                Object[] arrObjects = LocalImg.this.getObjectList(LocalImg.this.getAlbum((String) LocalImg.this.mDataAlbumName.get(arg2)));
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                String strURLTmp = (String) arrObjects[0];
                bundle.putString("url", strURLTmp.substring(0, strURLTmp.lastIndexOf("/") + 1));
                intent.putExtras(bundle);
                intent.setClass(LocalImg.this, GridActivity.class);
                LocalImg.this.startActivityForResult(intent, 0);
            }
        });
        this.mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                int unused = LocalImg.this.iDeletePosition = arg2;
                LocalImg.this.showInfoDelBookMark(LocalImg.this.getResources().getString(R.string.txt_bookmark_title), LocalImg.this.getResources().getString(R.string.txt_delbookmark_body));
                return true;
            }
        });
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
    }

    public void showInfoDelBookMark(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LocalImg.this.delBookMark();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    public void delBookMark() {
        new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").delete((this.mPageIndex * NumPerPage) + this.iDeletePosition);
        showAlbums(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Protocals.Album getAlbum(String strAlbumName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            if (this.mDataAlbum.get(i).name.equals(strAlbumName)) {
                return this.mDataAlbum.get(i);
            }
        }
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        quitSystem();
        return true;
    }

    /* access modifiers changed from: private */
    public int getSize() {
        if (this.mDataImg != null) {
            return this.mDataImg.size();
        }
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: private */
    public Bitmap getElement(int iIndex) {
        if (this.mDataImg == null || iIndex < 0 || iIndex >= this.mDataImg.size()) {
            return null;
        }
        return (Bitmap) this.mDataImg.get(iIndex);
    }

    class AlbumInfo {
        Protocals.Album mAlbum;
        Bitmap mThumbnail;
        int tag = -1;

        public AlbumInfo() {
        }
    }

    public final class ViewHolder {
        public ImageView iv_img_cat;
        public TextView tv_txt_cat;

        public ViewHolder() {
        }
    }

    class MyAdapter extends BaseAdapter {
        public MyAdapter() {
        }

        public int getCount() {
            return LocalImg.this.getSize();
        }

        public Object getItem(int iIndex) {
            return LocalImg.this.getElement(iIndex);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(LocalImg.this).inflate((int) R.layout.grid_category, (ViewGroup) null);
                holder.iv_img_cat = (ImageView) convertView.findViewById(R.id.iv_img_cat);
                holder.tv_txt_cat = (TextView) convertView.findViewById(R.id.tv_txt_cat);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iv_img_cat.setImageBitmap(LocalImg.this.getElement(position));
            holder.tv_txt_cat.setText((CharSequence) LocalImg.this.mDataAlbumName.get(position));
            return convertView;
        }
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final String REMOTE_VERSION = "remote_version";
        private static final int SET_PAGEINFO = 5;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        /* access modifiers changed from: private */
        public void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        /* access modifiers changed from: private */
        public void showMyDialog() {
            sendEmptyMessage(SHOW_DIALOG);
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (LocalImg.this.dialog != null) {
                        LocalImg.this.dialog.dismiss();
                    }
                    LocalImg.this.addAdapterData((String) paramMessage.obj);
                    if (LocalImg.this.mDataAlbumProc.size() > 0) {
                        new QueryImgTask().execute(LocalImg.this.getObjectList((Protocals.Album) LocalImg.this.mDataAlbumProc.get(0)));
                        LocalImg.this.mDataAlbumProc.remove(0);
                        break;
                    }
                    break;
                case SHOW_DIALOG /*6*/:
                    if (LocalImg.this.dialog != null) {
                        LocalImg.this.dialog.setMessage("Loading...");
                        LocalImg.this.dialog.show();
                        break;
                    }
                    break;
                case DISMISS_DIALOG /*7*/:
                    if (LocalImg.this.dialog != null) {
                        LocalImg.this.dialog.dismiss();
                        break;
                    }
                    break;
                case SHOW_MSG /*8*/:
                    Bundle bundle = paramMessage.getData();
                    if (bundle != null) {
                        new AlertDialog.Builder(LocalImg.this).setTitle("Message:").setIcon((int) R.drawable.toast_warnning).setMessage(bundle.getString(INFO)).setPositiveButton("Never show", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    private String GetImagePathByName(String strImgName) {
        for (int i = 0; i < this.mDataAlbum.size(); i++) {
            Protocals.Album album = this.mDataAlbum.get(i);
            if (album.name.equals(strImgName)) {
                return album.image;
            }
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void addAdapterData(String strImgName) {
        this.mDataAlbumName.add(strImgName);
        String strImgPath = GetImagePathByName(strImgName);
        if (!this.bLocal.booleanValue()) {
            strImgPath = mPath + this.strRelativePath + strImgPath;
        }
        GenUtil.systemPrintln("strImgPath = " + strImgPath);
        this.mDataImg.add(BitmapFactory.decodeFile(strImgPath));
        this.mAdapter.notifyDataSetChanged();
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
            LocalImg.this.myHandler.sendEmptyMessage(1);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            Message m = new Message();
            m.what = LocalImg.NumPerPage;
            m.obj = iPercent + "%";
            LocalImg.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP = (Environment.getExternalStorageState().equals("mounted") ? Folder.POPIMG : Folder.POPIMG1) + Constant.TMP;
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strAlbumImage = (String) arrParams[3];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    while (multiDownload.getPercntInt() < 100 && LocalImg.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = 2;
                            LocalImg.this.myHandler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            LocalImg.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    if (multiDownload.getPercntInt() != 100 || !LocalImg.this.bIndownload) {
                        m2.what = LocalImg.NumPerPage;
                    } else {
                        m2.obj = strAlbumImage;
                        m2.what = 1;
                    }
                    LocalImg.this.myHandler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    public class LocalAlbum {
        public long addtime;
        public boolean bIcon;
        public String category;
        public String image;
        public String indexfile;
        public String mini;
        public String name;

        public LocalAlbum() {
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.local_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.deletealbum /*2131230786*/:
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.txt_deletealbum_msg), 1).show();
                break;
        }
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                LocalImg.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        GenUtil.systemPrintln("requestCode = " + requestCode);
        GenUtil.systemPrintln("resultCode = " + resultCode);
        switch (requestCode) {
            case 0:
                switch (resultCode) {
                    case -1:
                        MainActivity.tabHost.setCurrentTab(1);
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }
}
