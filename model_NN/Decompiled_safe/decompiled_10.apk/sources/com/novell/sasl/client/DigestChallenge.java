package com.novell.sasl.client;

import java.util.ArrayList;
import java.util.Iterator;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.apache.james.mime4j.field.ContentTypeField;

class DigestChallenge {
    private static final int CIPHER_3DES = 1;
    private static final int CIPHER_DES = 2;
    private static final int CIPHER_RC4 = 8;
    private static final int CIPHER_RC4_40 = 4;
    private static final int CIPHER_RC4_56 = 16;
    private static final int CIPHER_RECOGNIZED_MASK = 31;
    private static final int CIPHER_UNRECOGNIZED = 32;
    public static final int QOP_AUTH = 1;
    public static final int QOP_AUTH_CONF = 4;
    public static final int QOP_AUTH_INT = 2;
    public static final int QOP_UNRECOGNIZED = 8;
    private String m_algorithm = null;
    private String m_characterSet = null;
    private int m_cipherOptions = 0;
    private int m_maxBuf = -1;
    private String m_nonce = null;
    private int m_qop = 0;
    private ArrayList m_realms = new ArrayList(5);
    private boolean m_staleFlag = false;

    DigestChallenge(byte[] challenge) throws SaslException {
        DirectiveList dirList = new DirectiveList(challenge);
        try {
            dirList.parseDirectives();
            checkSemantics(dirList);
        } catch (SaslException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void checkSemantics(DirectiveList dirList) throws SaslException {
        Iterator directives = dirList.getIterator();
        while (directives.hasNext()) {
            ParsedDirective directive = (ParsedDirective) directives.next();
            String name = directive.getName();
            if (name.equals("realm")) {
                handleRealm(directive);
            } else if (name.equals("nonce")) {
                handleNonce(directive);
            } else if (name.equals("qop")) {
                handleQop(directive);
            } else if (name.equals("maxbuf")) {
                handleMaxbuf(directive);
            } else if (name.equals(ContentTypeField.PARAM_CHARSET)) {
                handleCharset(directive);
            } else if (name.equals("algorithm")) {
                handleAlgorithm(directive);
            } else if (name.equals("cipher")) {
                handleCipher(directive);
            } else if (name.equals("stale")) {
                handleStale(directive);
            }
        }
        if (-1 == this.m_maxBuf) {
            this.m_maxBuf = 65536;
        }
        if (this.m_qop == 0) {
            this.m_qop = 1;
        } else if ((this.m_qop & 1) != 1) {
            throw new SaslException("Only qop-auth is supported by client");
        } else if ((this.m_qop & 4) == 4 && (this.m_cipherOptions & 31) == 0) {
            throw new SaslException("Invalid cipher options");
        } else if (this.m_nonce == null) {
            throw new SaslException("Missing nonce directive");
        } else if (this.m_staleFlag) {
            throw new SaslException("Unexpected stale flag");
        } else if (this.m_algorithm == null) {
            throw new SaslException("Missing algorithm directive");
        }
    }

    /* access modifiers changed from: package-private */
    public void handleNonce(ParsedDirective pd) throws SaslException {
        if (this.m_nonce != null) {
            throw new SaslException("Too many nonce values.");
        }
        this.m_nonce = pd.getValue();
    }

    /* access modifiers changed from: package-private */
    public void handleRealm(ParsedDirective pd) {
        this.m_realms.add(pd.getValue());
    }

    /* access modifiers changed from: package-private */
    public void handleQop(ParsedDirective pd) throws SaslException {
        if (this.m_qop != 0) {
            throw new SaslException("Too many qop directives.");
        }
        TokenParser parser = new TokenParser(pd.getValue());
        for (String token = parser.parseToken(); token != null; token = parser.parseToken()) {
            if (token.equals("auth")) {
                this.m_qop |= 1;
            } else if (token.equals("auth-int")) {
                this.m_qop |= 2;
            } else if (token.equals("auth-conf")) {
                this.m_qop |= 4;
            } else {
                this.m_qop |= 8;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void handleMaxbuf(ParsedDirective pd) throws SaslException {
        if (-1 != this.m_maxBuf) {
            throw new SaslException("Too many maxBuf directives.");
        }
        this.m_maxBuf = Integer.parseInt(pd.getValue());
        if (this.m_maxBuf == 0) {
            throw new SaslException("Max buf value must be greater than zero.");
        }
    }

    /* access modifiers changed from: package-private */
    public void handleCharset(ParsedDirective pd) throws SaslException {
        if (this.m_characterSet != null) {
            throw new SaslException("Too many charset directives.");
        }
        this.m_characterSet = pd.getValue();
        if (!this.m_characterSet.equals("utf-8")) {
            throw new SaslException("Invalid character encoding directive");
        }
    }

    /* access modifiers changed from: package-private */
    public void handleAlgorithm(ParsedDirective pd) throws SaslException {
        if (this.m_algorithm != null) {
            throw new SaslException("Too many algorithm directives.");
        }
        this.m_algorithm = pd.getValue();
        if (!"md5-sess".equals(this.m_algorithm)) {
            throw new SaslException("Invalid algorithm directive value: " + this.m_algorithm);
        }
    }

    /* access modifiers changed from: package-private */
    public void handleCipher(ParsedDirective pd) throws SaslException {
        if (this.m_cipherOptions != 0) {
            throw new SaslException("Too many cipher directives.");
        }
        TokenParser parser = new TokenParser(pd.getValue());
        String parseToken = parser.parseToken();
        for (String token = parser.parseToken(); token != null; token = parser.parseToken()) {
            if ("3des".equals(token)) {
                this.m_cipherOptions |= 1;
            } else if ("des".equals(token)) {
                this.m_cipherOptions |= 2;
            } else if ("rc4-40".equals(token)) {
                this.m_cipherOptions |= 4;
            } else if ("rc4".equals(token)) {
                this.m_cipherOptions |= 8;
            } else if ("rc4-56".equals(token)) {
                this.m_cipherOptions |= 16;
            } else {
                this.m_cipherOptions |= 32;
            }
        }
        if (this.m_cipherOptions == 0) {
            this.m_cipherOptions = 32;
        }
    }

    /* access modifiers changed from: package-private */
    public void handleStale(ParsedDirective pd) throws SaslException {
        if (this.m_staleFlag) {
            throw new SaslException("Too many stale directives.");
        } else if ("true".equals(pd.getValue())) {
            this.m_staleFlag = true;
        } else {
            throw new SaslException("Invalid stale directive value: " + pd.getValue());
        }
    }

    public ArrayList getRealms() {
        return this.m_realms;
    }

    public String getNonce() {
        return this.m_nonce;
    }

    public int getQop() {
        return this.m_qop;
    }

    public boolean getStaleFlag() {
        return this.m_staleFlag;
    }

    public int getMaxBuf() {
        return this.m_maxBuf;
    }

    public String getCharacterSet() {
        return this.m_characterSet;
    }

    public String getAlgorithm() {
        return this.m_algorithm;
    }

    public int getCipherOptions() {
        return this.m_cipherOptions;
    }
}
