package com.house365.app.analyse.ext.json;

public class Test {
    public static void main(String[] args) throws JSONException {
        JSONObject jsonObject = new JSONObject("{service:{name:register,method:function1,params:[1,2,3]}}");
        System.out.println(jsonObject.toString());
        JSONObject service = jsonObject.getJSONObject("service");
        System.out.println(service.toString());
        if (service.has("params")) {
            JSONArray array = service.getJSONArray("params");
            System.out.println(array.length());
            for (int i = 0; i < array.length(); i++) {
                System.out.println(array.get(i));
            }
            for (String s : array.toStringArray()) {
                System.out.println(s);
            }
        }
    }
}
