package com.house365.app.analyse.data;

public class Event {
    public static final int EVENT_AD = 5;
    public static final int EVENT_APP = 1;
    public static final int EVENT_CALL = 6;
    public static final int EVENT_CALL_API = 3;
    public static final int EVENT_ONCLICK = 4;
    public static final int EVENT_PAGE = 2;
}
