package com.house365.app.analyse;

import android.util.DisplayMetrics;

public class AnalyseConfig {
    private String app;
    private int bufferSize;
    private String channel;
    private DisplayMetrics dispalyMetrics;
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public DisplayMetrics getDispalyMetrics() {
        return this.dispalyMetrics;
    }

    public void setDispalyMetrics(DisplayMetrics dispalyMetrics2) {
        this.dispalyMetrics = dispalyMetrics2;
    }

    public String getApp() {
        return this.app;
    }

    public void setApp(String app2) {
        this.app = app2;
    }

    public AnalyseConfig(String url2, DisplayMetrics dispalyMetrics2, String app2, int bufferSize2, String channel2) {
        this.url = url2;
        this.dispalyMetrics = dispalyMetrics2;
        this.app = app2;
        this.bufferSize = bufferSize2;
        this.channel = channel2;
    }

    public int getBufferSize() {
        return this.bufferSize;
    }

    public void setBufferSize(int bufferSize2) {
        this.bufferSize = bufferSize2;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel2) {
        this.channel = channel2;
    }
}
