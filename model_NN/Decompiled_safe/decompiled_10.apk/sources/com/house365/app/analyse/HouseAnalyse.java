package com.house365.app.analyse;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.app.analyse.ext.json.JSONArray;
import com.house365.app.analyse.ext.json.JSONException;
import com.house365.app.analyse.ext.json.JSONObject;
import com.house365.app.analyse.http.AnalyseHttpAPI;
import com.house365.app.analyse.store.SharedPreferencesUtil;
import com.house365.core.application.BaseApplication;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class HouseAnalyse {
    private static final String HOUSE_ANALYSE = "HOUSE_ANALYSE";
    public static final int OP_E = 0;
    public static final int OP_S = 1;
    private static AnalyseHttpAPI api = new AnalyseHttpAPI();
    private static AnalyseConfig config;
    private static String deviceId;
    /* access modifiers changed from: private */
    public static int flushSize = 10;
    public static boolean isAyncing = false;
    private static String network;
    /* access modifiers changed from: private */
    public static SharedPreferencesUtil pref;
    public static String provider;
    private static String screen;
    private static Object syn_obj = new Object();
    private static String version;

    private static void init(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        deviceId = telephonyManager.getDeviceId();
        try {
            version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String IMSI = telephonyManager.getSubscriberId();
        if (IMSI == null) {
            provider = "未知";
        } else if (IMSI.startsWith("46000") || IMSI.startsWith("46002")) {
            provider = "中国移动";
        } else if (IMSI.startsWith("46001")) {
            provider = "中国联通";
        } else if (IMSI.startsWith("46003")) {
            provider = "中国电信";
        } else {
            provider = "未知";
        }
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            network = networkInfo.getTypeName();
        }
        pref = new SharedPreferencesUtil(context);
    }

    public static String getScreen(Context context) {
        if (screen == null && config != null) {
            DisplayMetrics dm = config.getDispalyMetrics();
            screen = String.valueOf(dm.widthPixels) + "*" + dm.heightPixels;
        }
        return screen;
    }

    public static void setInitConfig(AnalyseConfig tconfig) {
        config = tconfig;
        flushSize = config.getBufferSize();
    }

    private static long getTimeWithSeconds(Date date) {
        return date.getTime() / 1000;
    }

    public static AnalyseMetaData onEvent(Context context, int event, String eventobj, String page, Date startTime, Date endtTme, boolean saveData) {
        long timeWithSeconds;
        long timeWithSeconds2;
        if (deviceId == null) {
            init(context);
        }
        String city = ((BaseApplication) context.getApplicationContext()).getCityName();
        String app = config.getApp();
        String str = version;
        String str2 = deviceId;
        long timeWithSeconds3 = getTimeWithSeconds(new Date());
        String str3 = provider;
        String screen2 = getScreen(context);
        String str4 = Build.MODEL;
        if (startTime == null) {
            timeWithSeconds = -1;
        } else {
            timeWithSeconds = getTimeWithSeconds(startTime);
        }
        if (endtTme == null) {
            timeWithSeconds2 = -1;
        } else {
            timeWithSeconds2 = getTimeWithSeconds(endtTme);
        }
        AnalyseMetaData data = new AnalyseMetaData(app, str, 1, str2, event, eventobj, timeWithSeconds3, page, str3, screen2, str4, timeWithSeconds, timeWithSeconds2, config.getChannel(), network, city, Build.VERSION.RELEASE);
        if (saveData) {
            pref.addListItem(HOUSE_ANALYSE, data);
        }
        return data;
    }

    public static AnalyseMetaData onEvent(Context context, int event, String eventobj, String page, Date startTime, Date endtTme, String extra_data, boolean saveData) {
        long timeWithSeconds;
        long timeWithSeconds2;
        if (deviceId == null) {
            init(context);
        }
        String city = ((BaseApplication) context.getApplicationContext()).getCityName();
        String app = config.getApp();
        String str = version;
        String str2 = deviceId;
        long timeWithSeconds3 = getTimeWithSeconds(new Date());
        String str3 = provider;
        String screen2 = getScreen(context);
        String str4 = Build.MODEL;
        if (startTime == null) {
            timeWithSeconds = -1;
        } else {
            timeWithSeconds = getTimeWithSeconds(startTime);
        }
        if (endtTme == null) {
            timeWithSeconds2 = -1;
        } else {
            timeWithSeconds2 = getTimeWithSeconds(endtTme);
        }
        AnalyseMetaData data = new AnalyseMetaData(app, str, 1, str2, event, eventobj, timeWithSeconds3, page, str3, screen2, str4, timeWithSeconds, timeWithSeconds2, config.getChannel(), network, city, Build.VERSION.RELEASE, extra_data);
        if (saveData) {
            pref.addListItem(HOUSE_ANALYSE, data);
        }
        return data;
    }

    public static AnalyseMetaData onPageResume(Context context) {
        String pageName;
        ActivityInfo actInfo = getActivityInfo((Activity) context);
        if (actInfo != null) {
            CharSequence cs = actInfo.loadLabel(context.getPackageManager());
            pageName = cs == null ? StringUtils.EMPTY : cs.toString();
        } else {
            pageName = context.getClass().getName();
        }
        return onEvent(context, 2, pageName, pageName, new Date(), null, false);
    }

    public static AnalyseMetaData onPagePause(AnalyseMetaData data) {
        data.setEndtime(getTimeWithSeconds(new Date()));
        pref.addListItem(HOUSE_ANALYSE, data);
        ayncFlush();
        return data;
    }

    public static AnalyseMetaData onAppStart(Context context) {
        return onEvent(context, 1, config.getApp(), null, new Date(), null, false);
    }

    public static AnalyseMetaData onAppDestory(BaseApplication application, AnalyseMetaData data) {
        data.setCity(application.getCityName());
        data.setEndtime(getTimeWithSeconds(new Date()));
        pref.addListItem(HOUSE_ANALYSE, data);
        return data;
    }

    public static AnalyseMetaData onViewClick(Context context, String eventobj, String type, String extra_data) {
        String pageName;
        ActivityInfo actInfo = getActivityInfo((Activity) context);
        if (actInfo != null) {
            CharSequence cs = actInfo.loadLabel(context.getPackageManager());
            pageName = cs == null ? StringUtils.EMPTY : cs.toString();
        } else {
            pageName = context.getClass().getName();
        }
        AnalyseMetaData data = onEvent(context, 4, eventobj, pageName, new Date(), new Date(), extra_data, true);
        ayncFlush();
        return data;
    }

    public static AnalyseMetaData onCallAPI(Context context, String eventobj, String type) {
        AnalyseMetaData data = onEvent(context, 3, eventobj, type, new Date(), new Date(), true);
        ayncFlush();
        return data;
    }

    public static AnalyseMetaData onAD(Context context, String adname, String type, String extra_data) {
        AnalyseMetaData data = onEvent(context, 5, adname, type, new Date(), new Date(), extra_data, true);
        ayncFlush();
        return data;
    }

    public static AnalyseMetaData onCall(Context context, String toNo, String type) {
        AnalyseMetaData data = onEvent(context, 6, toNo, type, new Date(), new Date(), true);
        ayncFlush();
        return data;
    }

    public static boolean flush() {
        try {
            boolean flag = flushData(pref.getListWithCast(new AnalyseMetaData(), HOUSE_ANALYSE));
            if (!flag) {
                return flag;
            }
            pref.clean(HOUSE_ANALYSE);
            return flag;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean getAync() {
        boolean z;
        synchronized (syn_obj) {
            z = isAyncing;
        }
        return z;
    }

    public static void setAync() {
        synchronized (syn_obj) {
            isAyncing = !isAyncing;
        }
    }

    public static void ayncFlush() {
        if (!getAync()) {
            setAync();
            new Thread() {
                public void run() {
                    List<AnalyseMetaData> datas = null;
                    try {
                        datas = HouseAnalyse.pref.getListWithCast(new AnalyseMetaData(), HouseAnalyse.HOUSE_ANALYSE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (datas != null && datas.size() >= HouseAnalyse.flushSize && HouseAnalyse.flushData(datas)) {
                        for (AnalyseMetaData data : datas) {
                            HouseAnalyse.pref.removeListItem(HouseAnalyse.HOUSE_ANALYSE, data);
                        }
                    }
                    HouseAnalyse.setAync();
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    public static boolean flushData(List<AnalyseMetaData> datas) {
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("service", generateRequestData("analyse", "addAnalyseMetaDataWithList", datas).toString()));
            JSONObject json = new JSONObject(api.post(config.getUrl(), params));
            if (json == null || !"success".equals(json.getString("result")) || 1 != json.getInt("returnobject")) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static JSONObject generateRequestData(String service, String method, List<AnalyseMetaData> datas) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("name", service);
        json.put("method", method);
        JSONArray params = new JSONArray();
        params.put(new JSONArray((Collection) datas));
        json.put("params", params);
        Log.i("HouseAnalyse", json.toString());
        return json;
    }

    private static ActivityInfo getActivityInfo(Activity activity) {
        try {
            return activity.getPackageManager().getActivityInfo(activity.getComponentName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
