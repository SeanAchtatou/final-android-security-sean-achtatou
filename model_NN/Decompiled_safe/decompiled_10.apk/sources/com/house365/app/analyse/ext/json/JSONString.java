package com.house365.app.analyse.ext.json;

public interface JSONString {
    String toJSONString();
}
