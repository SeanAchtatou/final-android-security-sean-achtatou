package com.house365.app.analyse.store;

public class SharedPreferenceException extends Exception {
    public SharedPreferenceException() {
    }

    public SharedPreferenceException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public SharedPreferenceException(String detailMessage) {
        super(detailMessage);
    }

    public SharedPreferenceException(Throwable throwable) {
        super(throwable);
    }
}
