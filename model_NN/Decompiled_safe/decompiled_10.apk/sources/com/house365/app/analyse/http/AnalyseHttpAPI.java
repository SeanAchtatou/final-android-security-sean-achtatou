package com.house365.app.analyse.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLHandshakeException;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public class AnalyseHttpAPI {
    public static final int CHANNLE_TYPE_COMMONS = 2;
    public static final int CHANNLE_TYPE_STANDER = 1;
    protected int BUFFER_SIZE = 8192;
    protected String CHARENCODE = CharEncoding.UTF_8;
    protected int HTTPS_PORT = 443;
    protected int HTTP_PORT = 80;
    protected int TIMEOUT = 30000;
    protected boolean USEGZIP = true;
    private final DefaultHttpClient client = createHttpClient();
    private HttpRequestRetryHandler requestRetryHandler = new HttpRequestRetryHandler() {
        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (executionCount >= 3) {
                return false;
            }
            if (exception instanceof NoHttpResponseException) {
                return true;
            }
            if ((exception instanceof SSLHandshakeException) || (((HttpRequest) context.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest)) {
                return false;
            }
            return true;
        }
    };
    private ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
        public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
            String charset;
            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }
            Header contentEncoding = entity.getContentEncoding();
            if (EntityUtils.getContentCharSet(entity) == null) {
                charset = AnalyseHttpAPI.this.CHARENCODE;
            } else {
                charset = EntityUtils.getContentCharSet(entity);
            }
            return new String(EntityUtils.toByteArray(entity), charset);
        }
    };

    public AnalyseHttpAPI() {
        this.client.setHttpRequestRetryHandler(this.requestRetryHandler);
    }

    public DefaultHttpClient createHttpClient() {
        SchemeRegistry supportedSchemes = new SchemeRegistry();
        supportedSchemes.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), this.HTTP_PORT));
        supportedSchemes.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), this.HTTPS_PORT));
        HttpParams httpParams = createHttpParams();
        return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, supportedSchemes), httpParams);
    }

    private final HttpParams createHttpParams() {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(params, false);
        HttpConnectionParams.setConnectionTimeout(params, this.TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, this.TIMEOUT);
        HttpConnectionParams.setSocketBufferSize(params, this.BUFFER_SIZE);
        return params;
    }

    private String genrateGetUrl(String url, List<NameValuePair> olist) {
        if (olist == null || olist.size() <= 0) {
            return url;
        }
        return String.valueOf(url) + "?" + URLEncodedUtils.format(olist, this.CHARENCODE);
    }

    public String get(String url, List<NameValuePair> olist) throws ParseException, IOException {
        return (String) this.client.execute(new HttpGet(genrateGetUrl(url, olist)), this.responseHandler);
    }

    public InputStream getWithStream(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        return getUngzippedStream(getWithHttpEntity(url, olist));
    }

    public HttpEntity getWithHttpEntity(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        return this.client.execute(new HttpGet(genrateGetUrl(url, olist))).getEntity();
    }

    public String post(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        HttpPost request = new HttpPost(url);
        request.setEntity(entity);
        return (String) this.client.execute(request, this.responseHandler);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.app.analyse.http.AnalyseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.client.entity.UrlEncodedFormEntity]
     candidates:
      com.house365.app.analyse.http.AnalyseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.app.analyse.http.AnalyseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public String post(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        return post(url, (HttpEntity) new UrlEncodedFormEntity(olist, this.CHARENCODE));
    }

    public InputStream postWithStream(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        return getUngzippedStream(postWithHttpEntity(url, entity));
    }

    public HttpEntity postWithHttpEntity(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        HttpPost request = new HttpPost(url);
        request.setEntity(entity);
        return this.client.execute(request).getEntity();
    }

    public BasicNameValuePair generateGetParameter(String name, String value) {
        return new BasicNameValuePair(name, value);
    }

    public InputStream getUngzippedStream(HttpEntity entity) throws IOException {
        InputStream responseStream = entity.getContent();
        if (responseStream == null) {
            return responseStream;
        }
        Header header = entity.getContentEncoding();
        if (header == null) {
            return responseStream;
        }
        String contentEncoding = header.getValue();
        if (contentEncoding == null) {
            return responseStream;
        }
        if (contentEncoding.contains("gzip")) {
            responseStream = new GZIPInputStream(responseStream);
        }
        return responseStream;
    }
}
