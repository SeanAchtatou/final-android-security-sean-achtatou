package com.house365.app.analyse.ext.json;

import java.util.Iterator;

public class JSONML {
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0142, code lost:
        throw r12.syntaxError("Reserved attribute.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object parse(com.house365.app.analyse.ext.json.XMLTokener r12, boolean r13, com.house365.app.analyse.ext.json.JSONArray r14) throws com.house365.app.analyse.ext.json.JSONException {
        /*
            r11 = 91
            r10 = 45
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
        L_0x0008:
            java.lang.Object r7 = r12.nextContent()
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.LT
            if (r7 != r8) goto L_0x01c6
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.Character
            if (r8 == 0) goto L_0x00bd
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.SLASH
            if (r7 != r8) goto L_0x004e
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x003f
            com.house365.app.analyse.ext.json.JSONException r8 = new com.house365.app.analyse.ext.json.JSONException
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Expected a closing name instead of '"
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r10 = "'."
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            r8.<init>(r9)
            throw r8
        L_0x003f:
            java.lang.Object r8 = r12.nextToken()
            java.lang.Character r9 = com.house365.app.analyse.ext.json.XML.GT
            if (r8 == r9) goto L_0x0170
            java.lang.String r8 = "Misshaped close tag"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x004e:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.BANG
            if (r7 != r8) goto L_0x00ab
            char r1 = r12.next()
            if (r1 != r10) goto L_0x0067
            char r8 = r12.next()
            if (r8 != r10) goto L_0x0063
            java.lang.String r8 = "-->"
            r12.skipPast(r8)
        L_0x0063:
            r12.back()
            goto L_0x0008
        L_0x0067:
            if (r1 != r11) goto L_0x008c
            java.lang.Object r7 = r12.nextToken()
            java.lang.String r8 = "CDATA"
            boolean r8 = r7.equals(r8)
            if (r8 == 0) goto L_0x0085
            char r8 = r12.next()
            if (r8 != r11) goto L_0x0085
            if (r14 == 0) goto L_0x0008
            java.lang.String r8 = r12.nextCDATA()
            r14.put(r8)
            goto L_0x0008
        L_0x0085:
            java.lang.String r8 = "Expected 'CDATA['"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x008c:
            r3 = 1
        L_0x008d:
            java.lang.Object r7 = r12.nextMeta()
            if (r7 != 0) goto L_0x009a
            java.lang.String r8 = "Missing '>' after '<!'."
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x009a:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.LT
            if (r7 != r8) goto L_0x00a4
            int r3 = r3 + 1
        L_0x00a0:
            if (r3 > 0) goto L_0x008d
            goto L_0x0008
        L_0x00a4:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.GT
            if (r7 != r8) goto L_0x00a0
            int r3 = r3 + -1
            goto L_0x00a0
        L_0x00ab:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.QUEST
            if (r7 != r8) goto L_0x00b6
            java.lang.String r8 = "?>"
            r12.skipPast(r8)
            goto L_0x0008
        L_0x00b6:
            java.lang.String r8 = "Misshaped tag"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x00bd:
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x00db
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Bad tagName '"
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r7)
            java.lang.String r9 = "'."
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x00db:
            r6 = r7
            java.lang.String r6 = (java.lang.String) r6
            com.house365.app.analyse.ext.json.JSONArray r4 = new com.house365.app.analyse.ext.json.JSONArray
            r4.<init>()
            com.house365.app.analyse.ext.json.JSONObject r5 = new com.house365.app.analyse.ext.json.JSONObject
            r5.<init>()
            if (r13 == 0) goto L_0x0103
            r4.put(r6)
            if (r14 == 0) goto L_0x00f2
            r14.put(r4)
        L_0x00f2:
            r7 = 0
        L_0x00f3:
            if (r7 != 0) goto L_0x01d7
            java.lang.Object r7 = r12.nextToken()
            r0 = r7
        L_0x00fa:
            if (r0 != 0) goto L_0x010e
            java.lang.String r8 = "Misshaped tag"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0103:
            java.lang.String r8 = "tagName"
            r5.put(r8, r6)
            if (r14 == 0) goto L_0x00f2
            r14.put(r5)
            goto L_0x00f2
        L_0x010e:
            boolean r8 = r0 instanceof java.lang.String
            if (r8 != 0) goto L_0x0130
            if (r13 == 0) goto L_0x011d
            int r8 = r5.length()
            if (r8 <= 0) goto L_0x011d
            r4.put(r5)
        L_0x011d:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.SLASH
            if (r0 != r8) goto L_0x0173
            java.lang.Object r8 = r12.nextToken()
            java.lang.Character r9 = com.house365.app.analyse.ext.json.XML.GT
            if (r8 == r9) goto L_0x016b
            java.lang.String r8 = "Misshaped tag"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0130:
            java.lang.String r0 = (java.lang.String) r0
            if (r13 != 0) goto L_0x0143
            java.lang.String r8 = "tagName"
            if (r0 == r8) goto L_0x013c
            java.lang.String r8 = "childNode"
            if (r0 != r8) goto L_0x0143
        L_0x013c:
            java.lang.String r8 = "Reserved attribute."
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x0143:
            java.lang.Object r7 = r12.nextToken()
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.EQ
            if (r7 != r8) goto L_0x0165
            java.lang.Object r7 = r12.nextToken()
            boolean r8 = r7 instanceof java.lang.String
            if (r8 != 0) goto L_0x015a
            java.lang.String r8 = "Missing value"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x015a:
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = com.house365.app.analyse.ext.json.XML.stringToValue(r7)
            r5.accumulate(r0, r8)
            r7 = 0
            goto L_0x00f3
        L_0x0165:
            java.lang.String r8 = ""
            r5.accumulate(r0, r8)
            goto L_0x00f3
        L_0x016b:
            if (r14 != 0) goto L_0x0008
            if (r13 == 0) goto L_0x0171
            r7 = r4
        L_0x0170:
            return r7
        L_0x0171:
            r7 = r5
            goto L_0x0170
        L_0x0173:
            java.lang.Character r8 = com.house365.app.analyse.ext.json.XML.GT
            if (r0 == r8) goto L_0x017e
            java.lang.String r8 = "Misshaped tag"
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x017e:
            java.lang.Object r2 = parse(r12, r13, r4)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 == 0) goto L_0x0008
            boolean r8 = r2.equals(r6)
            if (r8 != 0) goto L_0x01b0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "Mismatched '"
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r6)
            java.lang.String r9 = "' and '"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r2)
            java.lang.String r9 = "'"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            com.house365.app.analyse.ext.json.JSONException r8 = r12.syntaxError(r8)
            throw r8
        L_0x01b0:
            r6 = 0
            if (r13 != 0) goto L_0x01be
            int r8 = r4.length()
            if (r8 <= 0) goto L_0x01be
            java.lang.String r8 = "childNodes"
            r5.put(r8, r4)
        L_0x01be:
            if (r14 != 0) goto L_0x0008
            if (r13 == 0) goto L_0x01c4
            r7 = r4
            goto L_0x0170
        L_0x01c4:
            r7 = r5
            goto L_0x0170
        L_0x01c6:
            if (r14 == 0) goto L_0x0008
            boolean r8 = r7 instanceof java.lang.String
            if (r8 == 0) goto L_0x01d2
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r7 = com.house365.app.analyse.ext.json.XML.stringToValue(r7)
        L_0x01d2:
            r14.put(r7)
            goto L_0x0008
        L_0x01d7:
            r0 = r7
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.app.analyse.ext.json.JSONML.parse(com.house365.app.analyse.ext.json.XMLTokener, boolean, com.house365.app.analyse.ext.json.JSONArray):java.lang.Object");
    }

    public static JSONArray toJSONArray(String string) throws JSONException {
        return toJSONArray(new XMLTokener(string));
    }

    public static JSONArray toJSONArray(XMLTokener x) throws JSONException {
        return (JSONArray) parse(x, true, null);
    }

    public static JSONObject toJSONObject(XMLTokener x) throws JSONException {
        return (JSONObject) parse(x, false, null);
    }

    public static JSONObject toJSONObject(String string) throws JSONException {
        return toJSONObject(new XMLTokener(string));
    }

    public static String toString(JSONArray ja) throws JSONException {
        int i;
        StringBuffer sb = new StringBuffer();
        String tagName = ja.getString(0);
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Object object = ja.opt(1);
        if (object instanceof JSONObject) {
            i = 2;
            JSONObject jo = (JSONObject) object;
            Iterator keys = jo.keys();
            while (keys.hasNext()) {
                String key = keys.next().toString();
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        } else {
            i = 1;
        }
        int length = ja.length();
        if (i >= length) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            do {
                Object object2 = ja.get(i);
                i++;
                if (object2 != null) {
                    if (object2 instanceof String) {
                        sb.append(XML.escape(object2.toString()));
                        continue;
                    } else if (object2 instanceof JSONObject) {
                        sb.append(toString((JSONObject) object2));
                        continue;
                    } else if (object2 instanceof JSONArray) {
                        sb.append(toString((JSONArray) object2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i < length);
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }

    public static String toString(JSONObject jo) throws JSONException {
        StringBuffer sb = new StringBuffer();
        String tagName = jo.optString("tagName");
        if (tagName == null) {
            return XML.escape(jo.toString());
        }
        XML.noSpace(tagName);
        String tagName2 = XML.escape(tagName);
        sb.append('<');
        sb.append(tagName2);
        Iterator keys = jo.keys();
        while (keys.hasNext()) {
            String key = keys.next().toString();
            if (!key.equals("tagName") && !key.equals("childNodes")) {
                XML.noSpace(key);
                String value = jo.optString(key);
                if (value != null) {
                    sb.append(' ');
                    sb.append(XML.escape(key));
                    sb.append('=');
                    sb.append('\"');
                    sb.append(XML.escape(value));
                    sb.append('\"');
                }
            }
        }
        JSONArray ja = jo.optJSONArray("childNodes");
        if (ja == null) {
            sb.append('/');
            sb.append('>');
        } else {
            sb.append('>');
            int length = ja.length();
            for (int i = 0; i < length; i++) {
                Object object = ja.get(i);
                if (object != null) {
                    if (object instanceof String) {
                        sb.append(XML.escape(object.toString()));
                    } else if (object instanceof JSONObject) {
                        sb.append(toString((JSONObject) object));
                    } else if (object instanceof JSONArray) {
                        sb.append(toString((JSONArray) object));
                    }
                }
            }
            sb.append('<');
            sb.append('/');
            sb.append(tagName2);
            sb.append('>');
        }
        return sb.toString();
    }
}
