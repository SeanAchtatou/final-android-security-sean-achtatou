package com.house365.app.analyse.data;

import com.house365.app.analyse.store.SharedPreferencesDTO;
import java.util.UUID;

public class AnalyseMetaData extends SharedPreferencesDTO {
    private String app;
    private String channel;
    private String city;
    private String data;
    private String deviceid;
    private long endtime;
    private int event;
    private String eventobj;
    private String ip;
    private String location;
    private String model;
    private String network;
    private long optime;
    private String os_version;
    private String page;
    private int platform;
    private String provider;
    private String screen;
    private long starttime;
    private String uuid;
    private String version;

    public AnalyseMetaData() {
    }

    public AnalyseMetaData(String app2, String version2, int platform2, String deviceid2, int event2, String eventobj2, long optime2, String page2, String provider2, String screen2, String model2, long starttime2, long endtime2, String channel2, String network2, String city2, String os_version2) {
        this.uuid = UUID.randomUUID().toString();
        this.app = app2;
        this.version = version2;
        this.platform = platform2;
        this.deviceid = deviceid2;
        this.event = event2;
        this.eventobj = eventobj2;
        this.optime = optime2;
        this.page = page2;
        this.provider = provider2;
        this.screen = screen2;
        this.model = model2;
        this.starttime = starttime2;
        this.endtime = endtime2;
        this.channel = channel2;
        this.network = network2;
        this.city = city2;
        this.os_version = os_version2;
    }

    public AnalyseMetaData(String app2, String version2, int platform2, String deviceid2, int event2, String eventobj2, long optime2, String page2, String provider2, String screen2, String model2, long starttime2, long endtime2, String channel2, String network2, String city2, String os_version2, String data2) {
        this.uuid = UUID.randomUUID().toString();
        this.app = app2;
        this.version = version2;
        this.platform = platform2;
        this.deviceid = deviceid2;
        this.event = event2;
        this.eventobj = eventobj2;
        this.optime = optime2;
        this.page = page2;
        this.provider = provider2;
        this.screen = screen2;
        this.model = model2;
        this.starttime = starttime2;
        this.endtime = endtime2;
        this.channel = channel2;
        this.network = network2;
        this.city = city2;
        this.os_version = os_version2;
        this.data = data2;
    }

    public String getApp() {
        return this.app;
    }

    public void setApp(String app2) {
        this.app = app2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public int getPlatform() {
        return this.platform;
    }

    public void setPlatform(int platform2) {
        this.platform = platform2;
    }

    public String getDeviceid() {
        return this.deviceid;
    }

    public void setDeviceid(String deviceid2) {
        this.deviceid = deviceid2;
    }

    public int getEvent() {
        return this.event;
    }

    public void setEvent(int event2) {
        this.event = event2;
    }

    public String getEventobj() {
        return this.eventobj;
    }

    public void setEventobj(String eventobj2) {
        this.eventobj = eventobj2;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip2) {
        this.ip = ip2;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public String getPage() {
        return this.page;
    }

    public void setPage(String page2) {
        this.page = page2;
    }

    public String getProvider() {
        return this.provider;
    }

    public void setProvider(String provider2) {
        this.provider = provider2;
    }

    public String getScreen() {
        return this.screen;
    }

    public void setScreen(String screen2) {
        this.screen = screen2;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model2) {
        this.model = model2;
    }

    public long getOptime() {
        return this.optime;
    }

    public void setOptime(long optime2) {
        this.optime = optime2;
    }

    public long getStarttime() {
        return this.starttime;
    }

    public void setStarttime(long starttime2) {
        this.starttime = starttime2;
    }

    public long getEndtime() {
        return this.endtime;
    }

    public void setEndtime(long endtime2) {
        this.endtime = endtime2;
    }

    public boolean isSame(Object o) {
        return this.uuid.equals(((AnalyseMetaData) o).getUuid());
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid2) {
        this.uuid = uuid2;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel2) {
        this.channel = channel2;
    }

    public String getNetwork() {
        return this.network;
    }

    public void setNetwork(String network2) {
        this.network = network2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getOs_version() {
        return this.os_version;
    }

    public void setOs_version(String os_version2) {
        this.os_version = os_version2;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data2) {
        this.data = data2;
    }
}
