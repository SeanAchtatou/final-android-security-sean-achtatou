package com.house365.app.analyse.store;

import java.io.Serializable;

public abstract class SharedPreferencesDTO<T> implements Serializable {
    private static final long serialVersionUID = -1594330378580200935L;

    public abstract boolean isSame(T t);
}
