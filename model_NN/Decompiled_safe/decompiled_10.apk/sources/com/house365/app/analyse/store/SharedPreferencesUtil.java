package com.house365.app.analyse.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.house365.app.analyse.ext.json.JSONArray;
import com.house365.app.analyse.ext.json.JSONException;
import com.house365.app.analyse.ext.json.JSONObject;
import com.house365.app.analyse.reflect.ReflectException;
import com.house365.app.analyse.reflect.ReflectUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SharedPreferencesUtil {
    private final SharedPreferences mPrefs;

    public SharedPreferencesUtil(Context context) {
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public SharedPreferences getSharedPreferences() {
        return this.mPrefs;
    }

    public void clean(String key) {
        this.mPrefs.edit().remove(key).commit();
    }

    public <T> void addListItem(String key, T t) {
        JSONArray array = getList(key);
        array.put(new JSONObject((Object) t));
        this.mPrefs.edit().putString(key, array.toString()).commit();
    }

    public JSONArray getList(String key) {
        String oldInfo = this.mPrefs.getString(key, null);
        if (oldInfo == null) {
            return new JSONArray();
        }
        try {
            return new JSONArray(oldInfo);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public <T extends SharedPreferencesDTO> List<T> getListWithCast(T t, String key) throws ReflectException, JSONException {
        JSONArray jsonArray = getList(key);
        List<T> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add((SharedPreferencesDTO) ReflectUtil.copy(t.getClass(), jsonArray.getJSONObject(i)));
        }
        return list;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T extends com.house365.app.analyse.store.SharedPreferencesDTO> void removeListItem(java.lang.String r8, T r9) {
        /*
            r7 = this;
            com.house365.app.analyse.ext.json.JSONArray r3 = r7.getList(r8)
            r1 = -1
            r2 = 0
        L_0x0006:
            int r5 = r3.length()
            if (r2 < r5) goto L_0x0024
        L_0x000c:
            r5 = -1
            if (r1 == r5) goto L_0x0023
            r3.remove(r1)
            android.content.SharedPreferences r5 = r7.mPrefs
            android.content.SharedPreferences$Editor r5 = r5.edit()
            java.lang.String r6 = r3.toString()
            android.content.SharedPreferences$Editor r5 = r5.putString(r8, r6)
            r5.commit()
        L_0x0023:
            return
        L_0x0024:
            java.lang.Class r5 = r9.getClass()     // Catch:{ Exception -> 0x003a }
            com.house365.app.analyse.ext.json.JSONObject r6 = r3.getJSONObject(r2)     // Catch:{ Exception -> 0x003a }
            java.lang.Object r4 = com.house365.app.analyse.reflect.ReflectUtil.copy(r5, r6)     // Catch:{ Exception -> 0x003a }
            com.house365.app.analyse.store.SharedPreferencesDTO r4 = (com.house365.app.analyse.store.SharedPreferencesDTO) r4     // Catch:{ Exception -> 0x003a }
            boolean r5 = r9.isSame(r4)     // Catch:{ Exception -> 0x003a }
            if (r5 == 0) goto L_0x003e
            r1 = r2
            goto L_0x000c
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x003e:
            int r2 = r2 + 1
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.app.analyse.store.SharedPreferencesUtil.removeListItem(java.lang.String, com.house365.app.analyse.store.SharedPreferencesDTO):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T extends com.house365.app.analyse.store.SharedPreferencesDTO> boolean hasListItem(java.lang.String r8, T r9) {
        /*
            r7 = this;
            com.house365.app.analyse.ext.json.JSONArray r3 = r7.getList(r8)
            r1 = 0
            r2 = 0
        L_0x0006:
            int r5 = r3.length()
            if (r2 < r5) goto L_0x000d
        L_0x000c:
            return r1
        L_0x000d:
            java.lang.Class r5 = r9.getClass()     // Catch:{ Exception -> 0x0023 }
            com.house365.app.analyse.ext.json.JSONObject r6 = r3.getJSONObject(r2)     // Catch:{ Exception -> 0x0023 }
            java.lang.Object r4 = com.house365.app.analyse.reflect.ReflectUtil.copy(r5, r6)     // Catch:{ Exception -> 0x0023 }
            com.house365.app.analyse.store.SharedPreferencesDTO r4 = (com.house365.app.analyse.store.SharedPreferencesDTO) r4     // Catch:{ Exception -> 0x0023 }
            boolean r5 = r9.isSame(r4)     // Catch:{ Exception -> 0x0023 }
            if (r5 == 0) goto L_0x0027
            r1 = 1
            goto L_0x000c
        L_0x0023:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0027:
            int r2 = r2 + 1
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.app.analyse.store.SharedPreferencesUtil.hasListItem(java.lang.String, com.house365.app.analyse.store.SharedPreferencesDTO):boolean");
    }

    public void putBoolean(String key, boolean value) {
        this.mPrefs.edit().putBoolean(key, value).commit();
    }

    public void putInt(String key, int value) {
        this.mPrefs.edit().putInt(key, value).commit();
    }

    public void putString(String key, String value) {
        this.mPrefs.edit().putString(key, value).commit();
    }

    public void putFloat(String key, float value) {
        this.mPrefs.edit().putFloat(key, value).commit();
    }

    public void putLong(String key, long value) {
        this.mPrefs.edit().putLong(key, value).commit();
    }

    public void putObject(String key, Object value) {
        this.mPrefs.edit().putString(key, new JSONObject(value).toString()).commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return this.mPrefs.getBoolean(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return this.mPrefs.getInt(key, defValue);
    }

    public String getString(String key, String defValue) {
        return this.mPrefs.getString(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return this.mPrefs.getFloat(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return this.mPrefs.getLong(key, defValue);
    }

    public <T> T getObject(String key, T t) throws SharedPreferenceException {
        try {
            return ReflectUtil.copy(t.getClass(), new JSONObject(this.mPrefs.getString(key, StringUtils.EMPTY)));
        } catch (Exception e) {
            throw new SharedPreferenceException("get object occurs exception", e);
        }
    }
}
