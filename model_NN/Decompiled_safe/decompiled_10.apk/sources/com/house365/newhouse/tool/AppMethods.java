package com.house365.newhouse.tool;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.house365.core.constant.CorePreferences;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.util.DialogUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.model.VirtualCity;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.user.UserLoginActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppMethods {
    public static CustomProgressDialog dialog;

    public static ArrayList mapKeyTolistSort(Map map, boolean with_nolimit) {
        ArrayList list = null;
        if (map != null && !map.isEmpty()) {
            list = new ArrayList();
            List<Map.Entry<String, String>> infoIds = new ArrayList<>(map.entrySet());
            Collections.sort(infoIds, new Comparator<Map.Entry<String, String>>() {
                public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                    return compare((Map.Entry<String, String>) ((Map.Entry) obj), (Map.Entry<String, String>) ((Map.Entry) obj2));
                }

                public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
                    return Integer.parseInt(o2.getValue()) - Integer.parseInt(o1.getValue());
                }
            });
            for (Map.Entry entry : infoIds) {
                if (!with_nolimit) {
                    list.add(entry.getKey());
                } else if (!entry.getKey().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    list.add(entry.getKey());
                }
            }
            Collections.reverse(list);
        }
        return list;
    }

    public static ArrayList mapKeyTolistSubWay(Map map, boolean with_nolimit) {
        ArrayList list = null;
        if (map != null && !map.isEmpty()) {
            list = new ArrayList();
            for (Map.Entry entry : map.entrySet()) {
                if (!with_nolimit) {
                    list.add(entry.getKey());
                } else if (!entry.getKey().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    list.add(entry.getKey());
                }
            }
            if (list != null && list.size() > 0) {
                int i = 0;
                while (true) {
                    if (i >= list.size()) {
                        break;
                    }
                    String tmp = list.get(i).toString();
                    if (tmp.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        list.remove(i);
                        list.add(0, tmp);
                        break;
                    }
                    i++;
                }
            }
        }
        return list;
    }

    public static ArrayList mapValueTolist(Map map) {
        ArrayList list = null;
        if (map != null && !map.isEmpty()) {
            list = new ArrayList();
            for (Map.Entry entry : map.entrySet()) {
                list.add(entry.getKey());
            }
        }
        return list;
    }

    public static String getMapKey(Map map, String value) {
        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey().toString();
            }
        }
        return null;
    }

    public static void setViewToastListener(final Context context, View view, final int msg) {
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(context, msg, 0).show();
            }
        });
    }

    public static Dialog showLocationSets(final Context context) {
        Dialog alertDialog = new AlertDialog.Builder(context).setTitle((int) R.string.text_location_title).setMessage((int) R.string.text_location_prompt).setCancelable(false).setPositiveButton((int) R.string.text_location_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                ((Activity) context).startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 1);
                dialog.dismiss();
            }
        }).setNegativeButton((int) R.string.text_location_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
                ((Activity) context).finish();
            }
        }).create();
        alertDialog.show();
        return alertDialog;
    }

    public static Dialog showCityChange(final Context context, final String locationCity, final NewHouseApplication mApplication, final int tag) {
        return new AlertDialog.Builder(context).setMessage(context.getResources().getString(R.string.text_map_location_city_msg, locationCity)).setCancelable(false).setPositiveButton((int) R.string.text_location_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                NewHouseApplication.this.setCity(AppArrays.getCity(locationCity));
                context.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_CHANGE_CITY));
                if (tag != 0) {
                    Intent location = new Intent(ActionCode.INTENT_ACTION_LOCATION_FINISH);
                    location.putExtra(ActionCode.LOCATION_FINISH, 1);
                    location.putExtra(ActionCode.LOCATION_TAG, tag);
                    context.sendBroadcast(location);
                }
                dialog.dismiss();
            }
        }).setNegativeButton((int) R.string.text_location_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                if (tag != 0) {
                    Intent location = new Intent(ActionCode.INTENT_ACTION_LOCATION_FINISH);
                    location.putExtra(ActionCode.LOCATION_FINISH, 3);
                    location.putExtra(ActionCode.LOCATION_TAG, tag);
                    context.sendBroadcast(location);
                }
                dialog.dismiss();
            }
        }).create();
    }

    public static void showLoginDialog(final Context context, int msg, final String type) {
        DialogUtil.showConfrimDialog(context, 0, msg, (int) R.string.text_apply_submit, (int) R.string.text_apply_cancel, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                Intent intent = new Intent(context, UserLoginActivity.class);
                intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 2);
                intent.putExtra(UserLoginActivity.INTENT_FROM_PUBLISH, type);
                context.startActivity(intent);
            }

            public void onNegative(DialogInterface dialog) {
            }
        });
    }

    public static void exitPublish(PublishHouse house, HashMap<String, String> publishVal, final Context context) {
        if (house != null || publishVal.isEmpty()) {
            ((Activity) context).finish();
            return;
        }
        DialogUtil.showConfrimDialog(context, (int) R.string.text_exit_publish_dialog_title, (int) R.string.text_exit_publish_dialog_msg, (int) R.string.text_apply_submit, (int) R.string.text_apply_cancel, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                ((Activity) context).finish();
            }

            public void onNegative(DialogInterface dialog) {
            }
        });
    }

    public static boolean isVirtual(String cityId) {
        VirtualCity[] localCitys = AppArrays.getCityList();
        if (localCitys == null || localCitys.length <= 0) {
            return false;
        }
        int i = 0;
        while (i < localCitys.length) {
            VirtualCity city_item = localCitys[i];
            if (city_item == null || !city_item.getCity_py().equals(cityId)) {
                i++;
            } else if (city_item.getCity_api() == 1) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean isJustNewHouse(String cityPy) {
        boolean justNewHouse = false;
        String cityId = AppArrays.getCityKey(cityPy);
        CorePreferences.ERROR("person:\t" + cityId);
        String[] localCitys = AppArrays.localCity;
        int i = 0;
        while (true) {
            if (i >= localCitys.length) {
                break;
            } else if (localCitys[i].equals(cityId)) {
                justNewHouse = false;
                break;
            } else {
                justNewHouse = true;
                i++;
            }
        }
        CorePreferences.ERROR("person:\t" + justNewHouse);
        return justNewHouse;
    }

    public static boolean isInCitys(String cityName) {
        boolean isIn = false;
        VirtualCity[] citys = AppArrays.getCityList();
        if (citys == null || citys.length <= 0) {
            return false;
        }
        for (VirtualCity city : citys) {
            if (city != null && !TextUtils.isEmpty(city.getCity_name()) && city.getCity_name().equals(cityName)) {
                return true;
            }
            isIn = false;
        }
        return isIn;
    }

    public static List<String> toList(String[] attr) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < attr.length; i++) {
            list.add(i, attr[i]);
        }
        return list;
    }
}
