package com.house365.newhouse.tool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.ui.UrlGetActivity;
import com.house365.newhouse.ui.award.AwardActivity;
import com.house365.newhouse.ui.block.BlockDetailActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.news.NewsDetailActivity;
import com.house365.newhouse.ui.privilege.BargainlHouseActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.CubeHouseRoomActivity;
import com.house365.newhouse.ui.privilege.EventListActivity;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;

public class IntentRedirect {
    public static void adLink(int ad_type, Ad ad, Context context) {
        if (ad_type == 2 || ad_type == 1 || ad_type == 5) {
            if ("house".equals(ad.getA_abouttype())) {
                Intent intent = new Intent(context, NewHouseDetailActivity.class);
                intent.putExtra("h_id", ad.getA_aboutid());
                context.startActivity(intent);
            } else if (App.Categroy.Event.COUPON.equals(ad.getA_abouttype())) {
                Intent intent2 = new Intent(context, CouponDetailsActivity.class);
                intent2.putExtra("id", ad.getA_aboutid());
                intent2.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.COUPON);
                context.startActivity(intent2);
            } else if ("event".equals(ad.getA_abouttype()) || App.Categroy.Event.SELL_EVENT.equals(ad.getA_abouttype())) {
                new Intent(context, EventListActivity.class);
                Intent intent3 = new Intent(context, EventListActivity.class);
                intent3.putExtra("e_id", ad.getA_aboutid());
                intent3.putExtra(CouponDetailsActivity.INTENT_TYPE, ad.getA_abouttype());
                context.startActivity(intent3);
            } else if (App.Categroy.Event.TJF.equals(ad.getA_abouttype())) {
                Intent intent4 = new Intent(context, BargainlHouseActivity.class);
                intent4.putExtra("e_id", ad.getA_aboutid());
                context.startActivity(intent4);
            } else if (App.Categroy.Event.TLF.equals(ad.getA_abouttype())) {
                Intent intent5 = new Intent(context, CubeHouseRoomActivity.class);
                intent5.putExtra("e_id", ad.getA_aboutid());
                context.startActivity(intent5);
            } else if (App.Categroy.News.NEWS.equals(ad.getA_abouttype())) {
                Intent intent6 = new Intent(context, NewsDetailActivity.class);
                intent6.putExtra("id", ad.getA_aboutid());
                context.startActivity(intent6);
            } else if (App.Categroy.Link.HREF.equals(ad.getA_abouttype())) {
                if (!TextUtils.isEmpty(ad.getA_href())) {
                    Intent intent7 = new Intent(context, UrlGetActivity.class);
                    intent7.putExtra(UrlGetActivity.INTENT_URL, ad.getA_href());
                    context.startActivity(intent7);
                }
            } else if (App.Categroy.Event.LOTTERY.equals(ad.getA_abouttype())) {
                award(context, ad);
            }
        } else if (ad_type == 3) {
            if ("house".equals(ad.getA_abouttype())) {
                Intent intent8 = new Intent(context, SecondSellDetailActivity.class);
                intent8.putExtra("id", ad.getA_aboutid());
                context.startActivity(intent8);
            } else if ("block".equals(ad.getA_abouttype())) {
                Intent intent9 = new Intent(context, BlockDetailActivity.class);
                intent9.putExtra("id", ad.getA_aboutid());
                intent9.putExtra(BlockDetailActivity.INTENT_BLOCKID, ad.getA_aboutid());
                intent9.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 1);
                context.startActivity(intent9);
            } else if (App.Categroy.News.NEWS.equals(ad.getA_abouttype())) {
                Intent intent10 = new Intent(context, NewsDetailActivity.class);
                intent10.putExtra("id", ad.getA_aboutid());
                context.startActivity(intent10);
            } else if (App.Categroy.Link.HREF.equals(ad.getA_abouttype())) {
                if (!TextUtils.isEmpty(ad.getA_href())) {
                    Intent intent11 = new Intent(context, UrlGetActivity.class);
                    intent11.putExtra(UrlGetActivity.INTENT_URL, ad.getA_href());
                    context.startActivity(intent11);
                }
            } else if (App.Categroy.Event.LOTTERY.equals(ad.getA_abouttype())) {
                award(context, ad);
            }
        } else if (ad_type != 4) {
        } else {
            if ("house".equals(ad.getA_abouttype())) {
                Intent intent12 = new Intent(context, SecondRentDetailActivity.class);
                intent12.putExtra("id", ad.getA_aboutid());
                context.startActivity(intent12);
            } else if ("block".equals(ad.getA_abouttype())) {
                Intent intent13 = new Intent(context, BlockDetailActivity.class);
                intent13.putExtra("id", ad.getA_aboutid());
                intent13.putExtra(BlockDetailActivity.INTENT_BLOCKID, ad.getA_aboutid());
                intent13.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 2);
                context.startActivity(intent13);
            } else if (App.Categroy.News.NEWS.equals(ad.getA_abouttype())) {
                Intent intent14 = new Intent(context, NewsDetailActivity.class);
                intent14.putExtra("id", ad.getA_aboutid());
                context.startActivity(intent14);
            } else if (App.Categroy.Link.HREF.equals(ad.getA_abouttype())) {
                if (!TextUtils.isEmpty(ad.getA_href())) {
                    Intent intent15 = new Intent(context, UrlGetActivity.class);
                    intent15.putExtra(UrlGetActivity.INTENT_URL, UrlGetActivity.generateUrlWithSystemParam(context, ad.getA_href()));
                    context.startActivity(intent15);
                }
            } else if (App.Categroy.Event.LOTTERY.equals(ad.getA_abouttype())) {
                award(context, ad);
            }
        }
    }

    public static void infoLink(String id, String type, Context context) {
        if ("house".equals(type)) {
            Intent intent = new Intent(context, NewHouseDetailActivity.class);
            intent.putExtra("h_id", id);
            context.startActivity(intent);
        } else if (App.Categroy.Event.TLF.equals(type)) {
            Intent intent2 = new Intent(context, CubeHouseRoomActivity.class);
            intent2.putExtra("e_id", id);
            context.startActivity(intent2);
        } else if (App.Categroy.Event.TJF.equals(type)) {
            Intent intent3 = new Intent(context, BargainlHouseActivity.class);
            intent3.putExtra("e_id", id);
            context.startActivity(intent3);
        } else if (App.Categroy.Event.COUPON.equals(type)) {
            Intent intent4 = new Intent(context, CouponDetailsActivity.class);
            intent4.putExtra("id", id);
            intent4.putExtra(CouponDetailsActivity.INTENT_TYPE, type);
            context.startActivity(intent4);
        } else if ("event".equals(type) || App.Categroy.Event.SELL_EVENT.equals(type)) {
            Intent intent5 = new Intent(context, EventListActivity.class);
            intent5.putExtra("e_id", id);
            intent5.putExtra(CouponDetailsActivity.INTENT_TYPE, type);
            context.startActivity(intent5);
        } else if (App.SELL.equals(type)) {
            Intent intent6 = new Intent(context, SecondSellDetailActivity.class);
            intent6.putExtra("id", id);
            context.startActivity(intent6);
        } else if (App.RENT.equals(type)) {
            Intent intent7 = new Intent(context, SecondRentDetailActivity.class);
            intent7.putExtra("id", id);
            context.startActivity(intent7);
        } else if ("block".equals(type)) {
            Intent intent8 = new Intent(context, BlockDetailActivity.class);
            intent8.putExtra("id", id);
            intent8.putExtra(BlockDetailActivity.INTENT_BLOCKID, id);
            intent8.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 2);
            context.startActivity(intent8);
        }
    }

    private static void award(Context context, Ad ad) {
        if (TextUtils.isEmpty(((NewHouseApplication) ((Activity) context).getApplication()).getMobile())) {
            Intent login = new Intent(context, UserLoginActivity.class);
            login.putExtra("id", ad.getA_aboutid());
            login.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 2);
            login.putExtra(UserLoginActivity.INTENT_FROM_PUBLISH, ActionCode.AWARD);
            context.startActivity(login);
            return;
        }
        Intent intent = new Intent(context, AwardActivity.class);
        intent.putExtra("id", ad.getA_aboutid());
        intent.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.LOTTERY);
        context.startActivity(intent);
    }
}
