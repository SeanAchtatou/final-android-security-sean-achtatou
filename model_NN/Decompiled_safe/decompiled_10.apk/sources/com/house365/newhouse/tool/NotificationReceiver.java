package com.house365.newhouse.tool;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.resource.ResourceUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.ui.SplashActivity;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class NotificationReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (((NewHouseApplication) context.getApplicationContext()).isEnablePushNotification()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd");
            Date showDate = new Date();
            int id = ResourceUtil.getResourceId(context, "notify_" + simpleDateFormat.format(showDate), ResourceUtil.ResourceType.string);
            if (id > 0) {
                String message = context.getResources().getString(id);
                String title = context.getResources().getString(R.string.app_name);
                Notification notification = new Notification();
                notification.icon = R.drawable.ic_launcher;
                notification.defaults = 7;
                notification.flags |= 16;
                notification.when = System.currentTimeMillis();
                notification.tickerText = message;
                Intent intent2 = new Intent(context, SplashActivity.class);
                intent.setFlags(268435456);
                intent.setFlags(8388608);
                intent.setFlags(1073741824);
                intent.setFlags(536870912);
                intent.setFlags(67108864);
                notification.setLatestEventInfo(context, title, message, PendingIntent.getActivity(context, 0, intent2, 134217728));
                ((NotificationManager) context.getSystemService("notification")).notify(new Random(System.currentTimeMillis()).nextInt(), notification);
                Log.e("当前通知时间：", simpleDateFormat.format(showDate));
            }
            Date date = new Date();
            String nextNotify = null;
            String[] notify_dates = FirstStartBroadcastRecevier.notify_dates;
            int i = 0;
            while (true) {
                if (i >= notify_dates.length) {
                    break;
                } else if (!TimeUtil.parseDate(notify_dates[i], "yyyy_MM_dd HH:mm:ss").after(date)) {
                    i++;
                } else if (i < notify_dates.length - 1) {
                    nextNotify = notify_dates[i];
                }
            }
            if (!TextUtils.isEmpty(nextNotify)) {
                Date nextDate = TimeUtil.parseDate(nextNotify, "yyyy_MM_dd HH:mm:ss");
                Calendar cal = Calendar.getInstance();
                cal.setTime(nextDate);
                Log.e("下次通知时间：", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime()));
                ((AlarmManager) context.getSystemService("alarm")).set(0, cal.getTimeInMillis(), PendingIntent.getBroadcast(context, 0, new Intent(context, NotificationReceiver.class), 0));
            }
        }
    }
}
