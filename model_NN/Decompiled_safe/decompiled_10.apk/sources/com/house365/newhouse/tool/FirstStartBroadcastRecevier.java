package com.house365.newhouse.tool;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.house365.core.util.TimeUtil;
import com.house365.newhouse.application.NewHouseApplication;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FirstStartBroadcastRecevier extends BroadcastReceiver {
    public static final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";
    public static final String START_ACTION = "com.house365.newhouse.startup";
    public static final String[] notify_dates = {"2013_09_19 10:00:00", "2013_10_01 10:00:00", "2013_10_15 10:00:00"};
    private SharedPreferences sharedPref;

    public void onReceive(Context context, Intent intent) {
        this.sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        NewHouseApplication mApplication = (NewHouseApplication) context.getApplicationContext();
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            if (mApplication.isEnablePushNotification()) {
                setNotify(context);
            }
        } else if (intent.getAction().equals("com.house365.newhouse.startup") && mApplication.isEnablePushNotification() && !this.sharedPref.getBoolean(NewHouseApplication.PREFERENCE_MESSAGE_NOTIFY_SETTED, false)) {
            setNotify(context);
        }
    }

    private void setNotify(Context context) {
        this.sharedPref.edit().putBoolean(NewHouseApplication.PREFERENCE_MESSAGE_NOTIFY_SETTED, true).commit();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        Intent toIntent = new Intent(context, NotificationReceiver.class);
        Date date = new Date();
        String nextNotify = null;
        String[] notify_dates2 = notify_dates;
        int i = 0;
        while (true) {
            if (i >= notify_dates2.length) {
                break;
            } else if (!TimeUtil.parseDate(notify_dates2[i], "yyyy_MM_dd HH:mm:ss").after(date)) {
                i++;
            } else if (i < notify_dates2.length - 1) {
                nextNotify = notify_dates2[i];
            }
        }
        Calendar cal = Calendar.getInstance();
        if (!TextUtils.isEmpty(nextNotify)) {
            cal.setTime(TimeUtil.parseDate(nextNotify, "yyyy_MM_dd HH:mm:ss"));
            Log.e("下次通知时间：", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cal.getTime()));
            alarmManager.set(0, cal.getTimeInMillis(), PendingIntent.getBroadcast(context, 0, toIntent, 0));
        }
    }
}
