package com.house365.newhouse.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.house365.newhouse.R;
import java.util.List;

public class DefineArrayAdapter<T> extends ArrayAdapter<T> {
    private String block_nodata;
    private String chosedGrade;
    private Context context;
    private String expand;
    private int gratiy;
    private String house_nodata;
    private LayoutInflater inflate;
    private boolean room;
    private boolean single;

    public DefineArrayAdapter(Context context2, int textViewResourceId, List<T> objects) {
        super(context2, textViewResourceId, objects);
        this.context = context2;
        this.house_nodata = context2.getResources().getString(R.string.text_house_nodata);
        this.block_nodata = context2.getResources().getString(R.string.text_block_keysearch_nodata);
        this.inflate = LayoutInflater.from(context2);
    }

    public DefineArrayAdapter(Context context2, int textViewResourceId, T[] objects) {
        super(context2, textViewResourceId, objects);
        this.context = context2;
        this.inflate = LayoutInflater.from(context2);
    }

    public void setGratiy(int gratiy2) {
        this.gratiy = gratiy2;
    }

    public void setChosedGrade(String chosedGrade2) {
        this.chosedGrade = chosedGrade2;
    }

    public void setExpand(String expand2) {
        this.expand = expand2;
    }

    public boolean isSingle() {
        return this.single;
    }

    public void setSingle(boolean single2) {
        this.single = single2;
    }

    public boolean isRoom() {
        return this.room;
    }

    public void setRoom(boolean room2) {
        this.room = room2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflate.inflate((int) R.layout.keysearch_item, parent, false);
            holder = new ViewHolder(null);
            holder.h_name = (TextView) convertView.findViewById(R.id.h_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        T item = getItem(position);
        if (item instanceof CharSequence) {
            holder.h_name.setText((CharSequence) item);
        } else {
            holder.h_name.setText(item.toString());
        }
        String house_name = holder.h_name.getText().toString().trim();
        if (this.room) {
            holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.font16_white);
            holder.h_name.setGravity(17);
        } else if (this.gratiy != 0) {
            if (this.gratiy == 17) {
                holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.normal_text16);
                if (this.single) {
                    holder.h_name.setGravity(3);
                    holder.h_name.setPadding(40, 0, 0, 0);
                } else {
                    holder.h_name.setGravity(17);
                }
                if (TextUtils.isEmpty(this.chosedGrade) || !this.chosedGrade.equals(house_name)) {
                    convertView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_list_condition_grade));
                } else {
                    convertView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_grade_selected));
                }
            } else if (this.gratiy == 3) {
                holder.h_name.setGravity(3);
                holder.h_name.setPadding(40, 0, 0, 0);
                if (TextUtils.isEmpty(this.expand) || !this.expand.equals(house_name)) {
                    holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.font16_condition);
                    convertView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_list_condition_expand));
                } else {
                    holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.font16_white);
                    convertView.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_expand_selected));
                }
            }
        } else if (house_name.equals(this.house_nodata) || house_name.equals(this.block_nodata)) {
            holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.font16_orange);
            holder.h_name.setGravity(17);
        } else {
            holder.h_name.setTextAppearance(this.inflate.getContext(), R.style.normal_text16);
            holder.h_name.setGravity(3);
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView h_name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
