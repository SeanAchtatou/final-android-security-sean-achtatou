package com.house365.newhouse.task;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.House;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseSearchTask extends BaseListAsyncTask<House> {
    public static final String TASK_HOUSE_NORMAL = "house";
    public static final String TASK_HOUSE_YH = "yh";
    private BaseListAdapter adapter;
    private String blockvalue;
    private String channelvalue;
    private String districtvalue;
    private String keywords;
    private PullToRefreshListView listView;
    private Location mLocation;
    private View nodata_layout;
    private OnFinish onFinish;
    private String ordervalue = StringUtils.EMPTY;
    private String pricevalue;
    private int radiusvalue;
    private RefreshInfo refreshInfo;
    private String tag_id;
    private String type;

    public interface OnFinish {
        void onFinsh(List<House> list);
    }

    public NewHouseSearchTask(Context context, String type2, PullToRefreshListView listView2, RefreshInfo listRefresh, BaseListAdapter adapter2, View nodata_layout2, Location mLocation2, int radiusvalue2, String districtvalue2, String channelvalue2, String pricevalue2, String blockvalue2, String ordervalue2, String keywords2, String tag_id2) {
        super(context, listView2, listRefresh, adapter2);
        this.type = type2;
        this.mLocation = mLocation2;
        this.radiusvalue = radiusvalue2;
        this.districtvalue = districtvalue2;
        this.keywords = keywords2;
        this.channelvalue = channelvalue2;
        this.pricevalue = pricevalue2;
        this.ordervalue = ordervalue2;
        this.blockvalue = blockvalue2;
        this.tag_id = tag_id2;
        this.listView = listView2;
        this.refreshInfo = listRefresh;
        this.nodata_layout = nodata_layout2;
        this.adapter = adapter2;
    }

    public NewHouseSearchTask(Context context, String type2, PullToRefreshListView listView2, RefreshInfo listRefresh, BaseListAdapter adapter2, View nodata_layout2) {
        super(context, listView2, listRefresh, adapter2);
        this.nodata_layout = nodata_layout2;
        this.type = type2;
        this.listView = listView2;
        this.refreshInfo = listRefresh;
        this.nodata_layout = nodata_layout2;
        this.adapter = adapter2;
    }

    public OnFinish getOnFinish() {
        return this.onFinish;
    }

    public void setOnFinish(OnFinish onFinish2) {
        this.onFinish = onFinish2;
    }

    public List<House> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseListBySearch(this.type, this.mLocation, this.radiusvalue, this.districtvalue, this.channelvalue, this.pricevalue, this.blockvalue, this.ordervalue, this.keywords, this.tag_id, this.refreshInfo.page);
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<House> v) {
        if (v == null || v.size() <= 0) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            this.nodata_layout.setVisibility(0);
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_house_nodata);
        } else {
            this.nodata_layout.setVisibility(8);
        }
        if (this.onFinish != null) {
            this.onFinish.onFinsh(v);
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.nodata_layout != null) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            this.nodata_layout.setVisibility(0);
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }
}
