package com.house365.newhouse.task;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.ui.CustomProgressDialog;

public class GetEventInfoTask extends CommonAsyncTask<Event> {
    private String cid;
    private String ctype;
    private CustomProgressDialog dialog;
    private OnFinish onFinish;

    public interface OnFinish {
        void onFinsh(Event event);
    }

    public GetEventInfoTask(Context context, String cid2, String ctype2) {
        super(context);
        this.cid = cid2;
        this.ctype = ctype2;
        this.loadingresid = R.string.loading;
        this.dialog = new CustomProgressDialog(context, R.style.dialog);
        initLoadDialog(this.loadingresid);
    }

    private void initLoadDialog(int resid) {
        if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
            this.loadingresid = 0;
        }
        if (resid != 0) {
            this.dialog.setResId(resid);
            setLoadingDialog(this.dialog);
        }
    }

    public OnFinish getOnFinish() {
        return this.onFinish;
    }

    public void setOnFinish(OnFinish onFinish2) {
        this.onFinish = onFinish2;
    }

    public void onAfterDoInBackgroup(Event v) {
        if (v == null) {
            ((BaseCommonActivity) this.context).showToast((int) R.string.msg_load_error);
            ((Activity) this.context).finish();
        } else if (this.onFinish != null) {
            this.onFinish.onFinsh(v);
        }
    }

    public Event onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getEventDetail(this.cid, this.ctype);
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
        ((Activity) this.context).finish();
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        super.onHttpRequestError();
        ((Activity) this.context).finish();
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        super.onParseError();
        ((Activity) this.context).finish();
    }
}
