package com.house365.newhouse.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.view.View;
import android.widget.Toast;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.lbs.MyLocation;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.secondsell.BlockChooseActivity;

public class LocationTask extends CommonAsyncTask<Location> {
    public static final int STATUS_CANCLE = 3;
    public static final int STATUS_ERROR = -1;
    public static final int STATUS_FAILURE = 2;
    public static final int STATUS_OUTCITYLIST = 4;
    public static final int STATUS_SUCCESS = 1;
    public static boolean isdoing;
    private Context context;
    private boolean gps_must;
    private boolean gps_open;
    private String locationCity;
    private LocationListener locationListener;
    private View locationing;
    private MyLocation myLocation;
    private boolean sameCity;
    private int stauts = 1;
    private int tag;

    public interface LocationListener {
        void onFinish(Location location, boolean z);
    }

    public LocationTask(Context context2) {
        super(context2);
        this.context = context2;
        this.myLocation = new MyLocation(context2);
    }

    public LocationTask(Context context2, View locationing2) {
        super(context2);
        this.context = context2;
        this.locationing = locationing2;
        this.myLocation = new MyLocation(context2);
    }

    public LocationTask(Context context2, int resID) {
        super(context2);
        this.context = context2;
        this.loadingresid = resID;
        this.myLocation = new MyLocation(context2);
    }

    public LocationListener getLocationListener() {
        return this.locationListener;
    }

    public void setLocationListener(LocationListener locationListener2) {
        this.locationListener = locationListener2;
    }

    public int getTag() {
        return this.tag;
    }

    public void setTag(int tag2) {
        this.tag = tag2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        isdoing = true;
        if (this.locationing != null) {
            this.locationing.setVisibility(0);
        }
        if (!DeviceUtil.isOpenLoaction(this.context)) {
            AppMethods.showLocationSets(this.context);
            this.gps_open = false;
            return;
        }
        this.gps_open = true;
    }

    public Location onDoInBackgroup() {
        try {
            if (!this.gps_open) {
                return null;
            }
            Location location = this.myLocation.getLocationByBaiduLocApi(true, 10000, true);
            if (location != null) {
                CommonResultInfo resultCoord = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getCoordConvertCity(location.getLatitude(), location.getLongitude());
                if (resultCoord.getResult() == 1) {
                    this.locationCity = resultCoord.getData();
                    this.stauts = 1;
                } else {
                    this.stauts = -1;
                }
            }
            this.mApplication.setLocation(location);
            return location;
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            this.stauts = -1;
            return null;
        }
    }

    public void onAfterDoInBackgroup(Location result) {
        if (this.locationing != null) {
            this.locationing.setVisibility(8);
        }
        this.gps_must = true;
        if (result == null || this.stauts != 1) {
            if (!this.gps_open) {
                Toast.makeText(this.context, "请打开位置服务", 0).show();
            } else {
                Toast.makeText(this.context, (int) R.string.text_location_error, 0).show();
                if (this.tag != 0) {
                    Intent location = new Intent(ActionCode.INTENT_ACTION_LOCATION_FINISH);
                    location.putExtra(ActionCode.LOCATION_FINISH, 2);
                    location.putExtra(ActionCode.LOCATION_TAG, this.tag);
                    this.context.sendBroadcast(location);
                }
            }
        } else if (!AppMethods.isInCitys(this.locationCity)) {
            Intent location2 = new Intent(ActionCode.INTENT_ACTION_LOCATION_FINISH);
            if (this.tag != -1) {
                ((Activity) this.context).finish();
                location2.putExtra(ActionCode.LOCATION_FINISH, 3);
                location2.putExtra(ActionCode.LOCATION_TAG, this.tag);
            } else {
                location2.putExtra(ActionCode.LOCATION_FINISH, 4);
                location2.putExtra(ActionCode.LOCATION_TAG, this.tag);
            }
            this.context.sendBroadcast(location2);
            Toast.makeText(this.context, "附近功能不支持哦！！", 0).show();
        } else if (!this.locationCity.equals(this.mApplication.getCityName())) {
            BlockChooseActivity.lastChangeCityDialog = AppMethods.showCityChange(this.context, this.locationCity, (NewHouseApplication) this.mApplication, this.tag);
            BlockChooseActivity.lastChangeCityDialog.show();
        } else {
            this.sameCity = true;
            if (this.tag != 0) {
                Intent location3 = new Intent(ActionCode.INTENT_ACTION_LOCATION_FINISH);
                location3.putExtra(ActionCode.LOCATION_FINISH, 1);
                location3.putExtra(ActionCode.LOCATION_TAG, this.tag);
                this.context.sendBroadcast(location3);
            }
        }
        this.gps_must = false;
        if (this.locationListener != null) {
            this.locationListener.onFinish(result, this.sameCity);
        }
        isdoing = false;
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.locationing != null) {
            this.locationing.setVisibility(8);
        }
        Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
        if (this.locationListener != null) {
            this.locationListener.onFinish(null, this.sameCity);
        }
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        super.onHttpRequestError();
        if (this.locationing != null) {
            this.locationing.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        super.onParseError();
        if (this.locationing != null) {
            this.locationing.setVisibility(8);
        }
    }
}
