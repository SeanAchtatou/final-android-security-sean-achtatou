package com.house365.newhouse.task;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.AwardInfo;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.ui.award.TaofangImageLoader;
import java.util.List;

public class AwardEventTask extends GetEventInfoTask {
    private Context context;
    private Event event;
    private TaofangImageLoader imageLoader;
    private List<AwardInfo> list;

    public AwardEventTask(Context context2, String cid, String ctype, Event event2) {
        super(context2, cid, ctype);
        this.loadingresid = R.string.award_info_load;
        this.context = context2;
        this.imageLoader = new TaofangImageLoader(context2);
        this.event = event2;
    }

    public Event onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        ((NewHouseApplication) this.context.getApplicationContext()).getAwardPics().clear();
        if (this.event == null) {
            this.event = super.onDoInBackgroup();
            this.list = this.event.getE_awards();
        } else {
            this.list = this.event.getE_lottery().getE_awards();
        }
        for (AwardInfo award : this.list) {
            try {
                Bitmap bitmap = this.imageLoader.loadBitmap(award.getA_pic(), (int) this.context.getResources().getDimension(R.dimen.award_image_width), (int) this.context.getResources().getDimension(R.dimen.award_image_height), 3);
                if (!(bitmap == null || bitmap.getWidth() == 0)) {
                    bitmap.getHeight();
                }
                ((NewHouseApplication) this.context.getApplicationContext()).addAwardPic(bitmap);
            } catch (Exception e) {
                throw new HtppApiException(this.context.getString(R.string.award_info_load_fail));
            }
        }
        return this.event;
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        ActivityUtil.showToast(this.context, (int) R.string.award_info_load_fail);
        ((Activity) this.context).finish();
    }
}
