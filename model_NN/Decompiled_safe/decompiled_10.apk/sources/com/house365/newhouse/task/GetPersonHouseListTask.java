package com.house365.newhouse.task;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.SecondHouse;
import java.util.List;

public class GetPersonHouseListTask extends BaseListAsyncTask<SecondHouse> {
    BaseListAdapter adapter;
    int app;
    GetHouseListOnFinishListener houseListOnFinishListener;
    RefreshInfo listRefresh;
    View nodata_layout;
    PullToRefreshListView pulllistView;

    public interface GetHouseListOnFinishListener {
        void OnFinish(List<SecondHouse> list);
    }

    public GetPersonHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, int app2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListOnFinishListener getHouseListOnFinishListener() {
        return this.houseListOnFinishListener;
    }

    public void setHouseListOnFinishListener(GetHouseListOnFinishListener houseListOnFinishListener2) {
        this.houseListOnFinishListener = houseListOnFinishListener2;
    }

    public View getNodata_layout() {
        return this.nodata_layout;
    }

    public void setNodata_layout(View nodata_layout2) {
        this.nodata_layout = nodata_layout2;
    }

    public List<SecondHouse> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getPersonHouseList(this.app);
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<SecondHouse> v) {
        if (this.houseListOnFinishListener != null) {
            this.houseListOnFinishListener.OnFinish(v);
        } else if (v == null || v.size() <= 0) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            if (this.nodata_layout != null) {
                this.nodata_layout.setVisibility(0);
                ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
                ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_house_nodata);
            }
        } else {
            if (!(this.listRefresh == null || this.pulllistView == null)) {
                this.adapter.clear();
                ViewUtil.onListDataComplete(this.context, v, this.listRefresh, this.adapter, this.pulllistView);
                this.adapter.notifyDataSetChanged();
            }
            if (this.nodata_layout != null) {
                this.nodata_layout.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.nodata_layout != null) {
            this.nodata_layout.setVisibility(0);
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }
}
