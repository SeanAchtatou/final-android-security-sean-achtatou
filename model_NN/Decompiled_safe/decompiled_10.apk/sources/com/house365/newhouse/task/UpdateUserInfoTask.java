package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;

public class UpdateUserInfoTask extends CommonAsyncTask<CommonResultInfo> {
    private String last_visit;

    public UpdateUserInfoTask(Context context, String last_visit2) {
        super(context);
        this.last_visit = last_visit2;
    }

    public void onAfterDoInBackgroup(CommonResultInfo v) {
    }

    public CommonResultInfo onDoInBackgroup() {
        try {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).updateUserInfo(this.last_visit);
        } catch (Exception e) {
            return null;
        }
    }
}
