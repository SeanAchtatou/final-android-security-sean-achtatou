package com.house365.newhouse.task;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;
import com.house365.app.analyse.ext.json.JSONException;
import com.house365.app.analyse.ext.json.JSONObject;
import com.house365.app.analyse.reflect.ReflectException;
import com.house365.app.analyse.reflect.ReflectUtil;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.PublishHouse;
import java.util.Map;

public class PublishHouseTask extends CommonAsyncTask<CommonResultInfo> {
    private int houseType;
    private PublishOnSuccessListener onSuccessListener;
    private Map<String, String> publishVal;
    private int type;

    public interface PublishOnSuccessListener {
        void OnSuccess(CommonResultInfo commonResultInfo, PublishHouse publishHouse);
    }

    public PublishHouseTask(Context context, Map<String, String> publishVal2, int type2) {
        super(context, R.string.text_publish_submiting);
        this.publishVal = publishVal2;
        this.type = type2;
    }

    public PublishHouseTask(Context context, Map<String, String> publishVal2, int houseType2, int type2) {
        super(context, R.string.text_publish_submiting);
        this.publishVal = publishVal2;
        this.houseType = houseType2;
        this.type = type2;
    }

    public PublishOnSuccessListener getOnSuccessListener() {
        return this.onSuccessListener;
    }

    public void setOnSuccessListener(PublishOnSuccessListener onSuccessListener2) {
        this.onSuccessListener = onSuccessListener2;
    }

    public void onAfterDoInBackgroup(CommonResultInfo v) {
        if (v == null) {
            ActivityUtil.showToast(this.context, (int) R.string.msg_load_error);
        } else if (v.getResult() == 1) {
            try {
                if (!TextUtils.isEmpty(v.getData())) {
                    PublishHouse house = (PublishHouse) ReflectUtil.copy(PublishHouse.class, new JSONObject(v.getData()));
                    if (this.onSuccessListener != null) {
                        this.onSuccessListener.OnSuccess(v, house);
                    }
                }
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            ActivityUtil.showToast(this.context, v.getMsg());
        } else {
            ActivityUtil.showToast(this.context, v.getMsg());
        }
    }

    public CommonResultInfo onDoInBackgroup() {
        try {
            if (this.type == 1) {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).publishHouse(this.publishVal);
            }
            if (this.type == 2) {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).OfferHouse(this.publishVal, this.houseType);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        super.onHttpRequestError();
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        super.onParseError();
    }
}
