package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;

public class GetPublishConfigTask extends CommonAsyncTask<HouseBaseInfo> {
    private PublishConfigOnFinish onFinishListener;
    private int publishType;

    public interface PublishConfigOnFinish {
        void onFail();

        void onSuccess(HouseBaseInfo houseBaseInfo);
    }

    public GetPublishConfigTask(Context context, int publishType2) {
        super(context, R.string.text_publish_config_loading);
        this.publishType = publishType2;
    }

    public PublishConfigOnFinish getOnFinishListener() {
        return this.onFinishListener;
    }

    public void setOnFinishListener(PublishConfigOnFinish onFinishListener2) {
        this.onFinishListener = onFinishListener2;
    }

    public void onAfterDoInBackgroup(HouseBaseInfo v) {
        if (v != null) {
            if (this.onFinishListener != null) {
                this.onFinishListener.onSuccess(v);
            }
        } else if (this.onFinishListener != null) {
            this.onFinishListener.onFail();
        }
    }

    public HouseBaseInfo onDoInBackgroup() {
        try {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSecondHousebaseinfo(this.publishType);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
