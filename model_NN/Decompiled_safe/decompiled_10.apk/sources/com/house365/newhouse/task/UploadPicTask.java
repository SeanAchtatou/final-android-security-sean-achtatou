package com.house365.newhouse.task;

import android.content.Context;
import android.widget.Toast;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import java.io.File;

public class UploadPicTask extends CommonAsyncTask<CommonResultInfo> {
    private File file;
    UploadPicListener uploadPicListener;

    public interface UploadPicListener {
        void onError();

        void onSuccess(String str);
    }

    public UploadPicTask(Context context, int resid, File file2, UploadPicListener uploadPicListener2) {
        super(context, resid);
        this.file = file2;
        this.uploadPicListener = uploadPicListener2;
    }

    public void onAfterDoInBackgroup(CommonResultInfo v) {
        if (v == null || v.getResult() != 1) {
            this.uploadPicListener.onError();
        } else {
            this.uploadPicListener.onSuccess(v.getData());
        }
    }

    public CommonResultInfo onDoInBackgroup() {
        try {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).uploadPic(this.file);
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        super.onHttpRequestError();
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        super.onParseError();
    }
}
