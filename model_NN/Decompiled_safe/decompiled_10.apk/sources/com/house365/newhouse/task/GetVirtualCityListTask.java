package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.VirtualCity;
import java.util.List;

public class GetVirtualCityListTask extends CommonAsyncTask<List<VirtualCity>> {
    private FinishListener finishListener;

    public interface FinishListener {
        void onFinish(List<VirtualCity> list);
    }

    public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
        onAfterDoInBackgroup((List<VirtualCity>) ((List) obj));
    }

    public GetVirtualCityListTask(Context context, FinishListener finishListener2) {
        super(context);
        this.finishListener = finishListener2;
    }

    public FinishListener getFinishListener() {
        return this.finishListener;
    }

    public void setFinishListener(FinishListener finishListener2) {
        this.finishListener = finishListener2;
    }

    public void onAfterDoInBackgroup(List<VirtualCity> r) {
        if (r != null && r.size() > 0 && this.finishListener != null) {
            this.finishListener.onFinish(r);
        }
    }

    public List<VirtualCity> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getVirtualCity();
    }
}
