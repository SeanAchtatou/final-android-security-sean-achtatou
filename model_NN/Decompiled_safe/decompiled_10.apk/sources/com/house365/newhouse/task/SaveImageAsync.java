package com.house365.newhouse.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.house365.newhouse.model.Photo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveImageAsync extends AsyncTask<Photo, Void, Bitmap> {
    private static final String ALBUM_PATH = "/sdcard/dcim/camera";
    private Context context;
    private String filename;
    ProgressDialog myDialog;
    private Photo photoItem;

    public SaveImageAsync(Context context2, String filename2) {
        this.context = context2;
        this.filename = filename2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.myDialog = ProgressDialog.show(this.context, "保存图片", "正在保存图片中，请稍等...", true);
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(Photo... params) {
        this.photoItem = params[0];
        Log.e("photo", String.valueOf(this.photoItem.getP_name()) + " : " + this.photoItem.getP_url());
        Bitmap bitmap = getBitmap();
        return bitmap != null ? bitmap : getItemPic(this.photoItem.getP_url());
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap result) {
        if (result != null) {
            this.myDialog.dismiss();
            Toast.makeText(this.context, "图片保存成功", 1).show();
            return;
        }
        this.myDialog.dismiss();
        Toast.makeText(this.context, "图片保存失败", 1).show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[SYNTHETIC, Splitter:B:21:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004d A[SYNTHETIC, Splitter:B:33:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x005f A[SYNTHETIC, Splitter:B:43:0x005f] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:26:0x0042=Splitter:B:26:0x0042, B:14:0x002d=Splitter:B:14:0x002d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap getItemPic(java.lang.String r8) {
        /*
            r7 = this;
            r4 = 0
            r0 = 0
            r2 = 0
            r3 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x002c, IOException -> 0x0041 }
            r5.<init>(r8)     // Catch:{ MalformedURLException -> 0x002c, IOException -> 0x0041 }
            java.net.URLConnection r0 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            r6 = 3000(0xbb8, float:4.204E-42)
            r0.setConnectTimeout(r6)     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            r7.storeInSD(r3)     // Catch:{ MalformedURLException -> 0x0073, IOException -> 0x0070, all -> 0x006d }
            if (r5 == 0) goto L_0x0076
            r4 = 0
        L_0x0023:
            if (r0 == 0) goto L_0x0026
            r0 = 0
        L_0x0026:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x0068 }
        L_0x002b:
            return r3
        L_0x002c:
            r1 = move-exception
        L_0x002d:
            r1.printStackTrace()     // Catch:{ all -> 0x0056 }
            if (r4 == 0) goto L_0x0033
            r4 = 0
        L_0x0033:
            if (r0 == 0) goto L_0x0036
            r0 = 0
        L_0x0036:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x002b
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x0041:
            r1 = move-exception
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x0056 }
            if (r4 == 0) goto L_0x0048
            r4 = 0
        L_0x0048:
            if (r0 == 0) goto L_0x004b
            r0 = 0
        L_0x004b:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x0051 }
            goto L_0x002b
        L_0x0051:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x0056:
            r6 = move-exception
        L_0x0057:
            if (r4 == 0) goto L_0x005a
            r4 = 0
        L_0x005a:
            if (r0 == 0) goto L_0x005d
            r0 = 0
        L_0x005d:
            if (r2 == 0) goto L_0x0062
            r2.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0062:
            throw r6
        L_0x0063:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0062
        L_0x0068:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002b
        L_0x006d:
            r6 = move-exception
            r4 = r5
            goto L_0x0057
        L_0x0070:
            r1 = move-exception
            r4 = r5
            goto L_0x0042
        L_0x0073:
            r1 = move-exception
            r4 = r5
            goto L_0x002d
        L_0x0076:
            r4 = r5
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.newhouse.task.SaveImageAsync.getItemPic(java.lang.String):android.graphics.Bitmap");
    }

    private void storeInSD(Bitmap bitmap) {
        Log.e("filename", ALBUM_PATH);
        File file = new File(ALBUM_PATH);
        if (!file.exists()) {
            file.mkdir();
        }
        File imageFile = new File(file, this.photoItem.getP_name());
        try {
            imageFile.createNewFile();
            Log.e("imagefile", imageFile.getPath());
            FileOutputStream fos = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private Bitmap getBitmap() {
        File imageFile = new File(this.filename, String.valueOf(this.photoItem.getP_name()) + ".jpg");
        if (!imageFile.exists()) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(new FileInputStream(imageFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
