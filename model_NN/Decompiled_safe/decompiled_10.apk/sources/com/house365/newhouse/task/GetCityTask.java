package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.VirtualCity;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class GetCityTask extends CommonAsyncTask<VirtualCity[]> {
    public static boolean isdoing;
    GetCityFinishListener getCityFinishListener;
    NewHouseApplication newApplication;

    public interface GetCityFinishListener {
        void onFinish();
    }

    public GetCityTask(Context context) {
        super(context);
    }

    public GetCityTask(Context context, int resid) {
        super(context, resid);
    }

    public void setNewApplication(NewHouseApplication newApplication2) {
        this.newApplication = newApplication2;
    }

    public GetCityFinishListener getCityFinishListener() {
        return this.getCityFinishListener;
    }

    public void setCityFinishListener(GetCityFinishListener getCityFinishListener2) {
        this.getCityFinishListener = getCityFinishListener2;
    }

    public static boolean reset() {
        AppArrays.citys = new LinkedHashMap();
        AppArrays.cityLocation = new HashMap();
        AppArrays.citySet = new LinkedHashSet();
        AppArrays.citys.clear();
        AppArrays.cityLocation.clear();
        AppArrays.citySet.clear();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        reset();
    }

    public void onAfterDoInBackgroup(VirtualCity[] result) {
        isdoing = false;
        if (result != null) {
            if (this.getCityFinishListener != null) {
                this.getCityFinishListener.onFinish();
            }
        } else if (this.getCityFinishListener != null) {
            this.getCityFinishListener.onFinish();
        }
    }

    public VirtualCity[] onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        isdoing = true;
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getCityConfig();
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        localDataSet();
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        localDataSet();
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        localDataSet();
    }

    public static void localDataSet() {
        AppArrays.fromNet = 2;
        if (AppArrays.citys == null) {
            AppArrays.initVirtualCity(AppArrays.getVirtualCityJson());
        }
    }
}
