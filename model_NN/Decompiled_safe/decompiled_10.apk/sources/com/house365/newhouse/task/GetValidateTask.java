package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;

public class GetValidateTask extends CommonAsyncTask<CommonResultInfo> {
    private String moblie;
    private ValidateOnSuccessListener onSuccessListener;
    private int publishType;

    public interface ValidateOnSuccessListener {
        void OnSuccess(CommonResultInfo commonResultInfo);
    }

    public GetValidateTask(Context context, String moblie2, int type) {
        super(context, R.string.text_publish_validate_loading);
        this.moblie = moblie2;
        this.publishType = type;
    }

    public ValidateOnSuccessListener getOnSuccessListener() {
        return this.onSuccessListener;
    }

    public void setOnSuccessListener(ValidateOnSuccessListener onSuccessListener2) {
        this.onSuccessListener = onSuccessListener2;
    }

    public void onAfterDoInBackgroup(CommonResultInfo v) {
        if (v == null) {
            ActivityUtil.showToast(this.context, (int) R.string.msg_load_error);
        } else if (v.getResult() == 1) {
            if (this.onSuccessListener != null) {
                this.onSuccessListener.OnSuccess(v);
            }
            ActivityUtil.showToast(this.context, v.getMsg());
        } else {
            ActivityUtil.showToast(this.context, v.getMsg());
        }
    }

    public CommonResultInfo onDoInBackgroup() {
        try {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getValidate(this.publishType, this.moblie);
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            return null;
        }
    }
}
