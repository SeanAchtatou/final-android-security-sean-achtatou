package com.house365.newhouse.task;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.ui.privilege.EventListActivity;
import com.house365.newhouse.ui.privilege.PrivilegeHomeActivity;
import java.util.ArrayList;
import java.util.List;

public class GetItemSaleTask extends BaseListAsyncTask<Event> {
    public static boolean stopRefresh;
    private GetItemSaleOnSuccessListener ItemSaleOnSuccessListener;
    private BaseListAdapter adapter;
    private List<Event> dataList;
    protected RefreshInfo listRefresh;
    private View nodata_layout;
    private PullToRefreshListView pullListView;
    private TextView tv_nodata;
    private String type;

    public interface GetItemSaleOnSuccessListener {
        void OnSuccess(List list, PullToRefreshListView pullToRefreshListView, String str);
    }

    public GetItemSaleTask(Context context, int loadingResId, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String type2) {
        super(context, listView, listRefresh2, adapter2);
        this.type = type2;
        this.pullListView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
    }

    public GetItemSaleTask(Context context, int loadingResId, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String type2, View nodata_layout2, TextView tv_nodata2) {
        super(context, listView, listRefresh2, adapter2);
        this.loadingresid = loadingResId;
        this.type = type2;
        this.pullListView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.nodata_layout = nodata_layout2;
        this.tv_nodata = tv_nodata2;
    }

    public View getNodata_layout() {
        return this.nodata_layout;
    }

    public void setNodata_layout(View nodata_layout2) {
        this.nodata_layout = nodata_layout2;
    }

    public TextView getTv_nodata() {
        return this.tv_nodata;
    }

    public void setTv_nodata(TextView tv_nodata2) {
        this.tv_nodata = tv_nodata2;
    }

    public GetItemSaleOnSuccessListener getItemSaleOnSuccessListener() {
        return this.ItemSaleOnSuccessListener;
    }

    public void setItemSaleOnSuccessListener(GetItemSaleOnSuccessListener itemSaleOnSuccessListener) {
        this.ItemSaleOnSuccessListener = itemSaleOnSuccessListener;
    }

    public List<Event> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        new ArrayList();
        List<Event> dataList2 = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getEventList(this.type, this.listRefresh.page);
        if (dataList2 == null || dataList2.size() <= 0) {
            return getLocalData();
        }
        ((NewHouseApplication) this.mApplication).saveEvent(dataList2, this.type);
        return dataList2;
    }

    private List<Event> getLocalData() {
        try {
            return ((NewHouseApplication) this.mApplication).getEvent(this.type);
        } catch (ReflectException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<Event> v) {
        if (TextUtils.isEmpty(this.type)) {
            return;
        }
        if (this.type.equals("event") || this.type.equals(App.Categroy.Event.SELL_EVENT)) {
            if (EventListActivity.ctype.equals(this.type)) {
                setData(v);
            } else {
                changeTabRefresh();
            }
        } else if (PrivilegeHomeActivity.showType.equals(this.type)) {
            setData(v);
        } else {
            changeTabRefresh();
        }
    }

    private void changeTabRefresh() {
        if (this.pullListView == null) {
            return;
        }
        if (this.listRefresh.refresh) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            this.pullListView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
            return;
        }
        this.pullListView.onRefreshComplete();
    }

    private void setData(List<Event> v) {
        if (v == null || v.size() <= 0) {
            setNoData();
            return;
        }
        this.adapter.clear();
        this.adapter.addAll(v);
        this.adapter.notifyDataSetChanged();
        if (this.ItemSaleOnSuccessListener != null && !stopRefresh) {
            this.ItemSaleOnSuccessListener.OnSuccess(v, this.pullListView, this.type);
        }
        if (v.size() == 0) {
            setNoData();
        } else {
            this.nodata_layout.setVisibility(8);
        }
    }

    private void setNoData() {
        ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
        this.nodata_layout.setVisibility(0);
        int nodata = 0;
        if (this.type == "event") {
            nodata = R.string.text_event_nodata;
        }
        if (this.type == App.Categroy.Event.SELL_EVENT) {
            nodata = R.string.text_sell_event_nodata;
        }
        if (this.type == App.Categroy.Event.COUPON) {
            nodata = R.string.text_coupon_nodata;
        }
        if (this.type == App.Categroy.Event.TJF) {
            nodata = R.string.text_bargain_nodata;
        }
        if (this.type == App.Categroy.Event.TLF) {
            nodata = R.string.text_cubehouse_nodata;
        }
        if (nodata != 0) {
            this.tv_nodata.setText(nodata);
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.nodata_layout != null) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            this.nodata_layout.setVisibility(0);
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }
}
