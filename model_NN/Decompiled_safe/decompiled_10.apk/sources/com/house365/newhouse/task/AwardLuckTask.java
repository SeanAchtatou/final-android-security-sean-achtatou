package com.house365.newhouse.task;

import android.content.Context;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.interfaces.TaskFinishListener;
import com.house365.newhouse.model.AwardInfo;

public class AwardLuckTask extends CommonAsyncTask<AwardInfo> {
    private String cid;
    private Context context;
    private String ctype;
    private TaskFinishListener<AwardInfo> listener;

    public AwardLuckTask(Context context2, String cid2, String ctype2, TaskFinishListener<AwardInfo> listener2) {
        super(context2);
        this.loadingresid = 0;
        this.listener = listener2;
        this.context = context2;
        this.cid = cid2;
        this.ctype = ctype2;
    }

    public AwardInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getTryLuck(this.cid, this.ctype);
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        ActivityUtil.showToast(this.context, (int) R.string.award_info_load_fail);
    }

    public void onAfterDoInBackgroup(AwardInfo r) {
        this.listener.onFinish(r);
    }
}
