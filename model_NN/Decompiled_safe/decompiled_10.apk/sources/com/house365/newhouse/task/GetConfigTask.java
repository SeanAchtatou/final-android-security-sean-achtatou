package com.house365.newhouse.task;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.ui.CustomProgressDialog;

public class GetConfigTask extends CommonAsyncTask<HouseBaseInfo> {
    private GetConfigOnSuccessListener configOnSuccessListener;
    CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);
    private String id;
    private int type;

    public interface GetConfigOnSuccessListener {
        void OnError(HouseBaseInfo houseBaseInfo);

        void OnSuccess(HouseBaseInfo houseBaseInfo);
    }

    public GetConfigTask(Context context, int resid, int type2) {
        super(context);
        this.type = type2;
        this.loadingresid = resid;
        this.context = context;
        initLoadDialog(resid);
    }

    public GetConfigTask(Context context, int resid) {
        super(context);
        this.loadingresid = resid;
        this.context = context;
        initLoadDialog(resid);
    }

    private void initLoadDialog(int resid) {
        if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
            this.loadingresid = 0;
        }
        if (resid != 0) {
            this.dialog.setResId(resid);
            setLoadingDialog(this.dialog);
        }
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public GetConfigOnSuccessListener getConfigOnSuccessListener() {
        return this.configOnSuccessListener;
    }

    public void setConfigOnSuccessListener(GetConfigOnSuccessListener configOnSuccessListener2) {
        this.configOnSuccessListener = configOnSuccessListener2;
    }

    public void onAfterDoInBackgroup(HouseBaseInfo info) {
        if (info != null) {
            if (info.getJsonStr().equals(this.context.getResources().getString(R.string.text_no_network))) {
                Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            } else if (this.configOnSuccessListener != null) {
                this.configOnSuccessListener.OnSuccess(info);
            }
        } else if (this.configOnSuccessListener != null) {
            this.configOnSuccessListener.OnError(info);
        }
    }

    public HouseBaseInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        try {
            if (this.type == 0) {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewHouseConfigInfo();
            }
            if (this.type == 1 || this.type == 2) {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSecondHousebaseinfo(this.type);
            }
            return null;
        } catch (JSONException e) {
            Log.e("error", "error");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
    }
}
