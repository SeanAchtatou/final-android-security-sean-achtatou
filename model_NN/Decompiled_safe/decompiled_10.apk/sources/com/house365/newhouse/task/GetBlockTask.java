package com.house365.newhouse.task;

import android.content.Context;
import android.location.Location;
import android.widget.Toast;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.lbs.MapUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Block;
import java.util.List;

public class GetBlockTask extends CommonAsyncTask<List<Block>> {
    private GetBlockTaskListener blockTaskListener;
    private Location location;
    private int radius = 2500;

    public interface GetBlockTaskListener {
        void onFinish(List<Block> list);
    }

    public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
        onAfterDoInBackgroup((List<Block>) ((List) obj));
    }

    public GetBlockTask(Context context, Location location2) {
        super(context);
        this.location = location2;
    }

    public GetBlockTaskListener getBlockTaskListener() {
        return this.blockTaskListener;
    }

    public void setBlockTaskListener(GetBlockTaskListener blockTaskListener2) {
        this.blockTaskListener = blockTaskListener2;
    }

    public void onAfterDoInBackgroup(List<Block> result) {
        if (this.blockTaskListener != null) {
            this.blockTaskListener.onFinish(result);
        }
    }

    public List<Block> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        if (this.location == null || this.radius <= 0) {
            return null;
        }
        double[][] points = MapUtil.GetTwoPointLocation(this.location.getLatitude(), this.location.getLongitude(), this.radius);
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockOfMap(null, points[0][0], points[0][1], points[1][0], points[1][1], null, null, null);
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
    }
}
