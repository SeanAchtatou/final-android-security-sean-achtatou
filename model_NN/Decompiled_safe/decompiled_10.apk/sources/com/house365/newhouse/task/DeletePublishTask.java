package com.house365.newhouse.task;

import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.ui.secondhouse.adapter.HousePubLishAdapter;
import java.util.HashMap;
import java.util.List;

public class DeletePublishTask extends CommonAsyncTask<Boolean> {
    private View btn_edit;
    private ListView list;
    private HashMap<String, List<PublishHouse>> mapData;
    private View nodata_layout;
    private int position;
    private boolean publish;
    private int publishType;
    private HousePubLishAdapter requireAdapter;
    private TextView tv_nodata;

    public DeletePublishTask(Context context, HashMap<String, List<PublishHouse>> mapData2, HousePubLishAdapter requireAdapter2, View nodata_layout2, View btn_edit2, TextView tv_nodata2, int publishType2, int position2, ListView list2, boolean publish2) {
        super(context);
        this.mapData = mapData2;
        this.requireAdapter = requireAdapter2;
        this.nodata_layout = nodata_layout2;
        this.btn_edit = btn_edit2;
        this.tv_nodata = tv_nodata2;
        this.publishType = publishType2;
        this.position = position2;
        this.list = list2;
        this.publish = publish2;
    }

    public void onAfterDoInBackgroup(Boolean v) {
        if (v.booleanValue()) {
            if (this.requireAdapter.getCount() <= 0) {
                this.list.setVisibility(8);
                this.nodata_layout.setVisibility(0);
                this.btn_edit.setVisibility(4);
                setNoDataUI();
            } else {
                this.btn_edit.setVisibility(0);
                this.requireAdapter.setType(1);
            }
            this.requireAdapter.notifyDataSetChanged();
        }
    }

    public Boolean onDoInBackgroup() {
        if (this.publishType == 4 || this.publishType == 5) {
            if (!this.publish) {
                ((NewHouseApplication) this.mApplication).removePublishRec(this.requireAdapter.getItem(this.position), 10, this.publishType);
                if (this.publishType == 4) {
                    this.mapData.get("sellHouseInfo").remove(this.position);
                }
                if (this.publishType == 5) {
                    this.mapData.get("rentHouseInfo").remove(this.position);
                }
            } else {
                ((NewHouseApplication) this.mApplication).removePublishRec(this.requireAdapter.getItem(this.position), 11, this.publishType);
                if (this.publishType == 4) {
                    this.mapData.get("sellOfferInfo").remove(this.position);
                }
                if (this.publishType == 5) {
                    this.mapData.get("rentOfferInfo").remove(this.position);
                }
            }
            this.requireAdapter.remove(this.position);
        }
        return true;
    }

    public void setNoDataUI() {
        this.tv_nodata.setText((int) R.string.text_publish_nodata);
    }
}
