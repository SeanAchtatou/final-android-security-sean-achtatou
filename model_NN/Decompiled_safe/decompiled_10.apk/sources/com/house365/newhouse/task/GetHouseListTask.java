package com.house365.newhouse.task;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.SecondHouse;
import java.util.List;

public class GetHouseListTask extends BaseListAsyncTask<SecondHouse> {
    BaseListAdapter adapter;
    String app;
    private String blockid;
    private int buildareaValue;
    private int busValue;
    private int districtValue;
    private int fitmentValue;
    private int hallValue;
    GetHouseListOnFinishListener houseListOnFinishListener;
    private int infofromValue;
    private int infotypeValue;
    private String keywords;
    RefreshInfo listRefresh;
    Location location;
    private String metroValue;
    View nodata_layout;
    int order;
    private int priceValue;
    PullToRefreshListView pulllistView;
    int radius;
    private int rentTypeValue;
    private int roomValue;
    private int streetValue;
    private int toiletValue;

    public interface GetHouseListOnFinishListener {
        void OnFinish(List<SecondHouse> list);
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, String blockid2, Location location2, int radius2, String keywords2, int order2, int infofromValue2, int infotypeValue2, int districtValue2, int streetValue2, int priceValue2, int buildareaValue2, int roomValue2, int hallValue2, int toiletValue2, String metroValue2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.blockid = blockid2;
        this.location = location2;
        this.radius = radius2;
        this.keywords = keywords2;
        this.infofromValue = infofromValue2;
        this.infotypeValue = infotypeValue2;
        this.districtValue = districtValue2;
        this.streetValue = streetValue2;
        this.priceValue = priceValue2;
        this.buildareaValue = buildareaValue2;
        this.roomValue = roomValue2;
        this.hallValue = hallValue2;
        this.toiletValue = toiletValue2;
        this.order = order2;
        this.metroValue = metroValue2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, String blockid2, Location location2, int radius2, String keywords2, int order2, int infofromValue2, int infotypeValue2, int districtValue2, int streetValue2, int priceValue2, int roomValue2, int hallValue2, int toiletValue2, int fitmentValue2, int rentTypeValue2, int busValue2, String metroValue2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.blockid = blockid2;
        this.location = location2;
        this.radius = radius2;
        this.keywords = keywords2;
        this.order = order2;
        this.infofromValue = infofromValue2;
        this.infotypeValue = infotypeValue2;
        this.districtValue = districtValue2;
        this.streetValue = streetValue2;
        this.priceValue = priceValue2;
        this.roomValue = roomValue2;
        this.hallValue = hallValue2;
        this.toiletValue = toiletValue2;
        this.fitmentValue = fitmentValue2;
        this.rentTypeValue = rentTypeValue2;
        this.order = order2;
        this.busValue = busValue2;
        this.metroValue = metroValue2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, String blockid2, int order2, int infofromValue2, int districtValue2, int streetValue2, int priceValue2, String metroValue2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.blockid = blockid2;
        this.infofromValue = infofromValue2;
        this.districtValue = districtValue2;
        this.streetValue = streetValue2;
        this.priceValue = priceValue2;
        this.order = order2;
        this.metroValue = metroValue2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, String blockid2, int roomValue2, int order2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.blockid = blockid2;
        this.order = order2;
        this.roomValue = roomValue2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, String blockid2, int infofromValue2, int priceValue2, int buildareaValue2, int roomValue2, int rentType, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.blockid = blockid2;
        this.infofromValue = infofromValue2;
        this.priceValue = priceValue2;
        this.buildareaValue = buildareaValue2;
        this.roomValue = roomValue2;
        this.rentTypeValue = rentType;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2, String app2, int infofromValue2, View nodata_layout2) {
        super(context, listView, listRefresh2, adapter2);
        this.app = app2;
        this.pulllistView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.infofromValue = infofromValue2;
        this.nodata_layout = nodata_layout2;
    }

    public GetHouseListOnFinishListener getHouseListOnFinishListener() {
        return this.houseListOnFinishListener;
    }

    public void setHouseListOnFinishListener(GetHouseListOnFinishListener houseListOnFinishListener2) {
        this.houseListOnFinishListener = houseListOnFinishListener2;
    }

    public View getNodata_layout() {
        return this.nodata_layout;
    }

    public void setNodata_layout(View nodata_layout2) {
        this.nodata_layout = nodata_layout2;
    }

    public List<SecondHouse> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        if (this.app.equals(App.SELL)) {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSellHouseList(this.app, this.location, this.radius, this.keywords, this.infofromValue, this.infotypeValue, this.districtValue, this.streetValue, this.priceValue, this.buildareaValue, this.roomValue, this.hallValue, this.toiletValue, this.listRefresh.page, this.order, this.metroValue, this.blockid);
        }
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseList(this.app, this.location, this.radius, this.keywords, this.infofromValue, this.infotypeValue, this.districtValue, this.streetValue, this.priceValue, this.buildareaValue, this.roomValue, this.hallValue, this.toiletValue, this.fitmentValue, this.rentTypeValue, this.listRefresh.page, this.order, this.busValue, this.metroValue, this.blockid);
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<SecondHouse> v) {
        if (this.houseListOnFinishListener != null) {
            this.houseListOnFinishListener.OnFinish(v);
        } else if (v == null || v.size() <= 0) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            if (this.nodata_layout != null) {
                this.nodata_layout.setVisibility(0);
                ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
                ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_house_nodata);
            }
        } else {
            if (!(this.listRefresh == null || this.pulllistView == null)) {
                this.adapter.clear();
                ViewUtil.onListDataComplete(this.context, v, this.listRefresh, this.adapter, this.pulllistView);
                this.adapter.notifyDataSetChanged();
            }
            if (this.nodata_layout != null) {
                this.nodata_layout.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.nodata_layout != null) {
            this.adapter.clear();
            this.adapter.notifyDataSetChanged();
            this.nodata_layout.setVisibility(0);
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }
}
