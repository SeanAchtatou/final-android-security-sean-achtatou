package com.house365.newhouse.task;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.EventNum;

public class GetTotalSaleTask extends CommonAsyncTask<EventNum> {
    private View nodata_layout;
    private GetTotalSaleNumOnSuccessListener onSuccessListener;

    public interface GetTotalSaleNumOnSuccessListener {
        void OnSuccess(EventNum eventNum);
    }

    public GetTotalSaleTask(Context context, View nodata_layout2) {
        super(context);
        this.nodata_layout = nodata_layout2;
    }

    public void onAfterDoInBackgroup(EventNum eventNum) {
        if (this.onSuccessListener != null) {
            this.onSuccessListener.OnSuccess(eventNum);
        }
    }

    public EventNum onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getEventNum();
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (this.nodata_layout != null) {
            this.nodata_layout.setVisibility(0);
            ((ImageView) this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }

    public GetTotalSaleNumOnSuccessListener getOnSuccessListener() {
        return this.onSuccessListener;
    }

    public void setOnSuccessListener(GetTotalSaleNumOnSuccessListener onSuccessListener2) {
        this.onSuccessListener = onSuccessListener2;
    }
}
