package com.house365.newhouse.task;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.view.LoadingDialog;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;

public class FavTask extends CommonAsyncTask<CommonResultInfo> {
    private int add_type;
    private Context context;
    private HouseInfo houseInfo;
    private String id;
    private ImageView imageView;
    protected LoadingDialog loadingDialog;
    private NewHouseApplication mApplication;
    private String type;

    public FavTask(Context context2, NewHouseApplication mApplication2, ImageView imageView2, HouseInfo houseInfo2) {
        super(context2);
        this.mApplication = mApplication2;
        this.context = context2;
        this.imageView = imageView2;
        this.houseInfo = houseInfo2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
        if (this.add_type == 0) {
            this.imageView.setTag(1);
            this.imageView.setImageResource(R.drawable.tab_unfollow);
            this.mApplication.saveFavHistory(this.houseInfo, this.type);
            Toast.makeText(this.context, (int) R.string.fav_success, 1).show();
        }
        if (this.add_type == 1) {
            this.imageView.setTag(0);
            this.imageView.setImageResource(R.drawable.tab_follow);
            this.mApplication.delFavHistory(this.houseInfo, this.type);
            Toast.makeText(this.context, (int) R.string.fav_cancle, 1).show();
        }
    }

    public CommonResultInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        this.type = this.houseInfo.getType();
        try {
            if (this.mApplication.hasFav(this.houseInfo, this.type)) {
                this.add_type = 1;
            } else {
                this.add_type = 0;
            }
            if (this.type.equals("house")) {
                this.id = this.houseInfo.getNewHouse().getH_id();
            }
            if (this.type.equals(App.RENT) || this.type.equals(App.SELL)) {
                this.id = this.houseInfo.getSellOrRent().getId();
            }
            if (this.type.equals("block")) {
                this.id = this.houseInfo.getBlock().getId();
            }
            if (this.add_type == 0) {
                return ((HttpApi) this.mApplication.getApi()).favHouse(this.id, this.type);
            }
            if (this.add_type == 1) {
                return ((HttpApi) this.mApplication.getApi()).removeFavHouse(this.id, this.type);
            }
            return null;
        } catch (Exception e) {
            CorePreferences.ERROR(e);
        }
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        super.onNetworkUnavailable();
        Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
    }
}
