package com.house365.newhouse.task;

import android.content.Context;
import android.widget.Toast;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;

public class GroupTask extends CommonAsyncTask<CommonResultInfo> {
    private Context context;
    private String id;
    private NewHouseApplication mApplication;
    private String mobile;
    private GroupTaskonFinish onFinish;
    private String truename;
    private String type;

    public interface GroupTaskonFinish {
        void onFinish();
    }

    public void showToast(String str) {
        Toast.makeText(this.context, str, 0).show();
    }

    public void showToast(int resId) {
        Toast.makeText(this.context, resId, 0).show();
    }

    public GroupTask(Context context2, NewHouseApplication mApplication2, String id2, String type2, String truename2, String mobile2) {
        super(context2);
        this.loadingresid = R.string.loading;
        this.mApplication = mApplication2;
        this.context = context2;
        this.id = id2;
        this.type = type2;
        this.truename = truename2;
        this.mobile = mobile2;
    }

    public GroupTaskonFinish getOnFinish() {
        return this.onFinish;
    }

    public void setOnFinish(GroupTaskonFinish onFinish2) {
        this.onFinish = onFinish2;
    }

    public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
        if (resultInfo != null) {
            if (resultInfo.getResult() == 1) {
                showToast(resultInfo.getMsg());
            } else {
                showToast(resultInfo.getMsg());
            }
            if (this.onFinish != null) {
                this.onFinish.onFinish();
                return;
            }
            return;
        }
        showToast((int) R.string.text_failue_work);
    }

    public CommonResultInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
        if (this.type.equals("house")) {
            return ((HttpApi) this.mApplication.getApi()).signUp(this.id, "house", this.truename, this.mobile);
        }
        if (this.type.equals(App.Categroy.Event.TLF)) {
            return ((HttpApi) this.mApplication.getApi()).signUp(this.id, App.Categroy.Event.TLF, null, this.mobile);
        }
        if (this.type.equals(App.Categroy.Event.TJF)) {
            return ((HttpApi) this.mApplication.getApi()).signUp(this.id, App.Categroy.Event.TJF, null, this.mobile);
        }
        if (this.type.equals(App.Categroy.Event.COUPON) || this.type.equals("event") || this.type.equals(App.Categroy.Event.SELL_EVENT)) {
            return ((HttpApi) this.mApplication.getApi()).signUp(this.id, this.type, this.truename, this.mobile);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        super.onNetworkUnavailable();
        Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
    }
}
