package com.house365.newhouse.interfaces;

public interface TaskFinishListener<T> {
    void onFinish(T t);
}
