package com.house365.newhouse.qrcode;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.qrcode.camera.CameraManager;
import com.house365.newhouse.qrcode.decoding.CaptureActivityHandler;
import com.house365.newhouse.qrcode.decoding.InactivityTimer;
import com.house365.newhouse.qrcode.view.ViewfinderView;
import com.house365.newhouse.tool.IntentRedirect;
import com.house365.newhouse.ui.UrlGetActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;

public class CaptureActivity extends Activity implements SurfaceHolder.Callback {
    private static final float BEEP_VOLUME = 0.1f;
    private static final long VIBRATE_DURATION = 200;
    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };
    private String characterSet;
    private Vector<BarcodeFormat> decodeFormats;
    private CaptureActivityHandler handler;
    private boolean hasSurface;
    private HeadNavigateView head_view;
    private InactivityTimer inactivityTimer;
    private MediaPlayer mediaPlayer;
    private boolean playBeep;
    private boolean vibrate;
    private ViewfinderView viewfinderView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.capture);
        CameraManager.init(getApplication());
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CaptureActivity.this.finish();
            }
        });
        this.viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        this.hasSurface = false;
        this.inactivityTimer = new InactivityTimer(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        SurfaceHolder surfaceHolder = ((SurfaceView) findViewById(R.id.preview_view)).getHolder();
        if (this.hasSurface) {
            initCamera(surfaceHolder);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(3);
        }
        this.decodeFormats = null;
        this.characterSet = null;
        this.playBeep = true;
        if (((AudioManager) getSystemService("audio")).getRingerMode() != 2) {
            this.playBeep = false;
        }
        initBeepSound();
        this.vibrate = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.handler != null) {
            this.handler.quitSynchronously();
            this.handler = null;
        }
        CameraManager.get().closeDriver();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.inactivityTimer.shutdown();
        super.onDestroy();
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        try {
            CameraManager.get().openDriver(surfaceHolder);
            if (this.handler == null) {
                this.handler = new CaptureActivityHandler(this, this.decodeFormats, this.characterSet);
            }
        } catch (IOException e) {
        } catch (RuntimeException e2) {
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this.hasSurface) {
            this.hasSurface = true;
            initCamera(holder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.hasSurface = false;
    }

    public ViewfinderView getViewfinderView() {
        return this.viewfinderView;
    }

    public Handler getHandler() {
        return this.handler;
    }

    public void drawViewfinder() {
        this.viewfinderView.drawViewfinder();
    }

    public void handleDecode(Result obj, Bitmap barcode) {
        this.inactivityTimer.onActivity();
        this.viewfinderView.drawResultBitmap(barcode);
        playBeepSoundAndVibrate();
        getCode(String.valueOf(obj.getBarcodeFormat().toString()) + ":" + obj.getText());
    }

    public static Map<String, String> getQueryParams(String url) {
        try {
            Map<String, String> params = new HashMap<>();
            String[] urlParts = url.split("\\?");
            if (urlParts.length > 1) {
                for (String param : urlParts[1].split("&")) {
                    String[] pair = param.split("=");
                    String key = URLDecoder.decode(pair[0], CharEncoding.UTF_8);
                    String value = StringUtils.EMPTY;
                    if (pair.length > 1) {
                        value = URLDecoder.decode(pair[1], CharEncoding.UTF_8);
                    }
                    params.put(key, value);
                }
            }
            return params;
        } catch (UnsupportedEncodingException ex) {
            throw new AssertionError(ex);
        }
    }

    private void getCode(String code) {
        String qrcode = code.replace("QR_CODE:", StringUtils.EMPTY);
        if (qrcode.contains("http://app.house365.com/ver.php")) {
            Map<String, String> mapParam = getQueryParams(qrcode);
            String qr_id = mapParam.get("id").toString();
            String qr_type = mapParam.get(CouponDetailsActivity.INTENT_TYPE).toString();
            if (qr_id != null && !TextUtils.isEmpty(qr_id)) {
                IntentRedirect.infoLink(qr_id, qr_type, this);
                return;
            }
            return;
        }
        Intent intent = new Intent(this, UrlGetActivity.class);
        intent.putExtra(UrlGetActivity.INTENT_URL, UrlGetActivity.generateUrlWithSystemParam(this, qrcode));
        startActivity(intent);
    }

    private void initBeepSound() {
        if (this.playBeep && this.mediaPlayer == null) {
            setVolumeControlStream(3);
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setAudioStreamType(3);
            this.mediaPlayer.setOnCompletionListener(this.beepListener);
            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                this.mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                this.mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                this.mediaPlayer.prepare();
            } catch (IOException e) {
                this.mediaPlayer = null;
            }
        }
    }

    private void playBeepSoundAndVibrate() {
        if (this.playBeep && this.mediaPlayer != null) {
            this.mediaPlayer.start();
        }
        if (this.vibrate) {
            ((Vibrator) getSystemService("vibrator")).vibrate((long) VIBRATE_DURATION);
        }
    }
}
