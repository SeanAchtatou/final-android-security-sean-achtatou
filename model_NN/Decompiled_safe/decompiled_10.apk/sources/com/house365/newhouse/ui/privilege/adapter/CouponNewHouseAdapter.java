package com.house365.newhouse.ui.privilege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.House;
import org.apache.commons.lang.StringUtils;

public class CouponNewHouseAdapter extends BaseCacheListAdapter<House> {
    private LayoutInflater inflater;

    public CouponNewHouseAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.coupon_newhouse_item, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.h_id = (TextView) convertView.findViewById(R.id.h_id);
            holder.h_name = (TextView) convertView.findViewById(R.id.h_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        House house = (House) getItem(position);
        holder.h_id.setText(house.getH_id());
        if (house.getH_name() != null && !house.getH_name().equals(StringUtils.EMPTY)) {
            holder.h_name.setText(house.getH_name());
        }
        return convertView;
    }

    private class ViewHolder {
        TextView h_id;
        TextView h_name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(CouponNewHouseAdapter couponNewHouseAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
