package com.house365.newhouse.ui.block;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.GetHouseListTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import com.house365.newhouse.ui.secondhouse.adapter.HouseListAdapter;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class BlockHouseListActivity extends BaseCommonActivity {
    /* access modifiers changed from: private */
    public HouseListAdapter adapter_sell;
    /* access modifiers changed from: private */
    public String app;
    protected HouseBaseInfo baseInfo;
    private View black_alpha_view;
    private String blockid;
    private Bundle bundle;
    /* access modifiers changed from: private */
    public boolean checkedRegion = true;
    private BroadcastReceiver chosedFinished = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String chose_from = intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
            if (chose_from.equals(ActionCode.SELL_SEARCH) || chose_from.equals(ActionCode.RENT_SEARCH)) {
                int type = SearchConditionPopView.getTagFlag(BlockHouseListActivity.this.text_region.getTag());
                if (type == 1) {
                    String district_Street = SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag());
                    if (district_Street == null || district_Street.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        BlockHouseListActivity.this.districtValue = 0;
                        BlockHouseListActivity.this.streetValue = 0;
                    } else if (district_Street.indexOf("+") != -1) {
                        String district = district_Street.substring(0, district_Street.indexOf("+"));
                        String street = district_Street.substring(district_Street.indexOf("+") + 1, district_Street.length());
                        BlockHouseListActivity.this.districtValue = SearchConditionPopView.getIndex(AppMethods.mapKeyTolistSubWay(BlockHouseListActivity.this.baseInfo.getDistrictStreet(), false), district) + 1;
                        BlockHouseListActivity.this.streetValue = Integer.parseInt((String) BlockHouseListActivity.this.baseInfo.getDistrictStreet().get(district).get(street));
                        BlockHouseListActivity.this.metroValue = null;
                    }
                } else if (type == 2) {
                    String subWay = SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag());
                    if (subWay == null || subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        BlockHouseListActivity.this.metroValue = null;
                    } else if (subWay.indexOf("+") != -1) {
                        String metro = subWay.substring(0, subWay.indexOf("+"));
                        BlockHouseListActivity.this.metroValue = ((Station) BlockHouseListActivity.this.baseInfo.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()))).getId();
                        BlockHouseListActivity.this.districtValue = 0;
                        BlockHouseListActivity.this.streetValue = 0;
                    }
                }
                BlockHouseListActivity.this.infofromValue = SearchConditionPopView.setIntValue(BlockHouseListActivity.this.text_resource, BlockHouseListActivity.this.infofromValue, HouseBaseInfo.INFOFROM, BlockHouseListActivity.this.baseInfo);
                if (BlockHouseListActivity.this.app.equals(App.SELL)) {
                    BlockHouseListActivity.this.priceValue = SearchConditionPopView.setIntValue(BlockHouseListActivity.this.text_price, BlockHouseListActivity.this.priceValue, "price", BlockHouseListActivity.this.baseInfo);
                } else if (BlockHouseListActivity.this.app.equals(App.RENT)) {
                    BlockHouseListActivity.this.priceValue = SearchConditionPopView.setIntValue(BlockHouseListActivity.this.text_price, BlockHouseListActivity.this.priceValue, HouseBaseInfo.PRICE_DEFAULT, BlockHouseListActivity.this.baseInfo);
                }
                String sortData = SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_sort.getTag());
                if (!TextUtils.isEmpty(sortData) && !sortData.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    BlockHouseListActivity.this.orderBy = BlockHouseListActivity.this.ordervalues[SearchConditionPopView.getIndex(BlockHouseListActivity.this.sortList, sortData)];
                }
                BlockHouseListActivity.this.refreshData();
            }
        }
    };
    /* access modifiers changed from: private */
    public RadioGroup condition_group;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public int infofromValue;
    private int infotypeValue;
    private String keywords;
    private PullToRefreshListView listview_sell;
    private TextView location_addr;
    private Location mLocation;
    /* access modifiers changed from: private */
    public String metroValue;
    private View nodata_layout;
    /* access modifiers changed from: private */
    public int orderBy;
    /* access modifiers changed from: private */
    public final int[] ordervalues = {-1, 2, 1, 3, 4, 6, 5};
    private ArrayList<String> priceList;
    /* access modifiers changed from: private */
    public int priceValue;
    private View price_layout;
    private int radiusvalue = 2500;
    private RefreshInfo refreshInfo_sell;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, String>> regionMap;
    /* access modifiers changed from: private */
    public View region_layout;
    private final String[] rent_orders = {ActionCode.PREFERENCE_SEARCH_DEFAULT, "发布日期由近到远", "发布日期由远到近", "租金由低到高", "租金由高到低", "面积由大到小", "面积由小到大"};
    private ArrayList<String> resourceList;
    private View resource_layout;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private final String[] sell_orders = {ActionCode.PREFERENCE_SEARCH_DEFAULT, "发布日期由近到远", "发布日期由远到近", "总价由低到高", "总价由高到低", "面积由大到小", "面积由小到大"};
    /* access modifiers changed from: private */
    public ArrayList<String> sortList = new ArrayList<>();
    private View sort_layout;
    /* access modifiers changed from: private */
    public int streetValue;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, Station>> subwayMap;
    /* access modifiers changed from: private */
    public List<TextView> textViews = new ArrayList();
    /* access modifiers changed from: private */
    public TextView text_price;
    /* access modifiers changed from: private */
    public TextView text_region;
    /* access modifiers changed from: private */
    public TextView text_resource;
    /* access modifiers changed from: private */
    public TextView text_sort;
    /* access modifiers changed from: private */
    public List<View> views = new ArrayList();

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        setContentView((int) R.layout.block_house_list);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(false);
        this.searchPop.setShowOnView(true);
        this.condition_group = this.searchPop.getCondition_group();
        this.condition_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                BlockHouseListActivity.this.searchPop.setShowGroup(true);
                int tag = SearchConditionPopView.getTagFlag(BlockHouseListActivity.this.text_region.getTag());
                String chosed = null;
                if (checkedId == R.id.rb_region) {
                    if (tag != 2) {
                        chosed = SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag());
                    }
                    BlockHouseListActivity.this.searchPop.setGradeData(BlockHouseListActivity.this.text_region, chosed, BlockHouseListActivity.this.regionMap, 1);
                    BlockHouseListActivity.this.checkedRegion = true;
                } else if (checkedId == R.id.rb_subway) {
                    if (tag != 1) {
                        chosed = SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag());
                    }
                    BlockHouseListActivity.this.searchPop.setGradeData(BlockHouseListActivity.this.subwayMap, BlockHouseListActivity.this.text_region, chosed, 2);
                    BlockHouseListActivity.this.checkedRegion = false;
                }
            }
        });
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
        this.bundle = getIntent().getExtras();
        this.keywords = this.bundle.getString("keywords");
        this.refreshInfo_sell = new RefreshInfo();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.bundle = getIntent().getExtras();
        if (this.bundle != null) {
            String from = this.bundle.getString("app");
            if (!TextUtils.isEmpty(from)) {
                this.app = from;
                if (this.app.equals(App.SELL)) {
                    this.text_region.setText((int) R.string.text_region_title);
                    this.text_resource.setText((int) R.string.text_resource_title);
                    this.text_price.setText((int) R.string.text_total_price_title);
                    this.text_sort.setText((int) R.string.text_search_orderby);
                } else if (this.app.equals(App.RENT)) {
                    this.text_region.setText((int) R.string.text_region_title);
                    this.text_resource.setText((int) R.string.text_resource_title);
                    this.text_price.setText((int) R.string.text_rent_title);
                    this.text_sort.setText((int) R.string.text_search_orderby);
                }
                initData();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
        this.adapter_sell.clear();
    }

    private void updataFinish() {
        Intent intent = new Intent();
        Bundle refreshBundle = new Bundle();
        refreshBundle.putInt("infofromValue", this.infofromValue);
        refreshBundle.putInt("priceValue", this.priceValue);
        if (this.text_region.getTag() != null) {
            refreshBundle.putString("distsubway", this.text_region.getTag().toString());
        }
        intent.putExtras(refreshBundle);
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockHouseListActivity.this.finish();
            }
        });
        this.listview_sell = (PullToRefreshListView) findViewById(R.id.list);
        this.region_layout = findViewById(R.id.newhouse_region_layout);
        this.price_layout = findViewById(R.id.newhouse_price_layout);
        this.resource_layout = findViewById(R.id.newhouse_type_layout);
        this.sort_layout = findViewById(R.id.newhouse_sort_layout);
        this.text_region = (TextView) findViewById(R.id.text_newhouse_region);
        this.text_price = (TextView) findViewById(R.id.text_newhouse_price);
        this.text_price.setText((int) R.string.text_total_price_title);
        this.text_resource = (TextView) findViewById(R.id.text_newhouse_type);
        this.text_resource.setText((int) R.string.text_resource_title);
        this.text_sort = (TextView) findViewById(R.id.text_newhouse_sort);
        this.views.add(this.region_layout);
        this.views.add(this.price_layout);
        this.views.add(this.resource_layout);
        this.views.add(this.sort_layout);
        this.textViews.add(this.text_region);
        this.textViews.add(this.text_price);
        this.textViews.add(this.text_resource);
        this.textViews.add(this.text_sort);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.listview_sell.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                BlockHouseListActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                BlockHouseListActivity.this.getMoreData();
            }
        });
        this.listview_sell.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (BlockHouseListActivity.this.app.equals(App.SELL)) {
                    Intent intent = new Intent(BlockHouseListActivity.this.context, SecondSellDetailActivity.class);
                    intent.putExtra("id", ((SecondHouse) BlockHouseListActivity.this.adapter_sell.getItem(position)).getId());
                    intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                    BlockHouseListActivity.this.startActivity(intent);
                } else if (BlockHouseListActivity.this.app.equals(App.RENT)) {
                    Intent intent2 = new Intent(BlockHouseListActivity.this.context, SecondRentDetailActivity.class);
                    intent2.putExtra("id", ((SecondHouse) BlockHouseListActivity.this.adapter_sell.getItem(position)).getId());
                    intent2.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                    BlockHouseListActivity.this.startActivity(intent2);
                }
            }
        });
    }

    private boolean has(List<Block> blocks, String blockId) {
        for (int i = 0; i < blocks.size(); i++) {
            if (blocks.get(i) != null && blocks.get(i).getId() != null) {
                return blockId.equals(blocks.get(i).getId());
            }
        }
        return false;
    }

    public void refreshData() {
        this.refreshInfo_sell.refresh = true;
        getTaskData();
    }

    public void getMoreData() {
        this.refreshInfo_sell.refresh = false;
        getTaskData();
    }

    private void getTaskData() {
        new GetHouseListTask(this.context, this.listview_sell, this.refreshInfo_sell, this.adapter_sell, this.app, this.blockid, this.orderBy, this.infofromValue, this.districtValue, this.streetValue, this.priceValue, this.metroValue, this.nodata_layout).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.app = this.bundle.getString("app");
        this.infofromValue = this.bundle.getInt("infofromValue");
        this.infotypeValue = this.bundle.getInt("infotypeValue");
        this.districtValue = this.bundle.getInt("districtValue");
        this.streetValue = this.bundle.getInt("streetValue");
        this.priceValue = this.bundle.getInt("priceValue");
        this.metroValue = this.bundle.getString(HouseBaseInfo.METRO);
        this.blockid = this.bundle.getString("blockid");
        String b_name = this.bundle.getString("b_name");
        if (!TextUtils.isEmpty(b_name)) {
            this.head_view.setTvTitleText(b_name);
        }
        if (this.app.equals(App.SELL)) {
            this.sortList = (ArrayList) AppMethods.toList(this.sell_orders);
            this.searchPop.setSearch_type(ActionCode.SELL_SEARCH);
        } else if (this.app.equals(App.RENT)) {
            this.sortList = (ArrayList) AppMethods.toList(this.rent_orders);
            this.searchPop.setSearch_type(ActionCode.RENT_SEARCH);
        }
        getConfig(R.string.loading);
        this.adapter_sell = new HouseListAdapter(this, this.app);
        this.listview_sell.setAdapter(this.adapter_sell);
        this.adapter_sell.notifyDataSetChanged();
        this.text_region.setText((int) R.string.text_region_title);
        if (AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity())) {
            this.sort_layout.setVisibility(8);
        } else {
            this.sort_layout.setVisibility(0);
        }
    }

    private void addNormalSearchListener(View layout, TextView showDataView, ArrayList dataList, int choseType) {
        final View view = layout;
        final TextView textView = showDataView;
        final ArrayList arrayList = dataList;
        final int i = choseType;
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockHouseListActivity.this.searchPop.setShowGroup(false);
                SearchConditionPopView.refreshViews(BlockHouseListActivity.this.views, BlockHouseListActivity.this.textViews, view, textView, BlockHouseListActivity.this.context);
                BlockHouseListActivity.this.searchPop.setGradeData(arrayList, textView, textView.getText().toString(), i);
            }
        });
    }

    /* access modifiers changed from: private */
    public void initSearchItem() {
        this.regionMap = this.baseInfo.getDistrictStreet();
        this.subwayMap = this.baseInfo.getMetro();
        this.resourceList = this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM);
        if (this.app.equals(App.SELL)) {
            this.priceList = this.baseInfo.getSell_config().get("price");
        } else if (this.app.equals(App.RENT)) {
            this.priceList = this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT);
        }
        String text_subway = this.bundle.getString("text_subway");
        if (this.metroValue != null && !TextUtils.isEmpty(text_subway)) {
            this.text_region.setText(text_subway);
            this.text_region.setTag("2:" + text_subway);
        }
        String regionStr = this.bundle.getString("text_region");
        if (!TextUtils.isEmpty(regionStr) && !regionStr.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.text_region.setText(regionStr);
            this.text_region.setTag("1:" + regionStr);
        }
        if (this.infofromValue != 0) {
            this.text_resource.setText(this.resourceList.get(this.infofromValue));
        }
        if (this.priceValue != 0) {
            this.text_price.setText(this.priceList.get(this.priceValue));
        }
        this.region_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchConditionPopView.refreshViews(BlockHouseListActivity.this.views, BlockHouseListActivity.this.textViews, BlockHouseListActivity.this.region_layout, BlockHouseListActivity.this.text_region, BlockHouseListActivity.this.context);
                BlockHouseListActivity.this.searchPop.setShowDataView(BlockHouseListActivity.this.text_region);
                BlockHouseListActivity.this.searchPop.setRegionOrSubway(BlockHouseListActivity.this.text_region);
                if (BlockHouseListActivity.this.subwayMap == null || BlockHouseListActivity.this.subwayMap.isEmpty()) {
                    BlockHouseListActivity.this.searchPop.setShowGroup(false);
                    BlockHouseListActivity.this.searchPop.setGradeData(BlockHouseListActivity.this.text_region, SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag()), BlockHouseListActivity.this.regionMap, 1);
                    return;
                }
                BlockHouseListActivity.this.searchPop.setShowGroup(true);
                if (BlockHouseListActivity.this.checkedRegion) {
                    BlockHouseListActivity.this.searchPop.setGradeData(BlockHouseListActivity.this.text_region, SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag()), BlockHouseListActivity.this.regionMap, 1);
                } else {
                    BlockHouseListActivity.this.searchPop.setGradeData(BlockHouseListActivity.this.subwayMap, BlockHouseListActivity.this.text_region, SearchConditionPopView.getTagData(BlockHouseListActivity.this.text_region.getTag()), 2);
                }
                BlockHouseListActivity.this.condition_group.setVisibility(0);
            }
        });
        if (this.app.equals(App.SELL)) {
            addNormalSearchListener(this.price_layout, this.text_price, this.priceList, 6);
        } else if (this.app.equals(App.RENT)) {
            addNormalSearchListener(this.price_layout, this.text_price, this.priceList, 9);
        }
        addNormalSearchListener(this.resource_layout, this.text_resource, this.resourceList, 8);
        addNormalSearchListener(this.sort_layout, this.text_sort, this.sortList, 0);
    }

    private void getConfig(int resid) {
        int type = 1;
        if (this.app.equals(App.SELL)) {
            type = 1;
        } else if (this.app.equals(App.RENT)) {
            type = 2;
        }
        GetConfigTask getConfig = new GetConfigTask(this, resid, type);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info != null) {
                    BlockHouseListActivity.this.baseInfo = info;
                    BlockHouseListActivity.this.initSearchItem();
                    BlockHouseListActivity.this.refreshData();
                }
            }

            public void OnError(HouseBaseInfo info) {
                Toast.makeText(BlockHouseListActivity.this.context, (int) R.string.text_city_config_error, 1).show();
            }
        });
        getConfig.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        updataFinish();
        return false;
    }
}
