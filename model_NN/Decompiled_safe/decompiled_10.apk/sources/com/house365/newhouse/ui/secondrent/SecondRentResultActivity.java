package com.house365.newhouse.ui.secondrent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.json.JSONObject;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TextUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayGestureDetector;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.ZoomEvent;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.GetHouseListTask;
import com.house365.newhouse.task.LocationTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.mapsearch.HouseBlockOverlay;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import com.house365.newhouse.ui.secondhouse.adapter.HouseListAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SecondRentResultActivity extends BaseBaiduMapActivity {
    public static final int FROM_TYPE = 1;
    public static String INTENT_FROM_NEARBY = "from_nearby";
    /* access modifiers changed from: private */
    public HouseListAdapter adapter_rent;
    private String app = App.RENT;
    protected HouseBaseInfo baseInfo;
    private View black_alpha_view;
    private String blockId;
    private String blockid;
    private RadioGroup bt_map_list_group;
    private Bundle bundle;
    /* access modifiers changed from: private */
    public int busValue;
    /* access modifiers changed from: private */
    public boolean checkedRegion;
    private BroadcastReceiver chosedFinished;
    /* access modifiers changed from: private */
    public boolean click_nearby_refresh;
    /* access modifiers changed from: private */
    public RadioGroup condition_group;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    private int fitmentValue;
    private int form_subway;
    private int from_litter_home;
    /* access modifiers changed from: private */
    public boolean from_nearby;
    private int hallValue;
    boolean hasInitedMap = false;
    private HeadNavigateView head_view;
    private HouseBlockOverlay houseBlock;
    private ManagedOverlay houseOverlay;
    /* access modifiers changed from: private */
    public int infofromValue;
    /* access modifiers changed from: private */
    public int infotypeValue;
    private String keywords;
    /* access modifiers changed from: private */
    public Animation leftInAnim;
    /* access modifiers changed from: private */
    public Animation leftOutAnim;
    private PullToRefreshListView listview_rent;
    private View loadingLayout;
    /* access modifiers changed from: private */
    public TextView location_addr;
    private Drawable location_marker;
    private BroadcastReceiver mChangeCity;
    /* access modifiers changed from: private */
    public Location mLocation;
    private BroadcastReceiver mLocationFinish;
    /* access modifiers changed from: private */
    public boolean mapChoose;
    MapController mapController;
    /* access modifiers changed from: private */
    public View mapLoading;
    MapView mapView;
    private View map_list_change;
    private int maptype;
    private Drawable marker;
    /* access modifiers changed from: private */
    public String metroValue;
    private ManagedOverlay myPositionOverlay;
    /* access modifiers changed from: private */
    public View nearby_refresh_layout;
    private View nodata_layout;
    /* access modifiers changed from: private */
    public int orderBy;
    private final String[] orders = {ActionCode.PREFERENCE_SEARCH_DEFAULT, "发布日期由近到远", "发布日期由远到近", "租金由低到高", "租金由高到低", "面积由大到小", "面积由小到大"};
    /* access modifiers changed from: private */
    public final int[] ordervalues = {-1, 2, 1, 3, 4, 6, 5};
    OverlayManager overlayManager;
    private ArrayList<String> priceDefalutList;
    private ArrayList<String> priceOfficeList;
    private ArrayList<String> priceStoreList;
    private String priceView_from;
    private View price_layout;
    /* access modifiers changed from: private */
    public final int[] radious;
    /* access modifiers changed from: private */
    public ArrayList<String> radiousList;
    /* access modifiers changed from: private */
    public int radiusvalue = 2500;
    private RefreshInfo refreshInfo_rent;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, String>> regionMap;
    /* access modifiers changed from: private */
    public View region_layout;
    private int rentTypeValue;
    /* access modifiers changed from: private */
    public int rent_priceValue;
    private ArrayList<String> resourceList;
    private View resource_layout;
    /* access modifiers changed from: private */
    public Animation rightInAnim;
    /* access modifiers changed from: private */
    public Animation rightOutAnim;
    private int roomValue;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private String search_type = ActionCode.RENT_SEARCH;
    /* access modifiers changed from: private */
    public ArrayList<String> sortList;
    private View sort_layout;
    /* access modifiers changed from: private */
    public int streetValue;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, Station>> subwayMap;
    /* access modifiers changed from: private */
    public List<TextView> textViews;
    /* access modifiers changed from: private */
    public TextView text_price;
    /* access modifiers changed from: private */
    public TextView text_region;
    /* access modifiers changed from: private */
    public TextView text_resource;
    /* access modifiers changed from: private */
    public TextView text_sort;
    private int toiletValue;
    /* access modifiers changed from: private */
    public ViewSwitcher viewSwitcher;
    /* access modifiers changed from: private */
    public List<View> views;

    public SecondRentResultActivity() {
        int[] iArr = new int[5];
        iArr[0] = 1000;
        iArr[1] = 2500;
        iArr[2] = 5000;
        iArr[3] = 10000;
        this.radious = iArr;
        this.sortList = new ArrayList<>();
        this.radiousList = new ArrayList<>();
        this.checkedRegion = true;
        this.views = new ArrayList();
        this.textViews = new ArrayList();
        this.priceView_from = StringUtils.EMPTY;
        this.maptype = 13;
        this.mChangeCity = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                SecondRentResultActivity.this.getConfig(R.string.loading);
            }
        };
        this.mLocationFinish = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int changeCity = intent.getIntExtra(ActionCode.LOCATION_FINISH, 0);
                int changetag = intent.getIntExtra(ActionCode.LOCATION_TAG, 0);
                switch (changeCity) {
                    case 1:
                        SecondRentResultActivity.this.setMyLocation();
                        SecondRentResultActivity.this.mapLoading.setVisibility(8);
                        SecondRentResultActivity.this.refreshData();
                        return;
                    case 2:
                    default:
                        return;
                    case 3:
                        if (changetag == 13) {
                            SecondRentResultActivity.this.finish();
                            return;
                        }
                        return;
                }
            }
        };
        this.chosedFinished = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE).equals(ActionCode.RENT_SEARCH)) {
                    int type = SearchConditionPopView.getTagFlag(SecondRentResultActivity.this.text_region.getTag());
                    if (SecondRentResultActivity.this.from_nearby && type == 10) {
                        SecondRentResultActivity.this.radiusvalue = SecondRentResultActivity.this.radious[SearchConditionPopView.getIndex(SecondRentResultActivity.this.radiousList, SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag()))];
                    } else if (type == 1) {
                        String district_Street = SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag());
                        if (district_Street == null || district_Street.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                            SecondRentResultActivity.this.districtValue = 0;
                            SecondRentResultActivity.this.streetValue = 0;
                        } else if (district_Street.indexOf("+") != -1) {
                            String district = district_Street.substring(0, district_Street.indexOf("+"));
                            String street = district_Street.substring(district_Street.indexOf("+") + 1, district_Street.length());
                            SecondRentResultActivity.this.districtValue = SearchConditionPopView.getIndex(AppMethods.mapKeyTolistSubWay(SecondRentResultActivity.this.baseInfo.getDistrictStreet(), false), district) + 1;
                            SecondRentResultActivity.this.streetValue = Integer.parseInt((String) SecondRentResultActivity.this.baseInfo.getDistrictStreet().get(district).get(street));
                            SecondRentResultActivity.this.metroValue = null;
                            SecondRentResultActivity.this.busValue = 0;
                        }
                    } else if (type == 2) {
                        String subWay = SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag());
                        if (subWay == null || subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                            SecondRentResultActivity.this.metroValue = null;
                        } else if (subWay.indexOf("+") != -1) {
                            String metro = subWay.substring(0, subWay.indexOf("+"));
                            SecondRentResultActivity.this.metroValue = ((Station) SecondRentResultActivity.this.baseInfo.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()))).getId();
                            SecondRentResultActivity.this.districtValue = 0;
                            SecondRentResultActivity.this.streetValue = 0;
                            SecondRentResultActivity.this.busValue = 0;
                        }
                    }
                    SecondRentResultActivity.this.infofromValue = SearchConditionPopView.setIntValue(SecondRentResultActivity.this.text_resource, SecondRentResultActivity.this.infofromValue, HouseBaseInfo.INFOFROM, SecondRentResultActivity.this.baseInfo);
                    SecondRentResultActivity.this.infotypeValue = SearchConditionPopView.setIntValue(SecondRentResultActivity.this.text_resource, SecondRentResultActivity.this.infofromValue, HouseBaseInfo.INFOTYPE, SecondRentResultActivity.this.baseInfo);
                    if (SecondRentResultActivity.this.infotypeValue == 3) {
                        SecondRentResultActivity.this.rent_priceValue = SearchConditionPopView.setIntValue(SecondRentResultActivity.this.text_price, SecondRentResultActivity.this.rent_priceValue, HouseBaseInfo.PRICE_OFFICE, SecondRentResultActivity.this.baseInfo);
                    } else if (SecondRentResultActivity.this.infotypeValue == 3) {
                        SecondRentResultActivity.this.rent_priceValue = SearchConditionPopView.setIntValue(SecondRentResultActivity.this.text_price, SecondRentResultActivity.this.rent_priceValue, HouseBaseInfo.PRICE_STORE, SecondRentResultActivity.this.baseInfo);
                    } else {
                        SecondRentResultActivity.this.rent_priceValue = SearchConditionPopView.setIntValue(SecondRentResultActivity.this.text_price, SecondRentResultActivity.this.rent_priceValue, HouseBaseInfo.PRICE_DEFAULT, SecondRentResultActivity.this.baseInfo);
                    }
                    String sortData = SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_sort.getTag());
                    if (!TextUtils.isEmpty(sortData) && !sortData.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        SecondRentResultActivity.this.orderBy = SecondRentResultActivity.this.ordervalues[SearchConditionPopView.getIndex(SecondRentResultActivity.this.sortList, sortData)];
                    }
                    Log.e("data", "dis\t" + SecondRentResultActivity.this.districtValue + "\n" + "street\t" + SecondRentResultActivity.this.streetValue + "\n" + "prc\t" + SecondRentResultActivity.this.rent_priceValue + "\n" + "resource\t" + SecondRentResultActivity.this.infofromValue + "\n" + "metro\t" + SecondRentResultActivity.this.metroValue + "\n" + "orderBy" + SecondRentResultActivity.this.orderBy + "\n" + "radiusvalue" + SecondRentResultActivity.this.radiusvalue);
                    SecondRentResultActivity.this.refreshData();
                }
            }
        };
    }

    public MapView getMapView() {
        return this.mapView;
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        setContentView((int) R.layout.second_search_result);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(false);
        this.searchPop.setShowOnView(true);
        this.searchPop.setSearch_type(ActionCode.RENT_SEARCH);
        this.condition_group = this.searchPop.getCondition_group();
        this.condition_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                SecondRentResultActivity.this.searchPop.setShowGroup(true);
                int tag = SearchConditionPopView.getTagFlag(SecondRentResultActivity.this.text_region.getTag());
                String chosed = null;
                if (checkedId == R.id.rb_region) {
                    if (tag != 2) {
                        chosed = SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag());
                    }
                    SecondRentResultActivity.this.searchPop.setGradeData(SecondRentResultActivity.this.text_region, chosed, SecondRentResultActivity.this.regionMap, 1);
                    SecondRentResultActivity.this.checkedRegion = true;
                } else if (checkedId == R.id.rb_subway) {
                    if (tag != 1) {
                        chosed = SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag());
                    }
                    SecondRentResultActivity.this.searchPop.setGradeData(SecondRentResultActivity.this.subwayMap, SecondRentResultActivity.this.text_region, chosed, 2);
                    SecondRentResultActivity.this.checkedRegion = false;
                }
            }
        });
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
        this.bundle = getIntent().getExtras();
        this.form_subway = this.bundle.getInt("FROM_SUBWAY");
        this.from_litter_home = getIntent().getIntExtra("FROM_LITTER_HOME", 0);
        this.priceView_from = getIntent().getStringExtra("priceView_from");
        registerReceiver(this.mLocationFinish, new IntentFilter(ActionCode.INTENT_ACTION_LOCATION_FINISH));
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        this.keywords = this.bundle.getString("keywords");
        this.refreshInfo_rent = new RefreshInfo();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((!this.click_nearby_refresh && !this.from_nearby) || requestCode != 1) {
            return;
        }
        if (DeviceUtil.isOpenLoaction(this.context)) {
            relocation();
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
        unregisterReceiver(this.mLocationFinish);
        unregisterReceiver(this.mChangeCity);
        this.adapter_rent.clear();
    }

    /* access modifiers changed from: private */
    public void updataFinish() {
        Intent intent = new Intent();
        Bundle refreshBundle = new Bundle();
        refreshBundle.putInt("infotypeValue", this.infotypeValue);
        refreshBundle.putInt("infofromValue", this.infofromValue);
        refreshBundle.putInt("rent_priceValue", this.rent_priceValue);
        if (this.text_region.getTag() != null) {
            refreshBundle.putString("distsubway", this.text_region.getTag().toString());
        }
        intent.putExtras(refreshBundle);
        setResult(-1, intent);
        LocationTask.isdoing = false;
        finish();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.sortList = (ArrayList) AppMethods.toList(this.orders);
        this.radiousList = (ArrayList) AppMethods.toList(AppArrays.getRadious());
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentResultActivity.this.updataFinish();
            }
        });
        this.map_list_change = findViewById(R.id.map_list_change);
        this.listview_rent = (PullToRefreshListView) findViewById(R.id.list);
        this.adapter_rent = new HouseListAdapter(this, this.app);
        this.region_layout = findViewById(R.id.newhouse_region_layout);
        this.price_layout = findViewById(R.id.newhouse_price_layout);
        this.resource_layout = findViewById(R.id.newhouse_type_layout);
        this.sort_layout = findViewById(R.id.newhouse_sort_layout);
        this.text_region = (TextView) findViewById(R.id.text_newhouse_region);
        this.text_price = (TextView) findViewById(R.id.text_newhouse_price);
        this.text_price.setText((int) R.string.text_rent_title);
        this.text_resource = (TextView) findViewById(R.id.text_newhouse_type);
        this.text_resource.setText((int) R.string.text_resource_title);
        this.text_sort = (TextView) findViewById(R.id.text_newhouse_sort);
        this.views.add(this.region_layout);
        this.views.add(this.price_layout);
        this.views.add(this.resource_layout);
        this.views.add(this.sort_layout);
        this.textViews.add(this.text_region);
        this.textViews.add(this.text_price);
        this.textViews.add(this.text_resource);
        this.textViews.add(this.text_sort);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.nearby_refresh_layout = findViewById(R.id.nearby_refresh_layout);
        this.loadingLayout = findViewById(R.id.loadingLayout);
        this.mapLoading = findViewById(R.id.mapLoading);
        this.location_addr = (TextView) findViewById(R.id.location_addr);
        this.bt_map_list_group = (RadioGroup) findViewById(R.id.bt_map_list_group);
        this.bt_map_list_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_list) {
                    if (SecondRentResultActivity.this.from_nearby) {
                        SecondRentResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    }
                    SecondRentResultActivity.this.viewSwitcher.setOutAnimation(SecondRentResultActivity.this.rightOutAnim);
                    SecondRentResultActivity.this.viewSwitcher.setInAnimation(SecondRentResultActivity.this.leftInAnim);
                    SecondRentResultActivity.this.viewSwitcher.setDisplayedChild(0);
                    SecondRentResultActivity.this.mapChoose = false;
                } else if (checkedId == R.id.bt_map) {
                    SecondRentResultActivity.this.nearby_refresh_layout.setVisibility(8);
                    SecondRentResultActivity.this.viewSwitcher.setOutAnimation(SecondRentResultActivity.this.leftOutAnim);
                    SecondRentResultActivity.this.viewSwitcher.setInAnimation(SecondRentResultActivity.this.rightInAnim);
                    SecondRentResultActivity.this.viewSwitcher.setDisplayedChild(1);
                    SecondRentResultActivity.this.mapChoose = true;
                    SecondRentResultActivity.this.mapLoading.setVisibility(8);
                    SecondRentResultActivity.this.initMap();
                }
            }
        });
        this.viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        this.leftOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        this.rightInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        this.rightOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        this.leftInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        this.mapView = (MapView) findViewById(R.id.mapview);
        this.mapController = this.mapView.getController();
        this.marker = getResources().getDrawable(R.drawable.ico_marker);
        this.location_marker = getResources().getDrawable(R.drawable.ico_location_marker);
        this.overlayManager = new OverlayManager(getApplication(), this.mapView);
        this.houseOverlay = this.overlayManager.createOverlay("houses", this.marker);
        this.myPositionOverlay = this.overlayManager.createOverlay("myposition", this.location_marker);
        this.houseBlock = new HouseBlockOverlay(this.context, this.overlayManager, null, this.houseOverlay, this.mapController, this.mApplication, this.mapView, null);
        this.houseOverlay.setOnOverlayGestureListener(new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
            public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
                return false;
            }

            public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (point == null) {
                    return true;
                }
                SecondRentResultActivity.this.moveMapToPoint(point);
                return true;
            }

            public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
            }

            public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item != null) {
                    SecondRentResultActivity.this.clickTap(item);
                }
            }

            public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
                return false;
            }

            public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item == null) {
                    return false;
                }
                SecondRentResultActivity.this.clickTap(item);
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickTap(ManagedOverlayItem item) {
        ManagedOverlayItem last = this.houseBlock.getLastItem();
        item.setTag(2);
        this.houseBlock.setHouseMarker(item, this.maptype);
        if (last != null && !last.getSnippet().equals(item.getSnippet())) {
            last.setTag(1);
            this.houseBlock.setHouseMarker(last, this.maptype);
        }
        this.houseBlock.setHouseOverlayTap(item, this.maptype, 0, 0, 0);
        this.houseBlock.setLastItem(item);
        moveMapToPoint(item.getPoint());
    }

    /* access modifiers changed from: private */
    public void moveMapToPoint(GeoPoint geopoint) {
        if (getMapView() != null && this.mapView.getController() != null) {
            this.mapView.getController().animateTo(geopoint);
            this.mapView.getController().setCenter(geopoint);
        }
    }

    private void refreshViews(View selectedView, TextView textView) {
        for (int i = 0; i < this.views.size(); i++) {
            if (selectedView.getId() == this.views.get(i).getId() && this.textViews.get(i).getId() == textView.getId()) {
                selectedView.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_search_selected));
                textView.setTextColor(getResources().getColor(R.color.group_blue));
            } else {
                this.views.get(i).setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_search_normal));
                this.textViews.get(i).setTextColor(getResources().getColor(R.color.gray));
            }
        }
    }

    /* access modifiers changed from: private */
    public void initMap() {
        double[] cityLoc = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(cityLoc[0], cityLoc[1]));
        this.houseOverlay.getOverlayItems().clear();
        if (this.mapController != null) {
            this.mapController.setZoom(12);
        }
        int page = this.listview_rent.getActureListView().getFirstVisiblePosition() / 20;
        int startIdx = page * 20;
        int endIdx = ((page + 1) * 20) - 1;
        List<Block> blocks = new ArrayList<>();
        if (endIdx > this.adapter_rent.getList().size() - 1) {
            endIdx = this.adapter_rent.getList().size() - 1;
        }
        if (endIdx < 0) {
            endIdx = 0;
        }
        for (int i = startIdx; i <= endIdx; i++) {
            if (this.adapter_rent.getList().size() <= 0) {
                showToast((int) R.string.text_no_result);
            } else if (!(this.adapter_rent.getList() == null || this.adapter_rent.getList().get(i) == null)) {
                SecondHouse house = (SecondHouse) this.adapter_rent.getList().get(i);
                if (house.getBlockinfo() != null && !has(blocks, house.getBlockinfo().getId())) {
                    blocks.add(house.getBlockinfo());
                }
            }
        }
        List<ManagedOverlayItem> ms = new ArrayList<>();
        for (Block block : blocks) {
            ms.add(new ManagedOverlayItem(BaiduMapUtil.getPoint(block.getLat(), block.getLng()), block.getId(), new JSONObject(block).toString()));
        }
        if (ms != null && ms.size() > 0) {
            this.houseOverlay.addAll(ms);
            this.houseBlock.setMarker(this.maptype, 2);
            this.overlayManager.populate();
        }
    }

    private boolean has(List<Block> blocks, String blockId2) {
        for (int i = 0; i < blocks.size(); i++) {
            if (blocks.get(i) != null) {
                return blockId2.equals(blocks.get(i).getId());
            }
        }
        return false;
    }

    public void refreshData() {
        this.refreshInfo_rent.refresh = true;
        new GetHouseListTask(this.context, this.listview_rent, this.refreshInfo_rent, this.adapter_rent, this.app, this.blockid, this.mLocation, this.radiusvalue, this.keywords, this.orderBy, this.infofromValue, this.infotypeValue, this.districtValue, this.streetValue, this.rent_priceValue, this.roomValue, this.hallValue, this.toiletValue, this.fitmentValue, this.rentTypeValue, this.busValue, this.metroValue, this.nodata_layout).execute(new Object[0]);
    }

    public void getMoreData() {
        this.refreshInfo_rent.refresh = false;
        new GetHouseListTask(this.context, this.listview_rent, this.refreshInfo_rent, this.adapter_rent, this.app, this.blockid, this.mLocation, this.radiusvalue, this.keywords, this.orderBy, this.infofromValue, this.infotypeValue, this.districtValue, this.streetValue, this.rent_priceValue, this.roomValue, this.hallValue, this.toiletValue, this.fitmentValue, this.rentTypeValue, this.busValue, this.metroValue, this.nodata_layout).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setMyLocation() {
        GeoPoint geopoint = BaiduMapUtil.getPoint(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude());
        this.myPositionOverlay.removeAll();
        this.myPositionOverlay.add(new ManagedOverlayItem(geopoint, "myposition", null));
        noLocationMap();
        this.overlayManager.populate();
    }

    private void noLocationMap() {
        double[] location = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(location[0], location[1]));
    }

    /* access modifiers changed from: private */
    public void relocation() {
        View loadView;
        if (this.mapChoose) {
            loadView = this.mapLoading;
        } else {
            loadView = this.loadingLayout;
        }
        if (!LocationTask.isdoing) {
            LocationTask location = new LocationTask(this.context, loadView);
            location.setTag(13);
            location.execute(new Object[0]);
            location.setLocationListener(new LocationTask.LocationListener() {
                public void onFinish(Location location, boolean sameCity) {
                    SecondRentResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    SecondRentResultActivity.this.mLocation = location;
                    if (SecondRentResultActivity.this.mLocation == null || SecondRentResultActivity.this.mLocation.getExtras() == null || SecondRentResultActivity.this.mLocation.getExtras().get("addr") == null) {
                        SecondRentResultActivity.this.location_addr.setText((int) R.string.text_changecity_location_failure);
                    } else {
                        TextUtil.setNullText(SecondRentResultActivity.this.mLocation.getExtras().get("addr").toString(), SecondRentResultActivity.this.location_addr);
                        SecondRentResultActivity.this.adapter_rent.setLocation(location);
                        SecondRentResultActivity.this.adapter_rent.setFrom_nearby(SecondRentResultActivity.this.from_nearby);
                    }
                    if (sameCity) {
                        SecondRentResultActivity.this.refreshData();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.infofromValue = this.bundle.getInt("infofromValue");
        this.infotypeValue = this.bundle.getInt("infotypeValue");
        this.districtValue = this.bundle.getInt("districtValue");
        this.streetValue = this.bundle.getInt("streetValue");
        this.rent_priceValue = this.bundle.getInt("rent_priceValue");
        this.fitmentValue = this.bundle.getInt("fitmentValue");
        this.rentTypeValue = this.bundle.getInt("renttypeValue");
        this.busValue = this.bundle.getInt("bus_id");
        this.metroValue = this.bundle.getString(HouseBaseInfo.METRO);
        this.roomValue = this.bundle.getInt("roomValue");
        this.blockid = getIntent().getStringExtra("blockid");
        String b_name = getIntent().getStringExtra("b_name");
        if (!TextUtils.isEmpty(b_name)) {
            this.head_view.setTvTitleText(b_name);
            this.map_list_change.setVisibility(8);
        }
        if (this.baseInfo == null) {
            getConfig(R.string.loading);
        }
        this.listview_rent.setAdapter(this.adapter_rent);
        this.adapter_rent.notifyDataSetChanged();
        this.from_nearby = getIntent().getBooleanExtra(INTENT_FROM_NEARBY, false);
        if (this.from_nearby) {
            relocation();
            this.text_region.setText("2.5km");
            this.text_region.setTag("0:2.5km");
            this.nearby_refresh_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SecondRentResultActivity.this.click_nearby_refresh = true;
                    if (!DeviceUtil.isOpenLoaction(SecondRentResultActivity.this.context)) {
                        AppMethods.showLocationSets(SecondRentResultActivity.this.context);
                    } else {
                        SecondRentResultActivity.this.relocation();
                    }
                }
            });
        } else {
            this.text_region.setText((int) R.string.text_region_title);
            this.nearby_refresh_layout.setVisibility(8);
            if (AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity())) {
                this.sort_layout.setVisibility(8);
            } else {
                this.sort_layout.setVisibility(0);
            }
            refreshData();
        }
        this.listview_rent.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                SecondRentResultActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                SecondRentResultActivity.this.getMoreData();
            }
        });
        this.listview_rent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(SecondRentResultActivity.this, SecondRentDetailActivity.class);
                intent.putExtra("id", ((SecondHouse) SecondRentResultActivity.this.adapter_rent.getItem(position)).getId());
                intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                SecondRentResultActivity.this.startActivity(intent);
            }
        });
    }

    private void addNormalSearchListener(View layout, TextView showDataView, ArrayList dataList, int choseType) {
        final View view = layout;
        final TextView textView = showDataView;
        final ArrayList arrayList = dataList;
        final int i = choseType;
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentResultActivity.this.searchPop.setShowGroup(false);
                SearchConditionPopView.refreshViews(SecondRentResultActivity.this.views, SecondRentResultActivity.this.textViews, view, textView, SecondRentResultActivity.this.context);
                SecondRentResultActivity.this.searchPop.setGradeData(arrayList, textView, SearchConditionPopView.getTagData(textView.getTag()), i);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initSearchItem() {
        this.regionMap = this.baseInfo.getDistrictStreet();
        this.subwayMap = this.baseInfo.getMetro();
        this.resourceList = this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM);
        this.priceDefalutList = this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT);
        this.priceOfficeList = this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_OFFICE);
        this.priceStoreList = this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_STORE);
        String text_subway = this.bundle.getString("text_subway");
        if (this.metroValue != null && !TextUtils.isEmpty(text_subway)) {
            this.text_region.setText(text_subway);
            this.text_region.setTag("2:" + text_subway);
        }
        String regionStr = this.bundle.getString("text_region");
        if (!TextUtils.isEmpty(regionStr) && !regionStr.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.text_region.setText(regionStr);
            this.text_region.setTag("1:" + regionStr);
        }
        if (this.infofromValue != 0) {
            String resourceStr = this.resourceList.get(this.infofromValue);
            this.text_resource.setText(resourceStr);
            this.text_resource.setTag("8:" + resourceStr);
        }
        if (this.infotypeValue == 3) {
            if (this.rent_priceValue != 0) {
                String priceStr = this.priceOfficeList.get(this.rent_priceValue);
                this.text_price.setText(priceStr);
                this.text_price.setTag("9:" + priceStr);
            }
            addNormalSearchListener(this.price_layout, this.text_price, this.priceOfficeList, 9);
        } else if (this.infotypeValue == 3) {
            if (this.rent_priceValue != 0) {
                String priceStr2 = this.priceStoreList.get(this.rent_priceValue);
                this.text_price.setText(priceStr2);
                this.text_price.setTag("9:" + priceStr2);
            }
            addNormalSearchListener(this.price_layout, this.text_price, this.priceStoreList, 9);
        } else {
            if (this.rent_priceValue != 0) {
                String priceStr3 = this.priceDefalutList.get(this.rent_priceValue);
                this.text_price.setText(priceStr3);
                this.text_price.setTag("9:" + priceStr3);
            }
            addNormalSearchListener(this.price_layout, this.text_price, this.priceDefalutList, 9);
        }
        if (this.from_nearby) {
            addNormalSearchListener(this.region_layout, this.text_region, this.radiousList, 10);
        } else {
            this.region_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SearchConditionPopView.refreshViews(SecondRentResultActivity.this.views, SecondRentResultActivity.this.textViews, SecondRentResultActivity.this.region_layout, SecondRentResultActivity.this.text_region, SecondRentResultActivity.this.context);
                    SecondRentResultActivity.this.searchPop.setShowDataView(SecondRentResultActivity.this.text_region);
                    SecondRentResultActivity.this.searchPop.setRegionOrSubway(SecondRentResultActivity.this.text_region);
                    if (SecondRentResultActivity.this.subwayMap == null || SecondRentResultActivity.this.subwayMap.isEmpty()) {
                        SecondRentResultActivity.this.searchPop.setShowGroup(false);
                        SecondRentResultActivity.this.searchPop.setGradeData(SecondRentResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag()), SecondRentResultActivity.this.regionMap, 1);
                        return;
                    }
                    SecondRentResultActivity.this.searchPop.setShowGroup(true);
                    if (SecondRentResultActivity.this.checkedRegion) {
                        SecondRentResultActivity.this.searchPop.setGradeData(SecondRentResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag()), SecondRentResultActivity.this.regionMap, 1);
                    } else {
                        SecondRentResultActivity.this.searchPop.setGradeData(SecondRentResultActivity.this.subwayMap, SecondRentResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondRentResultActivity.this.text_region.getTag()), 2);
                    }
                    SecondRentResultActivity.this.condition_group.setVisibility(0);
                }
            });
        }
        addNormalSearchListener(this.resource_layout, this.text_resource, this.resourceList, 8);
        this.text_sort.setTag("0:" + this.orders[0]);
        addNormalSearchListener(this.sort_layout, this.text_sort, this.sortList, 0);
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid, 2);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info != null) {
                    SecondRentResultActivity.this.baseInfo = info;
                    SecondRentResultActivity.this.initSearchItem();
                }
            }

            public void OnError(HouseBaseInfo info) {
                Toast.makeText(SecondRentResultActivity.this.context, (int) R.string.text_city_config_error, 1).show();
            }
        });
        getConfig.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        updataFinish();
        return false;
    }
}
