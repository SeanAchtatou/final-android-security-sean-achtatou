package com.house365.newhouse.ui.search;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.qrcode.CaptureActivity;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.search.KeyWordSearch;
import com.house365.newhouse.ui.secondrent.SecondRentResultActivity;
import com.house365.newhouse.ui.secondsell.AddBlockActivity;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

public class SecondRentSearchActivity extends BaseCommonActivity implements KeyWordSearch.DialogDimissListener {
    public static final String INTENT_BUS_NAME = "bus_name";
    public static final String INTENT_BUS_VALUE = "bus_value";
    public static final String INTENT_FORM_TYPE_CHOOSE = "litterChoose";
    public static final String INTENT_FROM_LITTERHOME = "litterHome";
    private static final int NORMAL_SEARCH_BACK = 1;
    public static final int REFRESH_BACK = 11;
    protected HouseBaseInfo baseInfo;
    private View black_alpha_view;
    private Button btn_capture;
    private Button btn_finish;
    /* access modifiers changed from: private */
    public int bus_value;
    private Button cancelDialog;
    private BroadcastReceiver chosedFinished = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String chose_from = intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
            int chose = intent.getIntExtra(SearchConditionPopView.INTENT_FROM_CONDITION, 0);
            if (chose_from.equals(ActionCode.RENT_SEARCH)) {
                SecondRentSearchActivity.this.refreshUI(chose);
            }
        }
    };
    private Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    /* access modifiers changed from: private */
    public int fitmentValue;
    private boolean fromLitterHome;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public int infofromValue;
    /* access modifiers changed from: private */
    public int infotypeValue;
    private KeyWordSearch keyWordSearch;
    private ListView key_list;
    /* access modifiers changed from: private */
    public EditText keyword_text;
    /* access modifiers changed from: private */
    public String metroValue;
    private LinearLayout nodata_layout;
    private LinearLayout popLayout;
    public BroadcastReceiver refershUIChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (SecondRentSearchActivity.this.baseInfo != null && intent.getExtras() != null) {
                SecondRentSearchActivity.this.text_rent_bus.setText(intent.getExtras().getString(SecondRentSearchActivity.INTENT_BUS_NAME).toString());
                SecondRentSearchActivity.this.bus_value = intent.getExtras().getInt(SecondRentSearchActivity.INTENT_BUS_VALUE, 0);
                SecondRentSearchActivity.this.refreshUI(3);
            }
        }
    };
    private View region_layout;
    private View region_total_layout;
    private View rent_bus_layout;
    private View rent_bus_total_layout;
    private View rent_fitment_layout;
    /* access modifiers changed from: private */
    public int rent_priceValue;
    private View rent_price_layout;
    private View rent_total_layout;
    private View rent_type_layout;
    /* access modifiers changed from: private */
    public View rent_type_total_layout;
    /* access modifiers changed from: private */
    public int renttypeValue;
    /* access modifiers changed from: private */
    public int roomValue;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private View search_bar;
    /* access modifiers changed from: private */
    public View sell_rent_fitment_total_layout;
    private View sell_rent_resource_layout;
    private View sell_rent_room_layout;
    /* access modifiers changed from: private */
    public View sell_rent_room_total_layout;
    private View senior_search_close_layout;
    /* access modifiers changed from: private */
    public View senior_search_close_total_layout;
    private View senior_search_open_layout;
    /* access modifiers changed from: private */
    public View senior_search_open_total_layout;
    /* access modifiers changed from: private */
    public int streetValue;
    private View subway_layout;
    private View subway_total_layout;
    /* access modifiers changed from: private */
    public TextView text_region;
    private TextView text_rent;
    /* access modifiers changed from: private */
    public TextView text_rent_bus;
    private TextView text_rent_bus_title;
    private TextView text_rent_title;
    private TextView text_rent_type;
    private TextView text_rent_type_title;
    private TextView text_sell_rent_fitment;
    private TextView text_sell_rent_fitment_title;
    private TextView text_sell_rent_resource;
    private TextView text_sell_rent_resource_title;
    private TextView text_sell_rent_room;
    private TextView text_sell_rent_room_title;
    /* access modifiers changed from: private */
    public TextView text_subway;
    private TextView text_type;
    private TextView text_type_title;
    private View type_layout;
    /* access modifiers changed from: private */
    public View type_total_layout;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String subway;
        if (requestCode == 11 && resultCode == -1 && data != null && data.getExtras() != null) {
            Bundle refreshBundle = data.getExtras();
            int infofromIndex = refreshBundle.getInt("infofromValue");
            int priceIndex = refreshBundle.getInt("rent_priceValue");
            int type = SearchConditionPopView.getTagFlag(refreshBundle.getString("distsubway"));
            if (type == 1) {
                String disStreet = SearchConditionPopView.getTagData(refreshBundle.getString("distsubway"));
                if (disStreet != null && !disStreet.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(disStreet);
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
            } else if (type == 2 && (subway = SearchConditionPopView.getTagData(refreshBundle.getString("distsubway"))) != null && !subway.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                this.text_subway.setText(subway);
            }
            if (infofromIndex != 0) {
                this.text_sell_rent_resource.setText(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM).get(infofromIndex).toString());
            } else {
                this.text_sell_rent_resource.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            }
            if (this.infotypeValue == 3) {
                if (priceIndex != 0) {
                    this.text_rent.setText(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_OFFICE).get(priceIndex).toString());
                } else {
                    this.text_rent.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
            } else if (this.infotypeValue == 4) {
                if (priceIndex != 0) {
                    this.text_rent.setText(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_STORE).get(priceIndex).toString());
                } else {
                    this.text_rent.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
            } else if (priceIndex != 0) {
                this.text_rent.setText(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).get(priceIndex).toString());
            } else {
                this.text_rent.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshUI(int chose) {
        switch (chose) {
            case 1:
                if (!this.text_subway.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
                if (!this.text_rent_bus.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_rent_bus.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    this.bus_value = 0;
                    return;
                }
                return;
            case 2:
                if (!this.text_region.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
                if (!this.text_rent_bus.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_rent_bus.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    this.bus_value = 0;
                    return;
                }
                return;
            case 3:
                if (!this.text_region.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
                if (!this.text_subway.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        View contentView = LayoutInflater.from(this.context).inflate((int) R.layout.second_rent_search_layout, (ViewGroup) null);
        setContentView(contentView);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(true);
        this.searchPop.setShowGroup(false);
        this.searchPop.setParentView(contentView);
        this.searchPop.setSearch_type(ActionCode.RENT_SEARCH);
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
        registerReceiver(this.refershUIChangeReceiver, new IntentFilter(ActionCode.UI_CHANGE));
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.finish();
            }
        });
        this.search_bar = findViewById(R.id.search_bar_layout);
        this.keyword_text = (EditText) findViewById(R.id.keyword_text);
        this.keyword_text.setHint((int) R.string.text_second_search_hint);
        this.btn_capture = (Button) findViewById(R.id.btn_capture);
        this.region_layout = findViewById(R.id.region_layout);
        this.rent_price_layout = findViewById(R.id.rent_layout);
        this.rent_bus_layout = findViewById(R.id.rent_bus_layout);
        this.sell_rent_resource_layout = findViewById(R.id.sell_rent_resource_layout);
        this.subway_layout = findViewById(R.id.subway_layout);
        this.region_total_layout = findViewById(R.id.region_total_layout);
        this.rent_total_layout = findViewById(R.id.rent_total_layout);
        this.subway_total_layout = findViewById(R.id.subway_total_layout);
        this.rent_bus_total_layout = findViewById(R.id.rent_bus_total_layout);
        this.text_region = (TextView) findViewById(R.id.text_region);
        this.text_subway = (TextView) findViewById(R.id.text_subway);
        this.text_rent = (TextView) findViewById(R.id.text_rent);
        this.text_rent_bus = (TextView) findViewById(R.id.text_rent_bus);
        this.text_sell_rent_resource = (TextView) findViewById(R.id.text_sell_rent_resource);
        this.text_rent_title = (TextView) findViewById(R.id.text_rent_title);
        this.text_rent_bus_title = (TextView) findViewById(R.id.text_rent_title);
        this.text_sell_rent_resource_title = (TextView) findViewById(R.id.text_sell_rent_resource_title);
        this.sell_rent_room_layout = findViewById(R.id.sell_rent_room_layout);
        this.rent_type_layout = findViewById(R.id.rent_type_layout);
        this.rent_fitment_layout = findViewById(R.id.sell_rent_fitment_layout);
        this.type_layout = findViewById(R.id.type_layout);
        this.rent_type_total_layout = findViewById(R.id.rent_type_total_layout);
        this.sell_rent_fitment_total_layout = findViewById(R.id.sell_rent_fitment_total_layout);
        this.sell_rent_room_total_layout = findViewById(R.id.sell_rent_room_total_layout);
        this.type_total_layout = findViewById(R.id.type_total_layout);
        this.text_sell_rent_room = (TextView) findViewById(R.id.text_sell_rent_room);
        this.text_rent_type = (TextView) findViewById(R.id.text_rent_type);
        this.text_sell_rent_fitment = (TextView) findViewById(R.id.text_sell_rent_fitment);
        this.text_type = (TextView) findViewById(R.id.text_type);
        this.text_type_title = (TextView) findViewById(R.id.text_type_title);
        this.text_rent_type_title = (TextView) findViewById(R.id.text_rent_type_title);
        this.text_sell_rent_fitment_title = (TextView) findViewById(R.id.text_sell_rent_fitment_title);
        this.text_sell_rent_room_title = (TextView) findViewById(R.id.text_sell_rent_room_title);
        this.btn_finish = (Button) findViewById(R.id.btn_finish);
        this.senior_search_open_total_layout = findViewById(R.id.senior_search_open_total_layout);
        this.senior_search_close_total_layout = findViewById(R.id.senior_search_close_total_layout);
        this.senior_search_open_layout = findViewById(R.id.senior_search_open_layout);
        this.senior_search_close_layout = findViewById(R.id.senior_search_close_layout);
        setFromLitterHome();
        this.senior_search_open_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.rent_type_total_layout.setVisibility(0);
                SecondRentSearchActivity.this.sell_rent_fitment_total_layout.setVisibility(0);
                SecondRentSearchActivity.this.sell_rent_room_total_layout.setVisibility(0);
                SecondRentSearchActivity.this.type_total_layout.setVisibility(0);
                SecondRentSearchActivity.this.senior_search_open_total_layout.setVisibility(8);
                SecondRentSearchActivity.this.senior_search_close_total_layout.setVisibility(0);
            }
        });
        this.senior_search_close_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.rent_type_total_layout.setVisibility(8);
                SecondRentSearchActivity.this.sell_rent_fitment_total_layout.setVisibility(8);
                SecondRentSearchActivity.this.sell_rent_room_total_layout.setVisibility(8);
                SecondRentSearchActivity.this.type_total_layout.setVisibility(8);
                SecondRentSearchActivity.this.senior_search_open_total_layout.setVisibility(0);
                SecondRentSearchActivity.this.senior_search_close_total_layout.setVisibility(8);
            }
        });
        this.btn_capture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.startActivity(new Intent(SecondRentSearchActivity.this, CaptureActivity.class));
            }
        });
        View.OnClickListener keysearch = new View.OnClickListener() {
            public void onClick(View v) {
            }
        };
        this.search_bar.setOnClickListener(keysearch);
        this.keyword_text.setOnClickListener(keysearch);
        this.btn_finish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SecondRentSearchActivity.this, SecondRentResultActivity.class);
                Bundle bundle = new Bundle();
                if (SecondRentSearchActivity.this.baseInfo != null && !SecondRentSearchActivity.this.baseInfo.getJsonStr().equals(SecondRentSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondRentSearchActivity.this.getChooseValue();
                    bundle.putInt("infofromValue", SecondRentSearchActivity.this.infofromValue);
                    bundle.putInt("infotypeValue", SecondRentSearchActivity.this.infotypeValue);
                    bundle.putInt("roomValue", SecondRentSearchActivity.this.roomValue);
                    bundle.putInt("streetValue", SecondRentSearchActivity.this.streetValue);
                    bundle.putInt("districtValue", SecondRentSearchActivity.this.districtValue);
                    bundle.putInt("renttypeValue", SecondRentSearchActivity.this.renttypeValue);
                    bundle.putInt("fitmentValue", SecondRentSearchActivity.this.fitmentValue);
                    bundle.putInt("rent_priceValue", SecondRentSearchActivity.this.rent_priceValue);
                    bundle.putString(HouseBaseInfo.METRO, SecondRentSearchActivity.this.metroValue);
                    bundle.putString("text_subway", SecondRentSearchActivity.this.text_subway.getText().toString());
                    bundle.putString("text_region", SecondRentSearchActivity.this.text_region.getText().toString());
                    bundle.putInt("bus_id", SecondRentSearchActivity.this.bus_value);
                    intent.putExtra("priceView_from", StringUtils.EMPTY);
                    bundle.putString("keywords", SecondRentSearchActivity.this.keyword_text.getText().toString());
                    if (SecondRentSearchActivity.this.bus_value != 0) {
                        bundle.putInt("FROM_SUBWAY", -3);
                    }
                    if (SecondRentSearchActivity.this.metroValue != null && !SecondRentSearchActivity.this.metroValue.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        bundle.putInt("FROM_SUBWAY", -2);
                    }
                }
                intent.putExtras(bundle);
                SecondRentSearchActivity.this.startActivityForResult(intent, 11);
            }
        });
        prepareKeySearch();
    }

    private void setFromLitterHome() {
        this.fromLitterHome = getIntent().getBooleanExtra("litterHome", false);
        if (this.fromLitterHome) {
            this.head_view.setVisibility(0);
            this.head_view.getTv_center().setText((int) R.string.text_rent_house_title);
        } else {
            this.head_view.setVisibility(8);
        }
        prepareKeySearch();
    }

    private void prepareKeySearch() {
        this.cancelDialog = (Button) findViewById(R.id.btn_cancel);
        this.cancelDialog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.popDismiss();
            }
        });
        this.popLayout = (LinearLayout) findViewById(R.id.key_search_container);
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        this.key_list = (ListView) findViewById(R.id.key_list);
        this.keyWordSearch = new KeyWordSearch(this, this.keyword_text);
        this.keyWordSearch.setSearchType(ActionCode.RENT_SEARCH);
        this.keyWordSearch.setDismissListener(this);
        this.keyWordSearch.setKey_list(this.key_list);
        this.keyWordSearch.setNodata_layout(this.nodata_layout);
        this.keyWordSearch.preparedCreate();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getConfig(this, R.string.loading, 2);
    }

    /* access modifiers changed from: private */
    public void setListener() {
        this.region_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.searchPop.setGradeData(SecondRentSearchActivity.this.text_region, SecondRentSearchActivity.this.text_region.getText().toString(), SecondRentSearchActivity.this.baseInfo.getDistrictStreet(), 1);
            }
        });
        this.subway_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.searchPop.setGradeData(SecondRentSearchActivity.this.baseInfo.getMetro(), SecondRentSearchActivity.this.text_subway, SecondRentSearchActivity.this.text_subway.getText().toString(), 2);
            }
        });
        this.rent_bus_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SecondRentSearchActivity.this, SecondBusActivity.class);
                intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, ActionCode.RENT_SEARCH);
                intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, 1);
                SecondRentSearchActivity.this.startActivity(intent);
            }
        });
        if (AppMethods.mapKeyTolistSubWay(this.baseInfo.getMetro(), true) == null) {
            this.subway_total_layout.setVisibility(8);
        }
        if (!((NewHouseApplication) this.mApplication).getCityName().equals("南京")) {
            this.rent_bus_total_layout.setVisibility(8);
        }
        addNormalSearchListener(this.sell_rent_resource_layout, this.text_sell_rent_resource, this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM), 0);
        addNormalSearchListener(this.type_layout, this.text_type, this.baseInfo.getSell_config().get(HouseBaseInfo.INFOTYPE), 0);
        addNormalSearchListener(this.sell_rent_room_layout, this.text_sell_rent_room, this.baseInfo.getSell_config().get(HouseBaseInfo.ROOM), 0);
        addNormalSearchListener(this.rent_type_layout, this.text_rent_type, this.baseInfo.getSell_config().get(HouseBaseInfo.RENTTYPE), 0);
        addNormalSearchListener(this.rent_fitment_layout, this.text_sell_rent_fitment, this.baseInfo.getSell_config().get(HouseBaseInfo.FITMENT), 0);
        if (this.infotypeValue == 3) {
            addNormalSearchListener(this.rent_price_layout, this.text_rent, this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_OFFICE), 0);
        } else if (this.infofromValue == 4) {
            addNormalSearchListener(this.rent_price_layout, this.text_rent, this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_STORE), 0);
        } else {
            addNormalSearchListener(this.rent_price_layout, this.text_rent, this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT), 0);
        }
    }

    /* access modifiers changed from: private */
    public void getChooseValue() {
        this.infofromValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM), this.text_sell_rent_resource.getText().toString());
        this.infotypeValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOTYPE), this.text_type.getText().toString());
        this.roomValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.ROOM), this.text_sell_rent_room.getText().toString());
        this.renttypeValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.RENTTYPE), this.text_rent_type.getText().toString());
        this.fitmentValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.FITMENT), this.text_sell_rent_fitment.getText().toString());
        if (this.infotypeValue == 3) {
            this.rent_priceValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_OFFICE), this.text_rent.getText().toString());
        } else if (this.infotypeValue == 4) {
            this.rent_priceValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_STORE), this.text_rent.getText().toString());
        } else {
            this.rent_priceValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT), this.text_rent.getText().toString());
        }
        String subWay = this.text_subway.getText().toString();
        if (subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.metroValue = null;
        }
        if (!(subWay == null || subWay.indexOf("+") == -1)) {
            String metro = subWay.substring(0, subWay.indexOf("+"));
            this.metroValue = ((Station) this.baseInfo.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()))).getId();
        }
        String district_Street = this.text_region.getText().toString();
        if (district_Street.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.districtValue = 0;
            this.streetValue = 0;
        } else if (district_Street != null && district_Street.indexOf("+") != -1) {
            String district = district_Street.substring(0, district_Street.indexOf("+"));
            String street = district_Street.substring(district_Street.indexOf("+") + 1, district_Street.length());
            this.districtValue = SearchConditionPopView.getIndex(AppMethods.mapKeyTolistSubWay(this.baseInfo.getDistrictStreet(), true), district) + 1;
            this.streetValue = Integer.parseInt((String) this.baseInfo.getDistrictStreet().get(district).get(street));
        }
    }

    private void addNormalSearchListener(View layout, final TextView showDataView, final ArrayList dataList, final int choseType) {
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentSearchActivity.this.searchPop.setGradeData(dataList, showDataView, showDataView.getText().toString(), choseType);
            }
        });
    }

    private void getConfig(Context context2, int resid, int type) {
        GetConfigTask getConfig = new GetConfigTask(this, resid, 2);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info == null) {
                    return;
                }
                if (info.getJsonStr().equals(SecondRentSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondRentSearchActivity.this.setViewEnable(R.string.text_no_network);
                    return;
                }
                SecondRentSearchActivity.this.baseInfo = info;
                SecondRentSearchActivity.this.setListener();
            }

            public void OnError(HouseBaseInfo info) {
                SecondRentSearchActivity.this.setViewEnable(R.string.text_city_config_error);
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setViewEnable(int msg) {
        this.subway_total_layout.setVisibility(8);
        this.rent_bus_total_layout.setVisibility(8);
        AppMethods.setViewToastListener(this, this.region_layout, msg);
        AppMethods.setViewToastListener(this, this.rent_price_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_rent_resource_layout, msg);
        AppMethods.setViewToastListener(this, this.subway_layout, msg);
        AppMethods.setViewToastListener(this, this.rent_bus_layout, msg);
        AppMethods.setViewToastListener(this, this.subway_total_layout, msg);
        AppMethods.setViewToastListener(this, this.rent_bus_total_layout, msg);
        AppMethods.setViewToastListener(this, this.rent_type_layout, msg);
        AppMethods.setViewToastListener(this, this.rent_fitment_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_rent_room_layout, msg);
        AppMethods.setViewToastListener(this, this.type_layout, msg);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.baseInfo == null) {
            getConfig(this, 0, 2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.refershUIChangeReceiver);
        unregisterReceiver(this.chosedFinished);
        super.onDestroy();
    }

    public void popDismiss() {
        this.btn_capture.setVisibility(0);
        this.cancelDialog.setVisibility(8);
        this.popLayout.setVisibility(8);
    }

    public void popShow() {
        this.btn_capture.setVisibility(8);
        this.cancelDialog.setVisibility(0);
        this.popLayout.setVisibility(0);
    }
}
