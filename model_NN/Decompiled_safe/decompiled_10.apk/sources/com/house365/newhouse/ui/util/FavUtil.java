package com.house365.newhouse.ui.util;

import android.widget.ImageView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseInfo;

public class FavUtil {
    public static void fixFavTab(NewHouseApplication mApplication, ImageView imageView, HouseInfo houseInfo) {
        if (mApplication.hasFav(houseInfo, houseInfo.getType())) {
            imageView.setTag(1);
            imageView.setImageResource(R.drawable.tab_unfollow);
            return;
        }
        imageView.setTag(0);
        imageView.setImageResource(R.drawable.tab_follow);
    }
}
