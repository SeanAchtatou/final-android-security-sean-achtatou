package com.house365.newhouse.ui.privilege;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.EventNum;
import com.house365.newhouse.task.GetItemSaleTask;
import com.house365.newhouse.task.GetTotalSaleTask;
import com.house365.newhouse.ui.privilege.adapter.EventAdapter;
import java.util.List;

public class PrivilegeHomeActivity extends BaseListActivity {
    public static final String INTENT_ASSIGN_SHOW = "assignShow";
    public static final String INTENT_EVENTS = "events";
    public static String showType = App.Categroy.Event.TLF;
    private String assignShow;
    private RadioButton bt_bargain;
    private RadioButton bt_coupon;
    private RadioButton bt_cubehouse;
    /* access modifiers changed from: private */
    public EventAdapter eventAdapter;
    private List<Event> eventList;
    private HeadNavigateView head_view;
    private RefreshInfo listRefresh = new RefreshInfo();
    private PullToRefreshListView listView;
    private View nodata_layout;
    private RadioGroup tab_group;
    private TextView tv_nodata;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.information_home);
    }

    private void doTask() {
        new GetItemSaleTask(this, 0, this.listView, this.listRefresh, this.eventAdapter, showType, this.nodata_layout, this.tv_nodata).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.listRefresh.refresh = true;
        doTask();
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.listRefresh.refresh = false;
        doTask();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PrivilegeHomeActivity.this.finish();
            }
        });
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.listView = (PullToRefreshListView) findViewById(R.id.list);
        this.eventAdapter = new EventAdapter(this);
        this.listView.setAdapter(this.eventAdapter);
        this.tab_group = (RadioGroup) findViewById(R.id.tab_group);
        this.bt_cubehouse = (RadioButton) findViewById(R.id.bt_cubehouse);
        this.bt_bargain = (RadioButton) findViewById(R.id.bt_bargain);
        this.bt_coupon = (RadioButton) findViewById(R.id.bt_coupon);
        this.assignShow = getIntent().getStringExtra("assignShow");
        if (!TextUtils.isEmpty(this.assignShow)) {
            showType = this.assignShow;
        }
        this.tab_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                PrivilegeHomeActivity.this.eventAdapter.clear();
                PrivilegeHomeActivity.this.eventAdapter.notifyDataSetChanged();
                if (checkedId == R.id.bt_cubehouse) {
                    PrivilegeHomeActivity.showType = App.Categroy.Event.TLF;
                } else if (checkedId == R.id.bt_bargain) {
                    PrivilegeHomeActivity.showType = App.Categroy.Event.TJF;
                } else if (checkedId == R.id.bt_coupon) {
                    PrivilegeHomeActivity.showType = App.Categroy.Event.COUPON;
                }
                PrivilegeHomeActivity.this.refreshData();
            }
        });
    }

    /* access modifiers changed from: private */
    public void setShowChecked() {
        if (showType.equals(App.Categroy.Event.COUPON)) {
            this.bt_coupon.setChecked(true);
        }
        if (showType.equals(App.Categroy.Event.TLF)) {
            this.bt_cubehouse.setChecked(true);
        }
        if (showType.equals(App.Categroy.Event.TJF)) {
            this.bt_bargain.setChecked(true);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.eventList = (List) getIntent().getSerializableExtra(INTENT_EVENTS);
        if (this.eventList == null || this.eventList.size() <= 0) {
            this.tab_group.setVisibility(0);
            GetTotalSaleTask nums = new GetTotalSaleTask(this, this.nodata_layout);
            nums.setOnSuccessListener(new GetTotalSaleTask.GetTotalSaleNumOnSuccessListener() {
                public void OnSuccess(EventNum eventNum) {
                    if (eventNum != null) {
                        int max = Math.max(Math.max(eventNum.getTjf(), eventNum.getTlf()), eventNum.getCoupon());
                        if (max == eventNum.getTjf()) {
                            PrivilegeHomeActivity.showType = App.Categroy.Event.TJF;
                        } else if (max == eventNum.getCoupon()) {
                            PrivilegeHomeActivity.showType = App.Categroy.Event.COUPON;
                        } else {
                            PrivilegeHomeActivity.showType = App.Categroy.Event.TLF;
                        }
                        PrivilegeHomeActivity.this.setShowChecked();
                    }
                    PrivilegeHomeActivity.this.refreshData();
                }
            });
            nums.execute(new Object[0]);
            this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
                public void onHeaderRefresh() {
                    PrivilegeHomeActivity.this.refreshData();
                }

                public void onFooterRefresh() {
                    PrivilegeHomeActivity.this.getMoreData();
                }
            });
        } else {
            this.eventAdapter.addAll(this.eventList);
            this.eventAdapter.notifyDataSetChanged();
            this.tab_group.setVisibility(8);
            if (showType.equals(App.Categroy.Event.COUPON)) {
                this.head_view.setTvTitleText((int) R.string.text_coupon);
            }
            if (showType.equals(App.Categroy.Event.TLF)) {
                this.head_view.setTvTitleText((int) R.string.text_cubehouse);
            }
            if (showType.equals(App.Categroy.Event.TJF)) {
                this.head_view.setTvTitleText((int) R.string.text_bargain);
            }
        }
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent();
                Class<?> clz = null;
                if (PrivilegeHomeActivity.showType.equals(App.Categroy.Event.COUPON)) {
                    clz = CouponDetailsActivity.class;
                    intent.putExtra("id", ((Event) PrivilegeHomeActivity.this.eventAdapter.getItem(position)).getE_id());
                    intent.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.COUPON);
                }
                if (PrivilegeHomeActivity.showType.equals(App.Categroy.Event.TLF)) {
                    clz = CubeHouseRoomActivity.class;
                    intent.putExtra("e_id", ((Event) PrivilegeHomeActivity.this.eventAdapter.getItem(position)).getE_id());
                }
                if (PrivilegeHomeActivity.showType.equals(App.Categroy.Event.TJF)) {
                    clz = BargainlHouseActivity.class;
                    intent.putExtra("e_id", ((Event) PrivilegeHomeActivity.this.eventAdapter.getItem(position)).getE_id());
                }
                intent.setClass(PrivilegeHomeActivity.this, clz);
                PrivilegeHomeActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.eventAdapter != null) {
            this.eventAdapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
