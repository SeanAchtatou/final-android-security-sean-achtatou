package com.house365.newhouse.ui.secondsell;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;

public class SecondSell_B_otherActivity extends BaseCommonActivity {
    private HeadNavigateView head_view;
    String house_B_other;
    TextView house_title;
    private String title_name;
    TextView txt_house_B_other;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.secondsell_detail_houseremarklayout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSell_B_otherActivity.this.finish();
            }
        });
        this.txt_house_B_other = (TextView) findViewById(R.id.house_remark);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.title_name = getIntent().getStringExtra("title_name");
        this.house_B_other = getIntent().getStringExtra("txt_house_B_other");
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.txt_house_B_other.setText(this.house_B_other);
        this.house_title.setText((int) R.string.text_field_bundled);
        this.head_view.setTvTitleText(this.title_name);
        CorePreferences.DEBUG("********\\\\\\\\\\\\house_B_other||||||||*****" + this.house_B_other);
    }
}
