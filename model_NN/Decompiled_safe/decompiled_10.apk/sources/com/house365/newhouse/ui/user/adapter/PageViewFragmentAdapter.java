package com.house365.newhouse.ui.user.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import java.util.HashMap;
import java.util.Map;

public class PageViewFragmentAdapter extends FragmentPagerAdapter {
    private Activity activity;
    private int count;
    Map<Integer, GuidArrayListFragment> map = new HashMap();
    private ViewPager vp;

    public PageViewFragmentAdapter(FragmentManager fm, ViewPager vp2, Activity activity2, int count2) {
        super(fm);
        this.activity = activity2;
        this.vp = vp2;
        this.count = count2;
    }

    public int getCount() {
        return this.count;
    }

    public Fragment getItem(int arg0) {
        GuidArrayListFragment fragment = GuidArrayListFragment.newInstance(this.activity, this.vp, arg0);
        this.map.put(Integer.valueOf(arg0), fragment);
        return fragment;
    }

    public void animate(int position) {
        GuidArrayListFragment f = this.map.get(Integer.valueOf(position));
        if (f != null) {
            f.startAnim(position);
        }
    }
}
