package com.house365.newhouse.ui.news;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.anim.AnimBean;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.News;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.news.adapter.NewsAdapter;
import java.util.List;

public class NewsHomeActivity extends BaseListActivity {
    /* access modifiers changed from: private */
    public NewsAdapter adapter;
    /* access modifiers changed from: private */
    public RadioGroup bt_new_second_group;
    /* access modifiers changed from: private */
    public RadioButton bt_news_type1;
    /* access modifiers changed from: private */
    public RadioButton bt_news_type2;
    private RadioButton bt_second;
    /* access modifiers changed from: private */
    public int chanel = 1;
    private boolean end_load;
    private HeadNavigateView head_view;
    private View item_title_ico;
    /* access modifiers changed from: private */
    public PullToRefreshListView listView;
    private RadioGroup news_type_group;
    /* access modifiers changed from: private */
    public View nodata_layout;
    /* access modifiers changed from: private */
    public RefreshInfo refreshInfo;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.news_home);
    }

    /* access modifiers changed from: protected */
    public void clean() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsHomeActivity.this.finish();
            }
        });
        this.listView = (PullToRefreshListView) findViewById(R.id.listView);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.item_title_ico = findViewById(R.id.item_title_ico);
        this.news_type_group = (RadioGroup) findViewById(R.id.news_type_group);
        this.bt_new_second_group = (RadioGroup) findViewById(R.id.bt_new_second_group);
        this.bt_news_type1 = (RadioButton) findViewById(R.id.bt_news_type1);
        this.bt_news_type2 = (RadioButton) findViewById(R.id.bt_news_type2);
        this.bt_second = (RadioButton) findViewById(R.id.bt_second);
        String cityId = ((NewHouseApplication) this.mApplication).getCity();
        if (AppMethods.isVirtual(cityId)) {
            this.news_type_group.setVisibility(8);
            this.bt_new_second_group.setVisibility(8);
            this.head_view.setTvTitleText((int) R.string.text_information_title);
        } else if (AppMethods.isJustNewHouse(cityId)) {
            this.bt_new_second_group.setVisibility(8);
            this.head_view.setTvTitleText((int) R.string.text_information_title);
        }
        this.bt_new_second_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_new) {
                    NewsHomeActivity.this.bt_news_type1.setText((int) R.string.text_news_type_1);
                    NewsHomeActivity.this.bt_news_type2.setText((int) R.string.text_news_type_2);
                    if (!NewsHomeActivity.this.bt_news_type1.isChecked()) {
                        NewsHomeActivity.this.bt_news_type1.setChecked(true);
                        return;
                    }
                    NewsHomeActivity.this.chanel = 1;
                    NewsHomeActivity.this.refreshData();
                    return;
                }
                NewsHomeActivity.this.bt_news_type1.setText((int) R.string.text_news_type_3);
                NewsHomeActivity.this.bt_news_type2.setText((int) R.string.text_news_type_4);
                if (!NewsHomeActivity.this.bt_news_type1.isChecked()) {
                    NewsHomeActivity.this.bt_news_type1.setChecked(true);
                    return;
                }
                NewsHomeActivity.this.chanel = 3;
                NewsHomeActivity.this.refreshData();
            }
        });
        this.news_type_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_news_type1) {
                    if (NewsHomeActivity.this.bt_new_second_group.getCheckedRadioButtonId() == R.id.bt_new) {
                        NewsHomeActivity.this.chanel = 1;
                    } else {
                        NewsHomeActivity.this.chanel = 3;
                    }
                } else if (NewsHomeActivity.this.bt_new_second_group.getCheckedRadioButtonId() == R.id.bt_new) {
                    NewsHomeActivity.this.chanel = 2;
                } else {
                    NewsHomeActivity.this.chanel = 4;
                }
                NewsHomeActivity.this.refreshData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.refreshInfo = new RefreshInfo();
        this.adapter = new NewsAdapter(this);
        this.listView.setAdapter(this.adapter);
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onFooterRefresh() {
                NewsHomeActivity.this.getMoreData();
            }

            public void onHeaderRefresh() {
                NewsHomeActivity.this.refreshData();
            }
        });
        refreshData();
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.adapter.setChannel(this.chanel);
        this.adapter.clear();
        this.adapter.notifyDataSetChanged();
        this.refreshInfo.refresh = true;
        new GetNewsTask(this, this.listView, this.refreshInfo, this.adapter, this.chanel).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.adapter.setChannel(this.chanel);
        this.refreshInfo.refresh = false;
        new GetNewsTask(this, this.listView, this.refreshInfo, this.adapter, this.chanel).execute(new Object[0]);
    }

    class GetNewsTask extends BaseListAsyncTask<News> {
        private int chanel_type;

        public GetNewsTask(Context context, PullToRefreshListView listView, RefreshInfo listRefresh, BaseListAdapter adapter, int chanel_type2) {
            super(context, listView, listRefresh, adapter);
            this.chanel_type = chanel_type2;
        }

        public List<News> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNews(this.chanel_type, this.listRefresh.getPage());
        }

        /* access modifiers changed from: protected */
        public void onAfterRefresh(List<News> v) {
            NewsHomeActivity.this.adapter.clear();
            NewsHomeActivity.this.adapter.notifyDataSetChanged();
            if (this.chanel_type == NewsHomeActivity.this.chanel && NewsHomeActivity.this.refreshInfo != null && NewsHomeActivity.this.listView != null) {
                ViewUtil.onListDataComplete(this.context, v, NewsHomeActivity.this.refreshInfo, NewsHomeActivity.this.adapter, NewsHomeActivity.this.listView);
                if (v == null || v.size() <= 0) {
                    NewsHomeActivity.this.nodata_layout.setVisibility(0);
                    ((ImageView) NewsHomeActivity.this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
                    ((TextView) NewsHomeActivity.this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_news_nodata);
                    return;
                }
                NewsHomeActivity.this.nodata_layout.setVisibility(8);
            }
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            NewsHomeActivity.this.adapter.clear();
            NewsHomeActivity.this.adapter.notifyDataSetChanged();
            NewsHomeActivity.this.nodata_layout.setVisibility(0);
            ((ImageView) NewsHomeActivity.this.nodata_layout.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
            ((TextView) NewsHomeActivity.this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.adapter.notifyDataSetChanged();
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_right, R.anim.slide_fix);
    }

    public AnimBean getFinishAnim() {
        return new AnimBean(R.anim.slide_fix, R.anim.slide_out_bottom);
    }
}
