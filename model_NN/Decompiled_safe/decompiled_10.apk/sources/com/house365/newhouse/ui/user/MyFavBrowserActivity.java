package com.house365.newhouse.ui.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;
import com.house365.newhouse.ui.user.adapter.FavHistoryListAdapter;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class MyFavBrowserActivity extends BaseListActivity {
    public static String INTENT_DATATYPE = "dataType";
    /* access modifiers changed from: private */
    public FavHistoryListAdapter adapter;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int dataType;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public ListView listView;
    /* access modifiers changed from: private */
    public List<HouseInfo> newHouseInfo;
    /* access modifiers changed from: private */
    public LinearLayout nodata_layout;
    /* access modifiers changed from: private */
    public List<HouseInfo> rentHouseInfo;
    /* access modifiers changed from: private */
    public List<HouseInfo> sellHouseInfo;
    /* access modifiers changed from: private */
    public String showType = "house";
    private RadioGroup tab_group;
    private RadioButton tab_rent;
    private RadioButton tab_sell;
    /* access modifiers changed from: private */
    public TextView tv_nodata;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_common_layout);
        this.context = this;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.dataType = getIntent().getIntExtra(INTENT_DATATYPE, 0);
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        if (this.dataType == 1) {
            this.head_view.setTvTitleText((int) R.string.text_fav_record_title);
        } else if (this.dataType == 2) {
            this.head_view.setTvTitleText((int) R.string.text_browse_history);
        }
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyFavBrowserActivity.this.finish();
            }
        });
        this.head_view.getBtn_right().setVisibility(4);
        this.listView = getListView();
        this.adapter = new FavHistoryListAdapter(this);
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.tab_group = (RadioGroup) findViewById(R.id.tab_group);
        this.tab_sell = (RadioButton) findViewById(R.id.bt_tab_sell);
        this.tab_rent = (RadioButton) findViewById(R.id.bt_tab_rent);
        if (AppMethods.isJustNewHouse(((NewHouseApplication) this.mApplication).getCity())) {
            this.tab_group.setVisibility(8);
            this.tab_sell.setVisibility(8);
            this.tab_rent.setVisibility(8);
        }
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.tab_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                MyFavBrowserActivity.this.adapter.clear();
                MyFavBrowserActivity.this.adapter.notifyDataSetChanged();
                if (checkedId == R.id.bt_tab_new) {
                    MyFavBrowserActivity.this.showType = "house";
                    MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.newHouseInfo, MyFavBrowserActivity.this.dataType);
                } else if (checkedId == R.id.bt_tab_sell) {
                    MyFavBrowserActivity.this.showType = App.SELL;
                    MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.sellHouseInfo, MyFavBrowserActivity.this.dataType);
                } else if (checkedId == R.id.bt_tab_rent) {
                    MyFavBrowserActivity.this.showType = App.RENT;
                    MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.rentHouseInfo, MyFavBrowserActivity.this.dataType);
                }
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                HouseInfo info = (HouseInfo) MyFavBrowserActivity.this.adapter.getItem(position);
                Intent intent = new Intent();
                Class<?> cls = null;
                if (MyFavBrowserActivity.this.showType.equals("house")) {
                    cls = NewHouseDetailActivity.class;
                    intent.putExtra("h_id", info.getNewHouse().getH_id());
                    intent.putExtra(NewHouseDetailActivity.INTENT_OUTLINE_HOUSE, info.getNewHouse());
                }
                if (MyFavBrowserActivity.this.showType.equals(App.RENT)) {
                    cls = SecondRentDetailActivity.class;
                    intent.putExtra("id", info.getSellOrRent().getId());
                }
                if (MyFavBrowserActivity.this.showType.equals(App.SELL)) {
                    cls = SecondSellDetailActivity.class;
                    intent.putExtra("id", info.getSellOrRent().getId());
                }
                intent.setClass(MyFavBrowserActivity.this.context, cls);
                intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                MyFavBrowserActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                new AlertDialog.Builder(MyFavBrowserActivity.this.context).setItems(new String[]{"删除"}, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            MyFavBrowserActivity.this.delHistory(position);
                        }
                    }
                }).create().show();
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void delHistory(final int position) {
        new CommonAsyncTask<Boolean>(this.context) {
            public void onAfterDoInBackgroup(Boolean v) {
                if (v.booleanValue()) {
                    if (MyFavBrowserActivity.this.adapter.getCount() <= 0) {
                        MyFavBrowserActivity.this.listView.setVisibility(8);
                        MyFavBrowserActivity.this.nodata_layout.setVisibility(0);
                        if (MyFavBrowserActivity.this.dataType == 1) {
                            if (MyFavBrowserActivity.this.showType.equals("house")) {
                                MyFavBrowserActivity.this.tv_nodata.setText((int) R.string.text_newhouse_nofav_msg);
                            } else if (MyFavBrowserActivity.this.showType.equals(App.RENT) || MyFavBrowserActivity.this.showType.equals(App.SELL)) {
                                MyFavBrowserActivity.this.tv_nodata.setText((int) R.string.text_second_nofav_msg);
                            }
                        } else if (MyFavBrowserActivity.this.dataType == 2) {
                            MyFavBrowserActivity.this.tv_nodata.setText((int) R.string.text_nobrowser_msg);
                        }
                    }
                    MyFavBrowserActivity.this.adapter.notifyDataSetChanged();
                }
            }

            public Boolean onDoInBackgroup() {
                HouseInfo houseInfo = (HouseInfo) MyFavBrowserActivity.this.adapter.getItem(position);
                switch (MyFavBrowserActivity.this.dataType) {
                    case 1:
                        ((NewHouseApplication) this.mApplication).delFavHistory(houseInfo, MyFavBrowserActivity.this.showType);
                        MyFavBrowserActivity.this.adapter.remove(position);
                        break;
                    case 2:
                        ((NewHouseApplication) this.mApplication).delBrowseHistory(houseInfo, MyFavBrowserActivity.this.showType);
                        MyFavBrowserActivity.this.adapter.remove(position);
                        break;
                }
                if (MyFavBrowserActivity.this.showType.equals("house")) {
                    MyFavBrowserActivity.this.newHouseInfo.remove(position);
                }
                if (MyFavBrowserActivity.this.showType.equals(App.SELL)) {
                    MyFavBrowserActivity.this.sellHouseInfo.remove(position);
                }
                if (MyFavBrowserActivity.this.showType.equals(App.RENT)) {
                    MyFavBrowserActivity.this.rentHouseInfo.remove(position);
                }
                return true;
            }
        }.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new GetHistoryListTask(this, this.dataType).execute(new Object[0]);
    }

    private class GetHistoryListTask extends CommonAsyncTask<List<HouseInfo>> {
        private int type;

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<HouseInfo>) ((List) obj));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public GetHistoryListTask(Context context, int type2) {
            super(context);
            this.loadingresid = R.string.loading;
            this.type = type2;
            MyFavBrowserActivity.this.listView.setVisibility(0);
            MyFavBrowserActivity.this.nodata_layout.setVisibility(8);
        }

        public void onAfterDoInBackgroup(List<HouseInfo> list) {
            if (MyFavBrowserActivity.this.showType.equals("house")) {
                MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.newHouseInfo, this.type);
            }
            if (MyFavBrowserActivity.this.showType.equals(App.SELL)) {
                MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.sellHouseInfo, this.type);
            }
            if (MyFavBrowserActivity.this.showType.equals(App.RENT)) {
                MyFavBrowserActivity.this.refreshUI(MyFavBrowserActivity.this.rentHouseInfo, this.type);
            }
        }

        public List<HouseInfo> onDoInBackgroup() {
            try {
                if (this.type == 1) {
                    MyFavBrowserActivity.this.newHouseInfo = ((NewHouseApplication) this.mApplication).getFavHistoryWithCast("house");
                    MyFavBrowserActivity.this.sellHouseInfo = ((NewHouseApplication) this.mApplication).getFavHistoryWithCast(App.SELL);
                    MyFavBrowserActivity.this.rentHouseInfo = ((NewHouseApplication) this.mApplication).getFavHistoryWithCast(App.RENT);
                    return null;
                } else if (this.type != 2) {
                    return null;
                } else {
                    MyFavBrowserActivity.this.newHouseInfo = ((NewHouseApplication) this.mApplication).getBrowseHistoryWithCast("house");
                    MyFavBrowserActivity.this.sellHouseInfo = ((NewHouseApplication) this.mApplication).getBrowseHistoryWithCast(App.SELL);
                    MyFavBrowserActivity.this.rentHouseInfo = ((NewHouseApplication) this.mApplication).getBrowseHistoryWithCast(App.RENT);
                    return null;
                }
            } catch (ReflectException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshUI(List<HouseInfo> houselist, int type) {
        this.adapter.clear();
        this.adapter.notifyDataSetChanged();
        if (houselist == null || houselist.size() <= 0) {
            this.adapter.notifyDataSetChanged();
            this.listView.setVisibility(8);
            this.nodata_layout.setVisibility(0);
            if (type == 1) {
                if (this.showType.equals("house")) {
                    this.tv_nodata.setText((int) R.string.text_newhouse_nofav_msg);
                } else if (this.showType.equals(App.RENT) || this.showType.equals(App.SELL)) {
                    this.tv_nodata.setText((int) R.string.text_second_nofav_msg);
                }
            } else if (type == 2) {
                this.tv_nodata.setText((int) R.string.text_nobrowser_msg);
            }
        } else {
            this.adapter.addAll(houselist);
            this.adapter.notifyDataSetChanged();
            this.listView.setVisibility(0);
            this.nodata_layout.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
