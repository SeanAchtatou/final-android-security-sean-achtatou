package com.house365.newhouse.ui.privilege.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.util.TimeUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import java.util.Date;

public class EventAdapter extends BaseCacheListAdapter<Event> {
    private String activities_cutoff_time;
    private int apply_type;
    private LayoutInflater inflater;
    private String text_adapter_bargain_area;
    private String text_bargain_type;
    private String text_coupon_type;
    private String text_cube_type;
    private String text_pub_adapter_price_unit;
    private String text_pub_original_price;
    private String text_pub_price_unit;

    public EventAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
        this.activities_cutoff_time = context.getResources().getString(R.string.activities_cutoff_time);
        this.text_pub_original_price = context.getResources().getString(R.string.text_pub_original_price);
        this.text_pub_adapter_price_unit = context.getResources().getString(R.string.text_pub_adapter_price_unit);
        this.text_adapter_bargain_area = context.getResources().getString(R.string.text_adapter_bargain_area);
        this.text_pub_price_unit = context.getResources().getString(R.string.text_pub_price_unit);
        this.text_bargain_type = context.getResources().getString(R.string.text_bargain_type);
        this.text_cube_type = context.getResources().getString(R.string.text_cube_type);
        this.text_coupon_type = context.getResources().getString(R.string.text_coupon_type);
    }

    public int getApply_type() {
        return this.apply_type;
    }

    public void setApply_type(int apply_type2) {
        this.apply_type = apply_type2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_list_privilege, (ViewGroup) null);
            holder = new ViewHolder(null);
            holder.h_name = (TextView) convertView.findViewById(R.id.h_name);
            holder.h_pic = (ImageView) convertView.findViewById(R.id.h_pic);
            holder.h_price = (TextView) convertView.findViewById(R.id.h_price);
            holder.h_old_price = (TextView) convertView.findViewById(R.id.h_old_price);
            holder.h_type_ico = (TextView) convertView.findViewById(R.id.h_type_ico);
            holder.h_apply_date = (TextView) convertView.findViewById(R.id.h_apply_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Event event = (Event) getItem(position);
        setCacheImage(holder.h_pic, event.getE_pic(), R.drawable.bg_default_img_photo, 3);
        holder.h_name.setText(event.getE_title());
        if (new Date().getTime() / 1000 > event.getE_endtime()) {
            holder.h_type_ico.setBackgroundResource(R.drawable.bg_event_end);
        } else {
            holder.h_type_ico.setBackgroundResource(R.drawable.bg_type_ico);
        }
        holder.h_apply_date.setText(String.valueOf(this.activities_cutoff_time) + TimeUtil.toDateWithFormat(event.getE_endtime(), "yyyy年MM月dd日"));
        if (App.Categroy.Event.TJF.equals(event.getE_type())) {
            holder.h_type_ico.setText(this.text_bargain_type);
            String s_price = event.getE_s_price();
            if (!TextUtils.isEmpty(s_price)) {
                holder.h_price.setText(String.valueOf(s_price) + this.text_pub_adapter_price_unit);
            } else {
                holder.h_price.setText(" ");
            }
            String o_price = event.getE_o_price();
            if (!TextUtils.isEmpty(o_price)) {
                holder.h_old_price.setText(String.valueOf(this.text_pub_original_price) + o_price + this.text_pub_adapter_price_unit);
            } else {
                holder.h_old_price.setText(" ");
            }
            holder.h_old_price.setVisibility(0);
            holder.h_price.setVisibility(0);
        } else if (App.Categroy.Event.TLF.equals(event.getE_type())) {
            holder.h_type_ico.setText(this.text_cube_type);
            String s_price2 = event.getE_s_price();
            if (!TextUtils.isEmpty(s_price2)) {
                holder.h_price.setText(s_price2);
            } else {
                holder.h_price.setText(" ");
            }
        } else if (App.Categroy.Event.COUPON.equals(event.getE_type()) || "event".equals(event.getE_type())) {
            holder.h_type_ico.setText(this.text_coupon_type);
            holder.h_price.setVisibility(4);
            holder.h_old_price.setVisibility(4);
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView h_apply_date;
        TextView h_name;
        TextView h_old_price;
        ImageView h_pic;
        TextView h_price;
        TextView h_type_ico;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
