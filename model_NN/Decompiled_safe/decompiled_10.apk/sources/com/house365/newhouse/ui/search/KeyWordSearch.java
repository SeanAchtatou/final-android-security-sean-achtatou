package com.house365.newhouse.ui.search;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.KeyItemArray;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.newhome.NewHouseSearchResultActivity;
import com.house365.newhouse.ui.secondrent.SecondRentResultActivity;
import com.house365.newhouse.ui.secondsell.SecondSellResultActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class KeyWordSearch implements View.OnClickListener, PopupWindow.OnDismissListener {
    public static final int COMBINATION_SEARCH = 0;
    public static final int PART_SEARCH = 1;
    private static final String TAG = "KeyWordSearch";
    private SimpleAdapter adapter;
    /* access modifiers changed from: private */
    public EditText anchor;
    DefineArrayAdapter<String> array_adapter;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public DialogDimissListener dismissListener;
    /* access modifiers changed from: private */
    public ListView key_list;
    /* access modifiers changed from: private */
    public LinearLayout nodata_layout;
    /* access modifiers changed from: private */
    public String searchType;

    interface DialogDimissListener {
        void popDismiss();

        void popShow();
    }

    public KeyWordSearch(Context context2, View anchor2) {
        this.context = context2;
        this.anchor = (EditText) anchor2;
    }

    /* access modifiers changed from: protected */
    public void preparedCreate() {
        initData();
        onResume();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        if (this.searchType.equals(ActionCode.SELL_SEARCH) || this.searchType.equals(ActionCode.RENT_SEARCH)) {
            this.anchor.setHint((int) R.string.text_second_search_hint);
        }
        this.anchor.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String keys = KeyWordSearch.this.anchor.getText().toString().trim();
                if (keys == null || TextUtils.isEmpty(keys)) {
                    KeyWordSearch.this.dismissListener.popDismiss();
                    return;
                }
                KeyWordSearch.this.nodata_layout.setVisibility(8);
                KeyWordSearch.this.key_list.setVisibility(0);
                if (KeyWordSearch.this.searchType.equals(ActionCode.NEW_SEARCH)) {
                    new NewHouseSearchTask(KeyWordSearch.this.context).execute(new Object[0]);
                } else {
                    new SecondHouseSearchTask(KeyWordSearch.this.context).execute(new Object[0]);
                }
                KeyWordSearch.this.dismissListener.popShow();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.anchor.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 0 && actionId != 6 && actionId != 2 && actionId != 5 && actionId != 3) {
                    return false;
                }
                KeyWordSearch.this.searchListener();
                return false;
            }
        });
        this.key_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String h_id = ((TextView) view.findViewById(R.id.h_id)).getText().toString();
                String h_name = ((TextView) view.findViewById(R.id.h_name)).getText().toString();
                if (position == 0) {
                    KeyWordSearch.this.searchListener();
                } else if (h_id != null && !TextUtils.isEmpty(h_id)) {
                    ((NewHouseApplication) KeyWordSearch.this.context.getApplicationContext()).addKeySearchHistroy(new KeyItemArray(h_name), KeyWordSearch.this.searchType);
                    Intent intent = new Intent();
                    Class<?> clazz = null;
                    if (KeyWordSearch.this.searchType.equals(ActionCode.NEW_SEARCH)) {
                        intent.putExtra("h_id", h_id);
                        clazz = NewHouseDetailActivity.class;
                    } else if (KeyWordSearch.this.searchType.equals(ActionCode.SELL_SEARCH)) {
                        intent.putExtra("blockid", h_id);
                        intent.putExtra("b_name", h_name);
                        clazz = SecondSellResultActivity.class;
                    } else if (KeyWordSearch.this.searchType.equals(ActionCode.RENT_SEARCH)) {
                        intent.putExtra("blockid", h_id);
                        intent.putExtra("b_name", h_name);
                        clazz = SecondRentResultActivity.class;
                    }
                    intent.setClass(KeyWordSearch.this.context, clazz);
                    KeyWordSearch.this.context.startActivity(intent);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void searchListener() {
        String keys = this.anchor.getText().toString().trim();
        if (keys != null && !TextUtils.isEmpty(keys)) {
            ((NewHouseApplication) this.context.getApplicationContext()).addKeySearchHistroy(new KeyItemArray(keys), this.searchType);
        }
        Intent intent = new Intent();
        Class<?> cls = null;
        intent.putExtra("keywords", keys);
        if (this.searchType.equals(ActionCode.NEW_SEARCH)) {
            cls = NewHouseSearchResultActivity.class;
        } else if (this.searchType.equals(ActionCode.SELL_SEARCH)) {
            cls = SecondSellResultActivity.class;
            intent.putExtra("priceView_from", "0");
        } else if (this.searchType.equals(ActionCode.RENT_SEARCH)) {
            cls = SecondRentResultActivity.class;
            intent.putExtra("priceView_from", "0");
        }
        intent.setClass(this.context, cls);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.anchor.setText(StringUtils.EMPTY);
    }

    /* access modifiers changed from: private */
    public void setNoKeyData() {
        if (this.searchType.equals(ActionCode.SELL_SEARCH) || this.searchType.equals(ActionCode.RENT_SEARCH)) {
            this.array_adapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, new String[]{this.context.getResources().getString(R.string.text_block_nodata)});
        } else {
            this.array_adapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, new String[]{this.context.getResources().getString(R.string.text_house_nodata)});
        }
        this.nodata_layout.setVisibility(8);
        this.array_adapter.notifyDataSetChanged();
        this.key_list.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public void setNoNetWork() {
        this.array_adapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, new String[]{this.context.getResources().getString(R.string.text_keysearch_nodata)});
        this.array_adapter.notifyDataSetChanged();
        this.key_list.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public void setKeyData(List<HashMap<String, String>> search_data) {
        HashMap<String, String> map = new HashMap<>();
        map.put("h_id", "0");
        map.put("h_name", String.valueOf(this.context.getString(R.string.text_search_title)) + "'" + this.anchor.getText().toString().trim() + "'");
        search_data.add(0, map);
        List<HashMap<String, String>> list = search_data;
        this.adapter = new SimpleAdapter(this.context, list, R.layout.search_keyword, new String[]{"h_id", "h_name"}, new int[]{R.id.h_id, R.id.h_name});
        this.key_list.setAdapter((ListAdapter) this.adapter);
    }

    private class NewHouseSearchTask extends CommonAsyncTask<List<House>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<House>) ((List) obj));
        }

        public NewHouseSearchTask(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<House> houseList) {
            if (houseList == null || houseList.size() <= 0) {
                KeyWordSearch.this.setNoKeyData();
                return;
            }
            List<HashMap<String, String>> search_data = new ArrayList<>();
            for (int i = 0; i < houseList.size(); i++) {
                HashMap<String, String> item = new HashMap<>();
                item.put("h_id", houseList.get(i).getH_id());
                item.put("h_name", houseList.get(i).getH_name());
                search_data.add(item);
            }
            KeyWordSearch.this.setKeyData(search_data);
        }

        public List<House> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewHouseListBySearch(KeyWordSearch.this.anchor.getText().toString());
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            KeyWordSearch.this.setNoNetWork();
        }
    }

    private class SecondHouseSearchTask extends CommonAsyncTask<List<Block>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Block>) ((List) obj));
        }

        public SecondHouseSearchTask(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<Block> bockList) {
            if (bockList == null || bockList.size() <= 0) {
                KeyWordSearch.this.setNoKeyData();
                return;
            }
            List<HashMap<String, String>> search_data = new ArrayList<>();
            for (int i = 0; i < bockList.size(); i++) {
                HashMap<String, String> item = new HashMap<>();
                Block block_item = bockList.get(i);
                item.put("h_id", block_item.getId());
                item.put("h_name", block_item.getBlockname());
                search_data.add(item);
            }
            KeyWordSearch.this.setKeyData(search_data);
        }

        public List<Block> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSecondHouseListBySearch(KeyWordSearch.this.anchor.getText().toString());
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            KeyWordSearch.this.setNoNetWork();
        }
    }

    public void onClick(View v) {
        v.getId();
    }

    public void onDismiss() {
        this.dismissListener.popDismiss();
    }

    public String getSearchType() {
        return this.searchType;
    }

    public void setSearchType(String searchType2) {
        this.searchType = searchType2;
    }

    public void setDismissListener(DialogDimissListener dismissListener2) {
        this.dismissListener = dismissListener2;
    }

    public LinearLayout getNodata_layout() {
        return this.nodata_layout;
    }

    public void setNodata_layout(LinearLayout nodata_layout2) {
        this.nodata_layout = nodata_layout2;
    }

    public ListView getKey_list() {
        return this.key_list;
    }

    public void setKey_list(ListView key_list2) {
        this.key_list = key_list2;
    }
}
