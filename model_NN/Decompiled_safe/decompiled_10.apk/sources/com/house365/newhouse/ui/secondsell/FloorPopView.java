package com.house365.newhouse.ui.secondsell;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.view.wheel.widget.OnWheelScrollListener;
import com.house365.core.view.wheel.widget.WheelView;
import com.house365.core.view.wheel.widget.adapters.ArrayWheelAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.HouseBaseInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class FloorPopView {
    private List<ArrayWheelAdapter> adapters = new ArrayList();
    /* access modifiers changed from: private */
    public List<String[]> attrDatas = new ArrayList();
    private Button btn_cancle;
    private Button btn_submit;
    /* access modifiers changed from: private */
    public boolean choose_single;
    private View contentView;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public boolean dismiss;
    /* access modifiers changed from: private */
    public String floorStr;
    /* access modifiers changed from: private */
    public WheelView floor_primary_wheel;
    /* access modifiers changed from: private */
    public WheelView floor_secondary_wheel;
    /* access modifiers changed from: private */
    public WheelView floor_total_wheel;
    private int height;
    /* access modifiers changed from: private */
    public TextView msgView;
    /* access modifiers changed from: private */
    public PopupWindow popWheel;
    private int[] resTitle;
    /* access modifiers changed from: private */
    public HashMap<String, String> resultData = new HashMap<>();
    private View secondary_layout;
    private String[] titles;
    private TextView tv_floor_secondary;
    /* access modifiers changed from: private */
    public int type;
    private TextView wheel_first_title;
    private TextView wheel_third_title;
    private List<WheelView> wheels = new ArrayList();
    private int width;

    public FloorPopView(Context context2, PopupWindow popWheel2, int width2, int height2) {
        this.context = context2;
        this.popWheel = popWheel2;
        this.width = width2;
        this.height = height2;
        this.contentView = LayoutInflater.from(context2).inflate((int) R.layout.floor_wheel_pop, (ViewGroup) null);
        this.btn_submit = (Button) this.contentView.findViewById(R.id.btn_submit);
        this.btn_cancle = (Button) this.contentView.findViewById(R.id.btn_cancle);
        this.secondary_layout = this.contentView.findViewById(R.id.secondary_layout);
        this.floor_primary_wheel = (WheelView) this.contentView.findViewById(R.id.floor_primary_wheel);
        this.floor_secondary_wheel = (WheelView) this.contentView.findViewById(R.id.floor_secondary_wheel);
        this.floor_total_wheel = (WheelView) this.contentView.findViewById(R.id.floor_total_wheel);
        this.wheel_first_title = (TextView) this.contentView.findViewById(R.id.wheel_first_title);
        this.wheel_third_title = (TextView) this.contentView.findViewById(R.id.wheel_third_title);
        this.tv_floor_secondary = (TextView) this.contentView.findViewById(R.id.tv_floor_secondary);
    }

    public HashMap<String, String> getResultData() {
        return this.resultData;
    }

    public void setResultData(HashMap<String, String> resultData2) {
        this.resultData = resultData2;
    }

    public String[] getTitles() {
        return this.titles;
    }

    public void setTitles(String[] titles2) {
        this.titles = titles2;
    }

    public WheelView getFloor_secondary_wheel() {
        return this.floor_secondary_wheel;
    }

    public void setFloor_secondary_wheel(WheelView floor_secondary_wheel2) {
        this.floor_secondary_wheel = floor_secondary_wheel2;
    }

    public TextView getTv_floor_secondary() {
        return this.tv_floor_secondary;
    }

    public void setTv_floor_secondary(TextView tv_floor_secondary2) {
        this.tv_floor_secondary = tv_floor_secondary2;
    }

    public boolean isChoose_single() {
        return this.choose_single;
    }

    public void setChoose_single(boolean choose_single2) {
        this.choose_single = choose_single2;
    }

    public boolean isDismiss() {
        return this.dismiss;
    }

    public void setDismiss(boolean dismiss2) {
        this.dismiss = dismiss2;
    }

    public String getFloorStr() {
        return this.floorStr;
    }

    public void setFloorStr(String floorStr2) {
        this.floorStr = floorStr2;
    }

    public View getSecondary_layout() {
        return this.secondary_layout;
    }

    public void setSecondary_layout(View secondary_layout2) {
        this.secondary_layout = secondary_layout2;
    }

    public int[] getResTitle() {
        return this.resTitle;
    }

    public void setResTitle(int[] resTitle2) {
        this.resTitle = resTitle2;
    }

    public TextView getMsgView() {
        return this.msgView;
    }

    public void setMsgView(TextView msgView2) {
        this.msgView = msgView2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    private void initView() {
        for (int i = 0; i < this.wheels.size(); i++) {
            WheelView item_wheel = this.wheels.get(i);
            item_wheel.setIs_shadow(false);
            item_wheel.setWheel_val_id(R.drawable.wheel_val_blue);
            item_wheel.setWheel_bg_id(0);
            item_wheel.setCurrentItem(0);
            item_wheel.setVisibleItems(3);
            item_wheel.setCyclic(true);
            ArrayWheelAdapter item_adapter = this.adapters.get(i);
            item_adapter.setItemResource(R.layout.wheel_item);
            item_adapter.setItemTextResource(R.id.tv_wheel_item);
            item_adapter.setCurrentItem(0);
            item_adapter.setCurrentStyle(R.style.font18_black);
            item_adapter.setNormalStyle(R.style.normal_text16);
            item_wheel.setViewAdapter(item_adapter);
        }
    }

    public void makePopWheel() {
        this.attrDatas.clear();
        String[] roomdata = null;
        if (this.type == 1) {
            roomdata = new String[20];
            for (int i = 0; i < 20; i++) {
                roomdata[i] = new StringBuilder(String.valueOf(i + 1)).toString();
            }
        } else if (this.type == 2) {
            roomdata = new String[99];
            for (int i2 = 0; i2 < 99; i2++) {
                roomdata[i2] = new StringBuilder(String.valueOf(i2 + 1)).toString();
            }
        }
        for (int i3 = 0; i3 < 3; i3++) {
            this.attrDatas.add(roomdata);
        }
        this.wheel_first_title.setText(this.resTitle[0]);
        this.tv_floor_secondary.setText(this.resTitle[1]);
        this.wheel_third_title.setText(this.resTitle[2]);
        ArrayWheelAdapter primaryAdapter = new ArrayWheelAdapter(this.context, this.attrDatas.get(0));
        ArrayWheelAdapter secondaryAdapter = new ArrayWheelAdapter(this.context, this.attrDatas.get(1));
        ArrayWheelAdapter totalAdapter = new ArrayWheelAdapter(this.context, this.attrDatas.get(2));
        this.wheels.add(this.floor_primary_wheel);
        this.wheels.add(this.floor_secondary_wheel);
        this.wheels.add(this.floor_total_wheel);
        this.adapters.add(primaryAdapter);
        this.adapters.add(secondaryAdapter);
        this.adapters.add(totalAdapter);
        initView();
        OnWheelScrollListener scrollListener = new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) {
                ((ArrayWheelAdapter) wheel.getViewAdapter()).setCurrentStyle(0);
            }

            public void onScrollingFinished(WheelView wheel) {
                int index = wheel.getCurrentItem();
                ArrayWheelAdapter<String> adapter = (ArrayWheelAdapter) wheel.getViewAdapter();
                adapter.setCurrentItem(index);
                adapter.setCurrentStyle(R.style.font18_black);
                adapter.setNormalStyle(R.style.normal_text16);
            }
        };
        this.floor_primary_wheel.addScrollingListener(scrollListener);
        this.floor_secondary_wheel.addScrollingListener(scrollListener);
        this.floor_total_wheel.addScrollingListener(scrollListener);
        this.btn_submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int primary = Integer.parseInt(((String[]) FloorPopView.this.attrDatas.get(0))[FloorPopView.this.floor_primary_wheel.getCurrentItem()]);
                int secondary = Integer.parseInt(((String[]) FloorPopView.this.attrDatas.get(1))[FloorPopView.this.floor_secondary_wheel.getCurrentItem()]);
                int total = Integer.parseInt(((String[]) FloorPopView.this.attrDatas.get(2))[FloorPopView.this.floor_total_wheel.getCurrentItem()]);
                if (FloorPopView.this.type == 1) {
                    FloorPopView.this.resultData.put(HouseBaseInfo.ROOM, new StringBuilder(String.valueOf(primary)).toString());
                    FloorPopView.this.resultData.put("hall", new StringBuilder(String.valueOf(secondary)).toString());
                    FloorPopView.this.resultData.put("toilet", new StringBuilder(String.valueOf(total)).toString());
                    FloorPopView.this.msgView.setText(String.valueOf(primary) + "室" + secondary + "厅" + total + "卫");
                    FloorPopView.this.popWheel.dismiss();
                } else if (FloorPopView.this.type != 2) {
                } else {
                    if (FloorPopView.this.choose_single) {
                        FloorPopView.this.resultData.put("floor", new StringBuilder(String.valueOf(primary)).toString());
                        FloorPopView.this.resultData.put("subfloor", StringUtils.EMPTY);
                        FloorPopView.this.resultData.put("totalfloor", new StringBuilder(String.valueOf(total)).toString());
                        if (total < primary) {
                            Toast.makeText(FloorPopView.this.context, "当前楼层不得大于总楼层", 0).show();
                            return;
                        }
                        FloorPopView.this.floorStr = "第" + primary + "层，" + "共" + total + "层";
                        FloorPopView.this.msgView.setText(FloorPopView.this.floorStr);
                        FloorPopView.this.popWheel.dismiss();
                        return;
                    }
                    FloorPopView.this.resultData.put("subfloor", new StringBuilder(String.valueOf(primary)).toString());
                    FloorPopView.this.resultData.put("floor", new StringBuilder(String.valueOf(secondary)).toString());
                    FloorPopView.this.resultData.put("totalfloor", new StringBuilder(String.valueOf(total)).toString());
                    if (total < secondary) {
                        Toast.makeText(FloorPopView.this.context, "输入楼层必须小于总楼层", 0).show();
                    } else if (secondary <= primary) {
                        Toast.makeText(FloorPopView.this.context, "当前楼层不得大于总楼层", 0).show();
                    } else {
                        FloorPopView.this.floorStr = "第" + primary + "层" + "-" + secondary + "层，" + "共" + total + "层";
                        FloorPopView.this.msgView.setText(FloorPopView.this.floorStr);
                        FloorPopView.this.popWheel.dismiss();
                    }
                }
            }
        });
        this.btn_cancle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FloorPopView.this.popWheel.dismiss();
                FloorPopView.this.msgView.setText(StringUtils.EMPTY);
            }
        });
        this.popWheel.setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                FloorPopView.this.dismiss = true;
            }
        });
        this.popWheel.setAnimationStyle(R.style.AnimationFade);
        this.popWheel.setContentView(this.contentView);
        if (!(this.width == 0 || this.height == 0)) {
            this.popWheel.setWidth(this.width);
            this.popWheel.setHeight(this.height);
        }
        this.popWheel.setTouchable(true);
        this.popWheel.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_pop_wheel));
        this.popWheel.setOutsideTouchable(false);
    }
}
