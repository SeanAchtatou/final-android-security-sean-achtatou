package com.house365.newhouse.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.house365.core.view.LoadingDialog;
import com.house365.newhouse.R;

public class CustomProgressDialog extends LoadingDialog {
    Context context;
    ImageView fore_color;
    int resId;

    public CustomProgressDialog(Context context2) {
        super(context2);
        this.context = context2;
        setResLayout(R.layout.anim_dialog);
    }

    public CustomProgressDialog(Context context2, int theme) {
        super(context2, theme);
        this.context = context2;
        setResLayout(R.layout.anim_dialog);
    }

    public CustomProgressDialog(Context context2, int theme, int resid) {
        super(context2, theme, resid);
        this.resId = resid;
        setResLayout(R.layout.anim_dialog);
    }

    public int getResId() {
        return this.resId;
    }

    public void setResId(int resId2) {
        this.resId = resId2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.anim_dialog);
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = 0;
        params.y = 0;
        params.gravity = 17;
        window.setAttributes(params);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View back_color = findViewById(R.id.back_color);
        this.fore_color = (ImageView) findViewById(R.id.fore_color);
        back_color.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.progress_translate));
    }

    public void cancel() {
        dismiss();
        ((Activity) this.context).finish();
        super.cancel();
    }
}
