package com.house365.newhouse.ui.award;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.wheel.widget.OnWheelChangedListener;
import com.house365.core.view.wheel.widget.OnWheelScrollListener;
import com.house365.core.view.wheel.widget.WheelView;
import com.house365.core.view.wheel.widget.adapters.AbstractWheelAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.interfaces.TaskFinishListener;
import com.house365.newhouse.model.AwardInfo;
import com.house365.newhouse.model.AwardLog;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.task.AwardEventTask;
import com.house365.newhouse.task.AwardLuckTask;
import com.house365.newhouse.task.GetEventInfoTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.award.widget.AwardWheelView;
import com.house365.newhouse.ui.award.widget.adapters.AwardAdapter;
import com.house365.newhouse.ui.award.widget.adapters.TrentsAdapter;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class AwardActivity extends BaseCommonActivity implements View.OnClickListener, TaskFinishListener<AwardInfo> {
    /* access modifiers changed from: private */
    public AwardAdapter adapterHistory;
    private NewHouseApplication app;
    /* access modifiers changed from: private */
    public ImageButton awardButton;
    /* access modifiers changed from: private */
    public TextView awardChances;
    private ImageButton awardHelp;
    private int[] awardSetting;
    /* access modifiers changed from: private */
    public int awardSize;
    private int[] awards = new int[3];
    private Button back;
    /* access modifiers changed from: private */
    public String chances;
    private OnWheelChangedListener changedListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (!AwardActivity.this.wheelScrolled) {
                AwardActivity.this.updateStatus();
            }
        }
    };
    private String content;
    /* access modifiers changed from: private */
    public ViewGroup cotentView;
    private Dialog dialog;
    /* access modifiers changed from: private */
    public String e_id;
    /* access modifiers changed from: private */
    public Event event;
    /* access modifiers changed from: private */
    public String helpText;
    private TextView helpTextView;
    /* access modifiers changed from: private */
    public ListView historyListView;
    private TaofangImageLoader imageLoader;
    /* access modifiers changed from: private */
    public List<Bitmap> images = new ArrayList();
    private boolean isFirstLoad = true;
    /* access modifiers changed from: private */
    public boolean isFromAd;
    private boolean isWin;
    /* access modifiers changed from: private */
    public Map<String, String> keys;
    /* access modifiers changed from: private */
    public List<AwardInfo> listAwards;
    /* access modifiers changed from: private */
    public List<AwardLog> listHistory;
    /* access modifiers changed from: private */
    public List<AwardLog> listLogs;
    /* access modifiers changed from: private */
    public ImageView noData;
    private Random random = new Random();
    OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollingStarted(WheelView wheel) {
            AwardActivity.this.wheelScrolled = true;
        }

        public void onScrollingFinished(WheelView wheel) {
            AwardActivity.this.wheelScrolled = false;
            AwardActivity.this.awardButton.setEnabled(true);
            AwardActivity.this.updateStatus();
        }
    };
    /* access modifiers changed from: private */
    public Button titleButton;
    /* access modifiers changed from: private */
    public ListView trendsListView;
    /* access modifiers changed from: private */
    public boolean wheelScrolled = false;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.award_main);
        this.app = (NewHouseApplication) getApplication();
        this.cotentView = (ViewGroup) findViewById(16908290);
        this.cotentView.setVisibility(8);
        this.keys = new HashMap();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        initWheel(R.id.slot_1);
        initWheel(R.id.slot_2);
        initWheel(R.id.slot_3);
        this.awardButton = (ImageButton) findViewById(R.id.award_btn_switcher);
        this.historyListView = (ListView) findViewById(R.id.award_history);
        this.trendsListView = (ListView) findViewById(R.id.award_trends);
        this.awardHelp = (ImageButton) findViewById(R.id.award_help);
        this.back = (Button) findViewById(R.id.award_back);
        this.titleButton = (Button) findViewById(R.id.award_title);
        this.awardChances = (TextView) findViewById(R.id.award_number);
        this.noData = (ImageView) findViewById(R.id.award_no_data);
        this.helpTextView = new TextView(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.awardHelp.setOnClickListener(this);
        this.awardButton.setOnClickListener(this);
        this.back.setOnClickListener(this);
        String id = getIntent().getStringExtra("id");
        String type = getIntent().getStringExtra(CouponDetailsActivity.INTENT_TYPE);
        this.event = (Event) getIntent().getExtras().get("event");
        if (this.event == null) {
            this.isFromAd = true;
        }
        AwardEventTask task = new AwardEventTask(this, id, type, this.event);
        task.setOnFinish(new GetEventInfoTask.OnFinish() {
            public void onFinsh(Event v) {
                AwardActivity.this.event = v;
                if (AwardActivity.this.isFromAd) {
                    AwardActivity.this.listLogs = AwardActivity.this.event.getE_logs();
                    AwardActivity.this.listAwards = AwardActivity.this.event.getE_awards();
                    AwardActivity.this.awardSize = AwardActivity.this.event.getE_awards().size();
                    AwardActivity.this.titleButton.setText(AwardActivity.this.event.getE_title());
                    AwardActivity.this.listHistory = AwardActivity.this.event.getE_mylogs();
                    AwardActivity.this.chances = String.valueOf(Integer.parseInt(AwardActivity.this.event.getE_winnum()) - Integer.parseInt(AwardActivity.this.event.getE_uwinnum()));
                    AwardActivity.this.helpText = AwardActivity.this.event.getE_content();
                    AwardActivity.this.e_id = AwardActivity.this.event.getE_id();
                } else {
                    AwardActivity.this.listLogs = AwardActivity.this.event.getE_lottery().getE_logs();
                    AwardActivity.this.listAwards = AwardActivity.this.event.getE_lottery().getE_awards();
                    AwardActivity.this.awardSize = AwardActivity.this.event.getE_lottery().getE_awards().size();
                    AwardActivity.this.titleButton.setText(AwardActivity.this.event.getE_lottery().getE_title());
                    AwardActivity.this.listHistory = AwardActivity.this.event.getE_lottery().getE_mylogs();
                    AwardActivity.this.chances = String.valueOf(Integer.parseInt(AwardActivity.this.event.getE_lottery().getE_winnum()) - Integer.parseInt(AwardActivity.this.event.getE_lottery().getE_uwinnum()));
                    AwardActivity.this.helpText = AwardActivity.this.event.getE_lottery().getE_content();
                    AwardActivity.this.e_id = AwardActivity.this.event.getE_lottery().getE_id();
                }
                if (AwardActivity.this.listAwards != null) {
                    AwardActivity.this.calculateAward();
                }
                AwardActivity.this.addImages();
                try {
                    AwardActivity.this.awardChances.setText(AwardActivity.this.chances);
                } catch (Exception e) {
                }
                if (AwardActivity.this.listHistory != null) {
                    AwardActivity.this.adapterHistory = new AwardAdapter(AwardActivity.this, AwardActivity.this.keys);
                    AwardActivity.this.adapterHistory.setList(AwardActivity.this.listHistory);
                    AwardActivity.this.historyListView.setAdapter((ListAdapter) AwardActivity.this.adapterHistory);
                    if (AwardActivity.this.listHistory.size() > 0) {
                        AwardActivity.this.noData.setVisibility(8);
                    }
                }
                AwardActivity.this.trendsListView.setAdapter((ListAdapter) new TrentsAdapter(AwardActivity.this, AwardActivity.this.listLogs, AwardActivity.this.keys));
                AwardActivity.this.trendsListView.post(new Runnable() {
                    public void run() {
                        new CountDownTimer(Long.MAX_VALUE, 1000) {
                            public void onTick(long millisUntilFinished) {
                                AwardActivity.this.trendsListView.setSelectionFromTop(AwardActivity.this.trendsListView.getFirstVisiblePosition() + 1, 0);
                            }

                            public void onFinish() {
                            }
                        }.start();
                    }
                });
                AwardActivity.this.historyListView.post(new Runnable() {
                    public void run() {
                        new CountDownTimer(Long.MAX_VALUE, 1000) {
                            public void onTick(long millisUntilFinished) {
                                AwardActivity.this.historyListView.setSelectionFromTop(AwardActivity.this.historyListView.getFirstVisiblePosition() + 1, 0);
                            }

                            public void onFinish() {
                            }
                        }.start();
                    }
                });
                AwardActivity.this.cotentView.setVisibility(0);
            }
        });
        task.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void calculateAward() {
        for (int i = 0; i < this.listAwards.size(); i++) {
            this.keys.put(this.listAwards.get(i).getA_content(), this.listAwards.get(i).getA_order());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.isFirstLoad && this.images.size() == 0) {
            addImages();
        }
        this.isFirstLoad = false;
    }

    /* access modifiers changed from: private */
    public void addImages() {
        if (this.imageLoader == null) {
            this.imageLoader = new TaofangImageLoader(this);
        }
        for (int i = 0; i < this.listAwards.size(); i++) {
            Bitmap bitmap = this.imageLoader.loadBitmap(this.listAwards.get(i).getA_pic(), (int) getResources().getDimension(R.dimen.award_image_width), (int) getResources().getDimension(R.dimen.award_image_height), 3);
            ((NewHouseApplication) getApplicationContext()).addAwardPic(bitmap);
            if (i == 0) {
                this.images.add(bitmap);
            } else {
                this.images.add(1, bitmap);
            }
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int totalHeight = 0;
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
            listView.setLayoutParams(params);
        }
    }

    private void initWheel(int id) {
        AwardWheelView wheel = getWheel(id);
        wheel.setViewAdapter(new SlotMachineAdapter(this));
        wheel.setLayoutParams(wheel.getLayoutParams());
        wheel.setCurrentItem(0);
        wheel.setVisibleItems(1);
        wheel.setWheel_bg_BackgroundResource(R.color.full_transparent);
        wheel.setIs_shadow(false);
        if (R.id.slot_3 == id) {
            wheel.addChangingListener(this.changedListener);
            wheel.addScrollingListener(this.scrolledListener);
        }
        wheel.setCyclic(true);
        wheel.setEnabled(false);
    }

    private void scrollWheel(int id, int scroll, int time) {
        getWheel(id).scroll(scroll, time);
    }

    /* access modifiers changed from: private */
    public void updateStatus() {
        if (this.isWin) {
            Toast.makeText(this, this.content, 0).show();
        } else {
            Toast.makeText(this, this.content, 0).show();
        }
    }

    private void startLotteryBack() {
        this.awardButton.setEnabled(false);
        new AwardLuckTask(this, this.e_id, App.Categroy.Event.LOTTERY, this).execute(new Object[0]);
    }

    private AwardWheelView getWheel(int id) {
        return (AwardWheelView) findViewById(id);
    }

    private class SlotMachineAdapter extends AbstractWheelAdapter {
        final int IMAGE_HEIGHT;
        final int IMAGE_WIDTH;
        private Context context;
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(this.IMAGE_WIDTH, this.IMAGE_HEIGHT);

        public SlotMachineAdapter(Context context2) {
            this.IMAGE_WIDTH = (int) AwardActivity.this.getResources().getDimension(R.dimen.award_image_width);
            this.IMAGE_HEIGHT = (int) AwardActivity.this.getResources().getDimension(R.dimen.award_image_height);
            this.context = context2;
        }

        public int getItemsCount() {
            return AwardActivity.this.images.size();
        }

        public View getItem(int index, View cachedView, ViewGroup parent) {
            ImageView img;
            if (cachedView != null) {
                img = (ImageView) cachedView;
            } else {
                img = new ImageView(this.context);
            }
            img.setLayoutParams(this.params);
            img.setImageBitmap((Bitmap) AwardActivity.this.images.get(index));
            return img;
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.award_btn_switcher) {
            startLotteryBack();
        } else if (v.getId() == R.id.award_help) {
            if (this.dialog == null) {
                this.dialog = new Dialog(this);
                this.dialog.requestWindowFeature(1);
                View view = LayoutInflater.from(this).inflate((int) R.layout.award_help_layout, (ViewGroup) null);
                this.helpTextView = (TextView) view.findViewById(R.id.award_help_text);
                this.helpTextView.setText(Html.fromHtml(this.helpText));
                this.dialog.setContentView(view);
                this.dialog.setCanceledOnTouchOutside(true);
            }
            this.dialog.show();
        } else if (v.getId() == R.id.award_back) {
            finish();
        }
    }

    private void startLottery(int first, int second, int third) {
        scrollWheel(R.id.slot_1, (((-this.images.size()) * 10) - first) - this.awards[0], 2000);
        scrollWheel(R.id.slot_2, (((-this.images.size()) * 10) - second) - this.awards[1], 3000);
        scrollWheel(R.id.slot_3, (((-this.images.size()) * 10) - third) - this.awards[2], 4000);
        this.awards[0] = this.images.size() - first;
        this.awards[1] = this.images.size() - second;
        this.awards[2] = this.images.size() - third;
    }

    public void onFinish(AwardInfo info) {
        int temp;
        if ("1".equals(info.getU_error())) {
            notWinning();
            this.awardButton.setEnabled(true);
            Toast.makeText(this, getString(R.string.award_pass), 0).show();
            return;
        }
        String order = info.getAward().getA_order();
        try {
            if (this.isFromAd) {
                temp = Integer.parseInt(this.event.getE_winnum()) - Integer.parseInt(info.getU_num());
            } else {
                temp = Integer.parseInt(this.event.getE_lottery().getE_winnum()) - Integer.parseInt(info.getU_num());
            }
            this.awardChances.setText(String.valueOf(temp));
        } catch (Exception e) {
        }
        if (order != null) {
            int orderInt = Integer.parseInt(order);
            if (orderInt == -1) {
                this.content = getString(R.string.award_notgot);
                this.isWin = false;
                notWinning();
                startLottery(this.awardSetting[0], this.awardSetting[1], this.awardSetting[2]);
                return;
            }
            this.isWin = true;
            this.listHistory = info.getU_logs();
            if (this.listHistory.size() > 0) {
                this.noData.setVisibility(8);
                this.adapterHistory.setList(this.listHistory);
                this.adapterHistory.notifyDataSetChanged();
            }
            this.content = getString(R.string.award_got);
            startLottery(orderInt, orderInt, orderInt);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        sendBroadcast(new Intent(ActionCode.INTENT_ACTION_SIGN));
    }

    private void notWinning() {
        this.awardSetting = new int[3];
        for (int i = 0; i < 3; i++) {
            this.awardSetting[i] = Math.abs(this.random.nextInt()) % this.awardSize;
        }
        if (this.awardSetting[0] == this.awardSetting[1] && this.awardSetting[0] == this.awardSetting[2]) {
            notWinning();
        }
    }
}
