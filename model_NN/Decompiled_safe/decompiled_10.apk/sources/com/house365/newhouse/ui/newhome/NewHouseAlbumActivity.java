package com.house365.newhouse.ui.newhome;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Album;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAlbumAdapter;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseAlbumActivity extends BaseCommonActivity {
    /* access modifiers changed from: private */
    public NewHouseAlbumAdapter adapter;
    /* access modifiers changed from: private */
    public int album_count = 0;
    /* access modifiers changed from: private */
    public List<Album> albums;
    /* access modifiers changed from: private */
    public String h_id;
    private HeadNavigateView head_view;
    private GridView houses_album;
    /* access modifiers changed from: private */
    public int resultCount = 0;
    private Button top_right;
    private TextView tvTitle;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_album);
    }

    /* access modifiers changed from: protected */
    public void clean() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText(String.valueOf(getResources().getString(R.string.title_house)) + getResources().getString(R.string.title_album));
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewHouseAlbumActivity.this.finish();
            }
        });
        this.houses_album = (GridView) findViewById(R.id.houses_album);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.h_id = getIntent().getStringExtra("h_id");
        this.adapter = new NewHouseAlbumAdapter(this);
        this.houses_album.setAdapter((ListAdapter) this.adapter);
        this.houses_album.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent intent = new Intent(NewHouseAlbumActivity.this, NewHouseAlbumSpecifyActivity.class);
                NewHouseAlbumActivity.this.album_count = arg2;
                intent.putExtra("album", (Album) NewHouseAlbumActivity.this.albums.get(arg2));
                NewHouseAlbumActivity.this.startActivity(intent);
            }
        });
        this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((Album) NewHouseAlbumActivity.this.albums.get(NewHouseAlbumActivity.this.album_count)).getA_photos();
            }
        });
        new GetHouseAlbumList(this, null).execute(new Void[0]);
    }

    private class GetHouseAlbumList extends AsyncTask<Void, Void, List<Album>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<Album>) ((List) obj));
        }

        private GetHouseAlbumList() {
        }

        /* synthetic */ GetHouseAlbumList(NewHouseAlbumActivity newHouseAlbumActivity, GetHouseAlbumList getHouseAlbumList) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            NewHouseAlbumActivity.this.showLoadingDialog();
        }

        /* access modifiers changed from: protected */
        public List<Album> doInBackground(Void... params) {
            try {
                NewHouseAlbumActivity.this.albums = ((HttpApi) ((NewHouseApplication) NewHouseAlbumActivity.this.mApplication).getApi()).getHousePhotoById(NewHouseAlbumActivity.this.h_id, StringUtils.EMPTY);
                return NewHouseAlbumActivity.this.albums;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Album> result) {
            NewHouseAlbumActivity.this.dismissLoadingDialog();
            if (result != null) {
                NewHouseAlbumActivity.this.adapter.addAll(result);
                NewHouseAlbumActivity.this.adapter.notifyDataSetChanged();
                NewHouseAlbumActivity.this.resultCount = result.size();
                if (NewHouseAlbumActivity.this.resultCount == 0) {
                    NewHouseAlbumActivity.this.showToast((int) R.string.text_no_result);
                    return;
                }
                return;
            }
            NewHouseAlbumActivity.this.showToast((int) R.string.net_error);
            NewHouseAlbumActivity.this.finish();
        }
    }
}
