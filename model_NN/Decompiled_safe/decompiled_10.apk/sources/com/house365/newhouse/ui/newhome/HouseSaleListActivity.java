package com.house365.newhouse.ui.newhome;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.SaleInfo;
import com.house365.newhouse.ui.newhome.adapter.HouseSaleInfoAdapter;
import java.util.List;

public class HouseSaleListActivity extends BaseListActivity {
    private HouseSaleInfoAdapter adapter;
    private HeadNavigateView head_view;
    String id;
    private RefreshInfo listRefresh = new RefreshInfo();
    private PullToRefreshListView listView;
    private NewHouseApplication mApplication;

    /* access modifiers changed from: private */
    public void refreshData() {
        this.listRefresh.refresh = true;
        new GetHouseSaleList(this, this.listView, this.listRefresh, this.adapter).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.listRefresh.refresh = false;
        new GetHouseSaleList(this, this.listView, this.listRefresh, this.adapter).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_sale_info);
        this.mApplication = (NewHouseApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void clean() {
        this.adapter.clear();
    }

    private class GetHouseSaleList extends BaseListAsyncTask<SaleInfo> {
        public GetHouseSaleList(Context context, PullToRefreshListView listView, RefreshInfo listRefresh, BaseListAdapter adapter) {
            super(context, listView, listRefresh, adapter);
        }

        public List<SaleInfo> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseSaleInfo(HouseSaleListActivity.this.id, this.listRefresh.page);
        }

        /* access modifiers changed from: protected */
        public void onAfterRefresh(List<SaleInfo> v) {
            if (v == null) {
                Toast.makeText(this.context, (int) R.string.text_no_result, 1).show();
            }
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_field_sale_info);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseSaleListActivity.this.finish();
            }
        });
        this.listView = (PullToRefreshListView) findViewById(R.id.listView);
        this.adapter = new HouseSaleInfoAdapter(this);
        this.listView.setAdapter(this.adapter);
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                HouseSaleListActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                HouseSaleListActivity.this.getMoreData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.id = getIntent().getStringExtra("id");
        refreshData();
    }
}
