package com.house365.newhouse.ui.common.task;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.house365.core.constant.CorePreferences;
import com.house365.core.task.CommonAsyncTask;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.ui.common.adapter.AdAdapter;
import java.util.List;

public class GetAdTask extends CommonAsyncTask<List<Ad>> {
    private AdAdapter adAdapter;
    private int ad_type;
    private CallBack callBack;
    private ImageView no_ad_img;
    private View viewpager_layout;

    public interface CallBack {
        void onFail();

        void onSuccess(Ad ad);
    }

    public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
        onAfterDoInBackgroup((List<Ad>) ((List) obj));
    }

    public GetAdTask(Context context, int ad_type2, AdAdapter adAdapter2, View viewpager_layout2, ImageView no_ad_img2) {
        super(context);
        this.viewpager_layout = viewpager_layout2;
        this.adAdapter = adAdapter2;
        this.ad_type = ad_type2;
        this.no_ad_img = no_ad_img2;
    }

    public GetAdTask(Context context, int ad_type2, CallBack callBack2) {
        super(context);
        this.ad_type = ad_type2;
        this.callBack = callBack2;
    }

    public void onAfterDoInBackgroup(List<Ad> v) {
        if (this.ad_type == 1) {
            if (v != null && v.size() > 0) {
                this.callBack.onSuccess(v.get(0));
            }
        } else if (v == null || v.size() <= 0) {
            this.callBack.onFail();
        } else {
            this.viewpager_layout.setVisibility(0);
            this.no_ad_img.setVisibility(8);
            this.adAdapter.clear();
            this.adAdapter.addAll(v);
            this.adAdapter.notifyDataSetChanged();
            this.callBack.onSuccess(null);
        }
    }

    public List<Ad> onDoInBackgroup() {
        try {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getAdList(this.ad_type);
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            return null;
        }
    }

    public CallBack getCallBack() {
        return this.callBack;
    }

    public void setCallBack(CallBack callBack2) {
        this.callBack = callBack2;
    }
}
