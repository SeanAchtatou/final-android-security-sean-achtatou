package com.house365.newhouse.ui.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.house365.newhouse.R;
import java.util.HashMap;
import java.util.List;

public class SearchAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private String key;
    private List<HashMap<String, String>> search_data;

    public SearchAdapter(Context context, List<HashMap<String, String>> search_data2, String key2) {
        this.inflater = LayoutInflater.from(context);
        this.search_data = search_data2;
        this.key = key2;
    }

    public int getCount() {
        return this.search_data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            View convertView2 = this.inflater.inflate((int) R.layout.search_keyword, (ViewGroup) null);
        }
        return null;
    }
}
