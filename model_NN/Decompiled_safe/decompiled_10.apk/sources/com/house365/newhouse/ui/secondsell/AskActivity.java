package com.house365.newhouse.ui.secondsell;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.RegexUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import org.apache.commons.lang.StringUtils;

public class AskActivity extends BaseCommonActivity {
    private Button btn_submit;
    /* access modifiers changed from: private */
    public EditText etMobile;
    /* access modifiers changed from: private */
    public EditText et_content;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public SecondHouse house;
    private NewHouseApplication mApplication;
    /* access modifiers changed from: private */
    public int type;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.ask_layout);
        this.mApplication = (NewHouseApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AskActivity.this.finish();
            }
        });
        this.etMobile = (EditText) findViewById(R.id.et_mobile);
        this.et_content = (EditText) findViewById(R.id.et_content);
        this.btn_submit = (Button) findViewById(R.id.btn_submit);
        this.btn_submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String content = AskActivity.this.et_content.getText().toString().trim();
                final String mobile = AskActivity.this.etMobile.getText().toString().trim();
                if (StringUtils.isEmpty(content)) {
                    AskActivity.this.showToast((int) R.string.text_ask_null);
                    AskActivity.this.et_content.requestFocus();
                } else if (content.length() < 5) {
                    AskActivity.this.showToast((int) R.string.txt_report_length);
                    AskActivity.this.et_content.requestFocus();
                } else if (StringUtils.isEmpty(mobile) || !RegexUtil.isNumberWithLen(mobile, 11) || !RegexUtil.isMobileNumber(mobile)) {
                    AskActivity.this.showToast((int) R.string.text_validate_mobile);
                    AskActivity.this.etMobile.requestFocus();
                } else {
                    new CommonAsyncTask<CommonResultInfo>(AskActivity.this, R.string.sending) {
                        public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
                            if (resultInfo != null && resultInfo.getResult() == 1) {
                                AskActivity.this.showToast((int) R.string.text_ask_send_success);
                                if (AskActivity.this.type == 1) {
                                    ((NewHouseApplication) this.mApplication).saveCallHistory(new HouseInfo(App.SELL, AskActivity.this.house), App.SELL);
                                } else if (AskActivity.this.type == 2) {
                                    ((NewHouseApplication) this.mApplication).saveCallHistory(new HouseInfo(App.RENT, AskActivity.this.house), App.RENT);
                                }
                                AskActivity.this.finish();
                            } else if (resultInfo == null) {
                                AskActivity.this.showToast((int) R.string.msg_load_error);
                            } else {
                                AskActivity.this.showToast(resultInfo.getMsg());
                            }
                        }

                        public CommonResultInfo onDoInBackgroup() {
                            try {
                                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).ask(AskActivity.this.type, AskActivity.this.house.getId(), mobile, content);
                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    }.execute(new Object[0]);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.type = getIntent().getIntExtra(CouponDetailsActivity.INTENT_TYPE, 1);
        this.house = (SecondHouse) getIntent().getSerializableExtra("house");
        this.et_content.setText(getResources().getString(R.string.text_ask_tip, this.house.getTitle()));
        if (this.mApplication.getMobile().toString() != null) {
            this.etMobile.setText(this.mApplication.getMobile().toString());
        }
    }
}
