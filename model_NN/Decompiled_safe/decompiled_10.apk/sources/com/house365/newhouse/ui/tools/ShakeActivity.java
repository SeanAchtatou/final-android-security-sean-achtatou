package com.house365.newhouse.ui.tools;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.mapapi.MKEvent;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.ShakeInfo;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.privilege.BargainlHouseActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.CubeHouseRoomActivity;
import com.house365.newhouse.ui.privilege.EventListActivity;
import org.apache.commons.lang.SystemUtils;

public class ShakeActivity extends BaseCommonActivity implements SensorEventListener {
    private static final int MSG_RATE = 1;
    private static final int MSG_SHOW_SHAKE_INFO = 2;
    private static final int MSG_SHOW_SHAKE_INFO_WITH_HIDE = 3;
    private int ANIMATION_HIDE_DURATION = 1500;
    private int ANIMATION_SHAKE_DURATION = 1500;
    private int ANIMATION_SHOW_DURATION = MKEvent.ERROR_PERMISSION_DENIED;
    /* access modifiers changed from: private */
    public ImageView ic_arrow;
    /* access modifiers changed from: private */
    public ImageView icon_shake;
    /* access modifiers changed from: private */
    public boolean isAniming = false;
    private SensorManager mSensorManager = null;
    /* access modifiers changed from: private */
    public Vibrator mVibrator = null;
    /* access modifiers changed from: private */
    public Animation rateAnim;
    /* access modifiers changed from: private */
    public Handler shakeHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ShakeActivity.this.icon_shake.startAnimation(ShakeActivity.this.rateAnim);
                    return;
                case 2:
                    ShakeActivity.this.setShakeInfo();
                    ShakeActivity.this.shakeInfoLayout.startAnimation(ShakeActivity.this.shakeInfoShowAnim);
                    return;
                case 3:
                    ShakeActivity.this.shakeInfoLayout.startAnimation(ShakeActivity.this.shakeInfoHideAnim);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ShakeInfo shakeInfo;
    /* access modifiers changed from: private */
    public Animation shakeInfoHideAnim;
    /* access modifiers changed from: private */
    public View shakeInfoLayout;
    /* access modifiers changed from: private */
    public Animation shakeInfoShowAnim;
    private TextView shake_content1;
    private TextView shake_content2;
    private RelativeLayout shake_layout;
    private TextView shake_title;
    /* access modifiers changed from: private */
    public TextView txt_more_info;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.shake_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        ((HeadNavigateView) findViewById(R.id.head_view)).getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShakeActivity.this.finish();
            }
        });
        this.shake_layout = (RelativeLayout) findViewById(R.id.shake_layout);
        this.shake_layout.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 480.0d) * 368.0d);
        this.icon_shake = (ImageView) findViewById(R.id.icon_shake);
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mVibrator = (Vibrator) getSystemService("vibrator");
        this.rateAnim = new RotateAnimation(SystemUtils.JAVA_VERSION_FLOAT, 30.0f, 1, 0.5f, 1, 0.5f);
        this.rateAnim.setInterpolator(new CycleInterpolator(3.0f));
        this.rateAnim.setDuration((long) this.ANIMATION_SHAKE_DURATION);
        this.rateAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ShakeActivity.this.shakeHandler.sendEmptyMessageDelayed(1, 1000);
            }
        });
        this.shakeInfoLayout = findViewById(R.id.shakeInfoLayout);
        this.shakeInfoLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ShakeActivity.this.shakeInfo == null) {
                    return;
                }
                if ("house".equals(ShakeActivity.this.shakeInfo.getType())) {
                    Intent intent = new Intent(ShakeActivity.this, NewHouseDetailActivity.class);
                    intent.putExtra("h_id", ShakeActivity.this.shakeInfo.getHouse().getH_id());
                    ShakeActivity.this.startActivity(intent);
                } else if (App.Categroy.Event.COUPON.equals(ShakeActivity.this.shakeInfo.getType())) {
                    Intent intent2 = new Intent(ShakeActivity.this, CouponDetailsActivity.class);
                    intent2.putExtra("id", ShakeActivity.this.shakeInfo.getEvent().getE_id());
                    intent2.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.COUPON);
                    ShakeActivity.this.startActivity(intent2);
                } else if ("event".equals(ShakeActivity.this.shakeInfo.getType())) {
                    new Intent(ShakeActivity.this, EventListActivity.class);
                    Intent intent3 = new Intent(ShakeActivity.this, EventListActivity.class);
                    intent3.putExtra("e_id", ShakeActivity.this.shakeInfo.getEvent().getE_id());
                    ShakeActivity.this.startActivity(intent3);
                } else if (App.Categroy.Event.TJF.equals(ShakeActivity.this.shakeInfo.getType())) {
                    Intent intent4 = new Intent(ShakeActivity.this, BargainlHouseActivity.class);
                    intent4.putExtra("e_id", ShakeActivity.this.shakeInfo.getEvent().getE_id());
                    ShakeActivity.this.startActivity(intent4);
                } else if (App.Categroy.Event.TLF.equals(ShakeActivity.this.shakeInfo.getType())) {
                    Intent intent5 = new Intent(ShakeActivity.this, CubeHouseRoomActivity.class);
                    intent5.putExtra("e_id", ShakeActivity.this.shakeInfo.getEvent().getE_id());
                    ShakeActivity.this.startActivity(intent5);
                }
            }
        });
        this.shake_title = (TextView) findViewById(R.id.shake_title);
        this.shake_content1 = (TextView) findViewById(R.id.shake_content1);
        this.shake_content2 = (TextView) findViewById(R.id.shake_content2);
        this.txt_more_info = (TextView) findViewById(R.id.txt_more_info);
        this.ic_arrow = (ImageView) findViewById(R.id.ic_arrow);
        this.shakeHandler.sendEmptyMessageDelayed(1, 1000);
        this.shakeInfoHideAnim = new TranslateAnimation(1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 2, 1.0f);
        this.shakeInfoHideAnim.setInterpolator(new AnticipateInterpolator(1.0f));
        this.shakeInfoHideAnim.setDuration((long) this.ANIMATION_HIDE_DURATION);
        this.shakeInfoHideAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ShakeActivity.this.setShakeInfo();
                ShakeActivity.this.shakeInfoLayout.startAnimation(ShakeActivity.this.shakeInfoShowAnim);
            }
        });
        this.shakeInfoShowAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        this.shakeInfoShowAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        this.shakeInfoShowAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ShakeActivity.this.isAniming = false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.isAniming = false;
        super.onResume();
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 3);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mSensorManager.unregisterListener(this);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mSensorManager.unregisterListener(this);
        super.onPause();
    }

    public void onSensorChanged(SensorEvent event) {
        try {
            int sensorType = event.sensor.getType();
            float[] values = event.values;
            if (sensorType != 1) {
                return;
            }
            if ((Math.abs(values[0]) > 10.0f || Math.abs(values[1]) > 10.0f || Math.abs(values[2]) > 10.0f) && !this.isAniming) {
                new DoSensorChangeTask(this).execute(new Object[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class DoSensorChangeTask extends CommonAsyncTask<ShakeInfo> {
        public DoSensorChangeTask(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ShakeActivity.this.isAniming = true;
        }

        public void onAfterDoInBackgroup(ShakeInfo result) {
            if (result == null) {
                ShakeActivity.this.showToast((int) R.string.msg_load_error);
                ShakeActivity.this.isAniming = false;
                return;
            }
            ShakeActivity.this.mVibrator.vibrate(50);
            if (ShakeActivity.this.shakeInfo == null) {
                ShakeActivity.this.shakeInfo = result;
                ShakeActivity.this.shakeHandler.sendEmptyMessageDelayed(2, 500);
                return;
            }
            ShakeActivity.this.shakeInfo = result;
            ShakeActivity.this.shakeHandler.sendEmptyMessageDelayed(3, 500);
        }

        public ShakeInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getShakeInfo();
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            ShakeActivity.this.isAniming = false;
            super.onNetworkUnavailable();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            ShakeActivity.this.txt_more_info.setVisibility(8);
            ShakeActivity.this.ic_arrow.setVisibility(8);
            ShakeActivity.this.isAniming = false;
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            ActivityUtil.showToast(ShakeActivity.this, (int) R.string.text_no_shake_info);
            ShakeActivity.this.txt_more_info.setVisibility(8);
            ShakeActivity.this.ic_arrow.setVisibility(8);
            ShakeActivity.this.isAniming = false;
        }
    }

    /* access modifiers changed from: private */
    public void setShakeInfo() {
        if ("house".equals(this.shakeInfo.getType())) {
            this.shake_title.setText(this.shakeInfo.getHouse().getH_name());
            this.shake_content1.setText(getResources().getString(R.string.text_field_project_address, this.shakeInfo.getHouse().getH_project_address()));
            this.shake_content2.setText(getResources().getString(R.string.text_house_avg_price, this.shakeInfo.getHouse().getH_price()));
        } else {
            this.shake_title.setText(this.shakeInfo.getEvent().getE_title());
            this.shake_content1.setText(getResources().getString(R.string.text_field_eventime, TimeUtil.toDate(this.shakeInfo.getEvent().getE_endtime())));
            this.shake_content2.setText(getResources().getString(R.string.text_event_join, this.shakeInfo.getEvent().getE_join()));
        }
        this.txt_more_info.setVisibility(0);
        this.ic_arrow.setVisibility(0);
    }
}
