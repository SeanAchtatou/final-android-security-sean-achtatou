package com.house365.newhouse.ui.secondsell;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;

public class SecondSell_houseRemarkActivity extends BaseCommonActivity {
    private HeadNavigateView head_view;
    private TextView house_remark;
    private TextView house_title;
    private String title_name;
    private String txt_house_remark;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.secondsell_detail_houseremarklayout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSell_houseRemarkActivity.this.finish();
            }
        });
        this.title_name = getIntent().getStringExtra("title_name");
        this.txt_house_remark = getIntent().getStringExtra("txt_house_remark");
        this.house_remark = (TextView) findViewById(R.id.house_remark);
        this.house_title = (TextView) findViewById(R.id.house_title);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.head_view.setTvTitleText(this.title_name);
        this.house_title.setText((int) R.string.house_discribe);
        this.house_remark.setText(this.txt_house_remark);
    }
}
