package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Block;

public class BlockListAdapter extends BaseCacheListAdapter<Block> {
    public BlockListAdapter(Context context) {
        super(context);
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.item_list_block, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.iv_block_pic = (ImageView) convertView.findViewById(R.id.iv_block_pic);
            holder.txt_title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.txt_sell_info = (TextView) convertView.findViewById(R.id.txt_sell_info);
            holder.txt_sell_price = (TextView) convertView.findViewById(R.id.txt_sell_price);
            holder.txt_rent_info = (TextView) convertView.findViewById(R.id.txt_rent_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Block block = (Block) getItem(position);
        setCacheImage(holder.iv_block_pic, block.getPic(), R.drawable.img_default_small, 1);
        holder.txt_title.setText(block.getBlockname());
        holder.txt_sell_info.setText(this.context.getResources().getString(R.string.block_sell_count, block.getSellcount()));
        if (block.getSellaverprice().equalsIgnoreCase("暂无均价")) {
            holder.txt_sell_price.setText(block.getSellaverprice());
        } else {
            holder.txt_sell_price.setText(this.context.getResources().getString(R.string.block_sell_price, block.getSellaverprice()));
        }
        holder.txt_rent_info.setText(this.context.getResources().getString(R.string.block_rent_count, block.getRentcount()));
        return convertView;
    }

    private class ViewHolder {
        ImageView iv_block_pic;
        TextView txt_rent_info;
        TextView txt_sell_info;
        TextView txt_sell_price;
        TextView txt_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(BlockListAdapter blockListAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
