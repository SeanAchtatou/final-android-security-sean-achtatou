package com.house365.newhouse.ui.secondrent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RegexUtil;
import com.house365.core.util.TextUtil;
import com.house365.core.util.resource.ResourceUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.task.GetPublishConfigTask;
import com.house365.newhouse.task.GetValidateTask;
import com.house365.newhouse.task.PublishHouseTask;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.user.MyPublishActivity;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

public class SecondRentPublishActivity extends BaseCommonActivity {
    /* access modifiers changed from: private */
    public Button btn_captcha;
    private Button btn_publish;
    private Button btn_relogin;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    /* access modifiers changed from: private */
    public EditText et_captcha;
    /* access modifiers changed from: private */
    public EditText et_contact;
    /* access modifiers changed from: private */
    public EditText et_tel;
    /* access modifiers changed from: private */
    public EditText et_title;
    private PublishHouse house;
    boolean[] isChooseItem = new boolean[6];
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    /* access modifiers changed from: private */
    public MyCountimer mctime;
    private View.OnClickListener publishSubmitListener = new View.OnClickListener() {
        public void onClick(View v) {
            SecondRentPublishActivity.this.publishVal.put("name", "HousePerson");
            SecondRentPublishActivity.this.publishVal.put("method", "secondhouse.arrSaveRentHouse");
            if (SecondRentPublishActivity.this.getResources().getString(R.string.text_channel_hint).equals(SecondRentPublishActivity.this.tv_channel.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_channel_hint);
            } else if (SecondRentPublishActivity.this.getResources().getString(R.string.text_buildarea_hint).equals(SecondRentPublishActivity.this.tv_buildarea.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_buildarea_hint);
            } else if (TextUtils.isEmpty(SecondRentPublishActivity.this.et_title.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_rent_title_hint);
            } else if (TextUtils.isEmpty(SecondRentPublishActivity.this.et_contact.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_contact_hint);
            } else if (!RegexUtil.isChineseLength(SecondRentPublishActivity.this.et_contact.getText().toString(), 1, 6)) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_contact_error);
            } else if (TextUtils.isEmpty(SecondRentPublishActivity.this.et_tel.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_tel_hint);
            } else if (!RegexUtil.isMobileNumber(SecondRentPublishActivity.this.et_tel.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_tel_error);
            } else if (TextUtils.isEmpty(SecondRentPublishActivity.this.et_captcha.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_captcha_hint);
            } else if (!SecondRentPublishActivity.this.validateCode.equals(SecondRentPublishActivity.this.et_captcha.getText().toString())) {
                SecondRentPublishActivity.this.showToast((int) R.string.text_captcha_error);
            } else if (!SecondRentPublishActivity.this.getResources().getString(R.string.text_price_rent_hint).equals(SecondRentPublishActivity.this.tv_rent_price.getText().toString()) || SecondRentPublishActivity.this.publishType != 5) {
                SecondRentPublishActivity.this.publishVal.put("address", SecondRentPublishActivity.this.et_title.getText().toString());
                SecondRentPublishActivity.this.publishVal.put("contactor", SecondRentPublishActivity.this.et_contact.getText().toString());
                SecondRentPublishActivity.this.publishVal.put("telno", SecondRentPublishActivity.this.et_tel.getText().toString());
                SecondRentPublishActivity.this.publishVal.put("validcode", SecondRentPublishActivity.this.et_captcha.getText().toString());
                PublishHouseTask publishTask = new PublishHouseTask(SecondRentPublishActivity.this.context, SecondRentPublishActivity.this.publishVal, 1);
                publishTask.execute(new Object[0]);
                publishTask.setOnSuccessListener(new PublishHouseTask.PublishOnSuccessListener() {
                    public void OnSuccess(CommonResultInfo v, PublishHouse publishHouse) {
                        SecondRentPublishActivity.this.mApplication.addPublishRec(SecondRentPublishActivity.this.setPublishData(publishHouse.getId(), publishHouse.getCreatetime()), 10, SecondRentPublishActivity.this.publishType);
                        Intent intent = new Intent(SecondRentPublishActivity.this.context, MyPublishActivity.class);
                        intent.putExtra(MyPublishActivity.INTENT_PUBLISH_TYPE, 5);
                        intent.putExtra(MyPublishActivity.INTENT_PUBLISH_OFFER, SecondRentPublishActivity.this.fileList());
                        SecondRentPublishActivity.this.startActivity(intent);
                        SecondRentPublishActivity.this.finish();
                    }
                });
            } else {
                SecondRentPublishActivity.this.showToast((int) R.string.text_price_rent_hint);
            }
        }
    };
    /* access modifiers changed from: private */
    public int publishType = 5;
    /* access modifiers changed from: private */
    public HashMap<String, String> publishVal = new HashMap<>();
    String streetValue;
    /* access modifiers changed from: private */
    public TextView tv_buildarea;
    /* access modifiers changed from: private */
    public TextView tv_channel;
    private TextView tv_contact_label;
    /* access modifiers changed from: private */
    public TextView tv_district;
    private TextView tv_remark_label;
    /* access modifiers changed from: private */
    public TextView tv_rent_price;
    /* access modifiers changed from: private */
    public TextView tv_rent_way;
    /* access modifiers changed from: private */
    public TextView tv_roomtype;
    private TextView tv_tel_label;
    private TextView tv_title_label;
    /* access modifiers changed from: private */
    public String validateCode = StringUtils.EMPTY;
    private View view_buildarea;
    private View view_captcha;
    private View view_channel;
    private View view_contact;
    private View view_district;
    private View view_rent_price;
    private View view_rent_way;
    private View view_roomtype;
    private View view_tel;
    private View view_title;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.second_rent_public_layout);
        this.context = this;
        this.mApplication = (NewHouseApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.btn_publish = (Button) findViewById(R.id.btn_publish);
        this.btn_publish.setOnClickListener(this.publishSubmitListener);
        this.view_channel = findViewById(R.id.view_channel);
        this.view_district = findViewById(R.id.view_district);
        this.view_roomtype = findViewById(R.id.view_roomtype);
        this.view_buildarea = findViewById(R.id.view_buildarea);
        this.view_rent_price = findViewById(R.id.view_rent_price);
        this.view_rent_way = findViewById(R.id.view_rent_way);
        this.view_title = findViewById(R.id.view_title);
        this.view_contact = findViewById(R.id.view_contact);
        this.view_tel = findViewById(R.id.view_tel);
        this.view_captcha = findViewById(R.id.view_captcha);
        this.tv_channel = (TextView) findViewById(R.id.tv_channel);
        this.tv_district = (TextView) findViewById(R.id.tv_district);
        this.tv_roomtype = (TextView) findViewById(R.id.tv_roomtype);
        this.tv_buildarea = (TextView) findViewById(R.id.tv_buildarea);
        this.tv_title_label = (TextView) findViewById(R.id.tv_title_label);
        this.tv_rent_price = (TextView) findViewById(R.id.tv_rent_price);
        this.tv_rent_way = (TextView) findViewById(R.id.tv_rent_way);
        this.et_title = (EditText) findViewById(R.id.et_title);
        this.et_contact = (EditText) findViewById(R.id.et_contact);
        this.et_tel = (EditText) findViewById(R.id.et_tel);
        this.et_captcha = (EditText) findViewById(R.id.et_captcha);
        this.btn_captcha = (Button) findViewById(R.id.btn_captcha);
        this.btn_captcha.setEnabled(false);
        this.et_tel.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.toString().length() != 11) {
                    return;
                }
                if (!TextUtil.isMobliePhone(SecondRentPublishActivity.this.et_tel.getText().toString())) {
                    SecondRentPublishActivity.this.showToast((int) R.string.text_mobile_error);
                    SecondRentPublishActivity.this.btn_captcha.setEnabled(false);
                    return;
                }
                SecondRentPublishActivity.this.btn_captcha.setEnabled(true);
            }
        });
        this.btn_relogin = (Button) findViewById(R.id.btn_relogin);
        this.btn_relogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppMethods.showLoginDialog(SecondRentPublishActivity.this.context, R.string.text_relogin_msg, StringUtils.EMPTY);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getPublistConfig();
        this.tv_channel.setText((int) R.string.public_house);
        this.publishVal.put(HouseBaseInfo.INFOTYPE, "1");
    }

    private void getPublistConfig() {
        GetPublishConfigTask publishConfigTask = new GetPublishConfigTask(this, this.publishType);
        publishConfigTask.setOnFinishListener(new GetPublishConfigTask.PublishConfigOnFinish() {
            public void onSuccess(HouseBaseInfo v) {
                if (v.getJsonStr().equals(SecondRentPublishActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondRentPublishActivity.this.showToast((int) R.string.text_no_network);
                    SecondRentPublishActivity.this.finish();
                    return;
                }
                SecondRentPublishActivity.this.initPublishViewListener(v);
                if (SecondRentPublishActivity.this.mApplication.getMobile().toString() != null) {
                    SecondRentPublishActivity.this.et_tel.setText(SecondRentPublishActivity.this.mApplication.getMobile().toString());
                }
            }

            public void onFail() {
            }
        });
        publishConfigTask.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setChooseTextView(TextView tv, CharSequence str) {
        tv.setText(str);
        tv.setTextColor(ResourceUtil.getColor(this, R.color.black));
    }

    /* access modifiers changed from: private */
    public PublishHouse setPublishData(String id, int createTime) {
        PublishHouse house2 = new PublishHouse(this.et_title.getText().toString(), this.publishVal.get("remark"), this.publishVal.get("contactor"), this.publishVal.get("telno"), this.tv_channel.getText().toString(), this.tv_district.getText().toString(), this.tv_rent_price.getText().toString(), this.tv_roomtype.getText().toString(), this.tv_rent_way.getText().toString(), this.tv_buildarea.getText().toString());
        house2.setCreatetime(createTime);
        return house2;
    }

    public static int parseValueToIndex(String[] arrays, String value) {
        if (arrays != null) {
            for (int i = 0; i < arrays.length; i++) {
                if (arrays[i].equals(value)) {
                    return i;
                }
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void initPublishViewListener(final HouseBaseInfo publishConfig) {
        this.view_channel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = TextUtil.arrayToShift((String[]) publishConfig.getSell_config().get(HouseBaseInfo.INFOTYPE).toArray(new String[0]));
                ActivityUtil.showSingleChooseDialog(SecondRentPublishActivity.this.context, (int) R.string.text_channel, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondRentPublishActivity.this.publishVal.put(HouseBaseInfo.INFOTYPE, new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_channel, chooseItems[which]);
                    }
                });
            }
        });
        this.view_district.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] districts = (String[]) publishConfig.getSell_config().get(HouseBaseInfo.DISTRICT).toArray(new String[0]);
                CorePreferences.DEBUG("****districts*****" + publishConfig.getSell_config().get(HouseBaseInfo.DISTRICT).toArray());
                AlertDialog.Builder builder = new AlertDialog.Builder(SecondRentPublishActivity.this.context);
                int access$16 = SecondRentPublishActivity.this.districtValue;
                final HouseBaseInfo houseBaseInfo = publishConfig;
                builder.setSingleChoiceItems(districts, access$16, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which >= 0) {
                            SecondRentPublishActivity.this.districtValue = which;
                            SecondRentPublishActivity.this.publishVal.put(HouseBaseInfo.DISTRICT, new StringBuilder(String.valueOf(SecondRentPublishActivity.this.districtValue)).toString());
                            if (which == 0) {
                                SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_district, districts[SecondRentPublishActivity.this.districtValue]);
                                dialog.dismiss();
                                return;
                            }
                            if (which > 0) {
                                final Map<String, String> districtStreetMap = houseBaseInfo.getDistrictStreet().get(districts[SecondRentPublishActivity.this.districtValue]);
                                final String[] districtStreets = (String[]) districtStreetMap.keySet().toArray(new String[0]);
                                AlertDialog.Builder builder = new AlertDialog.Builder(SecondRentPublishActivity.this.context);
                                int parseValueToIndex = SecondRentPublishActivity.parseValueToIndex(districtStreets, HouseBaseInfo.parseValueToKey(districtStreetMap, SecondRentPublishActivity.this.streetValue));
                                final String[] strArr = districts;
                                builder.setSingleChoiceItems(districtStreets, parseValueToIndex, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which >= 0) {
                                            String key = districtStreets[which];
                                            SecondRentPublishActivity.this.streetValue = (String) districtStreetMap.get(key);
                                            SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_district, String.valueOf(strArr[SecondRentPublishActivity.this.districtValue]) + "/" + districtStreets[which]);
                                            SecondRentPublishActivity.this.publishVal.put("streetid", SecondRentPublishActivity.this.streetValue);
                                        }
                                        dialog.dismiss();
                                    }
                                }).setTitle((int) R.string.street).create().show();
                            }
                            dialog.dismiss();
                        }
                    }
                }).setTitle((int) R.string.district).create().show();
            }
        });
        this.view_roomtype.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = (String[]) publishConfig.getSell_config().get(HouseBaseInfo.ROOM).toArray(new String[0]);
                ActivityUtil.showSingleChooseDialog(SecondRentPublishActivity.this.context, (int) R.string.text_roomtype, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondRentPublishActivity.this.publishVal.put(HouseBaseInfo.ROOM, new StringBuilder(String.valueOf(which)).toString());
                        SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_roomtype, chooseItems[which]);
                    }
                });
            }
        });
        this.view_rent_way.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = TextUtil.arrayToShift((String[]) publishConfig.getSell_config().get(HouseBaseInfo.RENTTYPE).toArray(new String[0]));
                ActivityUtil.showSingleChooseDialog(SecondRentPublishActivity.this.context, (int) R.string.text_rent_way, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondRentPublishActivity.this.publishVal.put(HouseBaseInfo.RENTTYPE, new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_rent_way, chooseItems[which]);
                    }
                });
            }
        });
        this.view_rent_price.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] chooseItems = TextUtil.arrayToShift((String[]) SecondRentPublishActivity.this.getPriceChooseItem(TextUtil.strToInt((String) SecondRentPublishActivity.this.publishVal.get(HouseBaseInfo.INFOTYPE)).intValue(), publishConfig));
                ActivityUtil.showSingleChooseDialog(SecondRentPublishActivity.this.context, (int) R.string.text_rent_price, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondRentPublishActivity.this.publishVal.put("price", new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_rent_price, chooseItems[which]);
                    }
                });
            }
        });
        this.view_buildarea.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] chooseItems = TextUtil.arrayToShift((String[]) SecondRentPublishActivity.this.getBuildAreaChooseItem(TextUtil.strToInt((String) SecondRentPublishActivity.this.publishVal.get(HouseBaseInfo.INFOTYPE)).intValue(), publishConfig));
                ActivityUtil.showSingleChooseDialog(SecondRentPublishActivity.this.context, (int) R.string.text_buildarea, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondRentPublishActivity.this.publishVal.put("buildarea", new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondRentPublishActivity.this.setChooseTextView(SecondRentPublishActivity.this.tv_buildarea, chooseItems[which]);
                    }
                });
            }
        });
        this.btn_captcha.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String telno = SecondRentPublishActivity.this.et_tel.getText().toString();
                if (!RegexUtil.isMobileNumber(telno)) {
                    SecondRentPublishActivity.this.showToast((int) R.string.text_mobile_error);
                    return;
                }
                GetValidateTask validateTask = new GetValidateTask(SecondRentPublishActivity.this.context, telno, SecondRentPublishActivity.this.publishType);
                validateTask.setOnSuccessListener(new GetValidateTask.ValidateOnSuccessListener() {
                    public void OnSuccess(CommonResultInfo v) {
                        SecondRentPublishActivity.this.mctime = new MyCountimer(DateUtils.MILLIS_PER_MINUTE, 1000);
                        SecondRentPublishActivity.this.mctime.start();
                        SecondRentPublishActivity.this.btn_captcha.setEnabled(false);
                        SecondRentPublishActivity.this.validateCode = v.getData();
                    }
                });
                validateTask.execute(new Object[0]);
            }
        });
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            SecondRentPublishActivity.this.btn_captcha.setEnabled(true);
            SecondRentPublishActivity.this.btn_captcha.setText((int) R.string.btn_captcha);
        }

        public void onTick(long millisUntilFinished) {
            SecondRentPublishActivity.this.btn_captcha.setText(TextUtil.getString(SecondRentPublishActivity.this.context, R.string.text_captcha_countdown, Long.valueOf(millisUntilFinished / 1000)));
        }
    }

    /* access modifiers changed from: private */
    public CharSequence[] getPriceChooseItem(int channelIndex, HouseBaseInfo publishConfig) {
        switch (channelIndex) {
            case 0:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 1:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 2:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 3:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_OFFICE).toArray(new String[0]);
            case 4:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_STORE).toArray(new String[0]);
            default:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
        }
    }

    /* access modifiers changed from: private */
    public CharSequence[] getBuildAreaChooseItem(int channelIndex, HouseBaseInfo publishConfig) {
        switch (channelIndex) {
            case 0:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 1:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 2:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 3:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_OFFICE).toArray(new String[0]);
            case 4:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_STORE).toArray(new String[0]);
            default:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mctime != null) {
            this.mctime.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.house = (PublishHouse) getIntent().getSerializableExtra(MyPublishActivity.INTENT_HOUSE);
        if (this.house != null) {
            this.view_channel.setEnabled(false);
            this.view_district.setEnabled(false);
            this.view_roomtype.setEnabled(false);
            this.view_buildarea.setEnabled(false);
            this.view_title.setEnabled(false);
            this.view_contact.setEnabled(false);
            this.view_tel.setEnabled(false);
            this.view_rent_price.setEnabled(false);
            this.view_rent_price.setEnabled(false);
            this.view_rent_way.setEnabled(false);
            this.et_title.setEnabled(false);
            this.et_contact.setEnabled(false);
            this.et_tel.setEnabled(false);
            this.et_captcha.setEnabled(false);
            this.btn_relogin.setVisibility(4);
            this.btn_captcha.setVisibility(4);
            this.view_captcha.setVisibility(4);
            this.btn_publish.setVisibility(8);
            this.tv_channel.setText(this.house.getInfotype());
            this.tv_district.setText(this.house.getDistrict());
            this.tv_roomtype.setText(this.house.getRoom());
            this.tv_buildarea.setText(this.house.getBuildarea());
            this.tv_rent_price.setText(this.house.getPrice());
            this.tv_rent_way.setText(this.house.getRenttype());
            this.et_title.setText(this.house.getTitle());
            this.et_contact.setText(this.house.getContactor());
            this.et_tel.setText(this.house.getTelno());
            return;
        }
        this.btn_relogin.setVisibility(0);
        this.btn_captcha.setVisibility(0);
        this.view_captcha.setVisibility(0);
        this.btn_publish.setVisibility(0);
        String mobile = this.mApplication.getMobile();
        if (!TextUtils.isEmpty(mobile)) {
            this.et_tel.setText(mobile);
        } else {
            this.et_tel.setText(StringUtils.EMPTY);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        AppMethods.exitPublish(this.house, this.publishVal, this.context);
        return false;
    }
}
