package com.house365.newhouse.ui.tools;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.tools.LoanCalActivity;
import com.house365.newhouse.ui.util.MathUtil;

public class LoanCalMethodOneResultActivity extends BaseCommonActivity {
    private TextView tvLoanTimesBusiness;
    private TextView tvLoanTimesFound;
    private TextView tvMonthRepayBusiness;
    private TextView tvMonthRepayFound;
    private TextView tvTotalRateAll;
    private TextView tvTotalRateBusiness;
    private TextView tvTotalRateFound;
    private TextView tvTotalRepayAll;
    private TextView tvTotalRepayBusiness;
    private TextView tvTotalRepayFound;
    private TextView tvUserLoanMoneyAll;
    private TextView tvUserLoanMoneyBusiness;
    private TextView tvUserLoanMoneyFound;
    private LinearLayout userLoanBusinessResultLayout;
    private LinearLayout userLoanFoundResultLayout;
    private LinearLayout userLoanResultLayout;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.loan_cal_method_one_result_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        ((HeadNavigateView) findViewById(R.id.head_view)).getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoanCalMethodOneResultActivity.this.finish();
            }
        });
        this.userLoanResultLayout = (LinearLayout) findViewById(R.id.userLoanResultLayout);
        this.tvUserLoanMoneyAll = (TextView) findViewById(R.id.tvUserLoanMoneyAll);
        this.tvTotalRepayAll = (TextView) findViewById(R.id.tvTotalRepayAll);
        this.tvTotalRateAll = (TextView) findViewById(R.id.tvTotalRateAll);
        this.userLoanBusinessResultLayout = (LinearLayout) findViewById(R.id.userLoanBusinessResultLayout);
        this.tvUserLoanMoneyBusiness = (TextView) findViewById(R.id.tvUserLoanMoneyBusiness);
        this.tvTotalRepayBusiness = (TextView) findViewById(R.id.tvTotalRepayBusiness);
        this.tvTotalRateBusiness = (TextView) findViewById(R.id.tvTotalRateBusiness);
        this.tvLoanTimesBusiness = (TextView) findViewById(R.id.tvLoanTimesBusiness);
        this.tvMonthRepayBusiness = (TextView) findViewById(R.id.tvMonthRepayBusiness);
        this.userLoanFoundResultLayout = (LinearLayout) findViewById(R.id.userLoanFoundResultLayout);
        this.tvUserLoanMoneyFound = (TextView) findViewById(R.id.tvUserLoanMoneyFound);
        this.tvTotalRepayFound = (TextView) findViewById(R.id.tvTotalRepayFound);
        this.tvTotalRateFound = (TextView) findViewById(R.id.tvTotalRateFound);
        this.tvLoanTimesFound = (TextView) findViewById(R.id.tvLoanTimesFound);
        this.tvMonthRepayFound = (TextView) findViewById(R.id.tvMonthRepayFound);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        LoanCalResultMethodOne loanCalResult = (LoanCalResultMethodOne) getIntent().getSerializableExtra(LoanCalResultMethodOne.INTENT_NAME);
        if (loanCalResult.getLoanType() == LoanCalActivity.LoanType.BUSINESS) {
            this.userLoanResultLayout.setVisibility(8);
            this.userLoanBusinessResultLayout.setVisibility(0);
            this.userLoanFoundResultLayout.setVisibility(8);
            this.tvUserLoanMoneyBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyBusiness())));
            this.tvTotalRepayBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayBusiness())));
            this.tvTotalRateBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateBusiness())));
            this.tvLoanTimesBusiness.setText(getResources().getString(R.string.cal_result_times, Integer.valueOf(loanCalResult.getLoantimesBusiness())));
            this.tvMonthRepayBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getMonthRepayBusiness())));
        } else if (loanCalResult.getLoanType() == LoanCalActivity.LoanType.FOUND) {
            this.userLoanResultLayout.setVisibility(8);
            this.userLoanBusinessResultLayout.setVisibility(8);
            this.userLoanFoundResultLayout.setVisibility(0);
            this.tvUserLoanMoneyFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyFound())));
            this.tvTotalRepayFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayFound())));
            this.tvTotalRateFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateFound())));
            this.tvLoanTimesFound.setText(getResources().getString(R.string.cal_result_times, Integer.valueOf(loanCalResult.getLoantimesFound())));
            this.tvMonthRepayFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getMonthRepayFound())));
        } else {
            this.userLoanResultLayout.setVisibility(0);
            this.userLoanBusinessResultLayout.setVisibility(0);
            this.userLoanFoundResultLayout.setVisibility(0);
            this.tvUserLoanMoneyAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getUserLoanMoneyBusiness() + loanCalResult.getUserLoanMoneyFound(), 2))));
            this.tvTotalRepayAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getTotalRepayBusiness() + loanCalResult.getTotalRepayFound(), 2))));
            this.tvTotalRateAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getTotalRateBusiness() + loanCalResult.getTotalRateFound(), 2))));
            this.tvUserLoanMoneyBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyBusiness())));
            this.tvTotalRepayBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayBusiness())));
            this.tvTotalRateBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateBusiness())));
            this.tvLoanTimesBusiness.setText(getResources().getString(R.string.cal_result_times, Integer.valueOf(loanCalResult.getLoantimesBusiness())));
            this.tvMonthRepayBusiness.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getMonthRepayBusiness())));
            this.tvUserLoanMoneyFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyFound())));
            this.tvTotalRepayFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayFound())));
            this.tvTotalRateFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateFound())));
            this.tvLoanTimesFound.setText(getResources().getString(R.string.cal_result_times, Integer.valueOf(loanCalResult.getLoantimesFound())));
            this.tvMonthRepayFound.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getMonthRepayFound())));
        }
    }
}
