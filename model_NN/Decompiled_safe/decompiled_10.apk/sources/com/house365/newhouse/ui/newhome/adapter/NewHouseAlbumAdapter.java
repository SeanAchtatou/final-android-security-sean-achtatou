package com.house365.newhouse.ui.newhome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Album;

public class NewHouseAlbumAdapter extends BaseCacheListAdapter<Album> {
    private LayoutInflater inflater;
    private Context mContext;

    public NewHouseAlbumAdapter(Context context) {
        super(context);
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder(null);
            convertView = this.inflater.inflate((int) R.layout.item_grid_album, (ViewGroup) null);
            holder.album_pic = (ImageView) convertView.findViewById(R.id.album_pic);
            holder.album_title = (TextView) convertView.findViewById(R.id.album_title);
            holder.album_num = (TextView) convertView.findViewById(R.id.album_num);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Album album = (Album) getItem(position);
        setCacheImage(holder.album_pic, album.getA_thumb(), R.drawable.bg_default_img_detail, 1);
        holder.album_num.setText(this.mContext.getResources().getString(R.string.text_house_photo_count, Integer.valueOf(album.getA_count())));
        holder.album_title.setText(album.getA_name());
        return convertView;
    }

    private static class ViewHolder {
        TextView album_num;
        ImageView album_pic;
        TextView album_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
