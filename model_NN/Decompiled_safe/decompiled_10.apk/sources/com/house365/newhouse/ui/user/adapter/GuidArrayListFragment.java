package com.house365.newhouse.ui.user.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.baidu.mapapi.MKEvent;
import com.house365.core.constant.CorePreferences;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.CityChangeActivity;
import org.apache.commons.lang.SystemUtils;

public class GuidArrayListFragment extends Fragment {
    /* access modifiers changed from: private */
    public int ANIMATION_SHOW_DURATION = MKEvent.ERROR_PERMISSION_DENIED;
    /* access modifiers changed from: private */
    public Activity activity;
    View btn_guide_enter;
    int mNum;
    View step_1_house;
    View step_1_map;
    View step_2_car;
    View step_2_cloud_left;
    View step_2_cloud_right;
    View step_2_house;
    View step_3_bg;
    View step_3_buy;
    View step_3_person;
    private ViewPager vp;

    public static final GuidArrayListFragment newInstance(Activity activity2, ViewPager vp2, int num) {
        GuidArrayListFragment f = new GuidArrayListFragment();
        f.vp = vp2;
        f.activity = activity2;
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);
        return f;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mNum = getArguments() != null ? getArguments().getInt("num") : 0;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.mNum == 0) {
            View v = inflater.inflate((int) R.layout.step_1_guide_item, (ViewGroup) null);
            this.step_1_map = v.findViewById(R.id.step_1_map);
            this.step_1_house = v.findViewById(R.id.step_1_house);
            return v;
        } else if (this.mNum == 1) {
            View v2 = inflater.inflate((int) R.layout.step_2_guide_item, (ViewGroup) null);
            this.step_2_house = v2.findViewById(R.id.step_2_house);
            this.step_2_cloud_left = v2.findViewById(R.id.step_2_cloud_left);
            this.step_2_cloud_right = v2.findViewById(R.id.step_2_cloud_right);
            this.step_2_car = v2.findViewById(R.id.step_2_car);
            return v2;
        } else if (this.mNum != 2) {
            return null;
        } else {
            View v3 = inflater.inflate((int) R.layout.step_3_guide_item, (ViewGroup) null);
            this.step_3_bg = v3.findViewById(R.id.step_3_bg);
            this.step_3_buy = v3.findViewById(R.id.step_3_buy);
            this.step_3_person = v3.findViewById(R.id.step_3_person);
            this.btn_guide_enter = v3.findViewById(R.id.btn_guide_enter);
            this.btn_guide_enter.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(GuidArrayListFragment.this.activity, CityChangeActivity.class);
                    intent.putExtra("isFirst", 1);
                    GuidArrayListFragment.this.startActivity(intent);
                    GuidArrayListFragment.this.activity.finish();
                }
            });
            return v3;
        }
    }

    public void onDetach() {
        super.onDetach();
        CorePreferences.DEBUG(String.valueOf(this.mNum) + ":onDetach");
    }

    public void onStart() {
        super.onStart();
        CorePreferences.DEBUG(String.valueOf(this.mNum) + ":onStart");
    }

    public void onResume() {
        super.onResume();
        CorePreferences.DEBUG(String.valueOf(this.mNum) + ":onResume");
        if (this.vp.getCurrentItem() == this.mNum) {
            startAnim(this.mNum);
        }
    }

    public void onPause() {
        super.onPause();
        CorePreferences.DEBUG(String.valueOf(this.mNum) + ":onPause");
    }

    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        CorePreferences.DEBUG(String.valueOf(this.mNum) + ":hidden:" + hidden);
    }

    public void startAnim(int position) {
        if (position == 0) {
            startAnim0();
        } else if (position == 1) {
            startAnim1();
        } else if (position == 2) {
            startAnim2();
        }
    }

    public void startAnim0() {
        ScaleAnimation step_1_mapAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        step_1_mapAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        step_1_mapAnim.setFillAfter(true);
        this.step_1_map.startAnimation(step_1_mapAnim);
        ScaleAnimation step_1_houseAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        step_1_houseAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        step_1_houseAnim.setStartOffset(300);
        step_1_houseAnim.setFillAfter(true);
        this.step_1_house.startAnimation(step_1_houseAnim);
    }

    public void startAnim1() {
        this.step_2_house.clearAnimation();
        this.step_2_cloud_left.clearAnimation();
        this.step_2_cloud_right.clearAnimation();
        this.step_2_car.clearAnimation();
        ScaleAnimation step_2_houseAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        step_2_houseAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        this.step_2_house.startAnimation(step_2_houseAnim);
        TranslateAnimation step_2_carAnim = new TranslateAnimation(2, -1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
        step_2_carAnim.setInterpolator(new OvershootInterpolator());
        step_2_carAnim.setDuration((long) (this.ANIMATION_SHOW_DURATION * 2));
        this.step_2_car.startAnimation(step_2_carAnim);
        TranslateAnimation step_2_cloud_leftAnim = new TranslateAnimation(2, -1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
        step_2_cloud_leftAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        step_2_cloud_leftAnim.setStartOffset(300);
        this.step_2_cloud_left.startAnimation(step_2_cloud_leftAnim);
        step_2_cloud_leftAnim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                TranslateAnimation step_2_cloud_rightAnim = new TranslateAnimation(2, 1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
                step_2_cloud_rightAnim.setDuration((long) GuidArrayListFragment.this.ANIMATION_SHOW_DURATION);
                step_2_cloud_rightAnim.setStartOffset(200);
                GuidArrayListFragment.this.step_2_cloud_right.startAnimation(step_2_cloud_rightAnim);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }
        });
    }

    public void startAnim2() {
        ScaleAnimation step_3_bgAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        step_3_bgAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        this.step_3_bg.startAnimation(step_3_bgAnim);
        ScaleAnimation step_3_buyAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        step_3_buyAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        step_3_buyAnim.setStartOffset(300);
        this.step_3_buy.startAnimation(step_3_buyAnim);
        new AnimationSet(false);
        TranslateAnimation step_3_personTransAnim = new TranslateAnimation(2, -1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
        step_3_personTransAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        step_3_personTransAnim.setInterpolator(new OvershootInterpolator());
        step_3_personTransAnim.setStartOffset(600);
        this.step_3_person.startAnimation(step_3_personTransAnim);
        ScaleAnimation btn_guide_enterAnim = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.2f, SystemUtils.JAVA_VERSION_FLOAT, 1.2f, 1, 0.5f, 1, 0.5f);
        btn_guide_enterAnim.setDuration((long) this.ANIMATION_SHOW_DURATION);
        btn_guide_enterAnim.setStartOffset(900);
        this.btn_guide_enter.startAnimation(btn_guide_enterAnim);
    }
}
