package com.house365.newhouse.ui.privilege;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.MyHtmlTagHandler;
import com.house365.core.util.TextUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.task.GroupTask;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.newhome.HouseIntroActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class BargainlHouseActivity extends BaseCommonActivity {
    public static final String INTENT_EID = "e_id";
    private static final int REQUET_FAV = 4;
    private static final int REQUET_TJF_APPLY = 1;
    /* access modifiers changed from: private */
    public TextView btn_apply;
    private TextView btn_refer;
    /* access modifiers changed from: private */
    public Event curEvent;
    private long curtime;
    private RelativeLayout descriptioninfo_detail_layout;
    private LinearLayout descriptioninfo_layout;
    private TextView e_build_area;
    /* access modifiers changed from: private */
    public TextView e_count_down_time;
    private TextView e_descriptioninfo;
    /* access modifiers changed from: private */
    public String e_id;
    private TextView e_intro;
    private TextView e_join_num;
    private TextView e_noteinfo;
    private ImageView e_pic;
    private LinearLayout e_project_intro_layout;
    private RelativeLayout e_project_intro_title_layout;
    private TextView e_room_num;
    private TextView e_sale_price;
    private TextView e_tjf_price;
    private TextView e_tjf_price_prefix;
    private String event_build_area;
    /* access modifiers changed from: private */
    public String event_count_down_time;
    private String event_intro;
    /* access modifiers changed from: private */
    public String event_intro_cut;
    private String event_join_num;
    private String event_original;
    private String event_room_num;
    /* access modifiers changed from: private */
    public String event_sale_phone;
    private String event_tjf_price;
    private TextView gray_line;
    private TextView h_build_type;
    private TextView h_deli_date;
    private TextView h_deli_standard;
    private TextView h_kfs;
    private TextView h_project_address;
    private HeadNavigateView head_view;
    List<HouseInfo> houseInfos = new ArrayList();
    private String house_build_type;
    private String house_deli_date;
    private String house_deli_standard;
    private String house_kfs;
    private String house_name;
    private TextView house_name_in;
    private RelativeLayout house_name_in_layout;
    private String house_project_address;
    private String house_room_num;
    private ScrollView house_scroll;
    /* access modifiers changed from: private */
    public long intCountDownTime;
    boolean isNetCon;
    private boolean isShowPic;
    private NewHouseApplication mApplication;
    private MyCountimer mctime;
    private RelativeLayout noteinfo_detail_layout;
    private LinearLayout noteinfo_layout;
    WindowManager.LayoutParams params = new WindowManager.LayoutParams();
    private int pic_count = 0;
    /* access modifiers changed from: private */
    public String type = App.Categroy.Event.TJF;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            String mobile = this.mApplication.getMobile();
            if (this.curEvent != null) {
                new GroupTask(this, this.mApplication, this.curEvent.getE_id(), App.Categroy.Event.TJF, null, mobile).execute(new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.bargain_house);
        this.mApplication = (NewHouseApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BargainlHouseActivity.this.finish();
            }
        });
        this.btn_apply = (TextView) findViewById(R.id.btn_apply);
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.house_scroll = (ScrollView) findViewById(R.id.house_scroll);
        this.btn_apply.setText(getString(R.string.apply_privilege));
        this.btn_apply.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.apply, 0, 0, 0);
        this.btn_refer.setText(getString(R.string.contact_house_sell));
        this.e_tjf_price = (TextView) findViewById(R.id.h_tjf_price);
        this.e_sale_price = (TextView) findViewById(R.id.h_sale_price);
        this.e_tjf_price_prefix = (TextView) findViewById(R.id.h_tjf_price_prefix);
        this.gray_line = (TextView) findViewById(R.id.gray_line);
        this.e_room_num = (TextView) findViewById(R.id.h_room_num);
        this.e_build_area = (TextView) findViewById(R.id.h_build_area);
        this.e_join_num = (TextView) findViewById(R.id.h_join_num);
        this.e_count_down_time = (TextView) findViewById(R.id.h_count_down_time);
        this.house_name_in = (TextView) findViewById(R.id.house_name_in);
        this.h_project_address = (TextView) findViewById(R.id.h_project_address);
        this.h_kfs = (TextView) findViewById(R.id.h_kfs);
        this.h_build_type = (TextView) findViewById(R.id.h_build_type);
        this.h_deli_date = (TextView) findViewById(R.id.h_deli_date);
        this.h_deli_standard = (TextView) findViewById(R.id.h_deli_standard);
        this.e_intro = (TextView) findViewById(R.id.h_intro);
        this.e_descriptioninfo = (TextView) findViewById(R.id.h_descriptioninfo);
        this.e_noteinfo = (TextView) findViewById(R.id.h_noteinfo);
        this.e_project_intro_layout = (LinearLayout) findViewById(R.id.h_project_intro_layout);
        this.e_project_intro_title_layout = (RelativeLayout) findViewById(R.id.h_project_intro_title_layout);
        this.house_name_in_layout = (RelativeLayout) findViewById(R.id.house_name_in_layout);
        this.descriptioninfo_detail_layout = (RelativeLayout) findViewById(R.id.descriptioninfo_detail_layout);
        this.noteinfo_detail_layout = (RelativeLayout) findViewById(R.id.noteinfo_detail_layout);
        this.e_pic = (ImageView) findViewById(R.id.h_pic);
        this.descriptioninfo_layout = (LinearLayout) findViewById(R.id.descriptioninfo_layout);
        this.noteinfo_layout = (LinearLayout) findViewById(R.id.noteinfo_layout);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.e_id = getIntent().getStringExtra("e_id");
        this.isShowPic = this.mApplication.isEnableImg();
        new GetHouseDetail(this).execute(new Object[0]);
        addBottomListener();
    }

    /* access modifiers changed from: private */
    public void ensureUi(Event event) {
        this.house_scroll.smoothScrollTo(0, 0);
        new MyHtmlTagHandler();
        this.curEvent = event;
        final House e_house = event.getE_house();
        this.head_view.setTvTitleText(event.getE_title());
        this.event_room_num = getResources().getString(R.string.text_pub_room, this.curEvent.getE_roomtype());
        this.event_build_area = getResources().getString(R.string.text_pub_build_area, this.curEvent.getE_area());
        this.event_join_num = getResources().getString(R.string.text_pub_follow_input, event.getE_join());
        this.event_tjf_price = getResources().getString(R.string.text_tjf_price, event.getE_s_price());
        this.event_original = getResources().getString(R.string.text_original_price, event.getE_o_price());
        this.house_name = getResources().getString(R.string.text_house_in_name, e_house.getH_name());
        this.house_project_address = getResources().getString(R.string.text_field_project_address, e_house.getH_project_address());
        this.house_kfs = getResources().getString(R.string.text_field_kfs, e_house.getH_kfs());
        this.house_deli_standard = getResources().getString(R.string.text_field_decoration_states, e_house.getH_deli_standard());
        this.house_deli_date = getResources().getString(R.string.text_field_deli_date, e_house.getH_deli_date());
        this.house_build_type = getResources().getString(R.string.text_field_build_type, e_house.getH_build_type());
        this.event_sale_phone = event.getE_tel();
        if (this.event_sale_phone == null || TextUtils.isEmpty(this.event_sale_phone)) {
            this.event_sale_phone = AppArrays.default_tel;
        }
        this.btn_refer.setText(this.event_sale_phone);
        TextUtil.setNullText(event.getE_s_price(), this.event_tjf_price, this.e_tjf_price);
        TextUtil.setNullText(event.getE_o_price(), this.event_original, this.e_sale_price);
        TextUtil.setNullText(event.getE_o_price(), this.gray_line);
        TextUtil.setNullText(this.curEvent.getE_roomtype(), this.event_room_num, this.e_room_num);
        TextUtil.setNullText(this.curEvent.getE_area(), this.event_build_area, this.e_build_area);
        TextUtil.setNullText(event.getE_join(), this.event_join_num, this.e_join_num);
        TextUtil.setNullText(e_house.getH_name(), this.house_name, this.house_name_in);
        TextUtil.setNullText(e_house.getH_kfs(), this.house_kfs, this.h_kfs);
        TextUtil.setNullText(e_house.getH_build_type(), this.house_build_type, this.h_build_type);
        TextUtil.setNullText(e_house.getH_deli_date(), this.house_deli_date, this.h_deli_date);
        TextUtil.setNullText(e_house.getH_deli_standard(), this.house_deli_standard, this.h_deli_standard);
        TextUtil.setNullText(e_house.getH_project_address(), this.house_project_address, this.h_project_address);
        this.e_pic.setVisibility(0);
        if (!this.isShowPic) {
            setImage(this.e_pic, StringUtils.EMPTY, (int) R.drawable.bg_default_ad, 1);
            this.e_pic.setEnabled(false);
        } else {
            setImage(this.e_pic, event.getE_pic(), (int) R.drawable.bg_default_ad, 1);
            if (!(e_house == null || e_house.getH_id() == null)) {
                this.house_name_in_layout.setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void onClick(View v) {
                        Intent intent = new Intent(BargainlHouseActivity.this, NewHouseDetailActivity.class);
                        intent.putExtra("h_id", e_house.getH_id());
                        intent.putExtra(NewHouseDetailActivity.INTENT_DISAPPER_EVENT, true);
                        BargainlHouseActivity.this.startActivity(intent);
                    }
                });
            }
            if (this.curEvent.getE_intro() == null || TextUtils.isEmpty(this.curEvent.getE_intro())) {
                this.e_project_intro_layout.setVisibility(8);
                this.e_intro.setText(StringUtils.EMPTY);
            } else {
                this.event_intro_cut = TextUtil.substring(event.getE_intro(), 150, "...");
                this.e_intro.setText(Html.fromHtml(this.event_intro_cut));
                this.e_project_intro_layout.setVisibility(0);
                this.e_project_intro_title_layout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(BargainlHouseActivity.this.event_intro_cut)) {
                            Intent intent = new Intent(BargainlHouseActivity.this, HouseIntroActivity.class);
                            intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, BargainlHouseActivity.this.getResources().getString(R.string.text_field_house_intro));
                            intent.putExtra("info", BargainlHouseActivity.this.curEvent.getE_intro());
                            BargainlHouseActivity.this.startActivity(intent);
                        }
                    }
                });
            }
            if (this.curEvent.getE_explain() == null || TextUtils.isEmpty(this.curEvent.getE_explain())) {
                this.descriptioninfo_layout.setVisibility(8);
                this.e_descriptioninfo.setText(StringUtils.EMPTY);
            } else {
                this.e_descriptioninfo.setText(Html.fromHtml(TextUtil.substring(this.curEvent.getE_explain(), 150, "...")));
                this.descriptioninfo_layout.setVisibility(0);
                this.descriptioninfo_detail_layout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(BargainlHouseActivity.this, HouseIntroActivity.class);
                        intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, BargainlHouseActivity.this.getResources().getString(R.string.text_house_online_title));
                        intent.putExtra("info", BargainlHouseActivity.this.curEvent.getE_explain());
                        BargainlHouseActivity.this.startActivity(intent);
                    }
                });
            }
            if (this.curEvent.getE_remark() != null || !TextUtils.isEmpty(this.curEvent.getE_remark())) {
                this.e_noteinfo.setText(Html.fromHtml(TextUtil.substring(this.curEvent.getE_remark(), 150, "...")));
                this.noteinfo_layout.setVisibility(0);
                this.noteinfo_detail_layout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(BargainlHouseActivity.this, HouseIntroActivity.class);
                        intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, BargainlHouseActivity.this.getResources().getString(R.string.text_group_offline_title));
                        intent.putExtra("info", BargainlHouseActivity.this.curEvent.getE_remark());
                        BargainlHouseActivity.this.startActivity(intent);
                    }
                });
            } else {
                this.noteinfo_layout.setVisibility(8);
                this.e_noteinfo.setText(StringUtils.EMPTY);
            }
        }
        this.curtime = System.currentTimeMillis();
        this.intCountDownTime = (event.getE_endtime() * 1000) - this.curtime;
        this.mctime = new MyCountimer(this.intCountDownTime, 1000);
        this.mctime.start();
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            BargainlHouseActivity.this.e_count_down_time.setText((int) R.string.text_bargain_event_over);
            BargainlHouseActivity.this.btn_apply.setEnabled(false);
        }

        public void onTick(long millisUntilFinished) {
            if (BargainlHouseActivity.this.intCountDownTime > 0) {
                BargainlHouseActivity.this.event_count_down_time = TimeUtil.formatLongToTimeStr(Long.valueOf(millisUntilFinished), "dd天HH小时mm分钟ss秒");
                if (millisUntilFinished > 0) {
                    BargainlHouseActivity.this.e_count_down_time.setText(Html.fromHtml(BargainlHouseActivity.this.event_count_down_time));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mctime != null) {
            this.mctime.cancel();
        }
    }

    /* access modifiers changed from: private */
    public void joinEvent() {
        String mobile = this.mApplication.getMobile();
        if (mobile == null || TextUtils.isEmpty(mobile)) {
            Intent intent = new Intent(this, UserLoginActivity.class);
            intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 2);
            startActivityForResult(intent, 1);
        } else if (this.curEvent != null) {
            new GroupTask(this, this.mApplication, this.curEvent.getE_id(), App.Categroy.Event.TJF, null, mobile).execute(new Object[0]);
        }
    }

    private void addBottomListener() {
        this.btn_apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BargainlHouseActivity.this.curEvent != null) {
                    BargainlHouseActivity.this.joinEvent();
                    HouseAnalyse.onViewClick(BargainlHouseActivity.this, "报名", App.Categroy.Event.TJF, BargainlHouseActivity.this.curEvent.getE_id());
                }
            }
        });
        this.btn_refer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BargainlHouseActivity.this.event_sale_phone != null) {
                    try {
                        TelUtil.getCallIntent(BargainlHouseActivity.this.event_sale_phone, BargainlHouseActivity.this, App.Categroy.Event.TJF);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private class GetHouseDetail extends CommonAsyncTask<Event> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseDetail(Context context) {
            super(context);
            this.loadingresid = R.string.loading;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(Event event) {
            if (event == null || TextUtils.isEmpty(event.getE_id())) {
                BargainlHouseActivity.this.showToast((int) R.string.msg_load_error);
                BargainlHouseActivity.this.finish();
                return;
            }
            BargainlHouseActivity.this.ensureUi(event);
        }

        public Event onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getEventDetail(BargainlHouseActivity.this.e_id, BargainlHouseActivity.this.type);
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            BargainlHouseActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            BargainlHouseActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            BargainlHouseActivity.this.finish();
        }
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
