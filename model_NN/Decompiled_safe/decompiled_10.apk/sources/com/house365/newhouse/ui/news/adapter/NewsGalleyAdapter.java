package com.house365.newhouse.ui.news.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;

public class NewsGalleyAdapter extends BaseCacheListAdapter<String> {
    public static int imgWidth = 160;
    private Context context;
    private LayoutInflater inflater;

    public NewsGalleyAdapter(Context context2) {
        super(context2);
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.news_gallery_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.album_pic = (ImageView) convertView.findViewById(R.id.album_pic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) holder.album_pic.getLayoutParams();
        lp.width = imgWidth;
        lp.height = (imgWidth / 4) * 3;
        holder.album_pic.setLayoutParams(lp);
        String src = (String) getItem(position);
        if (!TextUtils.isEmpty(src)) {
            setCacheImage(holder.album_pic, src, R.drawable.bg_default_img_detail, 1);
        }
        return convertView;
    }

    class ViewHolder {
        ImageView album_pic;

        ViewHolder() {
        }
    }
}
