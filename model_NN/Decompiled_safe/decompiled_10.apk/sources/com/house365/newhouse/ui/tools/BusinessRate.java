package com.house365.newhouse.ui.tools;

/* compiled from: LoanCalActivity */
class BusinessRate extends CalBean {
    private double rate12;
    private double rate36;
    private double rate360;
    private double rate60;

    public BusinessRate(String key, double rate122, double rate362, double rate602, double rate3602) {
        this.key = key;
        this.rate12 = rate122;
        this.rate36 = rate362;
        this.rate60 = rate602;
        this.rate360 = rate3602;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getRate12() {
        return this.rate12;
    }

    public void setRate12(double rate122) {
        this.rate12 = rate122;
    }

    public double getRate36() {
        return this.rate36;
    }

    public void setRate36(double rate362) {
        this.rate36 = rate362;
    }

    public double getRate60() {
        return this.rate60;
    }

    public void setRate60(double rate602) {
        this.rate60 = rate602;
    }

    public double getRate360() {
        return this.rate360;
    }

    public void setRate360(double rate3602) {
        this.rate360 = rate3602;
    }

    public double getRateByYear(LoanYears loanYears) {
        if (loanYears.getTimes() <= 12) {
            return getRate12();
        }
        if (loanYears.getTimes() <= 36) {
            return getRate36();
        }
        if (loanYears.getTimes() <= 60) {
            return getRate60();
        }
        return getRate360();
    }
}
