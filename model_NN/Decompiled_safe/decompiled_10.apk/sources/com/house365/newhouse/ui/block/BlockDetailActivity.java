package com.house365.newhouse.ui.block;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.util.TextUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.ScrollViewHorzFling;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.secondsell.SecondSell_B_otherActivity;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

public class BlockDetailActivity extends BaseBaiduMapActivity {
    public static final String INTENT_BLOCKID = "rent_blockid";
    public static final String INTENT_ID = "id";
    public static final String INTENT_RENT_TYPE = "rent_app";
    private ImageView B_pic;
    private int app;
    private View b_other_layout;
    private String blockId;
    private View block_address_layout;
    Block block_detail;
    private TextView block_house_rent_count;
    private View block_house_rent_layout;
    private TextView block_house_sell_count;
    private View block_house_sell_layout;
    private TextView block_house_sell_price;
    private View block_map_address_layout;
    private RelativeLayout block_pic_layout;
    private TextView block_street;
    private TextView block_title;
    /* access modifiers changed from: private */
    public HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public String id;
    boolean isShowPic;
    private MapView mMapView;
    private TextView more_h_pic;
    private View project_introduct_layout;
    private TextView text_district;
    private TextView tv_block_address;
    private TextView tv_block_company;
    private TextView tv_block_developers;
    private TextView tv_block_fees;
    private TextView tv_block_type;
    private TextView txt_expand_block_bo_ther;
    private TextView txt_expand_block_instruct;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.block_detail_layout);
    }

    public MapView getMapView() {
        return this.mMapView;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockDetailActivity.this.finish();
            }
        });
        ((ScrollViewHorzFling) findViewById(R.id.scrollView)).smoothScrollTo(0, 0);
        this.block_map_address_layout = findViewById(R.id.block_map_address_layout);
        this.block_house_rent_layout = findViewById(R.id.block_house_rent_layout);
        this.block_house_sell_layout = findViewById(R.id.block_house_sell_layout);
        this.block_title = (TextView) findViewById(R.id.block_title);
        this.block_house_sell_price = (TextView) findViewById(R.id.block_house_sell_price);
        this.block_house_sell_count = (TextView) findViewById(R.id.block_house_sell_count);
        this.block_house_rent_count = (TextView) findViewById(R.id.block_house_rent_count);
        this.text_district = (TextView) findViewById(R.id.text_district);
        this.block_street = (TextView) findViewById(R.id.block_street);
        this.tv_block_developers = (TextView) findViewById(R.id.tv_block_developers);
        this.tv_block_company = (TextView) findViewById(R.id.tv_block_company);
        this.tv_block_type = (TextView) findViewById(R.id.tv_block_type);
        this.tv_block_fees = (TextView) findViewById(R.id.tv_block_fees);
        this.tv_block_address = (TextView) findViewById(R.id.tv_block_address);
        this.block_address_layout = findViewById(R.id.block_address_layout);
        this.b_other_layout = findViewById(R.id.b_other_layout);
        this.project_introduct_layout = findViewById(R.id.project_introduct_layout);
        this.block_pic_layout = (RelativeLayout) findViewById(R.id.house_pic_layout);
        this.more_h_pic = (TextView) findViewById(R.id.more_h_pic);
        this.B_pic = (ImageView) findViewById(R.id.h_pic);
        this.B_pic.setVisibility(0);
        this.isShowPic = ((NewHouseApplication) this.mApplication).isEnableImg();
        if (!this.isShowPic) {
            this.block_pic_layout.setEnabled(false);
        }
        this.mMapView = (MapView) findViewById(R.id.mapView);
        this.txt_expand_block_instruct = (TextView) findViewById(R.id.txt_expand_block_instruct);
        this.txt_expand_block_bo_ther = (TextView) findViewById(R.id.txt_expand_block_b_other);
    }

    public void initInfo(final Block block_detail2) {
        String pic_first = block_detail2.getPic();
        if (pic_first == null || !this.isShowPic) {
            setImage(this.B_pic, StringUtils.EMPTY, R.drawable.bg_default_ad, 1);
        } else {
            setImage(this.B_pic, pic_first, R.drawable.bg_default_ad, 1);
        }
        if ((block_detail2.getH_picList().size() > 1) && this.isShowPic) {
            this.more_h_pic.setVisibility(0);
            this.block_pic_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(BlockDetailActivity.this, AlbumFullScreenActivity.class);
                    intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) block_detail2.getH_picList());
                    BlockDetailActivity.this.startActivity(intent);
                }
            });
        }
        this.block_title.setText(block_detail2.getBlockname());
        if (block_detail2.getSellaverprice().equalsIgnoreCase("0")) {
            this.block_house_sell_price.setText((int) R.string.block_no_price);
        } else {
            this.block_house_sell_price.setText(getResources().getString(R.string.block_sell_price, block_detail2.getSellaverprice()));
        }
        this.block_house_sell_count.setText(getResources().getString(R.string.block_house_count_info, block_detail2.getSellcount()));
        this.block_house_rent_count.setText(getString(R.string.block_house_count_info, new Object[]{block_detail2.getRentcount()}));
        this.text_district.setText(block_detail2.getDistrict());
        this.block_street.setText(block_detail2.getStreetname());
        this.tv_block_developers.setText(block_detail2.getDevelopers());
        this.tv_block_company.setText(block_detail2.getCompany());
        if (!TextUtils.isEmpty(block_detail2.getType())) {
            this.tv_block_type.setText(Html.fromHtml(block_detail2.getType()));
        }
        if (!TextUtils.isEmpty(block_detail2.getFees())) {
            this.tv_block_fees.setText(Html.fromHtml(block_detail2.getFees()));
        }
        this.tv_block_address.setText(block_detail2.getAddress());
        this.block_address_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (block_detail2.getId().equals("0")) {
                    BlockDetailActivity.this.showToast((int) R.string.no_map);
                    return;
                }
                String addr = block_detail2.getAddress();
                if (addr != null) {
                    if (addr.indexOf("（") != -1) {
                        addr = addr.substring(0, addr.indexOf("（"));
                    }
                    if (addr.indexOf("(") != -1) {
                        addr = addr.substring(0, addr.indexOf("("));
                    }
                } else {
                    addr = StringUtils.EMPTY;
                }
                try {
                    BlockDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(block_detail2.getLat())).toString(), new StringBuilder(String.valueOf(block_detail2.getLat())).toString(), addr));
                } catch (Exception e) {
                    BlockDetailActivity.this.showToast((int) R.string.no_map_app);
                }
            }
        });
        this.block_map_address_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (block_detail2.getId().equals("0")) {
                    BlockDetailActivity.this.showToast((int) R.string.no_map);
                    return;
                }
                String addr = block_detail2.getAddress();
                if (addr != null) {
                    if (addr.indexOf("（") != -1) {
                        addr = addr.substring(0, addr.indexOf("（"));
                    }
                    if (addr.indexOf("(") != -1) {
                        addr = addr.substring(0, addr.indexOf("("));
                    }
                } else {
                    addr = StringUtils.EMPTY;
                }
                try {
                    BlockDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(block_detail2.getLat())).toString(), new StringBuilder(String.valueOf(block_detail2.getLat())).toString(), addr));
                } catch (Exception e) {
                    BlockDetailActivity.this.showToast((int) R.string.no_map_app);
                }
            }
        });
        if (!TextUtils.isEmpty(block_detail2.getIntroduction())) {
            this.txt_expand_block_instruct.setText(Html.fromHtml(block_detail2.getIntroduction()));
        }
        if (!TextUtils.isEmpty(block_detail2.getB_other())) {
            this.txt_expand_block_bo_ther.setText(Html.fromHtml(block_detail2.getB_other()));
        }
        MapController mapController = this.mMapView.getController();
        if (block_detail2.getLat() <= 0.0d || block_detail2.getLng() <= 0.0d) {
            double[] cityLoc = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
            mapController.animateTo(BaiduMapUtil.getPoint(cityLoc[0], cityLoc[1]));
            mapController.setZoom(15);
        } else {
            GeoPoint point = BaiduMapUtil.getPoint(block_detail2.getLat(), block_detail2.getLng());
            mapController.animateTo(point);
            mapController.setZoom(15);
            OverlayManager overlayManager = new OverlayManager(getApplication(), this.mMapView);
            overlayManager.createOverlay("houses", getResources().getDrawable(R.drawable.ico_marker)).add(new ManagedOverlayItem(point, block_detail2.getId(), StringUtils.EMPTY));
            overlayManager.populate();
        }
        final NewHouseApplication application = (NewHouseApplication) this.mApplication;
        if (application.hasFav(new HouseInfo("block", block_detail2), "block")) {
            this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow_selected);
        }
        this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (application.hasFav(new HouseInfo("block", block_detail2), "block")) {
                    application.delFavHistory(new HouseInfo("block", block_detail2), "block");
                    BlockDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow_normal);
                    return;
                }
                application.saveFavHistory(new HouseInfo("block", block_detail2), "block");
                BlockDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow_selected);
            }
        });
        ((NewHouseApplication) this.mApplication).saveBrowseHistory(new HouseInfo("block", block_detail2), "block");
        this.block_house_sell_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!block_detail2.getId().equals("0")) {
                    Intent intent = new Intent(BlockDetailActivity.this, BlockHouseListActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("app", App.SELL);
                    bundle.putString("blockid", block_detail2.getId());
                    bundle.putString("b_name", block_detail2.getBlockname());
                    intent.putExtras(bundle);
                    BlockDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.project_introduct_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(BlockDetailActivity.this, Block_project_intructActivity.class);
                if (block_detail2.getIntroduction() != null) {
                    intent.putExtra("txt_project_intruct", Html.fromHtml(block_detail2.getIntroduction()).toString());
                }
                intent.putExtra("title_name", block_detail2.getBlockname());
                BlockDetailActivity.this.startActivity(intent);
            }
        });
        this.b_other_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(BlockDetailActivity.this, SecondSell_B_otherActivity.class);
                String txt_house_B_other = Html.fromHtml(block_detail2.getB_other()).toString();
                intent.putExtra("title_name", block_detail2.getBlockname());
                intent.putExtra("txt_house_B_other", txt_house_B_other);
                BlockDetailActivity.this.startActivity(intent);
            }
        });
        this.block_house_rent_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!block_detail2.getId().equals("0")) {
                    Intent intent = new Intent(BlockDetailActivity.this, BlockHouseListActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("app", App.RENT);
                    bundle.putString("blockid", block_detail2.getId());
                    bundle.putString("b_name", block_detail2.getBlockname());
                    intent.putExtras(bundle);
                    BlockDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.head_view.setTvTitleText(block_detail2.getBlockname());
        block_detail2.resetRecordtime();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.id = getIntent().getStringExtra("id");
        this.blockId = getIntent().getStringExtra(INTENT_BLOCKID);
        this.app = getIntent().getIntExtra(INTENT_RENT_TYPE, this.app);
        new GetHouseInfoTask(this, R.string.block_info_load, this.app, this.blockId).execute(new Object[0]);
    }

    public static Intent getDialIntent(String mobile) {
        String callUri = "tel:";
        if (mobile != null) {
            callUri = String.valueOf(callUri) + TextUtil.filterTel(mobile);
        }
        return new Intent("android.intent.action.DIAL", Uri.parse(callUri));
    }

    private class GetHouseInfoTask extends CommonAsyncTask<Block> {
        int app;
        String blockID;
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseInfoTask(Context context, int resid, int app2, String blockID2) {
            super(context, resid);
            this.app = app2;
            this.blockID = blockID2;
            this.loadingresid = resid;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(Block v) {
            if (v == null) {
                ActivityUtil.showToast(this.context, (int) R.string.block_info_load_error);
                BlockDetailActivity.this.finish();
                return;
            }
            BlockDetailActivity.this.block_detail = v;
            BlockDetailActivity.this.initView();
            BlockDetailActivity.this.initInfo(BlockDetailActivity.this.block_detail);
        }

        public Block onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockByid(this.app, BlockDetailActivity.this.id, this.blockID);
        }

        /* access modifiers changed from: protected */
        public void onDialogCancel() {
            BlockDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            BlockDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            BlockDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            BlockDetailActivity.this.finish();
        }
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
