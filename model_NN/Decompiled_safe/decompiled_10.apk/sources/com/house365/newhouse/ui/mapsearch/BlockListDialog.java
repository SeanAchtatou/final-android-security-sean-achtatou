package com.house365.newhouse.ui.mapsearch;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.baidu.mapapi.MKEvent;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.task.GetHouseListTask;
import com.house365.newhouse.ui.secondhouse.adapter.HouseListAdapter;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;

public class BlockListDialog extends Dialog {
    /* access modifiers changed from: private */
    public HouseListAdapter adapter;
    /* access modifiers changed from: private */
    public String app;
    private String blockId;
    private String blockName;
    /* access modifiers changed from: private */
    public TextView btn_room;
    private int buildarea;
    /* access modifiers changed from: private */
    public Context context;
    private PullToRefreshListView listView;
    private View nodata_layout;
    private int price;
    private RefreshInfo refreshInfo;
    private int rentType;
    private int resource;
    /* access modifiers changed from: private */
    public PopupWindow roomPop;
    /* access modifiers changed from: private */
    public int roomValue;
    private TextView tv_block;

    public BlockListDialog(Context context2, int theme) {
        super(context2, theme);
        this.context = context2;
    }

    public BlockListDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public void setInfoData(String app2, String blockId2, String blockName2, int price2, int resource2, int buildarea2, int rentType2) {
        this.app = app2;
        this.blockId = blockId2;
        this.blockName = blockName2;
        this.price = price2;
        this.resource = resource2;
        this.buildarea = buildarea2;
        this.rentType = rentType2;
        initView();
        initData();
        initPop();
        refreshData();
    }

    private void initPop() {
        this.roomPop = new PopupWindow(this.context);
        View contentView = LayoutInflater.from(this.context).inflate((int) R.layout.room_pop, (ViewGroup) null);
        this.roomPop.setAnimationStyle(R.style.AnimationFade);
        this.roomPop.setWidth(250);
        this.roomPop.setHeight(MKEvent.ERROR_PERMISSION_DENIED);
        this.roomPop.setContentView(contentView);
        this.roomPop.setFocusable(true);
        this.roomPop.setTouchable(true);
        this.roomPop.setBackgroundDrawable(this.context.getResources().getDrawable(17170445));
        this.roomPop.setOutsideTouchable(false);
        ListView roomlist = (ListView) contentView.findViewById(R.id.room_list);
        DefineArrayAdapter<String> arrayadapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, AppArrays.getRoom());
        arrayadapter.setRoom(true);
        arrayadapter.notifyDataSetChanged();
        roomlist.setAdapter((ListAdapter) arrayadapter);
        roomlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BlockListDialog.this.roomPop.dismiss();
                BlockListDialog.this.roomValue = (int) id;
                BlockListDialog.this.refreshData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.tv_block = (TextView) findViewById(R.id.tv_block);
        this.btn_room = (TextView) findViewById(R.id.btn_room);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.tv_block.setText(this.blockName);
        this.listView = (PullToRefreshListView) findViewById(R.id.listView);
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                BlockListDialog.this.refreshData();
            }

            public void onFooterRefresh() {
                BlockListDialog.this.getMoreData();
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent;
                MapSearchHomeActivity.toBlockDetail = true;
                if (BlockListDialog.this.app.equals(App.SELL)) {
                    intent = new Intent(BlockListDialog.this.context, SecondSellDetailActivity.class);
                } else {
                    intent = new Intent(BlockListDialog.this.context, SecondRentDetailActivity.class);
                }
                intent.putExtra("id", ((SecondHouse) BlockListDialog.this.adapter.getItem(position)).getId());
                BlockListDialog.this.context.startActivity(intent);
            }
        });
        this.btn_room.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockListDialog.this.roomPop.showAsDropDown(BlockListDialog.this.btn_room, 30, 10);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.refreshInfo = new RefreshInfo();
        this.adapter = new HouseListAdapter(this.context, this.app);
        this.listView.setAdapter(this.adapter);
    }

    public void refreshData() {
        this.adapter.clear();
        this.adapter.notifyDataSetChanged();
        this.refreshInfo.refresh = true;
        if (!TextUtils.isEmpty(this.app)) {
            new GetHouseListTask(this.context, this.listView, this.refreshInfo, this.adapter, this.app, this.blockId, this.resource, this.price, this.buildarea, this.roomValue, this.rentType, this.nodata_layout).execute(new Object[0]);
        }
    }

    public void getMoreData() {
        this.refreshInfo.refresh = false;
        if (!TextUtils.isEmpty(this.app)) {
            new GetHouseListTask(this.context, this.listView, this.refreshInfo, this.adapter, this.app, this.blockId, this.resource, this.price, this.buildarea, this.roomValue, this.rentType, this.nodata_layout).execute(new Object[0]);
        }
    }
}
