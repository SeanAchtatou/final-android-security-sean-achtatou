package com.house365.newhouse.ui.privilege.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Event;

public class MyHouseGroupAdapter extends BaseCacheListAdapter<Event> {
    private LayoutInflater inflater;
    private int markType;

    public MyHouseGroupAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.group_my_house_item, (ViewGroup) null);
        }
        ((TextView) convertView.findViewById(R.id.group_my_house_title)).setText(((Event) getItem(position)).getE_title());
        ((TextView) convertView.findViewById(R.id.group_my_house_tel)).setText(((Object) this.context.getResources().getText(R.string.event_sign_prefix)) + ((Event) getItem(position)).getE_phone());
        ImageView mark = (ImageView) convertView.findViewById(R.id.group_house_item_mark);
        if (this.markType == 0) {
            mark.setImageResource(R.drawable.bg_new_event);
        } else if (this.markType == 1) {
            mark.setImageResource(R.drawable.bg_sell_event);
        }
        return convertView;
    }

    public int getMarkType() {
        return this.markType;
    }

    public void setMarkType(int markType2) {
        this.markType = markType2;
    }
}
