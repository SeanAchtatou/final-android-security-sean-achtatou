package com.house365.newhouse.ui.util;

import com.house365.core.constant.CorePreferences;
import java.text.DecimalFormat;

public class MathUtil {
    public static double convertDouble(double d, int dec) {
        CorePreferences.DEBUG("befor:" + d);
        double t = ((double) Math.round(Math.pow(10.0d, (double) dec) * d)) / 100.0d;
        CorePreferences.DEBUG("after:" + t);
        return t;
    }

    public static String d2StrWith2Dec(double d) {
        return new DecimalFormat("#,##0.00").format(d);
    }
}
