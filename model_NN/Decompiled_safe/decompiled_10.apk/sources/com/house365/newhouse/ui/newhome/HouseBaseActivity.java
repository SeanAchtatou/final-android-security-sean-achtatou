package com.house365.newhouse.ui.newhome;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.TextUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.ui.util.TelUtil;
import java.text.SimpleDateFormat;

public class HouseBaseActivity extends BaseCommonActivity {
    private TextView h_aver_price;
    private TextView h_base;
    private TextView h_build_type;
    private TextView h_business;
    private TextView h_carport;
    private TextView h_carport_rate;
    private TextView h_channel;
    private TextView h_decoration_states;
    private TextView h_deli_date;
    private TextView h_dls;
    private TextView h_gh_rate;
    private TextView h_green_rate;
    private TextView h_kfs;
    private TextView h_opening_date;
    private TextView h_plot_rate;
    private TextView h_project_address;
    private TextView h_sale_address;
    /* access modifiers changed from: private */
    public TextView h_tel;
    private TextView h_tel_prefix;
    private TextView h_total_house;
    private TextView h_wy;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public House house;
    private String house_build_type;
    private String house_business;
    private String house_carport;
    private String house_carport_rate;
    private String house_channel;
    private String house_decoration_states;
    private String house_deli_date;
    private String house_dls;
    private String house_gh_rate;
    private String house_green_rate;
    private String house_info;
    private String house_kfs;
    private String house_plot_rate;
    private String house_price;
    private String house_project_address;
    private String house_sale_address;
    private String house_sale_starttime;
    private String house_tel;
    private String house_title;
    private String house_total_house;
    private String house_wy;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_base);
        this.house = (House) getIntent().getSerializableExtra("house");
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseBaseActivity.this.finish();
            }
        });
        if (this.house_title != null) {
            this.head_view.setTvTitleText(this.house_title);
        } else {
            this.head_view.setTvTitleText((int) R.string.text_house_base_title);
        }
        this.h_aver_price = (TextView) findViewById(R.id.h_aver_price);
        this.h_business = (TextView) findViewById(R.id.h_business);
        this.h_channel = (TextView) findViewById(R.id.h_channel);
        this.h_kfs = (TextView) findViewById(R.id.h_kfs);
        this.h_dls = (TextView) findViewById(R.id.h_dls);
        this.h_wy = (TextView) findViewById(R.id.h_wy);
        this.h_tel = (TextView) findViewById(R.id.h_tel);
        this.h_build_type = (TextView) findViewById(R.id.h_build_type);
        this.h_project_address = (TextView) findViewById(R.id.h_project_address);
        this.h_sale_address = (TextView) findViewById(R.id.h_sale_address);
        this.h_total_house = (TextView) findViewById(R.id.h_total_house);
        this.h_deli_date = (TextView) findViewById(R.id.h_deli_date);
        this.h_plot_rate = (TextView) findViewById(R.id.h_plot_rate);
        this.h_green_rate = (TextView) findViewById(R.id.h_green_rate);
        this.h_carport = (TextView) findViewById(R.id.h_carport);
        this.h_opening_date = (TextView) findViewById(R.id.h_opening_date);
        this.h_decoration_states = (TextView) findViewById(R.id.f226h_decoration_states);
        this.h_gh_rate = (TextView) findViewById(R.id.h_gh_rate);
        this.h_carport_rate = (TextView) findViewById(R.id.h_carport_rate);
        this.h_tel_prefix = (TextView) findViewById(R.id.h_tel_prefix);
        if (this.house != null) {
            ensureUi(this.house);
        }
    }

    private void ensureUi(House curHouse) {
        House curHouse2 = this.house;
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy年MM月dd日");
        this.house_price = getResources().getString(R.string.text_field_aver_price, this.house.getH_price());
        this.house_business = getResources().getString(R.string.text_field_district, this.house.getH_business());
        this.house_channel = getResources().getString(R.string.text_field_wy, this.house.getH_channel());
        this.house_kfs = getResources().getString(R.string.text_field_kfs, this.house.getH_kfs());
        this.house_dls = getResources().getString(R.string.text_field_dls, this.house.getH_dls());
        this.house_wy = getResources().getString(R.string.text_field_wys, this.house.getH_wy());
        this.house_project_address = getResources().getString(R.string.text_field_xm_address, this.house.getH_project_address());
        this.house_sale_address = getResources().getString(R.string.text_field_sale_address, this.house.getH_sale_address());
        this.house_tel = getResources().getString(R.string.text_field_sale_tel, this.house.getH_tel());
        this.house_total_house = getResources().getString(R.string.text_field_total_house, this.house.getH_total_house());
        this.house_plot_rate = getResources().getString(R.string.text_field_plot_rate, this.house.getH_plot_rate());
        this.house_green_rate = getResources().getString(R.string.text_field_green_rate, this.house.getH_green_rate());
        this.house_carport = getResources().getString(R.string.text_field_carport, this.house.getH_carport());
        this.house_build_type = getResources().getString(R.string.text_field_build_type, this.house.getH_build_type());
        this.house_deli_date = getResources().getString(R.string.text_field_deli_date, this.house.getH_deli_date());
        this.house_gh_rate = getResources().getString(R.string.text_field_gh_rate, this.house.getH_gh_rate());
        this.house_carport_rate = getResources().getString(R.string.text_field_carport_rate, this.house.getH_carport_rate());
        if (this.house.getH_sale_starttime() != 0) {
            this.house_sale_starttime = getResources().getString(R.string.text_field_start_sale_date, dateformat.format(Long.valueOf(this.house.getH_sale_starttime() * 1000)));
        } else {
            this.house_sale_starttime = getResources().getString(R.string.text_field_start_sale_date, "不详");
        }
        this.house_decoration_states = getResources().getString(R.string.text_field_decoration_states, this.house.getH_deli_standard());
        TextUtil.setNullText(this.house.getH_price(), this.house_price, this.h_aver_price);
        TextUtil.setNullText(this.house.getH_business(), this.house_business, this.h_business);
        TextUtil.setNullText(this.house.getH_wy(), this.house_wy, this.h_wy);
        TextUtil.setNullText(this.house.getH_kfs(), this.house_kfs, this.h_kfs);
        TextUtil.setNullText(this.house.getH_dls(), this.house_dls, this.h_dls);
        TextUtil.setNullText(this.house.getH_channel(), this.house_channel, this.h_channel);
        TextUtil.setNullText(this.house.getH_build_type(), this.house_build_type, this.h_build_type);
        TextUtil.setNullText(this.house.getH_deli_date(), this.house_deli_date, this.h_deli_date);
        TextUtil.setNullText(this.house.getH_total_house(), this.house_total_house, this.h_total_house);
        TextUtil.setNullText(this.house.getH_plot_rate(), this.house_plot_rate, this.h_plot_rate);
        TextUtil.setNullText(this.house.getH_green_rate(), this.house_green_rate, this.h_green_rate);
        TextUtil.setNullText(this.house.getH_carport(), this.house_carport, this.h_carport);
        TextUtil.setNullText(this.house.getH_project_address(), this.house_project_address, this.h_project_address);
        TextUtil.setNullText(this.house.getH_sale_address(), this.house_sale_address, this.h_sale_address);
        TextUtil.setNullText(this.house.getH_deli_standard(), this.house_decoration_states, this.h_decoration_states);
        TextUtil.setNullText(this.house.getH_gh_rate(), this.house_gh_rate, this.h_gh_rate);
        TextUtil.setNullText(this.house.getH_carport_rate(), this.house_carport_rate, this.h_carport_rate);
        if (this.house.getH_sale_starttime() == 0) {
            this.h_opening_date.setVisibility(8);
        } else {
            this.h_opening_date.setText(this.house_sale_starttime);
        }
        if (this.house.getH_tel() != null && !this.house.getH_tel().equals(" ") && !TextUtils.isEmpty(this.house.getH_tel())) {
            this.h_tel_prefix.setVisibility(0);
            this.h_tel.setText(this.house.getH_tel());
            this.h_tel.setVisibility(0);
            this.h_tel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ((NewHouseApplication) HouseBaseActivity.this.mApplication).saveCallHistory(new HouseInfo("house", HouseBaseActivity.this.house), "house");
                    TelUtil.getCallIntent(HouseBaseActivity.this.h_tel.getText().toString().trim(), HouseBaseActivity.this, "house");
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }
}
