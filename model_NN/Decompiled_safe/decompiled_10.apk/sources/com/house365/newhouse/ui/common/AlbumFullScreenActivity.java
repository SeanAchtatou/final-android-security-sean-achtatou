package com.house365.newhouse.ui.common;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.adapter.BaseAlbumAdapter;
import com.house365.core.util.FileUtil;
import com.house365.core.view.AlbumView;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.task.SaveImageAsync;
import java.util.List;
import org.apache.commons.lang.SystemUtils;

public class AlbumFullScreenActivity extends BaseCommonActivity {
    public static final String INTENT_ALBUM_PIC_LIST = "piclist";
    public static final String INTENT_ALBUM_POS = "pos";
    public static final String INTENT_FROM_NEWS = "fromNews";
    private static final int SHOW_HIDE_CONTROL_ANIMATION_TIME = 500;
    private BaseAlbumAdapter adapter;
    private AlbumView albumView;
    private boolean fromNews = false;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public View head_view_layout;
    /* access modifiers changed from: private */
    public AlphaAnimation hideAnimation;
    public int length = 0;
    /* access modifiers changed from: private */
    public int mPosition;
    /* access modifiers changed from: private */
    public List<String> piclist;
    /* access modifiers changed from: private */
    public AlphaAnimation showAnimation;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.album_fullscreen);
    }

    /* access modifiers changed from: private */
    public void updateShowInfo() {
        if (this.piclist.size() > 0) {
            int postext = this.mPosition + 1;
            this.head_view.setTvTitleText(String.format("%d/%d", Integer.valueOf(postext), Integer.valueOf(this.adapter.getCount())));
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlbumFullScreenActivity.this.finish();
            }
        });
        this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String filename = App.NEW + System.currentTimeMillis() + ".jpg";
                Photo photoItem = new Photo(filename, (String) AlbumFullScreenActivity.this.piclist.get(AlbumFullScreenActivity.this.mPosition));
                if (FileUtil.isSDCARDMounted()) {
                    new SaveImageAsync(AlbumFullScreenActivity.this, filename).execute(photoItem);
                    return;
                }
                AlbumFullScreenActivity.this.showToast((int) R.string.no_sd_info);
            }
        });
        this.head_view_layout = findViewById(R.id.head_view_layout);
        this.piclist = getIntent().getStringArrayListExtra(INTENT_ALBUM_PIC_LIST);
        this.mPosition = getIntent().getIntExtra(INTENT_ALBUM_POS, 0);
        this.fromNews = getIntent().getBooleanExtra(INTENT_FROM_NEWS, false);
        if (this.fromNews) {
            this.head_view.getBtn_right().setVisibility(0);
        } else {
            this.head_view.getBtn_right().setVisibility(4);
        }
        this.albumView = (AlbumView) findViewById(R.id.albumView);
        this.adapter = new BaseAlbumAdapter(this);
        this.adapter.setResBg(R.drawable.img_default_big);
        this.adapter.addAll(this.piclist);
        this.albumView.setAdapter(this.adapter);
        this.albumView.setCurrentPosition(this.mPosition);
        this.albumView.setAlbumViewListener(new AlbumView.AlumViewListener() {
            public void onFullScreen(boolean fullScreen, int position) {
                if (fullScreen) {
                    AlbumFullScreenActivity.this.head_view_layout.startAnimation(AlbumFullScreenActivity.this.hideAnimation);
                    AlbumFullScreenActivity.this.head_view_layout.setVisibility(4);
                    return;
                }
                AlbumFullScreenActivity.this.head_view_layout.startAnimation(AlbumFullScreenActivity.this.showAnimation);
                AlbumFullScreenActivity.this.head_view_layout.setVisibility(0);
            }

            public void onChange(int position) {
                AlbumFullScreenActivity.this.mPosition = position;
                AlbumFullScreenActivity.this.updateShowInfo();
            }
        });
        this.showAnimation = new AlphaAnimation((float) SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
        this.showAnimation.setFillAfter(true);
        this.showAnimation.setDuration(500);
        this.hideAnimation = new AlphaAnimation(1.0f, (float) SystemUtils.JAVA_VERSION_FLOAT);
        this.hideAnimation.setFillAfter(true);
        this.hideAnimation.setDuration(500);
        this.head_view_layout.startAnimation(this.hideAnimation);
        this.head_view_layout.setVisibility(4);
        updateShowInfo();
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }
}
