package com.house365.newhouse.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.newhouse.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuickAlphabeticView extends LinearLayout {
    private HashMap<String, Integer> alphaIndexer;
    private Context context;
    private String[] defineStr;
    private List<String> letterList = new ArrayList();
    /* access modifiers changed from: private */
    public TextView mDialogText;
    private Handler mHandler;
    private float mHight;
    private String[] mLetter;
    private ListView mList;

    public QuickAlphabeticView(Context context2) {
        super(context2);
        this.context = context2;
    }

    public QuickAlphabeticView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        TypedArray array = context2.obtainStyledAttributes(attrs, R.styleable.QuickAlphabeticView);
        CharSequence letterChar = array.getText(0);
        CharSequence defineChar = array.getText(1);
        if (letterChar != null) {
            this.mLetter = letterChar.toString().split(",");
        }
        if (defineChar != null) {
            this.defineStr = defineChar.toString().split(",");
        }
    }

    private void createTextView(String[] arrayData) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -1);
        lp.weight = 1.0f;
        if (arrayData != null && arrayData.length > 0) {
            for (int i = 0; i < arrayData.length; i++) {
                String itemArray = arrayData[i];
                if (!TextUtils.isEmpty(itemArray)) {
                    this.letterList.add(this.letterList.size(), itemArray);
                    TextView textView = new TextView(this.context);
                    textView.setGravity(17);
                    textView.setText(arrayData[i]);
                    textView.setTextAppearance(this.context, R.style.font14_black);
                    textView.setLayoutParams(lp);
                    addView(textView);
                }
            }
        }
    }

    public void init(Activity ctx) {
        this.mDialogText = (TextView) ctx.findViewById(R.id.fast_position);
        this.mDialogText.setVisibility(4);
        this.mHandler = new Handler();
        createTextView(this.defineStr);
        createTextView(this.mLetter);
    }

    public void setDefineStr(String[] defineStr2) {
        this.defineStr = defineStr2;
    }

    public void setListView(ListView mList2) {
        this.mList = mList2;
    }

    public void setAlphaIndexer(HashMap<String, Integer> alphaIndexer2) {
        this.alphaIndexer = alphaIndexer2;
    }

    public void setHight(float mHight2) {
        this.mHight = mHight2;
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        String key;
        this.mList.setEnabled(true);
        int act = event.getAction();
        float y = event.getY();
        int count = this.letterList.size();
        int selectIndex = (int) (y / ((float) (((int) this.mHight) / count)));
        if (selectIndex > -1 && selectIndex < count + 1) {
            if (selectIndex == 0) {
                key = this.letterList.get(selectIndex);
            } else {
                key = this.letterList.get(selectIndex - 1);
            }
            if (key.equals("#")) {
                key = getResources().getString(R.string.text_other_city);
            }
            String litterKey = key.toLowerCase();
            if (this.alphaIndexer.containsKey(litterKey)) {
                int pos = this.alphaIndexer.get(litterKey).intValue();
                if (this.mList.getHeaderViewsCount() > 0) {
                    this.mList.setSelectionFromTop(this.mList.getHeaderViewsCount() + pos, 0);
                } else {
                    this.mList.setSelectionFromTop(pos, 0);
                }
            }
            if (key.equals(getResources().getString(R.string.text_other_city))) {
                key = "#";
            }
            this.mDialogText.setText(key.toUpperCase());
        }
        if (act == 0) {
            if (this.mHandler != null) {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        if (QuickAlphabeticView.this.mDialogText != null && QuickAlphabeticView.this.mDialogText.getVisibility() == 4) {
                            QuickAlphabeticView.this.mDialogText.setVisibility(0);
                        }
                    }
                });
            }
        } else if (act == 1 && this.mHandler != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (QuickAlphabeticView.this.mDialogText != null && QuickAlphabeticView.this.mDialogText.getVisibility() == 0) {
                        QuickAlphabeticView.this.mDialogText.setVisibility(4);
                    }
                }
            });
        }
        return true;
    }
}
