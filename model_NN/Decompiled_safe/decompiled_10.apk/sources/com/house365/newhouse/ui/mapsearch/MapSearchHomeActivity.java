package com.house365.newhouse.ui.mapsearch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MKEvent;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Projection;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.json.JSONException;
import com.house365.core.json.JSONObject;
import com.house365.core.reflect.ReflectException;
import com.house365.core.reflect.ReflectUtil;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.AbstractCallBack;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.lbs.MyLocation;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayGestureDetector;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.ZoomEvent;
import com.house365.core.util.map.baidu.lazyload.LazyLoadCallback;
import com.house365.core.util.map.baidu.lazyload.LazyLoadException;
import com.house365.core.util.map.baidu.lazyload.LazyLoadListener;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import com.house365.newhouse.ui.util.DialogUtil;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

public class MapSearchHomeActivity extends BaseBaiduMapActivity {
    public static final String INTENT_GPS_MUST = "gps_must";
    public static final String INTENT_MAP_TYPE = "map_type";
    public static final int MAP_INIT_ZOOM = 13;
    public static final int MAP_SECOND_ZOOM = 15;
    public static List<ManagedOverlayItem> houseItems;
    public static List<ManagedOverlayItem> regionItems;
    public static boolean toBlockDetail;
    public static boolean toNewDetail;
    /* access modifiers changed from: private */
    public int MAPTYPE_STAUTS_CLOSE = 2;
    /* access modifiers changed from: private */
    public int MAPTYPE_STAUTS_OPEN = 1;
    private int animateDuration = MKEvent.ERROR_PERMISSION_DENIED;
    Bitmap bitmap = null;
    private View black_alpha_view;
    private List<View> blockviews = new ArrayList();
    /* access modifiers changed from: private */
    public Button btMapType;
    private RadioButton btMapTypeNew;
    private RadioButton btMapTypeSell;
    private RadioButton btMapTyperent;
    /* access modifiers changed from: private */
    public int buildarea_value;
    /* access modifiers changed from: private */
    public String channelvalue;
    /* access modifiers changed from: private */
    public boolean checkedRegion = true;
    private BroadcastReceiver chosedFinished = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String chose_from = intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
            if (chose_from.equals(ActionCode.NEW_SEARCH)) {
                if (((NewHouseApplication) MapSearchHomeActivity.this.mApplication).getCityName().equals("西安") && MapSearchHomeActivity.this.text_map_first.getText().equals(MapSearchHomeActivity.this.getResources().getText(R.string.btn_region_text))) {
                    MapSearchHomeActivity.this.text_map_first.setText((int) R.string.btn_xian_region_text);
                }
                MapSearchHomeActivity.this.setConPoiont(chose_from);
                MapSearchHomeActivity.this.pricevalue = SearchConditionPopView.setStringValue(MapSearchHomeActivity.this.text_map_second, MapSearchHomeActivity.this.pricevalue, "price", MapSearchHomeActivity.this.searchConfig);
                MapSearchHomeActivity.this.channelvalue = SearchConditionPopView.setStringValue(MapSearchHomeActivity.this.text_map_third, MapSearchHomeActivity.this.channelvalue, "channel", MapSearchHomeActivity.this.searchConfig);
            } else if (chose_from.equals(ActionCode.SELL_SEARCH)) {
                MapSearchHomeActivity.this.setConPoiont(chose_from);
                MapSearchHomeActivity.this.sell_resource_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_second, MapSearchHomeActivity.this.sell_resource_value, HouseBaseInfo.INFOFROM, MapSearchHomeActivity.this.searchConfig);
                MapSearchHomeActivity.this.total_price_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_third, MapSearchHomeActivity.this.total_price_value, "price", MapSearchHomeActivity.this.searchConfig);
                MapSearchHomeActivity.this.buildarea_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_forth, MapSearchHomeActivity.this.buildarea_value, "buildarea", MapSearchHomeActivity.this.searchConfig);
            } else if (chose_from.equals(ActionCode.RENT_SEARCH)) {
                MapSearchHomeActivity.this.setConPoiont(chose_from);
                MapSearchHomeActivity.this.rent_resource_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_second, MapSearchHomeActivity.this.rent_resource_value, HouseBaseInfo.INFOFROM, MapSearchHomeActivity.this.searchConfig);
                MapSearchHomeActivity.this.rent_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_third, MapSearchHomeActivity.this.rent_value, HouseBaseInfo.PRICE_DEFAULT, MapSearchHomeActivity.this.searchConfig);
                MapSearchHomeActivity.this.rent_type_value = SearchConditionPopView.setIntValue(MapSearchHomeActivity.this.text_map_forth, MapSearchHomeActivity.this.rent_type_value, HouseBaseInfo.RENTTYPE, MapSearchHomeActivity.this.searchConfig);
            }
            MapSearchHomeActivity.this.setMapZoom(15);
            MapSearchHomeActivity.this.regionShow = 2;
            if (MapSearchHomeActivity.this.conPoint != null) {
                MapSearchHomeActivity.this.moveMapToPoint(MapSearchHomeActivity.this.conPoint);
            }
            new GetHouseOverLayTask(context).execute(new Object[0]);
        }
    };
    /* access modifiers changed from: private */
    public String cityId;
    /* access modifiers changed from: private */
    public GeoPoint conPoint;
    /* access modifiers changed from: private */
    public RadioGroup condition_group;
    /* access modifiers changed from: private */
    public View condition_layout;
    /* access modifiers changed from: private */
    public Context context;
    public Handler handler = new Handler() {
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    MapSearchHomeActivity.this.clearRegionTag();
                    return;
                case 2:
                    MapSearchHomeActivity.this.containPoint(MapSearchHomeActivity.this.conPoint);
                    return;
                case 3:
                    MapSearchHomeActivity.this.setNodata();
                    return;
                case 4:
                    Toast.makeText(MapSearchHomeActivity.this.context, (int) R.string.text_no_network, 0).show();
                    break;
                case 5:
                    break;
                case 15:
                    MapSearchHomeActivity.this.regionShow = 2;
                    return;
                default:
                    return;
            }
            Toast.makeText(MapSearchHomeActivity.this.context, (int) R.string.text_http_parse_error, 0).show();
        }
    };
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public Animation hideMapTypeAnimation;
    /* access modifiers changed from: private */
    public HouseBlockOverlay houseBlock;
    /* access modifiers changed from: private */
    public ManagedOverlay houseOverlay;
    private Drawable house_marker;
    private View ico_third;
    /* access modifiers changed from: private */
    public boolean isAnimating = false;
    boolean isdoing = false;
    boolean islocating = false;
    private GeoPoint lastPoint;
    private int lastZoom;
    private View loadingLayout;
    /* access modifiers changed from: private */
    public TextView loadingtxt;
    private Drawable location_marker;
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            MapSearchHomeActivity.this.setJustNewHouse();
            MapSearchHomeActivity.this.clearRegionTag();
            MapSearchHomeActivity.this.clearNormalTag();
            MapSearchHomeActivity.this.cityId = ((NewHouseApplication) MapSearchHomeActivity.this.mApplication).getCity();
            if (AppMethods.isVirtual(MapSearchHomeActivity.this.cityId)) {
                MapSearchHomeActivity.this.condition_layout.setVisibility(8);
                MapSearchHomeActivity.this.setMapZoom(15);
                MapSearchHomeActivity.this.regionShow = 2;
            } else {
                MapSearchHomeActivity.this.condition_layout.setVisibility(0);
                MapSearchHomeActivity.this.setMapZoom(13);
                MapSearchHomeActivity.this.regionShow = 1;
            }
            MapSearchHomeActivity.this.getConfigInfo(MapSearchHomeActivity.this.type);
        }
    };
    MapController mapController;
    /* access modifiers changed from: private */
    public RadioGroup mapTypeLayout;
    MapView mapView;
    /* access modifiers changed from: private */
    public View map_first_layout;
    private View map_forth_layout;
    private View map_second_layout;
    private View map_third_layout;
    private View maplayout;
    /* access modifiers changed from: private */
    public int maptype = 11;
    /* access modifiers changed from: private */
    public int maptype_stauts = this.MAPTYPE_STAUTS_OPEN;
    private Handler mustOpenGPSHander = new Handler() {
        public void handleMessage(Message msg) {
            DialogUtil.openGpsDialog(MapSearchHomeActivity.this, new AbstractCallBack() {
                public void doCallBack() {
                    MapSearchHomeActivity.this.finish();
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public MyLocation myLocation;
    private ManagedOverlay myPositionOverlay;
    private Handler openGPSHander = new Handler() {
        public void handleMessage(Message msg) {
            DialogUtil.openGpsDialog(MapSearchHomeActivity.this, new AbstractCallBack() {
                public void doCallBack() {
                }
            });
        }
    };
    OverlayManager overlayManager;
    /* access modifiers changed from: private */
    public String pricevalue;
    protected List<String> regionList;
    /* access modifiers changed from: private */
    public ManagedOverlay regionOverlay;
    /* access modifiers changed from: private */
    public int regionShow = 1;
    private Drawable region_marker;
    private Button relocation;
    /* access modifiers changed from: private */
    public int rent_resource_value;
    /* access modifiers changed from: private */
    public int rent_type_value;
    /* access modifiers changed from: private */
    public int rent_value;
    private String res_area;
    private String res_channel;
    private String res_infofrom;
    private String res_price;
    private String res_region;
    private String res_rent;
    private String res_rent_type;
    private String res_total_price;
    protected HouseBaseInfo searchConfig;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    /* access modifiers changed from: private */
    public int sell_resource_value;
    /* access modifiers changed from: private */
    public Animation showMapTypeAnimation;
    public Handler showMapTypeHanlder = new Handler() {
        public void handleMessage(Message msg) {
            if (MapSearchHomeActivity.this.maptype_stauts == MapSearchHomeActivity.this.MAPTYPE_STAUTS_OPEN) {
                MapSearchHomeActivity.this.maptype_stauts = MapSearchHomeActivity.this.MAPTYPE_STAUTS_CLOSE;
                MapSearchHomeActivity.this.mapTypeLayout.startAnimation(MapSearchHomeActivity.this.hideMapTypeAnimation);
                MapSearchHomeActivity.this.btMapType.setBackgroundResource(R.drawable.bt_map_type);
                return;
            }
            MapSearchHomeActivity.this.maptype_stauts = MapSearchHomeActivity.this.MAPTYPE_STAUTS_OPEN;
            MapSearchHomeActivity.this.mapTypeLayout.startAnimation(MapSearchHomeActivity.this.showMapTypeAnimation);
            MapSearchHomeActivity.this.btMapType.setBackgroundResource(R.drawable.bt_map_type_back);
        }
    };
    private Animation statusInAnim;
    private Animation statusOutAnim;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, Station>> subwayMap;
    /* access modifiers changed from: private */
    public List<TextView> textViews = new ArrayList();
    /* access modifiers changed from: private */
    public TextView text_map_first;
    /* access modifiers changed from: private */
    public TextView text_map_forth;
    /* access modifiers changed from: private */
    public TextView text_map_second;
    /* access modifiers changed from: private */
    public TextView text_map_third;
    /* access modifiers changed from: private */
    public int total_price_value;
    /* access modifiers changed from: private */
    public int type;
    /* access modifiers changed from: private */
    public List<View> views = new ArrayList();

    public interface onTaskFinishListener {
        void onFinish(List<ManagedOverlayItem> list);
    }

    /* access modifiers changed from: private */
    public void setMapZoom(int zoom) {
        if (this.mapController != null) {
            this.mapController.setZoom(zoom);
        }
    }

    public MapView getMapView() {
        return this.mapView;
    }

    /* access modifiers changed from: private */
    public void setConPoiont(String chose_from) {
        String subWay;
        int type2 = SearchConditionPopView.getTagFlag(this.text_map_first.getTag());
        if (type2 == 1) {
            Station station = null;
            if (chose_from.equals(ActionCode.NEW_SEARCH)) {
                station = this.searchConfig.getDistXY().get((String) this.searchConfig.getConfig().get(HouseBaseInfo.DISTRICT).get(SearchConditionPopView.getTagData(this.text_map_first.getTag())));
            } else if (chose_from.equals(ActionCode.SELL_SEARCH) || chose_from.equals(ActionCode.RENT_SEARCH)) {
                station = this.searchConfig.getCoordinate().get(SearchConditionPopView.getTagData(this.text_map_first.getTag()));
            }
            if (station != null) {
                this.conPoint = BaiduMapUtil.getPoint(station.getX(), station.getY());
            }
        } else if (type2 == 2 && (subWay = SearchConditionPopView.getTagData(this.text_map_first.getTag())) != null && !subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE) && subWay != null && subWay.indexOf("+") != -1) {
            Station station2 = (Station) this.searchConfig.getMetro().get(subWay.substring(0, subWay.indexOf("+"))).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()));
            this.conPoint = BaiduMapUtil.getPoint(station2.getX(), station2.getY());
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        setContentView(LayoutInflater.from(this.context).inflate((int) R.layout.map_search_home, (ViewGroup) null));
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(false);
        this.searchPop.setShowOnView(true);
        this.searchPop.setFromMapSearch(true);
        this.searchPop.setSearch_type(ActionCode.NEW_SEARCH);
        this.condition_group = this.searchPop.getCondition_group();
        this.condition_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                MapSearchHomeActivity.this.searchPop.setShowGroup(true);
                if (checkedId == R.id.rb_region) {
                    MapSearchHomeActivity.this.searchPop.setGradeData(MapSearchHomeActivity.this.regionList, MapSearchHomeActivity.this.text_map_first, SearchConditionPopView.getTagData(MapSearchHomeActivity.this.text_map_first.getTag()), 1);
                    MapSearchHomeActivity.this.checkedRegion = true;
                } else if (checkedId == R.id.rb_subway) {
                    MapSearchHomeActivity.this.searchPop.setGradeData(MapSearchHomeActivity.this.subwayMap, MapSearchHomeActivity.this.text_map_first, SearchConditionPopView.getTagData(MapSearchHomeActivity.this.text_map_first.getTag()), 2);
                    MapSearchHomeActivity.this.checkedRegion = false;
                }
            }
        });
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        this.myLocation = new MyLocation(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
        unregisterReceiver(this.mChangeCity);
    }

    private void setTagText(TextView view, String normal) {
        view.setText(normal);
        view.setTag(null);
    }

    private void setRegionText() {
        if (((NewHouseApplication) this.mApplication).getCityName().equals("西安")) {
            this.text_map_first.setText((int) R.string.btn_xian_region_text);
        } else {
            this.text_map_first.setText((int) R.string.btn_region_text);
        }
    }

    /* access modifiers changed from: private */
    public void setNewSearch() {
        this.searchPop.setSearch_type(ActionCode.NEW_SEARCH);
        this.map_forth_layout.setVisibility(8);
        this.ico_third.setVisibility(8);
        setRegionText();
        setTagText(this.text_map_second, this.res_price);
        setTagText(this.text_map_third, this.res_channel);
        setTagText(this.text_map_forth, this.res_area);
        this.pricevalue = StringUtils.EMPTY;
        this.channelvalue = StringUtils.EMPTY;
    }

    /* access modifiers changed from: private */
    public void setSellSearch() {
        this.searchPop.setSearch_type(ActionCode.SELL_SEARCH);
        this.map_forth_layout.setVisibility(0);
        this.ico_third.setVisibility(0);
        this.text_map_first.setText((int) R.string.btn_region_text);
        setTagText(this.text_map_second, this.res_infofrom);
        setTagText(this.text_map_third, this.res_total_price);
        setTagText(this.text_map_forth, this.res_area);
        this.total_price_value = 0;
        this.sell_resource_value = 0;
        this.buildarea_value = 0;
    }

    /* access modifiers changed from: private */
    public void setRentSearch() {
        this.map_forth_layout.setVisibility(0);
        this.ico_third.setVisibility(0);
        this.searchPop.setSearch_type(ActionCode.RENT_SEARCH);
        this.text_map_first.setText((int) R.string.btn_region_text);
        setTagText(this.text_map_second, this.res_infofrom);
        setTagText(this.text_map_third, this.res_rent);
        setTagText(this.text_map_forth, this.res_rent_type);
        this.rent_value = 0;
        this.rent_resource_value = 0;
        this.rent_type_value = 0;
    }

    private void addNormalSearchListener(View layout, TextView showDataView, ArrayList dataList, int choseType) {
        final View view = layout;
        final TextView textView = showDataView;
        final ArrayList arrayList = dataList;
        final int i = choseType;
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapSearchHomeActivity.toNewDetail = false;
                SearchConditionPopView.refreshViews(MapSearchHomeActivity.this.views, MapSearchHomeActivity.this.textViews, view, textView, MapSearchHomeActivity.this.context);
                MapSearchHomeActivity.this.searchPop.setShowGroup(false);
                MapSearchHomeActivity.this.searchPop.setGradeData(arrayList, textView, SearchConditionPopView.getTagData(textView.getTag()), i);
            }
        });
    }

    /* access modifiers changed from: private */
    public void initSearchItem() {
        initMap();
        if (this.type == 0) {
            this.regionList = AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get(HouseBaseInfo.DISTRICT), true);
        } else {
            this.regionList = AppMethods.mapKeyTolistSubWay(this.searchConfig.getCoordinate(), true);
        }
        this.subwayMap = this.searchConfig.getMetro();
        this.map_first_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapSearchHomeActivity.toNewDetail = false;
                SearchConditionPopView.refreshViews(MapSearchHomeActivity.this.views, MapSearchHomeActivity.this.textViews, MapSearchHomeActivity.this.map_first_layout, MapSearchHomeActivity.this.text_map_first, MapSearchHomeActivity.this.context);
                MapSearchHomeActivity.this.searchPop.setShowDataView(MapSearchHomeActivity.this.text_map_first);
                MapSearchHomeActivity.this.searchPop.setRegionOrSubway(MapSearchHomeActivity.this.text_map_first);
                if (MapSearchHomeActivity.this.subwayMap == null || MapSearchHomeActivity.this.subwayMap.isEmpty()) {
                    MapSearchHomeActivity.this.searchPop.setShowGroup(false);
                    MapSearchHomeActivity.this.searchPop.setGradeData(MapSearchHomeActivity.this.regionList, MapSearchHomeActivity.this.text_map_first, SearchConditionPopView.getTagData(MapSearchHomeActivity.this.text_map_first.getTag()), 1);
                    return;
                }
                MapSearchHomeActivity.this.searchPop.setShowGroup(true);
                if (MapSearchHomeActivity.this.checkedRegion) {
                    MapSearchHomeActivity.this.searchPop.setGradeData(MapSearchHomeActivity.this.regionList, MapSearchHomeActivity.this.text_map_first, SearchConditionPopView.getTagData(MapSearchHomeActivity.this.text_map_first.getTag()), 1);
                } else {
                    MapSearchHomeActivity.this.searchPop.setGradeData(MapSearchHomeActivity.this.subwayMap, MapSearchHomeActivity.this.text_map_first, SearchConditionPopView.getTagData(MapSearchHomeActivity.this.text_map_first.getTag()), 2);
                }
                MapSearchHomeActivity.this.condition_group.setVisibility(0);
            }
        });
        if (this.type == 1) {
            addNormalSearchListener(this.map_second_layout, this.text_map_second, this.searchConfig.getSell_config().get(HouseBaseInfo.INFOFROM), 8);
            addNormalSearchListener(this.map_third_layout, this.text_map_third, this.searchConfig.getSell_config().get("price"), 6);
            addNormalSearchListener(this.map_forth_layout, this.text_map_forth, this.searchConfig.getSell_config().get("buildarea"), 7);
        } else if (this.type == 2) {
            addNormalSearchListener(this.map_second_layout, this.text_map_second, this.searchConfig.getSell_config().get(HouseBaseInfo.INFOFROM), 8);
            addNormalSearchListener(this.map_third_layout, this.text_map_third, this.searchConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT), 9);
            addNormalSearchListener(this.map_forth_layout, this.text_map_forth, this.searchConfig.getSell_config().get(HouseBaseInfo.RENTTYPE), 11);
        } else {
            addNormalSearchListener(this.map_second_layout, this.text_map_second, AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get("price"), false), 4);
            addNormalSearchListener(this.map_third_layout, this.text_map_third, AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get("channel"), false), 5);
        }
    }

    /* access modifiers changed from: private */
    public void clearRegionTag() {
        regionItems = null;
        this.conPoint = null;
        this.text_map_first.setTag(null);
        this.searchPop.setFirstExpand(null);
        this.searchPop.setFirstGrade(null);
        if (this.maptype == 11) {
            setRegionText();
        } else {
            this.text_map_first.setText(this.res_region);
        }
    }

    /* access modifiers changed from: private */
    public void clearNormalTag() {
        if (this.maptype == 11) {
            setNewSearch();
        } else if (this.maptype == 12) {
            setSellSearch();
        } else if (this.maptype == 13) {
            setRentSearch();
        }
    }

    /* access modifiers changed from: private */
    public void containPoint(GeoPoint center) {
        if (center != null && this.mapView != null) {
            Projection projection = this.mapView.getProjection();
            Point point = new Point();
            if (projection != null) {
                projection.toPixels(center, point);
                if (!new Rect(0, 0, getMapView().getMeasuredWidth(), getMapView().getMeasuredHeight()).contains(point.x, point.y)) {
                    clearRegionTag();
                    setRegionText();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            toNewDetail = false;
        }
        if (HouseBlockOverlay.dismiss && hasFocus) {
            toBlockDetail = false;
        }
    }

    private void initMap() {
        this.cityId = ((NewHouseApplication) this.mApplication).getCity();
        if (AppMethods.isVirtual(this.cityId)) {
            this.condition_layout.setVisibility(8);
            setMapZoom(15);
            this.regionShow = 2;
        } else {
            this.condition_layout.setVisibility(0);
            setMapZoom(13);
            clearOverlay(this.houseOverlay);
            this.houseOverlay.removeAll();
            this.regionShow = 1;
        }
        new GetHouseOverLayTask(this.context).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setJustNewHouse() {
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapSearchHomeActivity.this.clearFinish();
            }
        });
        this.btMapType = (Button) findViewById(R.id.btMapType);
        this.mapTypeLayout = (RadioGroup) findViewById(R.id.mapTypeLayout);
        this.btMapTypeNew = (RadioButton) findViewById(R.id.btMapTypeNew);
        this.btMapTypeSell = (RadioButton) findViewById(R.id.btMapTypeSell);
        this.btMapTyperent = (RadioButton) findViewById(R.id.btMapTypeRent);
        this.maplayout = findViewById(R.id.maplayout);
        this.condition_layout = findViewById(R.id.condition_layout);
        this.map_first_layout = findViewById(R.id.map_first_layout);
        this.map_second_layout = findViewById(R.id.map_second_layout);
        this.map_third_layout = findViewById(R.id.map_third_layout);
        this.map_forth_layout = findViewById(R.id.map_forth_layout);
        this.ico_third = findViewById(R.id.ico_third);
        this.text_map_first = (TextView) findViewById(R.id.text_map_first);
        this.text_map_second = (TextView) findViewById(R.id.text_map_second);
        this.text_map_third = (TextView) findViewById(R.id.text_map_third);
        this.text_map_forth = (TextView) findViewById(R.id.text_map_forth);
        this.views.add(this.map_first_layout);
        this.views.add(this.map_second_layout);
        this.views.add(this.map_third_layout);
        this.views.add(this.map_forth_layout);
        this.textViews.add(this.text_map_first);
        this.textViews.add(this.text_map_second);
        this.textViews.add(this.text_map_third);
        this.textViews.add(this.text_map_forth);
        this.blockviews.add(this.head_view);
        this.blockviews.add(this.condition_layout);
        this.blockviews.add(this.maplayout);
        setJustNewHouse();
        Animation.AnimationListener mapTypeAnimationListener = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                MapSearchHomeActivity.this.isAnimating = true;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                MapSearchHomeActivity.this.isAnimating = false;
            }
        };
        this.showMapTypeAnimation = new TranslateAnimation(1, 1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
        this.showMapTypeAnimation.setDuration((long) this.animateDuration);
        this.showMapTypeAnimation.setInterpolator(new AccelerateInterpolator());
        this.showMapTypeAnimation.setAnimationListener(mapTypeAnimationListener);
        this.showMapTypeAnimation.setFillAfter(true);
        this.hideMapTypeAnimation = new TranslateAnimation(1, SystemUtils.JAVA_VERSION_FLOAT, 1, 1.0f, 1, SystemUtils.JAVA_VERSION_FLOAT, 1, SystemUtils.JAVA_VERSION_FLOAT);
        this.hideMapTypeAnimation.setDuration((long) this.animateDuration);
        this.hideMapTypeAnimation.setInterpolator(new AccelerateInterpolator());
        this.hideMapTypeAnimation.setAnimationListener(mapTypeAnimationListener);
        this.hideMapTypeAnimation.setFillAfter(true);
        this.btMapType.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                synchronized (MapSearchHomeActivity.this) {
                    if (!MapSearchHomeActivity.this.isAnimating) {
                        MapSearchHomeActivity.this.showMapTypeHanlder.sendEmptyMessage(0);
                    }
                }
            }
        });
        this.res_region = getResources().getString(R.string.text_region_title);
        this.res_price = getResources().getString(R.string.text_search_price);
        this.res_channel = getResources().getString(R.string.text_search_channel);
        this.res_infofrom = getResources().getString(R.string.text_resource_title);
        this.res_total_price = getResources().getString(R.string.text_total_price_title);
        this.res_rent = getResources().getString(R.string.text_rent_title);
        this.res_area = getResources().getString(R.string.text_rent_buildarea_title);
        this.res_rent_type = getResources().getString(R.string.text_rent_type_title);
        this.mapTypeLayout.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                MapSearchHomeActivity.this.clearRegionTag();
                MapSearchHomeActivity.this.houseOverlay.removeAll();
                MapSearchHomeActivity.this.overlayManager.populate();
                MapSearchHomeActivity.toBlockDetail = false;
                MapSearchHomeActivity.toNewDetail = false;
                HouseBlockOverlay.dismiss = false;
                if (checkedId == R.id.btMapTypeNew) {
                    MapSearchHomeActivity.this.maptype = 11;
                    MapSearchHomeActivity.this.type = 0;
                    MapSearchHomeActivity.this.setNewSearch();
                } else if (checkedId == R.id.btMapTypeSell) {
                    MapSearchHomeActivity.this.maptype = 12;
                    MapSearchHomeActivity.this.type = 1;
                    MapSearchHomeActivity.this.setSellSearch();
                } else if (checkedId == R.id.btMapTypeRent) {
                    MapSearchHomeActivity.this.maptype = 13;
                    MapSearchHomeActivity.this.type = 2;
                    MapSearchHomeActivity.this.setRentSearch();
                }
                MapSearchHomeActivity.this.getConfigInfo(MapSearchHomeActivity.this.type);
                MapSearchHomeActivity.this.showMapTypeHanlder.sendEmptyMessage(0);
                MapSearchHomeActivity.this.refreshMapData();
            }
        });
        this.mapView = (MapView) findViewById(R.id.mapview);
        this.mapController = this.mapView.getController();
        this.house_marker = getResources().getDrawable(R.drawable.bg_house_block_normal);
        this.region_marker = getResources().getDrawable(R.drawable.bg_region_blue);
        this.location_marker = getResources().getDrawable(R.drawable.ico_location_marker);
        this.loadingLayout = findViewById(R.id.loadingLayout);
        this.loadingtxt = (TextView) findViewById(R.id.loadingtxt);
        this.relocation = (Button) findViewById(R.id.relocation);
        this.overlayManager = new OverlayManager(getApplication(), this.mapView);
        this.houseOverlay = this.overlayManager.createOverlay("houses", this.house_marker);
        this.regionOverlay = this.overlayManager.createOverlay("region", this.region_marker);
        this.myPositionOverlay = this.overlayManager.createOverlay("myposition", this.location_marker);
        this.houseBlock = new HouseBlockOverlay(this.context, this.overlayManager, this.regionOverlay, this.houseOverlay, this.mapController, this.mApplication, this.mapView, this.handler);
        this.houseBlock.setBlockviews(this.blockviews);
        noLocationMap();
        ManagedOverlayGestureDetector.OnOverlayGestureListener gestureListener = new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
            public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
                return false;
            }

            public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (point == null) {
                    return true;
                }
                MapSearchHomeActivity.this.moveMapToPoint(point);
                return true;
            }

            public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
            }

            public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item != null) {
                    MapSearchHomeActivity.this.clickTap(item);
                }
            }

            public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
                return false;
            }

            public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item == null) {
                    return false;
                }
                MapSearchHomeActivity.this.clickTap(item);
                return false;
            }
        };
        LazyLoadCallback regionCallback = new LazyLoadCallback() {
            public List<ManagedOverlayItem> lazyload(GeoPoint leftMap, GeoPoint bottmMap, ManagedOverlay overlay) throws LazyLoadException {
                int curZoom = MapSearchHomeActivity.this.mapView.getZoomLevel();
                MapSearchHomeActivity.this.cityId = ((NewHouseApplication) MapSearchHomeActivity.this.mApplication).getCity();
                if (AppMethods.isVirtual(MapSearchHomeActivity.this.cityId)) {
                    return null;
                }
                if (curZoom < 15) {
                    try {
                        MapSearchHomeActivity.this.handler.sendEmptyMessage(1);
                        MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.houseOverlay);
                        if (MapSearchHomeActivity.this.regionShow != 1) {
                            return null;
                        }
                        MapSearchHomeActivity.regionItems = HouseBlockOverlay.regionItems;
                        if (MapSearchHomeActivity.regionItems == null || MapSearchHomeActivity.regionItems.size() <= 0) {
                            return MapSearchHomeActivity.this.houseBlock.getRegionOverlayItem(MapSearchHomeActivity.this.maptype);
                        }
                        MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.regionOverlay);
                        return MapSearchHomeActivity.regionItems;
                    } catch (HtppApiException | UnknownHostException e) {
                        return null;
                    } catch (NetworkUnavailableException e2) {
                        MapSearchHomeActivity.this.handler.sendEmptyMessage(4);
                        return null;
                    } catch (HttpParseException e3) {
                        MapSearchHomeActivity.this.handler.sendEmptyMessage(5);
                        return null;
                    }
                } else {
                    MapSearchHomeActivity.this.regionShow = 2;
                    MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.regionOverlay);
                    return null;
                }
            }
        };
        LazyLoadCallback houseCallback = new LazyLoadCallback() {
            public List<ManagedOverlayItem> lazyload(GeoPoint leftMap, GeoPoint bottmMap, ManagedOverlay overlay) throws LazyLoadException {
                int curZoom = MapSearchHomeActivity.this.mapView.getZoomLevel();
                try {
                    MapSearchHomeActivity.this.cityId = ((NewHouseApplication) MapSearchHomeActivity.this.mApplication).getCity();
                    if (AppMethods.isVirtual(MapSearchHomeActivity.this.cityId)) {
                        if (MapSearchHomeActivity.toNewDetail) {
                            MapSearchHomeActivity.houseItems = HouseBlockOverlay.houseItems;
                            if (MapSearchHomeActivity.houseItems == null || MapSearchHomeActivity.houseItems.size() <= 0) {
                                return null;
                            }
                            MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.houseOverlay);
                            return MapSearchHomeActivity.houseItems;
                        } else if (MapSearchHomeActivity.this.regionShow != 2 || leftMap == null || bottmMap == null) {
                            return null;
                        } else {
                            return MapSearchHomeActivity.this.getHouseOverlayItem(leftMap, bottmMap);
                        }
                    } else if (curZoom >= 15) {
                        MapSearchHomeActivity.this.handler.sendEmptyMessage(2);
                        MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.regionOverlay);
                        if (MapSearchHomeActivity.toNewDetail) {
                            MapSearchHomeActivity.houseItems = HouseBlockOverlay.houseItems;
                            if (MapSearchHomeActivity.houseItems == null || MapSearchHomeActivity.houseItems.size() <= 0) {
                                return null;
                            }
                            MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.houseOverlay);
                            return MapSearchHomeActivity.houseItems;
                        } else if (MapSearchHomeActivity.this.regionShow != 2 || leftMap == null || bottmMap == null) {
                            return null;
                        } else {
                            return MapSearchHomeActivity.this.getHouseOverlayItem(leftMap, bottmMap);
                        }
                    } else {
                        MapSearchHomeActivity.this.regionShow = 1;
                        return null;
                    }
                } catch (NetworkUnavailableException e) {
                    MapSearchHomeActivity.this.handler.sendEmptyMessage(4);
                    return null;
                } catch (HtppApiException e2) {
                    return null;
                } catch (HttpParseException e3) {
                    MapSearchHomeActivity.this.handler.sendEmptyMessage(5);
                    return null;
                }
            }
        };
        LazyLoadListener loadListener = new LazyLoadListener() {
            public void onBegin(ManagedOverlay overlay) {
                MapSearchHomeActivity.this.showStatus(R.string.loading);
            }

            public void onSuccess(ManagedOverlay overlay) {
                MapSearchHomeActivity.this.hideStatus();
            }

            public void onError(LazyLoadException exception, ManagedOverlay overlay) {
                MapSearchHomeActivity.this.hideStatus();
                ActivityUtil.showToast(MapSearchHomeActivity.this, (int) R.string.msg_load_error);
            }
        };
        if (AppMethods.isVirtual(this.cityId)) {
            setMapZoom(15);
        } else {
            setMapZoom(13);
        }
        this.houseOverlay.setOnOverlayGestureListener(gestureListener);
        this.houseOverlay.setLazyLoadCallback(houseCallback);
        this.houseOverlay.setLazyLoadListener(loadListener);
        this.regionOverlay.setOnOverlayGestureListener(gestureListener);
        this.regionOverlay.setLazyLoadCallback(regionCallback);
        this.regionOverlay.setLazyLoadListener(loadListener);
        this.overlayManager.populate();
    }

    /* access modifiers changed from: private */
    public void clearOverlay(ManagedOverlay overlay) {
        List<ManagedOverlayItem> items = overlay.getOverlayItems();
        if (items != null) {
            try {
                items.clear();
            } catch (Exception e) {
                CorePreferences.ERROR(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        setRelocationVisible();
        getConfigInfo(this.type);
        int fromtype = getIntent().getIntExtra(INTENT_MAP_TYPE, 0);
        if (fromtype != 0) {
            this.mapTypeLayout.setVisibility(8);
            this.btMapType.setVisibility(8);
            this.maptype = fromtype;
        }
        if (this.maptype == 11) {
            this.btMapTypeNew.setChecked(true);
            setNewSearch();
        } else if (this.maptype == 12) {
            this.btMapTypeSell.setChecked(true);
            setSellSearch();
        } else if (this.maptype == 13) {
            this.btMapTyperent.setChecked(true);
            setRentSearch();
        }
        if (DeviceUtil.isOpenLoaction(this)) {
            reloaction(true);
        } else {
            noLocationMap();
        }
    }

    /* access modifiers changed from: private */
    public void clickTap(ManagedOverlayItem item) {
        if (this.regionShow == 1) {
            GeoPoint geopoint = item.getPoint();
            this.mapView.getController().setZoom(15);
            this.regionShow = 2;
            moveMapToPoint(geopoint);
            clearOverlay(this.regionOverlay);
            this.overlayManager.populate();
        } else if (this.regionShow == 2) {
            try {
                String blockSnippet = item.getSnippet();
                House house = (House) ReflectUtil.copy(House.class, new JSONObject(blockSnippet));
                if (!TextUtils.isEmpty(((Block) ReflectUtil.copy(Block.class, new JSONObject(blockSnippet))).getId()) || !TextUtils.isEmpty(house.getH_id())) {
                    if (this.maptype == 11) {
                        toNewDetail = true;
                    }
                    ManagedOverlayItem last = this.houseBlock.getLastItem();
                    item.setTag(2);
                    this.houseBlock.setHouseMarker(item, this.maptype);
                    if (last != null && !last.getSnippet().equals(item.getSnippet())) {
                        last.setTag(1);
                        this.houseBlock.setHouseMarker(last, this.maptype);
                    }
                    if (this.maptype == 11) {
                        this.houseBlock.setHouseOverlayTap(item, this.maptype, 0, 0, 0);
                    } else if (this.maptype == 12) {
                        this.houseBlock.setHouseOverlayTap(item, this.maptype, this.total_price_value, this.sell_resource_value, this.buildarea_value);
                    } else if (this.maptype == 13) {
                        this.houseBlock.setHouseOverlayTap(item, this.maptype, this.rent_value, this.rent_resource_value, this.rent_type_value);
                    }
                    this.houseBlock.setLastItem(item);
                    moveMapToPoint(item.getPoint());
                }
                clearOverlay(this.houseOverlay);
                this.overlayManager.populate();
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void showStatus(int resource) {
        this.loadingtxt.setText(resource);
        if (this.statusInAnim == null) {
            this.statusInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);
            this.statusInAnim.setFillAfter(true);
            this.statusInAnim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
        }
        this.loadingLayout.startAnimation(this.statusInAnim);
    }

    /* access modifiers changed from: private */
    public void hideStatus() {
        if (this.statusOutAnim == null) {
            this.statusOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_top);
            this.statusOutAnim.setFillAfter(true);
            this.statusOutAnim.setStartOffset(200);
            this.statusOutAnim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    MapSearchHomeActivity.this.loadingtxt.setText(StringUtils.EMPTY);
                }
            });
        }
        this.loadingLayout.startAnimation(this.statusOutAnim);
    }

    private void setRelocationVisible() {
        this.relocation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapSearchHomeActivity.this.reloaction(true);
            }
        });
    }

    /* access modifiers changed from: private */
    public void reloaction(boolean openGPS) {
        if (!DeviceUtil.isOpenLoaction(this)) {
            if (openGPS) {
                this.openGPSHander.sendEmptyMessage(0);
            }
        } else if (!this.islocating) {
            new RelocationTask(this).execute(new Object[0]);
        }
    }

    private class RelocationTask extends CommonAsyncTask<Location> {
        private final int STATUS_ERROR = -1;
        private final int STATUS_SUCCESS = 1;
        /* access modifiers changed from: private */
        public String locationCity;
        private int stauts = 1;

        public RelocationTask(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MapSearchHomeActivity.this.islocating = true;
            MapSearchHomeActivity.this.showStatus(R.string.text_location);
        }

        public Location onDoInBackgroup() {
            try {
                Location location = MapSearchHomeActivity.this.myLocation.getLocationByBaiduLocApi(true, 10000);
                if (location != null) {
                    CommonResultInfo resultCoord = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getCoordConvertCity(location.getLatitude(), location.getLongitude());
                    if (resultCoord.getResult() == 1) {
                        this.locationCity = resultCoord.getData();
                        this.stauts = 1;
                    } else {
                        this.stauts = -1;
                    }
                }
                this.mApplication.setLocation(location);
                return location;
            } catch (Exception e) {
                CorePreferences.ERROR(e);
                this.stauts = -1;
                return null;
            }
        }

        public void onAfterDoInBackgroup(Location result) {
            if (result == null || this.stauts != 1) {
                Toast.makeText(MapSearchHomeActivity.this, (int) R.string.text_location_error, 0).show();
                MapSearchHomeActivity.this.noLocationMap();
            } else if (!AppMethods.isInCitys(this.locationCity)) {
                MapSearchHomeActivity.this.clearRegionTag();
                MapSearchHomeActivity.this.noLocationMap();
                MapSearchHomeActivity.this.setMyLocation();
            } else if (!this.locationCity.equals(this.mApplication.getCityName())) {
                com.house365.core.util.DialogUtil.showConfrimDialog(MapSearchHomeActivity.this, MapSearchHomeActivity.this.getResources().getString(R.string.text_map_location_city_msg, this.locationCity), MapSearchHomeActivity.this.getResources().getString(R.string.dialog_button_ok), new ConfirmDialogListener() {
                    public void onPositive(DialogInterface dialog) {
                        ((NewHouseApplication) RelocationTask.this.mApplication).setCity(AppArrays.getCity(RelocationTask.this.locationCity));
                        MapSearchHomeActivity.this.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_CHANGE_CITY));
                        MapSearchHomeActivity.this.locationMap();
                        MapSearchHomeActivity.this.setMyLocation();
                    }

                    public void onNegative(DialogInterface dialog) {
                        MapSearchHomeActivity.this.noLocationMap();
                    }
                });
            } else {
                MapSearchHomeActivity.this.clearRegionTag();
                MapSearchHomeActivity.this.locationMap();
                MapSearchHomeActivity.this.setMyLocation();
            }
            MapSearchHomeActivity.this.hideStatus();
            MapSearchHomeActivity.this.islocating = false;
        }
    }

    /* access modifiers changed from: private */
    public void refreshMapData() {
        moveMapToPoint(this.mapView.getMapCenter());
    }

    /* access modifiers changed from: private */
    public void setMyLocation() {
        GeoPoint geopoint = BaiduMapUtil.getPoint(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude());
        this.myPositionOverlay.removeAll();
        this.myPositionOverlay.add(new ManagedOverlayItem(geopoint, "myposition", null));
    }

    /* access modifiers changed from: private */
    public void locationMap() {
        moveMapToPoint(BaiduMapUtil.getPoint(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude()));
    }

    /* access modifiers changed from: private */
    public void noLocationMap() {
        double[] location = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(location[0], location[1]));
        this.myPositionOverlay.removeAll();
    }

    /* access modifiers changed from: private */
    public void moveMapToPoint(GeoPoint geopoint) {
        if (getMapView() != null && this.mapView.getController() != null) {
            this.mapView.getController().animateTo(geopoint);
            this.mapView.getController().setCenter(geopoint);
        }
    }

    /* access modifiers changed from: private */
    public void setNodata() {
        if (this.text_map_first.getTag() == null && this.text_map_second.getTag() == null && this.text_map_third.getTag() == null && this.text_map_forth.getTag() == null) {
            if (this.maptype == 11) {
                Toast.makeText(this, (int) R.string.text_house_nodata, 0).show();
            } else if (this.maptype == 12 || this.maptype == 13) {
                Toast.makeText(this, (int) R.string.text_block_nodata, 0).show();
            }
        } else if (this.maptype == 11) {
            Toast.makeText(this, (int) R.string.text_new_map_nodata, 0).show();
        } else if (this.maptype == 12 || this.maptype == 13) {
            Toast.makeText(this, (int) R.string.text_sell_rent_map_nodata, 0).show();
        }
    }

    private class GetHouseOverLayTask extends CommonAsyncTask<List<ManagedOverlayItem>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<ManagedOverlayItem>) ((List) obj));
        }

        public GetHouseOverLayTask(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MapSearchHomeActivity.this.showStatus(R.string.loading);
        }

        public void onAfterDoInBackgroup(List<ManagedOverlayItem> result) {
            MapSearchHomeActivity.this.hideStatus();
            if (result == null || result.size() <= 0) {
                MapSearchHomeActivity.this.houseOverlay.removeAll();
                MapSearchHomeActivity.this.clearOverlay(MapSearchHomeActivity.this.houseOverlay);
                MapSearchHomeActivity.this.setNodata();
            } else if (MapSearchHomeActivity.this.regionShow == 1) {
                MapSearchHomeActivity.this.regionOverlay.addAll(result);
            } else if (MapSearchHomeActivity.this.regionShow == 2) {
                MapSearchHomeActivity.this.houseOverlay.addAll(result);
            }
            MapSearchHomeActivity.this.overlayManager.populate();
            MapSearchHomeActivity.this.isdoing = false;
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            MapSearchHomeActivity.this.hideStatus();
            Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
            MapSearchHomeActivity.this.isdoing = false;
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            MapSearchHomeActivity.this.hideStatus();
            MapSearchHomeActivity.this.isdoing = false;
            super.onHttpRequestError();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            MapSearchHomeActivity.this.hideStatus();
            MapSearchHomeActivity.this.isdoing = false;
            super.onParseError();
        }

        public List<ManagedOverlayItem> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            try {
                if (MapSearchHomeActivity.this.regionShow == 1) {
                    return MapSearchHomeActivity.this.houseBlock.getRegionOverlayItem(MapSearchHomeActivity.this.maptype);
                }
                if (MapSearchHomeActivity.this.regionShow != 2) {
                    return null;
                }
                GeoPoint leftMap = MapSearchHomeActivity.this.getGeoPointOfScreen(BaseBaiduMapActivity.GeoPointSceen.TOP_LEFT);
                GeoPoint bottmMap = MapSearchHomeActivity.this.getGeoPointOfScreen(BaseBaiduMapActivity.GeoPointSceen.BOTTOM_RIGHT);
                if (leftMap == null || bottmMap == null) {
                    return null;
                }
                return MapSearchHomeActivity.this.getHouseOverlayItem(leftMap, bottmMap);
            } catch (LazyLoadException e) {
                e.printStackTrace();
                return null;
            } catch (UnknownHostException e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: private */
    public List<ManagedOverlayItem> getHouseOverlayItem(GeoPoint leftMap, GeoPoint bottmMap) throws LazyLoadException, NetworkUnavailableException, HtppApiException, HttpParseException {
        List<ManagedOverlayItem> items = new LinkedList<>();
        if (this.maptype == 11) {
            return this.houseBlock.getNewHouseOverlayItem(leftMap, bottmMap, this.maptype, this.pricevalue, this.channelvalue);
        } else if (this.maptype == 12) {
            return this.houseBlock.getSellHouseOverlayItem(leftMap, bottmMap, this.maptype, String.valueOf(this.total_price_value), String.valueOf(this.sell_resource_value), String.valueOf(this.buildarea_value));
        } else if (this.maptype != 13) {
            return items;
        } else {
            return this.houseBlock.getRentHouseOverlayItem(leftMap, bottmMap, this.maptype, String.valueOf(this.rent_value), String.valueOf(this.rent_resource_value), String.valueOf(this.rent_type_value));
        }
    }

    public void getConfigInfo(int type2) {
        GetConfigTask getConfigTask = new GetConfigTask(this.context, R.string.loading);
        getConfigTask.setType(type2);
        getConfigTask.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                MapSearchHomeActivity.this.searchConfig = info;
                if (MapSearchHomeActivity.this.searchConfig != null) {
                    MapSearchHomeActivity.this.initSearchItem();
                }
            }

            public void OnError(HouseBaseInfo info) {
                Toast.makeText(MapSearchHomeActivity.this.context, (int) R.string.text_city_config_error, 0).show();
            }
        });
        getConfigTask.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        clearFinish();
        return false;
    }

    /* access modifiers changed from: private */
    public void clearFinish() {
        this.overlayManager.close();
        HouseBlockOverlay.regionItems = null;
        HouseBlockOverlay.houseItems = null;
        finish();
    }
}
