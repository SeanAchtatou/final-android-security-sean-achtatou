package com.house365.newhouse.ui.secondsell;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.resource.ResourceUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.secondrent.SecondRentOfferActivity;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddBlockActivity extends BaseCommonActivity {
    public static final String INTENT_FROM_TYPE = "from_type";
    private Button btn_submit;
    protected HouseBaseInfo configInfo;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public boolean disClicked;
    protected String district;
    /* access modifiers changed from: private */
    public int districtValue;
    private EditText et_addr;
    private EditText et_block;
    private int fromType;
    private HeadNavigateView head_view;
    protected HashMap<String, String> publishVal = new HashMap<>();
    protected String street;
    protected String streetValue;
    /* access modifiers changed from: private */
    public TextView tv_district;
    private View view_district;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.add_block);
        this.context = this;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AddBlockActivity.this.finish();
            }
        });
        this.tv_district = (TextView) findViewById(R.id.tv_district);
        this.et_block = (EditText) findViewById(R.id.et_block);
        this.et_addr = (EditText) findViewById(R.id.et_addr);
        this.view_district = findViewById(R.id.view_district);
        this.view_district.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AddBlockActivity.this.disClicked = true;
                if (AddBlockActivity.this.configInfo != null) {
                    AddBlockActivity.this.showDistrictDialog();
                } else {
                    AddBlockActivity.this.getConfig(R.string.loading);
                }
            }
        });
        this.btn_submit = (Button) findViewById(R.id.btn_submit);
        this.btn_submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AddBlockActivity.this.showEmptyToast();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showEmptyToast() {
        if (TextUtils.isEmpty(this.et_block.getText())) {
            showToast((int) R.string.text_block_hint);
        } else if (TextUtils.isEmpty(this.tv_district.getText())) {
            showToast((int) R.string.text_chose_hint);
        } else {
            sendBroadcast(new Intent(ActionCode.INTENT_ACTION_FINISH_BLOCKCHOOSE));
            if (this.fromType == 1) {
                SecondSellOfferActivity.district = this.district;
                SecondSellOfferActivity.street = this.street;
                SecondSellOfferActivity.blockName = this.et_block.getText().toString();
            }
            if (this.fromType == 2) {
                SecondRentOfferActivity.district = this.district;
                SecondRentOfferActivity.street = this.street;
                SecondRentOfferActivity.blockName = this.et_block.getText().toString();
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.fromType = getIntent().getIntExtra(INTENT_FROM_TYPE, 0);
        if (this.fromType == 1 || this.fromType == 2) {
            getConfig(R.string.loading);
        }
    }

    /* access modifiers changed from: private */
    public void showDistrictDialog() {
        List<String> distList = this.configInfo.getSell_config().get(HouseBaseInfo.DISTRICT);
        distList.remove(0);
        final String[] districts = (String[]) distList.toArray(new String[0]);
        new AlertDialog.Builder(this.context).setSingleChoiceItems(districts, this.districtValue, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int which2 = which + 1;
                if (which2 >= 0) {
                    AddBlockActivity.this.districtValue = which2;
                    AddBlockActivity.this.district = districts[AddBlockActivity.this.districtValue];
                    if (which2 == 0) {
                        AddBlockActivity.this.setChooseTextView(AddBlockActivity.this.tv_district, AddBlockActivity.this.district);
                        dialog.dismiss();
                        return;
                    }
                    if (which2 > 0) {
                        final Map<String, String> districtStreetMap = AddBlockActivity.this.configInfo.getDistrictStreet().get(AddBlockActivity.this.district);
                        final String[] districtStreets = (String[]) districtStreetMap.keySet().toArray(new String[0]);
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddBlockActivity.this.context);
                        int parseValueToIndex = AddBlockActivity.parseValueToIndex(districtStreets, HouseBaseInfo.parseValueToKey(districtStreetMap, new StringBuilder(String.valueOf(AddBlockActivity.this.streetValue)).toString()));
                        final String[] strArr = districts;
                        builder.setSingleChoiceItems(districtStreets, parseValueToIndex, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which >= 0) {
                                    AddBlockActivity.this.street = districtStreets[which];
                                    AddBlockActivity.this.streetValue = (String) districtStreetMap.get(AddBlockActivity.this.street);
                                    AddBlockActivity.this.setChooseTextView(AddBlockActivity.this.tv_district, String.valueOf(strArr[AddBlockActivity.this.districtValue]) + "+" + districtStreets[which]);
                                }
                                dialog.dismiss();
                            }
                        }).setTitle((int) R.string.street).create().show();
                    }
                    dialog.dismiss();
                }
            }
        }).setTitle((int) R.string.district).create().show();
    }

    public static int parseValueToIndex(String[] arrays, String value) {
        if (arrays != null) {
            for (int i = 0; i < arrays.length; i++) {
                if (arrays[i].equals(value)) {
                    return i;
                }
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void setChooseTextView(TextView tv, CharSequence str) {
        tv.setText(str);
        tv.setTextColor(ResourceUtil.getColor(this, R.color.black));
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid);
        getConfig.setType(this.fromType);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info != null) {
                    AddBlockActivity.this.configInfo = info;
                    if (AddBlockActivity.this.disClicked) {
                        AddBlockActivity.this.showDistrictDialog();
                    }
                }
            }

            public void OnError(HouseBaseInfo info) {
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.disClicked = false;
    }
}
