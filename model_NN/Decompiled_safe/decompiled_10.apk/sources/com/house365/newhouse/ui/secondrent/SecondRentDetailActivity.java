package com.house365.newhouse.ui.secondrent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.ScrollViewHorzFling;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.block.BlockDetailActivity;
import com.house365.newhouse.ui.block.BlockHouseListActivity;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.newhome.NewHouseAlbumActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.secondsell.AskActivity;
import com.house365.newhouse.ui.secondsell.SecondSell_B_otherActivity;
import com.house365.newhouse.ui.secondsell.SecondSell_houseRemarkActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

public class SecondRentDetailActivity extends BaseBaiduMapActivity {
    public static final String INTENT_ID = "id";
    /* access modifiers changed from: private */
    public String app = App.RENT;
    View b_other_layout;
    View block_address_layout;
    View block_layout;
    TextView block_name;
    private LinearLayout bottomBar;
    TextView btn_ask;
    private TextView btn_refer;
    private String from_block_list = StringUtils.EMPTY;
    ImageView h_pic;
    /* access modifiers changed from: private */
    public HeadNavigateView head_view;
    TextView house_Renovation;
    TextView house_area;
    TextView house_b_other;
    TextView house_company;
    View house_company_layout;
    /* access modifiers changed from: private */
    public SecondHouse house_detail;
    TextView house_floor;
    TextView house_forward;
    TextView house_instruct;
    TextView house_payment;
    TextView house_person_name;
    private RelativeLayout house_pic_layout;
    View house_remark_layout;
    TextView house_rentType;
    TextView house_rent_count;
    TextView house_rent_price;
    TextView house_street;
    TextView house_tel;
    TextView house_title;
    TextView house_type;
    TextView house_update_time;
    TextView house_year;
    /* access modifiers changed from: private */
    public String id;
    boolean isShowPic;
    private MapView mMapView;
    TextView more_h_pic;
    TextView mright;
    ImageView person_photo;
    View rent_count_layout;
    TextView rent_detail_sale_state;
    TextView text_block_infotype;
    TextView text_districk;
    View title_block_address_layout;
    TextView tv_block_address;
    TextView txt_expand_sell_bo_ther;
    TextView txt_expand_sell_instruct;
    TextView update_time;

    public MapView getMapView() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.secondrent_detail_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.bottomBar = (LinearLayout) findViewById(R.id.bottom_detail_tool);
        this.bottomBar.setVisibility(8);
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentDetailActivity.this.finish();
            }
        });
        ((ScrollViewHorzFling) findViewById(R.id.scrollView)).smoothScrollTo(0, 0);
        this.house_company_layout = findViewById(R.id.house_company_layout);
        this.house_pic_layout = (RelativeLayout) findViewById(R.id.house_pic_layout);
        this.h_pic = (ImageView) findViewById(R.id.h_pic);
        this.h_pic.setVisibility(0);
        this.isShowPic = ((NewHouseApplication) this.mApplication).isEnableImg();
        if (!this.isShowPic) {
            this.house_pic_layout.setEnabled(false);
        }
        this.house_pic_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondRentDetailActivity.this.startActivity(new Intent(SecondRentDetailActivity.this, NewHouseAlbumActivity.class));
            }
        });
        this.btn_ask = (TextView) findViewById(R.id.btn_apply);
        this.btn_ask.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseAnalyse.onViewClick(SecondRentDetailActivity.this, "问房", App.RENT, SecondRentDetailActivity.this.house_detail.getId());
                Intent intent = new Intent(SecondRentDetailActivity.this, AskActivity.class);
                intent.putExtra("house", SecondRentDetailActivity.this.house_detail);
                intent.putExtra(CouponDetailsActivity.INTENT_TYPE, 2);
                SecondRentDetailActivity.this.startActivity(intent);
            }
        });
        this.btn_ask.setText(getString(R.string.ask_housed));
        this.btn_ask.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ask_house, 0, 0, 0);
        this.rent_count_layout = findViewById(R.id.rent_count_layout);
        this.mMapView = (MapView) findViewById(R.id.mapView);
        this.house_update_time = (TextView) findViewById(R.id.house_update_time);
        this.house_instruct = (TextView) findViewById(R.id.house_instruct);
        this.rent_detail_sale_state = (TextView) findViewById(R.id.rent_detail_sale_state);
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.btn_refer.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.phone, 0, 0, 0);
        this.more_h_pic = (TextView) findViewById(R.id.more_h_pic);
        this.person_photo = (ImageView) findViewById(R.id.person_photo);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.house_rent_price = (TextView) findViewById(R.id.txt_rent_price);
        this.house_area = (TextView) findViewById(R.id.house_area);
        this.house_type = (TextView) findViewById(R.id.detail_house_type);
        this.house_rent_count = (TextView) findViewById(R.id.rent_count);
        this.house_rentType = (TextView) findViewById(R.id.text_rent_type_title);
        this.house_floor = (TextView) findViewById(R.id.house_floor);
        this.house_Renovation = (TextView) findViewById(R.id.house_Renovation);
        this.house_forward = (TextView) findViewById(R.id.house_forward);
        this.tv_block_address = (TextView) findViewById(R.id.tv_block_address);
        this.house_b_other = (TextView) findViewById(R.id.house_b_other);
        this.house_area = (TextView) findViewById(R.id.house_area);
        this.house_type = (TextView) findViewById(R.id.detail_house_type);
        this.text_block_infotype = (TextView) findViewById(R.id.house_channel);
        this.house_floor = (TextView) findViewById(R.id.house_floor);
        this.house_Renovation = (TextView) findViewById(R.id.house_Renovation);
        this.house_forward = (TextView) findViewById(R.id.house_forward);
        this.block_name = (TextView) findViewById(R.id.block_name);
        this.text_districk = (TextView) findViewById(R.id.text_districk);
        this.house_street = (TextView) findViewById(R.id.house_street);
        this.house_person_name = (TextView) findViewById(R.id.house_name);
        this.house_tel = (TextView) findViewById(R.id.house_tel);
        this.house_company = (TextView) findViewById(R.id.house_company);
        this.house_payment = (TextView) findViewById(R.id.priceterm);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.block_address_layout = findViewById(R.id.block_address_layout);
        this.title_block_address_layout = findViewById(R.id.title_block_address_layout);
        this.house_remark_layout = findViewById(R.id.house_remark_layout);
        this.b_other_layout = findViewById(R.id.b_other_layout);
        this.block_layout = findViewById(R.id.block_layout);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.id = getIntent().getStringExtra("id");
        this.from_block_list = getIntent().getStringExtra("FROM_BLOCK_LIST");
        new GetHouseInfoTask(this, R.string.house_info_load).execute(new Object[0]);
    }

    public void initInfo(final SecondHouse house_detail2) {
        if (house_detail2 != null) {
            this.bottomBar.setVisibility(0);
            String pic_first = house_detail2.getPics();
            if (pic_first == null || !this.isShowPic) {
                setImage(this.h_pic, StringUtils.EMPTY, R.drawable.bg_default_ad, 1);
            } else {
                setImage(this.h_pic, pic_first, R.drawable.bg_default_ad, 1);
            }
            final NewHouseApplication application = (NewHouseApplication) this.mApplication;
            if (application.hasFav(new HouseInfo(App.SELL, house_detail2), App.SELL)) {
                this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow);
            }
            this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HouseAnalyse.onViewClick(SecondRentDetailActivity.this, "收藏", App.RENT, house_detail2.getId());
                    if (application.hasFav(new HouseInfo(App.RENT, house_detail2), App.RENT)) {
                        application.delFavHistory(new HouseInfo(App.RENT, house_detail2), App.RENT);
                        SecondRentDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow);
                        SecondRentDetailActivity.this.showToast((int) R.string.fav_cancle);
                        return;
                    }
                    application.saveFavHistory(new HouseInfo(App.RENT, house_detail2), App.RENT);
                    SecondRentDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_unfollow);
                    SecondRentDetailActivity.this.showToast((int) R.string.fav_success);
                }
            });
            this.house_update_time.setText(house_detail2.getUpdatetime());
            if (house_detail2.getRemark() != null) {
                this.house_instruct.setText(Html.fromHtml(house_detail2.getRemark()));
            }
            if (house_detail2.getState() != null) {
                if (house_detail2.getState().equals("1")) {
                    this.rent_detail_sale_state.setText((int) R.string.text_Urgent_rent);
                    this.rent_detail_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    this.rent_detail_sale_state.setTextAppearance(this, R.style.font12_pop_green);
                } else {
                    this.rent_detail_sale_state.setVisibility(8);
                }
            }
            ((NewHouseApplication) this.mApplication).saveBrowseHistory(new HouseInfo(App.RENT, house_detail2), App.RENT);
            if ((house_detail2.getH_picList().size() > 1) && this.isShowPic) {
                this.more_h_pic.setVisibility(0);
                this.house_pic_layout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(SecondRentDetailActivity.this, AlbumFullScreenActivity.class);
                        intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) house_detail2.getH_picList());
                        SecondRentDetailActivity.this.startActivity(intent);
                    }
                });
            }
            this.head_view.setTvTitleText(house_detail2.getTitle());
            this.house_payment.setText(house_detail2.getPayment());
            this.house_title.setText(house_detail2.getTitle());
            this.house_area.setText(getResources().getString(R.string.house_detail_area, house_detail2.getBuildarea()));
            this.house_type.setText(house_detail2.getApartment());
            this.text_districk.setText(house_detail2.getDistrict());
            this.house_street.setText(house_detail2.getStreetname());
            this.text_block_infotype.setText(house_detail2.getInfotype());
            this.house_rent_price.setText(house_detail2.getPrice());
            this.house_rentType.setText(house_detail2.getRenttype());
            if (house_detail2.getBlockinfo() != null) {
                this.block_name.setText(house_detail2.getBlockinfo().getBlockname());
                if (house_detail2.getBlockinfo().getRentcount() != null) {
                    this.house_rent_count.setText(getResources().getString(R.string.block_house_count_info, house_detail2.getBlockinfo().getRentcount()));
                }
                this.tv_block_address.setText(house_detail2.getBlockinfo().getAddress());
                if (house_detail2.getBlockinfo().getB_other() != null) {
                    this.house_b_other.setText(Html.fromHtml(house_detail2.getBlockinfo().getB_other()));
                }
            }
            this.house_floor.setText(house_detail2.getStory());
            this.house_Renovation.setText(house_detail2.getFitment());
            this.house_forward.setText(house_detail2.getForward());
            if (house_detail2.getBrokerinfo() != null) {
                if (house_detail2.getBrokerinfo().getAgentshortname() == null) {
                    this.house_company_layout.setVisibility(8);
                } else {
                    this.house_company_layout.setVisibility(0);
                    this.house_company.setText(house_detail2.getBrokerinfo().getAgentshortname());
                }
                this.house_person_name.setText(house_detail2.getBrokerinfo().getTruename());
                this.house_tel.setText(house_detail2.getBrokerinfo().getTelno());
                setImage(this.person_photo, house_detail2.getBrokerinfo().getSmallphoto(), R.drawable.img_default_small, 1);
                if ("person".equals(house_detail2.getBrokerinfo().getType())) {
                    this.btn_refer.setText(getString(R.string.contact_house_rent));
                } else {
                    this.btn_refer.setText(getString(R.string.contact_middle_man));
                }
                if (house_detail2.getBrokerinfo().getTelno() == null) {
                    this.btn_refer.setEnabled(false);
                } else {
                    this.btn_refer.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (house_detail2.getBrokerinfo().getTelno() != null) {
                                try {
                                    TelUtil.getCallIntent(house_detail2.getBrokerinfo().getTelno(), SecondRentDetailActivity.this, App.RENT);
                                    ((NewHouseApplication) SecondRentDetailActivity.this.mApplication).saveCallHistory(new HouseInfo(App.RENT, house_detail2), App.RENT);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
            MapController mapController = this.mMapView.getController();
            if (house_detail2.getBlockinfo() != null) {
                if (house_detail2.getBlockinfo().getLat() <= 0.0d || house_detail2.getBlockinfo().getLng() <= 0.0d) {
                    this.mMapView.setVisibility(8);
                    this.block_address_layout.setVisibility(8);
                    this.title_block_address_layout.setVisibility(8);
                    this.tv_block_address.setVisibility(8);
                } else {
                    GeoPoint point = BaiduMapUtil.getPoint(house_detail2.getBlockinfo().getLat(), house_detail2.getBlockinfo().getLng());
                    mapController.animateTo(point);
                    mapController.setZoom(15);
                    OverlayManager overlayManager = new OverlayManager(getApplication(), this.mMapView);
                    overlayManager.createOverlay("houses", getResources().getDrawable(R.drawable.ico_marker)).add(new ManagedOverlayItem(point, house_detail2.getBlockinfo().getId(), StringUtils.EMPTY));
                    overlayManager.populate();
                }
            }
            this.block_address_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondRentDetailActivity.this.showToast((int) R.string.no_map);
                        return;
                    }
                    String addr = house_detail2.getBlockinfo().getAddress();
                    if (addr != null) {
                        if (addr.indexOf("（") != -1) {
                            addr = addr.substring(0, addr.indexOf("（"));
                        }
                        if (addr.indexOf("(") != -1) {
                            addr = addr.substring(0, addr.indexOf("("));
                        }
                    } else {
                        addr = StringUtils.EMPTY;
                    }
                    try {
                        SecondRentDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLat())).toString(), new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLng())).toString(), addr));
                    } catch (Exception e) {
                        SecondRentDetailActivity.this.showToast((int) R.string.no_map_app);
                    }
                }
            });
            this.house_remark_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getRemark() != null) {
                        Intent intent = new Intent(SecondRentDetailActivity.this, SecondSell_houseRemarkActivity.class);
                        String txt_house_remark = Html.fromHtml(house_detail2.getRemark()).toString();
                        intent.putExtra("title_name", house_detail2.getTitle());
                        intent.putExtra("txt_house_remark", txt_house_remark);
                        SecondRentDetailActivity.this.startActivity(intent);
                        return;
                    }
                    SecondRentDetailActivity.this.showToast((int) R.string.no_intr);
                }
            });
            this.b_other_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getB_other() != null) {
                        Intent intent = new Intent(SecondRentDetailActivity.this, SecondSell_B_otherActivity.class);
                        String txt_house_B_other = Html.fromHtml(house_detail2.getBlockinfo().getB_other()).toString();
                        intent.putExtra("title_name", house_detail2.getTitle());
                        intent.putExtra("txt_house_B_other", txt_house_B_other);
                        CorePreferences.DEBUG("********txt_house_B_other*****" + ((Object) Html.fromHtml(house_detail2.getBlockinfo().getB_other())));
                        SecondRentDetailActivity.this.startActivity(intent);
                        return;
                    }
                    SecondRentDetailActivity.this.showToast((int) R.string.no_remark);
                }
            });
            this.title_block_address_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondRentDetailActivity.this.showToast((int) R.string.no_map);
                        return;
                    }
                    String addr = house_detail2.getBlockinfo().getAddress();
                    if (addr != null) {
                        if (addr.indexOf("（") != -1) {
                            addr = addr.substring(0, addr.indexOf("（"));
                        }
                        if (addr.indexOf("(") != -1) {
                            addr = addr.substring(0, addr.indexOf("("));
                        }
                    } else {
                        addr = StringUtils.EMPTY;
                    }
                    try {
                        SecondRentDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLat())).toString(), new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLng())).toString(), addr));
                    } catch (Exception e) {
                        SecondRentDetailActivity.this.showToast((int) R.string.no_map_app);
                    }
                }
            });
            this.block_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondRentDetailActivity.this.showToast((int) R.string.no_block);
                        return;
                    }
                    Intent intent = new Intent(SecondRentDetailActivity.this, BlockDetailActivity.class);
                    intent.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 1);
                    intent.putExtra(BlockDetailActivity.INTENT_BLOCKID, house_detail2.getBlockinfo().getId());
                    SecondRentDetailActivity.this.startActivity(intent);
                }
            });
        }
        this.rent_count_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!house_detail2.getBlockinfo().getId().equals("0")) {
                    Intent intent = new Intent(SecondRentDetailActivity.this, BlockHouseListActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("app", App.RENT);
                    bundle.putString("blockid", house_detail2.getBlockinfo().getId());
                    bundle.putString("b_name", house_detail2.getBlockinfo().getBlockname());
                    intent.putExtras(bundle);
                    SecondRentDetailActivity.this.startActivity(intent);
                    return;
                }
                SecondRentDetailActivity.this.showToast((int) R.string.block_no_renthouse);
            }
        });
    }

    private class GetHouseInfoTask extends CommonAsyncTask<SecondHouse> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseInfoTask(Context context, int resid) {
            super(context, resid);
            this.loadingresid = resid;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(SecondHouse v) {
            if (v == null) {
                ActivityUtil.showToast(this.context, (int) R.string.house_info_load_error);
                ((NewHouseApplication) this.mApplication).saveBrowseHistory(new HouseInfo(App.RENT, SecondRentDetailActivity.this.house_detail), App.RENT);
                SecondRentDetailActivity.this.finish();
                return;
            }
            SecondRentDetailActivity.this.house_detail = v;
            SecondRentDetailActivity.this.initInfo(SecondRentDetailActivity.this.house_detail);
        }

        public SecondHouse onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseInfoByRowid(2, SecondRentDetailActivity.this.id, SecondRentDetailActivity.this.app);
        }

        /* access modifiers changed from: protected */
        public void onDialogCancel() {
            SecondRentDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            SecondRentDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            SecondRentDetailActivity.this.finish();
        }
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
