package com.house365.newhouse.ui.user;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;

public class ActivityPushReceiver extends BroadcastReceiver {
    private static final String TAG = "ActivityPushReceiver";

    public void onReceive(Context context, Intent intent) {
        showAppNotification(context, intent);
    }

    /* access modifiers changed from: package-private */
    public void showAppNotification(Context context, Intent intent) {
        intent.setClass(context, CouponDetailsActivity.class);
        intent.setFlags(268435456);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 268435456);
        Notification notif = new Notification(R.drawable.stat_sample, "notif", System.currentTimeMillis());
        notif.setLatestEventInfo(context, "365看房", "点击查看报名详情", pendingIntent);
        notif.defaults = -1;
        ((NotificationManager) context.getSystemService("notification")).notify(Integer.parseInt(intent.getStringExtra("id")), notif);
    }
}
