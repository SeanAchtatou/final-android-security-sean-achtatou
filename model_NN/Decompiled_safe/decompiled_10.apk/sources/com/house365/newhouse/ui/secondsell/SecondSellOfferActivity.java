package com.house365.newhouse.ui.secondsell;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.view.NoScrollGridView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.PublishHouseTask;
import com.house365.newhouse.task.UploadPicTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.secondhouse.adapter.OfferImageAdapter;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;
import com.house365.newhouse.ui.user.MyPublishActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class SecondSellOfferActivity extends BaseCommonActivity {
    public static String INTENT_CONFIG = "sell_config";
    public static String blockName;
    public static String district;
    public static String street;
    private View add_pic_layout;
    private Button btn_publish;
    private Button btn_relogin;
    /* access modifiers changed from: private */
    public List<String> c_thumb;
    protected HouseBaseInfo configInfo;
    /* access modifiers changed from: private */
    public Context context;
    private EditText curr_tel;
    private EditText et_buildarea;
    private EditText et_contactor;
    private EditText et_describe;
    private EditText et_sell_offer_price;
    private EditText et_title;
    private NoScrollGridView exist_photoes;
    protected boolean flat_changed;
    /* access modifiers changed from: private */
    public FloorPopView floorPop;
    private TextView floor_choose_msg;
    /* access modifiers changed from: private */
    public PublishHouse house;
    private OfferImageAdapter imageAdapter;
    /* access modifiers changed from: private */
    public List<String> infoTypes;
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SecondSellOfferActivity.blockName = null;
            SecondSellOfferActivity.district = null;
            SecondSellOfferActivity.street = null;
            SecondSellOfferActivity.this.publishVal.put(HouseBaseInfo.INFOTYPE, "1");
        }
    };
    private String mobile;
    private OfferPhotoesView offerView;
    /* access modifiers changed from: private */
    public PopupWindow popWheel;
    /* access modifiers changed from: private */
    public HashMap<String, String> publishVal = new HashMap<>();
    private RadioButton rb_multi_floor;
    private RadioButton rb_single_floor;
    private HashMap<String, String> result;
    /* access modifiers changed from: private */
    public boolean rg_changed;
    private RadioGroup rg_floor;
    private String streetId;
    private File tempfile;
    private TextView tv_block;
    private TextView tv_distreet;
    /* access modifiers changed from: private */
    public TextView tv_sell_flat;
    /* access modifiers changed from: private */
    public TextView tv_sell_type;
    /* access modifiers changed from: private */
    public boolean typeChoose;
    private View view_block;
    private View view_distreet;
    private View view_sell_flat;
    private View view_sell_type;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.tempfile = this.offerView.getTempfile();
        if (resultCode != 0) {
            if (requestCode == 1) {
                startActivityForResult(IntentUtil.startPhotoZoom(Uri.fromFile(this.tempfile), Uri.fromFile(this.tempfile), 7, 6, 210, 180), 3);
            }
            if (data != null) {
                if (requestCode == 2) {
                    startActivityForResult(IntentUtil.startPhotoZoom(data.getData(), Uri.fromFile(this.tempfile), 7, 6, 210, 180), 3);
                }
                if (requestCode == 3) {
                    if (this.tempfile.exists()) {
                        new UploadPicTask(this.context, R.string.uploading_pic, this.tempfile, new UploadPicTask.UploadPicListener() {
                            public void onSuccess(String url) {
                                SecondSellOfferActivity.this.c_thumb.add(url);
                                SecondSellOfferActivity.this.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_UPLOAD_PIC_FINISH));
                                ActivityUtil.showToast(SecondSellOfferActivity.this.context, (int) R.string.upload_pic_success);
                            }

                            public void onError() {
                                ActivityUtil.showToast(SecondSellOfferActivity.this.context, (int) R.string.upload_pic_failure);
                            }
                        }).execute(new Object[0]);
                    } else {
                        Toast.makeText(this.context, (int) R.string.no_sd_info, 0).show();
                    }
                }
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public static int getAndroidSDKVersion() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.second_sell_offer_layout);
        this.context = this;
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mChangeCity);
        unregisterReceiver(this.offerView.getmUploadFinish());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.typeChoose = false;
        this.house = (PublishHouse) getIntent().getSerializableExtra(MyPublishActivity.INTENT_HOUSE);
        if (this.house != null) {
            this.view_sell_type.setEnabled(false);
            this.view_block.setEnabled(false);
            this.view_sell_flat.setEnabled(false);
            this.view_distreet.setEnabled(false);
            this.et_title.setEnabled(false);
            this.et_describe.setEnabled(false);
            this.et_contactor.setEnabled(false);
            this.et_sell_offer_price.setEnabled(false);
            this.et_buildarea.setEnabled(false);
            this.curr_tel.setEnabled(false);
            this.tv_sell_type.setEnabled(false);
            this.tv_block.setEnabled(false);
            this.tv_sell_flat.setEnabled(false);
            this.floor_choose_msg.setEnabled(false);
            this.tv_distreet.setEnabled(false);
            this.rg_floor.setEnabled(false);
            this.rb_single_floor.setEnabled(false);
            this.rb_multi_floor.setEnabled(false);
            this.btn_publish.setVisibility(8);
            this.btn_relogin.setVisibility(8);
            this.tv_sell_type.setText(this.house.getInfotype());
            this.tv_block.setText(this.house.getBlockname());
            this.tv_sell_flat.setText(this.house.getRoom());
            if (this.house.getFloormsg().indexOf("层-") != -1) {
                this.rb_multi_floor.setChecked(true);
            } else {
                this.rb_single_floor.setChecked(true);
            }
            this.floor_choose_msg.setText(this.house.getFloormsg());
            this.et_title.setText(this.house.getTitle());
            this.et_describe.setText(this.house.getRemark());
            this.et_contactor.setText(this.house.getContactor());
            this.et_sell_offer_price.setText(this.house.getPrice());
            this.et_buildarea.setText(this.house.getBuildarea());
            this.curr_tel.setText(this.house.getTelno());
            if (!TextUtils.isEmpty(this.house.getDistrict())) {
                this.tv_distreet.setText(this.house.getDistrict());
                this.view_distreet.setVisibility(0);
            }
            infoTypeGone(this.house.getInfoTypeId());
            this.add_pic_layout.setVisibility(8);
            this.exist_photoes.setVisibility(0);
            this.imageAdapter = new OfferImageAdapter(this.context);
            this.imageAdapter.addAll(this.house.getPics());
            this.exist_photoes.setAdapter((ListAdapter) this.imageAdapter);
            this.exist_photoes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    Intent intent = new Intent(SecondSellOfferActivity.this.context, AlbumFullScreenActivity.class);
                    intent.putExtra(AlbumFullScreenActivity.INTENT_ALBUM_POS, position);
                    intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) SecondSellOfferActivity.this.house.getPics());
                    SecondSellOfferActivity.this.startActivity(intent);
                }
            });
            return;
        }
        this.rb_single_floor.setChecked(true);
        getConfig(0, true);
    }

    /* access modifiers changed from: private */
    public void setSaveData() {
        this.exist_photoes.setVisibility(8);
        this.add_pic_layout.setVisibility(0);
        this.rg_changed = false;
        this.flat_changed = false;
        this.popWheel.dismiss();
        pushData();
        if (!TextUtils.isEmpty(blockName)) {
            this.tv_block.setText(blockName);
            if (!TextUtils.isEmpty(district) || !TextUtils.isEmpty(street)) {
                this.view_distreet.setVisibility(0);
                this.tv_distreet.setText(String.valueOf(district) + "+" + street);
            }
        } else {
            this.tv_block.setText(StringUtils.EMPTY);
            this.view_distreet.setVisibility(8);
            this.tv_distreet.setText(StringUtils.EMPTY);
        }
        this.mobile = ((NewHouseApplication) this.mApplication).getMobile();
        if (!TextUtils.isEmpty(this.mobile)) {
            this.curr_tel.setText(this.mobile);
        } else {
            this.curr_tel.setText(StringUtils.EMPTY);
        }
    }

    /* access modifiers changed from: private */
    public void infoTypeGone(int info_type) {
        if (info_type == 3 || info_type == 4 || info_type == 5) {
            this.view_sell_flat.setVisibility(8);
        } else {
            this.view_sell_flat.setVisibility(0);
        }
    }

    private void pushData() {
        infoTypeGone(Integer.parseInt(this.publishVal.get(HouseBaseInfo.INFOTYPE).toString()));
        if (!TextUtils.isEmpty(this.publishVal.get(HouseBaseInfo.INFOTYPE))) {
            this.tv_sell_type.setText(this.infoTypes.get(Integer.parseInt(this.publishVal.get(HouseBaseInfo.INFOTYPE)) - 1));
        }
        if (!TextUtils.isEmpty(this.publishVal.get("buildarea"))) {
            this.et_buildarea.setText(this.publishVal.get("buildarea"));
        }
        if (!TextUtils.isEmpty(this.publishVal.get("price"))) {
            this.et_sell_offer_price.setText(this.publishVal.get("price"));
        }
        if (!TextUtils.isEmpty(this.publishVal.get("contactor"))) {
            this.et_contactor.setText(this.publishVal.get("contactor"));
        }
        if (!TextUtils.isEmpty(this.publishVal.get("remark"))) {
            this.et_describe.setText(this.publishVal.get("remark"));
        }
        if (!TextUtils.isEmpty(this.publishVal.get(LoanCalSelProActivity.INTENT_TITLE))) {
            this.et_title.setText(this.publishVal.get(LoanCalSelProActivity.INTENT_TITLE));
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        blockName = null;
        district = null;
        street = null;
        this.view_distreet = findViewById(R.id.view_distreet);
        this.tv_distreet = (TextView) findViewById(R.id.tv_distreet);
        this.tv_sell_type = (TextView) findViewById(R.id.tv_sell_type);
        this.tv_sell_type.setText((int) R.string.public_house);
        this.publishVal.put(HouseBaseInfo.INFOTYPE, "1");
        this.publishVal.put("lctype", "1");
        this.publishVal.put("floor", "1");
        this.publishVal.put("totalfloor", "1");
        this.tv_sell_flat = (TextView) findViewById(R.id.tv_sell_flat);
        this.floor_choose_msg = (TextView) findViewById(R.id.floor_choose_msg);
        this.tv_block = (TextView) findViewById(R.id.tv_block);
        this.curr_tel = (EditText) findViewById(R.id.curr_tel);
        this.et_sell_offer_price = (EditText) findViewById(R.id.et_sell_offer_price);
        this.et_buildarea = (EditText) findViewById(R.id.et_buildarea);
        this.et_title = (EditText) findViewById(R.id.et_title);
        this.et_describe = (EditText) findViewById(R.id.et_describe);
        this.et_contactor = (EditText) findViewById(R.id.et_contactor);
        this.exist_photoes = (NoScrollGridView) findViewById(R.id.exist_photoes);
        this.add_pic_layout = findViewById(R.id.add_pic_layout);
        this.popWheel = new PopupWindow(this.context);
        this.floorPop = new FloorPopView(this.context, this.popWheel, this.mApplication.getScreenWidth(), ((this.mApplication.getScreenWidth() * 3) / 5) + 50);
        this.rg_floor = (RadioGroup) findViewById(R.id.rg_floor);
        this.rb_single_floor = (RadioButton) findViewById(R.id.rb_single_floor);
        this.rb_multi_floor = (RadioButton) findViewById(R.id.rb_multi_floor);
        this.rb_single_floor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellOfferActivity.this.floorChoose();
                SecondSellOfferActivity.this.singleChoose();
                SecondSellOfferActivity.this.floorPop.setDismiss(false);
                SecondSellOfferActivity.this.popWheel.showAtLocation(LayoutInflater.from(SecondSellOfferActivity.this.context).inflate((int) R.layout.second_sell_offer_layout, (ViewGroup) null), 80, 0, 0);
            }
        });
        this.rb_multi_floor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellOfferActivity.this.floorChoose();
                SecondSellOfferActivity.this.multiChoose();
                SecondSellOfferActivity.this.floorPop.setDismiss(false);
                SecondSellOfferActivity.this.popWheel.showAtLocation(LayoutInflater.from(SecondSellOfferActivity.this.context).inflate((int) R.layout.second_sell_offer_layout, (ViewGroup) null), 80, 0, 0);
            }
        });
        this.rg_floor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radiogroup, int checkedId) {
                SecondSellOfferActivity.this.floorChoose();
                if (checkedId == R.id.rb_single_floor) {
                    SecondSellOfferActivity.this.singleChoose();
                } else if (checkedId == R.id.rb_multi_floor) {
                    SecondSellOfferActivity.this.multiChoose();
                }
                SecondSellOfferActivity.this.rg_changed = true;
                SecondSellOfferActivity.this.floorPop.setDismiss(false);
            }
        });
        this.view_block = findViewById(R.id.view_block);
        View.OnClickListener blockListener = new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellOfferActivity.this.prepareData();
                Intent intent = new Intent(SecondSellOfferActivity.this.context, BlockChooseActivity.class);
                intent.putExtra(BlockChooseActivity.INTENT_FROM_TYPE, App.SELL);
                SecondSellOfferActivity.this.startActivity(intent);
            }
        };
        this.tv_block.setOnClickListener(blockListener);
        this.view_block.setOnClickListener(blockListener);
        this.view_sell_type = findViewById(R.id.view_sell_type);
        this.view_sell_type.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SecondSellOfferActivity.this.infoTypes == null || SecondSellOfferActivity.this.infoTypes.size() <= 0) {
                    SecondSellOfferActivity.this.typeChoose = true;
                    SecondSellOfferActivity.this.getConfig(R.string.loading, false);
                    return;
                }
                SecondSellOfferActivity.this.showItemDialog((String[]) SecondSellOfferActivity.this.infoTypes.toArray(new String[0]), HouseBaseInfo.INFOTYPE, SecondSellOfferActivity.this.tv_sell_type);
            }
        });
        this.view_sell_flat = findViewById(R.id.view_sell_flat);
        this.view_sell_flat.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellOfferActivity.this.floorPop.setResTitle(new int[]{R.string.text_falt_room_title, R.string.text_falt_hall_title, R.string.text_falt_toilet_title});
                SecondSellOfferActivity.this.floorPop.setMsgView(SecondSellOfferActivity.this.tv_sell_flat);
                SecondSellOfferActivity.this.floorPop.setType(1);
                SecondSellOfferActivity.this.floorPop.makePopWheel();
                SecondSellOfferActivity.this.floorPop.setChoose_single(false);
                SecondSellOfferActivity.this.floorPop.getFloor_secondary_wheel().setVisibility(0);
                SecondSellOfferActivity.this.floorPop.getTv_floor_secondary().setVisibility(0);
                SecondSellOfferActivity.this.floorPop.getSecondary_layout().setVisibility(0);
                SecondSellOfferActivity.this.flat_changed = true;
                SecondSellOfferActivity.this.floorPop.setDismiss(false);
                SecondSellOfferActivity.this.popWheel.showAtLocation(LayoutInflater.from(SecondSellOfferActivity.this.context).inflate((int) R.layout.second_sell_offer_layout, (ViewGroup) null), 80, 0, 0);
            }
        });
        this.btn_publish = (Button) findViewById(R.id.btn_publish);
        this.btn_publish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SecondSellOfferActivity.this.showEmptyToast() == 0) {
                    SecondSellOfferActivity.this.prepareData();
                    PublishHouseTask publish = new PublishHouseTask(SecondSellOfferActivity.this.context, SecondSellOfferActivity.this.publishVal, 1, 2);
                    publish.setOnSuccessListener(new PublishHouseTask.PublishOnSuccessListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void OnSuccess(CommonResultInfo v, PublishHouse publishHouse) {
                            PublishHouse publishHouse2 = SecondSellOfferActivity.this.setPublishData(publishHouse.getId(), publishHouse.getCreatetime());
                            publishHouse2.setInfoTypeId(Integer.parseInt(((String) SecondSellOfferActivity.this.publishVal.get(HouseBaseInfo.INFOTYPE)).toString()));
                            ((NewHouseApplication) SecondSellOfferActivity.this.mApplication).addPublishRec(publishHouse2, 11, 4);
                            Intent intent = new Intent(SecondSellOfferActivity.this.context, MyPublishActivity.class);
                            intent.putExtra(MyPublishActivity.INTENT_PUBLISH_TYPE, 4);
                            intent.putExtra(MyPublishActivity.INTENT_PUBLISH_OFFER, true);
                            SecondSellOfferActivity.this.startActivity(intent);
                            SecondSellOfferActivity.this.finish();
                        }
                    });
                    publish.execute(new Object[0]);
                }
            }
        });
        this.btn_relogin = (Button) findViewById(R.id.btn_relogin);
        this.btn_relogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppMethods.showLoginDialog(SecondSellOfferActivity.this.context, R.string.text_relogin_msg, StringUtils.EMPTY);
            }
        });
        this.offerView = new OfferPhotoesView(this.context, (LinearLayout) findViewById(R.id.view_list_layout), (Button) findViewById(R.id.btn_add_pic));
        this.c_thumb = this.offerView.getC_thumb();
    }

    /* access modifiers changed from: private */
    public void floorChoose() {
        this.floorPop.setResTitle(new int[]{R.string.text_floor_primary_title, R.string.text_floor_secondary_title, R.string.text_floor_total_title});
        this.floorPop.setMsgView(this.floor_choose_msg);
        this.floorPop.setType(2);
        this.floorPop.makePopWheel();
    }

    /* access modifiers changed from: private */
    public void singleChoose() {
        this.floorPop.setChoose_single(true);
        this.floorPop.getFloor_secondary_wheel().setVisibility(8);
        this.floorPop.getTv_floor_secondary().setVisibility(8);
        this.floorPop.getSecondary_layout().setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void multiChoose() {
        this.floorPop.setChoose_single(false);
        this.floorPop.getFloor_secondary_wheel().setVisibility(0);
        this.floorPop.getTv_floor_secondary().setVisibility(0);
        this.floorPop.getSecondary_layout().setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.configInfo = (HouseBaseInfo) getIntent().getSerializableExtra(INTENT_CONFIG);
        if (this.configInfo == null) {
            getConfig(R.string.loading, false);
            return;
        }
        this.infoTypes = this.configInfo.getSell_config().get(HouseBaseInfo.INFOTYPE);
        this.infoTypes.remove(0);
    }

    /* access modifiers changed from: private */
    public int showEmptyToast() {
        int info_type = Integer.parseInt(this.publishVal.get(HouseBaseInfo.INFOTYPE).toString());
        if (info_type != 3 && info_type != 4 && info_type != 5 && TextUtils.isEmpty(this.tv_sell_flat.getText().toString())) {
            showToast((int) R.string.text_roomtype_hint);
            return 1;
        } else if (TextUtils.isEmpty(this.publishVal.get(HouseBaseInfo.INFOTYPE))) {
            showToast((int) R.string.text_sell_type_hint);
            return 1;
        } else if (TextUtils.isEmpty(this.tv_block.getText().toString())) {
            showToast((int) R.string.text_block_name_hint);
            return 1;
        } else if (TextUtils.isEmpty(this.et_sell_offer_price.getText().toString())) {
            showToast((int) R.string.text_sell_offer_price_hint);
            return 1;
        } else if (TextUtils.isEmpty(this.floor_choose_msg.getText().toString())) {
            showToast((int) R.string.text_sell_floor_hint);
            return 1;
        } else if (TextUtils.isEmpty(this.et_title.getText().toString())) {
            showToast((int) R.string.text_sell_resource_title_msg);
            return 1;
        } else if (!TextUtils.isEmpty(this.et_contactor.getText().toString())) {
            return 0;
        } else {
            showToast((int) R.string.text_contact_hint);
            return 1;
        }
    }

    /* access modifiers changed from: private */
    public void prepareData() {
        this.result = this.floorPop.getResultData();
        if (!TextUtils.isEmpty(district)) {
            getStreetId();
            this.publishVal.put("streetid", this.streetId);
            this.publishVal.put("blockname", blockName);
            this.publishVal.put(HouseBaseInfo.DISTRICT, district);
        }
        if (!TextUtils.isEmpty(this.result.get("subfloor"))) {
            this.publishVal.put("subfloor", this.result.get("subfloor"));
            this.publishVal.put("lctype", "2");
        } else {
            this.publishVal.put("lctype", "1");
        }
        if (!TextUtils.isEmpty(this.result.get("floor"))) {
            this.publishVal.put("floor", this.result.get("floor"));
        }
        if (!TextUtils.isEmpty(this.result.get("floor"))) {
            this.publishVal.put("totalfloor", this.result.get("totalfloor"));
        }
        int info_type = Integer.parseInt(this.publishVal.get(HouseBaseInfo.INFOTYPE).toString());
        if (info_type == 3 || info_type == 4 || info_type == 5) {
            this.publishVal.put(HouseBaseInfo.ROOM, StringUtils.EMPTY);
            this.publishVal.put("hall", StringUtils.EMPTY);
            this.publishVal.put("toilet", StringUtils.EMPTY);
        } else {
            this.publishVal.put(HouseBaseInfo.ROOM, this.result.get(HouseBaseInfo.ROOM));
            this.publishVal.put("hall", this.result.get("hall"));
            this.publishVal.put("toilet", this.result.get("toilet"));
        }
        this.publishVal.put("buildarea", this.et_buildarea.getText().toString());
        this.publishVal.put("price", this.et_sell_offer_price.getText().toString());
        this.publishVal.put("contactor", this.et_contactor.getText().toString());
        this.publishVal.put("telno", this.mobile);
        this.publishVal.put("remark", this.et_describe.getText().toString());
        this.publishVal.put(LoanCalSelProActivity.INTENT_TITLE, this.et_title.getText().toString());
        if (this.offerView.getC_thumb().size() > 0) {
            String pics = StringUtils.EMPTY;
            for (int i = 0; i < this.offerView.getC_thumb().size(); i++) {
                if (i != this.offerView.getC_thumb().size() - 1) {
                    pics = String.valueOf(pics) + this.offerView.getC_thumb().get(i) + ",";
                } else {
                    pics = String.valueOf(pics) + this.offerView.getC_thumb().get(i);
                }
            }
            this.publishVal.put("pics", pics);
        }
    }

    /* access modifiers changed from: private */
    public PublishHouse setPublishData(String id, int createTime) {
        PublishHouse house2 = new PublishHouse(this.publishVal.get(LoanCalSelProActivity.INTENT_TITLE), this.publishVal.get("remark"), this.publishVal.get("contactor"), this.publishVal.get("telno"), this.tv_sell_type.getText().toString(), this.tv_distreet.getText().toString(), this.publishVal.get("price"), this.tv_sell_flat.getText().toString(), this.floor_choose_msg.getText().toString(), this.publishVal.get("buildarea"), this.publishVal.get("blockname"), this.offerView.getC_thumb());
        house2.setId(id);
        house2.setCreatetime(createTime);
        return house2;
    }

    private void getStreetId() {
        for (Map.Entry entry : this.configInfo.getDistrictStreet().get(district).entrySet()) {
            if (entry.getKey().equals(street)) {
                this.streetId = entry.getValue().toString();
            }
        }
    }

    /* access modifiers changed from: private */
    public void showItemDialog(final String[] chooseItems, final String key, final TextView view) {
        ActivityUtil.showSingleChooseDialog(this.context, (int) R.string.text_sell_type, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
            public void onChoose(DialogInterface dialog, int which) {
                int info_type = which + 1;
                SecondSellOfferActivity.this.publishVal.put(key, new StringBuilder(String.valueOf(info_type)).toString());
                view.setText(chooseItems[which]);
                view.setTextColor(SecondSellOfferActivity.this.getResources().getColor(R.color.black));
                SecondSellOfferActivity.this.infoTypeGone(info_type);
            }
        });
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid, final boolean onresume) {
        GetConfigTask getConfig = new GetConfigTask(this, resid);
        getConfig.setType(1);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info != null) {
                    SecondSellOfferActivity.this.configInfo = info;
                    SecondSellOfferActivity.this.infoTypes = SecondSellOfferActivity.this.configInfo.getSell_config().get(HouseBaseInfo.INFOTYPE);
                    if (SecondSellOfferActivity.this.infoTypes != null && SecondSellOfferActivity.this.infoTypes.size() > 0) {
                        SecondSellOfferActivity.this.infoTypes.remove(0);
                    }
                    if (onresume) {
                        SecondSellOfferActivity.this.setSaveData();
                    }
                    if (SecondSellOfferActivity.this.typeChoose && SecondSellOfferActivity.this.infoTypes != null && SecondSellOfferActivity.this.infoTypes.size() > 0) {
                        SecondSellOfferActivity.this.showItemDialog((String[]) SecondSellOfferActivity.this.infoTypes.toArray(new String[0]), HouseBaseInfo.INFOTYPE, SecondSellOfferActivity.this.tv_sell_type);
                    }
                }
            }

            public void OnError(HouseBaseInfo info) {
            }
        });
        getConfig.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        if (this.popWheel.isShowing()) {
            this.popWheel.dismiss();
            if (this.rg_changed) {
                this.floor_choose_msg.setText(StringUtils.EMPTY);
            }
            if (!this.flat_changed) {
                return false;
            }
            this.tv_sell_flat.setText(StringUtils.EMPTY);
            return false;
        }
        AppMethods.exitPublish(this.house, this.publishVal, this.context);
        return false;
    }
}
