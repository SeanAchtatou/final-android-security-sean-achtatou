package com.house365.newhouse.ui.search;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.anim.AnimBean;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.view.text.KeywordsFlow;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HotTag;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.KeyItemArray;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.newhome.NewHouseSearchResultActivity;
import com.house365.newhouse.ui.secondrent.SecondRentResultActivity;
import com.house365.newhouse.ui.secondsell.SecondSellResultActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import org.apache.commons.lang.StringUtils;

public class KeyWordSearchActivity extends BaseCommonActivity implements SensorEventListener {
    private static int measureHeight;
    private SimpleAdapter adapter;
    DefineArrayAdapter<String> array_adapter;
    private Button btn_back;
    private Button btn_clean_history;
    private Button btn_search;
    /* access modifiers changed from: private */
    public RadioButton btn_search_history;
    private RadioGroup btn_search_history_hot_group;
    /* access modifiers changed from: private */
    public RadioButton btn_search_hot;
    private List<KeyItemArray> history_data;
    private ListView history_list;
    /* access modifiers changed from: private */
    public LinearLayout history_search_layout;
    /* access modifiers changed from: private */
    public RelativeLayout hot_search_layout;
    private InputMethodManager imm;
    /* access modifiers changed from: private */
    public boolean isAniming = false;
    private LinearLayout key_back;
    /* access modifiers changed from: private */
    public ListView key_list;
    /* access modifiers changed from: private */
    public EditText keywords;
    /* access modifiers changed from: private */
    public KeywordsFlow keywordsFlow;
    private SensorManager mSensorManager = null;
    /* access modifiers changed from: private */
    public Vibrator mVibrator = null;
    /* access modifiers changed from: private */
    public LinearLayout nodata_layout;
    /* access modifiers changed from: private */
    public Button search_close;
    /* access modifiers changed from: private */
    public LinearLayout search_hot_history_layout;
    /* access modifiers changed from: private */
    public String search_type;
    /* access modifiers changed from: private */
    public List<String> show_history_data = new ArrayList();
    /* access modifiers changed from: private */
    public int[] textBackColors = {R.drawable.bg_green_label, R.drawable.bg_blue_label, R.drawable.bg_orange_label, R.drawable.bg_red_label};
    /* access modifiers changed from: private */
    public int[] textColors = {R.color.cloud_green, R.color.cloud_blue, R.color.cloud_orange, R.color.cloud_red};

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.key_search_activity);
        this.search_type = getIntent().getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
        measureHeight = (int) getResources().getDimension(R.dimen.length40);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.key_back = (LinearLayout) findViewById(R.id.key_back);
        this.keywordsFlow = (KeywordsFlow) findViewById(R.id.keywordsFlow);
        this.keywordsFlow.setOnItemClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HotTag hot;
                if ((v instanceof TextView) && (hot = (HotTag) ((TextView) v).getTag()) != null) {
                    String aboutType = hot.getT_abouttype();
                    String aboutId = hot.getT_aboutid();
                    String keys = hot.getT_title();
                    if (keys != null && !TextUtils.isEmpty(keys)) {
                        ((NewHouseApplication) KeyWordSearchActivity.this.mApplication).addKeySearchHistroy(new KeyItemArray(keys), KeyWordSearchActivity.this.search_type);
                    }
                    Intent intent = new Intent();
                    Class<?> cls = null;
                    if (KeyWordSearchActivity.this.search_type.equals(ActionCode.NEW_SEARCH)) {
                        if (aboutId == null || TextUtils.isEmpty(aboutId) || aboutId.equals("0")) {
                            intent.putExtra("keywords", keys);
                            intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, KeyWordSearchActivity.this.search_type);
                            cls = NewHouseSearchResultActivity.class;
                        } else if (aboutType.equals("house")) {
                            cls = NewHouseDetailActivity.class;
                            intent.putExtra("h_id", aboutId);
                        } else if (aboutType.equals("dist")) {
                            cls = NewHouseSearchResultActivity.class;
                            intent.putExtra("districtvalue", aboutId);
                        }
                    } else if (aboutId == null || TextUtils.isEmpty(aboutId) || aboutId.equals("0")) {
                        intent.putExtra("keywords", keys);
                        intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, KeyWordSearchActivity.this.search_type);
                        if (KeyWordSearchActivity.this.search_type.equals(ActionCode.SELL_SEARCH)) {
                            cls = SecondSellResultActivity.class;
                        } else if (KeyWordSearchActivity.this.search_type.equals(ActionCode.RENT_SEARCH)) {
                            cls = SecondRentResultActivity.class;
                        }
                    } else if (aboutType.equals(HouseBaseInfo.ROOM)) {
                        intent.putExtra("roomValue", aboutId);
                        if (KeyWordSearchActivity.this.search_type.equals(ActionCode.RENT_SEARCH)) {
                            cls = SecondRentResultActivity.class;
                            intent.putExtra("priceView_from", "0");
                        }
                        if (KeyWordSearchActivity.this.search_type.equals(ActionCode.SELL_SEARCH)) {
                            cls = SecondSellResultActivity.class;
                            intent.putExtra("priceView_from", "0");
                        }
                    }
                    if (cls != null) {
                        intent.setClass(KeyWordSearchActivity.this, cls);
                        KeyWordSearchActivity.this.startActivity(intent);
                    }
                }
            }
        });
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mVibrator = (Vibrator) getSystemService("vibrator");
        this.keywords = (EditText) findViewById(R.id.keywords);
        this.btn_back = (Button) findViewById(R.id.btn_back);
        this.btn_search = (Button) findViewById(R.id.btn_search);
        this.search_close = (Button) findViewById(R.id.search_close);
        this.key_list = (ListView) findViewById(R.id.key_list);
        this.history_list = (ListView) findViewById(R.id.history_list);
        this.search_hot_history_layout = (LinearLayout) findViewById(R.id.search_hot_history_layout);
        this.history_search_layout = (LinearLayout) findViewById(R.id.history_search_layout);
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        ((TextView) this.nodata_layout.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_history);
        this.hot_search_layout = (RelativeLayout) findViewById(R.id.hot_search_layout);
        this.btn_search_history_hot_group = (RadioGroup) findViewById(R.id.btn_search_history_hot_group);
        this.btn_search_history = (RadioButton) findViewById(R.id.btn_search_history);
        this.btn_search_hot = (RadioButton) findViewById(R.id.btn_search_hot);
        this.btn_clean_history = (Button) findViewById(R.id.btn_clean_history);
        this.btn_back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyWordSearchActivity.this.finish();
            }
        });
        this.btn_search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyWordSearchActivity.this.searchListener();
            }
        });
        this.imm = (InputMethodManager) getSystemService("input_method");
        this.keywords.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                KeyWordSearchActivity.this.btn_search_history.setChecked(true);
                KeyWordSearchActivity.this.btn_search_hot.setChecked(false);
                return false;
            }
        });
        this.btn_search_history_hot_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.btn_search_history) {
                    KeyWordSearchActivity.this.hot_search_layout.setVisibility(8);
                    KeyWordSearchActivity.this.setHistoryData();
                    KeyWordSearchActivity.this.showKeywordInput();
                } else if (checkedId == R.id.btn_search_hot) {
                    KeyWordSearchActivity.this.hideKeywordInput();
                    KeyWordSearchActivity.this.nodata_layout.setVisibility(8);
                    KeyWordSearchActivity.this.history_search_layout.setVisibility(8);
                    ((ViewGroup) KeyWordSearchActivity.this.history_search_layout.getParent()).setVisibility(8);
                    KeyWordSearchActivity.this.hot_search_layout.setVisibility(0);
                    KeyWordSearchActivity.this.showKeywords();
                }
            }
        });
        this.btn_clean_history.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((NewHouseApplication) KeyWordSearchActivity.this.mApplication).cleanKeySearchHistroy(KeyWordSearchActivity.this.search_type);
                KeyWordSearchActivity.this.setHistoryData();
            }
        });
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int totalHeight = 0;
            int w = View.MeasureSpec.makeMeasureSpec(0, 1073741824);
            int h = View.MeasureSpec.makeMeasureSpec(measureHeight, 1073741824);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(w, h);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = listAdapter.getCount() + totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        }
    }

    /* access modifiers changed from: private */
    public void showKeywordInput() {
        this.keywords.requestFocus();
        this.imm.showSoftInput(this.keywords, 1);
        this.btn_search.setTextColor(getResources().getColor(R.color.group_blue));
    }

    /* access modifiers changed from: private */
    public void hideKeywordInput() {
        this.key_back.requestFocus();
        this.imm.hideSoftInputFromWindow(this.keywords.getWindowToken(), 2);
        this.btn_search.setTextColor(getResources().getColor(R.color.expandle_text));
    }

    /* access modifiers changed from: protected */
    public void initData() {
        setHotOrHistory();
        this.search_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                KeyWordSearchActivity.this.keywords.setText(StringUtils.EMPTY);
                KeyWordSearchActivity.this.setHotOrHistory();
            }
        });
        this.history_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String h_name = ((TextView) view.findViewById(R.id.h_name)).getText().toString();
                ((NewHouseApplication) KeyWordSearchActivity.this.mApplication).addKeySearchHistroy(new KeyItemArray(h_name), KeyWordSearchActivity.this.search_type);
                Intent intent = new Intent();
                Class<?> cls = null;
                intent.putExtra("keywords", h_name);
                intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, KeyWordSearchActivity.this.search_type);
                if (KeyWordSearchActivity.this.search_type.equals(ActionCode.NEW_SEARCH)) {
                    cls = NewHouseSearchResultActivity.class;
                } else if (KeyWordSearchActivity.this.search_type.equals(ActionCode.SELL_SEARCH)) {
                    cls = SecondSellResultActivity.class;
                    intent.putExtra("priceView_from", "0");
                } else if (KeyWordSearchActivity.this.search_type.equals(ActionCode.RENT_SEARCH)) {
                    cls = SecondRentResultActivity.class;
                    intent.putExtra("priceView_from", "0");
                }
                intent.setClass(KeyWordSearchActivity.this, cls);
                KeyWordSearchActivity.this.startActivity(intent);
            }
        });
        if (this.search_type.equals(ActionCode.SELL_SEARCH) || this.search_type.equals(ActionCode.RENT_SEARCH)) {
            this.keywords.setHint((int) R.string.text_second_search_hint);
        }
        this.keywords.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String keys = KeyWordSearchActivity.this.keywords.getText().toString().trim();
                if (keys == null || TextUtils.isEmpty(keys)) {
                    KeyWordSearchActivity.this.search_close.setVisibility(8);
                    KeyWordSearchActivity.this.key_list.setVisibility(8);
                    KeyWordSearchActivity.this.search_hot_history_layout.setVisibility(0);
                    KeyWordSearchActivity.this.setHistoryData();
                    KeyWordSearchActivity.this.array_adapter = new DefineArrayAdapter<>(KeyWordSearchActivity.this, (int) R.id.h_name, KeyWordSearchActivity.this.show_history_data);
                    KeyWordSearchActivity.this.array_adapter.notifyDataSetChanged();
                    KeyWordSearchActivity.this.key_list.setAdapter((ListAdapter) KeyWordSearchActivity.this.array_adapter);
                    return;
                }
                KeyWordSearchActivity.this.search_close.setVisibility(0);
                KeyWordSearchActivity.this.key_list.setVisibility(0);
                KeyWordSearchActivity.this.search_hot_history_layout.setVisibility(8);
                if (KeyWordSearchActivity.this.search_type.equals(ActionCode.NEW_SEARCH)) {
                    new NewHouseSearchTask(KeyWordSearchActivity.this).execute(new Object[0]);
                } else {
                    new SecondHouseSearchTask(KeyWordSearchActivity.this).execute(new Object[0]);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.keywords.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 0 && actionId != 6 && actionId != 2 && actionId != 5 && actionId != 3) {
                    return false;
                }
                KeyWordSearchActivity.this.searchListener();
                return false;
            }
        });
        this.key_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String h_id = ((TextView) view.findViewById(R.id.h_id)).getText().toString();
                String h_name = ((TextView) view.findViewById(R.id.h_name)).getText().toString();
                if (h_id != null && !TextUtils.isEmpty(h_id)) {
                    ((NewHouseApplication) KeyWordSearchActivity.this.mApplication).addKeySearchHistroy(new KeyItemArray(h_name), KeyWordSearchActivity.this.search_type);
                    Intent intent = new Intent();
                    Class<?> clazz = null;
                    if (KeyWordSearchActivity.this.search_type.equals(ActionCode.NEW_SEARCH)) {
                        intent.putExtra("h_id", h_id);
                        clazz = NewHouseDetailActivity.class;
                    } else if (KeyWordSearchActivity.this.search_type.equals(ActionCode.SELL_SEARCH)) {
                        intent.putExtra("blockid", h_id);
                        intent.putExtra("b_name", h_name);
                        clazz = SecondSellResultActivity.class;
                    } else if (KeyWordSearchActivity.this.search_type.equals(ActionCode.RENT_SEARCH)) {
                        intent.putExtra("blockid", h_id);
                        intent.putExtra("b_name", h_name);
                        clazz = SecondRentResultActivity.class;
                    }
                    intent.setClass(KeyWordSearchActivity.this, clazz);
                    KeyWordSearchActivity.this.startActivity(intent);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void searchListener() {
        String keys = this.keywords.getText().toString().trim();
        if (keys != null && !TextUtils.isEmpty(keys)) {
            ((NewHouseApplication) this.mApplication).addKeySearchHistroy(new KeyItemArray(keys), this.search_type);
        }
        Intent intent = new Intent();
        Class<?> cls = null;
        intent.putExtra("keywords", keys);
        if (this.search_type.equals(ActionCode.NEW_SEARCH)) {
            cls = NewHouseSearchResultActivity.class;
        } else if (this.search_type.equals(ActionCode.SELL_SEARCH)) {
            cls = SecondSellResultActivity.class;
            intent.putExtra("priceView_from", "0");
        } else if (this.search_type.equals(ActionCode.RENT_SEARCH)) {
            cls = SecondRentResultActivity.class;
            intent.putExtra("priceView_from", "0");
        }
        intent.setClass(this, cls);
        startActivity(intent);
    }

    public void setHotOrHistory() {
        try {
            this.history_data = ((NewHouseApplication) this.mApplication).getKeySearchHistory(this.search_type);
            List<String> list_data = new ArrayList<>();
            if (this.history_data == null || this.history_data.size() <= 0) {
                hideKeywordInput();
                this.history_search_layout.setVisibility(8);
                ((ViewGroup) this.history_search_layout.getParent()).setVisibility(8);
                this.nodata_layout.setVisibility(8);
                this.hot_search_layout.setVisibility(0);
                showKeywords();
                this.btn_search_hot.setChecked(true);
                this.btn_search_history.setChecked(false);
                return;
            }
            this.nodata_layout.setVisibility(8);
            this.history_search_layout.setVisibility(0);
            ((ViewGroup) this.history_search_layout.getParent()).setVisibility(0);
            this.hot_search_layout.setVisibility(8);
            this.btn_search_history.setChecked(true);
            this.btn_search_hot.setChecked(false);
            setHistoryAdapterData(list_data);
            showKeywordInput();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void setHistoryData() {
        try {
            this.history_data = ((NewHouseApplication) this.mApplication).getKeySearchHistory(this.search_type);
            List<String> list_data = new ArrayList<>();
            if (this.history_data == null || this.history_data.size() <= 0) {
                this.nodata_layout.setVisibility(0);
                this.history_search_layout.setVisibility(8);
                ((ViewGroup) this.history_search_layout.getParent()).setVisibility(8);
                return;
            }
            this.nodata_layout.setVisibility(8);
            this.history_search_layout.setVisibility(0);
            ((ViewGroup) this.history_search_layout.getParent()).setVisibility(0);
            setHistoryAdapterData(list_data);
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void setHistoryAdapterData(List<String> list_data) {
        for (int i = 0; i < this.history_data.size(); i++) {
            list_data.add(this.history_data.get(i).getArray());
        }
        Collections.reverse(list_data);
        this.array_adapter = new DefineArrayAdapter<>(this, (int) R.id.h_name, list_data);
        this.history_list.setAdapter((ListAdapter) this.array_adapter);
        setListViewHeightBasedOnChildren(this.history_list);
    }

    /* access modifiers changed from: private */
    public void showKeywords() {
        this.keywordsFlow.rubKeywords();
        if (this.search_type.equals(ActionCode.NEW_SEARCH)) {
            new GetHotTagTask(this, App.NEW).execute(new Object[0]);
        } else if (this.search_type.equals(ActionCode.SELL_SEARCH)) {
            new GetHotTagTask(this, App.SELL).execute(new Object[0]);
        } else if (this.search_type.equals(ActionCode.RENT_SEARCH)) {
            new GetHotTagTask(this, App.RENT).execute(new Object[0]);
        }
    }

    public void onSensorChanged(SensorEvent event) {
        try {
            int sensorType = event.sensor.getType();
            float[] values = event.values;
            if (sensorType != 1) {
                return;
            }
            if ((Math.abs(values[0]) > 13.0f || Math.abs(values[1]) > 13.0f || Math.abs(values[2]) > 13.0f) && !this.isAniming) {
                new DoSensorChangeTask(this, null).execute(new Void[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private class DoSensorChangeTask extends AsyncTask<Void, Void, Void> {
        private DoSensorChangeTask() {
        }

        /* synthetic */ DoSensorChangeTask(KeyWordSearchActivity keyWordSearchActivity, DoSensorChangeTask doSensorChangeTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            KeyWordSearchActivity.this.isAniming = true;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                Thread.sleep(500);
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            KeyWordSearchActivity.this.mVibrator.vibrate(50);
            KeyWordSearchActivity.this.showKeywords();
            KeyWordSearchActivity.this.isAniming = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.keywords.setText(StringUtils.EMPTY);
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 3);
        if (this.btn_search_history.isChecked()) {
            showKeywordInput();
        } else if (this.btn_search_hot.isChecked()) {
            this.history_search_layout.setVisibility(8);
            ((ViewGroup) this.history_search_layout.getParent()).setVisibility(8);
            this.nodata_layout.setVisibility(8);
            this.hot_search_layout.setVisibility(0);
            showKeywords();
            hideKeywordInput();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mSensorManager.unregisterListener(this);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mSensorManager.unregisterListener(this);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void feedKeywordsFlow(KeywordsFlow keywordsFlow2, List<HotTag> hotTag) {
        List ran_tmp = new ArrayList();
        clearRepeat(hotTag, ran_tmp);
        if (ran_tmp.size() > 0) {
            for (int i = 0; i < keywordsFlow2.getMaxShowItem(); i++) {
                keywordsFlow2.feedKeyword(hotTag.get(i).getT_title());
            }
        }
    }

    public List clearRepeat(List<HotTag> hotTag, List ran_tmp) {
        Random random = new Random();
        for (int i = 0; i < this.keywordsFlow.getMaxShowItem(); i++) {
            ran_tmp.add(Integer.valueOf(random.nextInt(hotTag.size())));
        }
        HashSet hashSet = new HashSet(ran_tmp);
        ran_tmp.clear();
        ran_tmp.addAll(hashSet);
        if (ran_tmp.size() < this.keywordsFlow.getMaxShowItem()) {
            clearRepeat(hotTag, ran_tmp);
        }
        return ran_tmp;
    }

    /* access modifiers changed from: private */
    public void setNoKeyData() {
        if (this.search_type.equals(ActionCode.SELL_SEARCH) || this.search_type.equals(ActionCode.RENT_SEARCH)) {
            this.array_adapter = new DefineArrayAdapter<>(this, (int) R.id.h_name, new String[]{getResources().getString(R.string.text_block_nodata)});
        } else {
            this.array_adapter = new DefineArrayAdapter<>(this, (int) R.id.h_name, new String[]{getResources().getString(R.string.text_house_nodata)});
        }
        this.array_adapter.notifyDataSetChanged();
        this.key_list.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public void setNoNetWork() {
        this.array_adapter = new DefineArrayAdapter<>(this, (int) R.id.h_name, new String[]{getResources().getString(R.string.text_keysearch_nodata)});
        this.array_adapter.notifyDataSetChanged();
        this.key_list.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public void setKeyData(List<HashMap<String, String>> search_data) {
        List<HashMap<String, String>> list = search_data;
        this.adapter = new SimpleAdapter(this, list, R.layout.keysearch_item, new String[]{"h_id", "h_name"}, new int[]{R.id.h_id, R.id.h_name});
        this.adapter.notifyDataSetChanged();
        this.key_list.setAdapter((ListAdapter) this.adapter);
    }

    class GetHotTagTask extends CommonAsyncTask<List<HotTag>> {
        private String type;

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<HotTag>) ((List) obj));
        }

        public GetHotTagTask(Context context, String type2) {
            super(context);
            this.type = type2;
        }

        public List<HotTag> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHotTagList(this.type);
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
        }

        public void onAfterDoInBackgroup(List<HotTag> tags) {
            if (tags != null && tags.size() > 0) {
                for (int i = 0; i < tags.size(); i++) {
                    String title = tags.get(i).getT_title();
                    if (title == null || TextUtils.isEmpty(title)) {
                        tags.remove(i);
                    }
                }
                KeyWordSearchActivity.this.keywordsFlow.setTagLists(tags);
                KeyWordSearchActivity.this.keywordsFlow.setMaxShowItem(tags.size());
                KeyWordSearchActivity.this.keywordsFlow.setTextColors(KeyWordSearchActivity.this.textColors);
                KeyWordSearchActivity.this.keywordsFlow.setTextBackColors(KeyWordSearchActivity.this.textBackColors);
                KeyWordSearchActivity.this.keywordsFlow.setDuration(800);
                KeyWordSearchActivity.this.feedKeywordsFlow(KeyWordSearchActivity.this.keywordsFlow, tags);
                KeyWordSearchActivity.this.keywordsFlow.go2Show(2);
            }
        }
    }

    private class NewHouseSearchTask extends CommonAsyncTask<List<House>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<House>) ((List) obj));
        }

        public NewHouseSearchTask(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<House> houseList) {
            if (houseList == null || houseList.size() <= 0) {
                KeyWordSearchActivity.this.setNoKeyData();
                return;
            }
            List<HashMap<String, String>> search_data = new ArrayList<>();
            for (int i = 0; i < houseList.size(); i++) {
                HashMap<String, String> item = new HashMap<>();
                item.put("h_id", houseList.get(i).getH_id());
                item.put("h_name", houseList.get(i).getH_name());
                search_data.add(item);
            }
            KeyWordSearchActivity.this.setKeyData(search_data);
        }

        public List<House> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewHouseListBySearch(KeyWordSearchActivity.this.keywords.getText().toString());
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            KeyWordSearchActivity.this.setNoNetWork();
        }
    }

    private class SecondHouseSearchTask extends CommonAsyncTask<List<Block>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Block>) ((List) obj));
        }

        public SecondHouseSearchTask(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<Block> bockList) {
            if (bockList == null || bockList.size() <= 0) {
                KeyWordSearchActivity.this.setNoKeyData();
                return;
            }
            List<HashMap<String, String>> search_data = new ArrayList<>();
            for (int i = 0; i < bockList.size(); i++) {
                HashMap<String, String> item = new HashMap<>();
                Block block_item = bockList.get(i);
                item.put("h_id", block_item.getId());
                item.put("h_name", block_item.getBlockname());
                search_data.add(item);
            }
            KeyWordSearchActivity.this.setKeyData(search_data);
        }

        public List<Block> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSecondHouseListBySearch(KeyWordSearchActivity.this.keywords.getText().toString());
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            KeyWordSearchActivity.this.setNoNetWork();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }

    public AnimBean getFinishAnim() {
        return null;
    }
}
