package com.house365.newhouse.ui.news;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.anim.AnimBean;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.News;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import java.util.ArrayList;
import java.util.Random;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.SystemUtils;

public class NewsDetailActivity extends BaseCommonActivity {
    public static final String INTENT_CHANNEL = "channel";
    public static final String INTENT_ID = "id";
    int channel;
    private HeadNavigateView head_view;
    String id;
    /* access modifiers changed from: private */
    public int imgWidth;
    /* access modifiers changed from: private */
    public News news;
    private TextView news_date;
    /* access modifiers changed from: private */
    public TextView news_source;
    private TextView news_title;
    private View news_title_layout;
    private TextView tv_title;
    private WebView webView;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.news_detail_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsDetailActivity.this.finish();
            }
        });
        this.tv_title = this.head_view.getTv_center();
        this.news_title = (TextView) findViewById(R.id.news_title);
        this.news_title_layout = findViewById(R.id.news_title_layout);
        this.news_title_layout.setBackgroundColor(getResources().getColor(new int[]{R.color.menu_1, R.color.menu_2, R.color.menu_3, R.color.menu_5}[new Random().nextInt(4)]));
        this.webView = (WebView) findViewById(R.id.webview);
        this.webView.setBackgroundColor(0);
        this.webView.getSettings().setDefaultTextEncodingName(CharEncoding.UTF_8);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                WebView.HitTestResult hitTestResult = view.getHitTestResult();
                if (hitTestResult == null || !(hitTestResult.getType() == 5 || hitTestResult.getType() == 6 || hitTestResult.getType() == 8)) {
                    view.loadUrl(url);
                }
                return true;
            }
        });
        this.news_date = (TextView) findViewById(R.id.news_date);
        this.news_source = (TextView) findViewById(R.id.news_source);
        this.webView.setOnTouchListener(new View.OnTouchListener() {
            long downTime;
            long upTime;

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public boolean onTouch(View v, MotionEvent event) {
                WebView.HitTestResult hitTestResult;
                int idx;
                switch (event.getAction()) {
                    case 0:
                        this.downTime = System.currentTimeMillis();
                        float x = (float) ((int) event.getRawX());
                        float y = (float) ((int) event.getRawY());
                        return false;
                    case 1:
                        this.upTime = System.currentTimeMillis();
                        if (this.upTime - this.downTime >= 500 || SystemUtils.JAVA_VERSION_FLOAT - event.getX() >= 5.0f || SystemUtils.JAVA_VERSION_FLOAT - event.getY() >= 5.0f || (hitTestResult = ((WebView) v).getHitTestResult()) == null) {
                            return false;
                        }
                        if ((hitTestResult.getType() != 5 && hitTestResult.getType() != 6 && hitTestResult.getType() != 8) || (idx = NewsDetailActivity.this.getIndex(hitTestResult.getExtra().toString())) == -1) {
                            return false;
                        }
                        Intent intent = new Intent(NewsDetailActivity.this, AlbumFullScreenActivity.class);
                        intent.putExtra(AlbumFullScreenActivity.INTENT_FROM_NEWS, true);
                        intent.putExtra(AlbumFullScreenActivity.INTENT_ALBUM_POS, idx);
                        intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) NewsDetailActivity.this.news.getImages());
                        NewsDetailActivity.this.startActivity(intent);
                        return false;
                    default:
                        return false;
                }
            }
        });
    }

    public int getIndex(String picUrl) {
        if (this.news.getImages() == null) {
            return -1;
        }
        for (int i = 0; i < this.news.getImages().size(); i++) {
            if (picUrl.contains(this.news.getImages().get(i))) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        int screenWidth = this.mApplication.getScreenWidth();
        this.imgWidth = (int) (((float) (screenWidth - (screenWidth / 5))) / this.mApplication.getDensity());
        this.id = getIntent().getStringExtra("id");
        this.channel = getIntent().getIntExtra("channel", 0);
        switch (this.channel) {
            case 1:
                this.tv_title.setText((int) R.string.text_news_type_1);
                break;
            case 2:
                this.tv_title.setText((int) R.string.text_news_type_2);
                break;
            case 3:
                this.tv_title.setText((int) R.string.text_news_type_3);
                break;
            case 4:
                this.tv_title.setText((int) R.string.text_news_type_4);
                break;
        }
        if (this.id != null) {
            this.news = ((NewHouseApplication) this.mApplication).getNews(this.id);
            if (this.news != null) {
                showContent(this.news);
            } else {
                new GetNewsTask(this, R.string.loading).execute(new Object[0]);
            }
        } else {
            new GetNewsTask(this, R.string.loading).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: private */
    public void showContent(News news2) {
        this.news_title.setText(news2.getN_title());
        this.news_date.setText(TimeUtil.toDateWithFormat(news2.getN_addtime(), "yyyy年MM月dd日\tHH:mm"));
        if (!TextUtils.isEmpty(news2.getN_from())) {
            this.news_source.setText("来源:" + news2.getN_from());
        } else {
            this.news_source.setText("来源:365");
        }
        if (!TextUtils.isEmpty(news2.getN_content())) {
            this.webView.loadDataWithBaseURL(null, news2.getN_content(), "text/html", CharEncoding.UTF_8, null);
        } else if (!TextUtils.isEmpty(news2.getN_source())) {
            WebSettings webSettings = this.webView.getSettings();
            webSettings.setBuiltInZoomControls(true);
            webSettings.setSupportZoom(true);
            webSettings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
            webSettings.setUseWideViewPort(true);
            webSettings.setLoadWithOverviewMode(true);
            this.webView.setInitialScale(100);
            this.webView.loadUrl(news2.getN_source());
        }
    }

    class GetNewsTask extends CommonAsyncTask<News> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetNewsTask(Context context, int resid) {
            super(context, resid);
            initLoadDialog(resid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(News v) {
            if (v == null) {
                NewsDetailActivity.this.showToast((int) R.string.net_error);
                NewsDetailActivity.this.finish();
                return;
            }
            NewsDetailActivity.this.news = v;
            ((NewHouseApplication) this.mApplication).saveNewsHistory(v);
            if (!TextUtils.isEmpty(v.getN_from())) {
                NewsDetailActivity.this.news_source.setText("来源:" + v.getN_from());
            } else {
                NewsDetailActivity.this.news_source.setText("来源:365");
            }
            NewsDetailActivity.this.showContent(v);
        }

        public News onDoInBackgroup() throws HtppApiException, NetworkUnavailableException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewsById(NewsDetailActivity.this.id, NewsDetailActivity.this.imgWidth);
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            NewsDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            NewsDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            NewsDetailActivity.this.finish();
        }
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_right, R.anim.slide_fix);
    }

    public AnimBean getFinishAnim() {
        return new AnimBean(R.anim.slide_fix, R.anim.slide_out_right);
    }
}
