package com.house365.newhouse.ui.user;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RegexUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.award.AwardActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.GroupHouseActivity;
import com.house365.newhouse.ui.search.SearchHomeActivity;

public class UserLoginActivity extends BaseCommonActivity {
    public static final int FROM_TYPE_APPLY_DIALOG = 1;
    public static final int FROM_TYPE_MORE_ACCENT_LOGIN = 2;
    public static final int FROM_TYPE_PERSON_EVENT = 4;
    public static final int FROM_TYPE_PERSON_PRIVILEGE = 3;
    public static final String INTENT_CID = "c_id";
    public static final String INTENT_COUPON_TYPE = "coupon_type";
    public static final String INTENT_FROM_PUBLISH = "from_publish";
    public static final String INTENT_HID = "h_id";
    public static final String INTENT_TO_LOGIN = "tologin";
    private Button btn_login;
    private Button btn_security_code;
    /* access modifiers changed from: private */
    public boolean coding;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String errorMsg = "ErrorMsg";
    /* access modifiers changed from: private */
    public EditText etCode;
    /* access modifiers changed from: private */
    public EditText etMobile;
    /* access modifiers changed from: private */
    public int fromType;
    /* access modifiers changed from: private */
    public String from_publish;
    private String frommobile;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    String success;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_login);
        this.context = this;
        this.mApplication = (NewHouseApplication) getApplication();
        this.fromType = getIntent().getIntExtra(INTENT_TO_LOGIN, 0);
        this.from_publish = getIntent().getStringExtra(INTENT_FROM_PUBLISH);
    }

    public void getCode(final String mobile) {
        new CommonAsyncTask<CommonResultInfo>(this.context) {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                UserLoginActivity.this.etCode.isFocused();
                UserLoginActivity.this.etMobile.setEnabled(false);
                UserLoginActivity.this.etCode.setEnabled(false);
                UserLoginActivity.this.coding = true;
            }

            public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
                if (resultInfo != null && resultInfo.getResult() == 1) {
                    UserLoginActivity.this.success = "success";
                    ActivityUtil.showToastView(this.context, String.valueOf(UserLoginActivity.this.getResources().getString(R.string.text_get_code_success)) + "\n" + mobile + "!");
                    UserLoginActivity.this.etCode.setText(" ");
                    UserLoginActivity.this.etMobile.isFocused();
                } else if (resultInfo == null) {
                    UserLoginActivity.this.showToast((int) R.string.text_failue_work);
                    UserLoginActivity.this.errorMsg = "failure";
                } else {
                    UserLoginActivity.this.errorMsg = resultInfo.getMsg();
                    UserLoginActivity.this.showToast(UserLoginActivity.this.errorMsg);
                }
                UserLoginActivity.this.etCode.setEnabled(true);
                UserLoginActivity.this.etMobile.setEnabled(true);
                UserLoginActivity.this.coding = false;
            }

            /* access modifiers changed from: protected */
            public void onHttpRequestError() {
                super.onHttpRequestError();
                UserLoginActivity.this.showToast((int) R.string.text_validate_timeout);
            }

            public CommonResultInfo onDoInBackgroup() {
                UserLoginActivity.this.coding = true;
                try {
                    return ((HttpApi) this.mApplication.getApi()).getValidateCode(mobile);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.btn_login = (Button) findViewById(R.id.btn_user_login);
        this.btn_security_code = (Button) findViewById(R.id.btn_security_code);
        this.etMobile = (EditText) findViewById(R.id.et_input_phone);
        this.etCode = (EditText) findViewById(R.id.et_security_code);
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_more_login_title);
        this.head_view.setTvTitleText((int) R.string.text_more_login_title);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserLoginActivity.this.finish();
            }
        });
        this.etMobile.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String mobile = UserLoginActivity.this.etMobile.getText().toString().trim();
                if (mobile.length() > 11) {
                    UserLoginActivity.this.etMobile.setText(mobile.substring(0, 11));
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.etCode.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String code = UserLoginActivity.this.etCode.getText().toString().trim();
                if (UserLoginActivity.this.etCode.length() > 6) {
                    UserLoginActivity.this.etCode.setText(code.substring(0, 6));
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.btn_security_code.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String mobile = UserLoginActivity.this.etMobile.getText().toString().trim();
                if (mobile == null || TextUtils.isEmpty(mobile)) {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_empty);
                } else if (!RegexUtil.isMobileNumber(mobile)) {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_mobile);
                } else {
                    UserLoginActivity.this.showToast(UserLoginActivity.this.getResources().getString(R.string.text_refuse_double_click));
                    if (UserLoginActivity.this.coding) {
                        UserLoginActivity.this.showToast(UserLoginActivity.this.getResources().getString(R.string.text_validate_coding));
                    } else {
                        UserLoginActivity.this.getCode(mobile);
                    }
                }
            }
        });
        this.btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String mobile = UserLoginActivity.this.etMobile.getText().toString();
                final String code = UserLoginActivity.this.etCode.getText().toString().trim();
                if (mobile == null || TextUtils.isEmpty(mobile)) {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_empty);
                } else if (!RegexUtil.isNumberWithLen(mobile, 11)) {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_mobile);
                    return;
                }
                if (!RegexUtil.isCharacterWithLen(code, 6)) {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_code);
                    return;
                }
                String currentMobile = UserLoginActivity.this.mApplication.getMobile();
                if (currentMobile == null || !currentMobile.equals(mobile)) {
                    new CommonAsyncTask<CommonResultInfo>(UserLoginActivity.this.context) {
                        public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
                            String id;
                            String type;
                            if (resultInfo != null && resultInfo.getResult() == 1) {
                                ((NewHouseApplication) this.mApplication).setMobile(mobile);
                                if (UserLoginActivity.this.fromType == 2) {
                                    ((Activity) this.context).setResult(-1);
                                    ((Activity) this.context).finish();
                                    if (!TextUtils.isEmpty(UserLoginActivity.this.from_publish)) {
                                        if (UserLoginActivity.this.from_publish.equals(ActionCode.SELL_SEARCH)) {
                                            Intent intent = new Intent(this.context, SearchHomeActivity.class);
                                            intent.putExtra(SearchHomeActivity.INTENT_PUBLISH, 12);
                                            UserLoginActivity.this.startActivity(intent);
                                        }
                                        if (UserLoginActivity.this.from_publish.equals(ActionCode.RENT_SEARCH)) {
                                            Intent intent2 = new Intent(this.context, SearchHomeActivity.class);
                                            intent2.putExtra(SearchHomeActivity.INTENT_PUBLISH, 13);
                                            UserLoginActivity.this.startActivity(intent2);
                                        }
                                        if (UserLoginActivity.this.from_publish.equals(ActionCode.AWARD)) {
                                            Event event = (Event) UserLoginActivity.this.getIntent().getExtras().get("event");
                                            if (event == null) {
                                                id = UserLoginActivity.this.getIntent().getStringExtra("id");
                                                type = App.Categroy.Event.LOTTERY;
                                            } else {
                                                id = event.getE_id();
                                                type = "event";
                                            }
                                            Intent intent3 = new Intent(this.context, AwardActivity.class);
                                            intent3.putExtra("id", id);
                                            intent3.putExtra(CouponDetailsActivity.INTENT_TYPE, type);
                                            intent3.putExtra("event", event);
                                            this.context.startActivity(intent3);
                                            ((Activity) this.context).finish();
                                        }
                                    }
                                } else if (UserLoginActivity.this.fromType == 1) {
                                    UserLoginActivity.this.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_SIGN));
                                    ((Activity) this.context).finish();
                                    Intent intent4 = new Intent(this.context, UserInfoActivity.class);
                                    intent4.putExtra("h_id", UserLoginActivity.this.getIntent().getStringExtra("h_id"));
                                    intent4.putExtra("c_id", UserLoginActivity.this.getIntent().getStringExtra("c_id"));
                                    intent4.putExtra("coupon_type", UserLoginActivity.this.getIntent().getStringExtra("coupon_type"));
                                    intent4.putExtra(UserInfoActivity.INTENT_FROM, 2);
                                    UserLoginActivity.this.startActivity(intent4);
                                } else if (UserLoginActivity.this.fromType == 1) {
                                    ((Activity) this.context).finish();
                                    Intent intent5 = new Intent(this.context, UserInfoActivity.class);
                                    intent5.putExtra("h_id", UserLoginActivity.this.getIntent().getStringExtra("h_id"));
                                    intent5.putExtra("c_id", UserLoginActivity.this.getIntent().getStringExtra("c_id"));
                                    intent5.putExtra("coupon_type", UserLoginActivity.this.getIntent().getStringExtra("coupon_type"));
                                    intent5.putExtra(UserInfoActivity.INTENT_FROM, 2);
                                    UserLoginActivity.this.startActivity(intent5);
                                } else if (UserLoginActivity.this.fromType == 3) {
                                    ((Activity) this.context).finish();
                                    UserLoginActivity.this.startActivity(new Intent(this.context, UserEventRecordActivity.class));
                                } else if (UserLoginActivity.this.fromType == 4) {
                                    ((Activity) this.context).finish();
                                    UserLoginActivity.this.startActivity(new Intent(this.context, GroupHouseActivity.class));
                                }
                                Intent intent6 = new Intent(ActionCode.INTENT_ACTION_REFRESH_PERSON_MOBILE);
                                intent6.putExtra("refresh_mobile", mobile);
                                UserLoginActivity.this.sendBroadcast(intent6);
                            } else if (resultInfo == null) {
                                UserLoginActivity.this.showToast((int) R.string.text_failue_work);
                            } else {
                                UserLoginActivity.this.showToast(resultInfo.getMsg());
                            }
                        }

                        public CommonResultInfo onDoInBackgroup() {
                            try {
                                return ((HttpApi) this.mApplication.getApi()).validateMobile(mobile, code);
                            } catch (Exception e) {
                                e.printStackTrace();
                                return null;
                            }
                        }
                    }.execute(new Object[0]);
                } else {
                    UserLoginActivity.this.showToast((int) R.string.text_validate_mobile_same_with_old);
                }
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.frommobile = this.mApplication.getMobile();
        if (this.frommobile == null || !RegexUtil.isNumberWithLen(this.frommobile, 11)) {
            this.etMobile.isFocused();
        } else {
            this.etMobile.setText(this.frommobile);
        }
        this.etCode.setHintTextColor(getResources().getColor(R.color.gray));
        this.btn_login.setText(getResources().getString(R.string.text_more_login_title));
    }
}
