package com.house365.newhouse.ui.secondsell;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.json.JSONObject;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TextUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayGestureDetector;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.ZoomEvent;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.GetHouseListTask;
import com.house365.newhouse.task.LocationTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.mapsearch.HouseBlockOverlay;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import com.house365.newhouse.ui.secondhouse.adapter.HouseListAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SecondSellResultActivity extends BaseBaiduMapActivity {
    public static String INTENT_FROM_NEARBY = "from_nearby";
    /* access modifiers changed from: private */
    public HouseListAdapter adapter_sell;
    private String app = App.SELL;
    protected HouseBaseInfo baseInfo;
    private View black_alpha_view;
    private String blockid;
    private RadioGroup bt_map_list_group;
    private int buildareaValue;
    private Bundle bundle;
    /* access modifiers changed from: private */
    public boolean checkedRegion;
    private BroadcastReceiver chosedFinished;
    /* access modifiers changed from: private */
    public boolean click_nearby_refresh;
    /* access modifiers changed from: private */
    public RadioGroup condition_group;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    /* access modifiers changed from: private */
    public boolean from_nearby;
    boolean hasInitedMap = false;
    private HeadNavigateView head_view;
    private HouseBlockOverlay houseBlock;
    private ManagedOverlay houseOverlay;
    /* access modifiers changed from: private */
    public int infofromValue;
    private int infotypeValue;
    private String keywords;
    /* access modifiers changed from: private */
    public Animation leftInAnim;
    /* access modifiers changed from: private */
    public Animation leftOutAnim;
    private PullToRefreshListView listview_sell;
    private View loadingLayout;
    /* access modifiers changed from: private */
    public TextView location_addr;
    private Drawable location_marker;
    private BroadcastReceiver mChangeCity;
    /* access modifiers changed from: private */
    public Location mLocation;
    private BroadcastReceiver mLocationFinish;
    /* access modifiers changed from: private */
    public boolean mapChoose;
    MapController mapController;
    /* access modifiers changed from: private */
    public View mapLoading;
    MapView mapView;
    private View map_list_change;
    private int maptype;
    private Drawable marker;
    /* access modifiers changed from: private */
    public String metroValue;
    private ManagedOverlay myPositionOverlay;
    /* access modifiers changed from: private */
    public View nearby_refresh_layout;
    private View nodata_layout;
    /* access modifiers changed from: private */
    public int orderBy;
    private final String[] orders = {ActionCode.PREFERENCE_SEARCH_DEFAULT, "发布日期由近到远", "发布日期由远到近", "总价由低到高", "总价由高到低", "面积由大到小", "面积由小到大"};
    /* access modifiers changed from: private */
    public final int[] ordervalues = {-1, 2, 1, 3, 4, 6, 5};
    OverlayManager overlayManager;
    ImageView pop_iv_block;
    TextView pop_txt_block_count;
    TextView pop_txt_block_name;
    private ArrayList<String> priceList;
    /* access modifiers changed from: private */
    public int priceValue;
    private View price_layout;
    /* access modifiers changed from: private */
    public final int[] radious;
    /* access modifiers changed from: private */
    public ArrayList<String> radiousList;
    /* access modifiers changed from: private */
    public int radiusvalue;
    private RefreshInfo refreshInfo_sell;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, String>> regionMap;
    /* access modifiers changed from: private */
    public View region_layout;
    private ArrayList<String> resourceList;
    private View resource_layout;
    /* access modifiers changed from: private */
    public Animation rightInAnim;
    /* access modifiers changed from: private */
    public Animation rightOutAnim;
    private int roomValue;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private String search_type = ActionCode.SELL_SEARCH;
    /* access modifiers changed from: private */
    public ArrayList<String> sortList;
    private View sort_layout;
    /* access modifiers changed from: private */
    public int streetValue;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, Station>> subwayMap;
    /* access modifiers changed from: private */
    public List<TextView> textViews;
    /* access modifiers changed from: private */
    public TextView text_price;
    /* access modifiers changed from: private */
    public TextView text_region;
    /* access modifiers changed from: private */
    public TextView text_resource;
    /* access modifiers changed from: private */
    public TextView text_sort;
    /* access modifiers changed from: private */
    public ViewSwitcher viewSwitcher;
    /* access modifiers changed from: private */
    public List<View> views;

    public SecondSellResultActivity() {
        int[] iArr = new int[5];
        iArr[0] = 1000;
        iArr[1] = 2500;
        iArr[2] = 5000;
        iArr[3] = 10000;
        this.radious = iArr;
        this.sortList = new ArrayList<>();
        this.radiousList = new ArrayList<>();
        this.checkedRegion = true;
        this.views = new ArrayList();
        this.textViews = new ArrayList();
        this.maptype = 12;
        this.mChangeCity = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                SecondSellResultActivity.this.getConfig(R.string.loading);
            }
        };
        this.mLocationFinish = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int changeCity = intent.getIntExtra(ActionCode.LOCATION_FINISH, 0);
                int changetag = intent.getIntExtra(ActionCode.LOCATION_TAG, 0);
                switch (changeCity) {
                    case 1:
                        SecondSellResultActivity.this.setMyLocation();
                        SecondSellResultActivity.this.mapLoading.setVisibility(8);
                        SecondSellResultActivity.this.refreshData();
                        return;
                    case 2:
                    default:
                        return;
                    case 3:
                        if (changetag == 12) {
                            SecondSellResultActivity.this.finish();
                            return;
                        }
                        return;
                }
            }
        };
        this.chosedFinished = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE).equals(ActionCode.SELL_SEARCH)) {
                    int type = SearchConditionPopView.getTagFlag(SecondSellResultActivity.this.text_region.getTag());
                    if (SecondSellResultActivity.this.from_nearby && type == 10) {
                        SecondSellResultActivity.this.radiusvalue = SecondSellResultActivity.this.radious[SearchConditionPopView.getIndex(SecondSellResultActivity.this.radiousList, SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag()))];
                    } else if (type == 1) {
                        String district_Street = SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag());
                        if (district_Street == null || district_Street.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                            SecondSellResultActivity.this.districtValue = 0;
                            SecondSellResultActivity.this.streetValue = 0;
                        } else if (district_Street.indexOf("+") != -1) {
                            String district = district_Street.substring(0, district_Street.indexOf("+"));
                            String street = district_Street.substring(district_Street.indexOf("+") + 1, district_Street.length());
                            SecondSellResultActivity.this.districtValue = SearchConditionPopView.getIndex(AppMethods.mapKeyTolistSubWay(SecondSellResultActivity.this.baseInfo.getDistrictStreet(), false), district) + 1;
                            SecondSellResultActivity.this.streetValue = Integer.parseInt((String) SecondSellResultActivity.this.baseInfo.getDistrictStreet().get(district).get(street));
                            SecondSellResultActivity.this.metroValue = null;
                        }
                    } else if (type == 2) {
                        String subWay = SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag());
                        if (subWay == null || subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                            SecondSellResultActivity.this.metroValue = null;
                        } else if (subWay.indexOf("+") != -1) {
                            String metro = subWay.substring(0, subWay.indexOf("+"));
                            SecondSellResultActivity.this.metroValue = ((Station) SecondSellResultActivity.this.baseInfo.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()))).getId();
                            SecondSellResultActivity.this.districtValue = 0;
                            SecondSellResultActivity.this.streetValue = 0;
                        }
                    }
                    SecondSellResultActivity.this.infofromValue = SearchConditionPopView.setIntValue(SecondSellResultActivity.this.text_resource, SecondSellResultActivity.this.infofromValue, HouseBaseInfo.INFOFROM, SecondSellResultActivity.this.baseInfo);
                    SecondSellResultActivity.this.priceValue = SearchConditionPopView.setIntValue(SecondSellResultActivity.this.text_price, SecondSellResultActivity.this.priceValue, "price", SecondSellResultActivity.this.baseInfo);
                    String sortData = SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_sort.getTag());
                    if (!TextUtils.isEmpty(sortData) && !sortData.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        SecondSellResultActivity.this.orderBy = SecondSellResultActivity.this.ordervalues[SearchConditionPopView.getIndex(SecondSellResultActivity.this.sortList, sortData)];
                    }
                    SecondSellResultActivity.this.refreshData();
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((!this.click_nearby_refresh && !this.from_nearby) || requestCode != 1) {
            return;
        }
        if (DeviceUtil.isOpenLoaction(this.context)) {
            relocation();
        } else {
            finish();
        }
    }

    public MapView getMapView() {
        return this.mapView;
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        setContentView((int) R.layout.second_search_result);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(false);
        this.searchPop.setShowOnView(true);
        this.searchPop.setSearch_type(ActionCode.SELL_SEARCH);
        this.condition_group = this.searchPop.getCondition_group();
        this.condition_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                SecondSellResultActivity.this.searchPop.setShowGroup(true);
                int tag = SearchConditionPopView.getTagFlag(SecondSellResultActivity.this.text_region.getTag());
                String chosed = null;
                if (checkedId == R.id.rb_region) {
                    if (tag != 2) {
                        chosed = SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag());
                    }
                    SecondSellResultActivity.this.searchPop.setGradeData(SecondSellResultActivity.this.text_region, chosed, SecondSellResultActivity.this.regionMap, 1);
                    SecondSellResultActivity.this.checkedRegion = true;
                } else if (checkedId == R.id.rb_subway) {
                    if (tag != 1) {
                        chosed = SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag());
                    }
                    SecondSellResultActivity.this.searchPop.setGradeData(SecondSellResultActivity.this.subwayMap, SecondSellResultActivity.this.text_region, chosed, 2);
                    SecondSellResultActivity.this.checkedRegion = false;
                }
            }
        });
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
        this.bundle = getIntent().getExtras();
        registerReceiver(this.mLocationFinish, new IntentFilter(ActionCode.INTENT_ACTION_LOCATION_FINISH));
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        this.keywords = this.bundle.getString("keywords");
        this.refreshInfo_sell = new RefreshInfo();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
        unregisterReceiver(this.mLocationFinish);
        unregisterReceiver(this.mChangeCity);
        this.adapter_sell.clear();
    }

    /* access modifiers changed from: private */
    public void updataFinish() {
        Intent intent = new Intent();
        Bundle refreshBundle = new Bundle();
        refreshBundle.putInt("infofromValue", this.infofromValue);
        refreshBundle.putInt("priceValue", this.priceValue);
        if (this.text_region.getTag() != null) {
            refreshBundle.putString("distsubway", this.text_region.getTag().toString());
        }
        intent.putExtras(refreshBundle);
        setResult(-1, intent);
        LocationTask.isdoing = false;
        finish();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.sortList = (ArrayList) AppMethods.toList(this.orders);
        this.radiousList = (ArrayList) AppMethods.toList(AppArrays.getRadious());
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellResultActivity.this.updataFinish();
            }
        });
        this.map_list_change = findViewById(R.id.map_list_change);
        this.listview_sell = (PullToRefreshListView) findViewById(R.id.list);
        this.bt_map_list_group = (RadioGroup) findViewById(R.id.bt_map_list_group);
        this.bt_map_list_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_list) {
                    if (SecondSellResultActivity.this.from_nearby) {
                        SecondSellResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    }
                    SecondSellResultActivity.this.viewSwitcher.setOutAnimation(SecondSellResultActivity.this.rightOutAnim);
                    SecondSellResultActivity.this.viewSwitcher.setInAnimation(SecondSellResultActivity.this.leftInAnim);
                    SecondSellResultActivity.this.viewSwitcher.setDisplayedChild(0);
                    SecondSellResultActivity.this.mapChoose = false;
                } else if (checkedId == R.id.bt_map) {
                    SecondSellResultActivity.this.nearby_refresh_layout.setVisibility(8);
                    SecondSellResultActivity.this.viewSwitcher.setOutAnimation(SecondSellResultActivity.this.leftOutAnim);
                    SecondSellResultActivity.this.viewSwitcher.setInAnimation(SecondSellResultActivity.this.rightInAnim);
                    SecondSellResultActivity.this.viewSwitcher.setDisplayedChild(1);
                    SecondSellResultActivity.this.mapLoading.setVisibility(8);
                    SecondSellResultActivity.this.mapChoose = true;
                    SecondSellResultActivity.this.initMap();
                }
            }
        });
        this.viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        this.region_layout = findViewById(R.id.newhouse_region_layout);
        this.price_layout = findViewById(R.id.newhouse_price_layout);
        this.resource_layout = findViewById(R.id.newhouse_type_layout);
        this.sort_layout = findViewById(R.id.newhouse_sort_layout);
        this.text_region = (TextView) findViewById(R.id.text_newhouse_region);
        this.text_price = (TextView) findViewById(R.id.text_newhouse_price);
        this.text_price.setText((int) R.string.text_total_price_title);
        this.text_resource = (TextView) findViewById(R.id.text_newhouse_type);
        this.text_resource.setText((int) R.string.text_resource_title);
        this.text_sort = (TextView) findViewById(R.id.text_newhouse_sort);
        this.views.add(this.region_layout);
        this.views.add(this.price_layout);
        this.views.add(this.resource_layout);
        this.views.add(this.sort_layout);
        this.textViews.add(this.text_region);
        this.textViews.add(this.text_price);
        this.textViews.add(this.text_resource);
        this.textViews.add(this.text_sort);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.nearby_refresh_layout = findViewById(R.id.nearby_refresh_layout);
        this.loadingLayout = findViewById(R.id.loadingLayout);
        this.mapLoading = findViewById(R.id.mapLoading);
        this.location_addr = (TextView) findViewById(R.id.location_addr);
        this.leftOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        this.rightInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        this.rightOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        this.leftInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        this.mapView = (MapView) findViewById(R.id.mapview);
        this.mapController = this.mapView.getController();
        this.marker = getResources().getDrawable(R.drawable.ico_marker);
        this.location_marker = getResources().getDrawable(R.drawable.ico_location_marker);
        this.overlayManager = new OverlayManager(getApplication(), this.mapView);
        this.houseOverlay = this.overlayManager.createOverlay("houses", this.marker);
        this.myPositionOverlay = this.overlayManager.createOverlay("myposition", this.location_marker);
        this.houseBlock = new HouseBlockOverlay(this.context, this.overlayManager, null, this.houseOverlay, this.mapController, this.mApplication, this.mapView, null);
        this.houseOverlay.setOnOverlayGestureListener(new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
            public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
                return false;
            }

            public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (point == null) {
                    return true;
                }
                SecondSellResultActivity.this.moveMapToPoint(point);
                return true;
            }

            public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
            }

            public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item != null) {
                    SecondSellResultActivity.this.clickTap(item);
                }
            }

            public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
                return false;
            }

            public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item == null) {
                    return false;
                }
                SecondSellResultActivity.this.clickTap(item);
                return false;
            }
        });
        this.adapter_sell = new HouseListAdapter(this, this.app);
    }

    /* access modifiers changed from: private */
    public void clickTap(ManagedOverlayItem item) {
        ManagedOverlayItem last = this.houseBlock.getLastItem();
        item.setTag(2);
        this.houseBlock.setHouseMarker(item, this.maptype);
        if (last != null && !last.getSnippet().equals(item.getSnippet())) {
            last.setTag(1);
            this.houseBlock.setHouseMarker(last, this.maptype);
        }
        this.houseBlock.setHouseOverlayTap(item, this.maptype, 0, 0, 0);
        this.houseBlock.setLastItem(item);
        moveMapToPoint(item.getPoint());
    }

    /* access modifiers changed from: private */
    public void moveMapToPoint(GeoPoint geopoint) {
        if (getMapView() != null && this.mapView.getController() != null) {
            this.mapView.getController().animateTo(geopoint);
            this.mapView.getController().setCenter(geopoint);
        }
    }

    /* access modifiers changed from: private */
    public void initMap() {
        double[] cityLoc = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(cityLoc[0], cityLoc[1]));
        this.houseOverlay.getOverlayItems().clear();
        if (this.mapController != null) {
            this.mapController.setZoom(12);
        }
        int page = this.listview_sell.getActureListView().getFirstVisiblePosition() / 20;
        int startIdx = page * 20;
        int endIdx = ((page + 1) * 20) - 1;
        List<Block> blocks = new ArrayList<>();
        if (endIdx > this.adapter_sell.getList().size() - 1) {
            endIdx = this.adapter_sell.getList().size() - 1;
        }
        if (endIdx < 0) {
            endIdx = 0;
        }
        for (int i = startIdx; i <= endIdx; i++) {
            if (this.adapter_sell.getList().size() <= 0) {
                showToast((int) R.string.text_no_result);
            } else if (!(this.adapter_sell.getList() == null || this.adapter_sell.getList().get(i) == null)) {
                SecondHouse house = (SecondHouse) this.adapter_sell.getList().get(i);
                if (house.getBlockinfo() != null && !has(blocks, house.getBlockinfo().getId())) {
                    blocks.add(house.getBlockinfo());
                }
            }
        }
        List<ManagedOverlayItem> ms = new ArrayList<>();
        for (Block block : blocks) {
            ms.add(new ManagedOverlayItem(BaiduMapUtil.getPoint(block.getLat(), block.getLng()), block.getId(), new JSONObject(block).toString()));
        }
        if (ms != null && ms.size() > 0) {
            this.houseOverlay.addAll(ms);
            this.houseBlock.setMarker(this.maptype, 2);
            this.overlayManager.populate();
        }
    }

    private boolean has(List<Block> blocks, String blockId) {
        for (int i = 0; i < blocks.size(); i++) {
            if (blocks.get(i) != null && blocks.get(i).getId() != null) {
                return blockId.equals(blocks.get(i).getId());
            }
        }
        return false;
    }

    public void refreshData() {
        this.refreshInfo_sell.refresh = true;
        getTaskData();
    }

    public void getMoreData() {
        this.refreshInfo_sell.refresh = false;
        getTaskData();
    }

    private void getTaskData() {
        new GetHouseListTask(this.context, this.listview_sell, this.refreshInfo_sell, this.adapter_sell, this.app, this.blockid, this.mLocation, this.radiusvalue, this.keywords, this.orderBy, this.infofromValue, this.infotypeValue, this.districtValue, this.streetValue, this.priceValue, this.buildareaValue, this.roomValue, 0, 0, this.metroValue, this.nodata_layout).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setMyLocation() {
        GeoPoint geopoint = BaiduMapUtil.getPoint(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude());
        this.myPositionOverlay.removeAll();
        this.myPositionOverlay.add(new ManagedOverlayItem(geopoint, "myposition", null));
        noLocationMap();
        this.overlayManager.populate();
    }

    private void noLocationMap() {
        double[] location = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(location[0], location[1]));
    }

    /* access modifiers changed from: private */
    public void relocation() {
        View loadView;
        if (this.mapChoose) {
            loadView = this.mapLoading;
        } else {
            loadView = this.loadingLayout;
        }
        if (!LocationTask.isdoing) {
            LocationTask location = new LocationTask(this.context, loadView);
            location.setTag(12);
            location.execute(new Object[0]);
            location.setLocationListener(new LocationTask.LocationListener() {
                public void onFinish(Location location, boolean sameCity) {
                    SecondSellResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    SecondSellResultActivity.this.mLocation = location;
                    if (SecondSellResultActivity.this.mLocation == null || SecondSellResultActivity.this.mLocation.getExtras() == null || SecondSellResultActivity.this.mLocation.getExtras().get("addr") == null) {
                        SecondSellResultActivity.this.location_addr.setText((int) R.string.text_changecity_location_failure);
                    } else {
                        TextUtil.setNullText(SecondSellResultActivity.this.mLocation.getExtras().get("addr").toString(), SecondSellResultActivity.this.location_addr);
                        SecondSellResultActivity.this.adapter_sell.setLocation(location);
                        SecondSellResultActivity.this.adapter_sell.setFrom_nearby(SecondSellResultActivity.this.from_nearby);
                    }
                    if (sameCity) {
                        SecondSellResultActivity.this.refreshData();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.infofromValue = this.bundle.getInt("infofromValue");
        this.infotypeValue = this.bundle.getInt("infotypeValue");
        this.districtValue = this.bundle.getInt("districtValue");
        this.streetValue = this.bundle.getInt("streetValue");
        this.priceValue = this.bundle.getInt("priceValue");
        this.buildareaValue = this.bundle.getInt("buildareaValue");
        this.roomValue = this.bundle.getInt("roomValue");
        this.metroValue = this.bundle.getString(HouseBaseInfo.METRO);
        this.blockid = this.bundle.getString("blockid");
        String b_name = this.bundle.getString("b_name");
        if (!TextUtils.isEmpty(b_name)) {
            this.head_view.setTvTitleText(b_name);
            this.map_list_change.setVisibility(8);
        }
        if (this.baseInfo == null) {
            getConfig(R.string.loading);
        }
        this.listview_sell.setAdapter(this.adapter_sell);
        this.adapter_sell.notifyDataSetChanged();
        this.from_nearby = getIntent().getBooleanExtra(INTENT_FROM_NEARBY, false);
        if (this.from_nearby) {
            relocation();
            this.radiusvalue = 2500;
            this.text_region.setText("2.5km");
            this.text_region.setTag("0:2.5km");
            this.nearby_refresh_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SecondSellResultActivity.this.click_nearby_refresh = true;
                    if (!DeviceUtil.isOpenLoaction(SecondSellResultActivity.this.context)) {
                        AppMethods.showLocationSets(SecondSellResultActivity.this.context);
                    } else {
                        SecondSellResultActivity.this.relocation();
                    }
                }
            });
        } else {
            this.text_region.setText((int) R.string.text_region_title);
            this.nearby_refresh_layout.setVisibility(8);
            if (AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity())) {
                this.sort_layout.setVisibility(8);
            } else {
                this.sort_layout.setVisibility(0);
            }
            refreshData();
        }
        this.listview_sell.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                SecondSellResultActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                SecondSellResultActivity.this.getMoreData();
            }
        });
        this.listview_sell.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(SecondSellResultActivity.this, SecondSellDetailActivity.class);
                intent.putExtra("id", ((SecondHouse) SecondSellResultActivity.this.adapter_sell.getItem(position)).getId());
                intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                SecondSellResultActivity.this.startActivity(intent);
            }
        });
    }

    private void addNormalSearchListener(View layout, TextView showDataView, ArrayList dataList, int choseType) {
        final View view = layout;
        final TextView textView = showDataView;
        final ArrayList arrayList = dataList;
        final int i = choseType;
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellResultActivity.this.searchPop.setShowGroup(false);
                SearchConditionPopView.refreshViews(SecondSellResultActivity.this.views, SecondSellResultActivity.this.textViews, view, textView, SecondSellResultActivity.this.context);
                SecondSellResultActivity.this.searchPop.setGradeData(arrayList, textView, SearchConditionPopView.getTagData(textView.getTag()), i);
            }
        });
    }

    /* access modifiers changed from: private */
    public void initSearchItem() {
        this.regionMap = this.baseInfo.getDistrictStreet();
        this.subwayMap = this.baseInfo.getMetro();
        this.resourceList = this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM);
        this.priceList = this.baseInfo.getSell_config().get("price");
        String text_subway = this.bundle.getString("text_subway");
        if (this.metroValue != null && !TextUtils.isEmpty(text_subway)) {
            this.text_region.setText(text_subway);
            this.text_region.setTag("2:" + text_subway);
        }
        String regionStr = this.bundle.getString("text_region");
        if (!TextUtils.isEmpty(regionStr) && !regionStr.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.text_region.setText(regionStr);
            this.text_region.setTag("1:" + regionStr);
        }
        if (this.infofromValue != 0) {
            String resourceStr = this.resourceList.get(this.infofromValue);
            this.text_resource.setText(resourceStr);
            this.text_resource.setTag("8:" + resourceStr);
        }
        if (this.priceValue != 0) {
            String priceStr = this.priceList.get(this.priceValue);
            this.text_price.setText(priceStr);
            this.text_price.setTag("6:" + priceStr);
        }
        if (this.from_nearby) {
            addNormalSearchListener(this.region_layout, this.text_region, this.radiousList, 10);
        } else {
            this.region_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SearchConditionPopView.refreshViews(SecondSellResultActivity.this.views, SecondSellResultActivity.this.textViews, SecondSellResultActivity.this.region_layout, SecondSellResultActivity.this.text_region, SecondSellResultActivity.this.context);
                    SecondSellResultActivity.this.searchPop.setShowDataView(SecondSellResultActivity.this.text_region);
                    SecondSellResultActivity.this.searchPop.setRegionOrSubway(SecondSellResultActivity.this.text_region);
                    if (SecondSellResultActivity.this.subwayMap == null || SecondSellResultActivity.this.subwayMap.isEmpty()) {
                        SecondSellResultActivity.this.searchPop.setShowGroup(false);
                        SecondSellResultActivity.this.searchPop.setGradeData(SecondSellResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag()), SecondSellResultActivity.this.regionMap, 1);
                        return;
                    }
                    SecondSellResultActivity.this.searchPop.setShowGroup(true);
                    if (SecondSellResultActivity.this.checkedRegion) {
                        SecondSellResultActivity.this.searchPop.setGradeData(SecondSellResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag()), SecondSellResultActivity.this.regionMap, 1);
                    } else {
                        SecondSellResultActivity.this.searchPop.setGradeData(SecondSellResultActivity.this.subwayMap, SecondSellResultActivity.this.text_region, SearchConditionPopView.getTagData(SecondSellResultActivity.this.text_region.getTag()), 2);
                    }
                    SecondSellResultActivity.this.condition_group.setVisibility(0);
                }
            });
        }
        addNormalSearchListener(this.price_layout, this.text_price, this.priceList, 6);
        addNormalSearchListener(this.resource_layout, this.text_resource, this.resourceList, 8);
        this.text_sort.setTag("0:" + this.orders[0]);
        addNormalSearchListener(this.sort_layout, this.text_sort, this.sortList, 0);
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid, 1);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info != null) {
                    SecondSellResultActivity.this.baseInfo = info;
                    SecondSellResultActivity.this.initSearchItem();
                }
            }

            public void OnError(HouseBaseInfo info) {
                Toast.makeText(SecondSellResultActivity.this.context, (int) R.string.text_city_config_error, 1).show();
            }
        });
        getConfig.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        updataFinish();
        return false;
    }
}
