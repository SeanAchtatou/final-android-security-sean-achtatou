package com.house365.newhouse.ui.secondsell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.ScrollViewHorzFling;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.block.BlockDetailActivity;
import com.house365.newhouse.ui.block.BlockHouseListActivity;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

public class SecondSellDetailActivity extends BaseBaiduMapActivity {
    public static final String INTENT_ID = "id";
    /* access modifiers changed from: private */
    public String app = App.SELL;
    View b_other_layout;
    View block_address_layout;
    View block_layout;
    TextView block_name;
    private LinearLayout bottomBar;
    TextView btn_ask;
    private TextView btn_refer;
    private String from_block_list = StringUtils.EMPTY;
    ImageView h_pic;
    /* access modifiers changed from: private */
    public HeadNavigateView head_view;
    TextView house_Renovation;
    TextView house_area;
    TextView house_b_other;
    TextView house_company;
    View house_company_layout;
    /* access modifiers changed from: private */
    public SecondHouse house_detail;
    TextView house_floor;
    TextView house_forward;
    TextView house_instruct;
    TextView house_person_name;
    private RelativeLayout house_pic_layout;
    View house_remark_layout;
    TextView house_sell_count;
    TextView house_street;
    TextView house_tel;
    TextView house_title;
    TextView house_type;
    TextView house_unit_price;
    TextView house_update_time;
    TextView house_year;
    /* access modifiers changed from: private */
    public String id;
    boolean isShowPic;
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    private MapView mMapView;
    TextView more_h_pic;
    TextView mright;
    ImageView person_photo;
    TextView priceterm;
    View sell_count_layout;
    TextView sell_detail_sale_state;
    TextView text_block_infotype;
    TextView text_districk;
    View title_block_address_layout;
    TextView tv_block_address;
    TextView txt_expand_sell_bo_ther;
    TextView txt_expand_sell_instruct;
    TextView txt_toatle_price;
    TextView update_time;

    public MapView getMapView() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.secondsell_detail_layout);
        this.mApplication = (NewHouseApplication) getApplication();
        this.from_block_list = getIntent().getStringExtra("FROM_BLOCK_LIST");
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellDetailActivity.this.finish();
            }
        });
        this.bottomBar = (LinearLayout) findViewById(R.id.bottom_detail_tool);
        this.bottomBar.setVisibility(8);
        this.sell_detail_sale_state = (TextView) findViewById(R.id.sell_detail_sale_state);
        this.house_update_time = (TextView) findViewById(R.id.house_update_time);
        ((ScrollViewHorzFling) findViewById(R.id.scrollView)).smoothScrollTo(0, 0);
        this.more_h_pic = (TextView) findViewById(R.id.more_h_pic);
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.btn_refer.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.phone, 0, 0, 0);
        this.house_pic_layout = (RelativeLayout) findViewById(R.id.house_pic_layout);
        this.sell_count_layout = findViewById(R.id.sell_count_layout);
        this.h_pic = (ImageView) findViewById(R.id.h_pic);
        this.h_pic.setVisibility(0);
        this.isShowPic = this.mApplication.isEnableImg();
        if (!this.isShowPic) {
            this.house_pic_layout.setEnabled(false);
        }
        this.mMapView = (MapView) findViewById(R.id.mapView);
        this.btn_ask = (TextView) findViewById(R.id.btn_apply);
        this.btn_ask.setText(getString(R.string.ask_housed));
        this.btn_ask.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.ask_house, 0, 0, 0);
        this.btn_ask.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseAnalyse.onViewClick(SecondSellDetailActivity.this, SecondSellDetailActivity.this.getString(R.string.ask_housed), App.SELL, SecondSellDetailActivity.this.house_detail.getId());
                Intent intent = new Intent(SecondSellDetailActivity.this, AskActivity.class);
                intent.putExtra("house", SecondSellDetailActivity.this.house_detail);
                intent.putExtra(CouponDetailsActivity.INTENT_TYPE, 1);
                SecondSellDetailActivity.this.startActivity(intent);
            }
        });
        this.house_company_layout = findViewById(R.id.house_company_layout);
        this.person_photo = (ImageView) findViewById(R.id.person_photo);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.txt_toatle_price = (TextView) findViewById(R.id.txt_toatle_price);
        this.house_area = (TextView) findViewById(R.id.house_area);
        this.house_unit_price = (TextView) findViewById(R.id.house_unit_price);
        this.house_type = (TextView) findViewById(R.id.detail_house_type);
        this.house_sell_count = (TextView) findViewById(R.id.house_sell_count);
        this.house_floor = (TextView) findViewById(R.id.house_floor);
        this.house_year = (TextView) findViewById(R.id.house_year);
        this.house_Renovation = (TextView) findViewById(R.id.house_Renovation);
        this.house_forward = (TextView) findViewById(R.id.house_forward);
        this.mright = (TextView) findViewById(R.id.mright);
        this.tv_block_address = (TextView) findViewById(R.id.tv_block_address);
        this.house_instruct = (TextView) findViewById(R.id.house_instruct);
        this.house_b_other = (TextView) findViewById(R.id.house_b_other);
        this.house_area = (TextView) findViewById(R.id.house_area);
        this.house_unit_price = (TextView) findViewById(R.id.house_unit_price);
        this.house_type = (TextView) findViewById(R.id.detail_house_type);
        this.text_block_infotype = (TextView) findViewById(R.id.house_channel);
        this.block_name = (TextView) findViewById(R.id.block_name);
        this.text_districk = (TextView) findViewById(R.id.text_districk);
        this.house_street = (TextView) findViewById(R.id.house_street);
        this.house_person_name = (TextView) findViewById(R.id.house_name);
        this.house_tel = (TextView) findViewById(R.id.house_tel);
        this.house_company = (TextView) findViewById(R.id.house_company);
        this.priceterm = (TextView) findViewById(R.id.priceterm);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.block_address_layout = findViewById(R.id.block_address_layout);
        this.title_block_address_layout = findViewById(R.id.title_block_address_layout);
        this.house_remark_layout = findViewById(R.id.house_remark_layout);
        this.b_other_layout = findViewById(R.id.b_other_layout);
        this.block_layout = findViewById(R.id.block_layout);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.id = getIntent().getStringExtra("id");
        new GetHouseInfoTask(this, R.string.house_info_load).execute(new Object[0]);
    }

    public void initInfo(final SecondHouse house_detail2) {
        if (house_detail2 != null) {
            this.head_view.setTvTitleText(house_detail2.getTitle());
            this.house_update_time.setText(house_detail2.getUpdatetime());
            String pic_first = house_detail2.getPics();
            if (pic_first == null || !this.isShowPic) {
                setImage(this.h_pic, StringUtils.EMPTY, R.drawable.bg_default_ad, 1);
            } else {
                setImage(this.h_pic, house_detail2.getPic(), R.drawable.img_default_small, 1);
                setImage(this.h_pic, pic_first, R.drawable.bg_default_ad, 1);
            }
            final NewHouseApplication application = this.mApplication;
            if (application.hasFav(new HouseInfo(App.SELL, house_detail2), App.SELL)) {
                this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_unfollow);
            }
            this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HouseAnalyse.onViewClick(SecondSellDetailActivity.this, "收藏", App.SELL, house_detail2.getId());
                    if (application.hasFav(new HouseInfo(App.SELL, house_detail2), App.SELL)) {
                        application.delFavHistory(new HouseInfo(App.SELL, house_detail2), App.SELL);
                        SecondSellDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_follow);
                        SecondSellDetailActivity.this.showToast((int) R.string.fav_cancle);
                        return;
                    }
                    application.saveFavHistory(new HouseInfo(App.SELL, house_detail2), App.SELL);
                    SecondSellDetailActivity.this.head_view.getBtn_right().setBackgroundResource(R.drawable.tab_unfollow);
                    SecondSellDetailActivity.this.showToast((int) R.string.fav_success);
                }
            });
            this.mApplication.saveBrowseHistory(new HouseInfo(App.SELL, house_detail2), App.SELL);
            this.priceterm.setText(house_detail2.getPriceterm());
            this.house_title.setText(house_detail2.getTitle());
            this.txt_toatle_price.setText(getResources().getString(R.string.wan, house_detail2.getPrice()));
            this.house_area.setText(getResources().getString(R.string.house_detail_area, house_detail2.getBuildarea()));
            this.house_unit_price.setText(getResources().getString(R.string.house_single_price, house_detail2.getAverprice()));
            this.house_type.setText(house_detail2.getApartment());
            this.text_districk.setText(house_detail2.getDistrict());
            this.house_street.setText(house_detail2.getStreetname());
            this.text_block_infotype.setText(house_detail2.getInfotype());
            if (house_detail2.getRemark() != null) {
                this.house_instruct.setText(Html.fromHtml(house_detail2.getRemark()));
            }
            if ((house_detail2.getH_picList().size() > 1) && this.isShowPic) {
                this.more_h_pic.setVisibility(0);
                this.house_pic_layout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(SecondSellDetailActivity.this, AlbumFullScreenActivity.class);
                        intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) house_detail2.getH_picList());
                        SecondSellDetailActivity.this.startActivity(intent);
                    }
                });
            }
            if (house_detail2.getState() != null) {
                if (house_detail2.getState().equals("1")) {
                    this.sell_detail_sale_state.setText((int) R.string.text_Urgent_Sale);
                    this.sell_detail_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    this.sell_detail_sale_state.setTextAppearance(this, R.style.font12_pop_green);
                } else if (house_detail2.getState().equals("2")) {
                    this.sell_detail_sale_state.setText((int) R.string.text_real_house);
                    this.sell_detail_sale_state.setBackgroundResource(R.drawable.bg_real_house);
                    this.sell_detail_sale_state.setTextAppearance(this, R.style.font12_pop_blue);
                } else if (house_detail2.getState().equals("3")) {
                    this.sell_detail_sale_state.setText((int) R.string.text_house_Sale);
                    this.sell_detail_sale_state.setBackgroundResource(R.drawable.bg_newhouse_sall_in);
                    this.sell_detail_sale_state.setTextColor((int) R.color.cloud_orange);
                    this.sell_detail_sale_state.setTextAppearance(this, R.style.font12_pop_orange);
                } else {
                    this.sell_detail_sale_state.setVisibility(8);
                }
            }
            if (house_detail2.getBlockinfo() != null) {
                this.block_name.setText(house_detail2.getBlockinfo().getBlockname());
                if (house_detail2.getBlockinfo().getSellcount() != null) {
                    this.house_sell_count.setText(getResources().getString(R.string.block_house_count_info, house_detail2.getBlockinfo().getSellcount()));
                }
                this.tv_block_address.setText(house_detail2.getBlockinfo().getAddress());
                if (house_detail2.getBlockinfo().getB_other() != null) {
                    this.house_b_other.setText(Html.fromHtml(house_detail2.getBlockinfo().getB_other()));
                }
            }
            this.house_floor.setText(house_detail2.getStory());
            this.house_year.setText(house_detail2.getBuildyear());
            this.house_Renovation.setText(house_detail2.getFitment());
            this.house_forward.setText(house_detail2.getForward());
            this.mright.setText(house_detail2.getMright());
            if (house_detail2.getBrokerinfo() != null) {
                if (house_detail2.getBrokerinfo().getAgentshortname() == null) {
                    this.house_company_layout.setVisibility(8);
                } else {
                    this.house_company_layout.setVisibility(0);
                    this.house_company.setText(house_detail2.getBrokerinfo().getAgentshortname());
                }
                this.house_person_name.setText(house_detail2.getBrokerinfo().getTruename());
                this.house_tel.setText(house_detail2.getBrokerinfo().getTelno());
                setImage(this.person_photo, house_detail2.getBrokerinfo().getSmallphoto(), R.drawable.img_default_small, 1);
                Log.e("SecondSellDetail", "========initInfo== type====" + house_detail2.getBrokerinfo().getType());
                if ("person".equals(house_detail2.getBrokerinfo().getType())) {
                    this.btn_refer.setText(getString(R.string.contact_house_owner));
                } else {
                    this.btn_refer.setText(getString(R.string.contact_middle_man));
                }
                if (house_detail2.getBrokerinfo().getTelno() == null) {
                    this.btn_refer.setEnabled(false);
                } else {
                    this.btn_refer.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (house_detail2.getBrokerinfo().getTelno() != null) {
                                try {
                                    TelUtil.getCallIntent(house_detail2.getBrokerinfo().getTelno(), SecondSellDetailActivity.this, App.SELL);
                                    SecondSellDetailActivity.this.mApplication.saveCallHistory(new HouseInfo(App.SELL, house_detail2), App.SELL);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }
            MapController mapController = this.mMapView.getController();
            if (house_detail2.getBlockinfo() != null) {
                if (house_detail2.getBlockinfo().getLat() > 0.0d && house_detail2.getBlockinfo().getLng() > 0.0d) {
                    GeoPoint point = BaiduMapUtil.getPoint(house_detail2.getBlockinfo().getLat(), house_detail2.getBlockinfo().getLng());
                    mapController.animateTo(point);
                    mapController.setZoom(15);
                    OverlayManager overlayManager = new OverlayManager(getApplication(), this.mMapView);
                    overlayManager.createOverlay("houses", getResources().getDrawable(R.drawable.ico_marker)).add(new ManagedOverlayItem(point, house_detail2.getBlockinfo().getId(), StringUtils.EMPTY));
                    overlayManager.populate();
                } else if (house_detail2.getBlockinfo().getLat() == 0.0d && house_detail2.getBlockinfo().getLng() == 0.0d) {
                    this.mMapView.setVisibility(8);
                    this.block_address_layout.setVisibility(8);
                    this.title_block_address_layout.setVisibility(8);
                    this.tv_block_address.setVisibility(8);
                }
            }
            this.block_address_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondSellDetailActivity.this.showToast((int) R.string.no_map);
                        return;
                    }
                    String addr = house_detail2.getBlockinfo().getAddress();
                    if (addr != null) {
                        if (addr.indexOf("（") != -1) {
                            addr = addr.substring(0, addr.indexOf("（"));
                        }
                        if (addr.indexOf("(") != -1) {
                            addr = addr.substring(0, addr.indexOf("("));
                        }
                    } else {
                        addr = StringUtils.EMPTY;
                    }
                    try {
                        SecondSellDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLat())).toString(), new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLng())).toString(), addr));
                    } catch (Exception e) {
                        SecondSellDetailActivity.this.showToast((int) R.string.no_map_app);
                    }
                }
            });
            this.house_remark_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getRemark() != null) {
                        Intent intent = new Intent(SecondSellDetailActivity.this, SecondSell_houseRemarkActivity.class);
                        String txt_house_remark = Html.fromHtml(house_detail2.getRemark()).toString();
                        intent.putExtra("title_name", house_detail2.getTitle());
                        intent.putExtra("txt_house_remark", txt_house_remark);
                        SecondSellDetailActivity.this.startActivity(intent);
                        return;
                    }
                    SecondSellDetailActivity.this.showToast((int) R.string.no_sell_intr);
                }
            });
            this.b_other_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getB_other() != null) {
                        Intent intent = new Intent(SecondSellDetailActivity.this, SecondSell_B_otherActivity.class);
                        String txt_house_B_other = Html.fromHtml(house_detail2.getBlockinfo().getB_other()).toString();
                        intent.putExtra("title_name", house_detail2.getTitle());
                        intent.putExtra("txt_house_B_other", txt_house_B_other);
                        SecondSellDetailActivity.this.startActivity(intent);
                        return;
                    }
                    SecondSellDetailActivity.this.showToast((int) R.string.no_sell_remark);
                }
            });
            this.title_block_address_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondSellDetailActivity.this.showToast((int) R.string.no_map);
                        return;
                    }
                    String addr = house_detail2.getBlockinfo().getAddress();
                    if (addr != null) {
                        if (addr.indexOf("（") != -1) {
                            addr = addr.substring(0, addr.indexOf("（"));
                        }
                        if (addr.indexOf("(") != -1) {
                            addr = addr.substring(0, addr.indexOf("("));
                        }
                    } else {
                        addr = StringUtils.EMPTY;
                    }
                    try {
                        SecondSellDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLat())).toString(), new StringBuilder(String.valueOf(house_detail2.getBlockinfo().getLng())).toString(), addr));
                    } catch (Exception e) {
                        SecondSellDetailActivity.this.showToast((int) R.string.no_map_app);
                    }
                }
            });
            this.block_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (house_detail2.getBlockinfo().getId().equals("0") || house_detail2.getBlockinfo().getId() == null) {
                        SecondSellDetailActivity.this.showToast((int) R.string.no_block);
                        return;
                    }
                    Intent intent = new Intent(SecondSellDetailActivity.this, BlockDetailActivity.class);
                    intent.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 1);
                    intent.putExtra(BlockDetailActivity.INTENT_BLOCKID, house_detail2.getBlockinfo().getId());
                    SecondSellDetailActivity.this.startActivity(intent);
                }
            });
            this.sell_count_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!house_detail2.getBlockinfo().getId().equals("0")) {
                        Intent intent = new Intent(SecondSellDetailActivity.this, BlockHouseListActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("app", App.SELL);
                        bundle.putString("blockid", house_detail2.getBlockinfo().getId());
                        bundle.putString("b_name", house_detail2.getBlockinfo().getBlockname());
                        intent.putExtras(bundle);
                        SecondSellDetailActivity.this.startActivity(intent);
                        return;
                    }
                    SecondSellDetailActivity.this.showToast((int) R.string.block_no_sellhouse);
                }
            });
        }
        this.bottomBar.setVisibility(0);
    }

    private class GetHouseInfoTask extends CommonAsyncTask<SecondHouse> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseInfoTask(Context context, int resid) {
            super(context, resid);
            this.loadingresid = resid;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(SecondHouse v) {
            if (v == null) {
                ActivityUtil.showToast(this.context, (int) R.string.house_info_load_error);
                ((NewHouseApplication) this.mApplication).saveBrowseHistory(new HouseInfo(App.SELL, SecondSellDetailActivity.this.house_detail), App.SELL);
                SecondSellDetailActivity.this.finish();
                return;
            }
            SecondSellDetailActivity.this.house_detail = v;
            SecondSellDetailActivity.this.initInfo(SecondSellDetailActivity.this.house_detail);
        }

        public SecondHouse onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseInfoByRowid(1, SecondSellDetailActivity.this.id, SecondSellDetailActivity.this.app);
        }

        /* access modifiers changed from: protected */
        public void onDialogCancel() {
            SecondSellDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            SecondSellDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            SecondSellDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            SecondSellDetailActivity.this.finish();
        }
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
