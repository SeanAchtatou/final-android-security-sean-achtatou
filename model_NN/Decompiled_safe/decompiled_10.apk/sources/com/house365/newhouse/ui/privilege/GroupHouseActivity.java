package com.house365.newhouse.ui.privilege;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.ui.privilege.adapter.MyHouseGroupAdapter;
import java.util.ArrayList;
import java.util.List;

public class GroupHouseActivity extends BaseListActivity implements PullToRefreshBase.OnRefreshListener, AdapterView.OnItemClickListener {
    public static final String TAG = "GroupHouseActivity";
    /* access modifiers changed from: private */
    public MyHouseGroupAdapter adapter;
    /* access modifiers changed from: private */
    public List<Event> dataList = new ArrayList();
    private HeadNavigateView headNavigateView;
    /* access modifiers changed from: private */
    public View nodata;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefresh;
    /* access modifiers changed from: private */
    public RefreshInfo refreshInfo = new RefreshInfo();
    private RadioGroup tab_event_group;
    private TextView tv_nodata;
    /* access modifiers changed from: private */
    public String type = "event";

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.event_list);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.headNavigateView = (HeadNavigateView) findViewById(R.id.head_view);
        this.headNavigateView.setTvTitleText((int) R.string.my_group_house);
        this.headNavigateView.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GroupHouseActivity.this.finish();
            }
        });
        this.pullToRefresh = (PullToRefreshListView) findViewById(R.id.list);
        this.nodata = findViewById(R.id.nodata_layout);
        this.tab_event_group = (RadioGroup) findViewById(R.id.tab_event_group);
        if (!((NewHouseApplication) this.mApplication).getCity().equals("nanjing")) {
            this.tab_event_group.setVisibility(8);
        } else {
            this.tab_event_group.setVisibility(0);
        }
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.tab_event_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkId) {
                if (checkId == R.id.bt_newhouse) {
                    GroupHouseActivity.this.type = "event";
                    GroupHouseActivity.this.adapter.setMarkType(0);
                } else if (checkId == R.id.bt_sell) {
                    GroupHouseActivity.this.type = App.Categroy.Event.SELL_EVENT;
                    GroupHouseActivity.this.adapter.setMarkType(1);
                }
                GroupHouseActivity.this.refreshData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.adapter = new MyHouseGroupAdapter(this);
        this.pullToRefresh.setAdapter(this.adapter);
        ((ListView) this.pullToRefresh.getRefreshableView()).setDivider(new BitmapDrawable());
        ((ListView) this.pullToRefresh.getRefreshableView()).setDividerHeight(54);
        this.pullToRefresh.setOnItemClickListener(this);
        this.pullToRefresh.setOnRefreshListener(this);
        refreshData();
    }

    public void onHeaderRefresh() {
        refreshData();
    }

    public void onFooterRefresh() {
        getMoreData();
    }

    public void refreshData() {
        this.refreshInfo.refresh = true;
        new GroupHouseTask(this, this.type).execute(new Object[0]);
    }

    public void getMoreData() {
        this.refreshInfo.refresh = false;
        new GroupHouseTask(this, this.type).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setNodata() {
        ((ImageView) this.nodata.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
        this.tv_nodata.setText((int) R.string.text_no_join_sell_event_msg);
    }

    class GroupHouseTask extends BaseListAsyncTask<Event> {
        private String ctype;

        public GroupHouseTask(Context context, String ctype2) {
            super(context, GroupHouseActivity.this.pullToRefresh, GroupHouseActivity.this.refreshInfo, GroupHouseActivity.this.adapter);
            this.ctype = ctype2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            GroupHouseActivity.this.adapter.clear();
            GroupHouseActivity.this.adapter.notifyDataSetChanged();
        }

        public List onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            GroupHouseActivity.this.dataList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getMySignList(GroupHouseActivity.this.type, ((NewHouseApplication) this.mApplication).getMobile(), this.listRefresh.page);
            return GroupHouseActivity.this.dataList;
        }

        public void onAfterRefresh(List<Event> list) {
            if (GroupHouseActivity.this.type.equals(this.ctype)) {
                if (list == null) {
                    GroupHouseActivity.this.nodata.setVisibility(0);
                    GroupHouseActivity.this.setNodata();
                    ViewUtil.onListDataComplete(this.context, list, this.listRefresh, GroupHouseActivity.this.adapter, GroupHouseActivity.this.pullToRefresh, (int) R.string.msg_load_error);
                } else if (list.size() > 0) {
                    GroupHouseActivity.this.nodata.setVisibility(8);
                    ViewUtil.onListDataComplete(this.context, list, this.listRefresh, GroupHouseActivity.this.adapter, GroupHouseActivity.this.pullToRefresh, (int) R.string.text_no_result);
                } else {
                    GroupHouseActivity.this.nodata.setVisibility(0);
                    GroupHouseActivity.this.setNodata();
                }
            } else if (this.listRefresh.refresh) {
                GroupHouseActivity.this.pullToRefresh.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
            } else {
                GroupHouseActivity.this.pullToRefresh.onRefreshComplete();
            }
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            if (GroupHouseActivity.this.nodata != null) {
                GroupHouseActivity.this.nodata.setVisibility(0);
                GroupHouseActivity.this.adapter.clear();
                GroupHouseActivity.this.adapter.notifyDataSetChanged();
                ((ImageView) GroupHouseActivity.this.nodata.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
                ((TextView) GroupHouseActivity.this.nodata.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
            }
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(this, EventListActivity.class);
        intent.putExtra("e_id", this.dataList.get(position).getE_id());
        intent.putExtra(EventListActivity.INTENT_TYPE, this.dataList.get(position).getE_type());
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }
}
