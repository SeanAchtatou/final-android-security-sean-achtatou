package com.house365.newhouse.ui.tools;

/* compiled from: LoanCalActivity */
class FoundRate extends CalBean {
    private double rate360;
    private double rate60;

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getRate60() {
        return this.rate60;
    }

    public void setRate60(double rate602) {
        this.rate60 = rate602;
    }

    public double getRate360() {
        return this.rate360;
    }

    public void setRate360(double rate3602) {
        this.rate360 = rate3602;
    }

    public FoundRate(String key, double rate602, double rate3602) {
        this.key = key;
        this.rate60 = rate602;
        this.rate360 = rate3602;
    }

    public double getRateByYear(LoanYears loanYears) {
        if (loanYears.getTimes() <= 60) {
            return getRate60();
        }
        return getRate360();
    }
}
