package com.house365.newhouse.ui.setting;

import android.os.Bundle;
import android.view.View;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;

public class AboutActivity extends BaseCommonActivity {
    private HeadNavigateView head_view;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.about);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AboutActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }
}
