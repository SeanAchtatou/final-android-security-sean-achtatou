package com.house365.newhouse.ui.award.widget.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.AwardInfo;
import com.house365.newhouse.model.AwardLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AwardAdapter extends BaseAdapter {
    private Context context;
    private Map<String, String> keys;
    private List<AwardLog> list;
    private List<AwardInfo> listInfo;

    public AwardAdapter(Context context2, Map<String, String> keys2) {
        this.context = context2;
        this.keys = keys2 == null ? new HashMap<>() : keys2;
    }

    public int getCount() {
        if (this.list.size() < 3) {
            return this.list.size();
        }
        return Integer.MAX_VALUE;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.award_item, (ViewGroup) null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.award_item_1);
        TextView result = (TextView) convertView.findViewById(R.id.award_item_2);
        if (this.list.size() != 0) {
            result.setText(String.valueOf(this.context.getString(R.string.award_got_hint_beofre)) + this.keys.get(this.list.get(position % this.list.size()).getS_content()) + this.context.getString(R.string.award_got_hint_after) + this.list.get(position % this.list.size()).getS_content());
        }
        return convertView;
    }

    public List<AwardLog> getList() {
        return this.list;
    }

    public void setList(List<AwardLog> list2) {
        this.list = list2;
    }

    public List<AwardInfo> getListInfo() {
        return this.listInfo;
    }

    public void setListInfo(List<AwardInfo> listInfo2) {
        this.listInfo = listInfo2;
    }
}
