package com.house365.newhouse.ui;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.action.ActionTag;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.inter.DialogOnPositiveListener;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.task.UpgradeTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.DialogUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.util.PackageUtil;
import com.house365.core.view.viewpager.PageIndicator;
import com.house365.core.view.viewpager.ex.ViewPagerCustomDuration;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.constant.ForumInfo;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.qrcode.CaptureActivity;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.UpdateUserInfoTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.adapter.HomeAdapter;
import com.house365.newhouse.ui.common.adapter.AdAdapter;
import com.house365.newhouse.ui.common.task.GetAdTask;
import com.house365.newhouse.ui.newhome.NewHomeActivity;
import com.house365.newhouse.ui.news.NewsHomeActivity;
import com.house365.newhouse.ui.privilege.EventListActivity;
import com.house365.newhouse.ui.privilege.PrivilegeHomeActivity;
import com.house365.newhouse.ui.search.NewSearchActivity;
import com.house365.newhouse.ui.search.SearchHomeActivity;
import com.house365.newhouse.ui.secondrent.SecondRentHomeActivity;
import com.house365.newhouse.ui.secondsell.SecondSellHomeActivity;
import com.house365.newhouse.ui.tools.LoanCalActivity;
import com.house365.newhouse.ui.tools.ShakeActivity;

public class MenuActivity extends BaseCommonActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private AdAdapter adAdapter;
    private HomeAdapter adapter;
    private String cityPy;
    private GridView gridView;
    private ImageView group;
    private Home home;
    private TextView homeCity;
    private Button homeLeft;
    private Button homeRight;
    private boolean isVirtualCity;
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            new GetConfigTask(context, 0).execute(new Object[0]);
            new GetConfigTask(context, 0, 1).execute(new Object[0]);
            new GetConfigTask(context, 0, 2).execute(new Object[0]);
            MenuActivity.this.refreshData();
        }
    };
    /* access modifiers changed from: private */
    public PageIndicator mIndicator;
    /* access modifiers changed from: private */
    public ViewPager mPager;
    private PersonSlidingMenuView menu;
    /* access modifiers changed from: private */
    public NewHouseApplication newApp;
    private ImageView no_ad_img;
    private ImageView privilege;
    private LinearLayout searchLayout;
    private View viewpager_layout;

    interface Home {
        void onItemClick(int i);
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.home_reality);
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        this.newApp = (NewHouseApplication) this.mApplication;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.menu = new PersonSlidingMenuView(this, this.newApp);
        this.gridView = (GridView) findViewById(R.id.home_grid);
        this.homeCity = (TextView) findViewById(R.id.home_city);
        this.searchLayout = (LinearLayout) findViewById(R.id.home_search);
        this.privilege = (ImageView) findViewById(R.id.home_privilege);
        this.group = (ImageView) findViewById(R.id.home_group);
        this.homeLeft = (Button) findViewById(R.id.home_left);
        this.homeRight = (Button) findViewById(R.id.home_right);
        this.viewpager_layout = findViewById(R.id.viewpager_layout);
        this.mIndicator = (PageIndicator) findViewById(R.id.indicator);
        this.no_ad_img = (ImageView) findViewById(R.id.no_ad_img);
        this.mPager = (ViewPager) findViewById(R.id.pager);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        new LocationTask(this).execute(new Object[0]);
        new UpgradeTask(this, true).execute(new Object[0]);
        new GetConfigTask(this, 0).execute(new Object[0]);
        new GetConfigTask(this, 0, 1).execute(new Object[0]);
        new GetConfigTask(this, 0, 2).execute(new Object[0]);
        this.adapter = new HomeAdapter(this);
        this.gridView.setOnItemClickListener(this);
        this.homeCity.setOnClickListener(this);
        this.searchLayout.setOnClickListener(this);
        this.privilege.setOnClickListener(this);
        this.group.setOnClickListener(this);
        this.homeRight.setOnClickListener(this);
        this.homeLeft.setOnClickListener(this);
        this.viewpager_layout.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.no_ad_img.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.group.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 413.0d) * 105.0d);
        this.privilege.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 413.0d) * 105.0d);
        this.adAdapter = new AdAdapter(this, 5);
        this.mPager.setAdapter(this.adAdapter);
        ((ViewPagerCustomDuration) this.mPager).setConsumeTouchEvent(true);
        ((ViewPagerCustomDuration) this.mPager).setScrollDuration(1000);
        this.mIndicator.setViewPager(this.mPager);
        refreshData();
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.cityPy = this.newApp.getCity();
        this.homeCity.setText(this.newApp.getCityName());
        this.isVirtualCity = AppMethods.isVirtual(this.cityPy);
        if (this.isVirtualCity) {
            if ("shenyang".equals(this.cityPy)) {
                this.home = new Shengyang();
            } else {
                this.home = new VirtualHome();
            }
            this.no_ad_img.setImageResource(R.drawable.home_ad_temp_virtual);
        } else {
            if ("shenyang".equals(this.cityPy)) {
                this.home = new Shengyang();
            } else {
                this.home = new RealityHome();
            }
            this.no_ad_img.setImageResource(R.drawable.home_ad_temp);
        }
        this.no_ad_img.setOnClickListener(this);
        this.adapter.setVirtual(this.isVirtualCity);
        this.adapter.setCity(this.cityPy);
        this.adapter.initData();
        this.gridView.setAdapter((ListAdapter) this.adapter);
        this.adapter.notifyDataSetChanged();
        this.viewpager_layout.setVisibility(8);
        this.no_ad_img.setVisibility(0);
        getAD();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mChangeCity);
        if (!(this.menu == null || this.menu.getmRefreshMobile() == null)) {
            unregisterReceiver(this.menu.getmRefreshMobile());
        }
        if (this.menu != null && this.menu.getmChangeCity() != null) {
            unregisterReceiver(this.menu.getmChangeCity());
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        if (this.menu.getSlidingMenu().isMenuShowing()) {
            this.menu.getSlidingMenu().showContent();
            return false;
        }
        exitDialog(this, R.string.app_title);
        return false;
    }

    private void getAD() {
        this.viewpager_layout.setVisibility(8);
        this.no_ad_img.setVisibility(0);
        GetAdTask task = new GetAdTask(this, 5, this.adAdapter, this.viewpager_layout, this.no_ad_img);
        task.setCallBack(new GetAdTask.CallBack() {
            public void onSuccess(Ad ad) {
                ((ViewPagerCustomDuration) MenuActivity.this.mPager).stopAutoFlowTimer();
                ((ViewPagerCustomDuration) MenuActivity.this.mPager).startAutoFlowTimer();
                MenuActivity.this.mIndicator.notifyDataSetChanged();
            }

            public void onFail() {
            }
        });
        task.execute(new Object[0]);
    }

    public void exitDialog(final Context context, int appid) {
        new AlertDialog.Builder(context).setTitle(appid).setMessage((int) R.string.confirm_exit_info).setPositiveButton((int) R.string.dialog_button_exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.sendBroadcast(new Intent(ActionTag.INTENT_ACTION_LOGGED_OUT));
            }
        }).setNegativeButton((int) R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    private class LocationTask extends CommonAsyncTask<Location> {
        private final int STATUS_ERROR = -1;
        private final int STATUS_SUCCESS = 1;
        /* access modifiers changed from: private */
        public String locationCity;
        private int stauts = 1;

        public LocationTask(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        public Location onDoInBackgroup() {
            try {
                if (this.mApplication.getLocation() != null) {
                    CommonResultInfo resultCoord = ((HttpApi) MenuActivity.this.newApp.getApi()).getCoordConvertCity(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude());
                    if (resultCoord == null || resultCoord.getResult() != 1 || TextUtils.isEmpty(resultCoord.getData())) {
                        this.stauts = -1;
                    } else {
                        this.locationCity = resultCoord.getData();
                        this.stauts = 1;
                    }
                }
                return this.mApplication.getLocation();
            } catch (Exception e) {
                CorePreferences.ERROR(e);
                this.stauts = -1;
                return null;
            }
        }

        public void onAfterDoInBackgroup(Location result) {
            if (result != null && this.stauts == 1 && AppMethods.isInCitys(this.locationCity) && !this.locationCity.equals(this.mApplication.getCityName())) {
                DialogUtil.showConfrimDialog(MenuActivity.this, MenuActivity.this.getResources().getString(R.string.text_map_location_city_msg, this.locationCity), MenuActivity.this.getResources().getString(R.string.dialog_button_ok), new ConfirmDialogListener() {
                    public void onPositive(DialogInterface dialog) {
                        MenuActivity.this.newApp.setCity(AppArrays.getCity(LocationTask.this.locationCity));
                        MenuActivity.this.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_CHANGE_CITY));
                    }

                    public void onNegative(DialogInterface dialog) {
                    }
                });
            }
        }
    }

    class VirtualHome implements Home {
        VirtualHome() {
        }

        public void onItemClick(int position) {
            switch (position) {
                case 0:
                    MenuActivity.this.jumpNewHouse();
                    return;
                case 1:
                    MenuActivity.this.jumpnewsHouse();
                    return;
                case 2:
                    MenuActivity.this.jumpQR();
                    return;
                case 3:
                    MenuActivity.this.jumpLoan();
                    return;
                default:
                    return;
            }
        }
    }

    class RealityHome implements Home {
        RealityHome() {
        }

        public void onItemClick(int position) {
            switch (position) {
                case 0:
                    MenuActivity.this.jumpNewHouse();
                    return;
                case 1:
                    MenuActivity.this.jumpSecondHouse();
                    return;
                case 2:
                    MenuActivity.this.jumpRentHouse();
                    return;
                case 3:
                    MenuActivity.this.jumpnewsHouse();
                    return;
                case 4:
                    MenuActivity.this.jumpCommunity();
                    return;
                case 5:
                    MenuActivity.this.jumpLoan();
                    return;
                case 6:
                    MenuActivity.this.jumpQR();
                    return;
                case 7:
                    MenuActivity.this.jumpShake();
                    return;
                default:
                    return;
            }
        }
    }

    class Shengyang implements Home {
        Shengyang() {
        }

        public void onItemClick(int position) {
            switch (position) {
                case 0:
                    MenuActivity.this.jumpNewHouse();
                    return;
                case 1:
                    MenuActivity.this.jumpnewsHouse();
                    return;
                case 2:
                    MenuActivity.this.jumpLoan();
                    return;
                case 3:
                    MenuActivity.this.jumpQR();
                    return;
                case 4:
                    MenuActivity.this.jumpShake();
                    return;
                default:
                    return;
            }
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.home.onItemClick(position);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        Class<?> cls;
        if (v.getId() == R.id.home_city) {
            startActivity(new Intent(this, CityChangeActivity.class));
        } else if (v.getId() == R.id.home_search) {
            Intent intent = new Intent();
            if (AppMethods.isJustNewHouse(((NewHouseApplication) this.mApplication).getCity())) {
                cls = NewSearchActivity.class;
                intent.putExtra("litterHome", true);
            } else {
                cls = SearchHomeActivity.class;
            }
            intent.setClass(this, cls);
            startActivity(intent);
        } else if (v.getId() == R.id.home_group) {
            startActivity(new Intent(this, EventListActivity.class));
        } else if (v.getId() == R.id.home_privilege) {
            startActivity(new Intent(this, PrivilegeHomeActivity.class));
        } else if (v.getId() == R.id.house_ad_close) {
        } else {
            if (v.getId() == R.id.home_left) {
                if (!this.menu.getSlidingMenu().isMenuShowing()) {
                    this.menu.getSlidingMenu().toggle();
                }
                this.menu.getSlidingMenu().toggle(true);
            } else if (v.getId() == R.id.home_right) {
                this.menu.getSlidingMenu().toggleSecondMenu(true);
            } else if (v.getId() == R.id.no_ad_img && !this.isVirtualCity) {
                startActivity(new Intent(this, PrivilegeHomeActivity.class));
            }
        }
    }

    /* access modifiers changed from: private */
    public void jumpNewHouse() {
        new UpdateUserInfoTask(this, App.NEW).execute(new Object[0]);
        startActivity(new Intent(this, NewHomeActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpSecondHouse() {
        new UpdateUserInfoTask(this, App.SELL).execute(new Object[0]);
        startActivity(new Intent(this, SecondSellHomeActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpRentHouse() {
        new UpdateUserInfoTask(this, App.RENT).execute(new Object[0]);
        startActivity(new Intent(this, SecondRentHomeActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpnewsHouse() {
        new UpdateUserInfoTask(this, App.Categroy.News.NEWS).execute(new Object[0]);
        startActivity(new Intent(this, NewsHomeActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpCommunity() {
        HouseAnalyse.onViewClick(this, getString(R.string.text_owner_community), getString(R.string.text_owner_community), null);
        final ForumInfo forum = AppArrays.getCityForum(((NewHouseApplication) this.mApplication).getCityKey());
        if (!forum.isApp()) {
            startActivity(IntentUtil.getUriIntent(this, forum.getUrl()));
        } else if (PackageUtil.isExistPackage(this, forum.getPkg())) {
            startActivity(IntentUtil.getOpenAppIntent(this, forum.getPkg()));
        } else {
            ActivityUtil.showConfrimDialog(this, getResources().getString(R.string.text_install_forum, forum.getName()), new DialogOnPositiveListener() {
                public void onPositive(DialogInterface dialog) {
                    MenuActivity.this.startActivity(IntentUtil.getUriIntent(MenuActivity.this, forum.getUrl()));
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void jumpLoan() {
        startActivity(new Intent(this, LoanCalActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpQR() {
        startActivity(new Intent(this, CaptureActivity.class));
    }

    /* access modifiers changed from: private */
    public void jumpShake() {
        startActivity(new Intent(this, ShakeActivity.class));
    }
}
