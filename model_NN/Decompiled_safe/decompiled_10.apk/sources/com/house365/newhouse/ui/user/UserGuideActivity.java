package com.house365.newhouse.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import com.house365.core.activity.BaseCommonActivityWithFragment;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.viewpager.CirclePageIndicator;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.ui.CityChangeActivity;
import com.house365.newhouse.ui.user.adapter.PageViewFragmentAdapter;
import java.util.List;

public class UserGuideActivity extends BaseCommonActivityWithFragment {
    private NewHouseApplication mApplication;
    private CirclePageIndicator mIndicator;
    private List<View> views;
    private ViewPager vp;
    /* access modifiers changed from: private */
    public PageViewFragmentAdapter vpAdapter;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.userguide);
        this.mApplication = (NewHouseApplication) getApplication();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        Intent intent = new Intent(this, CityChangeActivity.class);
        intent.putExtra("isFirst", 1);
        startActivity(intent);
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.vp = (ViewPager) findViewById(R.id.viewPager);
        this.vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                CorePreferences.ERROR("pagechange" + arg0);
                UserGuideActivity.this.vpAdapter.animate(arg0);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                CorePreferences.ERROR("onPageScrolled" + arg0);
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        this.vpAdapter = new PageViewFragmentAdapter(getSupportFragmentManager(), this.vp, this, 3);
        this.vp.setAdapter(this.vpAdapter);
        this.mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        this.mIndicator.setViewPager(this.vp);
        this.mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int arg0) {
                CorePreferences.ERROR("pagechange" + arg0);
                UserGuideActivity.this.vpAdapter.animate(arg0);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }
}
