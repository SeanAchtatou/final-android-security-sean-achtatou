package com.house365.newhouse.ui.secondsell;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.DeviceUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.task.GetBlockTask;
import com.house365.newhouse.task.LocationTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.secondrent.SecondRentOfferActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class BlockChooseActivity extends BaseCommonActivity {
    public static String INTENT_FROM_TYPE = "from_Type";
    public static Dialog lastChangeCityDialog;
    public static Dialog lastSetDialog;
    private SimpleAdapter adapter;
    private DefineArrayAdapter<String> array_adapter;
    /* access modifiers changed from: private */
    public boolean clickHome;
    /* access modifiers changed from: private */
    public Context context;
    private BroadcastReceiver finishReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BlockChooseActivity.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public String fromType;
    private HeadNavigateView head_view;
    private HomeKeyEventBroadCastReceiver homePress = new HomeKeyEventBroadCastReceiver();
    /* access modifiers changed from: private */
    public ListView key_list;
    /* access modifiers changed from: private */
    public EditText keywords;
    private BroadcastReceiver mLocation = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int changeCity = intent.getIntExtra(ActionCode.LOCATION_FINISH, 0);
            int intExtra = intent.getIntExtra(ActionCode.LOCATION_TAG, 0);
            switch (changeCity) {
                case 1:
                    BlockChooseActivity.this.doNearBy();
                    return;
                case 2:
                default:
                    return;
                case 3:
                    BlockChooseActivity.this.nearby_layout.setVisibility(8);
                    return;
                case 4:
                    BlockChooseActivity.this.nearby_layout.setVisibility(8);
                    BlockChooseActivity.this.outCity = true;
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public LinearLayout nearby_layout;
    /* access modifiers changed from: private */
    public ListView nearby_list;
    /* access modifiers changed from: private */
    public boolean noKeydataBack;
    /* access modifiers changed from: private */
    public boolean outCity;
    /* access modifiers changed from: private */
    public Button search_close;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.block_chose);
        this.context = this;
        registerReceiver(this.finishReceiver, new IntentFilter(ActionCode.INTENT_ACTION_FINISH_BLOCKCHOOSE));
        registerReceiver(this.homePress, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        registerReceiver(this.mLocation, new IntentFilter(ActionCode.INTENT_ACTION_LOCATION_FINISH));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.finishReceiver);
        unregisterReceiver(this.homePress);
        unregisterReceiver(this.mLocation);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockChooseActivity.this.finish();
            }
        });
        this.keywords = (EditText) findViewById(R.id.keywords);
        this.search_close = (Button) findViewById(R.id.search_close);
        this.search_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BlockChooseActivity.this.keywords.setText(StringUtils.EMPTY);
                BlockChooseActivity.this.key_list.setVisibility(8);
                if (!BlockChooseActivity.this.outCity) {
                    BlockChooseActivity.this.nearby_layout.setVisibility(0);
                } else {
                    BlockChooseActivity.this.nearby_layout.setVisibility(8);
                }
            }
        });
        this.keywords.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(BlockChooseActivity.this.keywords.getText().toString().trim())) {
                    BlockChooseActivity.this.search_close.setVisibility(0);
                    new BlockKeyTask(BlockChooseActivity.this.context).execute(new Object[0]);
                    return;
                }
                BlockChooseActivity.this.search_close.setVisibility(8);
                BlockChooseActivity.this.key_list.setVisibility(8);
                BlockChooseActivity.this.nearby_layout.setVisibility(0);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.nearby_layout = (LinearLayout) findViewById(R.id.nearby_layout);
        this.key_list = (ListView) findViewById(R.id.key_list);
        this.nearby_list = (ListView) findViewById(R.id.nearby_list);
        AdapterView.OnItemClickListener itemClick = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TextView b_district = (TextView) view.findViewById(R.id.b_district);
                TextView b_street = (TextView) view.findViewById(R.id.b_streetname);
                String blockName = ((TextView) view.findViewById(R.id.h_name)).getText().toString();
                String noNearData = BlockChooseActivity.this.getResources().getString(R.string.text_block_nearby_nodata);
                String noKeydata = BlockChooseActivity.this.getResources().getString(R.string.text_block_keysearch_nodata);
                String noNet = BlockChooseActivity.this.getResources().getString(R.string.text_keysearch_nodata);
                if (!blockName.equals(noNearData) && !blockName.equals(noNet)) {
                    if (blockName.equals(noKeydata)) {
                        Intent intent = new Intent(BlockChooseActivity.this.context, AddBlockActivity.class);
                        if (BlockChooseActivity.this.fromType.equals(App.SELL)) {
                            intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, 1);
                        } else if (BlockChooseActivity.this.fromType.equals(App.RENT)) {
                            intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, 2);
                        }
                        BlockChooseActivity.this.startActivity(intent);
                        return;
                    }
                    String district = b_district.getText().toString();
                    String street = b_street.getText().toString();
                    if (BlockChooseActivity.this.fromType.equals(App.SELL)) {
                        SecondSellOfferActivity.blockName = blockName;
                        SecondSellOfferActivity.district = district;
                        SecondSellOfferActivity.street = street;
                    } else if (BlockChooseActivity.this.fromType.equals(App.RENT)) {
                        SecondRentOfferActivity.blockName = blockName;
                        SecondRentOfferActivity.district = district;
                        SecondRentOfferActivity.street = street;
                    }
                    BlockChooseActivity.this.finish();
                }
            }
        };
        this.key_list.setOnItemClickListener(itemClick);
        this.nearby_list.setOnItemClickListener(itemClick);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.fromType = getIntent().getStringExtra(INTENT_FROM_TYPE);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        doNearBy();
    }

    /* access modifiers changed from: private */
    public void doNearBy() {
        if (!this.noKeydataBack) {
            if (!DeviceUtil.isOpenLoaction(this.context)) {
                this.nearby_layout.setVisibility(8);
                lastSetDialog = AppMethods.showLocationSets(this.context);
                lastSetDialog.show();
            } else {
                this.nearby_layout.setVisibility(0);
                relocation();
            }
        }
        this.noKeydataBack = false;
    }

    private void relocation() {
        LocationTask location = new LocationTask(this.context, (int) R.string.text_locationing_wait);
        location.setTag(-1);
        location.execute(new Object[0]);
        location.setLocationListener(new LocationTask.LocationListener() {
            public void onFinish(Location location, boolean sameCity) {
                GetBlockTask nearBlock = new GetBlockTask(BlockChooseActivity.this.context, location);
                nearBlock.setBlockTaskListener(new GetBlockTask.GetBlockTaskListener() {
                    public void onFinish(List<Block> result) {
                        if (result == null || result.size() <= 0) {
                            BlockChooseActivity.this.setNoKeyData(BlockChooseActivity.this.getResources().getString(R.string.text_block_nearby_nodata), BlockChooseActivity.this.nearby_list);
                        } else if (result.size() > 0) {
                            BlockChooseActivity.this.setKeyData(BlockChooseActivity.this.setBlockMap(result), BlockChooseActivity.this.nearby_list);
                        } else {
                            BlockChooseActivity.this.setNoKeyData(BlockChooseActivity.this.getResources().getString(R.string.text_block_nearby_nodata), BlockChooseActivity.this.nearby_list);
                        }
                    }
                });
                nearBlock.execute(new Object[0]);
            }
        });
    }

    /* access modifiers changed from: private */
    public void setKeyData(List<HashMap<String, String>> search_data, ListView listView) {
        List<HashMap<String, String>> list = search_data;
        this.adapter = new SimpleAdapter(this.context, list, R.layout.keysearch_item, new String[]{"h_id", "h_name", HouseBaseInfo.DISTRICT, "streetname"}, new int[]{R.id.h_id, R.id.h_name, R.id.b_district, R.id.b_streetname});
        this.adapter.notifyDataSetChanged();
        listView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: private */
    public void setNoKeyData(String msg, ListView listView) {
        this.array_adapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, new String[]{msg});
        this.array_adapter.notifyDataSetChanged();
        listView.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public void setNoNetWork(ListView listView) {
        this.array_adapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, new String[]{getResources().getString(R.string.text_keysearch_nodata)});
        this.array_adapter.notifyDataSetChanged();
        listView.setAdapter((ListAdapter) this.array_adapter);
    }

    /* access modifiers changed from: private */
    public List<HashMap<String, String>> setBlockMap(List<Block> bockList) {
        List<HashMap<String, String>> search_data = new ArrayList<>();
        for (int i = 0; i < bockList.size(); i++) {
            HashMap<String, String> item = new HashMap<>();
            Block block_item = bockList.get(i);
            item.put("h_id", block_item.getId());
            item.put("h_name", block_item.getBlockname());
            item.put(HouseBaseInfo.DISTRICT, block_item.getDistrict());
            item.put("streetname", block_item.getStreetname());
            search_data.add(item);
        }
        return search_data;
    }

    private class BlockKeyTask extends CommonAsyncTask<List<Block>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Block>) ((List) obj));
        }

        public BlockKeyTask(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<Block> bockList) {
            if (bockList != null) {
                if (bockList.size() > 0) {
                    BlockChooseActivity.this.setKeyData(BlockChooseActivity.this.setBlockMap(bockList), BlockChooseActivity.this.key_list);
                } else {
                    BlockChooseActivity.this.setNoKeyData(BlockChooseActivity.this.getResources().getString(R.string.text_block_keysearch_nodata), BlockChooseActivity.this.key_list);
                    BlockChooseActivity.this.noKeydataBack = true;
                }
                BlockChooseActivity.this.key_list.setVisibility(0);
                BlockChooseActivity.this.nearby_layout.setVisibility(8);
                return;
            }
            BlockChooseActivity.this.setNoKeyData(BlockChooseActivity.this.getResources().getString(R.string.text_block_keysearch_nodata), BlockChooseActivity.this.key_list);
            BlockChooseActivity.this.noKeydataBack = true;
        }

        public List<Block> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getSecondHouseListBySearch(BlockChooseActivity.this.keywords.getText().toString());
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
            BlockChooseActivity.this.key_list.setVisibility(0);
            BlockChooseActivity.this.nearby_layout.setVisibility(8);
            BlockChooseActivity.this.setNoNetWork(BlockChooseActivity.this.key_list);
        }
    }

    class HomeKeyEventBroadCastReceiver extends BroadcastReceiver {
        static final String SYSTEM_HOME_KEY = "homekey";
        static final String SYSTEM_REASON = "reason";
        static final String SYSTEM_RECENT_APPS = "recentapps";

        HomeKeyEventBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String reason;
            if (intent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && (reason = intent.getStringExtra(SYSTEM_REASON)) != null) {
                if (reason.equals(SYSTEM_HOME_KEY)) {
                    BlockChooseActivity.this.clickHome = true;
                    BlockChooseActivity.this.keywords.setText(StringUtils.EMPTY);
                    BlockChooseActivity.this.key_list.setVisibility(8);
                    if (BlockChooseActivity.lastChangeCityDialog != null && BlockChooseActivity.lastChangeCityDialog.isShowing()) {
                        BlockChooseActivity.lastChangeCityDialog.dismiss();
                    }
                    if (BlockChooseActivity.lastSetDialog != null && BlockChooseActivity.lastSetDialog.isShowing()) {
                        BlockChooseActivity.lastSetDialog.dismiss();
                    }
                } else if (reason.equals(SYSTEM_RECENT_APPS)) {
                    BlockChooseActivity.this.clickHome = true;
                    BlockChooseActivity.this.keywords.setText(StringUtils.EMPTY);
                    BlockChooseActivity.this.key_list.setVisibility(8);
                    if (BlockChooseActivity.lastChangeCityDialog != null && BlockChooseActivity.lastChangeCityDialog.isShowing()) {
                        BlockChooseActivity.lastChangeCityDialog.dismiss();
                    }
                    if (BlockChooseActivity.lastSetDialog != null && BlockChooseActivity.lastSetDialog.isShowing()) {
                        BlockChooseActivity.lastSetDialog.dismiss();
                    }
                }
            }
        }
    }
}
