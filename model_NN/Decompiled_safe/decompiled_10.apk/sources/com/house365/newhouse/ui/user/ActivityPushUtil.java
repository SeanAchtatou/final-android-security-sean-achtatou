package com.house365.newhouse.ui.user;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import java.util.Calendar;
import java.util.Date;

public class ActivityPushUtil {
    private AlarmManager alarmManager;
    private PendingIntent pushIntent;
    private long triggerAtTime;

    public ActivityPushUtil(Context context, String id, String type) {
        this.alarmManager = (AlarmManager) context.getSystemService("alarm");
        Intent intent = new Intent(context, ActivityPushReceiver.class);
        intent.putExtra("id", id);
        intent.putExtra(CouponDetailsActivity.INTENT_TYPE, type);
        this.pushIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    public void pushNotice(Date date) {
        Calendar cal_on = Calendar.getInstance();
        cal_on.set(date.getYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
        this.triggerAtTime = cal_on.getTimeInMillis();
        this.alarmManager.set(0, this.triggerAtTime, this.pushIntent);
    }
}
