package com.house365.newhouse.ui.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.BaseListAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.ui.privilege.BargainlHouseActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.CubeHouseRoomActivity;
import com.house365.newhouse.ui.privilege.adapter.EventAdapter;
import java.util.ArrayList;
import java.util.List;

public class UserEventRecordActivity extends BaseListActivity {
    /* access modifiers changed from: private */
    public EventAdapter adapter;
    private HeadNavigateView head_view;
    protected RefreshInfo listRefresh;
    private View no_data;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullListView;
    private RadioGroup rgEvent;
    /* access modifiers changed from: private */
    public String showType = App.Categroy.Event.TLF;

    /* access modifiers changed from: private */
    public void refreshData() {
        this.adapter.clear();
        this.adapter.notifyDataSetChanged();
        this.listRefresh.refresh = true;
        new GetMySignTask(this, 0, this.pullListView, this.listRefresh, this.adapter, this.no_data, this.showType).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.listRefresh.refresh = false;
        new GetMySignTask(this, 0, this.pullListView, this.listRefresh, this.adapter, this.no_data, this.showType).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_event_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserEventRecordActivity.this.finish();
            }
        });
        this.pullListView = (PullToRefreshListView) findViewById(R.id.list);
        this.no_data = findViewById(R.id.nodata_layout);
        this.listRefresh = new RefreshInfo();
        this.adapter = new EventAdapter(this);
        this.pullListView.setAdapter(this.adapter);
        this.pullListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                UserEventRecordActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                UserEventRecordActivity.this.getMoreData();
            }
        });
        this.pullListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent();
                Class<?> clz = null;
                if (UserEventRecordActivity.this.showType.equals(App.Categroy.Event.COUPON) || UserEventRecordActivity.this.showType.equals("event")) {
                    clz = CouponDetailsActivity.class;
                    intent.putExtra("id", ((Event) UserEventRecordActivity.this.adapter.getItem(position)).getE_id());
                    intent.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.COUPON);
                }
                if (UserEventRecordActivity.this.showType.equals(App.Categroy.Event.TLF)) {
                    clz = CubeHouseRoomActivity.class;
                    intent.putExtra("e_id", ((Event) UserEventRecordActivity.this.adapter.getItem(position)).getE_id());
                }
                if (UserEventRecordActivity.this.showType.equals(App.Categroy.Event.TJF)) {
                    clz = BargainlHouseActivity.class;
                    intent.putExtra("e_id", ((Event) UserEventRecordActivity.this.adapter.getItem(position)).getE_id());
                }
                intent.setClass(UserEventRecordActivity.this, clz);
                UserEventRecordActivity.this.startActivity(intent);
            }
        });
        this.rgEvent = (RadioGroup) findViewById(R.id.tab_group);
        this.rgEvent.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                UserEventRecordActivity.this.adapter.clear();
                UserEventRecordActivity.this.adapter.notifyDataSetChanged();
                if (checkedId == R.id.bt_tab_tlf) {
                    UserEventRecordActivity.this.showType = App.Categroy.Event.TLF;
                } else if (checkedId == R.id.bt_tab_tjf) {
                    UserEventRecordActivity.this.showType = App.Categroy.Event.TJF;
                } else if (checkedId == R.id.bt_tab_coupon) {
                    UserEventRecordActivity.this.showType = App.Categroy.Event.COUPON;
                }
                UserEventRecordActivity.this.refreshData();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        refreshData();
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }

    class GetMySignTask extends BaseListAsyncTask<Event> {
        private List<Event> dataList = new ArrayList();
        private View no_data;
        private TextView tv_data;
        private String type;

        public GetMySignTask(Context context, int loadingResId, PullToRefreshListView pullListView, RefreshInfo listRefresh, EventAdapter adapter, View no_data2, String type2) {
            super(context, pullListView, listRefresh, adapter);
            this.no_data = no_data2;
            this.type = type2;
            this.tv_data = (TextView) no_data2.findViewById(R.id.tv_nodata);
            this.tv_data.setText((int) R.string.text_no_join_event_msg);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            UserEventRecordActivity.this.adapter.clear();
            UserEventRecordActivity.this.adapter.notifyDataSetChanged();
        }

        public List onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            this.dataList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getMySignList(this.type, ((NewHouseApplication) this.mApplication).getMobile(), this.listRefresh.page);
            return this.dataList;
        }

        public void onAfterRefresh(List<Event> list) {
            UserEventRecordActivity.this.adapter.clear();
            UserEventRecordActivity.this.adapter.notifyDataSetChanged();
            if (this.type.equals(UserEventRecordActivity.this.showType)) {
                if (this.listRefresh != null && UserEventRecordActivity.this.pullListView != null) {
                    if (list == null) {
                        this.no_data.setVisibility(0);
                        ((ImageView) this.no_data.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
                        ((TextView) this.no_data.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
                        ViewUtil.onListDataComplete(this.context, list, this.listRefresh, UserEventRecordActivity.this.adapter, UserEventRecordActivity.this.pullListView, (int) R.string.msg_load_error);
                    } else if (list.size() > 0) {
                        this.no_data.setVisibility(8);
                        ViewUtil.onListDataComplete(this.context, list, this.listRefresh, UserEventRecordActivity.this.adapter, UserEventRecordActivity.this.pullListView, (int) R.string.text_no_result);
                    } else {
                        this.no_data.setVisibility(0);
                        ((ImageView) this.no_data.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_nodata);
                        ((TextView) this.no_data.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_join_event_msg);
                    }
                }
            } else if (this.listRefresh.refresh) {
                UserEventRecordActivity.this.pullListView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
            } else {
                UserEventRecordActivity.this.pullListView.onRefreshComplete();
            }
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            this.no_data.setVisibility(0);
            if (this.no_data != null) {
                this.no_data.setVisibility(0);
                UserEventRecordActivity.this.adapter.clear();
                UserEventRecordActivity.this.adapter.notifyDataSetChanged();
                ((ImageView) this.no_data.findViewById(R.id.iv_nodata)).setImageResource(R.drawable.ico_no_net);
                ((TextView) this.no_data.findViewById(R.id.tv_nodata)).setText((int) R.string.text_no_network_pull_down_refresh);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
