package com.house365.newhouse.ui.apn;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.dto.NotificationData;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONObject;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.block.BlockDetailActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.news.NewsDetailActivity;
import com.house365.newhouse.ui.privilege.BargainlHouseActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.CubeHouseRoomActivity;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;

public class APNActivity extends BaseCommonActivity {
    public static final String INTENT_IS_APN = "is_apn";
    private String APN_DETAIL_ID = "apn_detail_id";
    private String APN_DETAIL_TYPE = "apn_detail_type";
    private String APN_TYPE = "apn_type";

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void initData() {
        try {
            JSONObject json = new JSONObject(((NotificationData) getIntent().getSerializableExtra(Constants.NOTIFICATION_DATA)).getUri());
            String type = json.getString(this.APN_TYPE);
            String detailtype = json.getString(this.APN_DETAIL_TYPE);
            String id = json.getString(this.APN_DETAIL_ID);
            if (!TextUtils.isEmpty(type)) {
                Intent intent = null;
                if (type.equals(App.NEW)) {
                    if ("house".equals(detailtype)) {
                        intent = new Intent(this, NewHouseDetailActivity.class);
                        intent.putExtra("h_id", id);
                    } else if (App.Categroy.Event.COUPON.equals(detailtype)) {
                        intent = new Intent(this, CouponDetailsActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra(CouponDetailsActivity.INTENT_TYPE, App.Categroy.Event.COUPON);
                    } else if ("event".equals(detailtype)) {
                        intent = new Intent(this, CouponDetailsActivity.class);
                        intent.putExtra("id", id);
                        intent.putExtra(CouponDetailsActivity.INTENT_TYPE, "event");
                    } else if (App.Categroy.Event.TJF.equals(detailtype)) {
                        intent = new Intent(this, BargainlHouseActivity.class);
                        intent.putExtra("e_id", id);
                    } else if (App.Categroy.Event.TLF.equals(detailtype)) {
                        intent = new Intent(this, CubeHouseRoomActivity.class);
                        intent.putExtra("e_id", id);
                    } else if (App.Categroy.News.NEWS.equals(detailtype)) {
                        intent = new Intent(this, NewsDetailActivity.class);
                        intent.putExtra("id", id);
                    }
                } else if (type.equals(App.SELL)) {
                    if ("house".equals(detailtype)) {
                        intent = new Intent(this, SecondSellDetailActivity.class);
                        intent.putExtra("id", id);
                    } else if ("block".equals(detailtype)) {
                        intent = new Intent(this, BlockDetailActivity.class);
                        intent.putExtra("id", id);
                    } else if (App.Categroy.News.NEWS.equals(detailtype)) {
                        intent = new Intent(this, NewsDetailActivity.class);
                        intent.putExtra("id", id);
                    }
                } else if ("house".equals(detailtype)) {
                    intent = new Intent(this, SecondRentDetailActivity.class);
                    intent.putExtra("id", id);
                } else if ("block".equals(detailtype)) {
                    intent = new Intent(this, BlockDetailActivity.class);
                    intent.putExtra("id", id);
                } else if (App.Categroy.News.NEWS.equals(detailtype)) {
                    intent = new Intent(this, NewsDetailActivity.class);
                    intent.putExtra("id", id);
                }
                if (intent != null) {
                    intent.putExtra(INTENT_IS_APN, true);
                    startActivity(intent);
                } else if (!ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
                    startActivity(new Intent(this, SplashActivity.class));
                }
            }
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            if (!ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
                startActivity(new Intent(this, SplashActivity.class));
            }
        }
        finish();
    }
}
