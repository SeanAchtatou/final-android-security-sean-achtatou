package com.house365.newhouse.ui.news.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.util.TextUtil;
import com.house365.core.util.TimeUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.News;
import com.house365.newhouse.ui.news.NewsDetailActivity;

public class NewsAdapter extends BaseCacheListAdapter<News> {
    /* access modifiers changed from: private */
    public int channel;
    /* access modifiers changed from: private */
    public Context context;
    private LayoutInflater inflater;
    private NewHouseApplication mApplication;

    public NewsAdapter(Context context2) {
        super(context2);
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.mApplication = (NewHouseApplication) context2.getApplicationContext();
    }

    public int getChannel() {
        return this.channel;
    }

    public void setChannel(int channel2) {
        this.channel = channel2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        NewsGalleyAdapter adapter = new NewsGalleyAdapter(this.context);
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_list_news, (ViewGroup) null, false);
            holder = new ViewHolder();
            holder.news_title = (TextView) convertView.findViewById(R.id.news_title);
            holder.news_date = (TextView) convertView.findViewById(R.id.news_date);
            holder.galley_layout = (LinearLayout) convertView.findViewById(R.id.galley_layout);
            holder.news_pics = (GridView) convertView.findViewById(R.id.pic_gallery);
            holder.ico_news = (ImageView) convertView.findViewById(R.id.ico_news);
            holder.n_pic = (ImageView) convertView.findViewById(R.id.n_pic);
            holder.n_summary = (TextView) convertView.findViewById(R.id.n_summary);
            holder.news_click_layout = convertView.findViewById(R.id.news_click_layout);
            holder.bottom_line = convertView.findViewById(R.id.bottom_line);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        adapter.clear();
        adapter.notifyDataSetChanged();
        final News news = (News) getItem(position);
        if (position == getCount() - 1) {
            holder.bottom_line.setVisibility(0);
        } else {
            holder.bottom_line.setVisibility(8);
        }
        holder.news_title.setText(news.getN_title());
        holder.news_date.setText(TimeUtil.toDateWithFormat(news.getN_addtime(), "yyyy年MM月dd日\tHH:mm"));
        if (this.mApplication.hasNews(news)) {
            holder.news_title.setTextAppearance(this.context, R.style.item_news_16_readed);
            holder.ico_news.setImageResource(R.drawable.ico_news_read);
        } else {
            holder.ico_news.setImageResource(R.drawable.ico_news_normal);
            holder.news_title.setTextAppearance(this.context, R.style.item_news_16_normal);
        }
        String pic = news.getN_pic();
        if (!TextUtils.isEmpty(pic)) {
            holder.n_pic.setVisibility(0);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.n_pic.getLayoutParams();
            lp.width = this.mApplication.getScreenWidth() / 3;
            lp.height = (lp.width / 4) * 3;
            holder.n_pic.setLayoutParams(lp);
            setCacheImage(holder.n_pic, pic, R.drawable.bg_default_img_photo, 1);
        } else {
            holder.n_pic.setVisibility(8);
        }
        TextUtil.setNullText(news.getN_summary(), holder.n_summary);
        holder.news_click_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewsAdapter.this.context, NewsDetailActivity.class);
                intent.putExtra("channel", NewsAdapter.this.channel);
                intent.putExtra("id", news.getN_id());
                NewsAdapter.this.context.startActivity(intent);
            }
        });
        return convertView;
    }

    class ViewHolder {
        /* access modifiers changed from: private */
        public View bottom_line;
        /* access modifiers changed from: private */
        public LinearLayout galley_layout;
        /* access modifiers changed from: private */
        public ImageView ico_news;
        /* access modifiers changed from: private */
        public ImageView n_pic;
        /* access modifiers changed from: private */
        public TextView n_summary;
        /* access modifiers changed from: private */
        public View news_click_layout;
        /* access modifiers changed from: private */
        public TextView news_date;
        /* access modifiers changed from: private */
        public GridView news_pics;
        /* access modifiers changed from: private */
        public TextView news_title;

        ViewHolder() {
        }
    }
}
