package com.house365.newhouse.ui.tools;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.ActivityUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.util.MathUtil;
import java.util.ArrayList;
import java.util.List;

public class LoanCalActivity extends BaseCommonActivity {
    public int ACTION_CODE_PAYMETHOD = 4;
    public int ACTION_CODE_RATE = 3;
    public int ACTION_CODE_YEAR_BUSINESS = 1;
    public int ACTION_CODE_YEAR_FOUND = 2;
    /* access modifiers changed from: private */
    public List<BusinessRate> allBusinessRate = new ArrayList();
    private List<FoundRate> allFoundRate = new ArrayList();
    /* access modifiers changed from: private */
    public List<LoanYears> allLoanYear = new ArrayList();
    private Button btCal;
    private RadioGroup bt_loan_type_group;
    /* access modifiers changed from: private */
    public EditText etUserBusinessMoney;
    /* access modifiers changed from: private */
    public EditText etUserFoundMoney;
    private TextView lableUserBusinessLoanYear;
    private TextView lableUserBusinessMoney;
    private TextView lableUserFoundLoanYear;
    private TextView lableUserFoundMoney;
    /* access modifiers changed from: private */
    public int payMethod = 0;
    /* access modifiers changed from: private */
    public String[] payMethodStr = {"等额本息", "等额本金"};
    private TextView tvPayMethod;
    private TextView tvShowUserBusinessRate;
    private TextView tvShowUserFoundRate;
    private TextView tvUserBusinessLoanYear;
    private TextView tvUserBusinessRate;
    private TextView tvUserFoundLoanYear;
    /* access modifiers changed from: private */
    public LoanYears userBusinessLoanYear;
    private ViewGroup userBusinessLoanYearLayout;
    private double userBusinessMoney;
    private ViewGroup userBusinessMoneyLayout;
    /* access modifiers changed from: private */
    public BusinessRate userBusinessRate;
    /* access modifiers changed from: private */
    public LoanYears userFoundLoanYear;
    private ViewGroup userFoundLoanYearLayout;
    private double userFoundMoney;
    private ViewGroup userFoundMoneyLayout;
    private FoundRate userFoundRate;
    /* access modifiers changed from: private */
    public LoanType userLoanType;

    public enum LoanType {
        BUSINESS,
        FOUND,
        MIX
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.loan_cal_layout);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != -1) {
            return;
        }
        if (requestCode == this.ACTION_CODE_YEAR_BUSINESS) {
            this.userBusinessLoanYear = this.allLoanYear.get(data.getIntExtra(LoanCalSelProActivity.INTENT_RESULT_IDX, -1));
            this.tvUserBusinessLoanYear.setText(this.userBusinessLoanYear.getKey());
            setShowRate();
        } else if (requestCode == this.ACTION_CODE_YEAR_FOUND) {
            this.userFoundLoanYear = this.allLoanYear.get(data.getIntExtra(LoanCalSelProActivity.INTENT_RESULT_IDX, -1));
            this.tvUserFoundLoanYear.setText(this.userFoundLoanYear.getKey());
            setShowRate();
        } else if (requestCode == this.ACTION_CODE_RATE) {
            this.userBusinessRate = this.allBusinessRate.get(data.getIntExtra(LoanCalSelProActivity.INTENT_RESULT_IDX, -1));
            this.tvUserBusinessRate.setText(this.userBusinessRate.getKey());
            setShowRate();
        } else if (requestCode == this.ACTION_CODE_PAYMETHOD) {
            this.payMethod = data.getIntExtra(LoanCalSelProActivity.INTENT_RESULT_IDX, -1);
            this.tvPayMethod.setText(this.payMethodStr[this.payMethod]);
        }
    }

    /* access modifiers changed from: protected */
    public void initView() {
        ((HeadNavigateView) findViewById(R.id.head_view)).getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoanCalActivity.this.finish();
            }
        });
        this.bt_loan_type_group = (RadioGroup) findViewById(R.id.bt_loan_type_group);
        this.tvPayMethod = (TextView) findViewById(R.id.tvPayMethod);
        this.lableUserBusinessLoanYear = (TextView) findViewById(R.id.lableUserBusinessLoanYear);
        this.tvUserBusinessLoanYear = (TextView) findViewById(R.id.tvUserBusinessLoanYear);
        this.tvUserBusinessRate = (TextView) findViewById(R.id.tvUserBusinessRate);
        this.tvShowUserBusinessRate = (TextView) findViewById(R.id.tvShowUserBusinessRate);
        this.lableUserBusinessMoney = (TextView) findViewById(R.id.lableUserBusinessMoney);
        this.etUserBusinessMoney = (EditText) findViewById(R.id.etUserBusinessMoney);
        this.userBusinessMoneyLayout = (ViewGroup) findViewById(R.id.userBusinessMoneyLayout);
        this.userBusinessLoanYearLayout = (ViewGroup) findViewById(R.id.userBusinessLoanYearLayout);
        this.lableUserFoundLoanYear = (TextView) findViewById(R.id.lableUserFoundLoanYear);
        this.tvUserFoundLoanYear = (TextView) findViewById(R.id.tvUserFoundLoanYear);
        this.tvShowUserFoundRate = (TextView) findViewById(R.id.tvShowUserFoundRate);
        this.lableUserFoundMoney = (TextView) findViewById(R.id.lableUserFoundMoney);
        this.etUserFoundMoney = (EditText) findViewById(R.id.etUserFoundMoney);
        this.userFoundMoneyLayout = (ViewGroup) findViewById(R.id.userFoundMoneyLayout);
        this.userFoundLoanYearLayout = (ViewGroup) findViewById(R.id.userFoundLoanYearLayout);
        this.btCal = (Button) findViewById(R.id.btCal);
        this.bt_loan_type_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_business) {
                    LoanCalActivity.this.userLoanType = LoanType.BUSINESS;
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_business)).setTextAppearance(LoanCalActivity.this, R.style.title_cal_18_blue);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_found)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_mix)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                } else if (checkedId == R.id.bt_found) {
                    LoanCalActivity.this.userLoanType = LoanType.FOUND;
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_found)).setTextAppearance(LoanCalActivity.this, R.style.title_cal_18_blue);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_business)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_mix)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                } else {
                    LoanCalActivity.this.userLoanType = LoanType.MIX;
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_mix)).setTextAppearance(LoanCalActivity.this, R.style.title_cal_18_blue);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_business)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                    ((TextView) LoanCalActivity.this.findViewById(R.id.bt_found)).setTextAppearance(LoanCalActivity.this, R.style.label_cal_18_black);
                }
                LoanCalActivity.this.showView();
                LoanCalActivity.this.setShowRate();
            }
        });
        this.tvPayMethod.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoanCalActivity.this, LoanCalSelProActivity.class);
                intent.putExtra(LoanCalSelProActivity.INTENT_PROS, LoanCalActivity.this.payMethodStr);
                intent.putExtra(LoanCalSelProActivity.INTENT_IDX, LoanCalActivity.this.payMethod);
                intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, "还款方式");
                LoanCalActivity.this.startActivityForResult(intent, LoanCalActivity.this.ACTION_CODE_PAYMETHOD);
            }
        });
        this.tvUserBusinessLoanYear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoanCalActivity.this, LoanCalSelProActivity.class);
                intent.putExtra(LoanCalSelProActivity.INTENT_PROS, (String[]) LoanCalActivity.this.getClaBeanKeys(LoanCalActivity.this.allLoanYear).toArray(new String[0]));
                intent.putExtra(LoanCalSelProActivity.INTENT_IDX, LoanCalActivity.this.allLoanYear.indexOf(LoanCalActivity.this.userBusinessLoanYear));
                intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, "贷款期限");
                LoanCalActivity.this.startActivityForResult(intent, LoanCalActivity.this.ACTION_CODE_YEAR_BUSINESS);
            }
        });
        this.tvUserFoundLoanYear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoanCalActivity.this, LoanCalSelProActivity.class);
                intent.putExtra(LoanCalSelProActivity.INTENT_PROS, (String[]) LoanCalActivity.this.getClaBeanKeys(LoanCalActivity.this.allLoanYear).toArray(new String[0]));
                intent.putExtra(LoanCalSelProActivity.INTENT_IDX, LoanCalActivity.this.allLoanYear.indexOf(LoanCalActivity.this.userFoundLoanYear));
                intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, "贷款期限");
                LoanCalActivity.this.startActivityForResult(intent, LoanCalActivity.this.ACTION_CODE_YEAR_FOUND);
            }
        });
        this.tvUserBusinessRate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoanCalActivity.this, LoanCalSelProActivity.class);
                intent.putExtra(LoanCalSelProActivity.INTENT_PROS, (String[]) LoanCalActivity.this.getClaBeanKeys(LoanCalActivity.this.allBusinessRate).toArray(new String[0]));
                intent.putExtra(LoanCalSelProActivity.INTENT_IDX, LoanCalActivity.this.allBusinessRate.indexOf(LoanCalActivity.this.userBusinessRate));
                intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, "按揭利率");
                LoanCalActivity.this.startActivityForResult(intent, LoanCalActivity.this.ACTION_CODE_RATE);
            }
        });
        this.btCal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoanCalActivity.this.userLoanType == LoanType.BUSINESS) {
                    if (TextUtils.isEmpty(LoanCalActivity.this.etUserBusinessMoney.getText().toString())) {
                        ActivityUtil.showToast(LoanCalActivity.this, (int) R.string.msg_empty_business_money);
                        return;
                    }
                } else if (LoanCalActivity.this.userLoanType == LoanType.FOUND) {
                    if (TextUtils.isEmpty(LoanCalActivity.this.etUserFoundMoney.getText().toString())) {
                        ActivityUtil.showToast(LoanCalActivity.this, (int) R.string.msg_empty_found_money);
                        return;
                    }
                } else if (TextUtils.isEmpty(LoanCalActivity.this.etUserBusinessMoney.getText().toString())) {
                    ActivityUtil.showToast(LoanCalActivity.this, (int) R.string.msg_empty_business_money);
                    return;
                } else if (TextUtils.isEmpty(LoanCalActivity.this.etUserFoundMoney.getText().toString())) {
                    ActivityUtil.showToast(LoanCalActivity.this, (int) R.string.msg_empty_found_money);
                    return;
                }
                if (LoanCalActivity.this.payMethod == 0) {
                    LoanCalResultMethodOne result = LoanCalActivity.this.calPayMethodOne();
                    Intent intent = new Intent(LoanCalActivity.this, LoanCalMethodOneResultActivity.class);
                    intent.putExtra(LoanCalResultMethodOne.INTENT_NAME, result);
                    LoanCalActivity.this.startActivity(intent);
                    return;
                }
                LoanCalResultMethodTwo result2 = LoanCalActivity.this.calPayMethodTwo();
                Intent intent2 = new Intent(LoanCalActivity.this, LoanCalMethodTwoResultActivity.class);
                intent2.putExtra(LoanCalResultMethodTwo.INTENT_NAME, result2);
                LoanCalActivity.this.startActivity(intent2);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.allBusinessRate.add(new BusinessRate("12年07月06日利率(1.1倍)", 0.066d, 0.0677d, 0.0704d, 0.0721d));
        this.allBusinessRate.add(new BusinessRate("12年07月06日利率(85折)", 0.051d, 0.0523d, 0.0544d, 0.0557d));
        this.allBusinessRate.add(new BusinessRate("12年07月06日利率(8折)", 0.048d, 0.0492d, 0.0512d, 0.0524d));
        this.allBusinessRate.add(new BusinessRate("12年07月06日利率(7折)", 0.042d, 0.0431d, 0.0448d, 0.0459d));
        this.allBusinessRate.add(new BusinessRate("12年07月06日基准利率", 0.06d, 0.0615d, 0.064d, 0.0655d));
        this.allBusinessRate.add(new BusinessRate("12年06月08日利率(1.1倍)", 0.0694d, 0.0704d, 0.0732d, 0.0748d));
        this.allBusinessRate.add(new BusinessRate("12年06月08日利率(85折)", 0.0536d, 0.0544d, 0.0565d, 0.0578d));
        this.allBusinessRate.add(new BusinessRate("12年06月08日利率(8折)", 0.0505d, 0.0512d, 0.0532d, 0.0544d));
        this.allBusinessRate.add(new BusinessRate("12年06月08日基准利率", 0.0631d, 0.064d, 0.0665d, 0.068d));
        this.allBusinessRate.add(new BusinessRate("11年07月07日利率(1.1倍)", 0.0722d, 0.0732d, 0.0759d, 0.0775d));
        this.allBusinessRate.add(new BusinessRate("11年07月07日利率(85折)", 0.0558d, 0.0565d, 0.0586d, 0.0599d));
        this.allBusinessRate.add(new BusinessRate("11年07月07日基准利率", 0.0656d, 0.0665d, 0.069d, 0.0705d));
        this.allBusinessRate.add(new BusinessRate("11年04月06日利率(1.1倍)", 0.0694d, 0.0704d, 0.0732d, 0.0748d));
        this.allBusinessRate.add(new BusinessRate("11年04月06日利率(85折)", 0.0536d, 0.0544d, 0.0565d, 0.0578d));
        this.allBusinessRate.add(new BusinessRate("11年04月06日利率(7折)", 0.0442d, 0.0448d, 0.0466d, 0.0476d));
        this.allBusinessRate.add(new BusinessRate("11年04月06日基准利率", 0.0631d, 0.064d, 0.0665d, 0.068d));
        this.allBusinessRate.add(new BusinessRate("11年02月09日利率(1.1倍)", 0.0667d, 0.0671d, 0.071d, 0.0726d));
        this.allBusinessRate.add(new BusinessRate("11年02月09日利率(85折)", 0.0515d, 0.0519d, 0.0548d, 0.0561d));
        this.allBusinessRate.add(new BusinessRate("11年02月09日利率(7折)", 0.0424d, 0.0427d, 0.0452d, 0.0462d));
        this.allBusinessRate.add(new BusinessRate("11年02月09日基准利率", 0.0606d, 0.061d, 0.0645d, 0.066d));
        this.allFoundRate.add(new FoundRate("12年07月06日", 0.04d, 0.045d));
        this.allFoundRate.add(new FoundRate("12年06月08日", 0.042d, 0.047d));
        this.allFoundRate.add(new FoundRate("11年07月07日", 0.0445d, 0.049d));
        this.allFoundRate.add(new FoundRate("11年04月06日", 0.042d, 0.047d));
        this.allFoundRate.add(new FoundRate("11年02月09日", 0.04d, 0.045d));
        for (int i = 1; i <= 30; i++) {
            this.allLoanYear.add(new LoanYears(String.valueOf(i) + "年(" + (i * 12) + "期)", i * 12));
        }
        this.userLoanType = LoanType.BUSINESS;
        this.userBusinessLoanYear = this.allLoanYear.get(19);
        this.userFoundLoanYear = this.allLoanYear.get(19);
        this.userBusinessRate = this.allBusinessRate.get(4);
        this.userBusinessMoney = 0.0d;
        this.userFoundMoney = 0.0d;
        this.tvUserBusinessLoanYear.setText(this.userBusinessLoanYear.getKey());
        this.tvUserFoundLoanYear.setText(this.userFoundLoanYear.getKey());
        this.tvUserBusinessRate.setText(this.userBusinessRate.getKey());
        this.tvPayMethod.setText(this.payMethodStr[this.payMethod]);
        showView();
        setShowRate();
    }

    /* access modifiers changed from: private */
    public void showView() {
        if (this.userLoanType == LoanType.BUSINESS) {
            this.lableUserBusinessMoney.setVisibility(8);
            this.userFoundMoneyLayout.setVisibility(8);
            this.userBusinessMoneyLayout.setVisibility(0);
            this.lableUserBusinessLoanYear.setVisibility(8);
            this.userFoundLoanYearLayout.setVisibility(8);
            this.userBusinessLoanYearLayout.setVisibility(0);
        } else if (this.userLoanType == LoanType.FOUND) {
            this.lableUserFoundMoney.setVisibility(8);
            this.userFoundMoneyLayout.setVisibility(0);
            this.userBusinessMoneyLayout.setVisibility(8);
            this.lableUserFoundLoanYear.setVisibility(8);
            this.userFoundLoanYearLayout.setVisibility(0);
            this.userBusinessLoanYearLayout.setVisibility(8);
        } else {
            this.lableUserBusinessMoney.setVisibility(0);
            this.lableUserFoundMoney.setVisibility(0);
            this.userFoundMoneyLayout.setVisibility(0);
            this.userBusinessMoneyLayout.setVisibility(0);
            this.lableUserFoundLoanYear.setVisibility(0);
            this.lableUserBusinessLoanYear.setVisibility(0);
            this.userFoundLoanYearLayout.setVisibility(0);
            this.userBusinessLoanYearLayout.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void setShowRate() {
        if (this.userLoanType == LoanType.BUSINESS) {
            this.tvShowUserBusinessRate.setText(getResources().getString(R.string.business_rate, rateToString(this.userBusinessRate.getRateByYear(this.userBusinessLoanYear))));
            this.tvShowUserBusinessRate.setVisibility(0);
            this.tvShowUserFoundRate.setVisibility(8);
        } else if (this.userLoanType == LoanType.FOUND) {
            getFoundRateByBusinessRate(this.userBusinessRate);
            this.tvShowUserFoundRate.setText(getResources().getString(R.string.found_rate, rateToString(this.userFoundRate.getRateByYear(this.userFoundLoanYear))));
            this.tvShowUserBusinessRate.setVisibility(8);
            this.tvShowUserFoundRate.setVisibility(0);
        } else {
            this.tvShowUserBusinessRate.setText(getResources().getString(R.string.business_rate, rateToString(this.userBusinessRate.getRateByYear(this.userBusinessLoanYear))));
            getFoundRateByBusinessRate(this.userBusinessRate);
            this.tvShowUserFoundRate.setText(getResources().getString(R.string.found_rate, rateToString(this.userFoundRate.getRateByYear(this.userFoundLoanYear))));
            this.tvShowUserBusinessRate.setVisibility(0);
            this.tvShowUserFoundRate.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public List<String> getClaBeanKeys(List<? extends CalBean> ls) {
        List<String> keys = new ArrayList<>();
        for (CalBean bean : ls) {
            keys.add(bean.getKey());
        }
        return keys;
    }

    private String rateToString(double rate) {
        return String.valueOf((10000.0d * rate) / 100.0d) + "%";
    }

    private void getFoundRateByBusinessRate(BusinessRate businessRate) {
        String s = businessRate.getKey();
        String s2 = s.substring(0, s.indexOf("日"));
        for (FoundRate foundRate : this.allFoundRate) {
            if (foundRate.getKey().startsWith(s2)) {
                this.userFoundRate = foundRate;
            }
        }
    }

    /* access modifiers changed from: private */
    public LoanCalResultMethodOne calPayMethodOne() {
        LoanCalResultMethodOne result = new LoanCalResultMethodOne();
        result.setLoanType(this.userLoanType);
        if (this.userLoanType == LoanType.BUSINESS || this.userLoanType == LoanType.MIX) {
            double userLoanMoney = Double.parseDouble(this.etUserBusinessMoney.getText().toString()) * 10000.0d;
            int loantimes = this.userBusinessLoanYear.getTimes();
            double userLoanRate = this.userBusinessRate.getRateByYear(this.userBusinessLoanYear) / 12.0d;
            double totalRate = MathUtil.convertDouble(((((((double) loantimes) * userLoanMoney) * userLoanRate) * Math.pow(1.0d + userLoanRate, (double) loantimes)) / (Math.pow(1.0d + userLoanRate, (double) loantimes) - 1.0d)) - userLoanMoney, 2);
            double totalRepay = MathUtil.convertDouble(totalRate + userLoanMoney, 2);
            double monthRepay = MathUtil.convertDouble(totalRepay / ((double) loantimes), 2);
            result.setLoantimesBusiness(loantimes);
            result.setMonthRepayBusiness(monthRepay);
            result.setTotalRateBusiness(totalRate);
            result.setTotalRepayBusiness(totalRepay);
            result.setUserLoanMoneyBusiness(userLoanMoney);
        }
        if (this.userLoanType == LoanType.FOUND || this.userLoanType == LoanType.MIX) {
            double userLoanMoney2 = Double.parseDouble(this.etUserFoundMoney.getText().toString()) * 10000.0d;
            int loantimes2 = this.userFoundLoanYear.getTimes();
            double userLoanRate2 = this.userFoundRate.getRateByYear(this.userFoundLoanYear) / 12.0d;
            double totalRate2 = MathUtil.convertDouble(((((((double) loantimes2) * userLoanMoney2) * userLoanRate2) * Math.pow(1.0d + userLoanRate2, (double) loantimes2)) / (Math.pow(1.0d + userLoanRate2, (double) loantimes2) - 1.0d)) - userLoanMoney2, 2);
            double totalRepay2 = MathUtil.convertDouble(totalRate2 + userLoanMoney2, 2);
            double monthRepay2 = MathUtil.convertDouble(totalRepay2 / ((double) loantimes2), 2);
            result.setLoantimesFound(loantimes2);
            result.setMonthRepayFound(monthRepay2);
            result.setTotalRateFound(totalRate2);
            result.setTotalRepayFound(totalRepay2);
            result.setUserLoanMoneyFound(userLoanMoney2);
        }
        return result;
    }

    /* access modifiers changed from: private */
    public LoanCalResultMethodTwo calPayMethodTwo() {
        LoanCalResultMethodTwo result = new LoanCalResultMethodTwo();
        result.setLoanType(this.userLoanType);
        if (this.userLoanType == LoanType.BUSINESS || this.userLoanType == LoanType.MIX) {
            List<Double> monthRepays = new ArrayList<>();
            double userLoanMoney = Double.parseDouble(this.etUserBusinessMoney.getText().toString()) * 10000.0d;
            int loantimes = this.userBusinessLoanYear.getTimes();
            double userLoanRate = this.userBusinessRate.getRateByYear(this.userBusinessLoanYear) / 12.0d;
            double totalRate = ((userLoanMoney * userLoanRate) * ((double) (loantimes + 1))) / 2.0d;
            double totalRepay = totalRate + userLoanMoney;
            double b = userLoanMoney / ((double) loantimes);
            for (int i = 1; i <= loantimes; i++) {
                monthRepays.add(Double.valueOf(MathUtil.convertDouble((((userLoanMoney * userLoanRate) * ((double) ((loantimes - i) + 1))) / ((double) loantimes)) + b, 2)));
            }
            result.setLoantimesBusiness(loantimes);
            result.setMonthRepayBusinesses(monthRepays);
            result.setTotalRateBusiness(MathUtil.convertDouble(totalRate, 2));
            result.setTotalRepayBusiness(MathUtil.convertDouble(totalRepay, 2));
            result.setUserLoanMoneyBusiness(MathUtil.convertDouble(userLoanMoney, 2));
        }
        if (this.userLoanType == LoanType.FOUND || this.userLoanType == LoanType.MIX) {
            List<Double> monthRepays2 = new ArrayList<>();
            double userLoanMoney2 = Double.parseDouble(this.etUserFoundMoney.getText().toString()) * 10000.0d;
            int loantimes2 = this.userFoundLoanYear.getTimes();
            double userLoanRate2 = this.userFoundRate.getRateByYear(this.userFoundLoanYear) / 12.0d;
            double totalRate2 = ((userLoanMoney2 * userLoanRate2) * ((double) (loantimes2 + 1))) / 2.0d;
            double totalRepay2 = totalRate2 + userLoanMoney2;
            double b2 = userLoanMoney2 / ((double) loantimes2);
            for (int i2 = 1; i2 <= loantimes2; i2++) {
                monthRepays2.add(Double.valueOf(MathUtil.convertDouble((((userLoanMoney2 * userLoanRate2) * ((double) ((loantimes2 - i2) + 1))) / ((double) loantimes2)) + b2, 2)));
            }
            result.setLoantimesFound(loantimes2);
            result.setMonthRepayFounds(monthRepays2);
            result.setTotalRateFound(MathUtil.convertDouble(totalRate2, 2));
            result.setTotalRepayFound(MathUtil.convertDouble(totalRepay2, 2));
            result.setUserLoanMoneyFound(MathUtil.convertDouble(userLoanMoney2, 2));
        }
        return result;
    }
}
