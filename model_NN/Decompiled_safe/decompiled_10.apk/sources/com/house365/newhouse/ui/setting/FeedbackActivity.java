package com.house365.newhouse.ui.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.RegexUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import org.apache.commons.lang.StringUtils;

public class FeedbackActivity extends BaseCommonActivity {
    /* access modifiers changed from: private */
    public EditText etFeedback;
    /* access modifiers changed from: private */
    public EditText etMobile;
    private HeadNavigateView head_view;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.feedback);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_feedback);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FeedbackActivity.this.finish();
            }
        });
        this.etMobile = (EditText) findViewById(R.id.et_mobile);
        this.etFeedback = (EditText) findViewById(R.id.et_feedback);
        this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String feedback = FeedbackActivity.this.etFeedback.getText().toString().trim();
                final String mobile = FeedbackActivity.this.etMobile.getText().toString().trim();
                if (StringUtils.isEmpty(feedback)) {
                    FeedbackActivity.this.showToast((int) R.string.text_feedback_null);
                    FeedbackActivity.this.etFeedback.setText(StringUtils.EMPTY);
                    FeedbackActivity.this.etFeedback.requestFocus();
                } else if (StringUtils.isEmpty(mobile) || RegexUtil.isNumberWithLen(mobile, 11)) {
                    new CommonAsyncTask<CommonResultInfo>(FeedbackActivity.this) {
                        public void onAfterDoInBackgroup(CommonResultInfo resultInfo) {
                            if (resultInfo != null && resultInfo.getResult() == 1) {
                                FeedbackActivity.this.showToast((int) R.string.text_feedback_send_success);
                                FeedbackActivity.this.finish();
                            } else if (resultInfo == null) {
                                FeedbackActivity.this.showToast((int) R.string.text_failue_work);
                            } else {
                                FeedbackActivity.this.showToast(resultInfo.getMsg());
                            }
                        }

                        public CommonResultInfo onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
                            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).sendFeedback(feedback, mobile);
                        }

                        /* access modifiers changed from: protected */
                        public void onNetworkUnavailable() {
                            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
                        }
                    }.execute(new Object[0]);
                } else {
                    FeedbackActivity.this.showToast((int) R.string.text_validate_mobile);
                    FeedbackActivity.this.etMobile.requestFocus();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.etMobile.setText(((NewHouseApplication) this.mApplication).getMobile());
    }
}
