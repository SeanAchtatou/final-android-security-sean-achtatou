package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;

public class OfferImageAdapter extends BaseCacheListAdapter<String> {
    private LayoutInflater inflater;

    public OfferImageAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder(null);
            convertView = this.inflater.inflate((int) R.layout.offer_pic_item_view, (ViewGroup) null);
            holder.default_img = (ImageView) convertView.findViewById(R.id.default_img);
            holder.delete_img = (ImageView) convertView.findViewById(R.id.delete_img);
            holder.delete_img.setVisibility(8);
            holder.default_img.setLayoutParams(new RelativeLayout.LayoutParams(100, 100));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        setCacheImage(holder.default_img, (String) getItem(position), R.drawable.bg_default_img_detail, 1);
        return convertView;
    }

    private static class ViewHolder {
        ImageView default_img;
        ImageView delete_img;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
