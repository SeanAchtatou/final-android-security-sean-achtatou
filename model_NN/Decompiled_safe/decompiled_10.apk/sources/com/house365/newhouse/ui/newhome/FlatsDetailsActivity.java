package com.house365.newhouse.ui.newhome;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import java.util.ArrayList;

public class FlatsDetailsActivity extends BaseCommonActivity {
    private ImageView flats_pic;
    private String flats_title;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public Photo photo;
    private TextView text_flats_momo;
    private TextView text_flats_title;
    private TextView text_flats_towards;
    private TextView text_flats_type;
    private TextView text_flats_unit;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.flast_details);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FlatsDetailsActivity.this.finish();
            }
        });
        this.flats_pic = (ImageView) findViewById(R.id.flats_pic);
        this.text_flats_momo = (TextView) findViewById(R.id.text_flats_momo);
        this.text_flats_type = (TextView) findViewById(R.id.text_flats_type);
        this.text_flats_unit = (TextView) findViewById(R.id.text_flats_unit);
        this.text_flats_towards = (TextView) findViewById(R.id.text_flats_towards);
        this.text_flats_title = (TextView) findViewById(R.id.text_flats_title);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        if (getIntent().getExtras() != null) {
            this.flats_title = getIntent().getStringExtra("flats_title");
            this.photo = (Photo) getIntent().getSerializableExtra("photo");
        }
        this.flats_title = this.photo.getP_name();
        if (this.flats_title != null && !TextUtils.isEmpty(this.flats_title)) {
            this.text_flats_title.setText(this.flats_title);
            this.head_view.setTvTitleText(this.flats_title);
        }
        if (this.photo != null) {
            setImage(this.flats_pic, this.photo.getP_url(), (int) R.drawable.bg_default_img_photo, 2);
            this.text_flats_title.setText(this.photo.getP_tag());
            this.text_flats_type.setText(this.photo.getP_name());
            this.text_flats_unit.setText(this.photo.getP_area());
            if (this.photo.getP_url() == null || TextUtils.isEmpty(this.photo.getP_url())) {
                this.flats_pic.setEnabled(false);
            }
        }
        this.flats_pic.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(FlatsDetailsActivity.this, AlbumFullScreenActivity.class);
                ArrayList<String> picList = new ArrayList<>();
                picList.add(FlatsDetailsActivity.this.photo.getP_url());
                intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, picList);
                FlatsDetailsActivity.this.startActivity(intent);
            }
        });
    }
}
