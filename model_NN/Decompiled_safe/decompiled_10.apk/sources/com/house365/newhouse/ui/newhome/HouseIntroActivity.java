package com.house365.newhouse.ui.newhome;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.House;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;

public class HouseIntroActivity extends BaseCommonActivity {
    private TextView group_title;
    private TextView h_intro;
    private HeadNavigateView head_view;
    private House house;
    private String house_info;
    private String house_intro;
    private String house_title;
    private TextView utl_tv;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_intro);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseIntroActivity.this.finish();
            }
        });
        this.h_intro = (TextView) findViewById(R.id.h_intro);
        this.utl_tv = (TextView) findViewById(R.id.utl_tv);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.house = (House) getIntent().getSerializableExtra("house");
        this.house_title = getIntent().getStringExtra(LoanCalSelProActivity.INTENT_TITLE);
        this.house_info = getIntent().getStringExtra("info");
        if (this.house_title != null) {
            this.head_view.setTvTitleText(this.house_title);
        } else {
            this.head_view.setTvTitleText((int) R.string.text_field_project_intro);
        }
        if (this.house_info != null) {
            this.h_intro.setText(Html.fromHtml(this.house_info));
        } else {
            this.h_intro.setText(Html.fromHtml(this.house.getH_intro()));
        }
    }
}
