package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.util.TimeUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.ui.util.TelUtil;

public class ContactsHouseListAdapter extends BaseCacheListAdapter<HouseInfo> {
    private String app;
    /* access modifiers changed from: private */
    public NewHouseApplication application;

    public ContactsHouseListAdapter(Context context, String app2, NewHouseApplication application2) {
        super(context);
        this.app = app2;
        this.application = application2;
    }

    private class ViewHolder {
        View btn_contacts_call;
        TextView contact_sale_state;
        TextView txt_block;
        TextView txt_contacts_time;
        TextView txt_name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ContactsHouseListAdapter contactsHouseListAdapter, ViewHolder viewHolder) {
            this();
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.item_list_house_contacts, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.btn_contacts_call = convertView.findViewById(R.id.btn_contacts_call);
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_title);
            holder.txt_block = (TextView) convertView.findViewById(R.id.txt_block);
            holder.txt_contacts_time = (TextView) convertView.findViewById(R.id.txt_contacts_time);
            holder.contact_sale_state = (TextView) convertView.findViewById(R.id.contact_sale_state);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        HouseInfo houseInfo = (HouseInfo) getItem(position);
        String type = houseInfo.getType();
        if (type.equals("house")) {
            final House house = houseInfo.getNewHouse();
            holder.contact_sale_state.setText(house.getH_salestat_str());
            if (house.getH_salestat_str() != null) {
                String salestae = house.getH_salestat_str();
                holder.contact_sale_state.setVisibility(0);
                if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_sall_out))) {
                    holder.contact_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_house_sale_out));
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_gray);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_new))) {
                    holder.contact_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_sall_in));
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_orange);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_selling))) {
                    holder.contact_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_sale_house));
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_land))) {
                    holder.contact_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_real_house));
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_blue);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_last))) {
                    holder.contact_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_last));
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_red);
                } else {
                    holder.contact_sale_state.setVisibility(8);
                }
            } else {
                holder.contact_sale_state.setVisibility(8);
            }
            holder.txt_name.setText(house.getH_name());
            holder.txt_block.setVisibility(8);
            if (house.getH_tel() != null) {
                holder.btn_contacts_call.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            TelUtil.getCallIntent(house.getH_tel(), ContactsHouseListAdapter.this.context, "house");
                            ContactsHouseListAdapter.this.application.saveCallHistory(new HouseInfo("house", house), "house");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            holder.txt_contacts_time.setText(this.context.getResources().getString(R.string.text_contacts_time, TimeUtil.toDateAndTime(houseInfo.getOptime())));
        }
        if (type.equals(App.RENT) || type.equals(App.SELL)) {
            final SecondHouse house2 = houseInfo.getSellOrRent();
            if (house2.getTitle().length() > 10) {
                holder.txt_name.setText(house2.getTitle());
                holder.txt_name.setSingleLine();
                holder.txt_name.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
            } else {
                holder.txt_name.setText(house2.getTitle());
            }
            if (house2.getBlockinfo() != null) {
                holder.txt_block.setText(house2.getBlockinfo().getBlockname());
            }
            if (type.equals(App.RENT)) {
                if (house2.getBrokerinfo() != null) {
                    holder.btn_contacts_call.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            try {
                                TelUtil.getCallIntent(house2.getBrokerinfo().getTelno(), ContactsHouseListAdapter.this.context, App.RENT);
                                ContactsHouseListAdapter.this.application.saveCallHistory(new HouseInfo(App.RENT, house2), App.RENT);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else if (type.equals(App.SELL) && house2.getBrokerinfo() != null) {
                holder.btn_contacts_call.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        try {
                            TelUtil.getCallIntent(house2.getBrokerinfo().getTelno(), ContactsHouseListAdapter.this.context, App.SELL);
                            ContactsHouseListAdapter.this.application.saveCallHistory(new HouseInfo(App.SELL, house2), App.SELL);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if (type.equals(App.RENT)) {
                if (house2.getState().equals("1")) {
                    holder.contact_sale_state.setText((int) R.string.text_Urgent_Sale);
                    holder.contact_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else if (house2.getState().equals("2")) {
                    holder.contact_sale_state.setText((int) R.string.text_real_house);
                    holder.contact_sale_state.setBackgroundResource(R.drawable.bg_real_house);
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_blue);
                } else if (house2.getState().equals("3")) {
                    holder.contact_sale_state.setText((int) R.string.text_house_Sale);
                    holder.contact_sale_state.setBackgroundResource(R.drawable.bg_newhouse_sall_in);
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_orange);
                } else {
                    holder.contact_sale_state.setVisibility(8);
                }
            } else if (type.equals(App.SELL)) {
                if (house2.getState().equals("1")) {
                    holder.contact_sale_state.setText((int) R.string.text_Urgent_rent);
                    holder.contact_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    holder.contact_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else {
                    holder.contact_sale_state.setVisibility(8);
                }
            }
            holder.txt_contacts_time.setText(this.context.getResources().getString(R.string.text_contacts_time, TimeUtil.toDateAndTime(houseInfo.getOptime())));
        }
        return convertView;
    }
}
