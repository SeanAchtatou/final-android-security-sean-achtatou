package com.house365.newhouse.ui.tools;

/* compiled from: LoanCalResultMethodTwo */
class AdapterDataList {
    private int loantime;
    private double monthRepayAll;
    private double monthRepayBusiness;
    private double monthRepayFound;

    AdapterDataList() {
    }

    public double getMonthRepayBusiness() {
        return this.monthRepayBusiness;
    }

    public void setMonthRepayBusiness(double monthRepayBusiness2) {
        this.monthRepayBusiness = monthRepayBusiness2;
    }

    public double getMonthRepayFound() {
        return this.monthRepayFound;
    }

    public void setMonthRepayFound(double monthRepayFound2) {
        this.monthRepayFound = monthRepayFound2;
    }

    public double getMonthRepayAll() {
        return this.monthRepayAll;
    }

    public void setMonthRepayAll(double monthRepayAll2) {
        this.monthRepayAll = monthRepayAll2;
    }

    public int getLoantime() {
        return this.loantime;
    }

    public void setLoantime(int loantime2) {
        this.loantime = loantime2;
    }
}
