package com.house365.newhouse.ui.newhome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Album;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.newhome.adapter.NewHouseFlatsAdapter;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlatsListActivity extends BaseCommonActivity {
    public static final String INTENT_HIE = "h_id";
    /* access modifiers changed from: private */
    public NewHouseFlatsAdapter flats_adapter;
    /* access modifiers changed from: private */
    public PullToRefreshListView flats_listView;
    /* access modifiers changed from: private */
    public String h_id;
    private HeadNavigateView head_view;
    protected RefreshInfo listRefresh = new RefreshInfo();
    List<Photo> photo_list = new ArrayList();
    String type;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.flats_list);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FlatsListActivity.this.finish();
            }
        });
        this.flats_listView = (PullToRefreshListView) findViewById(R.id.flats_genal_list);
        this.flats_adapter = new NewHouseFlatsAdapter(this);
        this.flats_listView.setAdapter(this.flats_adapter);
    }

    /* access modifiers changed from: protected */
    public void showMoreData() {
        int count = this.flats_adapter.getCount();
        if (count + 10 < this.photo_list.size()) {
            for (int i = count; i < count + 10; i++) {
                this.flats_adapter.addItem(this.photo_list.get(i));
            }
        } else {
            for (int i2 = count; i2 < this.photo_list.size(); i2++) {
                this.flats_adapter.addItem(this.photo_list.get(i2));
            }
        }
        this.flats_adapter.notifyDataSetChanged();
    }

    private void initFlatAdapter() {
        if (this.photo_list.size() < 10) {
            this.flats_adapter.addAll(this.photo_list);
        } else {
            for (int i = 0; i < 10; i++) {
                this.flats_adapter.addItem(this.photo_list.get(i));
            }
        }
        this.flats_adapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.h_id = getIntent().getStringExtra("h_id");
        this.type = getIntent().getStringExtra(CouponDetailsActivity.INTENT_TYPE);
        String title_info = getIntent().getStringExtra(LoanCalSelProActivity.INTENT_TITLE);
        if (title_info == null || TextUtils.isEmpty(title_info)) {
            this.head_view.setTvTitleText((int) R.string.text_house_flasts_info_title);
        } else {
            this.head_view.setTvTitleText(title_info);
        }
        new GetHousePhotosList(this, R.string.loading).execute(new Object[0]);
        this.flats_listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                new GetHousePhotosList(FlatsListActivity.this, 0).execute(new Object[0]);
            }

            public void onFooterRefresh() {
                FlatsListActivity.this.flats_listView.onRefreshComplete();
            }
        });
        if (((NewHouseApplication) this.mApplication).isEnableImg()) {
            this.flats_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    Intent intent = new Intent();
                    Class<?> clazz = null;
                    TextView title = (TextView) view.findViewById(R.id.text_flats_num_one);
                    if (FlatsListActivity.this.type == null) {
                        intent.putExtra("flats_title", title.getText().toString());
                        intent.putExtra("photo", (Serializable) FlatsListActivity.this.flats_adapter.getItem(position));
                        clazz = FlatsDetailsActivity.class;
                    }
                    intent.setClass(FlatsListActivity.this, clazz);
                    FlatsListActivity.this.startActivity(intent);
                }
            });
        }
    }

    private class GetHousePhotosList extends CommonAsyncTask<List<Photo>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Photo>) ((List) obj));
        }

        public GetHousePhotosList(Context context, int resid) {
            super(context, resid);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        public void onAfterDoInBackgroup(List<Photo> r) {
            if (FlatsListActivity.this.listRefresh != null && FlatsListActivity.this.flats_listView != null) {
                ViewUtil.onListDataComplete(this.context, r, FlatsListActivity.this.listRefresh, FlatsListActivity.this.flats_adapter, FlatsListActivity.this.flats_listView);
            }
        }

        public List<Photo> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            if (AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity())) {
                FlatsListActivity.this.photo_list = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getVirtualHousePhotoById(FlatsListActivity.this.h_id, "hx");
            } else {
                List<Album> albums = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHousePhotoById(FlatsListActivity.this.h_id, "hx");
                if (!(albums == null || albums.size() <= 0 || albums.get(0) == null)) {
                    FlatsListActivity.this.photo_list = albums.get(0).getA_photos();
                }
            }
            return FlatsListActivity.this.photo_list;
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            if (FlatsListActivity.this.listRefresh.refresh) {
                FlatsListActivity.this.flats_listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
            } else {
                FlatsListActivity.this.flats_listView.onRefreshComplete();
            }
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            if (FlatsListActivity.this.listRefresh.refresh) {
                FlatsListActivity.this.flats_listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
            } else {
                FlatsListActivity.this.flats_listView.onRefreshComplete();
            }
        }
    }
}
