package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.lbs.MapUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.SecondHouse;

public class HouseListAdapter extends BaseCacheListAdapter<SecondHouse> {
    private String app;
    private boolean from_nearby;
    private Location location;

    public HouseListAdapter(Context context, String app2) {
        super(context);
        this.app = app2;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public boolean isFrom_nearby() {
        return this.from_nearby;
    }

    public void setFrom_nearby(boolean from_nearby2) {
        this.from_nearby = from_nearby2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.item_list_secondsellhouse, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.house_pic = (ImageView) convertView.findViewById(R.id.house_pic);
            holder.house_rent_pic = (ImageView) convertView.findViewById(R.id.house_rent_pic);
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_title);
            holder.txt_block = (TextView) convertView.findViewById(R.id.txt_block);
            holder.text_price = (TextView) convertView.findViewById(R.id.txt_price);
            holder.text_rent_price = (TextView) convertView.findViewById(R.id.txt_rent_price);
            holder.sell_txt_house_info = (TextView) convertView.findViewById(R.id.sell_txt_housetype);
            holder.text_street = (TextView) convertView.findViewById(R.id.txt_block_street);
            holder.rent_txt_house_info = (TextView) convertView.findViewById(R.id.rent_txt_housetype);
            holder.text_rentty = (TextView) convertView.findViewById(R.id.txt_renttype);
            holder.rent_txt_housearea = (TextView) convertView.findViewById(R.id.rent_txt_housearea);
            holder.sell_sale_state = (TextView) convertView.findViewById(R.id.sell_sale_state);
            holder.rent_sale_state = (TextView) convertView.findViewById(R.id.rent_sale_state);
            holder.h_near_distance = (TextView) convertView.findViewById(R.id.h_near_distance);
            holder.sell_layout = convertView.findViewById(R.id.sell_layout);
            holder.rent_layout = convertView.findViewById(R.id.rent_layout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SecondHouse house = (SecondHouse) getItem(position);
        Block block = house.getBlockinfo();
        if (!isFrom_nearby() || this.location == null || block == null || block.getLat() == 0.0d || block.getLng() == 0.0d) {
            holder.h_near_distance.setVisibility(8);
        } else {
            holder.h_near_distance.setText(MapUtil.getDistance(this.location, block.getLat(), block.getLng()));
            holder.h_near_distance.setVisibility(0);
        }
        holder.text_street.setText(house.getStreetname());
        if (holder.txt_name.length() > 10) {
            holder.txt_name.setText(house.getTitle());
            holder.txt_name.setSingleLine();
            holder.txt_name.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
        } else {
            holder.txt_name.setText(house.getTitle());
        }
        if (house.getBlockinfo() != null) {
            holder.txt_block.setVisibility(0);
            holder.txt_block.setText(house.getBlockinfo().getBlockname());
        } else {
            holder.txt_block.setVisibility(8);
        }
        if (this.app != null) {
            if (this.app.equals(App.SELL)) {
                holder.house_rent_pic.setVisibility(8);
                holder.house_pic.setVisibility(0);
                setCacheImage(holder.house_pic, house.getPic(), R.drawable.img_default_small, 1);
                holder.sell_sale_state.setVisibility(0);
                holder.rent_sale_state.setVisibility(8);
                holder.sell_layout.setVisibility(0);
                holder.rent_layout.setVisibility(8);
                holder.text_rentty.setVisibility(8);
                holder.text_rent_price.setVisibility(8);
                holder.text_price.setVisibility(0);
                holder.text_price.setText(this.context.getResources().getString(R.string.house_price, house.getPrice()));
                holder.sell_txt_house_info.setText(this.context.getResources().getString(R.string.house_info, Integer.valueOf(house.getRoom()), Integer.valueOf(house.getHall()), house.getBuildarea()));
                CorePreferences.DEBUG("sell_house.getState()" + house.getState());
                if (house.getState().equals("1")) {
                    holder.sell_sale_state.setText((int) R.string.text_Urgent_Sale);
                    holder.sell_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    holder.sell_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else if (house.getState().equals("2")) {
                    holder.sell_sale_state.setText((int) R.string.text_real_house);
                    holder.sell_sale_state.setBackgroundResource(R.drawable.bg_real_house);
                    holder.sell_sale_state.setTextAppearance(this.context, R.style.font12_pop_blue);
                } else if (house.getState().equals("3")) {
                    holder.sell_sale_state.setText((int) R.string.text_house_Sale);
                    holder.sell_sale_state.setBackgroundResource(R.drawable.bg_newhouse_sall_in);
                    holder.sell_sale_state.setTextAppearance(this.context, R.style.font12_pop_orange);
                } else {
                    holder.sell_sale_state.setVisibility(8);
                }
            } else {
                holder.house_rent_pic.setVisibility(0);
                holder.house_pic.setVisibility(8);
                setCacheImage(holder.house_rent_pic, house.getPic(), R.drawable.img_default_small, 1);
                holder.sell_sale_state.setVisibility(8);
                holder.rent_sale_state.setVisibility(0);
                holder.rent_layout.setVisibility(0);
                holder.sell_layout.setVisibility(8);
                holder.text_rentty.setVisibility(0);
                holder.text_price.setVisibility(8);
                holder.text_rent_price.setVisibility(0);
                holder.text_rent_price.setText(house.getPrice());
                holder.rent_txt_house_info.setText(this.context.getResources().getString(R.string.house_info, Integer.valueOf(house.getRoom()), Integer.valueOf(house.getHall()), house.getBuildarea()));
                holder.text_rentty.setText(house.getRenttype());
                holder.rent_txt_housearea.setVisibility(8);
                if (house.getState().equals("1")) {
                    holder.rent_sale_state.setText((int) R.string.text_Urgent_rent);
                    holder.rent_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
                    holder.rent_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else {
                    holder.rent_sale_state.setVisibility(8);
                }
            }
        }
        return convertView;
    }

    private class ViewHolder {
        TextView h_near_distance;
        ImageView house_pic;
        ImageView house_rent_pic;
        View rent_layout;
        TextView rent_sale_state;
        TextView rent_txt_house_info;
        TextView rent_txt_housearea;
        View sell_layout;
        TextView sell_sale_state;
        TextView sell_txt_house_info;
        TextView text_price;
        TextView text_rent_price;
        TextView text_rentty;
        TextView text_street;
        TextView txt_block;
        TextView txt_name;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(HouseListAdapter houseListAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
