package com.house365.newhouse.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import com.house365.core.action.ActionTag;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.lbs.MyLocation;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.ui.user.UserGuideActivity;
import com.house365.newhouse.ui.util.PushServiceUtil;
import org.apache.commons.lang.StringUtils;

public class SplashActivity extends BaseCommonActivity {
    private HomeKeyEventBroadCastReceiver homePress = new HomeKeyEventBroadCastReceiver();
    /* access modifiers changed from: private */
    public int loadtime = 3000;
    /* access modifiers changed from: private */
    public MyLocation myLocation;
    private NewHouseApplication newHouseApplication;
    private Handler startIntentHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (!SplashActivity.this.isFinishing()) {
                if (msg.what == 1) {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MenuActivity.class));
                } else {
                    SplashActivity.this.mApplication.setIsFirst(false);
                    SplashActivity.this.mApplication.setIsFirstVersion(false);
                    Intent intent = new Intent(SplashActivity.this, UserGuideActivity.class);
                    intent.putExtra("isFirst", 1);
                    SplashActivity.this.startActivity(intent);
                }
                SplashActivity.this.finish();
            }
        }
    };

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.splash);
        this.newHouseApplication = (NewHouseApplication) this.mApplication;
        registerReceiver(this.homePress, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        this.myLocation = new MyLocation(this);
        sendBroadcast(new Intent("com.house365.newhouse.startup"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.homePress);
    }

    /* access modifiers changed from: protected */
    public void initView() {
    }

    private void startPushService() {
        PushServiceUtil pushServiceUtil = new PushServiceUtil();
        this.newHouseApplication.removePullServiceManager();
        pushServiceUtil.startPNService(this, (NewHouseApplication) this.mApplication, 1, null, App.PULL_ACCOUNT_PASSWORD);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!DeviceUtil.isNetConnect(this)) {
            if (this.mApplication.isFirst()) {
                ((NewHouseApplication) this.mApplication).setCity(AppArrays.getCity(CorePreferences.getInstance(this).getCoreConfig().getDefaultCity()));
            }
            showToast((int) R.string.text_no_network);
            startPlush(System.currentTimeMillis());
            return;
        }
        new StartTask(this, null).execute(new Void[0]);
        CorePreferences.DEBUG("over");
    }

    private class StartTask extends AsyncTask<Void, Void, Void> {
        private long startTime;

        private StartTask() {
        }

        /* synthetic */ StartTask(SplashActivity splashActivity, StartTask startTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.startTime = System.currentTimeMillis();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            String def_city;
            if (!SplashActivity.this.mApplication.isFirst()) {
                SplashActivity.this.mApplication.setLocation(SplashActivity.this.myLocation.getLocationByBaiduLocApi(true, (long) SplashActivity.this.loadtime));
                return null;
            } else if (!CorePreferences.getInstance(SplashActivity.this).getCoreConfig().isDefaultCityForce()) {
                try {
                    CommonResultInfo result = ((HttpApi) ((NewHouseApplication) SplashActivity.this.mApplication).getApi()).getDefaultCity();
                    if (StringUtils.EMPTY == 0 || result.getResult() == 0) {
                        def_city = CorePreferences.getInstance(SplashActivity.this).getCoreConfig().getDefaultCity();
                        ((NewHouseApplication) SplashActivity.this.mApplication).setCity(AppArrays.getCity(def_city));
                        return null;
                    }
                    def_city = result.getData();
                    ((NewHouseApplication) SplashActivity.this.mApplication).setCity(AppArrays.getCity(def_city));
                    return null;
                } catch (Exception e1) {
                    def_city = CorePreferences.getInstance(SplashActivity.this).getCoreConfig().getDefaultCity();
                    CorePreferences.ERROR(e1);
                }
            } else {
                ((NewHouseApplication) SplashActivity.this.mApplication).setCity(AppArrays.getCity(CorePreferences.getInstance(SplashActivity.this).getCoreConfig().getDefaultCity()));
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            SplashActivity.this.startPlush(this.startTime);
        }
    }

    /* access modifiers changed from: private */
    public void startPlush(long startTime) {
        if (this.mApplication.isFirst() || this.mApplication.isFirstVersion()) {
            this.newHouseApplication.enablePushNotification(true);
            startPushService();
            this.startIntentHandler.sendEmptyMessage(2);
            return;
        }
        if (this.newHouseApplication.isEnablePushNotification()) {
            startPushService();
        }
        long range = System.currentTimeMillis() - startTime;
        if (range < ((long) this.loadtime)) {
            this.startIntentHandler.sendEmptyMessageDelayed(1, ((long) this.loadtime) - range);
        } else {
            this.startIntentHandler.sendEmptyMessage(1);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        exitDialog(this, R.string.app_title);
        return false;
    }

    public void exitDialog(final Context context, int appid) {
        if (!isFinishing() && this.mApplication.isFirst()) {
            new AlertDialog.Builder(context).setTitle(appid).setMessage((int) R.string.confirm_exit_info).setPositiveButton((int) R.string.dialog_button_exit, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SplashActivity.this.mApplication.setIsFirst(false);
                    dialog.dismiss();
                    context.sendBroadcast(new Intent(ActionTag.INTENT_ACTION_LOGGED_OUT));
                }
            }).setNegativeButton((int) R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
        }
    }

    class HomeKeyEventBroadCastReceiver extends BroadcastReceiver {
        static final String SYSTEM_HOME_KEY = "homekey";
        static final String SYSTEM_REASON = "reason";
        static final String SYSTEM_RECENT_APPS = "recentapps";

        HomeKeyEventBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String reason;
            if (intent.getAction().equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && (reason = intent.getStringExtra(SYSTEM_REASON)) != null) {
                if (reason.equals(SYSTEM_HOME_KEY)) {
                    SplashActivity.this.mApplication.setIsFirst(false);
                    ((Activity) context).finish();
                } else if (reason.equals(SYSTEM_RECENT_APPS)) {
                    SplashActivity.this.mApplication.setIsFirst(false);
                    ((Activity) context).finish();
                }
            }
        }
    }
}
