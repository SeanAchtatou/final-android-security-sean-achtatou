package com.house365.newhouse.ui.user.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.SecondHouse;

public class FavHistoryListAdapter extends BaseCacheListAdapter<HouseInfo> {
    private int apply_type;
    private LayoutInflater inflater;
    private Context mContext;
    private String text_house_price_m_unit = this.mContext.getResources().getString(R.string.text_house_price_m_unit);
    private String text_house_price_square_unit = this.mContext.getResources().getString(R.string.text_house_price_square_unit);
    private String text_house_price_unit = this.mContext.getResources().getString(R.string.text_house_price_unit);
    private String text_pub_no_price = this.mContext.getResources().getString(R.string.text_pub_no_price);

    public FavHistoryListAdapter(Context context) {
        super(context);
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
    }

    public int getApply_type() {
        return this.apply_type;
    }

    public void setApply_type(int apply_type2) {
        this.apply_type = apply_type2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_list_more_fav_history, (ViewGroup) null);
            holder = new ViewHolder(null);
            holder.h_name = (TextView) convertView.findViewById(R.id.h_name);
            holder.h_pic = (ImageView) convertView.findViewById(R.id.h_pic);
            holder.h_price = (TextView) convertView.findViewById(R.id.h_price);
            holder.h_add_or_block_name = (TextView) convertView.findViewById(R.id.h_add_or_block_name);
            holder.sell_add = (TextView) convertView.findViewById(R.id.sell_add);
            holder.rent_room_area_layout = (LinearLayout) convertView.findViewById(R.id.rent_room_area_layout);
            holder.sell_room_area_layout = (LinearLayout) convertView.findViewById(R.id.sell_room_area_layout);
            holder.second_layout = (RelativeLayout) convertView.findViewById(R.id.second_layout);
            holder.rent_room_area = (TextView) convertView.findViewById(R.id.rent_room_area);
            holder.sell_room_area = (TextView) convertView.findViewById(R.id.sell_room_area);
            holder.second_sale_state = (TextView) convertView.findViewById(R.id.second_sale_state);
            holder.h_sale_state = (TextView) convertView.findViewById(R.id.h_sale_state);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        HouseInfo houseInfo = (HouseInfo) getItem(position);
        String type = houseInfo.getType();
        String pic_str = null;
        if (type.equals("house")) {
            holder.h_add_or_block_name.setVisibility(0);
            holder.h_sale_state.setVisibility(0);
            holder.second_layout.setVisibility(8);
            holder.second_sale_state.setVisibility(8);
            holder.sell_room_area_layout.setVisibility(8);
            holder.rent_room_area_layout.setVisibility(8);
            holder.sell_add.setVisibility(8);
            House house = houseInfo.getNewHouse();
            pic_str = house.getH_pic();
            holder.h_name.setText(house.getH_name());
            holder.h_add_or_block_name.setText(house.getH_project_address());
            if (house.getH_price() == null || TextUtils.isEmpty(house.getH_price()) || house.getH_price().indexOf(this.text_pub_no_price) != -1) {
                holder.h_price.setText((int) R.string.text_pub_no_price);
            } else if (house.getH_price().indexOf(this.text_house_price_unit) != -1) {
                holder.h_price.setText(house.getH_price());
            } else {
                holder.h_price.setText(String.valueOf(house.getH_price().split(this.text_house_price_m_unit)[0]) + this.text_house_price_square_unit);
            }
            holder.h_sale_state.setText(house.getH_salestat_str());
            if (house.getH_salestat_str() != null) {
                String salestae = house.getH_salestat_str();
                if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_sall_out))) {
                    holder.h_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_house_sale_out));
                    holder.h_sale_state.setTextAppearance(this.context, R.style.font12_gray);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_new))) {
                    holder.h_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_sall_in));
                    holder.h_sale_state.setTextAppearance(this.context, R.style.font12_pop_orange);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_selling))) {
                    holder.h_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_sale_house));
                    holder.h_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_land))) {
                    holder.h_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_real_house));
                    holder.h_sale_state.setTextAppearance(this.context, R.style.font12_pop_blue);
                } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_last))) {
                    holder.h_sale_state.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_last));
                    holder.h_sale_state.setTextAppearance(this.context, R.style.font12_pop_red);
                } else {
                    holder.h_sale_state.setVisibility(8);
                }
            } else {
                holder.h_sale_state.setVisibility(8);
            }
        } else if (type.equals(App.RENT) || type.equals(App.SELL)) {
            SecondHouse secondHouse = houseInfo.getSellOrRent();
            pic_str = secondHouse.getPic();
            if (holder.h_name.length() > 10) {
                holder.h_name.setText(secondHouse.getTitle());
                holder.h_name.setSingleLine();
                holder.h_name.setEllipsize(TextUtils.TruncateAt.valueOf("END"));
            } else {
                holder.h_name.setText(secondHouse.getTitle());
            }
            holder.h_name.setText(secondHouse.getTitle());
            holder.second_layout.setVisibility(0);
            holder.h_sale_state.setVisibility(8);
            holder.second_sale_state.setVisibility(0);
            holder.second_sale_state.setText(secondHouse.getState());
            if (secondHouse.getState().equals("1")) {
                holder.second_sale_state.setText((int) R.string.text_Urgent_Sale);
                holder.second_sale_state.setTextAppearance(this.context, R.style.font12_pop_green);
                holder.second_sale_state.setBackgroundResource(R.drawable.bg_sale_house);
            } else if (secondHouse.getState().equals("2")) {
                holder.second_sale_state.setText((int) R.string.text_real_house);
                holder.second_sale_state.setTextAppearance(this.context, R.style.font12_pop_blue);
                holder.second_sale_state.setBackgroundResource(R.drawable.bg_real_house);
            } else if (secondHouse.getState().equals("3")) {
                holder.second_sale_state.setText((int) R.string.text_house_Sale);
                holder.second_sale_state.setTextAppearance(this.context, R.style.font12_pop_orange);
                holder.second_sale_state.setBackgroundResource(R.drawable.bg_newhouse_sall_in);
            } else {
                holder.second_sale_state.setVisibility(8);
            }
            if (type.equals(App.SELL)) {
                holder.h_add_or_block_name.setVisibility(8);
                holder.rent_room_area_layout.setVisibility(8);
                holder.sell_add.setVisibility(0);
                if (secondHouse.getBlockinfo().getBlockname() == null) {
                    holder.sell_add.setText(secondHouse.getStreetname());
                } else {
                    holder.sell_add.setText(String.valueOf(secondHouse.getBlockinfo().getBlockname()) + "  " + secondHouse.getStreetname());
                }
                holder.sell_room_area_layout.setVisibility(0);
                holder.sell_room_area.setText(this.context.getResources().getString(R.string.house_info, Integer.valueOf(secondHouse.getRoom()), Integer.valueOf(secondHouse.getHall()), secondHouse.getBuildarea()));
                holder.h_price.setText(this.context.getResources().getString(R.string.house_price, secondHouse.getPrice()));
            } else {
                holder.sell_room_area_layout.setVisibility(8);
                holder.h_add_or_block_name.setVisibility(0);
                holder.rent_room_area_layout.setVisibility(0);
                holder.sell_add.setVisibility(8);
                holder.h_add_or_block_name.setText(secondHouse.getBlockinfo().getBlockname());
                holder.h_add_or_block_name.setTextSize(14.0f);
                holder.rent_room_area.setText(this.context.getResources().getString(R.string.house_info, Integer.valueOf(secondHouse.getRoom()), Integer.valueOf(secondHouse.getHall()), "  " + secondHouse.getBuildarea()));
                holder.h_price.setText(String.valueOf(secondHouse.getRenttype()) + " : " + secondHouse.getPrice());
            }
        }
        setCacheImage(holder.h_pic, pic_str, R.drawable.bg_default_img_photo, 1);
        return convertView;
    }

    private static class ViewHolder {
        TextView h_add_or_block_name;
        TextView h_name;
        ImageView h_pic;
        TextView h_price;
        TextView h_sale_state;
        TextView rent_room_area;
        LinearLayout rent_room_area_layout;
        RelativeLayout second_layout;
        TextView second_sale_state;
        TextView sell_add;
        TextView sell_room_area;
        LinearLayout sell_room_area_layout;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
