package com.house365.newhouse.ui.newhome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.IntentUtil;
import com.house365.core.util.TextUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.map.baidu.MyPositionOverlay;
import com.house365.core.view.NoScrollListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.model.SaleInfo;
import com.house365.newhouse.task.FavTask;
import com.house365.newhouse.task.GroupTask;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAdapter;
import com.house365.newhouse.ui.newhome.adapter.NewHouseFlatsAdapter;
import com.house365.newhouse.ui.privilege.BargainlHouseActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.CubeHouseRoomActivity;
import com.house365.newhouse.ui.privilege.EventListActivity;
import com.house365.newhouse.ui.privilege.PrivilegeHomeActivity;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;
import com.house365.newhouse.ui.user.UserInfoActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.FavUtil;
import com.house365.newhouse.ui.util.TelUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseDetailActivity extends BaseBaiduMapActivity {
    private static final int FIXED_BTN = 1;
    public static final String INTENT_CHANNEL = "channel";
    public static final String INTENT_DISAPPER_EVENT = "disapper_event";
    public static final String INTENT_HID = "h_id";
    public static final String INTENT_OUTLINE_HOUSE = "outline_house";
    private static final int REQUET_FAV = 4;
    private static final int REQUET_LOG_CODE = 1;
    private static final int REQUET_LOG_FEED = 3;
    boolean apply_dialog_show;
    private TextView btn_apply;
    private TextView btn_bargain;
    private TextView btn_coupon;
    private TextView btn_cubehouse;
    private TextView btn_event;
    /* access modifiers changed from: private */
    public ImageView btn_focus;
    private RelativeLayout btn_house_base_layout;
    private RelativeLayout btn_house_traffic_layout;
    private Button btn_left;
    private RelativeLayout btn_map_layout;
    private TextView btn_refer;
    /* access modifiers changed from: private */
    public String channel;
    List<Event> couponHouseList = new ArrayList();
    /* access modifiers changed from: private */
    public House curHouse;
    private Animation detail_do_am;
    private LinearLayout detail_info_map;
    private ScrollView detail_info_scroll;
    private boolean disapper_event;
    List<SaleInfo> dynamic_saleInfos = new ArrayList();
    List<Event> eventHouseList = new ArrayList();
    private LinearLayout event_list_layout;
    /* access modifiers changed from: private */
    public NewHouseFlatsAdapter flatsAdapter;
    private NoScrollListView flats_listview;
    List<Photo> flats_photos;
    private TextView h_build_type;
    private TextView h_deli_date;
    private TextView h_deli_standard;
    private TextView h_developers;
    private TextView h_dynamic_dateline;
    /* access modifiers changed from: private */
    public TextView h_dynamic_momo;
    /* access modifiers changed from: private */
    public String h_id;
    private TextView h_intro;
    private ImageView h_pic;
    private TextView h_project_address;
    List<HouseInfo> houseInfos = new ArrayList();
    private String house_build_type;
    private String house_decoration_standard;
    private String house_deli_date;
    private RelativeLayout house_detail_dynamic_layout;
    private RelativeLayout house_dynamic_layout;
    private View house_flats_one;
    /* access modifiers changed from: private */
    public String house_intro;
    private String house_intro_cut;
    private String house_kfs;
    private RelativeLayout house_pic_layout;
    private String house_project_address;
    private RelativeLayout house_project_intro_layout;
    private LinearLayout house_recommend_layout;
    /* access modifiers changed from: private */
    public String house_sale_phone;
    boolean isShowPic;
    /* access modifiers changed from: private */
    public boolean isVirtual;
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    private GeoPoint mGeoPoint;
    private MapController mapCont;
    private MapView mapView;
    private RelativeLayout more_flats_layout;
    private TextView more_h_pic;
    private RelativeLayout more_recommend_layout;
    private TextView no_dynamic_momo;
    private TextView no_flats_momo;
    /* access modifiers changed from: private */
    public House outlineHouse;
    WindowManager.LayoutParams params;
    private RelativeLayout project_address_layout;
    private Event randcoupon;
    /* access modifiers changed from: private */
    public NewHouseAdapter recommendAdapter;
    private NoScrollListView recommend_listview;
    private String saleInfo_momo;
    private TextView text_house_event;
    private TextView text_house_price_unit;
    private TextView text_house_sale_state;
    private TextView title;

    interface TaskFinish {
        void onFinish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == -1) {
                String truename = data.getStringExtra("truename");
                String mobile = data.getStringExtra("mobile");
                if (this.curHouse != null) {
                    new GroupTask(this, this.mApplication, this.h_id, "house", truename, mobile).execute(new Object[0]);
                }
            }
        } else if (requestCode == 4 && resultCode == -1 && this.curHouse != null) {
            new FavTask(this, this.mApplication, this.btn_focus, new HouseInfo("house", this.curHouse)).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.newhouse_detail);
        this.mApplication = (NewHouseApplication) getApplication();
        this.h_id = getIntent().getStringExtra("h_id");
        this.channel = getIntent().getStringExtra("channel");
        this.disapper_event = getIntent().getBooleanExtra(INTENT_DISAPPER_EVENT, false);
        this.outlineHouse = (House) getIntent().getSerializableExtra(INTENT_OUTLINE_HOUSE);
        this.flatsAdapter = new NewHouseFlatsAdapter(this);
        this.recommendAdapter = new NewHouseAdapter(this);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.title = (TextView) findViewById(R.id.title);
        this.btn_left = (Button) findViewById(R.id.top_left_btn);
        this.h_pic = (ImageView) findViewById(R.id.h_pic);
        this.more_h_pic = (TextView) findViewById(R.id.more_h_pic);
        this.text_house_event = (TextView) findViewById(R.id.text_house_event);
        this.text_house_price_unit = (TextView) findViewById(R.id.text_house_price_unit);
        this.text_house_sale_state = (TextView) findViewById(R.id.text_house_sale_state);
        this.h_deli_date = (TextView) findViewById(R.id.h_deli_date);
        this.h_deli_standard = (TextView) findViewById(R.id.h_deli_standard);
        this.h_build_type = (TextView) findViewById(R.id.h_build_type);
        this.h_developers = (TextView) findViewById(R.id.h_kfs);
        this.h_project_address = (TextView) findViewById(R.id.h_project_address);
        this.h_intro = (TextView) findViewById(R.id.h_intro);
        this.no_flats_momo = (TextView) findViewById(R.id.no_flats_momo);
        this.no_dynamic_momo = (TextView) findViewById(R.id.no_dynamic_momo);
        this.h_dynamic_momo = (TextView) findViewById(R.id.h_dynamic_momo);
        this.h_dynamic_dateline = (TextView) findViewById(R.id.h_dynamic_dateline);
        this.flats_listview = (NoScrollListView) findViewById(R.id.flats_listview);
        this.recommend_listview = (NoScrollListView) findViewById(R.id.recommend_listview);
        this.btn_apply = (TextView) findViewById(R.id.btn_apply);
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.btn_refer.setText(getString(R.string.contact_house_sell));
        this.btn_refer.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.phone, 0, 0, 0);
        this.btn_focus = (ImageView) findViewById(R.id.top_right_imagebtn);
        this.btn_apply.setText(getString(R.string.apply_privilege));
        this.btn_apply.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.apply, 0, 0, 0);
        this.btn_left.setVisibility(0);
        this.btn_focus.setVisibility(0);
        this.house_flats_one = findViewById(R.id.house_flats_one);
        this.isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (this.isVirtual) {
            this.house_flats_one.setVisibility(8);
        } else {
            this.house_flats_one.setVisibility(0);
        }
        this.event_list_layout = (LinearLayout) findViewById(R.id.event_list_layout);
        this.btn_event = (TextView) findViewById(R.id.btn_event);
        this.btn_cubehouse = (TextView) findViewById(R.id.btn_cubehouse);
        this.btn_bargain = (TextView) findViewById(R.id.btn_bargain);
        this.btn_coupon = (TextView) findViewById(R.id.btn_coupon);
        this.btn_left.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewHouseDetailActivity.this.finish();
            }
        });
        this.house_pic_layout = (RelativeLayout) findViewById(R.id.house_pic_layout);
        this.btn_house_base_layout = (RelativeLayout) findViewById(R.id.btn_house_base_layout);
        this.btn_house_base_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.curHouse != null) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, HouseBaseActivity.class);
                    intent.putExtra("house", NewHouseDetailActivity.this.curHouse);
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.house_detail_dynamic_layout = (RelativeLayout) findViewById(R.id.house_detail_dynamic_layout);
        this.house_dynamic_layout = (RelativeLayout) findViewById(R.id.house_dynamic_layout);
        this.house_dynamic_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!TextUtils.isEmpty(NewHouseDetailActivity.this.h_dynamic_momo.getText().toString())) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, HouseSaleListActivity.class);
                    intent.putExtra("id", NewHouseDetailActivity.this.curHouse.getH_id());
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.house_project_intro_layout = (RelativeLayout) findViewById(R.id.house_project_intro_layout);
        this.house_project_intro_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!TextUtils.isEmpty(NewHouseDetailActivity.this.house_intro)) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, HouseIntroActivity.class);
                    intent.putExtra("house", NewHouseDetailActivity.this.curHouse);
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.btn_house_traffic_layout = (RelativeLayout) findViewById(R.id.btn_house_traffic_layout);
        this.btn_house_traffic_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.curHouse != null) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, HouseTrafficActivity.class);
                    intent.putExtra("house", NewHouseDetailActivity.this.curHouse);
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.btn_map_layout = (RelativeLayout) findViewById(R.id.btn_map_layout);
        this.detail_info_scroll = (ScrollView) findViewById(R.id.detail_info_scroll);
        this.mapView = (MapView) findViewById(R.id.map_view);
        this.mapCont = this.mapView.getController();
        this.detail_info_map = (LinearLayout) findViewById(R.id.detail_info_map);
        this.project_address_layout = (RelativeLayout) findViewById(R.id.project_address_layout);
        this.detail_info_scroll.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                event.getAction();
                return false;
            }
        });
        this.flats_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (NewHouseDetailActivity.this.isShowPic) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, FlatsDetailsActivity.class);
                    intent.putExtra("photo", (Serializable) NewHouseDetailActivity.this.flatsAdapter.getItem(position));
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.more_flats_layout = (RelativeLayout) findViewById(R.id.more_flats_layout);
        this.more_flats_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.flats_photos != null && NewHouseDetailActivity.this.flats_photos.size() > 0) {
                    Intent intent = new Intent(NewHouseDetailActivity.this, FlatsListActivity.class);
                    intent.putExtra("h_id", NewHouseDetailActivity.this.curHouse.getH_id());
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.recommend_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(NewHouseDetailActivity.this, NewHouseDetailActivity.class);
                intent.putExtra("h_id", ((House) NewHouseDetailActivity.this.recommendAdapter.getItem(position)).getH_id());
                NewHouseDetailActivity.this.startActivity(intent);
            }
        });
        this.house_recommend_layout = (LinearLayout) findViewById(R.id.house_recommend_layout);
        this.more_recommend_layout = (RelativeLayout) findViewById(R.id.more_recommend_layout);
        this.more_recommend_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewHouseDetailActivity.this, SalerOrRecommListActivity.class);
                intent.putExtra("h_dist", NewHouseDetailActivity.this.curHouse.getH_dist());
                intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, NewHouseDetailActivity.this.getResources().getString(R.string.text_house_recommend_title));
                NewHouseDetailActivity.this.startActivity(intent);
            }
        });
        this.isShowPic = this.mApplication.isEnableImg();
        if (!this.isShowPic) {
            this.house_pic_layout.setEnabled(false);
        }
        this.house_dynamic_layout.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        new GetHouseDetail(this).execute(new Object[0]);
        this.apply_dialog_show = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.curHouse != null) {
            markerHouse(this.curHouse);
            FavUtil.fixFavTab(this.mApplication, this.btn_focus, new HouseInfo("house", this.curHouse));
        }
    }

    /* access modifiers changed from: private */
    public void ensureUi(House house) {
        if (this.outlineHouse != null) {
            house = this.outlineHouse;
        }
        this.curHouse = house;
        this.detail_info_scroll.smoothScrollTo(0, 0);
        this.house_sale_phone = this.curHouse.getH_tel();
        if (this.house_sale_phone == null || TextUtils.isEmpty(this.house_sale_phone)) {
            this.house_sale_phone = AppArrays.default_tel;
        }
        this.mGeoPoint = new GeoPoint((int) (house.getH_lat() * 1000000.0d), (int) (house.getH_long() * 1000000.0d));
        new SimpleDateFormat("yyyy年MM月dd日");
        this.h_id = house.getH_id();
        this.btn_refer.setText(getString(R.string.contact_house_sell));
        this.house_kfs = getResources().getString(R.string.text_field_kfs, house.getH_kfs());
        this.house_decoration_standard = getResources().getString(R.string.text_field_decoration_states, house.getH_deli_standard());
        this.house_build_type = getResources().getString(R.string.text_field_build_type, house.getH_build_type());
        this.house_deli_date = getResources().getString(R.string.text_field_deli_date, house.getH_deli_date());
        this.title.setText(house.getH_name());
        if (house.getH_salestat_str() == null || TextUtils.isEmpty(house.getH_salestat_str())) {
            this.text_house_sale_state.setVisibility(8);
        } else {
            this.text_house_sale_state.setVisibility(0);
            this.text_house_sale_state.setText(house.getH_salestat_str());
            String salestae = house.getH_salestat_str();
            if (salestae.equals(getResources().getString(R.string.text_newhouse_sall_out))) {
                this.text_house_sale_state.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_house_sale_out));
                this.text_house_sale_state.setTextAppearance(this, R.style.font12_gray);
            } else if (salestae.equals(getResources().getString(R.string.text_newhouse_new))) {
                this.text_house_sale_state.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_newhouse_sall_in));
                this.text_house_sale_state.setTextAppearance(this, R.style.font12_pop_orange);
            } else if (salestae.equals(getResources().getString(R.string.text_newhouse_selling))) {
                this.text_house_sale_state.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_sale_house));
                this.text_house_sale_state.setTextAppearance(this, R.style.font12_pop_green);
            } else if (salestae.equals(getResources().getString(R.string.text_newhouse_land))) {
                this.text_house_sale_state.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_real_house));
                this.text_house_sale_state.setTextAppearance(this, R.style.font12_pop_blue);
            } else if (salestae.equals(getResources().getString(R.string.text_newhouse_last))) {
                this.text_house_sale_state.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_newhouse_last));
                this.text_house_sale_state.setTextAppearance(this, R.style.font12_pop_red);
            } else {
                this.text_house_sale_state.setVisibility(8);
            }
        }
        if (house.getH_price() == null || TextUtils.isEmpty(house.getH_price()) || house.getH_price().indexOf("待定") != -1) {
            this.text_house_price_unit.setText("参考均价：" + getResources().getString(R.string.text_pub_no_price));
        } else if (house.getH_price().indexOf("套") != -1) {
            this.text_house_price_unit.setText("参考均价：" + house.getH_price());
        } else {
            this.text_house_price_unit.setText("参考均价：" + house.getH_price().split("元/㎡")[0] + "元/平米");
        }
        TextUtil.setNullText(house.getH_deli_date(), this.house_deli_date, this.h_deli_date);
        TextUtil.setNullText(house.getH_deli_standard(), this.house_decoration_standard, this.h_deli_standard);
        TextUtil.setNullText(house.getH_build_type(), this.house_build_type, this.h_build_type);
        TextUtil.setNullText(house.getH_kfs(), this.house_kfs, this.h_developers);
        this.h_project_address.setText(house.getH_project_address());
        this.house_intro_cut = TextUtil.substring(house.getH_intro(), 145, "...");
        this.house_intro = house.getH_intro();
        if (this.house_intro == null) {
            this.house_project_intro_layout.setVisibility(8);
        }
        this.h_intro.setText(Html.fromHtml(this.house_intro_cut));
        String pic = house.getH_pic();
        this.h_pic.setVisibility(0);
        if (pic == null || !this.isShowPic) {
            setImage(this.h_pic, StringUtils.EMPTY, R.drawable.bg_default_ad, 1);
        } else {
            setImage(this.h_pic, pic, R.drawable.bg_default_ad, 1);
        }
        if (this.disapper_event || (house.getH_event() == null && house.getH_tlf() == null && house.getH_tjf() == null && house.getH_coupon() == null)) {
            this.event_list_layout.setVisibility(8);
        } else {
            this.event_list_layout.setVisibility(0);
        }
        setEventShow(house.getH_event(), this.btn_event, "event");
        setEventShow(house.getH_tlf(), this.btn_cubehouse, App.Categroy.Event.TLF);
        setEventShow(house.getH_tjf(), this.btn_bargain, App.Categroy.Event.TJF);
        setEventShow(house.getH_coupon(), this.btn_coupon, App.Categroy.Event.COUPON);
        if (house.getH_pic_num() > 1 && this.isShowPic) {
            this.more_h_pic.setVisibility(0);
            this.house_pic_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (AppMethods.isVirtual(NewHouseDetailActivity.this.mApplication.getCity())) {
                        new GetHouseAlbumList(NewHouseDetailActivity.this).execute(new Object[0]);
                        return;
                    }
                    Intent intent = new Intent(NewHouseDetailActivity.this, NewHouseAlbumActivity.class);
                    intent.putExtra("h_id", NewHouseDetailActivity.this.h_id);
                    NewHouseDetailActivity.this.startActivity(intent);
                }
            });
        }
        this.flats_photos = house.getH_rooms();
        this.flatsAdapter.clear();
        if (this.flats_photos == null || this.flats_photos.size() <= 0) {
            this.no_flats_momo.setVisibility(0);
            this.flats_listview.setVisibility(8);
        } else {
            this.no_flats_momo.setVisibility(8);
            this.flats_listview.setVisibility(0);
            if (this.flats_photos.size() < 3) {
                this.flatsAdapter.addAll(this.flats_photos);
                this.more_flats_layout.setEnabled(false);
            } else {
                this.flatsAdapter.addItem(this.flats_photos.get(0));
                this.flatsAdapter.addItem(this.flats_photos.get(1));
                this.more_flats_layout.setEnabled(true);
            }
            this.flatsAdapter.notifyDataSetChanged();
            this.flats_listview.setAdapter((ListAdapter) this.flatsAdapter);
        }
        List<SaleInfo> info_list = house.getH_news();
        if (info_list == null || info_list.size() <= 0) {
            this.no_dynamic_momo.setVisibility(0);
            this.house_dynamic_layout.setEnabled(false);
        } else {
            SaleInfo last_saleInfo = info_list.get(0);
            if (last_saleInfo == null || last_saleInfo.getSummary() == null || TextUtils.isEmpty(last_saleInfo.getSummary())) {
                this.no_dynamic_momo.setVisibility(0);
                this.house_dynamic_layout.setEnabled(false);
            } else {
                this.h_dynamic_dateline.setText(TimeUtil.toDateWithFormat(last_saleInfo.getAddtime(), "yyyy年MM月dd日"));
                this.saleInfo_momo = last_saleInfo.getSummary();
                this.h_dynamic_momo.setText(Html.fromHtml(TextUtil.substring(this.saleInfo_momo, 145, "...")));
                this.house_detail_dynamic_layout.setVisibility(0);
                this.house_dynamic_layout.setEnabled(true);
            }
        }
        List<House> recomm_list = house.getH_recomm_houselist();
        this.recommendAdapter.clear();
        if (recomm_list == null || recomm_list.size() <= 0) {
            this.house_recommend_layout.setVisibility(8);
        } else {
            this.house_recommend_layout.setVisibility(0);
            if (recomm_list.size() < 3) {
                this.recommendAdapter.addAll(recomm_list);
                this.more_recommend_layout.setEnabled(false);
            } else {
                this.recommendAdapter.addItem(recomm_list.get(0));
                this.recommendAdapter.addItem(recomm_list.get(1));
                this.more_recommend_layout.setEnabled(true);
            }
            this.recommendAdapter.notifyDataSetChanged();
            this.recommend_listview.setAdapter((ListAdapter) this.recommendAdapter);
        }
        markerHouse(house);
        addBottomListener();
        this.mApplication.saveBrowseHistory(new HouseInfo("house", this.curHouse), "house");
    }

    private void setEventShow(final List<Event> list, TextView view, final String type) {
        if (list == null || list.size() <= 0) {
            view.setVisibility(8);
            return;
        }
        final int count = list.size();
        view.setVisibility(0);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                Class<?> cls = null;
                if (count > 1) {
                    if (type.equals("event")) {
                        cls = EventListActivity.class;
                    } else {
                        cls = PrivilegeHomeActivity.class;
                        intent.putExtra("assignShow", type);
                        intent.putExtra(PrivilegeHomeActivity.INTENT_EVENTS, (Serializable) list);
                    }
                } else if (type.equals(App.Categroy.Event.COUPON)) {
                    cls = CouponDetailsActivity.class;
                    intent.putExtra("id", ((Event) list.get(0)).getE_id());
                    intent.putExtra(CouponDetailsActivity.INTENT_TYPE, type);
                } else if (type.equals("event") || type.equals(App.Categroy.Event.SELL_EVENT)) {
                    cls = EventListActivity.class;
                    intent.putExtra("e_id", ((Event) list.get(0)).getE_id());
                    intent.putExtra(EventListActivity.INTENT_TYPE, type);
                } else {
                    intent.putExtra("e_id", ((Event) list.get(0)).getE_id());
                    if (type.equals(App.Categroy.Event.TLF)) {
                        cls = CubeHouseRoomActivity.class;
                    }
                    if (type.equals(App.Categroy.Event.TJF)) {
                        cls = BargainlHouseActivity.class;
                    }
                }
                intent.setClass(NewHouseDetailActivity.this, cls);
                NewHouseDetailActivity.this.startActivity(intent);
            }
        });
    }

    private void markerHouse(House house) {
        View.OnClickListener getLineListener = new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.curHouse.getH_lat() == 0.0d || NewHouseDetailActivity.this.curHouse.getH_long() == 0.0d) {
                    NewHouseDetailActivity.this.showToast("该楼盘无地图信息");
                    return;
                }
                String addr = NewHouseDetailActivity.this.curHouse.getH_project_address();
                if (addr != null) {
                    if (addr.indexOf("（") != -1) {
                        addr = addr.substring(0, addr.indexOf("（"));
                    }
                    if (addr.indexOf("(") != -1) {
                        addr = addr.substring(0, addr.indexOf("("));
                    }
                } else {
                    addr = StringUtils.EMPTY;
                }
                try {
                    NewHouseDetailActivity.this.startActivity(IntentUtil.getMapIntent(new StringBuilder(String.valueOf(NewHouseDetailActivity.this.curHouse.getH_lat())).toString(), new StringBuilder(String.valueOf(NewHouseDetailActivity.this.curHouse.getH_long())).toString(), addr));
                } catch (Exception e) {
                    NewHouseDetailActivity.this.showToast("您的手机木有安装地图应用哦");
                }
            }
        };
        if (this.curHouse.getH_lat() == 0.0d || this.curHouse.getH_long() == 0.0d) {
            this.mapView.setVisibility(8);
            if (this.curHouse.getH_project_address() == null || TextUtils.isEmpty(house.getH_project_address())) {
                this.detail_info_map.setVisibility(8);
                return;
            }
            return;
        }
        GeoPoint point = new GeoPoint((int) (house.getH_lat() * 1000000.0d), (int) (house.getH_long() * 1000000.0d));
        this.mapView.getOverlays().add(new MyPositionOverlay(point, getResources().getDrawable(R.drawable.ico_marker)));
        this.mapCont.animateTo(point);
        this.mapCont.setCenter(point);
        this.mapCont.setZoom(16);
        this.project_address_layout.setOnClickListener(getLineListener);
        this.btn_map_layout.setOnClickListener(getLineListener);
    }

    public MapView getMapView() {
        return this.mapView;
    }

    private class GetHouseDetail extends CommonAsyncTask<House> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseDetail(Context context) {
            super(context);
            this.loadingresid = R.string.loading;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
        }

        public void onAfterDoInBackgroup(House house) {
            if (house == null || TextUtils.isEmpty(house.getH_id())) {
                Toast.makeText(NewHouseDetailActivity.this, (int) R.string.msg_load_error, 0).show();
                NewHouseDetailActivity.this.finish();
                return;
            }
            NewHouseDetailActivity.this.ensureUi(house);
            FavUtil.fixFavTab((NewHouseApplication) this.mApplication, NewHouseDetailActivity.this.btn_focus, new HouseInfo("house", NewHouseDetailActivity.this.curHouse));
        }

        public House onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            if (TextUtils.isEmpty(NewHouseDetailActivity.this.h_id)) {
                return null;
            }
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseInfoById(AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity()), NewHouseDetailActivity.this.h_id, NewHouseDetailActivity.this.channel);
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            if (NewHouseDetailActivity.this.outlineHouse != null) {
                NewHouseDetailActivity.this.ensureUi(new House());
                FavUtil.fixFavTab((NewHouseApplication) this.mApplication, NewHouseDetailActivity.this.btn_focus, new HouseInfo("house", NewHouseDetailActivity.this.curHouse));
                return;
            }
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            NewHouseDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            NewHouseDetailActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            NewHouseDetailActivity.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void joinEvent() {
        String mobile = this.mApplication.getMobile();
        if (mobile == null || TextUtils.isEmpty(mobile)) {
            Intent intent = new Intent(this, UserLoginActivity.class);
            intent.putExtra("h_id", this.h_id);
            intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 1);
            startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(this, UserInfoActivity.class);
        intent2.putExtra(UserInfoActivity.INTENT_FROM, 1);
        startActivityForResult(intent2, 1);
    }

    private void addBottomListener() {
        this.btn_apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.curHouse != null) {
                    NewHouseDetailActivity.this.joinEvent();
                    HouseAnalyse.onViewClick(NewHouseDetailActivity.this, "报名", "house", NewHouseDetailActivity.this.curHouse.getH_id());
                }
            }
        });
        this.btn_focus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.curHouse != null) {
                    new FavTask(NewHouseDetailActivity.this, NewHouseDetailActivity.this.mApplication, NewHouseDetailActivity.this.btn_focus, new HouseInfo("house", NewHouseDetailActivity.this.curHouse)).execute(new Object[0]);
                    HouseAnalyse.onViewClick(NewHouseDetailActivity.this, "收藏", "house", NewHouseDetailActivity.this.curHouse.getH_id());
                }
            }
        });
        this.btn_refer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (NewHouseDetailActivity.this.house_sale_phone != null) {
                    try {
                        TelUtil.getCallIntent(NewHouseDetailActivity.this.house_sale_phone, NewHouseDetailActivity.this, "house");
                        NewHouseDetailActivity.this.mApplication.saveCallHistory(new HouseInfo("house", NewHouseDetailActivity.this.curHouse), "house");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }

    private class GetHouseAlbumList extends CommonAsyncTask<List<Photo>> {
        private ArrayList<String> piclist = new ArrayList<>();

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Photo>) ((List) obj));
        }

        public GetHouseAlbumList(Context context) {
            super(context);
        }

        public void onAfterDoInBackgroup(List<Photo> result) {
            if (result != null && result.size() > 0) {
                for (Photo a : result) {
                    System.out.println(String.valueOf(a.getP_name()) + "=" + a.getP_thumb() + "=" + (a.getP_tag() == null) + "+" + a.getP_url());
                    this.piclist.add(a.getP_url());
                }
                Intent intent = new Intent(NewHouseDetailActivity.this, AlbumFullScreenActivity.class);
                intent.putExtra("h_id", NewHouseDetailActivity.this.h_id);
                intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, this.piclist);
                NewHouseDetailActivity.this.startActivity(intent);
            }
        }

        public List<Photo> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            this.piclist.clear();
            if (!NewHouseDetailActivity.this.isVirtual) {
                return null;
            }
            try {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getVirtualHousePhotoById(NewHouseDetailActivity.this.h_id, StringUtils.EMPTY);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 0).show();
        }
    }
}
