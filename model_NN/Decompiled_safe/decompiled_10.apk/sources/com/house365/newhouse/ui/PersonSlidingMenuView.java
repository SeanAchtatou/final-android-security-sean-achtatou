package com.house365.newhouse.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.constant.CorePreferences;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.task.UpgradeTask;
import com.house365.core.util.DialogUtil;
import com.house365.core.view.Switch;
import com.house365.core.view.menu.SlidingMenu;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.privilege.GroupHouseActivity;
import com.house365.newhouse.ui.setting.AboutActivity;
import com.house365.newhouse.ui.setting.FeedbackActivity;
import com.house365.newhouse.ui.setting.RecomAppActivity;
import com.house365.newhouse.ui.user.MyContactsActivity;
import com.house365.newhouse.ui.user.MyFavBrowserActivity;
import com.house365.newhouse.ui.user.MyPublishActivity;
import com.house365.newhouse.ui.user.UserEventRecordActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.PushServiceUtil;
import org.apache.commons.lang.SystemUtils;

public class PersonSlidingMenuView {
    private static final int CLEAR_CACHE_OK = 1;
    private View about_layout;
    private RelativeLayout applay_layout;
    private RelativeLayout browse_history_layout;
    private View clear_layout;
    private View feedback_layout;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    PersonSlidingMenuView.this.menuActivity.showToast((int) R.string.text_clear);
                    PersonSlidingMenuView.this.menuActivity.dismissLoadingDialog();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private View help_layout;
    private RelativeLayout last_contact_layout;
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (AppMethods.isJustNewHouse(PersonSlidingMenuView.this.newApp.getCity())) {
                PersonSlidingMenuView.this.mypublish_total_layout.setVisibility(8);
            } else {
                PersonSlidingMenuView.this.mypublish_total_layout.setVisibility(0);
            }
        }
    };
    private BroadcastReceiver mRefreshMobile = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            PersonSlidingMenuView.this.tv_user_phone.setText(intent.getStringExtra("refresh_mobile"));
        }
    };
    private SlidingMenu menu;
    /* access modifiers changed from: private */
    public MenuActivity menuActivity;
    private RelativeLayout myfav_layout;
    private RelativeLayout mypublish_layout;
    /* access modifiers changed from: private */
    public View mypublish_total_layout;
    /* access modifiers changed from: private */
    public NewHouseApplication newApp;
    private RelativeLayout person_group;
    private View recommapp_layout;
    private Switch switch_notify;
    private Switch switch_pic;
    /* access modifiers changed from: private */
    public TextView tv_user_phone;
    private View upgrade_layout;
    private RelativeLayout user_layout;

    public PersonSlidingMenuView(MenuActivity menuActivity2, NewHouseApplication application) {
        this.menuActivity = menuActivity2;
        this.newApp = application;
        menuActivity2.registerReceiver(this.mRefreshMobile, new IntentFilter(ActionCode.INTENT_ACTION_REFRESH_PERSON_MOBILE));
        menuActivity2.registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        initMenu();
        initSidebar();
    }

    public SlidingMenu getSlidingMenu() {
        return this.menu;
    }

    public BroadcastReceiver getmChangeCity() {
        return this.mChangeCity;
    }

    public void setmChangeCity(BroadcastReceiver mChangeCity2) {
        this.mChangeCity = mChangeCity2;
    }

    public BroadcastReceiver getmRefreshMobile() {
        return this.mRefreshMobile;
    }

    public void setmRefreshMobile(BroadcastReceiver mRefreshMobile2) {
        this.mRefreshMobile = mRefreshMobile2;
    }

    private void initMenu() {
        this.menu = new SlidingMenu(this.menuActivity);
        this.menu.setTouchModeAbove(1);
        this.menu.setShadowWidthRes(R.dimen.shadow_width);
        this.menu.setShadowDrawable((int) R.drawable.slide_menu_shadow_left);
        this.menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        this.menu.setSecondaryShadowDrawable((int) R.drawable.slide_menu_shadow);
        this.menu.setBehindScrollScale(SystemUtils.JAVA_VERSION_FLOAT);
        this.menu.setFadeDegree(SystemUtils.JAVA_VERSION_FLOAT);
        this.menu.attachToActivity(this.menuActivity, 1);
        this.menu.setMenu((int) R.layout.setting_layout);
        this.menu.setSecondaryMenu((int) R.layout.person);
        this.menu.setMode(2);
    }

    private void initSidebar() {
        View sidebar0 = this.menu.getMenu();
        this.clear_layout = sidebar0.findViewById(R.id.clear_layout);
        this.feedback_layout = sidebar0.findViewById(R.id.feedback_layout);
        this.help_layout = sidebar0.findViewById(R.id.help_layout);
        this.recommapp_layout = sidebar0.findViewById(R.id.recommapp_layout);
        this.about_layout = sidebar0.findViewById(R.id.about_layout);
        this.upgrade_layout = sidebar0.findViewById(R.id.upgrade_layout);
        this.switch_notify = (Switch) sidebar0.findViewById(R.id.switch_notify);
        this.switch_pic = (Switch) sidebar0.findViewById(R.id.switch_pic);
        this.switch_notify.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            public void onCheckedChanged(Switch switchView, boolean isChecked) {
                PersonSlidingMenuView.this.newApp.enablePushNotification(isChecked);
                if (isChecked) {
                    new PushServiceUtil().startPNService(PersonSlidingMenuView.this.menuActivity, PersonSlidingMenuView.this.newApp, 1, null, App.PULL_ACCOUNT_PASSWORD);
                    HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "开启推送", "设置", null);
                    return;
                }
                PersonSlidingMenuView.this.newApp.removePullServiceManager();
                HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "关闭推送", "设置", null);
            }
        });
        this.switch_pic.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            public void onCheckedChanged(Switch switchView, boolean isChecked) {
                if (isChecked) {
                    HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "有图", "设置", null);
                } else {
                    HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "无图", "设置", null);
                }
                PersonSlidingMenuView.this.newApp.enableImg(isChecked);
            }
        });
        this.clear_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DialogUtil.showConfrimDialog(PersonSlidingMenuView.this.menuActivity, (int) R.string.text_clear_dialog_msg, (int) R.string.dialog_button_ok, new ConfirmDialogListener() {
                    public void onPositive(DialogInterface dialog) {
                        PersonSlidingMenuView.this.cleanCache();
                        HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "清除缓存", "设置", null);
                    }

                    public void onNegative(DialogInterface dialog) {
                    }
                });
            }
        });
        this.upgrade_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseAnalyse.onViewClick(PersonSlidingMenuView.this.menuActivity, "更新", "设置", null);
                new UpgradeTask(PersonSlidingMenuView.this.menuActivity, false).execute(new Object[0]);
            }
        });
        this.help_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, RecomAppActivity.class);
                intent.putExtra(RecomAppActivity.INTENT_TITLE, PersonSlidingMenuView.this.menuActivity.getResources().getString(R.string.text_intro));
                intent.putExtra(RecomAppActivity.INTENT_LOADFILE, "http://app.house365.com/taofang/help.html");
                PersonSlidingMenuView.this.menuActivity.startActivity(intent);
            }
        });
        this.recommapp_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, RecomAppActivity.class);
                intent.putExtra(RecomAppActivity.INTENT_TITLE, PersonSlidingMenuView.this.menuActivity.getResources().getString(R.string.text_more_app));
                intent.putExtra(RecomAppActivity.INTENT_LOADFILE, "http://app.house365.com/client/?app=tlf&type=android");
                PersonSlidingMenuView.this.menuActivity.startActivity(intent);
            }
        });
        this.feedback_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, FeedbackActivity.class));
            }
        });
        this.about_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, AboutActivity.class));
            }
        });
        this.switch_notify.setChecked(this.newApp.isEnablePushNotification());
        this.switch_pic.setChecked(this.newApp.isEnableImg());
        View sidebar = this.menu.getSecondaryMenu();
        this.user_layout = (RelativeLayout) sidebar.findViewById(R.id.user_layout);
        this.applay_layout = (RelativeLayout) sidebar.findViewById(R.id.applay_layout);
        this.last_contact_layout = (RelativeLayout) sidebar.findViewById(R.id.last_contact_layout);
        this.myfav_layout = (RelativeLayout) sidebar.findViewById(R.id.myfav_layout);
        this.browse_history_layout = (RelativeLayout) sidebar.findViewById(R.id.browse_history_layout);
        this.mypublish_layout = (RelativeLayout) sidebar.findViewById(R.id.mypublish_layout);
        this.mypublish_total_layout = sidebar.findViewById(R.id.mypublish_total_layout);
        this.tv_user_phone = (TextView) sidebar.findViewById(R.id.tv_user_phone);
        if (TextUtils.isEmpty(this.newApp.getMobile())) {
            this.tv_user_phone.setText((int) R.string.text_more_login);
        } else {
            this.tv_user_phone.setText(this.newApp.getMobile());
        }
        this.user_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (TextUtils.isEmpty(PersonSlidingMenuView.this.newApp.getMobile())) {
                    Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, UserLoginActivity.class);
                    intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 2);
                    PersonSlidingMenuView.this.menuActivity.startActivity(intent);
                    return;
                }
                DialogUtil.showConfrimDialog(PersonSlidingMenuView.this.menuActivity, (int) R.string.text_login_dialog_title, (int) R.string.text_login_dialog_msg, (int) R.string.text_login_dialog_confirm, (int) R.string.text_login_dialog_cancle, new ConfirmDialogListener() {
                    public void onPositive(DialogInterface dialog) {
                        PersonSlidingMenuView.this.tv_user_phone.setText((int) R.string.text_more_login);
                        PersonSlidingMenuView.this.newApp.setMobile(null);
                    }

                    public void onNegative(DialogInterface dialog) {
                    }
                });
            }
        });
        CorePreferences.ERROR("person:\t" + this.newApp.getCity());
        if (AppMethods.isJustNewHouse(this.newApp.getCity())) {
            this.mypublish_total_layout.setVisibility(8);
        } else {
            this.mypublish_total_layout.setVisibility(0);
        }
        this.mypublish_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, MyPublishActivity.class));
            }
        });
        this.applay_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (TextUtils.isEmpty(PersonSlidingMenuView.this.newApp.getMobile())) {
                    Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, UserLoginActivity.class);
                    intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 3);
                    PersonSlidingMenuView.this.menuActivity.startActivity(intent);
                    return;
                }
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, UserEventRecordActivity.class));
            }
        });
        this.last_contact_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, MyContactsActivity.class));
            }
        });
        this.myfav_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, MyFavBrowserActivity.class);
                intent.putExtra(MyFavBrowserActivity.INTENT_DATATYPE, 1);
                PersonSlidingMenuView.this.menuActivity.startActivity(intent);
            }
        });
        this.browse_history_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, MyFavBrowserActivity.class);
                intent.putExtra(MyFavBrowserActivity.INTENT_DATATYPE, 2);
                PersonSlidingMenuView.this.menuActivity.startActivity(intent);
            }
        });
        this.person_group = (RelativeLayout) sidebar.findViewById(R.id.person_group_layout);
        this.person_group.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (TextUtils.isEmpty(PersonSlidingMenuView.this.newApp.getMobile())) {
                    Intent intent = new Intent(PersonSlidingMenuView.this.menuActivity, UserLoginActivity.class);
                    intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 4);
                    PersonSlidingMenuView.this.menuActivity.startActivity(intent);
                    return;
                }
                PersonSlidingMenuView.this.menuActivity.startActivity(new Intent(PersonSlidingMenuView.this.menuActivity, GroupHouseActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void cleanCache() {
        this.menuActivity.showLoadingDialog(R.string.text_more_cache_clearing);
        new Thread() {
            public void run() {
                PersonSlidingMenuView.this.newApp.clearPrefsCache();
                PersonSlidingMenuView.this.newApp.cleanAllFile();
                PersonSlidingMenuView.this.newApp.getDiskCache().clear();
                Message msg = new Message();
                msg.what = 1;
                PersonSlidingMenuView.this.handler.sendMessage(msg);
            }
        }.start();
    }
}
