package com.house365.newhouse.ui.newhome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Photo;

public class NewHouseAlbumSpecifyAdapter extends BaseCacheListAdapter<Photo> {
    private LayoutInflater inflater;

    public NewHouseAlbumSpecifyAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder(null);
            convertView = this.inflater.inflate((int) R.layout.item_grid_photo, (ViewGroup) null);
            holder.album_pic = (ImageView) convertView.findViewById(R.id.album_pic);
            holder.photo_title = (TextView) convertView.findViewById(R.id.photo_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Photo photo = (Photo) getItem(position);
        setCacheImage(holder.album_pic, photo.getP_thumb(), R.drawable.bg_default_img_detail, 1);
        holder.photo_title.setText(photo.getP_tag() == null ? photo.getP_name() : photo.getP_tag());
        return convertView;
    }

    private static class ViewHolder {
        ImageView album_pic;
        TextView photo_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
