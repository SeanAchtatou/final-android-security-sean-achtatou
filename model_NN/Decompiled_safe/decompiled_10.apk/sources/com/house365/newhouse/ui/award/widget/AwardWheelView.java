package com.house365.newhouse.ui.award.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.house365.core.view.wheel.widget.WheelView;

public class AwardWheelView extends WheelView {
    public AwardWheelView(Context context) {
        super(context);
    }

    public AwardWheelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AwardWheelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void initResourcesIfNecessary() {
    }

    /* access modifiers changed from: protected */
    public void drawCenterRect(Canvas canvas) {
    }
}
