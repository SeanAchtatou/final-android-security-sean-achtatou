package com.house365.newhouse.ui.privilege;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.TextUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.NoScrollListView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.task.GroupTask;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.newhome.FlatsDetailsActivity;
import com.house365.newhouse.ui.newhome.HouseIntroActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.newhome.adapter.NewHouseFlatsAdapter;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class CubeHouseRoomActivity extends BaseCommonActivity {
    public static final String INTENT_EID = "e_id";
    private static final int REQUET_LOG_FEED = 2;
    private static final int REQUET_TLF_APPLY = 1;
    /* access modifiers changed from: private */
    public TextView btn_apply;
    private TextView btn_refer;
    /* access modifiers changed from: private */
    public Event curEvent;
    private long curtime;
    private RelativeLayout descriptioninfo_detail_layout;
    private LinearLayout descriptioninfo_layout;
    /* access modifiers changed from: private */
    public TextView e_count_down_time;
    private TextView e_descriptioninfo;
    /* access modifiers changed from: private */
    public String e_id;
    private TextView e_join_num;
    private TextView e_noteinfo;
    private ImageView e_pic;
    private TextView e_tlf_privillege;
    private TextView e_tlf_privillege_prefix;
    private TextView e_tlf_sale_price;
    private TextView e_tlf_sale_price_prefix;
    /* access modifiers changed from: private */
    public String event_count_down_time;
    private String event_join_num;
    /* access modifiers changed from: private */
    public String event_sale_phone;
    private String event_sale_price;
    private String event_tlf_privillege;
    /* access modifiers changed from: private */
    public NewHouseFlatsAdapter flatsAdapter;
    private TextView gray_line;
    private TextView h_build_type;
    private TextView h_deli_date;
    private TextView h_deli_standard;
    private TextView h_kfs;
    private TextView h_project_address;
    private HeadNavigateView head_view;
    List<HouseInfo> houseInfos = new ArrayList();
    private String house_build_type;
    private String house_deli_date;
    private String house_deli_standard;
    private String house_kfs;
    private String house_name;
    private TextView house_name_in;
    private RelativeLayout house_name_in_layout;
    private String house_project_address;
    private NoScrollListView house_room_list;
    private LinearLayout house_room_list_layout;
    private ScrollView house_scroll;
    public long intCountDownTime;
    /* access modifiers changed from: private */
    public boolean isShowPic;
    private NewHouseApplication mApplication;
    private MyCountimer mctime;
    private RelativeLayout noteinfo_detail_layout;
    private LinearLayout noteinfo_layout;
    /* access modifiers changed from: private */
    public String type = App.Categroy.Event.TLF;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1 && this.curEvent != null) {
            new GroupTask(this, this.mApplication, this.curEvent.getE_id(), App.Categroy.Event.TLF, null, this.mApplication.getMobile()).execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.cubehouse_room);
        this.mApplication = (NewHouseApplication) getApplication();
    }

    private class GetHouseDetail extends CommonAsyncTask<Event> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public GetHouseDetail(Context context) {
            super(context);
            this.loadingresid = R.string.loading;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(Event event) {
            if (event == null || TextUtils.isEmpty(event.getE_id())) {
                CubeHouseRoomActivity.this.showToast((int) R.string.msg_load_error);
                CubeHouseRoomActivity.this.finish();
                return;
            }
            CubeHouseRoomActivity.this.ensureUi(event);
        }

        public Event onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getEventDetail(CubeHouseRoomActivity.this.e_id, CubeHouseRoomActivity.this.type);
        }

        /* access modifiers changed from: protected */
        public void onNetworkUnavailable() {
            Toast.makeText(this.context, (int) R.string.text_no_network, 1).show();
            CubeHouseRoomActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onHttpRequestError() {
            super.onHttpRequestError();
            CubeHouseRoomActivity.this.finish();
        }

        /* access modifiers changed from: protected */
        public void onParseError() {
            super.onParseError();
            CubeHouseRoomActivity.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void ensureUi(Event event) {
        this.curEvent = event;
        this.house_scroll.smoothScrollTo(0, 0);
        this.head_view.setTvTitleText(event.getE_title());
        this.event_tlf_privillege = event.getE_s_price();
        this.event_sale_price = event.getE_o_price();
        this.event_join_num = getResources().getString(R.string.text_group_apply_num, event.getE_join());
        this.event_sale_phone = event.getE_tel();
        if (this.event_sale_phone == null || TextUtils.isEmpty(this.event_sale_phone)) {
            this.event_sale_phone = AppArrays.default_tel;
        }
        this.btn_refer.setText(this.event_sale_phone);
        final House house = event.getE_house();
        this.house_name = getResources().getString(R.string.text_house_in_name, house.getH_name());
        this.house_project_address = getResources().getString(R.string.text_field_project_address, house.getH_project_address());
        this.house_deli_date = getResources().getString(R.string.text_field_deli_date, house.getH_deli_date());
        this.house_deli_standard = getResources().getString(R.string.text_field_decoration_states, house.getH_deli_standard());
        this.house_kfs = getResources().getString(R.string.text_field_kfs, house.getH_kfs());
        this.house_build_type = getResources().getString(R.string.text_field_build_type, house.getH_build_type());
        String pic_first = event.getE_pic();
        this.e_pic.setVisibility(0);
        if (!this.isShowPic) {
            setImage(this.e_pic, StringUtils.EMPTY, (int) R.drawable.bg_default_ad, 1);
            this.e_pic.setEnabled(false);
        } else {
            setImage(this.e_pic, pic_first, (int) R.drawable.bg_default_ad, 1);
        }
        ViewGroup.LayoutParams lp = this.gray_line.getLayoutParams();
        lp.width = this.e_tlf_sale_price.getWidth() + 130;
        this.gray_line.setLayoutParams(lp);
        this.gray_line.setVisibility(0);
        if (!TextUtils.isEmpty(this.event_sale_price)) {
            this.e_tlf_sale_price.setText(this.event_sale_price);
        } else {
            this.e_tlf_sale_price.setText("待定");
        }
        if (!TextUtils.isEmpty(this.event_tlf_privillege)) {
            this.e_tlf_privillege.setText(this.event_tlf_privillege);
        } else {
            this.e_tlf_privillege.setText("待定");
        }
        TextUtil.setNullText(event.getE_join(), this.event_join_num, this.e_join_num);
        this.e_tlf_privillege_prefix.setVisibility(0);
        this.e_tlf_sale_price_prefix.setVisibility(0);
        TextUtil.setNullText(house.getH_name(), this.house_name, this.house_name_in);
        TextUtil.setNullText(house.getH_kfs(), this.house_kfs, this.h_kfs);
        TextUtil.setNullText(house.getH_build_type(), this.house_build_type, this.h_build_type);
        TextUtil.setNullText(house.getH_deli_date(), this.house_deli_date, this.h_deli_date);
        TextUtil.setNullText(house.getH_deli_standard(), this.house_deli_standard, this.h_deli_standard);
        TextUtil.setNullText(house.getH_project_address(), this.house_project_address, this.h_project_address);
        if (this.curEvent.getE_explain() == null || TextUtils.isEmpty(this.curEvent.getE_explain())) {
            this.descriptioninfo_layout.setVisibility(8);
            this.e_descriptioninfo.setText(StringUtils.EMPTY);
        } else {
            this.e_descriptioninfo.setText(Html.fromHtml(TextUtil.substring(this.curEvent.getE_explain(), 150, "...")));
            this.descriptioninfo_layout.setVisibility(0);
            this.descriptioninfo_detail_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(CubeHouseRoomActivity.this, HouseIntroActivity.class);
                    intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, CubeHouseRoomActivity.this.getResources().getString(R.string.text_group_online_title));
                    intent.putExtra("info", CubeHouseRoomActivity.this.curEvent.getE_explain());
                    CubeHouseRoomActivity.this.startActivity(intent);
                }
            });
        }
        if (this.curEvent.getE_remark() != null || !TextUtils.isEmpty(this.curEvent.getE_remark())) {
            this.e_noteinfo.setText(Html.fromHtml(TextUtil.substring(this.curEvent.getE_remark(), 150, "...")));
            this.noteinfo_layout.setVisibility(0);
            this.noteinfo_detail_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(CubeHouseRoomActivity.this, HouseIntroActivity.class);
                    intent.putExtra(LoanCalSelProActivity.INTENT_TITLE, CubeHouseRoomActivity.this.getResources().getString(R.string.text_group_offline_title));
                    intent.putExtra("info", CubeHouseRoomActivity.this.curEvent.getE_remark());
                    CubeHouseRoomActivity.this.startActivity(intent);
                }
            });
        } else {
            this.noteinfo_layout.setVisibility(8);
            this.e_noteinfo.setText(StringUtils.EMPTY);
        }
        if (!(house == null || house.getH_id() == null)) {
            this.house_name_in_layout.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                public void onClick(View v) {
                    Intent intent = new Intent(CubeHouseRoomActivity.this, NewHouseDetailActivity.class);
                    intent.putExtra("h_id", house.getH_id());
                    intent.putExtra(NewHouseDetailActivity.INTENT_DISAPPER_EVENT, true);
                    CubeHouseRoomActivity.this.startActivity(intent);
                }
            });
        }
        this.curtime = System.currentTimeMillis();
        this.intCountDownTime = (event.getE_endtime() * 1000) - this.curtime;
        this.mctime = new MyCountimer(this.intCountDownTime, 1000);
        this.mctime.start();
        this.flatsAdapter.clear();
        List<Photo> photos = event.getE_rooms();
        if (photos != null && photos.size() > 0) {
            this.flatsAdapter.addAll(photos);
            this.flatsAdapter.notifyDataSetChanged();
            this.house_room_list_layout.setVisibility(0);
        }
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            CubeHouseRoomActivity.this.btn_apply.setEnabled(false);
            CubeHouseRoomActivity.this.e_count_down_time.setText((int) R.string.text_bargain_event_over);
        }

        public void onTick(long millisUntilFinished) {
            if (CubeHouseRoomActivity.this.intCountDownTime > 0) {
                CubeHouseRoomActivity.this.event_count_down_time = TimeUtil.formatLongToTimeStr(Long.valueOf(millisUntilFinished), "dd天HH小时mm分钟ss秒");
                if (millisUntilFinished > 0) {
                    CubeHouseRoomActivity.this.e_count_down_time.setText(Html.fromHtml(CubeHouseRoomActivity.this.event_count_down_time));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CubeHouseRoomActivity.this.finish();
            }
        });
        this.house_scroll = (ScrollView) findViewById(R.id.detail_info_scroll);
        this.house_room_list_layout = (LinearLayout) findViewById(R.id.house_room_list_layout);
        this.house_name_in_layout = (RelativeLayout) findViewById(R.id.house_name_in_layout);
        this.descriptioninfo_detail_layout = (RelativeLayout) findViewById(R.id.descriptioninfo_detail_layout);
        this.noteinfo_detail_layout = (RelativeLayout) findViewById(R.id.noteinfo_detail_layout);
        this.house_room_list = (NoScrollListView) findViewById(R.id.house_room_listview);
        this.flatsAdapter = new NewHouseFlatsAdapter(this);
        this.house_room_list.setAdapter((ListAdapter) this.flatsAdapter);
        this.house_room_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (!CubeHouseRoomActivity.this.isShowPic) {
                    Intent intent = new Intent(CubeHouseRoomActivity.this, FlatsDetailsActivity.class);
                    intent.putExtra("photo", (Serializable) CubeHouseRoomActivity.this.flatsAdapter.getItem(position));
                    CubeHouseRoomActivity.this.startActivity(intent);
                }
            }
        });
        this.btn_apply = (TextView) findViewById(R.id.btn_apply);
        if (getIntent().getStringExtra("apply_result") != null && getIntent().getStringExtra("apply_result").equals("success")) {
            Toast.makeText(this, (int) R.string.text_house_apply_toast, 1).show();
        }
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.btn_apply.setText(getString(R.string.apply_privilege));
        this.btn_apply.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.apply, 0, 0, 0);
        this.btn_refer.setText(getString(R.string.contact_house_sell));
        this.e_pic = (ImageView) findViewById(R.id.h_pic);
        this.e_tlf_privillege = (TextView) findViewById(R.id.h_tlf_privillege);
        this.e_tlf_sale_price = (TextView) findViewById(R.id.h_tlf_sale_price);
        this.gray_line = (TextView) findViewById(R.id.gray_line);
        this.e_count_down_time = (TextView) findViewById(R.id.h_cut_time);
        this.e_join_num = (TextView) findViewById(R.id.h_join_num);
        this.e_tlf_privillege_prefix = (TextView) findViewById(R.id.h_tlf_privillege_prefix);
        this.e_tlf_sale_price_prefix = (TextView) findViewById(R.id.h_tlf_sale_price_prefix);
        this.house_name_in = (TextView) findViewById(R.id.house_name_in);
        this.h_project_address = (TextView) findViewById(R.id.h_project_address);
        this.h_kfs = (TextView) findViewById(R.id.h_kfs);
        this.h_build_type = (TextView) findViewById(R.id.h_build_type);
        this.h_deli_date = (TextView) findViewById(R.id.h_deli_date);
        this.h_deli_standard = (TextView) findViewById(R.id.h_deli_standard);
        this.e_descriptioninfo = (TextView) findViewById(R.id.h_descriptioninfo);
        this.descriptioninfo_layout = (LinearLayout) findViewById(R.id.descriptioninfo_layout);
        this.noteinfo_layout = (LinearLayout) findViewById(R.id.noteinfo_layout);
        this.e_noteinfo = (TextView) findViewById(R.id.h_noteinfo);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.e_id = getIntent().getStringExtra("e_id");
        this.isShowPic = this.mApplication.isEnableImg();
        addBottomListener();
        new GetHouseDetail(this).execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void joinEvent() {
        String mobile = this.mApplication.getMobile();
        if (mobile == null || TextUtils.isEmpty(mobile)) {
            Intent intent = new Intent(this, UserLoginActivity.class);
            intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 2);
            startActivityForResult(intent, 1);
        } else if (this.curEvent != null) {
            new GroupTask(this, this.mApplication, this.curEvent.getE_id(), App.Categroy.Event.TLF, null, mobile).execute(new Object[0]);
        }
    }

    private void addBottomListener() {
        this.btn_apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CubeHouseRoomActivity.this.curEvent != null) {
                    CubeHouseRoomActivity.this.joinEvent();
                    HouseAnalyse.onViewClick(CubeHouseRoomActivity.this, "报名", App.Categroy.Event.TLF, CubeHouseRoomActivity.this.curEvent.getE_id());
                }
            }
        });
        this.btn_refer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CubeHouseRoomActivity.this.event_sale_phone != null) {
                    try {
                        TelUtil.getCallIntent(CubeHouseRoomActivity.this.event_sale_phone, CubeHouseRoomActivity.this, App.Categroy.Event.TLF);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
