package com.house365.newhouse.ui.tools;

/* compiled from: LoanCalActivity */
class LoanYears extends CalBean {
    int times;

    public LoanYears(String key, int times2) {
        this.key = key;
        this.times = times2;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getTimes() {
        return this.times;
    }

    public void setTimes(int times2) {
        this.times = times2;
    }
}
