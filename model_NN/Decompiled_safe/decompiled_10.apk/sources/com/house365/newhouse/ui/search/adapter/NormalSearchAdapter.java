package com.house365.newhouse.ui.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;

public class NormalSearchAdapter extends BaseListAdapter<String> {
    private NewHouseApplication application;
    private LayoutInflater inflater;
    private int type;

    public NormalSearchAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public NormalSearchAdapter(Context context, NewHouseApplication application2) {
        super(context);
        this.inflater = LayoutInflater.from(context);
        this.application = application2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if (this.type == 1) {
                convertView = this.inflater.inflate((int) R.layout.item_city, parent, false);
                holder.text_choose_momo = (TextView) convertView.findViewById(R.id.other_city);
                holder.city_chose = (ImageView) convertView.findViewById(R.id.city_chose);
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String momo = (String) getItem(position);
        holder.text_choose_momo.setText(momo);
        if (this.type == 1) {
            if (this.application == null || this.application.getCityName() == null || !this.application.getCityName().equals(momo)) {
                holder.city_chose.setVisibility(8);
            } else {
                holder.city_chose.setVisibility(0);
            }
        }
        return convertView;
    }

    class ViewHolder {
        /* access modifiers changed from: private */
        public ImageView city_chose;
        /* access modifiers changed from: private */
        public TextView text_choose_momo;

        ViewHolder() {
        }
    }
}
