package com.house365.newhouse.ui.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.house365.core.activity.BaseTabActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.secondrent.SecondRentOfferActivity;
import com.house365.newhouse.ui.secondrent.SecondRentPublishActivity;
import com.house365.newhouse.ui.secondsell.SecondSellOfferActivity;
import com.house365.newhouse.ui.secondsell.SecondSellPublishActivity;

public class SearchHomeActivity extends BaseTabActivity {
    public static final String INTENT_PUBLISH = "publish";
    private HeadNavigateView head_View;
    public int type;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.search_home_layout);
        this.head_View = (HeadNavigateView) findViewById(R.id.head_view);
        this.type = getIntent().getIntExtra(INTENT_PUBLISH, 0);
        if (this.type == 12 || this.type == 13) {
            this.head_View.setTvTitleText((int) R.string.publish);
        } else {
            this.head_View.setTvTitleText((int) R.string.text_search_title);
        }
        this.head_View.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchHomeActivity.this.finish();
            }
        });
        updateTabs();
        changeTab(0);
        getTabHost().setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                SearchHomeActivity.this.changeTab(Integer.parseInt(tabId));
            }
        });
    }

    public void changeTab(int tab) {
        super.changeTab(tab);
        int total = getTabWidget().getTabCount();
        for (int i = 0; i < total; i++) {
            TextView title = (TextView) getTabWidget().getChildAt(i).findViewById(R.id.item_title);
            if (tab == i) {
                title.setTextColor(getResources().getColor(R.color.group_blue));
            } else {
                title.setTextColor(getResources().getColor(R.color.group_normal));
            }
        }
    }

    private void updateTabs() {
        getTabHost().clearAllTabs();
        if (this.type != 12 && this.type != 13) {
            addTab("0", createView(R.string.text_newhome_title), new Intent(this, NewSearchActivity.class));
            if (!AppMethods.isJustNewHouse(((NewHouseApplication) this.mApplication).getCity())) {
                addTab("1", createView(R.string.text_second_house_title), new Intent(this, SecondSellSearchActivity.class));
                addTab("2", createView(R.string.text_rent_house_title), new Intent(this, SecondRentSearchActivity.class));
                changeTab(0);
            }
        } else if (this.type == 12) {
            addTab("0", createView(R.string.text_publish_sell), new Intent(this, SecondSellPublishActivity.class));
            addTab("1", createView(R.string.text_publish_sell_offer), new Intent(this, SecondSellOfferActivity.class));
        } else {
            addTab("0", createView(R.string.text_publish_rent), new Intent(this, SecondRentPublishActivity.class));
            addTab("1", createView(R.string.text_publish_rent_offer), new Intent(this, SecondRentOfferActivity.class));
        }
    }

    private View createView(int text) {
        View view = LayoutInflater.from(this).inflate((int) R.layout.text_item_tab, (ViewGroup) null);
        TextView title = (TextView) view.findViewById(R.id.item_title);
        ImageView item_title_ico = (ImageView) view.findViewById(R.id.item_title_ico);
        if (text == R.string.text_rent_house_title || text == R.string.text_publish_sell_offer || text == R.string.text_publish_rent_offer) {
            item_title_ico.setVisibility(8);
        }
        title.setText(text);
        return view;
    }
}
