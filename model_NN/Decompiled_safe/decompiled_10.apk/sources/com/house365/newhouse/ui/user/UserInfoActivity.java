package com.house365.newhouse.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.RegexUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.task.GroupTask;
import com.house365.newhouse.tool.ActionCode;
import org.apache.commons.lang.StringUtils;

public class UserInfoActivity extends BaseCommonActivity {
    public static final int FROM_TYPE_DETAIL_INTO = 1;
    public static final int FROM_TYPE_MORE_LOGIN_INTO = 2;
    public static final String INTENT_CID = "c_id";
    public static final String INTENT_COUPON_TYPE = "coupon_type";
    public static final String INTENT_FROM = "toApply";
    public static final String INTENT_HID = "h_id";
    private Button btn_user_apply;
    /* access modifiers changed from: private */
    public String c_id;
    /* access modifiers changed from: private */
    public String ctype;
    /* access modifiers changed from: private */
    public EditText et_input_phone;
    /* access modifiers changed from: private */
    public EditText et_username;
    /* access modifiers changed from: private */
    public int fromtype;
    /* access modifiers changed from: private */
    public String h_id;
    private HeadNavigateView head_view;
    private ImageView ico_clear;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_input_info);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.fromtype = getIntent().getIntExtra(INTENT_FROM, 0);
        this.h_id = getIntent().getStringExtra("h_id");
        this.c_id = getIntent().getStringExtra("c_id");
        this.ctype = getIntent().getStringExtra("coupon_type");
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_apply_personinfo_label);
        View.OnClickListener finish = new View.OnClickListener() {
            public void onClick(View v) {
                UserInfoActivity.this.finish();
            }
        };
        this.head_view.getBtn_left().setOnClickListener(finish);
        ((Button) findViewById(R.id.btn_user_cancle)).setOnClickListener(finish);
        this.et_username = (EditText) findViewById(R.id.et_username);
        this.et_input_phone = (EditText) findViewById(R.id.et_input_phone);
        this.btn_user_apply = (Button) findViewById(R.id.btn_user_apply);
        this.et_username.isFocused();
        this.ico_clear = (ImageView) findViewById(R.id.ico_clear);
        if (TextUtils.isEmpty(this.ctype)) {
            this.ico_clear.setVisibility(8);
            this.et_input_phone.setFocusable(false);
        } else if (this.ctype.equals("event") || this.ctype.equals(App.Categroy.Event.SELL_EVENT)) {
            this.ico_clear.setVisibility(0);
            this.et_input_phone.setFocusable(true);
        }
        this.ico_clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserInfoActivity.this.et_input_phone.setText(StringUtils.EMPTY);
            }
        });
        this.btn_user_apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String username = UserInfoActivity.this.et_username.getText().toString().trim();
                String mobile = UserInfoActivity.this.et_input_phone.getText().toString().trim();
                if (RegexUtil.isMobileNumber(mobile)) {
                    Intent intent = new Intent();
                    intent.putExtra("truename", username);
                    intent.putExtra("mobile", mobile);
                    if (UserInfoActivity.this.fromtype == 2) {
                        GroupTask group = null;
                        if (UserInfoActivity.this.h_id != null && !TextUtils.isEmpty(UserInfoActivity.this.h_id)) {
                            group = new GroupTask(UserInfoActivity.this, (NewHouseApplication) UserInfoActivity.this.mApplication, UserInfoActivity.this.h_id, "house", username, mobile);
                            group.execute(new Object[0]);
                        }
                        if (UserInfoActivity.this.c_id != null && !TextUtils.isEmpty(UserInfoActivity.this.c_id) && UserInfoActivity.this.ctype != null && !TextUtils.isEmpty(UserInfoActivity.this.ctype)) {
                            group = new GroupTask(UserInfoActivity.this, (NewHouseApplication) UserInfoActivity.this.mApplication, UserInfoActivity.this.c_id, UserInfoActivity.this.ctype, username, mobile);
                            group.execute(new Object[0]);
                        }
                        if (group != null) {
                            group.setOnFinish(new GroupTask.GroupTaskonFinish() {
                                public void onFinish() {
                                    UserInfoActivity.this.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_SIGN));
                                    UserInfoActivity.this.finish();
                                }
                            });
                            return;
                        }
                        return;
                    }
                    UserInfoActivity.this.setResult(-1, intent);
                    UserInfoActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        String mobile = ((NewHouseApplication) this.mApplication).getMobile();
        if (mobile != null && !TextUtils.isEmpty(mobile)) {
            this.et_input_phone.setText(mobile);
            this.et_input_phone.setTextColor(getResources().getColor(R.color.gray));
        }
    }
}
