package com.house365.newhouse.ui.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.secondhouse.adapter.ContactsHouseListAdapter;
import com.house365.newhouse.ui.secondrent.SecondRentDetailActivity;
import com.house365.newhouse.ui.secondsell.SecondSellDetailActivity;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class MyContactsActivity extends BaseListActivity {
    /* access modifiers changed from: private */
    public ContactsHouseListAdapter adapter;
    private String app;
    /* access modifiers changed from: private */
    public Context context;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public ListView listView;
    /* access modifiers changed from: private */
    public List<HouseInfo> newHouseInfo;
    /* access modifiers changed from: private */
    public LinearLayout nodata_layout;
    /* access modifiers changed from: private */
    public List<HouseInfo> rentHouseInfo;
    /* access modifiers changed from: private */
    public List<HouseInfo> sellHouseInfo;
    /* access modifiers changed from: private */
    public String showType = "house";
    private RadioGroup tab_group;
    private RadioButton tab_rent;
    private RadioButton tab_sell;
    /* access modifiers changed from: private */
    public TextView tv_nodata;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_common_layout);
        this.context = this;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_last_related);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyContactsActivity.this.finish();
            }
        });
        this.head_view.getBtn_right().setVisibility(4);
        this.listView = getListView();
        this.adapter = new ContactsHouseListAdapter(this.context, this.app, (NewHouseApplication) this.mApplication);
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.tab_group = (RadioGroup) findViewById(R.id.tab_group);
        this.tab_sell = (RadioButton) findViewById(R.id.bt_tab_sell);
        this.tab_rent = (RadioButton) findViewById(R.id.bt_tab_rent);
        if (AppMethods.isJustNewHouse(((NewHouseApplication) this.mApplication).getCity())) {
            this.tab_group.setVisibility(8);
            this.tab_sell.setVisibility(8);
            this.tab_rent.setVisibility(8);
        }
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.tab_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                MyContactsActivity.this.adapter.clear();
                MyContactsActivity.this.adapter.notifyDataSetChanged();
                if (checkedId == R.id.bt_tab_new) {
                    MyContactsActivity.this.showType = "house";
                    MyContactsActivity.this.refreshUI(MyContactsActivity.this.newHouseInfo);
                } else if (checkedId == R.id.bt_tab_sell) {
                    MyContactsActivity.this.showType = App.SELL;
                    MyContactsActivity.this.refreshUI(MyContactsActivity.this.sellHouseInfo);
                } else if (checkedId == R.id.bt_tab_rent) {
                    MyContactsActivity.this.showType = App.RENT;
                    MyContactsActivity.this.refreshUI(MyContactsActivity.this.rentHouseInfo);
                }
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                HouseInfo info = (HouseInfo) MyContactsActivity.this.adapter.getItem(position);
                Intent intent = new Intent();
                Class<?> cls = null;
                if (MyContactsActivity.this.showType.equals("house")) {
                    cls = NewHouseDetailActivity.class;
                    intent.putExtra("h_id", info.getNewHouse().getH_id());
                    intent.putExtra(NewHouseDetailActivity.INTENT_OUTLINE_HOUSE, info.getNewHouse());
                }
                if (MyContactsActivity.this.showType.equals(App.RENT)) {
                    cls = SecondRentDetailActivity.class;
                    intent.putExtra("id", info.getSellOrRent().getId());
                }
                if (MyContactsActivity.this.showType.equals(App.SELL)) {
                    cls = SecondSellDetailActivity.class;
                    intent.putExtra("id", info.getSellOrRent().getId());
                }
                intent.setClass(MyContactsActivity.this.context, cls);
                intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
                MyContactsActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                new AlertDialog.Builder(MyContactsActivity.this.context).setItems(new String[]{"删除"}, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            MyContactsActivity.this.delContact(position);
                        }
                    }
                }).create().show();
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshUI(List<HouseInfo> houselist) {
        this.adapter.clear();
        this.adapter.notifyDataSetChanged();
        if (houselist == null || houselist.size() <= 0) {
            this.adapter.notifyDataSetChanged();
            this.listView.setVisibility(8);
            this.nodata_layout.setVisibility(0);
            this.tv_nodata.setText((int) R.string.text_nocontact_msg);
            return;
        }
        this.adapter.addAll(houselist);
        this.adapter.notifyDataSetChanged();
        this.listView.setVisibility(0);
        this.nodata_layout.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void delContact(final int position) {
        new CommonAsyncTask<Boolean>(this.context) {
            public void onAfterDoInBackgroup(Boolean v) {
                if (v.booleanValue()) {
                    if (MyContactsActivity.this.adapter.getCount() <= 0) {
                        MyContactsActivity.this.listView.setVisibility(8);
                        MyContactsActivity.this.nodata_layout.setVisibility(0);
                        MyContactsActivity.this.tv_nodata.setText((int) R.string.text_nocontact_msg);
                    }
                    MyContactsActivity.this.adapter.notifyDataSetChanged();
                }
            }

            public Boolean onDoInBackgroup() {
                ((NewHouseApplication) this.mApplication).delCallHistory((HouseInfo) MyContactsActivity.this.adapter.getItem(position), MyContactsActivity.this.showType);
                MyContactsActivity.this.adapter.remove(position);
                return true;
            }
        }.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new GetContactListTask(this, this.showType).execute(new Object[0]);
    }

    private class GetContactListTask extends CommonAsyncTask<List<HouseInfo>> {
        String showType;

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<HouseInfo>) ((List) obj));
        }

        public GetContactListTask(Context context, String showType2) {
            super(context);
            this.loadingresid = R.string.loading;
            this.showType = showType2;
            MyContactsActivity.this.listView.setVisibility(0);
            MyContactsActivity.this.nodata_layout.setVisibility(8);
        }

        public List<HouseInfo> onDoInBackgroup() {
            try {
                MyContactsActivity.this.newHouseInfo = ((NewHouseApplication) this.mApplication).getCallHistoryWithCast("house");
                MyContactsActivity.this.sellHouseInfo = ((NewHouseApplication) this.mApplication).getCallHistoryWithCast(App.SELL);
                MyContactsActivity.this.rentHouseInfo = ((NewHouseApplication) this.mApplication).getCallHistoryWithCast(App.RENT);
                return null;
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return null;
        }

        public void onAfterDoInBackgroup(List<HouseInfo> list) {
            if (this.showType.equals("house")) {
                MyContactsActivity.this.refreshUI(MyContactsActivity.this.newHouseInfo);
            }
            if (this.showType.equals(App.SELL)) {
                MyContactsActivity.this.refreshUI(MyContactsActivity.this.sellHouseInfo);
            }
            if (this.showType.equals(App.RENT)) {
                MyContactsActivity.this.refreshUI(MyContactsActivity.this.rentHouseInfo);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
