package com.house365.newhouse.ui.common.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.adapter.BaseCacheListPagerAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.tool.IntentRedirect;

public class AdAdapter extends BaseCacheListPagerAdapter<Ad> {
    /* access modifiers changed from: private */
    public int ad_type;

    public AdAdapter(Context context, int ad_type2) {
        super(context);
        this.ad_type = ad_type2;
    }

    public View getAdapterView(PagerAdapter adapter, View container, int position) {
        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(-1, -1);
        ImageView iv = new ImageView(this.context);
        iv.setLayoutParams(mParams);
        final Ad ad = (Ad) getItem(position);
        setCacheImage(iv, ad.getA_src(), R.drawable.bg_default_ad, 1);
        iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AdAdapter.this.ad_type == 2) {
                    HouseAnalyse.onAD(AdAdapter.this.context, "横幅", App.NEW, ad.getA_id());
                    IntentRedirect.adLink(AdAdapter.this.ad_type, ad, AdAdapter.this.context);
                } else if (AdAdapter.this.ad_type == 3) {
                    HouseAnalyse.onAD(AdAdapter.this.context, "横幅", App.SELL, ad.getA_id());
                    IntentRedirect.adLink(AdAdapter.this.ad_type, ad, AdAdapter.this.context);
                } else if (AdAdapter.this.ad_type == 4) {
                    HouseAnalyse.onAD(AdAdapter.this.context, "横幅", App.RENT, ad.getA_id());
                    IntentRedirect.adLink(AdAdapter.this.ad_type, ad, AdAdapter.this.context);
                } else if (AdAdapter.this.ad_type == 5) {
                    HouseAnalyse.onAD(AdAdapter.this.context, "横幅", App.Home, ad.getA_id());
                    IntentRedirect.adLink(AdAdapter.this.ad_type, ad, AdAdapter.this.context);
                }
            }
        });
        return iv;
    }
}
