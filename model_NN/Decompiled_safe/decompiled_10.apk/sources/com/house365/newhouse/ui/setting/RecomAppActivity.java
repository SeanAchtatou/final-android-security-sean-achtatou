package com.house365.newhouse.ui.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;

public class RecomAppActivity extends BaseCommonActivity {
    public static String INTENT_LOADFILE = "loadFile";
    public static String INTENT_TITLE = LoanCalSelProActivity.INTENT_TITLE;
    private HeadNavigateView head_view;
    private String loadFile;
    private String title;
    private WebView webView;
    /* access modifiers changed from: private */
    public ProgressBar web_progress;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.webview_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.title = getIntent().getStringExtra(INTENT_TITLE);
        if (this.title != null && !TextUtils.isEmpty(this.title)) {
            this.head_view.setTvTitleText(this.title);
        }
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RecomAppActivity.this.finish();
            }
        });
        this.webView = (WebView) findViewById(R.id.webview);
        this.web_progress = (ProgressBar) findViewById(R.id.web_progress);
        this.webView.setBackgroundColor(0);
        this.webView.addJavascriptInterface(this, "someThing");
        this.webView.getSettings().setDefaultTextEncodingName("gb2312");
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                RecomAppActivity.this.web_progress.setVisibility(4);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                RecomAppActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        String loadFile2 = getIntent().getStringExtra(INTENT_LOADFILE);
        this.webView = (WebView) findViewById(R.id.webview);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.addJavascriptInterface(this, "someThing");
        this.webView.loadUrl(loadFile2);
    }
}
