package com.house365.newhouse.ui.tools;

import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.tools.LoanCalActivity;
import com.house365.newhouse.ui.util.MathUtil;
import java.util.List;

public class LoanCalMethodTwoResultActivity extends BaseCommonActivity {
    private TextView labelMonthRepayAll;
    private TextView labelMonthRepayBusiness;
    private TextView labelMonthRepayFound;
    private ListView mothRepays;
    private TextView tvTotalRateAll;
    private TextView tvTotalRepayAll;
    private TextView tvUserLoanMoneyAll;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.loan_cal_method_two_result_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        ((HeadNavigateView) findViewById(R.id.head_view)).getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoanCalMethodTwoResultActivity.this.finish();
            }
        });
        this.tvUserLoanMoneyAll = (TextView) findViewById(R.id.tvUserLoanMoneyAll);
        this.tvTotalRepayAll = (TextView) findViewById(R.id.tvTotalRepayAll);
        this.tvTotalRateAll = (TextView) findViewById(R.id.tvTotalRateAll);
        this.mothRepays = (ListView) findViewById(R.id.mothRepays);
        this.labelMonthRepayBusiness = (TextView) findViewById(R.id.labelMonthRepayBusiness);
        this.labelMonthRepayFound = (TextView) findViewById(R.id.labelMonthRepayFound);
        this.labelMonthRepayAll = (TextView) findViewById(R.id.labelMonthRepayAll);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        LoanCalResultMethodTwo loanCalResult = (LoanCalResultMethodTwo) getIntent().getSerializableExtra(LoanCalResultMethodTwo.INTENT_NAME);
        if (loanCalResult.getLoanType() == LoanCalActivity.LoanType.BUSINESS) {
            this.tvUserLoanMoneyAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyBusiness())));
            this.tvTotalRepayAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayBusiness())));
            this.tvTotalRateAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateBusiness())));
            this.labelMonthRepayBusiness.setVisibility(0);
            this.labelMonthRepayFound.setVisibility(8);
            this.labelMonthRepayAll.setVisibility(8);
        } else if (loanCalResult.getLoanType() == LoanCalActivity.LoanType.FOUND) {
            this.tvUserLoanMoneyAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getUserLoanMoneyFound())));
            this.tvTotalRepayAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRepayFound())));
            this.tvTotalRateAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(loanCalResult.getTotalRateFound())));
            this.labelMonthRepayBusiness.setVisibility(8);
            this.labelMonthRepayFound.setVisibility(0);
            this.labelMonthRepayAll.setVisibility(8);
        } else {
            this.tvUserLoanMoneyAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getUserLoanMoneyBusiness() + loanCalResult.getUserLoanMoneyFound(), 2))));
            this.tvTotalRepayAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getTotalRepayBusiness() + loanCalResult.getTotalRepayFound(), 2))));
            this.tvTotalRateAll.setText(getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(MathUtil.convertDouble(loanCalResult.getTotalRateBusiness() + loanCalResult.getTotalRateFound(), 2))));
            this.labelMonthRepayBusiness.setVisibility(0);
            this.labelMonthRepayFound.setVisibility(0);
            this.labelMonthRepayAll.setVisibility(0);
        }
        LoanCalMonthRepayAdapter adapter = new LoanCalMonthRepayAdapter(this, loanCalResult);
        this.mothRepays.setAdapter((ListAdapter) adapter);
        List ls = loanCalResult.getAdapterDataList();
        CorePreferences.DEBUG("ls.size:" + ls.size());
        adapter.addAll(ls);
        adapter.notifyDataSetChanged();
    }
}
