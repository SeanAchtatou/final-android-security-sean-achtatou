package com.house365.newhouse.ui.mapsearch;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.application.BaseApplication;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONException;
import com.house365.core.json.JSONObject;
import com.house365.core.reflect.ReflectException;
import com.house365.core.reflect.ReflectUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.MarkerRenderer;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.lazyload.LazyLoadException;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.RegionMap;
import com.house365.newhouse.ui.block.BlockDetailActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.apache.commons.lang.StringUtils;

public class HouseBlockOverlay {
    public static final int HOUSE_OVERLAY = 2;
    public static final int NODATA_TAG = 3;
    public static final int NONETWORK_TAG = 4;
    public static final int NORMAL = 0;
    public static final int PARSE_TAG = 5;
    public static final int PRESSED = 2;
    public static final int READ = 1;
    public static final int REGION_OVERLAY = 1;
    public static boolean dismiss;
    public static List<ManagedOverlayItem> houseItems;
    public static List<ManagedOverlayItem> regionItems;
    /* access modifiers changed from: private */
    public List<View> blockviews;
    private String buildarea;
    Context context;
    private float density;
    private DisplayMetrics dm;
    Handler handler;
    NinePatchDrawable[] hbDrawable = new NinePatchDrawable[this.resHb.length];
    Bitmap houseClickBit;
    Bitmap houseNormalBit;
    ManagedOverlay houseOverlay;
    ManagedOverlayItem lastItem;
    BaseApplication mApplication;
    MapController mapController;
    MapView mapView;
    OverlayManager overlayManager;
    private String price;
    Random random = new Random();
    NinePatchDrawable[] regDrawable = new NinePatchDrawable[this.resRegion.length];
    ManagedOverlay regionOverlay;
    int[] resHb = {R.drawable.bg_house_block_normal, R.drawable.bg_house_block_read, R.drawable.bg_house_block_selected};
    int[] resRegion = {R.drawable.bg_region_blue, R.drawable.bg_region_green, R.drawable.bg_region_red, R.drawable.bg_region_yellow};
    private String resource;

    public HouseBlockOverlay(Context context2, OverlayManager overlayManager2, ManagedOverlay regionOverlay2, ManagedOverlay houseOverlay2, MapController mapController2, BaseApplication mApplication2, MapView mapView2, Handler handler2) {
        this.context = context2;
        this.overlayManager = overlayManager2;
        this.houseOverlay = houseOverlay2;
        this.regionOverlay = regionOverlay2;
        this.mapController = mapController2;
        this.mApplication = mApplication2;
        this.mapView = mapView2;
        this.handler = handler2;
        this.dm = new DisplayMetrics();
        ((Activity) context2).getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        this.density = this.dm.density;
        setBitmap();
    }

    public ManagedOverlayItem getLastItem() {
        return this.lastItem;
    }

    public void setLastItem(ManagedOverlayItem lastItem2) {
        this.lastItem = lastItem2;
    }

    public List<View> getBlockviews() {
        return this.blockviews;
    }

    public void setBlockviews(List<View> blockviews2) {
        this.blockviews = blockviews2;
    }

    private void initDialog(int type, Block block, int price2, int resource2, int areaOrRent) {
        NewHouseApplication applicaiton = (NewHouseApplication) this.context.getApplicationContext();
        final BlockListDialog blockDialg = new BlockListDialog(this.context, R.style.dialog);
        blockDialg.setContentView((int) R.layout.block_house_dialog);
        blockDialg.setCancelable(true);
        blockDialg.setCanceledOnTouchOutside(true);
        Window window = blockDialg.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = 0;
        params.y = 0;
        params.width = applicaiton.getScreenWidth();
        params.height = applicaiton.getScreenHeight() / 2;
        params.gravity = 80;
        window.setAttributes(params);
        if (type == 12) {
            blockDialg.setInfoData(App.SELL, block.getId(), block.getBlockname(), price2, resource2, areaOrRent, 0);
        } else {
            blockDialg.setInfoData(App.RENT, block.getId(), block.getBlockname(), price2, resource2, 0, areaOrRent);
        }
        if (this.blockviews != null && this.blockviews.size() > 0) {
            dismiss = false;
            this.blockviews.get(0).setVisibility(8);
            this.blockviews.get(1).setVisibility(8);
            View maplayout = this.blockviews.get(2);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) maplayout.getLayoutParams();
            lp.height = applicaiton.getScreenHeight() / 2;
            maplayout.setLayoutParams(lp);
        }
        MapSearchHomeActivity.toBlockDetail = true;
        blockDialg.show();
        blockDialg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                blockDialg.dismiss();
                if (HouseBlockOverlay.this.blockviews != null && HouseBlockOverlay.this.blockviews.size() > 0) {
                    HouseBlockOverlay.dismiss = true;
                    ((View) HouseBlockOverlay.this.blockviews.get(0)).setVisibility(0);
                    ((View) HouseBlockOverlay.this.blockviews.get(1)).setVisibility(0);
                    View maplayout = (View) HouseBlockOverlay.this.blockviews.get(2);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) maplayout.getLayoutParams();
                    lp.height = -1;
                    maplayout.setLayoutParams(lp);
                }
            }
        });
    }

    public List<ManagedOverlayItem> getRegionOverlayItem(int maptype) throws LazyLoadException, UnknownHostException, NetworkUnavailableException, HtppApiException, HttpParseException {
        List<ManagedOverlayItem> items = new LinkedList<>();
        List<RegionMap> regionList = null;
        if (maptype == 11) {
            regionList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewHouseAreaCount();
        } else if (maptype == 12 || maptype == 13) {
            regionList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockAreaCount();
        }
        if (regionList != null && regionList.size() > 0) {
            for (RegionMap regionItem : regionList) {
                ManagedOverlayItem item = new ManagedOverlayItem(BaiduMapUtil.getPoint(regionItem.getA_lat(), regionItem.getA_lng()), regionItem.getA_id(), new JSONObject(regionItem).toString());
                item.setCustomRenderedDrawable(null);
                item.setTag(0);
                items.add(item);
            }
            setMarker(maptype, 1);
        }
        regionItems = items;
        return items;
    }

    public List<ManagedOverlayItem> getNewHouseOverlayItem(GeoPoint topLeft, GeoPoint bottomRight, int maptype, String price2, String channel) throws LazyLoadException, NetworkUnavailableException, HtppApiException, HttpParseException {
        return getHouseOverlayItem(topLeft, bottomRight, maptype, price2, channel, null, null);
    }

    public List<ManagedOverlayItem> getSellHouseOverlayItem(GeoPoint topLeft, GeoPoint bottomRight, int maptype, String price2, String resource2, String buildare) throws LazyLoadException, NetworkUnavailableException, HtppApiException, HttpParseException {
        return getHouseOverlayItem(topLeft, bottomRight, maptype, price2, null, resource2, buildare);
    }

    public List<ManagedOverlayItem> getRentHouseOverlayItem(GeoPoint topLeft, GeoPoint bottomRight, int maptype, String price2, String resource2, String areaOrRent) throws LazyLoadException, NetworkUnavailableException, HtppApiException, HttpParseException {
        return getHouseOverlayItem(topLeft, bottomRight, maptype, price2, null, resource2, areaOrRent);
    }

    public List<ManagedOverlayItem> getHouseOverlayItem(GeoPoint topLeft, GeoPoint bottomRight, int maptype, String price2, String channel, String resource2, String areaOrRent) throws LazyLoadException, NetworkUnavailableException, HtppApiException, HttpParseException {
        List<Block> blockList;
        LinkedList linkedList = new LinkedList();
        double y1 = ((double) topLeft.getLongitudeE6()) / 1000000.0d;
        double x1 = ((double) topLeft.getLatitudeE6()) / 1000000.0d;
        double y2 = ((double) bottomRight.getLongitudeE6()) / 1000000.0d;
        double x2 = ((double) bottomRight.getLatitudeE6()) / 1000000.0d;
        if (maptype == 11) {
            List<House> houseList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getNewHouseOfMap(x1, y1, x2, y2, price2, channel);
            if (houseList != null && houseList.size() > 0) {
                for (House house : houseList) {
                    ManagedOverlayItem managedOverlayItem = new ManagedOverlayItem(BaiduMapUtil.getPoint(house.getH_lat(), house.getH_long()), house.getH_id(), new JSONObject(house).toString());
                    managedOverlayItem.setTag(0);
                    linkedList.add(managedOverlayItem);
                }
                setMarker(maptype, 2);
            }
        } else if (maptype == 12) {
            List<Block> blockList2 = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockOfMap(App.SELL, x1, y1, x2, y2, price2, resource2, areaOrRent);
            if (blockList2 != null && blockList2.size() > 0) {
                for (Block block : blockList2) {
                    ManagedOverlayItem managedOverlayItem2 = new ManagedOverlayItem(BaiduMapUtil.getPoint(block.getLat(), block.getLng()), block.getId(), new JSONObject(block).toString());
                    managedOverlayItem2.setTag(0);
                    linkedList.add(managedOverlayItem2);
                }
                setMarker(maptype, 2);
            }
        } else if (maptype == 13 && (blockList = ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockOfMap(App.RENT, x1, y1, x2, y2, price2, resource2, areaOrRent)) != null && blockList.size() > 0) {
            for (Block block2 : blockList) {
                ManagedOverlayItem managedOverlayItem3 = new ManagedOverlayItem(BaiduMapUtil.getPoint(block2.getLat(), block2.getLng()), block2.getId(), new JSONObject(block2).toString());
                managedOverlayItem3.setTag(0);
                linkedList.add(managedOverlayItem3);
            }
            setMarker(maptype, 2);
        }
        houseItems = linkedList;
        if (linkedList == null || linkedList.size() <= 0) {
            this.handler.sendEmptyMessage(3);
        }
        return linkedList;
    }

    public void setHouseMarker(ManagedOverlayItem item, int maptype) {
        int tag = item.getTag();
        if (tag == 1) {
            setMarkerDrawble(maptype, 1, 2, item);
            item.setTag(1);
        } else if (tag == 2) {
            setMarkerDrawble(maptype, 2, 2, item);
            item.setTag(2);
        } else {
            setMarkerDrawble(maptype, 0, 2, item);
            item.setTag(0);
        }
    }

    public void setHouseOverlayTap(ManagedOverlayItem item, int maptype, int price2, int resource2, int areaOrRent) {
        String blockSnippet = item.getSnippet();
        if (maptype == 11) {
            try {
                String h_id = ((House) ReflectUtil.copy(House.class, new JSONObject(blockSnippet))).getH_id();
                if (!TextUtils.isEmpty(h_id)) {
                    Intent intent = new Intent(this.context, NewHouseDetailActivity.class);
                    intent.putExtra("h_id", h_id);
                    this.context.startActivity(intent);
                }
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if (maptype == 12 || maptype == 13) {
            try {
                Block block = (Block) ReflectUtil.copy(Block.class, new JSONObject(blockSnippet));
                if (block != null && !TextUtils.isEmpty(block.getId())) {
                    initDialog(maptype, block, price2, resource2, areaOrRent);
                }
            } catch (ReflectException e3) {
                e3.printStackTrace();
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void setLineOverlayTap(ManagedOverlayItem item, int maptype) {
        String blockSnippet = item.getSnippet();
        if (maptype == 11) {
            try {
                String h_id = ((House) ReflectUtil.copy(House.class, new JSONObject(blockSnippet))).getH_id();
                if (!TextUtils.isEmpty(h_id)) {
                    Intent intent = new Intent(this.context, NewHouseDetailActivity.class);
                    intent.putExtra("h_id", h_id);
                    this.context.startActivity(intent);
                }
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if (maptype == 12 || maptype == 13) {
            try {
                Block block = (Block) ReflectUtil.copy(Block.class, new JSONObject(blockSnippet));
                if (block != null && !TextUtils.isEmpty(block.getId())) {
                    Intent intent2 = new Intent(this.context, BlockDetailActivity.class);
                    intent2.putExtra(BlockDetailActivity.INTENT_RENT_TYPE, 1);
                    intent2.putExtra(BlockDetailActivity.INTENT_BLOCKID, block.getId());
                    this.context.startActivity(intent2);
                }
            } catch (ReflectException e3) {
                e3.printStackTrace();
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
        }
    }

    public void setBitmap() {
        for (int i = 0; i < this.resRegion.length; i++) {
            this.regDrawable[i] = (NinePatchDrawable) this.context.getResources().getDrawable(this.resRegion[i]);
        }
        for (int i2 = 0; i2 < this.resHb.length; i2++) {
            this.hbDrawable[i2] = (NinePatchDrawable) this.context.getResources().getDrawable(this.resHb[i2]);
        }
    }

    public Drawable setMarkerDrawble(int maptype, int clicked, int regionShow, ManagedOverlayItem item) {
        Block second;
        Bitmap bitmap = null;
        if (regionShow == 1) {
            try {
                RegionMap regionItem = (RegionMap) ReflectUtil.copy(RegionMap.class, new JSONObject(item.getSnippet()));
                if (regionItem != null && !TextUtils.isEmpty(regionItem.getA_name())) {
                    NinePatchDrawable drawable = this.regDrawable[this.random.nextInt(3)];
                    String textTitle = new StringBuilder(String.valueOf(regionItem.getA_house_count())).toString();
                    String unit = StringUtils.EMPTY;
                    if (maptype == 11) {
                        unit = "个楼盘";
                    } else if (maptype == 12 || maptype == 13) {
                        unit = "个小区";
                    }
                    String textTitle2 = String.valueOf(textTitle) + unit;
                    String r_name = regionItem.getA_name();
                    int dwidth = drawable.getIntrinsicWidth();
                    int dHeight = drawable.getIntrinsicHeight();
                    Paint fontPaint = new Paint();
                    fontPaint.setTextSize((float) ViewUtil.dip2pix(this.dm, 10.0f));
                    fontPaint.setAntiAlias(true);
                    fontPaint.setColor(-1);
                    Rect fontRect = new Rect();
                    fontPaint.getTextBounds(textTitle2, 0, textTitle2.length(), fontRect);
                    int fwidth = fontRect.width();
                    int fheight = fontRect.height();
                    int needWith = fwidth + 20;
                    if (needWith < dwidth) {
                        needWith = dwidth;
                    }
                    bitmap = Bitmap.createBitmap(needWith, dHeight, Bitmap.Config.ARGB_4444);
                    Canvas canvas = new Canvas(bitmap);
                    drawable.setBounds(0, 0, needWith, dHeight);
                    drawable.draw(canvas);
                    canvas.drawText(textTitle2, (float) 10, (float) ViewUtil.dip2pix(this.dm, 12.0f), fontPaint);
                    fontPaint.setColor(-16777216);
                    if (!TextUtils.isEmpty(r_name)) {
                        if (r_name.length() > 2) {
                            r_name = r_name.substring(0, 2);
                        }
                        fontPaint.getTextBounds(r_name, 0, r_name.length(), fontRect);
                        canvas.drawText(r_name, (float) ((needWith - fontRect.width()) / 2), (float) (((dHeight - fheight) / 3) * 2), fontPaint);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                bitmap = null;
            }
        } else if (regionShow == 2) {
            if (maptype == 11) {
                try {
                    House house = (House) ReflectUtil.copy(House.class, new JSONObject(item.getSnippet()));
                    if (house != null && !TextUtils.isEmpty(house.getH_id())) {
                        NinePatchDrawable drawable2 = this.hbDrawable[clicked];
                        String h_name = house.getH_name();
                        int dwidth2 = drawable2.getIntrinsicWidth();
                        int dheight = drawable2.getIntrinsicHeight();
                        Paint fontPaint2 = new Paint();
                        fontPaint2.setTextSize((float) ViewUtil.dip2pix(this.dm, 10.0f));
                        fontPaint2.setAntiAlias(true);
                        fontPaint2.setColor(-1);
                        Rect fontRect2 = new Rect();
                        fontPaint2.getTextBounds(h_name, 0, h_name.length(), fontRect2);
                        int fwidth2 = fontRect2.width();
                        int height = fontRect2.height();
                        int needWith2 = fwidth2 + 20;
                        if (needWith2 < dwidth2) {
                            needWith2 = dwidth2;
                        }
                        bitmap = Bitmap.createBitmap(needWith2, dheight, Bitmap.Config.ARGB_4444);
                        Canvas canvas2 = new Canvas(bitmap);
                        drawable2.setBounds(0, 0, needWith2, dheight);
                        drawable2.draw(canvas2);
                        canvas2.drawText(h_name, (float) 10, (float) ViewUtil.dip2pix(this.dm, 15.0f), fontPaint2);
                        if (clicked == 1) {
                            fontPaint2.setColor(-7829368);
                        } else if (clicked == 2) {
                            fontPaint2.setColor(-65536);
                        } else {
                            fontPaint2.setColor(this.context.getResources().getColor(R.color.menu_3));
                        }
                        canvas2.drawText(house.getH_price(), (float) 10, (float) (dheight - ViewUtil.dip2pix(this.dm, 10.0f)), fontPaint2);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if ((maptype == 12 || maptype == 13) && (second = (Block) ReflectUtil.copy(Block.class, new JSONObject(item.getSnippet()))) != null && !TextUtils.isEmpty(second.getId())) {
                NinePatchDrawable drawable3 = this.hbDrawable[clicked];
                String b_name = second.getBlockname();
                int dwidth3 = drawable3.getIntrinsicWidth();
                int dheight2 = drawable3.getIntrinsicHeight();
                Paint fontPaint3 = new Paint();
                fontPaint3.setTextSize((float) ViewUtil.dip2pix(this.dm, 10.0f));
                fontPaint3.setAntiAlias(true);
                fontPaint3.setColor(-1);
                Rect fontRect3 = new Rect();
                fontPaint3.getTextBounds(b_name, 0, b_name.length(), fontRect3);
                int fwidth3 = fontRect3.width();
                int height2 = fontRect3.height();
                int needWith3 = fwidth3 + 20;
                if (needWith3 < dwidth3) {
                    needWith3 = dwidth3;
                }
                bitmap = Bitmap.createBitmap(needWith3, dheight2, Bitmap.Config.ARGB_4444);
                Canvas canvas3 = new Canvas(bitmap);
                drawable3.setBounds(0, 0, needWith3, dheight2);
                drawable3.draw(canvas3);
                canvas3.drawText(b_name, (float) 10, (float) ViewUtil.dip2pix(this.dm, 15.0f), fontPaint3);
                if (clicked == 1) {
                    fontPaint3.setColor(-7829368);
                } else if (clicked == 2) {
                    fontPaint3.setColor(-65536);
                } else {
                    fontPaint3.setColor(this.context.getResources().getColor(R.color.menu_3));
                }
                if (maptype == 12) {
                    canvas3.drawText(String.valueOf(second.getSellaverprice()) + "元/平", (float) 10, (float) (dheight2 - ViewUtil.dip2pix(this.dm, 10.0f)), fontPaint3);
                } else {
                    canvas3.drawText(String.valueOf(second.getRentcount()) + "套", (float) 10, (float) (dheight2 - ViewUtil.dip2pix(this.dm, 10.0f)), fontPaint3);
                }
            }
        }
        if (bitmap == null) {
            return null;
        }
        BitmapDrawable bd = new BitmapDrawable(this.context.getResources(), bitmap);
        bd.setBounds((-bd.getIntrinsicWidth()) / 2, -bd.getIntrinsicHeight(), bd.getIntrinsicWidth() / 2, 0);
        item.setCustomRenderedDrawable(bd);
        return bd;
    }

    public void setMarker(final int maptype, final int regionShow) {
        MarkerRenderer render = new MarkerRenderer() {
            public Drawable render(ManagedOverlayItem item, Drawable defaultMarker, int bitState) {
                if (item.getCustomRenderedDrawable() != null) {
                    return item.getCustomRenderedDrawable();
                }
                return HouseBlockOverlay.this.setMarkerDrawble(maptype, 0, regionShow, item);
            }
        };
        if (regionShow == 1 && this.regionOverlay != null) {
            this.regionOverlay.setCustomMarkerRenderer(render);
        } else if (regionShow == 2) {
            this.houseOverlay.setCustomMarkerRenderer(render);
        }
    }
}
