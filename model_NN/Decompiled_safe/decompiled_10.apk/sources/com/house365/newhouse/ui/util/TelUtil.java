package com.house365.newhouse.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.util.DialogUtil;
import com.house365.newhouse.R;
import org.apache.commons.lang.StringUtils;

public class TelUtil {
    public static void getCallIntent(final String mobile, final Context context, final String type) {
        DialogUtil.showConfrimDialog(context, (int) R.string.text_call_dialog_title, (int) R.string.text_call_dialog_msg, (int) R.string.text_apply_submit, (int) R.string.text_apply_cancel, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                HouseAnalyse.onCall(context, mobile, type);
                String callUri = "tel:";
                if (mobile != null) {
                    callUri = String.valueOf(callUri) + TelUtil.filterTel(mobile);
                }
                Intent mIntent = new Intent("android.intent.action.CALL", Uri.parse(callUri));
                mIntent.addCategory("android.intent.category.DEFAULT");
                ((Activity) context).startActivity(mIntent);
            }

            public void onNegative(DialogInterface dialog) {
            }
        });
    }

    public static String filterTel(String tel) {
        if (StringUtils.EMPTY.equals(tel) || "暂无".equals(tel) || "待定".equals(tel)) {
            return "4008";
        }
        String tel2 = tel.replace("转", ",").replace("-", StringUtils.EMPTY).replace(" ", StringUtils.EMPTY);
        if (tel2.contains("/")) {
            return tel2.split("/")[0];
        }
        return tel2;
    }
}
