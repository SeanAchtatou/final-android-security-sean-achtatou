package com.house365.newhouse.ui.newhome;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.House;

public class HouseTrafficActivity extends BaseCommonActivity {
    private LinearLayout detail_info_bundled;
    private LinearLayout detail_info_traffic;
    private TextView h_bundled;
    private TextView h_traffic;
    private HeadNavigateView head_view;
    private House house;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_traffic);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseTrafficActivity.this.finish();
            }
        });
        this.detail_info_bundled = (LinearLayout) findViewById(R.id.detail_info_bundled);
        this.detail_info_traffic = (LinearLayout) findViewById(R.id.detail_info_traffic);
        this.h_traffic = (TextView) findViewById(R.id.h_traffic);
        this.h_bundled = (TextView) findViewById(R.id.h_bundled);
        this.detail_info_traffic.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.house = (House) getIntent().getSerializableExtra("house");
        this.head_view.setTvTitleText(this.house.getH_name());
        if (!TextUtils.isEmpty(this.house.getH_traffic())) {
            this.h_traffic.setText(Html.fromHtml(this.house.getH_traffic()));
        }
        this.detail_info_bundled.setVisibility(0);
        if (!TextUtils.isEmpty(this.house.getH_bundled())) {
            this.h_bundled.setText(Html.fromHtml(this.house.getH_bundled()));
        }
    }
}
