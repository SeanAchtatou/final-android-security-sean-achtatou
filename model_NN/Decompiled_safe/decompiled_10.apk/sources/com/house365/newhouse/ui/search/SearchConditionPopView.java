package com.house365.newhouse.ui.search;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SearchConditionPopView {
    public static final int BUS_CHOSED = 3;
    public static final String INTENT_FROM_CONDITION = "chose_condition";
    public static final String INTENT_SEARCH_TYPE = "search_type";
    public static final int NEAR_DISTANCE = 10;
    public static final int NEW_CHANNEL = 5;
    public static final int NEW_PRICE = 4;
    public static final int ORDER = 0;
    public static final int REGION_CHOSED = 1;
    public static final int RENT_PRICE = 9;
    public static final int RENT_TYPE = 11;
    public static final int SECOND_INFOFROM = 8;
    public static final int SELL_AREA = 7;
    public static final int SELL_PRICE = 6;
    public static final int SUBWAY_CHOSED = 2;
    /* access modifiers changed from: private */
    public View black_alpha_view;
    /* access modifiers changed from: private */
    public int choseType;
    private RadioGroup condition_group;
    private View contentView;
    private Context context;
    /* access modifiers changed from: private */
    public DefineArrayAdapter<String> expandAdapter;
    private List<String> expandData;
    private ListView expand_list;
    /* access modifiers changed from: private */
    public String firstExpand;
    /* access modifiers changed from: private */
    public String firstGrade;
    /* access modifiers changed from: private */
    public boolean fromMapSearch = false;
    /* access modifiers changed from: private */
    public DefineArrayAdapter<String> gradeAdapter;
    private List<String> gradeData;
    private ListView grade_list;
    private int height;
    /* access modifiers changed from: private */
    public boolean init = true;
    private LinkedHashMap<String, List<String>> map = new LinkedHashMap<>();
    private View parentView;
    /* access modifiers changed from: private */
    public MyPopView popWindow;
    private RadioButton rb_region;
    private RadioButton rb_subway;
    private int[] resData = {R.string.text_search_orderby, R.string.text_region_title, R.string.text_subway_title, R.string.btn_bus_text, R.string.text_search_price, R.string.text_search_channel, R.string.text_total_price_title, R.string.text_rent_buildarea_title, R.string.text_resource_title, R.string.text_rent_title, R.string.text_search_distance, R.string.text_rent_type_title};
    /* access modifiers changed from: private */
    public String search_type;
    private boolean showBottom;
    private TextView showDataView;
    private boolean showGroup;
    private boolean showOnView = true;
    private int width;

    public SearchConditionPopView(Context context2, View black_alpha_view2, int width2, int height2) {
        this.context = context2;
        this.width = width2;
        this.height = height2;
        this.black_alpha_view = black_alpha_view2;
        initPopWindow();
    }

    public void setSearch_type(String search_type2) {
        this.search_type = search_type2;
    }

    public void setShowOnView(boolean showOnView2) {
        this.showOnView = showOnView2;
    }

    public RadioGroup getCondition_group() {
        return this.condition_group;
    }

    public void setShowDataView(TextView showDataView2) {
        this.showDataView = showDataView2;
    }

    public boolean isFromMapSearch() {
        return this.fromMapSearch;
    }

    public void setFromMapSearch(boolean fromMapSearch2) {
        this.fromMapSearch = fromMapSearch2;
    }

    public void setShowGroup(boolean showGroup2) {
        this.showGroup = showGroup2;
        if (showGroup2) {
            this.init = true;
            this.condition_group.setVisibility(0);
            return;
        }
        this.condition_group.setVisibility(8);
    }

    private void initPopWindow() {
        this.popWindow = new MyPopView(this.context);
        this.contentView = LayoutInflater.from(this.context).inflate((int) R.layout.search_condition_pop, (ViewGroup) null);
        this.condition_group = (RadioGroup) this.contentView.findViewById(R.id.condition_group);
        this.rb_region = (RadioButton) this.contentView.findViewById(R.id.rb_region);
        this.rb_subway = (RadioButton) this.contentView.findViewById(R.id.rb_subway);
        this.grade_list = (ListView) this.contentView.findViewById(R.id.grade_list);
        this.expand_list = (ListView) this.contentView.findViewById(R.id.expand_list);
        this.popWindow.setAnimationStyle(R.style.AnimationFade);
        this.popWindow.setWidth(this.width);
        this.popWindow.setHeight(this.height);
        this.popWindow.setContentView(this.contentView);
        this.popWindow.setFocusable(true);
        this.popWindow.setTouchable(true);
        this.popWindow.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_grade_normal));
        this.popWindow.setOutsideTouchable(false);
        this.grade_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SearchConditionPopView.this.init = false;
                SearchConditionPopView.this.firstGrade = (String) SearchConditionPopView.this.gradeAdapter.getItem(position);
                SearchConditionPopView.this.setExpandData(SearchConditionPopView.this.firstGrade);
                SearchConditionPopView.this.gradeAdapter.setChosedGrade(SearchConditionPopView.this.firstGrade);
                SearchConditionPopView.this.gradeAdapter.notifyDataSetChanged();
                if (SearchConditionPopView.this.search_type.equals(ActionCode.NEW_SEARCH)) {
                    if (SearchConditionPopView.this.choseType != 2) {
                        SearchConditionPopView.this.sendBroad();
                    }
                } else if (!SearchConditionPopView.this.search_type.equals(ActionCode.SELL_SEARCH) && !SearchConditionPopView.this.search_type.equals(ActionCode.RENT_SEARCH)) {
                } else {
                    if (SearchConditionPopView.this.fromMapSearch) {
                        if (SearchConditionPopView.this.choseType != 2) {
                            SearchConditionPopView.this.sendBroad();
                        }
                    } else if (SearchConditionPopView.this.choseType != 1 && SearchConditionPopView.this.choseType != 2) {
                        SearchConditionPopView.this.sendBroad();
                    }
                }
            }
        });
        this.expand_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SearchConditionPopView.this.setMarked(String.valueOf(SearchConditionPopView.this.firstGrade) + "+" + ((String) SearchConditionPopView.this.expandAdapter.getItem(position)));
                SearchConditionPopView.this.firstExpand = (String) SearchConditionPopView.this.expandAdapter.getItem(position);
                SearchConditionPopView.this.expandAdapter.setChosedGrade(SearchConditionPopView.this.firstExpand);
                SearchConditionPopView.this.expandAdapter.notifyDataSetChanged();
                SearchConditionPopView.this.sendBroad();
                if (SearchConditionPopView.this.popWindow.isShowing()) {
                    SearchConditionPopView.this.popWindow.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void sendBroad() {
        Intent intent = new Intent(ActionCode.INTENT_SEARCH_CONDITION_FINISH);
        intent.putExtra(INTENT_FROM_CONDITION, this.choseType);
        intent.putExtra(INTENT_SEARCH_TYPE, this.search_type);
        this.context.sendBroadcast(intent);
        this.firstExpand = null;
    }

    public void setGradeData(List<String> gradeData2, TextView showDataView2, String chosed, int choseType2) {
        this.gradeData = gradeData2;
        this.showDataView = showDataView2;
        this.choseType = choseType2;
        if (gradeData2 != null && gradeData2.size() > 0) {
            this.gradeAdapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, gradeData2);
            this.gradeAdapter.setGratiy(17);
            if (TextUtils.isEmpty(chosed)) {
                this.firstGrade = gradeData2.get(0);
            } else {
                this.firstGrade = chosed;
            }
            this.gradeAdapter.setChosedGrade(this.firstGrade);
            this.gradeAdapter.setSingle(true);
            this.grade_list.setAdapter((ListAdapter) this.gradeAdapter);
            setExpandData(this.firstGrade);
            this.init = false;
            show();
        }
    }

    public void setGradeData(LinkedHashMap<String, LinkedHashMap<String, Station>> mapData, TextView showDataView2, String chosed, int choseType2) {
        this.showDataView = showDataView2;
        this.choseType = choseType2;
        this.gradeData = AppMethods.mapKeyTolistSubWay(mapData, false);
        if (this.gradeData != null && this.gradeData.size() > 0) {
            for (Map.Entry entry : mapData.entrySet()) {
                this.map.put(entry.getKey().toString(), AppMethods.mapKeyTolistSubWay((LinkedHashMap) entry.getValue(), false));
            }
            setDoubleCondition(chosed);
        }
    }

    public void setGradeData(TextView showDataView2, String chosed, LinkedHashMap<String, LinkedHashMap<String, String>> districtData, int choseType2) {
        this.showDataView = showDataView2;
        this.choseType = choseType2;
        this.gradeData = AppMethods.mapKeyTolistSubWay(districtData, false);
        if (this.gradeData != null && this.gradeData.size() > 0) {
            for (Map.Entry entry : districtData.entrySet()) {
                this.map.put(entry.getKey().toString(), AppMethods.mapKeyTolistSubWay((LinkedHashMap) entry.getValue(), false));
            }
            setDoubleCondition(chosed);
        }
    }

    public void setRegionOrSubway(TextView showDataView2) {
        if (getTagFlag(showDataView2.getTag()) == 2) {
            this.rb_subway.setChecked(true);
        } else {
            this.rb_region.setChecked(true);
        }
    }

    private void setDoubleCondition(String chosed) {
        this.gradeAdapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, this.gradeData);
        this.gradeAdapter.setGratiy(17);
        if (TextUtils.isEmpty(chosed) || chosed.indexOf("+") == -1) {
            this.firstGrade = this.gradeData.get(0);
        } else {
            this.firstGrade = chosed.substring(0, chosed.indexOf("+"));
            this.firstExpand = chosed.substring(chosed.indexOf("+") + 1, chosed.length());
        }
        this.gradeAdapter.setChosedGrade(this.firstGrade);
        this.grade_list.setAdapter((ListAdapter) this.gradeAdapter);
        setExpandData(this.firstGrade);
        this.init = false;
        show();
    }

    /* access modifiers changed from: private */
    public void setExpandData(String first) {
        if (this.map == null || this.map.isEmpty() || TextUtils.isEmpty(first)) {
            showGrade(first, false);
            return;
        }
        this.expandData = this.map.get(first);
        if (this.expandData == null || this.expandData.size() <= 0) {
            showGrade(first, false);
            return;
        }
        this.expandAdapter = new DefineArrayAdapter<>(this.context, (int) R.id.h_name, this.expandData);
        this.expandAdapter.setGratiy(3);
        this.expandAdapter.setExpand(this.firstExpand);
        this.expand_list.setAdapter((ListAdapter) this.expandAdapter);
        this.expand_list.setVisibility(0);
    }

    private void showGrade(String first, boolean hasExpand) {
        this.expand_list.setVisibility(8);
        if (!this.init) {
            setMarked(first);
            if (this.popWindow.isShowing()) {
                this.popWindow.dismiss();
            }
        }
    }

    public static int getIndex(List<String> data, String str) {
        for (int i = 0; i < data.size(); i++) {
            String temp = data.get(i);
            if (!TextUtils.isEmpty(temp) && temp.equals(str)) {
                return i;
            }
        }
        return 0;
    }

    public void setShowBottom(boolean showBottom2) {
        this.showBottom = showBottom2;
    }

    public void setParentView(View parentView2) {
        this.parentView = parentView2;
    }

    /* access modifiers changed from: private */
    public void setMarked(String data) {
        if (this.showOnView) {
            if (this.showBottom) {
                this.showDataView.setText(data);
            } else if (!data.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE) && !data.equals(ActionCode.PREFERENCE_SEARCH_DEFAULT)) {
                this.showDataView.setText(data);
            } else if (this.choseType >= 0 && this.choseType < this.resData.length) {
                this.showDataView.setText(this.resData[this.choseType]);
            }
            this.showDataView.setTag(String.valueOf(this.choseType) + ":" + data);
            return;
        }
        this.showDataView.setTag(data);
    }

    public static int getTagFlag(Object tagObj) {
        if (tagObj == null) {
            return 0;
        }
        String tag = tagObj.toString();
        if (tag.indexOf(":") != -1) {
            return Integer.parseInt(tag.substring(0, tag.indexOf(":")));
        }
        return 0;
    }

    public static String getTagData(Object tagObj) {
        if (tagObj == null) {
            return ActionCode.PREFERENCE_SEARCH_NOCHOSE;
        }
        String tag = tagObj.toString();
        if (tag.indexOf(":") != -1) {
            return tag.substring(tag.indexOf(":") + 1, tag.length());
        }
        return ActionCode.PREFERENCE_SEARCH_NOCHOSE;
    }

    public static String setStringValue(View view, String oldVlaue, String key, HouseBaseInfo baseInfo) {
        String value = oldVlaue;
        if (view.getTag() == null) {
            return value;
        }
        String tag = view.getTag().toString();
        if (tag.indexOf(":") != -1) {
            return (String) baseInfo.getConfig().get(key).get(tag.substring(tag.indexOf(":") + 1, tag.length()));
        }
        return value;
    }

    public static int setIntValue(View view, int oldVlaue, String key, HouseBaseInfo baseInfo) {
        int value = oldVlaue;
        if (view.getTag() == null) {
            return value;
        }
        String tag = view.getTag().toString();
        if (tag.indexOf(":") != -1) {
            return getIndex(baseInfo.getSell_config().get(key), tag.substring(tag.indexOf(":") + 1, tag.length()));
        }
        return value;
    }

    public static void refreshViews(List<View> views, List<TextView> textViews, View selectedView, TextView textView, Context con) {
        for (int i = 0; i < views.size(); i++) {
            if (selectedView.getId() == views.get(i).getId() && textViews.get(i).getId() == textView.getId()) {
                textView.setTextColor(con.getResources().getColor(R.color.group_blue));
            } else {
                textViews.get(i).setTextColor(con.getResources().getColor(R.color.gray));
            }
        }
    }

    public void setFirstGrade(String firstGrade2) {
        this.firstGrade = firstGrade2;
    }

    public void setFirstExpand(String firstExpand2) {
        this.firstExpand = firstExpand2;
    }

    public void show() {
        if (!this.popWindow.isShowing()) {
            this.black_alpha_view.setVisibility(0);
            if (!this.showBottom || this.parentView == null) {
                this.popWindow.showAsDropDown(this.showDataView, 0, 17);
            } else {
                this.popWindow.showAtLocation(this.parentView, 80, 0, 0);
            }
        }
    }

    class MyPopView extends PopupWindow {
        public MyPopView(Context context) {
            super(context);
        }

        public void dismiss() {
            super.dismiss();
            SearchConditionPopView.this.black_alpha_view.setVisibility(8);
        }
    }
}
