package com.house365.newhouse.ui.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.ActivityUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.secondrent.SecondRentResultActivity;
import com.house365.newhouse.ui.secondsell.AddBlockActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SecondBusExpandleActivity extends BaseCommonActivity {
    public static final int FROM_TYPE = 1;
    public static HouseBaseInfo info;
    /* access modifiers changed from: private */
    public SimpleAdapter adapter;
    /* access modifiers changed from: private */
    public int busvalue;
    /* access modifiers changed from: private */
    public int from_litter_home;
    /* access modifiers changed from: private */
    public int fromtype;
    private HeadNavigateView head_View;
    /* access modifiers changed from: private */
    public String id;
    /* access modifiers changed from: private */
    public ListView listView;
    protected String search_type;
    private TextView text_bus_name;
    /* access modifiers changed from: private */
    public String txt_bus_name;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.bus_search_normal_choose);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.from_litter_home = getIntent().getIntExtra("FROM_LITTER_HOME", 0);
        this.head_View = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_View.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondBusExpandleActivity.this.finish();
            }
        });
        this.head_View.getBtn_right().setText((int) R.string.text_search_choose_reset);
        this.head_View.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.head_View.setTvTitleText((int) R.string.btn_bus_text);
        this.id = getIntent().getStringExtra("bus_id");
        this.fromtype = getIntent().getIntExtra(AddBlockActivity.INTENT_FROM_TYPE, 0);
        this.search_type = getIntent().getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
        this.text_bus_name = (TextView) findViewById(R.id.bus_name);
        this.txt_bus_name = getIntent().getStringExtra(SecondRentSearchActivity.INTENT_BUS_NAME);
        this.text_bus_name.setText(this.txt_bus_name);
        this.listView = (ListView) findViewById(R.id.listview);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        new GetBusInfoTask(this, R.string.bus_info_load).execute(new Object[0]);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int id, long position) {
                String bus_id = ((TextView) v.findViewById(R.id.h_id)).getText().toString();
                SecondBusExpandleActivity.this.busvalue = Integer.parseInt(bus_id);
                String busname = ((TextView) v.findViewById(R.id.h_name)).getText().toString();
                Intent intent = new Intent();
                String str = new StringBuilder(String.valueOf(SecondBusExpandleActivity.this.txt_bus_name)).toString();
                String bus = str.substring(0, str.indexOf("("));
                if (SecondBusExpandleActivity.this.fromtype == 1) {
                    Intent broad_intent = new Intent(ActionCode.UI_CHANGE);
                    broad_intent.putExtra(SecondRentSearchActivity.INTENT_BUS_NAME, String.valueOf(bus) + "+" + busname);
                    broad_intent.putExtra(SecondRentSearchActivity.INTENT_BUS_VALUE, Integer.parseInt(bus_id));
                    SecondBusExpandleActivity.this.sendBroadcast(broad_intent);
                    SecondBusExpandleActivity.this.sendBroadcast(new Intent(ActionCode.CHOOSE_HOME_UI_CHANGE));
                    SecondBusExpandleActivity.this.finish();
                    return;
                }
                intent.setClass(SecondBusExpandleActivity.this, SecondRentResultActivity.class);
                intent.putExtra("bus_id", SecondBusExpandleActivity.this.busvalue);
                intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, 1);
                intent.putExtra("priceView_from", StringUtils.EMPTY);
                intent.putExtra("FROM_SUBWAY", -3);
                intent.putExtra("FROM_LITTER_HOME", SecondBusExpandleActivity.this.from_litter_home);
                SecondBusExpandleActivity.this.startActivity(intent);
            }
        });
    }

    public class GetBusInfoTask extends CommonAsyncTask<List<SecondHouse>> {
        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<SecondHouse>) ((List) obj));
        }

        public GetBusInfoTask(Context context, int resid) {
            super(context, resid);
        }

        public List<SecondHouse> onDoInBackgroup() {
            try {
                return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBusConfigInfo(SecondBusExpandleActivity.this.id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void onAfterDoInBackgroup(List<SecondHouse> houseList) {
            if (houseList == null || houseList.size() <= 0) {
                ActivityUtil.showToast(this.context, (int) R.string.bus_info_load_error);
                return;
            }
            List<HashMap<String, String>> search_data = new ArrayList<>();
            for (int i = 0; i < houseList.size(); i++) {
                HashMap<String, String> item = new HashMap<>();
                item.put("h_id", houseList.get(i).getId());
                item.put("h_name", houseList.get(i).getName());
                search_data.add(item);
                String busid = houseList.get(i).getId().toString();
                CorePreferences.DEBUG("busid" + busid + houseList.get(i).getName().toString());
            }
            SecondBusExpandleActivity.this.adapter = new SimpleAdapter(SecondBusExpandleActivity.this, search_data, R.layout.bussearch_item, new String[]{"h_id", "h_name"}, new int[]{R.id.h_id, R.id.h_name});
            SecondBusExpandleActivity.this.adapter.notifyDataSetChanged();
            SecondBusExpandleActivity.this.listView.setAdapter((ListAdapter) SecondBusExpandleActivity.this.adapter);
        }
    }
}
