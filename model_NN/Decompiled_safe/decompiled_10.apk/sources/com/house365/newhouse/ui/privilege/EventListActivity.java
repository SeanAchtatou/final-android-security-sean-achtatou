package com.house365.newhouse.ui.privilege;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.task.GetEventInfoTask;
import com.house365.newhouse.task.GetItemSaleTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.privilege.adapter.HouseGroupAdapter;

public class EventListActivity extends BaseListActivity {
    public static final String INTENT_ID = "e_id";
    public static final String INTENT_TYPE = "e_type";
    public static String ctype;
    /* access modifiers changed from: private */
    public HouseGroupAdapter adapter;
    private RadioButton bt_newhouse;
    private RadioButton bt_sell;
    public String e_id;
    private HeadNavigateView head_view;
    private RefreshInfo listRefresh = new RefreshInfo();
    PullToRefreshListView listView;
    private View nodata_layout;
    private BroadcastReceiver sign = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            EventListActivity.this.refreshData();
            EventListActivity.this.adapter.clear();
            EventListActivity.this.adapter.notifyDataSetChanged();
        }
    };
    private RadioGroup tab_event_group;
    private TextView tv_nodata;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        registerReceiver(this.sign, new IntentFilter(ActionCode.INTENT_ACTION_SIGN));
        setContentView((int) R.layout.event_list);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getTv_center().setText(getResources().getString(R.string.text_title_group));
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EventListActivity.this.finish();
            }
        });
        ctype = "event";
        this.tab_event_group = (RadioGroup) findViewById(R.id.tab_event_group);
        this.bt_newhouse = (RadioButton) findViewById(R.id.bt_newhouse);
        this.bt_sell = (RadioButton) findViewById(R.id.bt_sell);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.tv_nodata.setText((int) R.string.text_event_nodata);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.listView = (PullToRefreshListView) findViewById(R.id.list);
        ((ListView) this.listView.getRefreshableView()).setDivider(new BitmapDrawable());
        ((ListView) this.listView.getRefreshableView()).setDividerHeight(54);
        this.adapter = new HouseGroupAdapter(this);
        this.listView.setAdapter(this.adapter);
        if (!((NewHouseApplication) this.mApplication).getCity().equals("nanjing")) {
            this.tab_event_group.setVisibility(8);
            this.adapter.setShowIco(true);
        } else {
            this.tab_event_group.setVisibility(0);
        }
        this.tab_event_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radiogroup, int checkId) {
                EventListActivity.this.adapter.clear();
                EventListActivity.this.adapter.notifyDataSetChanged();
                if (checkId == R.id.bt_newhouse) {
                    EventListActivity.ctype = "event";
                } else if (checkId == R.id.bt_sell) {
                    EventListActivity.ctype = App.Categroy.Event.SELL_EVENT;
                }
                EventListActivity.this.refreshData();
            }
        });
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                EventListActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                EventListActivity.this.getMoreData();
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.listRefresh.refresh = true;
        getCoupon(ctype);
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.listRefresh.refresh = false;
        getCoupon(ctype);
    }

    private void getCoupon(String type) {
        new GetItemSaleTask(this, 0, this.listView, this.listRefresh, this.adapter, type, this.nodata_layout, this.tv_nodata).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.e_id = getIntent().getStringExtra("e_id");
        String fromType = getIntent().getStringExtra(INTENT_TYPE);
        if (!TextUtils.isEmpty(fromType)) {
            ctype = fromType;
        }
        if (ctype.equals("event")) {
            this.bt_newhouse.setChecked(true);
        } else if (ctype.equals(App.Categroy.Event.SELL_EVENT)) {
            this.bt_sell.setChecked(true);
        }
        if (!TextUtils.isEmpty(this.e_id)) {
            this.tab_event_group.setVisibility(8);
            this.adapter.setShowIco(true);
            GetEventInfoTask task = new GetEventInfoTask(this, this.e_id, ctype);
            task.execute(new Object[0]);
            task.setOnFinish(new GetEventInfoTask.OnFinish() {
                public void onFinsh(Event event) {
                    EventListActivity.this.adapter.clear();
                    EventListActivity.this.adapter.addItem(event);
                    EventListActivity.this.adapter.notifyDataSetChanged();
                    EventListActivity.this.listView.setOnRefreshListener(null);
                }
            });
            return;
        }
        getCoupon(ctype);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.sign);
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }

    public void finish() {
        super.finish();
        if (!TextUtils.isEmpty(this.e_id) && getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }
}
