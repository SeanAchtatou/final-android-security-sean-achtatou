package com.house365.newhouse.ui.util;

import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.PullAccount;
import com.house365.androidpn.client.PullConfiguration;
import com.house365.androidpn.client.PullServiceManager;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.ui.apn.APNActivity;

public class PushServiceUtil {
    public void startPNService(Context context, NewHouseApplication app, int accountType, String username, String password) {
        PullServiceManager serviceManager;
        try {
            if (app.getPullServiceManager() == null) {
                serviceManager = new PullServiceManager(context, new PullConfiguration("com.house365.apn.client.taofang_notification_service", CorePreferences.getInstance(context).getCoreConfig().getAppTag(), APNActivity.class.getName(), null, R.drawable.ic_launcher, "1234567890", "mobile.house365.com", 5222));
            } else {
                serviceManager = app.getPullServiceManager();
            }
            serviceManager.setAccount(getPullAccount(context, app, accountType, username, password));
            serviceManager.startService();
            serviceManager.setNotificationBroadCastenable(false);
            serviceManager.setNotificationEnable(true);
            app.setPullServiceManager(serviceManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetPullAccountToPushServer(Context context, BaseApplication app, int accountType, String username, String password) {
        PullAccount account = getPullAccount(context, app, accountType, username, password);
        Intent intent = new Intent();
        intent.setAction(Constants.getActionPullaccountChange(context));
        intent.putExtra(Constants.ACTION_PULLACCOUNT_FIELD_ACCOUNT, account);
        context.sendBroadcast(intent);
    }

    private PullAccount getPullAccount(Context context, BaseApplication app, int accountType, String username, String password) {
        TelephonyManager telManager = (TelephonyManager) context.getSystemService("phone");
        if (2 == accountType) {
            String phoneNo = telManager.getLine1Number();
            if (phoneNo != null) {
                username = phoneNo;
            }
            return new PullAccount(username, password, 2, CorePreferences.getInstance(context).getCoreConfig().getAppTag());
        } else if (1 == accountType) {
            return new PullAccount(telManager.getDeviceId(), password, 1, CorePreferences.getInstance(context).getCoreConfig().getAppTag());
        } else {
            return new PullAccount(username, password, 3, CorePreferences.getInstance(context).getCoreConfig().getAppTag());
        }
    }
}
