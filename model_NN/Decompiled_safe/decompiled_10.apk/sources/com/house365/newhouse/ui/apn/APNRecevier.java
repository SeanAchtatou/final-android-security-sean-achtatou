package com.house365.newhouse.ui.apn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.house365.core.constant.CorePreferences;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.ui.util.PushServiceUtil;

public class APNRecevier extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        CorePreferences.ERROR("action:" + intent.getAction());
        try {
            NewHouseApplication mApplicaiton = (NewHouseApplication) context.getApplicationContext();
            if (mApplicaiton.isEnablePushNotification()) {
                PushServiceUtil pushServiceUtil = new PushServiceUtil();
                if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                    mApplicaiton.removePullServiceManager();
                }
                pushServiceUtil.startPNService(context, (NewHouseApplication) context.getApplicationContext(), 1, null, App.PULL_ACCOUNT_PASSWORD);
            }
        } catch (Exception e) {
            CorePreferences.ERROR(e);
        }
    }
}
