package com.house365.newhouse.ui.tools;

/* compiled from: LoanCalActivity */
abstract class CalBean {
    String key;

    CalBean() {
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key2) {
        this.key = key2;
    }

    public boolean equals(Object o) {
        return getKey().equals(((CalBean) o).getKey());
    }
}
