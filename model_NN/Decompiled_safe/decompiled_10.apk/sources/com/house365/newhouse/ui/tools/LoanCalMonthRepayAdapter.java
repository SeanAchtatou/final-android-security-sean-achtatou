package com.house365.newhouse.ui.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.ui.tools.LoanCalActivity;
import com.house365.newhouse.ui.util.MathUtil;

public class LoanCalMonthRepayAdapter extends BaseListAdapter<AdapterDataList> {
    private Context context;
    private LoanCalResultMethodTwo result;

    public LoanCalMonthRepayAdapter(Context context2, LoanCalResultMethodTwo result2) {
        super(context2);
        this.context = context2;
        this.result = result2;
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.item_list_loan_cal_method_two_result, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.tvMonth = (TextView) convertView.findViewById(R.id.tvMonth);
            holder.tvMonthRepayBusiness = (TextView) convertView.findViewById(R.id.tvMonthRepayBusiness);
            holder.tvMonthRepayFound = (TextView) convertView.findViewById(R.id.tvMonthRepayFound);
            holder.tvMonthRepayAll = (TextView) convertView.findViewById(R.id.tvMonthRepayAll);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        AdapterDataList data = (AdapterDataList) getItem(position);
        holder.tvMonth.setText(this.context.getResources().getString(R.string.cal_result_times, Integer.valueOf(data.getLoantime())));
        if (this.result.getLoanType() == LoanCalActivity.LoanType.BUSINESS) {
            holder.tvMonthRepayBusiness.setVisibility(0);
            holder.tvMonthRepayFound.setVisibility(8);
            holder.tvMonthRepayAll.setVisibility(8);
            holder.tvMonthRepayBusiness.setText(this.context.getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(data.getMonthRepayBusiness())));
        } else if (this.result.getLoanType() == LoanCalActivity.LoanType.FOUND) {
            holder.tvMonthRepayBusiness.setVisibility(8);
            holder.tvMonthRepayFound.setVisibility(0);
            holder.tvMonthRepayAll.setVisibility(8);
            holder.tvMonthRepayFound.setText(this.context.getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(data.getMonthRepayFound())));
        } else {
            holder.tvMonthRepayBusiness.setVisibility(0);
            holder.tvMonthRepayFound.setVisibility(0);
            holder.tvMonthRepayAll.setVisibility(0);
            holder.tvMonthRepayBusiness.setText(this.context.getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(data.getMonthRepayBusiness())));
            holder.tvMonthRepayFound.setText(this.context.getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(data.getMonthRepayFound())));
            holder.tvMonthRepayAll.setText(this.context.getResources().getString(R.string.cal_result_money, MathUtil.d2StrWith2Dec(data.getMonthRepayAll())));
        }
        return convertView;
    }

    private class ViewHolder {
        TextView tvMonth;
        TextView tvMonthRepayAll;
        TextView tvMonthRepayBusiness;
        TextView tvMonthRepayFound;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(LoanCalMonthRepayAdapter loanCalMonthRepayAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
