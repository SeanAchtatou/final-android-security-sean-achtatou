package com.house365.newhouse.ui.search;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.qrcode.CaptureActivity;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.newhome.NewHouseSearchResultActivity;
import com.house365.newhouse.ui.search.KeyWordSearch;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewSearchActivity extends BaseCommonActivity implements View.OnClickListener, KeyWordSearch.DialogDimissListener {
    public static final String INTENT_FORM_TYPE_CHOOSE = "litterChoose";
    public static final String INTENT_FROM_LITTERHOME = "litterHome";
    public static final int REFRESH_BACK = 11;
    protected static final String TAG = "NewSearchActivity";
    private View black_alpha_view;
    private Button btn_capture;
    private Button btn_finish;
    private Button cancelDialog;
    /* access modifiers changed from: private */
    public String channelvalue;
    private BroadcastReceiver chosedFinished = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String chose_from = intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
            int chose = intent.getIntExtra(SearchConditionPopView.INTENT_FROM_CONDITION, 0);
            if (chose_from.equals(ActionCode.NEW_SEARCH)) {
                NewSearchActivity.this.refreshUI(chose);
            }
        }
    };
    private Context context;
    /* access modifiers changed from: private */
    public String districtvalue;
    /* access modifiers changed from: private */
    public LinearLayout feature_layout;
    private boolean fromLitterHome;
    private HeadNavigateView head_view;
    private KeyWordSearch keyWordSearch;
    private ListView key_list;
    /* access modifiers changed from: private */
    public EditText keyword_text;
    /* access modifiers changed from: private */
    public Location mLocation;
    /* access modifiers changed from: private */
    public HouseBaseInfo newHouseConfig;
    /* access modifiers changed from: private */
    public LinearLayout newhouse_price_layout;
    private View newhouse_price_total_layout;
    private LinearLayout nodata_layout;
    private LinearLayout popLayout;
    /* access modifiers changed from: private */
    public String pricevalue;
    /* access modifiers changed from: private */
    public int radiusvalue;
    /* access modifiers changed from: private */
    public LinearLayout region_layout;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private View search_bar;
    /* access modifiers changed from: private */
    public LinearLayout subway_layout;
    /* access modifiers changed from: private */
    public View subway_total_layout;
    /* access modifiers changed from: private */
    public TextView text_feature;
    private TextView text_feature_title;
    /* access modifiers changed from: private */
    public TextView text_newhouse_price;
    /* access modifiers changed from: private */
    public TextView text_region;
    private TextView text_region_title;
    /* access modifiers changed from: private */
    public TextView text_subway;
    /* access modifiers changed from: private */
    public TextView text_type;
    private View top_line;
    /* access modifiers changed from: private */
    public LinearLayout type_layout;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 11 && resultCode == -1 && data != null && data.getExtras() != null) {
            Bundle refreshBundle = data.getExtras();
            String distritIndex = refreshBundle.getString("districtvalue");
            String channelIndex = refreshBundle.getString("channelvalue");
            String priceIndex = refreshBundle.getString("pricevalue");
            int type = SearchConditionPopView.getTagFlag(refreshBundle.getString("distsubway"));
            if (type == 1) {
                if (distritIndex != null && !distritIndex.equals("0")) {
                    this.text_region.setText(AppMethods.getMapKey(this.newHouseConfig.getConfig().get(HouseBaseInfo.DISTRICT), distritIndex));
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
            } else if (type == 2) {
                this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                this.text_subway.setText(SearchConditionPopView.getTagData(refreshBundle.getString("distsubway")));
            }
            if (channelIndex == null || channelIndex.equals("0")) {
                this.text_type.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            } else {
                this.text_type.setText(AppMethods.getMapKey(this.newHouseConfig.getConfig().get("channel"), channelIndex));
            }
            if (priceIndex == null || priceIndex.equals("0")) {
                this.text_newhouse_price.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            } else {
                this.text_newhouse_price.setText(AppMethods.getMapKey(this.newHouseConfig.getConfig().get("price"), priceIndex));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        View contentView = LayoutInflater.from(this.context).inflate((int) R.layout.new_search_layout, (ViewGroup) null);
        setContentView(contentView);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(true);
        this.searchPop.setShowGroup(false);
        this.searchPop.setParentView(contentView);
        this.searchPop.setSearch_type(ActionCode.NEW_SEARCH);
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewSearchActivity.this.finish();
            }
        });
        this.region_layout = (LinearLayout) findViewById(R.id.region_layout);
        this.subway_layout = (LinearLayout) findViewById(R.id.subway_layout);
        this.type_layout = (LinearLayout) findViewById(R.id.type_layout);
        this.newhouse_price_layout = (LinearLayout) findViewById(R.id.newhouse_price_layout);
        this.feature_layout = (LinearLayout) findViewById(R.id.feature_layout);
        this.subway_total_layout = findViewById(R.id.subway_total_layout);
        this.newhouse_price_total_layout = findViewById(R.id.newhouse_price_total_layout);
        this.top_line = findViewById(R.id.top_line);
        this.text_region = (TextView) findViewById(R.id.text_region);
        this.text_subway = (TextView) findViewById(R.id.text_subway);
        this.text_type = (TextView) findViewById(R.id.text_type);
        this.text_newhouse_price = (TextView) findViewById(R.id.text_newhouse_price);
        this.btn_finish = (Button) findViewById(R.id.btn_finish);
        this.search_bar = findViewById(R.id.search_bar_layout);
        this.keyword_text = (EditText) findViewById(R.id.keyword_text);
        this.keyword_text.setHint((int) R.string.text_newsearch_hint);
        this.btn_capture = (Button) findViewById(R.id.btn_capture);
        this.text_region_title = (TextView) findViewById(R.id.text_region_title);
        this.text_feature_title = (TextView) findViewById(R.id.text_feature_title);
        this.text_feature = (TextView) findViewById(R.id.text_feature);
        setFromLitterHome();
        if (((NewHouseApplication) this.mApplication).getCityName().equals("西安")) {
            this.text_region_title.setText((int) R.string.btn_xian_region_text);
        } else {
            this.text_region_title.setText((int) R.string.btn_region_text);
        }
        this.keyword_text.requestFocus();
        this.btn_capture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewSearchActivity.this.startActivity(new Intent(NewSearchActivity.this, CaptureActivity.class));
            }
        });
        prepareKeySearch();
    }

    private void prepareKeySearch() {
        this.cancelDialog = (Button) findViewById(R.id.btn_cancel);
        this.cancelDialog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewSearchActivity.this.popDismiss();
            }
        });
        this.popLayout = (LinearLayout) findViewById(R.id.key_search_container);
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        this.key_list = (ListView) findViewById(R.id.key_list);
        this.keyWordSearch = new KeyWordSearch(this, this.keyword_text);
        this.keyWordSearch.setSearchType(ActionCode.NEW_SEARCH);
        this.keyWordSearch.setDismissListener(this);
        this.keyWordSearch.setKey_list(this.key_list);
        this.keyWordSearch.setNodata_layout(this.nodata_layout);
        this.keyWordSearch.preparedCreate();
    }

    private void setFromLitterHome() {
        this.fromLitterHome = getIntent().getBooleanExtra("litterHome", false);
        if (this.fromLitterHome) {
            this.head_view.setVisibility(0);
            this.head_view.getTv_center().setText((int) R.string.text_newhome_title);
            return;
        }
        this.head_view.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void getChooseValue() {
        this.districtvalue = (String) this.newHouseConfig.getConfig().get(HouseBaseInfo.DISTRICT).get(this.text_region.getText().toString());
        this.channelvalue = (String) this.newHouseConfig.getConfig().get("channel").get(this.text_type.getText().toString());
        this.pricevalue = (String) this.newHouseConfig.getConfig().get("price").get(this.text_newhouse_price.getText().toString());
        String subWay = this.text_subway.getText().toString();
        if (this.text_subway.getText().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE) || subWay == null) {
            this.mLocation = null;
            this.radiusvalue = 0;
        } else if (subWay != null && subWay.indexOf("+") != -1) {
            String metro = subWay.substring(0, subWay.indexOf("+"));
            Station station = (Station) this.newHouseConfig.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()));
            this.mLocation = new Location(StringUtils.EMPTY);
            this.mLocation.setLatitude(station.getX());
            this.mLocation.setLongitude(station.getY());
            this.radiusvalue = 2500;
        }
    }

    /* access modifiers changed from: private */
    public void addNormalSearchListener(LinearLayout layout, final TextView showDataView, final ArrayList dataList, final int choseType) {
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewSearchActivity.this.searchPop.setGradeData(dataList, showDataView, showDataView.getText().toString(), choseType);
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshUI(int chose) {
        switch (chose) {
            case 1:
                if (!this.text_subway.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    return;
                }
                return;
            case 2:
                if (!this.text_region.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info == null) {
                    return;
                }
                if (info.getJsonStr().equals(NewSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    NewSearchActivity.this.setViewEnable(R.string.text_no_network);
                    return;
                }
                NewSearchActivity.this.newHouseConfig = info;
                if (AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getMetro(), false) == null) {
                    NewSearchActivity.this.subway_total_layout.setVisibility(8);
                } else {
                    NewSearchActivity.this.subway_layout.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            NewSearchActivity.this.searchPop.setGradeData(NewSearchActivity.this.newHouseConfig.getMetro(), NewSearchActivity.this.text_subway, NewSearchActivity.this.text_subway.getText().toString(), 2);
                        }
                    });
                }
                NewSearchActivity.this.addNormalSearchListener(NewSearchActivity.this.region_layout, NewSearchActivity.this.text_region, AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getConfig().get(HouseBaseInfo.DISTRICT), false), 1);
                NewSearchActivity.this.addNormalSearchListener(NewSearchActivity.this.type_layout, NewSearchActivity.this.text_type, AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getConfig().get("channel"), false), 0);
                NewSearchActivity.this.addNormalSearchListener(NewSearchActivity.this.newhouse_price_layout, NewSearchActivity.this.text_newhouse_price, AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getConfig().get("price"), false), 0);
                List tagList = AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getConfig().get(HouseBaseInfo.TAG), true);
                if (tagList == null || tagList.size() <= 0) {
                    NewSearchActivity.this.feature_layout.setVisibility(8);
                    return;
                }
                NewSearchActivity.this.feature_layout.setVisibility(0);
                NewSearchActivity.this.addNormalSearchListener(NewSearchActivity.this.feature_layout, NewSearchActivity.this.text_feature, AppMethods.mapKeyTolistSubWay(NewSearchActivity.this.newHouseConfig.getConfig().get(HouseBaseInfo.TAG), false), 0);
            }

            public void OnError(HouseBaseInfo info) {
                NewSearchActivity.this.setViewEnable(R.string.text_city_config_error);
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setViewEnable(int msg) {
        this.subway_total_layout.setVisibility(8);
        AppMethods.setViewToastListener(this, this.region_layout, msg);
        AppMethods.setViewToastListener(this, this.subway_layout, msg);
        AppMethods.setViewToastListener(this, this.type_layout, msg);
        AppMethods.setViewToastListener(this, this.newhouse_price_layout, msg);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getConfig(R.string.loading);
        this.btn_finish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewSearchActivity.this, NewHouseSearchResultActivity.class);
                Bundle bundle = new Bundle();
                if (NewSearchActivity.this.newHouseConfig != null && !NewSearchActivity.this.newHouseConfig.getJsonStr().equals(NewSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    NewSearchActivity.this.getChooseValue();
                    bundle.putParcelable("locaion", NewSearchActivity.this.mLocation);
                    bundle.putInt("radiusvalue", NewSearchActivity.this.radiusvalue);
                    bundle.putString("districtvalue", NewSearchActivity.this.districtvalue);
                    bundle.putString("channelvalue", NewSearchActivity.this.channelvalue);
                    bundle.putString("pricevalue", NewSearchActivity.this.pricevalue);
                    bundle.putString("text_subway", NewSearchActivity.this.text_subway.getText().toString());
                    bundle.putString("keywords", NewSearchActivity.this.keyword_text.getText().toString());
                    if (NewSearchActivity.this.newHouseConfig.getConfig().get(HouseBaseInfo.TAG) != null) {
                        bundle.putString("tag_id", (String) NewSearchActivity.this.newHouseConfig.getConfig().get(HouseBaseInfo.TAG).get(NewSearchActivity.this.text_feature.getText().toString()));
                    }
                    if (!(NewSearchActivity.this.mLocation == null || NewSearchActivity.this.radiusvalue == 0)) {
                        bundle.putInt("FROM_SUBWAY", -2);
                    }
                }
                intent.putExtras(bundle);
                NewSearchActivity.this.startActivityForResult(intent, 11);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.newHouseConfig == null) {
            getConfig(0);
        }
    }

    public void onClick(View v) {
    }

    public void popDismiss() {
        this.btn_capture.setVisibility(0);
        this.cancelDialog.setVisibility(8);
        this.popLayout.setVisibility(8);
    }

    public void popShow() {
        this.btn_capture.setVisibility(8);
        this.cancelDialog.setVisibility(0);
        this.popLayout.setVisibility(0);
    }
}
