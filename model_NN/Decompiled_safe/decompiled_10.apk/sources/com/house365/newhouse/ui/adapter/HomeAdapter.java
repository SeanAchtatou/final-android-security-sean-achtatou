package com.house365.newhouse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.newhouse.R;
import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends BaseAdapter {
    private String city;
    private Context context;
    private LayoutInflater inflater;
    private boolean isVirtual;
    private List<HomeGridData> list = new ArrayList();

    public HomeAdapter(Context context2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.home_item, (ViewGroup) null);
        }
        ((TextView) convertView.findViewById(R.id.home_item_content)).setText(this.list.get(position).getName());
        ((ImageView) convertView.findViewById(R.id.home_item_image)).setBackgroundResource(this.list.get(position).getDrawableRes());
        return convertView;
    }

    public boolean isVirtual() {
        return this.isVirtual;
    }

    public void setVirtual(boolean isVirtual2) {
        this.isVirtual = isVirtual2;
    }

    class HomeGridData {
        private int drawableRes;
        private String name;

        HomeGridData() {
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public int getDrawableRes() {
            return this.drawableRes;
        }

        public void setDrawableRes(int drawableRes2) {
            this.drawableRes = drawableRes2;
        }
    }

    public void initData() {
        HomeGridData newHouse = new HomeGridData();
        newHouse.setName(this.context.getString(R.string.text_newhome_title));
        newHouse.setDrawableRes(R.drawable.home_newhouse);
        HomeGridData secondHouse = new HomeGridData();
        secondHouse.setName(this.context.getString(R.string.text_second_house_title));
        secondHouse.setDrawableRes(R.drawable.home_secondhouse);
        HomeGridData rentHouse = new HomeGridData();
        rentHouse.setName(this.context.getString(R.string.text_rent_house_title));
        rentHouse.setDrawableRes(R.drawable.home_renthouse);
        HomeGridData newsHouse = new HomeGridData();
        newsHouse.setName(this.context.getString(R.string.text_information_title));
        newsHouse.setDrawableRes(R.drawable.home_news);
        HomeGridData communityHouse = new HomeGridData();
        communityHouse.setName(this.context.getString(R.string.text_owner_community));
        communityHouse.setDrawableRes(R.drawable.home_community);
        HomeGridData loanHouse = new HomeGridData();
        loanHouse.setName(this.context.getString(R.string.text_mortaga_calculator));
        loanHouse.setDrawableRes(R.drawable.home_loan);
        HomeGridData qrHouse = new HomeGridData();
        qrHouse.setName(this.context.getString(R.string.text_capture_title));
        qrHouse.setDrawableRes(R.drawable.home_qr);
        HomeGridData shakeHouse = new HomeGridData();
        shakeHouse.setName(this.context.getString(R.string.text_shake_title));
        shakeHouse.setDrawableRes(R.drawable.home_shake);
        this.list.clear();
        this.list.add(newHouse);
        if (!this.isVirtual && !"shenyang".equals(this.city)) {
            this.list.add(secondHouse);
            this.list.add(rentHouse);
        }
        this.list.add(newsHouse);
        if (!this.isVirtual) {
            if (!"shenyang".equals(this.city)) {
                this.list.add(communityHouse);
            }
            this.list.add(loanHouse);
            this.list.add(qrHouse);
            this.list.add(shakeHouse);
            return;
        }
        this.list.add(qrHouse);
        this.list.add(loanHouse);
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }
}
