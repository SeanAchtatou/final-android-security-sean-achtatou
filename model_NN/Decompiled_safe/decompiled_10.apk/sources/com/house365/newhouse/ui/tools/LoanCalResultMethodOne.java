package com.house365.newhouse.ui.tools;

import com.house365.newhouse.ui.tools.LoanCalActivity;
import java.io.Serializable;

public class LoanCalResultMethodOne implements Serializable {
    public static final String INTENT_NAME = "LoanCalResultMethodOne";
    private LoanCalActivity.LoanType loanType;
    private int loantimesBusiness;
    private int loantimesFound;
    private double monthRepayBusiness;
    private double monthRepayFound;
    private double totalRateBusiness;
    private double totalRateFound;
    private double totalRepayBusiness;
    private double totalRepayFound;
    private double userLoanMoneyBusiness;
    private double userLoanMoneyFound;

    public LoanCalActivity.LoanType getLoanType() {
        return this.loanType;
    }

    public void setLoanType(LoanCalActivity.LoanType loanType2) {
        this.loanType = loanType2;
    }

    public double getUserLoanMoneyBusiness() {
        return this.userLoanMoneyBusiness;
    }

    public void setUserLoanMoneyBusiness(double userLoanMoneyBusiness2) {
        this.userLoanMoneyBusiness = userLoanMoneyBusiness2;
    }

    public int getLoantimesBusiness() {
        return this.loantimesBusiness;
    }

    public void setLoantimesBusiness(int loantimesBusiness2) {
        this.loantimesBusiness = loantimesBusiness2;
    }

    public double getTotalRepayBusiness() {
        return this.totalRepayBusiness;
    }

    public void setTotalRepayBusiness(double totalRepayBusiness2) {
        this.totalRepayBusiness = totalRepayBusiness2;
    }

    public double getTotalRateBusiness() {
        return this.totalRateBusiness;
    }

    public void setTotalRateBusiness(double totalRateBusiness2) {
        this.totalRateBusiness = totalRateBusiness2;
    }

    public double getMonthRepayBusiness() {
        return this.monthRepayBusiness;
    }

    public void setMonthRepayBusiness(double monthRepayBusiness2) {
        this.monthRepayBusiness = monthRepayBusiness2;
    }

    public double getUserLoanMoneyFound() {
        return this.userLoanMoneyFound;
    }

    public void setUserLoanMoneyFound(double userLoanMoneyFound2) {
        this.userLoanMoneyFound = userLoanMoneyFound2;
    }

    public int getLoantimesFound() {
        return this.loantimesFound;
    }

    public void setLoantimesFound(int loantimesFound2) {
        this.loantimesFound = loantimesFound2;
    }

    public double getTotalRepayFound() {
        return this.totalRepayFound;
    }

    public void setTotalRepayFound(double totalRepayFound2) {
        this.totalRepayFound = totalRepayFound2;
    }

    public double getTotalRateFound() {
        return this.totalRateFound;
    }

    public void setTotalRateFound(double totalRateFound2) {
        this.totalRateFound = totalRateFound2;
    }

    public double getMonthRepayFound() {
        return this.monthRepayFound;
    }

    public void setMonthRepayFound(double monthRepayFound2) {
        this.monthRepayFound = monthRepayFound2;
    }
}
