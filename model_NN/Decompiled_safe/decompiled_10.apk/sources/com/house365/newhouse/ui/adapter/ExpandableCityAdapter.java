package com.house365.newhouse.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.house365.core.view.pinned.BaseSectionListAdapter;
import com.house365.core.view.pinned.SectionListItem;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.VirtualCity;

public class ExpandableCityAdapter extends BaseSectionListAdapter {
    private NewHouseApplication application;

    public ExpandableCityAdapter(Context context) {
        super(context);
        this.application = (NewHouseApplication) context.getApplicationContext();
    }

    public BaseSectionListAdapter.SectionView initView(int position, View convertView, ViewGroup parent, boolean showHeader) {
        ViewHolder holder;
        BaseSectionListAdapter.SectionView sectionView = new BaseSectionListAdapter.SectionView();
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.expandale_city_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.mHeaderParent = (LinearLayout) convertView.findViewById(R.id.friends_item_header_parent);
            holder.mHeaderText = (TextView) convertView.findViewById(R.id.friends_item_header_text);
            holder.friends_item = (TextView) convertView.findViewById(R.id.friends_item);
            holder.ico_chose = (ImageView) convertView.findViewById(R.id.ico_chose);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SectionListItem currentItem = (SectionListItem) getItem(position);
        VirtualCity v_city = (VirtualCity) currentItem.item;
        String hotCity = this.context.getResources().getString(R.string.text_other_city);
        if (!(currentItem == null || v_city == null)) {
            if (!this.application.getCityName().equals(v_city.getCity_name()) || hotCity.equals(currentItem.section.toString().toUpperCase())) {
                holder.ico_chose.setVisibility(8);
            } else {
                holder.ico_chose.setVisibility(0);
            }
            holder.friends_item.setText(v_city.getCity_name());
            holder.mHeaderText.setText(currentItem.section.toString().toUpperCase());
            if (showHeader) {
                holder.mHeaderParent.setVisibility(0);
                holder.mHeaderText.setVisibility(0);
                sectionView.header = holder.mHeaderText;
            } else {
                holder.mHeaderParent.setVisibility(8);
                holder.mHeaderText.setVisibility(8);
            }
        }
        sectionView.view = convertView;
        return sectionView;
    }

    public void setHeaderViewText(View header, String title) {
        ((TextView) header.findViewById(R.id.header_text)).setText(title.toUpperCase());
    }

    class ViewHolder {
        TextView friends_item;
        ImageView ico_chose;
        LinearLayout mHeaderParent;
        TextView mHeaderText;

        ViewHolder() {
        }
    }
}
