package com.house365.newhouse.ui.tools;

import com.house365.newhouse.ui.tools.LoanCalActivity;
import com.house365.newhouse.ui.util.MathUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoanCalResultMethodTwo implements Serializable {
    public static final String INTENT_NAME = "LoanCalResultMethodTwo";
    private LoanCalActivity.LoanType loanType;
    private int loantimesBusiness;
    private int loantimesFound;
    private List<Double> monthRepayBusinesses;
    private List<Double> monthRepayFounds;
    private double totalRateBusiness;
    private double totalRateFound;
    private double totalRepayBusiness;
    private double totalRepayFound;
    private double userLoanMoneyBusiness;
    private double userLoanMoneyFound;

    public LoanCalActivity.LoanType getLoanType() {
        return this.loanType;
    }

    public void setLoanType(LoanCalActivity.LoanType loanType2) {
        this.loanType = loanType2;
    }

    public double getUserLoanMoneyBusiness() {
        return this.userLoanMoneyBusiness;
    }

    public void setUserLoanMoneyBusiness(double userLoanMoneyBusiness2) {
        this.userLoanMoneyBusiness = userLoanMoneyBusiness2;
    }

    public int getLoantimesBusiness() {
        return this.loantimesBusiness;
    }

    public void setLoantimesBusiness(int loantimesBusiness2) {
        this.loantimesBusiness = loantimesBusiness2;
    }

    public double getTotalRepayBusiness() {
        return this.totalRepayBusiness;
    }

    public void setTotalRepayBusiness(double totalRepayBusiness2) {
        this.totalRepayBusiness = totalRepayBusiness2;
    }

    public double getTotalRateBusiness() {
        return this.totalRateBusiness;
    }

    public void setTotalRateBusiness(double totalRateBusiness2) {
        this.totalRateBusiness = totalRateBusiness2;
    }

    public double getUserLoanMoneyFound() {
        return this.userLoanMoneyFound;
    }

    public void setUserLoanMoneyFound(double userLoanMoneyFound2) {
        this.userLoanMoneyFound = userLoanMoneyFound2;
    }

    public int getLoantimesFound() {
        return this.loantimesFound;
    }

    public void setLoantimesFound(int loantimesFound2) {
        this.loantimesFound = loantimesFound2;
    }

    public double getTotalRepayFound() {
        return this.totalRepayFound;
    }

    public void setTotalRepayFound(double totalRepayFound2) {
        this.totalRepayFound = totalRepayFound2;
    }

    public double getTotalRateFound() {
        return this.totalRateFound;
    }

    public void setTotalRateFound(double totalRateFound2) {
        this.totalRateFound = totalRateFound2;
    }

    public List<Double> getMonthRepayBusinesses() {
        return this.monthRepayBusinesses;
    }

    public void setMonthRepayBusinesses(List<Double> monthRepayBusinesses2) {
        this.monthRepayBusinesses = monthRepayBusinesses2;
    }

    public List<Double> getMonthRepayFounds() {
        return this.monthRepayFounds;
    }

    public void setMonthRepayFounds(List<Double> monthRepayFounds2) {
        this.monthRepayFounds = monthRepayFounds2;
    }

    public List<AdapterDataList> getAdapterDataList() {
        int maxSize;
        List<AdapterDataList> ls = new ArrayList<>();
        if (this.loanType == LoanCalActivity.LoanType.BUSINESS) {
            for (int i = 0; i < getMonthRepayBusinesses().size(); i++) {
                AdapterDataList item = new AdapterDataList();
                item.setLoantime(i + 1);
                item.setMonthRepayBusiness(getMonthRepayBusinesses().get(i).doubleValue());
                ls.add(item);
            }
        } else if (this.loanType == LoanCalActivity.LoanType.FOUND) {
            for (int i2 = 0; i2 < getMonthRepayFounds().size(); i2++) {
                AdapterDataList item2 = new AdapterDataList();
                item2.setLoantime(i2 + 1);
                item2.setMonthRepayFound(getMonthRepayFounds().get(i2).doubleValue());
                ls.add(item2);
            }
        } else {
            int bSize = getMonthRepayBusinesses().size();
            int fSize = getMonthRepayFounds().size();
            if (bSize > fSize) {
                maxSize = bSize;
            } else {
                maxSize = fSize;
            }
            for (int i3 = 0; i3 < maxSize; i3++) {
                AdapterDataList item3 = new AdapterDataList();
                item3.setLoantime(i3 + 1);
                if (i3 < bSize) {
                    item3.setMonthRepayBusiness(getMonthRepayBusinesses().get(i3).doubleValue());
                }
                if (i3 < fSize) {
                    item3.setMonthRepayFound(getMonthRepayFounds().get(i3).doubleValue());
                }
                item3.setMonthRepayAll(MathUtil.convertDouble(item3.getMonthRepayBusiness() + item3.getMonthRepayFound(), 2));
                ls.add(item3);
            }
        }
        return ls;
    }
}
