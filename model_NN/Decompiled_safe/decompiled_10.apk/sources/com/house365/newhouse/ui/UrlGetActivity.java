package com.house365.newhouse.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class UrlGetActivity extends BaseCommonActivity {
    public static String INTENT_URL = "url";
    private HeadNavigateView head_view;
    private float newx1;
    private float newx2;
    private float newy1;
    private float newy2;
    private float oldx1;
    private float oldx2;
    private float oldy1;
    private float oldy2;
    private WebView webView;
    /* access modifiers changed from: private */
    public ProgressBar web_progress;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.webview_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_urlget_title);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UrlGetActivity.this.finish();
            }
        });
        this.webView = (WebView) findViewById(R.id.webview);
        this.web_progress = (ProgressBar) findViewById(R.id.web_progress);
        this.webView.setScrollBarStyle(0);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.getSettings().setDefaultTextEncodingName("utf-8");
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                UrlGetActivity.this.web_progress.setVisibility(4);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.webView.loadUrl(getIntent().getStringExtra(INTENT_URL));
    }

    public static String generateUrlWithSystemParam(Context context, String url) {
        NewHouseApplication app;
        if (context instanceof Activity) {
            app = (NewHouseApplication) ((Activity) context).getApplication();
        } else {
            app = (NewHouseApplication) context;
        }
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("deviceid", app.getDeviceId()));
        olist.add(new BasicNameValuePair("phone", app.getMobile()));
        if (url.indexOf("?") == -1) {
            return String.valueOf(url) + "?" + URLEncodedUtils.format(olist, CharEncoding.UTF_8);
        }
        return String.valueOf(url) + "&" + URLEncodedUtils.format(olist, CharEncoding.UTF_8);
    }
}
