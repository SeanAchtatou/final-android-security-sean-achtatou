package com.house365.newhouse.ui.secondrent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.anim.AnimBean;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.panel.Panel;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.core.view.viewpager.PageIndicator;
import com.house365.core.view.viewpager.ex.ViewPagerCustomDuration;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.GetPersonHouseListTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.common.adapter.AdAdapter;
import com.house365.newhouse.ui.common.task.GetAdTask;
import com.house365.newhouse.ui.mapsearch.MapSearchHomeActivity;
import com.house365.newhouse.ui.search.KeyWordSearchActivity;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import com.house365.newhouse.ui.search.SearchHomeActivity;
import com.house365.newhouse.ui.search.SecondRentSearchActivity;
import com.house365.newhouse.ui.secondhouse.adapter.HouseListAdapter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

public class SecondRentHomeActivity extends BaseCommonActivity implements View.OnClickListener, PullToRefreshBase.OnRefreshListener, AdapterView.OnItemClickListener, Panel.OnPanelListener {
    /* access modifiers changed from: private */
    public AdAdapter adAdapter;
    private RelativeLayout adContainer;
    private HouseListAdapter adapterSell;
    private TranslateAnimation animation;
    private LinearLayout animationLayout;
    private Button backBtn;
    private RelativeLayout btn_map;
    private RelativeLayout btn_nearby;
    private RelativeLayout btn_region;
    protected boolean click_nearby;
    private ImageButton closeAd;
    private IntentFilter filter = new IntentFilter();
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SecondRentHomeActivity.this.adAdapter.clear();
            SecondRentHomeActivity.this.adAdapter.notifyDataSetChanged();
            SecondRentHomeActivity.this.top_switcher.setVisibility(4);
            SecondRentHomeActivity.this.panel_ad.setVisibility(4);
            SecondRentHomeActivity.this.getConfig(0);
            SecondRentHomeActivity.this.getAD();
            SecondRentHomeActivity.this.refreshData();
        }
    };
    /* access modifiers changed from: private */
    public PageIndicator mIndicator;
    /* access modifiers changed from: private */
    public ViewPager mPager;
    private NetState netchangeReceiver = new NetState(this, null);
    protected HouseBaseInfo newHouseConfig;
    private ImageView no_ad_img;
    private View nodata_layout;
    private ImageView panelContent;
    /* access modifiers changed from: private */
    public Panel panel_ad;
    private Button publishBtn;
    private PullToRefreshListView pullToRefresh;
    private RefreshInfo refreshInfo = new RefreshInfo();
    private Button searchBtn;
    private TextView titleView;
    private boolean toKeySearch;
    /* access modifiers changed from: private */
    public View top_switcher;
    private ImageView top_switcher_ico;
    private TextView tv_nodata;
    private View viewpager_layout;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.second_rent_home);
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        setNetWorkChange();
    }

    private void setNetWorkChange() {
        this.filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.netchangeReceiver, this.filter);
        this.netchangeReceiver.onReceive(this, null);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.titleView = (TextView) findViewById(R.id.second_rent_home_title);
        this.backBtn = (Button) findViewById(R.id.second_rent_home_back);
        this.searchBtn = (Button) findViewById(R.id.second_rent_home_search);
        this.publishBtn = (Button) findViewById(R.id.second_rent_home_publish);
        this.panel_ad = (Panel) findViewById(R.id.panel_ad);
        this.panelContent = (ImageView) this.panel_ad.getContent();
        this.top_switcher = findViewById(R.id.top_switcher);
        this.top_switcher_ico = (ImageView) findViewById(R.id.top_switcher_ico);
        this.viewpager_layout = findViewById(R.id.viewpager_layout);
        this.mIndicator = (PageIndicator) findViewById(R.id.indicator);
        this.mIndicator = (PageIndicator) findViewById(R.id.indicator);
        this.no_ad_img = (ImageView) findViewById(R.id.no_ad_img);
        this.mPager = (ViewPager) findViewById(R.id.pager);
        this.closeAd = (ImageButton) findViewById(R.id.house_ad_close);
        this.adContainer = (RelativeLayout) findViewById(R.id.house_ad_container);
        this.btn_nearby = (RelativeLayout) findViewById(R.id.newhousehome_nearby);
        this.btn_region = (RelativeLayout) findViewById(R.id.newhousehome_search_region);
        this.btn_map = (RelativeLayout) findViewById(R.id.newhousehome_map_search);
        this.pullToRefresh = (PullToRefreshListView) findViewById(R.id.newhousehome_list);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.tv_nodata.setText(getString(R.string.text_search_no_data));
        this.animationLayout = (LinearLayout) findViewById(R.id.newhousehome_movecontainer);
    }

    /* access modifiers changed from: private */
    public void getAD() {
        this.viewpager_layout.setVisibility(8);
        this.no_ad_img.setVisibility(0);
        GetAdTask task = new GetAdTask(this, 4, this.adAdapter, this.viewpager_layout, this.no_ad_img);
        task.setCallBack(new GetAdTask.CallBack() {
            public void onFail() {
                SecondRentHomeActivity.this.closeAd();
            }

            public void onSuccess(Ad ad) {
                ((ViewPagerCustomDuration) SecondRentHomeActivity.this.mPager).stopAutoFlowTimer();
                ((ViewPagerCustomDuration) SecondRentHomeActivity.this.mPager).startAutoFlowTimer();
                SecondRentHomeActivity.this.mIndicator.notifyDataSetChanged();
            }
        });
        task.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info == null) {
                    return;
                }
                if (info.getJsonStr().equals(SecondRentHomeActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondRentHomeActivity.this.setViewEnable(R.string.text_no_network);
                    return;
                }
                SecondRentHomeActivity.this.newHouseConfig = info;
                SecondRentHomeActivity.this.setNormalClick();
            }

            public void OnError(HouseBaseInfo info) {
                SecondRentHomeActivity.this.setViewEnable(R.string.text_city_config_error);
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setNormalClick() {
        this.btn_nearby.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(SecondRentHomeActivity.this, SecondRentResultActivity.class);
                intent.putExtra("from_nearby", true);
                SecondRentHomeActivity.this.startActivity(intent);
            }
        });
        this.btn_region.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(SecondRentHomeActivity.this, SecondRentSearchActivity.class);
                intent.putExtra("litterHome", true);
                SecondRentHomeActivity.this.startActivity(intent);
            }
        });
        this.btn_map.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SecondRentHomeActivity.this, MapSearchHomeActivity.class);
                intent.putExtra(MapSearchHomeActivity.INTENT_MAP_TYPE, 13);
                SecondRentHomeActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void setViewEnable(int msg) {
        AppMethods.setViewToastListener(this, this.btn_region, msg);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.titleView.setText((int) R.string.text_rent_house_title);
        this.backBtn.setOnClickListener(this);
        this.searchBtn.setOnClickListener(this);
        this.publishBtn.setOnClickListener(this);
        this.pullToRefresh.setOnRefreshListener(this);
        this.pullToRefresh.setOnItemClickListener(this);
        this.top_switcher.setOnClickListener(this);
        this.panel_ad.setOnPanelListener(this);
        this.btn_nearby.setOnClickListener(this);
        this.closeAd.setOnClickListener(this);
        this.adapterSell = new HouseListAdapter(this, App.RENT);
        this.pullToRefresh.setAdapter(this.adapterSell);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        refreshData();
        this.panel_ad.setInterpolator(new AccelerateDecelerateInterpolator());
        this.viewpager_layout.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.no_ad_img.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.adAdapter = new AdAdapter(this, 4);
        this.mPager.setAdapter(this.adAdapter);
        ((ViewPagerCustomDuration) this.mPager).setConsumeTouchEvent(true);
        ((ViewPagerCustomDuration) this.mPager).setScrollDuration(1000);
        this.mIndicator.setViewPager(this.mPager);
        getAD();
        getConfig(R.string.loading);
        this.animation = new TranslateAnimation(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, (float) this.viewpager_layout.getLayoutParams().height, SystemUtils.JAVA_VERSION_FLOAT);
        this.animation.setDuration(400);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.toKeySearch = false;
        if (this.newHouseConfig == null) {
            getConfig(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mChangeCity);
        unregisterReceiver(this.netchangeReceiver);
    }

    private class NetState extends BroadcastReceiver {
        private NetState() {
        }

        /* synthetic */ NetState(SecondRentHomeActivity secondRentHomeActivity, NetState netState) {
            this();
        }

        public void onReceive(Context arg0, Intent arg1) {
            ConnectivityManager manager = (ConnectivityManager) arg0.getSystemService("connectivity");
            NetworkInfo gprs = manager.getNetworkInfo(0);
            NetworkInfo wifi = manager.getNetworkInfo(1);
            if (gprs.isConnected() || wifi.isConnected()) {
                SecondRentHomeActivity.this.getConfig(0);
            }
        }
    }

    public void onPanelOpened(Panel panel) {
        this.top_switcher_ico.setBackgroundResource(R.drawable.top_switcher_up);
    }

    public void onPanelClosed(Panel panel) {
        this.top_switcher_ico.setBackgroundResource(R.drawable.top_switcher_down);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(this, SecondRentDetailActivity.class);
        intent.putExtra("id", ((SecondHouse) this.adapterSell.getItem(position)).getId());
        intent.putExtra("FROM_BLOCK_LIST", StringUtils.EMPTY);
        startActivity(intent);
    }

    public void onHeaderRefresh() {
        refreshData();
    }

    public void onFooterRefresh() {
        getMoreData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.house_ad_close:
                closeAd();
                return;
            case R.id.second_rent_home_back:
                finish();
                return;
            case R.id.second_rent_home_publish:
                if (TextUtils.isEmpty(((NewHouseApplication) this.mApplication).getMobile())) {
                    AppMethods.showLoginDialog(this, R.string.text_nologin_publish_msg, ActionCode.RENT_SEARCH);
                    return;
                }
                Intent intent = new Intent(this, SearchHomeActivity.class);
                intent.putExtra(SearchHomeActivity.INTENT_PUBLISH, 13);
                startActivity(intent);
                return;
            case R.id.second_rent_home_search:
                this.toKeySearch = true;
                Intent intent2 = new Intent(this, KeyWordSearchActivity.class);
                intent2.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, ActionCode.RENT_SEARCH);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    public AnimBean getStartAnim() {
        if (this.toKeySearch) {
            return null;
        }
        return super.getStartAnim();
    }

    /* access modifiers changed from: private */
    public void closeAd() {
        this.adContainer.setVisibility(8);
        this.animationLayout.startAnimation(this.animation);
    }

    public void refreshData() {
        this.refreshInfo.refresh = true;
        new GetPersonHouseListTask(this, this.pullToRefresh, this.refreshInfo, this.adapterSell, 2, this.nodata_layout).execute(new Object[0]);
    }

    public void getMoreData() {
        this.refreshInfo.refresh = false;
        new GetPersonHouseListTask(this, this.pullToRefresh, this.refreshInfo, this.adapterSell, 2, this.nodata_layout).execute(new Object[0]);
    }
}
