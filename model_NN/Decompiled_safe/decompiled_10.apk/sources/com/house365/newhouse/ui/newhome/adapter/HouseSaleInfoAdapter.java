package com.house365.newhouse.ui.newhome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.util.TimeUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.model.SaleInfo;

public class HouseSaleInfoAdapter extends BaseCacheListAdapter<SaleInfo> {
    private LayoutInflater inflater;

    public HouseSaleInfoAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_list_house_sale_info, (ViewGroup) null);
            holder = new ViewHolder(null);
            holder.txt_sale_title = (TextView) convertView.findViewById(R.id.txt_sale_title);
            holder.txt_sale_info = (TextView) convertView.findViewById(R.id.txt_sale_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SaleInfo saleInfo = (SaleInfo) getItem(position);
        holder.txt_sale_title.setText(TimeUtil.toDateWithFormat(saleInfo.getAddtime(), "yyyy年MM月dd日"));
        holder.txt_sale_info.setText(saleInfo.getSummary());
        return convertView;
    }

    private static class ViewHolder {
        TextView txt_sale_info;
        TextView txt_sale_title;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
