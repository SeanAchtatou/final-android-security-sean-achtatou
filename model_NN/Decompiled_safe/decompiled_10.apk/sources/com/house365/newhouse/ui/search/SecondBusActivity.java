package com.house365.newhouse.ui.search;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.adapter.DefineArrayAdapter;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.ui.secondsell.AddBlockActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SecondBusActivity extends BaseCommonActivity {
    public static final int FROM_TYPE = 1;
    /* access modifiers changed from: private */
    public SimpleAdapter adapter;
    DefineArrayAdapter<String> array_adapter;
    /* access modifiers changed from: private */
    public View bus_list_layout;
    /* access modifiers changed from: private */
    public int from_litter_home;
    /* access modifiers changed from: private */
    public int fromtype;
    private HeadNavigateView head_View;
    /* access modifiers changed from: private */
    public ListView key_list;
    /* access modifiers changed from: private */
    public EditText keywords;
    /* access modifiers changed from: private */
    public Button search_close;
    /* access modifiers changed from: private */
    public String search_type;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.bus_search);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.from_litter_home = getIntent().getIntExtra("FROM_LITTER_HOME", 0);
        this.head_View = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_View.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondBusActivity.this.finish();
            }
        });
        this.search_type = getIntent().getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
        this.keywords = (EditText) findViewById(R.id.keywords);
        this.search_close = (Button) findViewById(R.id.search_close);
        this.key_list = (ListView) findViewById(R.id.key_bus_list);
        this.bus_list_layout = findViewById(R.id.bus_list_layout);
        this.fromtype = getIntent().getIntExtra(AddBlockActivity.INTENT_FROM_TYPE, 0);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.search_close.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondBusActivity.this.keywords.setText(StringUtils.EMPTY);
            }
        });
        this.keywords.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String keys = SecondBusActivity.this.keywords.getText().toString().trim();
                if (keys == null || TextUtils.isEmpty(keys)) {
                    SecondBusActivity.this.search_close.setVisibility(8);
                    return;
                }
                SecondBusActivity.this.search_close.setVisibility(0);
                new SecondBusSearchTask(SecondBusActivity.this, null).execute(new Void[0]);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.key_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String bus_id = ((TextView) view.findViewById(R.id.h_id)).getText().toString();
                String bus_name = ((TextView) view.findViewById(R.id.h_name)).getText().toString();
                Intent intent = new Intent();
                if (SecondBusActivity.this.fromtype == 1) {
                    intent.putExtra("bus_id", bus_id);
                    intent.putExtra(SecondRentSearchActivity.INTENT_BUS_NAME, bus_name);
                    intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, SecondBusActivity.this.search_type);
                    intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, 1);
                    intent.putExtra("FROM_LITTER_HOME", SecondBusActivity.this.from_litter_home);
                    intent.setClass(SecondBusActivity.this, SecondBusExpandleActivity.class);
                    SecondBusActivity.this.startActivity(intent);
                    SecondBusActivity.this.finish();
                } else if (bus_id != null && !TextUtils.isEmpty(bus_id)) {
                    intent.putExtra("bus_id", bus_id);
                    intent.putExtra(SecondRentSearchActivity.INTENT_BUS_NAME, bus_name);
                    intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, SecondBusActivity.this.search_type);
                    intent.putExtra(AddBlockActivity.INTENT_FROM_TYPE, SecondBusActivity.this.fromtype);
                    intent.putExtra("FROM_LITTER_HOME", SecondBusActivity.this.from_litter_home);
                    intent.setClass(SecondBusActivity.this, SecondBusExpandleActivity.class);
                    SecondBusActivity.this.startActivity(intent);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.keywords.setText(StringUtils.EMPTY);
    }

    private class SecondBusSearchTask extends AsyncTask<Void, Void, List<SecondHouse>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<SecondHouse>) ((List) obj));
        }

        private SecondBusSearchTask() {
        }

        /* synthetic */ SecondBusSearchTask(SecondBusActivity secondBusActivity, SecondBusSearchTask secondBusSearchTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public List<SecondHouse> doInBackground(Void... params) {
            try {
                return ((HttpApi) ((NewHouseApplication) SecondBusActivity.this.mApplication).getApi()).getSecondBusBySearch(SecondBusActivity.this.keywords.getText().toString(), SecondBusActivity.this.search_type);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SecondHouse> houseList) {
            if (houseList != null && houseList.size() > 0) {
                List<HashMap<String, String>> search_data = new ArrayList<>();
                for (int i = 0; i < houseList.size(); i++) {
                    HashMap<String, String> item = new HashMap<>();
                    item.put("h_id", houseList.get(i).getId());
                    item.put("h_name", houseList.get(i).getName());
                    search_data.add(item);
                }
                SecondBusActivity.this.key_list.setVisibility(0);
                SecondBusActivity.this.bus_list_layout.setVisibility(8);
                SecondBusActivity.this.adapter = new SimpleAdapter(SecondBusActivity.this, search_data, R.layout.keysearch_item, new String[]{"h_id", "h_name"}, new int[]{R.id.h_id, R.id.h_name});
                SecondBusActivity.this.adapter.notifyDataSetChanged();
                SecondBusActivity.this.key_list.setAdapter((ListAdapter) SecondBusActivity.this.adapter);
            } else if (SecondBusActivity.this.isNetworkConnected(SecondBusActivity.this)) {
                SecondBusActivity.this.key_list.setVisibility(8);
                SecondBusActivity.this.bus_list_layout.setVisibility(0);
            } else {
                SecondBusActivity.this.showToast((int) R.string.bus_info_load_net_error);
            }
        }
    }

    public boolean isNetworkConnected(Context context) {
        NetworkInfo mNetworkInfo;
        if (context == null || (mNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
            return false;
        }
        return mNetworkInfo.isAvailable();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
