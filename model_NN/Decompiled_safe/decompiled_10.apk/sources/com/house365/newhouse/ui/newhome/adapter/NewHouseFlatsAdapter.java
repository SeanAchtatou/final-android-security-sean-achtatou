package com.house365.newhouse.ui.newhome.adapter;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.Photo;

public class NewHouseFlatsAdapter extends BaseCacheListAdapter<Photo> {
    private LayoutInflater inflater;
    private boolean isUseLoaction = false;
    private Location location;

    public boolean isUseLoaction() {
        return this.isUseLoaction;
    }

    public void setUseLoaction(boolean isUseLoaction2) {
        this.isUseLoaction = isUseLoaction2;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public NewHouseFlatsAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_house_room, (ViewGroup) null);
            holder = new ViewHolder(null);
            holder.h_flats_img_one = (ImageView) convertView.findViewById(R.id.h_flats_img_one);
            holder.text_flats_num_one = (TextView) convertView.findViewById(R.id.text_flats_num_one);
            holder.text_flats_one_type = (TextView) convertView.findViewById(R.id.text_flats_one_type);
            holder.text_flats_name = (TextView) convertView.findViewById(R.id.text_flats_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Photo photo = (Photo) getItem(position);
        holder.text_flats_name.setText(photo.getP_name());
        setCacheImage(holder.h_flats_img_one, photo.getP_url(), R.drawable.bg_default_img_photo, 3);
        holder.text_flats_num_one.setText(photo.getP_tag());
        if (photo.getP_tag() != null && photo.getP_tag().indexOf("㎡") == -1) {
            holder.text_flats_one_type.setText(photo.getP_area());
        }
        return convertView;
    }

    private static class ViewHolder {
        ImageView h_flats_img_one;
        TextView text_flats_name;
        TextView text_flats_num_one;
        TextView text_flats_one_type;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
