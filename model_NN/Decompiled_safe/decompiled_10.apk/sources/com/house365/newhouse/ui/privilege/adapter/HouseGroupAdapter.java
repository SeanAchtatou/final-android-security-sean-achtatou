package com.house365.newhouse.ui.privilege.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.Line;
import com.house365.newhouse.ui.award.AwardActivity;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import com.house365.newhouse.ui.privilege.HouseLineActivity;
import com.house365.newhouse.ui.user.UserInfoActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.Date;
import java.util.List;

public class HouseGroupAdapter extends BaseCacheListAdapter<Event> {
    protected static final String TAG = "HouseGroupAdapter";
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public LayoutInflater inflater;
    private NewHouseApplication newHouseApplication;
    private boolean showIco;

    public HouseGroupAdapter(Context context2) {
        super(context2);
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
        this.newHouseApplication = (NewHouseApplication) ((Activity) context2).getApplication();
    }

    public boolean isShowIco() {
        return this.showIco;
    }

    public void setShowIco(boolean showIco2) {
        this.showIco = showIco2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.group_house_item, (ViewGroup) null);
            holder = new ViewHolder(convertView);
            holder.title = (TextView) convertView.findViewById(R.id.group_house_item_title);
            holder.back_content = (TextView) convertView.findViewById(R.id.group_house_item_background);
            holder.fore_content = (TextView) convertView.findViewById(R.id.group_house_item_foreground);
            holder.expand = (ImageView) convertView.findViewById(R.id.group_house_expand);
            holder.houseMark = (ImageView) convertView.findViewById(R.id.group_house_item_mark);
            holder.call = (Button) convertView.findViewById(R.id.group_house_item_call);
            holder.sign = (Button) convertView.findViewById(R.id.group_house_item_sign);
            holder.lines = (GridView) convertView.findViewById(R.id.group_house_item_line);
            holder.lottery = (Button) convertView.findViewById(R.id.group_house_item_lottery);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (this.showIco) {
            holder.houseMark.setVisibility(0);
        } else {
            holder.houseMark.setVisibility(8);
        }
        final Event event = (Event) getItem(position);
        holder.lottery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ("1".equals(event.getE_lottery_join())) {
                    Intent intent = new Intent(HouseGroupAdapter.this.context, AwardActivity.class);
                    intent.putExtra("id", event.getE_id());
                    intent.putExtra(CouponDetailsActivity.INTENT_TYPE, event.getE_type());
                    intent.putExtra("event", event);
                    HouseGroupAdapter.this.context.startActivity(intent);
                    return;
                }
                HouseGroupAdapter.this.joinAward(event);
            }
        });
        holder.title.setText(event.getE_title());
        if (!TextUtils.isEmpty(event.getE_content())) {
            String content = event.getE_content();
            holder.back_content.setText(Html.fromHtml("\t\t" + content));
            holder.fore_content.setText(Html.fromHtml("\t\t" + content));
            if (content.length() < 60) {
                holder.expand.setVisibility(8);
            } else {
                holder.expand.setVisibility(0);
            }
        }
        String e_type = event.getE_type();
        if (e_type.equals("event")) {
            holder.houseMark.setImageResource(R.drawable.bg_new_event);
        } else if (e_type.equals(App.Categroy.Event.SELL_EVENT)) {
            holder.houseMark.setImageResource(R.drawable.bg_sell_event);
        }
        LineAdapter adapter = new LineAdapter(this.context, event);
        List<Line> lineList = event.getE_lines();
        if (lineList == null || lineList.size() <= 0) {
            holder.lines.setVisibility(8);
        } else {
            if (lineList.size() == 1) {
                holder.lines.setNumColumns(1);
            } else {
                holder.lines.setNumColumns(2);
            }
            adapter.addAll(lineList);
            holder.lines.setAdapter((ListAdapter) adapter);
            holder.lines.setVisibility(0);
        }
        long sysdate = new Date().getTime() / 1000;
        if (sysdate > event.getE_endtime()) {
            holder.sign.setEnabled(false);
        } else {
            holder.sign.setEnabled(true);
        }
        if (event.getE_lottery() == null || sysdate >= event.getE_lottery().getE_endtime()) {
            holder.lottery.setVisibility(8);
        } else {
            holder.lottery.setVisibility(0);
        }
        holder.call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (event != null) {
                    TelUtil.getCallIntent(event.getE_tel(), HouseGroupAdapter.this.context, "event");
                }
            }
        });
        holder.sign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (event != null) {
                    HouseGroupAdapter.this.joinAward(event);
                }
            }
        });
        final int backLineH = holder.back_content.getLineHeight();
        final int backlineC = holder.back_content.getLineCount() - 3;
        holder.expand.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int height = holder.getConvertView().getHeight();
                int top = backLineH * backlineC;
                AbsListView.LayoutParams lp = (AbsListView.LayoutParams) holder.getConvertView().getLayoutParams();
                if (holder.expand.getTag() != null) {
                    holder.expand.setTag(null);
                    holder.fore_content.setVisibility(0);
                    holder.back_content.setVisibility(8);
                    holder.expand.setImageDrawable(HouseGroupAdapter.this.context.getResources().getDrawable(R.drawable.bg_event_open));
                    lp.height = -(height - top);
                } else {
                    holder.expand.setTag(1);
                    holder.back_content.setVisibility(0);
                    holder.fore_content.setVisibility(8);
                    holder.expand.setImageDrawable(HouseGroupAdapter.this.context.getResources().getDrawable(R.drawable.bg_event_close));
                    lp.height = -(height + top);
                }
                holder.getConvertView().setLayoutParams(lp);
            }
        });
        return convertView;
    }

    /* access modifiers changed from: private */
    public void joinAward(Event event) {
        if (TextUtils.isEmpty(this.newHouseApplication.getMobile())) {
            Intent intent = new Intent(this.context, UserLoginActivity.class);
            intent.putExtra("c_id", event.getE_id());
            intent.putExtra("coupon_type", event.getE_type());
            intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 1);
            this.context.startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(this.context, UserInfoActivity.class);
        intent2.putExtra("c_id", event.getE_id());
        intent2.putExtra("coupon_type", event.getE_type());
        intent2.putExtra(UserInfoActivity.INTENT_FROM, 2);
        this.context.startActivity(intent2);
    }

    private class ViewHolder {
        TextView back_content;
        Button call;
        View convertView;
        ImageView expand;
        TextView fore_content;
        ImageView houseMark;
        GridView lines;
        Button lottery;
        Button sign;
        TextView title;

        public ViewHolder(View convertView2) {
            this.convertView = convertView2;
        }

        public View getConvertView() {
            return this.convertView;
        }

        public void setConvertView(View convertView2) {
            this.convertView = convertView2;
        }
    }

    class LineAdapter extends BaseCacheListAdapter<Line> {
        Event event;
        int[] res = {R.drawable.bg_line_pink, R.drawable.bg_line_red, R.drawable.bg_line_green, R.drawable.bg_line_yellow};

        public LineAdapter(Context context, Event event2) {
            super(context);
            this.event = event2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = HouseGroupAdapter.this.inflater.inflate((int) R.layout.line_item, (ViewGroup) null);
                Button btn_line = (Button) convertView.findViewById(R.id.btn_line);
                final Line line = (Line) getItem(position);
                int row = ((int) getItemId(position)) / 2;
                if (row > 3) {
                    row -= 3;
                }
                if (!TextUtils.isEmpty(line.getL_title())) {
                    btn_line.setText(line.getL_title());
                    btn_line.setBackgroundDrawable(this.context.getResources().getDrawable(this.res[row]));
                    btn_line.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Intent intent = new Intent(LineAdapter.this.context, HouseLineActivity.class);
                            intent.putExtra(HouseLineActivity.INTENT_LINE, line);
                            intent.putExtra("event", LineAdapter.this.event);
                            LineAdapter.this.context.startActivity(intent);
                        }
                    });
                }
            }
            return convertView;
        }
    }
}
