package com.house365.newhouse.ui.newhome;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.Album;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAlbumSpecifyAdapter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseAlbumSpecifyActivity extends BaseCommonActivity {
    public static String INTENT_VIRTUAL = "isvirtual";
    /* access modifiers changed from: private */
    public NewHouseAlbumSpecifyAdapter adapter;
    /* access modifiers changed from: private */
    public Album album;
    /* access modifiers changed from: private */
    public String h_id;
    private HeadNavigateView head_view;
    private GridView houses_album_specify;
    /* access modifiers changed from: private */
    public boolean isvirtual;
    private Bundle params;
    /* access modifiers changed from: private */
    public ArrayList<String> picList = new ArrayList<>();

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.house_album_specify);
    }

    private class GetHouseAlbumList extends AsyncTask<Void, Void, List<Photo>> {
        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<Photo>) ((List) obj));
        }

        private GetHouseAlbumList() {
        }

        /* synthetic */ GetHouseAlbumList(NewHouseAlbumSpecifyActivity newHouseAlbumSpecifyActivity, GetHouseAlbumList getHouseAlbumList) {
            this();
        }

        /* access modifiers changed from: protected */
        public List<Photo> doInBackground(Void... params) {
            if (!NewHouseAlbumSpecifyActivity.this.isvirtual || NewHouseAlbumSpecifyActivity.this.album != null) {
                return NewHouseAlbumSpecifyActivity.this.album.getA_photos();
            }
            try {
                return ((HttpApi) ((NewHouseApplication) NewHouseAlbumSpecifyActivity.this.mApplication).getApi()).getVirtualHousePhotoById(NewHouseAlbumSpecifyActivity.this.h_id, StringUtils.EMPTY);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Photo> result) {
            if (result == null || result.size() <= 0) {
                NewHouseAlbumSpecifyActivity.this.finish();
                return;
            }
            for (Photo a : result) {
                System.out.println(String.valueOf(a.getP_name()) + "=" + a.getP_thumb() + "=" + (a.getP_tag() == null) + "+" + a.getP_url());
                NewHouseAlbumSpecifyActivity.this.picList.add(a.getP_url());
            }
            NewHouseAlbumSpecifyActivity.this.adapter.addAll(result);
            NewHouseAlbumSpecifyActivity.this.adapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void clean() {
        this.adapter.clear();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.h_id = getIntent().getStringExtra("h_id");
        this.isvirtual = getIntent().getBooleanExtra(INTENT_VIRTUAL, false);
        this.params = getIntent().getExtras();
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewHouseAlbumSpecifyActivity.this.finish();
            }
        });
        this.houses_album_specify = (GridView) findViewById(R.id.houses_album_specify);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.adapter = new NewHouseAlbumSpecifyAdapter(this);
        this.houses_album_specify.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(NewHouseAlbumSpecifyActivity.this, AlbumFullScreenActivity.class);
                intent.putExtra(AlbumFullScreenActivity.INTENT_ALBUM_POS, position);
                intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, NewHouseAlbumSpecifyActivity.this.picList);
                NewHouseAlbumSpecifyActivity.this.startActivity(intent);
            }
        });
        this.houses_album_specify.setAdapter((ListAdapter) this.adapter);
        this.album = (Album) this.params.get("album");
        String title = String.valueOf(getResources().getString(R.string.title_house)) + getResources().getString(R.string.title_album);
        if (this.album != null) {
            title = String.valueOf(getResources().getString(R.string.title_house)) + this.album.getA_name() + getResources().getString(R.string.title_album);
            if (this.album.getA_photos() != null && this.album.getA_photos().size() > 0) {
                for (int i = 0; i < this.album.getA_photos().size(); i++) {
                    this.picList.add(this.album.getA_photos().get(i).getP_url());
                }
                this.adapter.addAll(this.album.getA_photos());
                this.adapter.notifyDataSetChanged();
            }
        } else {
            new GetHouseAlbumList(this, null).execute(new Void[0]);
        }
        this.head_view.setTvTitleText(title);
    }
}
