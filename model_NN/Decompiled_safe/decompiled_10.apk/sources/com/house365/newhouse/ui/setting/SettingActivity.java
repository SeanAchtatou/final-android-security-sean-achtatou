package com.house365.newhouse.ui.setting;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.task.UpgradeTask;
import com.house365.core.util.DialogUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.Switch;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.ui.util.PushServiceUtil;

public class SettingActivity extends BaseCommonActivity {
    private static final int CLEAR_CACHE_OK = 1;
    private View about_layout;
    private View clear_layout;
    private View feedback_layout;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    SettingActivity.this.showToast((int) R.string.text_clear);
                    SettingActivity.this.dismissLoadingDialog();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private HeadNavigateView head_view;
    private View help_layout;
    /* access modifiers changed from: private */
    public NewHouseApplication newApp;
    private View recommapp_layout;
    private Switch switch_notify;
    private Switch switch_pic;
    private View upgrade_layout;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.setting_layout);
        this.newApp = (NewHouseApplication) this.mApplication;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivity.this.finish();
            }
        });
        this.clear_layout = findViewById(R.id.clear_layout);
        this.feedback_layout = findViewById(R.id.feedback_layout);
        this.help_layout = findViewById(R.id.help_layout);
        this.recommapp_layout = findViewById(R.id.recommapp_layout);
        this.about_layout = findViewById(R.id.about_layout);
        this.upgrade_layout = findViewById(R.id.upgrade_layout);
        this.switch_notify = (Switch) findViewById(R.id.switch_notify);
        this.switch_pic = (Switch) findViewById(R.id.switch_pic);
        this.switch_notify.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            public void onCheckedChanged(Switch switchView, boolean isChecked) {
                SettingActivity.this.newApp.enablePushNotification(isChecked);
                if (isChecked) {
                    new PushServiceUtil().startPNService(SettingActivity.this, SettingActivity.this.newApp, 1, null, App.PULL_ACCOUNT_PASSWORD);
                    HouseAnalyse.onViewClick(SettingActivity.this, "开启推送", "设置", null);
                    return;
                }
                SettingActivity.this.newApp.removePullServiceManager();
                HouseAnalyse.onViewClick(SettingActivity.this, "关闭推送", "设置", null);
            }
        });
        this.switch_pic.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            public void onCheckedChanged(Switch switchView, boolean isChecked) {
                if (isChecked) {
                    HouseAnalyse.onViewClick(SettingActivity.this, "有图", "设置", null);
                } else {
                    HouseAnalyse.onViewClick(SettingActivity.this, "无图", "设置", null);
                }
                SettingActivity.this.newApp.enableImg(isChecked);
            }
        });
        this.clear_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                DialogUtil.showConfrimDialog(SettingActivity.this, (int) R.string.text_clear_dialog_msg, (int) R.string.dialog_button_ok, new ConfirmDialogListener() {
                    public void onPositive(DialogInterface dialog) {
                        SettingActivity.this.cleanCache();
                        HouseAnalyse.onViewClick(SettingActivity.this, "清除缓存", "设置", null);
                    }

                    public void onNegative(DialogInterface dialog) {
                    }
                });
            }
        });
        this.upgrade_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HouseAnalyse.onViewClick(SettingActivity.this, "更新", "设置", null);
                new UpgradeTask(SettingActivity.this, false).execute(new Object[0]);
            }
        });
        this.help_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, RecomAppActivity.class);
                intent.putExtra(RecomAppActivity.INTENT_TITLE, SettingActivity.this.getResources().getString(R.string.text_intro));
                intent.putExtra(RecomAppActivity.INTENT_LOADFILE, "http://app.house365.com/taofang/help.html");
                SettingActivity.this.startActivity(intent);
            }
        });
        this.recommapp_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, RecomAppActivity.class);
                intent.putExtra(RecomAppActivity.INTENT_TITLE, SettingActivity.this.getResources().getString(R.string.text_more_app));
                intent.putExtra(RecomAppActivity.INTENT_LOADFILE, "http://app.house365.com/client/?app=tlf&type=android");
                SettingActivity.this.startActivity(intent);
            }
        });
        this.feedback_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SettingActivity.this.startActivity(new Intent(SettingActivity.this, FeedbackActivity.class));
            }
        });
        this.about_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SettingActivity.this.startActivity(new Intent(SettingActivity.this, AboutActivity.class));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.switch_notify.setChecked(this.newApp.isEnablePushNotification());
        this.switch_pic.setChecked(this.newApp.isEnableImg());
    }

    /* access modifiers changed from: private */
    public void cleanCache() {
        showLoadingDialog(R.string.text_more_cache_clearing);
        new Thread() {
            public void run() {
                SettingActivity.this.newApp.clearPrefsCache();
                SettingActivity.this.newApp.cleanAllFile();
                SettingActivity.this.newApp.getDiskCache().clear();
                Message msg = new Message();
                msg.what = 1;
                SettingActivity.this.handler.sendMessage(msg);
            }
        }.start();
    }
}
