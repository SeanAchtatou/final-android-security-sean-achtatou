package com.house365.newhouse.ui.privilege;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.NoScrollListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.House;
import com.house365.newhouse.task.GetEventInfoTask;
import com.house365.newhouse.task.GroupTask;
import com.house365.newhouse.ui.MenuActivity;
import com.house365.newhouse.ui.SplashActivity;
import com.house365.newhouse.ui.UrlGetActivity;
import com.house365.newhouse.ui.apn.APNActivity;
import com.house365.newhouse.ui.newhome.NewHouseDetailActivity;
import com.house365.newhouse.ui.privilege.adapter.CouponNewHouseAdapter;
import com.house365.newhouse.ui.user.UserInfoActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class CouponDetailsActivity extends BaseCommonActivity {
    public static final String INTENT_ID = "id";
    public static final String INTENT_TYPE = "type";
    private static final int REQUET_LOG_CODE = 1;
    private String activities_cutoff_time;
    /* access modifiers changed from: private */
    public CouponNewHouseAdapter adapter;
    /* access modifiers changed from: private */
    public TextView btn_apply;
    private TextView btn_refer;
    private TextView c_end_time;
    private TextView c_join;
    private TextView c_momo;
    private TextView c_name;
    private ImageView c_pic;
    /* access modifiers changed from: private */
    public String cid;
    /* access modifiers changed from: private */
    public Event coupon;
    /* access modifiers changed from: private */
    public String cphone;
    /* access modifiers changed from: private */
    public String ctype;
    private ScrollView detail_info_scroll;
    private HeadNavigateView head_view;
    private NoScrollListView house_room_list;
    private RelativeLayout house_room_list_layout;
    private String text_pub_follow;
    private String text_pub_join;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == -1) {
            String truename = data.getStringExtra("truename");
            String mobile = data.getStringExtra("mobile");
            if (this.coupon != null) {
                new GroupTask(this, (NewHouseApplication) this.mApplication, this.cid, this.ctype, truename, mobile).execute(new Object[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.coupon);
        ((NotificationManager) getSystemService("notification")).cancel(Integer.parseInt(getIntent().getStringExtra("id")));
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CouponDetailsActivity.this.finish();
            }
        });
        this.activities_cutoff_time = getResources().getString(R.string.activities_cutoff_time);
        this.text_pub_join = getResources().getString(R.string.text_pub_join);
        this.text_pub_follow = getResources().getString(R.string.text_pub_follow);
        this.detail_info_scroll = (ScrollView) findViewById(R.id.detail_info_scroll);
        this.c_pic = (ImageView) findViewById(R.id.c_pic);
        this.c_name = (TextView) findViewById(R.id.c_name);
        this.c_join = (TextView) findViewById(R.id.c_join);
        this.c_momo = (TextView) findViewById(R.id.c_momo);
        this.c_end_time = (TextView) findViewById(R.id.c_end_time);
        this.btn_apply = (TextView) findViewById(R.id.btn_apply);
        this.btn_refer = (TextView) findViewById(R.id.btn_refer);
        this.btn_apply.setText(getString(R.string.apply_privilege));
        this.btn_apply.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.apply, 0, 0, 0);
        this.btn_refer.setText(getString(R.string.contact_house_sell));
        this.house_room_list_layout = (RelativeLayout) findViewById(R.id.house_room_list_layout);
        this.house_room_list = (NoScrollListView) findViewById(R.id.house_room_list);
        this.adapter = new CouponNewHouseAdapter(this);
        this.house_room_list.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.cid = getIntent().getStringExtra("id");
        this.ctype = getIntent().getStringExtra(INTENT_TYPE);
        GetEventInfoTask task = new GetEventInfoTask(this, this.cid, this.ctype);
        task.execute(new Object[0]);
        task.setOnFinish(new GetEventInfoTask.OnFinish() {
            public void onFinsh(Event v) {
                if (!TextUtils.isEmpty(v.getE_id())) {
                    CouponDetailsActivity.this.coupon = v;
                    CouponDetailsActivity.this.refreshUI();
                    return;
                }
                CouponDetailsActivity.this.showToast((int) R.string.text_no_result);
                CouponDetailsActivity.this.finish();
            }
        });
        this.house_room_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(CouponDetailsActivity.this, NewHouseDetailActivity.class);
                intent.putExtra("h_id", ((House) CouponDetailsActivity.this.adapter.getItem(position)).getH_id());
                intent.putExtra(NewHouseDetailActivity.INTENT_DISAPPER_EVENT, true);
                CouponDetailsActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshUI() {
        this.detail_info_scroll.smoothScrollTo(0, 0);
        if (this.coupon != null) {
            new MyCountimer((this.coupon.getE_endtime() * 1000) - System.currentTimeMillis(), 1000).start();
            this.cphone = this.coupon.getE_tel();
            if (this.cphone == null || TextUtils.isEmpty(this.cphone)) {
                this.cphone = AppArrays.default_tel;
            }
            this.btn_refer.setText(this.cphone);
            this.head_view.setTvTitleText(this.coupon.getE_title());
            boolean isShowPic = ((NewHouseApplication) this.mApplication).isEnableImg();
            if (this.coupon.getE_pic() == null || !isShowPic) {
                setImage(this.c_pic, StringUtils.EMPTY, (int) R.drawable.bg_default_ad, 1);
            } else {
                setImage(this.c_pic, this.coupon.getE_pic(), (int) R.drawable.bg_default_ad, 1);
            }
            this.c_name.setText(this.coupon.getE_title());
            this.c_join.setText(String.valueOf(this.coupon.getE_join()) + this.text_pub_join);
            this.c_end_time.setText(String.valueOf(this.activities_cutoff_time) + TimeUtil.toDateWithFormat(this.coupon.getE_endtime(), "yyyy年MM月dd日"));
            addBottomListener();
            List<House> about_houses = this.coupon.getE_about_houses();
            if (about_houses != null && about_houses.size() > 0) {
                this.house_room_list_layout.setVisibility(0);
                this.adapter.addAll(about_houses);
                this.adapter.notifyDataSetChanged();
            }
            String content = this.coupon.getE_content();
            if (content != null) {
                if (content.indexOf("\\r\\n") != -1) {
                    String momo = content.replaceAll("\\r\\n", "<br/>");
                    this.c_momo.setMovementMethod(LinkMovementMethod.getInstance());
                    this.c_momo.setText(Html.fromHtml(momo));
                } else {
                    this.c_momo.setText(Html.fromHtml(content));
                }
            }
            CharSequence text = this.c_momo.getText();
            if (text instanceof Spannable) {
                Spannable spannable = (Spannable) this.c_momo.getText();
                URLSpan[] urls = (URLSpan[]) spannable.getSpans(0, text.length(), URLSpan.class);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
                spannableStringBuilder.clearSpans();
                for (URLSpan url : urls) {
                    MyUrlSpan urlspan = null;
                    String[] coupon_momo = this.c_momo.getText().toString().split(url.getURL());
                    if (coupon_momo == null || coupon_momo.length <= 1) {
                        urlspan = new MyUrlSpan(url.getURL());
                    } else if (coupon_momo[1].charAt(0) == '/') {
                        urlspan = new MyUrlSpan(String.valueOf(url.getURL()) + "/");
                    }
                    if (!(spannable.getSpanStart(url) == -1 || spannable.getSpanEnd(url) == -1)) {
                        spannableStringBuilder.setSpan(urlspan, spannable.getSpanStart(url), spannable.getSpanEnd(url), 34);
                    }
                }
                this.c_momo.setText(spannableStringBuilder);
            }
        }
    }

    class MyUrlSpan extends ClickableSpan {
        private String url;

        public MyUrlSpan(String url2) {
            this.url = url2;
        }

        public void onClick(View v) {
            Intent intent = new Intent(CouponDetailsActivity.this, UrlGetActivity.class);
            intent.putExtra("url", this.url);
            CouponDetailsActivity.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void joinEvent() {
        Intent intent;
        String mobile = ((NewHouseApplication) this.mApplication).getMobile();
        if (mobile == null || TextUtils.isEmpty(mobile)) {
            intent = new Intent(this, UserLoginActivity.class);
            intent.putExtra("c_id", this.cid);
            intent.putExtra("coupon_type", this.ctype);
            intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 1);
        } else {
            intent = new Intent(this, UserInfoActivity.class);
            intent.putExtra(UserInfoActivity.INTENT_FROM, 1);
            intent.putExtra("coupon_type", this.ctype);
        }
        startActivityForResult(intent, 1);
    }

    private void addBottomListener() {
        this.btn_apply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CouponDetailsActivity.this.coupon != null) {
                    CouponDetailsActivity.this.joinEvent();
                    HouseAnalyse.onViewClick(CouponDetailsActivity.this, "报名", CouponDetailsActivity.this.ctype, CouponDetailsActivity.this.cid);
                }
            }
        });
        this.btn_refer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!TextUtils.isEmpty(CouponDetailsActivity.this.cphone)) {
                    try {
                        TelUtil.getCallIntent(CouponDetailsActivity.this.cphone, CouponDetailsActivity.this, CouponDetailsActivity.this.ctype);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void finish() {
        super.finish();
        if (getIntent() != null && getIntent().getBooleanExtra(APNActivity.INTENT_IS_APN, false) && !ActivityUtil.isAppOnForeground(this, MenuActivity.class.getName())) {
            startActivity(new Intent(this, SplashActivity.class));
        }
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            CouponDetailsActivity.this.btn_apply.setEnabled(false);
        }

        public void onTick(long millisUntilFinished) {
        }
    }
}
