package com.house365.newhouse.ui.secondsell;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.ui.common.AlbumFullScreenActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OfferPhotoesView {
    public static final int PHOTOHRAPH = 1;
    public static final int PHOTONONE = 0;
    public static final int PHOTORESOULT = 3;
    public static final int PHOTOZOOM = 2;
    /* access modifiers changed from: private */
    public Button btn_add;
    /* access modifiers changed from: private */
    public ImageView btn_item_add;
    /* access modifiers changed from: private */
    public List<String> c_thumb = new ArrayList();
    /* access modifiers changed from: private */
    public Context context;
    private String filename = (String.valueOf(CorePreferences.getAppTmpSDPath()) + "/photo.jpg");
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int index = msg.what;
            LinearLayout linear = (LinearLayout) OfferPhotoesView.this.linearItems.get(index);
            View unused = OfferPhotoesView.this.createItemView(linear, index, null);
            try {
                linear.addView(OfferPhotoesView.this.btn_item_add);
            } catch (Exception e) {
                linear.removeView(OfferPhotoesView.this.btn_item_add);
                linear.addView(OfferPhotoesView.this.btn_item_add);
            }
            if (index == 0) {
                try {
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                } catch (Exception e2) {
                    OfferPhotoesView.this.view_list_layout.removeView(linear);
                    OfferPhotoesView.this.view_list_layout.addView(linear);
                    linear.removeView(OfferPhotoesView.this.btn_item_add);
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                }
            } else if (index != OfferPhotoesView.this.lens - 1) {
                try {
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index - 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                } catch (Exception e3) {
                    OfferPhotoesView.this.view_list_layout.removeView(linear);
                    OfferPhotoesView.this.view_list_layout.addView(linear);
                    linear.removeView(OfferPhotoesView.this.btn_item_add);
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                }
            } else if (OfferPhotoesView.this.viewItems.size() == OfferPhotoesView.this.total) {
                try {
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index - 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    linear.removeView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView(linear);
                    OfferPhotoesView.this.view_list_layout.addView(linear);
                } catch (Exception e4) {
                    OfferPhotoesView.this.view_list_layout.removeView(linear);
                    OfferPhotoesView.this.view_list_layout.addView(linear);
                }
            } else {
                try {
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index - 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                } catch (Exception e5) {
                    OfferPhotoesView.this.view_list_layout.removeView(linear);
                    OfferPhotoesView.this.view_list_layout.addView(linear);
                    linear.removeView(OfferPhotoesView.this.btn_item_add);
                    if (linear.getChildCount() < OfferPhotoesView.this.len) {
                        linear.addView(OfferPhotoesView.this.btn_item_add);
                        OfferPhotoesView.this.view_list_layout.removeView(linear);
                        OfferPhotoesView.this.view_list_layout.addView(linear);
                        return;
                    }
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).removeView(OfferPhotoesView.this.btn_item_add);
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(index + 1)).addView(OfferPhotoesView.this.btn_item_add);
                    OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                    OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(index + 1));
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int len = 3;
    /* access modifiers changed from: private */
    public int lens;
    /* access modifiers changed from: private */
    public ArrayList<LinearLayout> linearItems = new ArrayList<>();
    private BroadcastReceiver mUploadFinish = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            OfferPhotoesView.this.btn_add.setVisibility(8);
            OfferPhotoesView.this.view_list_layout.requestFocus();
            int index = OfferPhotoesView.this.viewItems.size() / OfferPhotoesView.this.len;
            ((LinearLayout) OfferPhotoesView.this.linearItems.get(index)).removeView(OfferPhotoesView.this.btn_item_add);
            if (((LinearLayout) OfferPhotoesView.this.linearItems.get(index)).getChildCount() < OfferPhotoesView.this.len) {
                OfferPhotoesView.this.handler.sendEmptyMessage(index);
            }
        }
    };
    /* access modifiers changed from: private */
    public File tempfile;
    /* access modifiers changed from: private */
    public int total = 10;
    /* access modifiers changed from: private */
    public ArrayList<View> viewItems = new ArrayList<>();
    /* access modifiers changed from: private */
    public LinearLayout view_list_layout;

    public OfferPhotoesView(Context context2, LinearLayout view_list_layout2, Button btn_add2) {
        this.context = context2;
        this.view_list_layout = view_list_layout2;
        this.btn_add = btn_add2;
        context2.registerReceiver(this.mUploadFinish, new IntentFilter(ActionCode.INTENT_ACTION_UPLOAD_PIC_FINISH));
        this.tempfile = new File(this.filename);
        initAddBtn();
        initData();
    }

    public BroadcastReceiver getmUploadFinish() {
        return this.mUploadFinish;
    }

    public void setmUploadFinish(BroadcastReceiver mUploadFinish2) {
        this.mUploadFinish = mUploadFinish2;
    }

    public File getTempfile() {
        return this.tempfile;
    }

    public void setTempfile(File tempfile2) {
        this.tempfile = tempfile2;
    }

    public ArrayList<View> getViewItems() {
        return this.viewItems;
    }

    public void setViewItems(ArrayList<View> viewItems2) {
        this.viewItems = viewItems2;
    }

    public List<String> getC_thumb() {
        for (int i = 0; i < this.c_thumb.size(); i++) {
            if (TextUtils.isEmpty(this.c_thumb.get(i))) {
                this.c_thumb.remove(this.c_thumb.get(i));
            }
        }
        return this.c_thumb;
    }

    public void setC_thumb(List<String> c_thumb2) {
        this.c_thumb = c_thumb2;
    }

    private void initAddBtn() {
        ViewGroup.LayoutParams btn_lp = new ViewGroup.LayoutParams(-2, -2);
        this.btn_item_add = new ImageView(this.context);
        this.btn_item_add.setImageDrawable(this.context.getResources().getDrawable(R.drawable.bg_add_pic));
        btn_lp.width = calculateDpToPx(this.context, 90);
        btn_lp.height = -2;
        this.btn_item_add.setLayoutParams(btn_lp);
        this.btn_item_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                OfferPhotoesView.this.chooseImage();
            }
        });
    }

    /* access modifiers changed from: private */
    public View createItemView(LinearLayout linearview, int tag, Bitmap bitmap) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-2, -2);
        lp.width = -2;
        lp.height = calculateDpToPx(this.context, 60);
        lp.gravity = 16;
        linearview.setLayoutParams(lp);
        linearview.setOrientation(0);
        final LinearLayout itemView = (LinearLayout) LayoutInflater.from(this.context).inflate((int) R.layout.offer_pic_item_view, (ViewGroup) null);
        ImageView default_img = (ImageView) itemView.findViewById(R.id.default_img);
        final ImageView delete_img = (ImageView) itemView.findViewById(R.id.delete_img);
        if (bitmap != null) {
            default_img.setImageBitmap(bitmap);
        } else {
            default_img.setImageDrawable(this.context.getResources().getDrawable(R.drawable.bg_add_pic));
        }
        itemView.setLayoutParams(new ViewGroup.LayoutParams(calculateDpToPx(this.context, 90), -2));
        itemView.setPadding(20, 0, 0, 0);
        itemView.setAnimation(AnimationUtils.loadAnimation(this.context, R.anim.fade_in));
        linearview.addView(itemView);
        itemView.setTag(Integer.valueOf(tag));
        this.viewItems.add(itemView);
        for (int i = 0; i < this.viewItems.size(); i++) {
            View item_view = this.viewItems.get(i);
            ImageView image = (ImageView) item_view.findViewById(R.id.default_img);
            ViewGroup.LayoutParams lp_image = default_img.getLayoutParams();
            lp_image.width = calculateDpToPx(this.context, 50);
            lp_image.height = calculateDpToPx(this.context, 40);
            image.setLayoutParams(lp_image);
            ((ImageView) item_view.findViewById(R.id.delete_img)).setTag(Integer.valueOf(i));
            ((BaseCommonActivity) this.context).setImage(image, this.c_thumb.get(i), (int) R.drawable.ic_launcher, 1);
        }
        default_img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(OfferPhotoesView.this.context, AlbumFullScreenActivity.class);
                intent.putExtra(AlbumFullScreenActivity.INTENT_ALBUM_POS, Integer.parseInt(delete_img.getTag().toString()));
                intent.putStringArrayListExtra(AlbumFullScreenActivity.INTENT_ALBUM_PIC_LIST, (ArrayList) OfferPhotoesView.this.c_thumb);
                ((BaseCommonActivity) OfferPhotoesView.this.context).startActivity(intent);
            }
        });
        delete_img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int viewSize = OfferPhotoesView.this.viewItems.size();
                OfferPhotoesView.this.viewItems.remove(itemView);
                if (viewSize < OfferPhotoesView.this.total) {
                    int curLen = viewSize / OfferPhotoesView.this.len;
                    if (viewSize % OfferPhotoesView.this.len != 0) {
                        curLen++;
                    }
                    int currIndex = Integer.parseInt(itemView.getTag().toString());
                    ((LinearLayout) OfferPhotoesView.this.linearItems.get(currIndex)).removeView(itemView);
                    for (int i = 0; i < OfferPhotoesView.this.c_thumb.size(); i++) {
                        if (i == Integer.parseInt(delete_img.getTag().toString())) {
                            OfferPhotoesView.this.c_thumb.remove(OfferPhotoesView.this.c_thumb.get(i));
                        }
                    }
                    itemView.setAnimation(AnimationUtils.loadAnimation(OfferPhotoesView.this.context, R.anim.fade_out));
                    for (int i2 = currIndex; i2 < curLen; i2++) {
                        LinearLayout curLinear = (LinearLayout) OfferPhotoesView.this.linearItems.get(i2);
                        LinearLayout nextLinear = (LinearLayout) OfferPhotoesView.this.linearItems.get(i2 + 1);
                        curLinear.removeView(OfferPhotoesView.this.btn_item_add);
                        if (nextLinear != null) {
                            nextLinear.removeView(OfferPhotoesView.this.btn_item_add);
                            if (nextLinear.getChildAt(0) != null) {
                                View next_firstView = nextLinear.getChildAt(0);
                                nextLinear.removeView(next_firstView);
                                next_firstView.setTag(Integer.valueOf(i2));
                                curLinear.addView(next_firstView);
                            }
                        }
                    }
                    if (curLen > 0) {
                        ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).removeView(OfferPhotoesView.this.btn_item_add);
                        ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen - 1)).removeView(OfferPhotoesView.this.btn_item_add);
                        if (((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen - 1)).getChildCount() >= OfferPhotoesView.this.len) {
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).removeView(OfferPhotoesView.this.btn_item_add);
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).addView(OfferPhotoesView.this.btn_item_add);
                            OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(curLen));
                            OfferPhotoesView.this.view_list_layout.addView((View) OfferPhotoesView.this.linearItems.get(curLen));
                        } else {
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen - 1)).removeView(OfferPhotoesView.this.btn_item_add);
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen - 1)).addView(OfferPhotoesView.this.btn_item_add);
                        }
                    } else {
                        ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).removeView(OfferPhotoesView.this.btn_item_add);
                        if (((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).getChildCount() < OfferPhotoesView.this.len) {
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).removeView(OfferPhotoesView.this.btn_item_add);
                            ((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).addView(OfferPhotoesView.this.btn_item_add);
                        } else {
                            LinearLayout addlinear = new LinearLayout(OfferPhotoesView.this.context);
                            addlinear.addView(OfferPhotoesView.this.btn_item_add);
                            OfferPhotoesView.this.view_list_layout.addView(addlinear);
                        }
                    }
                    if (((LinearLayout) OfferPhotoesView.this.linearItems.get(curLen)).getChildCount() <= 0) {
                        OfferPhotoesView.this.view_list_layout.removeView((View) OfferPhotoesView.this.linearItems.get(curLen));
                    }
                }
            }
        });
        return linearview;
    }

    /* access modifiers changed from: private */
    public void chooseImage() {
        ActivityUtil.showPhotoChooseDialog(this.context, new ActivityUtil.PhotoDialogOnChooseListener() {
            public void choosePhoto(DialogInterface dialog) {
                if (OfferPhotoesView.this.tempfile != null) {
                    Intent shootIntent = new Intent("android.media.action.IMAGE_CAPTURE");
                    shootIntent.putExtra("output", Uri.fromFile(OfferPhotoesView.this.tempfile));
                    ((Activity) OfferPhotoesView.this.context).startActivityForResult(shootIntent, 1);
                    return;
                }
                Toast.makeText(OfferPhotoesView.this.context, (int) R.string.no_sd_info, 0).show();
            }

            public void chooseAlbum(DialogInterface dialog) {
                Intent intentAlbum = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
                intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                ((Activity) OfferPhotoesView.this.context).startActivityForResult(intentAlbum, 2);
            }
        });
    }

    public static int calculateDpToPx(Context context2, int padding_in_dp) {
        return (int) ((((float) padding_in_dp) * context2.getResources().getDisplayMetrics().density) + 0.5f);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.lens = this.total / this.len;
        if (this.total % this.len != 0) {
            this.lens++;
        }
        for (int i = 0; i < this.lens; i++) {
            this.linearItems.add(new LinearLayout(this.context));
        }
        this.btn_add.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OfferPhotoesView.this.chooseImage();
            }
        });
    }
}
