package com.house365.newhouse.ui.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.house365.core.activity.BaseListActivity;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.task.DeletePublishTask;
import com.house365.newhouse.ui.secondhouse.adapter.HousePubLishAdapter;
import com.house365.newhouse.ui.secondrent.SecondRentOfferActivity;
import com.house365.newhouse.ui.secondrent.SecondRentPublishActivity;
import com.house365.newhouse.ui.secondsell.SecondSellOfferActivity;
import com.house365.newhouse.ui.secondsell.SecondSellPublishActivity;
import java.util.HashMap;
import java.util.List;

public class MyPublishActivity extends BaseListActivity {
    public static String INTENT_HOUSE = "publish_house";
    public static String INTENT_PUBLISH_OFFER = "offer";
    public static String INTENT_PUBLISH_TYPE = "publishType";
    private RadioButton bt_public_tab_rent;
    private RadioButton bt_public_tab_rent_offer;
    private RadioButton bt_public_tab_sell;
    private RadioButton bt_public_tab_sell_offer;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public HeadNavigateView head_view;
    int i = 0;
    /* access modifiers changed from: private */
    public ListView list;
    /* access modifiers changed from: private */
    public HashMap<String, List<PublishHouse>> mapData = new HashMap<>();
    /* access modifiers changed from: private */
    public View nodata_layout;
    /* access modifiers changed from: private */
    public boolean publish;
    /* access modifiers changed from: private */
    public int publishType = 4;
    /* access modifiers changed from: private */
    public HousePubLishAdapter requireAdapter;
    private RadioGroup tab_group;
    private RadioGroup tab_group_other;
    /* access modifiers changed from: private */
    public TextView tv_nodata;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.user_common_layout);
        this.context = this;
        this.publishType = getIntent().getIntExtra(INTENT_PUBLISH_TYPE, 4);
        this.publish = getIntent().getBooleanExtra(INTENT_PUBLISH_OFFER, false);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyPublishActivity.this.finish();
            }
        });
        this.head_view.getBtn_right().setVisibility(0);
        this.head_view.getBtn_right().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MyPublishActivity.this.i++;
                List<PublishHouse> listdata = MyPublishActivity.this.requireAdapter.getList();
                if (listdata == null || listdata.size() <= 0) {
                    MyPublishActivity.this.head_view.getBtn_right().setVisibility(4);
                    MyPublishActivity.this.requireAdapter.setType(0);
                    MyPublishActivity.this.list.setAdapter((ListAdapter) MyPublishActivity.this.requireAdapter);
                    return;
                }
                if (MyPublishActivity.this.i == 1) {
                    MyPublishActivity.this.requireAdapter.setType(1);
                    MyPublishActivity.this.head_view.getBtn_right().setText((int) R.string.text_search_finish);
                } else if (MyPublishActivity.this.requireAdapter.getType() == 1) {
                    MyPublishActivity.this.head_view.getBtn_right().setText((int) R.string.text_mypublish_edit);
                    MyPublishActivity.this.requireAdapter.setType(0);
                } else {
                    MyPublishActivity.this.head_view.getBtn_right().setText((int) R.string.text_search_finish);
                    MyPublishActivity.this.requireAdapter.setType(1);
                }
                MyPublishActivity.this.list.setAdapter((ListAdapter) MyPublishActivity.this.requireAdapter);
            }
        });
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        this.tab_group = (RadioGroup) findViewById(R.id.public_tab_group);
        this.tab_group_other = (RadioGroup) findViewById(R.id.tab_group);
        this.tab_group_other.setVisibility(8);
        this.tab_group.setVisibility(0);
        this.bt_public_tab_sell = (RadioButton) findViewById(R.id.bt_public_tab_sell);
        this.bt_public_tab_rent = (RadioButton) findViewById(R.id.bt_public_tab_rent);
        this.bt_public_tab_sell_offer = (RadioButton) findViewById(R.id.bt_public_tab_sell_offer);
        this.bt_public_tab_rent_offer = (RadioButton) findViewById(R.id.bt_public_tab_rent_offer);
        if (this.publish) {
            if (this.publishType == 4) {
                this.bt_public_tab_sell_offer.setChecked(true);
            } else if (this.publishType == 5) {
                this.bt_public_tab_rent_offer.setChecked(true);
            }
        } else if (this.publishType == 4) {
            this.bt_public_tab_sell.setChecked(true);
        } else if (this.publishType == 5) {
            this.bt_public_tab_rent.setChecked(true);
        }
        this.list = getListView();
        this.requireAdapter = new HousePubLishAdapter(this, this.publishType);
        this.requireAdapter.setListView(this.list);
        this.requireAdapter.setNodata_layout(this.nodata_layout);
        this.requireAdapter.setTv_nodata(this.tv_nodata);
        this.requireAdapter.setBtn_edit(this.head_view.getBtn_right());
        this.list.setAdapter((ListAdapter) this.requireAdapter);
        this.tab_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_public_tab_sell) {
                    MyPublishActivity.this.publish = false;
                    MyPublishActivity.this.publishType = 4;
                    MyPublishActivity.this.requireAdapter.setOffer(MyPublishActivity.this.publish);
                    MyPublishActivity.this.requireAdapter.setPublicType(MyPublishActivity.this.publishType);
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("sellHouseInfo"), MyPublishActivity.this.publishType);
                } else if (checkedId == R.id.bt_public_tab_rent) {
                    MyPublishActivity.this.publish = false;
                    MyPublishActivity.this.publishType = 5;
                    MyPublishActivity.this.requireAdapter.setOffer(MyPublishActivity.this.publish);
                    MyPublishActivity.this.requireAdapter.setPublicType(MyPublishActivity.this.publishType);
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("rentHouseInfo"), MyPublishActivity.this.publishType);
                } else if (checkedId == R.id.bt_public_tab_sell_offer) {
                    MyPublishActivity.this.publish = true;
                    MyPublishActivity.this.publishType = 4;
                    MyPublishActivity.this.requireAdapter.setOffer(MyPublishActivity.this.publish);
                    MyPublishActivity.this.requireAdapter.setPublicType(MyPublishActivity.this.publishType);
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("sellOfferInfo"), MyPublishActivity.this.publishType);
                } else if (checkedId == R.id.bt_public_tab_rent_offer) {
                    MyPublishActivity.this.publish = true;
                    MyPublishActivity.this.publishType = 5;
                    MyPublishActivity.this.requireAdapter.setOffer(MyPublishActivity.this.publish);
                    MyPublishActivity.this.requireAdapter.setPublicType(MyPublishActivity.this.publishType);
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("rentOfferInfo"), MyPublishActivity.this.publishType);
                }
            }
        });
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                PublishHouse house = (PublishHouse) MyPublishActivity.this.requireAdapter.getItem(position);
                Intent intent = new Intent();
                Class<?> clz = null;
                if (!MyPublishActivity.this.publish) {
                    if (MyPublishActivity.this.publishType == 4) {
                        clz = SecondSellPublishActivity.class;
                    }
                    if (MyPublishActivity.this.publishType == 5) {
                        clz = SecondRentPublishActivity.class;
                    }
                } else {
                    if (MyPublishActivity.this.publishType == 4) {
                        clz = SecondSellOfferActivity.class;
                    }
                    if (MyPublishActivity.this.publishType == 5) {
                        clz = SecondRentOfferActivity.class;
                    }
                }
                intent.putExtra(MyPublishActivity.INTENT_HOUSE, house);
                intent.setClass(MyPublishActivity.this.context, clz);
                MyPublishActivity.this.startActivity(intent);
            }
        });
        this.list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                MyPublishActivity.this.requireAdapter.setLongclick(true);
                MyPublishActivity.this.delete(position);
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void delete(final int position) {
        new AlertDialog.Builder(this.context).setItems(new String[]{"删除"}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new DeletePublishTask(MyPublishActivity.this.context, MyPublishActivity.this.mapData, MyPublishActivity.this.requireAdapter, MyPublishActivity.this.nodata_layout, MyPublishActivity.this.head_view.getBtn_right(), MyPublishActivity.this.tv_nodata, MyPublishActivity.this.publishType, position, MyPublishActivity.this.list, MyPublishActivity.this.publish).execute(new Object[0]);
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.publishType == 4 || this.publishType == 5) {
            this.list.setAdapter((ListAdapter) this.requireAdapter);
        }
        new GetPublishHouseListTask(this.context, this.publishType).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.head_view.setTvTitleText((int) R.string.text_mypublish);
    }

    /* access modifiers changed from: private */
    public void refreshUI(List<PublishHouse> v, int publishType2) {
        if (publishType2 == 4 || publishType2 == 5) {
            this.requireAdapter.clear();
            this.requireAdapter.notifyDataSetChanged();
            if (v == null || v.size() <= 0) {
                this.head_view.getBtn_right().setVisibility(4);
                this.requireAdapter.notifyDataSetChanged();
                this.list.setVisibility(8);
                this.nodata_layout.setVisibility(0);
                setNoDataUI();
                return;
            }
            this.requireAdapter.addAll(v);
            this.requireAdapter.notifyDataSetChanged();
            this.list.setVisibility(0);
            this.nodata_layout.setVisibility(8);
            this.head_view.getBtn_right().setVisibility(0);
        }
    }

    private class GetPublishHouseListTask extends CommonAsyncTask<HashMap<String, List<PublishHouse>>> {
        int publishType;
        private HashMap<String, List<PublishHouse>> taskData = new HashMap<>();

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((HashMap<String, List<PublishHouse>>) ((HashMap) obj));
        }

        public GetPublishHouseListTask(Context context, int publishType2) {
            super(context, publishType2);
            this.loadingresid = R.string.loading;
            this.publishType = publishType2;
        }

        public HashMap<String, List<PublishHouse>> onDoInBackgroup() {
            try {
                List<PublishHouse> sellHouseInfo = ((NewHouseApplication) this.mApplication).queryPublishRec(10, 4);
                List<PublishHouse> rentHouseInfo = ((NewHouseApplication) this.mApplication).queryPublishRec(10, 5);
                List<PublishHouse> sellOfferInfo = ((NewHouseApplication) this.mApplication).queryPublishRec(11, 4);
                List<PublishHouse> rentOfferInfo = ((NewHouseApplication) this.mApplication).queryPublishRec(11, 5);
                this.taskData.put("sellHouseInfo", sellHouseInfo);
                this.taskData.put("rentHouseInfo", rentHouseInfo);
                this.taskData.put("sellOfferInfo", sellOfferInfo);
                this.taskData.put("rentOfferInfo", rentOfferInfo);
            } catch (ReflectException e) {
                e.printStackTrace();
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return this.taskData;
        }

        public void onAfterDoInBackgroup(HashMap<String, List<PublishHouse>> taskData2) {
            MyPublishActivity.this.mapData = taskData2;
            MyPublishActivity.this.requireAdapter.setMapData(MyPublishActivity.this.mapData);
            MyPublishActivity.this.requireAdapter.setOffer(MyPublishActivity.this.publish);
            MyPublishActivity.this.requireAdapter.setPublicType(this.publishType);
            if (!MyPublishActivity.this.publish) {
                if (this.publishType == 5) {
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("rentHouseInfo"), this.publishType);
                } else if (this.publishType == 4) {
                    MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("sellHouseInfo"), this.publishType);
                }
            } else if (this.publishType == 4) {
                MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("sellOfferInfo"), this.publishType);
            } else if (this.publishType == 5) {
                MyPublishActivity.this.refreshUI((List) MyPublishActivity.this.mapData.get("rentOfferInfo"), this.publishType);
            }
        }
    }

    public void setNoDataUI() {
        this.tv_nodata.setText((int) R.string.text_publish_nodata);
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.requireAdapter != null) {
            this.requireAdapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }
}
