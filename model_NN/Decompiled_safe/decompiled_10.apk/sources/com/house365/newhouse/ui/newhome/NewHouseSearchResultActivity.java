package com.house365.newhouse.ui.newhome;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.json.JSONObject;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TextUtil;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayGestureDetector;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.ZoomEvent;
import com.house365.core.util.map.baidu.lazyload.LazyLoadException;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.LocationTask;
import com.house365.newhouse.task.NewHouseSearchTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.mapsearch.HouseBlockOverlay;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAdapter;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseSearchResultActivity extends BaseBaiduMapActivity {
    public static String INTENT_FROM_NEARBY = "from_nearby";
    /* access modifiers changed from: private */
    public NewHouseAdapter adapter;
    private View black_alpha_view;
    private String blockvalue;
    private RadioGroup bt_map_list_group;
    private Bundle bundle;
    private ArrayList<String> channelList;
    /* access modifiers changed from: private */
    public String channelvalue;
    /* access modifiers changed from: private */
    public boolean checkedRegion = true;
    private BroadcastReceiver chosedFinished;
    /* access modifiers changed from: private */
    public boolean click_nearby_refresh;
    /* access modifiers changed from: private */
    public RadioGroup condition_group;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String districtvalue;
    private int form_subway;
    private int from_litter_home;
    /* access modifiers changed from: private */
    public boolean from_nearby;
    boolean hasInitedMap;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public HouseBlockOverlay houseBlock;
    /* access modifiers changed from: private */
    public ManagedOverlay houseOverlay;
    private boolean isVirtual;
    private String keywords;
    /* access modifiers changed from: private */
    public Animation leftInAnim;
    /* access modifiers changed from: private */
    public Animation leftOutAnim;
    private PullToRefreshListView listView;
    private View loadingLayout;
    /* access modifiers changed from: private */
    public TextView location_addr;
    private Drawable location_marker;
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    private BroadcastReceiver mChangeCity;
    /* access modifiers changed from: private */
    public Location mLocation;
    private BroadcastReceiver mLocationFinish;
    /* access modifiers changed from: private */
    public boolean mapChoose;
    MapController mapController;
    /* access modifiers changed from: private */
    public View mapLoading;
    MapView mapView;
    /* access modifiers changed from: private */
    public int maptype;
    private Drawable marker;
    private ManagedOverlay myPositionOverlay;
    /* access modifiers changed from: private */
    public View nearby_refresh_layout;
    private View nodata_layout;
    private final String[] orders = {ActionCode.PREFERENCE_SEARCH_DEFAULT, "价格降序", "价格升序", "开盘时间由远及近", "开盘时间由近及远", "关注度降序", "关注度升序"};
    /* access modifiers changed from: private */
    public String ordervalue = StringUtils.EMPTY;
    /* access modifiers changed from: private */
    public final String[] ordervalues = {"0", "2", "3", "4", "5", "6", "7"};
    OverlayManager overlayManager;
    private ArrayList<String> priceList;
    private View price_layout;
    /* access modifiers changed from: private */
    public String pricevalue;
    /* access modifiers changed from: private */
    public final int[] radious;
    /* access modifiers changed from: private */
    public ArrayList<String> radiousList = new ArrayList<>();
    /* access modifiers changed from: private */
    public int radiusvalue;
    private RefreshInfo refreshInfo = new RefreshInfo();
    /* access modifiers changed from: private */
    public ArrayList<String> regionList;
    /* access modifiers changed from: private */
    public View region_layout;
    /* access modifiers changed from: private */
    public List<House> result_houseList = new ArrayList();
    /* access modifiers changed from: private */
    public Animation rightInAnim;
    /* access modifiers changed from: private */
    public Animation rightOutAnim;
    /* access modifiers changed from: private */
    public HouseBaseInfo searchConfig;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    /* access modifiers changed from: private */
    public ArrayList<String> sortList = new ArrayList<>();
    private View sort_layout;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, LinkedHashMap<String, Station>> subwayMap;
    private String tag_id;
    /* access modifiers changed from: private */
    public List<TextView> textViews = new ArrayList();
    /* access modifiers changed from: private */
    public TextView text_price;
    /* access modifiers changed from: private */
    public TextView text_region;
    /* access modifiers changed from: private */
    public TextView text_sort;
    private String text_subway;
    /* access modifiers changed from: private */
    public TextView text_type;
    private TextView total;
    private View type_layout;
    /* access modifiers changed from: private */
    public ViewSwitcher viewSwitcher;
    /* access modifiers changed from: private */
    public List<View> views = new ArrayList();

    public NewHouseSearchResultActivity() {
        int[] iArr = new int[5];
        iArr[0] = 1000;
        iArr[1] = 2500;
        iArr[2] = 5000;
        iArr[3] = 10000;
        this.radious = iArr;
        this.hasInitedMap = false;
        this.maptype = 11;
        this.mChangeCity = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                NewHouseSearchResultActivity.this.getConfigInfo();
            }
        };
        this.mLocationFinish = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int changeCity = intent.getIntExtra(ActionCode.LOCATION_FINISH, 0);
                int changetag = intent.getIntExtra(ActionCode.LOCATION_TAG, 0);
                switch (changeCity) {
                    case 1:
                        NewHouseSearchResultActivity.this.setMyLocation();
                        NewHouseSearchResultActivity.this.mapLoading.setVisibility(8);
                        NewHouseSearchResultActivity.this.refreshData();
                        return;
                    case 2:
                    default:
                        return;
                    case 3:
                        if (changetag == 11) {
                            NewHouseSearchResultActivity.this.finish();
                            return;
                        }
                        return;
                }
            }
        };
        this.chosedFinished = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE).equals(ActionCode.NEW_SEARCH)) {
                    if (NewHouseSearchResultActivity.this.mApplication.getCityName().equals("西安") && NewHouseSearchResultActivity.this.text_region.getText().equals(NewHouseSearchResultActivity.this.getResources().getText(R.string.btn_region_text))) {
                        NewHouseSearchResultActivity.this.text_region.setText((int) R.string.btn_xian_region_text);
                    }
                    int type = SearchConditionPopView.getTagFlag(NewHouseSearchResultActivity.this.text_region.getTag());
                    if (NewHouseSearchResultActivity.this.from_nearby && type == 10) {
                        NewHouseSearchResultActivity.this.radiusvalue = NewHouseSearchResultActivity.this.radious[SearchConditionPopView.getIndex(NewHouseSearchResultActivity.this.radiousList, SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag()))];
                    } else if (type == 1) {
                        NewHouseSearchResultActivity.this.districtvalue = SearchConditionPopView.setStringValue(NewHouseSearchResultActivity.this.text_region, NewHouseSearchResultActivity.this.districtvalue, HouseBaseInfo.DISTRICT, NewHouseSearchResultActivity.this.searchConfig);
                        if (!TextUtils.isEmpty(NewHouseSearchResultActivity.this.districtvalue) && !NewHouseSearchResultActivity.this.districtvalue.equals("0")) {
                            NewHouseSearchResultActivity.this.mLocation = null;
                            NewHouseSearchResultActivity.this.radiusvalue = 0;
                        }
                    } else if (type == 2) {
                        String subWay = SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag());
                        if (subWay == null || subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                            NewHouseSearchResultActivity.this.mLocation = null;
                            NewHouseSearchResultActivity.this.radiusvalue = 0;
                        } else if (!(subWay == null || subWay.indexOf("+") == -1)) {
                            String metro = subWay.substring(0, subWay.indexOf("+"));
                            Station station = (Station) NewHouseSearchResultActivity.this.searchConfig.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()));
                            NewHouseSearchResultActivity.this.mLocation = new Location(StringUtils.EMPTY);
                            NewHouseSearchResultActivity.this.mLocation.setLatitude(station.getX());
                            NewHouseSearchResultActivity.this.mLocation.setLongitude(station.getY());
                            NewHouseSearchResultActivity.this.radiusvalue = 2500;
                            NewHouseSearchResultActivity.this.districtvalue = "0";
                        }
                    }
                    NewHouseSearchResultActivity.this.channelvalue = SearchConditionPopView.setStringValue(NewHouseSearchResultActivity.this.text_type, NewHouseSearchResultActivity.this.channelvalue, "channel", NewHouseSearchResultActivity.this.searchConfig);
                    NewHouseSearchResultActivity.this.pricevalue = SearchConditionPopView.setStringValue(NewHouseSearchResultActivity.this.text_price, NewHouseSearchResultActivity.this.pricevalue, "price", NewHouseSearchResultActivity.this.searchConfig);
                    String sortData = SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_sort.getTag());
                    if (!TextUtils.isEmpty(sortData) && !sortData.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                        NewHouseSearchResultActivity.this.ordervalue = NewHouseSearchResultActivity.this.ordervalues[SearchConditionPopView.getIndex(NewHouseSearchResultActivity.this.sortList, sortData)];
                    }
                    NewHouseSearchResultActivity.this.refreshData();
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((!this.click_nearby_refresh && !this.from_nearby) || requestCode != 1) {
            return;
        }
        if (DeviceUtil.isOpenLoaction(this.context)) {
            relocation();
        } else {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        setContentView(LayoutInflater.from(this.context).inflate((int) R.layout.new_search_result, (ViewGroup) null));
        this.bundle = getIntent().getExtras();
        this.keywords = this.bundle.getString("keywords");
        this.form_subway = this.bundle.getInt("FROM_SUBWAY");
        this.from_litter_home = getIntent().getIntExtra("FROM_LITTER_HOME", 0);
        this.listView = (PullToRefreshListView) findViewById(R.id.list);
        this.mApplication = (NewHouseApplication) getApplication();
        registerReceiver(this.mLocationFinish, new IntentFilter(ActionCode.INTENT_ACTION_LOCATION_FINISH));
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(false);
        this.searchPop.setShowOnView(true);
        this.searchPop.setSearch_type(ActionCode.NEW_SEARCH);
        this.condition_group = this.searchPop.getCondition_group();
        this.condition_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                NewHouseSearchResultActivity.this.searchPop.setShowGroup(true);
                if (checkedId == R.id.rb_region) {
                    NewHouseSearchResultActivity.this.searchPop.setGradeData(NewHouseSearchResultActivity.this.regionList, NewHouseSearchResultActivity.this.text_region, SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag()), 1);
                    NewHouseSearchResultActivity.this.checkedRegion = true;
                } else if (checkedId == R.id.rb_subway) {
                    NewHouseSearchResultActivity.this.searchPop.setGradeData(NewHouseSearchResultActivity.this.subwayMap, NewHouseSearchResultActivity.this.text_region, SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag()), 2);
                    NewHouseSearchResultActivity.this.checkedRegion = false;
                }
            }
        });
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
    }

    private void doTask() {
        NewHouseSearchTask task = new NewHouseSearchTask(this.context, "house", this.listView, this.refreshInfo, this.adapter, this.nodata_layout, this.mLocation, this.radiusvalue, this.districtvalue, this.channelvalue, this.pricevalue, this.blockvalue, this.ordervalue, this.keywords, this.tag_id);
        task.execute(new Object[0]);
        task.setOnFinish(new NewHouseSearchTask.OnFinish() {
            public void onFinsh(List<House> ls) {
                NewHouseSearchResultActivity.this.result_houseList.clear();
                if (ls == null || ls.size() <= 0) {
                    NewHouseSearchResultActivity.this.houseOverlay.getOverlayItems().clear();
                    NewHouseSearchResultActivity.this.houseOverlay.removeAll();
                    NewHouseSearchResultActivity.this.result_houseList.clear();
                    return;
                }
                NewHouseSearchResultActivity.this.result_houseList.addAll(ls);
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.refreshInfo.refresh = true;
        doTask();
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.refreshInfo.refresh = false;
        doTask();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.adapter.clear();
        unregisterReceiver(this.chosedFinished);
        unregisterReceiver(this.mLocationFinish);
        unregisterReceiver(this.mChangeCity);
    }

    /* access modifiers changed from: private */
    public void updataFinish() {
        Intent intent = new Intent();
        Bundle refreshBundle = new Bundle();
        refreshBundle.putString("districtvalue", this.districtvalue);
        refreshBundle.putString("channelvalue", this.channelvalue);
        refreshBundle.putString("pricevalue", this.pricevalue);
        if (this.text_region.getTag() != null) {
            refreshBundle.putString("distsubway", this.text_region.getTag().toString());
        }
        intent.putExtras(refreshBundle);
        setResult(-1, intent);
        LocationTask.isdoing = false;
        finish();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.sortList = (ArrayList) AppMethods.toList(this.orders);
        this.radiousList = (ArrayList) AppMethods.toList(AppArrays.getRadious());
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewHouseSearchResultActivity.this.updataFinish();
            }
        });
        this.total = (TextView) findViewById(R.id.total);
        this.region_layout = findViewById(R.id.newhouse_region_layout);
        this.price_layout = findViewById(R.id.newhouse_price_layout);
        this.type_layout = findViewById(R.id.newhouse_type_layout);
        this.sort_layout = findViewById(R.id.newhouse_sort_layout);
        this.text_region = (TextView) findViewById(R.id.text_newhouse_region);
        this.text_price = (TextView) findViewById(R.id.text_newhouse_price);
        this.text_type = (TextView) findViewById(R.id.text_newhouse_type);
        this.text_sort = (TextView) findViewById(R.id.text_newhouse_sort);
        this.views.add(this.region_layout);
        this.views.add(this.price_layout);
        this.views.add(this.type_layout);
        this.views.add(this.sort_layout);
        this.textViews.add(this.text_region);
        this.textViews.add(this.text_price);
        this.textViews.add(this.text_type);
        this.textViews.add(this.text_sort);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.nearby_refresh_layout = findViewById(R.id.nearby_refresh_layout);
        this.loadingLayout = findViewById(R.id.loadingLayout);
        this.mapLoading = findViewById(R.id.mapLoading);
        this.location_addr = (TextView) findViewById(R.id.location_addr);
        this.bt_map_list_group = (RadioGroup) findViewById(R.id.bt_map_list_group);
        this.bt_map_list_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bt_list) {
                    if (NewHouseSearchResultActivity.this.from_nearby) {
                        NewHouseSearchResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    }
                    NewHouseSearchResultActivity.this.viewSwitcher.setOutAnimation(NewHouseSearchResultActivity.this.rightOutAnim);
                    NewHouseSearchResultActivity.this.viewSwitcher.setInAnimation(NewHouseSearchResultActivity.this.leftInAnim);
                    NewHouseSearchResultActivity.this.viewSwitcher.setDisplayedChild(0);
                    NewHouseSearchResultActivity.this.mapChoose = false;
                } else if (checkedId == R.id.bt_map) {
                    NewHouseSearchResultActivity.this.nearby_refresh_layout.setVisibility(8);
                    NewHouseSearchResultActivity.this.viewSwitcher.setOutAnimation(NewHouseSearchResultActivity.this.leftOutAnim);
                    NewHouseSearchResultActivity.this.viewSwitcher.setInAnimation(NewHouseSearchResultActivity.this.rightInAnim);
                    NewHouseSearchResultActivity.this.viewSwitcher.setDisplayedChild(1);
                    NewHouseSearchResultActivity.this.mapLoading.setVisibility(8);
                    NewHouseSearchResultActivity.this.mapChoose = true;
                    NewHouseSearchResultActivity.this.initMap();
                }
            }
        });
        this.viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
        this.leftOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        this.rightInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        this.rightOutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        this.leftInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        this.mapView = (MapView) findViewById(R.id.mapview);
        this.mapController = this.mapView.getController();
        this.marker = getResources().getDrawable(R.drawable.ico_marker);
        this.location_marker = getResources().getDrawable(R.drawable.ico_location_marker);
        this.overlayManager = new OverlayManager(getApplication(), this.mapView);
        this.houseOverlay = this.overlayManager.createOverlay("houses", this.marker);
        this.myPositionOverlay = this.overlayManager.createOverlay("myposition", this.location_marker);
        this.houseBlock = new HouseBlockOverlay(this.context, this.overlayManager, null, this.houseOverlay, this.mapController, this.mApplication, this.mapView, null);
        this.houseOverlay.setOnOverlayGestureListener(new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
            public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
                return false;
            }

            public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (point == null) {
                    return true;
                }
                NewHouseSearchResultActivity.this.moveMapToPoint(point);
                return true;
            }

            public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
            }

            public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item != null) {
                    NewHouseSearchResultActivity.this.clickTap(item);
                }
            }

            public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
                return false;
            }

            public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item == null) {
                    return false;
                }
                NewHouseSearchResultActivity.this.clickTap(item);
                return false;
            }
        });
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                NewHouseSearchResultActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                NewHouseSearchResultActivity.this.getMoreData();
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                House house = (House) NewHouseSearchResultActivity.this.adapter.getItem(position);
                Intent intent = new Intent(NewHouseSearchResultActivity.this, NewHouseDetailActivity.class);
                intent.putExtra("h_id", house.getH_id());
                intent.putExtra("channel", house.getH_channel());
                NewHouseSearchResultActivity.this.startActivity(intent);
            }
        });
        this.adapter = new NewHouseAdapter(this);
        this.listView.setAdapter(this.adapter);
    }

    /* access modifiers changed from: private */
    public void clickTap(ManagedOverlayItem item) {
        ManagedOverlayItem last = this.houseBlock.getLastItem();
        item.setTag(2);
        this.houseBlock.setHouseMarker(item, this.maptype);
        if (last != null && !last.getSnippet().equals(item.getSnippet())) {
            last.setTag(1);
            this.houseBlock.setHouseMarker(last, this.maptype);
        }
        this.houseBlock.setHouseOverlayTap(item, this.maptype, 0, 0, 0);
        this.houseBlock.setLastItem(item);
        moveMapToPoint(item.getPoint());
    }

    /* access modifiers changed from: private */
    public void initMap() {
        this.houseOverlay.getOverlayItems().clear();
        this.houseOverlay.removeAll();
        this.overlayManager.populate();
        if (this.result_houseList == null || this.result_houseList.size() <= 0) {
            double[] cityLoc = AppArrays.getCiytLoaction(this.mApplication.getCity());
            if (cityLoc != null) {
                moveMapToPoint(BaiduMapUtil.getPoint(cityLoc[0], cityLoc[1]));
                if (this.mapController != null) {
                    this.mapController.setZoom(15);
                    return;
                }
                return;
            }
            return;
        }
        new GetHouseOverLayTask(this.result_houseList).execute(new Void[0]);
        int count = this.result_houseList.size();
        List lat_list = new ArrayList();
        List lon_list = new ArrayList();
        if (this.result_houseList.size() != 1) {
            for (int i = 0; i < count; i++) {
                List<House> list = this.result_houseList;
                if (list.get(i).getH_lat() > 0.0d) {
                    lat_list.add(Double.valueOf(list.get(i).getH_lat()));
                }
                if (list.get(i).getH_long() > 0.0d) {
                    lon_list.add(Double.valueOf(list.get(i).getH_long()));
                }
            }
            if (lat_list.size() <= 0 || lon_list.size() <= 0) {
                double[] cityLoc2 = AppArrays.getCiytLoaction(this.mApplication.getCity());
                if (cityLoc2 != null) {
                    moveMapToPoint(BaiduMapUtil.getPoint(cityLoc2[0], cityLoc2[1]));
                    if (this.mapController != null) {
                        this.mapController.setZoom(15);
                        return;
                    }
                    return;
                }
                return;
            }
            double max_lat = ((Double) Collections.max(lat_list)).doubleValue();
            double min_lat = ((Double) Collections.min(lat_list)).doubleValue();
            double max_lon = ((Double) Collections.max(lon_list)).doubleValue();
            double min_lon = ((Double) Collections.min(lon_list)).doubleValue();
            moveMapToPoint(BaiduMapUtil.getPoint((max_lat + min_lat) / 2.0d, (max_lon + min_lon) / 2.0d));
            GeoPoint max_point = BaiduMapUtil.getPoint(max_lat, max_lon);
            GeoPoint min_point = BaiduMapUtil.getPoint(min_lat, min_lon);
            if (this.mapController != null) {
                this.mapController.zoomToSpan((int) Math.abs(((double) (max_point.getLatitudeE6() - min_point.getLatitudeE6())) * 1.1d), (int) Math.abs(((double) (max_point.getLongitudeE6() - min_point.getLongitudeE6())) * 1.1d));
                return;
            }
            return;
        }
        House house_item = this.result_houseList.get(0);
        if (this.mapController != null) {
            this.mapController.setZoom(15);
        }
        if (house_item.getH_lat() == 0.0d || house_item.getH_long() == 0.0d) {
            double[] cityLoc3 = AppArrays.getCiytLoaction(this.mApplication.getCity());
            moveMapToPoint(BaiduMapUtil.getPoint(cityLoc3[0], cityLoc3[1]));
            return;
        }
        moveMapToPoint(BaiduMapUtil.getPoint(house_item.getH_lat(), house_item.getH_long()));
    }

    /* access modifiers changed from: private */
    public void moveMapToPoint(GeoPoint geopoint) {
        if (getMapView() != null && this.mapView.getController() != null) {
            this.mapView.getController().animateTo(geopoint);
            this.mapView.getController().setCenter(geopoint);
        }
    }

    /* access modifiers changed from: private */
    public List<ManagedOverlayItem> getHouseOverlayItem(List<House> resource_list) throws LazyLoadException {
        List<ManagedOverlayItem> items = new LinkedList<>();
        List<House> houseList = resource_list;
        if (houseList != null) {
            try {
                if (houseList.size() > 0) {
                    for (House house : houseList) {
                        items.add(new ManagedOverlayItem(BaiduMapUtil.getPoint(house.getH_lat(), house.getH_long()), house.getH_id(), new JSONObject(house).toString()));
                    }
                }
            } catch (Exception e) {
                throw new LazyLoadException(e.getMessage());
            }
        }
        return items;
    }

    private class GetHouseOverLayTask extends AsyncTask<Void, Void, List<ManagedOverlayItem>> {
        List<House> resource;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<ManagedOverlayItem>) ((List) obj));
        }

        public GetHouseOverLayTask(List<House> resource2) {
            this.resource = resource2;
        }

        /* access modifiers changed from: protected */
        public List<ManagedOverlayItem> doInBackground(Void... params) {
            try {
                return NewHouseSearchResultActivity.this.getHouseOverlayItem(this.resource);
            } catch (LazyLoadException e) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<ManagedOverlayItem> result) {
            if (result == null) {
                Toast.makeText(NewHouseSearchResultActivity.this, (int) R.string.text_no_result, 0).show();
                return;
            }
            NewHouseSearchResultActivity.this.houseOverlay.addAll(result);
            NewHouseSearchResultActivity.this.houseBlock.setMarker(NewHouseSearchResultActivity.this.maptype, 2);
            NewHouseSearchResultActivity.this.overlayManager.populate();
        }
    }

    /* access modifiers changed from: private */
    public void setMyLocation() {
        GeoPoint geopoint = BaiduMapUtil.getPoint(this.mApplication.getLocation().getLatitude(), this.mApplication.getLocation().getLongitude());
        this.myPositionOverlay.removeAll();
        this.myPositionOverlay.add(new ManagedOverlayItem(geopoint, "myposition", null));
        noLocationMap();
        this.overlayManager.populate();
    }

    private void noLocationMap() {
        double[] location = AppArrays.getCiytLoaction(this.mApplication.getCity());
        moveMapToPoint(BaiduMapUtil.getPoint(location[0], location[1]));
    }

    /* access modifiers changed from: private */
    public void relocation() {
        View loadView;
        if (this.mapChoose) {
            loadView = this.mapLoading;
        } else {
            loadView = this.loadingLayout;
        }
        if (!LocationTask.isdoing) {
            LocationTask location = new LocationTask(this, loadView);
            location.setTag(11);
            location.execute(new Object[0]);
            location.setLocationListener(new LocationTask.LocationListener() {
                public void onFinish(Location location, boolean sameCity) {
                    NewHouseSearchResultActivity.this.nearby_refresh_layout.setVisibility(0);
                    NewHouseSearchResultActivity.this.mLocation = location;
                    if (NewHouseSearchResultActivity.this.mLocation == null || NewHouseSearchResultActivity.this.mLocation.getExtras() == null || NewHouseSearchResultActivity.this.mLocation.getExtras().get("addr") == null) {
                        NewHouseSearchResultActivity.this.location_addr.setText((int) R.string.text_changecity_location_failure);
                    } else {
                        TextUtil.setNullText(NewHouseSearchResultActivity.this.mLocation.getExtras().get("addr").toString(), NewHouseSearchResultActivity.this.location_addr);
                        NewHouseSearchResultActivity.this.adapter.setLocation(location);
                        NewHouseSearchResultActivity.this.adapter.setFrom_nearby(NewHouseSearchResultActivity.this.from_nearby);
                    }
                    if (sameCity) {
                        NewHouseSearchResultActivity.this.refreshData();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.mLocation = (Location) this.bundle.getParcelable("locaion");
        this.radiusvalue = this.bundle.getInt("radiusvalue");
        this.districtvalue = this.bundle.getString("districtvalue");
        this.channelvalue = this.bundle.getString("channelvalue");
        this.pricevalue = this.bundle.getString("pricevalue");
        this.text_subway = this.bundle.getString("text_subway");
        this.blockvalue = this.bundle.getString("blockvalue");
        this.tag_id = this.bundle.getString("tag_id");
        getConfigInfo();
        this.from_nearby = getIntent().getBooleanExtra(INTENT_FROM_NEARBY, false);
        if (this.from_nearby) {
            this.radiusvalue = 2500;
            relocation();
            this.text_region.setText("2.5km");
            this.text_region.setTag("0:2.5km");
            this.nearby_refresh_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    NewHouseSearchResultActivity.this.click_nearby_refresh = true;
                    if (!DeviceUtil.isOpenLoaction(NewHouseSearchResultActivity.this)) {
                        AppMethods.showLocationSets(NewHouseSearchResultActivity.this);
                    } else {
                        NewHouseSearchResultActivity.this.relocation();
                    }
                }
            });
            return;
        }
        setCityRegionShow();
        this.nearby_refresh_layout.setVisibility(8);
        this.isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (this.isVirtual) {
            this.sort_layout.setVisibility(8);
        } else {
            this.sort_layout.setVisibility(0);
        }
        refreshData();
    }

    private void setCityRegionShow() {
        if (this.mApplication.getCityName().equals("西安")) {
            this.text_region.setText((int) R.string.btn_xian_region_text);
        } else {
            this.text_region.setText((int) R.string.btn_region_text);
        }
    }

    private void addNormalSearchListener(View layout, TextView showDataView, ArrayList dataList, int choseType) {
        final View view = layout;
        final TextView textView = showDataView;
        final ArrayList arrayList = dataList;
        final int i = choseType;
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchConditionPopView.refreshViews(NewHouseSearchResultActivity.this.views, NewHouseSearchResultActivity.this.textViews, view, textView, NewHouseSearchResultActivity.this.context);
                NewHouseSearchResultActivity.this.searchPop.setShowGroup(false);
                NewHouseSearchResultActivity.this.searchPop.setGradeData(arrayList, textView, SearchConditionPopView.getTagData(textView.getTag()), i);
            }
        });
    }

    /* access modifiers changed from: private */
    public void initSearchItem() {
        this.regionList = AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get(HouseBaseInfo.DISTRICT), false);
        this.subwayMap = this.searchConfig.getMetro();
        this.priceList = AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get("price"), false);
        this.channelList = AppMethods.mapKeyTolistSubWay(this.searchConfig.getConfig().get("channel"), false);
        if (!(this.mLocation == null || this.radiusvalue == 0 || TextUtils.isEmpty(this.text_subway))) {
            this.text_region.setText(this.text_subway);
            this.text_region.setTag("2:" + this.text_subway);
        }
        if (this.districtvalue != null && !this.districtvalue.equals("0")) {
            String regionStr = AppMethods.getMapKey(this.searchConfig.getConfig().get(HouseBaseInfo.DISTRICT), this.districtvalue);
            this.text_region.setText(regionStr);
            this.text_region.setTag("1:" + regionStr);
        }
        if (this.channelvalue != null && !this.channelvalue.equals("0")) {
            String typeStr = AppMethods.getMapKey(this.searchConfig.getConfig().get("channel"), this.channelvalue);
            this.text_type.setText(typeStr);
            this.text_type.setTag("5:" + typeStr);
        }
        if (this.pricevalue != null && !this.pricevalue.equals("0")) {
            String priceStr = AppMethods.getMapKey(this.searchConfig.getConfig().get("price"), this.pricevalue);
            this.text_price.setText(priceStr);
            this.text_price.setTag("4:" + priceStr);
        }
        if (this.from_nearby) {
            addNormalSearchListener(this.region_layout, this.text_region, this.radiousList, 10);
        } else if (this.subwayMap == null || this.subwayMap.isEmpty()) {
            addNormalSearchListener(this.region_layout, this.text_region, this.regionList, 1);
        } else {
            this.region_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SearchConditionPopView.refreshViews(NewHouseSearchResultActivity.this.views, NewHouseSearchResultActivity.this.textViews, NewHouseSearchResultActivity.this.region_layout, NewHouseSearchResultActivity.this.text_region, NewHouseSearchResultActivity.this.context);
                    NewHouseSearchResultActivity.this.searchPop.setShowDataView(NewHouseSearchResultActivity.this.text_region);
                    NewHouseSearchResultActivity.this.searchPop.setShowGroup(true);
                    NewHouseSearchResultActivity.this.searchPop.setRegionOrSubway(NewHouseSearchResultActivity.this.text_region);
                    if (NewHouseSearchResultActivity.this.checkedRegion) {
                        NewHouseSearchResultActivity.this.searchPop.setGradeData(NewHouseSearchResultActivity.this.regionList, NewHouseSearchResultActivity.this.text_region, SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag()), 1);
                    } else {
                        NewHouseSearchResultActivity.this.searchPop.setGradeData(NewHouseSearchResultActivity.this.subwayMap, NewHouseSearchResultActivity.this.text_region, SearchConditionPopView.getTagData(NewHouseSearchResultActivity.this.text_region.getTag()), 2);
                    }
                    NewHouseSearchResultActivity.this.condition_group.setVisibility(0);
                }
            });
        }
        addNormalSearchListener(this.price_layout, this.text_price, this.priceList, 4);
        addNormalSearchListener(this.type_layout, this.text_type, this.channelList, 5);
        this.text_sort.setTag("0:" + this.orders[0]);
        addNormalSearchListener(this.sort_layout, this.text_sort, this.sortList, 0);
    }

    public MapView getMapView() {
        return this.mapView;
    }

    public void getConfigInfo() {
        GetConfigTask getConfigTask = new GetConfigTask(this, R.string.loading);
        getConfigTask.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                NewHouseSearchResultActivity.this.searchConfig = info;
                if (NewHouseSearchResultActivity.this.searchConfig != null) {
                    NewHouseSearchResultActivity.this.initSearchItem();
                }
            }

            public void OnError(HouseBaseInfo info) {
                Toast.makeText(NewHouseSearchResultActivity.this, (int) R.string.text_city_config_error, 1).show();
            }
        });
        getConfigTask.execute(new Object[0]);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        updataFinish();
        return false;
    }
}
