package com.house365.newhouse.ui.newhome;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.House;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAdapter;
import com.house365.newhouse.ui.tools.LoanCalSelProActivity;

public class SalerOrRecommListActivity extends BaseCommonActivity {
    private String h_dist;
    private String h_id;
    private HeadNavigateView head_view;
    private PullToRefreshListView listView;
    /* access modifiers changed from: private */
    public NewHouseAdapter recommHouseAdapter;
    private RefreshInfo refreshInfo = new RefreshInfo();

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.saler_or_recomm_list);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.refreshInfo.setAvgpage(10);
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SalerOrRecommListActivity.this.finish();
            }
        });
        this.listView = (PullToRefreshListView) findViewById(R.id.genal_list);
        this.recommHouseAdapter = new NewHouseAdapter(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.h_id = getIntent().getStringExtra("h_id");
        this.h_dist = getIntent().getStringExtra("h_dist");
        this.head_view.setTvTitleText(getIntent().getStringExtra(LoanCalSelProActivity.INTENT_TITLE));
        this.recommHouseAdapter.clear();
        this.listView.setAdapter(this.recommHouseAdapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(SalerOrRecommListActivity.this, NewHouseDetailActivity.class);
                intent.putExtra("h_id", ((House) SalerOrRecommListActivity.this.recommHouseAdapter.getItem(position)).getH_id());
                SalerOrRecommListActivity.this.startActivity(intent);
            }
        });
        this.listView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onHeaderRefresh() {
                SalerOrRecommListActivity.this.refreshData();
            }

            public void onFooterRefresh() {
                SalerOrRecommListActivity.this.getMoreData();
            }
        });
    }

    public void setData() {
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.refreshInfo.refresh = true;
        setData();
    }

    /* access modifiers changed from: private */
    public void getMoreData() {
        this.refreshInfo.refresh = false;
        setData();
    }
}
