package com.house365.newhouse.ui.block;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;

public class Block_project_intructActivity extends BaseCommonActivity {
    private HeadNavigateView head_view;
    TextView house_title;
    String project_intruct;
    private String title_name;
    TextView txt_project_intruct;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.secondsell_detail_houseremarklayout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Block_project_intructActivity.this.finish();
            }
        });
        this.txt_project_intruct = (TextView) findViewById(R.id.house_remark);
        this.house_title = (TextView) findViewById(R.id.house_title);
        this.title_name = getIntent().getStringExtra("title_name");
        this.project_intruct = getIntent().getStringExtra("txt_project_intruct");
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.txt_project_intruct.setText(this.project_intruct);
        this.house_title.setText((int) R.string.text_field_project_intro);
        this.head_view.setTvTitleText(this.title_name);
    }
}
