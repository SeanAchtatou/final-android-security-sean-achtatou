package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.house365.core.adapter.BaseCacheListPagerAdapter;
import com.house365.newhouse.R;

public class HousePicAdapter extends BaseCacheListPagerAdapter<String> {
    public HousePicAdapter(Context context) {
        super(context);
    }

    public View getAdapterView(PagerAdapter adapter, View container, int position) {
        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(-1, -1);
        ImageView iv = new ImageView(this.context);
        iv.setLayoutParams(mParams);
        setCacheImage(iv, (String) getItem(position), R.drawable.img_default_big, 1);
        iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        return iv;
    }
}
