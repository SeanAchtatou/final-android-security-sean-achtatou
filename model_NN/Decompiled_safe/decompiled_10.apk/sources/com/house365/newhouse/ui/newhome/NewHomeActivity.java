package com.house365.newhouse.ui.newhome;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.anim.AnimBean;
import com.house365.core.image.ImageLoadedCallback;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.panel.Panel;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import com.house365.core.view.viewpager.PageIndicator;
import com.house365.core.view.viewpager.ex.ViewPagerCustomDuration;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.task.NewHouseSearchTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.tool.IntentRedirect;
import com.house365.newhouse.ui.common.adapter.AdAdapter;
import com.house365.newhouse.ui.common.task.GetAdTask;
import com.house365.newhouse.ui.mapsearch.MapSearchHomeActivity;
import com.house365.newhouse.ui.newhome.adapter.NewHouseAdapter;
import com.house365.newhouse.ui.search.KeyWordSearchActivity;
import com.house365.newhouse.ui.search.NewSearchActivity;
import com.house365.newhouse.ui.search.SearchConditionPopView;
import org.apache.commons.lang.SystemUtils;

public class NewHomeActivity extends BaseCommonActivity implements View.OnClickListener, Panel.OnPanelListener, PullToRefreshBase.OnRefreshListener, AdapterView.OnItemClickListener {
    public static final String INTENT_ASSIGN_SHOW = "assignShow";
    private static final int MSG_FULLSCREEN_AD_CLOSE = 2;
    private static final int MSG_FULLSCREEN_AD_OPEN = 1;
    private static final int MSG_FULLSCREEN_HANDLER_CLOSE = 4;
    private static final int MSG_FULLSCREEN_HANDLER_OPEN = 3;
    /* access modifiers changed from: private */
    public AdAdapter adAdapter;
    private RelativeLayout adContainer;
    private NewHouseAdapter adapter;
    private TranslateAnimation animation;
    private LinearLayout animationLayout;
    private RelativeLayout btn_map;
    private RelativeLayout btn_nearby;
    private RelativeLayout btn_region;
    protected boolean click_nearby;
    private ImageButton closeAd;
    private TextView common_bar_sec_text;
    private IntentFilter filter = new IntentFilter();
    /* access modifiers changed from: private */
    public Handler fullScreenAdHanlder = new Handler() {
        public void handleMessage(Message msg) {
            if (!ActivityUtil.isTopActivity(NewHomeActivity.this)) {
                NewHomeActivity.this.fullScreenAdHanlder.removeMessages(1);
                NewHomeActivity.this.fullScreenAdHanlder.removeMessages(2);
                NewHomeActivity.this.fullScreenAdHanlder.removeMessages(3);
                NewHomeActivity.this.fullScreenAdHanlder.removeMessages(4);
                return;
            }
            switch (msg.what) {
                case 1:
                    NewHomeActivity.this.panel_ad.setOpen(true, true);
                    return;
                case 2:
                    NewHomeActivity.this.panel_ad.setOpen(false, true);
                    return;
                case 3:
                default:
                    return;
            }
        }
    };
    private HeadNavigateView head_view;
    private BroadcastReceiver mChangeCity = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            NewHomeActivity.this.adAdapter.clear();
            NewHomeActivity.this.adAdapter.notifyDataSetChanged();
            NewHomeActivity.this.top_switcher.setVisibility(4);
            NewHomeActivity.this.panel_ad.setVisibility(4);
            NewHomeActivity.this.getConfig(0);
            NewHomeActivity.this.getAD();
            NewHomeActivity.this.refreshData();
        }
    };
    /* access modifiers changed from: private */
    public PageIndicator mIndicator;
    /* access modifiers changed from: private */
    public ViewPager mPager;
    private NetState netchangeReceiver = new NetState(this, null);
    private ImageView no_ad_img;
    private View nodata_layout;
    /* access modifiers changed from: private */
    public ImageView panelContent;
    /* access modifiers changed from: private */
    public Panel panel_ad;
    private PullToRefreshListView pullToRefresh;
    private RefreshInfo refreshInfo = new RefreshInfo();
    protected HouseBaseInfo secondHouseInfo;
    private boolean toKeySearch;
    /* access modifiers changed from: private */
    public View top_switcher;
    private ImageView top_switcher_ico;
    private View viewpager_layout;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.newhousehome);
        registerReceiver(this.mChangeCity, new IntentFilter(ActionCode.INTENT_ACTION_CHANGE_CITY));
        setNetWorkChange();
    }

    private void setNetWorkChange() {
        this.filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.netchangeReceiver, this.filter);
        this.netchangeReceiver.onReceive(this, null);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.common_bar_sec_text = (TextView) findViewById(R.id.common_bar_sec_text);
        if (AppMethods.isVirtual(((NewHouseApplication) this.mApplication).getCity())) {
            this.common_bar_sec_text.setText((int) R.string.text_house_recommend_title);
        } else {
            this.common_bar_sec_text.setText((int) R.string.text_newhouse_coupon);
        }
        this.panel_ad = (Panel) findViewById(R.id.panel_ad);
        this.panelContent = (ImageView) this.panel_ad.getContent();
        this.top_switcher = findViewById(R.id.top_switcher);
        this.top_switcher_ico = (ImageView) findViewById(R.id.top_switcher_ico);
        this.viewpager_layout = findViewById(R.id.viewpager_layout);
        this.mIndicator = (PageIndicator) findViewById(R.id.indicator);
        this.no_ad_img = (ImageView) findViewById(R.id.no_ad_img);
        this.mPager = (ViewPager) findViewById(R.id.pager);
        this.closeAd = (ImageButton) findViewById(R.id.house_ad_close);
        this.adContainer = (RelativeLayout) findViewById(R.id.house_ad_container);
        this.btn_nearby = (RelativeLayout) findViewById(R.id.newhousehome_nearby);
        this.btn_region = (RelativeLayout) findViewById(R.id.newhousehome_search_region);
        this.btn_map = (RelativeLayout) findViewById(R.id.newhousehome_map_search);
        this.pullToRefresh = (PullToRefreshListView) findViewById(R.id.newhousehome_list);
        this.adapter = new NewHouseAdapter(this);
        this.pullToRefresh.setAdapter(this.adapter);
        this.nodata_layout = findViewById(R.id.nodata_layout);
        this.animationLayout = (LinearLayout) findViewById(R.id.newhousehome_movecontainer);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.head_view.getBtn_left().setOnClickListener(this);
        this.head_view.getBtn_right().setVisibility(0);
        this.head_view.getBtn_right().setOnClickListener(this);
        this.pullToRefresh.setOnRefreshListener(this);
        this.pullToRefresh.setOnItemClickListener(this);
        this.top_switcher.setOnClickListener(this);
        this.panel_ad.setOnPanelListener(this);
        this.btn_nearby.setOnClickListener(this);
        this.closeAd.setOnClickListener(this);
        this.head_view.setTvTitleText((int) R.string.text_newhome_title);
        refreshData();
        this.panel_ad.setInterpolator(new AccelerateDecelerateInterpolator());
        this.viewpager_layout.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.no_ad_img.getLayoutParams().height = (int) ((((double) this.mApplication.getScreenWidth()) / 640.0d) * 240.0d);
        this.adAdapter = new AdAdapter(this, 2);
        this.mPager.setAdapter(this.adAdapter);
        ((ViewPagerCustomDuration) this.mPager).setConsumeTouchEvent(true);
        ((ViewPagerCustomDuration) this.mPager).setScrollDuration(1000);
        this.mIndicator.setViewPager(this.mPager);
        getAD();
        getConfig(R.string.loading);
        this.animation = new TranslateAnimation(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, (float) this.viewpager_layout.getLayoutParams().height, SystemUtils.JAVA_VERSION_FLOAT);
        this.animation.setDuration(400);
    }

    /* access modifiers changed from: private */
    public void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info == null) {
                    return;
                }
                if (info.getJsonStr().equals(NewHomeActivity.this.getResources().getString(R.string.text_no_network))) {
                    NewHomeActivity.this.setViewEnable(R.string.text_no_network);
                    return;
                }
                NewHomeActivity.this.secondHouseInfo = info;
                NewHomeActivity.this.setNormalClick();
            }

            public void OnError(HouseBaseInfo info) {
                NewHomeActivity.this.setViewEnable(R.string.text_city_config_error);
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setNormalClick() {
        this.btn_nearby.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(NewHomeActivity.this, NewHouseSearchResultActivity.class);
                intent.putExtra("from_nearby", true);
                NewHomeActivity.this.startActivity(intent);
            }
        });
        this.btn_region.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(NewHomeActivity.this, NewSearchActivity.class);
                intent.putExtra("litterHome", true);
                NewHomeActivity.this.startActivity(intent);
            }
        });
        this.btn_map.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewHomeActivity.this, MapSearchHomeActivity.class);
                intent.putExtra(MapSearchHomeActivity.INTENT_MAP_TYPE, 11);
                NewHomeActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void setViewEnable(int msg) {
        AppMethods.setViewToastListener(this, this.btn_region, msg);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.toKeySearch = false;
        if (this.secondHouseInfo == null) {
            getConfig(0);
        }
    }

    /* access modifiers changed from: private */
    public void getAD() {
        this.viewpager_layout.setVisibility(8);
        this.no_ad_img.setVisibility(0);
        GetAdTask task = new GetAdTask(this, 2, this.adAdapter, this.viewpager_layout, this.no_ad_img);
        task.setCallBack(new GetAdTask.CallBack() {
            public void onSuccess(Ad ad) {
                ((ViewPagerCustomDuration) NewHomeActivity.this.mPager).stopAutoFlowTimer();
                ((ViewPagerCustomDuration) NewHomeActivity.this.mPager).startAutoFlowTimer();
                NewHomeActivity.this.mIndicator.notifyDataSetChanged();
            }

            public void onFail() {
                NewHomeActivity.this.closeAd();
            }
        });
        task.execute(new Object[0]);
        new GetAdTask(this, 1, new GetAdTask.CallBack() {
            public void onSuccess(final Ad ad) {
                NewHomeActivity.this.setImage(NewHomeActivity.this.panelContent, ad.getA_src(), 1, new ImageLoadedCallback() {
                    public void imageLoadedSuccess() {
                        NewHomeActivity.this.top_switcher.setVisibility(0);
                        NewHomeActivity.this.panel_ad.setVisibility(0);
                        NewHomeActivity.this.fullScreenAdHanlder.sendEmptyMessage(1);
                        NewHomeActivity.this.fullScreenAdHanlder.sendEmptyMessageDelayed(2, 5000);
                    }

                    public void imageLoadedFail() {
                    }
                });
                NewHomeActivity.this.panelContent.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (ad != null) {
                            HouseAnalyse.onAD(NewHomeActivity.this, "新房全屏", App.NEW, ad.getA_id());
                            IntentRedirect.adLink(1, ad, NewHomeActivity.this);
                        }
                    }
                });
            }

            public void onFail() {
            }
        }).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mChangeCity);
        unregisterReceiver(this.netchangeReceiver);
    }

    private class NetState extends BroadcastReceiver {
        private NetState() {
        }

        /* synthetic */ NetState(NewHomeActivity newHomeActivity, NetState netState) {
            this();
        }

        public void onReceive(Context arg0, Intent arg1) {
            ConnectivityManager manager = (ConnectivityManager) arg0.getSystemService("connectivity");
            NetworkInfo gprs = manager.getNetworkInfo(0);
            NetworkInfo wifi = manager.getNetworkInfo(1);
            if (gprs.isConnected() || wifi.isConnected()) {
                NewHomeActivity.this.getConfig(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void refreshData() {
        this.refreshInfo.refresh = true;
        new NewHouseSearchTask(this, NewHouseSearchTask.TASK_HOUSE_YH, this.pullToRefresh, this.refreshInfo, this.adapter, this.nodata_layout).execute(new Object[0]);
    }

    private void getMoreData() {
        this.refreshInfo.refresh = false;
        new NewHouseSearchTask(this, NewHouseSearchTask.TASK_HOUSE_YH, this.pullToRefresh, this.refreshInfo, this.adapter, this.nodata_layout).execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.adapter != null) {
            this.adapter.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        finish();
        return false;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                finish();
                return;
            case R.id.btn_right:
                this.toKeySearch = true;
                Intent intent = new Intent(this, KeyWordSearchActivity.class);
                intent.putExtra(SearchConditionPopView.INTENT_SEARCH_TYPE, ActionCode.NEW_SEARCH);
                startActivity(intent);
                return;
            case R.id.house_ad_close:
                closeAd();
                return;
            case R.id.newhousehome_nearby:
            default:
                return;
            case R.id.top_switcher:
                if (this.panel_ad.isOpen()) {
                    this.fullScreenAdHanlder.sendEmptyMessage(2);
                    return;
                } else {
                    this.fullScreenAdHanlder.sendEmptyMessage(1);
                    return;
                }
        }
    }

    /* access modifiers changed from: private */
    public void closeAd() {
        this.adContainer.setVisibility(8);
        this.animationLayout.startAnimation(this.animation);
    }

    public AnimBean getStartAnim() {
        if (this.toKeySearch) {
            return null;
        }
        return super.getStartAnim();
    }

    public void onPanelOpened(Panel panel) {
        this.top_switcher_ico.setBackgroundResource(R.drawable.top_switcher_up);
    }

    public void onPanelClosed(Panel panel) {
        this.top_switcher_ico.setBackgroundResource(R.drawable.top_switcher_down);
        this.fullScreenAdHanlder.removeMessages(2);
    }

    public void onHeaderRefresh() {
        refreshData();
    }

    public void onFooterRefresh() {
        getMoreData();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        House house = (House) this.adapter.getItem(position);
        Intent intent = new Intent(this, NewHouseDetailActivity.class);
        intent.putExtra("h_id", house.getH_id());
        intent.putExtra("channel", house.getH_channel());
        startActivity(intent);
    }
}
