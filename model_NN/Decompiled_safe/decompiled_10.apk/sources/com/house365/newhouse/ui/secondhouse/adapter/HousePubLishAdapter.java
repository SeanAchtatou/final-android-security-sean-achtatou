package com.house365.newhouse.ui.secondhouse.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.newhouse.R;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.task.DeletePublishTask;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class HousePubLishAdapter extends BaseCacheListAdapter<PublishHouse> {
    /* access modifiers changed from: private */
    public View btn_edit;
    ListView listView;
    /* access modifiers changed from: private */
    public boolean longclick;
    /* access modifiers changed from: private */
    public HashMap<String, List<PublishHouse>> mapData;
    /* access modifiers changed from: private */
    public View nodata_layout;
    /* access modifiers changed from: private */
    public boolean offer;
    /* access modifiers changed from: private */
    public int publicType;
    /* access modifiers changed from: private */
    public TextView tv_nodata;
    private int type;

    public HousePubLishAdapter(Context context, int publicType2) {
        super(context);
        this.publicType = publicType2;
    }

    public int getPublicType() {
        return this.publicType;
    }

    public void setPublicType(int publicType2) {
        this.publicType = publicType2;
    }

    public boolean isOffer() {
        return this.offer;
    }

    public void setOffer(boolean offer2) {
        this.offer = offer2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public View getNodata_layout() {
        return this.nodata_layout;
    }

    public void setNodata_layout(View nodata_layout2) {
        this.nodata_layout = nodata_layout2;
    }

    public ListView getListView() {
        return this.listView;
    }

    public void setListView(ListView listView2) {
        this.listView = listView2;
    }

    public TextView getTv_nodata() {
        return this.tv_nodata;
    }

    public void setTv_nodata(TextView tv_nodata2) {
        this.tv_nodata = tv_nodata2;
    }

    public boolean isLongclick() {
        return this.longclick;
    }

    public void setLongclick(boolean longclick2) {
        this.longclick = longclick2;
    }

    public HashMap<String, List<PublishHouse>> getMapData() {
        return this.mapData;
    }

    public void setMapData(HashMap<String, List<PublishHouse>> mapData2) {
        this.mapData = mapData2;
    }

    public View getBtn_edit() {
        return this.btn_edit;
    }

    public void setBtn_edit(View btn_edit2) {
        this.btn_edit = btn_edit2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate((int) R.layout.item_list_publish, (ViewGroup) null);
            holder = new ViewHolder(this, null);
            holder.txt_title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.block_name = (TextView) convertView.findViewById(R.id.block_name);
            holder.txt_time = (TextView) convertView.findViewById(R.id.txt_time);
            holder.house_sell_room = (TextView) convertView.findViewById(R.id.house_sell_room);
            holder.house_rent_room = (TextView) convertView.findViewById(R.id.house_rent_room);
            holder.build_area = (TextView) convertView.findViewById(R.id.build_area);
            holder.txt_rentprice = (TextView) convertView.findViewById(R.id.txt_rentprice);
            holder.txt_renttype = (TextView) convertView.findViewById(R.id.renttype);
            holder.txt_total_price = (TextView) convertView.findViewById(R.id.text_total_price);
            holder.delete_img = (ImageView) convertView.findViewById(R.id.delete_img);
            holder.sell_layout = convertView.findViewById(R.id.sell_layout);
            holder.rent_layout = convertView.findViewById(R.id.rent_layout);
            if (this.type == 1) {
                holder.delete_img.setVisibility(0);
            } else {
                holder.delete_img.setVisibility(8);
            }
            holder.delete_img.setTag(holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        PublishHouse publishHouse = (PublishHouse) getItem(position);
        holder.txt_title.setText(publishHouse.getTitle());
        holder.txt_time.setText(publishHouse.getCreateData());
        if (this.offer) {
            String buildareaStr = publishHouse.getBuildarea();
            if (buildareaStr.indexOf(".") != -1) {
                holder.build_area.setText(String.valueOf(new BigDecimal((double) Float.parseFloat(buildareaStr)).setScale(2, 4).floatValue()) + this.context.getResources().getString(R.string.text_sell_area_unit));
            } else {
                holder.build_area.setText(String.valueOf(buildareaStr) + this.context.getResources().getString(R.string.text_sell_area_unit));
            }
            holder.house_rent_room.setText(publishHouse.getRoom());
            holder.block_name.setText(publishHouse.getBlockname());
            holder.build_area.setVisibility(0);
            holder.house_rent_room.setVisibility(0);
            holder.block_name.setVisibility(0);
        } else {
            holder.build_area.setText(StringUtils.EMPTY);
            holder.house_rent_room.setText(StringUtils.EMPTY);
            holder.build_area.setVisibility(4);
            holder.house_rent_room.setVisibility(4);
            holder.block_name.setVisibility(8);
        }
        if (this.publicType == 4) {
            holder.sell_layout.setVisibility(0);
            holder.rent_layout.setVisibility(8);
            holder.house_sell_room.setText(publishHouse.getRoom());
            if (this.offer) {
                String priceStr = publishHouse.getPrice();
                if (priceStr.indexOf(".") != -1) {
                    holder.txt_total_price.setText(String.valueOf(new BigDecimal((double) Float.parseFloat(priceStr)).setScale(2, 4).floatValue()) + this.context.getResources().getString(R.string.text_pub_adapter_price_unit));
                } else {
                    holder.txt_total_price.setText(String.valueOf(priceStr) + this.context.getResources().getString(R.string.text_pub_adapter_price_unit));
                }
            } else {
                holder.txt_total_price.setText(publishHouse.getPrice());
            }
        } else {
            holder.sell_layout.setVisibility(8);
            holder.rent_layout.setVisibility(0);
            holder.txt_renttype.setText(publishHouse.getRenttype());
            holder.txt_rentprice.setText(publishHouse.getPrice());
            if (this.offer) {
                String priceStr2 = publishHouse.getPrice();
                if (priceStr2.indexOf(".") != -1) {
                    holder.txt_rentprice.setText(String.valueOf(new BigDecimal((double) Float.parseFloat(priceStr2)).setScale(2, 4).floatValue()) + publishHouse.getPriceunit());
                } else {
                    holder.txt_rentprice.setText(String.valueOf(priceStr2) + publishHouse.getPriceunit());
                }
            } else {
                holder.txt_rentprice.setText(publishHouse.getPrice());
            }
        }
        final int pos = position;
        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new DeletePublishTask(HousePubLishAdapter.this.context, HousePubLishAdapter.this.mapData, HousePubLishAdapter.this, HousePubLishAdapter.this.nodata_layout, HousePubLishAdapter.this.btn_edit, HousePubLishAdapter.this.tv_nodata, HousePubLishAdapter.this.publicType, pos, HousePubLishAdapter.this.listView, HousePubLishAdapter.this.offer).execute(new Object[0]);
                ViewHolder btnholder = (ViewHolder) v.getTag();
                if (HousePubLishAdapter.this.longclick) {
                    btnholder.delete_img.setVisibility(8);
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {
        TextView block_name;
        TextView build_area;
        ImageView delete_img;
        TextView house_rent_room;
        TextView house_sell_room;
        View rent_layout;
        View sell_layout;
        TextView txt_rentprice;
        TextView txt_renttype;
        TextView txt_time;
        TextView txt_title;
        TextView txt_total_price;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(HousePubLishAdapter housePubLishAdapter, ViewHolder viewHolder) {
            this();
        }
    }
}
