package com.house365.newhouse.ui.privilege;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapController;
import com.baidu.mapapi.MapView;
import com.house365.core.activity.BaseBaiduMapActivity;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONObject;
import com.house365.core.task.CommonAsyncTask;
import com.house365.core.util.lbs.BaiduMapUtil;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayGestureDetector;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import com.house365.core.util.map.baidu.MarkerRenderer;
import com.house365.core.util.map.baidu.OverlayManager;
import com.house365.core.util.map.baidu.ZoomEvent;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.Line;
import com.house365.newhouse.ui.CustomProgressDialog;
import com.house365.newhouse.ui.mapsearch.HouseBlockOverlay;
import com.house365.newhouse.ui.user.UserInfoActivity;
import com.house365.newhouse.ui.user.UserLoginActivity;
import com.house365.newhouse.ui.util.TelUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class HouseLineActivity extends BaseBaiduMapActivity {
    public static final String INTENT_EVENT = "event";
    public static final String INTENT_LINE = "line";
    /* access modifiers changed from: private */
    public List<Block> blockList;
    private Button btn_call;
    /* access modifiers changed from: private */
    public Button btn_sign;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String e_type;
    /* access modifiers changed from: private */
    public Event event;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public HouseBlockOverlay houseBlock;
    /* access modifiers changed from: private */
    public List<House> houseList;
    private List lat_list = new ArrayList();
    /* access modifiers changed from: private */
    public Line line;
    /* access modifiers changed from: private */
    public ManagedOverlay lineOverlay;
    private List lon_list = new ArrayList();
    private MapController mapController;
    private MapView mapView;
    /* access modifiers changed from: private */
    public int maptype = 11;
    private Drawable marker;
    private MyCountimer mctime;
    /* access modifiers changed from: private */
    public OverlayManager overlayManager;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.group_line_map);
        this.context = this;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        findViewById(R.id.mapLoading).setVisibility(8);
        this.line = (Line) getIntent().getSerializableExtra(INTENT_LINE);
        this.event = (Event) getIntent().getSerializableExtra("event");
        this.e_type = this.event.getE_type();
        if (this.e_type.equals("event")) {
            this.maptype = 11;
        } else if (this.e_type.equals(App.Categroy.Event.SELL_EVENT)) {
            this.maptype = 12;
        }
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getTv_center().setText(this.line.getL_title());
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HouseLineActivity.this.finish();
            }
        });
        this.mapView = (MapView) findViewById(R.id.mapview);
        this.btn_call = (Button) findViewById(R.id.group_house_item_call);
        this.btn_sign = (Button) findViewById(R.id.group_house_item_sign);
        this.btn_call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HouseLineActivity.this.event != null && !TextUtils.isEmpty(HouseLineActivity.this.event.getE_tel())) {
                    TelUtil.getCallIntent(HouseLineActivity.this.event.getE_tel(), HouseLineActivity.this.context, "event");
                }
            }
        });
        this.btn_sign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HouseLineActivity.this.event == null) {
                    return;
                }
                if (TextUtils.isEmpty(((NewHouseApplication) HouseLineActivity.this.context.getApplicationContext()).getMobile())) {
                    Intent intent = new Intent(HouseLineActivity.this.context, UserLoginActivity.class);
                    intent.putExtra("c_id", HouseLineActivity.this.event.getE_id());
                    intent.putExtra("coupon_type", HouseLineActivity.this.event.getE_type());
                    intent.putExtra(UserLoginActivity.INTENT_TO_LOGIN, 1);
                    HouseLineActivity.this.context.startActivity(intent);
                    return;
                }
                Intent intent2 = new Intent(HouseLineActivity.this.context, UserInfoActivity.class);
                intent2.putExtra("c_id", HouseLineActivity.this.event.getE_id());
                intent2.putExtra("coupon_type", HouseLineActivity.this.event.getE_type());
                intent2.putExtra(UserInfoActivity.INTENT_FROM, 2);
                HouseLineActivity.this.context.startActivity(intent2);
            }
        });
        ((Button) findViewById(R.id.group_house_item_lottery)).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.marker = getResources().getDrawable(R.drawable.bg_house_block_normal);
        this.mapController = this.mapView.getController();
        this.overlayManager = new OverlayManager(this.context, this.mapView);
        this.lineOverlay = this.overlayManager.createOverlay(INTENT_LINE, this.marker);
        this.houseBlock = new HouseBlockOverlay(this.context, this.overlayManager, null, this.lineOverlay, this.mapController, this.mApplication, this.mapView, null);
        if (this.maptype == 11) {
            new GetHouseLineTask(this.context, R.string.loading).execute(new Object[0]);
        } else if (this.maptype == 12) {
            new GetBlockLineTask(this.context, R.string.loading).execute(new Object[0]);
        }
        this.lineOverlay.setOnOverlayGestureListener(new ManagedOverlayGestureDetector.OnOverlayGestureListener() {
            public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
                return false;
            }

            public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                if (item == null) {
                    return false;
                }
                ManagedOverlayItem last = HouseLineActivity.this.houseBlock.getLastItem();
                item.setTag(2);
                HouseLineActivity.this.houseBlock.setHouseMarker(item, HouseLineActivity.this.maptype);
                if (last != null && !last.getSnippet().equals(item.getSnippet())) {
                    last.setTag(1);
                    HouseLineActivity.this.houseBlock.setHouseMarker(last, HouseLineActivity.this.maptype);
                }
                HouseLineActivity.this.houseBlock.setLineOverlayTap(item, HouseLineActivity.this.maptype);
                HouseLineActivity.this.houseBlock.setLastItem(item);
                return false;
            }

            public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
                return false;
            }

            public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
            }

            public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
            }

            public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
                return false;
            }
        });
        long intCountDownTime = (this.event.getE_endtime() * 1000) - System.currentTimeMillis();
        if (intCountDownTime > 0) {
            this.btn_sign.setEnabled(true);
            this.mctime = new MyCountimer(intCountDownTime, 1000);
            this.mctime.start();
            return;
        }
        this.btn_sign.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mctime != null) {
            this.mctime.cancel();
        }
    }

    /* access modifiers changed from: private */
    public void initNewHouseMap() {
        if (this.houseList != null && this.houseList.size() > 0) {
            int count = this.houseList.size();
            if (this.houseList.size() != 1) {
                new GetEventOverLayTask(this.houseList).execute(new Void[0]);
                for (int i = 0; i < count; i++) {
                    List<House> list = this.houseList;
                    if (list.get(i).getH_lat() > 0.0d) {
                        this.lat_list.add(Double.valueOf(list.get(i).getH_lat()));
                    }
                    if (list.get(i).getH_long() > 0.0d) {
                        this.lon_list.add(Double.valueOf(list.get(i).getH_long()));
                    }
                }
                setCenterAndZoom();
                return;
            }
            House house = this.houseList.get(0);
            if (house.getH_lat() > 0.0d && house.getH_long() > 0.0d) {
                new GetEventOverLayTask(this.houseList).execute(new Void[0]);
                moveMapToPoint(BaiduMapUtil.getPoint(house.getH_lat(), house.getH_long()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void initBlockMap() {
        if (this.blockList != null && this.blockList.size() > 0) {
            int count = this.blockList.size();
            if (this.blockList.size() != 1) {
                new GetEventOverLayTask(this.blockList, 12).execute(new Void[0]);
                for (int i = 0; i < count; i++) {
                    List<Block> list = this.blockList;
                    if (list.get(i).getLat() > 0.0d) {
                        this.lat_list.add(Double.valueOf(list.get(i).getLat()));
                    }
                    if (list.get(i).getLng() > 0.0d) {
                        this.lon_list.add(Double.valueOf(list.get(i).getLng()));
                    }
                }
                setCenterAndZoom();
                return;
            }
            Block block = this.blockList.get(0);
            if (block.getLat() > 0.0d && block.getLng() > 0.0d) {
                new GetEventOverLayTask(this.blockList, 12).execute(new Void[0]);
                moveMapToPoint(BaiduMapUtil.getPoint(block.getLat(), block.getLng()));
            }
        }
    }

    private void setCenterAndZoom() {
        if (this.lat_list.size() <= 0 || this.lon_list.size() <= 0) {
            double[] cityLoc = AppArrays.getCiytLoaction(((NewHouseApplication) this.mApplication).getCity());
            if (cityLoc != null) {
                moveMapToPoint(BaiduMapUtil.getPoint(cityLoc[0], cityLoc[1]));
                if (this.mapController != null) {
                    this.mapController.setZoom(15);
                    return;
                }
                return;
            }
            return;
        }
        double max_lat = ((Double) Collections.max(this.lat_list)).doubleValue();
        double min_lat = ((Double) Collections.min(this.lat_list)).doubleValue();
        double max_lon = ((Double) Collections.max(this.lon_list)).doubleValue();
        double min_lon = ((Double) Collections.min(this.lon_list)).doubleValue();
        moveMapToPoint(BaiduMapUtil.getPoint((max_lat + min_lat) / 2.0d, (max_lon + min_lon) / 2.0d));
        GeoPoint max_point = BaiduMapUtil.getPoint(max_lat, max_lon);
        GeoPoint min_point = BaiduMapUtil.getPoint(min_lat, min_lon);
        if (this.mapController != null) {
            this.mapController.zoomToSpan((int) Math.abs(((double) (max_point.getLatitudeE6() - min_point.getLatitudeE6())) * 1.1d), (int) Math.abs(((double) (max_point.getLongitudeE6() - min_point.getLongitudeE6())) * 1.1d));
        }
    }

    private void moveMapToPoint(GeoPoint geopoint) {
        if (getMapView() != null && this.mapView.getController() != null) {
            this.mapView.getController().animateTo(geopoint);
            this.mapView.getController().setCenter(geopoint);
        }
    }

    private class GetEventOverLayTask extends AsyncTask<Void, Void, List<ManagedOverlayItem>> {
        List<Block> blocks;
        List<House> resource;
        int type;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<ManagedOverlayItem>) ((List) obj));
        }

        public GetEventOverLayTask(List<House> resource2) {
            this.resource = resource2;
        }

        public GetEventOverLayTask(List<Block> blocks2, int type2) {
            this.blocks = blocks2;
            this.type = type2;
        }

        /* access modifiers changed from: protected */
        public List<ManagedOverlayItem> doInBackground(Void... params) {
            List<ManagedOverlayItem> items = new LinkedList<>();
            if (this.resource != null && this.resource.size() > 0) {
                for (House house : this.resource) {
                    items.add(new ManagedOverlayItem(BaiduMapUtil.getPoint(house.getH_lat(), house.getH_long()), house.getH_id(), new JSONObject(house).toString()));
                }
            }
            if (!(this.type == 0 || this.blocks == null || this.blocks.size() <= 0)) {
                for (Block block : this.blocks) {
                    items.add(new ManagedOverlayItem(BaiduMapUtil.getPoint(block.getLat(), block.getLng()), block.getId(), new JSONObject(block).toString()));
                }
            }
            return items;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<ManagedOverlayItem> result) {
            if (result == null) {
                Toast.makeText(HouseLineActivity.this.context, (int) R.string.msg_load_error, 0).show();
                return;
            }
            HouseLineActivity.this.lineOverlay.addAll(result);
            HouseLineActivity.this.setMarker();
            HouseLineActivity.this.overlayManager.populate();
        }
    }

    public void setMarker() {
        this.lineOverlay.setCustomMarkerRenderer(new MarkerRenderer() {
            public Drawable render(ManagedOverlayItem item, Drawable defaultMarker, int bitState) {
                if (item.getCustomRenderedDrawable() != null) {
                    return item.getCustomRenderedDrawable();
                }
                return HouseLineActivity.this.houseBlock.setMarkerDrawble(HouseLineActivity.this.maptype, 0, 2, item);
            }
        });
    }

    public MapView getMapView() {
        return this.mapView;
    }

    class GetHouseLineTask extends CommonAsyncTask<List<House>> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<House>) ((List) obj));
        }

        public GetHouseLineTask(Context context, int resid) {
            super(context, resid);
            this.loadingresid = resid;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(List<House> result) {
            HouseLineActivity.this.houseList = result;
            HouseLineActivity.this.initNewHouseMap();
        }

        public List<House> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getHouseLineDetail(HouseLineActivity.this.line.getL_id(), HouseLineActivity.this.e_type);
        }
    }

    class GetBlockLineTask extends CommonAsyncTask<List<Block>> {
        CustomProgressDialog dialog = new CustomProgressDialog(this.context, R.style.dialog);

        public /* bridge */ /* synthetic */ void onAfterDoInBackgroup(Object obj) {
            onAfterDoInBackgroup((List<Block>) ((List) obj));
        }

        public GetBlockLineTask(Context context, int resid) {
            super(context, resid);
            this.loadingresid = resid;
            initLoadDialog(this.loadingresid);
        }

        private void initLoadDialog(int resid) {
            if ((this.context instanceof Activity) && ((Activity) this.context).isFinishing()) {
                this.loadingresid = 0;
            }
            if (resid != 0) {
                this.dialog.setResId(resid);
                setLoadingDialog(this.dialog);
            }
        }

        public void onAfterDoInBackgroup(List<Block> result) {
            HouseLineActivity.this.blockList = result;
            HouseLineActivity.this.initBlockMap();
        }

        public List<Block> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException {
            return ((HttpApi) ((NewHouseApplication) this.mApplication).getApi()).getBlockLineDetail(HouseLineActivity.this.line.getL_id(), HouseLineActivity.this.e_type);
        }
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            HouseLineActivity.this.btn_sign.setEnabled(false);
        }

        public void onTick(long millisUntilFinished) {
        }
    }
}
