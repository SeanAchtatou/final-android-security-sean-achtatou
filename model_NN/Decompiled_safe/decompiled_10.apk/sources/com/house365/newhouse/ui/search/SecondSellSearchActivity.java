package com.house365.newhouse.ui.search;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.Station;
import com.house365.newhouse.qrcode.CaptureActivity;
import com.house365.newhouse.task.GetConfigTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.search.KeyWordSearch;
import com.house365.newhouse.ui.secondsell.SecondSellResultActivity;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;

public class SecondSellSearchActivity extends BaseCommonActivity implements KeyWordSearch.DialogDimissListener {
    public static final String INTENT_FORM_TYPE_CHOOSE = "litterChoose";
    public static final String INTENT_FROM_LITTERHOME = "litterHome";
    private static final int NORMAL_SEARCH_BACK = 1;
    public static final int REFRESH_BACK = 11;
    protected HouseBaseInfo baseInfo;
    private View black_alpha_view;
    private Button btn_capture;
    private Button btn_finish;
    /* access modifiers changed from: private */
    public int buildareaValue;
    private Button cancelDialog;
    private BroadcastReceiver chosedFinished = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String chose_from = intent.getStringExtra(SearchConditionPopView.INTENT_SEARCH_TYPE);
            int chose = intent.getIntExtra(SearchConditionPopView.INTENT_FROM_CONDITION, 0);
            if (chose_from.equals(ActionCode.SELL_SEARCH)) {
                SecondSellSearchActivity.this.refreshUI(chose);
            }
        }
    };
    private Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    private boolean fromLitterHome;
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public int infofromValue;
    /* access modifiers changed from: private */
    public int infotypeValue;
    private KeyWordSearch keyWordSearch;
    private ListView key_list;
    /* access modifiers changed from: private */
    public EditText keyword_text;
    /* access modifiers changed from: private */
    public String metroValue;
    private LinearLayout nodata_layout;
    private LinearLayout popLayout;
    /* access modifiers changed from: private */
    public int priceValue;
    private View region_layout;
    private View region_total_layout;
    /* access modifiers changed from: private */
    public int roomValue;
    /* access modifiers changed from: private */
    public SearchConditionPopView searchPop;
    private View search_bar;
    private View sell_buildarea_layout;
    /* access modifiers changed from: private */
    public View sell_buildarea_total_layou;
    private View sell_rent_resource_layout;
    private View sell_rent_resource_total_layout;
    private View sell_rent_room_layout;
    /* access modifiers changed from: private */
    public View sell_rent_room_total_layout;
    private View sell_total_price_layout;
    private View sell_total_price_layout_total;
    private View senior_search_close_layout;
    /* access modifiers changed from: private */
    public View senior_search_close_total_layout;
    private View senior_search_open_layout;
    /* access modifiers changed from: private */
    public View senior_search_open_total_layout;
    /* access modifiers changed from: private */
    public int streetValue;
    private View subway_layout;
    /* access modifiers changed from: private */
    public View subway_total_layout;
    /* access modifiers changed from: private */
    public TextView text_region;
    private TextView text_sell_buildarea;
    private TextView text_sell_buildarea_title;
    private TextView text_sell_rent_resource;
    private TextView text_sell_rent_resource_title;
    private TextView text_sell_rent_room;
    private TextView text_sell_rent_room_title;
    private TextView text_sell_total_price;
    private TextView text_sell_total_price_title;
    /* access modifiers changed from: private */
    public TextView text_subway;
    private TextView text_type;
    private TextView text_type_title;
    private View type_layout;
    /* access modifiers changed from: private */
    public View type_total_layout;

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String subway;
        if (requestCode == 11 && resultCode == -1 && data != null && data.getExtras() != null) {
            Bundle refreshBundle = data.getExtras();
            int infofromIndex = refreshBundle.getInt("infofromValue");
            int priceIndex = refreshBundle.getInt("priceValue");
            int type = SearchConditionPopView.getTagFlag(refreshBundle.getString("distsubway"));
            if (type == 1) {
                String disStreet = SearchConditionPopView.getTagData(refreshBundle.getString("distsubway"));
                if (disStreet != null && !disStreet.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(disStreet);
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                }
            } else if (type == 2 && (subway = SearchConditionPopView.getTagData(refreshBundle.getString("distsubway"))) != null && !subway.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                this.text_subway.setText(subway);
            }
            if (infofromIndex != 0) {
                this.text_sell_rent_resource.setText(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM).get(infofromIndex).toString());
            } else {
                this.text_sell_rent_resource.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            }
            if (priceIndex != 0) {
                this.text_sell_total_price.setText(this.baseInfo.getSell_config().get("price").get(priceIndex).toString());
            } else {
                this.text_sell_total_price.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        this.context = this;
        View contentView = LayoutInflater.from(this.context).inflate((int) R.layout.second_sell_search_layout, (ViewGroup) null);
        setContentView(contentView);
        this.black_alpha_view = findViewById(R.id.black_alpha_view);
        this.searchPop = new SearchConditionPopView(this.context, this.black_alpha_view, this.mApplication.getScreenWidth(), this.mApplication.getScreenHeight() / 3);
        this.searchPop.setShowBottom(true);
        this.searchPop.setShowGroup(false);
        this.searchPop.setParentView(contentView);
        this.searchPop.setSearch_type(ActionCode.SELL_SEARCH);
        registerReceiver(this.chosedFinished, new IntentFilter(ActionCode.INTENT_SEARCH_CONDITION_FINISH));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.chosedFinished);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.finish();
            }
        });
        this.search_bar = findViewById(R.id.search_bar_layout);
        this.keyword_text = (EditText) findViewById(R.id.keyword_text);
        this.keyword_text.setHint((int) R.string.text_second_search_hint);
        this.btn_capture = (Button) findViewById(R.id.btn_capture);
        this.region_layout = findViewById(R.id.region_layout);
        this.sell_total_price_layout = findViewById(R.id.sell_total_price_layout);
        this.sell_rent_resource_layout = findViewById(R.id.sell_rent_resource_layout);
        this.subway_layout = findViewById(R.id.subway_layout);
        this.region_total_layout = findViewById(R.id.region_total_layout);
        this.subway_total_layout = findViewById(R.id.subway_total_layout);
        this.sell_total_price_layout_total = findViewById(R.id.sell_total_price_layout_total);
        this.text_region = (TextView) findViewById(R.id.text_region);
        this.text_subway = (TextView) findViewById(R.id.text_subway);
        this.text_sell_total_price = (TextView) findViewById(R.id.text_sell_total_price);
        this.text_sell_rent_resource = (TextView) findViewById(R.id.text_sell_rent_resource);
        this.text_sell_total_price_title = (TextView) findViewById(R.id.text_sell_total_price_title);
        this.text_sell_rent_resource_title = (TextView) findViewById(R.id.text_sell_rent_resource_title);
        this.sell_rent_room_layout = findViewById(R.id.sell_rent_room_layout);
        this.sell_buildarea_layout = findViewById(R.id.sell_buildarea_layout);
        this.type_layout = findViewById(R.id.type_layout);
        this.sell_rent_room_total_layout = findViewById(R.id.sell_rent_room_total_layout);
        this.sell_buildarea_total_layou = findViewById(R.id.sell_buildarea_total_layout);
        this.type_total_layout = findViewById(R.id.type_total_layout);
        this.sell_rent_resource_total_layout = findViewById(R.id.sell_rent_resource_total_layout);
        this.text_sell_buildarea = (TextView) findViewById(R.id.text_sell_buildarea);
        this.text_sell_rent_room = (TextView) findViewById(R.id.text_sell_rent_room);
        this.text_type = (TextView) findViewById(R.id.text_type);
        this.text_type_title = (TextView) findViewById(R.id.text_type_title);
        this.text_sell_buildarea_title = (TextView) findViewById(R.id.text_sell_buildarea_title);
        this.text_sell_rent_room_title = (TextView) findViewById(R.id.text_sell_rent_room_title);
        this.btn_finish = (Button) findViewById(R.id.btn_finish);
        this.senior_search_open_total_layout = findViewById(R.id.senior_search_open_total_layout);
        this.senior_search_close_total_layout = findViewById(R.id.senior_search_close_total_layout);
        this.senior_search_open_layout = findViewById(R.id.senior_search_open_layout);
        this.senior_search_close_layout = findViewById(R.id.senior_search_close_layout);
        setFromLitterHome();
        this.senior_search_open_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.sell_rent_room_total_layout.setVisibility(0);
                SecondSellSearchActivity.this.sell_buildarea_total_layou.setVisibility(0);
                SecondSellSearchActivity.this.type_total_layout.setVisibility(0);
                SecondSellSearchActivity.this.senior_search_open_total_layout.setVisibility(8);
                SecondSellSearchActivity.this.senior_search_close_total_layout.setVisibility(0);
            }
        });
        this.senior_search_close_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.sell_rent_room_total_layout.setVisibility(8);
                SecondSellSearchActivity.this.sell_buildarea_total_layou.setVisibility(8);
                SecondSellSearchActivity.this.type_total_layout.setVisibility(8);
                SecondSellSearchActivity.this.senior_search_open_total_layout.setVisibility(0);
                SecondSellSearchActivity.this.senior_search_close_total_layout.setVisibility(8);
            }
        });
        this.btn_capture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.startActivity(new Intent(SecondSellSearchActivity.this, CaptureActivity.class));
            }
        });
        View.OnClickListener keysearch = new View.OnClickListener() {
            public void onClick(View v) {
            }
        };
        this.search_bar.setOnClickListener(keysearch);
        this.keyword_text.setOnClickListener(keysearch);
        this.btn_finish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SecondSellSearchActivity.this, SecondSellResultActivity.class);
                Bundle bundle = new Bundle();
                if (SecondSellSearchActivity.this.baseInfo != null && !SecondSellSearchActivity.this.baseInfo.getJsonStr().equals(SecondSellSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondSellSearchActivity.this.getChooseValue();
                    bundle.putInt("infofromValue", SecondSellSearchActivity.this.infofromValue);
                    bundle.putInt("infotypeValue", SecondSellSearchActivity.this.infotypeValue);
                    bundle.putInt("priceValue", SecondSellSearchActivity.this.priceValue);
                    bundle.putInt("roomValue", SecondSellSearchActivity.this.roomValue);
                    bundle.putInt("streetValue", SecondSellSearchActivity.this.streetValue);
                    bundle.putInt("districtValue", SecondSellSearchActivity.this.districtValue);
                    bundle.putInt("buildareaValue", SecondSellSearchActivity.this.buildareaValue);
                    bundle.putString("text_subway", SecondSellSearchActivity.this.text_subway.getText().toString());
                    bundle.putString("text_region", SecondSellSearchActivity.this.text_region.getText().toString());
                    bundle.putString(HouseBaseInfo.METRO, SecondSellSearchActivity.this.metroValue);
                    bundle.putString("keywords", SecondSellSearchActivity.this.keyword_text.getText().toString());
                    intent.putExtra("priceView_from", StringUtils.EMPTY);
                }
                intent.putExtras(bundle);
                SecondSellSearchActivity.this.startActivityForResult(intent, 11);
            }
        });
        prepareKeySearch();
    }

    private void prepareKeySearch() {
        this.cancelDialog = (Button) findViewById(R.id.btn_cancel);
        this.cancelDialog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.popDismiss();
            }
        });
        this.popLayout = (LinearLayout) findViewById(R.id.key_search_container);
        this.nodata_layout = (LinearLayout) findViewById(R.id.nodata_layout);
        this.key_list = (ListView) findViewById(R.id.key_list);
        this.keyWordSearch = new KeyWordSearch(this, this.keyword_text);
        this.keyWordSearch.setSearchType(ActionCode.SELL_SEARCH);
        this.keyWordSearch.setDismissListener(this);
        this.keyWordSearch.setKey_list(this.key_list);
        this.keyWordSearch.setNodata_layout(this.nodata_layout);
        this.keyWordSearch.preparedCreate();
    }

    private void setFromLitterHome() {
        this.fromLitterHome = getIntent().getBooleanExtra("litterHome", false);
        if (this.fromLitterHome) {
            this.head_view.setVisibility(0);
            this.head_view.getTv_center().setText((int) R.string.text_second_house_title);
            return;
        }
        this.head_view.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getConfig(R.string.loading);
    }

    /* access modifiers changed from: private */
    public void setListener() {
        this.region_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.searchPop.setGradeData(SecondSellSearchActivity.this.text_region, SecondSellSearchActivity.this.text_region.getText().toString(), SecondSellSearchActivity.this.baseInfo.getDistrictStreet(), 1);
            }
        });
        this.subway_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.searchPop.setGradeData(SecondSellSearchActivity.this.baseInfo.getMetro(), SecondSellSearchActivity.this.text_subway, SecondSellSearchActivity.this.text_subway.getText().toString(), 2);
            }
        });
        addNormalSearchListener(this.sell_total_price_layout, this.text_sell_total_price, this.baseInfo.getSell_config().get("price"), 0);
        addNormalSearchListener(this.sell_rent_room_layout, this.text_sell_rent_room, this.baseInfo.getSell_config().get(HouseBaseInfo.ROOM), 0);
        addNormalSearchListener(this.sell_rent_resource_layout, this.text_sell_rent_resource, this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM), 0);
        addNormalSearchListener(this.sell_buildarea_layout, this.text_sell_buildarea, this.baseInfo.getSell_config().get("buildarea"), 0);
        addNormalSearchListener(this.type_layout, this.text_type, this.baseInfo.getSell_config().get(HouseBaseInfo.INFOTYPE), 0);
    }

    private void setMetro(String subWay) {
        if (subWay == null || subWay.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.metroValue = null;
        } else if (subWay.indexOf("+") != -1) {
            String metro = subWay.substring(0, subWay.indexOf("+"));
            this.metroValue = ((Station) this.baseInfo.getMetro().get(metro).get(subWay.substring(subWay.indexOf("+") + 1, subWay.length()))).getId();
        }
    }

    private void setDistrict(String district_Street) {
        if (district_Street == null || district_Street.equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
            this.districtValue = 0;
            this.streetValue = 0;
        } else if (district_Street.indexOf("+") != -1) {
            String district = district_Street.substring(0, district_Street.indexOf("+"));
            String street = district_Street.substring(district_Street.indexOf("+") + 1, district_Street.length());
            this.districtValue = SearchConditionPopView.getIndex(AppMethods.mapKeyTolistSubWay(this.baseInfo.getDistrictStreet(), true), district) + 1;
            this.streetValue = Integer.parseInt((String) this.baseInfo.getDistrictStreet().get(district).get(street));
        }
    }

    /* access modifiers changed from: private */
    public void getChooseValue() {
        this.infofromValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOFROM), this.text_sell_rent_resource.getText().toString());
        this.infotypeValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.INFOTYPE), this.text_type.getText().toString());
        this.roomValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get(HouseBaseInfo.ROOM), this.text_sell_rent_room.getText().toString());
        this.priceValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get("price"), this.text_sell_total_price.getText().toString());
        this.buildareaValue = SearchConditionPopView.getIndex(this.baseInfo.getSell_config().get("buildarea"), this.text_sell_buildarea.getText().toString());
        setMetro(this.text_subway.getText().toString());
        setDistrict(this.text_region.getText().toString());
    }

    private void addNormalSearchListener(View layout, final TextView showDataView, final ArrayList dataList, final int choseType) {
        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SecondSellSearchActivity.this.searchPop.setGradeData(dataList, showDataView, showDataView.getText().toString(), choseType);
            }
        });
    }

    /* access modifiers changed from: private */
    public void refreshUI(int chose) {
        switch (chose) {
            case 1:
                if (!this.text_subway.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_subway.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    return;
                }
                return;
            case 2:
                if (!this.text_region.getText().toString().equals(ActionCode.PREFERENCE_SEARCH_NOCHOSE)) {
                    this.text_region.setText(ActionCode.PREFERENCE_SEARCH_NOCHOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void getConfig(int resid) {
        GetConfigTask getConfig = new GetConfigTask(this, resid, 1);
        getConfig.setConfigOnSuccessListener(new GetConfigTask.GetConfigOnSuccessListener() {
            public void OnSuccess(HouseBaseInfo info) {
                if (info == null) {
                    return;
                }
                if (info.getJsonStr().equals(SecondSellSearchActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondSellSearchActivity.this.setViewEnable(R.string.text_no_network);
                    return;
                }
                SecondSellSearchActivity.this.baseInfo = info;
                if (AppMethods.mapKeyTolistSubWay(SecondSellSearchActivity.this.baseInfo.getMetro(), true) == null) {
                    SecondSellSearchActivity.this.subway_total_layout.setVisibility(8);
                }
                SecondSellSearchActivity.this.setListener();
            }

            public void OnError(HouseBaseInfo info) {
                SecondSellSearchActivity.this.setViewEnable(R.string.text_city_config_error);
            }
        });
        getConfig.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setViewEnable(int msg) {
        this.subway_total_layout.setVisibility(8);
        AppMethods.setViewToastListener(this, this.region_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_total_price_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_rent_resource_layout, msg);
        AppMethods.setViewToastListener(this, this.subway_layout, msg);
        AppMethods.setViewToastListener(this, this.subway_total_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_buildarea_layout, msg);
        AppMethods.setViewToastListener(this, this.type_layout, msg);
        AppMethods.setViewToastListener(this, this.sell_rent_room_layout, msg);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.baseInfo == null) {
            getConfig(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void popDismiss() {
        this.btn_capture.setVisibility(0);
        this.cancelDialog.setVisibility(8);
        this.popLayout.setVisibility(8);
    }

    public void popShow() {
        this.btn_capture.setVisibility(8);
        this.cancelDialog.setVisibility(0);
        this.popLayout.setVisibility(0);
    }
}
