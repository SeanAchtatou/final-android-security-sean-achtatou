package com.house365.newhouse.ui.newhome.adapter;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.adapter.BaseCacheListAdapter;
import com.house365.core.util.TextUtil;
import com.house365.core.util.lbs.MapUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.model.House;

public class NewHouseAdapter extends BaseCacheListAdapter<House> {
    private boolean from_nearby;
    private LayoutInflater inflater;
    private boolean isUseLoaction = false;
    private Location location;
    private String text_house_price_m_unit;
    private String text_house_price_square_unit;
    private String text_house_price_unit;
    private String text_pub_no_price;

    public boolean isUseLoaction() {
        return this.isUseLoaction;
    }

    public void setUseLoaction(boolean isUseLoaction2) {
        this.isUseLoaction = isUseLoaction2;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public boolean isFrom_nearby() {
        return this.from_nearby;
    }

    public void setFrom_nearby(boolean from_nearby2) {
        this.from_nearby = from_nearby2;
    }

    public NewHouseAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
        this.text_pub_no_price = context.getResources().getString(R.string.text_pub_no_price);
        this.text_house_price_unit = context.getResources().getString(R.string.text_house_price_unit);
        this.text_house_price_m_unit = context.getResources().getString(R.string.text_house_price_m_unit);
        this.text_house_price_square_unit = context.getResources().getString(R.string.text_house_price_square_unit);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.item_newhouse, (ViewGroup) null);
            holder = new ViewHolder();
            holder.h_name = (TextView) convertView.findViewById(R.id.h_name);
            holder.h_pic = (ImageView) convertView.findViewById(R.id.h_pic);
            holder.h_project_address = (TextView) convertView.findViewById(R.id.h_project_address);
            holder.h_near_distance = (TextView) convertView.findViewById(R.id.h_near_distance);
            holder.h_price = (TextView) convertView.findViewById(R.id.h_price);
            holder.h_sale_status = (TextView) convertView.findViewById(R.id.h_sale_state);
            holder.h_yhinfo = (TextView) convertView.findViewById(R.id.h_yhinfo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        House house = (House) getItem(position);
        if (!isFrom_nearby() || this.location == null || house.getH_lat() == 0.0d || house.getH_long() == 0.0d) {
            holder.h_near_distance.setVisibility(8);
        } else {
            holder.h_near_distance.setText(MapUtil.getDistance(this.location, house.getH_lat(), house.getH_long()));
            holder.h_near_distance.setVisibility(0);
        }
        holder.h_name.setText(house.getH_name());
        setCacheImage(holder.h_pic, house.getH_pic(), R.drawable.bg_default_img_photo, 1);
        holder.h_project_address.setText(house.getH_project_address());
        if (house.getH_price() == null || TextUtils.isEmpty(house.getH_price()) || house.getH_price().indexOf(this.text_pub_no_price) != -1) {
            holder.h_price.setText((int) R.string.text_pub_no_price);
        } else if (house.getH_price().indexOf(this.text_house_price_unit) != -1) {
            holder.h_price.setText(house.getH_price());
        } else {
            holder.h_price.setText(String.valueOf(house.getH_price().split(this.text_house_price_m_unit)[0]) + this.text_house_price_square_unit);
        }
        TextUtil.setNullText(house.getH_yhinfo(), holder.h_yhinfo);
        holder.h_sale_status.setText(house.getH_salestat_str());
        if (house.getH_salestat_str() != null) {
            String salestae = house.getH_salestat_str();
            holder.h_sale_status.setVisibility(0);
            if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_sall_out))) {
                holder.h_sale_status.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_house_sale_out));
                holder.h_sale_status.setTextAppearance(this.context, R.style.font12_gray);
            } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_new))) {
                holder.h_sale_status.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_sall_in));
                holder.h_sale_status.setTextAppearance(this.context, R.style.font12_pop_orange);
            } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_selling))) {
                holder.h_sale_status.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_sale_house));
                holder.h_sale_status.setTextAppearance(this.context, R.style.font12_pop_green);
            } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_land))) {
                holder.h_sale_status.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_real_house));
                holder.h_sale_status.setTextAppearance(this.context, R.style.font12_pop_blue);
            } else if (salestae.equals(this.context.getResources().getString(R.string.text_newhouse_last))) {
                holder.h_sale_status.setBackgroundDrawable(this.context.getResources().getDrawable(R.drawable.bg_newhouse_last));
                holder.h_sale_status.setTextAppearance(this.context, R.style.font12_pop_red);
            } else {
                holder.h_sale_status.setVisibility(8);
            }
        } else {
            holder.h_sale_status.setVisibility(8);
        }
        return convertView;
    }

    public class ViewHolder {
        TextView h_name;
        TextView h_near_distance;
        ImageView h_pic;
        TextView h_price;
        TextView h_project_address;
        TextView h_sale_status;
        TextView h_yhinfo;

        public ViewHolder() {
        }
    }
}
