package com.house365.newhouse.ui.award;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.util.store.DiskCache;
import com.house365.newhouse.api.HttpApi;
import java.io.IOException;
import java.io.InputStream;

public class TaofangImageLoader extends AsyncImageLoader {
    private BaseApplication<HttpApi> application;
    /* access modifiers changed from: private */
    public DiskCache mDiskCache = this.application.getDiskCache();

    public TaofangImageLoader(Context context) {
        super(context);
        this.application = (BaseApplication) ((Activity) context).getApplication();
    }

    public Bitmap loadBitmap(final String url, int w, int h, int scaleType) {
        Bitmap b = null;
        try {
            b = getBitMapFromStream(new AsyncImageLoader.BitMapInputStream() {
                public InputStream getInputStream() throws IOException {
                    return TaofangImageLoader.this.mDiskCache.getInputStream(url);
                }
            }, w, h, scaleType);
        } catch (IOException e) {
            if (0 == 0) {
                b = loadImageFromUrl(url, w, h, scaleType);
            }
        }
        Bitmap b2 = Bitmap.createScaledBitmap(b, w, h, true);
        b.recycle();
        return b2;
    }
}
