package com.house365.newhouse.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.DialogUtil;
import com.house365.core.util.lbs.MyLocation;
import com.house365.core.view.HeadNavigateView;
import com.house365.core.view.NoScrollListView;
import com.house365.core.view.pinned.BaseSectionListAdapter;
import com.house365.core.view.pinned.PinnedHeaderListView;
import com.house365.core.view.pinned.SectionListItem;
import com.house365.newhouse.R;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.VirtualCity;
import com.house365.newhouse.task.GetCityTask;
import com.house365.newhouse.task.UpdateUserInfoTask;
import com.house365.newhouse.tool.ActionCode;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.adapter.ExpandableCityAdapter;
import com.house365.newhouse.ui.search.adapter.NormalSearchAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CityChangeActivity extends BaseCommonActivity {
    private static final String FORMAT = "^[A-Z,a-z]";
    private static final int LOAD_ERROR = 2;
    private static final int LOAD_OK = 1;
    public static final int RESULT_SETTINGED = 1;
    public static int tab_index;
    private NormalSearchAdapter adapter;
    /* access modifiers changed from: private */
    public ExpandableCityAdapter cityAdapter;
    private VirtualCity[] citys;
    /* access modifiers changed from: private */
    public Location curLocation;
    /* access modifiers changed from: private */
    public TextView currentCity;
    /* access modifiers changed from: private */
    public RelativeLayout current_city_layout;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (!DeviceUtil.isNetConnect(CityChangeActivity.this)) {
                        CityChangeActivity.this.showToast((int) R.string.net_error);
                    }
                    if (CityChangeActivity.this.curLocation == null) {
                        CityChangeActivity.this.showToast((int) R.string.text_location_error_setting);
                    }
                    if (DeviceUtil.isNetConnect(CityChangeActivity.this) && CityChangeActivity.this.curLocation != null) {
                        CityChangeActivity.this.currentCity.setText(CityChangeActivity.this.location_cityName);
                        CityChangeActivity.this.ensureChange(CityChangeActivity.this.location_cityName);
                        CityChangeActivity.this.current_city_layout.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                String t = AppArrays.getCity(CityChangeActivity.this.currentCity.getText().toString());
                                if (!TextUtils.isEmpty(t)) {
                                    CityChangeActivity.this.changeCity(t);
                                }
                            }
                        });
                        break;
                    }
                case 2:
                    CityChangeActivity.this.currentCity.setText((int) R.string.text_changecity_location_failure);
                    CityChangeActivity.this.current_city_layout.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CityChangeActivity.this.openLocation();
                        }
                    });
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public int isFirst = 0;
    private QuickAlphabeticView letterView;
    protected String location_cityName;
    private HashMap<String, Integer> mIndex;
    private Map<String, List<VirtualCity>> mMap;
    private List<Integer> mPositions;
    private List<String> mSections;
    /* access modifiers changed from: private */
    public MyLocation mylocation;
    private NewHouseApplication newApp;
    private NoScrollListView othercity_listview;
    private RelativeLayout pinned_layout;
    private PinnedHeaderListView pinnedlistView;
    private List<SectionListItem> sectionS;
    private VirtualCity[] virtualCitys;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.city);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void loadLocation() {
        this.currentCity.setText((int) R.string.text_locationing);
        new Thread() {
            public void run() {
                try {
                    CityChangeActivity.this.mylocation = new MyLocation(CityChangeActivity.this);
                    CityChangeActivity.this.curLocation = CityChangeActivity.this.mylocation.getLocationByBaiduLocApi(true, 10000);
                    if (CityChangeActivity.this.curLocation != null) {
                        CityChangeActivity.this.mApplication.setLocation(CityChangeActivity.this.curLocation);
                        CommonResultInfo resultInfo = ((HttpApi) ((NewHouseApplication) CityChangeActivity.this.mApplication).getApi()).getCoordConvertCity(CityChangeActivity.this.curLocation.getLatitude(), CityChangeActivity.this.curLocation.getLongitude());
                        if (resultInfo == null || resultInfo.getResult() != 1 || TextUtils.isEmpty(resultInfo.getData())) {
                            CityChangeActivity.this.handler.sendEmptyMessage(2);
                            return;
                        }
                        CityChangeActivity.this.location_cityName = resultInfo.getData();
                        CityChangeActivity.this.handler.sendEmptyMessage(1);
                        return;
                    }
                    CityChangeActivity.this.handler.sendEmptyMessage(2);
                } catch (Exception e) {
                    e.printStackTrace();
                    CityChangeActivity.this.handler.sendEmptyMessage(2);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void changeCity(String city) {
        ((NewHouseApplication) this.mApplication).setCity(city);
        sendBroadcast(new Intent(ActionCode.INTENT_ACTION_CHANGE_CITY));
        new UpdateUserInfoTask(this, App.NEW).execute(new Object[0]);
        if (this.isFirst == 1) {
            startActivity(new Intent(this, MenuActivity.class));
        }
        finish();
    }

    private void initPineData() {
        this.mSections = new ArrayList();
        this.mMap = new HashMap();
        this.mPositions = new ArrayList();
        this.mIndex = new HashMap<>();
        for (int i = 0; i < this.virtualCitys.length; i++) {
            VirtualCity v_item = this.virtualCitys[i];
            if (v_item != null) {
                String firstName = v_item.getCity_py().substring(0, 1);
                if (firstName.matches(FORMAT)) {
                    if (this.mSections.contains(firstName)) {
                        this.mMap.get(firstName).add(v_item);
                    } else {
                        this.mSections.add(firstName);
                        List<VirtualCity> list = new ArrayList<>();
                        list.add(v_item);
                        this.mMap.put(firstName, list);
                    }
                } else if (this.mSections.contains("#")) {
                    this.mMap.get("#").add(v_item);
                } else {
                    this.mSections.add("#");
                    List<VirtualCity> list2 = new ArrayList<>();
                    list2.add(v_item);
                    this.mMap.put(firstName, list2);
                }
            }
        }
        Collections.sort(this.mSections);
        String hotCity = getResources().getString(R.string.text_other_city);
        this.mSections.add(0, hotCity);
        ArrayList arrayList = new ArrayList();
        VirtualCity[] allCity = this.citys;
        for (VirtualCity item : allCity) {
            if (item != null && !TextUtils.isEmpty(item.getCity_name())) {
                if (AppArrays.getLocalCityNames().contains(item.getCity_name())) {
                    arrayList.add(item);
                }
            }
        }
        this.mMap.put(hotCity, arrayList);
        int position = 0;
        for (int i2 = 0; i2 < this.mSections.size(); i2++) {
            this.mIndex.put(this.mSections.get(i2), Integer.valueOf(position));
            this.mPositions.add(Integer.valueOf(position));
            position += this.mMap.get(this.mSections.get(i2)).size();
        }
        this.sectionS = new ArrayList();
        for (int i3 = 0; i3 < this.mSections.size(); i3++) {
            String head = this.mSections.get(i3);
            for (Map.Entry entry : this.mMap.entrySet()) {
                if (entry.getKey().equals(head)) {
                    List<VirtualCity> head_letters = (List) entry.getValue();
                    for (int j = 0; j < head_letters.size(); j++) {
                        this.sectionS.add(new SectionListItem(head_letters.get(j), head));
                    }
                }
            }
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        this.letterView.setHight((float) this.letterView.getHeight());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setCityUI();
        GetCityTask.reset();
        GetCityTask getCity = new GetCityTask(this);
        getCity.setCityFinishListener(new GetCityTask.GetCityFinishListener() {
            public void onFinish() {
                CityChangeActivity.this.setCityUI();
            }
        });
        getCity.execute(new Object[0]);
        if (this.isFirst != 1) {
            openLocation();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        if (this.isFirst == 1) {
            changeCity(((NewHouseApplication) this.mApplication).getCity());
            return false;
        }
        finish();
        return false;
    }

    /* access modifiers changed from: protected */
    public void initView() {
        if (getIntent().getExtras() != null) {
            this.isFirst = getIntent().getExtras().getInt("isFirst", 0);
            tab_index = getIntent().getExtras().getInt("tab_index", 0);
        }
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.setTvTitleText((int) R.string.text_city_change);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CityChangeActivity.this.isFirst == 1) {
                    CityChangeActivity.this.changeCity(((NewHouseApplication) CityChangeActivity.this.mApplication).getCity());
                }
                CityChangeActivity.this.finish();
            }
        });
        this.currentCity = (TextView) findViewById(R.id.current_city);
        this.current_city_layout = (RelativeLayout) findViewById(R.id.current_city_layout);
        this.othercity_listview = (NoScrollListView) findViewById(R.id.othercity_listview);
        this.adapter = new NormalSearchAdapter(this, (NewHouseApplication) this.mApplication);
        this.adapter.setType(1);
        this.othercity_listview.setAdapter((ListAdapter) this.adapter);
        this.current_city_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CityChangeActivity.this.currentCity.getText().equals(CityChangeActivity.this.getResources().getString(R.string.text_locationing)) && !CityChangeActivity.this.currentCity.getText().equals(CityChangeActivity.this.getResources().getString(R.string.text_changecity_location_failure))) {
                    String t = AppArrays.getCity(CityChangeActivity.this.currentCity.getText().toString());
                    if (!TextUtils.isEmpty(t)) {
                        CityChangeActivity.this.changeCity(t);
                    }
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.house365.core.view.pinned.PinnedHeaderListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void setCityUI() {
        this.cityAdapter.clear();
        this.cityAdapter.notifyDataSetChanged();
        this.citys = AppArrays.getCityList();
        if (this.citys != null && this.citys.length > 0) {
            this.virtualCitys = this.citys;
            initPineData();
            this.letterView.setAlphaIndexer(this.mIndex);
            this.pinnedlistView.setPinnedHeaderView(LayoutInflater.from(this).inflate((int) R.layout.city_expandle_header, (ViewGroup) this.pinnedlistView, false));
            this.cityAdapter.addAll(this.sectionS);
            this.cityAdapter.notifyDataSetChanged();
            this.cityAdapter.setOnItemClickListener(new BaseSectionListAdapter.SectionListItemClickListener() {
                public void onSubItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    VirtualCity v_city = (VirtualCity) ((SectionListItem) CityChangeActivity.this.cityAdapter.getItem(position)).getItem();
                    if (v_city != null) {
                        String v_city_py = v_city.getCity_py();
                        String v_city_name = v_city.getCity_name();
                        if (TextUtils.isEmpty(v_city_py)) {
                            return;
                        }
                        if (CityChangeActivity.this.isFirst == 1) {
                            CityChangeActivity.this.changeCity(v_city_py);
                        } else if (!v_city_name.equals(((NewHouseApplication) CityChangeActivity.this.mApplication).getCityName())) {
                            CityChangeActivity.this.changeCity(v_city_py);
                        } else {
                            CityChangeActivity.this.finish();
                        }
                    }
                }

                public void onHeaderItemClick(View view, int startposition, int endposition) {
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pinned_layout = (RelativeLayout) findViewById(R.id.pinned_layout);
        this.pinnedlistView = (PinnedHeaderListView) findViewById(R.id.pinned_listview);
        this.letterView = (QuickAlphabeticView) findViewById(R.id.fast_scroll);
        this.letterView.init(this);
        this.letterView.setListView(this.pinnedlistView);
        this.letterView.setVisibility(0);
        this.cityAdapter = new ExpandableCityAdapter(this);
        this.pinnedlistView.setAdapter((ListAdapter) this.cityAdapter);
        if (this.isFirst == 1) {
            openLocation();
        }
    }

    /* access modifiers changed from: private */
    public void openLocation() {
        if (!DeviceUtil.isOpenLoaction(this)) {
            DialogUtil.showConfrimDialog(this, (int) R.string.text_location_title, (int) R.string.text_location_prompt, (int) R.string.text_location_yes, (int) R.string.text_location_no, new ConfirmDialogListener() {
                public void onPositive(DialogInterface dialog) {
                    CityChangeActivity.this.startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 1);
                    CityChangeActivity.this.loadLocation();
                }

                public void onNegative(DialogInterface dialog) {
                    CityChangeActivity.this.handler.sendEmptyMessage(2);
                }
            });
        } else {
            loadLocation();
        }
    }

    /* access modifiers changed from: private */
    public void ensureChange(final String current_City) {
        if (AppMethods.isInCitys(current_City) && !current_City.equals(AppArrays.getCityShowname(((NewHouseApplication) this.mApplication).getCity())) && !isFinishing()) {
            DialogUtil.showConfrimDialog(this, getResources().getString(R.string.text_map_location_city_msg, current_City), getResources().getString(R.string.dialog_button_ok), new ConfirmDialogListener() {
                public void onPositive(DialogInterface dialog) {
                    CityChangeActivity.this.changeCity(AppArrays.getCity(current_City));
                }

                public void onNegative(DialogInterface dialog) {
                }
            });
        }
    }
}
