package com.house365.newhouse.ui.user.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.house365.core.util.AbstractCallBack;
import java.util.List;

public class PageViewAdapter extends PagerAdapter {
    private AbstractCallBack finshMethod;
    private List<View> views;

    public PageViewAdapter(Context mcontext, AbstractCallBack finshMethod2, List<View> views2) {
        this.views = views2;
        this.finshMethod = finshMethod2;
    }

    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView(this.views.get(arg1));
    }

    public void finishUpdate(View arg0) {
    }

    public int getCount() {
        if (this.views != null) {
            return this.views.size();
        }
        return 0;
    }

    public Object instantiateItem(View arg0, int arg1) {
        ((ViewPager) arg0).addView(this.views.get(arg1), arg1);
        return this.views.get(arg1);
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void startUpdate(View arg0) {
    }
}
