package com.house365.newhouse.ui.tools;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.view.HeadNavigateView;
import com.house365.newhouse.R;
import java.util.Arrays;

public class LoanCalSelProActivity extends BaseCommonActivity {
    public static final String INTENT_IDX = "idx";
    public static final String INTENT_PROS = "pros";
    public static final String INTENT_RESULT_IDX = "result_idx";
    public static final String INTENT_TITLE = "title";
    private HeadNavigateView head_view;
    /* access modifiers changed from: private */
    public int idx;
    ListView listView;
    private String[] pros;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.loan_cal_sel_pro_layout);
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.head_view = (HeadNavigateView) findViewById(R.id.head_view);
        this.head_view.getBtn_left().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoanCalSelProActivity.this.finish();
            }
        });
        this.listView = (ListView) findViewById(R.id.listView);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.pros = getIntent().getStringArrayExtra(INTENT_PROS);
        this.idx = getIntent().getIntExtra(INTENT_IDX, 0);
        this.head_view.setTvTitleText(getIntent().getStringExtra(INTENT_TITLE));
        PropertyAdapter adapter = new PropertyAdapter(this);
        adapter.addAll(Arrays.asList(this.pros));
        this.listView.setAdapter((ListAdapter) adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Intent data = new Intent();
                data.putExtra(LoanCalSelProActivity.INTENT_RESULT_IDX, arg2);
                LoanCalSelProActivity.this.setResult(-1, data);
                LoanCalSelProActivity.this.finish();
            }
        });
    }

    class PropertyAdapter extends BaseListAdapter<String> {
        public PropertyAdapter(Context context) {
            super(context);
        }

        public View getView(int position, View convertView, ViewGroup viewgroup) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(LoanCalSelProActivity.this).inflate((int) R.layout.item_list_loan_pro, (ViewGroup) null);
                holder = new ViewHolder(this, null);
                holder.tvPro = (TextView) convertView.findViewById(R.id.tvPro);
                holder.inc_cal_pro = (ImageView) convertView.findViewById(R.id.inc_cal_pro);
                holder.divider_cal_pro = convertView.findViewById(R.id.divider_cal_pro);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvPro.setText((String) getItem(position));
            if (position == LoanCalSelProActivity.this.idx) {
                holder.inc_cal_pro.setVisibility(0);
            } else {
                holder.inc_cal_pro.setVisibility(8);
            }
            if (position == getCount() - 1) {
                holder.divider_cal_pro.setVisibility(8);
            } else {
                holder.divider_cal_pro.setVisibility(0);
            }
            return convertView;
        }

        private class ViewHolder {
            View divider_cal_pro;
            ImageView inc_cal_pro;
            TextView tvPro;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(PropertyAdapter propertyAdapter, ViewHolder viewHolder) {
                this();
            }
        }
    }
}
