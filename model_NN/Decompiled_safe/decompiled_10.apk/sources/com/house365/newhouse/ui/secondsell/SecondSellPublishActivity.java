package com.house365.newhouse.ui.secondsell;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.house365.core.activity.BaseCommonActivity;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RegexUtil;
import com.house365.core.util.TextUtil;
import com.house365.core.util.resource.ResourceUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.task.GetPublishConfigTask;
import com.house365.newhouse.task.GetValidateTask;
import com.house365.newhouse.task.PublishHouseTask;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.user.MyPublishActivity;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

public class SecondSellPublishActivity extends BaseCommonActivity {
    /* access modifiers changed from: private */
    public Button btn_captcha;
    private Button btn_publish;
    private Button btn_relogin;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int districtValue;
    /* access modifiers changed from: private */
    public EditText et_captcha;
    /* access modifiers changed from: private */
    public EditText et_contact;
    /* access modifiers changed from: private */
    public EditText et_tel;
    /* access modifiers changed from: private */
    public EditText et_title;
    private PublishHouse house;
    boolean[] isChooseItem = new boolean[6];
    /* access modifiers changed from: private */
    public NewHouseApplication mApplication;
    /* access modifiers changed from: private */
    public MyCountimer mctime;
    private View.OnClickListener publishSubmitListener = new View.OnClickListener() {
        public void onClick(View v) {
            SecondSellPublishActivity.this.publishVal.put("name", "HousePerson");
            SecondSellPublishActivity.this.publishVal.put("method", "secondhouse.arrSaveBuyHouse");
            if (SecondSellPublishActivity.this.getResources().getString(R.string.text_channel_hint).equals(SecondSellPublishActivity.this.tv_channel.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_channel_hint);
            } else if (SecondSellPublishActivity.this.getResources().getString(R.string.text_price_sell_hint).equals(SecondSellPublishActivity.this.tv_sell_price.getText().toString()) && SecondSellPublishActivity.this.publishType == 4) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_price_sell_hint);
            } else if (SecondSellPublishActivity.this.getResources().getString(R.string.text_buildarea_hint).equals(SecondSellPublishActivity.this.tv_buildarea.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_buildarea_hint);
            } else if (SecondSellPublishActivity.this.getResources().getString(R.string.text_sell_trait_hint).equals(SecondSellPublishActivity.this.tv_sell_trait.getText().toString()) && SecondSellPublishActivity.this.publishType == 4) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_sell_trait_hint);
            } else if (TextUtils.isEmpty(SecondSellPublishActivity.this.et_title.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_sell_title_hint);
            } else if (TextUtils.isEmpty(SecondSellPublishActivity.this.et_contact.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_contact_hint);
            } else if (!RegexUtil.isChineseLength(SecondSellPublishActivity.this.et_contact.getText().toString(), 1, 6)) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_contact_error);
            } else if (TextUtils.isEmpty(SecondSellPublishActivity.this.et_tel.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_tel_hint);
            } else if (!RegexUtil.isMobileNumber(SecondSellPublishActivity.this.et_tel.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_tel_error);
            } else if (TextUtils.isEmpty(SecondSellPublishActivity.this.et_captcha.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_captcha_hint);
            } else if (!SecondSellPublishActivity.this.validateCode.equals(SecondSellPublishActivity.this.et_captcha.getText().toString())) {
                SecondSellPublishActivity.this.showToast((int) R.string.text_captcha_error);
            } else {
                SecondSellPublishActivity.this.publishVal.put("address", SecondSellPublishActivity.this.et_title.getText().toString());
                SecondSellPublishActivity.this.publishVal.put("contactor", SecondSellPublishActivity.this.et_contact.getText().toString());
                SecondSellPublishActivity.this.publishVal.put("telno", SecondSellPublishActivity.this.et_tel.getText().toString());
                SecondSellPublishActivity.this.publishVal.put("validcode", SecondSellPublishActivity.this.et_captcha.getText().toString());
                PublishHouseTask publishTask = new PublishHouseTask(SecondSellPublishActivity.this.context, SecondSellPublishActivity.this.publishVal, 1);
                publishTask.execute(new Object[0]);
                publishTask.setOnSuccessListener(new PublishHouseTask.PublishOnSuccessListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void OnSuccess(CommonResultInfo v, PublishHouse publishHouse) {
                        SecondSellPublishActivity.this.mApplication.addPublishRec(SecondSellPublishActivity.this.setPublishData(publishHouse.getId(), publishHouse.getCreatetime()), 10, SecondSellPublishActivity.this.publishType);
                        Intent intent = new Intent(SecondSellPublishActivity.this.context, MyPublishActivity.class);
                        intent.putExtra(MyPublishActivity.INTENT_PUBLISH_TYPE, 4);
                        intent.putExtra(MyPublishActivity.INTENT_PUBLISH_OFFER, false);
                        SecondSellPublishActivity.this.startActivity(intent);
                        SecondSellPublishActivity.this.finish();
                    }
                });
            }
        }
    };
    /* access modifiers changed from: private */
    public int publishType = 4;
    /* access modifiers changed from: private */
    public HashMap<String, String> publishVal = new HashMap<>();
    /* access modifiers changed from: private */
    public String roomeValue;
    String streetValue;
    /* access modifiers changed from: private */
    public TextView tv_buildarea;
    /* access modifiers changed from: private */
    public TextView tv_channel;
    private TextView tv_contact_label;
    /* access modifiers changed from: private */
    public TextView tv_district;
    private TextView tv_remark_label;
    /* access modifiers changed from: private */
    public TextView tv_roomtype;
    /* access modifiers changed from: private */
    public TextView tv_sell_price;
    /* access modifiers changed from: private */
    public TextView tv_sell_trait;
    private TextView tv_tel_label;
    private TextView tv_title_label;
    /* access modifiers changed from: private */
    public String validateCode = StringUtils.EMPTY;
    private View view_buildarea;
    private View view_captcha;
    private View view_channel;
    private View view_contact;
    private View view_district;
    private View view_roomtype;
    private View view_sell_price;
    private View view_sell_trait;
    private View view_tel;
    private View view_title;

    /* access modifiers changed from: protected */
    public void preparedCreate(Bundle savedInstanceState) {
        setContentView((int) R.layout.second_sell_public_layout);
        this.context = this;
        this.mApplication = (NewHouseApplication) getApplication();
    }

    /* access modifiers changed from: protected */
    public void initView() {
        this.btn_publish = (Button) findViewById(R.id.btn_publish);
        this.btn_publish.setOnClickListener(this.publishSubmitListener);
        this.btn_publish.setOnClickListener(this.publishSubmitListener);
        this.view_channel = findViewById(R.id.view_channel);
        this.view_district = findViewById(R.id.view_district);
        this.view_roomtype = findViewById(R.id.view_roomtype);
        this.view_buildarea = findViewById(R.id.view_buildarea);
        this.view_sell_price = findViewById(R.id.view_sell_price);
        this.view_sell_trait = findViewById(R.id.view_sell_trait);
        this.view_title = findViewById(R.id.view_title);
        this.view_contact = findViewById(R.id.view_contact);
        this.view_tel = findViewById(R.id.view_tel);
        this.view_captcha = findViewById(R.id.view_captcha);
        this.tv_channel = (TextView) findViewById(R.id.tv_channel);
        this.tv_district = (TextView) findViewById(R.id.tv_district);
        this.tv_roomtype = (TextView) findViewById(R.id.tv_roomtype);
        this.tv_buildarea = (TextView) findViewById(R.id.tv_buildarea);
        this.tv_title_label = (TextView) findViewById(R.id.tv_title_label);
        this.tv_sell_price = (TextView) findViewById(R.id.tv_sell_price);
        this.tv_sell_trait = (TextView) findViewById(R.id.tv_sell_trait);
        this.et_title = (EditText) findViewById(R.id.et_title);
        this.et_contact = (EditText) findViewById(R.id.et_contact);
        this.et_tel = (EditText) findViewById(R.id.et_tel);
        this.et_captcha = (EditText) findViewById(R.id.et_captcha);
        this.btn_captcha = (Button) findViewById(R.id.btn_captcha);
        this.btn_captcha.setEnabled(false);
        this.et_tel.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                if (s.toString().length() != 11) {
                    return;
                }
                if (!TextUtil.isMobliePhone(SecondSellPublishActivity.this.et_tel.getText().toString())) {
                    SecondSellPublishActivity.this.showToast((int) R.string.text_mobile_error);
                    SecondSellPublishActivity.this.btn_captcha.setEnabled(false);
                    return;
                }
                SecondSellPublishActivity.this.btn_captcha.setEnabled(true);
            }
        });
        this.btn_relogin = (Button) findViewById(R.id.btn_relogin);
        this.btn_relogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppMethods.showLoginDialog(SecondSellPublishActivity.this.context, R.string.text_relogin_msg, StringUtils.EMPTY);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getPublistConfig();
    }

    private void getPublistConfig() {
        GetPublishConfigTask publishConfigTask = new GetPublishConfigTask(this, this.publishType);
        publishConfigTask.setOnFinishListener(new GetPublishConfigTask.PublishConfigOnFinish() {
            public void onSuccess(HouseBaseInfo v) {
                if (v.getJsonStr().equals(SecondSellPublishActivity.this.getResources().getString(R.string.text_no_network))) {
                    SecondSellPublishActivity.this.showToast((int) R.string.text_no_network);
                    SecondSellPublishActivity.this.finish();
                }
                SecondSellPublishActivity.this.initPublishViewListener(v);
                if (SecondSellPublishActivity.this.mApplication.getMobile().toString() != null) {
                    SecondSellPublishActivity.this.et_tel.setText(SecondSellPublishActivity.this.mApplication.getMobile().toString());
                }
            }

            public void onFail() {
            }
        });
        publishConfigTask.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void setChooseTextView(TextView tv, CharSequence str) {
        tv.setText(str);
        tv.setTextColor(ResourceUtil.getColor(this, R.color.black));
    }

    /* access modifiers changed from: private */
    public PublishHouse setPublishData(String id, int createTime) {
        String str = id;
        PublishHouse house2 = new PublishHouse(str, this.et_title.getText().toString(), this.publishVal.get("remark"), this.publishVal.get("contactor"), this.publishVal.get("telno"), this.tv_channel.getText().toString(), this.tv_district.getText().toString(), this.tv_sell_price.getText().toString(), this.tv_roomtype.getText().toString(), this.tv_sell_trait.getText().toString(), this.tv_buildarea.getText().toString());
        house2.setCreatetime(createTime);
        return house2;
    }

    public static int parseValueToIndex(String[] arrays, String value) {
        if (arrays != null) {
            for (int i = 0; i < arrays.length; i++) {
                if (arrays[i].equals(value)) {
                    return i;
                }
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void initPublishViewListener(final HouseBaseInfo publishConfig) {
        this.view_channel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = TextUtil.arrayToShift((String[]) publishConfig.getSell_config().get(HouseBaseInfo.INFOTYPE).toArray(new String[0]));
                ActivityUtil.showSingleChooseDialog(SecondSellPublishActivity.this.context, (int) R.string.text_channel, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondSellPublishActivity.this.publishVal.put(HouseBaseInfo.INFOTYPE, new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_channel, chooseItems[which]);
                    }
                });
            }
        });
        this.view_district.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] districts = (String[]) publishConfig.getSell_config().get(HouseBaseInfo.DISTRICT).toArray(new String[0]);
                AlertDialog.Builder builder = new AlertDialog.Builder(SecondSellPublishActivity.this.context);
                int access$17 = SecondSellPublishActivity.this.districtValue;
                final HouseBaseInfo houseBaseInfo = publishConfig;
                builder.setSingleChoiceItems(districts, access$17, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which >= 0) {
                            SecondSellPublishActivity.this.districtValue = which;
                            SecondSellPublishActivity.this.publishVal.put(HouseBaseInfo.DISTRICT, new StringBuilder(String.valueOf(SecondSellPublishActivity.this.districtValue)).toString());
                            if (which == 0) {
                                SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_district, districts[SecondSellPublishActivity.this.districtValue]);
                                dialog.dismiss();
                                return;
                            }
                            if (which > 0) {
                                final Map<String, String> districtStreetMap = houseBaseInfo.getDistrictStreet().get(districts[SecondSellPublishActivity.this.districtValue]);
                                final String[] districtStreets = (String[]) districtStreetMap.keySet().toArray(new String[0]);
                                AlertDialog.Builder builder = new AlertDialog.Builder(SecondSellPublishActivity.this.context);
                                int parseValueToIndex = SecondSellPublishActivity.parseValueToIndex(districtStreets, HouseBaseInfo.parseValueToKey(districtStreetMap, new StringBuilder(String.valueOf(SecondSellPublishActivity.this.streetValue)).toString()));
                                final String[] strArr = districts;
                                builder.setSingleChoiceItems(districtStreets, parseValueToIndex, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which >= 0) {
                                            String key = districtStreets[which];
                                            SecondSellPublishActivity.this.streetValue = (String) districtStreetMap.get(key);
                                            SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_district, String.valueOf(strArr[SecondSellPublishActivity.this.districtValue]) + "/" + districtStreets[which]);
                                            SecondSellPublishActivity.this.publishVal.put("streetid", SecondSellPublishActivity.this.streetValue);
                                        }
                                        dialog.dismiss();
                                    }
                                }).setTitle((int) R.string.street).create().show();
                            }
                            dialog.dismiss();
                        }
                    }
                }).setTitle((int) R.string.district).create().show();
            }
        });
        this.view_sell_price.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = TextUtil.arrayToShift((String[]) publishConfig.getSell_config().get("price").toArray(new String[0]));
                ActivityUtil.showSingleChooseDialog(SecondSellPublishActivity.this.context, (int) R.string.text_sell_price, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondSellPublishActivity.this.publishVal.put("price", new StringBuilder(String.valueOf(which + 1)).toString());
                        SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_sell_price, chooseItems[which]);
                    }
                });
            }
        });
        this.view_roomtype.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String[] chooseItems = (String[]) publishConfig.getSell_config().get(HouseBaseInfo.ROOM).toArray(new String[0]);
                ActivityUtil.showSingleChooseDialog(SecondSellPublishActivity.this.context, (int) R.string.text_roomtype, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondSellPublishActivity.this.publishVal.put(HouseBaseInfo.ROOM, new StringBuilder(String.valueOf(which)).toString());
                        SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_roomtype, chooseItems[which]);
                        SecondSellPublishActivity.this.roomeValue = chooseItems[which];
                    }
                });
            }
        });
        this.view_sell_trait.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] chooseItems = (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.FINDROOM).toArray(new String[0]);
                new AlertDialog.Builder(SecondSellPublishActivity.this.context).setTitle((int) R.string.text_sell_trait).setMultiChoiceItems(chooseItems, SecondSellPublishActivity.this.isChooseItem, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    }
                }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String temp = StringUtils.EMPTY;
                        String tempIdstr = StringUtils.EMPTY;
                        for (int i = 0; i < chooseItems.length; i++) {
                            if (SecondSellPublishActivity.this.isChooseItem[i]) {
                                temp = String.valueOf(temp) + ((Object) chooseItems[i]) + ",";
                                tempIdstr = String.valueOf(tempIdstr) + i + ",";
                            }
                        }
                        if (!TextUtils.isEmpty(tempIdstr)) {
                            SecondSellPublishActivity.this.publishVal.put(HouseBaseInfo.FINDROOM, tempIdstr.substring(0, tempIdstr.length() - 1));
                            SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_sell_trait, temp.substring(0, temp.length() - 1));
                        }
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
            }
        });
        this.view_buildarea.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final CharSequence[] chooseItems = TextUtil.arrayToShift((String[]) publishConfig.getSell_config().get("buildarea").toArray(new String[0]));
                ActivityUtil.showSingleChooseDialog(SecondSellPublishActivity.this.context, (int) R.string.text_buildarea, chooseItems, new ActivityUtil.ItemDialogOnChooseListener() {
                    public void onChoose(DialogInterface dialog, int which) {
                        SecondSellPublishActivity.this.publishVal.put("buildarea", new StringBuilder(String.valueOf(which + 1)).toString());
                        CorePreferences.DEBUG("****buildarea*****" + (which + 1));
                        SecondSellPublishActivity.this.setChooseTextView(SecondSellPublishActivity.this.tv_buildarea, chooseItems[which]);
                    }
                });
            }
        });
        this.btn_captcha.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String telno = SecondSellPublishActivity.this.et_tel.getText().toString();
                if (!RegexUtil.isMobileNumber(telno)) {
                    SecondSellPublishActivity.this.showToast((int) R.string.text_mobile_error);
                    return;
                }
                GetValidateTask validateTask = new GetValidateTask(SecondSellPublishActivity.this.context, telno, SecondSellPublishActivity.this.publishType);
                validateTask.setOnSuccessListener(new GetValidateTask.ValidateOnSuccessListener() {
                    public void OnSuccess(CommonResultInfo v) {
                        SecondSellPublishActivity.this.mctime = new MyCountimer(DateUtils.MILLIS_PER_MINUTE, 1000);
                        SecondSellPublishActivity.this.mctime.start();
                        SecondSellPublishActivity.this.btn_captcha.setEnabled(false);
                        SecondSellPublishActivity.this.validateCode = v.getData();
                    }
                });
                validateTask.execute(new Object[0]);
            }
        });
    }

    class MyCountimer extends CountDownTimer {
        public MyCountimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            SecondSellPublishActivity.this.btn_captcha.setEnabled(true);
            SecondSellPublishActivity.this.btn_captcha.setText((int) R.string.btn_captcha);
        }

        public void onTick(long millisUntilFinished) {
            SecondSellPublishActivity.this.btn_captcha.setText(TextUtil.getString(SecondSellPublishActivity.this.context, R.string.text_captcha_countdown, Long.valueOf(millisUntilFinished / 1000)));
        }
    }

    private CharSequence[] getPriceChooseItem(int channelIndex, HouseBaseInfo publishConfig) {
        switch (channelIndex) {
            case 0:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 1:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 2:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
            case 3:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_OFFICE).toArray(new String[0]);
            case 4:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_STORE).toArray(new String[0]);
            default:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.PRICE_DEFAULT).toArray(new String[0]);
        }
    }

    private CharSequence[] getBuildAreaChooseItem(int channelIndex, HouseBaseInfo publishConfig) {
        switch (channelIndex) {
            case 0:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 1:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 2:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
            case 3:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_OFFICE).toArray(new String[0]);
            case 4:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_STORE).toArray(new String[0]);
            default:
                return (CharSequence[]) publishConfig.getSell_config().get(HouseBaseInfo.BUILDERAREA_DEFAULT).toArray(new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mctime != null) {
            this.mctime.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.house = (PublishHouse) getIntent().getSerializableExtra(MyPublishActivity.INTENT_HOUSE);
        if (this.house != null) {
            this.view_channel.setEnabled(false);
            this.view_district.setEnabled(false);
            this.view_roomtype.setEnabled(false);
            this.view_buildarea.setEnabled(false);
            this.view_title.setEnabled(false);
            this.view_contact.setEnabled(false);
            this.view_tel.setEnabled(false);
            this.view_sell_price.setEnabled(false);
            this.view_sell_trait.setEnabled(false);
            this.et_title.setEnabled(false);
            this.et_contact.setEnabled(false);
            this.et_tel.setEnabled(false);
            this.et_captcha.setEnabled(false);
            this.btn_relogin.setVisibility(4);
            this.btn_captcha.setVisibility(4);
            this.view_captcha.setVisibility(4);
            this.btn_publish.setVisibility(8);
            this.tv_channel.setText(this.house.getInfotype());
            this.tv_district.setText(this.house.getDistrict());
            this.tv_roomtype.setText(this.house.getRoom());
            this.tv_buildarea.setText(this.house.getBuildarea());
            this.tv_sell_price.setText(this.house.getPrice());
            this.tv_sell_trait.setText(this.house.getFindroom());
            this.et_title.setText(this.house.getTitle());
            this.et_contact.setText(this.house.getContactor());
            this.et_tel.setText(this.house.getTelno());
            return;
        }
        this.btn_relogin.setVisibility(0);
        this.btn_captcha.setVisibility(0);
        this.view_captcha.setVisibility(0);
        this.btn_publish.setVisibility(0);
        String mobile = this.mApplication.getMobile();
        if (!TextUtils.isEmpty(mobile)) {
            this.et_tel.setText(mobile);
        } else {
            this.et_tel.setText(StringUtils.EMPTY);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        AppMethods.exitPublish(this.house, this.publishVal, this.context);
        return false;
    }
}
