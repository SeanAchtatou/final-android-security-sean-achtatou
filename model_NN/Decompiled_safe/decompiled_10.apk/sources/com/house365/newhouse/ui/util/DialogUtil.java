package com.house365.newhouse.ui.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.inter.DialogOnPositiveListener;
import com.house365.core.util.AbstractCallBack;
import com.house365.core.util.ActivityUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.constant.AppArrays;
import java.util.regex.Pattern;

public class DialogUtil {
    public static void openGpsDialog(final Context context, final AbstractCallBack finshMethod) {
        ActivityUtil.showConfrimDialog(context, R.string.text_location_no, R.string.text_location_yes, R.string.text_location_title, R.string.text_location_prompt, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                ((Activity) context).startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 1);
            }

            public void onNegative(DialogInterface dialog) {
                finshMethod.doCallBack();
            }
        });
    }

    public static void deleteDialog(final Context context, int position, final AbstractCallBack finshMethod) {
        new AlertDialog.Builder(context).setItems(AppArrays.editMenu, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (AppArrays.editMenu[which].equals("删除")) {
                    Context context = context;
                    final AbstractCallBack abstractCallBack = finshMethod;
                    ActivityUtil.showConfrimDialog(context, (int) R.string.text_confrim_delete, new DialogOnPositiveListener() {
                        public void onPositive(DialogInterface dialog) {
                            abstractCallBack.doCallBack();
                        }
                    });
                }
            }
        }).show();
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {
        if (Pattern.compile("1[0-9]{10}").matcher(phoneNumber).matches()) {
            return true;
        }
        return false;
    }
}
