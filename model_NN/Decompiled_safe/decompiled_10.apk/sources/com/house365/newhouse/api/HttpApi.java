package com.house365.newhouse.api;

import android.location.Location;
import android.text.TextUtils;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.core.bean.VersionInfo;
import com.house365.core.bean.common.CommonResultInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.HttpAPIAdapter;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.reflect.ReflectUtil;
import com.house365.core.thirdpart.auth.ThirdPartAuthActivity;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.lbs.MapUtil;
import com.house365.core.util.store.SharedPreferencesUtil;
import com.house365.newhouse.R;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.constant.Order;
import com.house365.newhouse.model.Ad;
import com.house365.newhouse.model.Album;
import com.house365.newhouse.model.AwardInfo;
import com.house365.newhouse.model.Block;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.EventNum;
import com.house365.newhouse.model.HotTag;
import com.house365.newhouse.model.House;
import com.house365.newhouse.model.HouseBaseInfo;
import com.house365.newhouse.model.News;
import com.house365.newhouse.model.Photo;
import com.house365.newhouse.model.RegionMap;
import com.house365.newhouse.model.SaleInfo;
import com.house365.newhouse.model.SecondHouse;
import com.house365.newhouse.model.ShakeInfo;
import com.house365.newhouse.model.VirtualCity;
import com.house365.newhouse.task.GetCityTask;
import com.house365.newhouse.tool.AppMethods;
import com.house365.newhouse.ui.privilege.CouponDetailsActivity;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

public class HttpApi extends HttpAPIAdapter {
    private final String API_KEY = "android";
    private final String VERSION = "3.0";
    private String baseUrl = "http://mtapi.house365.com";
    private NewHouseApplication mApplication;
    private SharedPreferencesUtil mPrefs;
    private String secondUrl = "http://mapi.house365.com/taofang/v1.0/esf/index.php";
    private String versionUrl = "http://app.house365.com/ver.php";
    private String virtualUrl = "http://mopen.house365.com/api/";

    public HttpApi(SharedPreferencesUtil mPrefs2, NewHouseApplication mApplication2) {
        super(mApplication2);
        this.mPrefs = mPrefs2;
        this.mApplication = mApplication2;
    }

    public VersionInfo getAppNewVersion() {
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("app", "taofang"));
        try {
            return (VersionInfo) getWithObject(this.versionUrl, olist, new VersionInfo());
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            return null;
        }
    }

    private void addCommonParams(Map<String, String> params) {
        params.put("city", this.mApplication.getCityKey());
        params.put("api_key", "android");
        params.put("v", "3.0");
        params.put("deviceid", this.mApplication.getDeviceId());
    }

    private void addPageInfo(Map<String, String> params) {
        params.put("pagesize", "20");
    }

    public String getWithCache(List<NameValuePair> olist, boolean virtual) throws HtppApiException, NetworkUnavailableException {
        String result;
        SharedPreferencesUtil sharedPreferencesUtil = this.mPrefs;
        this.mApplication.getClass();
        String result2 = sharedPreferencesUtil.getString(String.valueOf("apicache_") + URLEncodedUtils.format(olist, this.CHARENCODE), StringUtils.EMPTY);
        SharedPreferencesUtil sharedPreferencesUtil2 = this.mPrefs;
        this.mApplication.getClass();
        StringBuilder sb = new StringBuilder(String.valueOf("apicache_"));
        this.mApplication.getClass();
        if (!sharedPreferencesUtil2.getString(sb.append("last_request_date_").append(URLEncodedUtils.format(olist, this.CHARENCODE)).toString(), StringUtils.EMPTY).equals(TimeUtil.toDate())) {
            result2 = StringUtils.EMPTY;
        }
        if (!TextUtils.isEmpty(result2)) {
            return result2;
        }
        if (!DeviceUtil.isNetConnect(this.mApplication.getApplicationContext())) {
            return this.mApplication.getApplicationContext().getResources().getString(R.string.text_no_network);
        }
        if (virtual) {
            result = getWithString(this.virtualUrl, olist);
        } else {
            result = getWithString(this.baseUrl, olist);
        }
        SharedPreferencesUtil sharedPreferencesUtil3 = this.mPrefs;
        this.mApplication.getClass();
        sharedPreferencesUtil3.putString(String.valueOf("apicache_") + URLEncodedUtils.format(olist, this.CHARENCODE), result);
        if (TextUtils.isEmpty(result)) {
            return result;
        }
        SharedPreferencesUtil sharedPreferencesUtil4 = this.mPrefs;
        this.mApplication.getClass();
        StringBuilder sb2 = new StringBuilder(String.valueOf("apicache_"));
        this.mApplication.getClass();
        sharedPreferencesUtil4.putString(sb2.append("last_request_date_").append(URLEncodedUtils.format(olist, this.CHARENCODE)).toString(), TimeUtil.toDate());
        return result;
    }

    public String getCityWithCache(List<NameValuePair> olist) throws HtppApiException, NetworkUnavailableException {
        SharedPreferencesUtil sharedPreferencesUtil = this.mPrefs;
        this.mApplication.getClass();
        String result = sharedPreferencesUtil.getString(String.valueOf("apicache_") + URLEncodedUtils.format(olist, this.CHARENCODE), StringUtils.EMPTY);
        SharedPreferencesUtil sharedPreferencesUtil2 = this.mPrefs;
        this.mApplication.getClass();
        StringBuilder sb = new StringBuilder(String.valueOf("apicache_"));
        this.mApplication.getClass();
        if (!sharedPreferencesUtil2.getString(sb.append("last_request_date_").append(URLEncodedUtils.format(olist, this.CHARENCODE)).toString(), StringUtils.EMPTY).equals(TimeUtil.toDate())) {
            result = StringUtils.EMPTY;
        }
        if (TextUtils.isEmpty(result) && DeviceUtil.isNetConnect(this.mApplication.getApplicationContext())) {
            result = getWithString(this.virtualUrl, olist);
            SharedPreferencesUtil sharedPreferencesUtil3 = this.mPrefs;
            this.mApplication.getClass();
            sharedPreferencesUtil3.putString(String.valueOf("apicache_") + URLEncodedUtils.format(olist, this.CHARENCODE), result);
            if (!TextUtils.isEmpty(result)) {
                SharedPreferencesUtil sharedPreferencesUtil4 = this.mPrefs;
                this.mApplication.getClass();
                StringBuilder sb2 = new StringBuilder(String.valueOf("apicache_"));
                this.mApplication.getClass();
                sharedPreferencesUtil4.putString(sb2.append("last_request_date_").append(URLEncodedUtils.format(olist, this.CHARENCODE)).toString(), TimeUtil.toDate());
            }
        }
        return result;
    }

    private void addPageInfo(List<NameValuePair> olist) {
        olist.add(new BasicNameValuePair("pagesize", "20"));
    }

    private void addCommonParams(List<NameValuePair> olist) {
        olist.add(new BasicNameValuePair("api_key", "android"));
        olist.add(new BasicNameValuePair("version", this.mApplication.getVersion()));
        olist.add(new BasicNameValuePair("deviceid", this.mApplication.getDeviceId()));
        olist.add(new BasicNameValuePair("city", this.mApplication.getCityKey()));
    }

    private void addCommonParams(MultipartEntity entity) throws UnsupportedEncodingException {
        entity.addPart("api_key", new StringBody("android", Charset.forName(this.CHARENCODE)));
        entity.addPart("v", new StringBody(this.mApplication.getVersion(), Charset.forName(this.CHARENCODE)));
        entity.addPart("deviceid", new StringBody(this.mApplication.getDeviceId(), Charset.forName(this.CHARENCODE)));
        entity.addPart("city", new StringBody(this.mApplication.getCityKey(), Charset.forName(this.CHARENCODE)));
    }

    public VirtualCity[] getCityConfig() throws HtppApiException, NetworkUnavailableException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getCityList", StringUtils.EMPTY);
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("method", "getCityList"));
        String result = getCityWithCache(list);
        if (TextUtils.isEmpty(result)) {
            if (AppArrays.fromNet != 2 && AppArrays.citys == null) {
                AppArrays.initVirtualCity(AppArrays.getVirtualCityJson());
            }
            AppArrays.fromNet = 2;
        } else if (GetCityTask.reset() && AppArrays.citySet.isEmpty()) {
            try {
                JSONArray aJsonArray = new JSONArray(result);
                if (aJsonArray != null) {
                    try {
                        VirtualCity item = new VirtualCity();
                        for (int i = 0; i < aJsonArray.length(); i++) {
                            item = (VirtualCity) ReflectUtil.copy(item.getClass(), aJsonArray.getJSONObject(i));
                            if (item != null && !AppArrays.citySet.contains(item)) {
                                AppArrays.citySet.add(item);
                                AppArrays.citys.put(item.getCity_name(), item.getCity_py());
                                AppArrays.cityLocation.put(item.getCity_py(), String.valueOf(item.getCity_y()) + "," + item.getCity_x());
                            }
                        }
                    } catch (JSONException e) {
                        return null;
                    } catch (ReflectException e2) {
                        return null;
                    }
                }
                AppArrays.fromNet = 1;
            } catch (JSONException | ReflectException e3) {
                return null;
            }
        }
        return AppArrays.cityArray;
    }

    public HouseBaseInfo getNewHouseConfigInfo() throws HttpParseException, HtppApiException, NetworkUnavailableException, JSONException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getSearchConfig", App.NEW);
        List<NameValuePair> list = new ArrayList<>();
        addCommonParams(list);
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (!isVirtual) {
            list.add(new BasicNameValuePair("method", "newhouse.getSearchConfig"));
        } else {
            list.add(new BasicNameValuePair("method", "getSearchConfig"));
        }
        String result = getWithCache(list, isVirtual);
        HouseBaseInfo info = null;
        if (!TextUtils.isEmpty(result)) {
            if (result.equals(this.mApplication.getApplicationContext().getResources().getString(R.string.text_no_network))) {
                info = new HouseBaseInfo();
            } else {
                info = new HouseBaseInfo(result);
            }
            info.setJsonStr(result);
        }
        return info;
    }

    public List<House> getHouseListBySearch(String type, Location location, int radius, String district, String channel, String price, String block, String order, String k, String tag_id, int page) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseList", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (!isVirtual) {
            params.put("method", "newhouse.getHouseList");
        } else {
            params.put("method", "getHouseList");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        params.put("page", new StringBuilder().append(page).toString());
        if (!TextUtils.isEmpty(k)) {
            params.put("k", k);
        }
        if (location != null && radius > 0) {
            double[][] points = MapUtil.GetTwoPointLocation(location.getLatitude(), location.getLongitude(), radius);
            params.put("leftcoord", String.valueOf(points[0][0]) + "," + points[0][1]);
            params.put("rightcoord", String.valueOf(points[1][0]) + "," + points[1][1]);
        }
        if (!"0".equals(district) && !TextUtils.isEmpty(district)) {
            params.put("dist", district);
        }
        if (!"0".equals(channel) && !TextUtils.isEmpty(channel)) {
            params.put("p", channel);
        }
        if (!"0".equals(price) && !TextUtils.isEmpty(price)) {
            params.put("price", price);
        }
        if (!TextUtils.isEmpty(order)) {
            params.put("ordered", order);
        }
        if (!TextUtils.isEmpty(tag_id)) {
            params.put("tag_id", tag_id);
        }
        addCommonParams(params);
        addPageInfo(params);
        if (!isVirtual) {
            return getWithList(this.baseUrl, params, new House());
        }
        return getWithList(this.virtualUrl, params, new House());
    }

    public List<House> getNewHouseListBySearch(String k) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseList", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getHouseList");
        } else {
            params.put("method", "newhouse.getHouseList");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, "house");
        params.put("page", "1");
        if (!TextUtils.isEmpty(k)) {
            params.put("k", k);
        }
        addCommonParams(params);
        params.put("pagesize", "10");
        if (isVirtual) {
            return getWithList(this.virtualUrl, params, new House());
        }
        return getWithList(this.baseUrl, params, new House());
    }

    public House getHouseInfoById(boolean virtual, String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseDetail", App.NEW);
        Map<String, String> params = new HashMap<>();
        if (virtual) {
            params.put("method", "getHouseDetail");
        } else {
            params.put("method", "newhouse.getHouseDetail");
        }
        if (!TextUtils.isEmpty(type)) {
            params.put("p", type);
        }
        params.put("id", id);
        addCommonParams(params);
        if (virtual) {
            return (House) getWithObject(this.virtualUrl, params, new House());
        }
        return (House) getWithObject(this.baseUrl, params, new House());
    }

    public List<Event> getEventList(String type, int page) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getEventList", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getEventList");
        } else {
            params.put("method", "user.getEventList");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("phone", this.mApplication.getMobile());
        addCommonParams(params);
        if (isVirtual) {
            return getWithList(this.virtualUrl, params, new Event());
        }
        return getWithList(this.baseUrl, params, new Event());
    }

    public List<Event> getMySignList(String type, String mobile, int page) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getMySignList", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getMySignList");
        } else {
            params.put("method", "user.getMySignList");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        if (!type.equals("event") && !type.equals(App.Categroy.Event.SELL_EVENT)) {
            params.put("phone", mobile);
        }
        params.put("page", new StringBuilder().append(page).toString());
        addCommonParams(params);
        if (isVirtual) {
            return getWithList(this.virtualUrl, params, new Event());
        }
        return getWithList(this.baseUrl, params, new Event());
    }

    public EventNum getEventNum() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getEventCount", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getEventCount");
        } else {
            params.put("method", "user.getEventCount");
        }
        addCommonParams(params);
        if (isVirtual) {
            return (EventNum) getWithObject(this.virtualUrl, params, new EventNum());
        }
        return (EventNum) getWithObject(this.baseUrl, params, new EventNum());
    }

    public Event getEventDetail(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getEventDetail", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getEventDetail");
        } else {
            params.put("method", "user.getEventDetail");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        params.put("id", id);
        params.put("phone", this.mApplication.getMobile());
        addCommonParams(params);
        if (isVirtual) {
            return (Event) getWithObject(this.virtualUrl, params, new Event());
        }
        return (Event) getWithObject(this.baseUrl, params, new Event());
    }

    public AwardInfo getTryLuck(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "tryLuck", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "tryLucj");
        } else {
            params.put("method", "user.tryLuck");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        params.put("id", id);
        params.put("phone", this.mApplication.getMobile());
        addCommonParams(params);
        if (isVirtual) {
            return (AwardInfo) getWithObject(this.virtualUrl, params, new AwardInfo());
        }
        return (AwardInfo) getWithObject(this.baseUrl, params, new AwardInfo());
    }

    public CommonResultInfo signUp(String id, String type, String name, String mobile) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "signUp", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "signUp");
        } else {
            params.put("method", "user.signUp");
        }
        params.put("phone", mobile);
        params.put("id", id);
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        if (name != null) {
            params.put("name", name);
        }
        addCommonParams(params);
        if (isVirtual) {
            return (CommonResultInfo) getWithObject(this.virtualUrl, params, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.baseUrl, params, new CommonResultInfo());
    }

    public List<SaleInfo> getHouseSaleInfo(String id, int page) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseNewsById", App.NEW);
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getHouseNewsById");
        } else {
            params.put("method", "newhouse.getHouseNewsById");
        }
        params.put("id", id);
        params.put("page", new StringBuilder().append(page).toString());
        addCommonParams(params);
        addPageInfo(params);
        if (isVirtual) {
            return getWithList(this.virtualUrl, params, new SaleInfo());
        }
        return getWithList(this.baseUrl, params, new SaleInfo());
    }

    public List<Album> getHousePhotoById(String id, String ctg) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHousePhotoById", App.NEW);
        Map<String, String> params = new HashMap<>();
        params.put("method", "newhouse.getHousePhotoById");
        params.put("id", id);
        if (!StringUtils.EMPTY.equals(ctg)) {
            params.put("ctg", ctg);
        }
        addCommonParams(params);
        return getWithList(this.baseUrl, params, new Album());
    }

    public List<Photo> getVirtualHousePhotoById(String id, String ctg) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHousePhotoById", App.NEW);
        Map<String, String> params = new HashMap<>();
        params.put("method", "getHousePhotoById");
        params.put("id", id);
        if (!StringUtils.EMPTY.equals(ctg)) {
            params.put("ctg", ctg);
        }
        addCommonParams(params);
        return getWithList(this.virtualUrl, params, new Photo());
    }

    public CommonResultInfo favHouse(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "addFav", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "addFav");
        } else {
            params.put("method", "user.addFav");
        }
        params.put("id", id);
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        addCommonParams(params);
        if (isVirtual) {
            return (CommonResultInfo) getWithObject(this.virtualUrl, params, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.baseUrl, params, new CommonResultInfo());
    }

    public CommonResultInfo removeFavHouse(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "removeFav", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "removeFav");
        } else {
            params.put("method", "user.removeFav");
        }
        params.put("id", id);
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        addCommonParams(params);
        if (isVirtual) {
            return (CommonResultInfo) getWithObject(this.virtualUrl, params, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.baseUrl, params, new CommonResultInfo());
    }

    public List<HotTag> getHotTagList(String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHotTagList", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "getHotTagList");
        } else {
            params.put("method", "user.getHotTagList");
        }
        params.put(CouponDetailsActivity.INTENT_TYPE, type);
        addCommonParams(params);
        if (isVirtual) {
            return getWithList(this.virtualUrl, params, new HotTag());
        }
        return getWithList(this.baseUrl, params, new HotTag());
    }

    public List<Block> getSecondHouseListBySearch(String k) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getLegendBlockNameList", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("name", "HouseBlock"));
        olist.add(new BasicNameValuePair("method", "secondhouse.getLegendBlockNameList"));
        if (!TextUtils.isEmpty(k)) {
            olist.add(new BasicNameValuePair("keyword", k));
        }
        olist.add(new BasicNameValuePair("page", "1"));
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new Block());
    }

    public HouseBaseInfo getSecondHousebaseinfo(int type) throws HttpParseException, JSONException, HtppApiException, NetworkUnavailableException {
        List<NameValuePair> olist = new ArrayList<>();
        if (type == 1) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseSearchConf", "secondhouse");
            olist.add(new BasicNameValuePair("name", "HouseSell"));
            olist.add(new BasicNameValuePair("method", "secondhouse.getHouseSearchConf"));
        } else if (type == 2) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseSearchConf", "secondhouse");
            olist.add(new BasicNameValuePair("name", "HouseRent"));
            olist.add(new BasicNameValuePair("method", "secondhouse.getHouseSearchConf"));
        } else if (type == 3) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getBlockSearchConf", "secondhouse");
            olist.add(new BasicNameValuePair("name", "HouseBlock"));
            olist.add(new BasicNameValuePair("method", "secondhouse.getBlockSearchConf"));
        } else if (type == 4) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getBuyConf", "secondhouse");
            olist.add(new BasicNameValuePair("name", "HousePerson"));
            olist.add(new BasicNameValuePair("method", "secondhouse.getBuyConf"));
        } else if (type == 5) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getRentConf", "secondhouse");
            olist.add(new BasicNameValuePair("name", "HousePerson"));
            olist.add(new BasicNameValuePair("method", "secondhouse.getRentConf"));
        }
        addCommonParams(olist);
        String s = getWithCache(olist, false);
        if (TextUtils.isEmpty(s)) {
            return null;
        }
        if (!s.equals(this.mApplication.getApplicationContext().getResources().getString(R.string.text_no_network))) {
            return new HouseBaseInfo(s);
        }
        HouseBaseInfo info = new HouseBaseInfo();
        info.setJsonStr(s);
        return info;
    }

    public SecondHouse getHouseInfoByRowid(int type, String rowid, String app) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseDetail", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        if (type == 2) {
            olist.add(new BasicNameValuePair("name", "HouseRent"));
        } else if (type == 1) {
            olist.add(new BasicNameValuePair("name", "HouseSell"));
        }
        olist.add(new BasicNameValuePair("method", "secondhouse.getHouseDetail"));
        olist.add(new BasicNameValuePair("id", rowid));
        olist.add(new BasicNameValuePair("app", app));
        addCommonParams(olist);
        return (SecondHouse) getWithObject(this.baseUrl, olist, new SecondHouse());
    }

    public List<SecondHouse> getSellHouseList(String app, Location location, int radius, String keyword, int infofromValue, int infotypeValue, int districtValue, int streetValue, int priceValue, int buildareaValue, int roomValue, int hallValue, int toiletValue, int page, int order, String metroValue, String blockId) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        return getHouseList(app, location, radius, keyword, infofromValue, infotypeValue, districtValue, streetValue, priceValue, buildareaValue, roomValue, hallValue, toiletValue, 0, 0, page, order, 0, metroValue, blockId);
    }

    public List<SecondHouse> getHouseList(String app, Location location, int radius, String keyword, int infofromValue, int infotypeValue, int districtValue, int streetValue, int priceValue, int buildareaValue, int roomValue, int hallValue, int toiletValue, int fitmentValue, int rentTypeValue, int page, int order, int busValue, String metroValue, String blockId) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseList", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "secondhouse.getHouseList"));
        if (app.equals(App.SELL)) {
            olist.add(new BasicNameValuePair("name", "HouseSell"));
            olist.add(new BasicNameValuePair("buildarea", new StringBuilder(String.valueOf(buildareaValue)).toString()));
        } else if (app.equals(App.RENT)) {
            olist.add(new BasicNameValuePair("name", "HouseRent"));
            olist.add(new BasicNameValuePair(HouseBaseInfo.RENTTYPE, new StringBuilder(String.valueOf(rentTypeValue)).toString()));
            olist.add(new BasicNameValuePair("bus", new StringBuilder(String.valueOf(busValue)).toString()));
        }
        if (location != null && radius > 0) {
            double[][] points = MapUtil.GetTwoPointLocation(location.getLatitude(), location.getLongitude(), radius);
            olist.add(new BasicNameValuePair("leftcoord", String.valueOf(points[0][0]) + "," + points[0][1]));
            olist.add(new BasicNameValuePair("rightcoord", String.valueOf(points[1][0]) + "," + points[1][1]));
        }
        olist.add(new BasicNameValuePair(HouseBaseInfo.INFOFROM, new StringBuilder(String.valueOf(infofromValue)).toString()));
        olist.add(new BasicNameValuePair(HouseBaseInfo.INFOTYPE, new StringBuilder(String.valueOf(infotypeValue)).toString()));
        olist.add(new BasicNameValuePair(HouseBaseInfo.DISTRICT, new StringBuilder(String.valueOf(districtValue)).toString()));
        olist.add(new BasicNameValuePair("streetid", new StringBuilder(String.valueOf(streetValue)).toString()));
        olist.add(new BasicNameValuePair("price", new StringBuilder(String.valueOf(priceValue)).toString()));
        olist.add(new BasicNameValuePair(HouseBaseInfo.ROOM, new StringBuilder(String.valueOf(roomValue)).toString()));
        olist.add(new BasicNameValuePair("hall", new StringBuilder(String.valueOf(hallValue)).toString()));
        olist.add(new BasicNameValuePair("toilet", new StringBuilder(String.valueOf(toiletValue)).toString()));
        olist.add(new BasicNameValuePair("order", Order.getOrderBy(order)));
        olist.add(new BasicNameValuePair("desc", Order.getAsc(order)));
        olist.add(new BasicNameValuePair(HouseBaseInfo.FITMENT, new StringBuilder(String.valueOf(fitmentValue)).toString()));
        if (!TextUtils.isEmpty(metroValue)) {
            olist.add(new BasicNameValuePair(HouseBaseInfo.METRO, metroValue));
        }
        if (!TextUtils.isEmpty(blockId)) {
            olist.add(new BasicNameValuePair("blockid", blockId));
        }
        if (!TextUtils.isEmpty(keyword)) {
            olist.add(new BasicNameValuePair("keyword", keyword));
        }
        olist.add(new BasicNameValuePair("page", new StringBuilder(String.valueOf(page)).toString()));
        addPageInfo(olist);
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new SecondHouse());
    }

    public CommonResultInfo publishHouse(Map<String, String> publishVal) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        List<NameValuePair> olist = new ArrayList<>();
        if (publishVal != null) {
            for (Map.Entry<String, String> entry : publishVal.entrySet()) {
                olist.add(new BasicNameValuePair((String) entry.getKey(), (String) entry.getValue()));
            }
        }
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public CommonResultInfo OfferHouse(Map<String, String> publishVal, int type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        List<NameValuePair> olist = new ArrayList<>();
        if (type == 1) {
            olist.add(new BasicNameValuePair("method", "saveSellHouse"));
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "saveSellHouse", "secondhouse");
        } else if (type == 2) {
            olist.add(new BasicNameValuePair("method", "saveRentHouse"));
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "saveRentHouse", "secondhouse");
        }
        olist.add(new BasicNameValuePair("name", "HousePerson"));
        if (publishVal != null) {
            for (Map.Entry<String, String> entry : publishVal.entrySet()) {
                if (!TextUtils.isEmpty((CharSequence) entry.getValue())) {
                    olist.add(new BasicNameValuePair((String) entry.getKey(), (String) entry.getValue()));
                }
            }
        }
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.secondUrl, olist, new CommonResultInfo());
    }

    public CommonResultInfo getValidate(int type, String moblie) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "arrGetValidate", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("name", "HousePerson"));
        olist.add(new BasicNameValuePair("method", "secondhouse.arrGetValidate"));
        olist.add(new BasicNameValuePair("telno", moblie));
        if (4 == type) {
            olist.add(new BasicNameValuePair(CouponDetailsActivity.INTENT_TYPE, App.SELL));
        } else {
            olist.add(new BasicNameValuePair(CouponDetailsActivity.INTENT_TYPE, App.RENT));
        }
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public Block getBlockByid(int app, String id, String blockId) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getBlockDetail", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        if (app == 2 || app == 1) {
            olist.add(new BasicNameValuePair("id", blockId));
        } else {
            olist.add(new BasicNameValuePair("id", id));
        }
        CorePreferences.DEBUG("************app********" + app);
        CorePreferences.DEBUG("*********blockId*******" + blockId);
        olist.add(new BasicNameValuePair("name", "HouseBlock"));
        olist.add(new BasicNameValuePair("method", "secondhouse.getBlockDetail"));
        addCommonParams(olist);
        return (Block) getWithObject(this.baseUrl, olist, new Block());
    }

    public CommonResultInfo ask(int type, String id, String telno, String content) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "addHouseQuestion", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        if (type == 1) {
            olist.add(new BasicNameValuePair("name", "HouseSell"));
        } else {
            olist.add(new BasicNameValuePair("name", "HouseRent"));
        }
        olist.add(new BasicNameValuePair("method", "secondhouse.addHouseQuestion"));
        olist.add(new BasicNameValuePair("id", id));
        olist.add(new BasicNameValuePair("telno", telno));
        olist.add(new BasicNameValuePair("content", content));
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public List<SecondHouse> getSecondBusBySearch(String k, String search_type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getLegendBusLineList", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("name", "HouseRent"));
        olist.add(new BasicNameValuePair("method", "secondhouse.getLegendBusLineList"));
        if (!TextUtils.isEmpty(k)) {
            olist.add(new BasicNameValuePair("keyword", k));
        }
        olist.add(new BasicNameValuePair("page", "1"));
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new SecondHouse());
    }

    public List<SecondHouse> getBusConfigInfo(String id) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getBusLineDetail", "secondhouse");
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("method", "secondhouse.getBusLineDetail"));
        list.add(new BasicNameValuePair("name", "HouseRent"));
        list.add(new BasicNameValuePair("id", id));
        addCommonParams(list);
        return getWithList(this.baseUrl, list, new SecondHouse());
    }

    public List<House> getNewHouseOfMap(double x1, double y1, double x2, double y2, String price, String channel) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseList", App.NEW);
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            olist.add(new BasicNameValuePair("method", "getHouseList"));
        } else {
            olist.add(new BasicNameValuePair("method", "newhouse.getHouseList"));
        }
        olist.add(new BasicNameValuePair("leftcoord", String.valueOf(x1) + "," + y1));
        olist.add(new BasicNameValuePair("rightcoord", String.valueOf(x2) + "," + y2));
        if (!"0".equals(channel) && !TextUtils.isEmpty(channel)) {
            olist.add(new BasicNameValuePair("p", channel));
        }
        if (!"0".equals(price) && !TextUtils.isEmpty(price)) {
            olist.add(new BasicNameValuePair("price", price));
        }
        addCommonParams(olist);
        olist.add(new BasicNameValuePair("pagesize", "20"));
        if (isVirtual) {
            return getWithList(this.virtualUrl, olist, new House());
        }
        return getWithList(this.baseUrl, olist, new House());
    }

    public List<Block> getBlockOfMap(String app, double x1, double y1, double x2, double y2, String price, String resource, String areaOrRent) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getMapBlockList", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "secondhouse.getMapBlockList"));
        olist.add(new BasicNameValuePair("name", "HouseBlock"));
        olist.add(new BasicNameValuePair("leftcoord", String.valueOf(x1) + "," + y1));
        olist.add(new BasicNameValuePair("rightcoord", String.valueOf(x2) + "," + y2));
        olist.add(new BasicNameValuePair("tbl", app));
        if (!TextUtils.isEmpty(resource)) {
            olist.add(new BasicNameValuePair(HouseBaseInfo.INFOFROM, new StringBuilder(String.valueOf(resource)).toString()));
        }
        if (!TextUtils.isEmpty(price)) {
            olist.add(new BasicNameValuePair("price", new StringBuilder(String.valueOf(price)).toString()));
        }
        if (!TextUtils.isEmpty(areaOrRent)) {
            if (app.equals(App.SELL)) {
                olist.add(new BasicNameValuePair("buildarea", new StringBuilder(String.valueOf(areaOrRent)).toString()));
            } else if (app.equals(App.RENT)) {
                olist.add(new BasicNameValuePair(HouseBaseInfo.RENTTYPE, new StringBuilder(String.valueOf(areaOrRent)).toString()));
            }
        }
        addCommonParams(olist);
        olist.add(new BasicNameValuePair("pagesize", "20"));
        return getWithList(this.baseUrl, olist, new Block());
    }

    public CommonResultInfo getDefaultCity() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getDefaultCity", "user");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "user.getDefaultCity"));
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo(), 2000);
    }

    public CommonResultInfo sendFeedback(String feedback, String mobile) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "feedBack", "user");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "user.feedBack"));
        olist.add(new BasicNameValuePair("feedBack", feedback));
        olist.add(new BasicNameValuePair("phone", mobile));
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public CommonResultInfo getValidateCode(String mobile) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "sendAuthCode", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "sendAuthCode");
        } else {
            params.put("method", "user.sendAuthCode");
        }
        params.put("phone", mobile);
        addCommonParams(params);
        if (isVirtual) {
            return (CommonResultInfo) getWithObject(this.virtualUrl, params, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.baseUrl, params, new CommonResultInfo());
    }

    public CommonResultInfo validateMobile(String mobile, String code) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "authPhone", "user");
        Map<String, String> params = new HashMap<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            params.put("method", "authPhone");
        } else {
            params.put("method", "user.authPhone");
        }
        params.put("phone", mobile);
        params.put(ThirdPartAuthActivity.RESPONSE_TYPE_SERVER_SIDE, code);
        addCommonParams(params);
        if (isVirtual) {
            return (CommonResultInfo) getWithObject(this.virtualUrl, params, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.baseUrl, params, new CommonResultInfo());
    }

    public CommonResultInfo getCoordConvertCity(double latitude, double longitude) throws HtppApiException, NetworkUnavailableException, JSONException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "coordConverttoCity", "user");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "user.coordConverttoCity"));
        olist.add(new BasicNameValuePair("lat", new StringBuilder().append(latitude).toString()));
        olist.add(new BasicNameValuePair("lng", new StringBuilder().append(longitude).toString()));
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public List<Ad> getAdList(int position) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getAdListByPosition", "user");
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            olist.add(new BasicNameValuePair("method", "getAdListByPosition"));
        } else {
            olist.add(new BasicNameValuePair("method", "user.getAdListByPosition"));
        }
        olist.add(new BasicNameValuePair("position", new StringBuilder(String.valueOf(position)).toString()));
        addCommonParams(olist);
        if (isVirtual) {
            return getWithList(this.virtualUrl, olist, new Ad());
        }
        return getWithList(this.baseUrl, olist, new Ad());
    }

    public ShakeInfo getShakeInfo() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getSimpleTag", "user");
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (isVirtual) {
            olist.add(new BasicNameValuePair("method", "getSimpleTag"));
        } else {
            olist.add(new BasicNameValuePair("method", "user.getSimpleTag"));
        }
        olist.add(new BasicNameValuePair(CouponDetailsActivity.INTENT_TYPE, App.NEW));
        addCommonParams(olist);
        if (isVirtual) {
            return (ShakeInfo) getWithObject(this.virtualUrl, olist, new ShakeInfo());
        }
        return (ShakeInfo) getWithObject(this.baseUrl, olist, new ShakeInfo());
    }

    public List<News> getNews(int channel, int page) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getNewsList", App.Categroy.News.NEWS);
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (!isVirtual) {
            olist.add(new BasicNameValuePair("method", "news.getNewsList"));
        } else {
            olist.add(new BasicNameValuePair("method", "getNewsList"));
        }
        olist.add(new BasicNameValuePair("channel", new StringBuilder(String.valueOf(channel)).toString()));
        olist.add(new BasicNameValuePair("page", new StringBuilder(String.valueOf(page)).toString()));
        addCommonParams(olist);
        addPageInfo(olist);
        if (isVirtual) {
            return getWithList(this.virtualUrl, olist, new News());
        }
        return getWithList(this.baseUrl, olist, new News());
    }

    public News getNewsById(String id, int w) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getNewsDetail", App.Categroy.News.NEWS);
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (!isVirtual) {
            olist.add(new BasicNameValuePair("method", "news.getNewsDetail"));
        } else {
            olist.add(new BasicNameValuePair("method", "getNewsDetail"));
        }
        olist.add(new BasicNameValuePair("id", new StringBuilder(String.valueOf(id)).toString()));
        olist.add(new BasicNameValuePair("w", new StringBuilder(String.valueOf(w)).toString()));
        addCommonParams(olist);
        if (isVirtual) {
            return (News) getWithObject(this.virtualUrl, olist, new News());
        }
        return (News) getWithObject(this.baseUrl, olist, new News());
    }

    public CommonResultInfo deletePublish(int type, String id) throws HtppApiException, JSONException, NetworkUnavailableException, HttpParseException {
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("name", "HousePerson"));
        if (4 == type) {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "delBuyHouse", "secondhouse");
            olist.add(new BasicNameValuePair("method", "secondhouse.delBuyHouse"));
        } else {
            HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "delRentWantedHouse", "secondhouse");
            olist.add(new BasicNameValuePair("method", "secondhouse.delRentWantedHouse"));
        }
        olist.add(new BasicNameValuePair("id", id));
        addCommonParams(olist);
        return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
    }

    public CommonResultInfo updateUserInfo(String last_visit) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "updateUserInfo", "user");
        List<NameValuePair> olist = new ArrayList<>();
        boolean isVirtual = AppMethods.isVirtual(this.mApplication.getCity());
        if (!isVirtual) {
            olist.add(new BasicNameValuePair("method", "user.updateUserInfo"));
        } else {
            olist.add(new BasicNameValuePair("method", "updateUserInfo"));
        }
        olist.add(new BasicNameValuePair("last_visit", last_visit));
        addCommonParams(olist);
        if (!isVirtual) {
            return (CommonResultInfo) getWithObject(this.baseUrl, olist, new CommonResultInfo());
        }
        return (CommonResultInfo) getWithObject(this.virtualUrl, olist, new CommonResultInfo());
    }

    public CommonResultInfo uploadPic(File file) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "uploadPic", "uploadPic");
        try {
            MultipartEntity entity = new MultipartEntity();
            try {
                entity.addPart("name", new StringBody("HousePerson", Charset.forName(this.CHARENCODE)));
                entity.addPart("method", new StringBody("uploadPic", Charset.forName(this.CHARENCODE)));
                entity.addPart("uploadname", new StringBody("Filedata", Charset.forName(this.CHARENCODE)));
                entity.addPart("Filedata", new FileBody(file));
                addCommonParams(entity);
                return (CommonResultInfo) postWithObject(this.secondUrl, entity, new CommonResultInfo());
            } catch (UnsupportedEncodingException e) {
                e = e;
                throw new HtppApiException(e);
            }
        } catch (UnsupportedEncodingException e2) {
            e = e2;
            throw new HtppApiException(e);
        }
    }

    public List<VirtualCity> getVirtualCity() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getCityList", StringUtils.EMPTY);
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "getCityList"));
        addCommonParams(olist);
        return getWithList(this.virtualUrl, olist, new VirtualCity());
    }

    public List<RegionMap> getNewHouseAreaCount() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getAreaHouseCount", StringUtils.EMPTY);
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "newhouse.getAreaHouseCount"));
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new RegionMap());
    }

    public List<RegionMap> getBlockAreaCount() throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getDistrictAllBlockNum", StringUtils.EMPTY);
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "getDistrictAllBlockNum"));
        olist.add(new BasicNameValuePair("name", "HouseBlock"));
        olist.add(new BasicNameValuePair("city", this.mApplication.getCityKey()));
        return getWithList(this.secondUrl, olist, new RegionMap());
    }

    public List<SecondHouse> getPersonHouseList(int app) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getPersonHouseList", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "secondhouse.getPersonHouseList"));
        if (app == 2) {
            olist.add(new BasicNameValuePair("name", "HouseRent"));
        } else if (app == 1) {
            olist.add(new BasicNameValuePair("name", "HouseSell"));
        }
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new SecondHouse());
    }

    public List<House> getHouseLineDetail(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseLineDetail", App.NEW);
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "user.getHouseLineDetail"));
        olist.add(new BasicNameValuePair("id", id));
        olist.add(new BasicNameValuePair(CouponDetailsActivity.INTENT_TYPE, type));
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new House());
    }

    public List<Block> getBlockLineDetail(String id, String type) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        HouseAnalyse.onCallAPI(this.mApplication.getApplicationContext(), "getHouseLineDetail", "secondhouse");
        List<NameValuePair> olist = new ArrayList<>();
        olist.add(new BasicNameValuePair("method", "user.getHouseLineDetail"));
        olist.add(new BasicNameValuePair("id", id));
        olist.add(new BasicNameValuePair(CouponDetailsActivity.INTENT_TYPE, type));
        addCommonParams(olist);
        return getWithList(this.baseUrl, olist, new Block());
    }
}
