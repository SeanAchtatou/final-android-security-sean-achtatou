package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class EventNum extends BaseBean {
    private static final long serialVersionUID = 1206914119127136357L;
    private int coupon;
    private int event;
    private int tjf;
    private int tlf;

    public int getTlf() {
        return this.tlf;
    }

    public void setTlf(int tlf2) {
        this.tlf = tlf2;
    }

    public int getTjf() {
        return this.tjf;
    }

    public void setTjf(int tjf2) {
        this.tjf = tjf2;
    }

    public int getEvent() {
        return this.event;
    }

    public void setEvent(int event2) {
        this.event = event2;
    }

    public int getCoupon() {
        return this.coupon;
    }

    public void setCoupon(int coupon2) {
        this.coupon = coupon2;
    }
}
