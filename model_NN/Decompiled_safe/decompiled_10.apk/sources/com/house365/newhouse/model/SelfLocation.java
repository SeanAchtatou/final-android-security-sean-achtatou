package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class SelfLocation extends BaseBean {
    private static final long serialVersionUID = -5891270181606619884L;
    private double lat;
    private double lng;

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat2) {
        this.lat = lat2;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng2) {
        this.lng = lng2;
    }
}
