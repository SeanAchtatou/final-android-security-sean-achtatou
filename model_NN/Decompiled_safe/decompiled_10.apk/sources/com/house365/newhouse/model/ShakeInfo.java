package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class ShakeInfo extends BaseBean {
    private Event event;
    private House house;
    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public House getHouse() {
        return this.house;
    }

    public void setHouse(House house2) {
        this.house = house2;
    }

    public Event getEvent() {
        return this.event;
    }

    public void setEvent(Event event2) {
        this.event = event2;
    }
}
