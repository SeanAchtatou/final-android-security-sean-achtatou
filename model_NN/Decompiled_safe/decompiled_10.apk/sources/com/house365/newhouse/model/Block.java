package com.house365.newhouse.model;

import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class Block extends SharedPreferencesDTO<Block> {
    private String address;
    private int b_fav;
    private String b_other;
    private String blockname;
    private String company;
    private String developers;
    private String district;
    private String fees;
    private String id;
    private String introduction;
    private double lat;
    private double lng;
    private String pic;
    private String pics;
    private long recordtime;
    private String rentcount;
    private String sellaverprice;
    private String sellcount;
    private String streetname;
    private String type;

    public String getPics() {
        if (this.pics == null || (this.pics.indexOf(",") == -1 && this.pics.indexOf("[") == -1)) {
            return this.pics;
        }
        try {
            return new JSONArray(this.pics).getString(0);
        } catch (JSONException e) {
            return StringUtils.EMPTY;
        }
    }

    public List<String> getH_picList() {
        if (this.pics == null || (this.pics.indexOf(",") == -1 && this.pics.indexOf("[") == -1)) {
            return null;
        }
        return setPicList(this.pics);
    }

    public List<String> setPicList(String json) {
        List<String> tlist = new ArrayList<>();
        try {
            JSONArray ja = new JSONArray(json);
            for (int i = 0; i < ja.length(); i++) {
                tlist.add(ja.getString(i).toString());
            }
        } catch (JSONException e) {
        }
        return tlist;
    }

    public String setPics(String pics2) {
        return pics2;
    }

    public int getB_fav() {
        return this.b_fav;
    }

    public void setB_fav(int b_fav2) {
        this.b_fav = b_fav2;
    }

    public String getB_other() {
        return this.b_other;
    }

    public void setB_other(String b_other2) {
        this.b_other = b_other2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLng(double lng2) {
        this.lng = lng2;
    }

    public double getLat() {
        return this.lat;
    }

    public void setLat(double lat2) {
        this.lat = lat2;
    }

    public String getBlockname() {
        return this.blockname;
    }

    public void setBlockname(String blockname2) {
        this.blockname = blockname2;
    }

    public String getSellcount() {
        return this.sellcount;
    }

    public void setSellcount(String sellcount2) {
        this.sellcount = sellcount2;
    }

    public String getPic() {
        return this.pic;
    }

    public void setPic(String pic2) {
        this.pic = pic2;
    }

    public String getRentcount() {
        return this.rentcount;
    }

    public void setRentcount(String rentcount2) {
        this.rentcount = rentcount2;
    }

    public String getSellaverprice() {
        return this.sellaverprice;
    }

    public void setSellaverprice(String sellaverprice2) {
        this.sellaverprice = sellaverprice2;
    }

    public String getIntroduction() {
        return this.introduction;
    }

    public void setIntroduction(String introduction2) {
        this.introduction = introduction2;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district2) {
        this.district = district2;
    }

    public String getStreetname() {
        return this.streetname;
    }

    public void setStreetname(String streetname2) {
        this.streetname = streetname2;
    }

    public boolean isSame(Block o) {
        return o.getId().equals(getId());
    }

    public String getDevelopers() {
        return this.developers;
    }

    public void setDevelopers(String developers2) {
        this.developers = developers2;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company2) {
        this.company = company2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getFees() {
        return this.fees;
    }

    public void setFees(String fees2) {
        this.fees = fees2;
    }

    public long getRecordtime() {
        return this.recordtime;
    }

    public void setRecordtime(long recordtime2) {
        this.recordtime = recordtime2;
    }

    public void resetRecordtime() {
        setRecordtime(new Date().getTime() / 1000);
    }
}
