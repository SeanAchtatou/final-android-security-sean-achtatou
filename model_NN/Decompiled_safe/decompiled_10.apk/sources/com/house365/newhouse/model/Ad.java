package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class Ad extends BaseBean {
    private String a_aboutid;
    private String a_abouttype;
    private String a_href;
    private String a_id;
    private String a_src;
    private String a_title;

    public String getA_id() {
        return this.a_id;
    }

    public void setA_id(String a_id2) {
        this.a_id = a_id2;
    }

    public String getA_title() {
        return this.a_title;
    }

    public void setA_title(String a_title2) {
        this.a_title = a_title2;
    }

    public String getA_src() {
        return this.a_src;
    }

    public void setA_src(String a_src2) {
        this.a_src = a_src2;
    }

    public String getA_abouttype() {
        return this.a_abouttype;
    }

    public void setA_abouttype(String a_abouttype2) {
        this.a_abouttype = a_abouttype2;
    }

    public String getA_aboutid() {
        return this.a_aboutid;
    }

    public void setA_aboutid(String a_aboutid2) {
        this.a_aboutid = a_aboutid2;
    }

    public String getA_href() {
        return this.a_href;
    }

    public void setA_href(String a_href2) {
        this.a_href = a_href2;
    }
}
