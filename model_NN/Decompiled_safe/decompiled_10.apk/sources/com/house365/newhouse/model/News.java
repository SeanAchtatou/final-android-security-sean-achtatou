package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.List;

public class News extends SharedPreferencesDTO<News> {
    private List<String> images;
    private List<String> images_small;
    private long n_addtime;
    private String n_author;
    private String n_content;
    private String n_from;
    private String n_hits;
    private String n_id;
    private String n_pic;
    private String n_source;
    private String n_summary;
    private String n_title;

    public String getN_id() {
        return this.n_id;
    }

    public void setN_id(String n_id2) {
        this.n_id = n_id2;
    }

    public String getN_title() {
        return this.n_title;
    }

    public void setN_title(String n_title2) {
        this.n_title = n_title2;
    }

    public String getN_hits() {
        return this.n_hits;
    }

    public void setN_hits(String n_hits2) {
        this.n_hits = n_hits2;
    }

    public String getN_pic() {
        return this.n_pic;
    }

    public void setN_pic(String n_pic2) {
        this.n_pic = n_pic2;
    }

    public String getN_author() {
        return this.n_author;
    }

    public void setN_author(String n_author2) {
        this.n_author = n_author2;
    }

    public String getN_source() {
        return this.n_source;
    }

    public void setN_source(String n_source2) {
        this.n_source = n_source2;
    }

    public String getN_summary() {
        return this.n_summary;
    }

    public void setN_summary(String n_summary2) {
        this.n_summary = n_summary2;
    }

    public long getN_addtime() {
        return this.n_addtime;
    }

    public void setN_addtime(long n_addtime2) {
        this.n_addtime = n_addtime2;
    }

    public String getN_content() {
        return this.n_content;
    }

    public void setN_content(String n_content2) {
        this.n_content = n_content2;
    }

    public String getN_from() {
        return this.n_from;
    }

    public void setN_from(String n_from2) {
        this.n_from = n_from2;
    }

    public boolean isSame(News o) {
        return o.getN_id().equals(getN_id());
    }

    public List<String> getImages() {
        return this.images;
    }

    public void setImages(List<String> images2) {
        this.images = images2;
    }

    public List<String> getImages_small() {
        return this.images_small;
    }

    public void setImages_small(List<String> images_small2) {
        this.images_small = images_small2;
    }
}
