package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class SaleInfo extends BaseBean {
    private static final long serialVersionUID = 579267256238068654L;
    private long addtime;
    private String summary;

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary2) {
        this.summary = summary2;
    }

    public long getAddtime() {
        return this.addtime;
    }

    public void setAddtime(long addtime2) {
        this.addtime = addtime2;
    }
}
