package com.house365.newhouse.model;

import java.io.Serializable;
import java.util.List;

public class Album implements Serializable {
    private static final long serialVersionUID = 268125439397813532L;
    private int a_count;
    private String a_name;
    private List<Photo> a_photos;
    private String a_thumb;

    public String getA_name() {
        return this.a_name;
    }

    public void setA_name(String a_name2) {
        this.a_name = a_name2;
    }

    public int getA_count() {
        return this.a_count;
    }

    public void setA_count(int a_count2) {
        this.a_count = a_count2;
    }

    public String getA_thumb() {
        return this.a_thumb;
    }

    public void setA_thumb(String a_thumb2) {
        this.a_thumb = a_thumb2;
    }

    public List<Photo> getA_photos() {
        return this.a_photos;
    }

    public void setA_photos(List<Photo> a_photos2) {
        this.a_photos = a_photos2;
    }
}
