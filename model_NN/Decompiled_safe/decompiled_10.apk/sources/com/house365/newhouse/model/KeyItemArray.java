package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class KeyItemArray extends SharedPreferencesDTO<KeyItemArray> {
    private static final long serialVersionUID = 968265836389017986L;
    private String array;

    public KeyItemArray(String array2) {
        this.array = array2;
    }

    public KeyItemArray() {
    }

    public String getArray() {
        return this.array;
    }

    public void setArray(String array2) {
        this.array = array2;
    }

    public boolean isSame(KeyItemArray o) {
        return getArray().equals(o.getArray());
    }
}
