package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;
import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.json.JSONObject;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.tool.ActionCode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class HouseBaseInfo extends BaseBean {
    public static final String BLOCK = "block";
    public static final String BUILDERAREA = "buildarea";
    public static final String BUILDERAREA_DEFAULT = "buildarea_default";
    public static final String BUILDERAREA_OFFICE = "buildarea_office";
    public static final String BUILDERAREA_STORE = "buildarea_store";
    public static final String CHANNEL = "channel";
    public static final String COORDINATE = "districtCoordinate";
    public static final String DISTRICT = "district";
    public static final String DIST_XY = "dist_xy";
    public static final String FINDROOM = "findroom";
    public static final String FITMENT = "fitment";
    public static final String INFOFROM = "infofrom";
    public static final String INFOTYPE = "infotype";
    public static final String METRO = "metro";
    public static final String PERSON_PRICEUNIT = "person_priceunit";
    public static final String PRICE = "price";
    public static final String PRICE_DEFAULT = "price_default";
    public static final String PRICE_OFFICE = "price_office";
    public static final String PRICE_STORE = "price_store";
    public static final String RENTTYPE = "renttype";
    public static final String ROOM = "room";
    public static final String STREET = "street";
    public static final String TAG = "tag";
    private HashMap<String, LinkedHashMap<String, String>> config;
    private LinkedHashMap<String, Station> coordinate;
    private LinkedHashMap<String, Station> distXY;
    private LinkedHashMap<String, LinkedHashMap<String, String>> districtStreet;
    private String jsonStr;
    private LinkedHashMap<String, LinkedHashMap<String, Station>> metro;
    private LinkedHashMap<String, LinkedHashMap<String, String>> personPriceUnit;
    private HashMap<String, ArrayList> sell_config;
    private LinkedHashMap<String, String> tagMap;

    private void parseJsonToMap(JSONObject json, Map<String, String> map) throws JSONException {
        for (String name : JSONObject.getNames(json)) {
            if (json.getString(name) == null) {
                map.put(name, StringUtils.EMPTY);
            }
            map.put(name, json.getString(name));
        }
    }

    private void parseJsonToMapValueInt(JSONObject json, Map<String, String> map) throws JSONException {
        for (String name : JSONObject.getNames(json)) {
            if (json.getString(name) == null) {
                map.put(name, StringUtils.EMPTY);
            }
            map.put(json.getString(name), name);
        }
    }

    private void parseJsonToStreetItem(JSONObject json, LinkedHashMap<String, LinkedHashMap<String, String>> data) throws JSONException {
        LinkedHashMap<String, String> region_map = new LinkedHashMap<>();
        parseJsonToMap(json, region_map);
        for (String street_name : region_map.values()) {
            JSONObject street_item_json = new JSONObject(street_name);
            LinkedHashMap<String, String> districtPlateTmp = new LinkedHashMap<>();
            if (!street_name.equals("{}")) {
                parseJsonToMapValueInt(street_item_json, districtPlateTmp);
            }
            for (Map.Entry entry : region_map.entrySet()) {
                if (entry.getValue().equals(street_name)) {
                    data.put(entry.getKey().toString(), districtPlateTmp);
                }
            }
        }
    }

    private ArrayList<String> parseJsonToArray(JSONArray jsonArray, ArrayList<String> arrayList) throws JSONException {
        ArrayList<String> arr = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            arr.add(i, jsonArray.getString(i));
        }
        return arr;
    }

    public String getJsonStr() {
        return this.jsonStr;
    }

    public void setJsonStr(String jsonStr2) {
        this.jsonStr = jsonStr2;
    }

    public HouseBaseInfo() {
    }

    public HouseBaseInfo(String s) throws JSONException {
        this.jsonStr = s;
        this.config = new HashMap<>();
        this.sell_config = new HashMap<>();
        this.metro = new LinkedHashMap<>();
        this.tagMap = new LinkedHashMap<>();
        this.districtStreet = new LinkedHashMap<>();
        this.personPriceUnit = new LinkedHashMap<>();
        this.distXY = new LinkedHashMap<>();
        this.coordinate = new LinkedHashMap<>();
        this.sell_config.put(ROOM, new ArrayList(Arrays.asList(AppArrays.getRoom())));
        JSONObject json = new JSONObject(s);
        if (json.has("channel")) {
            parseInfoBean(json, "channel");
        }
        if (json.has("block")) {
            parseInfoBean(json, "block");
        }
        if (json.has(INFOTYPE)) {
            parseInfoBean(json, INFOTYPE);
        }
        if (json.has(DISTRICT)) {
            parseInfoBean(json, DISTRICT);
        }
        if (json.has(INFOFROM)) {
            parseInfoBean(json, INFOFROM);
        }
        if (json.has(FITMENT)) {
            parseInfoBean(json, FITMENT);
        }
        if (json.has(ROOM)) {
            parseInfoBean(json, ROOM);
        }
        if (json.has("price")) {
            parseInfoBean(json, "price");
        }
        if (json.has("buildarea")) {
            parseInfoBean(json, "buildarea");
        }
        if (json.has(FINDROOM)) {
            parseInfoBean(json, FINDROOM);
        }
        if (json.has(RENTTYPE)) {
            parseInfoBean(json, RENTTYPE);
        }
        if (json.has(PRICE_DEFAULT)) {
            parseInfoBean(json, PRICE_DEFAULT);
        }
        if (json.has(PRICE_OFFICE)) {
            parseInfoBean(json, PRICE_OFFICE);
        }
        if (json.has(PRICE_STORE)) {
            parseInfoBean(json, PRICE_STORE);
        }
        if (json.has(BUILDERAREA_DEFAULT)) {
            parseInfoBean(json, BUILDERAREA_DEFAULT);
        }
        if (json.has(BUILDERAREA_OFFICE)) {
            parseInfoBean(json, BUILDERAREA_OFFICE);
        }
        if (json.has(BUILDERAREA_STORE)) {
            parseInfoBean(json, BUILDERAREA_STORE);
        }
        if (json.has(STREET)) {
            parseJsonToStreetItem(json.getJSONObject(STREET), this.districtStreet);
        }
        if (json.has(PERSON_PRICEUNIT)) {
            parseJsonToStreetItem(json.getJSONObject(PERSON_PRICEUNIT), this.personPriceUnit);
        }
        if (json.has(TAG)) {
            JSONObject ja = new JSONObject(json.get(TAG).toString());
            Iterator iter = ja.keys();
            while (iter.hasNext()) {
                String key = iter.next().toString();
                this.tagMap.put(ja.getString(key), key);
            }
            this.tagMap.put(ActionCode.PREFERENCE_SEARCH_NOCHOSE, "0");
            this.config.put(TAG, this.tagMap);
        }
        if (json.has(DIST_XY)) {
            JSONArray ja2 = json.getJSONArray(DIST_XY);
            int i = 0;
            while (i < ja2.length()) {
                try {
                    Station station = new Station(ja2.getJSONObject(i));
                    if (station != null) {
                        this.distXY.put(station.getId(), station);
                    }
                    i++;
                } catch (JSONException e) {
                    this.distXY = null;
                }
            }
        }
        if (json.has(COORDINATE)) {
            try {
                JSONObject jo = new JSONObject(json.getString(COORDINATE));
                Iterator it = jo.keys();
                while (it.hasNext()) {
                    String key2 = it.next().toString();
                    this.coordinate.put(key2, new Station(jo.getString(key2)));
                }
                this.coordinate.put(ActionCode.PREFERENCE_SEARCH_NOCHOSE, null);
            } catch (JSONException e2) {
                this.coordinate = null;
            }
        }
        if (json.has(METRO)) {
            try {
                JSONArray ja3 = json.getJSONArray(METRO);
                for (int i2 = 0; i2 < ja3.length(); i2++) {
                    LinkedHashMap<String, Station> map = new LinkedHashMap<>();
                    JSONObject jo2 = ja3.getJSONObject(i2);
                    String name = jo2.getString("name");
                    JSONArray ja1 = jo2.getJSONArray("station");
                    for (int j = 0; j < ja1.length(); j++) {
                        JSONObject jo1 = ja1.getJSONObject(j);
                        map.put(jo1.getString("name"), new Station(jo1));
                    }
                    this.metro.put(name, map);
                }
            } catch (JSONException e3) {
                this.metro = null;
            }
        }
    }

    public static String parseValueToKey(Map<String, String> map, String value) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (((String) entry.getValue()).equals(value)) {
                return (String) entry.getKey();
            }
        }
        return null;
    }

    private void parseInfoBean(JSONObject json, String jsonkey) throws JSONException {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        JSONArray ja = json.getJSONArray(jsonkey);
        int i = 0;
        while (i < ja.length()) {
            try {
                JSONObject jo = ja.getJSONObject(i);
                Iterator it = jo.keys();
                while (it.hasNext()) {
                    String key = it.next().toString();
                    map.put(jo.getString(key), key);
                }
                i++;
            } catch (JSONException e) {
                this.sell_config.put(jsonkey, parseJsonToArray(ja, new ArrayList()));
            }
        }
        map.put(ActionCode.PREFERENCE_SEARCH_NOCHOSE, "0");
        this.config.put(jsonkey, map);
    }

    public HashMap<String, LinkedHashMap<String, String>> getConfig() {
        return this.config;
    }

    public LinkedHashMap<String, LinkedHashMap<String, Station>> getMetro() {
        return this.metro;
    }

    public LinkedHashMap<String, LinkedHashMap<String, String>> getDistrictStreet() {
        return this.districtStreet;
    }

    public LinkedHashMap<String, LinkedHashMap<String, String>> getPersonPriceUnit() {
        return this.personPriceUnit;
    }

    public HashMap<String, ArrayList> getSell_config() {
        return this.sell_config;
    }

    public LinkedHashMap<String, Station> getDistXY() {
        return this.distXY;
    }

    public LinkedHashMap<String, Station> getCoordinate() {
        return this.coordinate;
    }
}
