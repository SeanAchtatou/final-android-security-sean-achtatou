package com.house365.newhouse.model;

import com.house365.core.util.TimeUtil;
import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class PublishHouse extends SharedPreferencesDTO<PublishHouse> {
    private static final long serialVersionUID = 1262383458935133572L;
    private String blockname;
    private String buildarea;
    private String contactor;
    private String createdata;
    private int createtime;
    private String district;
    private String findroom;
    private String fitment;
    private String floormsg;
    private String id;
    private int infoTypeId;
    private String infotype;
    private List<String> pics;
    private String price;
    private String priceunit;
    private String remark;
    private String renttype;
    private String room;
    private String telno;
    private String title;

    public PublishHouse() {
    }

    public PublishHouse(String id2, String title2, String remark2, String contactor2, String telno2, String infotype2, String district2, String price2, String room2, String findroom2, String buildarea2) {
        this.id = id2;
        this.title = title2;
        this.remark = remark2;
        this.contactor = contactor2;
        this.telno = telno2;
        this.infotype = infotype2;
        this.district = district2;
        this.price = price2;
        this.room = room2;
        this.findroom = findroom2;
        this.buildarea = buildarea2;
    }

    public PublishHouse(String title2, String remark2, String contactor2, String telno2, String infotype2, String district2, String price2, String room2, String renttype2, String buildarea2) {
        this.title = title2;
        this.remark = remark2;
        this.contactor = contactor2;
        this.telno = telno2;
        this.infotype = infotype2;
        this.district = district2;
        this.price = price2;
        this.room = room2;
        this.renttype = renttype2;
        this.buildarea = buildarea2;
    }

    public PublishHouse(String title2, String remark2, String contactor2, String telno2, String infotype2, String district2, String price2, String room2, String floormsg2, String buildarea2, String blockname2, List<String> pics2) {
        this.title = title2;
        this.remark = remark2;
        this.contactor = contactor2;
        this.telno = telno2;
        this.infotype = infotype2;
        this.district = district2;
        this.price = price2;
        this.room = room2;
        this.floormsg = floormsg2;
        this.buildarea = buildarea2;
        this.blockname = blockname2;
        this.pics = pics2;
    }

    public PublishHouse(String title2, String remark2, String contactor2, String telno2, String infotype2, String district2, String price2, String room2, String floormsg2, String renttype2, String buildarea2, String blockname2, List<String> pics2, String fitment2, String priceunit2) {
        this.title = title2;
        this.remark = remark2;
        this.contactor = contactor2;
        this.telno = telno2;
        this.infotype = infotype2;
        this.district = district2;
        this.price = price2;
        this.room = room2;
        this.floormsg = floormsg2;
        this.renttype = renttype2;
        this.buildarea = buildarea2;
        this.blockname = blockname2;
        this.pics = pics2;
        this.fitment = fitment2;
        this.priceunit = priceunit2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public int getCreatetime() {
        return this.createtime;
    }

    public String getCreateData() {
        if (this.createtime != 0) {
            return TimeUtil.toDate((long) this.createtime);
        }
        return StringUtils.EMPTY;
    }

    public void setCreatetime(int createtime2) {
        this.createtime = createtime2;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark2) {
        this.remark = remark2;
    }

    public String getContactor() {
        return this.contactor;
    }

    public void setContactor(String contactor2) {
        this.contactor = contactor2;
    }

    public String getTelno() {
        return this.telno;
    }

    public void setTelno(String telno2) {
        this.telno = telno2;
    }

    public String getInfotype() {
        return this.infotype;
    }

    public void setInfotype(String infotype2) {
        this.infotype = infotype2;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district2) {
        this.district = district2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public String getRoom() {
        return this.room;
    }

    public void setRoom(String room2) {
        this.room = room2;
    }

    public String getFindroom() {
        return this.findroom;
    }

    public void setFindroom(String findroom2) {
        this.findroom = findroom2;
    }

    public String getRenttype() {
        return this.renttype;
    }

    public void setRenttype(String renttype2) {
        this.renttype = renttype2;
    }

    public String getBuildarea() {
        return this.buildarea;
    }

    public void setBuildarea(String buildarea2) {
        this.buildarea = buildarea2;
    }

    public String getBlockname() {
        return this.blockname;
    }

    public void setBlockname(String blockname2) {
        this.blockname = blockname2;
    }

    public List<String> getPics() {
        return this.pics;
    }

    public void setPics(List<String> pics2) {
        this.pics = pics2;
    }

    public String getFitment() {
        return this.fitment;
    }

    public void setFitment(String fitment2) {
        this.fitment = fitment2;
    }

    public String getPriceunit() {
        return this.priceunit;
    }

    public void setPriceunit(String priceunit2) {
        this.priceunit = priceunit2;
    }

    public String getFloormsg() {
        return this.floormsg;
    }

    public void setFloormsg(String floormsg2) {
        this.floormsg = floormsg2;
    }

    public int getInfoTypeId() {
        return this.infoTypeId;
    }

    public void setInfoTypeId(int infoTypeId2) {
        this.infoTypeId = infoTypeId2;
    }

    public boolean isSame(PublishHouse o) {
        return o.getId().equals(getId());
    }
}
