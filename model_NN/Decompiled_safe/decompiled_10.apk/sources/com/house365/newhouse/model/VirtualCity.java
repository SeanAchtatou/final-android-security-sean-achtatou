package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class VirtualCity extends SharedPreferencesDTO<VirtualCity> {
    private static final long serialVersionUID = -848818789722791210L;
    private int city_api;
    private String city_key;
    private String city_name;
    private String city_py;
    private String city_x;
    private String city_y;

    public String getCity_key() {
        return this.city_key;
    }

    public void setCity_key(String city_key2) {
        this.city_key = city_key2;
    }

    public String getCity_name() {
        return this.city_name;
    }

    public void setCity_name(String city_name2) {
        this.city_name = city_name2;
    }

    public double getCity_x() {
        if (this.city_x != null) {
            return Double.parseDouble(this.city_x);
        }
        return 0.0d;
    }

    public void setCity_x(String city_x2) {
        this.city_x = city_x2;
    }

    public double getCity_y() {
        if (this.city_y != null) {
            return Double.parseDouble(this.city_y);
        }
        return 0.0d;
    }

    public void setCity_y(String city_y2) {
        this.city_y = city_y2;
    }

    public int getCity_api() {
        return this.city_api;
    }

    public void setCity_api(int city_api2) {
        this.city_api = city_api2;
    }

    public String getCity_py() {
        return this.city_py;
    }

    public void setCity_py(String city_py2) {
        this.city_py = city_py2;
    }

    public boolean isSame(VirtualCity o) {
        return getCity_name().equals(o.getCity_name());
    }

    public int hashCode() {
        return this.city_name.hashCode();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof VirtualCity) || !this.city_name.equals(((VirtualCity) o).city_name)) {
            return false;
        }
        return true;
    }
}
