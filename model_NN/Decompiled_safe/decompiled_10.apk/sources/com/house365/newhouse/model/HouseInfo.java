package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

public class HouseInfo extends SharedPreferencesDTO<HouseInfo> {
    private static final long serialVersionUID = 2646488250479996501L;
    private Block block;
    private House newHouse;
    private long optime;
    private String refer_type;
    private SecondHouse sellOrRent;
    private String type;

    public HouseInfo() {
    }

    public HouseInfo(String type2, House newHouse2) {
        this.type = type2;
        this.newHouse = newHouse2;
        this.optime = new Date().getTime() / 1000;
    }

    public HouseInfo(String type2, SecondHouse sellOrRent2) {
        this.type = type2;
        this.sellOrRent = sellOrRent2;
        this.optime = new Date().getTime() / 1000;
    }

    public HouseInfo(String type2, Block block2) {
        this.type = type2;
        this.block = block2;
        this.optime = new Date().getTime() / 1000;
    }

    public HouseInfo(String type2) {
        this.type = type2;
        this.optime = new Date().getTime() / 1000;
    }

    public HouseInfo(String type2, House newHouse2, long optime2, String refer_type2) {
        this.type = type2;
        this.newHouse = newHouse2;
        this.optime = optime2;
        this.refer_type = refer_type2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getRefer_type() {
        return this.refer_type;
    }

    public void setRefer_type(String refer_type2) {
        this.refer_type = refer_type2;
    }

    public House getNewHouse() {
        return this.newHouse;
    }

    public void setNewHouse(House newHouse2) {
        this.newHouse = newHouse2;
    }

    public SecondHouse getSellOrRent() {
        return this.sellOrRent;
    }

    public void setSellOrRent(SecondHouse sellOrRent2) {
        this.sellOrRent = sellOrRent2;
    }

    public Block getBlock() {
        return this.block;
    }

    public void setBlock(Block block2) {
        this.block = block2;
    }

    public long getOptime() {
        if (this.optime <= 0) {
            this.optime = new Date().getTime() / 1000;
        }
        return this.optime;
    }

    public void setOptime(long optime2) {
        this.optime = optime2;
    }

    public boolean isSame(HouseInfo o) {
        String aId = StringUtils.EMPTY;
        String bId = StringUtils.EMPTY;
        if (o.getNewHouse() != null) {
            bId = o.getNewHouse().getH_id();
        }
        if (o.getSellOrRent() != null) {
            bId = o.getSellOrRent().getId();
        }
        if (o.getBlock() != null) {
            bId = o.getBlock().getId();
        }
        if (getNewHouse() != null) {
            aId = getNewHouse().getH_id();
        }
        if (getSellOrRent() != null) {
            aId = getSellOrRent().getId();
        }
        if (getBlock() != null) {
            aId = getBlock().getId();
        }
        return aId.equals(bId);
    }
}
