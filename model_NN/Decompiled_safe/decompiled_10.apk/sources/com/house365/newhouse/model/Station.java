package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;
import com.house365.core.json.JSONException;
import com.house365.core.json.JSONObject;

public class Station extends BaseBean {
    private static final long serialVersionUID = 1;
    private String id;
    private String name;
    private double x;
    private double y;

    public Station(JSONObject json) {
        try {
            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("x")) {
                this.x = json.getDouble("x");
            }
            if (json.has("y")) {
                this.y = json.getDouble("y");
            }
            if (json.has("id")) {
                this.id = json.getString("id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Station(String js) throws JSONException {
        JSONObject json = new JSONObject(js);
        try {
            if (json.has("name")) {
                this.name = json.getString("name");
            }
            if (json.has("x")) {
                this.x = json.getDouble("x");
            }
            if (json.has("y")) {
                this.y = json.getDouble("y");
            }
            if (json.has("id")) {
                this.id = json.getString("id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return this.name;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }
}
