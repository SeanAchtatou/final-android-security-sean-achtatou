package com.house365.newhouse.model;

import com.house365.core.bean.BaseBean;

public class HotTag extends BaseBean {
    private static final long serialVersionUID = 8684044812153926449L;
    private String t_aboutid;
    private String t_abouttype;
    private String t_id;
    private String t_rate;
    private String t_title;

    public String getT_id() {
        return this.t_id;
    }

    public void setT_id(String t_id2) {
        this.t_id = t_id2;
    }

    public String getT_title() {
        return this.t_title;
    }

    public void setT_title(String t_title2) {
        this.t_title = t_title2;
    }

    public String getT_rate() {
        return this.t_rate;
    }

    public void setT_rate(String t_rate2) {
        this.t_rate = t_rate2;
    }

    public String getT_aboutid() {
        return this.t_aboutid;
    }

    public void setT_aboutid(String t_aboutid2) {
        this.t_aboutid = t_aboutid2;
    }

    public String getT_abouttype() {
        return this.t_abouttype;
    }

    public void setT_abouttype(String t_abouttype2) {
        this.t_abouttype = t_abouttype2;
    }
}
