package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class Award extends SharedPreferencesDTO<House> {
    private static final long serialVersionUID = 1;
    private String a_content;
    private String a_id;
    private String a_num;
    private String a_order;
    private String a_pic;
    private String a_rate;

    public String getA_id() {
        return this.a_id;
    }

    public void setA_id(String a_id2) {
        this.a_id = a_id2;
    }

    public String getA_order() {
        return this.a_order;
    }

    public void setA_order(String a_order2) {
        this.a_order = a_order2;
    }

    public String getA_content() {
        return this.a_content;
    }

    public void setA_content(String a_content2) {
        this.a_content = a_content2;
    }

    public String getA_num() {
        return this.a_num;
    }

    public void setA_num(String a_num2) {
        this.a_num = a_num2;
    }

    public String getA_rate() {
        return this.a_rate;
    }

    public void setA_rate(String a_rate2) {
        this.a_rate = a_rate2;
    }

    public String getA_pic() {
        return this.a_pic;
    }

    public void setA_pic(String a_pic2) {
        this.a_pic = a_pic2;
    }

    public boolean isSame(House o) {
        return false;
    }
}
