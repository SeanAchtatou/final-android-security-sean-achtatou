package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class House extends SharedPreferencesDTO<House> {
    private static final long serialVersionUID = 1;
    private String h_build_type;
    private String h_built_area;
    private String h_bundled;
    private String h_business;
    private String h_carport;
    private String h_carport_rate;
    private String h_channel;
    private List<Event> h_coupon;
    private String h_deli_date;
    private String h_deli_standard;
    private String h_dist;
    private String h_dls;
    private List<Event> h_event;
    private int h_fav;
    private String h_floorarea;
    private String h_gh_rate;
    private String h_green_rate;
    private String h_id;
    private String h_intro;
    private String h_kfs;
    private String h_lat;
    private String h_licence;
    private String h_long;
    private String h_name;
    private List<SaleInfo> h_news;
    private String h_pay_type;
    private String h_pic;
    private int h_pic_num;
    private String h_plot_rate;
    private String h_price;
    private String h_project_address;
    private List<House> h_recomm_houselist;
    private List<Photo> h_rooms;
    private String h_sale_address;
    private long h_sale_starttime;
    private String h_salestat_str;
    private String h_tel;
    private List<Event> h_tjf;
    private List<Event> h_tlf;
    private String h_total_house;
    private String h_traffic;
    private String h_type;
    private String h_wy;
    private String h_yhinfo;
    private int ontime;

    public String getH_id() {
        return this.h_id;
    }

    public void setH_id(String h_id2) {
        this.h_id = h_id2;
    }

    public long getH_sale_starttime() {
        return this.h_sale_starttime;
    }

    public void setH_sale_starttime(long h_sale_starttime2) {
        this.h_sale_starttime = h_sale_starttime2;
    }

    public String getH_dist() {
        return this.h_dist;
    }

    public void setH_dist(String h_dist2) {
        this.h_dist = h_dist2;
    }

    public String getH_name() {
        return this.h_name;
    }

    public void setH_name(String h_name2) {
        this.h_name = h_name2;
    }

    public String getH_pic() {
        return this.h_pic;
    }

    public void setH_pic(String h_pic2) {
        this.h_pic = h_pic2;
    }

    public String getH_business() {
        return this.h_business;
    }

    public void setH_business(String h_business2) {
        this.h_business = h_business2;
    }

    public String getH_type() {
        return this.h_type;
    }

    public void setH_type(String h_type2) {
        this.h_type = h_type2;
    }

    public String getH_deli_date() {
        return this.h_deli_date;
    }

    public void setH_deli_date(String h_deli_date2) {
        this.h_deli_date = h_deli_date2;
    }

    public String getH_price() {
        return this.h_price;
    }

    public void setH_price(String h_price2) {
        this.h_price = h_price2;
    }

    public String getH_channel() {
        return this.h_channel;
    }

    public void setH_channel(String h_channel2) {
        this.h_channel = h_channel2;
    }

    public String getH_kfs() {
        return this.h_kfs;
    }

    public void setH_kfs(String h_kfs2) {
        this.h_kfs = h_kfs2;
    }

    public String getH_dls() {
        return this.h_dls;
    }

    public void setH_dls(String h_dls2) {
        this.h_dls = h_dls2;
    }

    public String getH_wy() {
        return this.h_wy;
    }

    public void setH_wy(String h_wy2) {
        this.h_wy = h_wy2;
    }

    public String getH_project_address() {
        if (this.h_project_address == null) {
            this.h_project_address = StringUtils.EMPTY;
        }
        return this.h_project_address;
    }

    public void setH_project_address(String h_project_address2) {
        this.h_project_address = h_project_address2;
    }

    public String getH_licence() {
        return this.h_licence;
    }

    public void setH_licence(String h_licence2) {
        this.h_licence = h_licence2;
    }

    public String getH_tel() {
        return this.h_tel;
    }

    public void setH_tel(String h_tel2) {
        this.h_tel = h_tel2;
    }

    public String getH_sale_address() {
        return this.h_sale_address;
    }

    public void setH_sale_address(String h_sale_address2) {
        this.h_sale_address = h_sale_address2;
    }

    public String getH_pay_type() {
        return this.h_pay_type;
    }

    public void setH_pay_type(String h_pay_type2) {
        this.h_pay_type = h_pay_type2;
    }

    public String getH_build_type() {
        return this.h_build_type;
    }

    public void setH_build_type(String h_build_type2) {
        this.h_build_type = h_build_type2;
    }

    public String getH_gh_rate() {
        return this.h_gh_rate;
    }

    public void setH_gh_rate(String h_gh_rate2) {
        this.h_gh_rate = h_gh_rate2;
    }

    public String getH_total_house() {
        return this.h_total_house;
    }

    public void setH_total_house(String h_total_house2) {
        this.h_total_house = h_total_house2;
    }

    public String getH_plot_rate() {
        return this.h_plot_rate;
    }

    public void setH_plot_rate(String h_plot_rate2) {
        this.h_plot_rate = h_plot_rate2;
    }

    public String getH_green_rate() {
        return this.h_green_rate;
    }

    public void setH_green_rate(String h_green_rate2) {
        this.h_green_rate = h_green_rate2;
    }

    public String getH_built_area() {
        return this.h_built_area;
    }

    public void setH_built_area(String h_built_area2) {
        this.h_built_area = h_built_area2;
    }

    public String getH_floorarea() {
        return this.h_floorarea;
    }

    public void setH_floorarea(String h_floorarea2) {
        this.h_floorarea = h_floorarea2;
    }

    public String getH_carport() {
        return this.h_carport;
    }

    public void setH_carport(String h_carport2) {
        this.h_carport = h_carport2;
    }

    public String getH_carport_rate() {
        return this.h_carport_rate;
    }

    public void setH_carport_rate(String h_carport_rate2) {
        this.h_carport_rate = h_carport_rate2;
    }

    public double getH_long() {
        try {
            if (this.h_long != null) {
                return Double.parseDouble(this.h_long);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0.0d;
    }

    public void setH_long(String h_long2) {
        this.h_long = h_long2;
    }

    public double getH_lat() {
        try {
            if (this.h_lat != null) {
                return Double.parseDouble(this.h_lat);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0.0d;
    }

    public void setH_lat(String h_lat2) {
        this.h_lat = h_lat2;
    }

    public String getH_intro() {
        return this.h_intro;
    }

    public void setH_intro(String h_intro2) {
        this.h_intro = h_intro2;
    }

    public String getH_traffic() {
        return this.h_traffic;
    }

    public void setH_traffic(String h_traffic2) {
        this.h_traffic = h_traffic2;
    }

    public String getH_bundled() {
        return this.h_bundled;
    }

    public void setH_bundled(String h_bundled2) {
        this.h_bundled = h_bundled2;
    }

    public int getH_pic_num() {
        return this.h_pic_num;
    }

    public void setH_pic_num(int h_pic_num2) {
        this.h_pic_num = h_pic_num2;
    }

    public String getH_deli_standard() {
        return this.h_deli_standard;
    }

    public void setH_deli_standard(String h_deli_standard2) {
        this.h_deli_standard = h_deli_standard2;
    }

    public String getH_salestat_str() {
        return this.h_salestat_str;
    }

    public void setH_salestat_str(String h_salestat_str2) {
        this.h_salestat_str = h_salestat_str2;
    }

    public List<SaleInfo> getH_news() {
        return this.h_news;
    }

    public void setH_news(List<SaleInfo> h_news2) {
        this.h_news = h_news2;
    }

    public List<Event> getH_tlf() {
        return this.h_tlf;
    }

    public void setH_tlf(List<Event> h_tlf2) {
        this.h_tlf = h_tlf2;
    }

    public List<Event> getH_tjf() {
        return this.h_tjf;
    }

    public void setH_tjf(List<Event> h_tjf2) {
        this.h_tjf = h_tjf2;
    }

    public List<Event> getH_event() {
        return this.h_event;
    }

    public void setH_event(List<Event> h_event2) {
        this.h_event = h_event2;
    }

    public List<Event> getH_coupon() {
        return this.h_coupon;
    }

    public void setH_coupon(List<Event> h_coupon2) {
        this.h_coupon = h_coupon2;
    }

    public List<Photo> getH_rooms() {
        return this.h_rooms;
    }

    public void setH_rooms(List<Photo> h_rooms2) {
        this.h_rooms = h_rooms2;
    }

    public List<House> getH_recomm_houselist() {
        return this.h_recomm_houselist;
    }

    public void setH_recomm_houselist(List<House> h_recomm_houselist2) {
        this.h_recomm_houselist = h_recomm_houselist2;
    }

    public int getH_fav() {
        return this.h_fav;
    }

    public void setH_fav(int h_fav2) {
        this.h_fav = h_fav2;
    }

    public int getOntime() {
        return this.ontime;
    }

    public void setOntime(int ontime2) {
        this.ontime = ontime2;
    }

    public String getH_yhinfo() {
        return this.h_yhinfo;
    }

    public void setH_yhinfo(String h_yhinfo2) {
        this.h_yhinfo = h_yhinfo2;
    }

    public boolean isSame(House o) {
        return getH_id().equals(o.getH_id());
    }
}
