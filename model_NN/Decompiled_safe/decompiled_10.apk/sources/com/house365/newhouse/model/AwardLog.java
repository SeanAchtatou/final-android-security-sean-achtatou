package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class AwardLog extends SharedPreferencesDTO<Event> {
    private static final long serialVersionUID = 1;
    private String s_content;
    private String s_id;
    private String s_join_time;
    private String s_name;
    private String s_phone;
    private String s_title;

    public String getS_id() {
        return this.s_id;
    }

    public void setS_id(String s_id2) {
        this.s_id = s_id2;
    }

    public String getS_phone() {
        return this.s_phone;
    }

    public void setS_phone(String s_phone2) {
        this.s_phone = s_phone2;
    }

    public String getS_name() {
        return this.s_name;
    }

    public void setS_name(String s_name2) {
        this.s_name = s_name2;
    }

    public String getS_join_time() {
        return this.s_join_time;
    }

    public void setS_join_time(String s_join_time2) {
        this.s_join_time = s_join_time2;
    }

    public String getS_title() {
        return this.s_title;
    }

    public void setS_title(String s_title2) {
        this.s_title = s_title2;
    }

    public String getS_content() {
        return this.s_content;
    }

    public void setS_content(String s_content2) {
        this.s_content = s_content2;
    }

    public boolean isSame(Event o) {
        return false;
    }
}
