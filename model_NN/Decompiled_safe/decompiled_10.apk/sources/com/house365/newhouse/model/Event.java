package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.List;

public class Event extends SharedPreferencesDTO<Event> {
    private List<House> e_about_houses;
    private long e_addtime;
    private String e_area;
    private List<AwardInfo> e_awards;
    private String e_content;
    private long e_endtime;
    private String e_explain;
    private House e_house;
    private String e_id;
    private String e_intro;
    private String e_join;
    private List<Line> e_lines;
    private List<AwardLog> e_logs;
    private Event e_lottery;
    private String e_lottery_join;
    private List<AwardLog> e_mylogs;
    private String e_o_price;
    private String e_p_price;
    private String e_phone;
    private String e_pic;
    private String e_price;
    private String e_remark;
    private List<Photo> e_rooms;
    private String e_roomtype;
    private String e_s_price;
    private long e_starttime;
    private String e_tel;
    private String e_title;
    private String e_type;
    private String e_uwinnum;
    private String e_winnum;

    public String getE_type() {
        return this.e_type;
    }

    public void setE_type(String e_type2) {
        this.e_type = e_type2;
    }

    public String getE_id() {
        return this.e_id;
    }

    public void setE_id(String e_id2) {
        this.e_id = e_id2;
    }

    public String getE_title() {
        return this.e_title;
    }

    public void setE_title(String e_title2) {
        this.e_title = e_title2;
    }

    public String getE_p_price() {
        return this.e_p_price;
    }

    public String getE_explain() {
        return this.e_explain;
    }

    public void setE_explain(String e_explain2) {
        this.e_explain = e_explain2;
    }

    public void setE_p_price(String e_p_price2) {
        this.e_p_price = e_p_price2;
    }

    public String getE_s_price() {
        return this.e_s_price;
    }

    public void setE_s_price(String e_s_price2) {
        this.e_s_price = e_s_price2;
    }

    public String getE_o_price() {
        return this.e_o_price;
    }

    public void setE_o_price(String e_o_price2) {
        this.e_o_price = e_o_price2;
    }

    public String getE_roomtype() {
        return this.e_roomtype;
    }

    public void setE_roomtype(String e_roomtype2) {
        this.e_roomtype = e_roomtype2;
    }

    public String getE_area() {
        return this.e_area;
    }

    public void setE_area(String e_area2) {
        this.e_area = e_area2;
    }

    public List<Photo> getE_rooms() {
        return this.e_rooms;
    }

    public void setE_rooms(List<Photo> e_rooms2) {
        this.e_rooms = e_rooms2;
    }

    public House getE_house() {
        return this.e_house;
    }

    public void setE_house(House e_house2) {
        this.e_house = e_house2;
    }

    public String getE_content() {
        return this.e_content;
    }

    public void setE_content(String e_content2) {
        this.e_content = e_content2;
    }

    public String getE_pic() {
        return this.e_pic;
    }

    public void setE_pic(String e_pic2) {
        this.e_pic = e_pic2;
    }

    public long getE_starttime() {
        return this.e_starttime;
    }

    public void setE_starttime(long e_starttime2) {
        this.e_starttime = e_starttime2;
    }

    public long getE_endtime() {
        return this.e_endtime;
    }

    public void setE_endtime(long e_endtime2) {
        this.e_endtime = e_endtime2;
    }

    public long getE_addtime() {
        return this.e_addtime;
    }

    public void setE_addtime(long e_addtime2) {
        this.e_addtime = e_addtime2;
    }

    public String getE_price() {
        return this.e_price;
    }

    public void setE_price(String e_price2) {
        this.e_price = e_price2;
    }

    public String getE_intro() {
        return this.e_intro;
    }

    public void setE_intro(String e_intro2) {
        this.e_intro = e_intro2;
    }

    public String getE_remark() {
        return this.e_remark;
    }

    public void setE_remark(String e_remark2) {
        this.e_remark = e_remark2;
    }

    public String getE_join() {
        return this.e_join;
    }

    public void setE_join(String e_join2) {
        this.e_join = e_join2;
    }

    public String getE_tel() {
        return this.e_tel;
    }

    public void setE_tel(String e_tel2) {
        this.e_tel = e_tel2;
    }

    public List<House> getE_about_houses() {
        return this.e_about_houses;
    }

    public void setE_about_houses(List<House> e_about_houses2) {
        this.e_about_houses = e_about_houses2;
    }

    public List<Line> getE_lines() {
        return this.e_lines;
    }

    public void setE_lines(List<Line> e_lines2) {
        this.e_lines = e_lines2;
    }

    public List<AwardInfo> getE_awards() {
        return this.e_awards;
    }

    public void setE_awards(List<AwardInfo> e_awards2) {
        this.e_awards = e_awards2;
    }

    public List<AwardLog> getE_logs() {
        return this.e_logs;
    }

    public void setE_logs(List<AwardLog> e_logs2) {
        this.e_logs = e_logs2;
    }

    public String getE_phone() {
        return this.e_phone;
    }

    public void setE_phone(String e_phone2) {
        this.e_phone = e_phone2;
    }

    public Event getE_lottery() {
        return this.e_lottery;
    }

    public void setE_lottery(Event e_lottery2) {
        this.e_lottery = e_lottery2;
    }

    public List<AwardLog> getE_mylogs() {
        return this.e_mylogs;
    }

    public void setE_mylogs(List<AwardLog> e_mylogs2) {
        this.e_mylogs = e_mylogs2;
    }

    public String getE_lottery_join() {
        return this.e_lottery_join;
    }

    public void setE_lottery_join(String e_lottery_join2) {
        this.e_lottery_join = e_lottery_join2;
    }

    public String getE_uwinnum() {
        return this.e_uwinnum;
    }

    public void setE_uwinnum(String e_uwinnum2) {
        this.e_uwinnum = e_uwinnum2;
    }

    public String getE_winnum() {
        return this.e_winnum;
    }

    public void setE_winnum(String e_winnum2) {
        this.e_winnum = e_winnum2;
    }

    public boolean isSame(Event o) {
        return getE_id().equals(o.getE_id());
    }
}
