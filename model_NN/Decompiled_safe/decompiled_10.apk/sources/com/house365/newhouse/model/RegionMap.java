package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class RegionMap extends SharedPreferencesDTO<RegionMap> {
    private int a_house_count;
    private String a_id;
    private String a_lat;
    private String a_lng;
    private String a_name;

    public RegionMap() {
    }

    public RegionMap(String a_id2, String a_name2, String a_lng2, String a_lat2, int a_house_count2) {
        this.a_id = a_id2;
        this.a_name = a_name2;
        this.a_lng = a_lng2;
        this.a_lat = a_lat2;
        this.a_house_count = a_house_count2;
    }

    public String getA_id() {
        return this.a_id;
    }

    public void setA_id(String a_id2) {
        this.a_id = a_id2;
    }

    public String getA_name() {
        return this.a_name;
    }

    public void setA_name(String a_name2) {
        this.a_name = a_name2;
    }

    public double getA_lng() {
        try {
            if (this.a_lng != null) {
                return Double.parseDouble(this.a_lng);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0.0d;
    }

    public void setA_lng(String a_lng2) {
        this.a_lng = a_lng2;
    }

    public double getA_lat() {
        try {
            if (this.a_lat != null) {
                return Double.parseDouble(this.a_lat);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0.0d;
    }

    public void setA_lat(String a_lat2) {
        this.a_lat = a_lat2;
    }

    public int getA_house_count() {
        return this.a_house_count;
    }

    public void setA_house_count(int a_house_count2) {
        this.a_house_count = a_house_count2;
    }

    public boolean isSame(RegionMap o) {
        return getA_id().equals(o.getA_id());
    }
}
