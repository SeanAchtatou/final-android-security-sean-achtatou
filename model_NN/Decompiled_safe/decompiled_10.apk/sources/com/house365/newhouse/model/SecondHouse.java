package com.house365.newhouse.model;

import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class SecondHouse extends SharedPreferencesDTO<House> {
    public static final int MAX_SIZE_INDOOR = 10;
    public static final int MAX_SIZE_LAYOUT = 3;
    public static final int MAX_SIZE_LOCATION = 5;
    private static final long serialVersionUID = -7640374228191881857L;
    private String apartment;
    private String averprice;
    private Block blockinfo;
    private BrokerInfo brokerinfo;
    private String buildarea;
    private String buildyear;
    private String district;
    private String enterdate;
    private String fitment;
    private String forward;
    private int hall;
    private String id;
    private String infotype;
    private String mright;
    private String name;
    private String payment;
    private String pic;
    private String pics;
    private String price;
    private String priceterm;
    private String remark;
    private String renttype;
    private int room;
    private int s_fav;
    private String state;
    private String story;
    private String streetname;
    private String title;
    private String type;
    private String updatetime;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getS_fav() {
        return this.s_fav;
    }

    public void setS_fav(int s_fav2) {
        this.s_fav = s_fav2;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state2) {
        this.state = state2;
    }

    public String getEnterdate() {
        return this.enterdate;
    }

    public void setEnterdate(String enterdate2) {
        this.enterdate = enterdate2;
    }

    public String getInfotype() {
        return this.infotype;
    }

    public void setInfotype(String infotype2) {
        this.infotype = infotype2;
    }

    public String getPriceterm() {
        return this.priceterm;
    }

    public void setPriceterm(String priceterm2) {
        this.priceterm = priceterm2;
    }

    public String getUpdatetime() {
        return this.updatetime;
    }

    public void setUpdatetime(String updatetime2) {
        this.updatetime = updatetime2;
    }

    public String getPayment() {
        return this.payment;
    }

    public void setPayment(String payment2) {
        this.payment = payment2;
    }

    public String getRenttype() {
        return this.renttype;
    }

    public void setRenttype(String renttype2) {
        this.renttype = renttype2;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district2) {
        this.district = district2;
    }

    public String getStreetname() {
        return this.streetname;
    }

    public void setStreetname(String streetname2) {
        this.streetname = streetname2;
    }

    public String getApartment() {
        return this.apartment;
    }

    public void setApartment(String apartment2) {
        this.apartment = apartment2;
    }

    public String getStory() {
        return this.story;
    }

    public void setStory(String story2) {
        this.story = story2;
    }

    public String getForward() {
        return this.forward;
    }

    public void setForward(String forward2) {
        this.forward = forward2;
    }

    public String getBuildyear() {
        return this.buildyear;
    }

    public void setBuildyear(String buildyear2) {
        this.buildyear = buildyear2;
    }

    public String getFitment() {
        return this.fitment;
    }

    public void setFitment(String fitment2) {
        this.fitment = fitment2;
    }

    public String getMright() {
        return this.mright;
    }

    public void setMright(String mright2) {
        this.mright = mright2;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark2) {
        this.remark = remark2;
    }

    public String getAverprice() {
        return this.averprice;
    }

    public void setAverprice(String averprice2) {
        this.averprice = averprice2;
    }

    public BrokerInfo getBrokerinfo() {
        return this.brokerinfo;
    }

    public void setBrokerinfo(BrokerInfo brokerinfo2) {
        this.brokerinfo = brokerinfo2;
    }

    public String getPics() {
        if (this.pics == null || (this.pics.indexOf(",") == -1 && this.pics.indexOf("[") == -1)) {
            return this.pics;
        }
        try {
            return new JSONArray(this.pics).getString(0);
        } catch (JSONException e) {
            return StringUtils.EMPTY;
        }
    }

    public List<String> getH_picList() {
        if (this.pics == null || (this.pics.indexOf(",") == -1 && this.pics.indexOf("[") == -1)) {
            return null;
        }
        return setPicList(this.pics);
    }

    public List<String> setPicList(String json) {
        List<String> tlist = new ArrayList<>();
        try {
            JSONArray ja = new JSONArray(json);
            for (int i = 0; i < ja.length(); i++) {
                tlist.add(ja.getString(i).toString());
            }
        } catch (JSONException e) {
        }
        return tlist;
    }

    public String setPics(String pics2) {
        return pics2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public int getRoom() {
        return this.room;
    }

    public void setRoom(int room2) {
        this.room = room2;
    }

    public int getHall() {
        return this.hall;
    }

    public void setHall(int hall2) {
        this.hall = hall2;
    }

    public String getBuildarea() {
        return this.buildarea;
    }

    public void setBuildarea(String buildarea2) {
        this.buildarea = buildarea2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getPic() {
        return this.pic;
    }

    public void setPic(String pic2) {
        this.pic = pic2;
    }

    public Block getBlockinfo() {
        return this.blockinfo;
    }

    public void setBlockinfo(Block blockinfo2) {
        this.blockinfo = blockinfo2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public boolean isSame(House o) {
        return false;
    }
}
