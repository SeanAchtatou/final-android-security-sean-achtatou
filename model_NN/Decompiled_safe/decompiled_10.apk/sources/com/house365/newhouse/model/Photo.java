package com.house365.newhouse.model;

import java.io.Serializable;

public class Photo implements Serializable {
    private static final long serialVersionUID = 5358649245886485683L;
    private String p_area;
    private String p_layout;
    private String p_name;
    private String p_tag;
    private String p_thumb;
    private String p_url;

    public Photo() {
    }

    public Photo(String p_name2, String p_url2) {
        this.p_name = p_name2;
        this.p_url = p_url2;
    }

    public String getP_name() {
        return this.p_name;
    }

    public void setP_name(String p_name2) {
        this.p_name = p_name2;
    }

    public String getP_thumb() {
        return this.p_thumb;
    }

    public void setP_thumb(String p_thumb2) {
        this.p_thumb = p_thumb2;
    }

    public String getP_url() {
        return this.p_url;
    }

    public void setP_url(String p_url2) {
        this.p_url = p_url2;
    }

    public String getP_tag() {
        return this.p_tag;
    }

    public void setP_tag(String p_tag2) {
        this.p_tag = p_tag2;
    }

    public String getP_area() {
        return this.p_area;
    }

    public void setP_area(String p_area2) {
        this.p_area = p_area2;
    }

    public String getP_layout() {
        return this.p_layout;
    }

    public void setP_layout(String p_layout2) {
        this.p_layout = p_layout2;
    }
}
