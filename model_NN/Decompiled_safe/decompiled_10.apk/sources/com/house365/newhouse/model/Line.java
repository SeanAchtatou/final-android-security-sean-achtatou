package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class Line extends SharedPreferencesDTO<Line> {
    private static final long serialVersionUID = 8623815749539246833L;
    private String l_id;
    private String l_order;
    private String l_title;

    public String getL_id() {
        return this.l_id;
    }

    public void setL_id(String l_id2) {
        this.l_id = l_id2;
    }

    public String getL_title() {
        return this.l_title;
    }

    public void setL_title(String l_title2) {
        this.l_title = l_title2;
    }

    public String getL_order() {
        return this.l_order;
    }

    public void setL_order(String l_order2) {
        this.l_order = l_order2;
    }

    public boolean isSame(Line o) {
        return getL_id().equals(o.getL_id());
    }
}
