package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;
import java.util.List;

public class AwardInfo extends SharedPreferencesDTO<House> {
    private static final long serialVersionUID = 1;
    private String a_content;
    private String a_id;
    private String a_num;
    private String a_order;
    private String a_pic;
    private String a_rate;
    private Award u_award;
    private String u_error;
    private List<AwardLog> u_logs;
    private String u_msg;
    private String u_num;

    public boolean isSame(House o) {
        return false;
    }

    public String getU_error() {
        return this.u_error;
    }

    public void setU_error(String u_error2) {
        this.u_error = u_error2;
    }

    public String getU_msg() {
        return this.u_msg;
    }

    public void setU_msg(String u_msg2) {
        this.u_msg = u_msg2;
    }

    public String getU_num() {
        return this.u_num;
    }

    public void setU_num(String u_num2) {
        this.u_num = u_num2;
    }

    public Award getAward() {
        return this.u_award;
    }

    public void setAward(Award u_award2) {
        this.u_award = u_award2;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getA_id() {
        return this.a_id;
    }

    public void setA_id(String a_id2) {
        this.a_id = a_id2;
    }

    public String getA_order() {
        return this.a_order;
    }

    public void setA_order(String a_order2) {
        this.a_order = a_order2;
    }

    public String getA_content() {
        return this.a_content;
    }

    public void setA_content(String a_content2) {
        this.a_content = a_content2;
    }

    public String getA_num() {
        return this.a_num;
    }

    public void setA_num(String a_num2) {
        this.a_num = a_num2;
    }

    public String getA_rate() {
        return this.a_rate;
    }

    public void setA_rate(String a_rate2) {
        this.a_rate = a_rate2;
    }

    public String getA_pic() {
        return this.a_pic;
    }

    public void setA_pic(String a_pic2) {
        this.a_pic = a_pic2;
    }

    public Award getU_award() {
        return this.u_award;
    }

    public void setU_award(Award u_award2) {
        this.u_award = u_award2;
    }

    public List<AwardLog> getU_logs() {
        return this.u_logs;
    }

    public void setU_logs(List<AwardLog> u_logs2) {
        this.u_logs = u_logs2;
    }
}
