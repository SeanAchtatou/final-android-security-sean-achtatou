package com.house365.newhouse.model;

import com.house365.core.util.store.SharedPreferencesDTO;

public class BrokerInfo extends SharedPreferencesDTO<BrokerInfo> {
    public static final int MAX_SIZE_INDOOR = 10;
    public static final int MAX_SIZE_LAYOUT = 3;
    public static final int MAX_SIZE_LOCATION = 5;
    private static final long serialVersionUID = -7640374228191881857L;
    private String agentshortname;
    private String smallphoto;
    private String telno;
    private String truename;
    private String type;

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getTruename() {
        return this.truename;
    }

    public void setTruename(String truename2) {
        this.truename = truename2;
    }

    public String getTelno() {
        return this.telno;
    }

    public void setTelno(String telno2) {
        this.telno = telno2;
    }

    public String getSmallphoto() {
        return this.smallphoto;
    }

    public void setSmallphoto(String smallphoto2) {
        this.smallphoto = smallphoto2;
    }

    public String getAgentshortname() {
        return this.agentshortname;
    }

    public void setAgentshortname(String agentshortname2) {
        this.agentshortname = agentshortname2;
    }

    public boolean isSame(BrokerInfo o) {
        return false;
    }
}
