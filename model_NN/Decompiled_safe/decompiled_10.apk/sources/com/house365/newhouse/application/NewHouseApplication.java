package com.house365.newhouse.application;

import android.graphics.Bitmap;
import com.house365.androidpn.client.PullServiceManager;
import com.house365.core.application.BaseApplicationWithMapService;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.newhouse.api.HttpApi;
import com.house365.newhouse.constant.App;
import com.house365.newhouse.constant.AppArrays;
import com.house365.newhouse.model.Event;
import com.house365.newhouse.model.EventNum;
import com.house365.newhouse.model.HouseInfo;
import com.house365.newhouse.model.KeyItemArray;
import com.house365.newhouse.model.News;
import com.house365.newhouse.model.PublishHouse;
import com.house365.newhouse.tool.ActionCode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class NewHouseApplication extends BaseApplicationWithMapService<HttpApi> {
    public static final String KEY_OFFER_GROUP = "offer_group_";
    public static final String KEY_OFFER_RENT = "offer_rent_";
    public static final String KEY_OFFER_SELL = "offer_sell_";
    public static final String KEY_PUBLISH_RENT = "publish_rent_";
    public static final String KEY_PUBLISH_SELL = "publish_sell_";
    public static final int KEY_TYPE_OFFER = 11;
    public static final int KEY_TYPE_PUBLISH = 10;
    public static final String PREFERENCE_MESSAGE_NOTIFY_SETTED = "PREFERENCE_MESSAGE_NOTIFY_HAS_SETTED";
    public final int CACHE_MAX_SIZE = 50;
    public final String KEY_APICACHE = "apicache_";
    public final String KEY_CITY = "city";
    private final int KEY_COUPON_CASH = 7;
    private final String KEY_COUPON_CASH_SUFFIX = App.Categroy.Event.COUPON;
    private final int KEY_EVENT_CASH = 6;
    private final String KEY_EVENT_CASH_SUFFIX = "event";
    private final int KEY_HISTORY_AWARD = 12;
    private final String KEY_HISTORY_BROWSE_SUFFIX = "browse";
    private final String KEY_HISTORY_CALL_SUFFIX = "call";
    private final String KEY_HISTORY_FAV_SUFFIX = "fav";
    private final int KEY_HISTORY_TYPE_BROWSE = 3;
    private final int KEY_HISTORY_TYPE_CALL = 2;
    private final int KEY_HISTORY_TYPE_FAV = 1;
    private final String KEY_KEY_HISTORY_AWARD_SUFFIX = ActionCode.AWARD;
    public final String KEY_LAST_DATE = "last_request_date_";
    public final String KEY_MOBILE = "moblie";
    private final String KEY_NEWS = App.Categroy.News.NEWS;
    private final int KEY_OVERDUE_SALE_CASH = 8;
    private final String KEY_OVERDUE_SALE_CASH_SUFFIX = "oldsale";
    private final int KEY_OVERDUE_TLF_CASH = 9;
    private final String KEY_OVERDUE_TLF_CASH_SUFFIX = "oldtlf";
    private final int KEY_SALE_CASH = 4;
    private final String KEY_TJF_CASH_SUFFIX = App.Categroy.Event.TJF;
    private final int KEY_TLF_CASH = 5;
    private final String KEY_TLF_CASH_SUFFIX = App.Categroy.Event.TLF;
    private final String PREFERENCE_KEYSEARCH_HISTORY = "keysearch";
    private final String SPLIT_CHAR = "_";
    private List<Bitmap> awardPics = new ArrayList();
    private HashMap<String, String> cityLocation;
    private int maxcount = 20;
    private PullServiceManager pullServiceManager = null;
    private EventNum saleNum;

    /* access modifiers changed from: protected */
    public void onAppCreate() {
    }

    /* access modifiers changed from: protected */
    public void onAppDestory() {
    }

    public void onAppCancel() {
    }

    /* access modifiers changed from: protected */
    public HttpApi instanceApi() {
        return new HttpApi(this.sharedPrefsUtil, this);
    }

    public boolean notifySetted() {
        return this.sharedPrefsUtil.getBoolean(PREFERENCE_MESSAGE_NOTIFY_SETTED, false);
    }

    public void setNotifySetted(boolean flag) {
        this.sharedPrefsUtil.putBoolean(PREFERENCE_MESSAGE_NOTIFY_SETTED, flag);
    }

    public double[] getCiytLoaction(String city) {
        String s = this.cityLocation.get(city);
        return new double[]{Double.parseDouble(s.split(",")[0]), Double.parseDouble(s.split(",")[1])};
    }

    public synchronized String getCityName() {
        AppArrays.context = getApplicationContext();
        return AppArrays.getCityShowname(getCity());
    }

    public void setCity(String citypy) {
        CorePreferences.ERROR("city:" + citypy);
        this.sharedPrefsUtil.putString("city", citypy);
    }

    public List<Bitmap> getAwardPics() {
        return this.awardPics;
    }

    public void addAwardPic(Bitmap awardPics2) {
        this.awardPics.add(awardPics2);
    }

    public String getCity() {
        return this.sharedPrefsUtil.getString("city", "nanjing");
    }

    public String getCityKey() {
        return AppArrays.getCityKey(getCity());
    }

    public String getMobile() {
        return this.sharedPrefsUtil.getString("moblie", StringUtils.EMPTY);
    }

    public void setMobile(String mobile) {
        this.sharedPrefsUtil.putString("moblie", mobile);
    }

    public EventNum getSaleNum() {
        return this.saleNum;
    }

    public void setSaleNum(EventNum saleNum2) {
        this.saleNum = saleNum2;
    }

    public PullServiceManager getPullServiceManager() {
        return this.pullServiceManager;
    }

    public void setPullServiceManager(PullServiceManager pullServiceManager2) {
        this.pullServiceManager = pullServiceManager2;
    }

    public void removePullServiceManager() {
        if (this.pullServiceManager != null) {
            this.pullServiceManager.stopService();
            this.pullServiceManager = null;
        }
    }

    private String getKeyWordsSearch(String search_type) {
        return String.valueOf("apicache_" + getCity() + "_" + search_type + "_") + "keysearch";
    }

    public void cleanKeySearchHistroy(String search_type) {
        this.sharedPrefsUtil.clean(getKeyWordsSearch(search_type));
    }

    public boolean hasKeyHistory(KeyItemArray histroy, String search_type) {
        return this.sharedPrefsUtil.hasListItem(getKeyWordsSearch(search_type), histroy);
    }

    public void delKeyHistory(KeyItemArray histroy, String search_type) {
        this.sharedPrefsUtil.removeListItem(getKeyWordsSearch(search_type), histroy);
    }

    public List<KeyItemArray> getKeySearchHistory(String search_type) throws ReflectException, JSONException {
        return this.sharedPrefsUtil.getListWithCast(new KeyItemArray(), getKeyWordsSearch(search_type));
    }

    public void addKeySearchHistroy(KeyItemArray histroy, String search_type) {
        if (hasKeyHistory(histroy, search_type)) {
            delKeyHistory(histroy, search_type);
        }
        int hcount = 0;
        List<KeyItemArray> histroys = null;
        try {
            histroys = this.sharedPrefsUtil.getListWithCast(new KeyItemArray(), getKeyWordsSearch(search_type));
            hcount = histroys.size();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (hcount >= 10) {
            this.sharedPrefsUtil.removeListItem(getKeyWordsSearch(search_type), (KeyItemArray) histroys.get(0));
        }
        this.sharedPrefsUtil.addListItem(getKeyWordsSearch(search_type), histroy);
    }

    private String getPrivilegeKey(String type) {
        String key = "apicache__" + getCity() + "_";
        String suffix = StringUtils.EMPTY;
        if (type.equals(App.Categroy.Event.COUPON)) {
            suffix = App.Categroy.Event.COUPON;
        }
        if (type.equals("event")) {
            suffix = "event";
        }
        if (type.equals(App.Categroy.Event.TJF)) {
            suffix = App.Categroy.Event.TJF;
        }
        if (type.equals(App.Categroy.Event.TLF)) {
            suffix = App.Categroy.Event.TLF;
        }
        return String.valueOf(key) + suffix;
    }

    public void saveEvent(List<Event> event_list, String type) {
        if (event_list == null) {
            event_list = new ArrayList<>();
        }
        this.sharedPrefsUtil.putList(getPrivilegeKey(type), event_list);
    }

    public List<Event> getEvent(String type) throws ReflectException, JSONException {
        return this.sharedPrefsUtil.getListWithCast(new Event(), getPrivilegeKey(type));
    }

    public void saveMySignList(List<Event> event_list, String type) {
        if (event_list == null) {
            event_list = new ArrayList<>();
        } else {
            this.sharedPrefsUtil.clean(getMySignKey(type));
        }
        this.sharedPrefsUtil.putList(getMySignKey(type), event_list);
    }

    public List<Event> getMySignList(String type) throws ReflectException, JSONException {
        return this.sharedPrefsUtil.getListWithCast(new Event(), getMySignKey(type));
    }

    public void delMySignList(HouseInfo houseInfo, String type) {
        this.sharedPrefsUtil.removeListItem(getMySignKey(type), houseInfo);
    }

    private String getMySignKey(String type) {
        String key = "apicache__" + getCity() + "_" + getMobile() + "_";
        String suffix = StringUtils.EMPTY;
        if (type.equals(App.Categroy.Event.COUPON)) {
            suffix = App.Categroy.Event.COUPON;
        }
        if (type.equals("event")) {
            suffix = "event";
        }
        if (type.equals(App.Categroy.Event.TJF)) {
            suffix = App.Categroy.Event.TJF;
        }
        if (type.equals(App.Categroy.Event.TLF)) {
            suffix = App.Categroy.Event.TLF;
        }
        return String.valueOf(key) + suffix;
    }

    public void saveBrowseHistory(HouseInfo houseInfo, String info_type) {
        if (hasBrowse(houseInfo, info_type)) {
            delBrowseHistory(houseInfo, info_type);
        }
        int hcount = 0;
        List<HouseInfo> houseInfos = null;
        try {
            houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(3, info_type));
            hcount = houseInfos.size();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (hcount >= this.maxcount) {
            this.sharedPrefsUtil.removeListItem(getHistoryKey(3, info_type), (HouseInfo) houseInfos.get(0));
        }
        this.sharedPrefsUtil.addListItem(getHistoryKey(3, info_type), houseInfo);
    }

    public void delBrowseHistory(HouseInfo houseInfo, String info_type) {
        this.sharedPrefsUtil.removeListItem(getHistoryKey(3, info_type), houseInfo);
    }

    public List<HouseInfo> getBrowseHistoryWithCast(String info_type) throws ReflectException, JSONException {
        List<HouseInfo> houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(3, info_type));
        Collections.reverse(houseInfos);
        return houseInfos;
    }

    public boolean hasBrowse(HouseInfo houseInfo, String info_type) {
        return this.sharedPrefsUtil.hasListItem(getHistoryKey(3, info_type), houseInfo);
    }

    public void saveFavHistory(HouseInfo houseInfo, String info_type) {
        if (hasFav(houseInfo, info_type)) {
            delFavHistory(houseInfo, info_type);
        }
        int hcount = 0;
        List<HouseInfo> houseInfos = null;
        try {
            houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(1, info_type));
            hcount = houseInfos.size();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (hcount >= this.maxcount) {
            this.sharedPrefsUtil.removeListItem(getHistoryKey(1, info_type), (HouseInfo) houseInfos.get(0));
        }
        this.sharedPrefsUtil.addListItem(getHistoryKey(1, info_type), houseInfo);
    }

    public void delFavHistory(HouseInfo houseInfo, String info_type) {
        this.sharedPrefsUtil.removeListItem(getHistoryKey(1, info_type), houseInfo);
    }

    public List<HouseInfo> getFavHistoryWithCast(String info_type) throws ReflectException, JSONException {
        List<HouseInfo> houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(1, info_type));
        Collections.reverse(houseInfos);
        return houseInfos;
    }

    public boolean hasFav(HouseInfo houseInfo, String info_type) {
        return this.sharedPrefsUtil.hasListItem(getHistoryKey(1, info_type), houseInfo);
    }

    public void saveCallHistory(HouseInfo houseInfo, String info_type) {
        if (hasCall(houseInfo, info_type)) {
            delCallHistory(houseInfo, info_type);
        }
        int hcount = 0;
        List<HouseInfo> houseInfos = null;
        try {
            houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(2, info_type));
            hcount = houseInfos.size();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (hcount >= this.maxcount) {
            this.sharedPrefsUtil.removeListItem(getHistoryKey(2, info_type), (HouseInfo) houseInfos.get(0));
        }
        this.sharedPrefsUtil.addListItem(getHistoryKey(2, info_type), houseInfo);
    }

    public boolean hasCall(HouseInfo houseInfo, String info_type) {
        return this.sharedPrefsUtil.hasListItem(getHistoryKey(2, info_type), houseInfo);
    }

    public void delCallHistory(HouseInfo houseInfo, String info_type) {
        this.sharedPrefsUtil.removeListItem(getHistoryKey(2, info_type), houseInfo);
    }

    public List<HouseInfo> getCallHistoryWithCast(String info_type) throws ReflectException, JSONException {
        List<HouseInfo> houseInfos = this.sharedPrefsUtil.getListWithCast(new HouseInfo(), getHistoryKey(2, info_type));
        Collections.reverse(houseInfos);
        return houseInfos;
    }

    public boolean hasPublishRec(Object o, int keyType, int type) {
        return this.sharedPrefsUtil.hasListItem(getKey(keyType, type), (PublishHouse) o);
    }

    public void addPublishRec(Object o, int keyType, int type) {
        if (hasPublishRec(o, keyType, type)) {
            removePublishRec(o, keyType, type);
        }
        List<PublishHouse> records = null;
        int r_count = 0;
        try {
            records = queryPublishRec(keyType, type);
            r_count = records.size();
        } catch (ReflectException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String key = getKey(keyType, type);
        if (r_count >= 50) {
            this.sharedPrefsUtil.removeListItem(key, records.get(0));
        }
        this.sharedPrefsUtil.addListItem(key, (PublishHouse) o);
    }

    public void removePublishRec(Object o, int keyType, int type) {
        this.sharedPrefsUtil.removeListItem(getKey(keyType, type), (PublishHouse) o);
    }

    public List<PublishHouse> queryPublishRec(int keyType, int type) throws ReflectException, JSONException {
        String key = getKey(keyType, type);
        new ArrayList();
        List<PublishHouse> publishHouses = this.sharedPrefsUtil.getListWithCast(new PublishHouse(), key);
        Collections.reverse(publishHouses);
        return publishHouses;
    }

    private String getKey(int keyType, int contentType) {
        String prefix = StringUtils.EMPTY;
        if (keyType == 10) {
            if (contentType == 4) {
                prefix = KEY_PUBLISH_SELL;
            } else if (contentType == 5) {
                prefix = KEY_PUBLISH_RENT;
            }
        }
        if (keyType == 11) {
            if (contentType == 4) {
                prefix = KEY_OFFER_SELL;
            } else if (contentType == 5) {
                prefix = KEY_OFFER_RENT;
            }
        }
        return "apicache_" + prefix + getCity();
    }

    private String getHistoryKey(int type, String info_type) {
        String suffix;
        String key = "apicache__" + getCity() + "_";
        switch (type) {
            case 1:
                suffix = "fav_" + info_type;
                break;
            case 2:
                suffix = "call_" + info_type;
                break;
            case 3:
                suffix = "browse_" + info_type;
                break;
            case 12:
                suffix = "award_" + info_type;
                break;
            default:
                suffix = StringUtils.EMPTY;
                break;
        }
        return String.valueOf(key) + suffix;
    }

    public void saveNewsHistory(News news) {
        List<News> newses = null;
        if (!hasNews(news)) {
            int hcount = 0;
            try {
                newses = this.sharedPrefsUtil.getListWithCast(new News(), "apicache_" + getCity() + "_" + App.Categroy.News.NEWS);
                hcount = newses.size();
            } catch (Exception e) {
                CorePreferences.ERROR(e);
            }
            if (hcount >= this.maxcount) {
                this.sharedPrefsUtil.removeListItem("apicache_" + getCity() + "_" + App.Categroy.News.NEWS, (News) newses.get(0));
            }
            this.sharedPrefsUtil.addListItem("apicache_" + getCity() + "_" + App.Categroy.News.NEWS, news);
        }
    }

    public News getNews(String id) {
        try {
            for (News news : this.sharedPrefsUtil.getListWithCast(new News(), "apicache_" + getCity() + "_" + App.Categroy.News.NEWS)) {
                if (news.getN_id().equals(id)) {
                    return news;
                }
            }
            return null;
        } catch (Exception e) {
            CorePreferences.ERROR(e);
            return null;
        }
    }

    public boolean hasNews(News news) {
        return this.sharedPrefsUtil.hasListItem("apicache_" + getCity() + "_" + App.Categroy.News.NEWS, news);
    }

    public void clearPrefsCache() {
        this.sharedPrefsUtil.cleanContain("apicache_");
    }

    private boolean hasFull(String key, int max) {
        int hcount = 0;
        try {
            hcount = this.sharedPrefsUtil.getList(key).length();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hcount >= max;
    }
}
