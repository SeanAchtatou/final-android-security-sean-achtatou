package com.house365.newhouse.constant;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.reflect.ReflectException;
import com.house365.core.reflect.ReflectUtil;
import com.house365.newhouse.application.NewHouseApplication;
import com.house365.newhouse.model.VirtualCity;
import com.house365.newhouse.task.GetCityTask;
import com.house365.newhouse.tool.ActionCode;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class AppArrays {
    public static VirtualCity[] cityArray;
    private static Map<String, ForumInfo> cityFourm;
    public static Map<String, String> cityLocation;
    public static HashSet<VirtualCity> citySet;
    /* access modifiers changed from: private */
    public static String city_key = StringUtils.EMPTY;
    public static Map<String, String> citys;
    public static Context context;
    public static String default_tel = "400-8181-365";
    public static String[] editMenu = {"删除"};
    public static int fromNet;
    private static String[] hall;
    public static String[] localCity = {"nj", "hf", "wx", "wh", "hz", "cz", "sz", "ks", "xa", "cq"};
    public static List<String> localCityName;
    /* access modifiers changed from: private */
    public static double[] location = null;
    private static String[] radious;
    private static String[] room;
    private static String showname = StringUtils.EMPTY;
    private static String[] toilet;
    private VirtualCity v_city;

    public static String[] getRadious() {
        if (radious == null) {
            radious = new String[]{"1km", "2.5km", "5km", "10km", "全城"};
        }
        return radious;
    }

    public static String[] getRoom() {
        if (room == null) {
            room = new String[]{ActionCode.PREFERENCE_SEARCH_NOCHOSE, "一室", "二室", "三室", "四室", "五室", "六室", "七室", "八室", "九室", "十室"};
        }
        return room;
    }

    public static String[] getHall() {
        if (hall == null) {
            hall = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        }
        return room;
    }

    public static String[] getToilet() {
        if (toilet == null) {
            toilet = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        }
        return room;
    }

    public static List<String> getLocalCityNames() {
        localCityName = new ArrayList();
        localCityName.add("南京");
        localCityName.add("合肥");
        localCityName.add("无锡");
        localCityName.add("芜湖");
        localCityName.add("杭州");
        localCityName.add("常州");
        localCityName.add("苏州");
        localCityName.add("昆山");
        localCityName.add("西安");
        localCityName.add("重庆");
        localCityName.add("沈阳");
        return localCityName;
    }

    public static Map<String, String> getCitys() {
        return citys;
    }

    public static VirtualCity[] getCityList() {
        if (citySet == null || citySet.isEmpty()) {
            initVirtualCity(getVirtualCityJson());
        } else {
            cityArray = new VirtualCity[citySet.size()];
            Iterator<VirtualCity> iter = citySet.iterator();
            int i = 0;
            while (iter.hasNext()) {
                VirtualCity item = iter.next();
                if (item != null && i < cityArray.length - 1) {
                    cityArray[i] = item;
                    i++;
                }
            }
        }
        return cityArray;
    }

    public static String getCityKey(final String citypy) {
        if (cityArray != null) {
            for (VirtualCity v_city2 : cityArray) {
                if (v_city2 != null && v_city2.getCity_py().equals(citypy)) {
                    city_key = v_city2.getCity_key();
                }
            }
        } else if (!GetCityTask.isdoing) {
            GetCityTask getCity = new GetCityTask(context);
            getCity.setCityFinishListener(new GetCityTask.GetCityFinishListener() {
                public void onFinish() {
                    AppArrays.cityArray = AppArrays.getCityList();
                    for (VirtualCity v_city : AppArrays.cityArray) {
                        if (v_city != null && !TextUtils.isEmpty(v_city.getCity_py()) && v_city.getCity_py().equals(citypy)) {
                            AppArrays.city_key = v_city.getCity_key();
                        }
                    }
                }
            });
            getCity.execute(new Object[0]);
        }
        return city_key;
    }

    public static String[] getCityShowNames() {
        return (String[]) citys.keySet().toArray(new String[0]);
    }

    public static String getCity(final String city_showname) {
        if (citys != null) {
            city_key = citys.get(city_showname);
        } else if (context != null && !GetCityTask.isdoing) {
            GetCityTask getCity = new GetCityTask(context);
            getCity.setCityFinishListener(new GetCityTask.GetCityFinishListener() {
                public void onFinish() {
                    AppArrays.city_key = AppArrays.citys.get(city_showname);
                }
            });
            getCity.execute(new Object[0]);
        }
        return city_key;
    }

    public static String getCityShowname(final String city) {
        if (citys != null) {
            setName(city);
        } else if (context != null && !GetCityTask.isdoing) {
            GetCityTask getCity = new GetCityTask(context);
            getCity.setCityFinishListener(new GetCityTask.GetCityFinishListener() {
                public void onFinish() {
                    AppArrays.setName(city);
                }
            });
            getCity.execute(new Object[0]);
        }
        return showname;
    }

    /* access modifiers changed from: private */
    public static synchronized void setName(String city) {
        synchronized (AppArrays.class) {
            boolean exist = false;
            if (citys == null || citys.isEmpty()) {
                initVirtualCity(getVirtualCityJson());
            }
            if (citys != null && !citys.isEmpty()) {
                Iterator iter = citys.entrySet().iterator();
                while (true) {
                    if (iter.hasNext()) {
                        Map.Entry<String, String> entry = iter.next();
                        if (entry != null && city.equals(((String) entry.getValue()).toString())) {
                            exist = true;
                            showname = (String) entry.getKey();
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!exist) {
                    ((NewHouseApplication) context).setCity(getCity(CorePreferences.getInstance(context).getCoreConfig().getDefaultCity()));
                    context.sendBroadcast(new Intent(ActionCode.INTENT_ACTION_CHANGE_CITY));
                }
            }
        }
    }

    public static double[] getCiytLoaction(final String city) {
        if (cityLocation != null) {
            String s = cityLocation.get(city);
            if (!TextUtils.isEmpty(s) && s.indexOf(",") != -1) {
                location = new double[2];
                location[0] = Double.parseDouble(s.split(",")[0]);
                location[1] = Double.parseDouble(s.split(",")[1]);
            }
        } else if (context != null && !GetCityTask.isdoing) {
            GetCityTask getCity = new GetCityTask(context);
            getCity.setCityFinishListener(new GetCityTask.GetCityFinishListener() {
                public void onFinish() {
                    String s = AppArrays.cityLocation.get(city);
                    if (!TextUtils.isEmpty(s) && s.indexOf(",") != -1) {
                        AppArrays.location = new double[2];
                        AppArrays.location[0] = Double.parseDouble(s.split(",")[0]);
                        AppArrays.location[1] = Double.parseDouble(s.split(",")[1]);
                    }
                }
            });
            getCity.execute(new Object[0]);
        }
        return location;
    }

    public static void initCityForums() {
        cityFourm = new HashMap();
        cityFourm.put("nj", new ForumInfo("nj", "com.house365.bbs.activity", "华侨路茶坊", "http://app.house365.com/d/chafang.apk", true));
        cityFourm.put("hf", new ForumInfo("hf", "cc.hefei.bbs.ui", "合肥论坛", "http://app.house365.com/d/Hefeibbs.apk", true));
        cityFourm.put("wx", new ForumInfo("wx", "com.house365.wxbbs.activity", "蠡园茶坊 ", "http://app.house365.com/d/chafang_wx.apk", true));
        cityFourm.put("wh", new ForumInfo("wh", "com.house365.whbbs.activity", "鸠兹茶坊 ", "http://app.house365.com/d/jiuzi.apk", true));
        cityFourm.put("hz", new ForumInfo("hz", StringUtils.EMPTY, StringUtils.EMPTY, "http://m.hz.house365.com/?app=bbs", false));
        cityFourm.put("cz", new ForumInfo("cz", StringUtils.EMPTY, StringUtils.EMPTY, "http://m.cz.house365.com/?app=bbs", false));
        cityFourm.put("sz", new ForumInfo("sz", "com.house365.szbbs.activity", "姑苏茶坊", "http://app.house365.com/d/chafang_sz.apk", true));
        cityFourm.put("ks", new ForumInfo("ks", StringUtils.EMPTY, StringUtils.EMPTY, "http://m.ks.house365.com/?app=bbs", false));
        cityFourm.put("xa", new ForumInfo("xa", StringUtils.EMPTY, StringUtils.EMPTY, "http://m.xa.house365.com/?app=bbs", false));
        cityFourm.put("cq", new ForumInfo("cq", StringUtils.EMPTY, StringUtils.EMPTY, "http://m.cq.house365.com/?app=bbs", false));
        cityFourm.put("sy", new ForumInfo("sy", StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, false));
    }

    public static ForumInfo getCityForum(String city) {
        if (cityFourm == null) {
            initCityForums();
        }
        return cityFourm.get(city);
    }

    public static boolean existCityForum(String city) {
        ForumInfo s = getCityForum(city);
        if (s == null || TextUtils.isEmpty(s.getUrl())) {
            return false;
        }
        return true;
    }

    public static String getVirtualCityJson() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            InputStream inputStream = context.getAssets().open("virtual_city.txt");
            byte[] buf = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
            while (true) {
                int len = inputStream.read(buf);
                if (len == -1) {
                    break;
                }
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {
        }
        return outputStream.toString();
    }

    public static VirtualCity[] initVirtualCity(String json) {
        if (GetCityTask.reset() && citySet.isEmpty()) {
            try {
                JSONArray aJsonArray = new JSONArray(json);
                if (aJsonArray != null) {
                    try {
                        VirtualCity item = new VirtualCity();
                        for (int i = 0; i < aJsonArray.length(); i++) {
                            item = (VirtualCity) ReflectUtil.copy(item.getClass(), aJsonArray.getJSONObject(i));
                            if (item != null && !citySet.contains(item)) {
                                citySet.add(item);
                                citys.put(item.getCity_name(), item.getCity_py());
                                cityLocation.put(item.getCity_py(), String.valueOf(item.getCity_y()) + "," + item.getCity_x());
                            }
                        }
                    } catch (JSONException e) {
                        e = e;
                        e.printStackTrace();
                        return getCityList();
                    } catch (ReflectException e2) {
                        e = e2;
                        e.printStackTrace();
                        return getCityList();
                    }
                }
            } catch (JSONException e3) {
                e = e3;
                e.printStackTrace();
                return getCityList();
            } catch (ReflectException e4) {
                e = e4;
                e.printStackTrace();
                return getCityList();
            }
        }
        return getCityList();
    }
}
