package com.house365.newhouse.constant;

public class ForumInfo {
    private String city;
    private boolean isApp;
    private String name;
    private String pkg;
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public boolean isApp() {
        return this.isApp;
    }

    public void setApp(boolean isApp2) {
        this.isApp = isApp2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getPkg() {
        return this.pkg;
    }

    public void setPkg(String pkg2) {
        this.pkg = pkg2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public ForumInfo(String city2, String pkg2, String name2, String url2, boolean isApp2) {
        this.city = city2;
        this.pkg = pkg2;
        this.name = name2;
        this.url = url2;
        this.isApp = isApp2;
    }
}
