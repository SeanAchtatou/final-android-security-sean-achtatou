package com.house365.newhouse.constant;

public interface App {
    public static final String BLOCK = "block";
    public static final int CONN_TYPE_DIAL = 2;
    public static final int CONN_TYPE_MSG = 1;
    public static final int FROM_BUS = -3;
    public static final int FROM_LITTER_HOME = -5;
    public static final int FROM_REGION = -1;
    public static final int FROM_SUBWAY = -2;
    public static final String Home = "home";
    public static final String NEW = "newhouse";
    public static final String NEW_DETAIL_BASE = "newhouse_detail";
    public static final String PULL_ACCOUNT_PASSWORD = "123456";
    public static final String RENT = "rent";
    public static final String SELL = "sell";
    public static final int TYPE_BLOCK = 3;
    public static final int TYPE_BUS = 4;
    public static final int TYPE_HOUSE_RENT = 2;
    public static final int TYPE_HOUSE_SELL = 1;
    public static final int TYPE_PUBLISH_RENT = 5;
    public static final int TYPE_PUBLISH_SELL = 4;
    public static final String[] editMenu = {"删除"};

    public static class Ad {
        public static final int FULL_SCREEN_CLOSE_TIME = 5000;
        public static final int HOME_SCROLL = 5;
        public static final int NEW_FULL_SCREEN = 1;
        public static final int NEW_SCROLL = 2;
        public static final int RENT_SCROLL = 4;
        public static final int SELL_SCROLL = 3;
    }

    public static class HousePhoto {
        public static final int PHOTO_ALBUM = -13;
        public static final int PHOTO_CAMERA = -12;
        public static final int PHOTO_NONE = 0;
        public static final int PHOTO_RESULT = -14;
    }

    public static class Map {
        public static final int EVENT = 14;
        public static final int MAP_ZOOM_LEVEL = 15;
        public static final int NEW = 11;
        public static final int RENT = 13;
        public static final int SELL = 12;
    }

    public static class Sensor {
        public static final int SENSOR_MIN_DIS = 10;
    }

    public static class Categroy {

        public class News {
            public static final int CHANNEL_NEW_GUIDE = 2;
            public static final int CHANNEL_NEW_HOT = 1;
            public static final int CHANNEL_SECOND_GUIDE = 4;
            public static final int CHANNEL_SECOND_HOT = 3;
            public static final String NEWS = "news";

            public News() {
            }
        }

        public class House {
            public static final String HOUSE = "house";

            public House() {
            }
        }

        public class Block {
            public static final String BLOCK = "block";

            public Block() {
            }
        }

        public class Event {
            public static final String COUPON = "coupon";
            public static final String EVENT = "event";
            public static final String LOTTERY = "lottery";
            public static final String SELL_EVENT = "event_secondhouse";
            public static final String TJF = "tjf";
            public static final String TLF = "tlf";

            public Event() {
            }
        }

        public class Link {
            public static final String HREF = "href";

            public Link() {
            }
        }
    }
}
