package com.house365.newhouse.constant;

public class Order {
    public static final int AREA_DOWN = 6;
    public static final int AREA_UP = 5;
    public static final String BUILDAREA = "buildarea";
    public static final int DEFAULT = -1;
    public static final String PRICE = "price";
    public static final int PRICE_DOWN = 4;
    public static final int PRICE_UP = 3;
    public static final String RATIO = "ratio";
    public static final int RATIO_DOWN = 10;
    public static final int RATIO_UP = 9;
    public static final String UPDATETIME = "updatetime";
    public static final int UPDATETIME_DOWN = 2;
    public static final int UPDATETIME_UP = 1;

    public static String getOrderBy(int order) {
        if (order == 1 || order == 2) {
            return UPDATETIME;
        }
        if (order == 3 || order == 4) {
            return "price";
        }
        if (order == 5 || order == 6) {
            return "buildarea";
        }
        if (order == 9 || order == 10) {
            return RATIO;
        }
        return null;
    }

    public static String getAsc(int order) {
        if (order == 1 || order == 3 || order == 5 || order == 9) {
            return "ASC";
        }
        if (order == 2 || order == 4 || order == 6 || order == 10) {
            return "DESC";
        }
        return null;
    }
}
