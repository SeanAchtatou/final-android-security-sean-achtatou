package com.house365.core.action;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class TabChangeReceiver extends BroadcastReceiver {
    private final TabChanger tabChanger;

    public TabChangeReceiver(TabChanger tabChanger2) {
        this.tabChanger = tabChanger2;
    }

    public void onReceive(Context context, Intent intent) {
        if (ActionTag.INTENT_ACTION_CHANGE_TAB.equals(intent.getAction())) {
            this.tabChanger.changeTab(intent.getIntExtra("tab_index", 0));
        }
    }
}
