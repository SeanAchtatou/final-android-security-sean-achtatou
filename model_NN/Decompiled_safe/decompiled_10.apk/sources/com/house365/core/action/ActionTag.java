package com.house365.core.action;

public class ActionTag {
    public static final String INTENT_ACTION_CAPTURE_FINISH = "com.house365.core.action.CAPTURE_FINISH";
    public static final String INTENT_ACTION_CHANGE_CITY = "com.house365.core.action.CHANGE_CITY";
    public static final String INTENT_ACTION_CHANGE_TAB = "com.house365.core.action.CHANGE_TAB";
    public static final String INTENT_ACTION_LOGGED_IN = "com.house365.core.action.LOGGED_IN";
    public static final String INTENT_ACTION_LOGGED_OUT = "com.house365.core.action.LOGGED_OUT";
    public static final String INTENT_ACTION_LOGGED_OUT_ACCOUNT = "com.house365.core.action.LOGGED_OUT_ACCOUNT";
    public static final String INTENT_ACTION_REFRESH_USER_INFO = "com.house365.core.action.REFRESH_USER_INFO";
    public static final String INTENT_ACTION_WEIXIN = "com.tencent.mm";
}
