package com.house365.core.action;

public interface TabChanger {
    void changeTab(int i);
}
