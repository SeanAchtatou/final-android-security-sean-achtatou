package com.house365.core.application;

import android.app.Application;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.house365.app.analyse.AnalyseConfig;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.core.constant.CorePreferences;
import com.house365.core.error.CrashHandler;
import com.house365.core.http.BaseHttpAPI;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.FileUtil;
import com.house365.core.util.PackageUtil;
import com.house365.core.util.appoptimizer.MemoryOptimizer;
import com.house365.core.util.store.BaseDiskCache;
import com.house365.core.util.store.DiskCache;
import com.house365.core.util.store.NullDiskCache;
import com.house365.core.util.store.SharedPreferencesUtil;

public abstract class BaseApplication<T extends BaseHttpAPI> extends Application {
    public CorePreferences AppCore;
    private final String KEY_ENABLE_IMG = "KEY_ENABLE_IMG";
    private final String KEY_ENABLE_PUSH_NOTIFICATION = "KEY_ENABLE_PUSH_NOTIFICATION";
    private final String KEY_FIRST_USE = "KEY_FIRST_USE";
    /* access modifiers changed from: private */
    public AnalyseMetaData analyseMetaData;
    protected T api;
    protected float density;
    private String deviceId;
    protected DiskCache diskcache;
    protected Location location;
    private SharedPreferences mPrefs;
    protected DisplayMetrics metrics;
    private int screenHeight;
    private int screenWidth;
    protected SharedPreferencesUtil sharedPrefsUtil;
    private String version;

    public abstract String getCityName();

    /* access modifiers changed from: protected */
    public abstract T instanceApi();

    public abstract void onAppCancel();

    /* access modifiers changed from: protected */
    public abstract void onAppCreate();

    /* access modifiers changed from: protected */
    public abstract void onAppDestory();

    public void onCreate() {
        super.onCreate();
        this.AppCore = CorePreferences.getInstance(this);
        CrashHandler.getInstance().init(getApplicationContext());
        new MemoryOptimizer().optimize();
        this.sharedPrefsUtil = new SharedPreferencesUtil(this);
        this.mPrefs = this.sharedPrefsUtil.getSharedPreferences();
        if (FileUtil.isSDCARDMounted()) {
            this.diskcache = new BaseDiskCache(this.AppCore.getCoreConfig().getAppTag(), CorePreferences.CACHEPATH);
        } else {
            this.diskcache = new BaseDiskCache(getCacheDir().getPath(), CorePreferences.CACHEPATH);
        }
        if (!this.diskcache.aviable()) {
            this.diskcache = new NullDiskCache();
        }
        this.api = instanceApi();
        initWindow();
        setDeviceId(DeviceUtil.getDeviceId(this));
        setVersion(PackageUtil.getVersion(this));
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            HouseAnalyse.setInitConfig(new AnalyseConfig(CorePreferences.getInstance(this).getCoreConfig().getAnalyseUrl(), this.metrics, CorePreferences.getInstance(this).getCoreConfig().getAppName(), CorePreferences.getInstance(this).getCoreConfig().getAnalyseBufferSize(), CorePreferences.getInstance(this).getCoreConfig().getAnalyseChannel()));
            this.analyseMetaData = HouseAnalyse.onAppStart(this);
        }
        onAppCreate();
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId2) {
        this.deviceId = deviceId2;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public void initWindow() {
        WindowManager wm = (WindowManager) getApplicationContext().getSystemService("window");
        this.screenWidth = wm.getDefaultDisplay().getWidth();
        this.screenHeight = wm.getDefaultDisplay().getHeight();
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        setMetrics(dm);
        setDensity(dm.density);
    }

    public SharedPreferencesUtil getSharedPrefsUtil() {
        return this.sharedPrefsUtil;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public DiskCache getDiskCache() {
        return this.diskcache;
    }

    public SharedPreferences getSharedPreferences() {
        return this.mPrefs;
    }

    public void setDensity(float density2) {
        this.density = density2;
    }

    public float getDensity() {
        return this.density;
    }

    public T getApi() {
        return this.api;
    }

    public DisplayMetrics getMetrics() {
        return this.metrics;
    }

    public void setMetrics(DisplayMetrics metrics2) {
        this.metrics = metrics2;
    }

    public void cleanAllFile() {
        try {
            FileUtil.delAllFile(CorePreferences.getAppTmpSDPath());
            FileUtil.delAllFile(CorePreferences.getAppDownloadSDPath());
            FileUtil.delAllFile(CorePreferences.getAppImageSDPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enableImg(boolean enable) {
        this.sharedPrefsUtil.putBoolean("KEY_ENABLE_IMG", enable);
    }

    public boolean isEnableImg() {
        return this.sharedPrefsUtil.getBoolean("KEY_ENABLE_IMG", true);
    }

    public void setIsFirst(boolean first) {
        this.sharedPrefsUtil.putBoolean("KEY_FIRST_USE", first);
    }

    public boolean isFirst() {
        return this.sharedPrefsUtil.getBoolean("KEY_FIRST_USE", true);
    }

    public void setIsFirstVersion(boolean first) {
        this.sharedPrefsUtil.putBoolean("KEY_FIRST_USE" + PackageUtil.getVersion(this), first);
    }

    public boolean isFirstVersion() {
        return this.sharedPrefsUtil.getBoolean("KEY_FIRST_USE" + PackageUtil.getVersion(this), true);
    }

    public void enablePushNotification(boolean enable) {
        this.sharedPrefsUtil.putBoolean("KEY_ENABLE_PUSH_NOTIFICATION", enable);
    }

    public boolean isEnablePushNotification() {
        return this.sharedPrefsUtil.getBoolean("KEY_ENABLE_PUSH_NOTIFICATION", true);
    }

    public void onTerminate() {
        super.onTerminate();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            new Thread() {
                public void run() {
                    if (BaseApplication.this.analyseMetaData != null) {
                        HouseAnalyse.onAppDestory(BaseApplication.this, BaseApplication.this.analyseMetaData);
                    }
                    HouseAnalyse.flush();
                }
            }.start();
        }
        onAppDestory();
    }

    public AnalyseMetaData getAnalyseMetaData() {
        return this.analyseMetaData;
    }
}
