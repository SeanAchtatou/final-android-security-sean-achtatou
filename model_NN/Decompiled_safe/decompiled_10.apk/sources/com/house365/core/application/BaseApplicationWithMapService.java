package com.house365.core.application;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.mapapi.BMapManager;
import com.house365.core.http.BaseHttpAPI;
import com.house365.core.util.lbs.OnLocationChangeListener;
import org.apache.commons.lang.StringUtils;

public abstract class BaseApplicationWithMapService<T extends BaseHttpAPI> extends BaseApplication<T> {
    protected LocationClient bLocationClient;
    protected BMapManager bMapManager;
    /* access modifiers changed from: private */
    public OnLocationChangeListener locationChangerListener;

    public void onCreate() {
        super.onCreate();
        getbLocationClient().registerLocationListener(new MyLocationListenner());
    }

    public void onTerminate() {
        super.onTerminate();
    }

    public BMapManager getbMapManager() {
        if (this.bMapManager == null) {
            String appBaiduMapKey = this.AppCore.getCoreConfig().getAppBaiduMapKey();
            if (!StringUtils.EMPTY.equals(this.AppCore.getCoreConfig().getAppBaiduMapKey())) {
                this.bMapManager = new BMapManager(this);
                this.bMapManager.init(this.AppCore.getCoreConfig().getAppBaiduMapKey(), null);
                this.bMapManager.getLocationManager().setNotifyInternal(10, 5);
            }
        }
        return this.bMapManager;
    }

    public void setbMapManager(BMapManager bMapManager2) {
        this.bMapManager = bMapManager2;
    }

    public LocationClient getbLocationClient() {
        if (this.bLocationClient == null) {
            this.bLocationClient = new LocationClient(this);
        }
        return this.bLocationClient;
    }

    public class MyLocationListenner implements BDLocationListener {
        public MyLocationListenner() {
        }

        public void onReceiveLocation(BDLocation location) {
            if (location != null) {
                if ((location.getLocType() == 61 || location.getLocType() == 65 || location.getLocType() == 68 || location.getLocType() == 161) && BaseApplicationWithMapService.this.locationChangerListener != null) {
                    BaseApplicationWithMapService.this.locationChangerListener.onGetLocation(location);
                }
            }
        }

        public void onReceivePoi(BDLocation poiLocation) {
            if (poiLocation != null) {
                if ((poiLocation.getLocType() == 61 || poiLocation.getLocType() == 65 || poiLocation.getLocType() == 68 || poiLocation.getLocType() == 161) && BaseApplicationWithMapService.this.locationChangerListener != null) {
                    BaseApplicationWithMapService.this.locationChangerListener.onGetPoi(poiLocation);
                }
            }
        }
    }

    public void removeLocationChangerListener() {
        this.locationChangerListener = null;
    }

    public void setLocationChangerListener(OnLocationChangeListener locationChangerListener2) {
        this.locationChangerListener = locationChangerListener2;
    }
}
