package com.house365.core.bean;

import android.database.Cursor;
import com.house365.core.reflect.ReflectException;
import com.house365.core.reflect.ReflectUtil;
import java.io.Serializable;
import java.lang.reflect.Field;

public class BaseBean implements Serializable {
    private static final long serialVersionUID = -3228723769762271774L;

    public static boolean isContainColumn(String colName, Cursor rs) {
        for (String s : rs.getColumnNames()) {
            if (s.equalsIgnoreCase(colName)) {
                return true;
            }
        }
        return false;
    }

    public void readFromCursor(Cursor cursor) {
        String s;
        for (Field f : getClass().getDeclaredFields()) {
            f.setAccessible(true);
            String name = f.getName();
            if (isContainColumn(name, cursor) && (s = cursor.getString(cursor.getColumnIndex(name))) != null && ReflectUtil.isSimpleDataType(f.getType())) {
                try {
                    f.set(this, ReflectUtil.getObjectFromString(f.getType(), s));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                } catch (ReflectException e3) {
                    e3.printStackTrace();
                }
            }
        }
    }
}
