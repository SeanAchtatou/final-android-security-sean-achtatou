package com.house365.core.bean;

import com.house365.core.util.store.SharedPreferencesDTO;

public class CoreConfig extends SharedPreferencesDTO<CoreConfig> {
    private static final long serialVersionUID = 7707855148878171091L;
    private int analyseBufferSize = 10;
    private String analyseChannel;
    private String analyseUrl;
    private String appBaiduMapKey;
    private String appName;
    private String appPath;
    private String appTag;
    private String defaultCity;
    private boolean defaultCityForce;
    private boolean isAnalyse;
    private boolean isDebug;
    private boolean isOpenBaiduStat;

    public boolean isDebug() {
        return this.isDebug;
    }

    public String getAppPath() {
        return this.appPath;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getAppTag() {
        return this.appTag;
    }

    public void setAppTag(String appTag2) {
        this.appTag = appTag2;
    }

    public String getAppBaiduMapKey() {
        return this.appBaiduMapKey;
    }

    public void setDebug(boolean isDebug2) {
        this.isDebug = isDebug2;
    }

    public void setAppPath(String appPath2) {
        this.appPath = appPath2;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public void setAppBaiduMapKey(String appBaiduMapKey2) {
        this.appBaiduMapKey = appBaiduMapKey2;
    }

    public boolean isSame(CoreConfig o) {
        return false;
    }

    public boolean isAnalyse() {
        return this.isAnalyse;
    }

    public void setAnalyse(boolean isAnalyse2) {
        this.isAnalyse = isAnalyse2;
    }

    public String getAnalyseUrl() {
        return this.analyseUrl;
    }

    public void setAnalyseUrl(String analyseUrl2) {
        this.analyseUrl = analyseUrl2;
    }

    public int getAnalyseBufferSize() {
        return this.analyseBufferSize;
    }

    public void setAnalyseBufferSize(int analyseBufferSize2) {
        this.analyseBufferSize = analyseBufferSize2;
    }

    public String getAnalyseChannel() {
        return this.analyseChannel;
    }

    public void setAnalyseChannel(String analyseChannel2) {
        this.analyseChannel = analyseChannel2;
    }

    public boolean isOpenBaiduStat() {
        return this.isOpenBaiduStat;
    }

    public void setOpenBaiduStat(boolean isOpenBaiduStat2) {
        this.isOpenBaiduStat = isOpenBaiduStat2;
    }

    public String getDefaultCity() {
        return this.defaultCity;
    }

    public void setDefaultCity(String defaultCity2) {
        this.defaultCity = defaultCity2;
    }

    public boolean isDefaultCityForce() {
        return this.defaultCityForce;
    }

    public void setDefaultCityForce(boolean defaultCityForce2) {
        this.defaultCityForce = defaultCityForce2;
    }
}
