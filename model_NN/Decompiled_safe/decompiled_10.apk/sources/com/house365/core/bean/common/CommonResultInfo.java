package com.house365.core.bean.common;

import com.house365.core.bean.BaseBean;

public class CommonResultInfo extends BaseBean {
    public static final int RESULT_FAILURE = 0;
    public static final int RESULT_OK = 1;
    private static final long serialVersionUID = -4138173548875938829L;
    private String data;
    private String msg;
    private int result;

    public int getResult() {
        return this.result;
    }

    public void setResult(int result2) {
        this.result = result2;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg2) {
        this.msg = msg2;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data2) {
        this.data = data2;
    }
}
