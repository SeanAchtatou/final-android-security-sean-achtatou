package com.house365.core.bean.common;

public class CommonTaskInfo<T> {
    public static final int HTTP_REQUEST_ERROR = 2;
    public static final int NETWORK_UNAVAILABLE = 1;
    public static final int PARSE_ERROR = 3;
    public static final int SUCCESS = 0;
    public int result;
    private T t;

    public void setData(T t2) {
        this.t = t2;
    }

    public T getData() {
        return this.t;
    }

    public int getResult() {
        return this.result;
    }

    public void setResult(int result2) {
        this.result = result2;
    }
}
