package com.house365.core.bean;

public class VersionInfo {
    public String apkFileUrl;
    public String iphoneVersion;
    public boolean isForced;
    public int versionCode;
    public String versionName;
    public String versionNote;

    public int getVersionCode() {
        return this.versionCode;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public String getVersionNote() {
        return this.versionNote;
    }

    public String getIphoneVersion() {
        return this.iphoneVersion;
    }

    public String getApkFileUrl() {
        return this.apkFileUrl;
    }

    public boolean isForced() {
        return this.isForced;
    }

    public void setVersionCode(int versionCode2) {
        this.versionCode = versionCode2;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public void setVersionNote(String versionNote2) {
        this.versionNote = versionNote2;
    }

    public void setIphoneVersion(String iphoneVersion2) {
        this.iphoneVersion = iphoneVersion2;
    }

    public void setApkFileUrl(String apkFileUrl2) {
        this.apkFileUrl = apkFileUrl2;
    }

    public void setForced(boolean isForced2) {
        this.isForced = isForced2;
    }
}
