package com.house365.core.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Group<R, T> implements Serializable {
    private static final long serialVersionUID = -3015999252656391616L;
    protected R r;
    protected List<T> subs;

    public Group(R r2) {
        this.r = r2;
        this.subs = new ArrayList();
    }

    public Group(R r2, List<T> subs2) {
        this.r = r2;
        this.subs = subs2;
    }

    public R getGroup() {
        return this.r;
    }

    public List<T> getSubs() {
        if (this.subs == null) {
            this.subs = new ArrayList();
        }
        return this.subs;
    }

    public void clear() {
        getSubs().clear();
    }

    public void addSubs(List<? extends T> tlist) {
        getSubs().addAll(tlist);
    }

    public int getSubsCount() {
        if (this.subs == null) {
            return 0;
        }
        return this.subs.size();
    }

    public T getSub(int i) {
        return this.subs.get(i);
    }

    public T removeSub(int i) {
        return this.subs.remove(i);
    }

    public void addSub(T t) {
        this.subs.add(t);
    }
}
