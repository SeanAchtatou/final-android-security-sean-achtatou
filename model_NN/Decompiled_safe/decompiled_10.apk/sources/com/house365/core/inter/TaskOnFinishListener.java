package com.house365.core.inter;

public interface TaskOnFinishListener<T> {
    void onError();

    void onSuccess(T t);
}
