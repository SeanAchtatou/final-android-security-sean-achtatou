package com.house365.core.inter;

public class JsInterface {
    private wvClientClickListener wvEnventPro = null;

    public interface wvClientClickListener {
        void wvHasClickEnvent();
    }

    public void setWvClientClickListener(wvClientClickListener listener) {
        this.wvEnventPro = listener;
    }

    public void javaFunction() {
        if (this.wvEnventPro != null) {
            this.wvEnventPro.wvHasClickEnvent();
        }
    }
}
