package com.house365.core.inter;

import android.content.DialogInterface;

@Deprecated
public interface DialogOnPositiveListener {
    void onPositive(DialogInterface dialogInterface);
}
