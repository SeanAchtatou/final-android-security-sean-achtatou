package com.house365.core.inter;

import android.content.DialogInterface;

public interface ConfirmDialogListener {
    void onNegative(DialogInterface dialogInterface);

    void onPositive(DialogInterface dialogInterface);
}
