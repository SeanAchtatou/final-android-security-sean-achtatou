package com.house365.core.json;

public interface JSONString {
    String toJSONString();
}
