package com.house365.core.json;

public class JSONUtil {
    public static boolean isJsonObject(String jsonString) {
        try {
            new JSONObject(jsonString);
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    public static boolean isJsonArray(String jsonString) {
        try {
            new JSONArray(jsonString);
            return true;
        } catch (JSONException e) {
            return false;
        }
    }
}
