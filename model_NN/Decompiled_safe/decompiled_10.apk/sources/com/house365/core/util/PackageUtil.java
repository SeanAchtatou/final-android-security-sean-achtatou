package com.house365.core.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class PackageUtil {
    public static boolean isExistPackage(Context context, String packagename) {
        List<PackageInfo> pkgList = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < pkgList.size(); i++) {
            PackageInfo pI = pkgList.get(i);
            if (pI.versionName != null && pI.packageName.equalsIgnoreCase(packagename)) {
                return true;
            }
        }
        return false;
    }

    public static PackageInfo getApkInfo(Context context, String archiveFilePath) {
        return context.getPackageManager().getPackageArchiveInfo(archiveFilePath, 128);
    }

    public static Bundle getAppMetaData(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ActivityInfo getActivityInfo(Activity activity) {
        try {
            return activity.getPackageManager().getActivityInfo(activity.getComponentName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public static String getVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return StringUtils.EMPTY;
        }
    }
}
