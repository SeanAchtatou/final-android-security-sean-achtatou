package com.house365.core.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.inter.DialogOnPositiveListener;
import java.util.List;

public class ActivityUtil {

    public interface InputTextBoxOnClickListener {
        void onClick(DialogInterface dialogInterface, int i, EditText editText);
    }

    public interface ItemDialogOnChooseListener {
        void onChoose(DialogInterface dialogInterface, int i);
    }

    public interface ItemLonngDialogOnChooseListener {
        void chooseFirst(DialogInterface dialogInterface, int i);
    }

    public interface PhotoDialogOnChooseListener {
        void chooseAlbum(DialogInterface dialogInterface);

        void choosePhoto(DialogInterface dialogInterface);
    }

    public static void showToastView(Context context, String msg) {
        View toastRoot = LayoutInflater.from(context).inflate(R.layout.toast, (ViewGroup) null);
        TextView message = (TextView) toastRoot.findViewById(R.id.message);
        message.setGravity(17);
        message.setText(msg);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(17, 0, 10);
        toastStart.setDuration(1);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

    public static void showToastView(Context context, int resId) {
        View toastRoot = LayoutInflater.from(context).inflate(R.layout.toast, (ViewGroup) null);
        ((TextView) toastRoot.findViewById(R.id.message)).setText(resId);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(17, 0, 10);
        toastStart.setDuration(1);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

    public static void showToast(Context context, CharSequence text) {
        Toast.makeText(context, text, 0).show();
    }

    public static void showToast(Context context, int resId) {
        Toast.makeText(context, resId, 0).show();
    }

    @Deprecated
    public static void showDialog(Context context, String msgStr) {
        new AlertDialog.Builder(context).setMessage(msgStr).setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Deprecated
    public static void showDialog(Context context, int msgResId) {
        new AlertDialog.Builder(context).setMessage(msgResId).setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Deprecated
    public static void showConfrimDialog(Context context, String msgStr, final DialogOnPositiveListener onPositiveListener) {
        new AlertDialog.Builder(context).setMessage(msgStr).setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogOnPositiveListener.this != null) {
                    DialogOnPositiveListener.this.onPositive(dialog);
                }
                dialog.dismiss();
            }
        }).show();
    }

    @Deprecated
    public static void showConfrimDialog(Context context, int msgStrId, final DialogOnPositiveListener onPositiveListener) {
        new AlertDialog.Builder(context).setMessage(msgStrId).setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (DialogOnPositiveListener.this != null) {
                    DialogOnPositiveListener.this.onPositive(dialog);
                }
                dialog.dismiss();
            }
        }).show();
    }

    @Deprecated
    public static void showConfrimDialog(Context context, int NegativeButtonId, int PositiveButtonId, int titleStrId, int msgStrId, final ConfirmDialogListener onConfirmDialogListener) {
        new AlertDialog.Builder(context).setCancelable(false).setTitle(titleStrId).setMessage(msgStrId).setNegativeButton(NegativeButtonId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (ConfirmDialogListener.this != null) {
                    ConfirmDialogListener.this.onNegative(dialog);
                }
            }
        }).setPositiveButton(PositiveButtonId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ConfirmDialogListener.this != null) {
                    ConfirmDialogListener.this.onPositive(dialog);
                }
                dialog.dismiss();
            }
        }).show();
    }

    public static void exitDialog(final Context context, int appid) {
        new AlertDialog.Builder(context).setTitle(appid).setMessage(R.string.confirm_exit_info).setPositiveButton(R.string.dialog_button_exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                context.sendBroadcast(new Intent(ActionTag.INTENT_ACTION_LOGGED_OUT));
                ((Activity) context).getApplication().onTerminate();
            }
        }).setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public static void inputTextBox(Context context, int titleId, String key, String content, int maxSize, int inputType, final InputTextBoxOnClickListener positiveClick) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(titleId);
        final EditText input = new EditText(context);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxSize)});
        input.setInputType(inputType);
        input.setText(content);
        input.setLines(1);
        alert.setView(input);
        alert.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputTextBoxOnClickListener.this.onClick(dialog, which, input);
                dialog.dismiss();
            }
        });
        alert.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    public static void inputTextBox(Context context, int titleId, String key, String content, int maxSize, int inputType, int line, final InputTextBoxOnClickListener positiveClick) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(titleId);
        final EditText input = new EditText(context);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxSize)});
        input.setInputType(inputType);
        input.setText(content);
        input.setLines(line);
        input.setGravity(48);
        alert.setView(input);
        alert.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputTextBoxOnClickListener.this.onClick(dialog, which, input);
            }
        });
        alert.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    public static void showItemLongChooseDialog(Context context, CharSequence[] items, final ItemLonngDialogOnChooseListener onChooseListener) {
        new AlertDialog.Builder(context).setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        ItemLonngDialogOnChooseListener.this.chooseFirst(dialog, which);
                        break;
                }
                dialog.dismiss();
            }
        }).show();
    }

    public static void showSingleChooseDialog(Context context, CharSequence[] items, final ItemDialogOnChooseListener onChooseListener) {
        new AlertDialog.Builder(context).setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ItemDialogOnChooseListener.this.onChoose(dialog, which);
                dialog.dismiss();
            }
        }).show();
    }

    public static void showSingleChooseDialog(Context context, int titleId, CharSequence[] items, ItemDialogOnChooseListener onChooseListener) {
        showSingleChooseDialog(context, context.getResources().getString(titleId), items, onChooseListener);
    }

    public static void showSingleChooseDialog(Context context, CharSequence title, CharSequence[] items, final ItemDialogOnChooseListener onChooseListener) {
        new AlertDialog.Builder(context).setTitle(title).setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ItemDialogOnChooseListener.this.onChoose(dialog, which);
                dialog.dismiss();
            }
        }).show();
    }

    public static void showPhotoChooseDialog(Context context, final PhotoDialogOnChooseListener onChooseListener) {
        new AlertDialog.Builder(context).setItems(R.array.uploadway, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        PhotoDialogOnChooseListener.this.chooseAlbum(dialog);
                        break;
                    case 1:
                        PhotoDialogOnChooseListener.this.choosePhoto(dialog);
                        break;
                }
                dialog.dismiss();
            }
        }).show();
    }

    public static void alignGalleryToLeft(Context context, Gallery gallery, int itemWidth) {
        int offset;
        DisplayMetrics metric = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metric);
        int galleryWidth = metric.widthPixels;
        if (galleryWidth <= itemWidth) {
            offset = ((galleryWidth / 2) - (itemWidth / 2)) - 5;
        } else {
            offset = (galleryWidth - itemWidth) - 10;
        }
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gallery.getLayoutParams();
        mlp.setMargins(-offset, mlp.topMargin, mlp.rightMargin, mlp.bottomMargin);
    }

    public static void toDial(final Context context, String telstr, String delimiter) {
        if (!TextUtils.isEmpty(telstr)) {
            final String[] tels = telstr.split(delimiter);
            if (tels != null && tels.length > 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder((Activity) context);
                builder.setTitle(R.string.dial_tel);
                builder.setSingleChoiceItems(tels, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String tel = tels[which];
                        dialog.dismiss();
                        try {
                            ((Activity) context).startActivity(IntentUtil.getDialIntent(tel));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.create().show();
            } else if (tels != null && tels.length == 1) {
                try {
                    ((Activity) context).startActivity(IntentUtil.getDialIntent(tels[0]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getTopActivity(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        if (runningTaskInfos != null) {
            return runningTaskInfos.get(0).topActivity.getClassName();
        }
        return null;
    }

    public static String getBaseActivity(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        if (runningTaskInfos != null) {
            return runningTaskInfos.get(0).baseActivity.getClassName();
        }
        return null;
    }

    public static boolean isTopActivity(Activity context) {
        String s = getTopActivity(context);
        if (s == null) {
            return false;
        }
        return context.getClass().getName().equals(s);
    }

    public static boolean isAppOnForeground(Context context, String baseActivtyName) {
        String s = getBaseActivity(context);
        if (s == null) {
            return false;
        }
        return baseActivtyName.equals(s);
    }

    public static boolean isServiceRunning(Context mContext, String className) {
        boolean isRunning = false;
        List<ActivityManager.RunningServiceInfo> serviceList = ((ActivityManager) mContext.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
        if (serviceList == null || serviceList.size() == 0) {
            return false;
        }
        int i = 0;
        while (true) {
            if (i >= serviceList.size()) {
                break;
            } else if (serviceList.get(i).service.getClassName().equals(className)) {
                isRunning = true;
                break;
            } else {
                i++;
            }
        }
        return isRunning;
    }
}
