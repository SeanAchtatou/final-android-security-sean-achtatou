package com.house365.core.util.map.google.lazyload;

import com.house365.core.util.map.google.ManagedOverlayException;

public class LazyLoadException extends ManagedOverlayException {
    private static final long serialVersionUID = 1;

    public LazyLoadException() {
    }

    public LazyLoadException(String s) {
        super(s);
    }

    public LazyLoadException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LazyLoadException(Throwable throwable) {
        super(throwable);
    }
}
