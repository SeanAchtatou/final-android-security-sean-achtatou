package com.house365.core.util.lbs;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.LocationListener;
import com.house365.core.application.BaseApplication;
import com.house365.core.application.BaseApplicationWithMapService;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang.SystemUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

public class MyLocation {
    public static final String LOACTION_TYPE_BAIDU = "bd09ll";
    public static final String LOACTION_TYPE_COMMON = "gcj02";
    public static final int LOCATION_BAIDU = 2;
    public static final int LOCATION_BASESTATION = 4;
    public static final int LOCATION_GOOGLE = 1;
    private final int DEVIANTLAT = 0;
    private final int DEVIANTLNG = 0;
    LocationListener baiduLocationListener = new LocationListener() {
        public void onLocationChanged(Location slocation) {
            if (slocation != null) {
                MyLocation.this.location = slocation;
            }
        }
    };
    private Context context;
    private android.location.LocationListener googleLocationListener = new android.location.LocationListener() {
        public void onLocationChanged(Location loc) {
            if (MyLocation.this.location != null) {
                CorePreferences.DEBUG("Location changed:Lat:" + MyLocation.this.location.getLatitude() + " Lng:" + MyLocation.this.location.getLongitude());
            }
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    private LocationManager googleLocationManager;
    private String googleUrl = "http://www.google.com/loc/json";
    private boolean isGpsEnabled;
    private boolean isNetWorkEnabled;
    /* access modifiers changed from: private */
    public Location location;
    private BMapManager mBMapMan;
    private LocationClient mLocationClient;

    public interface LocationCallBack {
        void onResult(Location location);
    }

    public MyLocation(Context context2) {
        this.context = context2;
    }

    @Deprecated
    public Location getLocation(int locationType) {
        return getLocation(locationType, false);
    }

    public Location getLocation(int locationType, boolean attachAddrInfoForBaiDu) {
        if ((locationType & 1) == 1) {
            getLocationByGoogleAPI(0);
            if (this.location != null) {
                return this.location;
            }
        }
        if ((locationType & 2) == 2) {
            getLocationByBaiduLocApi(true, 0, attachAddrInfoForBaiDu);
            if (this.location != null) {
                return this.location;
            }
        }
        if ((locationType & 4) == 4) {
            getLocationByBaseStation();
            if (this.location != null) {
                return this.location;
            }
        }
        return null;
    }

    @Deprecated
    public void getAsyncLocation(int locationType, LocationCallBack locationCallBack) {
        getAsyncLocation(locationType, locationCallBack, false);
    }

    public void getAsyncLocation(final int locationType, final LocationCallBack locationCallBack, boolean attachAddrInfoForBaiDu) {
        new Thread() {
            public void run() {
                MyLocation.this.location = null;
                if ((locationType & 1) == 1 && MyLocation.this.location == null) {
                    MyLocation.this.getLocationByGoogleAPI(0);
                }
                if ((locationType & 2) == 2 && MyLocation.this.location == null) {
                    MyLocation.this.getLocationByBaiduLocApi(true, 0);
                }
                if ((locationType & 4) == 4 && MyLocation.this.location == null) {
                    MyLocation.this.getLocationByBaseStation();
                }
                locationCallBack.onResult(MyLocation.this.location);
            }
        }.start();
    }

    @Deprecated
    public void removeLocationUpdate(int locationType) {
        if ((locationType & 1) == 1) {
            removeGoogleLocationUpdates();
        }
    }

    @Deprecated
    private LocationManager getGoogleLocationManager() {
        if (this.googleLocationManager == null) {
            this.googleLocationManager = (LocationManager) this.context.getSystemService("location");
        }
        return this.googleLocationManager;
    }

    public Location getLocationByGoogleAPI(long duration) {
        if (duration <= 0) {
            duration = 10000;
        }
        this.location = null;
        this.isGpsEnabled = getGoogleLocationManager().isProviderEnabled("gps");
        this.isNetWorkEnabled = getGoogleLocationManager().isProviderEnabled("network");
        boolean isRegGPSListener = false;
        boolean isRegNetworkListener = false;
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < duration && this.location == null) {
            if (this.isGpsEnabled) {
                if (!isRegGPSListener) {
                    getGoogleLocationManager().requestLocationUpdates("gps", 1000, (float) SystemUtils.JAVA_VERSION_FLOAT, this.googleLocationListener);
                    isRegGPSListener = true;
                }
                this.location = getGoogleLocationManager().getLastKnownLocation("gps");
            }
            if (this.location == null && this.isNetWorkEnabled) {
                if (!isRegNetworkListener) {
                    getGoogleLocationManager().requestLocationUpdates("network", 1000, (float) SystemUtils.JAVA_VERSION_FLOAT, this.googleLocationListener);
                    isRegNetworkListener = true;
                }
                this.location = getGoogleLocationManager().getLastKnownLocation("network");
            }
        }
        return this.location;
    }

    public void removeGoogleLocationUpdates() {
        try {
            getGoogleLocationManager().removeUpdates(this.googleLocationListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public Location getLocationByBaseStation() {
        this.location = null;
        TelephonyManager tm = (TelephonyManager) this.context.getSystemService("phone");
        int type = tm.getNetworkType();
        ArrayList<CellIDInfo> cellIDInfos = new ArrayList<>();
        if (type == 6 || type == 4 || type == 7) {
            CdmaCellLocation location2 = (CdmaCellLocation) tm.getCellLocation();
            int cellIDs = location2.getBaseStationId();
            int networkID = location2.getNetworkId();
            StringBuilder nsb = new StringBuilder();
            nsb.append(location2.getSystemId());
            CellIDInfo info = new CellIDInfo();
            info.cellId = cellIDs;
            info.locationAreaCode = networkID;
            info.mobileNetworkCode = nsb.toString();
            info.mobileCountryCode = tm.getNetworkOperator().substring(0, 3);
            info.radioType = "cdma";
            cellIDInfos.add(info);
        } else if (type == 2) {
            GsmCellLocation location3 = (GsmCellLocation) tm.getCellLocation();
            int cellIDs2 = location3.getCid();
            int lac = location3.getLac();
            CellIDInfo info2 = new CellIDInfo();
            info2.cellId = cellIDs2;
            info2.locationAreaCode = lac;
            info2.mobileCountryCode = tm.getNetworkOperator().substring(0, 3);
            info2.mobileNetworkCode = tm.getNetworkOperator().substring(3, 5);
            info2.radioType = "gsm";
            cellIDInfos.add(info2);
        } else if (type == 1 || type == 8 || type == 10) {
            GsmCellLocation location4 = (GsmCellLocation) tm.getCellLocation();
            int cellIDs3 = location4.getCid();
            int lac2 = location4.getLac();
            CellIDInfo info3 = new CellIDInfo();
            info3.cellId = cellIDs3;
            info3.locationAreaCode = lac2;
            info3.radioType = "gsm";
            cellIDInfos.add(info3);
        } else if (type == 3) {
            GsmCellLocation location5 = (GsmCellLocation) tm.getCellLocation();
            int cellIDs4 = location5.getCid();
            int lac3 = location5.getLac();
            CellIDInfo info4 = new CellIDInfo();
            info4.cellId = cellIDs4;
            info4.locationAreaCode = lac3;
            info4.radioType = "gsm";
            cellIDInfos.add(info4);
        } else {
            GsmCellLocation location6 = (GsmCellLocation) tm.getCellLocation();
            int cellIDs5 = location6.getCid();
            int lac4 = location6.getLac();
            CellIDInfo info5 = new CellIDInfo();
            info5.cellId = cellIDs5;
            info5.locationAreaCode = lac4;
            info5.radioType = "gsm";
            cellIDInfos.add(info5);
        }
        this.location = callGear(cellIDInfos);
        return this.location;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.json.JSONObject.put(java.lang.String, boolean):com.house365.core.json.JSONObject
     arg types: [java.lang.String, int]
     candidates:
      com.house365.core.json.JSONObject.put(java.lang.String, double):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, int):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, long):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, java.lang.Object):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, java.util.Collection):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, java.util.Map):com.house365.core.json.JSONObject
      com.house365.core.json.JSONObject.put(java.lang.String, boolean):com.house365.core.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.entity.StringEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    private Location callGear(ArrayList<CellIDInfo> cellID) {
        if (cellID == null) {
            return null;
        }
        JSONObject holder = new JSONObject();
        try {
            holder.put("version", "1.1.0");
            holder.put("host", "maps.google.com");
            holder.put("request_address", true);
            if ("460".equals(cellID.get(0).mobileCountryCode)) {
                holder.put("address_language", "zh_CN");
            } else {
                holder.put("address_language", "en_US");
            }
            JSONArray array = new JSONArray();
            JSONObject current_data = new JSONObject();
            current_data.put("cell_id", cellID.get(0).cellId);
            current_data.put("location_area_code", cellID.get(0).locationAreaCode);
            if (!cellID.get(0).radioType.equals("gsm")) {
                current_data.put("mobile_country_code", cellID.get(0).mobileCountryCode);
                current_data.put("mobile_network_code", cellID.get(0).mobileNetworkCode);
                current_data.put("age", 0);
            }
            array.put(current_data);
            holder.put("cell_towers", array);
            StringEntity se = new StringEntity(holder.toString());
            CorePreferences.DEBUG(holder.toString());
            Log.e("Location send", holder.toString());
            String sb = ((BaseApplication) ((Activity) this.context).getApplication()).getApi().post(this.googleUrl, (HttpEntity) se);
            if (sb.length() <= 1) {
                return null;
            }
            JSONObject data = (JSONObject) new JSONObject(sb.toString()).get("location");
            Location loc = new Location("network");
            loc.setLatitude(((Double) data.get("latitude")).doubleValue());
            loc.setLongitude(((Double) data.get("longitude")).doubleValue());
            loc.setAccuracy(Float.parseFloat(data.get("accuracy").toString()));
            loc.setTime(GetUTCTime());
            return loc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private long GetUTCTime() {
        Calendar cal = Calendar.getInstance(Locale.CHINA);
        cal.add(14, -(cal.get(15) + cal.get(16)));
        return cal.getTimeInMillis();
    }

    public BMapManager getmBMapMan() {
        return this.mBMapMan;
    }

    public void setmBMapMan(BMapManager mBMapMan2) {
        this.mBMapMan = mBMapMan2;
    }

    private BMapManager getBaiduMapManager() {
        if (this.mBMapMan == null) {
            this.mBMapMan = ((BaseApplicationWithMapService) ((Activity) this.context).getApplication()).getbMapManager();
        }
        return this.mBMapMan;
    }

    private LocationClient getBaiduLocationClient() {
        if (this.mLocationClient == null) {
            this.mLocationClient = ((BaseApplicationWithMapService) ((Activity) this.context).getApplication()).getbLocationClient();
        }
        return this.mLocationClient;
    }

    @Deprecated
    public Location getLocationByBaiduAPI(long duration) {
        if (duration <= 0) {
            duration = 10000;
        }
        this.location = null;
        getBaiduMapManager().getLocationManager().requestLocationUpdates(this.baiduLocationListener);
        getBaiduMapManager().start();
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < duration && this.location == null) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this.location;
    }

    public Location getLocationByBaiduLocApi(boolean gpsFirst, long duration) {
        return getLocationByBaiduLocApi(gpsFirst, duration, false);
    }

    public Location getLocationByBaiduLocApi(boolean gpsFirst, long duration, final boolean attchAddrInfo) {
        if (duration <= 0) {
            duration = 10000;
        }
        this.location = null;
        BaseApplicationWithMapService app = (BaseApplicationWithMapService) ((Activity) this.context).getApplication();
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(true);
        option.setCoorType(LOACTION_TYPE_BAIDU);
        if (attchAddrInfo) {
            option.setAddrType("all");
        }
        option.disableCache(false);
        if (gpsFirst) {
            option.setPriority(1);
        } else {
            option.setPriority(2);
        }
        app.getbLocationClient().setLocOption(option);
        app.setLocationChangerListener(new OnLocationChangeListener() {
            public void onGetLocation(BDLocation mlocation) {
                MyLocation.this.location = new Location("network");
                MyLocation.this.location.setLatitude(mlocation.getLatitude());
                MyLocation.this.location.setLongitude(mlocation.getLongitude());
                if (attchAddrInfo) {
                    Bundle extras = new Bundle();
                    extras.putString("addr", mlocation.getAddrStr());
                    extras.putString("city", mlocation.getCity());
                    MyLocation.this.location.setExtras(extras);
                }
            }

            public void onGetPoi(BDLocation poiLocation) {
            }
        });
        app.getbLocationClient().start();
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < duration && this.location == null) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        app.getbLocationClient().stop();
        return this.location;
    }

    @Deprecated
    public void removeBaiduLocationUpdate() {
        getBaiduMapManager().getLocationManager().removeUpdates(this.baiduLocationListener);
        getBaiduMapManager().stop();
    }

    public Address getAddressbyLocation(Context cntext, Location location2) {
        if (location2 == null) {
            return null;
        }
        try {
            List<Address> lstAddress = new Geocoder(cntext, Locale.getDefault()).getFromLocation(location2.getLatitude(), location2.getLongitude(), 1);
            if (lstAddress.size() > 0) {
                return lstAddress.get(0);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getAddressByLocationWithString(Context cntext, Location location2) {
        Address addr = getAddressbyLocation(cntext, location2);
        StringBuffer sb = new StringBuffer();
        if (addr != null && addr.getMaxAddressLineIndex() >= 2) {
            sb.append(addr.getAddressLine(1));
            sb.append(addr.getAddressLine(2));
        }
        return sb.toString();
    }
}
