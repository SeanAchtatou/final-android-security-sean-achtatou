package com.house365.core.util;

import android.text.TextUtils;
import java.util.regex.Pattern;

public class RegexUtil {
    public static boolean isNumber(String s) {
        return Pattern.compile("\\d*", 2).matcher(s).matches();
    }

    public static boolean isNumberWithLen(String s, int len) {
        return Pattern.compile("\\d{" + len + "}", 2).matcher(s).matches();
    }

    public static boolean isCharacter(String s) {
        return Pattern.compile("\\w*", 2).matcher(s).matches();
    }

    public static boolean isCharacterWithLen(String s, int len) {
        return Pattern.compile("\\w{" + len + "}", 2).matcher(s).matches();
    }

    public static boolean isNumCharCombinationWithLen(String s, int minLen, int maxLen) {
        return Pattern.compile("[A-Za-z0-9]{" + minLen + "," + maxLen + "}", 2).matcher(s).matches();
    }

    public static boolean isMobileNumber(String mobiles) {
        if (!TextUtils.isEmpty(mobiles)) {
            return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,1,2,5-9]))\\d{8}$").matcher(mobiles).matches();
        }
        return false;
    }

    public static boolean isChineseLength(String str, int minlen, int maxlen) {
        return Pattern.compile("[\\u4e00-\\u9fa5]{" + minlen + "," + maxlen + "}").matcher(str).matches();
    }
}
