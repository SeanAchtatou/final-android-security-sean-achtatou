package com.house365.core.util.map.google.lazyload;

import com.google.android.maps.GeoPoint;
import com.house365.core.util.map.google.ManagedOverlay;
import com.house365.core.util.map.google.ManagedOverlayItem;
import java.util.List;

public interface LazyLoadCallback {
    List<? extends ManagedOverlayItem> lazyload(GeoPoint geoPoint, GeoPoint geoPoint2, ManagedOverlay managedOverlay) throws LazyLoadException;
}
