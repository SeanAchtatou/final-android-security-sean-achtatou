package com.house365.core.util.map.google;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.house365.core.util.map.google.ManagedOverlayGestureDetector;
import com.house365.core.util.map.google.ManagedOverlayItem;
import com.house365.core.util.map.google.lazyload.DummyListenerListener;
import com.house365.core.util.map.google.lazyload.LazyLoadAnimation;
import com.house365.core.util.map.google.lazyload.LazyLoadCallback;
import com.house365.core.util.map.google.lazyload.LazyLoadException;
import com.house365.core.util.map.google.lazyload.LazyLoadListener;
import com.house365.core.util.map.google.lazyload.LazyLoadManager;
import java.util.ArrayList;
import java.util.List;

public class ManagedOverlay extends ItemizedOverlay<ManagedOverlayItem> {
    private static final String LOG_TAG = "Maps_ManagedOverlay";
    protected MarkerRenderer customMarkerRenderer;
    protected Drawable defaultMarker;
    protected ManagedOverlayGestureDetector gs;
    protected boolean isLazyLoadEnabled;
    private ArrayList<ManagedOverlayItem> items;
    protected int lastZoomlevel;
    protected LazyLoadManager lazyLoadManager;
    protected boolean longPressFinished;
    protected OverlayManager manager;
    private int minTouchableHeight;
    private int minTouchableWidth;
    protected String name;
    private Rect touchableBounds;
    protected boolean zoomFinished;

    protected ManagedOverlay(OverlayManager manager2, String name2, Drawable defaultMarker2) {
        super(defaultMarker2);
        this.items = new ArrayList<>();
        this.isLazyLoadEnabled = false;
        this.longPressFinished = false;
        this.zoomFinished = false;
        this.lastZoomlevel = -1;
        this.touchableBounds = new Rect();
        this.minTouchableWidth = 10;
        this.minTouchableHeight = 10;
        if (defaultMarker2.getBounds().isEmpty()) {
            boundCenterBottom(defaultMarker2);
        }
        this.defaultMarker = defaultMarker2;
        this.name = name2;
        this.manager = manager2;
        initGestureDetect();
        add(NullMarker.INSTANCE);
    }

    public static void boundToCenter(Drawable d) {
        boundCenter(d);
    }

    public static void boundToCenterBottom(Drawable d) {
        boundCenterBottom(d);
    }

    protected ManagedOverlay(OverlayManager manager2, Drawable defaultMarker2) {
        this(manager2, null, defaultMarker2);
    }

    protected ManagedOverlay(Drawable defaultMarker2) {
        this(null, null, defaultMarker2);
    }

    private void initGestureDetect() {
        ManagedOverlayGestureDetector.ManagedGestureListener managedGestureListener = new ManagedOverlayGestureDetector.ManagedGestureListener(this);
        this.gs = new ManagedOverlayGestureDetector(this.manager.ctx, managedGestureListener, this);
        managedGestureListener.setDetector(this.gs);
    }

    public synchronized void invokeLazyLoad(long delay) {
        if (this.isLazyLoadEnabled) {
            Log.d(LOG_TAG, "invokeLazyLoad(" + delay + ")");
            getLazyLoadManager().call(delay);
        }
    }

    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        if (this.longPressFinished) {
            this.longPressFinished = false;
            this.gs.invokeLongPressFinished();
        }
        if (this.zoomFinished) {
            this.zoomFinished = false;
            this.gs.invokeZoomFinished();
        }
        ManagedOverlay.super.draw(canvas, mapView, false);
        if (this.lastZoomlevel == -1) {
            this.lastZoomlevel = mapView.getZoomLevel();
        }
        if (mapView.getZoomLevel() != this.lastZoomlevel) {
            this.gs.invokeZoomEvent(this.lastZoomlevel, mapView.getZoomLevel());
            this.lastZoomlevel = mapView.getZoomLevel();
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
    }

    public void add(ManagedOverlayItem item) {
        if (this.items.size() == 1 && this.items.get(0).getClass().isAssignableFrom(NullMarker.class)) {
            this.items.clear();
        }
        item.setOverlay(this);
        this.items.add(item);
        setLastFocusedIndex(-1);
        populate();
    }

    public void addAll(List<ManagedOverlayItem> items2) {
        this.items.clear();
        if (items2 == null || items2.size() <= 0) {
            this.items.add(NullMarker.INSTANCE);
            setLastFocusedIndex(-1);
            populate();
            return;
        }
        for (int i = 0; i < items2.size(); i++) {
            ManagedOverlayItem managedOverlayItem = items2.get(i);
            managedOverlayItem.setOverlay(this);
            this.items.add(managedOverlayItem);
        }
        setLastFocusedIndex(-1);
        populate();
    }

    public ManagedOverlayItem createItem(GeoPoint p) {
        ManagedOverlayItem item = new ManagedOverlayItem.Builder(p).create();
        add(item);
        return item;
    }

    public ManagedOverlayItem createItem(GeoPoint p, String titel) {
        ManagedOverlayItem item = new ManagedOverlayItem.Builder(p).name(titel).create();
        add(item);
        return item;
    }

    public ManagedOverlayItem createItem(GeoPoint p, String titel, String snippet) {
        ManagedOverlayItem item = new ManagedOverlayItem.Builder(p).name(titel).snippet(snippet).create();
        add(item);
        return item;
    }

    public ManagedOverlayItem.Builder itemBuilder() {
        return new ManagedOverlayItem.Builder();
    }

    public void remove(ManagedOverlayItem item) {
        this.items.remove(item);
        setLastFocusedIndex(-1);
        populate();
    }

    public void remove(int index) {
        this.items.remove(index);
        setLastFocusedIndex(-1);
        populate();
    }

    /* access modifiers changed from: protected */
    public ManagedOverlayItem createItem(int i) {
        return this.items.get(i);
    }

    public int size() {
        return this.items.size();
    }

    public boolean onTap(GeoPoint p, MapView mapView) {
        this.gs.onTap(p);
        return ManagedOverlay.super.onTap(p, mapView);
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int index) {
        return this.gs.onTap(index);
    }

    public boolean onTouchEvent(MotionEvent event, MapView mapView) {
        return this.gs.onTouchEvent(event);
    }

    public boolean onTrackballEvent(MotionEvent event, MapView mapView) {
        return ManagedOverlay.super.onTrackballEvent(event, mapView);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public boolean isLazyLoadEnabled() {
        return this.isLazyLoadEnabled;
    }

    public LazyLoadAnimation enableLazyLoadAnimation(ImageView target) {
        return getLazyLoadManager().enableLazyLoadAnimation(target);
    }

    public Drawable getDefaultMarker() {
        return this.defaultMarker;
    }

    public LazyLoadAnimation getLazyLoadAnimation() {
        if (isLazyLoadEnabled()) {
            return getLazyLoadManager().getLazyLoadAnimation();
        }
        return null;
    }

    public LazyLoadCallback getLazyLoadCallback() {
        if (this.isLazyLoadEnabled) {
            return getLazyLoadManager().getLazyLoadCallback();
        }
        return null;
    }

    public void setLazyLoadCallback(LazyLoadCallback lazyLoadCallback) {
        if (lazyLoadCallback != null) {
            this.isLazyLoadEnabled = true;
            if (this.gs.getOverlayOnGestureListener() == null) {
                this.gs.setOverlayOnGestureListener(new DummyListenerListener());
            }
            getLazyLoadManager().setLazyLoadCallback(lazyLoadCallback);
        }
    }

    public void setOnOverlayGestureListener(ManagedOverlayGestureDetector.OnOverlayGestureListener listener) {
        this.gs.setOverlayOnGestureListener(listener);
    }

    public void setOnGestureListener(GestureDetector.OnGestureListener listener) {
        this.gs.setOnGestureListener(listener);
    }

    public OverlayManager getManager() {
        return this.manager;
    }

    public int getZoomlevel() {
        return this.lastZoomlevel;
    }

    public MarkerRenderer getCustomMarkerRenderer() {
        return this.customMarkerRenderer;
    }

    public void setCustomMarkerRenderer(MarkerRenderer customMarkerRenderer2) {
        this.customMarkerRenderer = customMarkerRenderer2;
    }

    public LazyLoadListener getLazyLoadListener() throws LazyLoadException {
        if (this.isLazyLoadEnabled) {
            return getLazyLoadManager().getLazyLoadListener();
        }
        return null;
    }

    public void setLazyLoadListener(LazyLoadListener lazyLoadListener) {
        if (this.isLazyLoadEnabled) {
            getLazyLoadManager().setLazyLoadListener(lazyLoadListener);
        }
    }

    public List<ManagedOverlayItem> getOverlayItems() {
        return this.items;
    }

    public int getMinTouchableWidth() {
        return this.minTouchableWidth;
    }

    public void setMinTouchableWidth(int minTouchableWidth2) {
        this.minTouchableWidth = minTouchableWidth2;
    }

    public int getMinTouchableHeight() {
        return this.minTouchableHeight;
    }

    public void setMinTouchableHeight(int minTouchableHeight2) {
        this.minTouchableHeight = minTouchableHeight2;
    }

    public MapView getMapView() {
        return getManager().getMapView();
    }

    private synchronized LazyLoadManager getLazyLoadManager() {
        if (this.lazyLoadManager == null) {
            this.lazyLoadManager = new LazyLoadManager(this);
        }
        return this.lazyLoadManager;
    }

    public void setOnFocusChangeListener(OnFocusChangeListener listener) {
        ManagedOverlay.super.setOnFocusChangeListener(listener);
    }

    private static final class NullMarker extends ManagedOverlayItem {
        public static final NullMarker INSTANCE = new NullMarker();
        private static Drawable marker;

        public NullMarker() {
            this(new GeoPoint(0, 0), null, null);
        }

        public NullMarker(GeoPoint point, String title, String snippet) {
            super(point, title, snippet);
            BitmapDrawable bd = new BitmapDrawable(Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565));
            bd.setBounds(0, 0, 0, 0);
            marker = bd;
        }

        public Drawable getMarker(int i) {
            return marker;
        }
    }

    public void close() {
        if (this.lazyLoadManager != null) {
            this.lazyLoadManager.close();
            this.lazyLoadManager = null;
        }
    }

    public static abstract class OnFocusChangeListener implements ItemizedOverlay.OnFocusChangeListener {
        public abstract void onFocusChanged(ManagedOverlay managedOverlay, ManagedOverlayItem managedOverlayItem);

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.house365.core.util.map.google.ManagedOverlay.OnFocusChangeListener.onFocusChanged(com.house365.core.util.map.google.ManagedOverlay, com.house365.core.util.map.google.ManagedOverlayItem):void
         arg types: [com.google.android.maps.ItemizedOverlay, com.google.android.maps.OverlayItem]
         candidates:
          com.house365.core.util.map.google.ManagedOverlay.OnFocusChangeListener.onFocusChanged(com.google.android.maps.ItemizedOverlay, com.google.android.maps.OverlayItem):void
          com.house365.core.util.map.google.ManagedOverlay.OnFocusChangeListener.onFocusChanged(com.house365.core.util.map.google.ManagedOverlay, com.house365.core.util.map.google.ManagedOverlayItem):void */
        public void onFocusChanged(ItemizedOverlay itemizedOverlay, OverlayItem overlayItem) {
            onFocusChanged((ManagedOverlay) ((ManagedOverlay) itemizedOverlay), (ManagedOverlayItem) ((ManagedOverlayItem) overlayItem));
        }
    }
}
