package com.house365.core.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ZoomButtonsController;
import com.house365.core.R;
import com.house365.core.adapter.BaseExpandableListAdapter;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.view.ListFooterView;
import com.house365.core.view.pulltorefresh.PullToRefreshExpandableListView;
import com.house365.core.view.pulltorefresh.PullToRefreshGridView;
import com.house365.core.view.pulltorefresh.PullToRefreshListView;
import java.lang.reflect.Field;
import java.util.List;

public class ViewUtil {

    public interface InvalidateFooterValue {
        boolean invalidateFooterValue(RefreshInfo refreshInfo, List list);
    }

    public static void setButtonPressed(Button button, boolean pressed) {
    }

    public static void setButtonPressed(ImageButton button, boolean pressed) {
        if (pressed) {
            button.requestFocus();
            button.setPressed(true);
            button.setFocusableInTouchMode(true);
            return;
        }
        button.setFocusable(false);
        button.setPressed(false);
        button.setFocusableInTouchMode(false);
    }

    public static void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public static int dip2pix(DisplayMetrics dm, float dipValue) {
        return (int) ((dipValue * dm.density) + 0.5f);
    }

    public static int pix2dip(DisplayMetrics dm, float pixValue) {
        return (int) ((pixValue / dm.density) + 0.5f);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int totalHeight = 0;
            int len = listAdapter.getCount();
            for (int i = 0; i < len; i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = (listView.getDividerHeight() * (listAdapter.getCount() - 1)) + totalHeight;
            listView.setLayoutParams(params);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, boolean):void
     arg types: [android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, int]
     candidates:
      com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, android.widget.ListView, com.house365.core.view.ListFooterView, int):void
      com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, boolean):void */
    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshListView listView, int errorStringId) {
        onListDataComplete(context, data, refreshInfo, adapter, listView, errorStringId, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, boolean):void
     arg types: [android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, int]
     candidates:
      com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, android.widget.ListView, com.house365.core.view.ListFooterView, int):void
      com.house365.core.util.ViewUtil.onListDataComplete(android.content.Context, java.util.List, com.house365.core.util.RefreshInfo, com.house365.core.adapter.BaseListAdapter, com.house365.core.view.pulltorefresh.PullToRefreshListView, int, boolean):void */
    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshListView listView) {
        onListDataComplete(context, data, refreshInfo, adapter, listView, R.string.msg_load_error, false);
    }

    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshListView listView, int errorStringId, boolean showRefreshViewIfNodata) {
        if (!showRefreshViewIfNodata && data != null) {
            data.size();
        }
        if (data == null) {
            if (refreshInfo.hasFooter) {
                listView.setFooterViewVisible(8);
                refreshInfo.hasFooter = false;
            }
            if (refreshInfo.refresh) {
                if (showRefreshViewIfNodata) {
                    adapter.clear();
                }
                adapter.notifyDataSetChanged();
                if (errorStringId != 0) {
                    Toast.makeText(context, errorStringId, 0).show();
                }
                listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                return;
            }
            listView.onRefreshComplete();
            if (errorStringId != 0) {
                Toast.makeText(context, errorStringId, 0).show();
                return;
            }
            return;
        }
        if (data.size() >= refreshInfo.getAvgpage() && !refreshInfo.hasFooter) {
            listView.setFooterViewVisible(0);
            refreshInfo.hasFooter = true;
        } else if (refreshInfo.hasFooter && data.size() < refreshInfo.getAvgpage()) {
            listView.setFooterViewVisible(8);
            refreshInfo.hasFooter = false;
        }
        if (refreshInfo.refresh) {
            adapter.clear();
        }
        adapter.addAll(data);
        adapter.notifyDataSetChanged();
        if (refreshInfo.refresh) {
            listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
        } else {
            listView.onRefreshComplete();
        }
    }

    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, ListView listView, ListFooterView footerView, int errorStringId) {
        onListDataComplete(context, data, refreshInfo, adapter, listView, footerView, errorStringId, false);
    }

    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, ListView listView, ListFooterView footerView) {
        onListDataComplete(context, data, refreshInfo, adapter, listView, footerView, R.string.msg_load_error, false);
    }

    public static void onListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, ListView listView, ListFooterView footerView, int errorStringId, boolean showRefreshViewIfNodata) {
        footerView.setPBGone();
        footerView.setTextView(R.string.text_more);
        if (!showRefreshViewIfNodata && data != null) {
            data.size();
        }
        if (data != null) {
            if (data.size() >= refreshInfo.getAvgpage() && !refreshInfo.hasFooter) {
                refreshInfo.hasFooter = true;
                footerView.setVisibility(0);
            } else if (refreshInfo.hasFooter && data.size() < refreshInfo.getAvgpage()) {
                footerView.setVisibility(8);
                refreshInfo.hasFooter = false;
            } else if (data.size() < refreshInfo.getAvgpage() && !refreshInfo.hasFooter) {
                footerView.setVisibility(8);
            } else if (data.size() == 0) {
                footerView.setVisibility(8);
            }
            if (refreshInfo.refresh) {
                adapter.clear();
            }
            adapter.addAll(data);
            adapter.notifyDataSetChanged();
            if (refreshInfo.refresh && refreshInfo.hasFooter) {
                footerView.setVisibility(0);
            }
        } else if (refreshInfo.refresh) {
            if (refreshInfo.hasFooter) {
                footerView.setVisibility(8);
                refreshInfo.hasFooter = false;
            }
            adapter.clear();
            adapter.notifyDataSetChanged();
            Toast.makeText(context, errorStringId, 0).show();
            footerView.setVisibility(0);
        } else {
            Toast.makeText(context, errorStringId, 0).show();
        }
    }

    public static void onPreLoadingListData(RefreshInfo refreshInfo, PullToRefreshListView listView, BaseListAdapter adapter) {
        if (refreshInfo.refresh) {
            refreshInfo.page = 1;
            listView.showRefreshView();
            return;
        }
        refreshInfo.page++;
    }

    public static void onPreLoadingListData(RefreshInfo refreshInfo, ListView listView, ListFooterView footerView) {
        if (refreshInfo.refresh) {
            refreshInfo.page = 0;
            footerView.setPBVisible();
            footerView.setVisibility(0);
            footerView.setTextView(R.string.loading);
            return;
        }
        refreshInfo.page++;
        footerView.setPBVisible();
    }

    public static void onPreLoadingExpandableListData(RefreshInfo refreshInfo, PullToRefreshExpandableListView listView) {
        if (refreshInfo.refresh) {
            refreshInfo.page = 1;
            listView.showRefreshView();
            return;
        }
        refreshInfo.page++;
    }

    public static void onExpandableListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseExpandableListAdapter adapter, PullToRefreshExpandableListView listView, int errorStringId) {
        onExpandableListDataComplete(context, data, refreshInfo, adapter, listView, errorStringId, false);
    }

    public static void onExpandableListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseExpandableListAdapter adapter, PullToRefreshExpandableListView listView) {
        onExpandableListDataComplete(context, data, refreshInfo, adapter, listView, R.string.msg_load_error, false);
    }

    public static void onExpandableListDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseExpandableListAdapter adapter, PullToRefreshExpandableListView listView, int errorStringId, boolean showRefreshViewIfNodata) {
        if (!showRefreshViewIfNodata && data != null) {
            data.size();
        }
        if (data == null) {
            if (refreshInfo.hasFooter) {
                listView.setFooterViewVisible(8);
                refreshInfo.hasFooter = false;
            }
            if (refreshInfo.refresh) {
                if (showRefreshViewIfNodata) {
                    adapter.clear();
                }
                adapter.notifyDataSetChanged();
                if (errorStringId != 0) {
                    Toast.makeText(context, errorStringId, 0).show();
                }
                listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                return;
            }
            listView.onRefreshComplete();
            if (errorStringId != 0) {
                Toast.makeText(context, errorStringId, 0).show();
                return;
            }
            return;
        }
        if (data.size() >= refreshInfo.getAvgpage() && !refreshInfo.hasFooter) {
            listView.setFooterViewVisible(0);
            refreshInfo.hasFooter = true;
        } else if (refreshInfo.hasFooter && data.size() < refreshInfo.getAvgpage()) {
            listView.setFooterViewVisible(8);
            refreshInfo.hasFooter = false;
        }
        if (refreshInfo.refresh) {
            adapter.clear();
        }
        adapter.addAllGroup(data);
        adapter.notifyDataSetChanged();
        if (refreshInfo.refresh) {
            listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
        } else {
            listView.onRefreshComplete();
        }
    }

    public static void setZoomControlGone(View view) {
        try {
            Field field = WebView.class.getDeclaredField("mZoomButtonsController");
            field.setAccessible(true);
            ZoomButtonsController mZoomButtonsController = new ZoomButtonsController(view);
            mZoomButtonsController.getZoomControls().setVisibility(8);
            try {
                field.set(view, mZoomButtonsController);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            }
        } catch (SecurityException e3) {
            e3.printStackTrace();
        } catch (NoSuchFieldException e4) {
            e4.printStackTrace();
        }
    }

    public static void onPreLoadingGridData(RefreshInfo refreshInfo, PullToRefreshGridView girdView, BaseListAdapter adapter) {
        if (refreshInfo.refresh) {
            refreshInfo.page = 1;
            girdView.showRefreshView();
            return;
        }
        refreshInfo.page++;
    }

    public static void onGridDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshGridView listView, int errorStringId) {
        onGridDataComplete(context, data, refreshInfo, adapter, listView, errorStringId, false);
    }

    public static void onGridDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshGridView listView) {
        onGridDataComplete(context, data, refreshInfo, adapter, listView, R.string.msg_load_error, false);
    }

    public static void onGridDataComplete(Context context, List data, RefreshInfo refreshInfo, BaseListAdapter adapter, PullToRefreshGridView listView, int errorStringId, boolean showRefreshViewIfNodata) {
        if (!showRefreshViewIfNodata && data != null) {
            data.size();
        }
        if (data == null) {
            if (refreshInfo.hasFooter) {
                listView.setFooterViewVisible(8);
                refreshInfo.hasFooter = false;
            }
            if (refreshInfo.refresh) {
                if (showRefreshViewIfNodata) {
                    adapter.clear();
                }
                adapter.notifyDataSetChanged();
                if (errorStringId != 0) {
                    Toast.makeText(context, errorStringId, 0).show();
                }
                listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                return;
            }
            listView.onRefreshComplete();
            if (errorStringId != 0) {
                Toast.makeText(context, errorStringId, 0).show();
                return;
            }
            return;
        }
        if (data.size() >= refreshInfo.getAvgpage() && !refreshInfo.hasFooter) {
            listView.setFooterViewVisible(0);
            refreshInfo.hasFooter = true;
        } else if (refreshInfo.hasFooter && data.size() < refreshInfo.getAvgpage()) {
            listView.setFooterViewVisible(8);
            refreshInfo.hasFooter = false;
        }
        if (refreshInfo.refresh) {
            adapter.clear();
        }
        adapter.addAll(data);
        adapter.notifyDataSetChanged();
        if (refreshInfo.refresh) {
            listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
        } else {
            listView.onRefreshComplete();
        }
    }
}
