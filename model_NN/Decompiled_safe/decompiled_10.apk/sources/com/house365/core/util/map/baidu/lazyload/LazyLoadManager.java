package com.house365.core.util.map.baidu.lazyload;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import com.baidu.mapapi.Projection;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LazyLoadManager {
    private static final String LOG_TAG = "Maps_LazyLoadHandler";
    protected static final int ON_BEGIN = 10;
    protected static final int ON_ERROR = 12;
    protected static final int ON_SUCCESS = 11;
    protected static final int REFRESH_ITEMS = 1;
    protected volatile boolean active = true;
    /* access modifiers changed from: private */
    public volatile long delay = 0;
    protected LazyLoadAnimation lazyLoadAnimation;
    protected LazyLoadCallback lazyLoadCallback;
    protected LazyLoadListener lazyLoadListener;
    protected LazyLoadHandler lazyloadHandler = new LazyLoadHandler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    this.overlay.addAll((List) message.obj);
                    this.overlay.getManager().getMapView().postInvalidate();
                    return;
                case 10:
                    if (LazyLoadManager.this.lazyLoadAnimation != null) {
                        LazyLoadManager.this.lazyLoadAnimation.start();
                    }
                    if (LazyLoadManager.this.lazyLoadListener != null) {
                        LazyLoadManager.this.lazyLoadListener.onBegin(this.overlay);
                        return;
                    }
                    return;
                case 11:
                    if (LazyLoadManager.this.lazyLoadListener != null) {
                        LazyLoadManager.this.lazyLoadListener.onSuccess(this.overlay);
                    }
                    if (LazyLoadManager.this.lazyLoadAnimation != null) {
                        LazyLoadManager.this.lazyLoadAnimation.stop();
                        return;
                    }
                    return;
                case 12:
                    if (LazyLoadManager.this.lazyLoadListener != null) {
                        LazyLoadManager.this.lazyLoadListener.onError((LazyLoadException) message.obj, this.overlay);
                    }
                    if (LazyLoadManager.this.lazyLoadAnimation != null) {
                        LazyLoadManager.this.lazyLoadAnimation.stop();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    protected Thread mainLoop;
    protected ManagedOverlay overlay;
    /* access modifiers changed from: private */
    public volatile boolean run = false;

    public LazyLoadManager(ManagedOverlay overlay2) {
        this.overlay = overlay2;
        this.lazyloadHandler.setOverlay(overlay2);
        invoke();
    }

    public LazyLoadAnimation enableLazyLoadAnimation(ImageView imageView) {
        this.lazyLoadAnimation = new LazyLoadAnimation(imageView);
        return this.lazyLoadAnimation;
    }

    public LazyLoadAnimation enableLazyLoadAnimation(ImageView imageView, AnimationDrawable anim) {
        this.lazyLoadAnimation = new LazyLoadAnimation(imageView);
        this.lazyLoadAnimation.setAnimationDrawable(anim);
        return this.lazyLoadAnimation;
    }

    public synchronized void call(long delay2) {
        this.run = true;
        this.delay = delay2;
    }

    public synchronized void close() {
        this.active = false;
    }

    /* access modifiers changed from: private */
    public synchronized void reset() {
        this.run = false;
        this.delay = 0;
    }

    public synchronized void invoke() {
        if (this.mainLoop == null) {
            this.mainLoop = new Thread(new Runnable() {
                public void run() {
                    while (LazyLoadManager.this.active) {
                        if (LazyLoadManager.this.run) {
                            Log.d(LazyLoadManager.LOG_TAG, "Lazy Loading...");
                            long mdelay = LazyLoadManager.this.delay;
                            LazyLoadManager.this.reset();
                            if (mdelay > 0) {
                                try {
                                    Log.d(LazyLoadManager.LOG_TAG, "Waiting " + mdelay);
                                    TimeUnit.MILLISECONDS.sleep(mdelay);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!(LazyLoadManager.this.lazyLoadListener == null && LazyLoadManager.this.lazyLoadAnimation == null)) {
                                LazyLoadManager.this.lazyloadHandler.sendEmptyMessage(10);
                            }
                            try {
                                Projection p = LazyLoadManager.this.overlay.getManager().getMapView().getProjection();
                                if (p != null) {
                                    List<? extends ManagedOverlayItem> newitems = LazyLoadManager.this.overlay.getLazyLoadCallback().lazyload(p.fromPixels(0, 0), p.fromPixels(LazyLoadManager.this.overlay.getManager().getMapView().getWidth(), LazyLoadManager.this.overlay.getManager().getMapView().getHeight()), LazyLoadManager.this.overlay);
                                    Message.obtain(LazyLoadManager.this.lazyloadHandler, 1, newitems).sendToTarget();
                                    if (!(LazyLoadManager.this.overlay.getLazyLoadListener() == null && LazyLoadManager.this.lazyLoadAnimation == null)) {
                                        LazyLoadManager.this.lazyloadHandler.sendEmptyMessage(11);
                                    }
                                    int size = 0;
                                    if (newitems != null) {
                                        size = newitems.size();
                                    }
                                    Log.d(LazyLoadManager.LOG_TAG, "LazyLoad - Success (" + size + ") items loaded.");
                                }
                            } catch (LazyLoadException e2) {
                                if (!(LazyLoadManager.this.lazyLoadListener == null && LazyLoadManager.this.lazyLoadAnimation == null)) {
                                    Message.obtain(LazyLoadManager.this.lazyloadHandler, 12, e2).sendToTarget();
                                }
                                Log.w(LazyLoadManager.LOG_TAG, "LazyLoad - Exception:" + e2);
                            }
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e3) {
                        }
                    }
                }
            });
            this.mainLoop.setDaemon(true);
            this.mainLoop.start();
        }
    }

    private static class LazyLoadHandler extends Handler {
        ManagedOverlay overlay;

        private LazyLoadHandler() {
        }

        /* synthetic */ LazyLoadHandler(LazyLoadHandler lazyLoadHandler) {
            this();
        }

        public void setOverlay(ManagedOverlay overlay2) {
            this.overlay = overlay2;
        }
    }

    public LazyLoadCallback getLazyLoadCallback() {
        return this.lazyLoadCallback;
    }

    public void setLazyLoadCallback(LazyLoadCallback lazyLoadCallback2) {
        this.lazyLoadCallback = lazyLoadCallback2;
    }

    public LazyLoadListener getLazyLoadListener() {
        return this.lazyLoadListener;
    }

    public void setLazyLoadListener(LazyLoadListener lazyLoadListener2) {
        this.lazyLoadListener = lazyLoadListener2;
    }

    public LazyLoadAnimation getLazyLoadAnimation() {
        return this.lazyLoadAnimation;
    }
}
