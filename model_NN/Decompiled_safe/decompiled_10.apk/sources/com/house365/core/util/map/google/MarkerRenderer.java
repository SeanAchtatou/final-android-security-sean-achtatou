package com.house365.core.util.map.google;

import android.graphics.drawable.Drawable;

public interface MarkerRenderer {
    Drawable render(ManagedOverlayItem managedOverlayItem, Drawable drawable, int i);
}
