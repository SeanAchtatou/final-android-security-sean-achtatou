package com.house365.core.util.map.google;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyPositionOverlay extends Overlay {
    private Drawable drawable;
    private GeoPoint geoPoint;

    public MyPositionOverlay(GeoPoint geoPoint2, Drawable drawable2) {
        this.geoPoint = geoPoint2;
        this.drawable = drawable2;
    }

    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        MyPositionOverlay.super.draw(canvas, mapView, shadow);
        Projection projection = mapView.getProjection();
        Point point = new Point();
        if (this.drawable != null) {
            projection.toPixels(this.geoPoint, point);
            Bitmap bitmap = ((BitmapDrawable) this.drawable).getBitmap();
            canvas.drawBitmap(bitmap, (float) (point.x - bitmap.getWidth()), (float) (point.y - bitmap.getHeight()), (Paint) null);
            return;
        }
        projection.toPixels(this.geoPoint, point);
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(6.0f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle((float) point.x, (float) point.y, 8.0f, paint);
    }
}
