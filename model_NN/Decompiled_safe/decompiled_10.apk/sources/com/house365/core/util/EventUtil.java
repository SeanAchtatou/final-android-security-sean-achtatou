package com.house365.core.util;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

public class EventUtil {
    public static boolean hasCover(MotionEvent ev, View view) {
        float x = ev.getX();
        float y = ev.getY();
        Rect mRectSrc = new Rect();
        view.getGlobalVisibleRect(mRectSrc);
        return mRectSrc.contains((int) x, (int) y);
    }
}
