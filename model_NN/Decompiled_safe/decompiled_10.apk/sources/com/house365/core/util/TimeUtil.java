package com.house365.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class TimeUtil {
    public static long getCurrentTime() {
        return System.currentTimeMillis() / 1000;
    }

    public static String getBetween(long time) {
        long timedeta = (System.currentTimeMillis() / 1000) - time;
        if (timedeta < 3600) {
            return String.valueOf(Math.round((float) (timedeta / 60))) + "分钟";
        }
        if (timedeta < 86400) {
            return String.valueOf(Math.round((float) (timedeta / 3600))) + "小时";
        }
        int n = Math.round((float) (timedeta / 86400));
        if (n < 3) {
            return String.valueOf(n) + "天";
        }
        return toDate(time);
    }

    public static String toDayAndHour(long time) {
        return new SimpleDateFormat("MM-dd HH:mm").format(new Date(1000 * time)).toString();
    }

    public static String toDateAndTime(long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(1000 * time)).toString();
    }

    public static String toTime(long time) {
        return new SimpleDateFormat("HH:mm").format(new Date(1000 * time)).toString();
    }

    public static String toDate(long time) {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(1000 * time)).toString();
    }

    public static String toDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
    }

    public static String toYear(long time) {
        return new SimpleDateFormat("yyyy").format(new Date(1000 * time)).toString();
    }

    public static String toMonth(long time) {
        return new SimpleDateFormat("MM").format(new Date(1000 * time)).toString();
    }

    public static String toDay(long time) {
        return new SimpleDateFormat("dd").format(new Date(1000 * time)).toString();
    }

    public static String toNDate(long time) {
        return new SimpleDateFormat("yyyy/MM/dd").format(Long.valueOf(1000 * time)).toString();
    }

    public static String toMDate(long time) {
        return new SimpleDateFormat("yyyy��MM��dd��").format(new Date(1000 * time)).toString();
    }

    public static String toWeekAndDate(long time) {
        return new SimpleDateFormat("yyyy��MM��dd�� HH:mm:ss").format(new Date(1000 * time)).toString();
    }

    public static String toDateWithFormat(long time, String format) {
        return new SimpleDateFormat(format).format(new Date(1000 * time)).toString();
    }

    public static int getBetweenDays(long start, long end) {
        return ((int) ((end - start) / 86400)) + 1;
    }

    public static String formatLongToTimeStr(Long l, String format) {
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = (int) Math.floor((double) (l.longValue() / 1000));
        if (second > 60) {
            minute = second / 60;
            second %= 60;
        }
        if (minute > 60) {
            hour = minute / 60;
            minute %= 60;
        }
        if (hour > 24) {
            day = hour / 24;
            hour %= 24;
        }
        return StringUtils.replaceEach(format, new String[]{"dd", "HH", "mm", "ss"}, new String[]{String.format("%0$02d", Integer.valueOf(day)), String.format("%0$02d", Integer.valueOf(hour)), String.format("%0$02d", Integer.valueOf(minute)), String.format("%0$02d", Integer.valueOf(second))});
    }

    public static boolean isCurday(long time, String date) {
        if (toDate(time).equals(date)) {
            return true;
        }
        return false;
    }

    public static List<String> getBetweenCalendar(long startime, long endtime) {
        List<String> list = new ArrayList<>();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        Date dateBegin = null;
        Date dateEnd = null;
        try {
            dateBegin = formater.parse(formater.format(Long.valueOf((startime - 86400) * 1000)));
            dateEnd = formater.parse(formater.format(Long.valueOf((endtime - 86400) * 1000)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        if (!(dateBegin == null || dateEnd == null)) {
            while (dateBegin.compareTo(dateEnd) <= 0) {
                calendar.setTime(dateBegin);
                calendar.add(5, 1);
                dateBegin = calendar.getTime();
                list.add(formater.format(dateBegin));
            }
        }
        return list;
    }

    public static String toDateStrWithFormat(Date date, String format) {
        return new SimpleDateFormat(format).format(date).toString();
    }

    public static Date parseDate(String s, String format) {
        try {
            return new SimpleDateFormat(format).parse(s);
        } catch (Exception e) {
            return null;
        }
    }

    public static long getDateSeconds(String s, String format) {
        Date d = parseDate(s, format);
        if (d != null) {
            return d.getTime() / 1000;
        }
        return System.currentTimeMillis() / 1000;
    }
}
