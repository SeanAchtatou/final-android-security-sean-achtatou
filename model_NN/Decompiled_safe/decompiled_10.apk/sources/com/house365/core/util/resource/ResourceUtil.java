package com.house365.core.util.resource;

import android.content.Context;
import android.content.res.ColorStateList;

public class ResourceUtil {

    public enum ResourceType {
        id,
        drawable,
        color,
        string
    }

    public static int getResourceId(Context context, String name, ResourceType type) {
        String sType = "id";
        if (type == ResourceType.id) {
            sType = "id";
        } else if (type == ResourceType.drawable) {
            sType = "drawable";
        } else if (type == ResourceType.color) {
            sType = "color";
        } else if (type == ResourceType.string) {
            sType = "string";
        }
        return context.getResources().getIdentifier(name, sType, context.getPackageName());
    }

    public static ColorStateList getColor(Context context, int id) {
        return context.getResources().getColorStateList(id);
    }
}
