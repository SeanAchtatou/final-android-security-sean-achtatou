package com.house365.core.util;

import android.os.Environment;
import android.os.StatFs;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import biz.source_code.base64Coder.Base64Coder;
import com.house365.core.constant.CorePreferences;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.lang.StringUtils;

public class FileUtil {
    public static boolean isSDCARDMounted() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean isExistFile(String filename) {
        if (!new File(filename).exists()) {
            return false;
        }
        return true;
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(line);
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static String getFileBase64String(File file) throws IOException {
        byte[] data = null;
        try {
            InputStream in = new FileInputStream(file);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Base64Coder.encodeLines(data);
    }

    public static String getFileNameFromUrl(String s) {
        int i = s.lastIndexOf("\\");
        return ((i < 0 || i >= s.length() + -1) && ((i = s.lastIndexOf("/")) < 0 || i >= s.length() + -1)) ? s : s.substring(i + 1);
    }

    public static String getMIMEType(File f) {
        String type;
        String fName = f.getName();
        String end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg") || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("3gp") || end.equals("mp4")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            type = CorePreferences.IMAGEPATH;
        } else if (end.equals("apk")) {
            type = "application/vnd.android.package-archive";
        } else if (end.equals("doc") || end.equals("docx")) {
            type = "application/msword";
        } else if (end.equals("xls") || end.equals("xlsx")) {
            type = "application/vnd.ms-excel";
        } else if (end.equals("ppt") || end.equals("pptx")) {
            type = "application/vnd.ms-powerpoint";
        } else if (end.equals("pdf")) {
            type = "application/pdf";
        } else {
            type = "*";
        }
        if (type.indexOf("/") == -1) {
            return String.valueOf(type) + "/*";
        }
        return type;
    }

    public static void chmod(String permission, String path) {
        try {
            Runtime.getRuntime().exec("chmod " + permission + " " + path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String filename) {
        File myFile = new File(filename);
        if (myFile.exists()) {
            myFile.delete();
        }
    }

    public static String getExtensionName(String filename) {
        int dot;
        if (filename == null || filename.length() <= 0 || (dot = filename.lastIndexOf(46)) <= -1 || dot >= filename.length() - 1) {
            return filename;
        }
        return filename.substring(dot + 1);
    }

    public static String getFileNameNoEx(String filename) {
        int dot;
        if (filename == null || filename.length() <= 0 || (dot = filename.lastIndexOf(46)) <= -1 || dot >= filename.length()) {
            return filename;
        }
        return filename.substring(0, dot);
    }

    public static String convertFileSize(long filesize) {
        String strUnit = "B";
        int intDivisor = 1;
        if (filesize >= 1073741824) {
            strUnit = "GB";
            intDivisor = 1073741824;
        } else if (filesize >= 1048576) {
            strUnit = "MB";
            intDivisor = 1048576;
        } else if (filesize >= 1024) {
            strUnit = "KB";
            intDivisor = AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END;
        }
        if (intDivisor == 1) {
            return String.valueOf(filesize) + " " + strUnit;
        }
        String strAfterComma = new StringBuilder().append((100 * (filesize % ((long) intDivisor))) / ((long) intDivisor)).toString();
        if (strAfterComma == StringUtils.EMPTY) {
            strAfterComma = ".0";
        }
        return String.valueOf(filesize / ((long) intDivisor)) + "." + strAfterComma + " " + strUnit;
    }

    public static double getDirSize(File file) {
        if (!file.exists()) {
            return 0.0d;
        }
        if (!file.isDirectory()) {
            return (double) file.length();
        }
        double size = 0.0d;
        for (File f : file.listFiles()) {
            size += getDirSize(f);
        }
        return size;
    }

    public static long getSDCardSize(String path) {
        StatFs statfs = new StatFs(new File(path).getPath());
        long nTotalBlocks = (long) statfs.getBlockCount();
        long nBlocSize = (long) statfs.getBlockSize();
        long availableBlocks = (long) statfs.getAvailableBlocks();
        long freeBlocks = (long) statfs.getFreeBlocks();
        return ((nTotalBlocks * nBlocSize) / 1024) / 1024;
    }

    public static void delFolder(String folderPath) {
        try {
            delAllFile(folderPath);
            new File(folderPath.toString()).delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean delAllFile(String path) {
        File temp;
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return false;
        }
        if (!file.isDirectory()) {
            return false;
        }
        String[] tempList = file.list();
        for (int i = 0; i < tempList.length; i++) {
            if (path.endsWith(File.separator)) {
                temp = new File(String.valueOf(path) + tempList[i]);
            } else {
                temp = new File(String.valueOf(path) + File.separator + tempList[i]);
            }
            if (temp.isFile()) {
                temp.delete();
            }
            if (temp.isDirectory()) {
                delAllFile(String.valueOf(path) + "/" + tempList[i]);
                delFolder(String.valueOf(path) + "/" + tempList[i]);
                flag = true;
            }
        }
        return flag;
    }
}
