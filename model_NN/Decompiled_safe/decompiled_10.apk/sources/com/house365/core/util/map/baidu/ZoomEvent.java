package com.house365.core.util.map.baidu;

public class ZoomEvent {
    public static final int ZOOM_IN = 1;
    public static final int ZOOM_OUT = -1;
    private int action;
    private long eventTime;
    private int zoomLevel;

    public int getAction() {
        return this.action;
    }

    public void setAction(int action2) {
        this.action = action2;
    }

    public long getEventTime() {
        return this.eventTime;
    }

    public void setEventTime(long eventTime2) {
        this.eventTime = eventTime2;
    }

    public int getZoomLevel() {
        return this.zoomLevel;
    }

    public void setZoomLevel(int zoomLevel2) {
        this.zoomLevel = zoomLevel2;
    }

    public String toString() {
        return "ZoomEvent={" + "action=" + this.action + " eventTime=" + this.eventTime + " zoomLevel=" + this.zoomLevel + "}";
    }
}
