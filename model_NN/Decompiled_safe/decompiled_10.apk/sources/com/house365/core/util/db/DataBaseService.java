package com.house365.core.util.db;

import android.content.Context;
import android.database.Cursor;
import com.house365.core.util.db.DataBaseOpenHelper;

public class DataBaseService {
    private DataBaseOpenHelper dbOpenHelper;

    public interface DBQuery {
        Object onQueryResult(Cursor cursor);
    }

    public DataBaseService() {
    }

    public DataBaseService(Context context, String dbname, int version, DataBaseOpenHelper.DataBaseOpenListener listener) {
        this.dbOpenHelper = new DataBaseOpenHelper(context, dbname, version, listener);
    }

    public void execute(String sql, Object[] values) {
        this.dbOpenHelper.getWritableDatabase().execSQL(sql, values);
    }

    public Object query(String sql, String[] values, DBQuery dbQuery) {
        Cursor cursor = this.dbOpenHelper.getReadableDatabase().rawQuery(sql, values);
        Object o = dbQuery.onQueryResult(cursor);
        cursor.close();
        return o;
    }

    public long getCount(String sql) {
        Cursor cursor = this.dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
        if (cursor.moveToNext()) {
            return cursor.getLong(0);
        }
        return 0;
    }

    public DataBaseOpenHelper getDbOpenHelper() {
        return this.dbOpenHelper;
    }

    public void destory() {
        this.dbOpenHelper.close();
    }
}
