package com.house365.core.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.inter.ConfirmDialogListener;

public class DialogUtil {
    public static void showConfrimDialog(Context context, String msg, String positiveButtonText, ConfirmDialogListener dialogListener) {
        showConfrimDialog(context, (String) null, msg, positiveButtonText, context.getResources().getString(R.string.dialog_button_cancel), dialogListener);
    }

    public static void showConfrimDialog(Context context, int msg, int positiveButtonText, ConfirmDialogListener dialogListener) {
        showConfrimDialog(context, -1, msg, positiveButtonText, R.string.dialog_button_cancel, dialogListener);
    }

    public static void showConfrimDialog(Context context, String title, String msg, String positiveButtonText, String negativeButtonText, final ConfirmDialogListener dialogListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(msg)) {
            builder.setMessage(msg);
        }
        if (!TextUtils.isEmpty(negativeButtonText)) {
            builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (ConfirmDialogListener.this != null) {
                        ConfirmDialogListener.this.onNegative(dialog);
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(positiveButtonText)) {
            builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (ConfirmDialogListener.this != null) {
                        ConfirmDialogListener.this.onPositive(dialog);
                    }
                    dialog.dismiss();
                }
            });
        }
        builder.show();
    }

    public static void showConfrimDialog(Context context, int title, int msg, int positiveButtonText, int negativeButtonText, final ConfirmDialogListener dialogListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        if (title > 0) {
            builder.setTitle(title);
        }
        if (msg > 0) {
            builder.setMessage(msg);
        }
        if (positiveButtonText > 0) {
            builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (ConfirmDialogListener.this != null) {
                        ConfirmDialogListener.this.onPositive(dialog);
                    }
                    dialog.dismiss();
                }
            });
        }
        if (negativeButtonText > 0) {
            builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (ConfirmDialogListener.this != null) {
                        ConfirmDialogListener.this.onNegative(dialog);
                    }
                }
            });
        }
        builder.show();
    }

    public static void exitDialog(final Context context, int appid) {
        showConfrimDialog(context, appid, R.string.confirm_exit_info, R.string.dialog_button_cancel, R.string.dialog_button_exit, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                context.sendBroadcast(new Intent(ActionTag.INTENT_ACTION_LOGGED_OUT));
                ((Activity) context).getApplication().onTerminate();
            }

            public void onNegative(DialogInterface dialog) {
            }
        });
    }
}
