package com.house365.core.util.map.google.lazyload;

import com.house365.core.util.map.google.ManagedOverlay;

public interface LazyLoadListener {
    void onBegin(ManagedOverlay managedOverlay);

    void onError(LazyLoadException lazyLoadException, ManagedOverlay managedOverlay);

    void onSuccess(ManagedOverlay managedOverlay);
}
