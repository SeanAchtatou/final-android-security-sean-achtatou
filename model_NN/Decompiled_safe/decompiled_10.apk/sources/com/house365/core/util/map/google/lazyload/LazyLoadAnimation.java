package com.house365.core.util.map.google.lazyload;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class LazyLoadAnimation {
    public static final int FRAME_DURATION = 150;
    private static final String LOG_TAG = "Maps_LazyLoadAnimation";
    AnimationDrawable anim;
    private boolean first = true;
    ImageView imageView;

    public LazyLoadAnimation(ImageView imageView2) {
        this.imageView = imageView2;
        this.imageView.setVisibility(4);
    }

    private void initDefault() {
        this.anim = new AnimationDrawable();
        this.anim.setOneShot(false);
        for (int i = 0; i < 8; i++) {
            Drawable drawable = Drawable.createFromStream(getClass().getResourceAsStream(String.valueOf("/de/android1/overlaymanager/lazyload/anim/") + String.format("loader0%s.png", Integer.valueOf(i))), null);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            this.anim.addFrame(drawable, 150);
        }
        this.imageView.setBackgroundDrawable(this.anim);
    }

    public void stop() {
        this.imageView.setVisibility(4);
        this.anim.setVisible(false, false);
        this.anim.stop();
        this.imageView.postInvalidate();
    }

    public void start() {
        if (this.first) {
            if (this.anim == null) {
                initDefault();
                this.anim.setBounds(0, 0, this.anim.getMinimumWidth(), this.anim.getMinimumHeight());
            }
            this.first = false;
        }
        this.imageView.setVisibility(0);
        this.anim.setVisible(true, true);
        this.anim.start();
        this.imageView.postInvalidate();
    }

    public void setAnimationDrawable(AnimationDrawable anim2) {
        this.anim = anim2;
        this.imageView.setBackgroundDrawable(anim2);
    }
}
