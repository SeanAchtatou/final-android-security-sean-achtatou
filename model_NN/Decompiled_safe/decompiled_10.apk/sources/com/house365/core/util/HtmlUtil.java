package com.house365.core.util;

import com.baidu.location.BDLocation;
import com.house365.androidpn.client.Constants;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParserConstants;

public class HtmlUtil {
    private static final String regxpForHtml = "<([^>]*)>";
    private static final String regxpForImaTagSrcAttrib = "src=\"([^\"]+)\"";
    private static final String regxpForImgTag = "<\\s*img\\s+([^>]*)\\s*>";

    public String replaceTag(String input) {
        if (!hasSpecialChars(input)) {
            return input;
        }
        StringBuffer filtered = new StringBuffer(input.length());
        for (int i = 0; i <= input.length() - 1; i++) {
            char c = input.charAt(i);
            switch (c) {
                case '\"':
                    filtered.append("&quot;");
                    break;
                case DateTimeParserConstants.COMMENT /*38*/:
                    filtered.append("&amp;");
                    break;
                case Constants.HEART_BEAT_INTERVAL:
                    filtered.append("&lt;");
                    break;
                case BDLocation.TypeCriteriaException:
                    filtered.append("&gt;");
                    break;
                default:
                    filtered.append(c);
                    break;
            }
        }
        return filtered.toString();
    }

    public boolean hasSpecialChars(String input) {
        boolean flag = false;
        if (input != null && input.length() > 0) {
            for (int i = 0; i <= input.length() - 1; i++) {
                switch (input.charAt(i)) {
                    case '\"':
                        flag = true;
                        break;
                    case DateTimeParserConstants.COMMENT /*38*/:
                        flag = true;
                        break;
                    case Constants.HEART_BEAT_INTERVAL:
                        flag = true;
                        break;
                    case BDLocation.TypeCriteriaException:
                        flag = true;
                        break;
                }
            }
        }
        return flag;
    }

    public static String filterHtml(String str) {
        Matcher matcher = Pattern.compile(regxpForHtml).matcher(str);
        StringBuffer sb = new StringBuffer();
        for (boolean result1 = matcher.find(); result1; result1 = matcher.find()) {
            matcher.appendReplacement(sb, StringUtils.EMPTY);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String fiterHtmlTag(String str, String tag) {
        Matcher matcher = Pattern.compile("<\\s*" + tag + "\\s+([^>]*)\\s*>").matcher(str);
        StringBuffer sb = new StringBuffer();
        for (boolean result1 = matcher.find(); result1; result1 = matcher.find()) {
            matcher.appendReplacement(sb, StringUtils.EMPTY);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    public static String replaceHtmlTag(String str, String beforeTag, String tagAttrib, String startTag, String endTag) {
        Pattern patternForTag = Pattern.compile("<\\s*" + beforeTag + "\\s+([^>]*)\\s*>");
        Pattern patternForAttrib = Pattern.compile(String.valueOf(tagAttrib) + "=\"([^\"]+)\"");
        Matcher matcherForTag = patternForTag.matcher(str);
        StringBuffer sb = new StringBuffer();
        for (boolean result = matcherForTag.find(); result; result = matcherForTag.find()) {
            StringBuffer sbreplace = new StringBuffer();
            Matcher matcherForAttrib = patternForAttrib.matcher(matcherForTag.group(1));
            if (matcherForAttrib.find()) {
                matcherForAttrib.appendReplacement(sbreplace, String.valueOf(startTag) + matcherForAttrib.group(1) + endTag);
            }
            matcherForTag.appendReplacement(sb, sbreplace.toString());
        }
        matcherForTag.appendTail(sb);
        return sb.toString();
    }
}
