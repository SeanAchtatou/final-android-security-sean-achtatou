package com.house365.core.util.map.baidu.lazyload;

import com.house365.core.util.map.baidu.ManagedOverlay;

public interface LazyLoadListener {
    void onBegin(ManagedOverlay managedOverlay);

    void onError(LazyLoadException lazyLoadException, ManagedOverlay managedOverlay);

    void onSuccess(ManagedOverlay managedOverlay);
}
