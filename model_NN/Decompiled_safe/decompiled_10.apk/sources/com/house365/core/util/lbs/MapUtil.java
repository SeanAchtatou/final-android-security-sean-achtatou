package com.house365.core.util.lbs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import com.google.android.maps.GeoPoint;
import com.house365.core.R;
import com.house365.core.inter.ConfirmDialogListener;
import com.house365.core.util.AbstractCallBack;
import com.house365.core.util.ActivityUtil;
import java.lang.reflect.Array;

public class MapUtil {
    private static final int DEVIANTLAT = -2083;
    private static final int DEVIANTLNG = 5169;
    private static final double ItudePer = 0.00899289d;
    private static final double PI = 3.14159265d;
    private static final double RADIUS = 6371229.0d;
    public static int latlongTometre = 110950;

    public static double distance(double wd1, double jd1, double wd2, double jd2) {
        return Math.hypot(((((jd2 - jd1) * PI) * RADIUS) * Math.cos((((wd1 + wd2) / 2.0d) * PI) / 180.0d)) / 180.0d, (((wd2 - wd1) * PI) * RADIUS) / 180.0d) / 1000.0d;
    }

    public static double getLatPer() {
        return ItudePer;
    }

    public static double getLngPer(double lat) {
        return ItudePer * Math.cos(lat);
    }

    public static String getDistance(double p1Lat, double p1Lng, double p2Lat, double p2Lng) {
        double distance = distance(p1Lat, p1Lng, p2Lat, p2Lng) * 1000.0d;
        if (distance > 10000.0d) {
            return String.valueOf((int) (distance / 1000.0d)) + "km";
        }
        return String.valueOf((int) distance) + "m";
    }

    public static String getDistance(Location loaction, double p2Lat, double p2Lng) {
        double distance = 0.0d;
        try {
            distance = distance(loaction.getLatitude(), loaction.getLongitude(), p2Lat, p2Lng) * 1000.0d;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (distance > 10000.0d) {
            return String.valueOf((int) (distance / 1000.0d)) + "km";
        }
        return String.valueOf((int) distance) + "m";
    }

    public static double[][] GetTwoPointLocation(double p1Lat, double p1Lng, int distance) {
        double secDistance = Math.sqrt(Math.pow((double) distance, 2.0d) / 2.0d) / ((double) latlongTometre);
        double[][] twoPointLoact = (double[][]) Array.newInstance(Double.TYPE, 2, 2);
        twoPointLoact[0][0] = p1Lat + secDistance;
        twoPointLoact[0][1] = p1Lng - secDistance;
        twoPointLoact[1][0] = p1Lat - secDistance;
        twoPointLoact[1][1] = p1Lng + secDistance;
        return twoPointLoact;
    }

    public static GeoPoint getPoint(Location location) {
        if (location != null) {
            return transformToSpark(location.getLatitude(), location.getLongitude());
        }
        return null;
    }

    public static GeoPoint getPoint(double lat, double lng) {
        return transformToSpark(lat, lng);
    }

    private static GeoPoint transformToSpark(double lat, double lng) {
        return new GeoPoint(((int) (lat * 1000000.0d)) + DEVIANTLAT, ((int) (lng * 1000000.0d)) + DEVIANTLNG);
    }

    public static boolean isAvailableGoogleMap() {
        try {
            Class.forName("com.google.android.maps.MapView");
            Class.forName("com.google.android.maps.GeoPoint");
            Class.forName("com.google.android.maps.MapController");
            Class.forName("com.google.android.maps.Overlay");
            Class.forName("com.google.android.maps.OverlayItem");
            Class.forName("com.google.android.maps.Projection");
            Class.forName("com.google.android.maps.MapActivity");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void openGpsDialog(final Context context, int msgId, final AbstractCallBack finshMethod) {
        ActivityUtil.showConfrimDialog(context, R.string.text_location_no, R.string.text_location_yes, R.string.text_location_title, msgId, new ConfirmDialogListener() {
            public void onPositive(DialogInterface dialog) {
                ((Activity) context).startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 1);
            }

            public void onNegative(DialogInterface dialog) {
                dialog.cancel();
                finshMethod.doCallBack();
            }
        });
    }
}
