package com.house365.core.util;

public class RefreshInfo {
    private int avgpage = 20;
    public boolean hasFooter = false;
    public int page = 1;
    public boolean refresh = true;

    public int getAvgpage() {
        return this.avgpage;
    }

    public void setAvgpage(int avgpage2) {
        this.avgpage = avgpage2;
    }

    public boolean isRefresh() {
        return this.refresh;
    }

    public int getPage() {
        return this.page;
    }

    public boolean isHasFooter() {
        return this.hasFooter;
    }

    public void setRefresh(boolean refresh2) {
        this.refresh = refresh2;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public void setHasFooter(boolean hasFooter2) {
        this.hasFooter = hasFooter2;
    }
}
