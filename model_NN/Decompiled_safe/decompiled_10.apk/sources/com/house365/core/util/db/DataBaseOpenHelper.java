package com.house365.core.util.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseOpenHelper extends SQLiteOpenHelper {
    private DataBaseOpenListener listener;

    public interface DataBaseOpenListener {
        String getCreateSql();

        String[] getIndexSql();

        void onVersionUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2);
    }

    public DataBaseOpenHelper(Context context, String dbname, int version, DataBaseOpenListener listener2) {
        super(context, dbname, (SQLiteDatabase.CursorFactory) null, version);
        this.listener = listener2;
    }

    public DataBaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.listener.getCreateSql());
        if (this.listener.getIndexSql() != null) {
            for (String execSQL : this.listener.getIndexSql()) {
                db.execSQL(execSQL);
            }
        }
    }

    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        this.listener.onVersionUpgrade(db, arg1, arg2);
    }
}
