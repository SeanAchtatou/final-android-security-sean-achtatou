package com.house365.core.util.map.google;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import java.util.ArrayList;
import java.util.List;

public class OverlayManager {
    Context ctx;
    MapView mapView;
    List<ManagedOverlay> overlays = new ArrayList();

    public void populate() {
        List<Overlay> mapoverlays = this.mapView.getOverlays();
        List<Overlay> newoverlays = new ArrayList<>();
        for (int i = 0; i < mapoverlays.size(); i++) {
            Overlay overlay = (Overlay) mapoverlays.get(i);
            if (!(overlay instanceof ManagedOverlay)) {
                newoverlays.add(overlay);
            }
        }
        for (int i2 = 0; i2 < this.overlays.size(); i2++) {
            ManagedOverlay overlay2 = this.overlays.get(i2);
            overlay2.init();
            newoverlays.add(overlay2);
        }
        mapoverlays.clear();
        mapoverlays.addAll(newoverlays);
        this.mapView.invalidate();
    }

    public void close() {
        for (ManagedOverlay managedOverlay : this.overlays) {
            managedOverlay.close();
        }
    }

    public Context getContext() {
        return this.ctx;
    }

    public OverlayManager(Context ctx2, MapView mapView2) {
        this.mapView = mapView2;
        this.ctx = ctx2;
    }

    public ManagedOverlay createOverlay(String name, Drawable defaultMarker) {
        ManagedOverlay overlay = new ManagedOverlay(this, name, defaultMarker);
        this.overlays.add(overlay);
        return overlay;
    }

    public ManagedOverlay createOverlay(Drawable defaultMarker) {
        return createOverlay(null, defaultMarker);
    }

    public ManagedOverlay createOverlay(String name) {
        return createOverlay(name, createDefaultMarker());
    }

    public ManagedOverlay createOverlay() {
        return createOverlay(null, createDefaultMarker());
    }

    public boolean removeOverlay(ManagedOverlay overlay) {
        if (overlay == null) {
            return false;
        }
        this.overlays.remove(overlay);
        return true;
    }

    public boolean removeOverlay(String name) {
        return removeOverlay(getOverlay(name));
    }

    public ManagedOverlay getOverlay(int i) {
        if (this.overlays.size() >= i) {
            return this.overlays.get(i);
        }
        return null;
    }

    public MapView getMapView() {
        return this.mapView;
    }

    public ManagedOverlay getOverlay(String name) {
        for (int i = 0; i < this.overlays.size(); i++) {
            ManagedOverlay overlay = this.overlays.get(i);
            if (name.equals(overlay.getName())) {
                return overlay;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Drawable createDefaultMarker() {
        Bitmap bitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(-16776961);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(6.0f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(25.0f, 25.0f, 25.0f, paint);
        BitmapDrawable bd = new BitmapDrawable(bitmap);
        bd.setBounds(0, 0, bd.getIntrinsicWidth(), bd.getIntrinsicHeight());
        return bd;
    }
}
