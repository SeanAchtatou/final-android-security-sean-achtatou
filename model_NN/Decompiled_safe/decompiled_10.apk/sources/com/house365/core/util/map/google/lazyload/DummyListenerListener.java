package com.house365.core.util.map.google.lazyload;

import android.view.MotionEvent;
import com.google.android.maps.GeoPoint;
import com.house365.core.util.map.google.ManagedOverlay;
import com.house365.core.util.map.google.ManagedOverlayGestureDetector;
import com.house365.core.util.map.google.ManagedOverlayItem;
import com.house365.core.util.map.google.ZoomEvent;

public class DummyListenerListener implements ManagedOverlayGestureDetector.OnOverlayGestureListener {
    public boolean onZoom(ZoomEvent zoom, ManagedOverlay overlay) {
        return false;
    }

    public boolean onDoubleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
        return false;
    }

    public void onLongPress(MotionEvent e, ManagedOverlay overlay) {
    }

    public void onLongPressFinished(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
    }

    public boolean onScrolled(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY, ManagedOverlay overlay) {
        return false;
    }

    public boolean onSingleTap(MotionEvent e, ManagedOverlay overlay, GeoPoint point, ManagedOverlayItem item) {
        return false;
    }
}
