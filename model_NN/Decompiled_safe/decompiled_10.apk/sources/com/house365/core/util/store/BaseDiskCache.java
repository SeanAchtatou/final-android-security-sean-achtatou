package com.house365.core.util.store;

import android.os.Environment;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.FileUtil;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BaseDiskCache implements DiskCache {
    private static final int MIN_FILE_SIZE_IN_BYTES = 100;
    private static final String NOMEDIA = ".nomedia";
    private static final String TAG = "BaseDiskCache";
    private boolean aviable = false;
    private File mStorageDirectory;

    public BaseDiskCache(String dirPath, String name) {
        File baseDirectory;
        if (FileUtil.isSDCARDMounted()) {
            baseDirectory = new File(Environment.getExternalStorageDirectory(), dirPath);
        } else {
            baseDirectory = new File(dirPath);
        }
        File storageDirectory = new File(baseDirectory, name);
        this.aviable = createDirectory(storageDirectory);
        this.mStorageDirectory = storageDirectory;
        cleanupSimple();
    }

    public boolean exists(String key) {
        return getFile(key).exists();
    }

    public File getFile(String hash) {
        return new File(String.valueOf(this.mStorageDirectory.toString()) + File.separator + Math.abs(hash.hashCode()));
    }

    public InputStream getInputStream(String hash) throws IOException {
        return new FileInputStream(getFile(hash));
    }

    public void store(String key, InputStream is) {
        CorePreferences.DEBUG("BaseDiskCache: " + key);
        InputStream is2 = new BufferedInputStream(is);
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(getFile(key)));
            byte[] b = new byte[AccessibilityEventCompat.TYPE_WINDOW_CONTENT_CHANGED];
            while (true) {
                int count = is2.read(b);
                if (count <= 0) {
                    os.close();
                    CorePreferences.DEBUG("BaseDiskCachestore complete: " + key);
                    return;
                }
                os.write(b, 0, count);
            }
        } catch (IOException e) {
            CorePreferences.ERROR("BaseDiskCachestore failed to store: " + key, e);
        }
    }

    public void invalidate(String key) {
        getFile(key).delete();
    }

    public void cleanup() {
        String[] children = this.mStorageDirectory.list();
        if (children != null) {
            for (String file : children) {
                File child = new File(this.mStorageDirectory, file);
                if (!child.equals(new File(this.mStorageDirectory, NOMEDIA)) && child.length() <= 100) {
                    CorePreferences.DEBUG("BaseDiskCacheDeleting: " + child);
                    child.delete();
                }
            }
        }
    }

    public void cleanupSimple() {
        String[] children = this.mStorageDirectory.list();
        if (children != null) {
            CorePreferences.DEBUG("BaseDiskCacheFound disk cache length to be: " + children.length);
            if (children.length > 2000) {
                CorePreferences.DEBUG("BaseDiskCacheDisk cache found to : " + children);
                for (int i = 0; i < 100; i++) {
                    File child = new File(this.mStorageDirectory, children[i]);
                    CorePreferences.DEBUG("BaseDiskCache  deleting: " + child.getName());
                    child.delete();
                }
            }
        }
    }

    public void clear() {
        String[] children = this.mStorageDirectory.list();
        if (children != null) {
            for (String file : children) {
                File child = new File(this.mStorageDirectory, file);
                if (!child.equals(new File(this.mStorageDirectory, NOMEDIA))) {
                    CorePreferences.DEBUG("BaseDiskCacheDeleting: " + child);
                    child.delete();
                }
            }
        }
        this.mStorageDirectory.delete();
    }

    private static final boolean createDirectory(File storageDirectory) {
        if (!storageDirectory.exists()) {
            Log.d(TAG, "Trying to create storageDirectory: " + String.valueOf(storageDirectory.mkdirs()));
            Log.d(TAG, "Exists: " + storageDirectory + " " + String.valueOf(storageDirectory.exists()));
            Log.d(TAG, "State: " + Environment.getExternalStorageState());
            Log.d(TAG, "Isdir: " + storageDirectory + " " + String.valueOf(storageDirectory.isDirectory()));
            Log.d(TAG, "Readable: " + storageDirectory + " " + String.valueOf(storageDirectory.canRead()));
            Log.d(TAG, "Writable: " + storageDirectory + " " + String.valueOf(storageDirectory.canWrite()));
            File tmp = storageDirectory.getParentFile();
            Log.d(TAG, "Exists: " + tmp + " " + String.valueOf(tmp.exists()));
            Log.d(TAG, "Isdir: " + tmp + " " + String.valueOf(tmp.isDirectory()));
            Log.d(TAG, "Readable: " + tmp + " " + String.valueOf(tmp.canRead()));
            Log.d(TAG, "Writable: " + tmp + " " + String.valueOf(tmp.canWrite()));
            File tmp2 = tmp.getParentFile();
            Log.d(TAG, "Exists: " + tmp2 + " " + String.valueOf(tmp2.exists()));
            Log.d(TAG, "Isdir: " + tmp2 + " " + String.valueOf(tmp2.isDirectory()));
            Log.d(TAG, "Readable: " + tmp2 + " " + String.valueOf(tmp2.canRead()));
            Log.d(TAG, "Writable: " + tmp2 + " " + String.valueOf(tmp2.canWrite()));
        }
        File nomediaFile = new File(storageDirectory, NOMEDIA);
        if (!nomediaFile.exists()) {
            try {
                Log.d(TAG, "Created file: " + nomediaFile + " " + String.valueOf(nomediaFile.createNewFile()));
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Unable to create .nomedia file for some reason.", e);
            }
        }
        if (storageDirectory.isDirectory() && storageDirectory.exists() && nomediaFile.exists()) {
            return true;
        }
        Log.d(TAG, "Unable to create storage directory and nomedia file.");
        return false;
    }

    public boolean aviable() {
        return this.aviable;
    }
}
