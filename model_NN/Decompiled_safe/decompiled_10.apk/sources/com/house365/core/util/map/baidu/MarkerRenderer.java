package com.house365.core.util.map.baidu;

import android.graphics.drawable.Drawable;

public interface MarkerRenderer {
    Drawable render(ManagedOverlayItem managedOverlayItem, Drawable drawable, int i);
}
