package com.house365.core.util.map.google;

import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import org.apache.commons.lang.StringUtils;

public class ManagedOverlayItem extends OverlayItem {
    private Drawable customRenderedDrawable;
    private ManagedOverlay parentOverlay;

    public ManagedOverlayItem(GeoPoint point, String title, String snippet) {
        super(point, title, snippet);
    }

    public ManagedOverlay getOverlay() {
        return this.parentOverlay;
    }

    public void setOverlay(ManagedOverlay parentOverlay2) {
        this.parentOverlay = parentOverlay2;
    }

    public Drawable getMarker(int i) {
        if (this.parentOverlay.customMarkerRenderer != null) {
            return this.parentOverlay.customMarkerRenderer.render(this, this.parentOverlay.defaultMarker, i);
        }
        return ManagedOverlayItem.super.getMarker(i);
    }

    public MapView getMapView() {
        return getOverlay().getManager().getMapView();
    }

    public Drawable getCustomRenderedDrawable() {
        return this.customRenderedDrawable;
    }

    public void setCustomRenderedDrawable(Drawable customRenderedDrawable2) {
        this.customRenderedDrawable = customRenderedDrawable2;
    }

    public static class Builder {
        private GeoPoint p = null;
        private String snippet = StringUtils.EMPTY;
        private String title = StringUtils.EMPTY;

        public Builder() {
        }

        public Builder(GeoPoint p2) {
            this.p = p2;
        }

        public Builder name(String title2) {
            this.title = title2;
            return this;
        }

        public Builder snippet(String snippet2) {
            this.snippet = snippet2;
            return this;
        }

        public ManagedOverlayItem create() {
            return new ManagedOverlayItem(this.p, this.title, this.snippet);
        }
    }
}
