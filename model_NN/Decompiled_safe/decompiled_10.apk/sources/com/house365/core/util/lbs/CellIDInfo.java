package com.house365.core.util.lbs;

public class CellIDInfo {
    public int cellId;
    public int locationAreaCode;
    public String mobileCountryCode;
    public String mobileNetworkCode;
    public String radioType;
}
