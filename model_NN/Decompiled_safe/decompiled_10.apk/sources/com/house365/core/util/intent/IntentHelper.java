package com.house365.core.util.intent;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import com.house365.core.util.FileUtil;
import com.house365.core.util.TextUtil;
import java.io.File;

public class IntentHelper {
    public static final String IMAGE_UNSPECIFIED = "image/*";

    public static Intent getSmsIntent(String mobile, String content) {
        String smsUri = "smsto:";
        if (mobile != null) {
            smsUri = String.valueOf(smsUri) + mobile;
        }
        Intent mIntent = new Intent("android.intent.action.SENDTO", Uri.parse(smsUri));
        if (content != null) {
            mIntent.putExtra("sms_body", content);
        }
        return mIntent;
    }

    public static Intent getCallIntent(String mobile) {
        String callUri = "tel:";
        if (mobile != null) {
            callUri = String.valueOf(callUri) + TextUtil.filterTel(mobile);
        }
        return new Intent("android.intent.action.CALL", Uri.parse(callUri));
    }

    public static Intent getCameraIntent() {
        return new Intent("android.media.action.IMAGE_CAPTURE");
    }

    public static Intent getCameraIntent(File tempfile) {
        Intent shootIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        shootIntent.putExtra("output", Uri.fromFile(tempfile));
        return shootIntent;
    }

    public static Intent getAlbumIntent() {
        Intent intentAlbum = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
        intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        return intentAlbum;
    }

    public static Intent startPhotoZoom(Uri input, Uri output) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(input, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("output", output);
        return intent;
    }

    public static void openFileByType(Context context, File f) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setDataAndType(Uri.fromFile(f), FileUtil.getMIMEType(f));
        context.startActivity(intent);
    }

    public static Intent getMIMEIntent(File f) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(f), FileUtil.getMIMEType(f));
        return intent;
    }
}
