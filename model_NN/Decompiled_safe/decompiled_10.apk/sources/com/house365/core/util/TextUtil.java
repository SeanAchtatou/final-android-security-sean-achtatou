package com.house365.core.util;

import android.content.Context;
import android.text.TextUtils;
import android.widget.TextView;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;

public class TextUtil {
    public static String getString(Context context, int id, Object... formatArgs) {
        return context.getResources().getString(id, formatArgs);
    }

    public static String substring(String str, int toCount, String more) {
        int reInt = 0;
        String reStr = StringUtils.EMPTY;
        if (str == null) {
            return StringUtils.EMPTY;
        }
        char[] tempChar = str.toCharArray();
        for (int kk = 0; kk < tempChar.length && toCount > reInt; kk++) {
            reInt += String.valueOf(tempChar[kk]).getBytes().length;
            reStr = String.valueOf(reStr) + tempChar[kk];
        }
        if (toCount == reInt || toCount == reInt - 1) {
            reStr = String.valueOf(reStr) + more;
        }
        return reStr;
    }

    public static String getRandString(int len) {
        StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
        StringBuffer sb = new StringBuffer();
        Random r = new Random();
        int range = buffer.length();
        for (int i = 0; i < len; i++) {
            sb.append(buffer.charAt(r.nextInt(range)));
        }
        return String.valueOf(new SimpleDateFormat("yyyyMMddhhmmss").format(Long.valueOf(new Date().getTime()))) + sb.toString();
    }

    public static String filterTel(String tel) {
        if (StringUtils.EMPTY.equals(tel) || "����".equals(tel) || "��".equals(tel)) {
            return "4008";
        }
        String tel2 = tel.replace("ת", ",").replace("-", StringUtils.EMPTY).replace(" ", StringUtils.EMPTY);
        if (tel2.contains("/")) {
            return tel2.split("/")[0];
        }
        return tel2;
    }

    public static boolean isVerifyUserName(String username) {
        if (username == null || username.length() < 3) {
            return false;
        }
        if (isEmail(username) || isMobliePhone(username)) {
            return true;
        }
        return false;
    }

    public static boolean isVerifyPassWord(String password) {
        if (password != null && password.length() >= 6) {
            return true;
        }
        return false;
    }

    public static boolean isEmail(String str) {
        return Pattern.compile("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$").matcher(str).matches();
    }

    public static boolean isMobliePhone(String mobiles) {
        if (!TextUtils.isEmpty(mobiles)) {
            return Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,2,5-9]))\\d{8}$").matcher(mobiles).matches();
        }
        return false;
    }

    public static String cutstr(String text, int length, String encode) {
        if (text == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int currentLength = 0;
        for (char c : text.toCharArray()) {
            try {
                currentLength += String.valueOf(c).getBytes(encode).length;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (currentLength > length) {
                break;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static String cutstr(String text, int length) {
        if (text == null) {
            return null;
        }
        return substring(HtmlUtil.filterHtml(text), length, "...");
    }

    public static String Html2Text(String inputString) {
        try {
            return Pattern.compile("<[^>]+", 2).matcher(Pattern.compile("<[^>]+>", 2).matcher(Pattern.compile("<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>", 2).matcher(Pattern.compile("<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>", 2).matcher(inputString).replaceAll(StringUtils.EMPTY)).replaceAll(StringUtils.EMPTY)).replaceAll(StringUtils.EMPTY)).replaceAll(StringUtils.EMPTY);
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }

    public static String getUTF8StringFromGBKString(String gbkStr) {
        try {
            return new String(getUTF8BytesFromGBKString(gbkStr), CharEncoding.UTF_8);
        } catch (UnsupportedEncodingException e) {
            throw new InternalError();
        }
    }

    public static String getUTF8(String gbkStr) {
        if (gbkStr == null) {
            return gbkStr;
        }
        try {
            return new String(gbkStr.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            return gbkStr;
        }
    }

    public static byte[] getUTF8BytesFromGBKString(String gbkStr) {
        int k;
        int n = gbkStr.length();
        byte[] utfBytes = new byte[(n * 3)];
        int i = 0;
        int k2 = 0;
        while (i < n) {
            int m = gbkStr.charAt(i);
            if (m >= 128 || m < 0) {
                int k3 = k2 + 1;
                utfBytes[k2] = (byte) ((m >> 12) | 224);
                int k4 = k3 + 1;
                utfBytes[k3] = (byte) (((m >> 6) & 63) | 128);
                k = k4 + 1;
                utfBytes[k4] = (byte) ((m & 63) | 128);
            } else {
                k = k2 + 1;
                utfBytes[k2] = (byte) m;
            }
            i++;
            k2 = k;
        }
        if (k2 >= utfBytes.length) {
            return utfBytes;
        }
        byte[] tmp = new byte[k2];
        System.arraycopy(utfBytes, 0, tmp, 0, k2);
        return tmp;
    }

    public static Integer strToInt(String str) {
        if (!TextUtils.isEmpty(str)) {
            return Integer.valueOf(Integer.parseInt(str));
        }
        return 0;
    }

    public static void setNullText(String temp, String str, TextView v) {
        if (temp == null || temp.equals(" ") || TextUtils.isEmpty(temp)) {
            v.setVisibility(8);
            return;
        }
        v.setVisibility(0);
        v.setText(str);
    }

    public static void setNullText(String temp, TextView v) {
        if (temp == null || temp.equals(" ") || TextUtils.isEmpty(temp)) {
            v.setVisibility(8);
            return;
        }
        v.setVisibility(0);
        v.setText(temp);
    }

    public static String[] arrayToShift(String[] tempArray) {
        if (tempArray == null || tempArray.length <= 1) {
            return null;
        }
        String[] chooseItems = new String[(tempArray.length - 1)];
        for (int i = 0; i < tempArray.length; i++) {
            if (i > 0) {
                chooseItems[i - 1] = tempArray[i];
            }
        }
        return chooseItems;
    }
}
