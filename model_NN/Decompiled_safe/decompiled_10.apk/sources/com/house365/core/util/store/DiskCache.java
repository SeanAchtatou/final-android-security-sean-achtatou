package com.house365.core.util.store;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public interface DiskCache {
    boolean aviable();

    void cleanup();

    void clear();

    boolean exists(String str);

    File getFile(String str);

    InputStream getInputStream(String str) throws IOException;

    void invalidate(String str);

    void store(String str, InputStream inputStream);
}
