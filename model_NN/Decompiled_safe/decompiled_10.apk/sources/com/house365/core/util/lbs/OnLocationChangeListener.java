package com.house365.core.util.lbs;

import com.baidu.location.BDLocation;

public interface OnLocationChangeListener {
    void onGetLocation(BDLocation bDLocation);

    void onGetPoi(BDLocation bDLocation);
}
