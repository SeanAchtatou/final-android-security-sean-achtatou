package com.house365.core.util.lbs;

import android.location.Location;
import com.baidu.mapapi.GeoPoint;

public class BaiduMapUtil {
    public static GeoPoint getPoint(Location location) {
        if (location != null) {
            return transformToSpark(location.getLatitude(), location.getLongitude());
        }
        return null;
    }

    public static GeoPoint getPoint(double lat, double lng) {
        return transformToSpark(lat, lng);
    }

    private static GeoPoint transformToSpark(double lat, double lng) {
        return new GeoPoint((int) (lat * 1000000.0d), (int) (lng * 1000000.0d));
    }
}
