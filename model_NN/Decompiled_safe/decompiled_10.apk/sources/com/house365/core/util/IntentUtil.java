package com.house365.core.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.constant.CorePreferences;
import java.io.File;
import org.apache.commons.lang.StringUtils;
import org.apache.james.mime4j.field.ContentTypeField;

public class IntentUtil {
    public static final String IMAGE_UNSPECIFIED = "image/*";

    public static Intent getSmsIntent(String mobile, String content) {
        String smsUri = "smsto:";
        if (mobile != null) {
            smsUri = String.valueOf(smsUri) + mobile;
        }
        Intent mIntent = new Intent("android.intent.action.SENDTO", Uri.parse(smsUri));
        if (content != null) {
            mIntent.putExtra("sms_body", content);
        }
        return mIntent;
    }

    public static Intent getCallIntent(String mobile) {
        String callUri = "tel:";
        if (mobile != null) {
            callUri = String.valueOf(callUri) + filterTel(mobile);
        }
        return new Intent("android.intent.action.CALL", Uri.parse(callUri));
    }

    public static Intent getDialIntent(String mobile) {
        String callUri = "tel:";
        if (mobile != null) {
            callUri = String.valueOf(callUri) + TextUtil.filterTel(mobile);
        }
        return new Intent("android.intent.action.DIAL", Uri.parse(callUri));
    }

    public static Intent getMapIntent(String lat, String lng, String markName) {
        String callUri = "geo:";
        if (!TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lng)) {
            callUri = String.valueOf(String.valueOf(callUri) + lat + "," + lng) + "?q=" + markName;
        }
        return new Intent("android.intent.action.VIEW", Uri.parse(callUri));
    }

    public static String filterTel(String tel) {
        if (StringUtils.EMPTY.equals(tel) || "暂无".equals(tel) || "待定".equals(tel)) {
            return "4008";
        }
        String tel2 = tel.replace("转", ",").replace("-", StringUtils.EMPTY).replace(" ", StringUtils.EMPTY);
        if (tel2.contains("/")) {
            return tel2.split("/")[0];
        }
        return tel2;
    }

    public static Intent getCameraIntent() {
        return new Intent("android.media.action.IMAGE_CAPTURE");
    }

    public static Intent getCameraIntent(File tempfile) {
        Intent shootIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        shootIntent.putExtra("output", Uri.fromFile(tempfile));
        return shootIntent;
    }

    public static Intent getAlbumIntent() {
        Intent intentAlbum = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
        intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        return intentAlbum;
    }

    public static Intent startPhotoZoom(Uri input, Uri output) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(input, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("output", output);
        return intent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    @Deprecated
    public static Intent startPhotoZoom(Uri uri, int width, int height) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", width);
        intent.putExtra("outputY", height);
        intent.putExtra("return-data", true);
        return intent;
    }

    public static Intent startPhotoZoom(Uri input, Uri output, int width, int height) {
        return startPhotoZoom(input, output, 1, 1, width, height, Bitmap.CompressFormat.JPEG.toString());
    }

    public static Intent startPhotoZoom(Uri input, Uri output, int aspectX, int aspectY, int width, int height) {
        return startPhotoZoom(input, output, aspectX, aspectY, width, height, Bitmap.CompressFormat.JPEG.toString());
    }

    public static Intent startPhotoZoom(Uri input, Uri output, int aspectX, int aspectY, int width, int height, String outputFormat) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(input, "image/*");
        intent.putExtra("crop", "true");
        if (aspectX > 0 && aspectY > 0) {
            intent.putExtra("aspectX", aspectX);
            intent.putExtra("aspectY", aspectY);
        }
        if (width > 0 && height > 0) {
            intent.putExtra("outputX", width);
            intent.putExtra("outputY", height);
        }
        intent.putExtra("output", output);
        if (!StringUtils.isEmpty(outputFormat)) {
            intent.putExtra("outputFormat", outputFormat);
        }
        return intent;
    }

    public static String getMIMEType(File f) {
        String type;
        String fName = f.getName();
        String end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg") || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("3gp") || end.equals("mp4")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            type = CorePreferences.IMAGEPATH;
        } else if (end.equals("apk")) {
            type = "application/vnd.android.package-archive";
        } else {
            type = "*";
        }
        if (!end.equals("apk")) {
            return String.valueOf(type) + "/*";
        }
        return type;
    }

    public static Intent getMIMEIntent(File f) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(f), getMIMEType(f));
        return intent;
    }

    public static void openApp(Context context, String packageName) {
        PackageInfo pi = null;
        try {
            pi = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (pi != null) {
            Intent resolveIntent = new Intent("android.intent.action.MAIN", (Uri) null);
            resolveIntent.addCategory("android.intent.category.LAUNCHER");
            resolveIntent.setPackage(pi.packageName);
            ResolveInfo ri = context.getPackageManager().queryIntentActivities(resolveIntent, 0).iterator().next();
            if (ri != null) {
                String packageName1 = ri.activityInfo.packageName;
                String className = ri.activityInfo.name;
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.LAUNCHER");
                intent.setComponent(new ComponentName(packageName1, className));
                context.startActivity(intent);
                return;
            }
            return;
        }
        Toast.makeText(context, R.string.text_noapp, 0).show();
    }

    public static Intent getOpenAppIntent(Context context, String packageName) {
        return context.getPackageManager().getLaunchIntentForPackage(packageName);
    }

    public static Intent getUriIntent(Context context, String uri) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(uri));
        return intent;
    }

    public static Intent getShareIntent(Context context, String shareSubject, String shareContent) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType(ContentTypeField.TYPE_TEXT_PLAIN);
        intent.putExtra("android.intent.extra.SUBJECT", shareSubject);
        intent.putExtra("android.intent.extra.TEXT", shareContent);
        intent.setFlags(268435456);
        return intent;
    }
}
