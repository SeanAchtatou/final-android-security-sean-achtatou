package com.house365.core.util.map.google;

import android.content.Context;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.google.android.maps.GeoPoint;

public class ManagedOverlayGestureDetector extends GestureDetector {
    public static final String LOG_TAG = "ManagedOverlayGestureDetector";
    protected boolean inLongPress;
    protected boolean inLongPressMoved;
    protected boolean inMoving;
    protected GeoPoint lastTapPoint;
    protected ManagedOverlayItem lastTappedOverlayItem;
    protected MotionEvent longPressMotionEvent;
    private ManagedGestureListener managedGestureListener;
    protected MotionEvent movingMotionEvent;
    protected MotionEvent movingMotionEvent1;
    protected float movingV;
    protected float movingV1;
    private GestureDetector.OnGestureListener onGestureListener;
    private ManagedOverlay overlay;
    private OnOverlayGestureListener overlayOnGestureListener;
    protected ZoomEvent zoomEvent;

    public interface OnOverlayGestureListener {
        boolean onDoubleTap(MotionEvent motionEvent, ManagedOverlay managedOverlay, GeoPoint geoPoint, ManagedOverlayItem managedOverlayItem);

        void onLongPress(MotionEvent motionEvent, ManagedOverlay managedOverlay);

        void onLongPressFinished(MotionEvent motionEvent, ManagedOverlay managedOverlay, GeoPoint geoPoint, ManagedOverlayItem managedOverlayItem);

        boolean onScrolled(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2, ManagedOverlay managedOverlay);

        boolean onSingleTap(MotionEvent motionEvent, ManagedOverlay managedOverlay, GeoPoint geoPoint, ManagedOverlayItem managedOverlayItem);

        boolean onZoom(ZoomEvent zoomEvent, ManagedOverlay managedOverlay);
    }

    public ManagedOverlayGestureDetector(ManagedGestureListener managedGestureListener2, ManagedOverlay overlay2, Handler handler) {
        this(null, managedGestureListener2, overlay2, handler);
    }

    public ManagedOverlayGestureDetector(ManagedGestureListener managedGestureListener2, ManagedOverlay overlay2) {
        this(null, managedGestureListener2, overlay2, null);
    }

    public ManagedOverlayGestureDetector(Context context, ManagedGestureListener managedGestureListener2, ManagedOverlay overlay2) {
        this(context, managedGestureListener2, overlay2, null);
    }

    public ManagedOverlayGestureDetector(Context context, ManagedGestureListener managedGestureListener2, ManagedOverlay overlay2, Handler handler) {
        super(context, managedGestureListener2, handler);
        this.lastTapPoint = null;
        this.overlay = overlay2;
        this.managedGestureListener = managedGestureListener2;
        this.managedGestureListener.setDetector(this);
    }

    public boolean invokeZoomEvent(int lastZoomLevel, int zoomLevel) {
        if (this.overlayOnGestureListener != null || this.overlay.isLazyLoadEnabled) {
            ZoomEvent zoomEvent2 = new ZoomEvent();
            zoomEvent2.setEventTime(System.currentTimeMillis());
            zoomEvent2.setZoomLevel(zoomLevel);
            if (lastZoomLevel > zoomLevel) {
                zoomEvent2.setAction(-1);
            } else {
                zoomEvent2.setAction(1);
            }
            this.zoomEvent = zoomEvent2;
            invokeZoomFinished();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void invokeZoomFinished() {
        if (getOverlayOnGestureListener() != null) {
            this.overlayOnGestureListener.onZoom(this.zoomEvent, this.overlay);
            resetState();
            this.overlay.invokeLazyLoad(1000);
        }
    }

    /* access modifiers changed from: protected */
    public void invokeLongPressFinished() {
        if (getOverlayOnGestureListener() != null) {
            getOverlayOnGestureListener().onLongPressFinished(this.longPressMotionEvent, this.overlay, this.lastTapPoint, this.lastTappedOverlayItem);
            if (this.inLongPressMoved) {
                this.overlay.invokeLazyLoad(0);
            }
            resetState();
        }
    }

    public OnOverlayGestureListener getOverlayOnGestureListener() {
        return this.overlayOnGestureListener;
    }

    public void setOverlayOnGestureListener(OnOverlayGestureListener overlayOnGestureListener2) {
        this.overlayOnGestureListener = overlayOnGestureListener2;
    }

    public GestureDetector.OnGestureListener getOnGestureListener() {
        return this.onGestureListener;
    }

    public void setOnGestureListener(GestureDetector.OnGestureListener onGestureListener2) {
        this.onGestureListener = onGestureListener2;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.inMoving && motionEvent.getAction() == 1 && getOverlayOnGestureListener() != null) {
            getOverlayOnGestureListener().onScrolled(this.movingMotionEvent, this.movingMotionEvent1, this.movingV, this.movingV1, this.overlay);
            this.inMoving = false;
            resetState();
            this.overlay.invokeLazyLoad(0);
        }
        if (this.inLongPress && !this.inMoving && motionEvent.getAction() == 1) {
            this.overlay.longPressFinished = true;
        }
        if (this.inLongPress && motionEvent.getAction() == 2) {
            this.inLongPressMoved = true;
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onTap(GeoPoint p) {
        this.lastTapPoint = p;
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int index) {
        this.lastTappedOverlayItem = this.overlay.getItem(index);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean resetState() {
        if (isReset()) {
            return false;
        }
        this.lastTappedOverlayItem = null;
        this.inLongPress = false;
        this.inLongPressMoved = false;
        this.inMoving = false;
        this.zoomEvent = null;
        this.longPressMotionEvent = null;
        this.inLongPressMoved = false;
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isReset() {
        return this.lastTappedOverlayItem == null && this.lastTapPoint == null;
    }

    public static class ManagedGestureListener extends GestureDetector.SimpleOnGestureListener {
        public static final String LOG_TAG = "ManagedGestureListener";
        protected ManagedOverlayGestureDetector detector;
        protected ManagedOverlay overlay;

        public ManagedGestureListener(ManagedOverlay overlay2) {
            this.overlay = overlay2;
        }

        public void setDetector(ManagedOverlayGestureDetector detector2) {
            this.detector = detector2;
        }

        public void onLongPress(MotionEvent e) {
            if (!this.detector.inMoving) {
                this.detector.inLongPress = true;
                this.detector.longPressMotionEvent = e;
            }
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onLongPress(e);
            }
            super.onLongPress(e);
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (this.detector.getOverlayOnGestureListener() != null) {
                this.detector.getOverlayOnGestureListener().onSingleTap(e, this.overlay, this.detector.lastTapPoint, this.detector.lastTappedOverlayItem);
            }
            return super.onSingleTapConfirmed(e);
        }

        public boolean onSingleTapUp(MotionEvent e) {
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onSingleTapUp(e);
            }
            return super.onSingleTapUp(e);
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (this.detector.getOverlayOnGestureListener() != null) {
                this.detector.getOverlayOnGestureListener().onDoubleTap(e, this.overlay, this.detector.lastTapPoint, this.detector.lastTappedOverlayItem);
            }
            return super.onDoubleTap(e);
        }

        public boolean onDoubleTapEvent(MotionEvent e) {
            return super.onDoubleTapEvent(e);
        }

        public boolean onDown(MotionEvent e) {
            this.detector.resetState();
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onDown(e);
            }
            return super.onDown(e);
        }

        public void onShowPress(MotionEvent e) {
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onShowPress(e);
            }
            if (this.detector.getOverlayOnGestureListener() != null) {
                this.detector.getOverlayOnGestureListener().onLongPress(e, this.overlay);
            }
            super.onShowPress(e);
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            this.detector.inMoving = true;
            this.detector.inLongPress = false;
            this.detector.movingMotionEvent = motionEvent;
            this.detector.movingMotionEvent1 = motionEvent1;
            this.detector.movingV = v;
            this.detector.movingV1 = v1;
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onScroll(motionEvent, motionEvent1, v, v1);
            }
            return super.onScroll(motionEvent, motionEvent1, v, v1);
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            if (this.detector.getOnGestureListener() != null) {
                this.detector.getOnGestureListener().onFling(motionEvent, motionEvent1, v, v1);
            }
            this.overlay.invokeLazyLoad(1000);
            return super.onFling(motionEvent, motionEvent1, v, v1);
        }
    }
}
