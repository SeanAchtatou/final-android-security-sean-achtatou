package com.house365.core.util.map.baidu.lazyload;

import com.baidu.mapapi.GeoPoint;
import com.house365.core.util.map.baidu.ManagedOverlay;
import com.house365.core.util.map.baidu.ManagedOverlayItem;
import java.util.List;

public interface LazyLoadCallback {
    List<? extends ManagedOverlayItem> lazyload(GeoPoint geoPoint, GeoPoint geoPoint2, ManagedOverlay managedOverlay) throws LazyLoadException;
}
