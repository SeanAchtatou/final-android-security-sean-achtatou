package com.house365.core.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;

public class HttpURLConnectionUtil {
    public static String CONNECTION_TYPE_GET = "GET";
    public static String CONNECTION_TYPE_POST = "POST";
    public static String ENCODE = "utf-8";
    private static TrustManager ignoreCertificationTrustManger = new X509TrustManager() {
        private X509Certificate[] certificates;

        public void checkClientTrusted(X509Certificate[] certificates2, String authType) throws CertificateException {
            if (this.certificates == null) {
                this.certificates = certificates2;
                System.out.println("init at checkClientTrusted");
            }
        }

        public void checkServerTrusted(X509Certificate[] ax509certificate, String s) throws CertificateException {
            if (this.certificates == null) {
                this.certificates = ax509certificate;
                System.out.println("init at checkServerTrusted");
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    };
    private static HostnameVerifier ignoreHostnameVerifier = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public void setSSL(String keyStore, String trustStorePass) {
        System.setProperty("javax.net.ssl.trustStore", keyStore);
        System.setProperty("javax.net.ssl.trustStorePassword", trustStorePass);
    }

    public String connectURL(String url, Map<String, Object> paramMap, String type) throws KeyManagementException, MalformedURLException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        return connectURL(url, paramMap, null, type);
    }

    public String connectURL(String url, Map<String, Object> paramMap, Map<String, String> head, String type) throws MalformedURLException, IOException, KeyManagementException, NoSuchAlgorithmException, NoSuchProviderException {
        HttpURLConnection connection;
        StringBuffer sb = new StringBuffer();
        if (type.equalsIgnoreCase(CONNECTION_TYPE_GET)) {
            url = String.valueOf(url) + "?" + new String(prepareParam(paramMap).getBytes(ENCODE), ENCODE);
        }
        if ("https".equalsIgnoreCase(url.substring(0, 5))) {
            HttpsURLConnection.setDefaultHostnameVerifier(ignoreHostnameVerifier);
            connection = (HttpsURLConnection) new URL(url).openConnection();
            TrustManager[] tm = {ignoreCertificationTrustManger};
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new SecureRandom());
            ((HttpsURLConnection) connection).setSSLSocketFactory(sslContext.getSocketFactory());
        } else {
            connection = (HttpURLConnection) new URL(url).openConnection();
        }
        if (type.equalsIgnoreCase(CONNECTION_TYPE_POST)) {
            connection.setRequestMethod(CONNECTION_TYPE_POST);
        }
        if (head == null) {
            head = new HashMap<>();
        }
        setDefHead(head);
        for (Map.Entry<String, String> entry : head.entrySet()) {
            connection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
        HttpURLConnection.setFollowRedirects(true);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();
        if (type.equalsIgnoreCase(CONNECTION_TYPE_POST)) {
            OutputStream out = connection.getOutputStream();
            out.write(prepareParam(paramMap).getBytes(ENCODE));
            out.flush();
            out.close();
        }
        BufferedReader in = getInputReader(connection);
        while (true) {
            String line = in.readLine();
            if (line == null) {
                in.close();
                return sb.toString();
            }
            sb.append(line);
        }
    }

    private BufferedReader getInputReader(HttpURLConnection connection) throws UnsupportedEncodingException, IOException {
        String encoding = connection.getContentEncoding();
        if (encoding == null || encoding.indexOf("gzip") == -1) {
            return new BufferedReader(new InputStreamReader(connection.getInputStream(), ENCODE));
        }
        return new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream()), ENCODE));
    }

    private void setDefHead(Map<String, String> head) {
        if (!head.containsKey("Content-Type")) {
            head.put("Content-Type", "application/x-www-form-urlencoded");
        }
    }

    private String prepareParam(Map<String, Object> paramMap) throws UnsupportedEncodingException {
        StringBuffer sb = new StringBuffer();
        if (paramMap == null || paramMap.isEmpty()) {
            return StringUtils.EMPTY;
        }
        for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
            String key = URLEncoder.encode((String) entry.getKey(), ENCODE);
            String value = URLEncoder.encode(entry.getValue().toString(), ENCODE);
            if (sb.length() >= 1) {
                sb.append("&");
            }
            sb.append(key).append("=").append(value);
        }
        return sb.toString();
    }
}
