package com.house365.core.http.exception;

public class HttpParseException extends Exception {
    public HttpParseException() {
    }

    public HttpParseException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HttpParseException(String detailMessage) {
        super(detailMessage);
    }

    public HttpParseException(Throwable throwable) {
        super(throwable);
    }
}
