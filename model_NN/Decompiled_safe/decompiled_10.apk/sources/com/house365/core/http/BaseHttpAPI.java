package com.house365.core.http;

import com.house365.core.constant.CorePreferences;
import com.house365.core.http.ex.SSLSocketFactoryEx;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLHandshakeException;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public abstract class BaseHttpAPI {
    public static final int CHANNLE_TYPE_COMMONS = 2;
    public static final int CHANNLE_TYPE_STANDER = 1;
    protected int BUFFER_SIZE;
    protected String CHARENCODE;
    protected int HTTPS_PORT;
    protected int HTTP_PORT;
    protected int TIMEOUT;
    protected boolean USEGZIP;
    private final DefaultHttpClient client;
    private HttpRequestRetryHandler requestRetryHandler;
    private ResponseHandler<String> responseHandler;

    public abstract String setCharEncode();

    public abstract int setConnectTimeOut();

    public abstract int setHttpPort();

    public abstract int setHttpsPort();

    public abstract boolean setUSEGIZP();

    public BaseHttpAPI() {
        this.TIMEOUT = 30000;
        this.CHARENCODE = CharEncoding.UTF_8;
        this.USEGZIP = true;
        this.HTTP_PORT = 80;
        this.HTTPS_PORT = 443;
        this.BUFFER_SIZE = 8192;
        this.requestRetryHandler = new HttpRequestRetryHandler() {
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                if (executionCount >= 3) {
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    return true;
                }
                if ((exception instanceof SSLHandshakeException) || (((HttpRequest) context.getAttribute("http.request")) instanceof HttpEntityEnclosingRequest)) {
                    return false;
                }
                return true;
            }
        };
        this.responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                String charset;
                String contentEncoding;
                HttpEntity entity = response.getEntity();
                if (entity == null) {
                    return null;
                }
                Header header = entity.getContentEncoding();
                if (!(header == null || (contentEncoding = header.getValue()) == null || !contentEncoding.contains("gzip"))) {
                    entity = new GzipDecompressingEntity(entity);
                }
                if (EntityUtils.getContentCharSet(entity) == null) {
                    charset = BaseHttpAPI.this.CHARENCODE;
                } else {
                    charset = EntityUtils.getContentCharSet(entity);
                }
                return new String(EntityUtils.toByteArray(entity), charset);
            }
        };
        this.TIMEOUT = setConnectTimeOut();
        this.CHARENCODE = setCharEncode();
        this.USEGZIP = setUSEGIZP();
        this.HTTP_PORT = setHttpPort();
        this.HTTPS_PORT = setHttpsPort();
        this.client = createDefHttpClient();
    }

    public DefaultHttpClient createDefHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry supportedSchemes = new SchemeRegistry();
            supportedSchemes.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), this.HTTP_PORT));
            supportedSchemes.register(new Scheme("https", sf, this.HTTPS_PORT));
            HttpParams httpParams = createHttpParams();
            return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, supportedSchemes), httpParams);
        } catch (Exception e) {
            e.printStackTrace();
            return createDefClient();
        }
    }

    public DefaultHttpClient createDefClient() {
        SchemeRegistry supportedSchemes = new SchemeRegistry();
        supportedSchemes.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), this.HTTP_PORT));
        supportedSchemes.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), this.HTTPS_PORT));
        HttpParams httpParams = createHttpParams();
        return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, supportedSchemes), httpParams);
    }

    public DefaultHttpClient createDefClient(HttpParams httpParams) {
        SchemeRegistry supportedSchemes = new SchemeRegistry();
        supportedSchemes.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), this.HTTP_PORT));
        supportedSchemes.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), this.HTTPS_PORT));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, supportedSchemes), httpParams);
    }

    private final HttpParams createHttpParams() {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(params, false);
        HttpConnectionParams.setConnectionTimeout(params, this.TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, this.TIMEOUT);
        HttpConnectionParams.setSocketBufferSize(params, this.BUFFER_SIZE);
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, CharEncoding.UTF_8);
        return params;
    }

    public DefaultHttpClient createHttpClientWithHttpParams(HttpParams httpParams) {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            SchemeRegistry supportedSchemes = new SchemeRegistry();
            supportedSchemes.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), this.HTTP_PORT));
            supportedSchemes.register(new Scheme("https", sf, this.HTTPS_PORT));
            return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, supportedSchemes), httpParams);
        } catch (Exception e) {
            e.printStackTrace();
            return createDefClient();
        }
    }

    private final HttpParams createHttpParams(int timeout) {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(params, false);
        HttpConnectionParams.setConnectionTimeout(params, timeout);
        HttpConnectionParams.setSoTimeout(params, this.TIMEOUT);
        HttpConnectionParams.setSocketBufferSize(params, this.BUFFER_SIZE);
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, CharEncoding.UTF_8);
        return params;
    }

    /* access modifiers changed from: protected */
    public void setDeaultRequestHeader(AbstractHttpMessage request) {
        if (this.USEGZIP) {
            request.addHeader("Accept-Encoding", "gzip");
        }
    }

    private Map<String, Object> convertToParamMap(List<NameValuePair> list) {
        Map<String, Object> paramMap = new HashMap<>();
        for (NameValuePair namevalue : list) {
            paramMap.put(namevalue.getName(), namevalue.getValue());
        }
        return paramMap;
    }

    private String genrateGetUrl(String url, List<NameValuePair> olist) {
        if (olist == null || olist.size() <= 0) {
            return url;
        }
        return String.valueOf(url) + "?" + URLEncodedUtils.format(olist, this.CHARENCODE);
    }

    public String get(String url, List<NameValuePair> olist) throws ParseException, IOException {
        String turl = genrateGetUrl(url, olist);
        CorePreferences.DEBUG("get:" + turl);
        HttpGet request = new HttpGet(turl);
        setDeaultRequestHeader(request);
        long starttime = System.currentTimeMillis();
        String s = (String) this.client.execute(request, this.responseHandler);
        CorePreferences.DEBUG("result:" + s);
        CorePreferences.DEBUG("request-response took time :" + (System.currentTimeMillis() - starttime) + "ms");
        return s;
    }

    public String get(String url, List<NameValuePair> olist, int timeout) throws ParseException, IOException {
        String turl = genrateGetUrl(url, olist);
        CorePreferences.DEBUG("get:" + turl);
        HttpGet request = new HttpGet(turl);
        setDeaultRequestHeader(request);
        String s = (String) createHttpClientWithHttpParams(createHttpParams(timeout)).execute(request, this.responseHandler);
        CorePreferences.DEBUG("result:" + s);
        return s;
    }

    public HttpEntity getWithHttpEntity(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        String turl = genrateGetUrl(url, olist);
        CorePreferences.DEBUG("get:" + turl);
        HttpGet request = new HttpGet(turl);
        setDeaultRequestHeader(request);
        return this.client.execute(request).getEntity();
    }

    public HttpEntity getWithHttpEntity(String url, List<NameValuePair> olist, int timeout) throws ClientProtocolException, IOException {
        String turl = genrateGetUrl(url, olist);
        CorePreferences.DEBUG("get:" + turl);
        HttpGet request = new HttpGet(turl);
        setDeaultRequestHeader(request);
        return createHttpClientWithHttpParams(createHttpParams(timeout)).execute(request).getEntity();
    }

    public InputStream getWithStream(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        return getUngzippedStream(getWithHttpEntity(url, olist));
    }

    public InputStream getWithStream(String url, List<NameValuePair> olist, int timeout) throws ClientProtocolException, IOException {
        return getUngzippedStream(getWithHttpEntity(url, olist, timeout));
    }

    public String post(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        CorePreferences.DEBUG("post:" + url);
        HttpPost request = new HttpPost(url);
        setDeaultRequestHeader(request);
        request.setEntity(entity);
        String s = (String) this.client.execute(request, this.responseHandler);
        CorePreferences.DEBUG(s);
        return s;
    }

    public String post(String url, HttpEntity entity, int timeout) throws ClientProtocolException, IOException {
        CorePreferences.DEBUG("post:" + url);
        HttpPost request = new HttpPost(url);
        setDeaultRequestHeader(request);
        request.setEntity(entity);
        String s = (String) createHttpClientWithHttpParams(createHttpParams(timeout)).execute(request, this.responseHandler);
        CorePreferences.DEBUG(s);
        return s;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.client.entity.UrlEncodedFormEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public String post(String url, List<NameValuePair> olist) throws ClientProtocolException, IOException {
        return post(url, (HttpEntity) new UrlEncodedFormEntity(olist, this.CHARENCODE));
    }

    public InputStream postWithStream(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        return getUngzippedStream(postWithHttpEntity(url, entity));
    }

    public HttpEntity postWithHttpEntity(String url, HttpEntity entity) throws ClientProtocolException, IOException {
        CorePreferences.DEBUG(url);
        HttpPost request = new HttpPost(url);
        setDeaultRequestHeader(request);
        request.setEntity(entity);
        return this.client.execute(request).getEntity();
    }

    public BasicNameValuePair generateGetParameter(String name, String value) {
        return new BasicNameValuePair(name, value);
    }

    public InputStream getUngzippedStream(HttpEntity entity) throws IOException {
        InputStream responseStream = entity.getContent();
        if (responseStream == null) {
            return responseStream;
        }
        Header header = entity.getContentEncoding();
        if (header == null) {
            return responseStream;
        }
        String contentEncoding = header.getValue();
        if (contentEncoding == null) {
            return responseStream;
        }
        if (contentEncoding.contains("gzip")) {
            responseStream = new GZIPInputStream(responseStream);
        }
        return responseStream;
    }
}
