package com.house365.core.http;

import android.content.Context;
import com.house365.core.bean.VersionInfo;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONObject;
import com.house365.core.reflect.ReflectUtil;
import com.house365.core.util.DeviceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.message.BasicNameValuePair;

public abstract class HttpAPIAdapter extends BaseHttpAPI {
    private Context context;

    public abstract VersionInfo getAppNewVersion();

    public HttpAPIAdapter(Context context2) {
        this.context = context2;
    }

    public int setConnectTimeOut() {
        return 30000;
    }

    public String setCharEncode() {
        return CharEncoding.UTF_8;
    }

    public boolean setUSEGIZP() {
        return true;
    }

    public int setHttpPort() {
        return 80;
    }

    public int setHttpsPort() {
        return 443;
    }

    public <T> T postWithObject(String url, List<NameValuePair> list, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = post(url, list);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("postWithObject occurs exception", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.entity.mime.MultipartEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public <T> T postWithObject(String url, MultipartEntity entity, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = post(url, (HttpEntity) entity);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("postWithObject occurs exception", e2);
        }
    }

    public <T> T postWithObject(String url, Map<String, String> params, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            List<NameValuePair> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                list.add(new BasicNameValuePair(((String) entry.getKey()).toString(), ((String) entry.getValue()).toString()));
            }
            String result = post(url, list);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("postWithObject occurs exception", e2);
        }
    }

    public <T> List<T> postWithList(String url, List<NameValuePair> list, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = post(url, list);
            try {
                List<T> ts = new ArrayList<>();
                JSONArray aJsonArray = new JSONArray(result);
                if (aJsonArray != null) {
                    for (int i = 0; i < aJsonArray.length(); i++) {
                        ts.add(ReflectUtil.copy(t.getClass(), aJsonArray.getJSONObject(i)));
                    }
                }
                return ts;
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithList occurs exception", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.entity.mime.MultipartEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public <T> List<T> postWithList(String url, MultipartEntity entity, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = post(url, (HttpEntity) entity);
            try {
                List<T> ts = new ArrayList<>();
                JSONArray aJsonArray = new JSONArray(result);
                if (aJsonArray != null) {
                    for (int i = 0; i < aJsonArray.length(); i++) {
                        ts.add(ReflectUtil.copy(t.getClass(), aJsonArray.getJSONObject(i)));
                    }
                }
                return ts;
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("postWithObject occurs exception", e2);
        }
    }

    public String getWithString(String url, Map<String, String> params) throws HtppApiException, NetworkUnavailableException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            List<NameValuePair> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                list.add(new BasicNameValuePair(((String) entry.getKey()).toString(), ((String) entry.getValue()).toString()));
            }
            return get(url, list);
        } catch (Exception e) {
            throw new HtppApiException("getWithObject occurs exception", e);
        }
    }

    public String getWithString(String url, Map<String, String> params, int timeout) throws HtppApiException, NetworkUnavailableException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            List<NameValuePair> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                list.add(new BasicNameValuePair(((String) entry.getKey()).toString(), ((String) entry.getValue()).toString()));
            }
            return get(url, list, timeout);
        } catch (Exception e) {
            throw new HtppApiException("getWithObject occurs exception", e);
        }
    }

    public String getWithString(String url, List<NameValuePair> list) throws HtppApiException, NetworkUnavailableException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            return get(url, list);
        } catch (Exception e) {
            throw new HtppApiException("getWithObject occurs exception", e);
        }
    }

    public String getWithString(String url, List<NameValuePair> list, int timeout) throws HtppApiException, NetworkUnavailableException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            return get(url, list, timeout);
        } catch (Exception e) {
            throw new HtppApiException("getWithObject occurs exception", e);
        }
    }

    public <T> T getWithObject(String url, List<NameValuePair> list, T t, int timeout) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = get(url, list, timeout);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithObject occurs exception", e2);
        }
    }

    public <T> T getWithObject(String url, List<NameValuePair> list, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = get(url, list);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithObject occurs exception", e2);
        }
    }

    public <T> T getWithObject(String url, Map<String, String> params, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            List<NameValuePair> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                list.add(new BasicNameValuePair(((String) entry.getKey()).toString(), ((String) entry.getValue()).toString()));
            }
            String result = get(url, list);
            try {
                return ReflectUtil.copy(t.getClass(), new JSONObject(result));
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithObject occurs exception", e2);
        }
    }

    public <T> List<T> getWithList(String url, List<NameValuePair> list, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            String result = get(url, list);
            try {
                List<T> ts = new ArrayList<>();
                JSONArray aJsonArray = new JSONArray(result);
                if (aJsonArray != null) {
                    for (int i = 0; i < aJsonArray.length(); i++) {
                        ts.add(ReflectUtil.copy(t.getClass(), aJsonArray.getJSONObject(i)));
                    }
                }
                return ts;
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithList occurs exception", e2);
        }
    }

    public <T> List<T> getListWithString(String str, T t) throws HttpParseException {
        try {
            JSONArray aJsonArray = new JSONArray(str);
            try {
                List<T> ts = new ArrayList<>();
                if (aJsonArray != null) {
                    for (int i = 0; i < aJsonArray.length(); i++) {
                        ts.add(ReflectUtil.copy(t.getClass(), aJsonArray.getJSONObject(i)));
                    }
                }
                return ts;
            } catch (Exception e) {
                e = e;
                throw new HttpParseException("data parese  occurs exception:" + str, e);
            }
        } catch (Exception e2) {
            e = e2;
            throw new HttpParseException("data parese  occurs exception:" + str, e);
        }
    }

    public <T> List<T> getWithList(String url, Map<String, String> params, T t) throws HtppApiException, NetworkUnavailableException, HttpParseException {
        if (!DeviceUtil.isNetConnect(this.context)) {
            throw new NetworkUnavailableException("Network Unavailable.");
        }
        try {
            List<NameValuePair> list = new ArrayList<>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                list.add(new BasicNameValuePair(((String) entry.getKey()).toString(), ((String) entry.getValue()).toString()));
            }
            String result = get(url, list);
            try {
                List<T> ts = new ArrayList<>();
                JSONArray aJsonArray = new JSONArray(result);
                if (aJsonArray != null) {
                    for (int i = 0; i < aJsonArray.length(); i++) {
                        ts.add(ReflectUtil.copy(t.getClass(), aJsonArray.getJSONObject(i)));
                    }
                }
                return ts;
            } catch (Exception e) {
                throw new HttpParseException("data parese  occurs exception:" + result, e);
            }
        } catch (Exception e2) {
            throw new HtppApiException("getWithList occurs exception", e2);
        }
    }
}
