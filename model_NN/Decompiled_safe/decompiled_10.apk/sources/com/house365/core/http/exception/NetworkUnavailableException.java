package com.house365.core.http.exception;

public class NetworkUnavailableException extends Exception {
    public NetworkUnavailableException() {
    }

    public NetworkUnavailableException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NetworkUnavailableException(String detailMessage) {
        super(detailMessage);
    }

    public NetworkUnavailableException(Throwable throwable) {
        super(throwable);
    }
}
