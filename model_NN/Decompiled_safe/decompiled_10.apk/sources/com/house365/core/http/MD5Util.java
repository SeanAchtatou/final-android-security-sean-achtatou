package com.house365.core.http;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Util {
    public static final String MD5_ARITHMETIC_NAME = "MD5";
    public static final String appMD5SecretKey = "-jj_quan";
    private static MD5Util mD5Util;
    private static final String[] strDigits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    private MD5Util() {
    }

    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        if (iRet < 0) {
            iRet += AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT;
        }
        return String.valueOf(strDigits[iRet / 16]) + strDigits[iRet % 16];
    }

    private static String byteToNum(byte bByte) {
        int iRet = bByte;
        System.out.println("iRet1=" + iRet);
        if (iRet < 0) {
            iRet += AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT;
        }
        return String.valueOf(iRet);
    }

    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (byte byteToArrayString : bByte) {
            sBuffer.append(byteToArrayString(byteToArrayString));
        }
        return sBuffer.toString();
    }

    public static String GetMD5Code(String strObj, String secretKey) {
        String resultString = null;
        if (TextUtils.isEmpty(secretKey)) {
            secretKey = appMD5SecretKey;
        }
        try {
            String resultString2 = new String(String.valueOf(strObj) + secretKey);
            try {
                return byteToString(MessageDigest.getInstance(MD5_ARITHMETIC_NAME).digest(resultString2.getBytes()));
            } catch (NoSuchAlgorithmException e) {
                ex = e;
                resultString = resultString2;
            }
        } catch (NoSuchAlgorithmException e2) {
            ex = e2;
            ex.printStackTrace();
            return resultString;
        }
    }

    public static MD5Util getInstance() {
        if (mD5Util != null) {
            return mD5Util;
        }
        MD5Util mD5Util2 = new MD5Util();
        mD5Util = mD5Util2;
        return mD5Util2;
    }
}
