package com.house365.core.http.exception;

public class HtppApiException extends Exception {
    public HtppApiException() {
    }

    public HtppApiException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HtppApiException(String detailMessage) {
        super(detailMessage);
    }

    public HtppApiException(Throwable throwable) {
        super(throwable);
    }
}
