package com.house365.core.anim;

public class SlipDirection {
    public static final int BOTTOM = 2;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int TOP = 1;
}
