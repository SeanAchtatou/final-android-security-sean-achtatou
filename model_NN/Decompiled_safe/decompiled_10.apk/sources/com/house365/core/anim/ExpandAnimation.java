package com.house365.core.anim;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

public class ExpandAnimation extends Animation {
    private View mAnimatedView;
    private boolean mIsVisibleAfter = false;
    private int mMarginEnd;
    private int mMarginStart;
    private LinearLayout.LayoutParams mViewLayoutParams;
    private boolean mWasEndedAlready = false;

    public ExpandAnimation(View view, int duration) {
        int i;
        setDuration((long) duration);
        this.mAnimatedView = view;
        this.mViewLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        this.mIsVisibleAfter = view.getVisibility() == 0;
        this.mMarginStart = this.mViewLayoutParams.bottomMargin;
        if (this.mMarginStart == 0) {
            i = 0 - view.getHeight();
        } else {
            i = 0;
        }
        this.mMarginEnd = i;
        view.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {
            this.mViewLayoutParams.bottomMargin = this.mMarginStart + ((int) (((float) (this.mMarginEnd - this.mMarginStart)) * interpolatedTime));
            this.mAnimatedView.requestLayout();
        } else if (!this.mWasEndedAlready) {
            this.mViewLayoutParams.bottomMargin = this.mMarginEnd;
            this.mAnimatedView.requestLayout();
            if (this.mIsVisibleAfter) {
                this.mAnimatedView.setVisibility(8);
            }
            this.mWasEndedAlready = true;
        }
    }
}
