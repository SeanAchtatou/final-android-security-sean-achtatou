package com.house365.core.anim;

public class AnimBean {
    private int in = 0;
    private int out = 0;

    public int getIn() {
        return this.in;
    }

    public void setIn(int in2) {
        this.in = in2;
    }

    public int getOut() {
        return this.out;
    }

    public void setOut(int out2) {
        this.out = out2;
    }

    public AnimBean(int in2, int out2) {
        this.in = in2;
        this.out = out2;
    }
}
