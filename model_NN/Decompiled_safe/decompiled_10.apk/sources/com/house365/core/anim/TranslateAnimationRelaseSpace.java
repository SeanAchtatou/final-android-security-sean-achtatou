package com.house365.core.anim;

import android.view.View;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import org.apache.commons.lang.SystemUtils;

public class TranslateAnimationRelaseSpace extends TranslateAnimation {
    private LinearLayout.LayoutParams mLayoutParams;
    private int mMarginBottomFromY;
    private int mMarginBottomToY;
    private boolean mVanishAfter = false;
    private View mView;

    public TranslateAnimationRelaseSpace(float fromX, float toX, float fromY, float toY, int duration, View view, boolean vanishAfter) {
        super(fromX, toX, fromY, toY);
        setDuration((long) duration);
        this.mView = view;
        this.mVanishAfter = vanishAfter;
        this.mLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        int height = this.mView.getHeight();
        this.mMarginBottomFromY = (((int) (((float) height) * fromY)) + this.mLayoutParams.bottomMargin) - height;
        this.mMarginBottomToY = ((int) (SystemUtils.JAVA_VERSION_FLOAT - ((((float) height) * toY) + ((float) this.mLayoutParams.bottomMargin)))) - height;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {
            this.mLayoutParams.setMargins(this.mLayoutParams.leftMargin, this.mLayoutParams.topMargin, this.mLayoutParams.rightMargin, this.mMarginBottomFromY + ((int) (((float) (this.mMarginBottomToY - this.mMarginBottomFromY)) * interpolatedTime)));
            this.mView.getParent().requestLayout();
        } else if (this.mVanishAfter) {
            this.mView.setVisibility(8);
        }
    }
}
