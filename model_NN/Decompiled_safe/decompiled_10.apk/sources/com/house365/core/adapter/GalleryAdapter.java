package com.house365.core.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import android.widget.ImageView;
import com.house365.core.R;

public class GalleryAdapter extends BaseCacheListAdapter<String> {
    private Gallery gallery;
    private int height = 0;

    public GalleryAdapter(Context context) {
        super(context);
    }

    public Gallery getGallery() {
        return this.gallery;
    }

    public void setGallery(Gallery gallery2) {
        this.gallery = gallery2;
    }

    public int getHeight() {
        if (this.height == 0 && this.gallery != null) {
            this.height = this.gallery.getHeight();
        }
        return this.height;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        String url = (String) getItem(arg0);
        ImageView i = new ImageView(this.context);
        if (url.endsWith(".thumb.jpg")) {
            setCacheImage(i, url, R.drawable.img_default_small, 2);
        } else {
            setCacheImage(i, url, R.drawable.img_default_big, 1);
        }
        i.setAdjustViewBounds(true);
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        if (getHeight() > 0) {
            i.setLayoutParams(new Gallery.LayoutParams(getHeight(), getHeight()));
        } else {
            i.setLayoutParams(new Gallery.LayoutParams(-2, -2));
        }
        return i;
    }
}
