package com.house365.core.adapter;

import android.content.Context;
import android.widget.ImageView;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseCacheListAdapter<T> extends BaseListAdapter<T> {
    protected BaseApplication application;
    protected Context context;
    private boolean hasFirstScrolled = false;
    protected AsyncImageLoader mAil;
    public Map<Integer, Object> viewReferences = new HashMap();

    public BaseCacheListAdapter(Context context2) {
        super(context2);
        this.context = context2;
        this.application = (BaseApplication) context2.getApplicationContext();
        this.mAil = new AsyncImageLoader(context2);
    }

    public void clear() {
        super.clear();
    }

    public void setCacheImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, this.mAil);
    }

    public void setCacheImageWithImageOper(ImageView imageView, String imageUrl, int resId, int scaleType, CacheImageUtil.ImageOperate imageOperate) {
        CacheImageUtil.setCacheImageWithImageOper(imageView, imageUrl, imageOperate, this.context.getResources(), resId, scaleType, this.mAil);
    }

    public void setCacheImageWithoutDef(ImageView imageView, String imageUrl, int scaleType) {
        CacheImageUtil.setCacheImageWithoutDef(imageView, imageUrl, scaleType, this.mAil);
    }

    public void setCacheImageFromLocalFile(ImageView imageView, String localFile, int resId, int scaleType) {
        CacheImageUtil.setCacheImageFromLocalFile(imageView, localFile, resId, scaleType, this.mAil);
    }

    public boolean hasFirstScrolled() {
        return this.hasFirstScrolled;
    }

    public void setHasFirstScrolled(boolean f) {
        this.hasFirstScrolled = f;
    }

    public void onScrollIdle(int startIdx, int endIdx) {
    }
}
