package com.house365.core.adapter;

import com.house365.core.bean.Group;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseExpandableListAdapter<T, R> extends android.widget.BaseExpandableListAdapter {
    protected final List<Group<T, R>> list = new ArrayList();

    public int getGroupCount() {
        return this.list.size();
    }

    public int getChildrenCount(int groupPosition) {
        return this.list.get(groupPosition).getSubsCount();
    }

    public T getGroup(int groupPosition) {
        return this.list.get(groupPosition).getGroup();
    }

    public R getChild(int groupPosition, int childPosition) {
        return this.list.get(groupPosition).getSub(childPosition);
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) childPosition;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void clear() {
        this.list.clear();
    }

    public void addAllGroup(List<? extends Group<T, R>> tlist) {
        this.list.addAll(tlist);
    }

    public void addGroup(List<? extends T> tlist) {
        for (T group : tlist) {
            this.list.add(new Group(group));
        }
    }

    public void addGroup(T t) {
        this.list.add(new Group(t));
    }

    public void addSubs(int groupPosition, List<? extends R> tlist) {
        this.list.get(groupPosition).addSubs(tlist);
    }

    public void addSub(int groupPosition, R r) {
        this.list.get(groupPosition).addSub(r);
    }

    public Group remove(int groupPosition) {
        return this.list.remove(groupPosition);
    }
}
