package com.house365.core.adapter;

import android.content.Context;
import android.widget.ImageView;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;

public abstract class BaseCacheExpandableListAdapter<R, T> extends BaseExpandableListAdapter<R, T> {
    protected Context context;
    private AsyncImageLoader mAil;
    private BaseApplication mApplication;

    public BaseCacheExpandableListAdapter(Context context2) {
        this.context = context2;
        this.mApplication = (BaseApplication) context2.getApplicationContext();
        this.mAil = new AsyncImageLoader(context2);
    }

    public void clear() {
        super.clear();
    }

    public void setCacheImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, this.mAil);
    }

    public void setCacheImageWithImageOper(ImageView imageView, String imageUrl, int resId, int scaleType, CacheImageUtil.ImageOperate imageOperate) {
        CacheImageUtil.setCacheImageWithImageOper(imageView, imageUrl, imageOperate, this.context.getResources(), resId, scaleType, this.mAil);
    }

    public void setCacheImageWithoutDef(ImageView imageView, String imageUrl, int scaleType) {
        CacheImageUtil.setCacheImageWithoutDef(imageView, imageUrl, scaleType, this.mAil);
    }
}
