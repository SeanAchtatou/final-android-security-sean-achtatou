package com.house365.core.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import com.house365.core.R;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import com.house365.core.touch.ImageViewTouch;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class BaseAlbumAdapter {
    protected BaseApplication application;
    /* access modifiers changed from: private */
    public Context context;
    private GalleryAdapter galleryAdapter;
    /* access modifiers changed from: private */
    public int img_def_big = R.drawable.img_default_big;
    protected final List<String> list;
    protected AsyncImageLoader mAil;
    private AlubmPagerAdapter pageAdapter;
    /* access modifiers changed from: private */
    public int resBg;

    public AlubmPagerAdapter getPageAdapter() {
        return this.pageAdapter;
    }

    public void setPageAdapter(AlubmPagerAdapter pageAdapter2) {
        this.pageAdapter = pageAdapter2;
    }

    public int getResBg() {
        return this.resBg;
    }

    public void setResBg(int resBg2) {
        this.resBg = resBg2;
    }

    public GalleryAdapter getGalleryAdapter() {
        return this.galleryAdapter;
    }

    public void setGalleryAdapter(GalleryAdapter galleryAdapter2) {
        this.galleryAdapter = galleryAdapter2;
    }

    public void setGallery(Gallery gallery) {
        this.galleryAdapter.setGallery(gallery);
    }

    public BaseAlbumAdapter(Context context2) {
        this.context = context2;
        this.list = new ArrayList();
        this.application = (BaseApplication) context2.getApplicationContext();
        this.mAil = new AsyncImageLoader(context2);
        this.pageAdapter = new AlubmPagerAdapter();
        this.galleryAdapter = new GalleryAdapter(context2);
    }

    public boolean addAll(List<String> list2) {
        this.galleryAdapter.addAll(list2);
        return this.list.addAll(list2);
    }

    public void clear() {
        this.galleryAdapter.clear();
        this.list.clear();
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public String getItem(int i) {
        return this.list.get(i);
    }

    public void notifyDataSetChanged() {
        this.galleryAdapter.notifyDataSetChanged();
        this.pageAdapter.notifyDataSetChanged();
    }

    public void setTouchImage(ImageViewTouch imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setTouchImage(imageView, imageUrl, this.context.getResources(), resId, scaleType, this.mAil);
    }

    public ImageViewTouch getImageView(int position) {
        return (ImageViewTouch) this.pageAdapter.views.get(Integer.valueOf(position)).get();
    }

    public void setImg_def_big(int img_def_big2) {
        this.img_def_big = img_def_big2;
    }

    class AlubmPagerAdapter extends PagerAdapter {
        public HashMap<Integer, SoftReference<ImageViewTouch>> views = new HashMap<>();

        AlubmPagerAdapter() {
        }

        public Object instantiateItem(View container, int position) {
            ImageViewTouch imageView = new ImageViewTouch(BaseAlbumAdapter.this.context);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            if (BaseAlbumAdapter.this.resBg != 0) {
                BaseAlbumAdapter.this.setTouchImage(imageView, StringUtils.EMPTY, BaseAlbumAdapter.this.resBg, 2);
            } else {
                imageView.setBackgroundColor(-16777216);
            }
            BaseAlbumAdapter.this.setTouchImage(imageView, BaseAlbumAdapter.this.getItem(position), BaseAlbumAdapter.this.img_def_big, 1);
            ((ViewPager) container).addView(imageView);
            this.views.put(Integer.valueOf(position), new SoftReference(imageView));
            return imageView;
        }

        public void destroyItem(View container, int position, Object object) {
            this.views.remove(Integer.valueOf(position));
            ImageViewTouch imageView = (ImageViewTouch) object;
            imageView.mBitmapDisplayed.recycle();
            imageView.clear();
            ((ViewPager) container).removeView(imageView);
        }

        public void startUpdate(View container) {
        }

        public void finishUpdate(View container) {
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageViewTouch) object);
        }

        public Parcelable saveState() {
            return null;
        }

        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        public int getCount() {
            return BaseAlbumAdapter.this.getCount();
        }
    }
}
