package com.house365.core.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseCacheListPagerAdapter<T> extends PagerAdapter {
    protected BaseApplication application;
    protected Context context;
    protected final List<T> list = new ArrayList();
    protected AsyncImageLoader mAil;

    public abstract View getAdapterView(PagerAdapter pagerAdapter, View view, int i);

    public BaseCacheListPagerAdapter(Context context2) {
        this.context = context2;
        this.application = (BaseApplication) context2.getApplicationContext();
        this.mAil = new AsyncImageLoader(context2);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View view = getAdapterView(this, container, position);
        container.addView(view);
        return view;
    }

    public int getItemPosition(Object object) {
        return -2;
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    public void setCacheImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, this.mAil);
    }

    public void setCacheImageWithImageOper(ImageView imageView, String imageUrl, int resId, int scaleType, CacheImageUtil.ImageOperate imageOperate) {
        CacheImageUtil.setCacheImageWithImageOper(imageView, imageUrl, imageOperate, this.context.getResources(), resId, scaleType, this.mAil);
    }

    public void setCacheImageWithoutDef(ImageView imageView, String imageUrl, int scaleType) {
        CacheImageUtil.setCacheImageWithoutDef(imageView, imageUrl, scaleType, this.mAil);
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public boolean addAll(List<? extends T> list2) {
        return this.list.addAll(list2);
    }

    public void clear() {
        this.list.clear();
    }

    public T getItem(int i) {
        return this.list.get(i);
    }

    public long getItemId(int id) {
        return (long) id;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        if (this.list == null) {
            return true;
        }
        return this.list.isEmpty();
    }

    public T remove(int i) {
        return this.list.remove(i);
    }

    public void addItem(T t) {
        this.list.add(t);
    }

    public List<T> getList() {
        return this.list;
    }
}
