package com.house365.core.adapter;

import android.content.Context;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseListAdapter<T> extends BaseAdapter {
    protected Context context;
    protected final List<T> list = new ArrayList();

    public BaseListAdapter(Context context2) {
        this.context = context2;
    }

    public boolean addAll(List<? extends T> list2) {
        return this.list.addAll(list2);
    }

    public void clear() {
        this.list.clear();
    }

    public int getCount() {
        if (this.list == null) {
            return 0;
        }
        return this.list.size();
    }

    public T getItem(int i) {
        return this.list.get(i);
    }

    public long getItemId(int id) {
        return (long) id;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        if (this.list == null) {
            return true;
        }
        return this.list.isEmpty();
    }

    public T remove(int i) {
        return this.list.remove(i);
    }

    public void addItem(T t) {
        this.list.add(t);
    }

    public List<T> getList() {
        return this.list;
    }
}
