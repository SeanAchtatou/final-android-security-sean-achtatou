package com.house365.core.adapter;

import android.content.Context;
import java.util.HashMap;

public abstract class BaseCanDelListAdapter<T> extends BaseCacheListAdapter<T> {
    protected HashMap<Integer, String> checkBoxState = new HashMap<>();
    protected boolean isCheckAll = false;
    protected boolean isShowDel = false;

    public BaseCanDelListAdapter(Context context) {
        super(context);
    }

    public boolean isShowDel() {
        return this.isShowDel;
    }

    public void setShowDel(boolean isShowDel2) {
        this.isShowDel = isShowDel2;
    }

    public HashMap<Integer, String> getCheckBoxState() {
        return this.checkBoxState;
    }

    public void setCheckBoxState(HashMap<Integer, String> checkBoxState2) {
        this.checkBoxState = checkBoxState2;
    }

    public boolean isCheckAll() {
        return this.isCheckAll;
    }

    public void setCheckAll(boolean isCheckAll2) {
        this.isCheckAll = isCheckAll2;
    }
}
