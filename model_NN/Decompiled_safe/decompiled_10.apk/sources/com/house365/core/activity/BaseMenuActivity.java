package com.house365.core.activity;

import android.content.Intent;
import android.view.KeyEvent;
import com.house365.core.anim.AnimBean;
import com.house365.core.anim.AnimCommon;

public abstract class BaseMenuActivity extends BaseCommonActivity {
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }
}
