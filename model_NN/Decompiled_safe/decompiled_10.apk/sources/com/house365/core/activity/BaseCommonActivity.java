package com.house365.core.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mobstat.StatService;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.anim.AnimBean;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import com.house365.core.image.ImageLoadedCallback;
import com.house365.core.touch.ImageViewTouch;
import com.house365.core.view.LoadingDialog;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class BaseCommonActivity extends Activity {
    protected AlertDialog.Builder alertDialog;
    AnalyseMetaData analyseMetadata;
    private AsyncImageLoader mAil;
    protected BaseApplication mApplication;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BaseCommonActivity.this.finish();
        }
    };
    protected Activity thisInstance;
    protected LoadingDialog tloadingDialog;

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initView();

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public String getTAG() {
        return getClass().getName();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisInstance = this;
        this.mApplication = (BaseApplication) getApplication();
        registerReceiver(this.mLoggedOutReceiver, new IntentFilter(ActionTag.INTENT_ACTION_LOGGED_OUT));
        preparedCreate(savedInstanceState);
        initView();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        dismissLoadingDialog();
        this.tloadingDialog = null;
        this.alertDialog = null;
        if (this.mAil != null) {
            this.mAil.clearCacheImage();
        }
        clean();
        unregisterReceiver(this.mLoggedOutReceiver);
    }

    public void setImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, getImageLoader());
    }

    public void setImage(ImageView imageView, String imageUrl, int scaleType, ImageLoadedCallback imgLoadObj) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, scaleType, getImageLoader(), imgLoadObj);
    }

    public void setTextImage(TextView textView, CacheImageUtil.TextImagePosition position, String imageUrl) {
        CacheImageUtil.setCacheTextImage(textView, position, imageUrl, getImageLoader());
    }

    public void setTouchImage(ImageViewTouch imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setTouchImage(imageView, imageUrl, getResources(), resId, scaleType, getImageLoader());
    }

    public AsyncImageLoader getImageLoader() {
        if (this.mAil == null) {
            this.mAil = new AsyncImageLoader(this);
        }
        return this.mAil;
    }

    public void setImage(ImageView imageView, final String localFileUri) {
        try {
            int w = imageView.getWidth();
            int h = imageView.getHeight();
            if (w < 0) {
                w = -1;
            }
            if (h < 0) {
                h = -1;
            }
            Bitmap bitmap = this.mAil.getBitMapFromStream(new AsyncImageLoader.BitMapInputStream() {
                public InputStream getInputStream() throws IOException {
                    return new FileInputStream(localFileUri);
                }
            }, w, h, -1);
            if (bitmap != null && !bitmap.isRecycled()) {
                imageView.setImageBitmap(bitmap);
            }
        } catch (IOException e) {
            CorePreferences.ERROR(e);
        }
    }

    private LoadingDialog getLoadingDialog() {
        if (this.tloadingDialog == null) {
            this.tloadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.tloadingDialog.setCancelable(isCancelDialog());
            if (isCancelDialog()) {
                this.tloadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        BaseCommonActivity.this.onExitDialog();
                    }
                });
            }
        }
        return this.tloadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void showLoadingDialog(int resid) {
        if (getLoadingDialog() != null) {
            getLoadingDialog().setMessage(getResources().getString(resid));
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public AlertDialog.Builder getAlertDialog() {
        if (this.alertDialog == null) {
            this.alertDialog = new AlertDialog.Builder(this);
        }
        return this.alertDialog;
    }

    public void showToast(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void showToast(int resId) {
        Toast.makeText(this, resId, 0).show();
    }

    public void onExitDialog() {
        this.thisInstance.finish();
    }

    public boolean isCancelDialog() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void clean() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            this.analyseMetadata = HouseAnalyse.onPageResume(this);
        }
        if (CorePreferences.getInstance(this).getCoreConfig().isOpenBaiduStat()) {
            StatService.onResume(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            HouseAnalyse.onPagePause(this.analyseMetadata);
        }
        if (CorePreferences.getInstance(this).getCoreConfig().isOpenBaiduStat()) {
            StatService.onPause(this);
        }
    }

    public void finish() {
        super.finish();
        AnimBean animBean = getFinishAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_right, R.anim.slide_fix);
    }

    public AnimBean getFinishAnim() {
        return new AnimBean(R.anim.slide_fix, R.anim.slide_out_right);
    }
}
