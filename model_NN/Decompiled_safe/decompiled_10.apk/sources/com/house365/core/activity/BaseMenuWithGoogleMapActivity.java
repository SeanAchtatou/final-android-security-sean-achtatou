package com.house365.core.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Projection;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.core.R;
import com.house365.core.anim.AnimBean;
import com.house365.core.anim.AnimCommon;
import com.house365.core.application.BaseApplication;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import com.house365.core.view.LoadingDialog;

public abstract class BaseMenuWithGoogleMapActivity extends MapActivity {
    AnalyseMetaData analyseMetadata;
    protected AsyncImageLoader mAil;
    protected BaseApplication mApplication;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BaseMenuWithGoogleMapActivity.this.finish();
        }
    };
    boolean mVisibleFromClient = true;
    protected Activity thisInstance;
    protected LoadingDialog tloadingDialog;

    public enum GeoPointSceen {
        TOP_LEFT,
        BOTTOM_RIGHT
    }

    public abstract MapView getMapView();

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.maps.MapActivity, android.app.Activity, com.house365.core.activity.BaseMenuWithGoogleMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onCreate(android.os.Bundle r4) {
        /*
            r3 = this;
            com.house365.core.activity.BaseMenuWithGoogleMapActivity.super.onCreate(r4)
            r3.thisInstance = r3
            android.content.BroadcastReceiver r0 = r3.mLoggedOutReceiver
            android.content.IntentFilter r1 = new android.content.IntentFilter
            java.lang.String r2 = "com.house365.core.action.LOGGED_OUT"
            r1.<init>(r2)
            r3.registerReceiver(r0, r1)
            android.app.Application r0 = r3.getApplication()
            com.house365.core.application.BaseApplication r0 = (com.house365.core.application.BaseApplication) r0
            r3.mApplication = r0
            r3.preparedCreate(r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.activity.BaseMenuWithGoogleMapActivity.onCreate(android.os.Bundle):void");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, com.house365.core.activity.BaseMenuWithGoogleMapActivity] */
    public AsyncImageLoader getImageLoader() {
        if (this.mAil == null) {
            this.mAil = new AsyncImageLoader(this);
        }
        return this.mAil;
    }

    public void setImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, getImageLoader());
    }

    public void invalidate(String imageUrl) {
        getImageLoader().invalidate(imageUrl);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        BaseMenuWithGoogleMapActivity.super.onDestroy();
        if (this.mAil != null) {
            this.mAil.clearCacheImage();
        }
        unregisterReceiver(this.mLoggedOutReceiver);
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.content.Context, com.google.android.maps.MapActivity, com.house365.core.activity.BaseMenuWithGoogleMapActivity] */
    /* access modifiers changed from: protected */
    public void onResume() {
        BaseMenuWithGoogleMapActivity.super.onResume();
        this.analyseMetadata = HouseAnalyse.onPageResume(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        BaseMenuWithGoogleMapActivity.super.onPause();
        HouseAnalyse.onPagePause(this.analyseMetadata);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.content.Context, com.house365.core.activity.BaseMenuWithGoogleMapActivity] */
    private LoadingDialog getLoadingDialog() {
        if (this.tloadingDialog == null) {
            this.tloadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.tloadingDialog.setCancelable(isCancelDialog());
            if (isCancelDialog()) {
                this.tloadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        BaseMenuWithGoogleMapActivity.this.onExitDialog();
                    }
                });
            }
        }
        return this.tloadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void showLoadingDialog(int resid) {
        if (getLoadingDialog() != null) {
            getLoadingDialog().setMessage(getResources().getString(resid));
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public boolean isCancelDialog() {
        return true;
    }

    public void onExitDialog() {
        this.thisInstance.finish();
    }

    public GeoPoint getGeoPointOfScreen(GeoPointSceen screenType) {
        Projection projection = getMapView().getProjection();
        if (screenType == GeoPointSceen.TOP_LEFT) {
            return projection.fromPixels(0, 0);
        }
        if (screenType != GeoPointSceen.BOTTOM_RIGHT) {
            return projection.fromPixels(0, 0);
        }
        int w = 0;
        int h = 0;
        if (getMapView().getWidth() != 0) {
            w = getMapView().getWidth();
            h = getMapView().getHeight();
        }
        if (w == 0 && getMapView().getMeasuredWidth() != 0) {
            w = getMapView().getMeasuredWidth();
            h = getMapView().getMeasuredHeight();
        }
        if (w == 0) {
            w = this.mApplication.getMetrics().widthPixels;
            h = this.mApplication.getMetrics().heightPixels;
        }
        return projection.fromPixels(w, h);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            return false;
        }
        return BaseMenuWithGoogleMapActivity.super.onKeyDown(keyCode, event);
    }

    public void startActivity(Intent intent) {
        BaseMenuWithGoogleMapActivity.super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        BaseMenuWithGoogleMapActivity.super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_bottom, R.anim.slide_fix);
    }
}
