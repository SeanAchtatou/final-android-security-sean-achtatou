package com.house365.core.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.Toast;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.GeoPoint;
import com.baidu.mapapi.MapActivity;
import com.baidu.mapapi.MapView;
import com.baidu.mapapi.Projection;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.anim.AnimBean;
import com.house365.core.anim.AnimCommon;
import com.house365.core.application.BaseApplicationWithMapService;
import com.house365.core.constant.CorePreferences;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import com.house365.core.view.LoadingDialog;

public abstract class BaseMenuWithBaiduMapActivity extends MapActivity {
    AnalyseMetaData analyseMetadata;
    protected AsyncImageLoader mAil;
    protected BaseApplicationWithMapService mApplication;
    protected BMapManager mBMapMan;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            BaseMenuWithBaiduMapActivity.this.finish();
        }
    };
    protected Activity thisInstance;
    protected LoadingDialog tloadingDialog;

    public enum GeoPointSceen {
        TOP_LEFT,
        BOTTOM_RIGHT
    }

    public abstract MapView getMapView();

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initView();

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisInstance = this;
        registerReceiver(this.mLoggedOutReceiver, new IntentFilter(ActionTag.INTENT_ACTION_LOGGED_OUT));
        this.mApplication = (BaseApplicationWithMapService) getApplication();
        preparedCreate(savedInstanceState);
        this.mBMapMan = this.mApplication.getbMapManager();
        super.initMapActivity(this.mBMapMan);
        initView();
        initData();
    }

    public AsyncImageLoader getImageLoader() {
        if (this.mAil == null) {
            this.mAil = new AsyncImageLoader(this);
        }
        return this.mAil;
    }

    public void setImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, getImageLoader());
    }

    public void invalidate(String imageUrl) {
        getImageLoader().invalidate(imageUrl);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        dismissLoadingDialog();
        this.tloadingDialog = null;
        if (this.mAil != null) {
            this.mAil.clearCacheImage();
        }
        clean();
        unregisterReceiver(this.mLoggedOutReceiver);
    }

    private LoadingDialog getLoadingDialog() {
        if (this.tloadingDialog == null) {
            this.tloadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.tloadingDialog.setCancelable(isCancelDialog());
            if (isCancelDialog()) {
                this.tloadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        BaseMenuWithBaiduMapActivity.this.onExitDialog();
                    }
                });
            }
        }
        return this.tloadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void showLoadingDialog(int resid) {
        if (getLoadingDialog() != null) {
            getLoadingDialog().setMessage(getResources().getString(resid));
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public void showToast(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void showToast(int resId) {
        Toast.makeText(this, resId, 0).show();
    }

    public void onExitDialog() {
        this.thisInstance.finish();
    }

    public boolean isCancelDialog() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void clean() {
        if (this.mAil != null) {
            this.mAil.clearCacheImage();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.mBMapMan != null) {
            this.mBMapMan.start();
        }
        super.onResume();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            this.analyseMetadata = HouseAnalyse.onPageResume(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.mBMapMan != null) {
            this.mBMapMan.stop();
        }
        super.onPause();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            HouseAnalyse.onPagePause(this.analyseMetadata);
        }
    }

    public GeoPoint getGeoPointOfScreen(GeoPointSceen screenType) {
        Projection projection = getMapView().getProjection();
        if (screenType == GeoPointSceen.TOP_LEFT) {
            return projection.fromPixels(0, 0);
        }
        if (screenType != GeoPointSceen.BOTTOM_RIGHT) {
            return projection.fromPixels(0, 0);
        }
        int w = 0;
        int h = 0;
        if (getMapView().getWidth() != 0) {
            w = getMapView().getWidth();
            h = getMapView().getHeight();
        }
        if (w == 0 && getMapView().getMeasuredWidth() != 0) {
            w = getMapView().getMeasuredWidth();
            h = getMapView().getMeasuredHeight();
        }
        if (w == 0) {
            w = this.mApplication.getMetrics().widthPixels;
            h = this.mApplication.getMetrics().heightPixels;
        }
        return projection.fromPixels(w, h);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            AnimCommon.set(animBean.getIn(), animBean.getOut());
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_bottom, R.anim.slide_fix);
    }
}
