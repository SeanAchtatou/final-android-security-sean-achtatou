package com.house365.core.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.baidu.mobstat.StatService;
import com.house365.app.analyse.HouseAnalyse;
import com.house365.app.analyse.data.AnalyseMetaData;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.anim.AnimBean;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.LoadingDialog;

public abstract class BaseListActivity extends ListActivity {
    protected AlertDialog.Builder alertDialog;
    AnalyseMetaData analyseMetadata;
    protected LoadingDialog loadingDialog;
    protected BaseApplication mApplication;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            CorePreferences.DEBUG("onReceive: " + intent);
            BaseListActivity.this.finish();
        }
    };
    protected Activity thisInstance;

    /* access modifiers changed from: protected */
    public abstract void clean();

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initView();

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisInstance = this;
        this.mApplication = (BaseApplication) getApplication();
        registerReceiver(this.mLoggedOutReceiver, new IntentFilter(ActionTag.INTENT_ACTION_LOGGED_OUT));
        preparedCreate(savedInstanceState);
        initView();
        initData();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mLoggedOutReceiver);
        clean();
    }

    private LoadingDialog getLoadingDialog() {
        if (this.loadingDialog == null) {
            this.loadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.loadingDialog.setCancelable(false);
            this.loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    BaseListActivity.this.thisInstance.finish();
                }
            });
        }
        return this.loadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public AlertDialog.Builder getAlertDialog() {
        if (this.alertDialog == null) {
            this.alertDialog = new AlertDialog.Builder(this);
        }
        return this.alertDialog;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            this.analyseMetadata = HouseAnalyse.onPageResume(this);
        }
        if (CorePreferences.getInstance(this).getCoreConfig().isOpenBaiduStat()) {
            StatService.onResume(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (CorePreferences.getInstance(this).getCoreConfig().isAnalyse()) {
            HouseAnalyse.onPagePause(this.analyseMetadata);
        }
        if (CorePreferences.getInstance(this).getCoreConfig().isOpenBaiduStat()) {
            StatService.onPause(this);
        }
    }

    public void finish() {
        super.finish();
        AnimBean animBean = getFinishAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_right, R.anim.slide_fix);
    }

    public AnimBean getFinishAnim() {
        return new AnimBean(R.anim.slide_fix, R.anim.slide_out_right);
    }
}
