package com.house365.core.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.action.TabChangeReceiver;
import com.house365.core.action.TabChanger;
import com.house365.core.anim.AnimBean;
import com.house365.core.anim.AnimCommon;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.core.image.AsyncImageLoader;
import com.house365.core.image.CacheImageUtil;
import com.house365.core.image.ImageLoadedCallback;
import com.house365.core.touch.ImageViewTouch;
import com.house365.core.view.LoadingDialog;

public abstract class BaseTabActivity extends TabActivity implements TabChanger {
    protected AlertDialog.Builder alertDialog;
    private AsyncImageLoader mAil;
    protected BaseApplication mApplication;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            CorePreferences.DEBUG("onReceive: " + intent);
            BaseTabActivity.this.finish();
        }
    };
    private TabChangeReceiver tabChangeReceiver;
    protected Activity thisInstance;
    protected LoadingDialog tloadingDialog;

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mApplication = (BaseApplication) getApplication();
        registerReceiver(this.mLoggedOutReceiver, new IntentFilter(ActionTag.INTENT_ACTION_LOGGED_OUT));
        if (this.tabChangeReceiver == null) {
            this.tabChangeReceiver = new TabChangeReceiver(this);
            registerReceiver(this.tabChangeReceiver, new IntentFilter(ActionTag.INTENT_ACTION_CHANGE_TAB));
        }
        preparedCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mLoggedOutReceiver);
        if (this.tabChangeReceiver != null) {
            unregisterReceiver(this.tabChangeReceiver);
        }
    }

    public void addTab(String tag, View view, Intent intent) {
        TabHost.TabSpec tabspec = getTabHost().newTabSpec(tag);
        tabspec.setContent(intent);
        tabspec.setIndicator(view);
        getTabHost().addTab(tabspec);
    }

    public void changeTab(int tab) {
        getTabHost().setCurrentTab(tab);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (!(AnimCommon.in == 0 || AnimCommon.out == 0)) {
            super.overridePendingTransition(AnimCommon.in, AnimCommon.out);
            AnimCommon.clear();
        }
        super.onPause();
    }

    private LoadingDialog getLoadingDialog() {
        if (this.tloadingDialog == null) {
            this.tloadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.tloadingDialog.setCancelable(isCancelDialog());
            if (isCancelDialog()) {
                this.tloadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        BaseTabActivity.this.onExitDialog();
                    }
                });
            }
        }
        return this.tloadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void showLoadingDialog(int resid) {
        if (getLoadingDialog() != null) {
            getLoadingDialog().setMessage(getResources().getString(resid));
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public AlertDialog.Builder getAlertDialog() {
        if (this.alertDialog == null) {
            this.alertDialog = new AlertDialog.Builder(this);
        }
        return this.alertDialog;
    }

    public void showToast(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void showToast(int resId) {
        Toast.makeText(this, resId, 0).show();
    }

    public void onExitDialog() {
        this.thisInstance.finish();
    }

    public boolean isCancelDialog() {
        return true;
    }

    public void setImage(ImageView imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, resId, scaleType, getImageLoader());
    }

    public void setImage(ImageView imageView, String imageUrl, int scaleType, ImageLoadedCallback imgLoadObj) {
        CacheImageUtil.setCacheImage(imageView, imageUrl, scaleType, getImageLoader(), imgLoadObj);
    }

    public void setTextImage(TextView textView, CacheImageUtil.TextImagePosition position, String imageUrl) {
        CacheImageUtil.setCacheTextImage(textView, position, imageUrl, getImageLoader());
    }

    public void setTouchImage(ImageViewTouch imageView, String imageUrl, int resId, int scaleType) {
        CacheImageUtil.setTouchImage(imageView, imageUrl, getResources(), resId, scaleType, getImageLoader());
    }

    public AsyncImageLoader getImageLoader() {
        if (this.mAil == null) {
            this.mAil = new AsyncImageLoader(this);
        }
        return this.mAil;
    }

    public void finish() {
        super.finish();
        AnimBean animBean = getFinishAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        AnimBean animBean = getStartAnim();
        if (animBean != null && animBean.getIn() > 0 && animBean.getOut() > 0) {
            overridePendingTransition(animBean.getIn(), animBean.getOut());
        }
    }

    public AnimBean getStartAnim() {
        return new AnimBean(R.anim.slide_in_right, R.anim.slide_fix);
    }

    public AnimBean getFinishAnim() {
        return new AnimBean(R.anim.slide_fix, R.anim.slide_out_right);
    }
}
