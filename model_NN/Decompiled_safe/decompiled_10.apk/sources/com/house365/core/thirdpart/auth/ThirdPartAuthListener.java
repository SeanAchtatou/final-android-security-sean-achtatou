package com.house365.core.thirdpart.auth;

import android.os.Bundle;
import com.house365.core.thirdpart.auth.dto.AccessToken;

public interface ThirdPartAuthListener {
    void onAccessDenied();

    void onAuthException(Bundle bundle);

    void onComplete(AccessToken accessToken, Bundle bundle);
}
