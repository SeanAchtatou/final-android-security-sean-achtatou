package com.house365.core.thirdpart.auth;

public interface PageLoadingListener {
    void onPageFinished();

    void onPageStarted();
}
