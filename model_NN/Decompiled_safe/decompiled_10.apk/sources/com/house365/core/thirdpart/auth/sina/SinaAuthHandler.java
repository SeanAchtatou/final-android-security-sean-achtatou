package com.house365.core.thirdpart.auth.sina;

import android.os.Bundle;
import android.webkit.WebView;
import com.house365.core.thirdpart.auth.AppKeyInfo;
import com.house365.core.thirdpart.auth.AuthHandler;
import com.house365.core.thirdpart.auth.ThirdPartAuthListener;
import com.house365.core.thirdpart.auth.URLUtil;
import com.house365.core.thirdpart.auth.dto.AccessToken;

public class SinaAuthHandler extends AuthHandler {
    public SinaAuthHandler(AppKeyInfo appKeyInfo, ThirdPartAuthListener weiboAuthListener) {
        super(appKeyInfo, weiboAuthListener);
    }

    public boolean handleAuth(WebView view, String url) {
        if (!url.startsWith(this.appKeyInfo.getRedirect_uri())) {
            return false;
        }
        Bundle values = URLUtil.parseUrl(url);
        String error = values.getString("error");
        String error_code = values.getString("error_code");
        if (error == null && error_code == null) {
            AccessToken accessToken = new AccessToken(values);
            accessToken.setTokenSecret(this.appKeyInfo.getApp_secret());
            accessToken.setUid(values.getString(AccessToken.UID));
            this.weiboAuthListener.onComplete(accessToken, values);
        } else if (error.equals("access_denied")) {
            this.weiboAuthListener.onAccessDenied();
        } else {
            this.weiboAuthListener.onAuthException(values);
        }
        view.stopLoading();
        return true;
    }
}
