package com.house365.core.thirdpart.auth;

import android.webkit.WebView;

public abstract class AuthHandler {
    protected AppKeyInfo appKeyInfo;
    protected ThirdPartAuthListener weiboAuthListener;

    public abstract boolean handleAuth(WebView webView, String str);

    public AuthHandler(AppKeyInfo appKeyInfo2, ThirdPartAuthListener weiboAuthListener2) {
        this.appKeyInfo = appKeyInfo2;
        this.weiboAuthListener = weiboAuthListener2;
    }

    public AppKeyInfo getAppKeyInfo() {
        return this.appKeyInfo;
    }

    public void setAppKeyInfo(AppKeyInfo appKeyInfo2) {
        this.appKeyInfo = appKeyInfo2;
    }
}
