package com.house365.core.thirdpart.auth;

public class AppKeyInfo {
    private String app_key;
    private String app_secret;
    private String redirect_uri;
    private int type;

    public String getApp_key() {
        return this.app_key;
    }

    public void setApp_key(String app_key2) {
        this.app_key = app_key2;
    }

    public String getApp_secret() {
        return this.app_secret;
    }

    public void setApp_secret(String app_secret2) {
        this.app_secret = app_secret2;
    }

    public String getRedirect_uri() {
        return this.redirect_uri;
    }

    public void setRedirect_uri(String redirect_uri2) {
        this.redirect_uri = redirect_uri2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public AppKeyInfo(int type2, String app_key2, String app_secret2, String redirect_uri2) {
        this.type = type2;
        this.app_key = app_key2;
        this.app_secret = app_secret2;
        this.redirect_uri = redirect_uri2;
    }
}
