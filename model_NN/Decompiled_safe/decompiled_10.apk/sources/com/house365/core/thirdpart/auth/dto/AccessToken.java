package com.house365.core.thirdpart.auth.dto;

import android.os.Bundle;
import android.text.TextUtils;

public class AccessToken {
    public static final String EXPIRES = "expires_in";
    public static final String OPENID = "openid";
    public static final String TOKEN = "access_token";
    public static final String UID = "uid";
    private long expiresIn = 0;
    private String openid;
    private String token;
    private String tokenSecret;
    private String uid;

    public AccessToken() {
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    public void setTokenSecret(String tokenSecret2) {
        this.tokenSecret = tokenSecret2;
    }

    public AccessToken(Bundle values) {
        this.token = values.getString(TOKEN);
        setExpiresIn(values.getString(EXPIRES));
    }

    public AccessToken(String token2, String tokenSecret2) {
        this.token = token2;
        this.tokenSecret = tokenSecret2;
    }

    public AccessToken(String token2, String tokenSecret2, String expiresIn2) {
        this.token = token2;
        this.tokenSecret = tokenSecret2;
        if (expiresIn2 != null && !expiresIn2.equals("0")) {
            setExpiresIn(System.currentTimeMillis() + (Long.parseLong(expiresIn2) * 1000));
        }
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(long expiresIn2) {
        this.expiresIn = expiresIn2;
    }

    public void setExpiresIn(String expiresIn2) {
        if (expiresIn2 != null && !expiresIn2.equals("0")) {
            setExpiresIn(System.currentTimeMillis() + (Long.parseLong(expiresIn2) * 1000));
        }
    }

    public boolean isSessionValid() {
        return !TextUtils.isEmpty(this.token) && (getExpiresIn() == 0 || System.currentTimeMillis() < getExpiresIn());
    }

    public String getOpenid() {
        return this.openid;
    }

    public void setOpenid(String openid2) {
        this.openid = openid2;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid2) {
        this.uid = uid2;
    }
}
