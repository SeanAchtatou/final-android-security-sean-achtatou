package com.house365.core.thirdpart.auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.action.ActionTag;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.LoadingDialog;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public abstract class ThirdPartAuthActivity extends Activity implements ThirdPartAuthListener, PageLoadingListener {
    public static final String RESPONSE_TYPE_CLIENT_SIDE = "token";
    public static final String RESPONSE_TYPE_KEY = "response_type";
    public static final String RESPONSE_TYPE_SERVER_SIDE = "code";
    protected AlertDialog.Builder alertDialog;
    /* access modifiers changed from: private */
    public AuthHandler authHandler;
    private BroadcastReceiver mLoggedOutReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            ThirdPartAuthActivity.this.finish();
        }
    };
    private String response_type;
    protected Activity thisInstance;
    protected LoadingDialog tloadingDialog;
    private WebView webView;

    /* access modifiers changed from: protected */
    public abstract WebView getWebView();

    /* access modifiers changed from: protected */
    public abstract void initView();

    /* access modifiers changed from: protected */
    public abstract AuthHandler initWeiboConfig();

    /* access modifiers changed from: protected */
    public abstract void preparedCreate(Bundle bundle);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisInstance = this;
        registerReceiver(this.mLoggedOutReceiver, new IntentFilter(ActionTag.INTENT_ACTION_LOGGED_OUT));
        this.response_type = getIntent().getStringExtra(RESPONSE_TYPE_KEY);
        preparedCreate(savedInstanceState);
        initView();
        initData();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        clearCookies();
        this.authHandler = initWeiboConfig();
        this.webView = getWebView();
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (!ThirdPartAuthActivity.this.authHandler.handleAuth(view, url)) {
                    super.onPageStarted(view, url, favicon);
                    ThirdPartAuthActivity.this.onPageStarted();
                }
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                ThirdPartAuthActivity.this.dismissLoadingDialog();
                ThirdPartAuthActivity.this.onPageFinished();
            }
        });
        List<NameValuePair> oList = new ArrayList<>();
        oList.add(new BasicNameValuePair("client_id", this.authHandler.getAppKeyInfo().getApp_key()));
        oList.add(new BasicNameValuePair(RESPONSE_TYPE_KEY, this.response_type));
        oList.add(new BasicNameValuePair("redirect_uri", this.authHandler.getAppKeyInfo().getRedirect_uri()));
        oList.add(new BasicNameValuePair("display", "mobile"));
        if (RESPONSE_TYPE_SERVER_SIDE.equals(this.response_type)) {
            oList.add(new BasicNameValuePair("state", UUID.randomUUID().toString()));
            oList.add(new BasicNameValuePair("scope", "get_simple_userinfo,get_user_info"));
        }
        String url = URLUtil.genrateGetUrl(Thirdpart.getInstace(this).getAuthUrl(this.authHandler.getAppKeyInfo().getType()), oList);
        CorePreferences.DEBUG("thirdpart authenrize url : " + url);
        this.webView.loadUrl(url);
    }

    public void clearCookies() {
        CookieSyncManager createInstance = CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeAllCookie();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        dismissLoadingDialog();
        this.tloadingDialog = null;
        clean();
        unregisterReceiver(this.mLoggedOutReceiver);
    }

    private LoadingDialog getLoadingDialog() {
        if (this.tloadingDialog == null) {
            this.tloadingDialog = new LoadingDialog(this, R.style.dialog, R.string.loading);
            this.tloadingDialog.setCancelable(isCancelDialog());
            if (isCancelDialog()) {
                this.tloadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ThirdPartAuthActivity.this.onExitDialog();
                    }
                });
            }
        }
        return this.tloadingDialog;
    }

    public void showLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    public void showLoadingDialog(int resid) {
        if (getLoadingDialog() != null) {
            getLoadingDialog().setMessage(getResources().getString(resid));
            getLoadingDialog().show();
        }
    }

    public void dismissLoadingDialog() {
        if (getLoadingDialog() != null) {
            getLoadingDialog().dismiss();
        }
    }

    public AlertDialog.Builder getAlertDialog() {
        if (this.alertDialog == null) {
            this.alertDialog = new AlertDialog.Builder(this);
        }
        return this.alertDialog;
    }

    public void showToast(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void showToast(int resId) {
        Toast.makeText(this, resId, 0).show();
    }

    public void onExitDialog() {
        this.thisInstance.finish();
    }

    public boolean isCancelDialog() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void clean() {
    }
}
