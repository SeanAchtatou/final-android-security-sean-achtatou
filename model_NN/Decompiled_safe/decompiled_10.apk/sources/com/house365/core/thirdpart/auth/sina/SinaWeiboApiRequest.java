package com.house365.core.thirdpart.auth.sina;

import android.text.TextUtils;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONObject;
import com.house365.core.thirdpart.auth.AppKeyInfo;
import com.house365.core.thirdpart.auth.Thirdpart;
import com.house365.core.thirdpart.auth.WeiboApiRequest;
import com.house365.core.thirdpart.auth.WeiboAsyncRequest;
import com.house365.core.thirdpart.auth.WeiboException;
import com.house365.core.thirdpart.auth.WeiboHttpAPi;
import com.house365.core.thirdpart.auth.dto.AccessToken;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

public class SinaWeiboApiRequest implements WeiboApiRequest {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.entity.mime.MultipartEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public String post(WeiboHttpAPi httpApi, Thirdpart weibo, AppKeyInfo appkeyInfo, String content, String pic, WeiboAsyncRequest.RequestListener listener) throws WeiboException {
        String s;
        try {
            AccessToken accessToken = weibo.getAccessToken(1);
            if (!TextUtils.isEmpty(pic)) {
                MultipartEntity entity = new MultipartEntity();
                entity.addPart(AccessToken.TOKEN, new StringBody(accessToken.getToken(), Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("status", new StringBody(content, Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("pic", new FileBody(new File(pic)));
                s = httpApi.post(weibo.getUploadUrl(1), (HttpEntity) entity);
            } else {
                List<NameValuePair> oList = new ArrayList<>();
                oList.add(new BasicNameValuePair(AccessToken.TOKEN, accessToken.getToken()));
                oList.add(new BasicNameValuePair("status", content));
                s = httpApi.post(weibo.getUpdateUrl(1), oList);
            }
            int errCode = 0;
            String error = StringUtils.EMPTY;
            try {
                JSONObject json = new JSONObject(s);
                error = json.getString("error");
                errCode = json.getInt("error_code");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (errCode == 0) {
                return s;
            }
            throw new WeiboException(getReadableMsg(error, errCode), errCode);
        } catch (Exception e2) {
            CorePreferences.ERROR(e2);
            throw new WeiboException("send weibo occurs exception", e2);
        }
    }

    private static String getReadableMsg(String err, int errCode) {
        switch (errCode) {
            case 10009:
                return "任务过多，系统繁忙！";
            case 10010:
                return "任务超时！";
            case 10013:
                return "不合法的微博用户！";
            case 10014:
                return "应用的接口访问权限受限！";
            case 10018:
                return "请求长度超过限制！";
            case 10022:
                return "IP请求频次超过上限！";
            case 10023:
                return "用户请求频次超过上限！";
            case 20006:
                return "图片太大！";
            case 20008:
                return "内容为空！";
            case 20012:
                return "输入文字太长，请确认不超过140个字符！";
            case 20014:
                return "安全检查参数有误，请再调用一次！";
            case 20015:
                return "账号、IP或应用非法，暂时无法完成此操作！";
            case 20017:
                return "不能重复分享相同内容！";
            case 20019:
                return "不能重复分享相同内容！";
            case 20032:
                return "发布成功，目前服务器可能会有延迟，请耐心等待1-2分钟！";
            case 21301:
                return "认证失败！";
            case 21603:
                return "通知发送达到限制！";
            default:
                return err;
        }
    }
}
