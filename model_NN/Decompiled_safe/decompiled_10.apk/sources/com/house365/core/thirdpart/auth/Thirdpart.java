package com.house365.core.thirdpart.auth;

import android.content.Context;
import android.text.TextUtils;
import com.house365.core.thirdpart.auth.dto.AccessToken;
import com.house365.core.util.store.SharedPreferenceException;
import com.house365.core.util.store.SharedPreferencesUtil;

public class Thirdpart {
    public static final String ACTION_THIRDPART_TYPE = "ACTION_THIRDPART_TYPE";
    private static String QQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url = "https://graph.qq.com/oauth2.0/authorize";
    public static String QQ_UPDATE_URL = "https://open.t.qq.com/api/t/add";
    public static String QQ_UPLOAD_URL = "https://open.t.qq.com/api/t/add_pic";
    public static String QQ_URL_OAUTH2_ACCESS_AUTHORIZE = "https://open.t.qq.com/cgi-bin/oauth2/authorize";
    public static String SINA_SERVER = "https://api.weibo.com/2/";
    public static String SINA_UPDATE_URL = (String.valueOf(SINA_SERVER) + "statuses/update.json");
    public static String SINA_UPLOAD_URL = (String.valueOf(SINA_SERVER) + "statuses/upload.json");
    public static String SINA_URL_OAUTH2_ACCESS_AUTHORIZE = "https://api.weibo.com/oauth2/authorize";
    public static final int TYPE_QQ_COMMUNITY_LOGIN = 3;
    public static final int TYPE_QQ_WEIBO = 2;
    public static final int TYPE_SINA_LOGIN = 4;
    public static final int TYPE_SINA_WEIBO = 1;
    private static Thirdpart instance;
    private String KEY_PREFIX = "ACCESS_TOKEN_";
    private AppKeyInfo appKeyInfo;
    private SharedPreferencesUtil sharedPrefUtil;

    private Thirdpart(Context context) {
        this.sharedPrefUtil = new SharedPreferencesUtil(context.getSharedPreferences("weibo", 0));
    }

    public static Thirdpart getInstace(Context context) {
        if (instance == null) {
            instance = new Thirdpart(context);
        }
        return instance;
    }

    public AppKeyInfo getAppKeyInfo() {
        return this.appKeyInfo;
    }

    public void setAppKeyInfo(AppKeyInfo appKeyInfo2) {
        this.appKeyInfo = appKeyInfo2;
    }

    public void setQQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url(String qQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url) {
        if (TextUtils.isEmpty(qQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url)) {
            qQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url = QQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url;
        }
        QQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url = qQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url;
    }

    public boolean isBind(int type) {
        AccessToken accessToken;
        try {
            accessToken = (AccessToken) this.sharedPrefUtil.getObject(String.valueOf(this.KEY_PREFIX) + type, AccessToken.class);
        } catch (SharedPreferenceException e) {
            e.printStackTrace();
            accessToken = null;
        }
        if (accessToken == null || !accessToken.isSessionValid()) {
            return false;
        }
        return true;
    }

    public void unBind(int type) {
        this.sharedPrefUtil.clean(String.valueOf(this.KEY_PREFIX) + type);
    }

    public void bind(int type, AccessToken accessToken) {
        this.sharedPrefUtil.putObject(String.valueOf(this.KEY_PREFIX) + type, accessToken);
    }

    public String getAuthUrl(int type) {
        if (type == 1) {
            return SINA_URL_OAUTH2_ACCESS_AUTHORIZE;
        }
        if (type == 2) {
            return QQ_URL_OAUTH2_ACCESS_AUTHORIZE;
        }
        if (type == 3) {
            return QQ_COMM_URL_OAUTH2_ACCESS_AUTHORIZE_url;
        }
        return SINA_URL_OAUTH2_ACCESS_AUTHORIZE;
    }

    public String getUploadUrl(int type) {
        if (type == 1) {
            return SINA_UPLOAD_URL;
        }
        if (type == 2) {
            return QQ_UPLOAD_URL;
        }
        return SINA_UPLOAD_URL;
    }

    public String getUpdateUrl(int type) {
        if (type == 1) {
            return SINA_UPDATE_URL;
        }
        if (type == 2) {
            return QQ_UPDATE_URL;
        }
        return SINA_UPDATE_URL;
    }

    public AccessToken getAccessToken(int type) {
        try {
            return (AccessToken) this.sharedPrefUtil.getObject(String.valueOf(this.KEY_PREFIX) + type, AccessToken.class);
        } catch (SharedPreferenceException e) {
            e.printStackTrace();
            return null;
        }
    }
}
