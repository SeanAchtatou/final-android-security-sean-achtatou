package com.house365.core.thirdpart.auth;

import com.house365.core.thirdpart.auth.WeiboAsyncRequest;

public interface WeiboApiRequest {
    String post(WeiboHttpAPi weiboHttpAPi, Thirdpart thirdpart, AppKeyInfo appKeyInfo, String str, String str2, WeiboAsyncRequest.RequestListener requestListener) throws WeiboException;
}
