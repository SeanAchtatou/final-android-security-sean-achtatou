package com.house365.core.thirdpart.auth;

public class WeiboException extends Exception {
    private int errorCode = 0;

    public WeiboException() {
    }

    public WeiboException(String detailMessage, int errorCode2) {
        super(detailMessage);
        this.errorCode = errorCode2;
    }

    public WeiboException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public WeiboException(String detailMessage) {
        super(detailMessage);
    }

    public WeiboException(Throwable throwable) {
        super(throwable);
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode2) {
        this.errorCode = errorCode2;
    }
}
