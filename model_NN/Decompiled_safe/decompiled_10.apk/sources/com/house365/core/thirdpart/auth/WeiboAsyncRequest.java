package com.house365.core.thirdpart.auth;

import android.content.Context;
import com.house365.core.thirdpart.auth.qqwb.QQWeiboApiRequest;
import com.house365.core.thirdpart.auth.sina.SinaWeiboApiRequest;
import java.io.IOException;

public class WeiboAsyncRequest {
    /* access modifiers changed from: private */
    public WeiboHttpAPi httpApi;
    /* access modifiers changed from: private */
    public Thirdpart weibo;

    public interface RequestListener {
        void onComplete(String str);

        void onError(WeiboException weiboException);

        void onIOException(IOException iOException);
    }

    public WeiboAsyncRequest(Thirdpart weibo2, Context context) {
        this.weibo = weibo2;
        this.httpApi = new WeiboHttpAPi(context);
    }

    public void request(Context context, AppKeyInfo appkeyInfo, String content, String pic, RequestListener listener) {
        final AppKeyInfo appKeyInfo = appkeyInfo;
        final String str = content;
        final String str2 = pic;
        final RequestListener requestListener = listener;
        new Thread() {
            public void run() {
                String resp;
                try {
                    if (appKeyInfo.getType() == 2) {
                        resp = new QQWeiboApiRequest().post(WeiboAsyncRequest.this.httpApi, WeiboAsyncRequest.this.weibo, appKeyInfo, str, str2, requestListener);
                    } else if (appKeyInfo.getType() == 1) {
                        resp = new SinaWeiboApiRequest().post(WeiboAsyncRequest.this.httpApi, WeiboAsyncRequest.this.weibo, appKeyInfo, str, str2, requestListener);
                    } else {
                        resp = new SinaWeiboApiRequest().post(WeiboAsyncRequest.this.httpApi, WeiboAsyncRequest.this.weibo, appKeyInfo, str, str2, requestListener);
                    }
                    requestListener.onComplete(resp);
                } catch (WeiboException e) {
                    requestListener.onError(e);
                }
            }
        }.start();
    }
}
