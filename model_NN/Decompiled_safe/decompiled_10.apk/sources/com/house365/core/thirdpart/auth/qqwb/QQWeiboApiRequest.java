package com.house365.core.thirdpart.auth.qqwb;

import android.text.TextUtils;
import com.house365.core.constant.CorePreferences;
import com.house365.core.json.JSONObject;
import com.house365.core.thirdpart.auth.AppKeyInfo;
import com.house365.core.thirdpart.auth.Thirdpart;
import com.house365.core.thirdpart.auth.WeiboApiRequest;
import com.house365.core.thirdpart.auth.WeiboAsyncRequest;
import com.house365.core.thirdpart.auth.WeiboException;
import com.house365.core.thirdpart.auth.WeiboHttpAPi;
import com.house365.core.thirdpart.auth.dto.AccessToken;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;

public class QQWeiboApiRequest implements WeiboApiRequest {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String
     arg types: [java.lang.String, org.apache.http.entity.mime.MultipartEntity]
     candidates:
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, java.util.List<org.apache.http.NameValuePair>):java.lang.String
      com.house365.core.http.BaseHttpAPI.post(java.lang.String, org.apache.http.HttpEntity):java.lang.String */
    public String post(WeiboHttpAPi httpApi, Thirdpart weibo, AppKeyInfo appkeyInfo, String content, String pic, WeiboAsyncRequest.RequestListener listener) throws WeiboException {
        String s;
        try {
            AccessToken accessToken = weibo.getAccessToken(2);
            if (!TextUtils.isEmpty(pic)) {
                MultipartEntity entity = new MultipartEntity();
                entity.addPart("oauth_consumer_key", new StringBody(appkeyInfo.getApp_key(), Charset.forName(CharEncoding.UTF_8)));
                entity.addPart(AccessToken.TOKEN, new StringBody(accessToken.getToken(), Charset.forName(CharEncoding.UTF_8)));
                entity.addPart(AccessToken.OPENID, new StringBody(accessToken.getOpenid(), Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("oauth_version", new StringBody("2.a", Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("scope", new StringBody("all", Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("format", new StringBody("json", Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("content", new StringBody(content, Charset.forName(CharEncoding.UTF_8)));
                entity.addPart("pic", new FileBody(new File(pic)));
                s = httpApi.post(weibo.getUploadUrl(2), (HttpEntity) entity);
            } else {
                List<NameValuePair> oList = new ArrayList<>();
                oList.add(new BasicNameValuePair("oauth_consumer_key", appkeyInfo.getApp_key()));
                oList.add(new BasicNameValuePair(AccessToken.TOKEN, accessToken.getToken()));
                oList.add(new BasicNameValuePair(AccessToken.OPENID, accessToken.getOpenid()));
                oList.add(new BasicNameValuePair("oauth_version", "2.a"));
                oList.add(new BasicNameValuePair("scope", "all"));
                oList.add(new BasicNameValuePair("format", "json"));
                oList.add(new BasicNameValuePair("content", content));
                s = httpApi.post(weibo.getUpdateUrl(2), oList);
            }
            int errCode = 0;
            int ret = 0;
            try {
                JSONObject json = new JSONObject(s);
                ret = json.getInt("ret");
                errCode = json.getInt("errcode");
                String msg = json.getString("msg");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (errCode == 0) {
                return s;
            }
            throw new WeiboException(getReadableMsg(ret, errCode), errCode);
        } catch (Exception e2) {
            CorePreferences.ERROR(e2);
            throw new WeiboException("send weibo occurs exception", e2);
        }
    }

    private String getReadableMsg(int type, int errcode) {
        switch (errcode) {
            case 1:
                return "clientip错误！";
            case 2:
                return "内容超出长度限制或为空！";
            case 4:
                return "讲文明，树新风！";
            case 5:
                return "禁止访问！";
            case 9:
                return type == 1 ? "包含垃圾信息！" : "图片大小超出限制或为0！";
            case 10:
                if (type == 1) {
                    return "频率限制！";
                }
                return " 图片格式错误，目前仅支持gif、jpeg、jpg、png、bmp及ico格式";
            case 13:
                return "请不要连续发表重复内容！";
            case 16:
                return "腾讯服务器内部错误导致发表失败！";
            case 70:
                return "上传图片失败！";
            case 1472:
                return "腾讯服务器内部错误导致发表失败！";
            default:
                return null;
        }
    }
}
