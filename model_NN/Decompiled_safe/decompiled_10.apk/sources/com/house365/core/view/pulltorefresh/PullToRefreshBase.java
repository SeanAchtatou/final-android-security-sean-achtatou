package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import com.house365.core.view.pulltorefresh.internal.LoadingLayout;
import org.apache.commons.lang.SystemUtils;

public abstract class PullToRefreshBase<T extends View> extends LinearLayout {
    static final float FRICTION = 2.0f;
    static final int MANUAL_REFRESHING = 3;
    public static final int MODE_BOTH = 3;
    public static final int MODE_PULL_DOWN_TO_REFRESH = 1;
    public static final int MODE_PULL_UP_TO_REFRESH = 2;
    static final int PULL_TO_REFRESH = 0;
    static final int REFRESHING = 2;
    static final int RELEASE_TO_REFRESH = 1;
    protected int mCurrentMode;
    private PullToRefreshBase<T>.SmoothScrollRunnable mCurrentSmoothScrollRunnable;
    private boolean mDisableScrollingWhileRefreshing = true;
    protected LoadingLayout mFooterLayout;
    private final Handler mHandler = new Handler();
    protected int mHeaderHeight;
    protected LoadingLayout mHeaderLayout;
    private float mInitialMotionY;
    private boolean mIsBeingDragged = false;
    private boolean mIsPullToRefreshEnabled = true;
    private float mLastMotionX;
    private float mLastMotionY;
    protected int mMode = 1;
    private OnRefreshListener mOnRefreshListener;
    T mRefreshableView;
    private int mState = 0;
    private int mTouchSlop;

    public interface OnLastItemVisibleListener {
        void onLastItemVisible();
    }

    public interface OnRefreshListener {
        void onFooterRefresh();

        void onHeaderRefresh();
    }

    /* access modifiers changed from: protected */
    public abstract T createRefreshableView(Context context, AttributeSet attributeSet);

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullDown();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullUp();

    final class SmoothScrollRunnable implements Runnable {
        static final int ANIMATION_DURATION_MS = 190;
        static final int ANIMATION_FPS = 16;
        private boolean mContinueRunning = true;
        private int mCurrentY = -1;
        private final Handler mHandler;
        private final Interpolator mInterpolator;
        private final int mScrollFromY;
        private final int mScrollToY;
        private long mStartTime = -1;

        public SmoothScrollRunnable(Handler handler, int fromY, int toY) {
            this.mHandler = handler;
            this.mScrollFromY = fromY;
            this.mScrollToY = toY;
            this.mInterpolator = new AccelerateDecelerateInterpolator();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.mStartTime == -1) {
                this.mStartTime = System.currentTimeMillis();
            } else {
                this.mCurrentY = this.mScrollFromY - Math.round(((float) (this.mScrollFromY - this.mScrollToY)) * this.mInterpolator.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.mStartTime) * 1000) / 190, 1000L), 0L)) / 1000.0f));
                PullToRefreshBase.this.setHeaderScroll(this.mCurrentY);
            }
            if (this.mContinueRunning && this.mScrollToY != this.mCurrentY) {
                this.mHandler.postDelayed(this, 16);
            }
        }

        public void stop() {
            this.mContinueRunning = false;
            this.mHandler.removeCallbacks(this);
        }
    }

    public PullToRefreshBase(Context context) {
        super(context);
        init(context, null);
    }

    public PullToRefreshBase(Context context, int mode) {
        super(context);
        this.mMode = mode;
        init(context, null);
    }

    public PullToRefreshBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public final T getAdapterView() {
        return this.mRefreshableView;
    }

    public final T getRefreshableView() {
        return this.mRefreshableView;
    }

    public final boolean isPullToRefreshEnabled() {
        return this.mIsPullToRefreshEnabled;
    }

    public final boolean isDisableScrollingWhileRefreshing() {
        return this.mDisableScrollingWhileRefreshing;
    }

    public final boolean isRefreshing() {
        return this.mState == 2 || this.mState == 3;
    }

    public final void setDisableScrollingWhileRefreshing(boolean disableScrollingWhileRefreshing) {
        this.mDisableScrollingWhileRefreshing = disableScrollingWhileRefreshing;
    }

    public final void onRefreshComplete() {
        if (this.mState != 0) {
            resetHeader();
        }
    }

    public final void setOnRefreshListener(OnRefreshListener listener) {
        this.mOnRefreshListener = listener;
    }

    public final void setPullToRefreshEnabled(boolean enable) {
        this.mIsPullToRefreshEnabled = enable;
    }

    public void setReleaseLabel(String releaseLabel, int model) {
        if (this.mHeaderLayout != null && model == 1) {
            this.mHeaderLayout.setReleaseLabel(releaseLabel);
        }
        if (this.mFooterLayout != null && model == 2) {
            this.mFooterLayout.setReleaseLabel(releaseLabel);
        }
    }

    public void setPullLabel(String pullLabel, int model) {
        if (this.mHeaderLayout != null && model == 1) {
            this.mHeaderLayout.setPullLabel(pullLabel);
        }
        if (this.mFooterLayout != null && model == 2) {
            this.mFooterLayout.setPullLabel(pullLabel);
        }
    }

    public void setRefreshingLabel(String refreshingLabel, int model) {
        if (this.mHeaderLayout != null && model == 1) {
            this.mHeaderLayout.setRefreshingLabel(refreshingLabel);
        }
        if (this.mFooterLayout != null && model == 2) {
            this.mFooterLayout.setRefreshingLabel(refreshingLabel);
        }
    }

    public final void setRefreshing() {
        setRefreshing(true);
    }

    public final void setRefreshing(boolean doScroll) {
        if (!isRefreshing()) {
            setRefreshingInternal(doScroll);
            this.mState = 3;
        }
    }

    public final boolean hasPullFromTop() {
        return this.mCurrentMode != 2;
    }

    public final boolean onTouchEvent(MotionEvent event) {
        if (!this.mIsPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.mDisableScrollingWhileRefreshing) {
            return true;
        }
        if (event.getAction() == 0 && event.getEdgeFlags() != 0) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                if (!isReadyForPull()) {
                    return false;
                }
                float y = event.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                return true;
            case 1:
            case 3:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                this.mIsBeingDragged = false;
                if (this.mState != 1 || this.mOnRefreshListener == null) {
                    smoothScrollTo(0);
                } else {
                    setRefreshingInternal(true);
                    if (getCurrentMode() == 1) {
                        this.mOnRefreshListener.onHeaderRefresh();
                    } else if (getCurrentMode() == 2) {
                        this.mOnRefreshListener.onFooterRefresh();
                    }
                }
                return true;
            case 2:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                this.mLastMotionY = event.getY();
                pullEvent();
                return true;
            default:
                return false;
        }
    }

    public final boolean onInterceptTouchEvent(MotionEvent event) {
        if (!this.mIsPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.mDisableScrollingWhileRefreshing) {
            return true;
        }
        int action = event.getAction();
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            return false;
        } else if (action != 0 && this.mIsBeingDragged) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (isReadyForPull()) {
                        float y = event.getY();
                        this.mInitialMotionY = y;
                        this.mLastMotionY = y;
                        this.mLastMotionX = event.getX();
                        this.mIsBeingDragged = false;
                        break;
                    }
                    break;
                case 2:
                    if (isReadyForPull()) {
                        float y2 = event.getY();
                        float dy = y2 - this.mLastMotionY;
                        float yDiff = Math.abs(dy);
                        float xDiff = Math.abs(event.getX() - this.mLastMotionX);
                        if (yDiff > ((float) this.mTouchSlop) && yDiff > xDiff) {
                            if ((this.mMode != 1 && this.mMode != 3) || dy < 1.0E-4f || !isReadyForPullDown()) {
                                if ((this.mMode == 2 || this.mMode == 3) && dy <= 1.0E-4f && isReadyForPullUp()) {
                                    this.mLastMotionY = y2;
                                    this.mIsBeingDragged = true;
                                    if (this.mMode == 3) {
                                        this.mCurrentMode = 2;
                                        break;
                                    }
                                }
                            } else {
                                this.mLastMotionY = y2;
                                this.mIsBeingDragged = true;
                                if (this.mMode == 3) {
                                    this.mCurrentMode = 1;
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
            return this.mIsBeingDragged;
        }
    }

    /* access modifiers changed from: protected */
    public void addRefreshableView(Context context, View view) {
        addView(view, new LinearLayout.LayoutParams(-1, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public final int getCurrentMode() {
        return this.mCurrentMode;
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getFooterLayout() {
        return this.mFooterLayout;
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getHeaderLayout() {
        return this.mHeaderLayout;
    }

    /* access modifiers changed from: protected */
    public final int getHeaderHeight() {
        return this.mHeaderHeight;
    }

    /* access modifiers changed from: protected */
    public final int getMode() {
        return this.mMode;
    }

    /* access modifiers changed from: protected */
    public void resetHeader() {
        this.mState = 0;
        this.mIsBeingDragged = false;
        if (this.mHeaderLayout != null) {
            this.mHeaderLayout.reset();
        }
        if (this.mFooterLayout != null) {
            this.mFooterLayout.reset();
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean doScroll) {
        this.mState = 2;
        if (this.mHeaderLayout != null) {
            this.mHeaderLayout.refreshing();
        }
        if (this.mFooterLayout != null) {
            this.mFooterLayout.refreshing();
        }
        if (doScroll) {
            smoothScrollTo(this.mCurrentMode == 1 ? -this.mHeaderHeight : this.mHeaderHeight);
        }
    }

    /* access modifiers changed from: protected */
    public void setHeaderRefreshingInternal(boolean doScroll) {
        this.mState = 2;
        if (this.mHeaderLayout != null) {
            this.mHeaderLayout.refreshing();
        }
        if (doScroll) {
            smoothScrollTo(-this.mHeaderHeight);
        }
    }

    /* access modifiers changed from: protected */
    public final void setHeaderScroll(int y) {
        scrollTo(0, y);
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollTo(int y) {
        if (this.mCurrentSmoothScrollRunnable != null) {
            this.mCurrentSmoothScrollRunnable.stop();
        }
        if (getScrollY() != y) {
            this.mCurrentSmoothScrollRunnable = new SmoothScrollRunnable(this.mHandler, getScrollY(), y);
            this.mHandler.post(this.mCurrentSmoothScrollRunnable);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void init(android.content.Context r18, android.util.AttributeSet r19) {
        /*
            r17 = this;
            r2 = 1
            r0 = r17
            r0.setOrientation(r2)     // Catch:{ Exception -> 0x016b }
            int r2 = android.view.ViewConfiguration.getTouchSlop()     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mTouchSlop = r2     // Catch:{ Exception -> 0x016b }
            int[] r2 = com.house365.core.R.styleable.PullToRefresh     // Catch:{ Exception -> 0x016b }
            r0 = r18
            r1 = r19
            android.content.res.TypedArray r14 = r0.obtainStyledAttributes(r1, r2)     // Catch:{ Exception -> 0x016b }
            r2 = 3
            boolean r2 = r14.hasValue(r2)     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x0029
            r2 = 3
            r3 = 1
            int r2 = r14.getInteger(r2, r3)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mMode = r2     // Catch:{ Exception -> 0x016b }
        L_0x0029:
            android.view.View r2 = r17.createRefreshableView(r18, r19)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mRefreshableView = r2     // Catch:{ Exception -> 0x016b }
            r0 = r17
            T r2 = r0.mRefreshableView     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r1 = r18
            r0.addRefreshableView(r1, r2)     // Catch:{ Exception -> 0x016b }
            int r2 = com.house365.core.R.string.pull_to_refresh_pull_label     // Catch:{ Exception -> 0x016b }
            r0 = r18
            java.lang.String r6 = r0.getString(r2)     // Catch:{ Exception -> 0x016b }
            int r2 = com.house365.core.R.string.pull_to_refresh_refreshing_label     // Catch:{ Exception -> 0x016b }
            r0 = r18
            java.lang.String r7 = r0.getString(r2)     // Catch:{ Exception -> 0x016b }
            int r2 = com.house365.core.R.string.pull_to_refresh_release_label     // Catch:{ Exception -> 0x016b }
            r0 = r18
            java.lang.String r5 = r0.getString(r2)     // Catch:{ Exception -> 0x016b }
            int r2 = com.house365.core.R.string.pull_to_load_more_label     // Catch:{ Exception -> 0x016b }
            r0 = r18
            java.lang.String r11 = r0.getString(r2)     // Catch:{ Exception -> 0x016b }
            int r2 = com.house365.core.R.string.pull_to_load_more_release_label     // Catch:{ Exception -> 0x016b }
            r0 = r18
            java.lang.String r12 = r0.getString(r2)     // Catch:{ Exception -> 0x016b }
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = new com.house365.core.view.pulltorefresh.internal.LoadingLayout     // Catch:{ Exception -> 0x016b }
            r4 = 1
            r3 = r18
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mHeaderLayout = r2     // Catch:{ Exception -> 0x016b }
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r8 = new com.house365.core.view.pulltorefresh.internal.LoadingLayout     // Catch:{ Exception -> 0x016b }
            r10 = 2
            r9 = r18
            r13 = r7
            r8.<init>(r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mFooterLayout = r8     // Catch:{ Exception -> 0x016b }
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r3 = 1
            if (r2 == r3) goto L_0x008b
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r3 = 3
            if (r2 != r3) goto L_0x00b1
        L_0x008b:
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mHeaderLayout     // Catch:{ Exception -> 0x016b }
            r3 = 0
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x016b }
            r8 = -1
            r9 = -2
            r4.<init>(r8, r9)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.addView(r2, r3, r4)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mHeaderLayout     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.measureView(r2)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mHeaderLayout     // Catch:{ Exception -> 0x016b }
            int r2 = r2.getMeasuredHeight()     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mHeaderHeight = r2     // Catch:{ Exception -> 0x016b }
        L_0x00b1:
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r3 = 2
            if (r2 == r3) goto L_0x00bf
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r3 = 3
            if (r2 != r3) goto L_0x00e4
        L_0x00bf:
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mFooterLayout     // Catch:{ Exception -> 0x016b }
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x016b }
            r4 = -1
            r8 = -2
            r3.<init>(r4, r8)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.addView(r2, r3)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mFooterLayout     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.measureView(r2)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mFooterLayout     // Catch:{ Exception -> 0x016b }
            int r2 = r2.getMeasuredHeight()     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mHeaderHeight = r2     // Catch:{ Exception -> 0x016b }
        L_0x00e4:
            r2 = 2
            boolean r2 = r14.hasValue(r2)     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x010c
            r2 = 2
            r3 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            int r15 = r14.getColor(r2, r3)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mHeaderLayout     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x00ff
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mHeaderLayout     // Catch:{ Exception -> 0x016b }
            r2.setTextColor(r15)     // Catch:{ Exception -> 0x016b }
        L_0x00ff:
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mFooterLayout     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x010c
            r0 = r17
            com.house365.core.view.pulltorefresh.internal.LoadingLayout r2 = r0.mFooterLayout     // Catch:{ Exception -> 0x016b }
            r2.setTextColor(r15)     // Catch:{ Exception -> 0x016b }
        L_0x010c:
            r2 = 1
            boolean r2 = r14.hasValue(r2)     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x011e
            r2 = 1
            r3 = -1
            int r2 = r14.getResourceId(r2, r3)     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.setBackgroundResource(r2)     // Catch:{ Exception -> 0x016b }
        L_0x011e:
            r2 = 0
            boolean r2 = r14.hasValue(r2)     // Catch:{ Exception -> 0x016b }
            if (r2 == 0) goto L_0x0132
            r0 = r17
            T r2 = r0.mRefreshableView     // Catch:{ Exception -> 0x016b }
            r3 = 0
            r4 = -1
            int r3 = r14.getResourceId(r3, r4)     // Catch:{ Exception -> 0x016b }
            r2.setBackgroundResource(r3)     // Catch:{ Exception -> 0x016b }
        L_0x0132:
            r14.recycle()     // Catch:{ Exception -> 0x016b }
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            switch(r2) {
                case 2: goto L_0x0170;
                case 3: goto L_0x0159;
                default: goto L_0x013c;
            }     // Catch:{ Exception -> 0x016b }
        L_0x013c:
            r2 = 0
            r0 = r17
            int r3 = r0.mHeaderHeight     // Catch:{ Exception -> 0x016b }
            int r3 = -r3
            r4 = 0
            r8 = 0
            r0 = r17
            r0.setPadding(r2, r3, r4, r8)     // Catch:{ Exception -> 0x016b }
        L_0x0149:
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r3 = 3
            if (r2 == r3) goto L_0x0158
            r0 = r17
            int r2 = r0.mMode     // Catch:{ Exception -> 0x016b }
            r0 = r17
            r0.mCurrentMode = r2     // Catch:{ Exception -> 0x016b }
        L_0x0158:
            return
        L_0x0159:
            r2 = 0
            r0 = r17
            int r3 = r0.mHeaderHeight     // Catch:{ Exception -> 0x016b }
            int r3 = -r3
            r4 = 0
            r0 = r17
            int r8 = r0.mHeaderHeight     // Catch:{ Exception -> 0x016b }
            int r8 = -r8
            r0 = r17
            r0.setPadding(r2, r3, r4, r8)     // Catch:{ Exception -> 0x016b }
            goto L_0x0149
        L_0x016b:
            r16 = move-exception
            com.house365.core.constant.CorePreferences.ERROR(r16)
            goto L_0x0158
        L_0x0170:
            r2 = 0
            r3 = 0
            r4 = 0
            r0 = r17
            int r8 = r0.mHeaderHeight     // Catch:{ Exception -> 0x016b }
            int r8 = -r8
            r0 = r17
            r0.setPadding(r2, r3, r4, r8)     // Catch:{ Exception -> 0x016b }
            goto L_0x0149
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.view.pulltorefresh.PullToRefreshBase.init(android.content.Context, android.util.AttributeSet):void");
    }

    private void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    private boolean pullEvent() {
        int newHeight;
        int oldHeight = getScrollY();
        switch (this.mCurrentMode) {
            case 2:
                newHeight = Math.round(Math.max(this.mInitialMotionY - this.mLastMotionY, (float) SystemUtils.JAVA_VERSION_FLOAT) / FRICTION);
                break;
            default:
                newHeight = Math.round(Math.min(this.mInitialMotionY - this.mLastMotionY, (float) SystemUtils.JAVA_VERSION_FLOAT) / FRICTION);
                break;
        }
        setHeaderScroll(newHeight);
        if (newHeight != 0) {
            if (this.mState == 0 && this.mHeaderHeight < Math.abs(newHeight)) {
                this.mState = 1;
                switch (this.mCurrentMode) {
                    case 1:
                        this.mHeaderLayout.releaseToRefresh();
                        return true;
                    case 2:
                        this.mFooterLayout.releaseToRefresh();
                        return true;
                    default:
                        return true;
                }
            } else if (this.mState == 1 && this.mHeaderHeight >= Math.abs(newHeight)) {
                this.mState = 0;
                switch (this.mCurrentMode) {
                    case 1:
                        this.mHeaderLayout.pullToRefresh();
                        return true;
                    case 2:
                        this.mFooterLayout.pullToRefresh();
                        return true;
                    default:
                        return true;
                }
            }
        }
        if (oldHeight == newHeight) {
            return false;
        }
        return true;
    }

    private boolean isReadyForPull() {
        switch (this.mMode) {
            case 1:
                return isReadyForPullDown();
            case 2:
                return isReadyForPullUp();
            case 3:
                return isReadyForPullUp() || isReadyForPullDown();
            default:
                return false;
        }
    }

    public void setLongClickable(boolean longClickable) {
        getRefreshableView().setLongClickable(longClickable);
    }
}
