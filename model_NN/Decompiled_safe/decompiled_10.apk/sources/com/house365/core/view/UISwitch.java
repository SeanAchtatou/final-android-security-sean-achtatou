package com.house365.core.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.house365.core.R;

public class UISwitch extends LinearLayout implements View.OnTouchListener {
    private static final int SWIPE_MIN_DISTANCE = 10;
    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
        }
    };
    private ImageView closeImage;
    private Context context;
    private GestureDetector mGesture = null;
    private ImageView openImage;
    private Animation slideLeftIn;
    private Animation slideLeftOut;
    private Animation slideRightIn;
    private Animation slideRightOut;
    private ViewFlipper viewFlipper;

    public UISwitch(Context context2) {
        super(context2);
        this.context = context2;
        init(context2);
        this.mGesture = new GestureDetector(context2, new GestureListener());
    }

    public UISwitch(Context context2, AttributeSet attrs) {
        super(context2, attrs);
    }

    public boolean onTouch(View v, MotionEvent event) {
        return this.mGesture.onTouchEvent(event);
    }

    private void init(Context context2) {
        RelativeLayout.LayoutParams params_cal = new RelativeLayout.LayoutParams(-2, -2);
        this.viewFlipper = new ViewFlipper(context2);
        this.openImage = new ImageView(context2);
        this.openImage.setBackgroundResource(R.drawable.on);
        this.closeImage = new ImageView(context2);
        this.closeImage.setBackgroundResource(R.drawable.off);
        this.viewFlipper.addView(this.openImage);
        this.viewFlipper.addView(this.closeImage);
        this.openImage.setOnTouchListener(this);
        this.closeImage.setOnTouchListener(this);
        this.openImage.setLongClickable(true);
        this.closeImage.setLongClickable(true);
        addView(this.viewFlipper, params_cal);
        setAnimation();
    }

    private void setAnimation() {
        this.slideLeftIn = AnimationUtils.loadAnimation(this.context, R.anim.slide_in_left);
        this.slideLeftOut = AnimationUtils.loadAnimation(this.context, R.anim.slide_out_left);
        this.slideRightIn = AnimationUtils.loadAnimation(this.context, R.anim.slide_in_right);
        this.slideRightOut = AnimationUtils.loadAnimation(this.context, R.anim.slide_out_right);
        this.slideLeftIn.setAnimationListener(this.animationListener);
        this.slideLeftOut.setAnimationListener(this.animationListener);
        this.slideRightIn.setAnimationListener(this.animationListener);
        this.slideRightOut.setAnimationListener(this.animationListener);
    }

    class GestureListener extends GestureDetector.SimpleOnGestureListener {
        GestureListener() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (e1.getX() - e2.getX() <= 10.0f || !UISwitch.this.isOpen()) {
                    if (e2.getX() - e1.getX() > 10.0f && !UISwitch.this.isOpen()) {
                        UISwitch.this.setLastViewFlipper();
                        return true;
                    }
                    return false;
                }
                UISwitch.this.setNextViewFlipper();
                return true;
            } catch (Exception e) {
            }
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        public boolean onDoubleTap(MotionEvent e) {
            return super.onDoubleTap(e);
        }

        public boolean onDoubleTapEvent(MotionEvent e) {
            return super.onDoubleTapEvent(e);
        }

        public boolean onDown(MotionEvent e) {
            return super.onDown(e);
        }

        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return super.onScroll(e1, e2, distanceX, distanceY);
        }

        public void onShowPress(MotionEvent e) {
            super.onShowPress(e);
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }
    }

    /* access modifiers changed from: private */
    public void setNextViewFlipper() {
        this.viewFlipper.setInAnimation(this.slideLeftIn);
        this.viewFlipper.setOutAnimation(this.slideLeftOut);
        this.viewFlipper.showNext();
    }

    /* access modifiers changed from: private */
    public void setLastViewFlipper() {
        this.viewFlipper.setInAnimation(this.slideRightIn);
        this.viewFlipper.setOutAnimation(this.slideRightOut);
        this.viewFlipper.showPrevious();
    }

    public boolean isOpen() {
        if (((ImageView) this.viewFlipper.getCurrentView()) == this.openImage) {
            return true;
        }
        return false;
    }

    public void setOpenImage(ImageView openImage2) {
        this.openImage = openImage2;
    }

    public void setCloseImage(ImageView closeImage2) {
        this.closeImage = closeImage2;
    }
}
