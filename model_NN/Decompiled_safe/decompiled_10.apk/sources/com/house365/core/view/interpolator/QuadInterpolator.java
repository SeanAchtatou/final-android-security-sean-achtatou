package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class QuadInterpolator implements Interpolator {
    private EasingType.Type type;

    public QuadInterpolator(EasingType.Type type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float in(float t) {
        return t * t;
    }

    private float out(float t) {
        return (-t) * (t - 2.0f);
    }

    private float inout(float t) {
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return 0.5f * t2 * t2;
        }
        float t3 = t2 - 1.0f;
        return -0.5f * (((t3 - 2.0f) * t3) - 1.0f);
    }
}
