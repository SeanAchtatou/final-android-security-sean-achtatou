package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import com.house365.core.view.pulltorefresh.internal.EmptyViewMethodAccessor;

public class PullToRefreshExpandableListView extends PullToRefreshAdapterViewBase<ExpandableListView> {

    class InternalExpandableListView extends ExpandableListView implements EmptyViewMethodAccessor {
        public InternalExpandableListView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setEmptyView(View emptyView) {
            PullToRefreshExpandableListView.this.setEmptyView(emptyView);
        }

        public void setEmptyViewInternal(View emptyView) {
            super.setEmptyView(emptyView);
        }

        public ContextMenu.ContextMenuInfo getContextMenuInfo() {
            return super.getContextMenuInfo();
        }
    }

    public PullToRefreshExpandableListView(Context context) {
        super(context);
    }

    public PullToRefreshExpandableListView(Context context, int mode) {
        super(context, mode);
    }

    public PullToRefreshExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public final ExpandableListView createRefreshableView(Context context, AttributeSet attrs) {
        ExpandableListView lv = new InternalExpandableListView(context, attrs);
        lv.setId(16908298);
        return lv;
    }

    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return ((InternalExpandableListView) getRefreshableView()).getContextMenuInfo();
    }

    public void onRefreshComplete(CharSequence lastUpdated) {
        this.mHeaderLayout.setLastUpdate(lastUpdated);
        super.onRefreshComplete();
    }

    public void setFooterViewVisible(int visibility) {
        if (visibility == 8) {
            removeView(this.mFooterLayout);
            this.mMode = 1;
            this.mCurrentMode = 1;
            setPadding(0, -this.mHeaderHeight, 0, 0);
            return;
        }
        this.mMode = 3;
        addView(this.mFooterLayout);
        setPadding(0, -this.mHeaderHeight, 0, -this.mHeaderHeight);
    }

    public void setAdapter(BaseExpandableListAdapter adapter) {
        ((ExpandableListView) getRefreshableView()).setAdapter(adapter);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        ((ExpandableListView) getRefreshableView()).setOnItemClickListener(listener);
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
        ((ExpandableListView) getRefreshableView()).setOnItemLongClickListener(listener);
    }

    public void setOnChildClickListener(ExpandableListView.OnChildClickListener onChildClickListener) {
        ((ExpandableListView) getRefreshableView()).setOnChildClickListener(onChildClickListener);
    }

    public void setOnGroupClickListener(ExpandableListView.OnGroupClickListener onGroupClickListener) {
        ((ExpandableListView) getRefreshableView()).setOnGroupClickListener(onGroupClickListener);
    }

    public void setOnGroupExpandListener(ExpandableListView.OnGroupExpandListener onGroupExpandListener) {
        ((ExpandableListView) getRefreshableView()).setOnGroupExpandListener(onGroupExpandListener);
    }

    public void setOnGroupCollapseListener(ExpandableListView.OnGroupCollapseListener onGroupCollapseListener) {
        ((ExpandableListView) getRefreshableView()).setOnGroupCollapseListener(onGroupCollapseListener);
    }

    public void showRefreshView() {
        setHeaderRefreshingInternal(true);
    }

    public ListView getActureListView() {
        return (ListView) getRefreshableView();
    }
}
