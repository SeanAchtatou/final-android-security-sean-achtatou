package com.house365.core.view.interpolator;

public class EasingType {

    public enum Type {
        IN,
        OUT,
        INOUT
    }
}
