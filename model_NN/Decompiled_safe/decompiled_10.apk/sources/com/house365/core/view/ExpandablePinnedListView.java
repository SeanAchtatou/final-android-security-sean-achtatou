package com.house365.core.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

public class ExpandablePinnedListView extends ExpandableListView implements AbsListView.OnScrollListener, ExpandableListView.OnGroupClickListener {
    private static final int MAX_ALPHA = 255;
    private PinnedHeaderAdapter mAdapter;
    private float mDownX;
    private float mDownY;
    private View mHeaderView;
    private int mHeaderViewHeight;
    private boolean mHeaderViewVisible;
    private int mHeaderViewWidth;
    private int mOldState = -1;

    public interface PinnedHeaderAdapter {
        public static final int PINNED_HEADER_GONE = 0;
        public static final int PINNED_HEADER_PUSHED_UP = 2;
        public static final int PINNED_HEADER_VISIBLE = 1;

        void configurePinnedHeader(View view, int i, int i2, int i3);

        int getGroupClickStatus(int i);

        int getPinnedHeaderState(int i, int i2);

        void setGroupClickStatus(int i, int i2);
    }

    public ExpandablePinnedListView(Context context) {
        super(context);
        registerListener();
    }

    public ExpandablePinnedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        registerListener();
    }

    public ExpandablePinnedListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        registerListener();
    }

    public void setHeaderView(View view) {
        this.mHeaderView = view;
        view.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        if (this.mHeaderView != null) {
            setFadingEdgeLength(0);
        }
        requestLayout();
    }

    private void headerViewClick() {
        int groupPosition = ExpandableListView.getPackedPositionGroup(getExpandableListPosition(getFirstVisiblePosition()));
        if (this.mAdapter.getGroupClickStatus(groupPosition) == 1) {
            collapseGroup(groupPosition);
            this.mAdapter.setGroupClickStatus(groupPosition, 0);
        } else {
            expandGroup(groupPosition);
            this.mAdapter.setGroupClickStatus(groupPosition, 1);
        }
        setSelectedGroup(groupPosition);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mHeaderViewVisible) {
            switch (ev.getAction()) {
                case 0:
                    this.mDownX = ev.getX();
                    this.mDownY = ev.getY();
                    if (this.mDownX <= ((float) this.mHeaderViewWidth) && this.mDownY <= ((float) this.mHeaderViewHeight)) {
                        return true;
                    }
                case 1:
                    float x = ev.getX();
                    float y = ev.getY();
                    float offsetX = Math.abs(x - this.mDownX);
                    float offsetY = Math.abs(y - this.mDownY);
                    if (x <= ((float) this.mHeaderViewWidth) && y <= ((float) this.mHeaderViewHeight) && offsetX <= ((float) this.mHeaderViewWidth) && offsetY <= ((float) this.mHeaderViewHeight)) {
                        if (this.mHeaderView == null) {
                            return true;
                        }
                        headerViewClick();
                        return true;
                    }
            }
        }
        return super.onTouchEvent(ev);
    }

    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        this.mAdapter = (PinnedHeaderAdapter) adapter;
    }

    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (this.mAdapter.getGroupClickStatus(groupPosition) == 0) {
            this.mAdapter.setGroupClickStatus(groupPosition, 1);
            parent.expandGroup(groupPosition);
            parent.setSelectedGroup(groupPosition);
        } else if (this.mAdapter.getGroupClickStatus(groupPosition) == 1) {
            this.mAdapter.setGroupClickStatus(groupPosition, 0);
            parent.collapseGroup(groupPosition);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.mHeaderView != null) {
            measureChild(this.mHeaderView, widthMeasureSpec, heightMeasureSpec);
            this.mHeaderViewWidth = this.mHeaderView.getMeasuredWidth();
            this.mHeaderViewHeight = this.mHeaderView.getMeasuredHeight();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        long flatPostion = getExpandableListPosition(getFirstVisiblePosition());
        int groupPos = ExpandableListView.getPackedPositionGroup(flatPostion);
        int childPos = ExpandableListView.getPackedPositionChild(flatPostion);
        int state = this.mAdapter.getPinnedHeaderState(groupPos, childPos);
        if (!(this.mHeaderView == null || this.mAdapter == null || state == this.mOldState)) {
            this.mOldState = state;
            this.mHeaderView.layout(0, 0, this.mHeaderViewWidth, this.mHeaderViewHeight);
        }
        configureHeaderView(groupPos, childPos);
    }

    public void configureHeaderView(int groupPosition, int childPosition) {
        int y;
        int alpha;
        if (this.mHeaderView != null && this.mAdapter != null && ((ExpandableListAdapter) this.mAdapter).getGroupCount() != 0) {
            switch (this.mAdapter.getPinnedHeaderState(groupPosition, childPosition)) {
                case 0:
                    this.mHeaderViewVisible = false;
                    return;
                case 1:
                    this.mAdapter.configurePinnedHeader(this.mHeaderView, groupPosition, childPosition, 255);
                    if (this.mHeaderView.getTop() != 0) {
                        this.mHeaderView.layout(0, 0, this.mHeaderViewWidth, this.mHeaderViewHeight);
                    }
                    this.mHeaderViewVisible = true;
                    return;
                case 2:
                    int bottom = getChildAt(0).getBottom();
                    int headerHeight = this.mHeaderView.getHeight();
                    if (bottom < headerHeight) {
                        y = bottom - headerHeight;
                        alpha = ((headerHeight + y) * 255) / headerHeight;
                    } else {
                        y = 0;
                        alpha = 255;
                    }
                    this.mAdapter.configurePinnedHeader(this.mHeaderView, groupPosition, childPosition, alpha);
                    if (this.mHeaderView.getTop() != y) {
                        this.mHeaderView.layout(0, y, this.mHeaderViewWidth, this.mHeaderViewHeight + y);
                    }
                    this.mHeaderViewVisible = true;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.mHeaderViewVisible) {
            drawChild(canvas, this.mHeaderView, getDrawingTime());
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        long flatPos = getExpandableListPosition(firstVisibleItem);
        configureHeaderView(ExpandableListView.getPackedPositionGroup(flatPos), ExpandableListView.getPackedPositionChild(flatPos));
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    private void registerListener() {
        setOnScrollListener(this);
        setOnGroupClickListener(this);
    }
}
