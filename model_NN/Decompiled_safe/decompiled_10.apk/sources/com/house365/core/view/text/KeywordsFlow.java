package com.house365.core.view.text;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.house365.core.R;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import org.apache.commons.lang.SystemUtils;

public class KeywordsFlow<T> extends FrameLayout implements ViewTreeObserver.OnGlobalLayoutListener {
    public static final int ANIMATION_IN = 1;
    public static final int ANIMATION_OUT = 2;
    public static final long ANIM_DURATION = 800;
    public static final int CENTER_TO_LOCATION = 3;
    public static final int IDX_DIS_Y = 3;
    public static final int IDX_TXT_LENGTH = 2;
    public static final int IDX_X = 0;
    public static final int IDX_Y = 1;
    public static final int LOCATION_TO_CENTER = 4;
    public static final int LOCATION_TO_OUTSIDE = 2;
    public static final int OUTSIDE_TO_LOCATION = 1;
    public static final int TEXT_SIZE_MAX = 25;
    public static final int TEXT_SIZE_MIN = 15;
    private static AlphaAnimation animAlpha2Opaque;
    private static AlphaAnimation animAlpha2Transparent;
    private static ScaleAnimation animScaleLarge2Normal;
    private static ScaleAnimation animScaleNormal2Large;
    private static ScaleAnimation animScaleNormal2Zero;
    private static ScaleAnimation animScaleZero2Normal;
    private static Interpolator interpolator;
    private long animDuration;
    private boolean enableShow;
    private int height;
    private View.OnClickListener itemClickListener;
    private long lastStartAnimationTime;
    public int max = 20;
    private Random random;
    private List<T> tagLists;
    private int[] textBackColors;
    private int[] textColors;
    private int txtAnimInType;
    private int txtAnimOutType;
    private Vector<String> vecKeywords;
    private int width;

    public KeywordsFlow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public KeywordsFlow(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KeywordsFlow(Context context) {
        super(context);
        init();
    }

    public int[] getTextColors() {
        return this.textColors;
    }

    public void setTextColors(int[] textColors2) {
        this.textColors = textColors2;
    }

    public int[] getTextBackColors() {
        return this.textBackColors;
    }

    public void setTextBackColors(int[] textBackColors2) {
        this.textBackColors = textBackColors2;
    }

    public List<T> getTagLists() {
        return this.tagLists;
    }

    public void setTagLists(List<T> tagLists2) {
        this.tagLists = tagLists2;
    }

    private void init() {
        this.lastStartAnimationTime = 0;
        this.animDuration = 800;
        this.random = new Random();
        this.vecKeywords = new Vector<>(this.max);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        interpolator = AnimationUtils.loadInterpolator(getContext(), 17432582);
        animAlpha2Opaque = new AlphaAnimation((float) SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
        animAlpha2Transparent = new AlphaAnimation(1.0f, (float) SystemUtils.JAVA_VERSION_FLOAT);
        animScaleLarge2Normal = new ScaleAnimation(2.0f, 1.0f, 2.0f, 1.0f);
        animScaleNormal2Large = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f);
        animScaleZero2Normal = new ScaleAnimation(SystemUtils.JAVA_VERSION_FLOAT, 1.0f, SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
        animScaleNormal2Zero = new ScaleAnimation(1.0f, SystemUtils.JAVA_VERSION_FLOAT, 1.0f, SystemUtils.JAVA_VERSION_FLOAT);
    }

    public void setMaxShowItem(int max2) {
        this.max = max2;
    }

    public int getMaxShowItem() {
        return this.max;
    }

    public long getDuration() {
        return this.animDuration;
    }

    public void setDuration(long duration) {
        this.animDuration = duration;
    }

    public boolean feedKeyword(String keyword) {
        if (this.vecKeywords.size() < this.max) {
            return this.vecKeywords.add(keyword);
        }
        return false;
    }

    public boolean go2Show(int animType) {
        if (System.currentTimeMillis() - this.lastStartAnimationTime <= this.animDuration) {
            return false;
        }
        this.enableShow = true;
        if (animType == 1) {
            this.txtAnimInType = 1;
            this.txtAnimOutType = 4;
        } else if (animType == 2) {
            this.txtAnimInType = 3;
            this.txtAnimOutType = 2;
        }
        disapper();
        return show();
    }

    private void disapper() {
        for (int i = getChildCount() - 1; i >= 0; i--) {
            final TextView txt = (TextView) getChildAt(i);
            if (txt.getVisibility() == 8) {
                removeView(txt);
            } else {
                FrameLayout.LayoutParams layParams = (FrameLayout.LayoutParams) txt.getLayoutParams();
                AnimationSet animSet = getAnimationSet(new int[]{layParams.leftMargin, layParams.topMargin, txt.getWidth()}, this.width >> 1, this.height >> 1, this.txtAnimOutType);
                txt.startAnimation(animSet);
                animSet.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        txt.setOnClickListener(null);
                        txt.setClickable(false);
                        txt.setVisibility(8);
                    }
                });
            }
        }
    }

    private boolean show() {
        int ranColor;
        if (this.width <= 0 || this.height <= 0 || this.vecKeywords == null || this.vecKeywords.size() <= 0 || !this.enableShow) {
            return false;
        }
        this.enableShow = false;
        this.lastStartAnimationTime = System.currentTimeMillis();
        int xCenter = this.width >> 1;
        int yCenter = this.height >> 1;
        int size = this.vecKeywords.size();
        int xItem = this.width / size;
        int yItem = this.height / size;
        Log.d("ANDROID_LAB", "--------------------------width=" + this.width + " height=" + this.height + "  xItem=" + xItem + " yItem=" + yItem + "---------------------------");
        LinkedList<Integer> listX = new LinkedList<>();
        LinkedList<Integer> listY = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            listX.add(Integer.valueOf(i * xItem));
            listY.add(Integer.valueOf((i * yItem) + (yItem >> 2)));
        }
        LinkedList<TextView> listTxtTop = new LinkedList<>();
        LinkedList<TextView> listTxtBottom = new LinkedList<>();
        int tempNum = 0;
        for (int i2 = 0; i2 < size; i2++) {
            String keyword = this.vecKeywords.get(i2);
            int ranBackColor = 0;
            if (this.textColors == null || this.textColors.length <= 0) {
                ranColor = -16777216 | this.random.nextInt(7864319);
            } else {
                int ranNum = this.random.nextInt(this.textColors.length);
                while (tempNum == ranNum) {
                    ranNum = this.random.nextInt(this.textColors.length);
                }
                tempNum = ranNum;
                ranColor = this.textColors[ranNum];
                if (this.textBackColors != null && this.textBackColors.length > 0) {
                    ranBackColor = this.textBackColors[ranNum];
                }
            }
            int[] xy = randomXY(this.random, listX, listY, xItem);
            int nextInt = this.random.nextInt(11) + 15;
            TextView textView = new TextView(getContext());
            textView.setOnClickListener(this.itemClickListener);
            textView.setText(keyword);
            if (this.textColors == null || this.textColors.length <= 0) {
                textView.setTextColor(ranColor);
            } else {
                textView.setTextColor(getResources().getColor(ranColor));
            }
            textView.setTextSize(17.0f);
            textView.setGravity(17);
            if (ranBackColor != 0) {
                textView.setBackgroundDrawable(getResources().getDrawable(ranBackColor));
            }
            int strWidth = (int) Math.ceil((double) textView.getPaint().measureText(keyword));
            xy[2] = strWidth;
            if (xy[0] + strWidth > this.width - (xItem >> 1)) {
                xy[0] = ((this.width - strWidth) - xItem) + this.random.nextInt(xItem >> 1);
            } else if (xy[0] == 0) {
                xy[0] = Math.max(this.random.nextInt(xItem), xItem / 3);
            }
            xy[3] = Math.abs(xy[1] - yCenter);
            if (getTagLists() != null && getTagLists().size() > 0) {
                textView.setTag(getTagLists().get(i2));
            }
            textView.setTag(R.string.keywordsflow_tag, xy);
            if (xy[1] > yCenter) {
                listTxtBottom.add(textView);
            } else {
                listTxtTop.add(textView);
            }
        }
        attach2Screen(listTxtTop, xCenter, yCenter, yItem);
        attach2Screen(listTxtBottom, xCenter, yCenter, yItem);
        return true;
    }

    private void attach2Screen(LinkedList<TextView> listTxt, int xCenter, int yCenter, int yItem) {
        int size = listTxt.size();
        sortXYList(listTxt, size);
        for (int i = 0; i < size; i++) {
            TextView txt = listTxt.get(i);
            int[] iXY = (int[]) txt.getTag(R.string.keywordsflow_tag);
            int yDistance = iXY[1] - yCenter;
            int yMove = Math.abs(yDistance);
            int k = i - 1;
            while (true) {
                if (k < 0) {
                    break;
                }
                int[] kXY = (int[]) listTxt.get(k).getTag(R.string.keywordsflow_tag);
                int startX = kXY[0];
                int endX = startX + kXY[2];
                if ((kXY[1] - yCenter) * yDistance > 0) {
                    if (isXMixed(startX, endX, iXY[0], iXY[0] + iXY[2])) {
                        int tmpMove = Math.abs(iXY[1] - kXY[1]);
                        if (tmpMove > yItem) {
                            yMove = tmpMove;
                        } else if (yMove > 0) {
                            yMove = 0;
                        }
                    }
                }
                k--;
            }
            if (yMove > yItem) {
                int maxMove = yMove - yItem;
                iXY[1] = iXY[1] - ((Math.max(this.random.nextInt(maxMove), maxMove >> 1) * yDistance) / Math.abs(yDistance));
                iXY[3] = Math.abs(iXY[1] - yCenter);
                sortXYList(listTxt, i + 1);
            }
            FrameLayout.LayoutParams layParams = new FrameLayout.LayoutParams(-2, -2);
            layParams.gravity = 51;
            layParams.leftMargin = iXY[0];
            layParams.topMargin = iXY[1];
            addView(txt, layParams);
            txt.startAnimation(getAnimationSet(iXY, xCenter, yCenter, this.txtAnimInType));
        }
    }

    public AnimationSet getAnimationSet(int[] xy, int xCenter, int yCenter, int type) {
        AnimationSet animSet = new AnimationSet(true);
        animSet.setInterpolator(interpolator);
        if (type == 1) {
            animSet.addAnimation(animAlpha2Opaque);
            animSet.addAnimation(animScaleLarge2Normal);
            animSet.addAnimation(new TranslateAnimation((float) (((xy[0] + (xy[2] >> 1)) - xCenter) << 1), SystemUtils.JAVA_VERSION_FLOAT, (float) ((xy[1] - yCenter) << 1), SystemUtils.JAVA_VERSION_FLOAT));
        } else if (type == 2) {
            animSet.addAnimation(animAlpha2Transparent);
            animSet.addAnimation(animScaleNormal2Large);
            animSet.addAnimation(new TranslateAnimation(SystemUtils.JAVA_VERSION_FLOAT, (float) (((xy[0] + (xy[2] >> 1)) - xCenter) << 1), SystemUtils.JAVA_VERSION_FLOAT, (float) ((xy[1] - yCenter) << 1)));
        } else if (type == 4) {
            animSet.addAnimation(animAlpha2Transparent);
            animSet.addAnimation(animScaleNormal2Zero);
            animSet.addAnimation(new TranslateAnimation(SystemUtils.JAVA_VERSION_FLOAT, (float) ((-xy[0]) + xCenter), SystemUtils.JAVA_VERSION_FLOAT, (float) ((-xy[1]) + yCenter)));
        } else if (type == 3) {
            animSet.addAnimation(animAlpha2Opaque);
            animSet.addAnimation(animScaleZero2Normal);
            animSet.addAnimation(new TranslateAnimation((float) ((-xy[0]) + xCenter), SystemUtils.JAVA_VERSION_FLOAT, (float) ((-xy[1]) + yCenter), SystemUtils.JAVA_VERSION_FLOAT));
        }
        animSet.setDuration(this.animDuration);
        return animSet;
    }

    private void sortXYList(LinkedList<TextView> listTxt, int endIdx) {
        for (int i = 0; i < endIdx; i++) {
            for (int k = i + 1; k < endIdx; k++) {
                if (((int[]) listTxt.get(k).getTag(R.string.keywordsflow_tag))[3] < ((int[]) listTxt.get(i).getTag(R.string.keywordsflow_tag))[3]) {
                    listTxt.set(i, listTxt.get(k));
                    listTxt.set(k, listTxt.get(i));
                }
            }
        }
    }

    private boolean isXMixed(int startA, int endA, int startB, int endB) {
        if (startB >= startA && startB <= endA) {
            return true;
        }
        if (endB >= startA && endB <= endA) {
            return true;
        }
        if (startA >= startB && startA <= endB) {
            return true;
        }
        if (endA < startB || endA > endB) {
            return false;
        }
        return true;
    }

    private int[] randomXY(Random ran, LinkedList<Integer> listX, LinkedList<Integer> listY, int xItem) {
        int[] arr = new int[4];
        arr[0] = listX.remove(ran.nextInt(listX.size())).intValue();
        arr[1] = listY.remove(ran.nextInt(listY.size())).intValue();
        return arr;
    }

    public void onGlobalLayout() {
        int tmpW = getWidth();
        int tmpH = getHeight();
        if (this.width != tmpW || this.height != tmpH) {
            this.width = tmpW;
            this.height = tmpH;
            show();
        }
    }

    public Vector<String> getKeywords() {
        return this.vecKeywords;
    }

    public void rubKeywords() {
        this.vecKeywords.clear();
    }

    public void rubAllViews() {
        removeAllViews();
    }

    public void setOnItemClickListener(View.OnClickListener listener) {
        this.itemClickListener = listener;
    }
}
