package com.house365.core.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.R;

public class ListFooterView extends RelativeLayout {
    private ProgressBar progressbar;
    private TextView textView;

    public ListFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public ListFooterView(Context context) {
        super(context);
        initialize(context);
    }

    private void initialize(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.pull_to_item_footer, (ViewGroup) null);
        this.textView = (TextView) view.findViewById(R.id.more);
        this.progressbar = (ProgressBar) view.findViewById(R.id.more_progress);
        addView(view);
    }

    public void setTextView(int resid) {
        this.textView.setText(resid);
    }

    public void setPBGone() {
        this.progressbar.setVisibility(8);
    }

    public void setPBVisible() {
        this.progressbar.setVisibility(0);
    }
}
