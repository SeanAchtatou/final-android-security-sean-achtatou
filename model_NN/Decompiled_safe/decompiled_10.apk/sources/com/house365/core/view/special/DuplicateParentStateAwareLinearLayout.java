package com.house365.core.view.special;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class DuplicateParentStateAwareLinearLayout extends LinearLayout {
    public DuplicateParentStateAwareLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DuplicateParentStateAwareLinearLayout(Context context) {
        super(context);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void dispatchSetPressed(boolean pressed) {
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i).isDuplicateParentStateEnabled()) {
                getChildAt(i).setPressed(pressed);
            }
        }
    }
}
