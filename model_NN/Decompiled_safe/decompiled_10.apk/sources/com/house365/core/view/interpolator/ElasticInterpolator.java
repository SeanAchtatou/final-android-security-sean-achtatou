package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class ElasticInterpolator implements Interpolator {
    private float amplitude;
    private float period;
    private EasingType.Type type;

    public ElasticInterpolator(EasingType.Type type2, float amplitude2, float period2) {
        this.type = type2;
        this.amplitude = amplitude2;
        this.period = period2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t, this.amplitude, this.period);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t, this.amplitude, this.period);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t, this.amplitude, this.period);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float in(float t, float a, float p) {
        float s;
        if (t == SystemUtils.JAVA_VERSION_FLOAT) {
            return SystemUtils.JAVA_VERSION_FLOAT;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == SystemUtils.JAVA_VERSION_FLOAT) {
            p = 0.3f;
        }
        if (a == SystemUtils.JAVA_VERSION_FLOAT || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) ((((double) p) / 6.283185307179586d) * Math.asin((double) (1.0f / a)));
        }
        float t2 = t - 1.0f;
        return (float) (-(Math.pow(2.0d, (double) (10.0f * t2)) * ((double) a) * Math.sin((((double) (t2 - s)) * 6.283185307179586d) / ((double) p))));
    }

    private float out(float t, float a, float p) {
        float s;
        if (t == SystemUtils.JAVA_VERSION_FLOAT) {
            return SystemUtils.JAVA_VERSION_FLOAT;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == SystemUtils.JAVA_VERSION_FLOAT) {
            p = 0.3f;
        }
        if (a == SystemUtils.JAVA_VERSION_FLOAT || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) (Math.asin((double) (1.0f / a)) * (((double) p) / 6.283185307179586d));
        }
        return (float) ((((double) a) * Math.pow(2.0d, (double) (-10.0f * t)) * Math.sin((((double) (t - s)) * 6.283185307179586d) / ((double) p))) + 1.0d);
    }

    private float inout(float t, float a, float p) {
        float s;
        if (t == SystemUtils.JAVA_VERSION_FLOAT) {
            return SystemUtils.JAVA_VERSION_FLOAT;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        if (p == SystemUtils.JAVA_VERSION_FLOAT) {
            p = 0.45000002f;
        }
        if (a == SystemUtils.JAVA_VERSION_FLOAT || a < 1.0f) {
            a = 1.0f;
            s = p / 4.0f;
        } else {
            s = (float) ((((double) p) / 6.283185307179586d) * Math.asin((double) (1.0f / a)));
        }
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            float t3 = t2 - 1.0f;
            return (float) (Math.pow(2.0d, (double) (10.0f * t3)) * ((double) a) * Math.sin((((double) (t3 - s)) * 6.283185307179586d) / ((double) p)) * -0.5d);
        }
        float t4 = t2 - 1.0f;
        return (float) ((Math.pow(2.0d, (double) (-10.0f * t4)) * ((double) a) * Math.sin((((double) (t4 - s)) * 6.283185307179586d) / ((double) p)) * 0.5d) + 1.0d);
    }
}
