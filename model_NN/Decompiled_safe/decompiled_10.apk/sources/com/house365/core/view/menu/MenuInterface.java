package com.house365.core.view.menu;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

public abstract class MenuInterface {
    public abstract void drawFade(Canvas canvas, int i, CustomViewBehind customViewBehind, View view);

    public abstract void drawShadow(Canvas canvas, Drawable drawable, int i);

    public abstract int getAbsLeftBound(CustomViewBehind customViewBehind, View view);

    public abstract int getAbsRightBound(CustomViewBehind customViewBehind, View view);

    public abstract int getMenuLeft(CustomViewBehind customViewBehind, View view);

    public abstract boolean marginTouchAllowed(View view, int i, int i2);

    public abstract boolean menuClosedSlideAllowed(int i);

    public abstract boolean menuOpenSlideAllowed(int i);

    public abstract boolean menuOpenTouchAllowed(View view, int i);

    public abstract void scrollBehindTo(int i, int i2, CustomViewBehind customViewBehind, float f);
}
