package com.house365.core.view.common;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.Scroller;

public class ScrollerView extends LinearLayout {
    Scroller mScroller;

    public ScrollerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mScroller = new Scroller(context);
    }

    public ScrollerView(Context context) {
        super(context);
        this.mScroller = new Scroller(context);
    }

    public void beginScroll(int x, int y, int duration) {
        this.mScroller.startScroll(getScrollX(), getScrollY(), x, y, duration);
        invalidate();
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        }
    }
}
