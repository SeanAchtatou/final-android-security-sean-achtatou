package com.house365.core.view.wheel.widget;

public interface WheelAdapter {
    String getItem(int i);

    int getItemsCount();

    int getMaximumLength();
}
