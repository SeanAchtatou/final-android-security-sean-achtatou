package com.house365.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.house365.core.R;

public class Switch extends ImageView {
    private static final int TEXT_SIZE = 12;
    private String checkedString;
    private boolean drawText;
    private boolean mChecked;
    private OnCheckedChangeListener mOnCheckedChangeListener;
    private Paint mPaint;
    private int offImage;
    private int onImage;
    private String unCheckedString;

    public interface OnCheckedChangeListener {
        void onCheckedChanged(Switch switchR, boolean z);
    }

    public Switch(Context context) {
        this(context, null);
    }

    public Switch(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Switch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.drawText = false;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Switch);
        this.onImage = a.getResourceId(0, -1);
        this.offImage = a.getResourceId(1, -1);
        this.drawText = a.getBoolean(2, false);
        if (this.drawText) {
            this.checkedString = context.getResources().getString(a.getResourceId(3, -1));
            this.unCheckedString = context.getResources().getString(a.getResourceId(4, -1));
        }
        a.recycle();
        setChecked(false);
        configPaint();
    }

    private void configPaint() {
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setTextSize(12.0f);
        this.mPaint.setColor(-1);
    }

    public boolean isDrawText() {
        return this.drawText;
    }

    public void setDrawText(boolean drawText2) {
        this.drawText = drawText2;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
        if (this.mChecked) {
            setImageResource(this.onImage);
            setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            setImageResource(this.offImage);
            setScaleType(ImageView.ScaleType.FIT_XY);
        }
        postInvalidate();
    }

    public boolean isChecked() {
        return this.mChecked;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.drawText) {
            return;
        }
        if (this.mChecked) {
            canvas.drawText(this.checkedString, ((float) getWidth()) * 0.6f, ((float) getHeight()) * 0.7f, this.mPaint);
        } else {
            canvas.drawText(this.unCheckedString, ((float) getWidth()) * 0.1f, ((float) getHeight()) * 0.7f, this.mPaint);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean z;
        switch (event.getAction()) {
            case 1:
                if (this.mChecked) {
                    z = false;
                } else {
                    z = true;
                }
                setChecked(z);
                if (this.mOnCheckedChangeListener != null) {
                    this.mOnCheckedChangeListener.onCheckedChanged(this, this.mChecked);
                    break;
                }
                break;
        }
        return true;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        this.mOnCheckedChangeListener = listener;
    }
}
