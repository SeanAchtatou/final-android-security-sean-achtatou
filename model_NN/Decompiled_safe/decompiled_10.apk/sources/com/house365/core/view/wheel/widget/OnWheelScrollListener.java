package com.house365.core.view.wheel.widget;

public interface OnWheelScrollListener {
    void onScrollingFinished(WheelView wheelView);

    void onScrollingStarted(WheelView wheelView);
}
