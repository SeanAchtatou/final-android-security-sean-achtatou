package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class ExpoInterpolator implements Interpolator {
    private EasingType.Type type;

    public ExpoInterpolator(EasingType.Type type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float in(float t) {
        return (float) (t == SystemUtils.JAVA_VERSION_FLOAT ? 0.0d : Math.pow(2.0d, (double) (10.0f * (t - 1.0f))));
    }

    private float out(float t) {
        double d = 1.0d;
        if (t < 1.0f) {
            d = 1.0d + (-Math.pow(2.0d, (double) (-10.0f * t)));
        }
        return (float) d;
    }

    private float inout(float t) {
        if (t == SystemUtils.JAVA_VERSION_FLOAT) {
            return SystemUtils.JAVA_VERSION_FLOAT;
        }
        if (t >= 1.0f) {
            return 1.0f;
        }
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return (float) (Math.pow(2.0d, (double) (10.0f * (t2 - 1.0f))) * 0.5d);
        }
        return (float) (((-Math.pow(2.0d, (double) (-10.0f * (t2 - 1.0f)))) + 2.0d) * 0.5d);
    }
}
