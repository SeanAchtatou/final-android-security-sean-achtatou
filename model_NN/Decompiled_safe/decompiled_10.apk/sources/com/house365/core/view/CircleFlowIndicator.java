package com.house365.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.house365.core.R;
import org.apache.commons.lang.SystemUtils;

@Deprecated
public class CircleFlowIndicator extends View implements FlowIndicator, Animation.AnimationListener {
    private static final int STYLE_FILL = 1;
    private static final int STYLE_STROKE = 0;
    private float activeRadius = 0.5f;
    /* access modifiers changed from: private */
    public Animation animation;
    public Animation.AnimationListener animationListener = this;
    private float circleSeparation = ((2.0f * this.radius) + this.radius);
    private int currentScroll = 0;
    /* access modifiers changed from: private */
    public int fadeOutTime = 0;
    private int flowWidth = 0;
    private boolean mCentered = false;
    private final Paint mPaintActive = new Paint(1);
    private final Paint mPaintInactive = new Paint(1);
    private float radius = 4.0f;
    private FadeTimer timer;
    private ViewFlow viewFlow;

    public CircleFlowIndicator(Context context) {
        super(context);
        initColors(-1, -1, 1, 0);
    }

    public CircleFlowIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleFlowIndicator);
        int activeType = a.getInt(6, 1);
        int activeColor = a.getColor(2, 16777215);
        int inactiveType = a.getInt(5, 0);
        int inactiveColor = a.getColor(3, 1157627903);
        this.radius = a.getDimension(1, 4.0f);
        this.circleSeparation = a.getDimension(7, (2.0f * this.radius) + this.radius);
        this.activeRadius = a.getDimension(8, 0.5f);
        this.fadeOutTime = a.getInt(4, 0);
        this.mCentered = a.getBoolean(0, false);
        initColors(activeColor, inactiveColor, activeType, inactiveType);
    }

    private void initColors(int activeColor, int inactiveColor, int activeType, int inactiveType) {
        switch (inactiveType) {
            case 1:
                this.mPaintInactive.setStyle(Paint.Style.FILL);
                break;
            default:
                this.mPaintInactive.setStyle(Paint.Style.STROKE);
                break;
        }
        this.mPaintInactive.setColor(inactiveColor);
        switch (activeType) {
            case 0:
                this.mPaintActive.setStyle(Paint.Style.STROKE);
                break;
            default:
                this.mPaintActive.setStyle(Paint.Style.FILL);
                break;
        }
        this.mPaintActive.setColor(activeColor);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int count = 3;
        if (this.viewFlow != null) {
            count = this.viewFlow.getViewsCount();
        }
        int leftPadding = getPaddingLeft();
        for (int iLoop = 0; iLoop < count; iLoop++) {
            canvas.drawCircle(((float) leftPadding) + this.radius + (((float) iLoop) * this.circleSeparation) + SystemUtils.JAVA_VERSION_FLOAT, ((float) getPaddingTop()) + this.radius, this.radius, this.mPaintInactive);
        }
        float cx = SystemUtils.JAVA_VERSION_FLOAT;
        if (this.flowWidth != 0) {
            cx = (((float) this.currentScroll) * this.circleSeparation) / ((float) this.flowWidth);
        }
        canvas.drawCircle(((float) leftPadding) + this.radius + cx + SystemUtils.JAVA_VERSION_FLOAT, ((float) getPaddingTop()) + this.radius, this.radius + this.activeRadius, this.mPaintActive);
    }

    public void onSwitched(View view, int position) {
    }

    public void setViewFlow(ViewFlow view) {
        resetTimer();
        this.viewFlow = view;
        this.flowWidth = this.viewFlow.getWidth();
        invalidate();
    }

    public void onScrolled(int h, int v, int oldh, int oldv) {
        setVisibility(0);
        resetTimer();
        this.flowWidth = this.viewFlow.getWidth();
        if (this.viewFlow.getViewsCount() * this.flowWidth != 0) {
            this.currentScroll = h % (this.viewFlow.getViewsCount() * this.flowWidth);
        } else {
            this.currentScroll = h;
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int count = 3;
        if (this.viewFlow != null) {
            count = this.viewFlow.getViewsCount();
        }
        int result = (int) (((float) (getPaddingLeft() + getPaddingRight())) + (((float) (count * 2)) * this.radius) + (((float) (count - 1)) * (this.circleSeparation - (2.0f * this.radius))) + 1.0f);
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = (int) ((2.0f * this.radius) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    public void setFillColor(int color) {
        this.mPaintActive.setColor(color);
        invalidate();
    }

    public void setStrokeColor(int color) {
        this.mPaintInactive.setColor(color);
        invalidate();
    }

    private void resetTimer() {
        if (this.fadeOutTime <= 0) {
            return;
        }
        if (this.timer == null || !this.timer._run) {
            this.timer = new FadeTimer(this, null);
            this.timer.execute(new Void[0]);
            return;
        }
        this.timer.resetTimer();
    }

    private class FadeTimer extends AsyncTask<Void, Void, Void> {
        /* access modifiers changed from: private */
        public boolean _run;
        private int timer;

        private FadeTimer() {
            this.timer = 0;
            this._run = true;
        }

        /* synthetic */ FadeTimer(CircleFlowIndicator circleFlowIndicator, FadeTimer fadeTimer) {
            this();
        }

        public void resetTimer() {
            this.timer = 0;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            while (this._run) {
                try {
                    Thread.sleep(1);
                    this.timer++;
                    if (this.timer == CircleFlowIndicator.this.fadeOutTime) {
                        this._run = false;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            CircleFlowIndicator.this.animation = AnimationUtils.loadAnimation(CircleFlowIndicator.this.getContext(), 17432577);
            CircleFlowIndicator.this.animation.setAnimationListener(CircleFlowIndicator.this.animationListener);
            CircleFlowIndicator.this.startAnimation(CircleFlowIndicator.this.animation);
        }
    }

    public void onAnimationEnd(Animation animation2) {
        setVisibility(8);
    }

    public void onAnimationRepeat(Animation animation2) {
    }

    public void onAnimationStart(Animation animation2) {
    }
}
