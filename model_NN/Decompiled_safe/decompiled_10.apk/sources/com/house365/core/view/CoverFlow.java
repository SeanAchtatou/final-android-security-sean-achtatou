package com.house365.core.view;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;
import android.widget.ImageView;
import org.apache.commons.lang.SystemUtils;

public class CoverFlow extends Gallery {
    private boolean mAlphaMode = true;
    private Camera mCamera = new Camera();
    private boolean mCircleMode = false;
    private int mCoveflowCenter;
    private int mMaxRotationAngle = 50;
    private int mMaxZoom = -500;

    public CoverFlow(Context context) {
        super(context);
        setStaticTransformationsEnabled(true);
    }

    public CoverFlow(Context context, AttributeSet attrs) {
        super(context, attrs);
        setStaticTransformationsEnabled(true);
    }

    public CoverFlow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setStaticTransformationsEnabled(true);
    }

    public int getMaxRotationAngle() {
        return this.mMaxRotationAngle;
    }

    public void setMaxRotationAngle(int maxRotationAngle) {
        this.mMaxRotationAngle = maxRotationAngle;
    }

    public boolean getCircleMode() {
        return this.mCircleMode;
    }

    public void setCircleMode(boolean isCircle) {
        this.mCircleMode = isCircle;
    }

    public boolean getAlphaMode() {
        return this.mAlphaMode;
    }

    public void setAlphaMode(boolean isAlpha) {
        this.mAlphaMode = isAlpha;
    }

    public int getMaxZoom() {
        return this.mMaxZoom;
    }

    public void setMaxZoom(int maxZoom) {
        this.mMaxZoom = maxZoom;
    }

    private int getCenterOfCoverflow() {
        return (((getWidth() - getPaddingLeft()) - getPaddingRight()) / 2) + getPaddingLeft();
    }

    private static int getCenterOfView(View view) {
        return view.getLeft() + (view.getWidth() / 2);
    }

    /* access modifiers changed from: protected */
    public boolean getChildStaticTransformation(View child, Transformation t) {
        int childCenter = getCenterOfView(child);
        int childWidth = child.getWidth();
        t.clear();
        t.setTransformationType(Transformation.TYPE_MATRIX);
        if (childCenter == this.mCoveflowCenter) {
            transformImageBitmap((ImageView) child, t, 0, 0);
        } else {
            int rotationAngle = (int) ((((float) (this.mCoveflowCenter - childCenter)) / ((float) childWidth)) * ((float) this.mMaxRotationAngle));
            if (Math.abs(rotationAngle) > this.mMaxRotationAngle) {
                if (rotationAngle < 0) {
                    rotationAngle = -this.mMaxRotationAngle;
                } else {
                    rotationAngle = this.mMaxRotationAngle;
                }
            }
            ImageView imageView = (ImageView) child;
            int i = this.mCoveflowCenter - childCenter;
            if (childWidth == 0) {
                childWidth = 1;
            }
            transformImageBitmap(imageView, t, rotationAngle, (int) Math.floor((double) (i / childWidth)));
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.mCoveflowCenter = getCenterOfCoverflow();
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void transformImageBitmap(ImageView child, Transformation t, int rotationAngle, int d) {
        this.mCamera.save();
        Matrix imageMatrix = t.getMatrix();
        int imageHeight = child.getLayoutParams().height;
        int imageWidth = child.getLayoutParams().width;
        int rotation = Math.abs(rotationAngle);
        this.mCamera.translate(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, 100.0f);
        if (rotation <= this.mMaxRotationAngle) {
            this.mCamera.translate(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, (float) (((double) this.mMaxZoom) + (((double) rotation) * 1.5d)));
            if (this.mCircleMode) {
                if (rotation < 40) {
                    this.mCamera.translate(SystemUtils.JAVA_VERSION_FLOAT, 155.0f, SystemUtils.JAVA_VERSION_FLOAT);
                } else {
                    this.mCamera.translate(SystemUtils.JAVA_VERSION_FLOAT, 255.0f - (((float) rotation) * 2.5f), SystemUtils.JAVA_VERSION_FLOAT);
                }
            }
            if (this.mAlphaMode) {
                child.setAlpha((int) (255.0d - (((double) rotation) * 2.5d)));
            }
        }
        this.mCamera.rotateY((float) rotationAngle);
        this.mCamera.getMatrix(imageMatrix);
        imageMatrix.preTranslate((float) (-(imageWidth / 2)), (float) (-(imageHeight / 2)));
        imageMatrix.postTranslate((float) (imageWidth / 2), (float) (imageHeight / 2));
        this.mCamera.restore();
    }
}
