package com.house365.core.view.wheel.widget;

public interface OnWheelClickedListener {
    void onItemClicked(WheelView wheelView, int i);
}
