package com.house365.core.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.house365.core.R;
import com.house365.core.view.wheel.widget.OnWheelChangedListener;
import com.house365.core.view.wheel.widget.WheelView;
import com.house365.core.view.wheel.widget.adapters.ArrayWheelAdapter;
import com.house365.core.view.wheel.widget.adapters.WheelViewAdapter;

public class WheelDialog extends Dialog {
    private WheelView wheelView;

    public interface OnClickListener {
        void onClick(WheelDialog wheelDialog, int i);
    }

    public WheelDialog(Context context) {
        super(context);
    }

    public WheelDialog(Context context, int theme) {
        super(context, theme);
    }

    public WheelView getWheelView() {
        return this.wheelView;
    }

    public void setWheelView(WheelView wheelView2) {
        this.wheelView = wheelView2;
    }

    public void setWheelAdapter(ArrayWheelAdapter wheelAdapter) {
        this.wheelView.setViewAdapter(wheelAdapter);
    }

    public static class Builder {
        private Context context;
        /* access modifiers changed from: private */
        public int idx = -1;
        private String message;
        /* access modifiers changed from: private */
        public OnClickListener negativeButtonClickListener;
        private String negativeButtonText;
        /* access modifiers changed from: private */
        public OnClickListener positiveButtonClickListener;
        private String positiveButtonText;
        private String title;
        private WheelViewAdapter wheelAdapter;

        public Builder(Context context2) {
            this.context = context2;
        }

        public Builder setMessage(int message2) {
            this.message = (String) this.context.getText(message2);
            return this;
        }

        public Builder setTitle(int title2) {
            this.title = (String) this.context.getText(title2);
            return this;
        }

        public Builder setTitle(String title2) {
            this.title = title2;
            return this;
        }

        public Builder setWheelAdapter(WheelViewAdapter wheelAdapter2) {
            this.wheelAdapter = wheelAdapter2;
            return this;
        }

        public Builder setPositiveButton(int positiveButtonText2, OnClickListener listener) {
            this.positiveButtonText = (String) this.context.getText(positiveButtonText2);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText2, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText2;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText2, OnClickListener listener) {
            this.negativeButtonText = (String) this.context.getText(negativeButtonText2);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText2, OnClickListener listener) {
            this.negativeButtonText = negativeButtonText2;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public WheelDialog create() {
            final WheelDialog dialog = new WheelDialog(this.context, R.style.dialog);
            dialog.getWindow().setGravity(81);
            View contentlayout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.dialog_wheel, (ViewGroup) null);
            dialog.addContentView(contentlayout, new ViewGroup.LayoutParams(-1, -2));
            WheelView wheelView = (WheelView) contentlayout.findViewById(R.id.wheelView);
            wheelView.setViewAdapter(this.wheelAdapter);
            wheelView.setVisibleItems(3);
            wheelView.setCurrentItem(0);
            wheelView.addChangingListener(new OnWheelChangedListener() {
                public void onChanged(WheelView wheel, int oldValue, int newValue) {
                    Builder.this.idx = newValue;
                }
            });
            ((TextView) contentlayout.findViewById(R.id.dialog_title)).setText(this.title);
            if (this.positiveButtonText != null) {
                ((Button) contentlayout.findViewById(R.id.dialog_button_ok)).setText(this.positiveButtonText);
                if (this.positiveButtonClickListener != null) {
                    ((Button) contentlayout.findViewById(R.id.dialog_button_ok)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.positiveButtonClickListener.onClick(dialog, Builder.this.idx);
                        }
                    });
                }
            } else {
                contentlayout.findViewById(R.id.dialog_button_ok).setVisibility(8);
            }
            if (this.negativeButtonText != null) {
                ((Button) contentlayout.findViewById(R.id.dialog_button_cancel)).setText(this.negativeButtonText);
                if (this.negativeButtonClickListener != null) {
                    ((Button) contentlayout.findViewById(R.id.dialog_button_cancel)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.negativeButtonClickListener.onClick(dialog, Builder.this.idx);
                        }
                    });
                }
            } else {
                contentlayout.findViewById(R.id.dialog_button_cancel).setVisibility(8);
            }
            if (this.message != null) {
                ((TextView) contentlayout.findViewById(R.id.dialog_message)).setText(this.message);
            }
            dialog.setContentView(contentlayout);
            return dialog;
        }
    }
}
