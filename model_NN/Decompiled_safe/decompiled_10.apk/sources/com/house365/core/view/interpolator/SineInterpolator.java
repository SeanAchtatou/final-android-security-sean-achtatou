package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class SineInterpolator implements Interpolator {
    private EasingType.Type type;

    public SineInterpolator(EasingType.Type type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float in(float t) {
        return (float) ((-Math.cos(((double) t) * 1.5707963267948966d)) + 1.0d);
    }

    private float out(float t) {
        return (float) Math.sin(((double) t) * 1.5707963267948966d);
    }

    private float inout(float t) {
        return (float) (-0.5d * (Math.cos(3.141592653589793d * ((double) t)) - 1.0d));
    }
}
