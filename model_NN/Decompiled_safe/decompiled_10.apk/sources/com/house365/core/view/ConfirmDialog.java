package com.house365.core.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.R;
import com.house365.core.image.ImageUtil;

public class ConfirmDialog extends Dialog {
    public ConfirmDialog(Context context) {
        super(context);
    }

    public ConfirmDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String message;
        private String messagePic;
        private Bitmap messagePicBitmap;
        private int messagePicHeight;
        private int messagePicWidth;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener negativeButtonClickListener;
        private String negativeButtonText;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener positiveButtonClickListener;
        private String positiveButtonText;
        private String title;

        public Builder(Context context2) {
            this.context = context2;
        }

        public Builder setMessage(String message2) {
            this.message = message2;
            return this;
        }

        public Builder setMessagePic(String pic) {
            this.messagePic = pic;
            return this;
        }

        public Builder setMessagePic(String pic, int messagePicWidth2, int messagePicHeight2) {
            this.messagePic = pic;
            this.messagePicWidth = messagePicWidth2;
            this.messagePicHeight = messagePicHeight2;
            return this;
        }

        public Bitmap getMessagePicBitmap() {
            return this.messagePicBitmap;
        }

        public void setMessagePicBitmap(Bitmap messagePicBitmap2) {
            this.messagePicBitmap = messagePicBitmap2;
        }

        public Builder setMessagePicBitmap(Bitmap messagePicBitmap2, int messagePicWidth2, int messagePicHeight2) {
            this.messagePicBitmap = messagePicBitmap2;
            this.messagePicWidth = messagePicWidth2;
            this.messagePicHeight = messagePicHeight2;
            return this;
        }

        public Builder setMessage(int message2) {
            this.message = (String) this.context.getText(message2);
            return this;
        }

        public Builder setTitle(int title2) {
            this.title = (String) this.context.getText(title2);
            return this;
        }

        public Builder setTitle(String title2) {
            this.title = title2;
            return this;
        }

        public Builder setContentView(View v) {
            return this;
        }

        public Builder setPositiveButton(int positiveButtonText2, DialogInterface.OnClickListener listener) {
            this.positiveButtonText = (String) this.context.getText(positiveButtonText2);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText2, DialogInterface.OnClickListener listener) {
            this.positiveButtonText = positiveButtonText2;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText2, DialogInterface.OnClickListener listener) {
            this.negativeButtonText = (String) this.context.getText(negativeButtonText2);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText2, DialogInterface.OnClickListener listener) {
            this.negativeButtonText = negativeButtonText2;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public ConfirmDialog create() {
            final ConfirmDialog dialog = new ConfirmDialog(this.context, R.style.dialog);
            View layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.dialog_confirm, (ViewGroup) null);
            dialog.addContentView(layout, new WindowManager.LayoutParams(-1, -2));
            ((TextView) layout.findViewById(R.id.dialog_title)).setText(this.title);
            if (this.positiveButtonText != null) {
                ((Button) layout.findViewById(R.id.dialog_button_ok)).setText(this.positiveButtonText);
                if (this.positiveButtonClickListener != null) {
                    ((Button) layout.findViewById(R.id.dialog_button_ok)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.positiveButtonClickListener.onClick(dialog, -1);
                        }
                    });
                }
            } else {
                layout.findViewById(R.id.dialog_button_ok).setVisibility(8);
            }
            if (this.negativeButtonText != null) {
                ((Button) layout.findViewById(R.id.dialog_button_cancel)).setText(this.negativeButtonText);
                if (this.negativeButtonClickListener != null) {
                    ((Button) layout.findViewById(R.id.dialog_button_cancel)).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Builder.this.negativeButtonClickListener.onClick(dialog, -2);
                        }
                    });
                }
            } else {
                layout.findViewById(R.id.dialog_button_cancel).setVisibility(8);
            }
            if (this.message != null) {
                ((TextView) layout.findViewById(R.id.dialog_message)).setText(this.message);
            }
            if (this.messagePic != null) {
                ImageView imageView = (ImageView) layout.findViewById(R.id.dialog_message_pic);
                if (this.messagePicWidth > 0 && this.messagePicHeight > 0) {
                    ViewGroup.LayoutParams imageLayoutPrams = imageView.getLayoutParams();
                    if (imageLayoutPrams == null) {
                        new WindowManager.LayoutParams(this.messagePicWidth, this.messagePicHeight);
                    } else {
                        imageLayoutPrams.width = this.messagePicWidth;
                        imageLayoutPrams.height = this.messagePicHeight;
                    }
                }
                imageView.setVisibility(0);
                ImageUtil.asyncImg(this.messagePic, imageView, this.context);
            } else if (this.messagePicBitmap != null && !this.messagePicBitmap.isRecycled()) {
                ImageView imageView2 = (ImageView) layout.findViewById(R.id.dialog_message_pic);
                if (this.messagePicWidth > 0 && this.messagePicHeight > 0) {
                    ViewGroup.LayoutParams imageLayoutPrams2 = imageView2.getLayoutParams();
                    if (imageLayoutPrams2 == null) {
                        new WindowManager.LayoutParams(this.messagePicWidth, this.messagePicHeight);
                    } else {
                        imageLayoutPrams2.width = this.messagePicWidth;
                        imageLayoutPrams2.height = this.messagePicHeight;
                    }
                }
                imageView2.setVisibility(0);
                imageView2.setImageBitmap(this.messagePicBitmap);
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}
