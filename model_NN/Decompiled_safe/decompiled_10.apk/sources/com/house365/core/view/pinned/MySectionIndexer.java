package com.house365.core.view.pinned;

import android.widget.SectionIndexer;
import com.house365.core.constant.CorePreferences;
import java.util.Arrays;
import org.apache.commons.lang.StringUtils;

public class MySectionIndexer implements SectionIndexer {
    private boolean buildedIdx = false;
    private final int mCount;
    private IndexerRanger[] mIndexerRangers;
    private final int[] mPositions;
    private final String[] mSections;

    class IndexerRanger {
        public int end;
        public int start;

        IndexerRanger() {
        }
    }

    public MySectionIndexer(String[] sections, int[] counts) {
        if (sections == null || counts == null) {
            throw new NullPointerException();
        } else if (sections.length != counts.length) {
            throw new IllegalArgumentException("The sections and counts arrays must have the same length");
        } else {
            this.buildedIdx = false;
            this.mSections = sections;
            this.mPositions = new int[counts.length];
            this.mIndexerRangers = new IndexerRanger[counts.length];
            int position = 0;
            for (int i = 0; i < counts.length; i++) {
                if (this.mSections[i] == null) {
                    this.mSections[i] = StringUtils.EMPTY;
                } else {
                    this.mSections[i] = this.mSections[i].trim();
                }
                this.mPositions[i] = position;
                IndexerRanger indexerRanger = new IndexerRanger();
                indexerRanger.start = position;
                position += counts[i];
                indexerRanger.end = position + -1 < indexerRanger.start ? indexerRanger.start : position - 1;
                this.mIndexerRangers[i] = indexerRanger;
            }
            this.mCount = position;
            this.buildedIdx = true;
        }
    }

    public Object[] getSections() {
        return this.mSections;
    }

    public int getPositionForSection(int section) {
        if (section < 0 || section >= this.mSections.length) {
            return -1;
        }
        try {
            return this.mPositions[section];
        } catch (Exception e) {
            CorePreferences.ERROR("mPositions[section]" + section);
            e.printStackTrace();
            return 0;
        }
    }

    public int getSectionForPosition(int position) {
        while (!this.buildedIdx) {
            try {
                Thread.currentThread();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (position < 0 || position >= this.mCount) {
            return -1;
        }
        int index = Arrays.binarySearch(this.mPositions, position);
        return index < 0 ? (-index) - 2 : index;
    }

    public IndexerRanger getIndexerRanger(int section) {
        if (section < 0 || section > this.mSections.length) {
            return null;
        }
        return this.mIndexerRangers[section];
    }
}
