package com.house365.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import com.house365.core.R;
import com.house365.core.adapter.BaseAlbumAdapter;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.core.touch.ImageViewTouch;
import com.house365.core.touch.ScaleGestureDetector;
import org.apache.commons.lang.SystemUtils;

public class AlbumView extends RelativeLayout {
    public static final int BOTTOM = 3;
    public static final int CENTER_X = 4;
    public static final int CENTER_Y = 5;
    public static final int LEFT = 0;
    public static final int RIGHT = 2;
    private static final int SHOW_HIDE_CONTROL_ANIMATION_TIME = 500;
    public static final int TOP = 1;
    /* access modifiers changed from: private */
    public BaseAlbumAdapter adapter;
    /* access modifiers changed from: private */
    public AlumViewListener alumViewListener;
    protected BaseApplication application;
    private Context context;
    /* access modifiers changed from: private */
    public int currPos;
    private Gallery gallery;
    private float galleryHeight;
    private float galleryPaddingBottom;
    private float galleryPaddingLeft;
    private float galleryPaddingRight;
    private float galleryPaddingTop;
    private int galleryPosition;
    private float gallerySpacing;
    private float galleryWidth;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            AlbumView.this.mOnScale = false;
        }
    };
    private AlphaAnimation hideAnimation;
    /* access modifiers changed from: private */
    public boolean isFullScreen = false;
    /* access modifiers changed from: private */
    public GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public boolean mOnPagerScoll = false;
    /* access modifiers changed from: private */
    public boolean mOnScale = false;
    ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        public void onPageSelected(int position) {
            ImageViewTouch preImageView = null;
            try {
                if (AlbumView.this.prePos != position && AlbumView.this.prePos >= 0) {
                    preImageView = AlbumView.this.adapter.getImageView(position);
                }
            } catch (Exception e) {
            }
            if (preImageView != null) {
                preImageView.setImageBitmapResetBase(preImageView.mBitmapDisplayed.getBitmap(), true);
            }
            AlbumView.this.currPos = position;
            AlbumView.this.setCurrentPosition(position);
            if (AlbumView.this.alumViewListener != null) {
                AlbumView.this.alumViewListener.onChange(AlbumView.this.currPos);
            }
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            AlbumView.this.mOnPagerScoll = true;
        }

        public void onPageScrollStateChanged(int state) {
            if (state == 1) {
                AlbumView.this.mOnPagerScoll = true;
            } else if (state == 2) {
                AlbumView.this.mOnPagerScoll = false;
            } else {
                AlbumView.this.mOnPagerScoll = false;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean mPaused;
    /* access modifiers changed from: private */
    public ScaleGestureDetector mScaleGestureDetector;
    private float pageMargin;
    private int pageMarginColor;
    private int pageMarginDrawable;
    /* access modifiers changed from: private */
    public int prePos;
    private AlphaAnimation showAnimation;
    private boolean useGallery = false;
    /* access modifiers changed from: private */
    public ViewPager viewPager;

    public interface AlumViewListener {
        void onChange(int i);

        void onFullScreen(boolean z, int i);
    }

    public AlbumView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        getAttr(context2, attrs);
        init(context2);
    }

    public AlbumView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        getAttr(context2, attrs);
        init(context2);
    }

    public AlbumView(Context context2) {
        super(context2);
        init(context2);
    }

    private void getAttr(Context context2, AttributeSet attrs) {
        TypedArray a = context2.obtainStyledAttributes(attrs, R.styleable.AlbumView);
        this.galleryWidth = a.getDimension(0, -1.0f);
        this.galleryHeight = a.getDimension(1, -1.0f);
        this.gallerySpacing = a.getDimension(3, SystemUtils.JAVA_VERSION_FLOAT);
        this.galleryPaddingLeft = a.getDimension(4, SystemUtils.JAVA_VERSION_FLOAT);
        this.galleryPaddingTop = a.getDimension(5, SystemUtils.JAVA_VERSION_FLOAT);
        this.galleryPaddingRight = a.getDimension(6, SystemUtils.JAVA_VERSION_FLOAT);
        this.galleryPaddingBottom = a.getDimension(7, SystemUtils.JAVA_VERSION_FLOAT);
        this.pageMargin = a.getDimension(8, -1.0f);
        this.galleryPosition = a.getInt(2, 3);
        this.pageMarginDrawable = a.getResourceId(9, -1);
        this.pageMarginColor = a.getColor(10, -1);
        this.useGallery = a.getBoolean(11, false);
        a.recycle();
    }

    public void init(Context context2) {
        int i;
        int i2 = -1;
        this.context = context2;
        this.application = (BaseApplication) context2.getApplicationContext();
        this.viewPager = new ViewPager(context2);
        this.viewPager.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        if (this.pageMarginDrawable != -1) {
            this.viewPager.setPageMarginDrawable(this.pageMarginDrawable);
        }
        if (this.pageMarginColor != -1) {
            this.viewPager.setPageMarginDrawable(new ColorDrawable(this.pageMarginColor));
        }
        if (this.pageMargin != -1.0f) {
            this.viewPager.setPageMargin((int) this.pageMargin);
        }
        this.showAnimation = new AlphaAnimation((float) SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
        this.showAnimation.setFillAfter(true);
        this.showAnimation.setDuration(500);
        this.hideAnimation = new AlphaAnimation(1.0f, (float) SystemUtils.JAVA_VERSION_FLOAT);
        this.hideAnimation.setFillAfter(true);
        this.hideAnimation.setDuration(500);
        addView(this.viewPager);
        if (this.useGallery) {
            this.gallery = new Gallery(context2);
            if (this.galleryWidth > SystemUtils.JAVA_VERSION_FLOAT) {
                i = (int) this.galleryWidth;
            } else {
                i = -1;
            }
            if (this.galleryHeight > SystemUtils.JAVA_VERSION_FLOAT) {
                i2 = (int) this.galleryHeight;
            }
            RelativeLayout.LayoutParams galleryLayout = new RelativeLayout.LayoutParams(i, i2);
            galleryLayout.addRule(12);
            this.gallery.setLayoutParams(galleryLayout);
            this.gallery.setPadding((int) this.galleryPaddingLeft, (int) this.galleryPaddingTop, (int) this.galleryPaddingRight, (int) this.galleryPaddingBottom);
            this.gallery.setSpacing((int) this.gallerySpacing);
            addView(this.gallery);
        }
        this.viewPager.setOnPageChangeListener(this.mPageChangeListener);
        setupOnTouchListeners(this.viewPager);
        hideControls();
    }

    /* access modifiers changed from: private */
    public void showControls() {
        if (this.useGallery) {
            this.gallery.startAnimation(this.showAnimation);
            this.gallery.setVisibility(0);
        }
        this.isFullScreen = false;
    }

    /* access modifiers changed from: private */
    public void hideControls() {
        if (this.useGallery) {
            this.gallery.startAnimation(this.hideAnimation);
            this.gallery.setVisibility(8);
        }
        this.isFullScreen = true;
    }

    public void setAdapter(BaseAlbumAdapter adapter2) {
        this.adapter = adapter2;
        this.viewPager.setAdapter(adapter2.getPageAdapter());
        if (this.useGallery) {
            this.gallery.setAdapter((SpinnerAdapter) adapter2.getGalleryAdapter());
            this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    AlbumView.this.setCurrentPosition(position);
                }
            });
            adapter2.setGallery(this.gallery);
        }
    }

    public ImageViewTouch getCurrentImageView() {
        return this.adapter.getImageView(this.currPos);
    }

    private void setupOnTouchListeners(View rootView) {
        if (Build.VERSION.SDK_INT >= 7) {
            this.mScaleGestureDetector = new ScaleGestureDetector(this.context, new MyOnScaleGestureListener(this, null));
        }
        this.mGestureDetector = new GestureDetector(this.context, new MyGestureListener(this, null));
        rootView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                RectF rect;
                try {
                    CorePreferences.DEBUG("mOnScale:" + AlbumView.this.mOnScale + ",mOnPagerScoll:" + AlbumView.this.mOnPagerScoll);
                    if (!AlbumView.this.mOnScale && !AlbumView.this.mOnPagerScoll) {
                        AlbumView.this.mGestureDetector.onTouchEvent(event);
                    }
                    if (Build.VERSION.SDK_INT >= 7 && !AlbumView.this.mOnPagerScoll) {
                        AlbumView.this.mScaleGestureDetector.onTouchEvent(event);
                    }
                    ImageViewTouch imageView = AlbumView.this.getCurrentImageView();
                    if (AlbumView.this.mOnScale) {
                        return true;
                    }
                    Matrix m = imageView.getImageViewMatrix();
                    if (imageView.mBitmapDisplayed.getBitmap() != null) {
                        rect = new RectF(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, (float) imageView.mBitmapDisplayed.getBitmap().getWidth(), (float) imageView.mBitmapDisplayed.getBitmap().getHeight());
                    } else {
                        rect = new RectF(SystemUtils.JAVA_VERSION_FLOAT, SystemUtils.JAVA_VERSION_FLOAT, (float) AlbumView.this.application.getMetrics().widthPixels, (float) AlbumView.this.application.getMetrics().heightPixels);
                    }
                    m.mapRect(rect);
                    if (((double) rect.right) > ((double) imageView.getWidth()) + 0.1d && ((double) rect.left) < -0.1d) {
                        return true;
                    }
                    try {
                        AlbumView.this.viewPager.onTouchEvent(event);
                        return true;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        return true;
                    }
                } catch (Exception e2) {
                    CorePreferences.ERROR(e2);
                    return true;
                }
            }
        });
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private MyGestureListener() {
        }

        /* synthetic */ MyGestureListener(AlbumView albumView, MyGestureListener myGestureListener) {
            this();
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (AlbumView.this.mOnScale) {
                return true;
            }
            if (AlbumView.this.mPaused) {
                return false;
            }
            Log.d("MyGestureListener2", "e1:" + e1);
            ImageViewTouch imageView = AlbumView.this.getCurrentImageView();
            imageView.panBy(-distanceX, -distanceY);
            imageView.center(true, true);
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (AlbumView.this.isFullScreen) {
                AlbumView.this.showControls();
            } else {
                AlbumView.this.hideControls();
            }
            if (AlbumView.this.alumViewListener == null) {
                return true;
            }
            AlbumView.this.alumViewListener.onFullScreen(AlbumView.this.isFullScreen, AlbumView.this.currPos);
            return true;
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (AlbumView.this.mPaused) {
                return false;
            }
            ImageViewTouch imageView = AlbumView.this.getCurrentImageView();
            if (imageView.mBaseZoom < 1.0f) {
                if (imageView.getScale() > 2.0f) {
                    imageView.zoomTo(1.0f);
                } else {
                    imageView.zoomToPoint(3.0f, e.getX(), e.getY());
                }
            } else if (imageView.getScale() > (imageView.mMinZoom + imageView.mMaxZoom) / 2.0f) {
                imageView.zoomTo(imageView.mMinZoom);
            } else {
                imageView.zoomToPoint(imageView.mMaxZoom, e.getX(), e.getY());
            }
            return true;
        }
    }

    private class MyOnScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        float currentMiddleX;
        float currentMiddleY;
        float currentScale;

        private MyOnScaleGestureListener() {
        }

        /* synthetic */ MyOnScaleGestureListener(AlbumView albumView, MyOnScaleGestureListener myOnScaleGestureListener) {
            this();
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
            ImageViewTouch imageView = AlbumView.this.getCurrentImageView();
            if (this.currentScale > imageView.mMaxZoom) {
                this.currentScale = imageView.mMaxZoom;
                imageView.zoomToNoCenterValue(this.currentScale, this.currentMiddleX, this.currentMiddleY);
            } else if (this.currentScale < imageView.mMinZoom) {
                this.currentScale = imageView.mMinZoom;
                imageView.zoomToNoCenterValue(this.currentScale, this.currentMiddleX, this.currentMiddleY);
            } else {
                imageView.zoomToNoCenter(this.currentScale, this.currentMiddleX, this.currentMiddleY);
            }
            imageView.center(true, true);
            CorePreferences.DEBUG("mOnScale.close" + AlbumView.this.mOnScale);
            AlbumView.this.handler.sendEmptyMessageDelayed(1, 300);
        }

        public boolean onScaleBegin(ScaleGestureDetector detector) {
            AlbumView.this.mOnScale = true;
            return true;
        }

        public boolean onScale(ScaleGestureDetector detector, float mx, float my) {
            ImageViewTouch imageView = AlbumView.this.getCurrentImageView();
            this.currentScale = imageView.getScale() * detector.getScaleFactor();
            this.currentMiddleX = mx;
            this.currentMiddleY = my;
            CorePreferences.DEBUG("currentScale:" + this.currentScale + ",detector.isInProgress():" + detector.isInProgress());
            if (!detector.isInProgress()) {
                return true;
            }
            if (this.currentScale > imageView.mMaxZoom) {
                this.currentScale = imageView.mMaxZoom;
                imageView.zoomToNoCenterValue(this.currentScale, this.currentMiddleX, this.currentMiddleY);
                return true;
            } else if (this.currentScale < imageView.mMinZoom) {
                this.currentScale = imageView.mMinZoom;
                imageView.zoomToNoCenterValue(this.currentScale, this.currentMiddleX, this.currentMiddleY);
                return true;
            } else {
                imageView.zoomToNoCenter(this.currentScale, this.currentMiddleX, this.currentMiddleY);
                return true;
            }
        }
    }

    public void setCurrentPosition(int position) {
        this.prePos = this.currPos;
        this.currPos = position;
        this.viewPager.setCurrentItem(this.currPos);
        if (this.useGallery) {
            this.gallery.setSelection(this.currPos);
        }
    }

    public void setAlbumViewListener(AlumViewListener alumViewListener2) {
        this.alumViewListener = alumViewListener2;
    }
}
