package com.house365.core.view.pulltorefresh.internal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.house365.core.R;
import org.apache.commons.lang.SystemUtils;

public class LoadingLayout extends FrameLayout {
    static final int DEFAULT_ROTATION_ANIMATION_DURATION = 150;
    private final ImageView mHeaderImage;
    private final ProgressBar mHeaderProgress;
    private final TextView mHeaderText;
    private String mPullLabel;
    private TextView mRefreshViewLastUpdated;
    private String mRefreshingLabel;
    private String mReleaseLabel;
    private final Animation mResetRotateAnimation;
    private final Animation mRotateAnimation = new RotateAnimation(SystemUtils.JAVA_VERSION_FLOAT, -180.0f, 1, 0.5f, 1, 0.5f);

    public LoadingLayout(Context context, int mode, String releaseLabel, String pullLabel, String refreshingLabel) {
        super(context);
        ViewGroup header = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.pull_to_refresh_header, this);
        this.mHeaderText = (TextView) header.findViewById(R.id.pull_to_refresh_text);
        this.mHeaderImage = (ImageView) header.findViewById(R.id.pull_to_refresh_image);
        this.mHeaderProgress = (ProgressBar) header.findViewById(R.id.pull_to_refresh_progress);
        this.mRefreshViewLastUpdated = (TextView) header.findViewById(R.id.pull_to_refresh_updated_at);
        Interpolator interpolator = new LinearInterpolator();
        this.mRotateAnimation.setInterpolator(interpolator);
        this.mRotateAnimation.setDuration(150);
        this.mRotateAnimation.setFillAfter(true);
        this.mResetRotateAnimation = new RotateAnimation(-180.0f, SystemUtils.JAVA_VERSION_FLOAT, 1, 0.5f, 1, 0.5f);
        this.mResetRotateAnimation.setInterpolator(interpolator);
        this.mResetRotateAnimation.setDuration(150);
        this.mResetRotateAnimation.setFillAfter(true);
        this.mReleaseLabel = releaseLabel;
        this.mPullLabel = pullLabel;
        this.mRefreshingLabel = refreshingLabel;
        switch (mode) {
            case 2:
                this.mHeaderImage.setImageResource(R.drawable.pulltorefresh_up_arrow);
                return;
            default:
                this.mHeaderImage.setImageResource(R.drawable.pulltorefresh_down_arrow);
                return;
        }
    }

    public void reset() {
        this.mHeaderText.setText(this.mPullLabel);
        this.mHeaderImage.setVisibility(0);
        this.mHeaderProgress.setVisibility(8);
    }

    public void releaseToRefresh() {
        this.mHeaderText.setText(this.mReleaseLabel);
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.startAnimation(this.mRotateAnimation);
    }

    public void setPullLabel(String pullLabel) {
        this.mPullLabel = pullLabel;
    }

    public void refreshing() {
        this.mHeaderText.setText(this.mRefreshingLabel);
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.setVisibility(4);
        this.mHeaderProgress.setVisibility(0);
    }

    public void setRefreshingLabel(String refreshingLabel) {
        this.mRefreshingLabel = refreshingLabel;
    }

    public void setReleaseLabel(String releaseLabel) {
        this.mReleaseLabel = releaseLabel;
    }

    public void pullToRefresh() {
        this.mHeaderText.setText(this.mPullLabel);
        this.mHeaderImage.clearAnimation();
        this.mHeaderImage.startAnimation(this.mResetRotateAnimation);
    }

    public void setTextColor(int color) {
        this.mHeaderText.setTextColor(color);
    }

    public void setLastUpdate(CharSequence lastUpdated) {
        if (lastUpdated != null) {
            this.mRefreshViewLastUpdated.setVisibility(0);
            this.mRefreshViewLastUpdated.setText(lastUpdated);
            return;
        }
        this.mRefreshViewLastUpdated.setVisibility(8);
    }
}
