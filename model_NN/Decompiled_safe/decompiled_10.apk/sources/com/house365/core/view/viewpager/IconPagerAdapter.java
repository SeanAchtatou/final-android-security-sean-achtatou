package com.house365.core.view.viewpager;

public interface IconPagerAdapter {
    int getCount();

    int getIconResId(int i);
}
