package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.house365.core.R;

public class PullToRefreshWebView extends PullToRefreshBase<WebView> {
    private final WebChromeClient defaultWebChromeClient = new WebChromeClient() {
        public void onProgressChanged(WebView view, int newProgress) {
        }
    };

    public PullToRefreshWebView(Context context) {
        super(context);
        ((WebView) this.mRefreshableView).setWebChromeClient(this.defaultWebChromeClient);
    }

    public PullToRefreshWebView(Context context, int mode) {
        super(context, mode);
        ((WebView) this.mRefreshableView).setWebChromeClient(this.defaultWebChromeClient);
    }

    public PullToRefreshWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((WebView) this.mRefreshableView).setWebChromeClient(this.defaultWebChromeClient);
    }

    /* access modifiers changed from: protected */
    public WebView createRefreshableView(Context context, AttributeSet attrs) {
        WebView webView = new WebView(context, attrs);
        webView.setId(R.id.webview);
        return webView;
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullDown() {
        return ((WebView) this.mRefreshableView).getScrollY() == 0;
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullUp() {
        return ((WebView) this.mRefreshableView).getScrollY() >= ((WebView) this.mRefreshableView).getContentHeight() - ((WebView) this.mRefreshableView).getHeight();
    }
}
