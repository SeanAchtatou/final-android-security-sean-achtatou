package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.house365.core.view.pulltorefresh.PullToRefreshBase;
import com.house365.core.view.pulltorefresh.internal.EmptyViewMethodAccessor;

public abstract class PullToRefreshAdapterViewBase<T extends AbsListView> extends PullToRefreshBase<T> implements AbsListView.OnScrollListener {
    private View mEmptyView;
    private int mLastSavedFirstVisibleItem = -1;
    private PullToRefreshBase.OnLastItemVisibleListener mOnLastItemVisibleListener;
    private AbsListView.OnScrollListener mOnScrollListener;
    private FrameLayout mRefreshableViewHolder;

    public abstract ContextMenu.ContextMenuInfo getContextMenuInfo();

    public PullToRefreshAdapterViewBase(Context context) {
        super(context);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public PullToRefreshAdapterViewBase(Context context, int mode) {
        super(context, mode);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public PullToRefreshAdapterViewBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((AbsListView) this.mRefreshableView).setOnScrollListener(this);
    }

    public final void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.mOnLastItemVisibleListener != null && visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount && firstVisibleItem != this.mLastSavedFirstVisibleItem) {
            this.mLastSavedFirstVisibleItem = firstVisibleItem;
            this.mOnLastItemVisibleListener.onLastItemVisible();
        }
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    public final void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(view, scrollState);
        }
    }

    public final void setEmptyView(View newEmptyView) {
        if (this.mEmptyView != null) {
            this.mRefreshableViewHolder.removeView(this.mEmptyView);
        }
        if (newEmptyView != null) {
            newEmptyView.setClickable(true);
            ViewParent newEmptyViewParent = newEmptyView.getParent();
            if (newEmptyViewParent != null && (newEmptyViewParent instanceof ViewGroup)) {
                ((ViewGroup) newEmptyViewParent).removeView(newEmptyView);
            }
            this.mRefreshableViewHolder.addView(newEmptyView, -1, -1);
            if (this.mRefreshableView instanceof EmptyViewMethodAccessor) {
                ((EmptyViewMethodAccessor) this.mRefreshableView).setEmptyViewInternal(newEmptyView);
            } else {
                ((AbsListView) this.mRefreshableView).setEmptyView(newEmptyView);
            }
        }
    }

    public final void setOnLastItemVisibleListener(PullToRefreshBase.OnLastItemVisibleListener listener) {
        this.mOnLastItemVisibleListener = listener;
    }

    public final void setOnScrollListener(AbsListView.OnScrollListener listener) {
        this.mOnScrollListener = listener;
    }

    /* access modifiers changed from: protected */
    public void addRefreshableView(Context context, T refreshableView) {
        this.mRefreshableViewHolder = new FrameLayout(context);
        this.mRefreshableViewHolder.addView(refreshableView, -1, -1);
        addView(this.mRefreshableViewHolder, new LinearLayout.LayoutParams(-1, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullDown() {
        return isFirstItemVisible();
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullUp() {
        return isLastItemVisible();
    }

    private boolean isFirstItemVisible() {
        View firstVisibleChild;
        if (((AbsListView) this.mRefreshableView).getCount() == getNumberInternalViews()) {
            return true;
        }
        if (((AbsListView) this.mRefreshableView).getFirstVisiblePosition() != 0 || (firstVisibleChild = ((AbsListView) this.mRefreshableView).getChildAt(0)) == null) {
            return false;
        }
        return firstVisibleChild.getTop() >= ((AbsListView) this.mRefreshableView).getTop();
    }

    private boolean isLastItemVisible() {
        View lastVisibleChild;
        int count = ((AbsListView) this.mRefreshableView).getCount();
        int lastVisiblePosition = ((AbsListView) this.mRefreshableView).getLastVisiblePosition();
        if (count == getNumberInternalViews()) {
            return true;
        }
        if (lastVisiblePosition != (count - 1) - getNumberInternalFooterViews() || (lastVisibleChild = ((AbsListView) this.mRefreshableView).getChildAt(lastVisiblePosition - ((AbsListView) this.mRefreshableView).getFirstVisiblePosition())) == null) {
            return false;
        }
        return lastVisibleChild.getBottom() <= ((AbsListView) this.mRefreshableView).getBottom();
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalViews() {
        return getNumberInternalHeaderViews() + getNumberInternalFooterViews();
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalHeaderViews() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalFooterViews() {
        return 0;
    }
}
