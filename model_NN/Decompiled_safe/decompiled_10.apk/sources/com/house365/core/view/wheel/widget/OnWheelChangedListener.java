package com.house365.core.view.wheel.widget;

public interface OnWheelChangedListener {
    void onChanged(WheelView wheelView, int i, int i2);
}
