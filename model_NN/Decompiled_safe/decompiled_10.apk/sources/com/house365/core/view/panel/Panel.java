package com.house365.core.view.panel;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.house365.core.R;
import org.apache.commons.lang.SystemUtils;

public class Panel extends LinearLayout {
    public static final int BOTTOM = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
    private static final String TAG = "Panel";
    public static final int TOP = 0;
    /* access modifiers changed from: private */
    public Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        public void onAnimationEnd(Animation animation) {
            Panel.this.mState = State.READY;
            if (Panel.this.mIsShrinking) {
                Panel.this.mContent.setVisibility(8);
            }
            Panel.this.postProcess();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
            Panel.this.mState = State.ANIMATING;
        }
    };
    View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (Panel.this.enableHanlderClick) {
                if (Panel.this.mBringToFront) {
                    Panel.this.bringToFront();
                }
                if (Panel.this.initChange()) {
                    Panel.this.post(Panel.this.startAnimation);
                }
            }
        }
    };
    public boolean enableHanlderClick = true;
    /* access modifiers changed from: private */
    public boolean mBringToFront;
    private Drawable mClosedHandle;
    /* access modifiers changed from: private */
    public View mContent;
    /* access modifiers changed from: private */
    public int mContentHeight;
    private int mContentId;
    /* access modifiers changed from: private */
    public int mContentWidth;
    /* access modifiers changed from: private */
    public int mDuration;
    /* access modifiers changed from: private */
    public GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public PanelOnGestureListener mGestureListener;
    private View mHandle;
    private int mHandleId;
    /* access modifiers changed from: private */
    public Interpolator mInterpolator;
    /* access modifiers changed from: private */
    public boolean mIsShrinking;
    /* access modifiers changed from: private */
    public boolean mLinearFlying;
    private Drawable mOpenedHandle;
    /* access modifiers changed from: private */
    public int mOrientation;
    /* access modifiers changed from: private */
    public int mPosition;
    /* access modifiers changed from: private */
    public State mState;
    /* access modifiers changed from: private */
    public float mTrackX;
    /* access modifiers changed from: private */
    public float mTrackY;
    /* access modifiers changed from: private */
    public float mVelocity;
    private float mWeight;
    private OnPanelListener panelListener;
    Runnable startAnimation = new Runnable() {
        public void run() {
            int calculatedDuration;
            int fromXDelta = 0;
            int toXDelta = 0;
            int fromYDelta = 0;
            int toYDelta = 0;
            if (Panel.this.mState == State.FLYING) {
                Panel.this.mIsShrinking = (Panel.this.mPosition == 0 || Panel.this.mPosition == 2) ^ (Panel.this.mVelocity > SystemUtils.JAVA_VERSION_FLOAT);
            }
            if (Panel.this.mOrientation == 1) {
                int height = Panel.this.mContentHeight;
                if (!Panel.this.mIsShrinking) {
                    fromYDelta = Panel.this.mPosition == 0 ? -height : height;
                } else {
                    toYDelta = Panel.this.mPosition == 0 ? -height : height;
                }
                if (Panel.this.mState == State.TRACKING) {
                    if (Math.abs(Panel.this.mTrackY - ((float) fromYDelta)) < Math.abs(Panel.this.mTrackY - ((float) toYDelta))) {
                        Panel.this.mIsShrinking = !Panel.this.mIsShrinking;
                        toYDelta = fromYDelta;
                    }
                    fromYDelta = (int) Panel.this.mTrackY;
                } else if (Panel.this.mState == State.FLYING) {
                    fromYDelta = (int) Panel.this.mTrackY;
                }
                if (Panel.this.mState != State.FLYING || !Panel.this.mLinearFlying) {
                    calculatedDuration = (Panel.this.mDuration * Math.abs(toYDelta - fromYDelta)) / Panel.this.mContentHeight;
                } else {
                    calculatedDuration = Math.max((int) (1000.0f * Math.abs(((float) (toYDelta - fromYDelta)) / Panel.this.mVelocity)), 20);
                }
            } else {
                int width = Panel.this.mContentWidth;
                if (!Panel.this.mIsShrinking) {
                    fromXDelta = Panel.this.mPosition == 2 ? -width : width;
                } else {
                    toXDelta = Panel.this.mPosition == 2 ? -width : width;
                }
                if (Panel.this.mState == State.TRACKING) {
                    if (Math.abs(Panel.this.mTrackX - ((float) fromXDelta)) < Math.abs(Panel.this.mTrackX - ((float) toXDelta))) {
                        Panel.this.mIsShrinking = !Panel.this.mIsShrinking;
                        toXDelta = fromXDelta;
                    }
                    fromXDelta = (int) Panel.this.mTrackX;
                } else if (Panel.this.mState == State.FLYING) {
                    fromXDelta = (int) Panel.this.mTrackX;
                }
                if (Panel.this.mState != State.FLYING || !Panel.this.mLinearFlying) {
                    calculatedDuration = (Panel.this.mDuration * Math.abs(toXDelta - fromXDelta)) / Panel.this.mContentWidth;
                } else {
                    calculatedDuration = Math.max((int) (1000.0f * Math.abs(((float) (toXDelta - fromXDelta)) / Panel.this.mVelocity)), 20);
                }
            }
            Panel panel = Panel.this;
            Panel.this.mTrackY = SystemUtils.JAVA_VERSION_FLOAT;
            panel.mTrackX = SystemUtils.JAVA_VERSION_FLOAT;
            if (calculatedDuration == 0) {
                Panel.this.mState = State.READY;
                if (Panel.this.mIsShrinking) {
                    Panel.this.mContent.setVisibility(8);
                }
                Panel.this.postProcess();
                return;
            }
            TranslateAnimation animation = new TranslateAnimation((float) fromXDelta, (float) toXDelta, (float) fromYDelta, (float) toYDelta);
            animation.setDuration((long) calculatedDuration);
            animation.setAnimationListener(Panel.this.animationListener);
            if (Panel.this.mState == State.FLYING && Panel.this.mLinearFlying) {
                animation.setInterpolator(new LinearInterpolator());
            } else if (Panel.this.mInterpolator != null) {
                animation.setInterpolator(Panel.this.mInterpolator);
            }
            Panel.this.startAnimation(animation);
        }
    };
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        int initX;
        int initY;
        boolean setInitialPosition;

        public boolean onTouch(View v, MotionEvent event) {
            int i = -1;
            if (Panel.this.mState != State.ANIMATING) {
                int action = event.getAction();
                if (action == 0) {
                    if (Panel.this.mBringToFront) {
                        Panel.this.bringToFront();
                    }
                    this.initX = 0;
                    this.initY = 0;
                    if (Panel.this.mContent.getVisibility() == 8) {
                        if (Panel.this.mOrientation == 1) {
                            if (Panel.this.mPosition != 0) {
                                i = 1;
                            }
                            this.initY = i;
                        } else {
                            if (Panel.this.mPosition != 2) {
                                i = 1;
                            }
                            this.initX = i;
                        }
                    }
                    this.setInitialPosition = true;
                } else {
                    if (this.setInitialPosition) {
                        this.initX *= Panel.this.mContentWidth;
                        this.initY *= Panel.this.mContentHeight;
                        Panel.this.mGestureListener.setScroll(this.initX, this.initY);
                        this.setInitialPosition = false;
                        this.initX = -this.initX;
                        this.initY = -this.initY;
                    }
                    event.offsetLocation((float) this.initX, (float) this.initY);
                }
                if (!Panel.this.mGestureDetector.onTouchEvent(event) && action == 1) {
                    Panel.this.post(Panel.this.startAnimation);
                }
            }
            return false;
        }
    };

    public interface OnPanelListener {
        void onPanelClosed(Panel panel);

        void onPanelOpened(Panel panel);
    }

    private enum State {
        ABOUT_TO_ANIMATE,
        ANIMATING,
        READY,
        TRACKING,
        FLYING
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Panel(Context context, AttributeSet attrs) {
        super(context, attrs);
        int i = 1;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Panel);
        this.mDuration = a.getInteger(0, 750);
        this.mPosition = a.getInteger(1, 1);
        this.mLinearFlying = a.getBoolean(4, false);
        this.mWeight = a.getFraction(5, 0, 1, SystemUtils.JAVA_VERSION_FLOAT);
        if (this.mWeight < SystemUtils.JAVA_VERSION_FLOAT || this.mWeight > 1.0f) {
            this.mWeight = SystemUtils.JAVA_VERSION_FLOAT;
            Log.w(TAG, String.valueOf(a.getPositionDescription()) + ": weight must be > 0 and <= 1");
        }
        this.mOpenedHandle = a.getDrawable(6);
        this.mClosedHandle = a.getDrawable(7);
        RuntimeException e = null;
        this.mHandleId = a.getResourceId(2, 0);
        e = this.mHandleId == 0 ? new IllegalArgumentException(String.valueOf(a.getPositionDescription()) + ": The handle attribute is required and must refer to a valid child.") : e;
        this.mContentId = a.getResourceId(3, 0);
        e = this.mContentId == 0 ? new IllegalArgumentException(String.valueOf(a.getPositionDescription()) + ": The content attribute is required and must refer to a valid child.") : e;
        a.recycle();
        if (e != null) {
            throw e;
        }
        if (!(this.mPosition == 0 || this.mPosition == 1)) {
            i = 0;
        }
        this.mOrientation = i;
        setOrientation(this.mOrientation);
        this.mState = State.READY;
        this.mGestureListener = new PanelOnGestureListener();
        this.mGestureDetector = new GestureDetector(this.mGestureListener);
        this.mGestureDetector.setIsLongpressEnabled(false);
        setBaselineAligned(false);
    }

    public void setOnPanelListener(OnPanelListener onPanelListener) {
        this.panelListener = onPanelListener;
    }

    public View getHandle() {
        return this.mHandle;
    }

    public View getContent() {
        return this.mContent;
    }

    public void setInterpolator(Interpolator i) {
        this.mInterpolator = i;
    }

    public boolean setOpen(boolean open, boolean animate) {
        int i = 0;
        if (this.mState != State.READY || !(isOpen() ^ open)) {
            return false;
        }
        this.mIsShrinking = !open;
        if (animate) {
            this.mState = State.ABOUT_TO_ANIMATE;
            if (!this.mIsShrinking) {
                this.mContent.setVisibility(0);
            }
            post(this.startAnimation);
            return true;
        }
        View view = this.mContent;
        if (!open) {
            i = 8;
        }
        view.setVisibility(i);
        postProcess();
        return true;
    }

    public boolean isOpen() {
        return this.mContent.getVisibility() == 0;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.mHandle = findViewById(this.mHandleId);
        if (this.mHandle == null) {
            throw new RuntimeException("Your Panel must have a child View whose id attribute is 'R.id." + getResources().getResourceEntryName(this.mHandleId) + "'");
        }
        this.mHandle.setOnTouchListener(this.touchListener);
        this.mHandle.setOnClickListener(this.clickListener);
        this.mContent = findViewById(this.mContentId);
        if (this.mContent == null) {
            throw new RuntimeException("Your Panel must have a child View whose id attribute is 'R.id." + getResources().getResourceEntryName(this.mHandleId) + "'");
        }
        removeView(this.mHandle);
        removeView(this.mContent);
        if (this.mPosition == 0 || this.mPosition == 2) {
            addView(this.mContent);
            addView(this.mHandle);
        } else {
            addView(this.mHandle);
            addView(this.mContent);
        }
        if (this.mClosedHandle != null) {
            this.mHandle.setBackgroundDrawable(this.mClosedHandle);
        }
        this.mContent.setClickable(true);
        this.mContent.setVisibility(8);
        if (this.mWeight > SystemUtils.JAVA_VERSION_FLOAT) {
            ViewGroup.LayoutParams params = this.mContent.getLayoutParams();
            if (this.mOrientation == 1) {
                params.height = -1;
            } else {
                params.width = -1;
            }
            this.mContent.setLayoutParams(params);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent != null && (parent instanceof FrameLayout)) {
            this.mBringToFront = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View parent;
        if (this.mWeight > SystemUtils.JAVA_VERSION_FLOAT && this.mContent.getVisibility() == 0 && (parent = (View) getParent()) != null) {
            if (this.mOrientation == 1) {
                heightMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) (((float) parent.getHeight()) * this.mWeight), 1073741824);
            } else {
                widthMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) (((float) parent.getWidth()) * this.mWeight), 1073741824);
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        this.mContentWidth = this.mContent.getWidth();
        this.mContentHeight = this.mContent.getHeight();
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.mState == State.ABOUT_TO_ANIMATE && !this.mIsShrinking) {
            int delta = this.mOrientation == 1 ? this.mContentHeight : this.mContentWidth;
            if (this.mPosition == 2 || this.mPosition == 0) {
                delta = -delta;
            }
            if (this.mOrientation == 1) {
                canvas.translate(SystemUtils.JAVA_VERSION_FLOAT, (float) delta);
            } else {
                canvas.translate((float) delta, SystemUtils.JAVA_VERSION_FLOAT);
            }
        }
        if (this.mState == State.TRACKING || this.mState == State.FLYING) {
            canvas.translate(this.mTrackX, this.mTrackY);
        }
        super.dispatchDraw(canvas);
    }

    /* access modifiers changed from: private */
    public float ensureRange(float v, int min, int max) {
        return Math.min(Math.max(v, (float) min), (float) max);
    }

    public boolean initChange() {
        if (this.mState != State.READY) {
            return false;
        }
        this.mState = State.ABOUT_TO_ANIMATE;
        this.mIsShrinking = this.mContent.getVisibility() == 0;
        if (!this.mIsShrinking) {
            this.mContent.setVisibility(0);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void postProcess() {
        if (this.mIsShrinking && this.mClosedHandle != null) {
            this.mHandle.setBackgroundDrawable(this.mClosedHandle);
        } else if (!this.mIsShrinking && this.mOpenedHandle != null) {
            this.mHandle.setBackgroundDrawable(this.mOpenedHandle);
        }
        if (this.panelListener == null) {
            return;
        }
        if (this.mIsShrinking) {
            this.panelListener.onPanelClosed(this);
        } else {
            this.panelListener.onPanelOpened(this);
        }
    }

    class PanelOnGestureListener implements GestureDetector.OnGestureListener {
        float scrollX;
        float scrollY;

        PanelOnGestureListener() {
        }

        public void setScroll(int initScrollX, int initScrollY) {
            this.scrollX = (float) initScrollX;
            this.scrollY = (float) initScrollY;
        }

        public boolean onDown(MotionEvent e) {
            this.scrollY = SystemUtils.JAVA_VERSION_FLOAT;
            this.scrollX = SystemUtils.JAVA_VERSION_FLOAT;
            Panel.this.initChange();
            return true;
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            Panel.this.mState = State.FLYING;
            Panel panel = Panel.this;
            if (Panel.this.mOrientation != 1) {
                velocityY = velocityX;
            }
            panel.mVelocity = velocityY;
            Panel.this.post(Panel.this.startAnimation);
            return true;
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            Panel.this.mState = State.TRACKING;
            float tmpY = SystemUtils.JAVA_VERSION_FLOAT;
            float tmpX = SystemUtils.JAVA_VERSION_FLOAT;
            if (Panel.this.mOrientation == 1) {
                this.scrollY -= distanceY;
                tmpY = Panel.this.mPosition == 0 ? Panel.this.ensureRange(this.scrollY, -Panel.this.mContentHeight, 0) : Panel.this.ensureRange(this.scrollY, 0, Panel.this.mContentHeight);
            } else {
                this.scrollX -= distanceX;
                tmpX = Panel.this.mPosition == 2 ? Panel.this.ensureRange(this.scrollX, -Panel.this.mContentWidth, 0) : Panel.this.ensureRange(this.scrollX, 0, Panel.this.mContentWidth);
            }
            if (!(tmpX == Panel.this.mTrackX && tmpY == Panel.this.mTrackY)) {
                Panel.this.mTrackX = tmpX;
                Panel.this.mTrackY = tmpY;
                Panel.this.invalidate();
            }
            return true;
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }
    }

    public boolean isEnableHanlderClick() {
        return this.enableHanlderClick;
    }

    public void setEnableHanlderClick(boolean enableHanlderClick2) {
        this.enableHanlderClick = enableHanlderClick2;
    }
}
