package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.house365.core.view.pulltorefresh.internal.EmptyViewMethodAccessor;

public class PullToRefreshListView extends PullToRefreshAdapterViewBase<ListView> {

    class InternalListView extends ListView implements EmptyViewMethodAccessor {
        public InternalListView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setAdapter(ListAdapter adapter) {
            super.setAdapter(adapter);
        }

        public void setEmptyView(View emptyView) {
            PullToRefreshListView.this.setEmptyView(emptyView);
        }

        public void setEmptyViewInternal(View emptyView) {
            super.setEmptyView(emptyView);
        }

        public ContextMenu.ContextMenuInfo getContextMenuInfo() {
            return super.getContextMenuInfo();
        }
    }

    public PullToRefreshListView(Context context) {
        super(context);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshListView(Context context, int mode) {
        super(context, mode);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDisableScrollingWhileRefreshing(false);
    }

    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return ((InternalListView) getRefreshableView()).getContextMenuInfo();
    }

    /* access modifiers changed from: protected */
    public final ListView createRefreshableView(Context context, AttributeSet attrs) {
        ListView lv = new InternalListView(context, attrs);
        lv.setId(16908298);
        return lv;
    }

    public void onRefreshComplete(CharSequence lastUpdated) {
        this.mHeaderLayout.setLastUpdate(lastUpdated);
        super.onRefreshComplete();
    }

    public void setFooterViewVisible(int visibility) {
        if (visibility == 8) {
            removeView(this.mFooterLayout);
            this.mMode = 1;
            this.mCurrentMode = 1;
            setPadding(0, -this.mHeaderHeight, 0, 0);
            return;
        }
        this.mMode = 3;
        addView(this.mFooterLayout);
        setPadding(0, -this.mHeaderHeight, 0, -this.mHeaderHeight);
    }

    public void setAdapter(ListAdapter adapter) {
        ((ListView) getRefreshableView()).setAdapter(adapter);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        ((ListView) getRefreshableView()).setOnItemClickListener(listener);
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
        ((ListView) getRefreshableView()).setOnItemLongClickListener(listener);
    }

    public void showRefreshView() {
        setHeaderRefreshingInternal(true);
    }

    public ListView getActureListView() {
        return (ListView) getRefreshableView();
    }
}
