package com.house365.core.view.pinned;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.SectionIndexer;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.constant.CorePreferences;
import com.house365.core.view.pinned.MySectionIndexer;
import com.house365.core.view.pinned.PinnedHeaderListView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public abstract class BaseSectionListAdapter extends BaseListAdapter<SectionListItem> implements AdapterView.OnItemClickListener, PinnedHeaderListView.PinnedHeaderAdapter, SectionIndexer, AbsListView.OnScrollListener {
    private final Map<String, View> currentViewSections = new HashMap();
    protected final LayoutInflater inflater;
    /* access modifiers changed from: private */
    public SectionListItemClickListener linkedListener;
    private int[] mCounts;
    private MySectionIndexer mIndexer;
    private int mSectionCounts = 0;
    private String[] mSections;
    private View transparentSectionView;

    public interface SectionListItemClickListener {
        void onHeaderItemClick(View view, int i, int i2);

        void onSubItemClick(AdapterView<?> adapterView, View view, int i, long j);
    }

    public abstract SectionView initView(int i, View view, ViewGroup viewGroup, boolean z);

    public abstract void setHeaderViewText(View view, String str);

    public BaseSectionListAdapter(Context context) {
        super(context);
        this.inflater = LayoutInflater.from(context);
    }

    public boolean addAll(List<? extends SectionListItem> list) {
        super.addAll(list);
        this.mSectionCounts = 0;
        updateTotalCount();
        this.mIndexer = new MySectionIndexer(this.mSections, this.mCounts);
        return true;
    }

    private boolean isTheSame(String previousSection, String newSection) {
        if (previousSection == null) {
            return newSection == null;
        }
        return previousSection.equals(newSection);
    }

    private void fillSections() {
        this.mSections = new String[this.mSectionCounts];
        this.mCounts = new int[this.mSectionCounts];
        int count = getCount();
        int newSectionIndex = -1;
        int newSectionCounts = 1;
        String previousSection = null;
        for (int i = 0; i < count; i++) {
            newSectionCounts++;
            String currentSection = ((SectionListItem) getItem(i)).section;
            if (!isTheSame(previousSection, currentSection)) {
                newSectionIndex++;
                newSectionCounts = 1;
                this.mSections[newSectionIndex] = currentSection;
                previousSection = currentSection;
                this.mCounts[newSectionIndex] = 1;
            } else {
                this.mCounts[newSectionIndex] = newSectionCounts;
            }
        }
    }

    private synchronized void updateTotalCount() {
        String currentSection = null;
        int count = getCount();
        for (int i = 0; i < count; i++) {
            SectionListItem item = (SectionListItem) getItem(i);
            if (!isTheSame(currentSection, item.section)) {
                this.mSectionCounts++;
                currentSection = item.section;
            }
        }
        fillSections();
    }

    public synchronized String getSectionName(int position) {
        return null;
    }

    /* access modifiers changed from: protected */
    public Integer getLinkedPosition(int position) {
        return Integer.valueOf(position);
    }

    /* access modifiers changed from: protected */
    public synchronized void replaceSectionViewsInMaps(String section, View theView) {
        if (this.currentViewSections.containsKey(theView)) {
            this.currentViewSections.remove(theView);
        }
        this.currentViewSections.put(section, theView);
    }

    public View getView(final int position, final View convertView, ViewGroup parent) {
        boolean showHeader;
        int section = getSectionForPosition(position);
        if (getCount() == 1) {
            showHeader = true;
        } else {
            showHeader = getPositionForSection(section) == position;
        }
        SectionView sectionView = initView(position, convertView, parent, showHeader);
        if (showHeader) {
            final MySectionIndexer.IndexerRanger indexerRanger = this.mIndexer.getIndexerRanger(section);
            sectionView.header.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (indexerRanger != null) {
                        BaseSectionListAdapter.this.linkedListener.onHeaderItemClick(convertView, indexerRanger.start, indexerRanger.end);
                    } else {
                        BaseSectionListAdapter.this.linkedListener.onHeaderItemClick(convertView, position, -1);
                    }
                }
            });
        }
        return sectionView.view;
    }

    public class SectionView {
        public View header;
        public View view;

        public SectionView() {
        }
    }

    public int getRealPosition(int pos) {
        return pos - 1;
    }

    public synchronized View getTransparentSectionView() {
        return this.transparentSectionView;
    }

    /* access modifiers changed from: protected */
    public void sectionClicked(String section) {
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (this.linkedListener != null) {
            this.linkedListener.onSubItemClick(parent, view, getLinkedPosition(position).intValue(), id);
        }
    }

    public void setOnItemClickListener(SectionListItemClickListener linkedListener2) {
        this.linkedListener = linkedListener2;
    }

    public int getPinnedHeaderState(int position) {
        int realPosition = position;
        if (this.mIndexer == null || realPosition < 0) {
            return 0;
        }
        int nextSectionPosition = getPositionForSection(getSectionForPosition(realPosition) + 1);
        if (nextSectionPosition == -1 || realPosition != nextSectionPosition - 1) {
            return 1;
        }
        return 2;
    }

    public void configurePinnedHeader(View header, int position, int alpha) {
        int section = getSectionForPosition(position);
        try {
            setHeaderViewText(header, (String) this.mIndexer.getSections()[section]);
        } catch (Exception e) {
            String title = (String) this.mIndexer.getSections()[0];
            CorePreferences.ERROR("section e:" + section + ",title:" + title);
            setHeaderViewText(header, title);
            e.printStackTrace();
        }
    }

    public Object[] getSections() {
        if (this.mIndexer != null) {
            return this.mIndexer.getSections();
        }
        return new String[]{StringUtils.EMPTY};
    }

    public int getPositionForSection(int section) {
        if (this.mIndexer == null) {
            return -1;
        }
        return this.mIndexer.getPositionForSection(section);
    }

    public int getSectionForPosition(int position) {
        if (this.mIndexer != null) {
            return this.mIndexer.getSectionForPosition(position);
        }
        CorePreferences.ERROR("mIndexer is null");
        return -1;
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (view instanceof PinnedHeaderListView) {
            ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
        }
    }
}
