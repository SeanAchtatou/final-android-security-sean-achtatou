package com.house365.core.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class AutoFeedLayout extends ViewGroup {
    private static final int VIEW_MARGIN = 5;

    public AutoFeedLayout(Context context) {
        super(context);
    }

    public AutoFeedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoFeedLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        for (int index = 0; index < getChildCount(); index++) {
            getChildAt(index).measure(0, 0);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        Log.d("onLayout", "changed = " + changed + " left = " + l + " top = " + t + " right = " + r + " botom = " + b);
        int row = 0;
        int lengthX = l;
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            int width = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();
            lengthX += width + 5;
            int lengthY = ((height + 5) * row) + 5 + height + t;
            if (lengthX > r) {
                lengthX = width + 5 + l;
                row++;
                lengthY = ((height + 5) * row) + 5 + height + t;
            }
            Log.d("child onLayout", "i��" + i + "lengthX��" + lengthX + ",lengthY��" + lengthY);
            child.layout(lengthX - width, lengthY - height, lengthX, lengthY);
        }
    }
}
