package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class BounceInterpolator implements Interpolator {
    private EasingType.Type type;

    public BounceInterpolator(EasingType.Type type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float out(float t) {
        if (((double) t) < 0.36363636363636365d) {
            return 7.5625f * t * t;
        }
        if (((double) t) < 0.7272727272727273d) {
            float t2 = (float) (((double) t) - 0.5454545454545454d);
            return (7.5625f * t2 * t2) + 0.75f;
        } else if (((double) t) < 0.9090909090909091d) {
            float t3 = (float) (((double) t) - 0.8181818181818182d);
            return (7.5625f * t3 * t3) + 0.9375f;
        } else {
            float t4 = (float) (((double) t) - 0.9545454545454546d);
            return (7.5625f * t4 * t4) + 0.984375f;
        }
    }

    private float in(float t) {
        return 1.0f - out(1.0f - t);
    }

    private float inout(float t) {
        if (t < 0.5f) {
            return in(t * 2.0f) * 0.5f;
        }
        return (out((t * 2.0f) - 1.0f) * 0.5f) + 0.5f;
    }
}
