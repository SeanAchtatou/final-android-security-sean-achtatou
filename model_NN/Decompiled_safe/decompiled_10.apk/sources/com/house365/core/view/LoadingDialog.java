package com.house365.core.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import com.house365.core.R;

public class LoadingDialog extends Dialog {
    private Context context;
    private String loadingText;
    private TextView loading_text;
    private int resLayout = R.layout.dialog_loading;

    public LoadingDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public LoadingDialog(Context context2, int theme) {
        super(context2, theme);
        this.context = context2;
    }

    public LoadingDialog(Context context2, int theme, int resid) {
        super(context2, theme);
        this.context = context2;
        this.loadingText = context2.getResources().getString(resid);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.resLayout != 0) {
            setContentView(this.resLayout);
            if (this.resLayout == R.layout.dialog_loading) {
                this.loading_text = (TextView) findViewById(R.id.loading_text);
                this.loading_text.setText(this.loadingText);
            }
        }
    }

    public void setMessage(String message) {
        this.loadingText = message;
    }

    public void setMessage(int resid) {
        this.loadingText = this.context.getResources().getString(resid);
    }

    public int getResLayout() {
        return this.resLayout;
    }

    public void setResLayout(int resLayout2) {
        this.resLayout = resLayout2;
    }

    public void dismiss() {
        super.dismiss();
    }
}
