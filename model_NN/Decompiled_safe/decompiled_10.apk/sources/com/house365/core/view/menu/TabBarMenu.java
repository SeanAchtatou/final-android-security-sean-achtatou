package com.house365.core.view.menu;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.R;
import com.house365.core.view.common.ScrollerView;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

public class TabBarMenu extends RelativeLayout {
    private final int MSG_ANIMATION_END = 2000;
    private final int MSG_ANIMATION_START = 1000;
    private Handler animatingHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1000:
                    TabBarMenu.this.mAnimating = true;
                    if (TabBarMenu.this.tabBarChangeListener != null) {
                        TabBarMenu.this.tabBarChangeListener.onChangeBefore(TabBarMenu.this.currIndex, ((Integer) msg.obj).intValue());
                        return;
                    }
                    return;
                case 2000:
                    TabBarMenu.this.mAnimating = false;
                    int currentX = TabBarMenu.this.currIndex * TabBarMenu.this.tabBarWitdh;
                    int scrollX = Math.abs(TabBarMenu.this.tabselectedImage.getScrollX());
                    if (currentX != scrollX) {
                        TabBarMenu.this.tabselectedImage.beginScroll(scrollX - currentX, 0, 30);
                    }
                    TabBarMenu.this.setCurrentStyle();
                    if (TabBarMenu.this.tabBarChangeListener != null) {
                        TabBarMenu.this.tabBarChangeListener.onChangeAfter(TabBarMenu.this.currIndex);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private int animationFactor = 5;
    /* access modifiers changed from: private */
    public int currIndex = 0;
    /* access modifiers changed from: private */
    public int lastX;
    /* access modifiers changed from: private */
    public boolean mAnimating = false;
    private boolean mBringToFront;
    private Context mContext;
    private GestureDetector mGesture = null;
    /* access modifiers changed from: private */
    public TabBarChangeListener tabBarChangeListener;
    /* access modifiers changed from: private */
    public int tabBarWitdh = 0;
    private List<TextView> tabTitleViewList = new ArrayList();
    private float tab_bar_height;
    private float tab_bar_width;
    private int tab_color;
    private int tab_num;
    private int tab_selected_background;
    private int tab_selected_color;
    private String tab_text;
    private String[] tab_text_arrays;
    private float tab_textsize;
    /* access modifiers changed from: private */
    public ScrollerView tabselectedImage;

    public interface TabBarChangeListener {
        void onChangeAfter(int i);

        void onChangeBefore(int i, int i2);
    }

    public TabBarMenu(Context context) {
        super(context);
        this.mContext = context;
    }

    public TabBarMenu(Context context, AttributeSet attrs) throws Exception {
        super(context, attrs);
        this.mContext = context;
        initAttributes(context, attrs);
        initContent(context);
    }

    public TabBarMenu(Context context, AttributeSet attrs, int defStyle) throws Exception {
        super(context, attrs, defStyle);
        this.mContext = context;
        initAttributes(context, attrs);
        initContent(context);
    }

    private void initAttributes(Context context, AttributeSet attributeSet) throws Exception {
        String layoutwith = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_width");
        String layoutheight = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height");
        if (!isDip(layoutwith) || !isDip(layoutheight)) {
            throw new Exception("layout_width or layout_height muse be dip");
        }
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        this.tab_bar_width = (float) getPixOfDip(dm, layoutwith);
        this.tab_bar_height = (float) getPixOfDip(dm, layoutheight);
        TypedArray styledAttrs = this.mContext.obtainStyledAttributes(attributeSet, R.styleable.FlowTabBarView);
        this.tab_selected_background = styledAttrs.getResourceId(0, 0);
        this.tab_color = styledAttrs.getColor(1, 0);
        this.tab_selected_color = styledAttrs.getColor(2, 0);
        this.tab_textsize = styledAttrs.getFloat(3, 14.0f);
        CharSequence tabtxt = styledAttrs.getText(4);
        if (tabtxt != null) {
            this.tab_text = tabtxt.toString();
            this.tab_text_arrays = init_tabtitle(this.tab_text);
        }
        if (this.tab_bar_height == SystemUtils.JAVA_VERSION_FLOAT) {
            this.tab_bar_height = -2.0f;
        }
    }

    private boolean isDip(String s) {
        return s.indexOf("dip") > 0 || s.indexOf("dp") > 0;
    }

    private int getPixOfDip(DisplayMetrics dm, String s) {
        return dip2pix(dm, Float.parseFloat(s.replaceAll("dip", StringUtils.EMPTY).replaceAll("dp", StringUtils.EMPTY)));
    }

    public static int dip2pix(DisplayMetrics dm, float dipValue) {
        return (int) ((dipValue * dm.density) + 0.5f);
    }

    private void initContent(Context context) {
        this.mGesture = new GestureDetector(this.mContext, new MyGestureListener());
        this.mGesture.isLongpressEnabled();
        setClickable(true);
        setLayoutParams(new RelativeLayout.LayoutParams((int) this.tab_bar_width, (int) this.tab_bar_height));
        this.tabBarWitdh = Math.round(this.tab_bar_width / ((float) this.tab_num));
        this.tabselectedImage = new ScrollerView(context);
        this.tabselectedImage.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.tabselectedImage.setBackgroundResource(17170445);
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(this.tabBarWitdh, (int) this.tab_bar_height));
        imageView.setBackgroundResource(this.tab_selected_background);
        this.tabselectedImage.addView(imageView);
        addView(this.tabselectedImage);
        LinearLayout clayout = new LinearLayout(context);
        clayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        clayout.getWidth();
        clayout.setWeightSum((float) this.tab_num);
        addView(clayout);
        if (this.tab_num > 0) {
            for (int i = 0; i < this.tab_num; i++) {
                TextView tabTitle = new TextView(context);
                tabTitle.setLayoutParams(new LinearLayout.LayoutParams(this.tabBarWitdh, (int) this.tab_bar_height, 1.0f));
                tabTitle.setTextColor(this.tab_color);
                tabTitle.setTextSize(this.tab_textsize);
                tabTitle.setGravity(17);
                tabTitle.setText(this.tab_text_arrays[i]);
                tabTitle.setId(i);
                clayout.addView(tabTitle);
                this.tabTitleViewList.add(tabTitle);
                if (i == this.currIndex) {
                    tabTitle.setTextColor(this.tab_selected_color);
                } else {
                    tabTitle.setTextColor(this.tab_color);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewParent parent = getParent();
        if (parent != null && (parent instanceof FrameLayout)) {
            this.mBringToFront = true;
        }
    }

    private String[] init_tabtitle(String text) {
        String[] text_arrays = null;
        if (text == null || text.indexOf(",") <= 0) {
            return text_arrays;
        }
        String[] text_arrays2 = text.split(",");
        this.tab_num = text_arrays2.length;
        return text_arrays2;
    }

    public int getCurrIndex() {
        return this.currIndex;
    }

    public void setCurrIndex(int currIndex2) {
        int currentX = this.currIndex * this.tabBarWitdh;
        int needMoveX = currIndex2 * this.tabBarWitdh;
        int duration = Math.abs(currentX - needMoveX) * this.animationFactor;
        this.animatingHandler.sendMessage(this.animatingHandler.obtainMessage(1000, Integer.valueOf(duration)));
        this.tabselectedImage.beginScroll(currentX - needMoveX, 0, duration);
        this.animatingHandler.sendEmptyMessageDelayed(2000, (long) duration);
        this.currIndex = currIndex2;
    }

    public int getTabnum() {
        return this.tab_num;
    }

    public void setTabnum(int tabnum) {
        this.tab_num = tabnum;
    }

    public List<TextView> getTabTitleViewList() {
        return this.tabTitleViewList;
    }

    public void setTabTitle(String tabTitle) {
        this.tab_text_arrays = init_tabtitle(tabTitle);
        int i = 0;
        for (TextView tv : this.tabTitleViewList) {
            tv.setText(this.tab_text_arrays[i]);
            i++;
        }
    }

    public TabBarChangeListener getTabBarChangeListener() {
        return this.tabBarChangeListener;
    }

    public void setTabBarChangeListener(TabBarChangeListener tabBarChangeListener2) {
        this.tabBarChangeListener = tabBarChangeListener2;
    }

    public int getAnimationFactor() {
        return this.animationFactor;
    }

    public void setAnimationFactor(int animationFactor2) {
        this.animationFactor = animationFactor2;
    }

    public void setCurrentStyle() {
        int i = 0;
        for (TextView tv : this.tabTitleViewList) {
            if (i == this.currIndex) {
                tv.setTextColor(this.tab_selected_color);
            } else {
                tv.setTextColor(this.tab_color);
            }
            i++;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        if (this.mAnimating) {
            return true;
        }
        int action = event.getAction();
        if (action == 0 && this.mBringToFront) {
            bringToFront();
        }
        if (!this.mGesture.onTouchEvent(event) && (action == 1 || action == 3)) {
            this.currIndex = (int) Math.round(((double) this.lastX) / ((double) this.tabBarWitdh));
            int needMoveX = this.currIndex * this.tabBarWitdh;
            int duration = Math.abs(this.lastX - needMoveX) * this.animationFactor;
            this.animatingHandler.sendMessage(this.animatingHandler.obtainMessage(1000, Integer.valueOf(duration)));
            this.tabselectedImage.beginScroll(this.lastX - needMoveX, 0, duration);
            this.lastX -= needMoveX;
            this.animatingHandler.sendEmptyMessageDelayed(2000, (long) duration);
        }
        return super.dispatchTouchEvent(event);
    }

    /* access modifiers changed from: private */
    public int ensureX(int x) {
        int downX = x;
        if (downX < 0) {
            downX = 0;
        }
        if (((float) downX) > this.tab_bar_width - ((float) this.tabBarWitdh)) {
            return (int) (this.tab_bar_width - ((float) this.tabBarWitdh));
        }
        return downX;
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        MyGestureListener() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }

        public boolean onDown(MotionEvent e) {
            TabBarMenu.this.lastX = (int) (e.getX() - ((float) (TabBarMenu.this.tabBarWitdh / 2)));
            TabBarMenu.this.lastX = TabBarMenu.this.ensureX(TabBarMenu.this.lastX);
            TabBarMenu.this.tabselectedImage.scrollTo(-TabBarMenu.this.lastX, 0);
            return false;
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (e2.getAction() == 2) {
                TabBarMenu.this.lastX = Math.round(((float) TabBarMenu.this.lastX) - distanceX);
                TabBarMenu.this.lastX = TabBarMenu.this.ensureX(TabBarMenu.this.lastX);
                TabBarMenu.this.tabselectedImage.scrollTo(-TabBarMenu.this.lastX, 0);
            }
            return false;
        }

        public void onShowPress(MotionEvent e) {
            super.onShowPress(e);
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }
    }
}
