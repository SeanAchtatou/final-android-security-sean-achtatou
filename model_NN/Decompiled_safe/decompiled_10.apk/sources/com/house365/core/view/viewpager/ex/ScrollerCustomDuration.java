package com.house365.core.view.viewpager.ex;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class ScrollerCustomDuration extends Scroller {
    private double mScrollDuration = 1.0d;

    public ScrollerCustomDuration(Context context) {
        super(context);
    }

    public ScrollerCustomDuration(Context context, Interpolator interpolator) {
        super(context, interpolator);
    }

    public void setScrollDuration(long scrollDuration) {
        this.mScrollDuration = (double) scrollDuration;
    }

    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, (int) this.mScrollDuration);
    }
}
