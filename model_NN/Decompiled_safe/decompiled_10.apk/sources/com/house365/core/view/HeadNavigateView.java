package com.house365.core.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.house365.core.R;
import org.apache.commons.lang.SystemUtils;

public class HeadNavigateView extends RelativeLayout {
    private int bg_drawable;
    private int btn_color;
    private Button btn_left;
    private Button btn_right;
    private float btn_textsize;
    private int left_drawable;
    private int right_drawable;
    private String strBtnLeft;
    private String strBtnRight;
    private String strTitle;
    private int title_color;
    private float title_textsize;
    private TextView tv_center;

    public HeadNavigateView(Context context) {
        super(context);
    }

    public HeadNavigateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
        initContent(context);
    }

    private void initAttributes(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray styledAttrs = context.obtainStyledAttributes(attributeSet, R.styleable.HeadNavigateView);
            CharSequence t1 = styledAttrs.getText(1);
            CharSequence t2 = styledAttrs.getText(2);
            CharSequence t3 = styledAttrs.getText(3);
            this.left_drawable = styledAttrs.getResourceId(4, 0);
            this.right_drawable = styledAttrs.getResourceId(5, 0);
            this.bg_drawable = styledAttrs.getResourceId(0, 0);
            this.btn_color = styledAttrs.getColor(6, 0);
            this.title_color = styledAttrs.getColor(8, 0);
            this.title_textsize = styledAttrs.getDimension(9, 20.0f);
            this.btn_textsize = styledAttrs.getDimension(7, 16.0f);
            styledAttrs.recycle();
            if (t1 != null) {
                this.strBtnLeft = t1.toString();
            }
            if (t2 != null) {
                this.strBtnRight = t2.toString();
            }
            if (t3 != null) {
                this.strTitle = t3.toString();
            }
            if (this.title_color == 0) {
                this.title_color = context.getResources().getColor(R.color.white);
            }
            if (this.btn_color == 0) {
                this.btn_color = context.getResources().getColor(R.color.white);
            }
        }
    }

    private void initContent(Context context) {
        View headnavView = LayoutInflater.from(context).inflate(R.layout.headnavigate, (ViewGroup) null);
        this.btn_left = (Button) headnavView.findViewById(R.id.btn_left);
        this.btn_right = (Button) headnavView.findViewById(R.id.btn_right);
        this.tv_center = (TextView) headnavView.findViewById(R.id.tv_center);
        if (this.left_drawable != 0) {
            this.btn_left.setBackgroundResource(this.left_drawable);
            this.btn_left.setVisibility(0);
        }
        if (this.right_drawable != 0) {
            this.btn_right.setBackgroundResource(this.right_drawable);
            this.btn_right.setVisibility(0);
        }
        if (this.strBtnLeft != null) {
            this.btn_left.setTextColor(this.btn_color);
            this.btn_left.setText(this.strBtnLeft);
            if (this.btn_textsize > SystemUtils.JAVA_VERSION_FLOAT) {
                this.btn_left.setTextSize(this.btn_textsize);
            }
        }
        if (this.strBtnRight != null) {
            this.btn_right.setTextColor(this.btn_color);
            this.btn_right.setText(this.strBtnRight);
            if (this.btn_textsize > SystemUtils.JAVA_VERSION_FLOAT) {
                this.btn_right.setTextSize(this.btn_textsize);
            }
        }
        if (this.strTitle != null) {
            this.tv_center.setText(this.strTitle);
            this.tv_center.setTextColor(this.title_color);
            this.tv_center.setVisibility(0);
            if (this.title_textsize > SystemUtils.JAVA_VERSION_FLOAT) {
                this.tv_center.setTextSize(this.title_textsize);
            }
        }
        addView(headnavView);
    }

    public Button getBtn_left() {
        return this.btn_left;
    }

    public Button getBtn_right() {
        return this.btn_right;
    }

    public TextView getTv_center() {
        return this.tv_center;
    }

    public String getStrBtnLeft() {
        return this.strBtnLeft;
    }

    public String getStrBtnRight() {
        return this.strBtnRight;
    }

    public String getStrTitle() {
        return this.strTitle;
    }

    public int getLeft_drawable() {
        return this.left_drawable;
    }

    public int getRight_drawable() {
        return this.right_drawable;
    }

    public int getBg_drawable() {
        return this.bg_drawable;
    }

    public int getBtn_color() {
        return this.btn_color;
    }

    public int getTitle_color() {
        return this.title_color;
    }

    public float getTitle_textsize() {
        return this.title_textsize;
    }

    public float getBtn_textsize() {
        return this.btn_textsize;
    }

    public void setBtn_left(Button btn_left2) {
        this.btn_left = btn_left2;
    }

    public void setBtn_right(Button btn_right2) {
        this.btn_right = btn_right2;
    }

    public void setTv_center(TextView tv_center2) {
        this.tv_center = tv_center2;
    }

    public void setStrBtnLeft(String strBtnLeft2) {
        this.strBtnLeft = strBtnLeft2;
    }

    public void setStrBtnRight(String strBtnRight2) {
        this.strBtnRight = strBtnRight2;
    }

    public void setStrTitle(String strTitle2) {
        this.strTitle = strTitle2;
    }

    public void setLeft_drawable(int left_drawable2) {
        this.left_drawable = left_drawable2;
    }

    public void setRight_drawable(int right_drawable2) {
        this.right_drawable = right_drawable2;
    }

    public void setBg_drawable(int bg_drawable2) {
        this.bg_drawable = bg_drawable2;
    }

    public void setBtn_color(int btn_color2) {
        this.btn_color = btn_color2;
    }

    public void setTitle_color(int title_color2) {
        this.title_color = title_color2;
    }

    public void setTitle_textsize(float title_textsize2) {
        this.title_textsize = title_textsize2;
    }

    public void setBtn_textsize(float btn_textsize2) {
        this.btn_textsize = btn_textsize2;
    }

    public void setTvTitleText(CharSequence text) {
        getTv_center().setText(text);
        getTv_center().setVisibility(0);
        getTv_center().setTextColor(this.title_color);
        getTv_center().setTextSize(this.title_textsize);
    }

    public void setTvTitleText(int textId) {
        getTv_center().setText(textId);
        getTv_center().setVisibility(0);
        getTv_center().setTextColor(this.title_color);
        getTv_center().setTextSize(this.title_textsize);
    }
}
