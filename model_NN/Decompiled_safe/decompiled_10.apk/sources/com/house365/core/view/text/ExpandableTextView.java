package com.house365.core.view.text;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.TextView;

public class ExpandableTextView extends TextView {
    public static final int STATE_HIDE = 0;
    public static final int STATE_SHOW = 1;
    int currentLine;
    private int extend_height = 5;
    int lines;
    private int maxLines;
    int state = 0;
    private TextLineListener textLineListener;

    public interface TextLineListener {
        void hasMore(boolean z);
    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getLines(attrs);
    }

    private int getLines(AttributeSet attrs) {
        try {
            this.lines = Integer.parseInt(attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "lines"));
        } catch (NumberFormatException e) {
            this.lines = -1;
        }
        return this.lines;
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getLines(attrs);
    }

    public ExpandableTextView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } catch (ArrayIndexOutOfBoundsException e) {
            setText(getText().toString());
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void setGravity(int gravity) {
        try {
            super.setGravity(gravity);
        } catch (ArrayIndexOutOfBoundsException e) {
            setText(getText().toString());
            super.setGravity(gravity);
        }
    }

    public void setText(CharSequence text, TextView.BufferType type) {
        try {
            super.setText(text, type);
        } catch (ArrayIndexOutOfBoundsException e) {
            setText(text.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.maxLines <= 0) {
            this.maxLines = getLineCount();
            boolean has = false;
            if (this.maxLines > this.lines) {
                has = true;
                setEllipsize(TextUtils.TruncateAt.END);
                setLines(this.lines);
            }
            getLayoutParams().height = (this.lines * getLineHeight()) + this.extend_height;
            if (this.textLineListener != null) {
                this.textLineListener.hasMore(has);
            }
        }
    }

    public void show(int duration) {
        setLines(this.maxLines);
        this.state = 1;
        ExpandAnimation anim = new ExpandAnimation(this, (this.lines * getLineHeight()) + this.extend_height, (this.maxLines * getLineHeight()) + this.extend_height);
        anim.setDuration((long) duration);
        startAnimation(anim);
    }

    public void hide(int duration) {
        this.state = 0;
        ExpandAnimation anim = new ExpandAnimation(this, (this.maxLines * getLineHeight()) + this.extend_height, (this.lines * getLineHeight()) + this.extend_height);
        anim.setDuration((long) duration);
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ExpandableTextView.this.setLines(ExpandableTextView.this.lines);
                ExpandableTextView.this.setEllipsize(TextUtils.TruncateAt.END);
            }
        });
        startAnimation(anim);
    }

    public TextLineListener getTextLineListener() {
        return this.textLineListener;
    }

    public void setTextLineListener(TextLineListener textLineListener2) {
        this.textLineListener = textLineListener2;
    }

    class ExpandAnimation extends Animation {
        private final int _finishHeight;
        private final int _startHeight;
        private final TextView _view;

        public ExpandAnimation(TextView view, int startHeight, int finishHeight) {
            this._view = view;
            this._startHeight = startHeight;
            this._finishHeight = finishHeight;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            this._view.getLayoutParams().height = (int) ((((float) (this._finishHeight - this._startHeight)) * interpolatedTime) + ((float) this._startHeight));
            this._view.requestLayout();
        }

        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        public boolean willChangeBounds() {
            return true;
        }
    }
}
