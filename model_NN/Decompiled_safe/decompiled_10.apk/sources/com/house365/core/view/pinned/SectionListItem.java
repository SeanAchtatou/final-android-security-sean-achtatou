package com.house365.core.view.pinned;

public class SectionListItem {
    public Object item;
    public String section;

    public SectionListItem(Object item2, String section2) {
        this.item = item2;
        this.section = section2;
    }

    public Object getItem() {
        return this.item;
    }

    public String getSection() {
        return this.section;
    }

    public void setItem(Object item2) {
        this.item = item2;
    }

    public void setSection(String section2) {
        this.section = section2;
    }

    public String toString() {
        return this.item.toString();
    }
}
