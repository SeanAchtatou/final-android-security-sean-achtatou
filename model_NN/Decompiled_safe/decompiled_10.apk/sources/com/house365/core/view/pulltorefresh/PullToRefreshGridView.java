package com.house365.core.view.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.house365.core.R;
import com.house365.core.view.pulltorefresh.internal.EmptyViewMethodAccessor;

public class PullToRefreshGridView extends PullToRefreshAdapterViewBase<GridView> {

    class InternalGridView extends GridView implements EmptyViewMethodAccessor {
        public InternalGridView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public void setEmptyView(View emptyView) {
            PullToRefreshGridView.this.setEmptyView(emptyView);
        }

        public void setEmptyViewInternal(View emptyView) {
            super.setEmptyView(emptyView);
        }

        public ContextMenu.ContextMenuInfo getContextMenuInfo() {
            return super.getContextMenuInfo();
        }
    }

    public PullToRefreshGridView(Context context) {
        super(context);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshGridView(Context context, int mode) {
        super(context, mode);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDisableScrollingWhileRefreshing(false);
    }

    /* access modifiers changed from: protected */
    public final GridView createRefreshableView(Context context, AttributeSet attrs) {
        GridView gv = new InternalGridView(context, attrs);
        gv.setId(R.id.gridview);
        return gv;
    }

    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return ((InternalGridView) getRefreshableView()).getContextMenuInfo();
    }

    public void onRefreshComplete(CharSequence lastUpdated) {
        this.mHeaderLayout.setLastUpdate(lastUpdated);
        super.onRefreshComplete();
    }

    public void setFooterViewVisible(int visibility) {
        if (visibility == 8) {
            removeView(this.mFooterLayout);
            this.mMode = 1;
            this.mCurrentMode = 1;
            setPadding(0, -this.mHeaderHeight, 0, 0);
            return;
        }
        this.mMode = 3;
        addView(this.mFooterLayout);
        setPadding(0, -this.mHeaderHeight, 0, -this.mHeaderHeight);
    }

    public void setAdapter(ListAdapter adapter) {
        ((GridView) getRefreshableView()).setAdapter(adapter);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        ((GridView) getRefreshableView()).setOnItemClickListener(listener);
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
        ((GridView) getRefreshableView()).setOnItemLongClickListener(listener);
    }

    public void showRefreshView() {
        setHeaderRefreshingInternal(true);
    }

    public GridView getActureListView() {
        return (GridView) getRefreshableView();
    }
}
