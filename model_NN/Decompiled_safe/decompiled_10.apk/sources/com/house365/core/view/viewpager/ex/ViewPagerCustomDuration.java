package com.house365.core.view.viewpager.ex;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.animation.Interpolator;
import java.lang.reflect.Field;

public class ViewPagerCustomDuration extends ViewPager {
    private boolean consumeTouchEvent = false;
    /* access modifiers changed from: private */
    public Handler handler;
    private ScrollerCustomDuration mScroller = null;
    /* access modifiers changed from: private */
    public long timeSpan = 3000;

    public ViewPagerCustomDuration(Context context, AttributeSet attrs) {
        super(context, attrs);
        postInitViewPager();
    }

    public ViewPagerCustomDuration(Context context) {
        super(context);
        postInitViewPager();
    }

    private void postInitViewPager() {
        Class<?> viewpager = ViewPager.class;
        try {
            Field scroller = viewpager.getDeclaredField("mScroller");
            scroller.setAccessible(true);
            Field interpolator = viewpager.getDeclaredField("sInterpolator");
            interpolator.setAccessible(true);
            this.mScroller = new ScrollerCustomDuration(getContext(), (Interpolator) interpolator.get(null));
            scroller.set(this, this.mScroller);
        } catch (Exception e) {
        }
    }

    public void setScrollDuration(long scrollDuration) {
        this.mScroller.setScrollDuration(scrollDuration);
    }

    public void startAutoFlowTimer() {
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (ViewPagerCustomDuration.this.getAdapter() != null && ViewPagerCustomDuration.this.getAdapter().getCount() != 0) {
                    ViewPagerCustomDuration.this.setCurrentItem((ViewPagerCustomDuration.this.getCurrentItem() + 1) % ViewPagerCustomDuration.this.getAdapter().getCount(), true);
                    sendMessageDelayed(ViewPagerCustomDuration.this.handler.obtainMessage(0), ViewPagerCustomDuration.this.timeSpan);
                }
            }
        };
        this.handler.sendMessageDelayed(this.handler.obtainMessage(0), this.timeSpan);
    }

    public void stopAutoFlowTimer() {
        if (this.handler != null) {
            this.handler.removeMessages(0);
        }
        this.handler = null;
    }

    public void setTimeSpan(long timeSpan2) {
        this.timeSpan = timeSpan2;
    }

    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        return super.onInterceptTouchEvent(arg0);
    }

    public boolean onTouchEvent(MotionEvent arg0) {
        boolean flag = super.onTouchEvent(arg0);
        if (this.consumeTouchEvent) {
            return true;
        }
        return flag;
    }

    public void setConsumeTouchEvent(boolean consumeTouchEvent2) {
        this.consumeTouchEvent = consumeTouchEvent2;
    }
}
