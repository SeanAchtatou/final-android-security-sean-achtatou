package com.house365.core.view.interpolator;

import android.view.animation.Interpolator;
import com.house365.core.view.interpolator.EasingType;
import org.apache.commons.lang.SystemUtils;

public class CircInterpolator implements Interpolator {
    private EasingType.Type type;

    public CircInterpolator(EasingType.Type type2) {
        this.type = type2;
    }

    public float getInterpolation(float t) {
        if (this.type == EasingType.Type.IN) {
            return in(t);
        }
        if (this.type == EasingType.Type.OUT) {
            return out(t);
        }
        if (this.type == EasingType.Type.INOUT) {
            return inout(t);
        }
        return SystemUtils.JAVA_VERSION_FLOAT;
    }

    private float in(float t) {
        return (float) (-(Math.sqrt((double) (1.0f - (t * t))) - 1.0d));
    }

    private float out(float t) {
        float t2 = t - 1.0f;
        return (float) Math.sqrt((double) (1.0f - (t2 * t2)));
    }

    private float inout(float t) {
        float t2 = t * 2.0f;
        if (t2 < 1.0f) {
            return (float) (-0.5d * (Math.sqrt((double) (1.0f - (t2 * t2))) - 1.0d));
        }
        float t3 = t2 - 2.0f;
        return (float) (0.5d * (Math.sqrt((double) (1.0f - (t3 * t3))) + 1.0d));
    }
}
