package com.house365.core.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

public abstract class ListDialog extends AlertDialog {
    public abstract void preparedCreate(Bundle bundle);

    public ListDialog(Context context) {
        super(context);
    }

    public ListDialog(Context context, int theme) {
        super(context, theme);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preparedCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
    }

    public void dismiss() {
        super.dismiss();
    }
}
