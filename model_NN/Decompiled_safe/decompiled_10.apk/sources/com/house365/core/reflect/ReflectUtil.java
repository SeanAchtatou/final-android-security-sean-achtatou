package com.house365.core.reflect;

import com.house365.core.json.JSONArray;
import com.house365.core.json.JSONException;
import com.house365.core.json.JSONObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class ReflectUtil {
    public static final String CLASS_SUFFIX = ".class";
    public static final String JAR_SUFFIX = ".jar";
    public static final String SEARCH_TYPE_FILE = "file";
    public static final String SEARCH_TYPE_JAR = "jar";

    public static Object invokeMethod(Object owner, String methodName, Object[] args) throws ReflectException {
        try {
            Class clazz = owner.getClass();
            Class[] argsClass = null;
            if (args != null) {
                argsClass = new Class[args.length];
                for (int i = 0; i < args.length; i++) {
                    argsClass[i] = args[i].getClass();
                }
            }
            Method method = clazz.getDeclaredMethod(methodName, argsClass);
            if (isNeedSetAccessible(method.getModifiers())) {
                method.setAccessible(true);
            }
            return method.invoke(owner, args);
        } catch (Exception e) {
            throw new ReflectException("Invoke method occurs exception", e);
        }
    }

    public static Object invokeMethod(Object owner, String methodName, Class[] clazzs, Object[] args) throws ReflectException {
        try {
            Method method = owner.getClass().getDeclaredMethod(methodName, clazzs);
            if (isNeedSetAccessible(method.getModifiers())) {
                method.setAccessible(true);
            }
            return method.invoke(owner, args);
        } catch (Exception e) {
            throw new ReflectException("Invoke method occurs exception", e);
        }
    }

    public static Object invokeStaticMethod(String className, String methodName, Class[] clazzs, Object[] args) throws ReflectException {
        try {
            Class clazz = Class.forName(className);
            Method method = clazz.getDeclaredMethod(methodName, clazz);
            if (isNeedSetAccessible(method.getModifiers())) {
                method.setAccessible(true);
            }
            return method.invoke(null, args);
        } catch (Exception e) {
            throw new ReflectException("Invoke static method occurs exception", e);
        }
    }

    public static Object invokeStaticMethod(String className, String methodName, Object[] args) throws ReflectException {
        try {
            Class clazz = Class.forName(className);
            Class[] argsClass = null;
            if (args != null) {
                argsClass = new Class[args.length];
                for (int i = 0; i < args.length; i++) {
                    argsClass[i] = args[i].getClass();
                }
            }
            Method method = clazz.getDeclaredMethod(methodName, argsClass);
            if (isNeedSetAccessible(method.getModifiers())) {
                method.setAccessible(true);
            }
            return method.invoke(null, args);
        } catch (Exception e) {
            throw new ReflectException("Invoke static method occurs exception", e);
        }
    }

    public static Method getMethod(Object owner, String methodName, int paramsNumber) {
        for (Method method : owner.getClass().getDeclaredMethods()) {
            if (method.getName().equals(methodName) && method.getGenericParameterTypes().length == paramsNumber) {
                return method;
            }
        }
        return null;
    }

    public static Method getMethod(Class clazz, String methodName, int paramsNumber) {
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getName().equals(methodName) && method.getGenericParameterTypes().length == paramsNumber) {
                return method;
            }
        }
        return null;
    }

    public static Object newInstance(String className, Object[] args) throws Exception {
        return newInstance(Class.forName(className), args);
    }

    public static Object newInstance(Class clazz, Object[] args) throws ReflectException {
        try {
            Class[] argsClass = null;
            if (args != null) {
                argsClass = new Class[args.length];
                for (int i = 0; i < args.length; i++) {
                    argsClass[i] = args[i].getClass();
                }
            }
            Constructor constructor = clazz.getDeclaredConstructor(argsClass);
            if (isNeedSetAccessible(constructor.getModifiers())) {
                constructor.setAccessible(true);
            }
            return constructor.newInstance(new Object[0]);
        } catch (Exception e) {
            throw new ReflectException("New instance occurs exception", e);
        }
    }

    private static boolean isNeedSetAccessible(int modefiers) {
        if (Modifier.isPrivate(modefiers) || Modifier.isProtected(modefiers)) {
            return true;
        }
        return false;
    }

    public static Object[] castDataType(Class[] clazzs, String[] objects) throws ReflectException {
        if (clazzs.length != objects.length) {
            return null;
        }
        Object[] rObjects = new Object[objects.length];
        for (int i = 0; i < clazzs.length; i++) {
            rObjects[i] = getObjectFromString(clazzs[i], objects[i]);
        }
        return rObjects;
    }

    public static boolean isSimpleDataType(Class clazz) {
        if (clazz == Boolean.TYPE || clazz == Character.TYPE || clazz == Byte.TYPE || clazz == Short.TYPE || clazz == Integer.TYPE || clazz == Long.TYPE || clazz == Float.TYPE || clazz == Double.TYPE || clazz == Boolean.class || clazz == Character.class || clazz == Byte.class || clazz == Short.class || clazz == Integer.class || clazz == Long.class || clazz == Float.class || clazz == Double.class || clazz == String.class) {
            return true;
        }
        return false;
    }

    public static boolean isArrayDataType(Object o) {
        if ((o instanceof boolean[]) || (o instanceof char[]) || (o instanceof byte[]) || (o instanceof short[]) || (o instanceof int[]) || (o instanceof long[]) || (o instanceof float[]) || (o instanceof double[]) || (o instanceof Boolean[]) || (o instanceof Character[]) || (o instanceof Byte[]) || (o instanceof Short[]) || (o instanceof Integer[]) || (o instanceof Long[]) || (o instanceof Float[]) || (o instanceof Double[]) || (o instanceof String[]) || (o instanceof Collection)) {
            return true;
        }
        return false;
    }

    public static boolean isCollectionDataType(Object o) {
        if (o instanceof Collection) {
            return true;
        }
        return false;
    }

    public static boolean isListType(Class clazz) {
        if (clazz == List.class) {
            return true;
        }
        return false;
    }

    public static Class getObjectClass(Class clazz) {
        Class rClazz = clazz;
        if (clazz == Boolean.TYPE) {
            return Boolean.class;
        }
        if (clazz == Character.TYPE) {
            return Character.class;
        }
        if (clazz == Byte.TYPE) {
            return Byte.class;
        }
        if (clazz == Short.TYPE) {
            return Short.class;
        }
        if (clazz == Integer.TYPE) {
            return Integer.class;
        }
        if (clazz == Long.TYPE) {
            return Long.class;
        }
        if (clazz == Float.TYPE) {
            return Float.class;
        }
        if (clazz == Double.TYPE) {
            return Double.class;
        }
        return rClazz;
    }

    /* JADX INFO: Multiple debug info for r8v19 java.util.ArrayList: [D('obj' java.lang.String), D('obj' java.util.ArrayList)] */
    public static Object getObjectFromString(Field field, String s) throws ReflectException {
        Class clazz = field.getType();
        if (clazz == Boolean.TYPE || clazz == Boolean.class) {
            try {
                return Boolean.valueOf(Boolean.parseBoolean(s));
            } catch (Exception e) {
                return false;
            }
        } else if (clazz == Character.TYPE || clazz == Character.class) {
            try {
                return Character.valueOf(s.charAt(0));
            } catch (Exception e2) {
                return -1;
            }
        } else if (clazz == Byte.TYPE || clazz == Byte.class) {
            try {
                return Byte.valueOf(Byte.parseByte(s));
            } catch (Exception e3) {
                return -1;
            }
        } else if (clazz == Short.TYPE || clazz == Short.class) {
            try {
                return Short.valueOf(Short.parseShort(s));
            } catch (Exception e4) {
                return -1;
            }
        } else if (clazz == Integer.TYPE || clazz == Integer.class) {
            try {
                return Integer.valueOf(Integer.parseInt(s));
            } catch (Exception e5) {
                return -1;
            }
        } else if (clazz == Long.TYPE || clazz == Long.class) {
            try {
                return Long.valueOf(Long.parseLong(s));
            } catch (Exception e6) {
                return -1;
            }
        } else if (clazz == Float.TYPE || clazz == Float.class) {
            try {
                return Float.valueOf(Float.parseFloat(s));
            } catch (Exception e7) {
                return -1;
            }
        } else if (clazz == Double.TYPE || clazz == Double.class) {
            try {
                return Double.valueOf(Double.parseDouble(s));
            } catch (Exception e8) {
                return -1;
            }
        } else if (clazz == String.class) {
            if (s == null || StringUtils.EMPTY.equals(s) || "null".equalsIgnoreCase(s)) {
                return null;
            }
            return s;
        } else if (isListType(clazz)) {
            ArrayList obj = new ArrayList();
            Class genericClass = getFieldGenericType(field);
            try {
                JSONArray jsonArray = new JSONArray(s);
                boolean flag = isSimpleDataType(genericClass);
                for (int i = 0; i < jsonArray.length(); i++) {
                    obj.add(flag ? getObjectFromString(genericClass, jsonArray.getString(i)) : copy(genericClass, jsonArray.getJSONObject(i)));
                }
                return obj;
            } catch (JSONException e9) {
                e9.printStackTrace();
                return obj;
            }
        } else {
            try {
                return copy(clazz, new JSONObject(s));
            } catch (JSONException e10) {
                return null;
            }
        }
    }

    public static Object getObjectFromString(Class clazz, String s) throws ReflectException {
        if (clazz == Boolean.TYPE || clazz == Boolean.class) {
            try {
                return Boolean.valueOf(Boolean.parseBoolean(s));
            } catch (Exception e) {
                return false;
            }
        } else if (clazz == Character.TYPE || clazz == Character.class) {
            return s;
        } else {
            if (clazz == Byte.TYPE || clazz == Byte.class) {
                try {
                    return Byte.valueOf(Byte.parseByte(s));
                } catch (Exception e2) {
                    return -1;
                }
            } else if (clazz == Short.TYPE || clazz == Short.class) {
                try {
                    return Short.valueOf(Short.parseShort(s));
                } catch (Exception e3) {
                    return -1;
                }
            } else if (clazz == Integer.TYPE || clazz == Integer.class) {
                try {
                    return Integer.valueOf(Integer.parseInt(s));
                } catch (Exception e4) {
                    return -1;
                }
            } else if (clazz == Long.TYPE || clazz == Long.class) {
                try {
                    return Long.valueOf(Long.parseLong(s));
                } catch (Exception e5) {
                    return -1;
                }
            } else if (clazz == Float.TYPE || clazz == Float.class) {
                try {
                    return Float.valueOf(Float.parseFloat(s));
                } catch (Exception e6) {
                    return -1;
                }
            } else if (clazz == Double.TYPE || clazz == Double.class) {
                try {
                    return Double.valueOf(Double.parseDouble(s));
                } catch (Exception e7) {
                    return -1;
                }
            } else if (clazz != String.class) {
                try {
                    return copy(clazz, new JSONObject(s));
                } catch (JSONException e8) {
                    return null;
                }
            } else if (s == null || StringUtils.EMPTY.equals(s) || "null".equalsIgnoreCase(s)) {
                return null;
            } else {
                return s;
            }
        }
    }

    public static Object copy(Class clazz, JSONObject json) throws ReflectException {
        try {
            Object o = newInstance(clazz, (Object[]) null);
            for (Field field : clazz.getDeclaredFields()) {
                if (isNeedSetAccessible(field.getModifiers())) {
                    field.setAccessible(true);
                }
                String fieldName = field.getName();
                if (json.has(fieldName)) {
                    field.set(o, getObjectFromString(field, json.get(fieldName).toString()));
                }
            }
            return o;
        } catch (Exception e) {
            throw new ReflectException("Copy json data to class occurs exception", e);
        }
    }

    public static Field getField(Class clazz, String varName) {
        for (Field field : clazz.getDeclaredFields()) {
            if (isNeedSetAccessible(field.getModifiers())) {
                field.setAccessible(true);
            }
            if (varName.endsWith(field.getName())) {
                return field;
            }
        }
        return null;
    }

    public static <T> T cast(Class clazz, Object o) {
        return clazz.cast(o);
    }

    public static boolean isExtendsOf(Class clazz, Class parent) {
        for (Class t = clazz.getSuperclass(); t != null; t = t.getSuperclass()) {
            if (t == parent) {
                return true;
            }
        }
        return false;
    }

    public static Method[] getMethod(Class clazz, int modifier) {
        List<Method> methods = new ArrayList<>();
        for (Method method : clazz.getDeclaredMethods()) {
            if ((method.getModifiers() & modifier) > 0) {
                methods.add(method);
            }
        }
        return (Method[]) methods.toArray(new Method[methods.size()]);
    }

    public static Class getSuperClassGenricType(Class clazz, int index) {
        Type genType = clazz.getGenericSuperclass();
        if (!(genType instanceof ParameterizedType)) {
            return Object.class;
        }
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        if (index >= params.length || index < 0) {
            throw new RuntimeException("�����������" + (index < 0 ? "����С��0" : "�����˲��������"));
        } else if (!(params[index] instanceof Class)) {
            return Object.class;
        } else {
            return (Class) params[index];
        }
    }

    public static Class getSuperClassGenricType(Class clazz) {
        return getSuperClassGenricType(clazz, 0);
    }

    public static Class getMethodGenericReturnType(Method method, int index) {
        Type returnType = method.getGenericReturnType();
        if (!(returnType instanceof ParameterizedType)) {
            return Object.class;
        }
        Type[] typeArguments = ((ParameterizedType) returnType).getActualTypeArguments();
        if (index < typeArguments.length && index >= 0) {
            return (Class) typeArguments[index];
        }
        throw new RuntimeException("�����������" + (index < 0 ? "����С��0" : "�����˲��������"));
    }

    public static Class getMethodGenericReturnType(Method method) {
        return getMethodGenericReturnType(method, 0);
    }

    public static List<Class> getMethodGenericParameterTypes(Method method, int index) {
        List<Class> results = new ArrayList<>();
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        if (index >= genericParameterTypes.length || index < 0) {
            throw new RuntimeException("�����������" + (index < 0 ? "����С��0" : "�����˲��������"));
        }
        Type genericParameterType = genericParameterTypes[index];
        if (genericParameterType instanceof ParameterizedType) {
            for (Type parameterArgType : ((ParameterizedType) genericParameterType).getActualTypeArguments()) {
                results.add((Class) parameterArgType);
            }
        }
        return results;
    }

    public static List<Class> getMethodGenericParameterTypes(Method method) {
        return getMethodGenericParameterTypes(method, 0);
    }

    public static Class getFieldGenericType(Field field, int index) {
        Type genericFieldType = field.getGenericType();
        if (!(genericFieldType instanceof ParameterizedType)) {
            return Object.class;
        }
        Type[] fieldArgTypes = ((ParameterizedType) genericFieldType).getActualTypeArguments();
        if (index < fieldArgTypes.length && index >= 0) {
            return (Class) fieldArgTypes[index];
        }
        throw new RuntimeException("�����������" + (index < 0 ? "����С��0" : "�����˲��������"));
    }

    public static Class getFieldGenericType(Field field) {
        return getFieldGenericType(field, 0);
    }

    public static void main(String[] args) {
        String[] a = new String[0];
        System.out.println(isArrayDataType(a));
        System.out.println(isCollectionDataType(a));
        List<Integer> b = new ArrayList<>();
        System.out.println(isArrayDataType(b));
        System.out.println(isCollectionDataType(b));
        Map<String, String> c = new HashMap<>();
        System.out.println(isArrayDataType(c));
        System.out.println(isCollectionDataType(c));
    }
}
