package com.house365.core.reflect;

public class ReflectException extends Exception {
    public ReflectException() {
    }

    public ReflectException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ReflectException(String detailMessage) {
        super(detailMessage);
    }

    public ReflectException(Throwable throwable) {
        super(throwable);
    }
}
