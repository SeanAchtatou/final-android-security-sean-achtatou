package com.house365.core.error;

import android.content.Context;
import android.os.Process;
import android.text.format.Time;
import com.house365.core.constant.CorePreferences;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;
import org.apache.commons.lang.StringUtils;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    private static final String CRASH_REPORTER_EXTENSION = ".cr";
    private static CrashHandler INSTANCE = null;
    private static final String STACK_TRACE = "STACK_TRACE";
    private static final String VERSION_CODE = "versionCode";
    private static final String VERSION_NAME = "versionName";
    /* access modifiers changed from: private */
    public Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private Properties mDeviceCrashInfo = new Properties();

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CrashHandler();
        }
        return INSTANCE;
    }

    public void init(Context ctx) {
        this.mContext = ctx;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.mDefaultHandler == null) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                CorePreferences.ERROR("Error : ", e);
            }
            Process.killProcess(Process.myPid());
            System.exit(10);
            return;
        }
        this.mDefaultHandler.uncaughtException(thread, ex);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        r0 = r4.getLocalizedMessage();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean handleException(java.lang.Throwable r4) {
        /*
            r3 = this;
            r2 = 1
            java.lang.String r1 = "handleException"
            com.house365.core.constant.CorePreferences.ERROR(r1, r4)
            if (r4 != 0) goto L_0x0009
        L_0x0008:
            return r2
        L_0x0009:
            java.lang.String r0 = r4.getLocalizedMessage()
            if (r0 == 0) goto L_0x0008
            com.house365.core.error.CrashHandler$1 r1 = new com.house365.core.error.CrashHandler$1
            r1.<init>(r0)
            r1.start()
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.error.CrashHandler.handleException(java.lang.Throwable):boolean");
    }

    public void sendPreviousReportsToServer() {
        sendCrashReportsToServer(this.mContext);
    }

    private void sendCrashReportsToServer(Context ctx) {
        String[] crFiles = getCrashReportFiles(ctx);
        if (crFiles != null && crFiles.length > 0) {
            TreeSet<String> sortedFiles = new TreeSet<>();
            sortedFiles.addAll(Arrays.asList(crFiles));
            Iterator it = sortedFiles.iterator();
            while (it.hasNext()) {
                File cr = new File(ctx.getFilesDir(), (String) it.next());
                postReport(cr);
                cr.delete();
            }
        }
    }

    private void postReport(File file) {
    }

    private String[] getCrashReportFiles(Context ctx) {
        return ctx.getFilesDir().list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(CrashHandler.CRASH_REPORTER_EXTENSION);
            }
        });
    }

    private String saveCrashInfoToFile(Throwable ex) {
        Writer info = new StringWriter();
        PrintWriter printWriter = new PrintWriter(info);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String result = info.toString();
        printWriter.close();
        this.mDeviceCrashInfo.put("EXEPTION", ex.getLocalizedMessage());
        this.mDeviceCrashInfo.put(STACK_TRACE, result);
        try {
            Time t = new Time("GMT+8");
            t.setToNow();
            String fileName = "crash-" + ((t.year * 10000) + (t.month * 100) + t.monthDay) + "-" + ((t.hour * 10000) + (t.minute * 100) + t.second) + CRASH_REPORTER_EXTENSION;
            FileOutputStream trace = this.mContext.openFileOutput(fileName, 0);
            this.mDeviceCrashInfo.store(trace, StringUtils.EMPTY);
            trace.flush();
            trace.close();
            return fileName;
        } catch (Exception e) {
            CorePreferences.ERROR("an error occured while writing report file...", e);
            return null;
        }
    }
}
