package com.house365.core.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import com.house365.core.R;

public abstract class ActivityFragment extends LocalActivityManagerFragment {
    private TabHost mTabHost;

    public abstract Intent getIntentActivity(Activity activity);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fragment, container, false);
        this.mTabHost = (TabHost) view.findViewById(16908306);
        this.mTabHost.setup(getLocalActivityManager());
        this.mTabHost.addTab(this.mTabHost.newTabSpec("map").setIndicator("map").setContent(getIntentActivity(getActivity())));
        return view;
    }
}
