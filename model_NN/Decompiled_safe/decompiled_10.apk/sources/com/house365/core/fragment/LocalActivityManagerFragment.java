package com.house365.core.fragment;

import android.app.LocalActivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class LocalActivityManagerFragment extends Fragment {
    private static final String KEY_STATE_BUNDLE = "localActivityManagerState";
    private LocalActivityManager mLocalActivityManager;

    /* access modifiers changed from: protected */
    public LocalActivityManager getLocalActivityManager() {
        return this.mLocalActivityManager;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle state = null;
        if (savedInstanceState != null) {
            state = savedInstanceState.getBundle(KEY_STATE_BUNDLE);
        }
        this.mLocalActivityManager = new LocalActivityManager(getActivity(), true);
        this.mLocalActivityManager.dispatchCreate(state);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(KEY_STATE_BUNDLE, this.mLocalActivityManager.saveInstanceState());
    }

    public void onResume() {
        super.onResume();
        this.mLocalActivityManager.dispatchResume();
    }

    public void onPause() {
        super.onPause();
        this.mLocalActivityManager.dispatchPause(getActivity().isFinishing());
    }

    public void onStop() {
        super.onStop();
        this.mLocalActivityManager.dispatchStop();
    }

    public void onDestroy() {
        super.onDestroy();
        this.mLocalActivityManager.dispatchDestroy(getActivity().isFinishing());
    }
}
