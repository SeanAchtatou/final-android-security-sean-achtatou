package com.house365.core.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import com.house365.core.R;
import com.house365.core.adapter.BaseListAdapter;
import com.house365.core.application.BaseApplication;
import com.house365.core.bean.common.CommonTaskInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.ListFooterView;
import com.house365.core.view.LoadingDialog;
import com.house365.core.view.pulltorefresh.PullToRefreshGridView;
import java.util.List;

public abstract class BaseGridAsyncTask<T> extends AsyncTask<Object, Object, Object> {
    private BaseListAdapter adapter;
    protected Context context;
    private ListFooterView footerView;
    protected RefreshInfo listRefresh;
    protected LoadingDialog loadingDialog;
    protected int loadingresid;
    protected BaseApplication mApplication;
    private PullToRefreshGridView pullListView;

    public abstract List<T> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException;

    public BaseGridAsyncTask(Context context2, int loadingResId, PullToRefreshGridView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2) {
        this.context = context2;
        this.loadingresid = loadingResId;
        this.pullListView = listView;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
    }

    public BaseGridAsyncTask(Context context2, PullToRefreshGridView listView, RefreshInfo listRefresh2, BaseListAdapter adapter2) {
        this.context = context2;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.pullListView = listView;
        this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
    }

    public BaseGridAsyncTask(Context context2) {
        this.context = context2;
        this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
    }

    public LoadingDialog getLoadingDialog() {
        if (this.loadingDialog == null) {
            this.loadingDialog = new LoadingDialog(this.context, R.style.dialog, this.loadingresid);
            this.loadingDialog.setCancelable(true);
        }
        return this.loadingDialog;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.loadingresid > 0) {
            getLoadingDialog().show();
        }
        if (this.listRefresh != null && this.pullListView != null) {
            ViewUtil.onPreLoadingGridData(this.listRefresh, this.pullListView, this.adapter);
        }
    }

    /* access modifiers changed from: protected */
    public CommonTaskInfo<List<T>> doInBackground(Object... params) {
        CommonTaskInfo<List<T>> commonTaskInfo = new CommonTaskInfo<>();
        commonTaskInfo.setResult(0);
        List<T> v = null;
        try {
            v = onDoInBackgroup();
        } catch (NetworkUnavailableException e) {
            CorePreferences.ERROR(e);
            commonTaskInfo.setResult(1);
        } catch (HtppApiException e2) {
            CorePreferences.ERROR(e2);
            commonTaskInfo.setResult(2);
        } catch (HttpParseException e3) {
            CorePreferences.ERROR(e3);
            commonTaskInfo.setResult(3);
        }
        commonTaskInfo.setData(v);
        return commonTaskInfo;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.loadingresid > 0) {
            getLoadingDialog().dismiss();
        }
        if (!(this.context instanceof Activity) || !((Activity) this.context).isFinishing()) {
            CommonTaskInfo<List<T>> commonTaskInfo = (CommonTaskInfo) result;
            if (commonTaskInfo.getResult() == 1) {
                if (this.listRefresh.refresh) {
                    this.pullListView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.pullListView.onRefreshComplete();
                }
                onNetworkUnavailable();
            } else if (commonTaskInfo.getResult() == 2) {
                if (this.listRefresh.refresh) {
                    this.pullListView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.pullListView.onRefreshComplete();
                }
                onHttpRequestError();
            } else if (commonTaskInfo.getResult() == 3) {
                if (this.listRefresh.refresh) {
                    this.pullListView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.pullListView.onRefreshComplete();
                }
                onParseError();
            } else {
                List v = commonTaskInfo.getData();
                if (this.listRefresh != null) {
                    if (this.pullListView != null) {
                        ViewUtil.onGridDataComplete(this.context, v, this.listRefresh, this.adapter, this.pullListView);
                    }
                    if (this.listRefresh.isRefresh()) {
                        onAfterRefresh(v);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<T> list) {
    }

    /* access modifiers changed from: protected */
    public void onDialogCancel() {
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        ActivityUtil.showToast(this.context, R.string.text_network_unavailable);
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        ActivityUtil.showToast(this.context, R.string.text_http_request_error);
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        ActivityUtil.showToast(this.context, R.string.text_http_parse_error);
    }
}
