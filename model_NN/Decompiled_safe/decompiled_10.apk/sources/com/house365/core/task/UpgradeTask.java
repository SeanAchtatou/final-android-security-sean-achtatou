package com.house365.core.task;

import android.content.Context;
import com.house365.core.R;
import com.house365.core.helper.VersionUpgradeHelper;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.DeviceUtil;

public class UpgradeTask extends CommonAsyncTask<Integer> {
    private int NEED_UPDATE = 1;
    private int NET_ERROR = 0;
    private int NO_NEED_UPDATE = 2;
    boolean isAuto;
    VersionUpgradeHelper verUpgrade;

    public UpgradeTask(Context context, boolean isAuto2) {
        super(context);
        this.isAuto = isAuto2;
        if (!isAuto2) {
            setLoadingresid(R.string.version_infoing);
        }
        this.verUpgrade = new VersionUpgradeHelper(context);
    }

    public void onAfterDoInBackgroup(Integer result) {
        if (result.intValue() == this.NET_ERROR && !this.isAuto) {
            ActivityUtil.showToast(this.context, R.string.net_error);
        } else if (result.intValue() == this.NEED_UPDATE) {
            this.verUpgrade.showUpdateDialog();
        } else if (!this.isAuto) {
            this.verUpgrade.notNewVersionShow();
        }
    }

    public Integer onDoInBackgroup() {
        if (!DeviceUtil.isNetConnect(this.context)) {
            return Integer.valueOf(this.NET_ERROR);
        }
        return Integer.valueOf(this.verUpgrade.needUpdate() ? this.NEED_UPDATE : this.NO_NEED_UPDATE);
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        if (!this.isAuto) {
            super.onNetworkUnavailable();
        }
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        if (!this.isAuto) {
            ActivityUtil.showToast(this.context, R.string.net_error);
        }
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        super.onParseError();
    }
}
