package com.house365.core.task;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import com.house365.core.R;
import com.house365.core.adapter.BaseExpandableListAdapter;
import com.house365.core.application.BaseApplication;
import com.house365.core.bean.Group;
import com.house365.core.bean.common.CommonTaskInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.util.ActivityUtil;
import com.house365.core.util.RefreshInfo;
import com.house365.core.util.TimeUtil;
import com.house365.core.util.ViewUtil;
import com.house365.core.view.LoadingDialog;
import com.house365.core.view.pulltorefresh.PullToRefreshExpandableListView;
import java.util.List;

public abstract class BaseExpandableListAsyncTask<T, E> extends AsyncTask<Object, Object, Object> {
    private BaseExpandableListAdapter adapter;
    protected Context context;
    protected RefreshInfo listRefresh;
    private PullToRefreshExpandableListView listView;
    protected LoadingDialog loadingDialog;
    protected int loadingresid;
    protected BaseApplication mApplication;

    public abstract List<Group<T, E>> onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException;

    public BaseExpandableListAsyncTask(Context context2, int loadingResId, PullToRefreshExpandableListView listView2, RefreshInfo listRefresh2, BaseExpandableListAdapter adapter2) {
        this.context = context2;
        this.loadingresid = loadingResId;
        this.listView = listView2;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
    }

    public BaseExpandableListAsyncTask(Context context2, PullToRefreshExpandableListView listView2, RefreshInfo listRefresh2, BaseExpandableListAdapter adapter2) {
        this.context = context2;
        this.listRefresh = listRefresh2;
        this.adapter = adapter2;
        this.listView = listView2;
        this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
    }

    public LoadingDialog getLoadingDialog() {
        if (this.loadingDialog == null) {
            this.loadingDialog = new LoadingDialog(this.context, R.style.dialog, this.loadingresid);
            this.loadingDialog.setCancelable(true);
        }
        return this.loadingDialog;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.loadingresid > 0) {
            getLoadingDialog().show();
        }
        if (this.listRefresh != null) {
            ViewUtil.onPreLoadingExpandableListData(this.listRefresh, this.listView);
        }
    }

    /* access modifiers changed from: protected */
    public CommonTaskInfo<List<Group<T, E>>> doInBackground(Object... params) {
        CommonTaskInfo<List<Group<T, E>>> commonTaskInfo = new CommonTaskInfo<>();
        commonTaskInfo.setResult(0);
        List<Group<T, E>> v = null;
        try {
            v = onDoInBackgroup();
        } catch (NetworkUnavailableException e) {
            CorePreferences.ERROR(e);
            commonTaskInfo.setResult(1);
        } catch (HtppApiException e2) {
            CorePreferences.ERROR(e2);
            commonTaskInfo.setResult(2);
        } catch (HttpParseException e3) {
            CorePreferences.ERROR(e3);
            commonTaskInfo.setResult(3);
        }
        commonTaskInfo.setData(v);
        return commonTaskInfo;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.loadingresid > 0) {
            getLoadingDialog().dismiss();
        }
        if (!(this.context instanceof Activity) || !((Activity) this.context).isFinishing()) {
            CommonTaskInfo<List<Group<T, E>>> commonTaskInfo = (CommonTaskInfo) result;
            if (commonTaskInfo.getResult() == 1) {
                if (this.listRefresh.refresh) {
                    this.listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.listView.onRefreshComplete();
                }
                onNetworkUnavailable();
            } else if (commonTaskInfo.getResult() == 2) {
                if (this.listRefresh.refresh) {
                    this.listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.listView.onRefreshComplete();
                }
                onHttpRequestError();
            } else if (commonTaskInfo.getResult() == 3) {
                if (this.listRefresh.refresh) {
                    this.listView.onRefreshComplete(TimeUtil.toDateAndTime(System.currentTimeMillis() / 1000));
                } else {
                    this.listView.onRefreshComplete();
                }
                onParseError();
            } else {
                List v = commonTaskInfo.getData();
                if (this.listRefresh != null) {
                    if (this.listView != null) {
                        ViewUtil.onExpandableListDataComplete(this.context, v, this.listRefresh, this.adapter, this.listView);
                    } else {
                        ViewUtil.onExpandableListDataComplete(this.context, v, this.listRefresh, this.adapter, this.listView);
                    }
                    if (this.listRefresh.isRefresh()) {
                        onAfterRefresh(v);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAfterRefresh(List<Group<T, E>> list) {
    }

    /* access modifiers changed from: protected */
    public void onDialogCancel() {
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        ActivityUtil.showToast(this.context, R.string.text_network_unavailable);
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        ActivityUtil.showToast(this.context, R.string.text_http_request_error);
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        ActivityUtil.showToast(this.context, R.string.text_http_parse_error);
    }
}
