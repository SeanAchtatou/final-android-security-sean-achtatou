package com.house365.core.task;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import com.house365.core.R;
import com.house365.core.application.BaseApplication;
import com.house365.core.bean.common.CommonTaskInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.exception.HtppApiException;
import com.house365.core.http.exception.HttpParseException;
import com.house365.core.http.exception.NetworkUnavailableException;
import com.house365.core.util.ActivityUtil;
import com.house365.core.view.LoadingDialog;

public abstract class CommonAsyncTask<V> extends AsyncTask<Object, Object, Object> {
    protected Context context;
    protected LoadingDialog loadingDialog;
    protected int loadingresid;
    protected BaseApplication mApplication;

    public abstract void onAfterDoInBackgroup(V v);

    public abstract V onDoInBackgroup() throws NetworkUnavailableException, HtppApiException, HttpParseException;

    public CommonAsyncTask(Context context2) {
        this.context = context2;
        try {
            this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
        } catch (ClassCastException e) {
            this.mApplication = (BaseApplication) context2;
        }
    }

    public CommonAsyncTask(Context context2, int resid) {
        this.context = context2;
        this.loadingresid = resid;
        try {
            this.mApplication = (BaseApplication) ((Activity) context2).getApplication();
        } catch (ClassCastException e) {
            this.mApplication = (BaseApplication) context2;
        }
    }

    public LoadingDialog getLoadingDialog() {
        if (this.loadingDialog == null) {
            this.loadingDialog = new LoadingDialog(this.context, R.style.dialog, this.loadingresid);
            this.loadingDialog.setCancelable(true);
            this.loadingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    CommonAsyncTask.this.onDialogCancel();
                }
            });
        }
        return this.loadingDialog;
    }

    public void setLoadingDialog(LoadingDialog loadingDialog2) {
        this.loadingDialog = loadingDialog2;
    }

    public int getLoadingresid() {
        return this.loadingresid;
    }

    public void setLoadingresid(int loadingresid2) {
        this.loadingresid = loadingresid2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.loadingresid > 0 && getLoadingDialog() != null) {
            getLoadingDialog().show();
        }
    }

    /* access modifiers changed from: protected */
    public CommonTaskInfo<V> doInBackground(Object... params) {
        CommonTaskInfo<V> commonTaskInfo = new CommonTaskInfo<>();
        commonTaskInfo.setResult(0);
        V v = null;
        try {
            v = onDoInBackgroup();
        } catch (NetworkUnavailableException e) {
            CorePreferences.ERROR(e);
            commonTaskInfo.setResult(1);
        } catch (HtppApiException e2) {
            CorePreferences.ERROR(e2);
            commonTaskInfo.setResult(2);
        } catch (HttpParseException e3) {
            CorePreferences.ERROR(e3);
            commonTaskInfo.setResult(3);
        }
        commonTaskInfo.setData(v);
        return commonTaskInfo;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object result) {
        if (this.loadingresid > 0) {
            getLoadingDialog().dismiss();
        }
        if (!(this.context instanceof Activity) || !((Activity) this.context).isFinishing()) {
            CommonTaskInfo<V> commonTaskInfo = (CommonTaskInfo) result;
            if (commonTaskInfo.getResult() == 1) {
                onNetworkUnavailable();
            } else if (commonTaskInfo.getResult() == 2) {
                onHttpRequestError();
            } else if (commonTaskInfo.getResult() == 3) {
                onParseError();
            } else {
                onAfterDoInBackgroup(commonTaskInfo.getData());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDialogCancel() {
    }

    /* access modifiers changed from: protected */
    public void onNetworkUnavailable() {
        ActivityUtil.showToast(this.context, R.string.text_network_unavailable);
    }

    /* access modifiers changed from: protected */
    public void onHttpRequestError() {
        ActivityUtil.showToast(this.context, R.string.text_http_request_error);
    }

    /* access modifiers changed from: protected */
    public void onParseError() {
        ActivityUtil.showToast(this.context, R.string.text_http_parse_error);
    }
}
