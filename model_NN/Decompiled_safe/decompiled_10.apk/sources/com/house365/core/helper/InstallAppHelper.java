package com.house365.core.helper;

import android.content.Context;
import android.content.DialogInterface;
import com.house365.core.R;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.FileUtil;
import com.house365.core.util.intent.IntentHelper;
import java.io.File;

public class InstallAppHelper {
    private static InstallAppHelper InstallAppHelper = null;
    /* access modifiers changed from: private */
    public String apkUrl;
    private String appName;
    private boolean isconfirm;
    /* access modifiers changed from: private */
    public Context mContext;
    private String packageName;

    public InstallAppHelper(Context context) {
        this.mContext = context;
    }

    public InstallAppHelper(Context context, String packagename, String appname, String apkurl) {
        this.mContext = context;
        this.packageName = packagename;
        this.apkUrl = apkurl;
        this.appName = appname;
    }

    public static InstallAppHelper getInstance(Context context) {
        if (InstallAppHelper == null) {
            return new InstallAppHelper(context);
        }
        return InstallAppHelper;
    }

    public static InstallAppHelper getInstance(Context context, String packagename, String appname, String apkurl) {
        if (InstallAppHelper == null) {
            return new InstallAppHelper(context, packagename, appname, apkurl);
        }
        return InstallAppHelper;
    }

    public void openFile(String fileurl) {
        if (!PackageHelper.isInstalledApp(this.mContext, this.packageName)) {
            installApp();
        } else if (!FileUtil.isExistFile(getFilePath(fileurl))) {
            downloadFileConfirm(fileurl);
        } else {
            IntentHelper.openFileByType(this.mContext, new File(getFilePath(fileurl)));
        }
    }

    public String getFilePath(String fileurl) {
        return String.valueOf(CorePreferences.getAppDownloadSDPath()) + "/" + FileUtil.getFileNameFromUrl(fileurl);
    }

    private void installApp() {
        final String installapkfile = String.valueOf(CorePreferences.getAppTmpSDPath()) + "/" + FileUtil.getFileNameFromUrl(this.apkUrl);
        if (FileUtil.isExistFile(installapkfile)) {
            IntentHelper.openFileByType(this.mContext, new File(installapkfile));
        } else if (DeviceUtil.isWifiConnect(this.mContext)) {
            String downloadtitle = this.mContext.getResources().getString(R.string.confirm_install_title);
            String downloadmessage = this.mContext.getResources().getString(R.string.apk_confirm_install_msg, this.appName);
            final String downloading = this.mContext.getResources().getString(R.string.apk_downloading, this.appName);
            ViewHelper.showConfirmDialog(this.mContext, downloadtitle, downloadmessage, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new Downloadhelper(InstallAppHelper.this.mContext, true).downLoadFile(downloading, InstallAppHelper.this.apkUrl, installapkfile);
                }
            });
        } else {
            ViewHelper.showDialog(this.mContext, R.string.confirm_wifinet_title, R.string.confirm_nowifi_install_msg, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    private void downloadFileConfirm(final String fileurl) {
        if (!DeviceUtil.isWifiConnect(this.mContext)) {
            ViewHelper.showConfirmDialog(this.mContext, this.mContext.getResources().getString(R.string.confirm_wifinet_title), this.mContext.getResources().getString(R.string.confirm_wifinet_msg), R.string.confirm_download_button_ok, R.string.confirm_download_button_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    InstallAppHelper.this.downloadFile(fileurl);
                }
            });
            return;
        }
        downloadFile(fileurl);
    }

    /* access modifiers changed from: private */
    public void downloadFile(String fileurl) {
        new Downloadhelper(this.mContext).downLoadFile(this.mContext.getResources().getString(R.string.downloading), fileurl, getFilePath(fileurl));
    }
}
