package com.house365.core.helper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.application.BaseApplication;
import com.house365.core.bean.VersionInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.http.HttpAPIAdapter;
import com.house365.core.util.DeviceUtil;
import com.house365.core.util.FileUtil;
import com.house365.core.view.LoadingDialog;

public class VersionUpgradeHelper {
    private static VersionUpgradeHelper VersionUpgradeHelper = null;
    private static boolean isCannel = false;
    /* access modifiers changed from: private */
    public AlertDialog alertDialog;
    /* access modifiers changed from: private */
    public AlertDialog alertUpdateDialog;
    public Context context = null;
    private VersionInfo currVersion;
    private BaseApplication<HttpAPIAdapter> mApplication;
    /* access modifiers changed from: private */
    public VersionInfo newVersion;
    private ProgressDialog progressDialog;
    protected LoadingDialog tloadingDialog;

    public interface VersionUpgradeListener {
        void onGetedVersion();

        void onPreGetVersion();
    }

    public VersionUpgradeHelper(Context context2) {
        this.context = context2;
        this.mApplication = (BaseApplication) context2.getApplicationContext();
    }

    private void initData() {
        isCannel = false;
    }

    public void checkAndUpdateByHand() {
        if (!DeviceUtil.isNetConnect(this.context)) {
            Toast.makeText(this.context, R.string.net_error, 0).show();
        } else if (needUpdate()) {
            showUpdateDialog();
        } else {
            notNewVersionShow();
        }
    }

    public void checkAndUpdateByHand(VersionUpgradeListener versionUpListener) {
        if (DeviceUtil.isNetConnect(this.context)) {
            versionUpListener.onPreGetVersion();
            boolean needUpdate = needUpdate();
            versionUpListener.onGetedVersion();
            if (needUpdate) {
                showUpdateDialog();
            } else {
                notNewVersionShow();
            }
        } else {
            Toast.makeText(this.context, R.string.net_error, 0).show();
        }
    }

    public void checkAndUpdateByAuto() {
        if (DeviceUtil.isNetConnect(this.context) && needUpdate()) {
            showUpdateDialog();
        }
    }

    public void getVerInfo() {
        this.currVersion = getCurrentVersion();
        this.newVersion = getServerVerCode();
    }

    private VersionInfo getServerVerCode() {
        try {
            return this.mApplication.getApi().getAppNewVersion();
        } catch (Exception e) {
            CorePreferences.ERROR(e.getMessage());
            return null;
        }
    }

    public boolean needUpdate() {
        getVerInfo();
        if (this.newVersion == null || this.newVersion.getVersionCode() <= this.currVersion.getVersionCode()) {
            return false;
        }
        return true;
    }

    public void notNewVersionShow() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.context.getResources().getString(R.string.version_current));
        sb.append(this.currVersion.versionName);
        sb.append("\n" + this.context.getResources().getString(R.string.version_current_is_newset));
        if (this.alertDialog == null) {
            this.alertDialog = new AlertDialog.Builder(this.context).setTitle(R.string.version_update_title).setMessage(sb.toString()).setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    VersionUpgradeHelper.this.alertDialog = null;
                }
            }).create();
            this.alertDialog.show();
        }
    }

    public void showUpdateDialog() {
        if (this.alertUpdateDialog == null) {
            String newverStr = this.context.getResources().getString(R.string.version_update_newver, this.newVersion.versionName);
            final String version_downloading = this.context.getResources().getString(R.string.version_downloading, this.newVersion.versionName);
            final String installapkfile = String.valueOf(CorePreferences.getAppTmpSDPath()) + "/" + FileUtil.getFileNameFromUrl(this.newVersion.apkFileUrl);
            this.alertUpdateDialog = new AlertDialog.Builder(this.context).setTitle(newverStr).setMessage(this.newVersion.getVersionNote()).setPositiveButton(R.string.version_update_immediately, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new Downloadhelper(VersionUpgradeHelper.this.context, true).downLoadFile(version_downloading, VersionUpgradeHelper.this.newVersion.getApkFileUrl(), installapkfile);
                }
            }).setNegativeButton(R.string.version_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    VersionUpgradeHelper.this.alertUpdateDialog = null;
                }
            }).create();
        }
        this.alertUpdateDialog.show();
    }

    public VersionInfo getCurrentVersion() {
        try {
            PackageInfo info = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0);
            VersionInfo version = new VersionInfo();
            version.versionCode = info.versionCode;
            version.versionName = info.versionName;
            CorePreferences.DEBUG("��ǰ�汾��" + version.versionCode);
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
