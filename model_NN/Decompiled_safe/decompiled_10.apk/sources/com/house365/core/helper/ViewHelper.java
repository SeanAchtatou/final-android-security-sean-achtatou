package com.house365.core.helper;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.util.AbstractCallBack;
import com.house365.core.util.intent.IntentHelper;
import java.io.File;

public class ViewHelper {

    public interface InputTextBoxOnClickListener {
        void onClick(DialogInterface dialogInterface, int i, EditText editText);
    }

    public static void showDialog(Context mContext, int titleId, int messageId, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(mContext);
        tDialog.setTitle(titleId);
        tDialog.setMessage(messageId);
        tDialog.setPositiveButton(R.string.dialog_button_ok, listener);
        tDialog.show();
    }

    public static void showDialog(Context context, CharSequence title, CharSequence message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setTitle(title);
        tDialog.setMessage(message);
        tDialog.setPositiveButton(R.string.dialog_button_ok, listener);
        tDialog.show();
    }

    public static void showConfirmDialog(Context context, CharSequence title, CharSequence message, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setTitle(title);
        tDialog.setMessage(message);
        tDialog.setPositiveButton(R.string.dialog_button_ok, listener);
        tDialog.setNegativeButton(context.getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        tDialog.show();
    }

    public static void showConfirmDialog(Context context, CharSequence title, CharSequence message, int textPositiveId, int textNegativeId, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setTitle(title);
        tDialog.setMessage(message);
        tDialog.setPositiveButton(textPositiveId, listener);
        tDialog.setNegativeButton(textNegativeId, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        tDialog.show();
    }

    public static ProgressDialog showProgress(Context context, CharSequence title, CharSequence message, boolean indeterminate, boolean cancelable, DialogInterface.OnCancelListener listener) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setIndeterminate(indeterminate);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(listener);
        dialog.show();
        return dialog;
    }

    public static ProgressDialog showProgressHorizontal(Context context, CharSequence title, CharSequence message, boolean cancelable, DialogInterface.OnCancelListener listener) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setProgressStyle(1);
        dialog.incrementProgressBy(1);
        dialog.setIndeterminate(false);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(listener);
        dialog.show();
        return dialog;
    }

    public void showOpenFileDialog(final Context context, CharSequence title, CharSequence message, final String apkfile) {
        AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
        tDialog.setTitle(title);
        tDialog.setMessage(message);
        tDialog.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                IntentHelper.openFileByType(context, new File(apkfile));
            }
        });
        tDialog.setNegativeButton(context.getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        tDialog.show();
    }

    public static void showAlertDialogWithCloseInTime(AlertDialog.Builder alertDialogBuilder, long millisecond) {
        final AlertDialog alertDialog = alertDialogBuilder.show();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        }, millisecond);
    }

    public static void showAlertDialogWithCloseInTime(AlertDialog.Builder alertDialogBuilder, long millisecond, final AbstractCallBack callback) {
        final AlertDialog alertDialog = alertDialogBuilder.show();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                callback.doCallBack();
            }
        }, millisecond);
    }

    public static void setButtonPressed(Button button, boolean pressed) {
        if (pressed) {
            button.setPressed(true);
            button.setFocusableInTouchMode(true);
            button.requestFocus();
            return;
        }
        button.setFocusable(false);
        button.setPressed(false);
        button.setFocusableInTouchMode(false);
    }

    public static void showToastView(Context context, String msg) {
        View toastRoot = LayoutInflater.from(context).inflate(R.layout.toast, (ViewGroup) null);
        ((TextView) toastRoot.findViewById(R.id.message)).setText(msg);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(17, 0, 10);
        toastStart.setDuration(1);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

    public static void showToastView(Context context, int resId) {
        View toastRoot = LayoutInflater.from(context).inflate(R.layout.toast, (ViewGroup) null);
        ((TextView) toastRoot.findViewById(R.id.message)).setText(resId);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(17, 0, 10);
        toastStart.setDuration(1);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

    public static void showToast(Context context, CharSequence text) {
        Toast.makeText(context, text, 0).show();
    }

    public static void showToast(Context context, int resId) {
        Toast.makeText(context, resId, 0).show();
    }

    public static void inputTextBox(Context context, int titleId, String key, String content, int maxSize, int inputType, final InputTextBoxOnClickListener positiveClick) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(titleId);
        final EditText input = new EditText(context);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxSize)});
        input.setInputType(inputType);
        input.setText(content);
        input.setLines(1);
        alert.setView(input);
        alert.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputTextBoxOnClickListener.this.onClick(dialog, which, input);
                dialog.dismiss();
            }
        });
        alert.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    public static void inputTextBox(Context context, int titleId, String key, String content, int maxSize, int inputType, int line, final InputTextBoxOnClickListener positiveClick) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(titleId);
        final EditText input = new EditText(context);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxSize)});
        input.setInputType(inputType);
        input.setText(content);
        input.setLines(line);
        input.setGravity(48);
        alert.setView(input);
        alert.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                InputTextBoxOnClickListener.this.onClick(dialog, which, input);
            }
        });
        alert.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}
