package com.house365.core.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.house365.core.R;
import com.house365.core.util.intent.IntentHelper;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.apache.commons.lang.StringUtils;

public class Downloadhelper {
    public final int DOWNLOAD_STATUS_CANCEL = 3;
    public final int DOWNLOAD_STATUS_ERROR = 2;
    public final int DOWNLOAD_STATUS_INPROCESS = 0;
    public final int DOWNLOAD_STATUS_INPROCESS_MAX = 4;
    public final int DOWNLOAD_STATUS_SUCCEED = 1;
    private int connectTimeout = 30000;
    private boolean isDownloading = true;
    private boolean isHorizontalLoading = false;
    public Context mContext = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 0:
                        Downloadhelper downloadhelper = Downloadhelper.this;
                        downloadhelper.nowSize = downloadhelper.nowSize + msg.arg2;
                        Downloadhelper.this.mProgress.setProgress(Downloadhelper.this.nowSize);
                        break;
                    case 1:
                        Downloadhelper.this.closeProgress();
                        IntentHelper.openFileByType(Downloadhelper.this.mContext, new File((String) msg.obj));
                        break;
                    case 2:
                        Downloadhelper.this.closeProgress();
                        Toast.makeText(Downloadhelper.this.mContext, R.string.download_error, 1).show();
                        Downloadhelper.this.setDownloading(false);
                        break;
                    case 3:
                        Downloadhelper.this.closeProgress();
                        String str = (String) msg.obj;
                        Downloadhelper.this.setDownloading(false);
                        break;
                    case 4:
                        Downloadhelper.this.mProgress.setMax(Downloadhelper.this.maxSize);
                        break;
                }
                super.handleMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgress;
    private Proxy mProxy = null;
    /* access modifiers changed from: private */
    public int maxSize;
    /* access modifiers changed from: private */
    public int nowSize = 0;
    private int readTimeout = 30000;

    public Downloadhelper(Context context) {
        this.mContext = context;
        setDefaultHostnameVerifier();
    }

    public Downloadhelper(Context context, boolean ishorizontal) {
        this.mContext = context;
        this.isHorizontalLoading = ishorizontal;
        setDefaultHostnameVerifier();
    }

    public void downLoadFile(CharSequence downloadMessage, final String fileUrl, final String filePath) {
        if (this.isHorizontalLoading) {
            this.mProgress = ViewHelper.showProgressHorizontal(this.mContext, StringUtils.EMPTY, downloadMessage, true, new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    Message msg = new Message();
                    msg.what = 3;
                    msg.obj = filePath;
                    Downloadhelper.this.mHandler.sendMessage(msg);
                }
            });
        } else {
            this.mProgress = ViewHelper.showProgress(this.mContext, StringUtils.EMPTY, downloadMessage, false, true, new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    Message msg = new Message();
                    msg.what = 3;
                    msg.obj = filePath;
                    Downloadhelper.this.mHandler.sendMessage(msg);
                }
            });
        }
        new Thread(new Runnable() {
            public void run() {
                if (Downloadhelper.this.isDownloadFile(Downloadhelper.this.mContext, fileUrl, filePath)) {
                    Message msg = new Message();
                    msg.what = 1;
                    msg.obj = filePath;
                    Downloadhelper.this.mHandler.sendMessage(msg);
                    return;
                }
                Message msg2 = new Message();
                msg2.what = 2;
                msg2.obj = filePath;
                Downloadhelper.this.mHandler.sendMessage(msg2);
            }
        }).start();
    }

    public boolean isDownloadFile(Context context, String strurl, String filename) {
        try {
            return urlDownloadToFile(context, strurl, filename);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void detectProxy() {
        NetworkInfo ni = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.getType() == 0) {
            String proxyHost = android.net.Proxy.getDefaultHost();
            int port = android.net.Proxy.getDefaultPort();
            if (proxyHost != null) {
                this.mProxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, port));
            }
        }
    }

    private void setDefaultHostnameVerifier() {
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }

    /* JADX WARN: Type inference failed for: r9v10, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String SendAndWaitResponse(java.lang.String r12, java.lang.String r13) {
        /*
            r11 = this;
            r11.detectProxy()
            r7 = 0
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r10 = "requestData"
            r9.<init>(r10, r12)
            r6.add(r9)
            r3 = 0
            org.apache.http.client.entity.UrlEncodedFormEntity r5 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ IOException -> 0x0065 }
            java.lang.String r9 = "utf-8"
            r5.<init>(r6, r9)     // Catch:{ IOException -> 0x0065 }
            java.net.URL r8 = new java.net.URL     // Catch:{ IOException -> 0x0065 }
            r8.<init>(r13)     // Catch:{ IOException -> 0x0065 }
            java.net.Proxy r9 = r11.mProxy     // Catch:{ IOException -> 0x0065 }
            if (r9 == 0) goto L_0x005c
            java.net.Proxy r9 = r11.mProxy     // Catch:{ IOException -> 0x0065 }
            java.net.URLConnection r9 = r8.openConnection(r9)     // Catch:{ IOException -> 0x0065 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0065 }
            r3 = r0
        L_0x002e:
            int r9 = r11.connectTimeout     // Catch:{ IOException -> 0x0065 }
            r3.setConnectTimeout(r9)     // Catch:{ IOException -> 0x0065 }
            int r9 = r11.readTimeout     // Catch:{ IOException -> 0x0065 }
            r3.setReadTimeout(r9)     // Catch:{ IOException -> 0x0065 }
            r9 = 1
            r3.setDoOutput(r9)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r9 = "Content-type"
            java.lang.String r10 = "application/x-www-form-urlencoded;charset=utf-8"
            r3.addRequestProperty(r9, r10)     // Catch:{ IOException -> 0x0065 }
            r3.connect()     // Catch:{ IOException -> 0x0065 }
            java.io.OutputStream r4 = r3.getOutputStream()     // Catch:{ IOException -> 0x0065 }
            r5.writeTo(r4)     // Catch:{ IOException -> 0x0065 }
            r4.flush()     // Catch:{ IOException -> 0x0065 }
            java.io.InputStream r1 = r3.getInputStream()     // Catch:{ IOException -> 0x0065 }
            java.lang.String r7 = com.house365.core.util.FileUtil.convertStreamToString(r1)     // Catch:{ IOException -> 0x0065 }
            r3.disconnect()
        L_0x005b:
            return r7
        L_0x005c:
            java.net.URLConnection r9 = r8.openConnection()     // Catch:{ IOException -> 0x0065 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0065 }
            r3 = r0
            goto L_0x002e
        L_0x0065:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x006d }
            r3.disconnect()
            goto L_0x005b
        L_0x006d:
            r9 = move-exception
            r3.disconnect()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.helper.Downloadhelper.SendAndWaitResponse(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x00af A[SYNTHETIC, Splitter:B:48:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00b4 A[SYNTHETIC, Splitter:B:51:0x00b4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean urlDownloadToFile(android.content.Context r13, java.lang.String r14, java.lang.String r15) {
        /*
            r12 = this;
            r0 = 0
            r12.detectProxy()
            r5 = 0
            r7 = 0
            java.net.URL r10 = new java.net.URL     // Catch:{ IOException -> 0x00d0 }
            r10.<init>(r14)     // Catch:{ IOException -> 0x00d0 }
            r1 = 0
            java.net.Proxy r11 = r12.mProxy     // Catch:{ IOException -> 0x00d0 }
            if (r11 == 0) goto L_0x0072
            java.net.Proxy r11 = r12.mProxy     // Catch:{ IOException -> 0x00d0 }
            java.net.URLConnection r1 = r10.openConnection(r11)     // Catch:{ IOException -> 0x00d0 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ IOException -> 0x00d0 }
        L_0x0018:
            int r11 = r12.connectTimeout     // Catch:{ IOException -> 0x00d0 }
            r1.setConnectTimeout(r11)     // Catch:{ IOException -> 0x00d0 }
            int r11 = r12.readTimeout     // Catch:{ IOException -> 0x00d0 }
            r1.setReadTimeout(r11)     // Catch:{ IOException -> 0x00d0 }
            r11 = 1
            r1.setDoInput(r11)     // Catch:{ IOException -> 0x00d0 }
            int r11 = r1.getContentLength()     // Catch:{ IOException -> 0x00d0 }
            r12.maxSize = r11     // Catch:{ IOException -> 0x00d0 }
            r1.connect()     // Catch:{ IOException -> 0x00d0 }
            java.io.InputStream r5 = r1.getInputStream()     // Catch:{ IOException -> 0x00d0 }
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x00d0 }
            r3.<init>(r15)     // Catch:{ IOException -> 0x00d0 }
            r3.createNewFile()     // Catch:{ IOException -> 0x00d0 }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d0 }
            r8.<init>(r3)     // Catch:{ IOException -> 0x00d0 }
            boolean r11 = r12.isHorizontalLoading     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            if (r11 == 0) goto L_0x0051
            android.os.Message r6 = new android.os.Message     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r6.<init>()     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r11 = 4
            r6.what = r11     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            android.os.Handler r11 = r12.mHandler     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r11.sendMessage(r6)     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
        L_0x0051:
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r9 = new byte[r11]     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r4 = 0
        L_0x0056:
            int r4 = r5.read(r9)     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            if (r4 <= 0) goto L_0x0062
            boolean r11 = r12.isDownloading()     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            if (r11 != 0) goto L_0x0079
        L_0x0062:
            boolean r0 = r12.isDownloading()     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            if (r5 == 0) goto L_0x006b
            r5.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x006b:
            if (r8 == 0) goto L_0x00cb
            r8.close()     // Catch:{ IOException -> 0x00c7 }
            r7 = r8
        L_0x0071:
            return r0
        L_0x0072:
            java.net.URLConnection r1 = r10.openConnection()     // Catch:{ IOException -> 0x00d0 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ IOException -> 0x00d0 }
            goto L_0x0018
        L_0x0079:
            r11 = 0
            r8.write(r9, r11, r4)     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            boolean r11 = r12.isHorizontalLoading     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            if (r11 == 0) goto L_0x0056
            android.os.Message r6 = new android.os.Message     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r6.<init>()     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r11 = 0
            r6.what = r11     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r6.arg2 = r4     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            android.os.Handler r11 = r12.mHandler     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            r11.sendMessage(r6)     // Catch:{ IOException -> 0x0091, all -> 0x00cd }
            goto L_0x0056
        L_0x0091:
            r2 = move-exception
            r7 = r8
        L_0x0093:
            r2.printStackTrace()     // Catch:{ all -> 0x00ac }
            r0 = 0
            if (r5 == 0) goto L_0x009c
            r5.close()     // Catch:{ IOException -> 0x00a7 }
        L_0x009c:
            if (r7 == 0) goto L_0x0071
            r7.close()     // Catch:{ IOException -> 0x00a2 }
            goto L_0x0071
        L_0x00a2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0071
        L_0x00a7:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x009c
        L_0x00ac:
            r11 = move-exception
        L_0x00ad:
            if (r5 == 0) goto L_0x00b2
            r5.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x00b2:
            if (r7 == 0) goto L_0x00b7
            r7.close()     // Catch:{ IOException -> 0x00bd }
        L_0x00b7:
            throw r11
        L_0x00b8:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b2
        L_0x00bd:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b7
        L_0x00c2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006b
        L_0x00c7:
            r2 = move-exception
            r2.printStackTrace()
        L_0x00cb:
            r7 = r8
            goto L_0x0071
        L_0x00cd:
            r11 = move-exception
            r7 = r8
            goto L_0x00ad
        L_0x00d0:
            r2 = move-exception
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.helper.Downloadhelper.urlDownloadToFile(android.content.Context, java.lang.String, java.lang.String):boolean");
    }

    /* access modifiers changed from: private */
    public void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
                this.nowSize = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isDownloading() {
        return this.isDownloading;
    }

    public void setDownloading(boolean isDownloading2) {
        this.isDownloading = isDownloading2;
    }
}
