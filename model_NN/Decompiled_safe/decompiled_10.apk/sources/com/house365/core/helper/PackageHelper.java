package com.house365.core.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.house365.core.bean.VersionInfo;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.FileUtil;
import java.util.List;

public class PackageHelper {
    private Context mContext = null;
    private ProgressDialog mProgress = null;

    public PackageHelper(Context context) {
        this.mContext = context;
    }

    public static boolean isInstalledApp(Context context, String packagename) {
        List<PackageInfo> pkgList = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < pkgList.size(); i++) {
            PackageInfo pI = pkgList.get(i);
            if (pI.versionName != null && pI.packageName.equalsIgnoreCase(packagename)) {
                return true;
            }
        }
        return false;
    }

    public static void installApk(Context context, String apkfile) {
        FileUtil.isExistFile(apkfile);
    }

    public static VersionInfo getCurrentVersion(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            VersionInfo version = new VersionInfo();
            version.versionCode = info.versionCode;
            version.versionName = info.versionName;
            CorePreferences.DEBUG("��ǰ�汾��" + version.versionCode);
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
