package com.house365.core.image;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.widget.ImageView;
import com.house365.core.application.BaseApplication;
import com.house365.core.constant.CorePreferences;
import com.house365.core.util.store.DiskCache;
import com.house365.core.util.store.IOUtil;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.WeakHashMap;
import org.apache.commons.lang.StringUtils;

public class AsyncImageLoader {
    public static final int IMAGE_SCALE_TYPE_HIGH = 1;
    public static final int IMAGE_SCALE_TYPE_LOW = 3;
    public static final int IMAGE_SCALE_TYPE_MIDDLE = 2;
    public static final int IMAGE_SCALE_TYPE_NONE = -1;
    private static final int MSG_REPLY = 2;
    private static final int MSG_REQUEST = 1;
    private static final int MSG_STOP = 3;
    private final int IMAGE_HIGH_SCALE = 1;
    private final int IMAGE_MAX_SCALE = 4;
    public int IMAGE_MAX_SIZE = 480;
    private final int IMAGE_MIDDLE_SCALE = 2;
    private BaseApplication application;
    private Context context;
    /* access modifiers changed from: private */
    public WeakHashMap<String, WeakReference<Bitmap>> imageCache = new WeakHashMap<>();
    /* access modifiers changed from: private */
    public DiskCache mDiskCache;
    /* access modifiers changed from: private */
    public Handler mImageLoaderHandler;
    /* access modifiers changed from: private */
    public boolean mImageLoaderIdle = true;
    /* access modifiers changed from: private */
    public Handler mImageManagerHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 2:
                        ImageRef imageRef = (ImageRef) AsyncImageLoader.this.mRequestQueue.remove();
                        if (!(imageRef == null || imageRef.imageView == null || imageRef.imageView.getTag() == null || imageRef.url == null || !(msg.obj instanceof Bitmap) || msg.obj == null)) {
                            Bitmap bitmap = (Bitmap) msg.obj;
                            if (imageRef.url.equals((String) imageRef.imageView.getTag())) {
                                imageRef.handler.sendMessage(imageRef.handler.obtainMessage(0, bitmap));
                                break;
                            }
                        }
                        break;
                }
            }
            AsyncImageLoader.this.mImageLoaderIdle = true;
            if (AsyncImageLoader.this.mImageLoaderHandler != null) {
                AsyncImageLoader.this.sendRequest();
            }
        }
    };
    private Stack<ImageRef> mImageQueue = new Stack<>();
    /* access modifiers changed from: private */
    public Queue<ImageRef> mRequestQueue = new LinkedList();

    public interface BitMapInputStream {
        InputStream getInputStream() throws IOException;
    }

    public AsyncImageLoader(Context context2) {
        this.application = (BaseApplication) ((Activity) context2).getApplication();
        this.mDiskCache = this.application.getDiskCache();
        this.context = context2;
        if (this.application.getMetrics() != null && this.application.getMetrics().widthPixels > this.IMAGE_MAX_SIZE) {
            this.IMAGE_MAX_SIZE = this.application.getMetrics().widthPixels;
        }
    }

    public BaseApplication getApplication() {
        return this.application;
    }

    public void setApplication(BaseApplication application2) {
        this.application = application2;
    }

    public void loadDrawableByLocalFile(ImageView imageView, final String localFile, final ImageViewLoadListener imageCallback, int scaleType) {
        if (imageView != null) {
            if ((imageView.getTag() == null || !imageView.getTag().toString().equals(localFile)) && localFile != null && !localFile.equals(StringUtils.EMPTY)) {
                imageView.setTag(localFile);
                int w = imageView.getWidth();
                int h = imageView.getHeight();
                if (w <= 0) {
                    w = -1;
                }
                if (h <= 0) {
                    h = -1;
                }
                Handler handler = new Handler() {
                    public void handleMessage(Message message) {
                        if (message.what == 0) {
                            imageCallback.imageLoaded((Bitmap) message.obj, localFile);
                        } else if (message.what == 1) {
                            imageCallback.imageLoading();
                        } else {
                            imageCallback.imageLoadedFailure();
                        }
                    }
                };
                if (this.imageCache.containsKey(localFile)) {
                    Bitmap bitmap = (Bitmap) this.imageCache.get(localFile).get();
                    if (bitmap == null || bitmap.isRecycled()) {
                        this.imageCache.remove(localFile);
                    } else {
                        handler.sendMessage(handler.obtainMessage(0, bitmap));
                        return;
                    }
                }
                handler.sendMessage(handler.obtainMessage(1, null));
                queueImage(new ImageRef(imageView, localFile, scaleType, w, h, handler, false));
            }
        }
    }

    class ImageRef {
        boolean fromNet;
        Handler handler;
        int height = 0;
        ImageView imageView;
        int scaleType;
        String url;
        int width = 0;

        public ImageRef(ImageView imageView2, String url2, int scaleType2, int width2, int height2, Handler handler2, boolean fromNet2) {
            this.imageView = imageView2;
            this.url = url2;
            this.scaleType = scaleType2;
            this.width = width2;
            this.height = height2;
            this.handler = handler2;
            this.fromNet = fromNet2;
        }
    }

    public synchronized void queueImage(ImageRef imageRef) {
        try {
            Iterator<ImageRef> iterator = this.mImageQueue.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().imageView == imageRef.imageView) {
                    iterator.remove();
                }
            }
        } catch (Exception e) {
            CorePreferences.ERROR(e);
        }
        this.mImageQueue.push(imageRef);
        sendRequest();
        return;
    }

    /* access modifiers changed from: private */
    public void sendRequest() {
        if (this.mImageLoaderHandler == null) {
            HandlerThread imageLoader = new HandlerThread("image_loader");
            imageLoader.start();
            this.mImageLoaderHandler = new ImageLoaderHandler(imageLoader.getLooper());
        }
        if (this.mImageLoaderIdle && this.mImageQueue.size() > 0) {
            ImageRef imageRef = this.mImageQueue.pop();
            this.mImageLoaderHandler.sendMessage(this.mImageLoaderHandler.obtainMessage(1, imageRef));
            this.mImageLoaderIdle = false;
            this.mRequestQueue.add(imageRef);
        }
    }

    class ImageLoaderHandler extends Handler {
        public ImageLoaderHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 1:
                        Bitmap bitmap = null;
                        if (msg.obj != null && (msg.obj instanceof ImageRef)) {
                            ImageRef imageRef = (ImageRef) msg.obj;
                            final String url = imageRef.url;
                            if (url != null) {
                                try {
                                    if (!imageRef.fromNet) {
                                        bitmap = AsyncImageLoader.this.getBitMapFromStream(new BitMapInputStream() {
                                            public InputStream getInputStream() throws IOException {
                                                return new FileInputStream(url);
                                            }
                                        }, imageRef.width, imageRef.height, imageRef.scaleType);
                                        AsyncImageLoader.this.imageCache.put(url, new WeakReference(bitmap));
                                    } else if (AsyncImageLoader.this.mDiskCache.exists(url)) {
                                        bitmap = AsyncImageLoader.this.getBitMapFromStream(new BitMapInputStream() {
                                            public InputStream getInputStream() throws IOException {
                                                return AsyncImageLoader.this.mDiskCache.getInputStream(url);
                                            }
                                        }, imageRef.width, imageRef.height, imageRef.scaleType);
                                        if (bitmap != null && !bitmap.isRecycled()) {
                                            AsyncImageLoader.this.imageCache.put(url, new WeakReference(bitmap));
                                        }
                                    } else {
                                        bitmap = AsyncImageLoader.this.loadImageFromUrl(url, imageRef.width, imageRef.height, imageRef.scaleType);
                                        AsyncImageLoader.this.imageCache.put(url, new WeakReference(bitmap));
                                    }
                                } catch (Exception e) {
                                    imageRef.handler.sendMessage(imageRef.handler.obtainMessage(2, null));
                                    CorePreferences.ERROR(StringUtils.EMPTY, e);
                                }
                            } else {
                                return;
                            }
                        }
                        if (AsyncImageLoader.this.mImageManagerHandler != null) {
                            AsyncImageLoader.this.mImageManagerHandler.sendMessage(AsyncImageLoader.this.mImageManagerHandler.obtainMessage(2, bitmap));
                            return;
                        }
                        return;
                    case 2:
                    default:
                        return;
                    case 3:
                        Looper.myLooper().quit();
                        return;
                }
            }
        }
    }

    public void loadDrawable(ImageView imageView, final String imageUrl, final ImageViewLoadListener imageCallback, int scaleType) {
        if (imageView != null && imageUrl != null && !imageUrl.equals(StringUtils.EMPTY)) {
            final Handler handler = new Handler() {
                public void handleMessage(Message message) {
                    if (message.what == 0) {
                        imageCallback.imageLoaded((Bitmap) message.obj, imageUrl);
                    } else if (message.what == 1) {
                        imageCallback.imageLoading();
                    } else {
                        imageCallback.imageLoadedFailure();
                    }
                }
            };
            final ImageView imageView2 = imageView;
            final String str = imageUrl;
            final int i = scaleType;
            new Thread() {
                public void run() {
                    imageView2.setTag(str);
                    int w = imageView2.getWidth();
                    int h = imageView2.getHeight();
                    if (w <= 0) {
                        w = -1;
                    }
                    if (h <= 0) {
                        h = -1;
                    }
                    if (AsyncImageLoader.this.imageCache.containsKey(str)) {
                        Bitmap bitmap = (Bitmap) ((WeakReference) AsyncImageLoader.this.imageCache.get(str)).get();
                        if (bitmap == null || bitmap.isRecycled()) {
                            AsyncImageLoader.this.imageCache.remove(str);
                        } else {
                            handler.sendMessage(handler.obtainMessage(0, bitmap));
                            return;
                        }
                    }
                    handler.sendMessage(handler.obtainMessage(1, null));
                    AsyncImageLoader.this.queueImage(new ImageRef(imageView2, str, i, w, h, handler, true));
                }
            }.start();
        }
    }

    public Bitmap loadImageFromUrl(final String url, int w, int h, int scaleType) {
        Bitmap b = null;
        InputStream is = null;
        try {
            is = this.application.getApi().getWithStream(url, null);
            if (this.mDiskCache.aviable()) {
                this.mDiskCache.store(url, new FlushedInputStream(is));
                b = getBitMapFromStream(new BitMapInputStream() {
                    public InputStream getInputStream() throws IOException {
                        return AsyncImageLoader.this.mDiskCache.getInputStream(url);
                    }
                }, w, h, scaleType);
            } else {
                final InputStream is2 = is;
                b = getBitMapFromStream(new BitMapInputStream() {
                    public InputStream getInputStream() throws IOException {
                        return is2;
                    }
                }, w, h, scaleType);
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
            throw th;
        }
        return b;
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    public void clearCacheImage() {
        this.mImageQueue.clear();
        for (WeakReference<Bitmap> softReference : this.imageCache.values()) {
            if (softReference.get() != null) {
                ((Bitmap) softReference.get()).recycle();
                softReference.clear();
            }
        }
        this.imageCache.clear();
    }

    public void invalidate(String imageUrl) {
        WeakReference<Bitmap> softReference = this.imageCache.get(imageUrl);
        if (!(softReference == null || softReference.get() == null)) {
            ((Bitmap) softReference.get()).recycle();
            softReference.clear();
        }
        this.imageCache.remove(imageUrl);
        if (this.mDiskCache.exists(imageUrl)) {
            this.mDiskCache.invalidate(imageUrl);
        }
    }

    public Bitmap getBitMapFromStream(BitMapInputStream inputStream, int w, int h, int scaleType) throws IOException {
        int i = -1;
        try {
            byte[] data = IOUtil.toByteArray(inputStream.getInputStream());
            if (data == null) {
                return null;
            }
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, opts);
            int min = (w > 0 || h > 0) ? Math.min(w, h) : -1;
            if (w > 0 || h > 0) {
                i = w * h;
            }
            int inSampleSize = computeSampleSize(opts, min, i, scaleType);
            CorePreferences.DEBUG("[AsyncImageLoader.inSampleSize]" + inSampleSize);
            opts.inSampleSize = inSampleSize;
            opts.inJustDecodeBounds = false;
            return BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            return null;
        }
    }

    public int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels, int scaleType) {
        int roundedSize;
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = ((initialSize + 7) / 8) * 8;
        }
        if (scaleType == 3) {
            if (roundedSize > 4) {
                return 4;
            }
            return roundedSize;
        } else if (scaleType == 2) {
            if (roundedSize > 2) {
                return 2;
            }
            return roundedSize;
        } else if (scaleType != 1 || roundedSize <= 1) {
            return roundedSize;
        } else {
            return 1;
        }
    }

    private int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        double w = (double) options.outWidth;
        double h = (double) options.outHeight;
        int lowerBound = maxNumOfPixels == -1 ? 1 : (int) Math.ceil(Math.sqrt((w * h) / ((double) maxNumOfPixels)));
        int upperBound = minSideLength == -1 ? 128 : (int) Math.min(Math.floor(w / ((double) minSideLength)), Math.floor(h / ((double) minSideLength)));
        if (upperBound < lowerBound) {
            return lowerBound;
        }
        if (maxNumOfPixels == -1 && minSideLength == -1) {
            return 1;
        }
        if (minSideLength != -1) {
            return upperBound;
        }
        return lowerBound;
    }
}
