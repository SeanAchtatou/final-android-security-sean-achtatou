package com.house365.core.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

public class ImageUtil {
    private static final int OPTIONS_NONE = 0;
    public static final int OPTIONS_RECYCLE_INPUT = 2;
    private static final int OPTIONS_SCALE_UP = 1;

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x004a A[SYNTHETIC, Splitter:B:33:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x005c A[SYNTHETIC, Splitter:B:43:0x005c] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x002a=Splitter:B:14:0x002a, B:26:0x003f=Splitter:B:26:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getBitmapbyUrl(java.lang.String r7) {
        /*
            r4 = 0
            r0 = 0
            r2 = 0
            r3 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x003e }
            r5.<init>(r7)     // Catch:{ MalformedURLException -> 0x0029, IOException -> 0x003e }
            java.net.URLConnection r0 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x006d, all -> 0x006a }
            r6 = 3000(0xbb8, float:4.204E-42)
            r0.setConnectTimeout(r6)     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x006d, all -> 0x006a }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x006d, all -> 0x006a }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x006d, all -> 0x006a }
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ MalformedURLException -> 0x0070, IOException -> 0x006d, all -> 0x006a }
            if (r5 == 0) goto L_0x0073
            r4 = 0
        L_0x0020:
            if (r0 == 0) goto L_0x0023
            r0 = 0
        L_0x0023:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0028:
            return r3
        L_0x0029:
            r1 = move-exception
        L_0x002a:
            r1.printStackTrace()     // Catch:{ all -> 0x0053 }
            if (r4 == 0) goto L_0x0030
            r4 = 0
        L_0x0030:
            if (r0 == 0) goto L_0x0033
            r0 = 0
        L_0x0033:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0039 }
            goto L_0x0028
        L_0x0039:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0028
        L_0x003e:
            r1 = move-exception
        L_0x003f:
            r1.printStackTrace()     // Catch:{ all -> 0x0053 }
            if (r4 == 0) goto L_0x0045
            r4 = 0
        L_0x0045:
            if (r0 == 0) goto L_0x0048
            r0 = 0
        L_0x0048:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x004e }
            goto L_0x0028
        L_0x004e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0028
        L_0x0053:
            r6 = move-exception
        L_0x0054:
            if (r4 == 0) goto L_0x0057
            r4 = 0
        L_0x0057:
            if (r0 == 0) goto L_0x005a
            r0 = 0
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005f:
            throw r6
        L_0x0060:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005f
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0028
        L_0x006a:
            r6 = move-exception
            r4 = r5
            goto L_0x0054
        L_0x006d:
            r1 = move-exception
            r4 = r5
            goto L_0x003f
        L_0x0070:
            r1 = move-exception
            r4 = r5
            goto L_0x002a
        L_0x0073:
            r4 = r5
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.house365.core.image.ImageUtil.getBitmapbyUrl(java.lang.String):android.graphics.Bitmap");
    }

    public static String saveImage(Bitmap bm, String file, int quality) throws IOException {
        if (bm != null) {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(file)));
            bm.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            bos.flush();
            bos.close();
        }
        return file;
    }

    public static Bitmap returnBitMap(String url) {
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (bitmap != null) {
            return getRoundedCornerBitmap(bitmap);
        }
        return null;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, 8.0f, 8.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float percentoflen) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, ((float) bitmap.getWidth()) * percentoflen, ((float) bitmap.getHeight()) * percentoflen, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap setAlpha(Bitmap sourceImg, int number) {
        int[] argb = new int[(sourceImg.getWidth() * sourceImg.getHeight())];
        sourceImg.getPixels(argb, 0, sourceImg.getWidth(), 0, 0, sourceImg.getWidth(), sourceImg.getHeight());
        int number2 = (number * MotionEventCompat.ACTION_MASK) / 100;
        for (int i = 0; i < argb.length; i++) {
            argb[i] = (number2 << 24) | (argb[i] & 16777215);
        }
        return Bitmap.createBitmap(argb, sourceImg.getWidth(), sourceImg.getHeight(), Bitmap.Config.ARGB_8888);
    }

    public static void asyncImg(String imageUrl, final ImageView imageView, Context context) {
        if (imageUrl == null || imageUrl.equals(StringUtils.EMPTY)) {
            imageView.setImageBitmap(null);
        } else {
            new AsyncImageLoader(context).loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                public void imageLoaded(Bitmap bitmap, String imageUrl) {
                    if (imageView != null && bitmap != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                }

                public void imageLoading() {
                }

                public void imageLoadedFailure() {
                }
            }, -1);
        }
    }

    public static void showImageWithRounderCorner(ImageView imageView, Bitmap cachedImage) {
        imageView.setImageBitmap(getRoundedCornerBitmap(cachedImage));
        AlphaAnimation alphaAnimation = new AlphaAnimation((float) SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
        alphaAnimation.setDuration(400);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        imageView.startAnimation(alphaAnimation);
    }

    public static void showImage(ImageView imageView, Bitmap cachedImage, long duration) {
        imageView.setImageBitmap(cachedImage);
        if (duration > 0) {
            AlphaAnimation alphaAnimation = new AlphaAnimation((float) SystemUtils.JAVA_VERSION_FLOAT, 1.0f);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            alphaAnimation.setDuration(400);
            imageView.startAnimation(alphaAnimation);
        }
    }

    public static Bitmap extractThumbImg(Bitmap source, int width, int height) {
        return extractThumbImg(source, width, height, 0);
    }

    public static Bitmap extractThumbImg(Bitmap source, int width, int height, int options) {
        float scale;
        if (source == null) {
            return null;
        }
        if (source.getWidth() < source.getHeight()) {
            scale = ((float) width) / ((float) source.getWidth());
        } else {
            scale = ((float) height) / ((float) source.getHeight());
        }
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        return transform(matrix, source, width, height, options | 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap transform(Matrix scaler, Bitmap source, int targetWidth, int targetHeight, int options) {
        Bitmap b1;
        Bitmap b2;
        boolean scaleUp = (options & 1) != 0;
        boolean recycle = (options & 2) != 0;
        int deltaX = source.getWidth() - targetWidth;
        int deltaY = source.getHeight() - targetHeight;
        if (scaleUp || (deltaX >= 0 && deltaY >= 0)) {
            float bitmapWidthF = (float) source.getWidth();
            float bitmapHeightF = (float) source.getHeight();
            if (bitmapWidthF / bitmapHeightF > ((float) targetWidth) / ((float) targetHeight)) {
                float scale = ((float) targetHeight) / bitmapHeightF;
                if (scale < 0.9f || scale > 1.0f) {
                    scaler.setScale(scale, scale);
                } else {
                    scaler = null;
                }
            } else {
                float scale2 = ((float) targetWidth) / bitmapWidthF;
                if (scale2 < 0.9f || scale2 > 1.0f) {
                    scaler.setScale(scale2, scale2);
                } else {
                    scaler = null;
                }
            }
            if (scaler != null) {
                b1 = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), scaler, true);
            } else {
                b1 = source;
            }
            if (recycle && b1 != source) {
                source.recycle();
            }
            b2 = Bitmap.createBitmap(b1, Math.max(0, b1.getWidth() - targetWidth) / 2, Math.max(0, b1.getHeight() - targetHeight) / 2, targetWidth, targetHeight);
            if (b2 != b1 && (recycle || b1 != source)) {
                b1.recycle();
            }
        } else {
            b2 = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b2);
            int deltaXHalf = Math.max(0, deltaX / 2);
            int deltaYHalf = Math.max(0, deltaY / 2);
            Rect rect = new Rect(deltaXHalf, deltaYHalf, Math.min(targetWidth, source.getWidth()) + deltaXHalf, Math.min(targetHeight, source.getHeight()) + deltaYHalf);
            int dstX = (targetWidth - rect.width()) / 2;
            int dstY = (targetHeight - rect.height()) / 2;
            c.drawBitmap(source, rect, new Rect(dstX, dstY, targetWidth - dstX, targetHeight - dstY), (Paint) null);
            if (recycle) {
                source.recycle();
            }
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap createReflectedImage(Bitmap originalImage) {
        try {
            int width = originalImage.getWidth();
            int height = originalImage.getHeight();
            Matrix matrix = new Matrix();
            matrix.preScale(1.0f, -1.0f);
            Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height / 2, width, height / 2, matrix, false);
            Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height / 2) + height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmapWithReflection);
            canvas.drawBitmap(originalImage, (float) SystemUtils.JAVA_VERSION_FLOAT, (float) SystemUtils.JAVA_VERSION_FLOAT, (Paint) null);
            canvas.drawRect(SystemUtils.JAVA_VERSION_FLOAT, (float) height, (float) width, (float) (height + 4), new Paint());
            canvas.drawBitmap(reflectionImage, (float) SystemUtils.JAVA_VERSION_FLOAT, (float) (height + 4), (Paint) null);
            Paint paint = new Paint();
            paint.setShader(new LinearGradient((float) SystemUtils.JAVA_VERSION_FLOAT, (float) originalImage.getHeight(), (float) SystemUtils.JAVA_VERSION_FLOAT, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            canvas.drawRect(SystemUtils.JAVA_VERSION_FLOAT, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
            return bitmapWithReflection;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            return null;
        }
    }

    public static Bitmap drawableConverBitmap(Drawable drawable) {
        Bitmap.Config config;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap getBitMapFromFile(String file) {
        try {
            return BitmapFactory.decodeFile(file);
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    public static byte[] bitmapToByte(Bitmap b) {
        if (b == null) {
            return null;
        }
        ByteArrayOutputStream o = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, o);
        return o.toByteArray();
    }

    public static Bitmap drawableToBitmap(Drawable d) {
        if (d == null) {
            return null;
        }
        return ((BitmapDrawable) d).getBitmap();
    }

    public static Drawable bitmapToDrawable(Bitmap b) {
        if (b == null) {
            return null;
        }
        return new BitmapDrawable(b);
    }

    public static byte[] drawableToByte(Drawable d) {
        return bitmapToByte(drawableToBitmap(d));
    }

    public static InputStream getInputStreamFromUrl(String imageUrl) {
        try {
            return (InputStream) new URL(imageUrl).getContent();
        } catch (MalformedURLException e) {
            closeInputStream(null);
            throw new RuntimeException("MalformedURLException occurred. ", e);
        } catch (IOException e2) {
            closeInputStream(null);
            throw new RuntimeException("IOException occurred. ", e2);
        }
    }

    public static Drawable getDrawableFromUrl(String imageUrl) {
        InputStream stream = getInputStreamFromUrl(imageUrl);
        Drawable d = Drawable.createFromStream(stream, "src");
        closeInputStream(stream);
        return d;
    }

    public static Bitmap getBitmapFromUrl(String imageUrl) {
        InputStream stream = getInputStreamFromUrl(imageUrl);
        Bitmap b = BitmapFactory.decodeStream(stream);
        closeInputStream(stream);
        return b;
    }

    private static void closeInputStream(InputStream s) {
        if (s != null) {
            try {
                s.close();
            } catch (IOException e) {
                throw new RuntimeException("IOException occurred. ", e);
            }
        }
    }
}
