package com.house365.core.image;

public interface ImageLoadedCallback {
    void imageLoadedFail();

    void imageLoadedSuccess();
}
