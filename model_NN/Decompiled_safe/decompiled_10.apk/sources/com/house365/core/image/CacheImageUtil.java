package com.house365.core.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.house365.core.touch.ImageViewTouch;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public class CacheImageUtil {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$house365$core$image$CacheImageUtil$TextImagePosition;
    private static WeakHashMap<String, WeakReference<Bitmap>> resourceImageCache = new WeakHashMap<>();

    public interface ImageOperate {
        Bitmap operate(Bitmap bitmap);
    }

    public enum TextImagePosition {
        background,
        left,
        top,
        right,
        bottom
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$house365$core$image$CacheImageUtil$TextImagePosition() {
        int[] iArr = $SWITCH_TABLE$com$house365$core$image$CacheImageUtil$TextImagePosition;
        if (iArr == null) {
            iArr = new int[TextImagePosition.values().length];
            try {
                iArr[TextImagePosition.background.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[TextImagePosition.bottom.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[TextImagePosition.left.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[TextImagePosition.right.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[TextImagePosition.top.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$house365$core$image$CacheImageUtil$TextImagePosition = iArr;
        }
        return iArr;
    }

    public static void setCacheImage(final ImageView imageView, String imageUrl, final int resId, int scaleType, AsyncImageLoader mAil) {
        imageView.setImageResource(resId);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        if (!TextUtils.isEmpty(imageUrl)) {
            if (mAil.getApplication().isEnableImg()) {
                mAil.loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                    public void imageLoaded(Bitmap bitmap, String imageUrl) {
                        if (bitmap == null || bitmap.isRecycled()) {
                            imageView.setImageResource(resId);
                            imageView.setScaleType(ImageView.ScaleType.CENTER);
                            return;
                        }
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        ImageUtil.showImage(imageView, bitmap, 0);
                    }

                    public void imageLoading() {
                        imageView.setImageResource(resId);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                    }

                    public void imageLoadedFailure() {
                    }
                }, scaleType);
                return;
            }
            imageView.setImageResource(resId);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
        }
    }

    public static void setCacheImage(final ImageView imageView, String imageUrl, int scaleType, AsyncImageLoader mAil, final ImageLoadedCallback imgLoadedObj) {
        if (TextUtils.isEmpty(imageUrl)) {
            imgLoadedObj.imageLoadedFail();
        } else if (mAil.getApplication().isEnableImg()) {
            int w = imageView.getWidth();
            int h = imageView.getHeight();
            if (w < 0) {
            }
            if (h < 0) {
            }
            mAil.loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                public void imageLoaded(Bitmap bitmap, String imageUrl) {
                    if (bitmap == null || bitmap.isRecycled()) {
                        imgLoadedObj.imageLoadedFail();
                        return;
                    }
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    ImageUtil.showImage(imageView, bitmap, 0);
                    imgLoadedObj.imageLoadedSuccess();
                }

                public void imageLoadedFailure() {
                    imgLoadedObj.imageLoadedFail();
                }

                public void imageLoading() {
                }
            }, scaleType);
        } else {
            imgLoadedObj.imageLoadedFail();
        }
    }

    public static void setTouchImage(final ImageViewTouch imageView, String imageUrl, final Resources resource, final int resId, int scaleType, AsyncImageLoader mAil) {
        if (TextUtils.isEmpty(imageUrl)) {
            imageView.setImageBitmapResetBase(getResourceBitMap(resource, resId), true);
        } else if (mAil.getApplication().isEnableImg()) {
            int w = imageView.getWidth();
            int h = imageView.getHeight();
            if (w < 0) {
            }
            if (h < 0) {
            }
            mAil.loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                public void imageLoaded(Bitmap bitmap, String imageUrl) {
                    if (bitmap == null || bitmap.isRecycled()) {
                        ImageViewTouch.this.setImageBitmapResetBase(CacheImageUtil.getResourceBitMap(resource, resId), true);
                    } else {
                        ImageViewTouch.this.setImageBitmapResetBase(bitmap, true);
                    }
                }

                public void imageLoadedFailure() {
                }

                public void imageLoading() {
                    ImageViewTouch.this.setImageBitmapResetBase(CacheImageUtil.getResourceBitMap(resource, resId), true);
                }
            }, scaleType);
        } else {
            imageView.setImageBitmapResetBase(getResourceBitMap(resource, resId), true);
        }
    }

    public static Bitmap getResourceBitMap(Resources resource, int resId) {
        Bitmap bitmap = null;
        String key = new StringBuilder(String.valueOf(resId)).toString();
        if (resourceImageCache.containsKey(key)) {
            bitmap = (Bitmap) resourceImageCache.get(key).get();
            if (bitmap != null && !bitmap.isRecycled()) {
                return bitmap;
            }
            resourceImageCache.remove(key);
        }
        while (bitmap == null) {
            try {
                bitmap = BitmapFactory.decodeResource(resource, resId);
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                System.gc();
            }
        }
        return bitmap;
    }

    public static void setCacheTextImage(TextView textView, TextImagePosition position, String imageUrl, AsyncImageLoader mAil) {
        Bitmap bitmap;
        if (!TextUtils.isEmpty(imageUrl) && (bitmap = mAil.loadImageFromUrl(imageUrl, 0, 0, -1)) != null && !bitmap.isRecycled()) {
            Drawable drawable = new BitmapDrawable(bitmap);
            switch ($SWITCH_TABLE$com$house365$core$image$CacheImageUtil$TextImagePosition()[position.ordinal()]) {
                case 1:
                    textView.setBackgroundDrawable(drawable);
                    return;
                case 2:
                    textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, drawable, (Drawable) null, (Drawable) null);
                    return;
                case 3:
                    textView.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    return;
                case 4:
                    textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, drawable, (Drawable) null);
                    return;
                case 5:
                    textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, drawable);
                    return;
                default:
                    return;
            }
        }
    }

    public static void setCacheImageWithImageOper(final ImageView imageView, String imageUrl, final ImageOperate imageOperate, Resources resource, final int resId, int scaleType, AsyncImageLoader mAil) {
        imageView.setImageResource(resId);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        if (!TextUtils.isEmpty(imageUrl)) {
            if (mAil.getApplication().isEnableImg()) {
                mAil.loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                    public void imageLoaded(Bitmap bitmap, String imageUrl) {
                        if (bitmap == null || bitmap.isRecycled()) {
                            imageView.setImageResource(resId);
                            imageView.setScaleType(ImageView.ScaleType.CENTER);
                            return;
                        }
                        Bitmap bitmap2 = ImageOperate.this.operate(bitmap);
                        if (bitmap2 != null && !bitmap2.isRecycled()) {
                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                            ImageUtil.showImage(imageView, bitmap2, 0);
                        }
                    }

                    public void imageLoadedFailure() {
                    }

                    public void imageLoading() {
                        imageView.setImageResource(resId);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                    }
                }, scaleType);
                return;
            }
            imageView.setImageResource(resId);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
        }
    }

    public static void setCacheImageWithoutDef(final ImageView imageView, String imageUrl, int scaleType, AsyncImageLoader mAil) {
        if (!TextUtils.isEmpty(imageUrl)) {
            mAil.loadDrawable(imageView, imageUrl, new ImageViewLoadListener() {
                public void imageLoaded(Bitmap bitmap, String imageUrl) {
                    if (bitmap != null && !bitmap.isRecycled()) {
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        ImageUtil.showImage(imageView, bitmap, 0);
                    }
                }

                public void imageLoadedFailure() {
                }

                public void imageLoading() {
                }
            }, scaleType);
        }
    }

    public static void setCacheImageFromLocalFile(final ImageView imageView, String localFile, final int resId, int scaleType, AsyncImageLoader mAil) {
        imageView.setImageResource(resId);
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        if (!TextUtils.isEmpty(localFile)) {
            mAil.loadDrawableByLocalFile(imageView, localFile, new ImageViewLoadListener() {
                public void imageLoaded(Bitmap bitmap, String imageUrl) {
                    if (bitmap == null || bitmap.isRecycled()) {
                        imageView.setImageResource(resId);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                        return;
                    }
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    ImageUtil.showImage(imageView, bitmap, 0);
                }

                public void imageLoading() {
                    imageView.setImageResource(resId);
                    imageView.setScaleType(ImageView.ScaleType.CENTER);
                }

                public void imageLoadedFailure() {
                }
            }, scaleType);
        }
    }
}
