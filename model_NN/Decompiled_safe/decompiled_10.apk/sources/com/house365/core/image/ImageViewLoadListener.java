package com.house365.core.image;

import android.graphics.Bitmap;

public interface ImageViewLoadListener {
    void imageLoaded(Bitmap bitmap, String str);

    void imageLoadedFailure();

    void imageLoading();
}
