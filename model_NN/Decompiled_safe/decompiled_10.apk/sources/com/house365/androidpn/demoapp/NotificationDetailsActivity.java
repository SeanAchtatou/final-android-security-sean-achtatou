package com.house365.androidpn.demoapp;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.dto.NotificationData;
import com.house365.androidpn.client.util.LogUtil;

public class NotificationDetailsActivity extends Activity {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationDetailsActivity.class);
    private String callbackActivityClassName;
    private String callbackActivityPackageName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NotificationData notificationData = (NotificationData) getIntent().getSerializableExtra(Constants.NOTIFICATION_DATA);
        setContentView(createView(notificationData.getTitle(), notificationData.getMessage(), notificationData.getUri()));
    }

    private View createView(String title, String message, String uri) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setBackgroundColor(-1118482);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        TextView textTitle = new TextView(this);
        textTitle.setText(title);
        textTitle.setTextSize(18.0f);
        textTitle.setTypeface(Typeface.DEFAULT, 1);
        textTitle.setTextColor(-16777216);
        textTitle.setGravity(17);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(30, 30, 30, 0);
        textTitle.setLayoutParams(layoutParams);
        linearLayout.addView(textTitle);
        TextView textDetails = new TextView(this);
        textDetails.setText(message);
        textDetails.setTextSize(14.0f);
        textDetails.setTextColor(-13421773);
        textDetails.setGravity(17);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(30, 10, 30, 20);
        textDetails.setLayoutParams(layoutParams2);
        linearLayout.addView(textDetails);
        Button okButton = new Button(this);
        okButton.setText("Ok");
        okButton.setWidth(100);
        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                NotificationDetailsActivity.this.finish();
            }
        });
        LinearLayout innerLayout = new LinearLayout(this);
        innerLayout.setGravity(17);
        innerLayout.addView(okButton);
        linearLayout.addView(innerLayout);
        return linearLayout;
    }
}
