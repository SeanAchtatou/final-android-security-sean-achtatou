package com.house365.androidpn.demoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.PullAccount;
import com.house365.androidpn.client.PullConfiguration;
import com.house365.androidpn.client.PullServiceManager;

public class DemoAppActivity extends Activity {
    PullAccount account;
    private Button button;
    /* access modifiers changed from: private */
    public String deviceId;
    /* access modifiers changed from: private */
    public String phoneNo;
    private RadioGroup radioGroup;
    private PullServiceManager serviceManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serviceManager = new PullServiceManager(this, new PullConfiguration("testpn.com.house365.androidpn.service.NotificationService", "testpn", NotificationDetailsActivity.class.getName(), "com.house365.apn.client.broadcast", R.drawable.icon, "1234567890", "192.168.107.67", 5222));
        setContentView(R.layout.main);
        this.radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        TelephonyManager telManager = (TelephonyManager) getSystemService("phone");
        this.deviceId = telManager.getDeviceId();
        this.phoneNo = telManager.getLine1Number();
        this.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int id = group.getCheckedRadioButtonId();
                if (id == R.id.radioButton1) {
                    DemoAppActivity.this.account = new PullAccount(DemoAppActivity.this.deviceId, "123", 1, "testpn");
                } else if (id == R.id.radioButton2) {
                    DemoAppActivity.this.account = new PullAccount(DemoAppActivity.this.phoneNo == null ? "130000000" : DemoAppActivity.this.phoneNo, "456", 2, "testpn");
                } else {
                    DemoAppActivity.this.account = new PullAccount("110739666", "789", 3, "testpn");
                }
            }
        });
        this.serviceManager.startService();
        this.button = (Button) findViewById(R.id.btn_start);
        this.account = new PullAccount(this.deviceId, "123", 1, "testpn");
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Constants.getActionPullaccountChange(DemoAppActivity.this));
                intent.putExtra(Constants.ACTION_PULLACCOUNT_FIELD_ACCOUNT, DemoAppActivity.this.account);
                DemoAppActivity.this.sendBroadcast(intent);
            }
        });
        this.serviceManager.setNotificationEnable(true);
    }
}
