package com.house365.androidpn.client;

import android.text.TextUtils;

public class PullConfiguration {
    private String actionPrefix;
    private String apiKey;
    private String broadcastAction;
    private String host;
    private int iconId;
    private String notifyDetailClass;
    private int port;
    private String serviceAction;
    private String version = "0.5.0";

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getNotifyDetailClass() {
        return this.notifyDetailClass;
    }

    public void setNotifyDetailClass(String notifyDetailClass2) {
        this.notifyDetailClass = notifyDetailClass2;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void setIconId(int iconId2) {
        this.iconId = iconId2;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        this.apiKey = apiKey2;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host2) {
        if (TextUtils.isEmpty(host2)) {
            host2 = "127.0.0.1";
        }
        this.host = host2;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port2) {
        this.port = port2;
    }

    public PullConfiguration(String serviceAction2, String actionPrefix2, String notifyDetailClass2, int iconId2, String apiKey2, String host2, int port2) {
        this.notifyDetailClass = notifyDetailClass2;
        this.iconId = iconId2;
        this.apiKey = apiKey2;
        this.host = host2;
        this.port = port2;
        this.serviceAction = serviceAction2;
        this.actionPrefix = actionPrefix2;
    }

    public PullConfiguration(String serviceAction2, String actionPrefix2, String notifyDetailClass2, String broadcastAction2, int iconId2, String apiKey2, String host2, int port2) {
        this.notifyDetailClass = notifyDetailClass2;
        this.iconId = iconId2;
        this.apiKey = apiKey2;
        this.host = host2;
        this.port = port2;
        this.serviceAction = serviceAction2;
        this.actionPrefix = actionPrefix2;
        this.broadcastAction = broadcastAction2;
    }

    public String getServiceAction() {
        return this.serviceAction;
    }

    public void setServiceAction(String serviceAction2) {
        this.serviceAction = serviceAction2;
    }

    public String getActionPrefix() {
        return this.actionPrefix;
    }

    public void setActionPrefix(String actionPrefix2) {
        this.actionPrefix = actionPrefix2;
    }

    public String getBroadcastAction() {
        return this.broadcastAction;
    }

    public void setBroadcastAction(String broadcastAction2) {
        this.broadcastAction = broadcastAction2;
    }
}
