package com.house365.androidpn.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import com.house365.androidpn.client.listener.PersistentConnectionListener;
import com.house365.androidpn.client.net.NotificationIQ;
import com.house365.androidpn.client.net.NotificationIQProvider;
import com.house365.androidpn.client.net.NotificationPacketListener;
import com.house365.androidpn.client.service.NotificationService;
import com.house365.androidpn.client.util.LogUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.provider.ProviderManager;

public class XmppManager {
    /* access modifiers changed from: private */
    public static final String LOGTAG = LogUtil.makeLogTag(XmppManager.class);
    public static final int WAKE_LOCK_CLOSE = 2;
    public static final int WAKE_LOCK_OPEN = 1;
    private static final String XMPP_RESOURCE_NAME = "AndroidpnClient";
    /* access modifiers changed from: private */
    public XMPPConnection connection;
    private ConnectionListener connectionListener;
    private Context context;
    private Future<?> futureTask;
    private Handler handler;
    private long lockHoldTime = 0;
    private PacketListener notificationPacketListener;
    /* access modifiers changed from: private */
    public PullAccount pullAccount;
    private boolean running = false;
    /* access modifiers changed from: private */
    public SharedPreferences sharedPrefs;
    private List<Runnable> taskList;
    private NotificationService.TaskSubmitter taskSubmitter;
    private NotificationService.TaskTracker taskTracker;
    private PowerManager.WakeLock wakeLock;
    /* access modifiers changed from: private */
    public String xmppHost;
    /* access modifiers changed from: private */
    public int xmppPort;

    public XmppManager(NotificationService notificationService) {
        this.context = notificationService;
        this.taskSubmitter = notificationService.getTaskSubmitter();
        this.taskTracker = notificationService.getTaskTracker();
        this.sharedPrefs = notificationService.getSharedPreferences();
        this.xmppHost = this.sharedPrefs.getString(Constants.XMPP_HOST, "localhost");
        this.xmppPort = this.sharedPrefs.getInt(Constants.XMPP_PORT, 5222);
        this.connectionListener = new PersistentConnectionListener(this);
        this.notificationPacketListener = new NotificationPacketListener(this);
        this.handler = new Handler();
        this.taskList = new ArrayList();
    }

    public Handler getHandler() {
        return this.handler;
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    public Context getContext() {
        return this.context;
    }

    public void connect() {
        Log.d(LOGTAG, "connect()...");
        this.pullAccount = getPullAccount();
        Log.d(LOGTAG, "pullAccount()...:" + this.pullAccount);
        if (this.pullAccount != null) {
            Log.d(LOGTAG, "pullAccount()...:" + this.pullAccount.getSerNo());
            acquireWakeLock();
            ConnectTask();
            RegisterTask();
            LoginTask();
            releaseWakeLock();
        }
    }

    public void disconnect() {
        Log.d(LOGTAG, "disconnect()...");
        removeAccount();
        terminatePersistentConnection();
    }

    public void releaseWakeLock() {
        if (this.wakeLock != null && this.wakeLock.isHeld()) {
            this.wakeLock.release();
            this.wakeLock = null;
        }
    }

    public void acquireWakeLock() {
        if (this.wakeLock == null) {
            this.wakeLock = ((PowerManager) getContext().getSystemService("power")).newWakeLock(1, getClass().getCanonicalName());
        }
        this.wakeLock.acquire();
    }

    public void terminatePersistentConnection() {
        Log.d(LOGTAG, "terminatePersistentConnection()...");
        addTask(new Runnable() {
            final XmppManager xmppManager;

            {
                this.xmppManager = XmppManager.this;
            }

            public void run() {
                if (this.xmppManager.isConnected()) {
                    Log.d(XmppManager.LOGTAG, "terminatePersistentConnection()... run()");
                    this.xmppManager.getConnection().removePacketListener(this.xmppManager.getNotificationPacketListener());
                    this.xmppManager.getConnection().disconnect();
                }
                this.xmppManager.runTask();
            }
        });
    }

    public XMPPConnection getConnection() {
        return this.connection;
    }

    public void setConnection(XMPPConnection connection2) {
        this.connection = connection2;
    }

    public ConnectionListener getConnectionListener() {
        return this.connectionListener;
    }

    public PacketListener getNotificationPacketListener() {
        return this.notificationPacketListener;
    }

    public void reregisterAccount(PullAccount pullAccount2) {
        Log.d(LOGTAG, "reregisterAccount()..." + pullAccount2);
        removeAccount();
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.XMPP_F_USERNAME, pullAccount2.getUsername());
        editor.putString(Constants.XMPP_F_PASSWORD, pullAccount2.getPassword());
        editor.putInt(Constants.XMPP_F_ACCOUNTTYPE, pullAccount2.getAccountType());
        editor.putString(Constants.XMPP_F_APP, pullAccount2.getApp());
        editor.commit();
        disconnect();
        connect();
    }

    private boolean hasPullAccount() {
        return this.sharedPrefs.contains(Constants.XMPP_F_USERNAME);
    }

    private PullAccount getPullAccount() {
        Log.d(LOGTAG, "hasPullAccount()()..." + hasPullAccount());
        if (hasPullAccount()) {
            return new PullAccount(this.sharedPrefs.getString(Constants.XMPP_F_USERNAME, StringUtils.EMPTY), this.sharedPrefs.getString(Constants.XMPP_F_PASSWORD, StringUtils.EMPTY), this.sharedPrefs.getInt(Constants.XMPP_F_ACCOUNTTYPE, 1), this.sharedPrefs.getString(Constants.XMPP_F_APP, StringUtils.EMPTY));
        }
        return null;
    }

    public List<Runnable> getTaskList() {
        return this.taskList;
    }

    public Future<?> getFutureTask() {
        return this.futureTask;
    }

    public void runTask() {
        synchronized (this.taskList) {
            this.running = false;
            this.futureTask = null;
            if (!this.taskList.isEmpty()) {
                this.taskList.remove(0);
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(this.taskList.get(0));
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                    try {
                        this.futureTask.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        this.taskTracker.decrease();
    }

    /* access modifiers changed from: private */
    public boolean isConnected() {
        return this.connection != null && this.connection.isConnected();
    }

    /* access modifiers changed from: private */
    public boolean isAuthenticated() {
        return this.connection != null && this.connection.isConnected() && this.connection.isAuthenticated();
    }

    /* access modifiers changed from: private */
    public boolean isRegistered() {
        PullAccount pullAcount = getPullAccount();
        if (pullAcount == null || !this.sharedPrefs.contains(Constants.XMPP_USERNAME) || !this.sharedPrefs.contains(Constants.XMPP_PASSWORD)) {
            return false;
        }
        String string = this.sharedPrefs.getString(Constants.XMPP_USERNAME, StringUtils.EMPTY);
        String string2 = this.sharedPrefs.getString(Constants.XMPP_PASSWORD, StringUtils.EMPTY);
        if (!pullAcount.getSerNo().equals(this.sharedPrefs.getString(Constants.XMPP_USERNAME, StringUtils.EMPTY)) || !pullAcount.getPassword().equals(this.sharedPrefs.getString(Constants.XMPP_PASSWORD, StringUtils.EMPTY))) {
            return false;
        }
        return true;
    }

    private void submitConnectTask() {
        Log.d(LOGTAG, "submitConnectTask()...");
        addTask(new ConnectTask(this, null));
    }

    private void submitRegisterTask() {
        Log.d(LOGTAG, "submitRegisterTask()...:");
        submitConnectTask();
        addTask(new RegisterTask(this, null));
    }

    /* access modifiers changed from: private */
    public void submitLoginTask() {
        Log.d(LOGTAG, "submitLoginTask()...:");
        this.pullAccount = getPullAccount();
        Log.d(LOGTAG, "pullAccount()...:" + this.pullAccount);
        if (this.pullAccount != null) {
            submitRegisterTask();
            addTask(new LoginTask(this, null));
        }
    }

    private void addTask(Runnable runnable) {
        this.taskTracker.increase();
        synchronized (this.taskList) {
            if (!this.taskList.isEmpty() || this.running) {
                runTask();
                this.taskList.add(runnable);
            } else {
                this.running = true;
                this.futureTask = this.taskSubmitter.submit(runnable);
                if (this.futureTask == null) {
                    this.taskTracker.decrease();
                }
            }
        }
    }

    private void removeAccount() {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.remove(Constants.XMPP_USERNAME);
        editor.remove(Constants.XMPP_PASSWORD);
        editor.commit();
    }

    public void ConnectTask() {
        Log.i(LOGTAG, "ConnectTask.run()...");
        if (!isConnected()) {
            ConnectionConfiguration connConfig = new ConnectionConfiguration(this.xmppHost, this.xmppPort);
            connConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
            connConfig.setSASLAuthenticationEnabled(false);
            connConfig.setCompressionEnabled(false);
            connConfig.setReconnectionAllowed(true);
            connConfig.setSendPresence(true);
            XMPPConnection connection2 = new XMPPConnection(connConfig);
            setConnection(connection2);
            try {
                connection2.connect();
                Log.i(LOGTAG, "XMPP connected successfully");
                ProviderManager.getInstance().addIQProvider("notification", "androidpn:iq:notification", new NotificationIQProvider());
            } catch (Exception e) {
                Log.e(LOGTAG, "XMPP connection failed", e);
            }
        } else {
            Log.i(LOGTAG, "XMPP connected already");
        }
    }

    public void RegisterTask() {
        if (!isRegistered()) {
            try {
                Registration registration = new Registration();
                PacketFilter packetFilter = new AndFilter(new PacketIDFilter(registration.getPacketID()), new PacketTypeFilter(IQ.class));
                this.connection.addPacketListener(new PacketListener() {
                    public void processPacket(Packet packet) {
                        Log.d("RegisterTask.PacketListener", "processPacket().....");
                        Log.d("RegisterTask.PacketListener", "packet=" + packet.toXML());
                        if (packet instanceof IQ) {
                            IQ response = (IQ) packet;
                            if (response.getType() == IQ.Type.ERROR) {
                                if (!response.getError().toString().contains("409")) {
                                    Log.e(XmppManager.LOGTAG, "Unknown error while registering XMPP account! " + response.getError().getCondition());
                                }
                            } else if (response.getType() == IQ.Type.RESULT) {
                                SharedPreferences.Editor editor = XmppManager.this.sharedPrefs.edit();
                                editor.putString(Constants.XMPP_USERNAME, XmppManager.this.pullAccount.getSerNo());
                                editor.putString(Constants.XMPP_PASSWORD, XmppManager.this.pullAccount.getPassword());
                                editor.commit();
                                Log.i(XmppManager.LOGTAG, "Account registered successfully");
                            }
                        }
                    }
                }, packetFilter);
                registration.setType(IQ.Type.SET);
                registration.addAttribute("username", this.pullAccount.getSerNo());
                registration.addAttribute("password", this.pullAccount.getPassword());
                this.connection.sendPacket(registration);
            } catch (Exception e) {
                Log.e(LOGTAG, "RegisterTask.run()... other error");
            }
        } else {
            Log.i(LOGTAG, "Account registered already");
        }
    }

    public void LoginTask() {
        Log.i(LOGTAG, "LoginTask.run()...");
        if (!isAuthenticated()) {
            try {
                getConnection().login(this.pullAccount.getSerNo(), this.pullAccount.getPassword(), XMPP_RESOURCE_NAME);
                Log.i(LOGTAG, "Loggedn in successfully");
                if (getConnectionListener() != null) {
                    getConnection().addConnectionListener(getConnectionListener());
                }
                PacketFilter packetFilter = new PacketTypeFilter(NotificationIQ.class);
                this.connection.addPacketListener(getNotificationPacketListener(), packetFilter);
            } catch (XMPPException e) {
                Log.e(LOGTAG, "LoginTask.run()... xmpp error");
                Log.e(LOGTAG, "Failed to login to xmpp server. Caused by: " + e.getMessage());
                String errorMessage = e.getMessage();
                if (errorMessage == null || errorMessage.contains("401")) {
                }
            } catch (Exception e2) {
                Log.e(LOGTAG, "LoginTask.run()... other error");
                Log.e(LOGTAG, "Failed to login to xmpp server. Caused by: " + e2.getMessage());
            }
        } else {
            Log.i(LOGTAG, "Logged in already");
        }
    }

    public void sendHeartBeatPkg() {
        if (isAuthenticated()) {
            getConnection().sendPacket(new Presence(Presence.Type.available));
        }
    }

    @Deprecated
    private class ConnectTask implements Runnable {
        final XmppManager xmppManager;

        private ConnectTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ ConnectTask(XmppManager xmppManager2, ConnectTask connectTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "ConnectTask.run()...");
            if (!this.xmppManager.isConnected()) {
                ConnectionConfiguration connConfig = new ConnectionConfiguration(XmppManager.this.xmppHost, XmppManager.this.xmppPort);
                connConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                connConfig.setSASLAuthenticationEnabled(false);
                connConfig.setCompressionEnabled(false);
                XMPPConnection connection = new XMPPConnection(connConfig);
                this.xmppManager.setConnection(connection);
                try {
                    connection.connect();
                    Log.i(XmppManager.LOGTAG, "XMPP connected successfully");
                    ProviderManager.getInstance().addIQProvider("notification", "androidpn:iq:notification", new NotificationIQProvider());
                } catch (XMPPException e) {
                    Log.e(XmppManager.LOGTAG, "XMPP connection failed", e);
                }
                this.xmppManager.runTask();
                return;
            }
            Log.i(XmppManager.LOGTAG, "XMPP connected already");
            this.xmppManager.runTask();
        }
    }

    @Deprecated
    private class RegisterTask implements Runnable {
        final XmppManager xmppManager;

        private RegisterTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ RegisterTask(XmppManager xmppManager2, RegisterTask registerTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "RegisterTask.run()...");
            if (!this.xmppManager.isRegistered()) {
                Registration registration = new Registration();
                PacketFilter packetFilter = new AndFilter(new PacketIDFilter(registration.getPacketID()), new PacketTypeFilter(IQ.class));
                XmppManager.this.connection.addPacketListener(new PacketListener() {
                    public void processPacket(Packet packet) {
                        Log.d("RegisterTask.PacketListener", "processPacket().....");
                        Log.d("RegisterTask.PacketListener", "packet=" + packet.toXML());
                        if (packet instanceof IQ) {
                            IQ response = (IQ) packet;
                            if (response.getType() == IQ.Type.ERROR) {
                                if (!response.getError().toString().contains("409")) {
                                    Log.e(XmppManager.LOGTAG, "Unknown error while registering XMPP account! " + response.getError().getCondition());
                                }
                            } else if (response.getType() == IQ.Type.RESULT) {
                                SharedPreferences.Editor editor = XmppManager.this.sharedPrefs.edit();
                                editor.putString(Constants.XMPP_USERNAME, XmppManager.this.pullAccount.getSerNo());
                                editor.putString(Constants.XMPP_PASSWORD, XmppManager.this.pullAccount.getPassword());
                                editor.commit();
                                Log.i(XmppManager.LOGTAG, "Account registered successfully");
                                RegisterTask.this.xmppManager.runTask();
                            }
                        }
                    }
                }, packetFilter);
                registration.setType(IQ.Type.SET);
                registration.addAttribute("username", XmppManager.this.pullAccount.getSerNo());
                registration.addAttribute("password", XmppManager.this.pullAccount.getPassword());
                XmppManager.this.connection.sendPacket(registration);
                return;
            }
            Log.i(XmppManager.LOGTAG, "Account registered already");
            this.xmppManager.runTask();
        }
    }

    @Deprecated
    private class LoginTask implements Runnable {
        final XmppManager xmppManager;

        private LoginTask() {
            this.xmppManager = XmppManager.this;
        }

        /* synthetic */ LoginTask(XmppManager xmppManager2, LoginTask loginTask) {
            this();
        }

        public void run() {
            Log.i(XmppManager.LOGTAG, "LoginTask.run()...");
            if (!this.xmppManager.isAuthenticated()) {
                try {
                    this.xmppManager.getConnection().login(XmppManager.this.pullAccount.getSerNo(), XmppManager.this.pullAccount.getPassword(), XmppManager.XMPP_RESOURCE_NAME);
                    Log.i(XmppManager.LOGTAG, "Loggedn in successfully");
                    if (this.xmppManager.getConnectionListener() != null) {
                        this.xmppManager.getConnection().addConnectionListener(this.xmppManager.getConnectionListener());
                    }
                    PacketFilter packetFilter = new PacketTypeFilter(NotificationIQ.class);
                    XmppManager.this.connection.addPacketListener(this.xmppManager.getNotificationPacketListener(), packetFilter);
                    this.xmppManager.runTask();
                } catch (XMPPException e) {
                    Log.e(XmppManager.LOGTAG, "LoginTask.run()... xmpp error");
                    Log.e(XmppManager.LOGTAG, "Failed to login to xmpp server. Caused by: " + e.getMessage());
                    String errorMessage = e.getMessage();
                    if (errorMessage != null && errorMessage.contains("401")) {
                        this.xmppManager.submitLoginTask();
                    }
                } catch (Exception e2) {
                    Log.e(XmppManager.LOGTAG, "LoginTask.run()... other error");
                    Log.e(XmppManager.LOGTAG, "Failed to login to xmpp server. Caused by: " + e2.getMessage());
                }
            } else {
                Log.i(XmppManager.LOGTAG, "Logged in already");
                this.xmppManager.runTask();
            }
        }
    }
}
