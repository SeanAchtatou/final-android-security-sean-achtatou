package com.house365.androidpn.client.net;

import android.content.Intent;
import android.util.Log;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.XmppManager;
import com.house365.androidpn.client.dto.NotificationData;
import com.house365.androidpn.client.util.LogUtil;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;

public class NotificationPacketListener implements PacketListener {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationPacketListener.class);
    private final XmppManager xmppManager;

    public NotificationPacketListener(XmppManager xmppManager2) {
        this.xmppManager = xmppManager2;
    }

    public void processPacket(Packet packet) {
        Log.d(LOGTAG, "NotificationPacketListener.processPacket()...");
        Log.d(LOGTAG, "packet.toXML()=" + packet.toXML());
        if (packet instanceof NotificationIQ) {
            NotificationIQ notification = (NotificationIQ) packet;
            if (notification.getChildElementXML().contains("androidpn:iq:notification")) {
                Intent intent = new Intent(Constants.getActionShowNotification(this.xmppManager.getContext()));
                intent.putExtra(Constants.NOTIFICATION_DATA, new NotificationData(notification.getId(), notification.getApiKey(), notification.getTitle(), notification.getMessage(), notification.getUri()));
                this.xmppManager.getContext().sendBroadcast(intent);
            }
        }
    }
}
