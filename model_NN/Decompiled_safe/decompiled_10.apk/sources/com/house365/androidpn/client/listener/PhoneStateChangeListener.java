package com.house365.androidpn.client.listener;

import android.telephony.PhoneStateListener;
import com.house365.androidpn.client.service.NotificationService;
import com.house365.androidpn.client.util.LogUtil;

public class PhoneStateChangeListener extends PhoneStateListener {
    private static final String LOGTAG = LogUtil.makeLogTag(PhoneStateChangeListener.class);
    private final NotificationService notificationService;

    public PhoneStateChangeListener(NotificationService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onDataConnectionStateChanged(int state) {
        super.onDataConnectionStateChanged(state);
    }

    private String getState(int state) {
        switch (state) {
            case 0:
                return "DATA_DISCONNECTED";
            case 1:
                return "DATA_CONNECTING";
            case 2:
                return "DATA_CONNECTED";
            case 3:
                return "DATA_SUSPENDED";
            default:
                return "DATA_<UNKNOWN>";
        }
    }
}
