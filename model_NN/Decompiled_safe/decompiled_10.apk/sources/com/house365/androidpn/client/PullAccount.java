package com.house365.androidpn.client;

import java.io.Serializable;

public class PullAccount implements Serializable {
    public static final int ACCOUNT_TYPE_DEVICE = 1;
    public static final int ACCOUNT_TYPE_OTHER = 3;
    public static final int ACCOUNT_TYPE_PHONENO = 2;
    private static final long serialVersionUID = 7316859143942131623L;
    private int accountType;
    private String app;
    private String password;
    private String username;

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public int getAccountType() {
        return this.accountType;
    }

    public void setAccountType(int accountType2) {
        this.accountType = accountType2;
    }

    public String getApp() {
        return this.app;
    }

    public void setApp(String app2) {
        this.app = app2;
    }

    public PullAccount(String username2, String password2, int accountType2, String app2) {
        this.username = username2;
        this.password = password2;
        this.accountType = accountType2;
        this.app = app2;
    }

    public PullAccount(String username2, int accountType2, String app2) {
        this.username = username2;
        this.accountType = accountType2;
        this.app = app2;
    }

    public PullAccount(String serNo) {
        String[] accInfos = serNo.split("|");
        this.username = accInfos[0];
        this.accountType = Integer.parseInt(accInfos[1]);
        this.app = accInfos[2];
    }

    public String getSerNo() {
        return String.valueOf(this.username) + "|" + this.accountType + "|" + this.app;
    }
}
