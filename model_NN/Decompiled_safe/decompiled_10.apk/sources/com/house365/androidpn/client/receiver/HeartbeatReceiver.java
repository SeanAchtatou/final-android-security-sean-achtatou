package com.house365.androidpn.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import com.house365.androidpn.client.service.NotificationService;
import com.house365.androidpn.client.util.LogUtil;
import com.house365.androidpn.client.util.NetworkUtil;

public class HeartbeatReceiver extends BroadcastReceiver {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationReceiver.class);
    public static final int WAKE_LOCK_CLOSE = 2;
    public static final int WAKE_LOCK_OPEN = 1;
    /* access modifiers changed from: private */
    public NotificationService notificationService;
    private PowerManager.WakeLock wakeLock;
    private Handler wakeLockHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                HeartbeatReceiver.this.acquireWakeLock();
            } else if (msg.what == 2) {
                HeartbeatReceiver.this.releaseWakeLock();
            }
        }
    };

    public HeartbeatReceiver(NotificationService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onReceive(Context context, Intent intent) {
        Log.i(LOGTAG, "HeartbeatReceiver.onReceive...");
        acquireWakeLock();
        if (NetworkUtil.isNetworkAvailable(this.notificationService)) {
            new Thread() {
                public void run() {
                    HeartbeatReceiver.this.notificationService.getXmppManager().connect();
                }
            }.start();
        } else {
            Log.i(LOGTAG, "HeartbeatReceiver. Network  Is UnAvailable...,re heart—beat at next min");
        }
        releaseWakeLock();
    }

    public void releaseWakeLock() {
        if (this.wakeLock != null && this.wakeLock.isHeld()) {
            this.wakeLock.release();
            this.wakeLock = null;
        }
    }

    public void acquireWakeLock() {
        if (this.wakeLock == null) {
            this.wakeLock = ((PowerManager) this.notificationService.getSystemService("power")).newWakeLock(1, getClass().getCanonicalName());
            this.wakeLock.acquire();
        }
    }
}
