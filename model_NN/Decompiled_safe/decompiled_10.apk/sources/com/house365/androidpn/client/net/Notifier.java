package com.house365.androidpn.client.net;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.dto.NotificationData;
import com.house365.androidpn.client.util.LogUtil;
import java.util.Random;
import org.apache.commons.lang.StringUtils;

public class Notifier {
    private static final String LOGTAG = LogUtil.makeLogTag(Notifier.class);
    private static final Random random = new Random(System.currentTimeMillis());
    private Context context;
    private NotificationManager notificationManager;
    private SharedPreferences sharedPrefs;

    public Notifier(Context context2) {
        this.context = context2;
        this.sharedPrefs = context2.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        this.notificationManager = (NotificationManager) context2.getSystemService("notification");
    }

    public void notify(NotificationData notificationData) {
        Log.d(LOGTAG, "notify()...");
        if (isNotificationEnabled()) {
            if (isNotificationToastEnabled()) {
                Toast.makeText(this.context, notificationData.getMessage(), 1).show();
            }
            Notification notification = new Notification();
            notification.icon = getNotificationIcon();
            notification.defaults = 4;
            if (isNotificationSoundEnabled()) {
                notification.defaults |= 1;
            }
            if (isNotificationVibrateEnabled()) {
                notification.defaults |= 2;
            }
            notification.flags |= 16;
            notification.when = System.currentTimeMillis();
            notification.tickerText = notificationData.getMessage();
            Class clazz = null;
            try {
                clazz = Class.forName(this.sharedPrefs.getString(Constants.CALLBACK_ACTIVITY_CLASS_NAME, StringUtils.EMPTY));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(this.context, clazz);
            intent.putExtra(Constants.NOTIFICATION_DATA, notificationData);
            intent.setFlags(268435456);
            intent.setFlags(8388608);
            intent.setFlags(1073741824);
            intent.setFlags(536870912);
            intent.setFlags(67108864);
            notification.setLatestEventInfo(this.context, notificationData.getTitle(), notificationData.getMessage(), PendingIntent.getActivity(this.context, 0, intent, 134217728));
            this.notificationManager.notify(random.nextInt(), notification);
        }
        if (isNotificationBroadcastEnabled()) {
            String callbackBroadcastAction = this.sharedPrefs.getString(Constants.CALLBACK_BROADCAST_ACTION, StringUtils.EMPTY);
            if (callbackBroadcastAction.length() > 0) {
                Intent intent2 = new Intent(callbackBroadcastAction);
                intent2.putExtra(Constants.NOTIFICATION_DATA, notificationData);
                this.context.sendBroadcast(intent2);
            }
        }
    }

    private int getNotificationIcon() {
        return this.sharedPrefs.getInt(Constants.NOTIFICATION_ICON, 0);
    }

    private boolean isNotificationEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_NOTIFICATION_ENABLED, true);
    }

    private boolean isNotificationSoundEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_SOUND_ENABLED, true);
    }

    private boolean isNotificationVibrateEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_VIBRATE_ENABLED, true);
    }

    private boolean isNotificationToastEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_TOAST_ENABLED, false);
    }

    private boolean isNotificationBroadcastEnabled() {
        return this.sharedPrefs.getBoolean(Constants.SETTINGS_BROADCAST_ENABLED, false);
    }
}
