package com.house365.androidpn.client.util;

public class LogUtil {
    public static String makeLogTag(Class cls) {
        return "apn_" + cls.getSimpleName();
    }
}
