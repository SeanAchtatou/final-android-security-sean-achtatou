package com.house365.androidpn.client.net;

import org.jivesoftware.smack.packet.IQ;

public class NotificationIQ extends IQ {
    private String apiKey;
    private String id;
    private String message;
    private String title;
    private String uri;

    public String getChildElementXML() {
        StringBuilder buf = new StringBuilder();
        buf.append("<").append("notification").append(" xmlns=\"").append("androidpn:iq:notification").append("\">");
        if (this.id != null) {
            buf.append("<id>").append(this.id).append("</id>");
        }
        buf.append("</").append("notification").append("> ");
        return buf.toString();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        this.apiKey = apiKey2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String url) {
        this.uri = url;
    }
}
