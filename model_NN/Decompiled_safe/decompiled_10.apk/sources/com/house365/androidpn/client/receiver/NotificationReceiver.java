package com.house365.androidpn.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.dto.NotificationData;
import com.house365.androidpn.client.net.Notifier;
import com.house365.androidpn.client.util.LogUtil;

public final class NotificationReceiver extends BroadcastReceiver {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationReceiver.class);

    public void onReceive(Context context, Intent intent) {
        NotificationData notificationData;
        Log.d(LOGTAG, "NotificationReceiver.onReceive()...");
        String action = intent.getAction();
        Log.d(LOGTAG, "action=" + action + ",self action=" + Constants.getActionShowNotification(context));
        if (Constants.getActionShowNotification(context).equals(action) && (notificationData = (NotificationData) intent.getSerializableExtra(Constants.NOTIFICATION_DATA)) != null) {
            Log.d(LOGTAG, "notificationId=" + notificationData.getId());
            Log.d(LOGTAG, "notificationApiKey=" + notificationData.getApi_key());
            Log.d(LOGTAG, "notificationTitle=" + notificationData.getTitle());
            Log.d(LOGTAG, "notificationMessage=" + notificationData.getMessage());
            Log.d(LOGTAG, "notificationUri=" + notificationData.getUri());
            new Notifier(context).notify(notificationData);
        }
    }
}
