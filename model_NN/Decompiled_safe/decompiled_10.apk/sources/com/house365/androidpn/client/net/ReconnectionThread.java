package com.house365.androidpn.client.net;

import android.util.Log;
import com.baidu.mapapi.MKEvent;
import com.house365.androidpn.client.XmppManager;
import com.house365.androidpn.client.util.LogUtil;

public class ReconnectionThread extends Thread {
    private static final String LOGTAG = LogUtil.makeLogTag(ReconnectionThread.class);
    private int waiting = 0;
    /* access modifiers changed from: private */
    public final XmppManager xmppManager;

    public ReconnectionThread(XmppManager xmppManager2) {
        this.xmppManager = xmppManager2;
    }

    public void run() {
        while (!isInterrupted() && !this.xmppManager.getConnection().isConnected()) {
            try {
                Log.d(LOGTAG, "Trying to reconnect in " + waiting() + " seconds");
                Thread.sleep(2000);
                this.xmppManager.connect();
                this.waiting++;
            } catch (InterruptedException e) {
                this.xmppManager.getHandler().post(new Runnable() {
                    public void run() {
                        ReconnectionThread.this.xmppManager.getConnectionListener().reconnectionFailed(e);
                    }
                });
                return;
            }
        }
    }

    private int waiting() {
        if (this.waiting > 20) {
            return 600;
        }
        if (this.waiting > 13) {
            return MKEvent.ERROR_PERMISSION_DENIED;
        }
        return this.waiting <= 7 ? 10 : 60;
    }
}
