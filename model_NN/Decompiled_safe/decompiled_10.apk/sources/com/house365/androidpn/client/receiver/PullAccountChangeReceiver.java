package com.house365.androidpn.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.PullAccount;
import com.house365.androidpn.client.service.NotificationService;
import com.house365.androidpn.client.util.LogUtil;

public class PullAccountChangeReceiver extends BroadcastReceiver {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationReceiver.class);
    private NotificationService notificationService;

    public PullAccountChangeReceiver(NotificationService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onReceive(Context context, Intent intent) {
        Log.d(LOGTAG, "PullAccountChangeReceiver.onReceive()...");
        PullAccount pullAccount = (PullAccount) intent.getSerializableExtra(Constants.ACTION_PULLACCOUNT_FIELD_ACCOUNT);
        if (pullAccount != null) {
            this.notificationService.resetPullAccount(pullAccount);
        }
    }
}
