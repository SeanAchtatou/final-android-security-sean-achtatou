package com.house365.androidpn.client.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.house365.androidpn.client.Constants;
import com.house365.androidpn.client.PullAccount;
import com.house365.androidpn.client.XmppManager;
import com.house365.androidpn.client.receiver.HeartbeatReceiver;
import com.house365.androidpn.client.receiver.NotificationReceiver;
import com.house365.androidpn.client.receiver.PullAccountChangeReceiver;
import com.house365.androidpn.client.util.LogUtil;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.lang.time.DateUtils;

public class NotificationService extends Service {
    private static final String LOGTAG = LogUtil.makeLogTag(NotificationService.class);
    private BroadcastReceiver connectivityReceiver;
    private String deviceId;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private BroadcastReceiver heartbeatReceiver = new HeartbeatReceiver(this);
    private BroadcastReceiver notificationReceiver = new NotificationReceiver();
    private PhoneStateListener phoneStateListener;
    private BroadcastReceiver pullaccountChangeReceiver = new PullAccountChangeReceiver(this);
    private SharedPreferences sharedPrefs;
    private TaskSubmitter taskSubmitter = new TaskSubmitter(this);
    private TaskTracker taskTracker = new TaskTracker(this);
    private TelephonyManager telephonyManager;
    private XmppManager xmppManager;

    public void onCreate() {
        Log.d(LOGTAG, " NotificationService onCreate()...");
        super.onCreate();
        this.sharedPrefs = getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        this.telephonyManager = (TelephonyManager) getSystemService("phone");
        this.xmppManager = new XmppManager(this);
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.start();
            }
        });
    }

    public void onStart(Intent intent, int startId) {
        Log.d(LOGTAG, "NotificationService onStart()...");
    }

    public void onDestroy() {
        Log.d(LOGTAG, "NotificationService onDestroy()...");
        stop();
    }

    public IBinder onBind(Intent intent) {
        Log.d(LOGTAG, "NotificationService onBind()...");
        return null;
    }

    public void onRebind(Intent intent) {
        Log.d(LOGTAG, "NotificationService onRebind()...");
    }

    public boolean onUnbind(Intent intent) {
        Log.d(LOGTAG, "NotificationService onUnbind()...");
        return true;
    }

    public ExecutorService getExecutorService() {
        return this.executorService;
    }

    public TaskSubmitter getTaskSubmitter() {
        return this.taskSubmitter;
    }

    public TaskTracker getTaskTracker() {
        return this.taskTracker;
    }

    public XmppManager getXmppManager() {
        return this.xmppManager;
    }

    public SharedPreferences getSharedPreferences() {
        return this.sharedPrefs;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void connect() {
        Log.d(LOGTAG, "connect()...");
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.getXmppManager().connect();
            }
        });
    }

    public void disconnect() {
        Log.d(LOGTAG, "disconnect()...");
        this.taskSubmitter.submit(new Runnable() {
            public void run() {
                NotificationService.this.getXmppManager().disconnect();
            }
        });
    }

    private void registerNotificationReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.getActionShowNotification(this));
        registerReceiver(this.notificationReceiver, filter);
    }

    private void unregisterNotificationReceiver() {
        unregisterReceiver(this.notificationReceiver);
    }

    private void registerHeartbeatReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.getActionXMPPHeartbeat(this));
        registerReceiver(this.heartbeatReceiver, filter);
    }

    private void unregisterHeartbeatReceiver() {
        unregisterReceiver(this.heartbeatReceiver);
    }

    private void registerPullaccountChangeReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.getActionPullaccountChange(this));
        registerReceiver(this.pullaccountChangeReceiver, filter);
    }

    private void unregisterPullaccountChangeReceiver() {
        unregisterReceiver(this.pullaccountChangeReceiver);
    }

    private void registerConnectivityReceiver() {
        Log.d(LOGTAG, "registerConnectivityReceiver()...");
        this.telephonyManager.listen(this.phoneStateListener, 64);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(this.connectivityReceiver, filter);
    }

    private void unregisterConnectivityReceiver() {
        Log.d(LOGTAG, "unregisterConnectivityReceiver()...");
        this.telephonyManager.listen(this.phoneStateListener, 0);
        unregisterReceiver(this.connectivityReceiver);
    }

    /* access modifiers changed from: private */
    public void start() {
        Log.d(LOGTAG, "NotificationService init...");
        registerNotificationReceiver();
        registerPullaccountChangeReceiver();
        registerHeartbeatReceiver();
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis(), DateUtils.MILLIS_PER_MINUTE, getHeartbeatIntent());
    }

    private PendingIntent getHeartbeatIntent() {
        return PendingIntent.getBroadcast(this, 0, new Intent(Constants.getActionXMPPHeartbeat(this)), 0);
    }

    private void stop() {
        Log.d(LOGTAG, "stop()...");
        try {
            unregisterNotificationReceiver();
            unregisterPullaccountChangeReceiver();
            unregisterHeartbeatReceiver();
            ((AlarmManager) getSystemService("alarm")).cancel(getHeartbeatIntent());
            this.xmppManager.disconnect();
            this.executorService.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetPullAccount(PullAccount pullAccount) {
        Log.d(LOGTAG, "reregister account");
        this.xmppManager.reregisterAccount(pullAccount);
    }

    public class TaskSubmitter {
        final NotificationService notificationService;

        public TaskSubmitter(NotificationService notificationService2) {
            this.notificationService = notificationService2;
        }

        public Future submit(Runnable task) {
            if (this.notificationService.getExecutorService().isTerminated() || this.notificationService.getExecutorService().isShutdown() || task == null) {
                return null;
            }
            return this.notificationService.getExecutorService().submit(task);
        }
    }

    public class TaskTracker {
        public int count = 0;
        final NotificationService notificationService;

        public TaskTracker(NotificationService notificationService2) {
            this.notificationService = notificationService2;
        }

        public void increase() {
            synchronized (this.notificationService.getTaskTracker()) {
                this.notificationService.getTaskTracker().count++;
            }
        }

        public void decrease() {
            synchronized (this.notificationService.getTaskTracker()) {
                TaskTracker taskTracker = this.notificationService.getTaskTracker();
                taskTracker.count--;
            }
        }
    }
}
