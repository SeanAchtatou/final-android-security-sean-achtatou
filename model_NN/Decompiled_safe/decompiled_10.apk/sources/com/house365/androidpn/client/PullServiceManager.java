package com.house365.androidpn.client;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.house365.androidpn.client.util.LogUtil;

public final class PullServiceManager {
    private static final String LOGTAG = LogUtil.makeLogTag(PullServiceManager.class);
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public PullConfiguration pullConfiguration;
    private SharedPreferences sharedPrefs;

    public PullServiceManager(Context context2, PullConfiguration pullConfiguration2) {
        this.context = context2;
        this.pullConfiguration = pullConfiguration2;
        this.sharedPrefs = context2.getSharedPreferences(Constants.SHARED_PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.ACTION_PREFIX, pullConfiguration2.getActionPrefix());
        editor.putString(Constants.API_KEY, pullConfiguration2.getApiKey());
        editor.putString(Constants.VERSION, pullConfiguration2.getVersion());
        editor.putString(Constants.XMPP_HOST, pullConfiguration2.getHost());
        editor.putInt(Constants.XMPP_PORT, pullConfiguration2.getPort());
        editor.putString(Constants.CALLBACK_ACTIVITY_CLASS_NAME, pullConfiguration2.getNotifyDetailClass());
        editor.putInt(Constants.NOTIFICATION_ICON, pullConfiguration2.getIconId());
        editor.putString(Constants.CALLBACK_BROADCAST_ACTION, pullConfiguration2.getBroadcastAction());
        editor.commit();
    }

    public void startService() {
        new Thread(new Runnable() {
            public void run() {
                PullServiceManager.this.context.startService(new Intent(PullServiceManager.this.pullConfiguration.getServiceAction()));
            }
        }).start();
    }

    public void stopService() {
        this.context.stopService(new Intent(this.pullConfiguration.getServiceAction()));
    }

    public void setAccount(PullAccount pullAccount) {
        Log.d(LOGTAG, "setAccount()..." + pullAccount);
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putString(Constants.XMPP_F_USERNAME, pullAccount.getUsername());
        editor.putString(Constants.XMPP_F_PASSWORD, pullAccount.getPassword());
        editor.putInt(Constants.XMPP_F_ACCOUNTTYPE, pullAccount.getAccountType());
        editor.putString(Constants.XMPP_F_APP, pullAccount.getApp());
        editor.commit();
    }

    public void cleanAccount() {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.remove(Constants.XMPP_F_USERNAME);
        editor.remove(Constants.XMPP_F_PASSWORD);
        editor.remove(Constants.XMPP_F_ACCOUNTTYPE);
        editor.remove(Constants.XMPP_F_APP);
        editor.commit();
    }

    public void setNotificationEnable(boolean flag) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putBoolean(Constants.SETTINGS_NOTIFICATION_ENABLED, flag);
        editor.commit();
    }

    public void setNotificationSoundEnable(boolean flag) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putBoolean(Constants.SETTINGS_SOUND_ENABLED, flag);
        editor.commit();
    }

    public void setNotificationVibrateEnable(boolean flag) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putBoolean(Constants.SETTINGS_VIBRATE_ENABLED, flag);
        editor.commit();
    }

    public void setNotificationToastEnable(boolean flag) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putBoolean(Constants.SETTINGS_TOAST_ENABLED, flag);
        editor.commit();
    }

    public void setNotificationBroadCastenable(boolean flag) {
        SharedPreferences.Editor editor = this.sharedPrefs.edit();
        editor.putBoolean(Constants.SETTINGS_BROADCAST_ENABLED, flag);
        editor.commit();
    }
}
