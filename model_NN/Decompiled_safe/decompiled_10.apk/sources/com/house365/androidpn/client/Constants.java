package com.house365.androidpn.client;

import android.content.Context;
import android.content.SharedPreferences;
import org.apache.commons.lang.StringUtils;

public class Constants {
    public static String ACTION_PREFIX = "ACTION_PREFIX";
    public static final String ACTION_PULLACCOUNT_FIELD_ACCOUNT = "ACTION_PULLACCOUNT_FIELD_ACCOUNT";
    public static final String API_KEY = "API_KEY";
    public static final String CALLBACK_ACTIVITY_CLASS_NAME = "CALLBACK_ACTIVITY_CLASS_NAME";
    public static final String CALLBACK_ACTIVITY_PACKAGE_NAME = "CALLBACK_ACTIVITY_PACKAGE_NAME";
    public static final String CALLBACK_BROADCAST_ACTION = "CALLBACK_BROADCAST_ACTION";
    public static final int HEART_BEAT_INTERVAL = 60;
    public static final String NOTIFICATION_DATA = "NOTIFICATION_DATA";
    public static final String NOTIFICATION_ICON = "NOTIFICATION_ICON";
    public static final String SETTINGS_BROADCAST_ENABLED = "SETTINGS_BROADCAST_ENABLED";
    public static final String SETTINGS_NOTIFICATION_ENABLED = "SETTINGS_NOTIFICATION_ENABLED";
    public static final String SETTINGS_SOUND_ENABLED = "SETTINGS_SOUND_ENABLED";
    public static final String SETTINGS_TOAST_ENABLED = "SETTINGS_TOAST_ENABLED";
    public static final String SETTINGS_VIBRATE_ENABLED = "SETTINGS_VIBRATE_ENABLED";
    public static final String SHARED_PREFERENCE_NAME = "client_preferences";
    public static final String VERSION = "VERSION";
    public static final String XMPP_F_ACCOUNTTYPE = "XMPP_F_ACCOUNTTYPE";
    public static final String XMPP_F_APP = "XMPP_F_APP";
    public static final String XMPP_F_PASSWORD = "XMPP_F_PASSWORD";
    public static final String XMPP_F_USERNAME = "XMPP_F_USERNAME";
    public static final String XMPP_HOST = "XMPP_HOST";
    public static final String XMPP_PASSWORD = "XMPP_PASSWORD";
    public static final String XMPP_PORT = "XMPP_PORT";
    public static final String XMPP_USERNAME = "XMPP_USERNAME";
    private static SharedPreferences sharedPrefs;

    public static String getActionShowNotification(Context context) {
        if (sharedPrefs == null) {
            sharedPrefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        }
        return String.valueOf(sharedPrefs.getString(ACTION_PREFIX, StringUtils.EMPTY)) + ".com.house365.androidpn.client.SHOW_NOTIFICATION";
    }

    public static String getActionPullaccountChange(Context context) {
        if (sharedPrefs == null) {
            sharedPrefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        }
        return String.valueOf(sharedPrefs.getString(ACTION_PREFIX, StringUtils.EMPTY)) + ".com.house365.androidpn.client.PULLACCOUNT_CHANGE";
    }

    public static String getActionXMPPHeartbeat(Context context) {
        if (sharedPrefs == null) {
            sharedPrefs = context.getSharedPreferences(SHARED_PREFERENCE_NAME, 0);
        }
        return String.valueOf(sharedPrefs.getString(ACTION_PREFIX, StringUtils.EMPTY)) + ".com.house365.androidpn.client.XMPP_HEARTBEAT";
    }
}
