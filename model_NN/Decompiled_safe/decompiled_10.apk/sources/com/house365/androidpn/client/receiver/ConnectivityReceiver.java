package com.house365.androidpn.client.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.house365.androidpn.client.service.NotificationService;
import com.house365.androidpn.client.util.LogUtil;

public class ConnectivityReceiver extends BroadcastReceiver {
    private static final String LOGTAG = LogUtil.makeLogTag(ConnectivityReceiver.class);
    private NotificationService notificationService;

    public ConnectivityReceiver(NotificationService notificationService2) {
        this.notificationService = notificationService2;
    }

    public void onReceive(Context context, Intent intent) {
        Log.d(LOGTAG, "ConnectivityReceiver.onReceive()...");
    }
}
