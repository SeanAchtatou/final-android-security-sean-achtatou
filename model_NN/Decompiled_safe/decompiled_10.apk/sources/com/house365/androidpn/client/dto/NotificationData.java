package com.house365.androidpn.client.dto;

import java.io.Serializable;

public class NotificationData implements Serializable {
    private static final long serialVersionUID = 810741337450506672L;
    public String api_key;
    public String id;
    public String message;
    public String title;
    public String uri;

    public NotificationData() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getApi_key() {
        return this.api_key;
    }

    public void setApi_key(String api_key2) {
        this.api_key = api_key2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri2) {
        this.uri = uri2;
    }

    public NotificationData(String id2, String api_key2, String title2, String message2, String uri2) {
        this.id = id2;
        this.api_key = api_key2;
        this.title = title2;
        this.message = message2;
        this.uri = uri2;
    }
}
