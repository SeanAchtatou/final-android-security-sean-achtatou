package com.google.zxing.pdf417.decoder;

import com.google.zxing.FormatException;
import com.google.zxing.common.DecoderResult;
import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.ClassUtils;

final class DecodedBitStreamParser {
    private static final int AL = 28;
    private static final int ALPHA = 0;
    private static final int AS = 27;
    private static final int BEGIN_MACRO_PDF417_CONTROL_BLOCK = 928;
    private static final int BEGIN_MACRO_PDF417_OPTIONAL_FIELD = 923;
    private static final int BYTE_COMPACTION_MODE_LATCH = 901;
    private static final int BYTE_COMPACTION_MODE_LATCH_6 = 924;
    private static final String[] EXP900 = {"000000000000000000000000000000000000000000001", "000000000000000000000000000000000000000000900", "000000000000000000000000000000000000000810000", "000000000000000000000000000000000000729000000", "000000000000000000000000000000000656100000000", "000000000000000000000000000000590490000000000", "000000000000000000000000000531441000000000000", "000000000000000000000000478296900000000000000", "000000000000000000000430467210000000000000000", "000000000000000000387420489000000000000000000", "000000000000000348678440100000000000000000000", "000000000000313810596090000000000000000000000", "000000000282429536481000000000000000000000000", "000000254186582832900000000000000000000000000", "000228767924549610000000000000000000000000000", "205891132094649000000000000000000000000000000"};
    private static final int LL = 27;
    private static final int LOWER = 1;
    private static final int MACRO_PDF417_TERMINATOR = 922;
    private static final int MAX_NUMERIC_CODEWORDS = 15;
    private static final int MIXED = 2;
    private static final char[] MIXED_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '&', CharUtils.CR, 9, ',', ':', '#', '-', ClassUtils.PACKAGE_SEPARATOR_CHAR, '$', '/', '+', '%', '*', '=', '^'};
    private static final int ML = 28;
    private static final int MODE_SHIFT_TO_BYTE_COMPACTION_MODE = 913;
    private static final int NUMERIC_COMPACTION_MODE_LATCH = 902;
    private static final int PAL = 29;
    private static final int PL = 25;
    private static final int PS = 29;
    private static final int PUNCT = 3;
    private static final char[] PUNCT_CHARS = {';', '<', '>', '@', '[', '\\', '}', '_', '`', '~', '!', CharUtils.CR, 9, ',', ':', 10, '-', ClassUtils.PACKAGE_SEPARATOR_CHAR, '$', '/', '\"', '|', '*', '(', ')', '?', '{', '}', '\''};
    private static final int PUNCT_SHIFT = 4;
    private static final int TEXT_COMPACTION_MODE_LATCH = 900;

    private DecodedBitStreamParser() {
    }

    private static StringBuffer add(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer(5);
        StringBuffer stringBuffer2 = new StringBuffer(5);
        StringBuffer stringBuffer3 = new StringBuffer(str.length());
        for (int i = 0; i < str.length(); i++) {
            stringBuffer3.append('0');
        }
        int i2 = 0;
        for (int length = str.length() - 3; length > -1; length -= 3) {
            stringBuffer.setLength(0);
            stringBuffer.append(str.charAt(length));
            stringBuffer.append(str.charAt(length + 1));
            stringBuffer.append(str.charAt(length + 2));
            stringBuffer2.setLength(0);
            stringBuffer2.append(str2.charAt(length));
            stringBuffer2.append(str2.charAt(length + 1));
            stringBuffer2.append(str2.charAt(length + 2));
            int parseInt = Integer.parseInt(stringBuffer.toString());
            int parseInt2 = Integer.parseInt(stringBuffer2.toString());
            int i3 = ((parseInt + parseInt2) + i2) % 1000;
            i2 = (i2 + (parseInt + parseInt2)) / 1000;
            stringBuffer3.setCharAt(length + 2, (char) ((i3 % 10) + 48));
            stringBuffer3.setCharAt(length + 1, (char) (((i3 / 10) % 10) + 48));
            stringBuffer3.setCharAt(length, (char) ((i3 / 100) + 48));
        }
        return stringBuffer3;
    }

    private static int byteCompaction(int i, int[] iArr, int i2, StringBuffer stringBuffer) {
        if (i == BYTE_COMPACTION_MODE_LATCH) {
            int i3 = 0;
            long j = 0;
            char[] cArr = new char[6];
            int[] iArr2 = new int[6];
            boolean z = false;
            while (i2 < iArr[0] && !z) {
                int i4 = i2 + 1;
                int i5 = iArr[i2];
                if (i5 < TEXT_COMPACTION_MODE_LATCH) {
                    iArr2[i3] = i5;
                    i3++;
                    j = (j * 900) + ((long) i5);
                    i2 = i4;
                } else if (i5 == TEXT_COMPACTION_MODE_LATCH || i5 == BYTE_COMPACTION_MODE_LATCH || i5 == NUMERIC_COMPACTION_MODE_LATCH || i5 == BYTE_COMPACTION_MODE_LATCH_6 || i5 == BEGIN_MACRO_PDF417_CONTROL_BLOCK || i5 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i5 == MACRO_PDF417_TERMINATOR) {
                    i2 = i4 - 1;
                    z = true;
                } else {
                    i2 = i4;
                }
                if (i3 % 5 == 0 && i3 > 0) {
                    int i6 = 0;
                    while (i6 < 6) {
                        cArr[5 - i6] = (char) ((int) (j % 256));
                        i6++;
                        j >>= 8;
                    }
                    stringBuffer.append(cArr);
                    i3 = 0;
                }
            }
            for (int i7 = (i3 / 5) * 5; i7 < i3; i7++) {
                stringBuffer.append((char) iArr2[i7]);
            }
        } else if (i == BYTE_COMPACTION_MODE_LATCH_6) {
            int i8 = 0;
            long j2 = 0;
            boolean z2 = false;
            while (i2 < iArr[0] && !z2) {
                int i9 = i2 + 1;
                int i10 = iArr[i2];
                if (i10 < TEXT_COMPACTION_MODE_LATCH) {
                    i8++;
                    j2 = (j2 * 900) + ((long) i10);
                    r12 = i9;
                } else if (i10 == TEXT_COMPACTION_MODE_LATCH || i10 == BYTE_COMPACTION_MODE_LATCH || i10 == NUMERIC_COMPACTION_MODE_LATCH || i10 == BYTE_COMPACTION_MODE_LATCH_6 || i10 == BEGIN_MACRO_PDF417_CONTROL_BLOCK || i10 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i10 == MACRO_PDF417_TERMINATOR) {
                    r12 = i9 - 1;
                    z2 = true;
                } else {
                    r12 = i9;
                }
                if (i8 % 5 == 0 && i8 > 0) {
                    char[] cArr2 = new char[6];
                    int i11 = 0;
                    while (i11 < 6) {
                        cArr2[5 - i11] = (char) ((int) (255 & j2));
                        i11++;
                        j2 >>= 8;
                    }
                    stringBuffer.append(cArr2);
                }
            }
        }
        return i2;
    }

    static DecoderResult decode(int[] iArr) throws FormatException {
        int byteCompaction;
        StringBuffer stringBuffer = new StringBuffer(100);
        int i = 2;
        int i2 = iArr[1];
        while (i < iArr[0]) {
            switch (i2) {
                case TEXT_COMPACTION_MODE_LATCH /*900*/:
                    byteCompaction = textCompaction(iArr, i, stringBuffer);
                    break;
                case BYTE_COMPACTION_MODE_LATCH /*901*/:
                    byteCompaction = byteCompaction(i2, iArr, i, stringBuffer);
                    break;
                case NUMERIC_COMPACTION_MODE_LATCH /*902*/:
                    byteCompaction = numericCompaction(iArr, i, stringBuffer);
                    break;
                case MODE_SHIFT_TO_BYTE_COMPACTION_MODE /*913*/:
                    byteCompaction = byteCompaction(i2, iArr, i, stringBuffer);
                    break;
                case BYTE_COMPACTION_MODE_LATCH_6 /*924*/:
                    byteCompaction = byteCompaction(i2, iArr, i, stringBuffer);
                    break;
                default:
                    byteCompaction = textCompaction(iArr, i - 1, stringBuffer);
                    break;
            }
            if (byteCompaction < iArr.length) {
                i = byteCompaction + 1;
                i2 = iArr[byteCompaction];
            } else {
                throw FormatException.getFormatInstance();
            }
        }
        return new DecoderResult(null, stringBuffer.toString(), null, null);
    }

    private static String decodeBase900toBase10(int[] iArr, int i) {
        String str;
        int i2 = 0;
        StringBuffer stringBuffer = null;
        while (i2 < i) {
            StringBuffer multiply = multiply(EXP900[(i - i2) - 1], iArr[i2]);
            if (stringBuffer != null) {
                multiply = add(stringBuffer.toString(), multiply.toString());
            }
            i2++;
            stringBuffer = multiply;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= stringBuffer.length()) {
                str = null;
                break;
            } else if (stringBuffer.charAt(i3) == '1') {
                str = stringBuffer.toString().substring(i3 + 1);
                break;
            } else {
                i3++;
            }
        }
        return str == null ? stringBuffer.toString() : str;
    }

    /* JADX INFO: additional move instructions added (13) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARN: Type inference failed for: r3v14 */
    /* JADX WARN: Type inference failed for: r3v15 */
    /* JADX WARN: Type inference failed for: r3v16 */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARN: Type inference failed for: r3v18 */
    /* JADX WARN: Type inference failed for: r3v19 */
    /* JADX WARN: Type inference failed for: r3v20 */
    /* JADX WARN: Type inference failed for: r3v21 */
    /* JADX WARN: Type inference failed for: r3v22 */
    /* JADX WARN: Type inference failed for: r3v23 */
    /* JADX WARN: Type inference failed for: r3v24 */
    /* JADX WARN: Type inference failed for: r3v25 */
    /* JADX WARN: Type inference failed for: r3v26 */
    /* JADX WARN: Type inference failed for: r3v27 */
    /* JADX WARN: Type inference failed for: r3v28 */
    /* JADX WARN: Type inference failed for: r3v29 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void decodeTextCompaction(int[] r11, int[] r12, int r13, java.lang.StringBuffer r14) {
        /*
            r9 = 913(0x391, float:1.28E-42)
            r8 = 28
            r7 = 26
            r6 = 29
            r1 = 0
            r4 = r1
            r2 = r1
            r3 = r1
        L_0x000c:
            if (r4 >= r13) goto L_0x00c8
            r0 = r11[r4]
            switch(r3) {
                case 0: goto L_0x001d;
                case 1: goto L_0x0046;
                case 2: goto L_0x006d;
                case 3: goto L_0x00a0;
                case 4: goto L_0x00b9;
                default: goto L_0x0013;
            }
        L_0x0013:
            r0 = r1
        L_0x0014:
            if (r0 == 0) goto L_0x0019
            r14.append(r0)
        L_0x0019:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x000c
        L_0x001d:
            if (r0 >= r7) goto L_0x0023
            int r0 = r0 + 65
            char r0 = (char) r0
            goto L_0x0014
        L_0x0023:
            if (r0 != r7) goto L_0x0028
            r0 = 32
            goto L_0x0014
        L_0x0028:
            r5 = 27
            if (r0 != r5) goto L_0x002f
            r3 = 1
            r0 = r1
            goto L_0x0014
        L_0x002f:
            if (r0 != r8) goto L_0x0034
            r3 = 2
            r0 = r1
            goto L_0x0014
        L_0x0034:
            if (r0 != r6) goto L_0x003c
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x003c:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x0046:
            if (r0 >= r7) goto L_0x004c
            int r0 = r0 + 97
            char r0 = (char) r0
            goto L_0x0014
        L_0x004c:
            if (r0 != r7) goto L_0x0051
            r0 = 32
            goto L_0x0014
        L_0x0051:
            if (r0 != r8) goto L_0x0056
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x0056:
            if (r0 != r8) goto L_0x005b
            r3 = 2
            r0 = r1
            goto L_0x0014
        L_0x005b:
            if (r0 != r6) goto L_0x0063
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x0063:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x006d:
            r5 = 25
            if (r0 >= r5) goto L_0x0076
            char[] r5 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.MIXED_CHARS
            char r0 = r5[r0]
            goto L_0x0014
        L_0x0076:
            r5 = 25
            if (r0 != r5) goto L_0x007d
            r3 = 3
            r0 = r1
            goto L_0x0014
        L_0x007d:
            if (r0 != r7) goto L_0x0082
            r0 = 32
            goto L_0x0014
        L_0x0082:
            r5 = 27
            if (r0 != r5) goto L_0x0088
            r0 = r1
            goto L_0x0014
        L_0x0088:
            if (r0 != r8) goto L_0x008d
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x008d:
            if (r0 != r6) goto L_0x0095
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x0095:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x00a0:
            if (r0 >= r6) goto L_0x00a8
            char[] r5 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.PUNCT_CHARS
            char r0 = r5[r0]
            goto L_0x0014
        L_0x00a8:
            if (r0 != r6) goto L_0x00ae
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x00ae:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x00b9:
            if (r0 >= r6) goto L_0x00c2
            char[] r3 = com.google.zxing.pdf417.decoder.DecodedBitStreamParser.PUNCT_CHARS
            char r0 = r3[r0]
            r3 = r2
            goto L_0x0014
        L_0x00c2:
            if (r0 != r6) goto L_0x00c9
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x00c8:
            return
        L_0x00c9:
            r0 = r1
            r3 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.pdf417.decoder.DecodedBitStreamParser.decodeTextCompaction(int[], int[], int, java.lang.StringBuffer):void");
    }

    private static StringBuffer multiply(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            stringBuffer.append('0');
        }
        int i3 = i / 100;
        int i4 = (i / 10) % 10;
        int i5 = i % 10;
        StringBuffer stringBuffer2 = stringBuffer;
        int i6 = 0;
        while (i6 < i5) {
            i6++;
            stringBuffer2 = add(stringBuffer2.toString(), str);
        }
        int i7 = 0;
        while (i7 < i4) {
            i7++;
            stringBuffer2 = add(stringBuffer2.toString(), new StringBuffer().append(str).append('0').toString().substring(1));
        }
        for (int i8 = 0; i8 < i3; i8++) {
            stringBuffer2 = add(stringBuffer2.toString(), new StringBuffer().append(str).append("00").toString().substring(2));
        }
        return stringBuffer2;
    }

    private static int numericCompaction(int[] iArr, int i, StringBuffer stringBuffer) {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < TEXT_COMPACTION_MODE_LATCH) {
                iArr2[i2] = i4;
                i2++;
                i = i3;
            } else if (i4 == TEXT_COMPACTION_MODE_LATCH || i4 == BYTE_COMPACTION_MODE_LATCH || i4 == BYTE_COMPACTION_MODE_LATCH_6 || i4 == BEGIN_MACRO_PDF417_CONTROL_BLOCK || i4 == BEGIN_MACRO_PDF417_OPTIONAL_FIELD || i4 == MACRO_PDF417_TERMINATOR) {
                i = i3 - 1;
                z = true;
            } else {
                i = i3;
            }
            if (i2 % 15 == 0 || i4 == NUMERIC_COMPACTION_MODE_LATCH || z) {
                stringBuffer.append(decodeBase900toBase10(iArr2, i2));
                i2 = 0;
            }
        }
        return i;
    }

    private static int textCompaction(int[] iArr, int i, StringBuffer stringBuffer) {
        int[] iArr2 = new int[(iArr[0] << 1)];
        int[] iArr3 = new int[(iArr[0] << 1)];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i4 < TEXT_COMPACTION_MODE_LATCH) {
                iArr2[i2] = i4 / 30;
                iArr2[i2 + 1] = i4 % 30;
                i2 += 2;
                i = i3;
            } else {
                switch (i4) {
                    case TEXT_COMPACTION_MODE_LATCH /*900*/:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case BYTE_COMPACTION_MODE_LATCH /*901*/:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case NUMERIC_COMPACTION_MODE_LATCH /*902*/:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case MODE_SHIFT_TO_BYTE_COMPACTION_MODE /*913*/:
                        iArr2[i2] = MODE_SHIFT_TO_BYTE_COMPACTION_MODE;
                        iArr3[i2] = i4;
                        i2++;
                        i = i3;
                        continue;
                    case BYTE_COMPACTION_MODE_LATCH_6 /*924*/:
                        i = i3 - 1;
                        z = true;
                        continue;
                    default:
                        i = i3;
                        continue;
                }
            }
        }
        decodeTextCompaction(iArr2, iArr3, i2, stringBuffer);
        return i;
    }
}
