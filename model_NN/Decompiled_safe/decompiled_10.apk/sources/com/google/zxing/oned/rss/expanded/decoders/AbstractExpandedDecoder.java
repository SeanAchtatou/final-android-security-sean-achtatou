package com.google.zxing.oned.rss.expanded.decoders;

import com.baidu.location.BDLocation;
import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;
import com.house365.androidpn.client.Constants;

public abstract class AbstractExpandedDecoder {
    protected final GeneralAppIdDecoder generalDecoder;
    protected final BitArray information;

    AbstractExpandedDecoder(BitArray bitArray) {
        this.information = bitArray;
        this.generalDecoder = new GeneralAppIdDecoder(bitArray);
    }

    public static AbstractExpandedDecoder createDecoder(BitArray bitArray) {
        if (bitArray.get(1)) {
            return new AI01AndOtherAIs(bitArray);
        }
        if (!bitArray.get(2)) {
            return new AnyAIDecoder(bitArray);
        }
        switch (GeneralAppIdDecoder.extractNumericValueFromBitArray(bitArray, 1, 4)) {
            case 4:
                return new AI013103decoder(bitArray);
            case 5:
                return new AI01320xDecoder(bitArray);
            default:
                switch (GeneralAppIdDecoder.extractNumericValueFromBitArray(bitArray, 1, 5)) {
                    case 12:
                        return new AI01392xDecoder(bitArray);
                    case 13:
                        return new AI01393xDecoder(bitArray);
                    default:
                        switch (GeneralAppIdDecoder.extractNumericValueFromBitArray(bitArray, 1, 7)) {
                            case 56:
                                return new AI013x0x1xDecoder(bitArray, "310", "11");
                            case 57:
                                return new AI013x0x1xDecoder(bitArray, "320", "11");
                            case 58:
                                return new AI013x0x1xDecoder(bitArray, "310", "13");
                            case 59:
                                return new AI013x0x1xDecoder(bitArray, "320", "13");
                            case Constants.HEART_BEAT_INTERVAL /*60*/:
                                return new AI013x0x1xDecoder(bitArray, "310", "15");
                            case BDLocation.TypeGpsLocation:
                                return new AI013x0x1xDecoder(bitArray, "320", "15");
                            case BDLocation.TypeCriteriaException:
                                return new AI013x0x1xDecoder(bitArray, "310", "17");
                            case BDLocation.TypeNetWorkException:
                                return new AI013x0x1xDecoder(bitArray, "320", "17");
                            default:
                                throw new IllegalStateException(new StringBuffer().append("unknown decoder: ").append(bitArray).toString());
                        }
                }
        }
    }

    public abstract String parseInformation() throws NotFoundException;
}
