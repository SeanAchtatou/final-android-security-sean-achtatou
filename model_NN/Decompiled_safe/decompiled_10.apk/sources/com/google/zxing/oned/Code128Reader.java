package com.google.zxing.oned;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.BitArray;

public final class Code128Reader extends OneDReader {
    private static final int CODE_CODE_A = 101;
    private static final int CODE_CODE_B = 100;
    private static final int CODE_CODE_C = 99;
    private static final int CODE_FNC_1 = 102;
    private static final int CODE_FNC_2 = 97;
    private static final int CODE_FNC_3 = 96;
    private static final int CODE_FNC_4_A = 101;
    private static final int CODE_FNC_4_B = 100;
    static final int[][] CODE_PATTERNS;
    private static final int CODE_SHIFT = 98;
    private static final int CODE_START_A = 103;
    private static final int CODE_START_B = 104;
    private static final int CODE_START_C = 105;
    private static final int CODE_STOP = 106;
    private static final int MAX_AVG_VARIANCE = 64;
    private static final int MAX_INDIVIDUAL_VARIANCE = 179;

    static {
        int[][] iArr = new int[107][];
        iArr[0] = new int[]{2, 1, 2, 2, 2, 2};
        iArr[1] = new int[]{2, 2, 2, 1, 2, 2};
        iArr[2] = new int[]{2, 2, 2, 2, 2, 1};
        iArr[3] = new int[]{1, 2, 1, 2, 2, 3};
        iArr[4] = new int[]{1, 2, 1, 3, 2, 2};
        iArr[5] = new int[]{1, 3, 1, 2, 2, 2};
        iArr[6] = new int[]{1, 2, 2, 2, 1, 3};
        iArr[7] = new int[]{1, 2, 2, 3, 1, 2};
        iArr[8] = new int[]{1, 3, 2, 2, 1, 2};
        iArr[9] = new int[]{2, 2, 1, 2, 1, 3};
        iArr[10] = new int[]{2, 2, 1, 3, 1, 2};
        iArr[11] = new int[]{2, 3, 1, 2, 1, 2};
        iArr[12] = new int[]{1, 1, 2, 2, 3, 2};
        iArr[13] = new int[]{1, 2, 2, 1, 3, 2};
        iArr[14] = new int[]{1, 2, 2, 2, 3, 1};
        iArr[15] = new int[]{1, 1, 3, 2, 2, 2};
        iArr[16] = new int[]{1, 2, 3, 1, 2, 2};
        iArr[17] = new int[]{1, 2, 3, 2, 2, 1};
        iArr[18] = new int[]{2, 2, 3, 2, 1, 1};
        iArr[19] = new int[]{2, 2, 1, 1, 3, 2};
        iArr[20] = new int[]{2, 2, 1, 2, 3, 1};
        iArr[21] = new int[]{2, 1, 3, 2, 1, 2};
        iArr[22] = new int[]{2, 2, 3, 1, 1, 2};
        iArr[23] = new int[]{3, 1, 2, 1, 3, 1};
        iArr[24] = new int[]{3, 1, 1, 2, 2, 2};
        iArr[25] = new int[]{3, 2, 1, 1, 2, 2};
        iArr[26] = new int[]{3, 2, 1, 2, 2, 1};
        iArr[27] = new int[]{3, 1, 2, 2, 1, 2};
        iArr[28] = new int[]{3, 2, 2, 1, 1, 2};
        iArr[29] = new int[]{3, 2, 2, 2, 1, 1};
        iArr[30] = new int[]{2, 1, 2, 1, 2, 3};
        iArr[31] = new int[]{2, 1, 2, 3, 2, 1};
        iArr[32] = new int[]{2, 3, 2, 1, 2, 1};
        iArr[33] = new int[]{1, 1, 1, 3, 2, 3};
        iArr[34] = new int[]{1, 3, 1, 1, 2, 3};
        iArr[35] = new int[]{1, 3, 1, 3, 2, 1};
        iArr[36] = new int[]{1, 1, 2, 3, 1, 3};
        iArr[37] = new int[]{1, 3, 2, 1, 1, 3};
        iArr[38] = new int[]{1, 3, 2, 3, 1, 1};
        iArr[39] = new int[]{2, 1, 1, 3, 1, 3};
        iArr[40] = new int[]{2, 3, 1, 1, 1, 3};
        iArr[41] = new int[]{2, 3, 1, 3, 1, 1};
        iArr[42] = new int[]{1, 1, 2, 1, 3, 3};
        iArr[43] = new int[]{1, 1, 2, 3, 3, 1};
        iArr[44] = new int[]{1, 3, 2, 1, 3, 1};
        iArr[45] = new int[]{1, 1, 3, 1, 2, 3};
        iArr[46] = new int[]{1, 1, 3, 3, 2, 1};
        iArr[47] = new int[]{1, 3, 3, 1, 2, 1};
        iArr[48] = new int[]{3, 1, 3, 1, 2, 1};
        iArr[49] = new int[]{2, 1, 1, 3, 3, 1};
        iArr[50] = new int[]{2, 3, 1, 1, 3, 1};
        iArr[51] = new int[]{2, 1, 3, 1, 1, 3};
        iArr[52] = new int[]{2, 1, 3, 3, 1, 1};
        iArr[53] = new int[]{2, 1, 3, 1, 3, 1};
        iArr[54] = new int[]{3, 1, 1, 1, 2, 3};
        iArr[55] = new int[]{3, 1, 1, 3, 2, 1};
        iArr[56] = new int[]{3, 3, 1, 1, 2, 1};
        iArr[57] = new int[]{3, 1, 2, 1, 1, 3};
        iArr[58] = new int[]{3, 1, 2, 3, 1, 1};
        iArr[59] = new int[]{3, 3, 2, 1, 1, 1};
        iArr[60] = new int[]{3, 1, 4, 1, 1, 1};
        iArr[61] = new int[]{2, 2, 1, 4, 1, 1};
        iArr[62] = new int[]{4, 3, 1, 1, 1, 1};
        iArr[63] = new int[]{1, 1, 1, 2, 2, 4};
        iArr[MAX_AVG_VARIANCE] = new int[]{1, 1, 1, 4, 2, 2};
        iArr[65] = new int[]{1, 2, 1, 1, 2, 4};
        iArr[66] = new int[]{1, 2, 1, 4, 2, 1};
        iArr[67] = new int[]{1, 4, 1, 1, 2, 2};
        iArr[68] = new int[]{1, 4, 1, 2, 2, 1};
        iArr[69] = new int[]{1, 1, 2, 2, 1, 4};
        iArr[70] = new int[]{1, 1, 2, 4, 1, 2};
        iArr[71] = new int[]{1, 2, 2, 1, 1, 4};
        iArr[72] = new int[]{1, 2, 2, 4, 1, 1};
        iArr[73] = new int[]{1, 4, 2, 1, 1, 2};
        iArr[74] = new int[]{1, 4, 2, 2, 1, 1};
        iArr[75] = new int[]{2, 4, 1, 2, 1, 1};
        iArr[76] = new int[]{2, 2, 1, 1, 1, 4};
        iArr[77] = new int[]{4, 1, 3, 1, 1, 1};
        iArr[78] = new int[]{2, 4, 1, 1, 1, 2};
        iArr[79] = new int[]{1, 3, 4, 1, 1, 1};
        iArr[80] = new int[]{1, 1, 1, 2, 4, 2};
        iArr[81] = new int[]{1, 2, 1, 1, 4, 2};
        iArr[82] = new int[]{1, 2, 1, 2, 4, 1};
        iArr[83] = new int[]{1, 1, 4, 2, 1, 2};
        iArr[84] = new int[]{1, 2, 4, 1, 1, 2};
        iArr[85] = new int[]{1, 2, 4, 2, 1, 1};
        iArr[86] = new int[]{4, 1, 1, 2, 1, 2};
        iArr[87] = new int[]{4, 2, 1, 1, 1, 2};
        iArr[88] = new int[]{4, 2, 1, 2, 1, 1};
        iArr[89] = new int[]{2, 1, 2, 1, 4, 1};
        iArr[90] = new int[]{2, 1, 4, 1, 2, 1};
        iArr[91] = new int[]{4, 1, 2, 1, 2, 1};
        iArr[92] = new int[]{1, 1, 1, 1, 4, 3};
        iArr[93] = new int[]{1, 1, 1, 3, 4, 1};
        iArr[94] = new int[]{1, 3, 1, 1, 4, 1};
        iArr[95] = new int[]{1, 1, 4, 1, 1, 3};
        iArr[CODE_FNC_3] = new int[]{1, 1, 4, 3, 1, 1};
        iArr[CODE_FNC_2] = new int[]{4, 1, 1, 1, 1, 3};
        iArr[CODE_SHIFT] = new int[]{4, 1, 1, 3, 1, 1};
        iArr[CODE_CODE_C] = new int[]{1, 1, 3, 1, 4, 1};
        iArr[100] = new int[]{1, 1, 4, 1, 3, 1};
        iArr[101] = new int[]{3, 1, 1, 1, 4, 1};
        iArr[CODE_FNC_1] = new int[]{4, 1, 1, 1, 3, 1};
        iArr[CODE_START_A] = new int[]{2, 1, 1, 4, 1, 2};
        iArr[CODE_START_B] = new int[]{2, 1, 1, 2, 1, 4};
        iArr[CODE_START_C] = new int[]{2, 1, 1, 2, 3, 2};
        iArr[CODE_STOP] = new int[]{2, 3, 3, 1, 1, 1, 2};
        CODE_PATTERNS = iArr;
    }

    private static int decodeCode(BitArray bitArray, int[] iArr, int i) throws NotFoundException {
        recordPattern(bitArray, i, iArr);
        int i2 = MAX_AVG_VARIANCE;
        int i3 = -1;
        for (int i4 = 0; i4 < CODE_PATTERNS.length; i4++) {
            int patternMatchVariance = patternMatchVariance(iArr, CODE_PATTERNS[i4], MAX_INDIVIDUAL_VARIANCE);
            if (patternMatchVariance < i2) {
                i3 = i4;
                i2 = patternMatchVariance;
            }
        }
        if (i3 >= 0) {
            return i3;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int[] findStartPattern(BitArray bitArray) throws NotFoundException {
        int i;
        int i2;
        boolean z;
        int size = bitArray.getSize();
        int i3 = 0;
        while (i3 < size && !bitArray.get(i3)) {
            i3++;
        }
        int[] iArr = new int[6];
        int length = iArr.length;
        int i4 = i3;
        boolean z2 = false;
        int i5 = i3;
        int i6 = 0;
        while (i4 < size) {
            if (bitArray.get(i4) ^ z2) {
                iArr[i6] = iArr[i6] + 1;
                z = z2;
                i = i6;
            } else {
                if (i6 == length - 1) {
                    int i7 = MAX_AVG_VARIANCE;
                    int i8 = -1;
                    int i9 = CODE_START_A;
                    while (i9 <= CODE_START_C) {
                        int patternMatchVariance = patternMatchVariance(iArr, CODE_PATTERNS[i9], MAX_INDIVIDUAL_VARIANCE);
                        if (patternMatchVariance < i7) {
                            i8 = i9;
                        } else {
                            patternMatchVariance = i7;
                        }
                        i9++;
                        i7 = patternMatchVariance;
                    }
                    if (i8 < 0 || !bitArray.isRange(Math.max(0, i5 - ((i4 - i5) / 2)), i5, false)) {
                        i2 = iArr[0] + iArr[1] + i5;
                        for (int i10 = 2; i10 < length; i10++) {
                            iArr[i10 - 2] = iArr[i10];
                        }
                        iArr[length - 2] = 0;
                        iArr[length - 1] = 0;
                        i = i6 - 1;
                    } else {
                        return new int[]{i5, i4, i8};
                    }
                } else {
                    i = i6 + 1;
                    i2 = i5;
                }
                iArr[i] = 1;
                z = !z2;
                i5 = i2;
            }
            i4++;
            z2 = z;
            i6 = i;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.zxing.Result decodeRow(int r19, com.google.zxing.common.BitArray r20, java.util.Hashtable r21) throws com.google.zxing.NotFoundException, com.google.zxing.FormatException, com.google.zxing.ChecksumException {
        /*
            r18 = this;
            int[] r13 = findStartPattern(r20)
            r1 = 2
            r3 = r13[r1]
            switch(r3) {
                case 103: goto L_0x000f;
                case 104: goto L_0x0056;
                case 105: goto L_0x0059;
                default: goto L_0x000a;
            }
        L_0x000a:
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.getFormatInstance()
            throw r1
        L_0x000f:
            r1 = 101(0x65, float:1.42E-43)
        L_0x0011:
            r8 = 0
            r7 = 0
            java.lang.StringBuffer r14 = new java.lang.StringBuffer
            r2 = 20
            r14.<init>(r2)
            r2 = 0
            r6 = r13[r2]
            r2 = 1
            r5 = r13[r2]
            r2 = 6
            int[] r15 = new int[r2]
            r10 = 0
            r4 = 0
            r2 = 0
            r9 = 1
            r12 = r7
            r7 = r8
            r8 = r1
            r1 = r2
            r2 = r3
            r3 = r10
            r10 = r4
            r4 = r6
        L_0x002f:
            if (r7 != 0) goto L_0x015c
            r6 = 0
            r0 = r20
            int r11 = decodeCode(r0, r15, r5)
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x003d
            r9 = 1
        L_0x003d:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x0046
            int r1 = r1 + 1
            int r3 = r1 * r11
            int r2 = r2 + r3
        L_0x0046:
            r3 = 0
            r4 = r5
        L_0x0048:
            int r0 = r15.length
            r16 = r0
            r0 = r16
            if (r3 >= r0) goto L_0x005c
            r16 = r15[r3]
            int r4 = r4 + r16
            int r3 = r3 + 1
            goto L_0x0048
        L_0x0056:
            r1 = 100
            goto L_0x0011
        L_0x0059:
            r1 = 99
            goto L_0x0011
        L_0x005c:
            switch(r11) {
                case 103: goto L_0x0077;
                case 104: goto L_0x0077;
                case 105: goto L_0x0077;
                default: goto L_0x005f;
            }
        L_0x005f:
            switch(r8) {
                case 99: goto L_0x0100;
                case 100: goto L_0x00d4;
                case 101: goto L_0x007c;
                default: goto L_0x0062;
            }
        L_0x0062:
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
        L_0x0066:
            if (r12 == 0) goto L_0x006b
            switch(r3) {
                case 99: goto L_0x0158;
                case 100: goto L_0x0154;
                case 101: goto L_0x0150;
                default: goto L_0x006b;
            }
        L_0x006b:
            r9 = r6
            r12 = r7
            r7 = r8
            r8 = r3
            r3 = r10
            r10 = r11
            r17 = r5
            r5 = r4
            r4 = r17
            goto L_0x002f
        L_0x0077:
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.getFormatInstance()
            throw r1
        L_0x007c:
            r3 = 64
            if (r11 >= r3) goto L_0x008b
            int r3 = r11 + 32
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x008b:
            r3 = 96
            if (r11 >= r3) goto L_0x009a
            int r3 = r11 + -64
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x009a:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x009f
            r9 = 0
        L_0x009f:
            switch(r11) {
                case 96: goto L_0x00ad;
                case 97: goto L_0x00ad;
                case 98: goto L_0x00b1;
                case 99: goto L_0x00c3;
                case 100: goto L_0x00ba;
                case 101: goto L_0x00ad;
                case 102: goto L_0x00ad;
                case 103: goto L_0x00a2;
                case 104: goto L_0x00a2;
                case 105: goto L_0x00a2;
                case 106: goto L_0x00cc;
                default: goto L_0x00a2;
            }
        L_0x00a2:
            r3 = r6
            r6 = r7
            r7 = r8
        L_0x00a5:
            r8 = r6
            r6 = r9
            r17 = r3
            r3 = r7
            r7 = r17
            goto L_0x0066
        L_0x00ad:
            r3 = r6
            r6 = r7
            r7 = r8
            goto L_0x00a5
        L_0x00b1:
            r3 = 1
            r6 = 100
            r17 = r7
            r7 = r6
            r6 = r17
            goto L_0x00a5
        L_0x00ba:
            r3 = 100
            r17 = r6
            r6 = r7
            r7 = r3
            r3 = r17
            goto L_0x00a5
        L_0x00c3:
            r3 = 99
            r17 = r6
            r6 = r7
            r7 = r3
            r3 = r17
            goto L_0x00a5
        L_0x00cc:
            r3 = 1
            r7 = r8
            r17 = r3
            r3 = r6
            r6 = r17
            goto L_0x00a5
        L_0x00d4:
            r3 = 96
            if (r11 >= r3) goto L_0x00e3
            int r3 = r11 + 32
            char r3 = (char) r3
            r14.append(r3)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x00e3:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x01ec
            r3 = 0
        L_0x00e8:
            switch(r11) {
                case 96: goto L_0x00eb;
                case 97: goto L_0x00eb;
                case 98: goto L_0x00f4;
                case 99: goto L_0x00fb;
                case 100: goto L_0x00eb;
                case 101: goto L_0x00f8;
                case 102: goto L_0x00eb;
                case 103: goto L_0x00eb;
                case 104: goto L_0x00eb;
                case 105: goto L_0x00eb;
                case 106: goto L_0x00fe;
                default: goto L_0x00eb;
            }
        L_0x00eb:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x00f4:
            r6 = 1
            r8 = 99
            goto L_0x00eb
        L_0x00f8:
            r8 = 101(0x65, float:1.42E-43)
            goto L_0x00eb
        L_0x00fb:
            r8 = 99
            goto L_0x00eb
        L_0x00fe:
            r7 = 1
            goto L_0x00eb
        L_0x0100:
            r3 = 100
            if (r11 >= r3) goto L_0x0116
            r3 = 10
            if (r11 >= r3) goto L_0x010d
            r3 = 48
            r14.append(r3)
        L_0x010d:
            r14.append(r11)
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r9
            goto L_0x0066
        L_0x0116:
            r3 = 106(0x6a, float:1.49E-43)
            if (r11 == r3) goto L_0x01e9
            r3 = 0
        L_0x011b:
            switch(r11) {
                case 100: goto L_0x013b;
                case 101: goto L_0x0130;
                case 102: goto L_0x0127;
                case 103: goto L_0x011e;
                case 104: goto L_0x011e;
                case 105: goto L_0x011e;
                case 106: goto L_0x0146;
                default: goto L_0x011e;
            }
        L_0x011e:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0127:
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0130:
            r8 = 101(0x65, float:1.42E-43)
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x013b:
            r8 = 100
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0146:
            r7 = 1
            r17 = r3
            r3 = r8
            r8 = r7
            r7 = r6
            r6 = r17
            goto L_0x0066
        L_0x0150:
            r3 = 99
            goto L_0x006b
        L_0x0154:
            r3 = 101(0x65, float:1.42E-43)
            goto L_0x006b
        L_0x0158:
            r3 = 100
            goto L_0x006b
        L_0x015c:
            int r6 = r20.getSize()
        L_0x0160:
            if (r5 >= r6) goto L_0x016d
            r0 = r20
            boolean r7 = r0.get(r5)
            if (r7 == 0) goto L_0x016d
            int r5 = r5 + 1
            goto L_0x0160
        L_0x016d:
            int r7 = r5 - r4
            int r7 = r7 / 2
            int r7 = r7 + r5
            int r6 = java.lang.Math.min(r6, r7)
            r7 = 0
            r0 = r20
            boolean r6 = r0.isRange(r5, r6, r7)
            if (r6 != 0) goto L_0x0184
            com.google.zxing.NotFoundException r1 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r1
        L_0x0184:
            int r1 = r1 * r3
            int r1 = r2 - r1
            int r1 = r1 % 103
            if (r1 == r3) goto L_0x0190
            com.google.zxing.ChecksumException r1 = com.google.zxing.ChecksumException.getChecksumInstance()
            throw r1
        L_0x0190:
            int r1 = r14.length()
            if (r1 <= 0) goto L_0x01a1
            if (r9 == 0) goto L_0x01a1
            r2 = 99
            if (r8 != r2) goto L_0x01b0
            int r2 = r1 + -2
            r14.delete(r2, r1)
        L_0x01a1:
            java.lang.String r1 = r14.toString()
            int r2 = r1.length()
            if (r2 != 0) goto L_0x01b6
            com.google.zxing.FormatException r1 = com.google.zxing.FormatException.getFormatInstance()
            throw r1
        L_0x01b0:
            int r2 = r1 + -1
            r14.delete(r2, r1)
            goto L_0x01a1
        L_0x01b6:
            r2 = 1
            r2 = r13[r2]
            r3 = 0
            r3 = r13[r3]
            int r2 = r2 + r3
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r2 = r2 / r3
            int r3 = r5 + r4
            float r3 = (float) r3
            r4 = 1073741824(0x40000000, float:2.0)
            float r3 = r3 / r4
            com.google.zxing.Result r4 = new com.google.zxing.Result
            r5 = 0
            r6 = 2
            com.google.zxing.ResultPoint[] r6 = new com.google.zxing.ResultPoint[r6]
            r7 = 0
            com.google.zxing.ResultPoint r8 = new com.google.zxing.ResultPoint
            r0 = r19
            float r9 = (float) r0
            r8.<init>(r2, r9)
            r6[r7] = r8
            r2 = 1
            com.google.zxing.ResultPoint r7 = new com.google.zxing.ResultPoint
            r0 = r19
            float r8 = (float) r0
            r7.<init>(r3, r8)
            r6[r2] = r7
            com.google.zxing.BarcodeFormat r2 = com.google.zxing.BarcodeFormat.CODE_128
            r4.<init>(r1, r5, r6, r2)
            return r4
        L_0x01e9:
            r3 = r9
            goto L_0x011b
        L_0x01ec:
            r3 = r9
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.oned.Code128Reader.decodeRow(int, com.google.zxing.common.BitArray, java.util.Hashtable):com.google.zxing.Result");
    }
}
