package com.google.zxing.oned;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Hashtable;

public abstract class OneDReader implements Reader {
    protected static final int INTEGER_MATH_SHIFT = 8;
    protected static final int PATTERN_MATCH_RESULT_SCALE_FACTOR = 256;

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.zxing.Result doDecode(com.google.zxing.BinaryBitmap r18, java.util.Hashtable r19) throws com.google.zxing.NotFoundException {
        /*
            r17 = this;
            int r8 = r18.getWidth()
            int r2 = r18.getHeight()
            com.google.zxing.common.BitArray r4 = new com.google.zxing.common.BitArray
            r4.<init>(r8)
            int r9 = r2 >> 1
            if (r19 == 0) goto L_0x0049
            com.google.zxing.DecodeHintType r1 = com.google.zxing.DecodeHintType.TRY_HARDER
            r0 = r19
            boolean r1 = r0.containsKey(r1)
            if (r1 == 0) goto L_0x0049
            r1 = 1
            r3 = r1
        L_0x001d:
            r5 = 1
            if (r3 == 0) goto L_0x004c
            r1 = 8
        L_0x0022:
            int r1 = r2 >> r1
            int r10 = java.lang.Math.max(r5, r1)
            if (r3 == 0) goto L_0x004e
            r1 = r2
        L_0x002b:
            r3 = 0
            r7 = r3
            r3 = r4
            r4 = r19
        L_0x0030:
            if (r7 >= r1) goto L_0x0044
            int r5 = r7 + 1
            int r5 = r5 >> 1
            r6 = r7 & 1
            if (r6 != 0) goto L_0x0051
            r6 = 1
        L_0x003b:
            if (r6 == 0) goto L_0x0053
        L_0x003d:
            int r5 = r5 * r10
            int r11 = r9 + r5
            if (r11 < 0) goto L_0x0044
            if (r11 < r2) goto L_0x0055
        L_0x0044:
            com.google.zxing.NotFoundException r1 = com.google.zxing.NotFoundException.getNotFoundInstance()
            throw r1
        L_0x0049:
            r1 = 0
            r3 = r1
            goto L_0x001d
        L_0x004c:
            r1 = 5
            goto L_0x0022
        L_0x004e:
            r1 = 15
            goto L_0x002b
        L_0x0051:
            r6 = 0
            goto L_0x003b
        L_0x0053:
            int r5 = -r5
            goto L_0x003d
        L_0x0055:
            r0 = r18
            com.google.zxing.common.BitArray r3 = r0.getBlackRow(r11, r3)     // Catch:{ NotFoundException -> 0x00f2 }
            r5 = 0
            r6 = r5
        L_0x005d:
            r5 = 2
            if (r6 >= r5) goto L_0x00f3
            r5 = 1
            if (r6 != r5) goto L_0x0094
            r3.reverse()
            if (r4 == 0) goto L_0x0094
            com.google.zxing.DecodeHintType r5 = com.google.zxing.DecodeHintType.NEED_RESULT_POINT_CALLBACK
            boolean r5 = r4.containsKey(r5)
            if (r5 == 0) goto L_0x0094
            java.util.Hashtable r5 = new java.util.Hashtable
            r5.<init>()
            java.util.Enumeration r12 = r4.keys()
        L_0x0079:
            boolean r13 = r12.hasMoreElements()
            if (r13 == 0) goto L_0x0093
            java.lang.Object r13 = r12.nextElement()
            com.google.zxing.DecodeHintType r14 = com.google.zxing.DecodeHintType.NEED_RESULT_POINT_CALLBACK
            boolean r14 = r13.equals(r14)
            if (r14 != 0) goto L_0x0079
            java.lang.Object r14 = r4.get(r13)
            r5.put(r13, r14)
            goto L_0x0079
        L_0x0093:
            r4 = r5
        L_0x0094:
            r0 = r17
            com.google.zxing.Result r5 = r0.decodeRow(r11, r3, r4)     // Catch:{ ReaderException -> 0x00ec }
            r12 = 1
            if (r6 != r12) goto L_0x00eb
            com.google.zxing.ResultMetadataType r12 = com.google.zxing.ResultMetadataType.ORIENTATION     // Catch:{ ReaderException -> 0x00ec }
            java.lang.Integer r13 = new java.lang.Integer     // Catch:{ ReaderException -> 0x00ec }
            r14 = 180(0xb4, float:2.52E-43)
            r13.<init>(r14)     // Catch:{ ReaderException -> 0x00ec }
            r5.putMetadata(r12, r13)     // Catch:{ ReaderException -> 0x00ec }
            com.google.zxing.ResultPoint[] r12 = r5.getResultPoints()     // Catch:{ ReaderException -> 0x00ec }
            r13 = 0
            com.google.zxing.ResultPoint r14 = new com.google.zxing.ResultPoint     // Catch:{ ReaderException -> 0x00ec }
            float r15 = (float) r8     // Catch:{ ReaderException -> 0x00ec }
            r16 = 0
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.getX()     // Catch:{ ReaderException -> 0x00ec }
            float r15 = r15 - r16
            r16 = 1065353216(0x3f800000, float:1.0)
            float r15 = r15 - r16
            r16 = 0
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.getY()     // Catch:{ ReaderException -> 0x00ec }
            r14.<init>(r15, r16)     // Catch:{ ReaderException -> 0x00ec }
            r12[r13] = r14     // Catch:{ ReaderException -> 0x00ec }
            r13 = 1
            com.google.zxing.ResultPoint r14 = new com.google.zxing.ResultPoint     // Catch:{ ReaderException -> 0x00ec }
            float r15 = (float) r8     // Catch:{ ReaderException -> 0x00ec }
            r16 = 1
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.getX()     // Catch:{ ReaderException -> 0x00ec }
            float r15 = r15 - r16
            r16 = 1065353216(0x3f800000, float:1.0)
            float r15 = r15 - r16
            r16 = 1
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.getY()     // Catch:{ ReaderException -> 0x00ec }
            r14.<init>(r15, r16)     // Catch:{ ReaderException -> 0x00ec }
            r12[r13] = r14     // Catch:{ ReaderException -> 0x00ec }
        L_0x00eb:
            return r5
        L_0x00ec:
            r5 = move-exception
            int r5 = r6 + 1
            r6 = r5
            goto L_0x005d
        L_0x00f2:
            r5 = move-exception
        L_0x00f3:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.oned.OneDReader.doDecode(com.google.zxing.BinaryBitmap, java.util.Hashtable):com.google.zxing.Result");
    }

    protected static int patternMatchVariance(int[] iArr, int[] iArr2, int i) {
        int length = iArr.length;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            i3 += iArr[i4];
            i2 += iArr2[i4];
        }
        if (i3 < i2) {
            return Integer.MAX_VALUE;
        }
        int i5 = (i3 << 8) / i2;
        int i6 = (i * i5) >> 8;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            int i9 = iArr[i8] << 8;
            int i10 = iArr2[i8] * i5;
            int i11 = i9 > i10 ? i9 - i10 : i10 - i9;
            if (i11 > i6) {
                return Integer.MAX_VALUE;
            }
            i7 += i11;
        }
        return i7 / i3;
    }

    protected static void recordPattern(BitArray bitArray, int i, int[] iArr) throws NotFoundException {
        int i2;
        boolean z;
        int length = iArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            iArr[i3] = 0;
        }
        int size = bitArray.getSize();
        if (i >= size) {
            throw NotFoundException.getNotFoundInstance();
        }
        boolean z2 = !bitArray.get(i);
        int i4 = 0;
        while (true) {
            if (i >= size) {
                i2 = i4;
                break;
            }
            if (bitArray.get(i) ^ z2) {
                iArr[i4] = iArr[i4] + 1;
                z = z2;
            } else {
                i2 = i4 + 1;
                if (i2 == length) {
                    break;
                }
                iArr[i2] = 1;
                int i5 = i2;
                z = !z2;
                i4 = i5;
            }
            i++;
            z2 = z;
        }
        if (i2 == length) {
            return;
        }
        if (i2 != length - 1 || i != size) {
            throw NotFoundException.getNotFoundInstance();
        }
    }

    protected static void recordPatternInReverse(BitArray bitArray, int i, int[] iArr) throws NotFoundException {
        int length = iArr.length;
        boolean z = bitArray.get(i);
        while (i > 0 && length >= 0) {
            i--;
            if (bitArray.get(i) != z) {
                length--;
                z = !z;
            }
        }
        if (length >= 0) {
            throw NotFoundException.getNotFoundInstance();
        }
        recordPattern(bitArray, i + 1, iArr);
    }

    public Result decode(BinaryBitmap binaryBitmap) throws NotFoundException, FormatException {
        return decode(binaryBitmap, null);
    }

    public Result decode(BinaryBitmap binaryBitmap, Hashtable hashtable) throws NotFoundException, FormatException {
        try {
            return doDecode(binaryBitmap, hashtable);
        } catch (NotFoundException e) {
            if (!(hashtable != null && hashtable.containsKey(DecodeHintType.TRY_HARDER)) || !binaryBitmap.isRotateSupported()) {
                throw e;
            }
            BinaryBitmap rotateCounterClockwise = binaryBitmap.rotateCounterClockwise();
            Result doDecode = doDecode(rotateCounterClockwise, hashtable);
            Hashtable resultMetadata = doDecode.getResultMetadata();
            doDecode.putMetadata(ResultMetadataType.ORIENTATION, new Integer((resultMetadata == null || !resultMetadata.containsKey(ResultMetadataType.ORIENTATION)) ? 270 : (((Integer) resultMetadata.get(ResultMetadataType.ORIENTATION)).intValue() + 270) % 360));
            ResultPoint[] resultPoints = doDecode.getResultPoints();
            int height = rotateCounterClockwise.getHeight();
            for (int i = 0; i < resultPoints.length; i++) {
                resultPoints[i] = new ResultPoint((((float) height) - resultPoints[i].getY()) - 1.0f, resultPoints[i].getX());
            }
            return doDecode;
        }
    }

    public abstract Result decodeRow(int i, BitArray bitArray, Hashtable hashtable) throws NotFoundException, ChecksumException, FormatException;

    public void reset() {
    }
}
