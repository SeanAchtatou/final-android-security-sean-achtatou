package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Hashtable;

public final class CodaBarReader extends OneDReader {
    private static final char[] ALPHABET = ALPHABET_STRING.toCharArray();
    private static final String ALPHABET_STRING = "0123456789-$:/.+ABCDTN";
    private static final int[] CHARACTER_ENCODINGS = {3, 6, 9, 96, 18, 66, 33, 36, 48, 72, 12, 24, 37, 81, 84, 21, 26, 41, 11, 14, 26, 41};
    private static final char[] STARTEND_ENCODING = {'E', '*', 'A', 'B', 'C', 'D', 'T', 'N'};
    private static final int minCharacterLength = 6;

    private static boolean arrayContains(char[] cArr, char c) {
        if (cArr == null) {
            return false;
        }
        for (char c2 : cArr) {
            if (c2 == c) {
                return true;
            }
        }
        return false;
    }

    private static int[] findAsteriskPattern(BitArray bitArray) throws NotFoundException {
        int i;
        int size = bitArray.getSize();
        int i2 = 0;
        while (i2 < size && !bitArray.get(i2)) {
            i2++;
        }
        int[] iArr = new int[7];
        int length = iArr.length;
        int i3 = i2;
        boolean z = false;
        int i4 = 0;
        while (i3 < size) {
            if (bitArray.get(i3) ^ z) {
                iArr[i] = iArr[i] + 1;
            } else {
                if (i == length - 1) {
                    try {
                        if (arrayContains(STARTEND_ENCODING, toNarrowWidePattern(iArr)) && bitArray.isRange(Math.max(0, i2 - ((i3 - i2) / 2)), i2, false)) {
                            return new int[]{i2, i3};
                        }
                    } catch (IllegalArgumentException e) {
                    }
                    i2 += iArr[0] + iArr[1];
                    for (int i5 = 2; i5 < length; i5++) {
                        iArr[i5 - 2] = iArr[i5];
                    }
                    iArr[length - 2] = 0;
                    iArr[length - 1] = 0;
                    i--;
                } else {
                    i++;
                }
                iArr[i] = 1;
                z = !z;
            }
            boolean z2 = z;
            i3++;
            i2 = i2;
            i4 = i;
            z = z2;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static char toNarrowWidePattern(int[] iArr) {
        int length = iArr.length;
        int i = Integer.MAX_VALUE;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (iArr[i3] < i) {
                i = iArr[i3];
            }
            if (iArr[i3] > i2) {
                i2 = iArr[i3];
            }
        }
        do {
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 0; i6 < length; i6++) {
                if (iArr[i6] > i2) {
                    i4 |= 1 << ((length - 1) - i6);
                    i5++;
                }
            }
            if (i5 == 2 || i5 == 3) {
                for (int i7 = 0; i7 < CHARACTER_ENCODINGS.length; i7++) {
                    if (CHARACTER_ENCODINGS[i7] == i4) {
                        return ALPHABET[i7];
                    }
                }
            }
            i2--;
        } while (i2 > i);
        return '!';
    }

    public Result decodeRow(int i, BitArray bitArray, Hashtable hashtable) throws NotFoundException {
        int[] findAsteriskPattern = findAsteriskPattern(bitArray);
        findAsteriskPattern[1] = 0;
        int i2 = findAsteriskPattern[1];
        int size = bitArray.getSize();
        while (i2 < size && !bitArray.get(i2)) {
            i2++;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int[] iArr = {0, 0, 0, 0, 0, 0, 0};
            recordPattern(bitArray, i2, iArr);
            char narrowWidePattern = toNarrowWidePattern(iArr);
            if (narrowWidePattern == '!') {
                throw NotFoundException.getNotFoundInstance();
            }
            stringBuffer.append(narrowWidePattern);
            int i3 = i2;
            for (int i4 : iArr) {
                i3 += i4;
            }
            int i5 = i3;
            while (i5 < size && !bitArray.get(i5)) {
                i5++;
            }
            if (i5 >= size) {
                int i6 = 0;
                for (int i7 : iArr) {
                    i6 += i7;
                }
                int i8 = (i5 - i2) - i6;
                if (i5 != size && i8 / 2 < i6) {
                    throw NotFoundException.getNotFoundInstance();
                } else if (stringBuffer.length() < 2) {
                    throw NotFoundException.getNotFoundInstance();
                } else {
                    char charAt = stringBuffer.charAt(0);
                    if (!arrayContains(STARTEND_ENCODING, charAt)) {
                        throw NotFoundException.getNotFoundInstance();
                    }
                    int i9 = 1;
                    while (i9 < stringBuffer.length()) {
                        if (stringBuffer.charAt(i9) == charAt && i9 + 1 != stringBuffer.length()) {
                            stringBuffer.delete(i9 + 1, stringBuffer.length() - 1);
                            i9 = stringBuffer.length();
                        }
                        i9++;
                    }
                    if (stringBuffer.length() > 6) {
                        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                        stringBuffer.deleteCharAt(0);
                        return new Result(stringBuffer.toString(), null, new ResultPoint[]{new ResultPoint(((float) (findAsteriskPattern[1] + findAsteriskPattern[0])) / 2.0f, (float) i), new ResultPoint(((float) (i2 + i5)) / 2.0f, (float) i)}, BarcodeFormat.CODABAR);
                    }
                    throw NotFoundException.getNotFoundInstance();
                }
            } else {
                i2 = i5;
            }
        }
    }
}
