package com.google.zxing.client.result.optional;

import com.google.zxing.Result;
import com.google.zxing.client.result.TextParsedResult;
import org.apache.commons.lang.CharEncoding;

final class NDEFTextResultParser extends AbstractNDEFResultParser {
    NDEFTextResultParser() {
    }

    static String[] decodeTextPayload(byte[] bArr) {
        byte b = bArr[0];
        boolean z = (b & 128) != 0;
        byte b2 = b & 31;
        return new String[]{bytesToString(bArr, 1, b2, CharEncoding.US_ASCII), bytesToString(bArr, b2 + 1, (bArr.length - b2) - 1, z ? CharEncoding.UTF_16 : "UTF8")};
    }

    public static TextParsedResult parse(Result result) {
        NDEFRecord readRecord;
        byte[] rawBytes = result.getRawBytes();
        if (rawBytes == null || (readRecord = NDEFRecord.readRecord(rawBytes, 0)) == null || !readRecord.isMessageBegin() || !readRecord.isMessageEnd() || !readRecord.getType().equals(NDEFRecord.TEXT_WELL_KNOWN_TYPE)) {
            return null;
        }
        String[] decodeTextPayload = decodeTextPayload(readRecord.getPayload());
        return new TextParsedResult(decodeTextPayload[0], decodeTextPayload[1]);
    }
}
