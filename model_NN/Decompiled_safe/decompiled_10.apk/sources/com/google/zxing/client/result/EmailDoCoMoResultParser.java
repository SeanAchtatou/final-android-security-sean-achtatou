package com.google.zxing.client.result;

import com.google.zxing.Result;
import org.apache.commons.lang.ClassUtils;

final class EmailDoCoMoResultParser extends AbstractDoCoMoResultParser {
    private static final char[] ATEXT_SYMBOLS = {'@', ClassUtils.PACKAGE_SEPARATOR_CHAR, '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^', '_', '`', '{', '|', '}', '~'};

    EmailDoCoMoResultParser() {
    }

    private static boolean isAtextSymbol(char c) {
        for (char c2 : ATEXT_SYMBOLS) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    static boolean isBasicallyValidEmailAddress(String str) {
        if (str == null) {
            return false;
        }
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && ((charAt < '0' || charAt > '9') && !isAtextSymbol(charAt)))) {
                return false;
            }
            if (charAt == '@') {
                if (z) {
                    return false;
                }
                z = true;
            }
        }
        return z;
    }

    public static EmailAddressParsedResult parse(Result result) {
        String[] matchDoCoMoPrefixedField;
        String text = result.getText();
        if (text == null || !text.startsWith("MATMSG:") || (matchDoCoMoPrefixedField = matchDoCoMoPrefixedField("TO:", text, true)) == null) {
            return null;
        }
        String str = matchDoCoMoPrefixedField[0];
        if (isBasicallyValidEmailAddress(str)) {
            return new EmailAddressParsedResult(str, matchSingleDoCoMoPrefixedField("SUB:", text, false), matchSingleDoCoMoPrefixedField("BODY:", text, false), new StringBuffer().append("mailto:").append(str).toString());
        }
        return null;
    }
}
