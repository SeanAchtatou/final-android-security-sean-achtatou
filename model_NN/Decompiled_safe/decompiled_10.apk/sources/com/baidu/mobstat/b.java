package com.baidu.mobstat;

import android.content.Context;
import android.net.wifi.WifiManager;
import com.baidu.mobstat.a.a;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b {
    private static String a = "Android";
    private static JSONObject q = new JSONObject();
    private static b w = new b();
    private String b;
    private String c = null;
    private int d = -1;
    private String e;
    private String f;
    private int g;
    private int h;
    private String i = null;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private int p = 0;
    private JSONArray r = new JSONArray();
    private JSONArray s = new JSONArray();
    private JSONArray t = new JSONArray();
    private JSONArray u = new JSONArray();
    private boolean v = false;

    private b() {
    }

    public static b a() {
        return w;
    }

    private String a(String str, Context context) {
        if (str.equals("000000000000000")) {
            String g2 = q.g(context);
            if (g2 != null) {
                g2 = g2.replaceAll(":", StringUtils.EMPTY);
                str = g2;
            }
            com.baidu.mobstat.a.b.a("stat", "imei=null,mac=" + g2);
        }
        return str;
    }

    private void a(boolean z) {
        this.v = z;
    }

    private boolean d() {
        return this.v;
    }

    public void a(long j2, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("t", j2);
            jSONObject.put("c", str);
        } catch (JSONException e2) {
            com.baidu.mobstat.a.b.a("stat", e2);
        }
        c(jSONObject, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00cf, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00d0, code lost:
        com.baidu.mobstat.a.b.a(r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(android.content.Context r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            int r0 = r2.d     // Catch:{ all -> 0x00c6 }
            r1 = -1
            if (r0 == r1) goto L_0x0008
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            com.baidu.mobstat.a.a.d(r3, r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "android.permission.INTERNET"
            com.baidu.mobstat.a.a.d(r3, r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            com.baidu.mobstat.a.a.d(r3, r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r3.getSystemService(r0)     // Catch:{ all -> 0x00c6 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = android.os.Build.VERSION.SDK     // Catch:{ all -> 0x00c6 }
            r2.b = r1     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ all -> 0x00c6 }
            r2.k = r1     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = r0.getDeviceId()     // Catch:{ Exception -> 0x00c9 }
            r2.f = r1     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r1 = r2.f     // Catch:{ Exception -> 0x00c9 }
            java.lang.String r1 = r2.a(r1, r3)     // Catch:{ Exception -> 0x00c9 }
            r2.f = r1     // Catch:{ Exception -> 0x00c9 }
        L_0x0035:
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x00cf }
            r2.j = r0     // Catch:{ Exception -> 0x00cf }
        L_0x003b:
            int r0 = com.baidu.mobstat.q.a(r3)     // Catch:{ Exception -> 0x00d5 }
            r2.g = r0     // Catch:{ Exception -> 0x00d5 }
            int r0 = com.baidu.mobstat.q.b(r3)     // Catch:{ Exception -> 0x00d5 }
            r2.h = r0     // Catch:{ Exception -> 0x00d5 }
            android.content.res.Resources r0 = r3.getResources()     // Catch:{ Exception -> 0x00d5 }
            android.content.res.Configuration r0 = r0.getConfiguration()     // Catch:{ Exception -> 0x00d5 }
            int r0 = r0.orientation     // Catch:{ Exception -> 0x00d5 }
            r1 = 2
            if (r0 != r1) goto L_0x0070
            java.lang.String r0 = "stat"
            java.lang.String r1 = "Configuration.ORIENTATION_LANDSCAPE"
            com.baidu.mobstat.a.b.a(r0, r1)     // Catch:{ Exception -> 0x00d5 }
            int r0 = r2.g     // Catch:{ Exception -> 0x00d5 }
            int r1 = r2.h     // Catch:{ Exception -> 0x00d5 }
            r0 = r0 ^ r1
            r2.g = r0     // Catch:{ Exception -> 0x00d5 }
            int r0 = r2.g     // Catch:{ Exception -> 0x00d5 }
            int r1 = r2.h     // Catch:{ Exception -> 0x00d5 }
            r0 = r0 ^ r1
            r2.h = r0     // Catch:{ Exception -> 0x00d5 }
            int r0 = r2.g     // Catch:{ Exception -> 0x00d5 }
            int r1 = r2.h     // Catch:{ Exception -> 0x00d5 }
            r0 = r0 ^ r1
            r2.g = r0     // Catch:{ Exception -> 0x00d5 }
        L_0x0070:
            java.lang.String r0 = r2.i     // Catch:{ Exception -> 0x00da }
            if (r0 != 0) goto L_0x007c
            java.lang.String r0 = "BaiduMobAd_CHANNEL"
            java.lang.String r0 = com.baidu.mobstat.q.a(r3, r0)     // Catch:{ Exception -> 0x00da }
            r2.i = r0     // Catch:{ Exception -> 0x00da }
        L_0x007c:
            java.lang.String r0 = r2.c     // Catch:{ all -> 0x00c6 }
            if (r0 != 0) goto L_0x0088
            java.lang.String r0 = "BaiduMobAd_STAT_ID"
            java.lang.String r0 = com.baidu.mobstat.q.a(r3, r0)     // Catch:{ all -> 0x00c6 }
            r2.c = r0     // Catch:{ all -> 0x00c6 }
        L_0x0088:
            int r0 = com.baidu.mobstat.q.c(r3)     // Catch:{ Exception -> 0x00df }
            r2.d = r0     // Catch:{ Exception -> 0x00df }
            java.lang.String r0 = com.baidu.mobstat.q.d(r3)     // Catch:{ Exception -> 0x00df }
            r2.e = r0     // Catch:{ Exception -> 0x00df }
        L_0x0094:
            java.lang.String r0 = "BaiduMobAd_CELL_LOCATION"
            java.lang.String r0 = com.baidu.mobstat.q.a(r3, r0)     // Catch:{ Exception -> 0x00eb }
            if (r0 == 0) goto L_0x00e4
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x00eb }
            java.lang.String r1 = "false"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00eb }
            if (r0 == 0) goto L_0x00e4
            java.lang.String r0 = "0_0_0"
            r2.l = r0     // Catch:{ Exception -> 0x00eb }
        L_0x00ac:
            java.lang.String r0 = com.baidu.mobstat.q.f(r3)     // Catch:{ Exception -> 0x00f0 }
            r2.m = r0     // Catch:{ Exception -> 0x00f0 }
        L_0x00b2:
            java.lang.String r0 = com.baidu.mobstat.q.h(r3)     // Catch:{ Exception -> 0x00f5 }
            r2.n = r0     // Catch:{ Exception -> 0x00f5 }
        L_0x00b8:
            java.lang.String r0 = com.baidu.mobstat.q.i(r3)     // Catch:{ Exception -> 0x00c0 }
            r2.o = r0     // Catch:{ Exception -> 0x00c0 }
            goto L_0x0006
        L_0x00c0:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x0006
        L_0x00c6:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x00c9:
            r1 = move-exception
            com.baidu.mobstat.a.b.a(r1)     // Catch:{ all -> 0x00c6 }
            goto L_0x0035
        L_0x00cf:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x003b
        L_0x00d5:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x0070
        L_0x00da:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x007c
        L_0x00df:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x0094
        L_0x00e4:
            java.lang.String r0 = com.baidu.mobstat.q.e(r3)     // Catch:{ Exception -> 0x00eb }
            r2.l = r0     // Catch:{ Exception -> 0x00eb }
            goto L_0x00ac
        L_0x00eb:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x00ac
        L_0x00f0:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x00b2
        L_0x00f5:
            r0 = move-exception
            com.baidu.mobstat.a.b.a(r0)     // Catch:{ all -> 0x00c6 }
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.b.a(android.content.Context):void");
    }

    public void a(Context context, boolean z) {
        boolean z2;
        com.baidu.mobstat.a.b.a("stat", "sendLogData() begin.");
        if (z) {
            try {
                if (!((WifiManager) context.getSystemService("wifi")).isWifiEnabled()) {
                    com.baidu.mobstat.a.b.a("stat", "sendLogData() does not send because of only_wifi setting");
                    return;
                }
            } catch (Exception e2) {
                com.baidu.mobstat.a.b.a("stat", "sendLogData exception when get wifimanager");
                return;
            }
        }
        JSONObject jSONObject = new JSONObject();
        synchronized (q) {
            try {
                q.put("t", System.currentTimeMillis());
                q.put("ss", m.a().c());
                jSONObject.put("he", q);
                synchronized (this.r) {
                    try {
                        jSONObject.put("pr", this.r);
                        synchronized (this.s) {
                            try {
                                jSONObject.put("ev", this.s);
                                synchronized (this.t) {
                                    synchronized (this.u) {
                                        try {
                                            jSONObject.put("ex", this.u);
                                            String jSONObject2 = jSONObject.toString();
                                            com.baidu.mobstat.a.b.a("stat", "---Send Data Is:" + jSONObject2);
                                            try {
                                                q.a(context, "http://hmma.baidu.com/app.gif", jSONObject2, 50000, 50000);
                                                z2 = true;
                                            } catch (Exception e3) {
                                                com.baidu.mobstat.a.b.a("stat", e3);
                                                z2 = false;
                                            }
                                            if (z2) {
                                                a(false);
                                                this.u = new JSONArray();
                                                this.s = new JSONArray();
                                                this.r = new JSONArray();
                                                b(context);
                                                m.a().b();
                                                g.a().d(context);
                                            }
                                        } catch (JSONException e4) {
                                            com.baidu.mobstat.a.b.a("stat", e4);
                                            return;
                                        }
                                    }
                                }
                            } catch (JSONException e5) {
                                com.baidu.mobstat.a.b.a("stat", e5);
                                return;
                            }
                        }
                    } catch (JSONException e6) {
                        com.baidu.mobstat.a.b.a("stat", e6.toString());
                        return;
                    }
                }
            } catch (JSONException e7) {
                com.baidu.mobstat.a.b.a("stat", e7);
                return;
            }
        }
        com.baidu.mobstat.a.b.a("stat", "sendLogData() end.");
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(String str, String str2, int i2, long j2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("i", str);
            jSONObject.put("l", str2);
            jSONObject.put("c", i2);
            jSONObject.put("t", j2);
            b(jSONObject, false);
            com.baidu.mobstat.a.b.a("stat", "put event:" + jSONObject.toString());
        } catch (JSONException e2) {
            com.baidu.mobstat.a.b.a("stat", e2);
        }
    }

    public void a(JSONObject jSONObject, boolean z) {
        if (jSONObject != null && !z) {
            int length = jSONObject.toString().getBytes().length;
            com.baidu.mobstat.a.b.a("stat", "putSession:addSize is:", Integer.valueOf(length));
            if (length + this.p > 204800) {
                com.baidu.mobstat.a.b.a("stat", "putSession: size is full!");
                return;
            }
        }
        synchronized (this.r) {
            try {
                this.r.put(this.r.length(), jSONObject);
            } catch (JSONException e2) {
                com.baidu.mobstat.a.b.a("stat", e2);
            }
        }
        return;
    }

    public synchronized void b() {
        if (q.length() <= 0) {
            try {
                q.put("o", a == null ? StringUtils.EMPTY : a);
                q.put("s", this.b == null ? StringUtils.EMPTY : this.b);
                q.put("k", this.c == null ? StringUtils.EMPTY : this.c);
                q.put("v", "2.2");
                q.put("a", this.d);
                q.put("n", this.e == null ? StringUtils.EMPTY : this.e);
                q.put("d", this.f == null ? StringUtils.EMPTY : this.f);
                q.put("w", this.g);
                q.put("h", this.h);
                q.put("c", this.i == null ? StringUtils.EMPTY : this.i);
                q.put("op", this.j == null ? StringUtils.EMPTY : this.j);
                q.put("m", this.k == null ? StringUtils.EMPTY : this.k);
                q.put("cl", this.l);
                q.put("gl", this.m == null ? StringUtils.EMPTY : this.m);
                q.put("wl", this.n == null ? StringUtils.EMPTY : this.n);
                q.put("l", this.o == null ? StringUtils.EMPTY : this.o);
                q.put("t", System.currentTimeMillis());
                q.put("sq", 0);
                com.baidu.mobstat.a.b.a("stat", q.toString());
            } catch (JSONException e2) {
                com.baidu.mobstat.a.b.a("stat", "header ini error");
                throw new RuntimeException("header ini error");
            }
        }
    }

    public void b(Context context) {
        com.baidu.mobstat.a.b.a("stat", "flush cache to __local_stat_cache.json");
        JSONObject jSONObject = new JSONObject();
        try {
            synchronized (this.r) {
                jSONObject.put("pr", new JSONArray(this.r.toString()));
            }
            synchronized (this.s) {
                jSONObject.put("ev", new JSONArray(this.s.toString()));
            }
            synchronized (this.u) {
                jSONObject.put("ex", new JSONArray(this.u.toString()));
            }
        } catch (JSONException e2) {
            com.baidu.mobstat.a.b.a("stat", "flushLogWithoutHeader() construct cache error");
        }
        String jSONObject2 = jSONObject.toString();
        if (d()) {
            com.baidu.mobstat.a.b.a("stat", "cache.json exceed 204800B,stop flush.");
            return;
        }
        int length = jSONObject2.getBytes().length;
        if (length >= 204800) {
            a(true);
            return;
        }
        this.p = length;
        com.baidu.mobstat.a.b.a("stat", "flush:cacheFileSize is:" + this.p);
        a.a(false, context, "__local_stat_cache.json", jSONObject2, false);
    }

    public void b(String str) {
        this.i = str;
    }

    public void b(JSONObject jSONObject, boolean z) {
        if (jSONObject != null && !z) {
            int length = jSONObject.toString().getBytes().length;
            com.baidu.mobstat.a.b.a("stat", "putEvent:eventSize is:", Integer.valueOf(length));
            if (length + this.p > 204800) {
                com.baidu.mobstat.a.b.a("stat", "putEvent: size is full!");
                return;
            }
        }
        try {
            String string = jSONObject.getString("i");
            String string2 = jSONObject.getString("l");
            long j2 = jSONObject.getLong("t") / DateUtils.MILLIS_PER_HOUR;
            synchronized (this.s) {
                int length2 = this.s.length();
                int i2 = length2;
                for (int i3 = 0; i3 < length2; i3++) {
                    try {
                        JSONObject jSONObject2 = this.s.getJSONObject(i3);
                        String string3 = jSONObject2.getString("i");
                        String string4 = jSONObject2.getString("l");
                        if (jSONObject2.getLong("t") / DateUtils.MILLIS_PER_HOUR == j2) {
                            if (string3.equals(string) && string4.equals(string2)) {
                                int i4 = jSONObject2.getInt("c") + jSONObject.getInt("c");
                                try {
                                    jSONObject2.remove("c");
                                    jSONObject2.put("c", i4);
                                    i2 = i3;
                                } catch (JSONException e2) {
                                    e = e2;
                                    i2 = i3;
                                    com.baidu.mobstat.a.b.a("stat", e);
                                }
                            }
                        }
                    } catch (JSONException e3) {
                        e = e3;
                        com.baidu.mobstat.a.b.a("stat", e);
                    }
                }
                if (i2 >= length2) {
                    try {
                        this.s.put(length2, jSONObject);
                    } catch (JSONException e4) {
                        com.baidu.mobstat.a.b.a("stat", e4);
                    }
                }
            }
        } catch (JSONException e5) {
            com.baidu.mobstat.a.b.a("stat", e5);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.a.a.a(boolean, android.content.Context, java.lang.String):java.lang.String
     arg types: [int, android.content.Context, java.lang.String]
     candidates:
      com.baidu.mobstat.a.a.a(java.lang.String, java.lang.String, boolean):void
      com.baidu.mobstat.a.a.a(boolean, android.content.Context, java.lang.String):java.lang.String */
    public void c(Context context) {
        com.baidu.mobstat.a.b.a("stat", "LoadLastSession()");
        if (context != null && a.c(context, "__local_last_session.json")) {
            String a2 = a.a(false, context, "__local_last_session.json");
            if (a2.equals(StringUtils.EMPTY)) {
                com.baidu.mobstat.a.b.a("stat", "loadLastSession(): last_session.json file not found.");
                return;
            }
            a.a(false, context, "__local_last_session.json", new JSONObject().toString(), false);
            c(a2);
            b(context);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.b.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.baidu.mobstat.b.a(java.lang.String, android.content.Context):java.lang.String
      com.baidu.mobstat.b.a(long, java.lang.String):void
      com.baidu.mobstat.b.a(android.content.Context, boolean):void
      com.baidu.mobstat.b.a(org.json.JSONObject, boolean):void */
    public void c(String str) {
        if (!str.equals("{}") && !str.equals(StringUtils.EMPTY)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                a(jSONObject, false);
                com.baidu.mobstat.a.b.a("stat", "Load last session:" + jSONObject);
            } catch (JSONException e2) {
                com.baidu.mobstat.a.b.a("stat", "putSession()" + e2);
            }
        }
    }

    public void c(JSONObject jSONObject, boolean z) {
        if (jSONObject != null && !z) {
            int length = jSONObject.toString().getBytes().length;
            com.baidu.mobstat.a.b.a("stat", "putException:addSize is:", Integer.valueOf(length));
            if (length + this.p > 204800) {
                com.baidu.mobstat.a.b.a("stat", "putException: size is full!");
                return;
            }
        }
        synchronized (this.u) {
            try {
                this.u.put(this.u.length(), jSONObject);
            } catch (JSONException e2) {
                com.baidu.mobstat.a.b.a("stat", e2);
            }
        }
        return;
    }

    public boolean c() {
        return this.r.length() == 0 && this.s.length() == 0 && this.u.length() == 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.a.a.a(boolean, android.content.Context, java.lang.String):java.lang.String
     arg types: [int, android.content.Context, java.lang.String]
     candidates:
      com.baidu.mobstat.a.a.a(java.lang.String, java.lang.String, boolean):void
      com.baidu.mobstat.a.a.a(boolean, android.content.Context, java.lang.String):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.b.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.baidu.mobstat.b.a(java.lang.String, android.content.Context):java.lang.String
      com.baidu.mobstat.b.a(long, java.lang.String):void
      com.baidu.mobstat.b.a(android.content.Context, boolean):void
      com.baidu.mobstat.b.a(org.json.JSONObject, boolean):void */
    public void d(Context context) {
        if (context != null && a.c(context, "__local_stat_cache.json")) {
            String a2 = a.a(false, context, "__local_stat_cache.json");
            if (a2.equals(StringUtils.EMPTY)) {
                com.baidu.mobstat.a.b.a("stat", "stat_cache file not found.");
                return;
            }
            com.baidu.mobstat.a.b.a("stat", "loadStatData, ");
            try {
                this.p = a2.getBytes().length;
                com.baidu.mobstat.a.b.a("stat", "load Stat Data:cacheFileSize is:" + this.p);
                JSONObject jSONObject = new JSONObject(a2);
                com.baidu.mobstat.a.b.a("stat", "Load cache:" + a2);
                long currentTimeMillis = System.currentTimeMillis();
                JSONArray jSONArray = jSONObject.getJSONArray("pr");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    if (currentTimeMillis - jSONObject2.getLong("s") <= 604800000) {
                        a(jSONObject2, true);
                    }
                }
                JSONArray jSONArray2 = jSONObject.getJSONArray("ev");
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
                    if (currentTimeMillis - jSONObject3.getLong("t") <= 604800000) {
                        b(jSONObject3, true);
                    }
                }
                JSONArray jSONArray3 = jSONObject.getJSONArray("ex");
                for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                    JSONObject jSONObject4 = jSONArray3.getJSONObject(i4);
                    if (currentTimeMillis - jSONObject4.getLong("t") <= 604800000) {
                        c(jSONObject4, true);
                    }
                }
            } catch (JSONException e2) {
                com.baidu.mobstat.a.b.a("stat", "Load stat data error:" + e2);
            }
        }
    }
}
